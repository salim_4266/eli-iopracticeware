VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPaymentsNew 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Payments"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   FillColor       =   &H0077742D&
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtAdjAmt 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2280
      MaxLength       =   10
      TabIndex        =   31
      Top             =   1440
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CheckBox chkInclude 
      BackColor       =   &H0077742D&
      Caption         =   "Include comment on statements"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5040
      TabIndex        =   12
      Top             =   1800
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.TextBox txtAdj 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6120
      MaxLength       =   64
      TabIndex        =   11
      Top             =   1440
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.TextBox txtPayAmt 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   10
      TabIndex        =   10
      Top             =   1440
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.TextBox txtDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      MaxLength       =   10
      TabIndex        =   3
      Top             =   960
      Width           =   2055
   End
   Begin VB.TextBox txtCheck 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   20
      TabIndex        =   1
      Top             =   960
      Width           =   3975
   End
   Begin VB.TextBox txtAmount 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   10
      TabIndex        =   0
      Top             =   480
      Width           =   2055
   End
   Begin VB.ComboBox lstMethod 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   6840
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   480
      Width           =   2415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFinData 
      Height          =   990
      Left            =   5400
      TabIndex        =   5
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdComment 
      Height          =   990
      Left            =   8280
      TabIndex        =   6
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":01E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   240
      TabIndex        =   7
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":03CF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10680
      TabIndex        =   8
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":05AE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   990
      Left            =   3960
      TabIndex        =   9
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":078D
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   4215
      Left            =   120
      TabIndex        =   4
      Top             =   2640
      Visible         =   0   'False
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   7435
      _Version        =   393216
      Cols            =   11
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid2 
      Height          =   3855
      Left            =   3240
      TabIndex        =   29
      Top             =   2640
      Visible         =   0   'False
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   6800
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   11098385
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDist 
      Height          =   735
      Left            =   9840
      TabIndex        =   32
      Top             =   120
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":0976
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNote 
      Height          =   990
      Left            =   6840
      TabIndex        =   33
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsNew.frx":0B62
   End
   Begin VB.Label lblPatient 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   30
      Top             =   120
      Width           =   1710
   End
   Begin VB.Label lblBalAmt 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   2520
      TabIndex        =   28
      Top             =   7440
      Width           =   450
   End
   Begin VB.Label lblBal 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Open Balance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   2520
      TabIndex        =   27
      Top             =   7080
      Width           =   1455
   End
   Begin VB.Label lblClmAmt 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   240
      TabIndex        =   26
      Top             =   7440
      Width           =   450
   End
   Begin VB.Label lblClm 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Claim Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   240
      TabIndex        =   25
      Top             =   7080
      Width           =   1425
   End
   Begin VB.Label lblInst 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Use Arrow Keys to move from cell to cell"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   6480
      TabIndex        =   24
      Top             =   2280
      Visible         =   0   'False
      Width           =   5325
   End
   Begin VB.Label lblPrint 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading ..."
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   10560
      TabIndex        =   23
      Top             =   1920
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblTAdjs 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   9240
      TabIndex        =   22
      Top             =   7440
      Width           =   450
   End
   Begin VB.Label lblTAdj 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Adjustments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   9240
      TabIndex        =   21
      Top             =   7080
      Width           =   1815
   End
   Begin VB.Label lblTPaid 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   7440
      TabIndex        =   20
      Top             =   7440
      Width           =   450
   End
   Begin VB.Label lblTPay 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Paid"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   7440
      TabIndex        =   19
      Top             =   7080
      Width           =   1035
   End
   Begin VB.Label lblAdj 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Comment"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5010
      TabIndex        =   18
      Top             =   1440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Payment Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5160
      TabIndex        =   17
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label lblAmountLeft 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Amount to disburse"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   16
      Top             =   2160
      Width           =   2505
   End
   Begin VB.Label lblCheck 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Check #"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   15
      Top             =   960
      Width           =   945
   End
   Begin VB.Label lblAmount 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Amount"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   945
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Method"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   5760
      TabIndex        =   13
      Top             =   480
      Width           =   945
   End
End
Attribute VB_Name = "frmPaymentsNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private PrintReceiptOn As Boolean
Private PatientId As Long
Private ReceivableId As Long
Private CurrentOpenBalance As Single
Private UserId As Long
Private GetCheckOn As Boolean
Private FirstTimeSet As Boolean
Private PaymentMethod As String
Private AdjustmentCode As String
Private AllOn As Boolean
Private CheckId As Long
Private PatPol1 As Long
Private PatPol2 As Long
Private PatPolId As Long
Private CurrentRow As Integer
Private CurrentCol As Integer
Private RunningCheckBalance As String

Private Sub cmdDist_Click()
Dim i As Integer
Dim TempAmt As Single
Dim MaxAmt As Single
Dim OpnBal As Single
If (Trim(txtAmount.Text) = "") Then
    frmEventMsgs.Header = "Must Enter an Amount"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtAmount.SetFocus
    Exit Sub
ElseIf (Trim(lstMethod.ListIndex) < 0) Then
    frmEventMsgs.Header = "Must Enter a Payment Method"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    lstMethod.SetFocus
    Exit Sub
ElseIf (Trim(txtDate.Text) = "") Then
    frmEventMsgs.Header = "Must Enter a Payment Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtDate.SetFocus
    Exit Sub
End If
MaxAmt = val(Trim(txtAmount.Text))
OpnBal = val(Trim(lblBalAmt.Caption))
RunningCheckBalance = Trim(Str(MoneyLeft(0)))
lblAmountLeft.Tag = RunningCheckBalance
Call SetDisburseRemaining
If (OpnBal > 0) Then
    For i = 1 To MSFlexGrid1.Rows - 1
        If (Trim(MSFlexGrid1.TextMatrix(i, 4)) <> "") Then
            If ((val(RunningCheckBalance) - val(MSFlexGrid1.TextMatrix(i, 4))) > 0) Then
                MSFlexGrid1.TextMatrix(i, 6) = MSFlexGrid1.TextMatrix(i, 4)
                Call CalculateBalance(i)
                RunningCheckBalance = lblAmountLeft.Tag
            ElseIf (val(RunningCheckBalance) > 0) Then
                MSFlexGrid1.TextMatrix(i, 6) = RunningCheckBalance
                Call CalculateBalance(i)
                RunningCheckBalance = lblAmountLeft.Tag
            End If
        End If
    Next i
End If
End Sub

Private Sub cmdFinData_Click()
Dim AInsId As Long
Dim RcvId As Long
Dim TheDate As String, InvDt As String
Dim ApplTemp As ApplicationTemplates
If (CurrentRow > 0) Then
    RcvId = 0
    Set ApplTemp = New ApplicationTemplates
    If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 1)) <> "") Then
        RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 1)), AInsId, InvDt)
    End If
    Set ApplTemp = Nothing
'''''    frmPayments.AccessType = ""
'''''    frmPayments.Whom = True
'''''    frmPayments.CurrentAction = ""
'''''    frmPayments.PatientId = PatientId
'''''    frmPayments.ReceivableId = RcvID
'''''    If (frmPayments.LoadPayments(True, "")) Then
'''''        frmPayments.Show 1
'''''        If (frmPayments.CurrentAction <> "Home") And (frmPayments.CurrentAction <> "BP") Then
'''''            Call LoadPaymentsNew(PatientId, ReceivableId, AllOn)
'''''        End If
'''''    Else
'''''        frmEventMsgs.Header = "No Financial Data"
'''''        frmEventMsgs.AcceptText = ""
'''''        frmEventMsgs.RejectText = "Ok"
'''''        frmEventMsgs.CancelText = ""
'''''        frmEventMsgs.Other0Text = ""
'''''        frmEventMsgs.Other1Text = ""
'''''        frmEventMsgs.Other2Text = ""
'''''        frmEventMsgs.Other3Text = ""
'''''        frmEventMsgs.Other4Text = ""
'''''        frmEventMsgs.Show 1
'''''    End If
Dim PatientDemographics As New PatientDemographics
PatientDemographics.PatientId = PatientId
Call PatientDemographics.DisplayPatientFinancialScreen
Set PatientDemographics = Nothing
Call LoadPaymentsNew(PatientId, ReceivableId, AllOn)
End If
End Sub

Private Sub cmdHome_Click()
MyPracticeRepositoryCmd.CommandType = adCmdText
Unload frmPaymentsNew
End Sub

Private Sub cmdApply_Click()
Dim i As Integer
Dim VerifyOn As Boolean
Dim ApplTemp  As ApplicationTemplates
VerifyOn = False
For i = 1 To MSFlexGrid1.Rows - 1
    If (val(Trim(MSFlexGrid1.TextMatrix(i, 6))) <> 0) Then
        If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
            VerifyOn = True
            Exit For
        End If
    End If
    If (val(Trim(MSFlexGrid1.TextMatrix(i, 7))) <> 0) Then
        If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
            VerifyOn = True
            Exit For
        End If
    End If
Next i
If (VerifyOn) Then
    Call CompleteCurrentPatient(PatientId)
    Dim ApplList As New ApplicationAIList
    Dim ApptId As Long
    ApptId = ApplList.ApplGetAppt(ReceivableId)
    Set ApplList = Nothing
    Dim TelerikReporting As New Reporting
    Dim ParamArgs(0) As Variant
            
    ParamArgs(0) = Array("EncounterId", Str(ApptId))
    'Invoke Patient Glasses telerik Report.
    Call TelerikReporting.ViewReport("Glasses Receipt", ParamArgs)
End If
MyPracticeRepositoryCmd.CommandType = adCmdText
Unload frmPaymentsNew
End Sub

Private Sub cmdAll_Click()
AllOn = Not (AllOn)
If (AllOn) Then
    cmdAll.Text = "Show Current Items"
Else
    cmdAll.Text = "Show All Items"
End If
Call LoadPaymentsNew(PatientId, ReceivableId, AllOn)
lblInst.Visible = True
MSFlexGrid1.SetFocus
MSFlexGrid1.RowSel = 1
CurrentRow = 1
End Sub

Private Sub cmdComment_Click()
If (PatientId > 0) And (CurrentRow > 0) Then
    If Not (lblAdj.Visible) Then
        lblAdj.Visible = True
        txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
        txtAdj.Visible = True
        chkInclude.Visible = True
        If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
            chkInclude.Value = 1
        Else
            chkInclude.Value = 0
        End If
    End If
    txtAdj.SetFocus
End If
End Sub

Public Function LoadPaymentsNew(PatId As Long, RcvId As Long, AllOn As Boolean) As Boolean
Dim Bs As Boolean
Dim CurRow As Integer
Dim PatName As String, Temp As String
Dim TheDate As String, ADate As String
Dim ClmAmt As Single, ClmBal As Single
Dim RetPat As Patient
Dim RetRcv As PatientReceivables
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
LoadPaymentsNew = False
CurrentOpenBalance = 0
If (PatId > 0) And (RcvId > 0) Then
    ReceivableId = RcvId
    PatientId = PatId
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        PatName = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
    End If
    Set RetPat = Nothing
    lblPatient.Caption = PatName
    UserId = UserLogin.iId
    CheckId = 0
    RunningCheckBalance = ""
    AdjustmentCode = "X"
    PaymentMethod = "K"
    lstMethod.Clear
    lstMethod.ListIndex = -1
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetCodes("PayableType", True, lstMethod)
    Set ApplList = Nothing
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    txtDate.Text = ADate
    cmdComment.Visible = False
    lblInst.Visible = False
    FirstTimeSet = False
    GetCheckOn = False
    CurRow = CurrentRow
    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
    lblAmountLeft.Tag = Trim(RunningCheckBalance)
    lblAmountLeft.Visible = True
    lblAdj.Visible = False
    txtAdj.Visible = False
    chkInclude.Value = 0
    chkInclude.Visible = False
    lblPrint.Caption = "Loading Grid Entries"
    lblPrint.Visible = True
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplGrid = MSFlexGrid1
    Set ApplList.ApplGrid1 = MSFlexGrid2
    If (AllOn) Then
        LoadPaymentsNew = ApplList.ApplLoadPaymentsbyServicesbyReceivable(PatientId, 0, True)
    Else
        LoadPaymentsNew = ApplList.ApplLoadPaymentsbyServicesbyReceivable(PatientId, ReceivableId, False)
    End If
    Set ApplList = Nothing
    
    ClmAmt = 0
    ClmBal = 0
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        ClmAmt = Round(RetRcv.ReceivableAmount, 2)
        ClmBal = Round(RetRcv.ReceivableBalance, 2)
    End If
    Set RetRcv = Nothing
    CurrentOpenBalance = ClmBal
    
    Call DisplayDollarAmount(Trim(Str(ClmAmt)), Temp)
    lblClmAmt.Caption = Trim(Temp)
    Call DisplayDollarAmount(Trim(Str(ClmBal)), Temp)
    lblBalAmt.Caption = Trim(Temp)
    lblPrint.Visible = False
    
    If (LoadPaymentsNew) Then
        Call SetGridTotals
        CurrentRow = CurRow
        If (CurrentRow < 0) Or (CurrentRow > 12) Or (CurrentRow > MSFlexGrid1.Row) Then
            CurrentRow = 1
        End If
        MSFlexGrid1.RowSel = CurrentRow
        lblInst.Visible = True
        cmdComment.Visible = False
        cmdAll.Visible = True
        cmdNote.Visible = True
        MSFlexGrid1.RowSel = 1
        MSFlexGrid1.Visible = True
        CurrentRow = 1
    End If
End If
End Function

Private Sub cmdNote_Click()
Dim IPlan As Long, ApptId As Long
Dim ADate As String, AName As String
Dim InvId As String, SrvId As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
InvId = ""
frmNotes.NoteId = 0
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = 0
frmNotes.CurrentAction = ""
frmNotes.SetTo = "B"
frmNotes.EyeContext = ""
frmNotes.MaintainOn = True
Set ApplList = New ApplicationAIList
ApptId = ApplList.ApplGetAppt(ReceivableId)
Set ApplList = Nothing
Set ApplTemp = New ApplicationTemplates
Call ApplTemp.ApplGetInvoice(ReceivableId, InvId, ADate, IPlan, ApptId)
Set ApplTemp = Nothing
frmNotes.AppointmentId = ApptId
frmNotes.SystemReference = "R" + InvId
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Sub Form_Load()
AllOn = False
End Sub

Private Sub lstMethod_Click()
If (lstMethod.ListIndex >= 0) Then
    PaymentMethod = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
End If
End Sub

Private Sub txtCheck_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    lstMethod.SetFocus
End If
End Sub

Private Sub txtAmount_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
Else
    Call DisplayDollarAmount(txtAmount.Text, Temp)
    txtAmount.Text = Trim(Temp)
    RunningCheckBalance = Trim(Temp)
    Call DisplayDollarAmount(Trim(RunningCheckBalance), Temp)
    txtAmount.Text = Trim(Temp)
    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(Temp)
    lblAmountLeft.Tag = Trim(RunningCheckBalance)
    txtCheck.SetFocus
End If
End Sub

Private Sub txtAmount_Validate(Cancel As Boolean)
Call txtAmount_KeyPress(13)
End Sub

Private Sub txtDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDate.SetFocus
        txtDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtDate, "D")
        txtDate.SetFocus
    End If
End If
End Sub

Private Sub txtDate_Validate(Cancel As Boolean)
If (Trim(txtDate.Text) <> "") Then
    Call txtDate_KeyPress(13)
End If
End Sub

Private Sub MSFlexGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
MSFlexGrid1.AllowBigSelection = False
If (KeyCode = vbKeyRight) And (Shift = 0) Then
    CurrentRow = MSFlexGrid1.RowSel
    CurrentCol = MSFlexGrid1.ColSel
ElseIf (KeyCode = vbKeyLeft) And (Shift = 0) Then
    CurrentRow = MSFlexGrid1.RowSel
    CurrentCol = MSFlexGrid1.ColSel
End If
End Sub

Private Sub MSFlexGrid1_KeyPress(KeyAscii As Integer)
If (CurrentRow <> MSFlexGrid1.RowSel) Then
    CurrentRow = MSFlexGrid1.RowSel
End If
MSFlexGrid1.AllowBigSelection = True
MSFlexGrid1.Highlight = flexHighlightAlways
MSFlexGrid1.AllowBigSelection = False
CurrentCol = MSFlexGrid1.ColSel
If (MSFlexGrid1.TextMatrix(CurrentRow, 1) <> "") Then
    If (KeyAscii = 13) Or (KeyAscii = 10) Then
        If ((CurrentCol > 4) And (CurrentCol < 8)) Then
            Call MsFlexGrid1_LeaveCell
            Call MsFlexGrid1_EnterCell
        Else
            Call MSFlexGrid1_Click
        End If
    Else
        If (CurrentCol > 4) And (CurrentCol < 8) Then
            If (KeyAscii >= 32) And (KeyAscii < 125) And (KeyAscii <> 39) And (KeyAscii <> 37) Then
                If (CurrentCol = 6) Then
                    Call txtPayAmt_KeyPress(KeyAscii)
                ElseIf (CurrentCol = 7) Then
                    Call txtAdjAmt_KeyPress(KeyAscii)
                End If
                If (KeyAscii <> 0) Then
                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) + Chr(KeyAscii)
                End If
            ElseIf (KeyAscii = 8) Or (KeyAscii = 126) Then
                If (Len(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) > 1) Then
                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = Left(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol), Len(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) - 1)
                Else
                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
                End If
            Else
                Exit Sub
            End If
            If (CurrentCol = 6) Then
                txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
            ElseIf (CurrentCol = 7) Then
                txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
            End If
        End If
    End If
Else
    KeyAscii = 0
End If
End Sub

Private Sub MSFlexGrid1_Click()
Dim i As Integer
Dim Amt As Single, Chg As Single, ATotal As Single
Dim Paid As Single, Adjs As Single
Dim Temp As String
Dim ProcessBalance As Boolean
ProcessBalance = False
lblAdj.Visible = False
txtAdj.Visible = False
chkInclude.Visible = False
MSFlexGrid1.AllowBigSelection = False
If (MSFlexGrid1.ColSel <> 13) And (MSFlexGrid1.ColSel <> 0) Then
    Call MsFlexGrid1_EnterCell
    Exit Sub
End If
If (MSFlexGrid1.Row > 0) And (Trim(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, 1)) <> "") Then
    If (CurrentRow > 0) Then
        If (Trim(txtPayAmt.Text) <> "") Then
            MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
            ProcessBalance = True
            FirstTimeSet = True
        End If
        If (Trim(txtAdjAmt.Text) <> "") Then
            MSFlexGrid1.TextMatrix(CurrentRow, 7) = Trim(txtAdjAmt.Text)
            ProcessBalance = True
            FirstTimeSet = True
        End If
        If (Trim(txtAdj.Text) <> "") Then
            MSFlexGrid2.TextMatrix(CurrentRow, 1) = Trim(txtAdj.Text)
            MSFlexGrid2.TextMatrix(CurrentRow, 2) = "F"
            If (chkInclude.Value = 1) Then
                MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T"
            End If
        End If
        If (ProcessBalance) Then
            Chg = val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 3)))
            If (Chg <> val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 4)))) Then
                Chg = val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 4)))
            End If
            Paid = val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)))
            Adjs = val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)))
            ATotal = Chg - (Paid + Adjs)
            Call DisplayDollarAmount(Trim(Str(ATotal)), Temp)
            MSFlexGrid1.TextMatrix(CurrentRow, 8) = Trim(Temp)
        End If
    End If
    If (val(Trim(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, 4))) > 0) Then
        CurrentRow = MSFlexGrid1.Row
        txtAdj.Text = ""
        chkInclude.Value = 0
        txtPayAmt.Text = ""
        txtAdjAmt.Text = ""
        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)) <> "") Then
            txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6))
        End If
        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)) <> "") Then
            txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))
        End If
' Display Comment
        lblAdj.Visible = True
        txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
        txtAdj.Visible = True
        chkInclude.Visible = True
        If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
            chkInclude.Value = 1
        Else
            chkInclude.Value = 0
        End If
    Else
        frmEventMsgs.Header = "No Open Balance"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    txtAdj.Text = ""
    txtPayAmt.Text = ""
    txtAdjAmt.Text = ""
End If
Call SetDisburseRemaining
End Sub

Private Sub MsFlexGrid1_EnterCell()
If (MSFlexGrid1.TextMatrix(CurrentRow, 1) = "") Then
    Exit Sub
End If
MSFlexGrid1.AllowBigSelection = False
CurrentCol = MSFlexGrid1.ColSel
CurrentRow = MSFlexGrid1.RowSel
' Display Comment
lblAdj.Visible = True
txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
txtAdj.Visible = True
chkInclude.Visible = True
If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
    chkInclude.Value = 1
Else
    chkInclude.Value = 0
End If
If (CurrentCol = 6) Then
    FirstTimeSet = True
    txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
ElseIf (CurrentCol = 7) Then
    FirstTimeSet = True
    txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
ElseIf (CurrentCol = 0) Then
    MSFlexGrid1_Click
End If
End Sub

Private Sub MsFlexGrid1_LeaveCell()
If (MSFlexGrid1.TextMatrix(CurrentRow, 1) = "") Then
    Exit Sub
End If
MSFlexGrid1.AllowBigSelection = True
CurrentCol = MSFlexGrid1.ColSel
CurrentRow = MSFlexGrid1.RowSel
' Display Comment
lblAdj.Visible = True
txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
txtAdj.Visible = True
chkInclude.Visible = True
If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
    chkInclude.Value = 1
Else
    chkInclude.Value = 0
End If
If (CurrentCol = 6) Then
    Call txtPayAmt_KeyPress(13)
    If (Trim(txtPayAmt.Text) = "") Then
        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
    End If
ElseIf (CurrentCol = 7) Then
    Call txtAdjAmt_KeyPress(13)
    If (Trim(txtAdjAmt.Text) = "") Then
        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
    End If
End If
MSFlexGrid1.Highlight = flexHighlightAlways
MSFlexGrid1.AllowBigSelection = False
End Sub

Private Sub txtPayAmt_Validate(Cancel As Boolean)
If (Trim(txtPayAmt.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6))) Then
    Call txtPayAmt_KeyPress(13)
End If
End Sub

Private Sub txtPayAmt_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    Else
        If (FirstTimeSet) Then
            txtPayAmt.Text = ""
            MSFlexGrid1.TextMatrix(CurrentRow, 6) = ""
            FirstTimeSet = False
        End If
    End If
Else
    Call DisplayDollarAmount(txtPayAmt.Text, Temp)
    txtPayAmt.Text = Trim(Temp)
    If (MoneyLeft(CurrentRow) < val(Trim(txtPayAmt.Text))) Then
        frmEventMsgs.Header = "Insufficient funds to apply"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPayAmt.Text = ""
        If (txtPayAmt.Visible) Then
            txtPayAmt.SetFocus
        End If
    End If
    If Not (VerifyAgainstOpenBalance(CurrentRow, val(Trim(txtPayAmt.Text)), val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))))) And (val(Trim(txtPayAmt.Text)) <> 0) Then
        frmEventMsgs.Header = "Payment exceeds Open Balance"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Apply"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 4) Then
            txtPayAmt.Text = ""
            If (txtPayAmt.Visible) Then
                txtPayAmt.SetFocus
            End If
        Else
            If (CurrentRow > 0) Then
                MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
                Call CalculateBalance(CurrentRow)
            End If
            If (txtAdjAmt.Visible) Then
                txtAdjAmt.SetFocus
            End If
        End If
    Else
        If (CurrentRow > 0) Then
            MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
            Call CalculateBalance(CurrentRow)
        End If
        If (txtAdjAmt.Visible) Then
            txtAdjAmt.SetFocus
        End If
    End If
End If
End Sub

Private Sub txtAdjAmt_Validate(Cancel As Boolean)
If (Trim(txtAdjAmt.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))) Then
    Call txtAdjAmt_KeyPress(13)
End If
End Sub

Private Sub txtAdjAmt_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    Else
        If (FirstTimeSet) Then
            txtAdjAmt.Text = ""
            MSFlexGrid1.TextMatrix(CurrentRow, 7) = ""
            FirstTimeSet = False
        End If
    End If
Else
    Call DisplayDollarAmount(txtAdjAmt.Text, Temp)
    txtAdjAmt.Text = Trim(Temp)
    If (CurrentRow > 0) Then
        MSFlexGrid1.TextMatrix(CurrentRow, 7) = Trim(txtAdjAmt.Text)
        Call CalculateBalance(CurrentRow)
    End If
End If
End Sub

Private Sub txtAdj_Validate(Cancel As Boolean)
Call txtAdj_KeyPress(13)
End Sub

Private Sub txtAdj_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If (CurrentRow > 0) Then
        MSFlexGrid2.TextMatrix(CurrentRow, 1) = Trim(txtAdj.Text)
    End If
    KeyAscii = 0
End If
End Sub

Private Sub chkInclude_Click()
If (CurrentRow > 0) Then
    MSFlexGrid2.TextMatrix(CurrentRow, 2) = "F"
    If (chkInclude.Value = 1) Then
        MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T"
    End If
End If
End Sub

Private Function MoneyLeft(ARow As Integer) As Single
Dim i As Integer
Dim Amt As Single
Amt = 0
MoneyLeft = 0
If (MSFlexGrid1.Visible) Then
    For i = 0 To MSFlexGrid1.Rows - 1
        If (i <> ARow) Then
            Amt = Amt + val(Trim(MSFlexGrid1.TextMatrix(i, 6)))
        End If
    Next i
End If
MoneyLeft = Int(((val(Trim(txtAmount.Text)) - Amt) + 0.004) * 100) / 100
End Function

Private Function VerifyAgainstOpenBalance(ARow As Integer, Amt1 As Single, Amt2 As Single) As Boolean
VerifyAgainstOpenBalance = False
If (val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))) >= (Amt1 + Amt2)) Then
    VerifyAgainstOpenBalance = True
End If
End Function

Private Function CompleteCurrentPatient(PatId As Long) As Boolean
Dim ComOn As Boolean
Dim z As Integer
Dim i As Integer, j As Integer
Dim p1 As Long, ItemId As Long
Dim RcvId As Long, InsrId As Long, AInsId As Long
Dim k As Single, PAmt As Single
Dim InvDt As String
Dim Srv As String, Amt As String, Cmt As String
Dim InvId As String, CurInvId As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
CompleteCurrentPatient = False
If (PatId > 0) Then
    If (Trim(PaymentMethod) = "") Then
        PaymentMethod = "K"
    End If
    CurInvId = ""
    k = MoneyLeft(0)
    If (MSFlexGrid1.Visible) Then
' Payments
        ComOn = False
        lblPrint.Caption = "Posting ..."
        lblPrint.Visible = True
        DoEvents
        Call DisplayDollarAmount(Trim(Str(k)), RunningCheckBalance)
        lblAmountLeft.Tag = Trim(RunningCheckBalance)
        lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(lblAmountLeft.Tag)
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplPostCheckRecord(CheckId, PatientId, "P", txtAmount.Text, RunningCheckBalance, txtCheck.Text, txtDate.Text, txtDate.Text)
        For i = 1 To MSFlexGrid1.Rows - 1
            If (val(Trim(MSFlexGrid1.TextMatrix(i, 6))) <> 0) Then
                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
                    AInsId = 0
                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), AInsId, InvDt)
                    If (RcvId > 0) Then
                        ItemId = val(Trim(MSFlexGrid2.TextMatrix(i, 3)))
                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
                        j = InStrPS(Srv, " ")
                        If (j > 0) Then
                            Srv = Left(Srv, j - 1)
                        End If
                        Amt = Trim(MSFlexGrid1.TextMatrix(i, 6))
                        Cmt = Trim(MSFlexGrid2.TextMatrix(i, 1))
                        If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
                            ComOn = True
                        End If
                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "P", AInsId, Amt, txtDate.Text, "P", PaymentMethod, "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, "", CheckId, "", "", 0)
                    End If
                End If
            End If
' Adjustments
            If (val(Trim(MSFlexGrid1.TextMatrix(i, 7))) <> 0) Then
                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
                    AInsId = 0
                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), AInsId, InvDt)
                    If (RcvId > 0) Then
                        ItemId = val(Trim(MSFlexGrid2.TextMatrix(i, 3)))
                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
                        j = InStrPS(Srv, " ")
                        If (j > 0) Then
                            Srv = Left(Srv, j - 1)
                        End If
                        Amt = MSFlexGrid1.TextMatrix(i, 7)
                        Cmt = MSFlexGrid2.TextMatrix(i, 1)
                        If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
                            ComOn = True
                        End If
                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "P", AInsId, Amt, txtDate.Text, AdjustmentCode, PaymentMethod, "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, "", CheckId, "", "", 0)
                    End If
                End If
            End If
        Next i
' Queue to Send if Okay to do this
        lblPrint.Caption = "Batching ..."
        lblPrint.Visible = True
        PAmt = 0
        InsrId = 0
        CurInvId = ""
        Set ApplList = New ApplicationAIList
        For i = 1 To MSFlexGrid1.Rows - 1
            InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
            If (CurInvId = "") Then
                CurInvId = InvId
            ElseIf (InvId <> CurInvId) Then
                If (PAmt > 0) Then
                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(CurInvId, AInsId, InvDt)
                    Call ApplTemp.BatchTransaction(RcvId, "P", Trim(Str(PAmt)), "P", False, 0, False, False, False)
                    Call ApplList.ApplSetReceivableBalance(CurInvId, "", True)
                End If
                PAmt = 0
                InsrId = 0
                CurInvId = InvId
            End If
            If (val(Trim(MSFlexGrid1.TextMatrix(i, 8))) > 0) And ((val(Trim(MSFlexGrid1.TextMatrix(i, 6))) > 0) Or (val(Trim(MSFlexGrid1.TextMatrix(i, 7))) > 0)) Then
                PAmt = PAmt + val(Trim(MSFlexGrid1.TextMatrix(i, 8)))
            End If
        Next i
        Set ApplList = Nothing
    End If
    Set ApplTemp = Nothing
    lblPrint.Visible = False
    CompleteCurrentPatient = True
End If
End Function

Private Function CalculateFields(ARow As Integer) As Boolean
Dim Bal As Single
Dim Adj As Single
Dim Allow As Single
Dim Paid As Single
Dim Cover As Single
Dim Charge As Single
Dim Temp1 As String
Dim Temp2 As String
CalculateFields = True
If ((Trim(MSFlexGrid1.TextMatrix(ARow, 6)) = "") And (Trim(MSFlexGrid1.TextMatrix(ARow, 7)) = "") Or _
   (val(Trim(MSFlexGrid1.TextMatrix(ARow, 6))) = 0) And (val(Trim(MSFlexGrid1.TextMatrix(ARow, 7))) = 0)) Then
    If (ARow > 0) Then
        Cover = val(Trim(MSFlexGrid2.TextMatrix(ARow, 0)))
        If (Cover <= 0) Then
            Cover = 1
        Else
            Cover = Cover / 100
        End If
        Allow = 0
        Charge = val(Trim(MSFlexGrid1.TextMatrix(ARow, 3)))
        If (Allow > 0) And (Charge >= val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))) Then
            If (Charge >= Allow) Then
                Paid = (Int(((Allow * Cover) + 0.005) * 100) / 100) - (Charge - val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))))
                Adj = (val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))) - Paid) - (Int(((Allow * (1 - Cover)) + 0.005) * 100) / 100)
            Else
                Paid = Int(((Charge * Cover) + 0.004) * 100) / 100
                Adj = 0
            End If
        Else
            Paid = 0
            Adj = 0
        End If
        Call DisplayDollarAmount(Trim(Str(Paid)), Temp1)
        Call DisplayDollarAmount(Trim(Str(Adj)), Temp2)
        Bal = MoneyLeft(ARow)
' removed to allow calculator function on cell
' is covered display of negative balances in check amount
' and if movement out of field occurs.
        txtPayAmt.Text = Trim(Temp1)
        MSFlexGrid1.TextMatrix(ARow, 6) = Trim(txtPayAmt.Text)
        txtAdjAmt.Text = Trim(Temp2)
        MSFlexGrid1.TextMatrix(ARow, 7) = Trim(txtAdjAmt.Text)
        Call CalculateBalance(ARow)
    End If
End If
End Function

Private Function CalculateBalance(ARow As Integer) As Boolean
Dim Bal As Single
Dim Adj As Single
Dim Paid As Single
Dim PrevBal As Single
Dim Temp1 As String
CalculateBalance = False
PrevBal = val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))
Paid = val(Trim(MSFlexGrid1.TextMatrix(ARow, 6)))
Adj = val(Trim(MSFlexGrid1.TextMatrix(ARow, 7)))
Bal = ((PrevBal - (Paid + Adj)) * 100) / 100
Call DisplayDollarAmount(Trim(Str(Bal)), Temp1)
MSFlexGrid1.TextMatrix(ARow, 8) = Trim(Temp1)
Call SetDisburseRemaining
CalculateBalance = True
End Function

Private Function SetGridTotals() As Boolean
Dim i As Integer
Dim p As Single, a As Single
Dim Temp1 As String, Temp2 As String
p = 0
a = 0
For i = 1 To MSFlexGrid1.Rows - 1
    p = p + val(Trim(MSFlexGrid1.TextMatrix(i, 6)))
    a = a + val(Trim(MSFlexGrid1.TextMatrix(i, 7)))
Next i
Call DisplayDollarAmount(Trim(Str(p)), Temp1)
Call DisplayDollarAmount(Trim(Str(a)), Temp2)
lblTPay.Visible = True
lblTPaid.Caption = Trim(Temp1)
lblTAdj.Visible = True
lblTAdjs.Caption = Trim(Temp2)
p = CurrentOpenBalance - (val(lblTPaid.Caption) + val(lblTAdjs.Caption))
Call DisplayDollarAmount(Trim(Str(p)), Temp1)
lblBalAmt.Caption = Trim(Temp1)
End Function

Private Function SetDisburseRemaining() As Boolean
Dim Amt As Single
Dim Temp As String
SetDisburseRemaining = True
Amt = MoneyLeft(0)
lblAmountLeft.Tag = Trim(Str(Amt))
Call DisplayDollarAmount(lblAmountLeft.Tag, Temp)
lblAmountLeft.Tag = Trim(Temp)
lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(lblAmountLeft.Tag)
Call SetGridTotals
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
