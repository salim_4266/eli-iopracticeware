VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmViewFinancial 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox SecondPolicyHolder 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2160
      MaxLength       =   35
      TabIndex        =   42
      Top             =   4320
      Width           =   5295
   End
   Begin VB.ListBox lstOldPolicy2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   795
      ItemData        =   "ViewFinancial.frx":0000
      Left            =   2160
      List            =   "ViewFinancial.frx":0002
      TabIndex        =   41
      Top             =   4800
      Width           =   9705
   End
   Begin VB.TextBox txtPrimaryMember2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   32
      TabIndex        =   39
      Top             =   3000
      Width           =   1455
   End
   Begin VB.TextBox txtPrimaryMember1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   32
      TabIndex        =   38
      Top             =   2520
      Width           =   1455
   End
   Begin VB.TextBox FirstPolicyHolder 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2160
      MaxLength       =   35
      TabIndex        =   37
      Top             =   645
      Width           =   5295
   End
   Begin VB.TextBox txtSecondMember1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   32
      TabIndex        =   36
      Top             =   6360
      Width           =   1455
   End
   Begin VB.TextBox txtSecondMember2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   32
      TabIndex        =   35
      Top             =   6840
      Width           =   1455
   End
   Begin VB.ListBox lstOldPolicy1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   795
      ItemData        =   "ViewFinancial.frx":0004
      Left            =   2160
      List            =   "ViewFinancial.frx":0006
      TabIndex        =   33
      Top             =   1080
      Width           =   9705
   End
   Begin VB.TextBox txtPrimaryMember3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   32
      TabIndex        =   32
      Top             =   3480
      Width           =   1455
   End
   Begin VB.TextBox txtSecondMember3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   32
      TabIndex        =   31
      Top             =   7320
      Width           =   1455
   End
   Begin VB.TextBox txtPrimarySDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   30
      Top             =   3480
      Width           =   1335
   End
   Begin VB.TextBox txtPrimarySDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   29
      Top             =   2520
      Width           =   1335
   End
   Begin VB.TextBox txtPrimarySDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   28
      Top             =   3000
      Width           =   1335
   End
   Begin VB.TextBox txtSecondSDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   27
      Top             =   7320
      Width           =   1335
   End
   Begin VB.TextBox txtSecondSDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   26
      Top             =   6360
      Width           =   1335
   End
   Begin VB.TextBox txtSecondSDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   25
      Top             =   6840
      Width           =   1335
   End
   Begin VB.TextBox txtPrimaryEDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   24
      Top             =   3480
      Width           =   1335
   End
   Begin VB.TextBox txtPrimaryEDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   23
      Top             =   2520
      Width           =   1335
   End
   Begin VB.TextBox txtPrimaryEDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   22
      Top             =   3000
      Width           =   1335
   End
   Begin VB.TextBox txtSecondEDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   21
      Top             =   7320
      Width           =   1335
   End
   Begin VB.TextBox txtSecondEDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   20
      Top             =   6360
      Width           =   1335
   End
   Begin VB.TextBox txtSecondEDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   19
      Top             =   6840
      Width           =   1335
   End
   Begin VB.TextBox txtPrimary1 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   18
      Top             =   2520
      Width           =   375
   End
   Begin VB.TextBox txtPrimary2 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   17
      Top             =   3000
      Width           =   375
   End
   Begin VB.TextBox txtPrimary3 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   16
      Top             =   3480
      Width           =   375
   End
   Begin VB.TextBox txtSecond1 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   15
      Top             =   6360
      Width           =   375
   End
   Begin VB.TextBox txtSecond2 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   14
      Top             =   6840
      Width           =   375
   End
   Begin VB.TextBox txtSecond3 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   13
      Top             =   7320
      Width           =   375
   End
   Begin VB.TextBox txtCopayP1 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   12
      Top             =   2520
      Width           =   735
   End
   Begin VB.TextBox txtCopayP2 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   11
      Top             =   3000
      Width           =   735
   End
   Begin VB.TextBox txtCopayP3 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   10
      Top             =   3480
      Width           =   735
   End
   Begin VB.TextBox txtCopayS1 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   9
      Top             =   6360
      Width           =   735
   End
   Begin VB.TextBox txtCopayS2 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   8
      Top             =   6840
      Width           =   735
   End
   Begin VB.TextBox txtCopayS3 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      MaxLength       =   10
      TabIndex        =   7
      Top             =   7320
      Width           =   735
   End
   Begin VB.TextBox txtSecondGroup1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   32
      TabIndex        =   6
      Top             =   6360
      Width           =   1215
   End
   Begin VB.TextBox txtSecondGroup3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   32
      TabIndex        =   5
      Top             =   7320
      Width           =   1215
   End
   Begin VB.TextBox txtSecondGroup2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   32
      TabIndex        =   4
      Top             =   6840
      Width           =   1215
   End
   Begin VB.TextBox txtFirstGroup1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   32
      TabIndex        =   3
      Top             =   2520
      Width           =   1215
   End
   Begin VB.TextBox txtFirstGroup3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   32
      TabIndex        =   2
      Top             =   3480
      Width           =   1215
   End
   Begin VB.TextBox txtFirstGroup2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   32
      TabIndex        =   1
      Top             =   3000
      Width           =   1215
   End
   Begin VB.ComboBox lstInsType 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "ViewFinancial.frx":0008
      Left            =   120
      List            =   "ViewFinancial.frx":0018
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1560
      Width           =   1935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   34
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ViewFinancial.frx":004E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   40
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ViewFinancial.frx":022D
   End
   Begin VB.Line Line1 
      BorderColor     =   &H000000C0&
      X1              =   120
      X2              =   11880
      Y1              =   4080
      Y2              =   4080
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy #"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5640
      TabIndex        =   66
      Top             =   2160
      Width           =   1305
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   480
      TabIndex        =   65
      Top             =   2160
      Width           =   2865
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy Holder"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   64
      Top             =   645
      Width           =   2025
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Financial Responsibility for"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   63
      Top             =   120
      Width           =   3450
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy Holder"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   62
      Top             =   4320
      Width           =   2025
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   480
      TabIndex        =   61
      Top             =   6000
      Width           =   2865
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy #"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5640
      TabIndex        =   60
      Top             =   6000
      Width           =   1305
   End
   Begin VB.Label lblPatientName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3600
      TabIndex        =   59
      Top             =   120
      Width           =   3885
   End
   Begin VB.Label lblPrimaryInsurer2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   58
      Top             =   3000
      Width           =   4215
   End
   Begin VB.Label lblPrimaryInsurer1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   57
      Top             =   2520
      Width           =   4215
   End
   Begin VB.Label lblSecondInsurer1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   56
      Top             =   6360
      Width           =   4215
   End
   Begin VB.Label lblSecondInsurer2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   55
      Top             =   6840
      Width           =   4215
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Past Policies"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   54
      Top             =   4800
      Width           =   2025
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Past Policies"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   53
      Top             =   1080
      Width           =   2025
   End
   Begin VB.Label lblPrimaryInsurer3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   52
      Top             =   3480
      Width           =   4215
   End
   Begin VB.Label lblSecondInsurer3 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   51
      Top             =   7320
      Width           =   4215
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8520
      TabIndex        =   50
      Top             =   2160
      Width           =   1305
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8520
      TabIndex        =   49
      Top             =   6000
      Width           =   1305
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   9960
      TabIndex        =   48
      Top             =   2160
      Width           =   1305
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   9960
      TabIndex        =   47
      Top             =   6000
      Width           =   1305
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Copay"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      TabIndex        =   46
      Top             =   2160
      Width           =   825
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Copay"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4800
      TabIndex        =   45
      Top             =   6000
      Width           =   825
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Group #"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7200
      TabIndex        =   44
      Top             =   6000
      Width           =   1185
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Group #"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7200
      TabIndex        =   43
      Top             =   2160
      Width           =   1065
   End
End
Attribute VB_Name = "frmViewFinancial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public ThePolicyHolder1 As Long
Public ThePolicyHolder2 As Long

Private InsuranceType As String

Private PolicyHolder_1 As Long
Private PrimaryFirstCurrent As Long
Private PrimarySecondCurrent As Long
Private PrimaryThirdCurrent As Long
Private FirstInsurerId1 As Long
Private FirstInsurerId2 As Long
Private FirstInsurerId3 As Long

Private PolicyHolder_2 As Long
Private SecondFirstCurrent As Long
Private SecondSecondCurrent As Long
Private SecondThirdCurrent As Long
Private SecondInsurerId1 As Long
Private SecondInsurerId2 As Long
Private SecondInsurerId3 As Long

Private RetrieveInsurer As Insurer
Private RetrievePatientFinancial As PatientFinance
Private RetrieveAnotherPatient As Patient

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientFinancial.BorderStyle = 1
    frmPatientFinancial.ClipControls = True
    frmPatientFinancial.Caption = Mid(frmPatientFinancial.Name, 4, Len(frmPatientFinancial.Name) - 3)
    frmPatientFinancial.AutoRedraw = True
    frmPatientFinancial.Refresh
End If
End Sub

Private Sub lblPrimaryInsurer1_Click()
If (lblPrimaryInsurer1.Caption <> "") Then
    Call DisplayPlan(FirstInsurerId1)
End If
End Sub

Private Sub lblPrimaryInsurer2_Click()
If (lblPrimaryInsurer2.Caption <> "") Then
    Call DisplayPlan(FirstInsurerId2)
End If
End Sub

Private Sub lblPrimaryInsurer3_Click()
If (lblPrimaryInsurer3.Caption <> "") Then
    Call DisplayPlan(FirstInsurerId3)
End If
End Sub

Private Sub lblSecondInsurer1_Click()
If (lblSecondInsurer1.Caption <> "") Then
    Call DisplayPlan(SecondInsurerId1)
End If
End Sub

Private Sub lblSecondInsurer2_Click()
If (lblSecondInsurer2.Caption <> "") Then
    Call DisplayPlan(SecondInsurerId2)
End If
End Sub

Private Sub lblSecondInsurer3_Click()
If (lblSecondInsurer3.Caption <> "") Then
    Call DisplayPlan(SecondInsurerId3)
End If
End Sub

Private Sub lstInsType_Click()
Dim InsType As String
If (lstInsType.ListIndex >= 0) Then
    InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    If (InsType <> InsuranceType) Then
        Call FinancialLoadDisplay(PatientId, ThePolicyHolder1, ThePolicyHolder2, InsType)
    End If
End If
End Sub

Private Sub cmdDone_Click()
Call PostCopay
Unload frmViewFinancial
End Sub

Private Sub cmdHome_Click()
Unload frmViewFinancial
End Sub

Public Function FinancialLoadDisplay(PatId As Long, FirstPolicyHolderId As Long, SecondPolicyHolderId As Long, InsType As String) As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim Copay As String
Dim FirstFinancialId As Long
Dim SecondFinancialId As Long
Dim RetrievePatient As Patient
Dim LocalDate As ManagedDate
FinancialLoadDisplay = True
Set LocalDate = New ManagedDate
Set RetrievePatient = New Patient
PatientId = PatId
RetrievePatient.PatientId = PatId
If (RetrievePatient.RetrievePatient) Then
    lblPatientName.Caption = RetrievePatient.FirstName + " " _
                           + RetrievePatient.MiddleInitial + " " _
                           + RetrievePatient.LastName + " " _
                           + RetrievePatient.NameRef
End If
Set RetrievePatient = Nothing
FirstPolicyHolder.Text = ""
SecondPolicyHolder.Text = ""
txtPrimary1.Text = ""
txtPrimary2.Text = ""
txtPrimary3.Text = ""
lblPrimaryInsurer1.Caption = ""
lblPrimaryInsurer2.Caption = ""
lblPrimaryInsurer3.Caption = ""
txtCopayP1.Text = ""
txtCopayP2.Text = ""
txtCopayP3.Text = ""
txtPrimaryMember1.Text = ""
txtPrimaryMember2.Text = ""
txtPrimaryMember3.Text = ""
txtFirstGroup1.Text = ""
txtFirstGroup2.Text = ""
txtFirstGroup3.Text = ""
txtPrimarySDate1.Text = ""
txtPrimarySDate2.Text = ""
txtPrimarySDate3.Text = ""
txtPrimaryEDate1.Text = ""
txtPrimaryEDate2.Text = ""
txtPrimaryEDate3.Text = ""

txtSecond1.Text = ""
txtSecond2.Text = ""
txtSecond3.Text = ""
lblSecondInsurer1.Caption = ""
lblSecondInsurer2.Caption = ""
lblSecondInsurer3.Caption = ""
txtCopayS1.Text = ""
txtCopayS2.Text = ""
txtCopayS3.Text = ""
txtSecondMember1.Text = ""
txtSecondMember2.Text = ""
txtSecondMember3.Text = ""
txtSecondGroup1.Text = ""
txtSecondGroup2.Text = ""
txtSecondGroup3.Text = ""
txtSecondSDate1.Text = ""
txtSecondSDate2.Text = ""
txtSecondSDate3.Text = ""
txtSecondEDate1.Text = ""
txtSecondEDate2.Text = ""
txtSecondEDate3.Text = ""

If (Trim(InsType) = "") Then
    InsType = "M"
End If
InsuranceType = InsType
lstInsType.ListIndex = -1
For i = 0 To lstInsType.ListCount - 1
    If (Left(lstInsType.List(i), 1) = InsType) Then
        lstInsType.ListIndex = i
        Exit For
    End If
Next i
' Policy Holder 1 Insurer Stuff
PolicyHolder_1 = 0
FirstInsurerId1 = 0
FirstInsurerId2 = 0
FirstInsurerId3 = 0
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
If (FirstPolicyHolderId <> 0) Then
    Call GetActiveFinancial(FirstPolicyHolderId, PrimaryFirstCurrent, PrimarySecondCurrent, PrimaryThirdCurrent)
    Call SetPolicy(lstOldPolicy1, FirstPolicyHolderId)
    ThePolicyHolder1 = FirstPolicyHolderId
    If (FirstPolicyHolderId > 0) Then
        RetrieveAnotherPatient.PatientId = FirstPolicyHolderId
        If (RetrieveAnotherPatient.RetrievePatient) Then
            PolicyHolder_1 = FirstPolicyHolderId
            FirstPolicyHolder.Text = RetrieveAnotherPatient.FirstName + " " _
                                    + RetrieveAnotherPatient.MiddleInitial + " " _
                                    + RetrieveAnotherPatient.LastName + " " _
                                    + RetrieveAnotherPatient.NameRef
            If (PrimaryFirstCurrent > 0) Then
                RetrievePatientFinancial.PatientId = FirstPolicyHolderId
                RetrievePatientFinancial.FinancialId = PrimaryFirstCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId1 = RetrievePatientFinancial.PrimaryInsurerId
                            lblPrimaryInsurer1.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP1.Text = Trim(Copay)
                        End If
                        txtPrimaryMember1.Text = RetrievePatientFinancial.PrimaryPerson
                        txtFirstGroup1.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimarySDate1.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimaryEDate1.Text = LocalDate.ExposedDate
                        End If
                        txtPrimary1.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                    End If
                End If
            End If
            If (PrimarySecondCurrent > 0) Then
                RetrievePatientFinancial.PatientId = FirstPolicyHolderId
                RetrievePatientFinancial.FinancialId = PrimarySecondCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId2 = RetrievePatientFinancial.PrimaryInsurerId
                            lblPrimaryInsurer2.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP2.Text = Trim(Copay)
                        End If
                        txtPrimaryMember2.Text = RetrievePatientFinancial.PrimaryPerson
                        txtFirstGroup2.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimarySDate2.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimaryEDate2.Text = LocalDate.ExposedDate
                        End If
                        txtPrimary2.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                    End If
                End If
            End If
            If (PrimaryThirdCurrent > 0) Then
                RetrievePatientFinancial.PatientId = FirstPolicyHolderId
                RetrievePatientFinancial.FinancialId = PrimaryThirdCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId3 = RetrievePatientFinancial.PrimaryInsurerId
                            lblPrimaryInsurer3.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP3.Text = Trim(Copay)
                        End If
                        txtPrimaryMember3.Text = RetrievePatientFinancial.PrimaryPerson
                        txtFirstGroup3.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimarySDate3.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimaryEDate3.Text = LocalDate.ExposedDate
                        End If
                        txtPrimary3.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                    End If
                End If
            End If
        End If
    End If
End If

' Policy Holder 2 Insurer Stuff
PolicyHolder_2 = 0
SecondInsurerId1 = 0
SecondInsurerId2 = 0
SecondInsurerId3 = 0
If (SecondPolicyHolderId <> 0) Then
    Call GetActiveFinancial(SecondPolicyHolderId, SecondFirstCurrent, SecondSecondCurrent, SecondThirdCurrent)
    Call SetPolicy(lstOldPolicy2, SecondPolicyHolderId)
    ThePolicyHolder2 = SecondPolicyHolderId
    If (SecondPolicyHolderId > 0) Then
        RetrieveAnotherPatient.PatientId = SecondPolicyHolderId
        If (RetrieveAnotherPatient.RetrievePatient) Then
            PolicyHolder_2 = SecondPolicyHolderId
            SecondPolicyHolder.Text = RetrieveAnotherPatient.FirstName + " " _
                                    + RetrieveAnotherPatient.MiddleInitial + " " _
                                    + RetrieveAnotherPatient.LastName + " " _
                                    + RetrieveAnotherPatient.NameRef
            If (SecondFirstCurrent <> 0) Then
                RetrievePatientFinancial.PatientId = SecondPolicyHolderId
                RetrievePatientFinancial.FinancialId = SecondFirstCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId1 = RetrievePatientFinancial.PrimaryInsurerId
                            lblSecondInsurer1.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS1.Text = Trim(Copay)
                        End If
                        txtSecondMember1.Text = RetrievePatientFinancial.PrimaryPerson
                        txtSecondGroup1.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondSDate1.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondEDate1.Text = LocalDate.ExposedDate
                        End If
                        txtSecond1.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                    End If
                End If
            End If
            If (SecondSecondCurrent <> 0) Then
                RetrievePatientFinancial.PatientId = SecondPolicyHolderId
                RetrievePatientFinancial.FinancialId = SecondSecondCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId2 = RetrievePatientFinancial.PrimaryInsurerId
                            lblSecondInsurer2.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS2.Text = Trim(Copay)
                        End If
                        txtSecondGroup2.Text = RetrievePatientFinancial.PrimaryGroup
                        txtSecondMember2.Text = RetrievePatientFinancial.PrimaryPerson
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondSDate2.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondEDate2.Text = LocalDate.ExposedDate
                        End If
                        txtSecond2.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                    End If
                End If
            End If
            If (SecondThirdCurrent <> 0) Then
                RetrievePatientFinancial.PatientId = SecondPolicyHolderId
                RetrievePatientFinancial.FinancialId = SecondThirdCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId3 = RetrievePatientFinancial.PrimaryInsurerId
                            lblSecondInsurer3.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS3.Text = Trim(Copay)
                        End If
                        txtSecondMember3.Text = RetrievePatientFinancial.PrimaryPerson
                        txtSecondGroup3.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondSDate3.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondEDate3.Text = LocalDate.ExposedDate
                        End If
                        txtSecond3.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                    End If
                End If
            End If
        End If
    End If
End If
txtPrimary1.Locked = True
txtPrimary2.Locked = True
txtPrimary3.Locked = True
txtCopayP1.Locked = False
txtCopayP2.Locked = False
txtCopayP3.Locked = False
txtFirstGroup1.Locked = True
txtFirstGroup2.Locked = True
txtFirstGroup3.Locked = True
txtPrimaryMember1.Locked = True
txtPrimaryMember2.Locked = True
txtPrimaryMember3.Locked = True
If (Trim(lblPrimaryInsurer1.Caption) <> "") Then
    txtPrimaryMember1.Locked = False
    txtFirstGroup1.Locked = False
End If
If (Trim(lblPrimaryInsurer2.Caption) <> "") Then
    txtPrimaryMember2.Locked = False
    txtFirstGroup2.Locked = False
End If
If (Trim(lblPrimaryInsurer3.Caption) <> "") Then
    txtPrimaryMember3.Locked = False
    txtFirstGroup3.Locked = False
End If
txtPrimarySDate1.Locked = True
txtPrimarySDate2.Locked = True
txtPrimarySDate3.Locked = True
txtPrimaryEDate1.Locked = True
txtPrimaryEDate2.Locked = True
txtPrimaryEDate3.Locked = True
txtSecond1.Locked = True
txtSecond2.Locked = True
txtSecond3.Locked = True
txtCopayS1.Locked = False
txtCopayS2.Locked = False
txtCopayS3.Locked = False
txtSecondGroup1.Locked = True
txtSecondGroup2.Locked = True
txtSecondGroup3.Locked = True
txtSecondMember1.Locked = True
txtSecondMember2.Locked = True
txtSecondMember3.Locked = True
If (Trim(lblSecondInsurer1.Caption) <> "") Then
    txtSecondMember1.Locked = False
    txtSecondGroup1.Locked = False
End If
If (Trim(lblSecondInsurer2.Caption) <> "") Then
    txtSecondMember2.Locked = False
    txtSecondGroup2.Locked = False
End If
If (Trim(lblSecondInsurer3.Caption) <> "") Then
    txtSecondMember3.Locked = False
    txtSecondGroup3.Locked = False
End If
txtSecondSDate1.Locked = True
txtSecondSDate2.Locked = True
txtSecondSDate3.Locked = True
txtSecondEDate1.Locked = True
txtSecondEDate2.Locked = True
txtSecondEDate3.Locked = True
Set LocalDate = Nothing
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
Exit Function
UIError_Label:
    FinancialLoadDisplay = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function SetPolicy(AListbox As ListBox, PolicyHolderid As Long) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim InsRec As Insurer
Dim FinRec As PatientFinance
Dim LocalDate As ManagedDate
Set LocalDate = New ManagedDate
Set InsRec = New Insurer
Set FinRec = New PatientFinance
SetPolicy = True
AListbox.Clear
If (PolicyHolderid > 0) Then
    FinRec.PatientId = PolicyHolderid
    FinRec.FinancialId = 0
    FinRec.PrimaryInsurerId = 0
    FinRec.PrimaryPIndicator = 0
    FinRec.PrimaryStartDate = ""
    FinRec.PrimaryEndDate = ""
    FinRec.PrimaryInsType = ""
    If (lstInsType.ListIndex >= 0) Then
        FinRec.PrimaryInsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    End If
    FinRec.FinancialStatus = "X"
    If (FinRec.FindPatientFinancial > 0) Then
        i = 1
        While (FinRec.SelectPatientFinancial(i))
            If (FinRec.PrimaryInsurerId > 0) Then
                InsRec.InsurerId = FinRec.PrimaryInsurerId
                If (InsRec.RetrieveInsurer) Then
                    DisplayText = Space(75)
                    Mid(DisplayText, 1, Len(Trim(InsRec.InsurerName))) = Trim(InsRec.InsurerName)
                    Mid(DisplayText, 21, Len(Trim(InsRec.InsurerAddress))) = Trim(InsRec.InsurerAddress)
                    Mid(DisplayText, 36, Len(Trim(InsRec.InsurerCity))) = Trim(InsRec.InsurerCity)
                    LocalDate.ExposedDate = FinRec.PrimaryStartDate
                    If (LocalDate.ConvertManagedDateToDisplayDate) Then
                        Mid(DisplayText, 50, 10) = LocalDate.ExposedDate + " "
                    End If
                    LocalDate.ExposedDate = FinRec.PrimaryEndDate
                    If (LocalDate.ConvertManagedDateToDisplayDate) Then
                        Mid(DisplayText, 62, 10) = LocalDate.ExposedDate
                    End If
                    DisplayText = DisplayText + Trim(Str(FinRec.FinancialId))
                    AListbox.AddItem DisplayText
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set LocalDate = Nothing
Set InsRec = Nothing
Set FinRec = Nothing
End Function

Private Function GetActiveFinancial(PolicyHolderid As Long, FirstCurrent As Long, SecondCurrent As Long, ThirdCurrent As Long) As Boolean
Dim i As Integer
Dim FinRec As PatientFinance
Set FinRec = New PatientFinance
GetActiveFinancial = False
FirstCurrent = 0
SecondCurrent = 0
ThirdCurrent = 0
If (PolicyHolderid > 0) Then
    FinRec.PatientId = PolicyHolderid
    FinRec.PrimaryInsurerId = 0
    FinRec.FinancialId = 0
    FinRec.PrimaryStartDate = ""
    FinRec.PrimaryEndDate = ""
    FinRec.PrimaryInsType = ""
    If (lstInsType.ListIndex >= 0) Then
        FinRec.PrimaryInsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    End If
    FinRec.FinancialStatus = "C"
    If (FinRec.FindPatientFinancial > 0) Then
        i = 1
        While (FinRec.SelectPatientFinancial(i))
            If (FinRec.PrimaryPIndicator = 1) Then
                FirstCurrent = FinRec.FinancialId
            ElseIf (FinRec.PrimaryPIndicator = 2) Then
                SecondCurrent = FinRec.FinancialId
            ElseIf (FinRec.PrimaryPIndicator = 3) Then
                ThirdCurrent = FinRec.FinancialId
            End If
            i = i + 1
        Wend
        GetActiveFinancial = True
    End If
End If
Set FinRec = Nothing
End Function

Private Sub DisplayPlan(Rec As Long)
If (Rec > 0) Then
    frmInsurer.ReadOnly = True
    frmInsurer.DeleteOn = False
    If (frmInsurer.InsurerLoadDisplay(Rec, "")) Then
        frmInsurer.Show 1
    End If
End If
End Sub

Private Function PostCopay() As Boolean
Dim FinId As Long
Dim AMbr As String
Dim AGrp As String
Dim Copay As String
Dim RetFin As PatientFinance
PostCopay = False
If (Trim(txtCopayP1.Text) <> "") Then
    FinId = PrimaryFirstCurrent
    Copay = Trim(txtCopayP1.Text)
    AMbr = Trim(txtPrimaryMember1.Text)
    AGrp = Trim(txtFirstGroup1.Text)
    GoSub PostIt
End If
If (Trim(txtCopayP2.Text) <> "") Then
    FinId = PrimarySecondCurrent
    Copay = Trim(txtCopayP2.Text)
    AMbr = Trim(txtPrimaryMember2.Text)
    AGrp = Trim(txtFirstGroup2.Text)
    GoSub PostIt
End If
If (Trim(txtCopayP3.Text) <> "") Then
    FinId = PrimaryThirdCurrent
    Copay = Trim(txtCopayP3.Text)
    AMbr = Trim(txtPrimaryMember3.Text)
    AGrp = Trim(txtFirstGroup3.Text)
    GoSub PostIt
End If
If (Trim(txtCopayS1.Text) <> "") Then
    FinId = SecondFirstCurrent
    Copay = Trim(txtCopayS1.Text)
    AMbr = Trim(txtSecondMember1.Text)
    AGrp = Trim(txtSecondGroup1.Text)
    GoSub PostIt
End If
If (Trim(txtCopayS2.Text) <> "") Then
    FinId = SecondSecondCurrent
    Copay = Trim(txtCopayS2.Text)
    AMbr = Trim(txtSecondMember2.Text)
    AGrp = Trim(txtSecondGroup2.Text)
    GoSub PostIt
End If
If (Trim(txtCopayS3.Text) <> "") Then
    FinId = SecondThirdCurrent
    Copay = Trim(txtCopayS3.Text)
    AMbr = Trim(txtSecondMember3.Text)
    AGrp = Trim(txtSecondGroup3.Text)
    GoSub PostIt
End If
PostCopay = True
Exit Function
PostIt:
    If (FinId > 0) Then
        Set RetFin = New PatientFinance
        RetFin.FinancialId = FinId
        If (RetFin.RetrievePatientFinance) Then
            RetFin.FinancialCopay = Trim(Copay)
            RetFin.PrimaryPerson = Trim(AMbr)
            RetFin.PrimaryGroup = Trim(AGrp)
            RetFin.ApplyPatientFinance
        End If
        Set RetFin = Nothing
    End If
    Return
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
