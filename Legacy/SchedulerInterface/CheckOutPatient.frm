VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmCheckoutPatient 
   BackColor       =   &H00785211&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00785211&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "CheckOutPatient.frx":0000
      Left            =   9480
      List            =   "CheckOutPatient.frx":0002
      TabIndex        =   4
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H00946414&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5700
      ItemData        =   "CheckOutPatient.frx":0004
      Left            =   420
      List            =   "CheckOutPatient.frx":0006
      TabIndex        =   0
      Top             =   1440
      Width           =   11055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   480
      TabIndex        =   2
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CheckOutPatient.frx":0008
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   990
      Left            =   2880
      TabIndex        =   3
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CheckOutPatient.frx":01E7
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   480
      TabIndex        =   6
      Top             =   1080
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   9724948
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Appointment Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   480
      TabIndex        =   7
      Top             =   840
      Width           =   1560
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9480
      TabIndex        =   5
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00785211&
      Caption         =   "Check Out Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   360
      Left            =   4740
      TabIndex        =   1
      Top             =   240
      Width           =   2535
   End
End
Attribute VB_Name = "frmCheckoutPatient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public ActivityId As Long
Public PatientId As Long
Public AppointmentId As Long
Public ReferralId As Long
Public ActivityDate As String
Private LocationId As Long
Private ShowAll As Boolean
Private Const ChkDoneInProgress As Integer = 0
Private Const ChkDone As Integer = 1
Private Const ChkOut As Integer = 2

Private Sub cmdAll_Click()
ShowAll = Not ShowAll
If (ShowAll) Then
    cmdAll.Text = "Show Todays Check Out"
Else
    cmdAll.Text = "Show All Check Out"
    LocationId = dbPracticeId
    lstLoc.ListIndex = -1
End If
Call ActivityLoadList
End Sub

Private Sub cmdHome_Click()
CurrentAction = "Home"
Unload frmCheckoutPatient
End Sub

Private Sub Form_Load()
LocationId = 0
ShowAll = False
If UserLogin.HasPermission(epPowerUser) Then
    frmCheckoutPatient.BorderStyle = 1
    frmCheckoutPatient.ClipControls = True
    frmCheckoutPatient.Caption = Mid(frmCheckoutPatient.Name, 4, Len(frmCheckoutPatient.Name) - 3)
    frmCheckoutPatient.AutoRedraw = True
    frmCheckoutPatient.Refresh
End If
End Sub

Private Sub lstAppts_Click()
Dim ActId As Long
Dim ADate  As String
Dim InvId As String
Dim AStatus As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
If (lstAppts.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    ActivityId = ApplList.ApplGetActivityId(lstAppts.List(lstAppts.ListIndex))
    PatientId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
    AppointmentId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
    ReferralId = ApplList.ApplGetReferralId(lstAppts.List(lstAppts.ListIndex))
    ActivityDate = SSDateCombo1.DateValue
    If (GetActivityDate(ActivityId, ADate)) Then
        ActivityDate = ADate
    End If
    Call FormatTodaysDate(ActivityDate, False)
    If (Trim(ActivityDate) = "") Then
        ActivityDate = Trim(SSDateCombo1.DateValue)
    End If
    If (AppointmentId > 0) Then
        If ApplList.CheckAppointmentLocation(AppointmentId) Then
            InvId = ApplList.FormatInvoiceNumber(PatientId, AppointmentId)
            Set ApplTemp = New ApplicationTemplates
            If (ApplTemp.ApplIsInvoicePresent(InvId)) Then
                frmEventMsgs.Header = "Activity already checked out."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                ActId = ActivityId
                Call ApplList.SetActivityDoneInProgress(ActId, ChkDone, UserLogin.iId)
                Set ApplTemp = Nothing
                Set ApplList = Nothing
                Call ActivityLoadList
            Else
                Set ApplTemp = Nothing
                If (ApplList.GetActivityStatus(AppointmentId, "", AStatus, ActId)) Then
                    If (AStatus = "U") Then
                        frmEventMsgs.Header = "Activity is being checked out."
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                    Else
                        If (ActId <> ActivityId) And (ActivityId > 0) Then
                            ActId = ActivityId
                        End If
                        Call ApplList.SetActivityDoneInProgress(ActId, ChkDoneInProgress, UserLogin.iId)
                        Unload frmCheckoutPatient
                    End If
                    Set ApplList = Nothing
                End If
            End If
            Else
                ActivityId = 0
        End If
    End If
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        LocationId = -1
    Else
        Set ApplList = New ApplicationAIList
        LocationId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
    Call ActivityLoadList
    If (frmPatientLocator.Visible) Then
        If (lstAppts.ListCount < 1) Then
            frmEventMsgs.Header = "Nothing Outstanding"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            LocationId = dbPracticeId
            DoEvents
            Call ActivityLoadList
            lstLoc.ListIndex = -1
        End If
    End If
End If
End Sub

Private Sub SSDateCombo1_Change()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_Click()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_CloseUp()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") Then
        ActivityDate = SSDateCombo1.DateValue
        Call FormatTodaysDate(ActivityDate, False)
        If (ActivityDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = ActivityDate
        End If
    Else
        If (ActivityDate = "") Then
            SSDateCombo1.Text = ""
        ElseIf (SSDateCombo1.DateValue = "") Then
            SSDateCombo1.Text = ActivityDate
        Else
            ActivityDate = SSDateCombo1.DateValue
            ActivityDate = Mid(ActivityDate, 1, 2) + Mid(ActivityDate, 4, 2) + Mid(ActivityDate, 9, 2)
        End If
        ATemp = ActivityDate
        If (Len(ActivityDate) > 10) Then
            ATemp = Mid(ActivityDate, 11, Len(ActivityDate) - 10)
        ElseIf (ActivityDate = "") Then
            Call FormatTodaysDate(ActivityDate, False)
            ATemp = Mid(ActivityDate, 1, 2) + Mid(ActivityDate, 4, 2) + Mid(ActivityDate, 9, 2)
        End If
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        ActivityDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(ActivityDate, False)
        If (ActivityDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = ActivityDate
        End If
    End If
    Call ActivityLoadList
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        ActivityDate = ActivityDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(ActivityDate) > 0) Then
        ActivityDate = Left(ActivityDate, Len(ActivityDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(ActivityDate) < 1) Then
        ActivityDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo1_Validate(Cancel As Boolean)
Call SSDateCombo1_KeyPress(13)
End Sub

Public Function ActivityLoadList() As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim LocId As Long
Dim TheDate As String
Dim LocalDate As ManagedDate
Dim ApptList As ApplicationAIList
ActivityLoadList = False
cmdAll.Visible = CheckConfigCollection("ALLCHECK") = "T"
If (lstLoc.ListCount < 1) Then
    Set ApptList = New ApplicationAIList
    Set ApptList.ApplList = lstLoc
    Call ApptList.ApplLoadLocation(False, True)
    Set ApptList = Nothing
    LocationId = dbPracticeId
End If
If CheckConfigCollection("MULTIOFFICE") = "T" Then
    Label1.Visible = True
    lstLoc.Visible = True
Else
    Label1.Visible = False
    lstLoc.Visible = False
    LocationId = -1
End If
lstAppts.Clear
Set LocalDate = New ManagedDate
Set ApptList = New ApplicationAIList
TheDate = ""
If (Trim(ActivityDate) <> "") Then
    TheDate = ActivityDate
End If
Call FormatTodaysDate(TheDate, False)
LocalDate.ExposedDate = TheDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    ApptList.ApplDate = LocalDate.ExposedDate
End If
If (ShowAll) Then
    ApptList.ApplDate = vbTab
    LocationId = -1
    lstLoc.ListIndex = -1
End If
Set ApptList.ApplList = lstAppts
ApptList.ApplResourceId = 0
ApptList.ApplLocId = LocationId
Set ApptList.ApplLocList = lstLoc
Call ApptList.ApplRetrieveCheckOutList
ActivityLoadList = True
Set LocalDate = Nothing
Set ApptList = Nothing
If (LocationId > -1) And Not (frmPatientLocator.Visible) Then
    For i = 0 To lstLoc.ListCount - 1
        If (LocationId = val(Trim(Mid(lstLoc.List(i), 56, 5)))) Then
            LocId = i
            Exit For
        End If
    Next i
    If (LocId > 0) Then
        lstLoc.ListIndex = LocId
    End If
End If
Exit Function
UIError_Label:
    Set LocalDate = Nothing
    Set ApptList = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetActivityDate(ActId As Long, ADate As String) As Boolean
Dim RetAct As PracticeActivity
GetActivityDate = False
If (ActId > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityId = ActId
    If (RetAct.RetrieveActivity) Then
        ADate = RetAct.ActivityDate
        ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
        GetActivityDate = True
    End If
    Set RetAct = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
