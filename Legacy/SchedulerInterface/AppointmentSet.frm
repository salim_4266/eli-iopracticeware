VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmAppointmentSet 
   BackColor       =   &H0077742D&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Set Appointment"
   ClientHeight    =   8160
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7335
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   7335
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame frRecalls 
      BackColor       =   &H0077742D&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   7215
      Left            =   0
      TabIndex        =   10
      Top             =   480
      Visible         =   0   'False
      Width           =   7215
      Begin VB.ListBox lstRecalls 
         Appearance      =   0  'Flat
         BackColor       =   &H006C6928&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   4590
         ItemData        =   "AppointmentSet.frx":0000
         Left            =   120
         List            =   "AppointmentSet.frx":0007
         TabIndex        =   14
         Top             =   960
         Width           =   6915
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdRecall 
         Height          =   855
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   6720
         Width           =   1095
         _Version        =   131072
         _ExtentX        =   1931
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AppointmentSet.frx":0017
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdRecall 
         Height          =   855
         Index           =   0
         Left            =   5160
         TabIndex        =   12
         Top             =   6720
         Width           =   1935
         _Version        =   131072
         _ExtentX        =   3413
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AppointmentSet.frx":01F7
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdRecall 
         Height          =   855
         Index           =   2
         Left            =   1320
         TabIndex        =   13
         Top             =   6720
         Width           =   1095
         _Version        =   131072
         _ExtentX        =   1931
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AppointmentSet.frx":03DB
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H0077742D&
         Caption         =   "Pending Recalls"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   270
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   1725
      End
   End
   Begin VB.ComboBox lstInsType 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "AppointmentSet.frx":05BF
      Left            =   5160
      List            =   "AppointmentSet.frx":05CF
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   480
      Width           =   1935
   End
   Begin VB.TextBox txtNote 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      MaxLength       =   64
      TabIndex        =   0
      Top             =   960
      Width           =   6975
   End
   Begin VB.ListBox lstLocs 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4590
      ItemData        =   "AppointmentSet.frx":0604
      Left            =   4080
      List            =   "AppointmentSet.frx":0606
      TabIndex        =   2
      Top             =   1440
      Width           =   2955
   End
   Begin VB.ListBox lstReferral 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   750
      ItemData        =   "AppointmentSet.frx":0608
      Left            =   120
      List            =   "AppointmentSet.frx":060A
      TabIndex        =   3
      Top             =   6240
      Width           =   6975
   End
   Begin VB.ListBox lstApptType 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4590
      ItemData        =   "AppointmentSet.frx":060C
      Left            =   120
      List            =   "AppointmentSet.frx":060E
      TabIndex        =   1
      Top             =   1440
      Width           =   3915
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   5160
      TabIndex        =   4
      Top             =   7200
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AppointmentSet.frx":0610
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   855
      Left            =   120
      TabIndex        =   8
      Top             =   7200
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AppointmentSet.frx":07EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRecord 
      Height          =   855
      Left            =   3480
      TabIndex        =   9
      Top             =   7200
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AppointmentSet.frx":09DB
   End
   Begin VB.Label lblAppt 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Date/Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   270
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   2475
   End
   Begin VB.Label lblPreCert 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   1905
   End
End
Attribute VB_Name = "frmAppointmentSet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ApptDate As String
Public ApptTime As String
Public DoctorId As Long
Public LocationId As Long
Public PatientId As Long
Public ReferralId As Long
Public ExternalIdPosted As Long

Public RV As String

Private TriggerOn As Boolean
Private SurgeryAppointmentSet As Long

Dim RS As Recordset


Public Function LoadForm(PatId As Long, ADate As String, ATime As String, FilterBy As String) As Boolean

Dim u As Integer
Dim TDate As String
Dim PatName As String
Dim RetPat As Patient
Dim ApplTbl As ApplicationTables
Dim ApplSch As ApplicationScheduler
LoadForm = False
If (PatId > 0) And (Trim(ADate) <> "") And (Trim(ATime) <> "") Then
    SurgeryAppointmentSet = 0
    ApptDate = ADate
    ApptTime = ATime
    TDate = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
    Set ApplSch = New ApplicationScheduler
    If (lstApptType.ListCount < 1) Then
        Set ApplSch.lstStartTime = lstApptType
        LoadForm = ApplSch.ApplLoadApptTypebyOtherRank(FilterBy)
    End If
    lstInsType.ListIndex = 0
    Set ApplTbl = New ApplicationTables
    Set ApplTbl.lstBox = lstReferral
    Call ApplTbl.ApplLoadReferrals(PatId, TDate)
    Set ApplTbl = Nothing
    lstReferral.ListIndex = 0
'    TriggerOn = True
'    For u = 0 To lstReferral.ListCount - 1
'        If (ReferralId = Val(Mid(lstReferral.List(u), 86, 5))) Then
'            lstReferral.ListIndex = u
'            Exit For
'        End If
'    Next u
    TriggerOn = False
    Set ApplSch.lstStartTime = lstLocs
    LoadForm = ApplSch.LoadLocList
    Set ApplSch = Nothing
    If (LocationId >= 0) Then
        For u = 0 To lstLocs.ListCount - 1
            If (val(Trim(Mid(lstLocs.List(u), 56, Len(lstLocs.List(u)) - 55))) = LocationId) Then
                lstLocs.ListIndex = u
            End If
        Next u
    End If
    Set ApplSch = Nothing
    
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        PatName = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
    End If
    Set RetPat = Nothing
    PatientId = PatId
    TDate = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
    lblAppt.Caption = "Date: " + TDate + " Time: " + ApptTime + " for " + PatName
End If
End Function

Private Sub cmdDone_Click()
Dim zz As Integer
Dim Proceed As Boolean
Dim Perm As String, Temp As String
Dim TodaysDate As String
Dim LDate As String, DrName As String
Dim InsType As String, ApptType As String
Dim ApptTypeId As Long
Dim AId As Long, LocId As Long
Dim ApplSch As ApplicationScheduler
Dim ApplTbl As ApplicationTables
Dim ApplTemp As ApplicationTemplates

If (lstLocs.ListIndex < 0) Then
    frmEventMsgs.Header = "You must select 1 Location for an appointment."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    LocId = val(Trim(Mid(lstLocs.List(lstLocs.ListIndex), 57, Len(lstLocs.List(lstLocs.ListIndex)) - 56)))
    Proceed = True
    If (lstApptType.ListIndex < 0) Then
        frmEventMsgs.Header = "Select an appointment type"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Proceed = False
    End If

    TodaysDate = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
    If (Proceed) Then
        Set ApplTemp = New ApplicationTemplates
        If Not (ApplTemp.ApplIsPatientActive(PatientId)) Then
            frmEventMsgs.Header = "Patient Is Inactive"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Proceed = False
        End If
        Set ApplTemp = Nothing
    End If

    Set ApplTbl = New ApplicationTables
    Set ApplSch = New ApplicationScheduler
    If (Proceed) Then
        Proceed = False
        If (ReferralId > 0) Then
            If Not (ApplTbl.ApplIsActiveReferral(ReferralId, PatientId, TodaysDate)) Then
                frmEventMsgs.Header = "Referral inactive"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Continue"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result <> 2) Then
                    lstReferral.ListIndex = -1
                Else
                    Proceed = True
                End If
            Else
                Proceed = True
            End If
        Else
            Proceed = True
        End If
    End If

    If (Proceed) Then
        Proceed = False
        If Not (ApplSch.IsPracticeAvailable(TodaysDate, ApptTime, LocId)) Then
            frmEventMsgs.Header = "Practice closed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Continue"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                Proceed = True
            End If
        Else
            Proceed = True
        End If
    End If

    If (Proceed) Then
        Proceed = False
        If Not (ApplSch.IsResourceAvailable(DoctorId, LocId, TodaysDate, ApptTime)) Then
            frmEventMsgs.Header = "Location Conflict"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = ""
            If (Temp = "F") Then
                frmEventMsgs.Header = "Resource not available"
                frmEventMsgs.RejectText = "Continue"
            End If
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                Proceed = True
            End If
        Else
            Proceed = True
        End If
    End If

    If (Proceed) Then
        ApptTypeId = val(Trim(Mid(lstApptType.List(lstApptType.ListIndex), 57, Len(lstApptType.List(lstApptType.ListIndex)) - 56)))
        ApptType = Trim(Left(lstApptType.List(lstApptType.ListIndex), 56))
        InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
' Deal with Surgery Appointment Types
        If (IsSurgeryType(ApptTypeId)) Then
            If Not (SpecifySurgeryAppointment(PatientId, AId)) Then
                frmEventMsgs.Header = "You must specify a Surgery Order Date before setting a Surgery Appt."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Set ApplSch = Nothing
                Set ApplTbl = Nothing
                Exit Sub
            End If
        End If
        If (ApplSch.SetAppointment(PatientId, ApptTypeId, DoctorId, LocId, _
                                   TodaysDate, ApptTime, InsType, ReferralId, _
                                   Trim(txtNote.Text), UserLogin.iId, AId, RV)) Then
            ExternalIdPosted = AId
            If (SurgeryAppointmentSet > 0) Then
                Call PlantSurgery(AId, SurgeryAppointmentSet)
            End If
        End If
    End If
    Set ApplSch = Nothing
    Set ApplTbl = Nothing
    
    If Proceed Then setRecallsStat
End If
End Sub

Private Sub cmdHome_Click()
Unload frmAppointmentSet
End Sub

Private Sub setRecallsStat()
Dim SQL As String
SQL = " select * from PracticeTransactionJournal " & _
      " where TransactionType = 'L' and TransactionStatus = 'P' " & _
      " and TransactionTypeId = " & PatientId & _
      " order by TransactionDate desc "
Set RS = GetRS(SQL)
If RS.RecordCount > 0 Then
    lstRecalls.Clear
    Dim DisplayText As String
    Dim ADate As String
    
    Do Until RS.EOF
        DisplayText = Space(75)
        ADate = Mid(RS("Transactiondate"), 5, 2) + "/" + Mid(RS("Transactiondate"), 7, 2) + "/" + Left(RS("Transactiondate"), 4)
        Mid(DisplayText, 1, Len(ADate)) = ADate
        Mid(DisplayText, 20, 54) = Left(Trim(RS("TransactionRef")), 54)
        DisplayText = DisplayText + Trim(Str(RS("TransactionID")))
        lstRecalls.AddItem DisplayText

        RS.MoveNext
    Loop
    lstRecalls.ListIndex = 0
    frRecalls.Width = Me.Width
    frRecalls.Height = Me.Height
    frRecalls.Visible = True
Else
    Unload Me
End If
End Sub

Private Sub cmdRecall_Click(Index As Integer)

If Index > 0 Then
    Dim i As Integer
    Dim Rec As Long
    Dim ApplList As New ApplicationAIList
    For i = 0 To lstRecalls.ListCount - 1
        If Index = 2 _
        Or lstRecalls.Selected(i) Then
            Rec = myTrim(lstRecalls.List(i), 1)
            If Rec > 0 Then
                If Not (ApplList.ApplIsTransactionStatus(Rec, "S")) Then
                    Call ApplList.ApplSetTransactionStatus(Rec, "Z")
                End If
            End If
        End If
    Next
    Set ApplList = Nothing
End If

Unload frmAppointmentSet
End Sub

Private Sub cmdRecord_Click()
Dim Temp As String
Dim TheDate As String
'Dim i As Integer
Dim i1 As Integer, i2 As Integer
'Dim c1 As Long
'Dim d1 As Boolean
'Dim a11 As String, b1 As String, e1 As String
'Dim RefId As Long
Dim l1 As Long, l2 As Long, l3 As Long
Dim a1 As String, a2 As String, a3 As String, a4 As String
Dim InsType As String, ADate As String, TheText As String



Dim RetrieveInsurer As New Insurer
Dim TheDocs As New SchedulerResource
Dim ApplTbl As New ApplicationTables

If (frmPatientReferral.ReferralLoadDisplay(PatientId, 0, "M")) Then
    frmPatientReferral.CurrentAction = ""
    frmPatientReferral.Show 1
    If (frmPatientReferral.CurrentAction = "") Then
        If (frmPatientReferral.TheReferralId > 0) Then
            ADate = ""
            Call FormatTodaysDate(ADate, False)
            Call ApplTbl.ApplGetReferral(PatientId, frmPatientReferral.TheReferralId, a1, i1, i2, a2, a3, a4, l1, l2, l3)
            Set ApplTbl = Nothing
            TheDate = ""
            Call FormatTodaysDate(TheDate, False)
            TheDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
            Temp = Space(85)
            Mid(Temp, 1, Len(Trim(a1))) = Trim(a1)
            Mid(Temp, 18, Len(Trim(Str(i2)))) = Trim(Str(i2))
            If (l1 > 0) Then
                TheDocs.ResourceId = l1
                If (TheDocs.RetrieveSchedulerResource) Then
                    TheText = Trim(TheDocs.ResourceName)
                    Mid(Temp, 21, 10) = Left(TheText, 10)
                End If
                Set TheDocs = Nothing
            End If
            ADate = Mid(a3, 5, 2) + "/" + Mid(a3, 7, 2) + "/" + Left(a3, 4)
            Mid(Temp, 32, 10) = ADate
            If (a3 < TheDate) Then
                Mid(Temp, 42, 1) = "X"
            End If
            If (l3 > 0) Then
                RetrieveInsurer.InsurerId = l3
                If (RetrieveInsurer.RetrieveInsurer) Then
                    TheText = Trim(RetrieveInsurer.InsurerName) + " " _
                            + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                            + Trim(RetrieveInsurer.InsurerGroupName)
                    Mid(Temp, 44, 20) = Left(TheText, 20)
                End If
                Set RetrieveInsurer = Nothing
            End If
            Temp = Left(Temp, 85) + Trim(Str(frmPatientReferral.TheReferralId))
            lstReferral.AddItem Temp
        End If
    End If
End If

Set RetrieveInsurer = Nothing
Set RetrieveInsurer = Nothing
Set ApplTbl = Nothing
End Sub

Private Sub lstReferral_Click()
If (lstReferral.ListIndex = 0) Then
    ReferralId = 0
Else
    ReferralId = val(Trim(Mid(lstReferral.List(lstReferral.ListIndex), 86, Len(lstReferral.List(lstReferral.ListIndex)) - 85)))
End If
End Sub

Private Sub lstReferral_DblClick()
Dim Temp As String
Dim TheDate As String
Dim c1 As Long
Dim d1 As Boolean
Dim a11 As String, b1 As String, e1 As String
Dim i1 As Integer, i2 As Integer
Dim l1 As Long, l2 As Long, l3 As Long
Dim a1 As String, a2 As String, a3 As String, a4 As String
Dim ADate As String, InsType As String, TheText As String
Dim RetrieveInsurer As Insurer
Dim TheDocs As SchedulerResource
Dim ApplSch As ApplicationScheduler
Dim ApplTbl As ApplicationTables
If (lstReferral.ListIndex >= 0) And (Not (TriggerOn)) Then
    If (lstReferral.ListIndex = 0) Then
        ReferralId = 0
    Else
        ReferralId = val(Trim(Mid(lstReferral.List(lstReferral.ListIndex), 86, Len(lstReferral.List(lstReferral.ListIndex)) - 85)))
        If (frmPatientReferral.ReferralLoadDisplay(PatientId, ReferralId, "M")) Then
            frmPatientReferral.CurrentAction = ""
            frmPatientReferral.Show 1
            If (frmPatientReferral.CurrentAction <> "") Then
                ADate = ""
                Call FormatTodaysDate(ADate, False)
                If (frmPatientReferral.TheReferralId > 0) Then
                    Set ApplTbl = New ApplicationTables
                    Call ApplTbl.ApplGetReferral(PatientId, frmPatientReferral.TheReferralId, a1, i1, i2, a2, a3, a4, l1, l2, l3)
                    Set ApplTbl = Nothing
                    TheDate = ""
                    Call FormatTodaysDate(TheDate, False)
                    TheDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
                    If (lstReferral.ListIndex > 0) Then
                        Temp = lstReferral.List(lstReferral.ListIndex)
                    Else
                        Temp = Space(85)
                    End If
                    Mid(Temp, 1, Len(Trim(a1))) = Trim(a1)
                    Mid(Temp, 18, 2) = Trim(Str(i2))
                    If (l1 > 0) Then
                        Set TheDocs = New SchedulerResource
                        TheDocs.ResourceId = l1
                        If (TheDocs.RetrieveSchedulerResource) Then
                            TheText = Trim(TheDocs.ResourceName)
                            Mid(Temp, 21, 10) = Left(TheText, 10)
                        End If
                        Set TheDocs = Nothing
                    End If
                    ADate = Mid(a3, 5, 2) + "/" + Mid(a3, 7, 2) + "/" + Left(a3, 4)
                    Mid(Temp, 32, 10) = ADate
                    If (a3 < TheDate) Then
                        Mid(Temp, 42, 1) = "X"
                    End If
                    If (l3 > 0) Then
                        Set RetrieveInsurer = New Insurer
                        RetrieveInsurer.InsurerId = l3
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            TheText = Trim(RetrieveInsurer.InsurerName) + " " _
                                        + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                        + Trim(RetrieveInsurer.InsurerGroupName)
                            Mid(Temp, 44, 20) = Left(TheText, 20)
                        End If
                        Set RetrieveInsurer = Nothing
                    End If
                    Temp = Left(Temp, 85) + Trim(Str(frmPatientReferral.TheReferralId))
                    If (lstReferral.ListIndex > 0) Then
                        lstReferral.List(lstReferral.ListIndex) = Temp
                    Else
                        lstReferral.AddItem Temp
                    End If
                End If
            End If
        End If
    End If
End If
End Sub

Private Function IsSurgeryType(ApptTypeId As Long) As Boolean
Dim RetApptType As SchedulerAppointmentType
IsSurgeryType = False
If (ApptTypeId > 0) Then
    Set RetApptType = New SchedulerAppointmentType
    RetApptType.AppointmentTypeId = ApptTypeId
    If (RetApptType.RetrieveSchedulerAppointmentType) Then
        If (RetApptType.AppointmentTypeResourceId8 = 1) Then
            IsSurgeryType = True
        End If
    End If
    Set RetApptType = Nothing
End If
End Function

Private Function SpecifySurgeryAppointment(PatId As Long, ApptId As Long) As Boolean
Dim LocId As Long
Dim ClnId As Long
Dim myApptId As Long
Dim ApptTypeId As Long
Dim ADate As String
Dim InsType As String
Dim ApplClm As ApplicationClaims
SpecifySurgeryAppointment = False
If (PatId > 0) Then
    myApptId = frmSurgicalAI.AppointmentId
    If (IsApptwithSurgeryAction(myApptId, ClnId)) Then
        SurgeryAppointmentSet = ClnId
        SpecifySurgeryAppointment = True
        Exit Function
    End If
    ApptTypeId = GetSurgeryOrderApptType
    frmEventMsgs.Header = "When was this surgery ordered ?"
    frmEventMsgs.AcceptText = "Select a Past Visit"
    frmEventMsgs.RejectText = ""
    frmEventMsgs.RejectText = "Enter an Order Date"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        frmReviewAppts.SetApptOn = True
        frmReviewAppts.SetApptId = 0
        If (frmReviewAppts.LoadApptsList(PatId, 0, False)) Then
            frmReviewAppts.Show 1
            myApptId = frmReviewAppts.SetApptId
            If Not (IsApptwithSurgeryAction(myApptId, ClnId)) Then
                SpecifySurgeryAppointment = SetClinicalSurgery(myApptId, PatId)
            End If
        End If
        frmReviewAppts.SetApptOn = False
        frmReviewAppts.SetApptId = 0
    ElseIf (frmEventMsgs.Result = 2) Then
' Need the Order Date
        frmQuickPay.OrderDate = ""
        frmQuickPay.OrderDateOn = True
        frmQuickPay.Show 1
' Build an appointment
        If (Trim(frmQuickPay.OrderDate) <> "") Then
            ADate = frmQuickPay.OrderDate
            ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
            LocId = val(Trim(Mid(lstLocs.List(lstLocs.ListIndex), 57, Len(lstLocs.List(lstLocs.ListIndex)) - 56)))
            InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
            Set ApplClm = New ApplicationClaims
            myApptId = ApplClm.BuildAppointment(PatId, DoctorId, LocId, ApptTypeId, ADate, "M", ReferralId, 0, False, False)
            Set ApplClm = Nothing
            If (myApptId > 0) Then
                Dim RetApt As New SchedulerAppointment
                RetApt.AppointmentId = myApptId
                If (RetApt.RetrieveSchedulerAppointment) Then
                    RetApt.AppointmentComments = "ADD VIA SURGERY ORDER"
                    Call RetApt.ApplySchedulerAppointment
                End If
                SpecifySurgeryAppointment = SetClinicalSurgery(myApptId, PatId)
            End If
        End If
        frmQuickPay.OrderDate = ""
        frmQuickPay.OrderDateOn = False
    ElseIf (frmEventMsgs.Result = 3) Then
        myApptId = frmDisposition.AppointmentId
        If Not (IsApptwithSurgeryAction(myApptId, ClnId)) Then
            SpecifySurgeryAppointment = SetClinicalSurgery(myApptId, PatId)
            SurgeryAppointmentSet = ClnId
        End If
    End If
End If
End Function

Private Function SetClinicalSurgery(ApptId As Long, PatId As Long) As Boolean
Dim RetCln As PatientClinical
SetClinicalSurgery = False
If (PatId > 0) And (ApptId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = 0
    If (RetCln.RetrievePatientClinical) Then
        RetCln.PatientId = PatId
        RetCln.AppointmentId = ApptId
        RetCln.Symptom = "1"
        RetCln.ClinicalType = "A"
        RetCln.ClinicalSurgery = "T"
        RetCln.ClinicalDraw = ""
        RetCln.ClinicalPermanent = ""
        RetCln.ClinicalHighlights = ""
        RetCln.ClinicalFollowup = ""
        RetCln.ClinicalPostOpPeriod = 0
        RetCln.ImageDescriptor = ""
        RetCln.ImageInstructions = ""
        RetCln.EyeContext = ""
        RetCln.Findings = "SCHEDULE SURGERY-3/Not Selected-F/-1/-7/"
        SetClinicalSurgery = RetCln.ApplyPatientClinical
        If SetClinicalSurgery Then
            SurgeryAppointmentSet = RetCln.ClinicalId
        End If
    End If
    Set RetCln = Nothing
End If
End Function

Private Function IsApptwithSurgeryAction(ApptId As Long, ClnId As Long) As Boolean
Dim i As Integer
Dim RetCln As PatientClinical
IsApptwithSurgeryAction = False
ClnId = 0
If (ApptId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalType = "A"
    RetCln.AppointmentId = ApptId
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        Do Until Not (RetCln.SelectPatientClinical(i))
            If (Left(RetCln.Findings, 16) = "SCHEDULE SURGERY") Then
                IsApptwithSurgeryAction = True
                ClnId = RetCln.ClinicalId
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetCln = Nothing
End If
End Function

Private Function PlantSurgery(ApptId As Long, ClnId As Long) As Boolean
Dim RetCln As PatientClinicalSurgeryPlan
PlantSurgery = False
If (ApptId > 0) And (ClnId > 0) Then
    Set RetCln = New PatientClinicalSurgeryPlan
    RetCln.SurgeryId = 0
    If (RetCln.RetrievePatientClinicalPlan) Then
        RetCln.SurgeryClnId = ClnId
        RetCln.SurgeryRefType = "F"
        RetCln.SurgeryValue = Trim(Str(ApptId))
        RetCln.SurgeryStatus = "A"
        Call RetCln.ApplyPatientClinicalPlan
        PlantSurgery = True
    End If
End If
End Function
Private Function GetSurgeryOrderApptType() As Long
Dim RetA As SchedulerAppointmentType
GetSurgeryOrderApptType = 0
Set RetA = New SchedulerAppointmentType
RetA.AppointmentType = "SURGERY ORDER"
If (RetA.FindAppointmentType > 0) Then
    If (RetA.SelectAppointmentType(1)) Then
        GetSurgeryOrderApptType = RetA.AppointmentTypeId
    End If
End If
Set RetA = Nothing
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
