VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmQuickPay 
   BackColor       =   &H0077742D&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Take Patient Payment"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6885
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   6885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkInc 
      BackColor       =   &H0077742D&
      Caption         =   "Include on bills"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   1080
      TabIndex        =   19
      Top             =   2880
      Width           =   2055
   End
   Begin VB.TextBox txtReason 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   20
      TabIndex        =   17
      Top             =   2400
      Width           =   5535
   End
   Begin VB.TextBox txtAdjAmt 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5040
      MaxLength       =   7
      TabIndex        =   5
      Top             =   1800
      Width           =   1575
   End
   Begin VB.ComboBox lstAdj 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1800
      Width           =   2415
   End
   Begin VB.TextBox txtCheck 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4200
      MaxLength       =   20
      TabIndex        =   3
      Top             =   1200
      Width           =   2415
   End
   Begin VB.ComboBox lstMethod 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   4200
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   480
      Width           =   2415
   End
   Begin VB.TextBox txtDate 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   10
      TabIndex        =   1
      Top             =   1200
      Width           =   1455
   End
   Begin VB.TextBox txtAmount 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   7
      TabIndex        =   0
      Top             =   480
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   5040
      TabIndex        =   10
      Top             =   3360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "QuickPay.frx":0000
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2175
      Left            =   1800
      TabIndex        =   14
      Top             =   480
      Width           =   3375
      _Version        =   65537
      _ExtentX        =   5953
      _ExtentY        =   3836
      _StockProps     =   76
   End
   Begin VB.Label lblReason 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Reason:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   18
      Top             =   2400
      Width           =   750
   End
   Begin VB.Label lblAdjAmt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Adj Amt:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4155
      TabIndex        =   16
      Top             =   1800
      Width           =   825
   End
   Begin VB.Label lblAdj 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Adj:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   15
      Top             =   1800
      Width           =   375
   End
   Begin VB.Label lblOrderDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Order Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   720
      TabIndex        =   13
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblBal 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Open Balance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4200
      TabIndex        =   12
      Top             =   120
      Width           =   1260
   End
   Begin VB.Label lblPat 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pat Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   11
      Top             =   120
      Width           =   900
   End
   Begin VB.Label lblCheck 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Check #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3240
      TabIndex        =   9
      Top             =   1200
      Width           =   765
   End
   Begin VB.Label lblPayAmt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   8
      Top             =   480
      Width           =   735
   End
   Begin VB.Label lblPayDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pay Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   840
   End
   Begin VB.Label lblMethod 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Method"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3240
      TabIndex        =   6
      Top             =   480
      Width           =   675
   End
End
Attribute VB_Name = "frmQuickPay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AppealOn As Boolean
Public AppealNumber As Boolean
Public OrderType As String
Public OrderDateOn As Boolean
Public OrderDate As String
Private TheBalance As Single
Private ReceivableId As Long
Private PatientId As Long
Private InvId As String

Private Sub cmdDone_Click()
If (OrderDateOn) Then
    OrderDate = SSMonth1.DateValue
    Call FormatTodaysDate(OrderDate, False)
Else
    Dim IncOn As Boolean
    Dim Adj As String
    Dim AdjPay As String
    Dim Rsn As String
    Dim z As Integer
    Dim Amt As Single
    Dim PlanId As Long
    Dim IType As Integer
    Dim Temp As String
    Dim ItemId As Long
    Dim TheSrv As String
    Dim TheRef As String
    Dim PayType As String
    Dim PayMethod As String
    Dim ARsn As String
    Dim TheCom As Boolean
    Dim TrigOn As Boolean
    Dim ApplList As ApplicationAIList
    Dim ApplTemp As ApplicationTemplates
    Dim ApptId As Long
    Dim TelerikReporting As New Reporting
    Dim ParamArgs(0) As Variant
            
    If Not (VerifyBalance) Then
        frmEventMsgs.Header = "Open Balance Exceeded."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
    If (Trim(txtCheck.Text) = "") And (PayMethod = "K") Then
        frmEventMsgs.Header = "Check must be entered"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtCheck.SetFocus
        Exit Sub
    End If
    If (Trim(txtAmount.Text) = "") Then
        frmEventMsgs.Header = "Payment Amount must be entered"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtAmount.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
    If (Trim(txtDate.Text) = "") Then
        frmEventMsgs.Header = "Payment Date must be entered"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtDate.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
    If (lstMethod.ListIndex < 0) Then
        If (val(Trim(txtAmount.Text)) <> 0) Then
            frmEventMsgs.Header = "Method must be selected"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            lstMethod.SetFocus
            Exit Sub
        End If
    End If
    PayMethod = ""
    If (lstMethod.ListIndex >= 0) Then
        PayMethod = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
    End If
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplPostQuickPayment(ReceivableId, PatientId, InvId, txtAmount.Text, txtDate.Text, "P", PayMethod, Trim(txtCheck.Text), "", False, UserLogin.iId)
    
    If (lstAdj.ListIndex >= 0) Then
        Adj = Mid(lstAdj.List(lstAdj.ListIndex), 40, 1)
    End If
    If (Trim(txtAdjAmt.Text) <> "") Then
        AdjPay = Trim(txtAdjAmt.Text)
    End If
    If (Trim(txtReason.Text) <> "") Then
        Rsn = Trim(txtReason.Text)
    End If
    IncOn = False
    If (chkInc.Value = 1) Then
        IncOn = True
    End If
    If (Trim(Adj) <> "") And (Trim(AdjPay) <> "") Then
        If (Trim(txtCheck.Text) = "") Then
            Call ApplTemp.ApplPostQuickPayment(ReceivableId, PatientId, InvId, AdjPay, txtDate.Text, Adj, "", "", Rsn, IncOn, UserLogin.iId)
        Else
            Call ApplTemp.ApplPostQuickPayment(ReceivableId, PatientId, InvId, AdjPay, txtDate.Text, Adj, PayMethod, Trim(txtCheck.Text), Rsn, IncOn, UserLogin.iId)
        End If
    End If
    If PrintOut Then
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppt(ReceivableId)
        Set ApplList = Nothing
        If (OrderType = "G") Then
            ParamArgs(0) = Array("EncounterId", Str(ApptId))
            'Invoke Patient Glasses telerik Report.
            Call TelerikReporting.ViewReport("Glasses Receipt", ParamArgs)
        Else
            ParamArgs(0) = Array("EncounterId", Str(ApptId))
            'Invoke PatientReceipt telerik Report.
            Call TelerikReporting.ViewReport("Patient Receipt", ParamArgs)
        End If
    End If
    Set ApplTemp = Nothing
End If

Unload frmQuickPay
End Sub

Private Function PrintOut() As Boolean
frmEventMsgs.Header = "Print Receipt?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1

PrintOut = (frmEventMsgs.Result = 2)
End Function

Public Function LoadPayments(RcvId As Long) As Boolean
Dim ATemp As String
Dim PatName As String
Dim RetPat As Patient
Dim RetRcv As PatientReceivables
Dim ApplList As ApplicationAIList
LoadPayments = False
If (RcvId > 0) Then
    lstMethod.Clear
    lstMethod.ListIndex = -1
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetCodes("PayableType", True, lstMethod)
    Set ApplList = Nothing
    lstAdj.Clear
    lstAdj.ListIndex = -1
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetCodes("PaymentType", True, lstAdj)
    Set ApplList = Nothing
    ReceivableId = RcvId
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        Set RetPat = New Patient
        RetPat.PatientId = RetRcv.PatientId
        If (RetPat.RetrievePatient) Then
            PatName = Trim(RetPat.LastName) + ", " + Trim(RetPat.FirstName)
            lblPat.Caption = PatName
            Call DisplayDollarAmount(Trim(Str(RetRcv.ReceivableBalance)), ATemp)
            lblBal.Caption = "Balance: $" + Trim(ATemp)
            TheBalance = RetRcv.ReceivableBalance
            ATemp = ""
            Call FormatTodaysDate(ATemp, False)
            txtDate.Text = ATemp
            PatientId = RetRcv.PatientId
            InvId = RetRcv.ReceivableInvoice
        End If
        Set RetPat = Nothing
    End If
    Set RetRcv = Nothing
    LoadPayments = True
End If
End Function

Private Sub Form_Activate()
OrderDate = ""
If (OrderDateOn) Then
    lblPat.Visible = False
    lblBal.Visible = False
    lblMethod.Visible = False
    lblPayAmt.Visible = False
    lblCheck.Visible = False
    lblPayDate.Visible = False
    lblAdj.Visible = False
    lstAdj.Visible = False
    lblOrderDate.Visible = False
    txtDate.Visible = False
    txtAmount.Visible = False
    txtCheck.Visible = False
    txtAdjAmt.Visible = False
    lstMethod.Visible = False
    lblOrderDate.Visible = True
    SSMonth1.Visible = True
    frmQuickPay.Caption = "Set Surgery Order Date"
    chkInc.Visible = False
    lblReason.Visible = False
    txtReason.Visible = False
Else
    lblPat.Visible = True
    lblBal.Visible = True
    lblMethod.Visible = True
    lblPayAmt.Visible = True
    lblCheck.Visible = True
    lblPayDate.Visible = True
    txtDate.Visible = True
    txtAmount.Visible = True
    txtCheck.Visible = True
    lstMethod.Visible = True
    lblOrderDate.Visible = False
    SSMonth1.Visible = False
    frmQuickPay.Caption = "Take Patient Payment"
'    chkInc.Visible = False
'    lblReason.Visible = False
'    txtReason.Visible = False
    chkInc.Visible = True
    If (OrderType = "G") Then
        chkInc.Visible = False
    End If
    If (OrderType = "G") Then
        lblReason.Caption = "Comment"
    End If
End If
End Sub

Private Sub txtAdjAmt_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
Else
    Call DisplayDollarAmount(txtAdjAmt.Text, Temp)
    txtAdjAmt.Text = Trim(Temp)
    If Not (VerifyBalance) Then
        txtAdjAmt.Text = ""
        txtAdjAmt.SetFocus
    End If
End If
End Sub

Private Sub txtAdjAmt_Validate(Cancel As Boolean)
Call txtAdjAmt_KeyPress(13)
End Sub

Private Sub txtAmount_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
Else
    Call DisplayDollarAmount(txtAmount.Text, Temp)
    txtAmount.Text = Trim(Temp)
    If Not (VerifyBalance) Then
        txtAmount.Text = ""
        txtAmount.SetFocus
    End If
End If
End Sub

Private Function VerifyBalance() As Boolean
Dim mypay As Single
Dim myadj As Single
VerifyBalance = True
mypay = val(txtAmount.Text)
myadj = val(txtAdjAmt.Text)
If (TheBalance < (mypay + myadj)) Then
    frmEventMsgs.Header = "Payment and Adjustments cannot exceed open balance."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    VerifyBalance = False
End If
End Function
Private Sub txtAmount_Validate(Cancel As Boolean)
Call txtAmount_KeyPress(13)
End Sub
Public Sub FrmClose()
Unload Me
End Sub
