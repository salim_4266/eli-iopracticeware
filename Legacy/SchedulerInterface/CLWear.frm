VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCLWear 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "CLWear.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9480
      TabIndex        =   2
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":305A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAny 
      Height          =   990
      Left            =   8760
      TabIndex        =   3
      Top             =   6720
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":3239
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem1 
      Height          =   1095
      Left            =   480
      TabIndex        =   4
      Top             =   720
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":341E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem2 
      Height          =   1095
      Left            =   480
      TabIndex        =   5
      Top             =   1920
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":35FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem3 
      Height          =   1095
      Left            =   480
      TabIndex        =   6
      Top             =   3120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":37DE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem4 
      Height          =   1095
      Left            =   480
      TabIndex        =   7
      Top             =   4320
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":39BE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem5 
      Height          =   1095
      Left            =   480
      TabIndex        =   8
      Top             =   5520
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":3B9E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem6 
      Height          =   1095
      Left            =   3240
      TabIndex        =   9
      Top             =   720
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":3D7E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem7 
      Height          =   1095
      Left            =   3240
      TabIndex        =   10
      Top             =   1920
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":3F5E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem8 
      Height          =   1095
      Left            =   3240
      TabIndex        =   11
      Top             =   3120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":413E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem9 
      Height          =   1095
      Left            =   3240
      TabIndex        =   12
      Top             =   4320
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":431E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem10 
      Height          =   1095
      Left            =   3240
      TabIndex        =   13
      Top             =   5520
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":44FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem11 
      Height          =   1095
      Left            =   6000
      TabIndex        =   14
      Top             =   720
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":46DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem12 
      Height          =   1095
      Left            =   6000
      TabIndex        =   15
      Top             =   1920
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":48C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem13 
      Height          =   1095
      Left            =   6000
      TabIndex        =   16
      Top             =   3120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":4AA1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem14 
      Height          =   1095
      Left            =   6000
      TabIndex        =   17
      Top             =   4320
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":4C82
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem15 
      Height          =   1095
      Left            =   6000
      TabIndex        =   18
      Top             =   5520
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":4E63
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem16 
      Height          =   1095
      Left            =   8760
      TabIndex        =   19
      Top             =   720
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":5044
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem17 
      Height          =   1095
      Left            =   8760
      TabIndex        =   20
      Top             =   1920
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":5225
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem19 
      Height          =   1095
      Left            =   8760
      TabIndex        =   21
      Top             =   4320
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":5406
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem18 
      Height          =   1095
      Left            =   8760
      TabIndex        =   22
      Top             =   3120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":55E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem20 
      Height          =   1095
      Left            =   8760
      TabIndex        =   23
      Top             =   5520
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":57C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1095
      Left            =   480
      TabIndex        =   24
      Top             =   6720
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":59A9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   480
      TabIndex        =   25
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLWear.frx":5B88
   End
   Begin VB.Label ConfirmLabel 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Confirm"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   4800
      TabIndex        =   1
      Top             =   240
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.Label ChoiceLabel 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Choice"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   945
   End
End
Attribute VB_Name = "frmCLWear"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public EyeContext As String
Public CLInventoryId As Long
Public CLPrescription As String
Public AddReading As String
Public BaseCurve As String
Public PeriphCurve As String
Public Diameter As String

Private CLDescription As String
Private ChoiceStage(15) As String
Private TotalItems As Integer
Private CurrentItemIndex As Integer
Private CurrentChoiceStage As Long
Private Criteria As String
Private Eye As String
Private CLDescription1 As String
Private CLDescription2 As String

Private Sub cmdDone_Click()
Unload frmCLWear
End Sub

Private Sub cmdHome_Click()
CLInventoryId = 0
CLPrescription = ""
Unload frmCLWear
End Sub

Private Sub cmdItem1_Click()
Call ChoiceHandler(cmdItem1)
End Sub

Private Sub cmdItem2_Click()
Call ChoiceHandler(cmdItem2)
End Sub

Private Sub cmdItem3_Click()
Call ChoiceHandler(cmdItem3)
End Sub

Private Sub cmdItem4_Click()
Call ChoiceHandler(cmdItem4)
End Sub

Private Sub cmdItem5_Click()
Call ChoiceHandler(cmdItem5)
End Sub

Private Sub cmdItem6_Click()
Call ChoiceHandler(cmdItem6)
End Sub

Private Sub cmdItem7_Click()
Call ChoiceHandler(cmdItem7)
End Sub

Private Sub cmdItem8_Click()
Call ChoiceHandler(cmdItem8)
End Sub

Private Sub cmdItem9_Click()
Call ChoiceHandler(cmdItem9)
End Sub

Private Sub cmdItem10_Click()
Call ChoiceHandler(cmdItem10)
End Sub

Private Sub cmdItem11_Click()
Call ChoiceHandler(cmdItem11)
End Sub

Private Sub cmdItem12_Click()
Call ChoiceHandler(cmdItem12)
End Sub

Private Sub cmdItem13_Click()
Call ChoiceHandler(cmdItem13)
End Sub

Private Sub cmdItem14_Click()
Call ChoiceHandler(cmdItem14)
End Sub

Private Sub cmdItem15_Click()
Call ChoiceHandler(cmdItem15)
End Sub

Private Sub cmdItem16_Click()
Call ChoiceHandler(cmdItem16)
End Sub

Private Sub cmdItem17_Click()
Call ChoiceHandler(cmdItem17)
End Sub

Private Sub cmdItem18_Click()
Call ChoiceHandler(cmdItem18)
End Sub

Private Sub cmdItem19_Click()
Call ChoiceHandler(cmdItem19)
End Sub

Private Sub cmdItem20_Click()
Call ChoiceHandler(cmdItem20)
End Sub

Private Sub cmdMore_Click()
If (CurrentItemIndex > TotalItems) Then
    CurrentItemIndex = 1
End If
Call ItemLoader
End Sub

Private Sub cmdAny_Click()
Call ChoiceHandler(cmdAny)
End Sub

Private Sub ClearItems()
cmdItem1.Visible = False
cmdItem1.Text = ""
cmdItem2.Visible = False
cmdItem2.Text = ""
cmdItem3.Visible = False
cmdItem3.Text = ""
cmdItem4.Visible = False
cmdItem4.Text = ""
cmdItem5.Visible = False
cmdItem5.Text = ""
cmdItem6.Visible = False
cmdItem6.Text = ""
cmdItem7.Visible = False
cmdItem7.Text = ""
cmdItem8.Visible = False
cmdItem8.Text = ""
cmdItem9.Visible = False
cmdItem9.Text = ""
cmdItem10.Visible = False
cmdItem10.Text = ""
cmdItem11.Visible = False
cmdItem11.Text = ""
cmdItem12.Visible = False
cmdItem12.Text = ""
cmdItem13.Visible = False
cmdItem13.Text = ""
cmdItem14.Visible = False
cmdItem14.Text = ""
cmdItem15.Visible = False
cmdItem15.Text = ""
cmdItem16.Visible = False
cmdItem16.Text = ""
cmdItem17.Visible = False
cmdItem17.Text = ""
cmdItem18.Visible = False
cmdItem18.Text = ""
cmdItem19.Visible = False
cmdItem19.Text = ""
cmdItem20.Visible = False
cmdItem20.Text = ""
End Sub

Private Function ItemLoader() As Boolean
Dim i As Integer
Dim Ref As Integer
Dim Temp As String
Dim RetrieveCLInventory As CLInventory
Set RetrieveCLInventory = New CLInventory
Call ClearItems
If (CurrentChoiceStage > 0 And CurrentChoiceStage < 15) Then
    TotalItems = 0
    While (TotalItems < 1) And (CurrentChoiceStage < 15)
        RetrieveCLInventory.FieldSelect = ChoiceStage(CurrentChoiceStage)
        RetrieveCLInventory.Criteria = Criteria
        RetrieveCLInventory.OrderBy = ChoiceStage(CurrentChoiceStage)
        RetrieveCLInventory.FindCLInventorySingleField
        TotalItems = RetrieveCLInventory.CLInventoryTotal
        If (Trim(RetrieveCLInventory.SingleFieldValue) = "") And (TotalItems < 2) Then
            TotalItems = 0
        End If
        If (TotalItems < 1) Then
            CurrentChoiceStage = CurrentChoiceStage + 1
            Call ChoiceStageHandler(False)
        End If
    Wend
    If (TotalItems > 0) Then
        i = 1
        Ref = CurrentItemIndex
        While (RetrieveCLInventory.SelectCLInventory(Ref)) And (i < 21)
            Temp = Trim(RetrieveCLInventory.SingleFieldValue)
            If (Temp <> "") Then
                If (i = 1) Then
                    cmdItem1.Text = Temp
                    cmdItem1.Visible = True
                ElseIf (i = 2) Then
                    cmdItem2.Text = Temp
                    cmdItem2.Visible = True
                ElseIf (i = 3) Then
                    cmdItem3.Text = Temp
                    cmdItem3.Visible = True
                ElseIf (i = 4) Then
                    cmdItem4.Text = Temp
                    cmdItem4.Visible = True
                ElseIf (i = 5) Then
                    cmdItem5.Text = Temp
                    cmdItem5.Visible = True
                ElseIf (i = 6) Then
                    cmdItem6.Text = Temp
                    cmdItem6.Visible = True
                ElseIf (i = 7) Then
                    cmdItem7.Text = Temp
                    cmdItem7.Visible = True
                ElseIf (i = 8) Then
                    cmdItem8.Text = Temp
                    cmdItem8.Visible = True
                ElseIf (i = 9) Then
                    cmdItem9.Text = Temp
                    cmdItem9.Visible = True
                ElseIf (i = 10) Then
                    cmdItem10.Text = Temp
                    cmdItem10.Visible = True
                ElseIf (i = 11) Then
                    cmdItem11.Text = Temp
                    cmdItem11.Visible = True
                ElseIf (i = 12) Then
                    cmdItem12.Text = Temp
                    cmdItem12.Visible = True
                ElseIf (i = 13) Then
                    cmdItem13.Text = Temp
                    cmdItem13.Visible = True
                ElseIf (i = 14) Then
                    cmdItem14.Text = Temp
                    cmdItem14.Visible = True
                ElseIf (i = 15) Then
                    cmdItem15.Text = Temp
                    cmdItem15.Visible = True
                ElseIf (i = 16) Then
                    cmdItem16.Text = Temp
                    cmdItem16.Visible = True
                ElseIf (i = 17) Then
                    cmdItem17.Text = Temp
                    cmdItem17.Visible = True
                ElseIf (i = 18) Then
                    cmdItem18.Text = Temp
                    cmdItem18.Visible = True
                ElseIf (i = 19) Then
                    cmdItem19.Text = Temp
                    cmdItem19.Visible = True
                ElseIf (i = 20) Then
                    cmdItem20.Text = Temp
                    cmdItem20.Visible = True
                End If
            End If
            i = i + 1
            Ref = Ref + 1
            CurrentItemIndex = Ref
        Wend
    ElseIf (CurrentChoiceStage = 15) Then
        RetrieveCLInventory.Criteria = Criteria
        RetrieveCLInventory.OrderBy = "InventoryID"
        ItemLoader = RetrieveCLInventory.FindCLInventory()
        TotalItems = RetrieveCLInventory.CLInventoryTotal
        i = 0
        If (TotalItems = 1) Then
            Temp = Eye + ": " + RetrieveCLInventory.Manufacturer + " " + RetrieveCLInventory.Series
            CLDescription1 = Temp
            Temp = RetrieveCLInventory.Type_ + " " + RetrieveCLInventory.Material + " " + RetrieveCLInventory.Disposable + " " + RetrieveCLInventory.WearTime + " " + RetrieveCLInventory.Tint
            CLDescription2 = Temp
'        If (RetrieveCLInventory.BaseCurve <> "") Then
'            temp = "BC:" + RetrieveCLInventory.BaseCurve + "   D:" + RetrieveCLInventory.Diameter + "   Sphere:" + RetrieveCLInventory.SpherePower
'        End If
'        If (RetrieveCLInventory.CylinderPower <> "") Then
'            temp = temp + "   Cylinder:" + RetrieveCLInventory.CylinderPower
'        End If
'        If (RetrieveCLInventory.Axis <> "") Then
'            temp = temp + "   Axis:" + RetrieveCLInventory.Axis
'        End If
            CLPrescription = CLDescription1 + " " + CLDescription2
            CLInventoryId = RetrieveCLInventory.InventoryId
            ConfirmLabel.Caption = CLDescription1 + Chr(10) + Chr(13) + CLDescription2
        End If
    End If
ElseIf (CurrentChoiceStage = 15) Then
    RetrieveCLInventory.Criteria = Criteria
    RetrieveCLInventory.OrderBy = "InventoryID"
    ItemLoader = RetrieveCLInventory.FindCLInventory()
    TotalItems = RetrieveCLInventory.CLInventoryTotal
    i = 0
    If (TotalItems = 1) Then
        Temp = Eye + ": " + RetrieveCLInventory.Manufacturer + " " + RetrieveCLInventory.Series
        CLDescription1 = Temp
        Temp = RetrieveCLInventory.Type_ + " " + RetrieveCLInventory.Material + " " + RetrieveCLInventory.Disposable + " " + RetrieveCLInventory.WearTime + " " + RetrieveCLInventory.Tint
        CLDescription2 = Temp
'        If (RetrieveCLInventory.BaseCurve <> "") Then
'            temp = "BC:" + RetrieveCLInventory.BaseCurve + "   D:" + RetrieveCLInventory.Diameter + "   Sphere:" + RetrieveCLInventory.SpherePower
'        End If
'        If (RetrieveCLInventory.CylinderPower <> "") Then
'            temp = temp + "   Cylinder:" + RetrieveCLInventory.CylinderPower
'        End If
'        If (RetrieveCLInventory.Axis <> "") Then
'            temp = temp + "   Axis:" + RetrieveCLInventory.Axis
'        End If
        CLPrescription = CLDescription1 + " " + CLDescription2
        CLInventoryId = RetrieveCLInventory.InventoryId
        ConfirmLabel.Caption = CLDescription1 + Chr(10) + Chr(13) + CLDescription2
    End If
End If
If (TotalItems > 20) Then
    cmdMore.Visible = True
Else
    cmdMore.Visible = False
End If
If (CurrentChoiceStage = 6) Then
    cmdAny.Visible = True
Else
    cmdAny.Visible = False
End If
'If (i = 1) Then
'    CurrentChoiceStage = 14
'    Call ChoiceStageHandler
'End If
If (CurrentChoiceStage = 15) Then
    cmdDone.Visible = True
    ConfirmLabel.Visible = True
Else
    cmdDone.Visible = False
    ConfirmLabel.Visible = False
End If
End Function

Private Sub ChoiceStageHandler(ARef As Boolean)
CurrentItemIndex = 1
If (CurrentChoiceStage < 1 Or CurrentChoiceStage > 15) Then
    Unload frmCLWear
ElseIf (CurrentChoiceStage = 1) Then
    ChoiceLabel.Caption = "Select contact lens type:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 2) Then
    ChoiceLabel.Caption = "Select contact lens material:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 3) Then
    ChoiceLabel.Caption = "Select contact lense disposability:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 4) Then
    ChoiceLabel.Caption = "Select contact lense wear time:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 5) Then
    ChoiceLabel.Caption = "Select contact lens manufacturer:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 6) Then
    ChoiceLabel.Caption = "Select contact lens series:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 7) Then
    ChoiceLabel.Caption = "Select contact lens tint:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 8) Then
    ChoiceLabel.Caption = "Select contact lens base curve:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 9) Then
    ChoiceLabel.Caption = "Select contact lens diameter:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 10) Then
    ChoiceLabel.Caption = "Select contact lens sphere power:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 11) Then
    ChoiceLabel.Caption = "Select contact lens cylinder power:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 12) Then
    ChoiceLabel.Caption = "Select contact lens axis:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 13) Then
    ChoiceLabel.Caption = "Select contact lens peripheral curve:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 14) Then
    ChoiceLabel.Caption = "Select contact lens Add Reading:"
    If (ARef) Then
        Call ItemLoader
    End If
ElseIf (CurrentChoiceStage = 15) Then
    ChoiceLabel.Caption = "Confirm contact lens choice:"
    If (ARef) Then
        Call ItemLoader
    End If
End If
End Sub

Private Sub Form_Load()
ChoiceLabel.Caption = ""
ConfirmLabel.Caption = ""
CurrentChoiceStage = 1
Criteria = ""
Eye = EyeContext
ChoiceStage(1) = "Type_"
ChoiceStage(2) = "Material"
ChoiceStage(3) = "Disposable"
ChoiceStage(4) = "WearTime"
ChoiceStage(5) = "Manufacturer"
ChoiceStage(6) = "Series"
ChoiceStage(7) = "Tint"
'ChoiceStage(8) = "BaseCurve"
'ChoiceStage(9) = "Diameter"
'ChoiceStage(10) = "SpherePower"
'ChoiceStage(11) = "CylinderPower"
'ChoiceStage(12) = "Axis"
'ChoiceStage(13) = "PeriphCurve"
'ChoiceStage(14) = "AddReading"
Call ChoiceStageHandler(True)
End Sub

Private Sub ChoiceHandler(AButton As fpBtn)
If (CurrentChoiceStage > 0 And CurrentChoiceStage < 15 And AButton.Name <> "cmdAny") Then
    If (Criteria <> "") Then
        Criteria = Criteria + " AND"
    End If
    Criteria = Criteria + " " + ChoiceStage(CurrentChoiceStage) + " = '" + AButton.Text + "'"
End If
CurrentChoiceStage = CurrentChoiceStage + 1
Call ChoiceStageHandler(True)
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

