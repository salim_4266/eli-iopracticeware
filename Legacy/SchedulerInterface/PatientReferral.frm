VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPatientReferral 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ComboBox lstP1 
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   360
      Style           =   1  'Simple Combo
      TabIndex        =   22
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox txtReferTo 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   60
      TabIndex        =   5
      Top             =   5160
      Width           =   4815
   End
   Begin VB.ListBox lstPayers 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1380
      ItemData        =   "PatientReferral.frx":0000
      Left            =   4515
      List            =   "PatientReferral.frx":0002
      TabIndex        =   21
      Top             =   1680
      Width           =   4935
   End
   Begin VB.TextBox txtVisitsLeft 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   18
      Top             =   4620
      Width           =   975
   End
   Begin VB.TextBox ExpireDate 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   10
      TabIndex        =   3
      Top             =   4125
      Width           =   2415
   End
   Begin VB.TextBox Comments 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1035
      Left            =   4515
      MaxLength       =   64
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   5640
      Width           =   5295
   End
   Begin VB.TextBox ReferralDate 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   10
      TabIndex        =   2
      Top             =   3615
      Width           =   2415
   End
   Begin VB.TextBox ReferralFrom 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   60
      TabIndex        =   1
      Top             =   3120
      Width           =   4935
   End
   Begin VB.TextBox Referral 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   32
      TabIndex        =   0
      Top             =   1185
      Width           =   4935
   End
   Begin VB.TextBox PatientName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   35
      TabIndex        =   7
      Top             =   720
      Width           =   4935
   End
   Begin VB.TextBox ReferralVisits 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4515
      MaxLength       =   3
      TabIndex        =   4
      Top             =   4620
      Width           =   975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   15
      Top             =   6840
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientReferral.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   405
      TabIndex        =   17
      Top             =   6840
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientReferral.frx":01E3
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   20
      Top             =   1680
      Width           =   2100
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "# of Visits Left"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6120
      TabIndex        =   19
      Top             =   4620
      Width           =   2100
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Expiration Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   16
      Top             =   4125
      Width           =   2100
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "# of Visits"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   14
      Top             =   4620
      Width           =   2100
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Reason"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   13
      Top             =   5640
      Width           =   2100
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Referral To"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   12
      Top             =   5115
      Width           =   2100
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Referral #"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   11
      Top             =   1185
      Width           =   2100
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   10
      Top             =   720
      Width           =   2100
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Create Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   9
      Top             =   3615
      Width           =   2100
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Referred From"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2190
      TabIndex        =   8
      Top             =   3120
      Width           =   2100
   End
End
Attribute VB_Name = "frmPatientReferral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public ThePatientId As Long
Public TheReferralId As Long

Private VisitsTotal As Long
Private ReferralInsurerId As Long
Private ReferralFromDoctor As Long
Private ReferToDoctor As Long

Private Sub cmdHome_Click()
CurrentAction = "HomeNoChanges"
Unload frmPatientReferral
End Sub

Private Sub cmdDone_Click()
On Error GoTo BadNews
Dim RefCnt As Integer
Dim TheVisits As Integer, VLeft As Integer
Dim RefText As String, RefComment As String
Dim SDate As String, EDate As String
Dim ApplTbl As ApplicationTables
Dim ApplSch As ApplicationScheduler
If (Trim(Referral.Text) = "") Then
    frmEventMsgs.Header = "Referral Number must be entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    ReferralDate.SetFocus
    ReferralDate.Text = ""
    SendKeys "{Home}"
    Exit Sub
End If

If (Trim(ReferralDate.Text) = "") Then
    frmEventMsgs.Header = "Referral Date must be entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    ReferralDate.SetFocus
    ReferralDate.Text = ""
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(ExpireDate.Text) = "") Then
    frmEventMsgs.Header = "Expiration Referral Date must be entered"
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    ExpireDate.SetFocus
    ExpireDate.Text = ""
    SendKeys "{Home}"
    Exit Sub
End If
If (lstPayers.ListIndex < 0) Then
    frmEventMsgs.Header = "Must select an Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    lstPayers.SetFocus
    Exit Sub
End If
If (Trim(ReferralFrom.Text) = "") Or (ReferralFromDoctor < 1) Then
    frmEventMsgs.Header = "Must have a referring doctor"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    ReferralFrom.SetFocus
    ReferralFrom.Text = ""
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtReferTo.Text) = "") Then
    frmEventMsgs.Header = "Must have a refer to doctor"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtReferTo.SetFocus
    txtReferTo.Text = ""
    SendKeys "{Home}"
    Exit Sub
End If
RefText = Trim(Referral.Text)
TheVisits = val(UCase(Trim(ReferralVisits.Text)))
VLeft = val(UCase(Trim(txtVisitsLeft.Text))) - 1
If (VLeft < 1) Then
    VLeft = 0
End If
If (TheReferralId < 1) Then
    VLeft = TheVisits
Else
    Set ApplSch = New ApplicationScheduler
    RefCnt = ApplSch.ReferralInUse(TheReferralId, ThePatientId)
    Set ApplSch = Nothing
    VLeft = TheVisits - RefCnt
End If
If (VLeft < 1) Then
    VLeft = 0
End If
SDate = ReferralDate.Text
EDate = ExpireDate.Text
RefComment = UCase(Trim(Comments.Text))
Set ApplTbl = New ApplicationTables
Call ApplTbl.ApplPostReferral(TheReferralId, ThePatientId, RefText, TheVisits, VLeft, SDate, EDate, RefComment, ReferToDoctor, ReferralFromDoctor, ReferralInsurerId)
Set ApplTbl = Nothing
Unload frmPatientReferral
Exit Sub
BadNews:
    MsgBox Error(Err)
    Resume LeaveFast
LeaveFast:
    On Error GoTo 0
End Sub

Public Function ReferralLoadDisplay(PatId As Long, RefId As Long, InsTy As String) As Boolean
Dim p1 As Long, p2 As Long
Dim r1 As String, r2 As String
Dim b1 As Boolean, b2 As Boolean
Dim InsName As String, n1 As String
Dim SDate As String, EDate As String
Dim DocText As String, TheText As String
Dim RefToId As Long, RefFromId As Long, PlanId As Long
Dim i As Integer, TheVisits As Integer, VLeft As Integer
Dim RefText As String, RefComment As String, PatName As String
Dim l1 As String, l2 As String, l3 As String
Dim LocalDate As ManagedDate
Dim ApplTbl As ApplicationTables
Dim ApplSch As ApplicationScheduler
Dim ApplTemp As ApplicationTemplates
ThePatientId = PatId
TheReferralId = 0
lstPayers.Clear
ReferralLoadDisplay = False
If (PatId > 0) Then
    Set LocalDate = New ManagedDate
    Set ApplSch = New ApplicationScheduler
    Set ApplTbl = New ApplicationTables
    Set ApplTemp = New ApplicationTemplates
    If (ApplTemp.ApplGetPatientName(PatId, PatName, p1, r1, b1, p2, r2, b2, False)) Then
        PatientName.Text = PatName
        PatientName.Locked = True
        If (ApplTbl.ApplGetReferral(PatId, RefId, RefText, TheVisits, VLeft, SDate, EDate, RefComment, RefToId, RefFromId, PlanId)) Then
            TheReferralId = RefId
            Referral.Text = RefText
            ReferralVisits.Text = ""
            If (TheVisits > 0) Then
                ReferralVisits.Text = Trim(Str(TheVisits))
            End If
            txtVisitsLeft.Text = Trim(Str(VLeft))
            txtVisitsLeft.Locked = True
            LocalDate.ExposedDate = SDate
            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                ReferralDate.Text = LocalDate.ExposedDate
            End If
            LocalDate.ExposedDate = EDate
            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                ExpireDate.Text = LocalDate.ExposedDate
            End If
            Comments.Text = RefComment
            If (RefFromId > 0) Then
                Call ApplSch.GetReferralFrom(RefFromId, DocText, False)
                ReferralFrom.Text = DocText
                ReferralFromDoctor = val(Trim(Mid(DocText, 57, Len(DocText) - 56)))
            End If
            If (RefToId > 0) Then
                Call ApplSch.GetReferralTo(RefToId, DocText)
                txtReferTo.Text = DocText
                ReferToDoctor = val(Trim(Mid(DocText, 57, Len(DocText) - 56)))
            End If
            ReferralInsurerId = 0
            Call ApplTemp.ApplGetAllPayers(PatId, n1, p1, b1, r1, p2, b2, r2, "", "", lstP1, l1, l2, l3, True, True, True, True)
            For i = 0 To lstP1.ListCount - 1
                lstPayers.AddItem lstP1.List(i)
            Next i
            If (PlanId > 0) Then
                For i = 0 To lstPayers.ListCount - 1
                    If (PlanId = val(Trim(Mid(lstPayers.List(i), 77, Len(lstPayers.List(i)) - 76)))) Then
                        lstPayers.ListIndex = i
                        Exit For
                    End If
                Next i
                If (lstPayers.ListIndex < 0) Then
                    Call ApplSch.GetInsurer(PlanId, InsName)
                    InsName = Trim(InsName) + Space(75 - Len(Trim(InsName))) + Trim(Str(PlanId))
                    lstPayers.AddItem InsName
                    lstPayers.ListIndex = lstPayers.ListCount - 1
                End If
            Else
                If (lstPayers.ListCount > 0) And (RefId < 1) Then
                    lstPayers.ListIndex = 0
                End If
            End If
        Else
            ReferralInsurerId = 0
            Call ApplTemp.ApplGetAllPayers(PatId, n1, p1, b1, r1, p2, b2, r2, "", "", lstP1, l1, l2, l3, True, True, True, True)
            For i = 0 To lstP1.ListCount - 1
                lstPayers.AddItem lstP1.List(i)
            Next i
            If (lstPayers.ListCount > 0) Then
                lstPayers.ListIndex = 0
            End If
        End If
    End If
    Set LocalDate = Nothing
    Set ApplSch = Nothing
    Set ApplTbl = Nothing
    Set ApplTemp = Nothing
    ReferralLoadDisplay = True
End If
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientReferral.BorderStyle = 1
    frmPatientReferral.ClipControls = True
    frmPatientReferral.Caption = Mid(frmPatientReferral.Name, 4, Len(frmPatientReferral.Name) - 3)
    frmPatientReferral.AutoRedraw = True
    frmPatientReferral.Refresh
End If
End Sub

Private Sub lstPayers_Click()
If (lstPayers.ListIndex >= 0) Then
    If (Len(lstPayers.List(lstPayers.ListIndex)) < 70) Then
        ReferralInsurerId = val(Trim(Mid(lstPayers.List(lstPayers.ListIndex), 57, Len(lstPayers.List(lstPayers.ListIndex)) - 56)))
    Else
        ReferralInsurerId = val(Trim(Mid(lstPayers.List(lstPayers.ListIndex), 77, Len(lstPayers.List(lstPayers.ListIndex)) - 76)))
    End If
End If
End Sub

Private Sub Referral_KeyPress(KeyAscii As Integer)
If ((KeyAscii >= 48) And (KeyAscii <= 57)) Or ((UCase(Chr(KeyAscii)) >= "A") And (UCase(Chr(KeyAscii)) <= "Z")) Then

ElseIf (KeyAscii = 32) Or (KeyAscii = 8) Or (KeyAscii = Asc(vbBack)) Or (KeyAscii = Asc(vbCr)) Or (KeyAscii = Asc(vbTab)) Then

Else
    frmEventMsgs.Header = "Invalid Character [A-Z, 0-9, Space]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
    Referral.SetFocus
    Referral.Text = ""
    SendKeys "{Home}"
End If
End Sub

Private Sub ReferralDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(ReferralDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        ReferralDate.SetFocus
        ReferralDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(ReferralDate, "D")
    End If
End If
End Sub

Private Sub ReferralDate_Validate(Cancel As Boolean)
Call ReferralDate_KeyPress(13)
End Sub

Private Sub ExpireDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(ExpireDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        ExpireDate.SetFocus
        ExpireDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(ExpireDate, "D")
    End If
End If
End Sub

Private Sub ExpireDate_Validate(Cancel As Boolean)
Call ExpireDate_KeyPress(13)
End Sub

Private Sub ReferralFrom_Click()
Dim PermOn As Boolean
Dim DisplayText As String
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("REFERREDFROM")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection = "Create Referred From Doctor") Then
        frmVendors.TheType = "D"
        frmVendors.DeleteOn = False
        If (frmVendors.VendorLoadDisplay(0, PermOn)) Then
            frmVendors.Show 1
        End If
        ReferralFromDoctor = frmVendors.TheVendorId
    Else
        ReferralFromDoctor = frmVendors.TheVendorId
        DisplayText = UCase(frmSelectDialogue.Selection)
        ReferralFromDoctor = val(Trim(Mid(DisplayText, 57, Len(DisplayText) - 56)))
        ReferralFrom.Text = DisplayText
    End If
Else
    ReferralFromDoctor = 0
End If
End Sub

Private Sub ReferralFrom_KeyPress(KeyAscii As Integer)
Call ReferralFrom_Click
KeyAscii = 0
End Sub

Private Sub ReferralVisits_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) And (KeyAscii <> 10) And (KeyAscii <> 8) Then
    If Not (IsNumeric(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Numbers [0123456789]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtReferTo_Click()
Dim DisplayText As String
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledDoctor")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    DisplayText = UCase(frmSelectDialogue.Selection)
    ReferToDoctor = val(Trim(Mid(DisplayText, 57, 5)))
    txtReferTo.Text = DisplayText
Else
    ReferToDoctor = 0
End If
End Sub

Private Sub txtReferTo_KeyPress(KeyAscii As Integer)
Call txtReferTo_Click
KeyAscii = 0
End Sub

Private Sub GetCurrentPayers(PatId As Long)
Dim i As Integer
Dim RetString As String
Dim RetFin As PatientFinance
Dim RetrieveInsurer As Insurer
lstPayers.Clear
Set RetrieveInsurer = New Insurer
Set RetFin = New PatientFinance
RetFin.PatientId = PatId
If (RetFin.FindPatientFinancial > 0) Then
    i = 1
    While (RetFin.SelectPatientFinancial(i))
        If (RetFin.FinancialStatus <> "X") Then
            RetrieveInsurer.InsurerId = RetFin.PrimaryInsurerId
            If (RetrieveInsurer.RetrieveInsurer) Then
                RetString = Space(75)
                Mid(RetString, 1, Len(RetrieveInsurer.InsurerName)) = Trim(RetrieveInsurer.InsurerName)
                RetString = RetString + Trim(Str(RetrieveInsurer.InsurerId))
                lstPayers.AddItem RetString
            End If
            If (RetFin.PrimaryPIndicator = 1) Then
                lstPayers.ListIndex = lstPayers.ListCount - 1
            End If
        End If
        i = i + 1
    Wend
    i = 1
    While (RetFin.SelectPatientFinancial(i))
        If (RetFin.FinancialStatus = "X") Then
            RetrieveInsurer.InsurerId = RetFin.PrimaryInsurerId
            If (RetrieveInsurer.RetrieveInsurer) Then
                RetString = Space(75)
                Mid(RetString, 1, Len(RetrieveInsurer.InsurerName)) = Trim(RetrieveInsurer.InsurerName)
                RetString = RetString + Trim(Str(RetrieveInsurer.InsurerId))
                lstPayers.AddItem RetString
            End If
        End If
        i = i + 1
    Wend
End If
Set RetFin = Nothing
Set RetrieveInsurer = Nothing
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

