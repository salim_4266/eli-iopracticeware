VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MsFlxGrd.ocx"
Begin VB.Form frmCollections 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkColl 
      BackColor       =   &H0077742D&
      Caption         =   "Collection Hold"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   960
      Width           =   2175
   End
   Begin VB.ListBox lstLog 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5340
      ItemData        =   "Collections.frx":0000
      Left            =   960
      List            =   "Collections.frx":0002
      TabIndex        =   16
      Top             =   1560
      Visible         =   0   'False
      Width           =   9735
   End
   Begin VB.CheckBox chkWrite 
      BackColor       =   &H0077742D&
      Caption         =   "Exclude WriteOffs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   1200
      Value           =   1  'Checked
      Width           =   2175
   End
   Begin VB.ListBox lstPrint 
      Height          =   450
      ItemData        =   "Collections.frx":0004
      Left            =   2160
      List            =   "Collections.frx":0006
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "Collections.frx":0008
      Left            =   10200
      List            =   "Collections.frx":000A
      TabIndex        =   4
      Top             =   120
      Width           =   1575
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "Collections.frx":000C
      Left            =   8520
      List            =   "Collections.frx":000E
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "Collections.frx":0010
      Left            =   6360
      List            =   "Collections.frx":0012
      TabIndex        =   2
      Top             =   120
      Width           =   2055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   10440
      TabIndex        =   0
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":0014
   End
   Begin MSFlexGridLib.MSFlexGrid lstDocs 
      Height          =   5895
      Left            =   120
      TabIndex        =   1
      Top             =   1560
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   10398
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1095
      Left            =   6120
      TabIndex        =   8
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":01F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   1095
      Left            =   7800
      TabIndex        =   9
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":03D5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1110
      Left            =   120
      TabIndex        =   10
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":05BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   1095
      Left            =   1440
      TabIndex        =   11
      Top             =   7680
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":079B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect 
      Height          =   1095
      Left            =   2520
      TabIndex        =   12
      Top             =   7680
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":097F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWrite 
      Height          =   1095
      Left            =   9120
      TabIndex        =   14
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":0B64
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   1095
      Left            =   4800
      TabIndex        =   15
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":0D4D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdView 
      Height          =   1095
      Left            =   3600
      TabIndex        =   18
      Top             =   7680
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Collections.frx":0F2D
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   240
      TabIndex        =   7
      Top             =   600
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Collections"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   240
      TabIndex        =   6
      Top             =   120
      Width           =   1410
   End
End
Attribute VB_Name = "frmCollections"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MinAge As Integer
Private AlrSnt As Integer
Private DayLstSnt As Integer
Private BadDebtWO As String

Private ResourceId As Long
Private ResourceLocId As Long
Private TheTest As String
Private LtrId As Integer
Private CurrentCol As Integer
Private CurrentRow As Integer
Private m_Direction As String
Private m_SortColumnName As String
Private m_SortColumn As Integer
Private m_SortOrder As SortSettings

Private Sub cmdDisplay_Click()
Call LoadCollections(False, False)
End Sub

Private Sub cmdDone_Click()
Unload frmCollections
End Sub

Private Sub cmdHome_Click()
Unload frmCollections
End Sub

Private Sub cmdClear_Click()
Dim i As Integer
For i = 1 To lstDocs.Rows - 1
    lstDocs.TextMatrix(i, 0) = ""
Next i
End Sub

Private Sub cmdNotes_Click()
Dim i As Integer
Dim k As Integer
Dim PatId As Long
If (lstDocs.Rows > 0) Then
    frmEventMsgs.Header = "Post Notes or View Notes ?"
    frmEventMsgs.AcceptText = "Post"
    frmEventMsgs.RejectText = "View"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        For i = 1 To lstDocs.Rows - 1
            If (lstDocs.TextMatrix(i, 9) <> "") Then
                Call DoPostNote(i)
                lstDocs.TextMatrix(i, 9) = ""
            End If
        Next i
    ElseIf (frmEventMsgs.Result = 2) Then
        If (CurrentRow > 0) Then
            PatId = val(Trim(lstDocs.TextMatrix(CurrentRow, 1)))
            frmNotes.PatientId = PatId
            frmNotes.NoteId = 0
            frmNotes.AppointmentId = 0
            frmNotes.SystemReference = ""
            frmNotes.CurrentAction = ""
            frmNotes.SetTo = "Y"
            frmNotes.MaintainOn = True
            If (frmNotes.LoadNotes) Then
                frmNotes.Show 1
            End If
        End If
    End If
End If
End Sub

Private Sub cmdPrint_Click()
Dim i As Integer
Dim k As Integer
If (lstDocs.Rows > 0) And (LtrId < lstStatus.ListCount) Then
    frmEventMsgs.Header = "Do you want to create all the letters ?"
    frmEventMsgs.AcceptText = "Send"
    frmEventMsgs.RejectText = "Print"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Or (frmEventMsgs.Result = 1) Then
        For i = 1 To lstDocs.Rows - 1
            If (lstDocs.TextMatrix(i, 0) = "Y") Then
                If (frmEventMsgs.Result = 1) Then
                    Call DoCollection(i, LtrId, lstDocs.TextMatrix(i, 5), True)
                Else
                    Call DoCollection(i, LtrId, lstDocs.TextMatrix(i, 5), False)
                End If
                For k = 0 To 100
                    DoEvents
                Next k
            End If
        Next i
    End If
End If
End Sub

Private Sub cmdView_Click()
If (CurrentRow > 0) Then
    Call DoView(CurrentRow)
End If
End Sub

Private Sub cmdWrite_Click()
Dim i As Integer
Dim k As Integer
If (lstDocs.Rows > 0) Then
    frmEventMsgs.Header = "Are You Sure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        For i = 1 To lstDocs.Rows - 1
            If (lstDocs.TextMatrix(i, 0) = "Y") Then
                Call DoWriteoff(i)
            End If
        Next i
        Call LoadCollections(False, False)
    End If
End If
End Sub

Private Sub cmdSelect_Click()
Dim i As Integer
For i = 1 To lstDocs.Rows - 1
    lstDocs.TextMatrix(i, 0) = "Y"
Next i
End Sub

Private Sub Form_Load()
m_SortColumn = 0
m_Direction = "A"
If UserLogin.HasPermission(epPowerUser) Then
    frmCollections.BorderStyle = 1
    frmCollections.ClipControls = True
    frmCollections.Caption = Mid(frmCollections.Name, 4, Len(frmCollections.Name) - 3)
    frmCollections.AutoRedraw = True
    frmCollections.Refresh
End If
End Sub

Private Sub SortByColumn(ByVal sort_column As Integer, PassiveOn As Boolean)
Dim i As Integer
Dim Temp As String
If (sort_column > 0) Then
    lstDocs.Visible = False
    lstDocs.Refresh
    If (InStrPS(lstDocs.TextMatrix(0, sort_column), "Date") > 0) Then
        For i = 1 To lstDocs.Rows - 1
            Temp = lstDocs.TextMatrix(i, sort_column)
            If (Trim(Temp) <> "") And (InStrPS(Temp, "/") > 0) Then
                Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
            End If
            lstDocs.TextMatrix(i, sort_column) = Temp
        Next i
    End If
' Sort using the clicked column.
    lstDocs.col = sort_column
    lstDocs.ColSel = sort_column
    lstDocs.Row = 0
    lstDocs.RowSel = 0
' If this is a new sort column, sort ascending.
' Otherwise switch which sort order we use.
    If m_SortColumn <> sort_column Then
        m_SortOrder = flexSortGenericAscending
        m_Direction = "A"
    ElseIf m_SortOrder = flexSortGenericAscending Then
        If Not (PassiveOn) Then
            m_SortOrder = flexSortGenericDescending
            m_Direction = "D"
        End If
    Else
        If Not (PassiveOn) Then
            m_SortOrder = flexSortGenericAscending
            m_Direction = "A"
        End If
    End If
    lstDocs.Sort = m_SortOrder

' Restore the previous sort column's name.
    If (m_SortColumn <> sort_column) And (m_SortColumn > 0) Then
        If (m_SortColumnName <> "") Then
            lstDocs.TextMatrix(0, m_SortColumn) = m_SortColumnName
        End If
        m_SortColumnName = lstDocs.TextMatrix(0, sort_column)
    ElseIf (Trim(m_SortColumnName) = "") Then
        m_SortColumnName = lstDocs.TextMatrix(0, sort_column)
    End If

' Display the new sort column's name.
    m_SortColumn = sort_column
    If m_SortOrder = flexSortGenericAscending Then
        lstDocs.TextMatrix(0, m_SortColumn) = ">" & m_SortColumnName
    Else
        lstDocs.TextMatrix(0, m_SortColumn) = "<" & m_SortColumnName
    End If
    If (InStrPS(m_SortColumnName, "Date") > 0) Then
        For i = 1 To lstDocs.Rows - 1
            Temp = lstDocs.TextMatrix(i, sort_column)
            If (Trim(Temp) <> "") And (IsNumeric(Temp)) Then
                Temp = Mid(Temp, 5, 2) + "/" + Mid(Temp, 7, 2) + "/" + Left(Temp, 4)
                lstDocs.TextMatrix(i, sort_column) = Temp
            End If
        Next i
    End If
    lstDocs.Visible = True
End If
End Sub

Public Function LoadCollections(InitOn As Boolean, AnyOn As Boolean) As Boolean
Dim p As Integer
Dim i As Integer
Dim m As Integer
Dim TotalLtrs As Integer
Dim Temp As String
Dim ADate As String
Dim DateNow As String
Dim ApplList As ApplicationAIList
BadDebtWO = ">"
' What is this?  A string or an number?  Why assigning a Val() to a string variable?
' Now all GetGlobalEntry are replaced with CheckConfigCollection, but this still doesn't make sense.
'Call GetGlobalEntry("BADDEBTWO", Temp)
'If (Trim(Temp) <> "") Then
'    BadDebtWO = Val(Trim(Temp))
'End If
MinAge = val(CheckConfigCollection("MONTHLYMINIMUMAGE", "0"))
AlrSnt = val(CheckConfigCollection("MONTHLYALREADYSENT", "0"))
DayLstSnt = val(CheckConfigCollection("MONTHLYDAYSLASTSENT", "0"))
TotalLtrs = val(CheckConfigCollection("MONTHLYSENDLETTER", "0"))
LoadCollections = False
ADate = ""
Call FormatTodaysDate(ADate, False)
ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
If (InitOn) Then
    ADate = ""
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstDr
    Call ApplList.ApplLoadStaff(False)
    Set ApplList = Nothing
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstLoc
    Call ApplList.ApplLoadLocation(False, True)
    Set ApplList = Nothing
    lstStatus.AddItem "Candidate Letter 1"
    If (TotalLtrs > 1) Then
        lstStatus.AddItem "Candidate Letter 2"
    End If
    If (TotalLtrs > 2) Then
        lstStatus.AddItem "Candidate Letter 3"
    End If
    If (TotalLtrs > 3) Then
        lstStatus.AddItem "Candidate Letter 4"
    End If
    If (TotalLtrs > 4) Then
        lstStatus.AddItem "Candidate Letter 5"
    End If
    m_Direction = "A"
    ResourceId = -1
    ResourceLocId = -1
    lstLoc.ListIndex = 0
    lstDr.ListIndex = 0
    lstStatus.ListIndex = 0
    LtrId = 1
End If
Set ApplList = New ApplicationAIList
ApplList.ApplStartDate = ADate
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId

' is it collection hold
ApplList.ApplConfirmStat = "-1"
If (chkColl.Value = 1) Then
    ApplList.ApplConfirmStat = "0"
End If
' is exclude writeoff on
ApplList.ApplOtherFilter = "0"
If (chkWrite.Value = 1) Then
    ApplList.ApplOtherFilter = "-1"
End If
Set ApplList.ApplGrid = lstDocs
Call ApplList.ApplRetrieveCollectionList(MinAge, AlrSnt, DayLstSnt, LtrId)
Set ApplList = Nothing
LoadCollections = True
If (InitOn) Then
    If (lstDocs.Rows < 1) Then
        frmEventMsgs.Header = "No Items Found"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
If (AnyOn) Then
    Call SortByColumn(m_SortColumn, False)
Else
    Call SortByColumn(m_SortColumn, True)
End If
End Function

Private Sub lstDocs_EnterCell()
Dim Temp As String
CurrentCol = lstDocs.ColSel
CurrentRow = lstDocs.RowSel
lstDocs.AllowBigSelection = True
lstDocs.Highlight = flexHighlightAlways
lstDocs.AllowBigSelection = False
If (CurrentCol = 9) Then
    Temp = lstDocs.TextMatrix(CurrentRow, CurrentCol)
    lstDocs.TextMatrix(CurrentRow, CurrentCol) = Trim(Temp)
    SendKeys "{End}"
End If
End Sub

Private Sub lstDocs_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
If lstDocs.MouseRow = 0 Then    ' Sort by the clicked column if row zero is hit
    Call SortByColumn(lstDocs.MouseCol, False)
End If
End Sub

Private Sub lstDocs_KeyPress(KeyAscii As Integer)
Dim Temp As String
CurrentCol = lstDocs.ColSel
CurrentRow = lstDocs.RowSel
lstDocs.AllowBigSelection = True
lstDocs.Highlight = flexHighlightAlways
lstDocs.AllowBigSelection = False
If (CurrentCol <> 9) Then
    KeyAscii = 0
Else
    If (KeyAscii <> vbKeyBack) And (KeyAscii > 31) And (KeyAscii < 126) Then
        Temp = lstDocs.TextMatrix(CurrentRow, CurrentCol)
        Temp = Temp + Chr(KeyAscii)
        lstDocs.TextMatrix(CurrentRow, CurrentCol) = Temp
    ElseIf (KeyAscii = vbKeyDelete) Or (KeyAscii = vbKeyBack) Then
        Temp = lstDocs.TextMatrix(CurrentRow, CurrentCol)
        If (Len(Temp) > 0) Then
            Temp = Left(Temp, Len(Temp) - 1)
            lstDocs.TextMatrix(CurrentRow, CurrentCol) = Temp
        End If
    End If
End If
End Sub

Private Sub lstDocs_Click()
Dim RcvId As Long
Dim PatId As Long
Dim Temp As String
Dim PatientDemographics As PatientDemographics
CurrentCol = lstDocs.ColSel
CurrentRow = lstDocs.RowSel
If (CurrentRow = 0) Then
    Exit Sub
End If
If (CurrentCol = 0) Then
    If (Trim(lstDocs.TextMatrix(CurrentRow, 0)) = "") Then
        lstDocs.TextMatrix(CurrentRow, 0) = "Y"
    Else
        lstDocs.TextMatrix(CurrentRow, 0) = " "
    End If
ElseIf (CurrentCol = 1) Then
    PatId = val(Trim(lstDocs.TextMatrix(CurrentRow, 1)))
    If (PatId > 0) Then
        Set PatientDemographics = New PatientDemographics
        PatientDemographics.PatientId = PatId
        Call PatientDemographics.DisplayPatientInfoScreen
        Set PatientDemographics = Nothing
    End If
ElseIf (CurrentCol = 2) Then
    PatId = val(Trim(lstDocs.TextMatrix(CurrentRow, 1)))
    If (PatId > 0) Then
        Set PatientDemographics = New PatientDemographics
        PatientDemographics.PatientId = PatId
        Call PatientDemographics.DisplayPatientInfoScreen
        Set PatientDemographics = Nothing
    End If
ElseIf (CurrentCol = 5) Or (CurrentCol = 7) Then
    PatId = val(Trim(lstDocs.TextMatrix(CurrentRow, 1)))
    RcvId = val(Trim(lstDocs.TextMatrix(CurrentRow, 10)))
    If (PatId > 0) Then
'''''        frmPayments.CurrentAction = ""
'''''        frmPayments.PatientId = PatId
'''''        frmPayments.ReceivableId = RcvID
'''''        If (frmPayments.LoadPayments(False, "")) Then
'''''            frmPayments.AccessType = ""
'''''            frmPayments.CurrentAction = ""
'''''            frmPayments.Whom = False
'''''            frmPayments.Show 1
'''''        End If
            Set PatientDemographics = New PatientDemographics
            PatientDemographics.PatientId = PatId
            Call PatientDemographics.DisplayPatientFinancialScreen
            Set PatientDemographics = Nothing
    End If
ElseIf (CurrentCol = 9) Then
    Temp = lstDocs.TextMatrix(CurrentRow, CurrentCol)
    lstDocs.TextMatrix(CurrentRow, CurrentCol) = Trim(Temp)
    SendKeys "{End}"
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        ResourceLocId = -1
    Else
        Set ApplList = New ApplicationAIList
        ResourceLocId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As ApplicationAIList
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = -1
    Else
        Set ApplList = New ApplicationAIList
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub lstLog_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
lstLog.Visible = False
lstLog.Clear
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    LtrId = lstStatus.ListIndex + 1
End If
End Sub

Private Sub DoCollection(Idx As Integer, LtrId As Integer, Amt As String, PostOn As Boolean)
Dim RcvId As Long, ApptId As Long
Dim PatId As Long, TransId As Long
Dim DateNow As String, Temp As String
Dim RetTrn As PracticeTransactionJournal
Dim RetRcv As PatientReceivables
Dim ApplTemp As ApplicationTemplates
If (Idx > 0) And (LtrId > 0) Then
    Set RetTrn = New PracticeTransactionJournal
    PatId = val(Trim(lstDocs.TextMatrix(Idx, 1)))
    RcvId = val(lstDocs.TextMatrix(Idx, 10))
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        ApptId = RetRcv.AppointmentId
    End If
    Set RetRcv = Nothing
    If (RcvId > 0) And (PatId > 0) And (ApptId > 0) Then
        DateNow = ""
        Call FormatTodaysDate(DateNow, False)
        DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
        Temp = "LTR-" + Trim(Str(LtrId))
        Set RetTrn = New PracticeTransactionJournal
        RetTrn.TransactionJournalId = 0
        If (RetTrn.RetrieveTransactionJournal) Then
            RetTrn.TransactionJournalTypeId = ApptId
            RetTrn.TransactionJournalType = "W"
            RetTrn.TransactionJournalBatch = "-"
            RetTrn.TransactionJournalAction = "P"
            RetTrn.TransactionJournalStatus = "P"
            RetTrn.TransactionJournalRcvrId = 0
            RetTrn.TransactionJournalServiceItem = 0
            RetTrn.TransactionJournalAssign = LtrId
            RetTrn.TransactionJournalTime = -1
            RetTrn.TransactionJournalDate = DateNow
            RetTrn.TransactionJournalReference = "/Ltr/CollectionLtr" + Trim(Str(LtrId))
            RetTrn.TransactionJournalRemark = Amt
            Call RetTrn.ApplyTransactionJournal
            TransId = RetTrn.TransactionJournalId
            If Not (FM.IsFileThere(DocumentDirectory + "Collection" + Trim(Temp) + "_" + Trim(PatId) + "-" + Trim(Str(TransId)) + ".doc")) Then
                lblPrint.Caption = "File Creation In Progress"
                lblPrint.Visible = True
                DoEvents
                Set ApplTemp = New ApplicationTemplates
                Set ApplTemp.lstBox = lstPrint
                Call ApplTemp.PrintTransaction(TransId, PatId, "W", True, True, "/Ltr/" + Trim(Temp), False, 0, "S", "", 0)
                Set ApplTemp = Nothing
                Call PrintDocument("Collections", DocumentDirectory + "Collection" + Trim(Temp) + "_" + Trim(Str(PatId)) + "-" + Trim(Str(TransId)) + ".doc", "")
            Else
                Call PrintDocument("Collections", DocumentDirectory + "Collection" + Trim(Temp) + "_" + Trim(Str(PatId)) + "-" + Trim(Str(TransId)) + ".doc", "")
            End If
            lblPrint.Visible = False
            DoEvents
        End If
        Set RetTrn = Nothing
    End If
End If
End Sub

Private Sub DoWriteoff(Idx As Integer)
Dim i As Integer
Dim ItmId As Long
Dim RcvId As Long
Dim Chg As Single
Dim Bal As Single
Dim DateNow As String
Dim RetRcv As PatientReceivables
Dim RetSrv As PatientReceivableService
Dim RetPay As PatientReceivablePayment
Dim ApplList As ApplicationAIList
If (Idx > 0) Then
    RcvId = val(lstDocs.TextMatrix(Idx, 10))
    If (RcvId > 0) Then
        DateNow = ""
        Call FormatTodaysDate(DateNow, False)
        DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
        Set RetRcv = New PatientReceivables
        RetRcv.ReceivableId = RcvId
        If (RetRcv.RetrievePatientReceivable) Then
            Set RetSrv = New PatientReceivableService
            RetSrv.Invoice = RetRcv.ReceivableInvoice
            If (RetSrv.FindPatientReceivableService > 0) Then
                i = 1
                While (RetSrv.SelectPatientReceivableService(i))
                    Bal = ApplList.ApplGetServiceBalance(RetSrv.Service, RetRcv.ReceivableInvoice, ItmId, Chg, False)
                    If (Bal > 0) Then
                        Set RetPay = New PatientReceivablePayment
                        RetPay.PaymentId = 0
                        If (RetPay.RetrievePatientReceivablePayments) Then
                            RetPay.PaymentAmount = Bal
                            RetPay.PaymentType = BadDebtWO
                            RetPay.PaymentFinancialType = "C"
                            RetPay.PaymentPayerId = 0
                            RetPay.PaymentPayerType = "O"
                            RetPay.PaymentAssignBy = UserLogin.iId
                            RetPay.PaymentDate = DateNow
                            RetPay.PaymentService = RetSrv.Service
                            RetPay.PaymentServiceItem = RetSrv.ItemId
                            RetPay.ReceivableId = RetRcv.ReceivableId
                            Call RetPay.ApplyPatientReceivablePayments
                        End If
                        Set RetPay = Nothing
                    End If
                Wend
            End If
            Set RetSrv = Nothing
            RetRcv.ReceivableWriteOff = "W"
            Call RetRcv.ApplyPatientReceivable
        End If
        Set RetRcv = Nothing
    End If
End If
End Sub

Private Sub DoView(Idx As Integer)
Dim i As Integer
Dim RcvId As Long
Dim DateNow As String
Dim DisplayItem As String
Dim PatName As String
Dim RetRcv As PatientReceivables
Dim RetTrn As PracticeTransactionJournal
If (Idx > 0) Then
    PatName = Trim(lstDocs.TextMatrix(Idx, 2))
    RcvId = val(lstDocs.TextMatrix(Idx, 10))
    If (RcvId > 0) Then
        lstLog.Clear
        lstLog.AddItem "Transaction Log for " + PatName
        Set RetTrn = New PracticeTransactionJournal
        RetTrn.TransactionJournalType = "R"
        RetTrn.TransactionJournalTypeId = RcvId
        RetTrn.TransactionJournalStatus = ""
        RetTrn.TransactionJournalAction = ""
        If (RetTrn.FindTransactionJournal > 0) Then
            i = 1
            While (RetTrn.SelectTransactionJournal(i))
                DisplayItem = Space(75)
                DateNow = Mid(RetTrn.TransactionJournalDate, 7, 4) + Mid(RetTrn.TransactionJournalDate, 1, 2) + Mid(RetTrn.TransactionJournalDate, 4, 2)
                Mid(DisplayItem, 1, 1) = RetTrn.TransactionJournalStatus
                Mid(DisplayItem, 2, 1) = RetTrn.TransactionJournalType
                Mid(DisplayItem, 4, 10) = DateNow
                Mid(DisplayItem, 15, 20) = Trim(RetTrn.TransactionJournalReference)
                Mid(DisplayItem, 37, 20) = Trim(RetTrn.TransactionJournalRemark)
                Mid(DisplayItem, 59, 3) = Trim(RetTrn.TransactionJournalAssign)
                DisplayItem = DisplayItem + Trim(Str(RetTrn.TransactionJournalId))
                lstLog.AddItem DisplayItem
                i = i + 1
            Wend
        End If
        Set RetTrn = Nothing
        Set RetTrn = New PracticeTransactionJournal
        RetTrn.TransactionJournalType = "W"
        RetTrn.TransactionJournalTypeId = RcvId
        RetTrn.TransactionJournalStatus = ""
        RetTrn.TransactionJournalAction = ""
        If (RetTrn.FindTransactionJournal > 0) Then
            i = 1
            While (RetTrn.SelectTransactionJournal(i))
                DisplayItem = Space(75)
                DateNow = Mid(RetTrn.TransactionJournalDate, 7, 4) + Mid(RetTrn.TransactionJournalDate, 1, 2) + Mid(RetTrn.TransactionJournalDate, 4, 2)
                Mid(DisplayItem, 1, 1) = RetTrn.TransactionJournalStatus
                Mid(DisplayItem, 2, 1) = RetTrn.TransactionJournalType
                Mid(DisplayItem, 4, 10) = DateNow
                Mid(DisplayItem, 15, 20) = Trim(RetTrn.TransactionJournalReference)
                Mid(DisplayItem, 37, 20) = Trim(RetTrn.TransactionJournalRemark)
                Mid(DisplayItem, 59, 3) = Trim(RetTrn.TransactionJournalAssign)
                DisplayItem = DisplayItem + Trim(Str(RetTrn.TransactionJournalId))
                lstLog.AddItem DisplayItem
                i = i + 1
            Wend
        End If
        Set RetTrn = Nothing
        Set RetTrn = New PracticeTransactionJournal
        RetTrn.TransactionJournalType = "S"
        RetTrn.TransactionJournalTypeId = RcvId
        RetTrn.TransactionJournalStatus = ""
        RetTrn.TransactionJournalAction = ""
        If (RetTrn.FindTransactionJournal > 0) Then
            i = 1
            While (RetTrn.SelectTransactionJournal(i))
                DisplayItem = Space(75)
                DateNow = Mid(RetTrn.TransactionJournalDate, 7, 4) + Mid(RetTrn.TransactionJournalDate, 1, 2) + Mid(RetTrn.TransactionJournalDate, 4, 2)
                Mid(DisplayItem, 1, 1) = RetTrn.TransactionJournalStatus
                Mid(DisplayItem, 2, 1) = RetTrn.TransactionJournalType
                Mid(DisplayItem, 4, 10) = DateNow
                Mid(DisplayItem, 15, 20) = Trim(RetTrn.TransactionJournalReference)
                Mid(DisplayItem, 37, 20) = Trim(RetTrn.TransactionJournalRemark)
                Mid(DisplayItem, 59, 3) = Trim(RetTrn.TransactionJournalAssign)
                DisplayItem = DisplayItem + Trim(Str(RetTrn.TransactionJournalId))
                lstLog.AddItem DisplayItem
                i = i + 1
            Wend
        End If
        Set RetTrn = Nothing
    End If
    lstLog.Visible = True
End If
End Sub

Private Sub DoPostNote(Idx As Integer)
Dim RcvId As Long
Dim PatId As Long
Dim ApptId As Long
Dim DateNow As String
Dim RetRcv As PatientReceivables
Dim RetNote As PatientNotes
If (Idx > 0) Then
    If (Trim(lstDocs.TextMatrix(Idx, 9)) <> "") Then
        RcvId = val(lstDocs.TextMatrix(Idx, 10))
        If (RcvId > 0) Then
            DateNow = ""
            Call FormatTodaysDate(DateNow, False)
            DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
            Set RetRcv = New PatientReceivables
            RetRcv.ReceivableId = RcvId
            If (RetRcv.RetrievePatientReceivable) Then
                PatId = RetRcv.PatientId
                ApptId = RetRcv.AppointmentId
                Set RetNote = New PatientNotes
                RetNote.NotesId = 0
                If (RetNote.RetrieveNotes) Then
                    RetNote.NotesPatientId = PatId
                    RetNote.NotesAppointmentId = ApptId
                    RetNote.NotesDate = DateNow
                    RetNote.NotesType = "Y"
                    RetNote.NotesText1 = Trim(lstDocs.TextMatrix(Idx, 9))
                    RetNote.NotesText2 = ""
                    RetNote.NotesText3 = ""
                    RetNote.NotesText4 = ""
                    RetNote.NotesUser = Str(UserLogin.iId)
                    RetNote.NotesSystem = ""
                    RetNote.NotesEye = ""
                    RetNote.NotesCategory = ""
                    RetNote.NotesAlertMask = ""
                    RetNote.NotesCommentOn = False
                    RetNote.NotesClaimOn = False
                    RetNote.NotesILPNRef = ""
                    RetNote.NotesHighlight = ""
                    If Not (RetNote.ApplyNotes) Then
                        Call PinpointError("frmNotes-Done", "Update Failed")
                    End If
                End If
                Set RetNote = Nothing
            End If
        End If
        Set RetRcv = Nothing
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

