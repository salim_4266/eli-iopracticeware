VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "SSCALB32.OCX"
Begin VB.Form frmScheduleWeekly 
   BackColor       =   &H0073702B&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00FF0000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstStartTime 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   705
      ItemData        =   "ScheduleWeekly.frx":0000
      Left            =   6840
      List            =   "ScheduleWeekly.frx":0002
      MultiSelect     =   1  'Simple
      TabIndex        =   5
      Top             =   1320
      Visible         =   0   'False
      Width           =   1095
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   1935
      Left            =   7920
      TabIndex        =   0
      Top             =   240
      Width           =   3135
      _Version        =   65537
      _ExtentX        =   5530
      _ExtentY        =   3413
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   14285823
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   870
      Left            =   5280
      TabIndex        =   1
      Top             =   1200
      Width           =   1380
      _Version        =   131072
      _ExtentX        =   2434
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleWeekly.frx":0004
   End
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   1995
      Left            =   0
      TabIndex        =   2
      Top             =   240
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   2090
      _StockProps     =   79
      Caption         =   "Sunday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay2 
      Height          =   3345
      Left            =   0
      TabIndex        =   6
      Top             =   2280
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "Monday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay3 
      Height          =   3345
      Left            =   3960
      TabIndex        =   7
      Top             =   2280
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "Tuesday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay4 
      Height          =   3345
      Left            =   7920
      TabIndex        =   8
      Top             =   2280
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "Wednesday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay5 
      Height          =   3075
      Left            =   0
      TabIndex        =   9
      Top             =   5640
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "Thurday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay6 
      Height          =   3075
      Left            =   3960
      TabIndex        =   10
      Top             =   5640
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "Friday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay7 
      Height          =   3075
      Left            =   7920
      TabIndex        =   11
      Top             =   5640
      Width           =   3975
      _Version        =   65537
      _ExtentX        =   7011
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "Saturday"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
      TimeSelectionBar=   0   'False
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0073702B&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Dr. Jones Schedule for Thursday, January 16, 1999"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   255
      Left            =   3600
      TabIndex        =   4
      Top             =   0
      Width           =   4770
   End
   Begin VB.Label lblDetails 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Appointment Details"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   8640
      Visible         =   0   'False
      Width           =   1965
   End
End
Attribute VB_Name = "frmScheduleWeekly"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private ApptDate As String
Private TheDoctorId As Long
Private DontWorry As Boolean
Private CurrentSelectedDate As String

Private Sub cmdExit_Click()
CurrentSelectedDate = ""
Unload frmScheduleWeekly
End Sub

Public Function LoadDoctorSchedule(DoctorId As Long, ADate As String) As Boolean
Dim i As Integer
Dim TheName As String, TheDate As String
Dim ApplList As ApplicationAIList
Dim ApplSch As ApplicationScheduler
DontWorry = False
TheDate = ""
If (Trim(ADate) <> "") Then
    TheDate = ADate
    ApptDate = ADate
End If
Call FormatTodaysDate(TheDate, False)
Set ApplList = New ApplicationAIList
Call ApplList.ApplGetDoctor(DoctorId, TheName)
Label1.Caption = Trim(Left(TheName, 30)) + " for Week Beginning " + TheDate
Set ApplList = Nothing
Call GetWeekStart(TheDate, ApptDate)
Set ApplSch = New ApplicationScheduler
Set ApplSch.lstStartTime = lstStartTime
LoadDoctorSchedule = ApplSch.LoadDoctorScheduleWeekly(ApptDate, DoctorId, SSDay1, SSDay2, SSDay3, SSDay4, SSDay5, SSDay6, SSDay7, SSMonth1)
Set ApplSch = Nothing
CurrentSelectedDate = ApptDate
DontWorry = True
TheDoctorId = DoctorId
End Function

Private Sub SSDay1_Click()
Dim k As Integer
If (SSDay1.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay1.Tasks.Item(SSDay1.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay1_DblClick()
Call SSDay1_Click
End Sub

Private Sub SSDay2_Click()
Dim k As Integer
If (SSDay2.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay2.Tasks.Item(SSDay2.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay2_DblClick()
Call SSDay2_Click
End Sub

Private Sub SSDay3_Click()
Dim k As Integer
If (SSDay3.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay3.Tasks.Item(SSDay3.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay3_DblClick()
Call SSDay3_Click
End Sub

Private Sub SSDay4_Click()
Dim k As Integer
If (SSDay4.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay4.Tasks.Item(SSDay4.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay4_DblClick()
Call SSDay4_Click
End Sub

Private Sub SSDay5_Click()
Dim k As Integer
If (SSDay5.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay5.Tasks.Item(SSDay5.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay5_DblClick()
Call SSDay5_Click
End Sub

Private Sub SSDay6_Click()
Dim k As Integer
If (SSDay6.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay6.Tasks.Item(SSDay6.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay6_DblClick()
Call SSDay6_Click
End Sub

Private Sub SSDay7_Click()
Dim k As Integer
If (SSDay7.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay7.Tasks.Item(SSDay7.TaskSelected).Text
    lblDetails.Visible = True
End If
End Sub

Private Sub SSDay7_DblClick()
Call SSDay7_Click
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
Dim DocId As Long
Dim ADate As String
If (Trim(SelDate) <> "") And (DontWorry) Then
    OldSelDate = SelDate
    ADate = SelDate
    DocId = TheDoctorId
    Call FormatTodaysDate(ADate, False)
    ApptDate = ADate
    If (ADate <> CurrentSelectedDate) Then
        CurrentSelectedDate = ADate
        Call LoadDoctorSchedule(DocId, ADate)
    End If
End If
End Sub

Private Sub SSMonth1_SelChanged(SelDate As String, OldSelDate As String, Selected As Integer)
Call SSMonth1_SelChange(SelDate, OldSelDate, Selected, False)
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
