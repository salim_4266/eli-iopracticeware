VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "HomeExternalFeatureHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler

Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)
    
    Select Case e.FeatureKey
        ' Administrative section
            Case "Administrative_Confirmations"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdConfirms.Value = True
                End If
            Case "Administrative_Contacts"
                frmHome.cmdMain24.Value = True
            Case "Administrative_Inventory"
                'Spectales or CL?
                frmHome.cmdMain29.Value = True
            Case "Administrative_Miscellaneous Letters"
                If PrepareExecutePendingTransactionsFeature() Then
                    frmReview.cmdMisc.Value = True
                End If
            Case "Administrative_Submit Miscellaneous Letters"
                If PrepareExecuteSubmitTransactionsFeature() Then
                    frmReview.cmdMisc.Value = True
                End If
            Case "Administrative_Patient Lists/Reminders"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdPatientList.Value = True
                End If
            Case "Administrative_Patient Locator"
                frmHome.cmdMain9.Value = True
            Case "Administrative_Patient Station"
                frmHome.cmdMain6.Value = True
            Case "Administrative_Waiting Room"
                frmHome.cmdMain7.Value = True
            Case "Administrative_Exam Room"
                frmHome.cmdMain8.Value = True
            Case "Administrative_Recalls"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdRecall.Value = True
                End If
            Case "Administrative_Pre-Certifications"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdPreCert.Value = True
                End If
            Case "Administrative_Notes"
                'Practice Notes
                frmHome.cmdMain28.Value = True
            Case "Administrative_Clock In"
                frmHome.cmdClockIn.Value = True
            Case "Administrative_Clock Out"
                frmHome.cmdClockOut.Value = True
            Case "Administrative_Check In"
                frmHome.cmdMain1.Value = True
            Case "Administrative_Check Out"
                frmHome.cmdMain2.Value = True
            'TODO: showing links doesn't work, because it toggles buttons on VB6 homescreen
            'Case "Administrative_Links"
            '    frmHome.cmdINet.Value = True
                        
      ' Billing section
            Case "Billing_Pending Claims"
                If PrepareExecutePendingTransactionsFeature() Then
                    frmReview.cmdClaims.Value = True
                End If
            Case "Billing_Patient Statements"
                If PrepareExecuteSubmitTransactionsFeature() Then
                    Call ShowPatientStatements
                End If
            Case "Billing_Billing Sent"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdClaimSent.Value = True
                End If
            Case "Billing_Remittances"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdRemit.Value = True
                End If
            Case "Billing_Insurer Payments"
                frmHome.cmdMain13.Value = True
            Case "Billing_Patient Payments"
                frmHome.cmdMain26.Value = True
            Case "Billing_Appeals"
                If PrepareExecutePendingTransactionsFeature() Then
                    frmReview.ShowClaimFollowUp (1)
                End If
            Case "Billing_Denials"
                If PrepareExecutePendingTransactionsFeature() Then
                    frmReview.ShowClaimFollowUp (2)
                End If
                            
        'Clinical section
            Case "Clinical_Rx Status"
                frmHome.cmdRxStatus.Value = True
            Case "Clinical_Direct Messaging"
                frmHome.cmdDirectMsgng.Value = True
            Case "Clinical_Rx Requests"
                frmHome.cmdRxRequest.Value = True
            Case "Clinical_Consultation Letters"
                If PrepareExecutePendingTransactionsFeature() Then
                    frmReview.cmdConsult.Value = True
                End If
             Case "Clinical_Submit Referral Letters"
                If PrepareExecuteSubmitTransactionsFeature() Then
                    frmReview.cmdRefer.Value = True
                End If
            Case "Clinical_Submit Consultation Letters"
                If PrepareExecuteSubmitTransactionsFeature() Then
                    frmReview.cmdConsult.Value = True
                End If
            Case "Clinical_Transcription"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdTranscrib.Value = True
                End If
            Case "Clinical_Surgeries"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdFacAdm.Value = True
                End If
            Case "Clinical_Chart Requests"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdPtnChartReq.Value = True
                End If
            Case "Clinical_CL Rx"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdCLFulFill.Value = True
                End If
            Case "Clinical_Incomplete Charts"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdUCharts.Value = True
                End If
            Case "Clinical_Follow-Up"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdExternal.Value = True
                End If
            Case "Clinical_Spec Rx"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdPCFulFill.Value = True
                End If
            Case "Clinical_Spec Orders"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdPCOrders.Value = True
                End If
            Case "Clinical_CL Orders"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdCLOrders.Value = True
                End If
            Case "Clinical_Referral Follow-Up"
                If PrepareExecutePendingTransactionsFeature(True) Then
                    frmReview.cmdRefer.Value = True
                End If
                                
        'Generic application
            Case "Application_Settings"
                frmHome.cmdMain16.Value = True
            Case "Application_LogOut"
                frmHome.cmdLogin.Value = True
                                
            Case Else
                Call ShowUnknownEventWarning
    End Select
End Sub

' Copy & pasted code from cmdClaims_Click() on frmReview so we can show the screen directly
Private Sub ShowPatientStatements()
    'S=Statements, small 's' stands for Monthly Statements
    If (frmReviewDocs.LoadDocumentsList(True, "R", "S", True)) Then
        frmReviewDocs.Show 1
    Else
        frmEventMsgs.Header = "No Claims"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End Sub

Private Function PrepareExecutePendingTransactionsFeature(Optional ByVal AllowWithSubmitPermission As Boolean = False) As Boolean

    PrepareExecutePendingTransactionsFeature = False
    If (frmHome.Login()) Then
        Dim HasReviewPermission As Boolean
        HasReviewPermission = UserLogin.HasPermission(epReview)
        If (HasReviewPermission Or (AllowWithSubmitPermission And UserLogin.HasPermission(epSubmit))) Then
            'User does have permission
            PrepareExecutePendingTransactionsFeature = True
            'Prepare review form (If user doesn't have Review permission then default to SubmitTransactions layout)
            frmReview.SendOn = Not HasReviewPermission
            frmReview.PatientId = 0
        Else
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If

End Function

Private Function PrepareExecuteSubmitTransactionsFeature(Optional ByVal AllowWithReviewPermission As Boolean = False) As Boolean

    PrepareExecuteSubmitTransactionsFeature = False
    If (frmHome.Login()) Then
        If (UserLogin.HasPermission(epSubmit) Or (AllowWithReviewPermission And UserLogin.HasPermission(epReview))) Then
            'User does have permission
            PrepareExecuteSubmitTransactionsFeature = True
            'Prepare review form
            frmReview.SendOn = True
            frmReview.PatientId = 0
        Else
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If

End Function

Private Function CheckUserHasPermission(epPermission As EPermissions) As Boolean
    'By default have it set to false
    CheckUserHasPermission = False
    
    If frmHome.Login() Then
        If UserLogin.HasPermission(epPermission) Then
            'User does have permission
            CheckUserHasPermission = True
        Else
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If

End Function

Private Sub ShowUnknownEventWarning()
    frmEventMsgs.Header = "We are working to enable access to this functionality. Sorry for inconvenience."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End Sub
