VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCLOrders 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "PatientSearch"
   ClientHeight    =   9000
   ClientLeft      =   120
   ClientTop       =   690
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Moveable        =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtPat 
      Height          =   375
      Left            =   840
      TabIndex        =   18
      Top             =   720
      Width           =   4335
   End
   Begin VB.TextBox txtANote 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   17
      Top             =   7080
      Visible         =   0   'False
      Width           =   11655
   End
   Begin VB.TextBox txtLensOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   5880
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   16
      Top             =   5400
      Visible         =   0   'False
      Width           =   5895
   End
   Begin VB.TextBox txtShipping 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   15
      Top             =   7560
      Visible         =   0   'False
      Width           =   11655
   End
   Begin VB.TextBox txtLensOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   14
      Top             =   5400
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.TextBox txtFrame 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   13
      Top             =   4680
      Visible         =   0   'False
      Width           =   11655
   End
   Begin VB.TextBox txtNote 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      MaxLength       =   128
      TabIndex        =   9
      Top             =   4200
      Visible         =   0   'False
      Width           =   9135
   End
   Begin VB.TextBox txtRef 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   960
      MaxLength       =   14
      TabIndex        =   7
      Top             =   3840
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.ListBox lstMan 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      ItemData        =   "CLOrders.frx":0000
      Left            =   7080
      List            =   "CLOrders.frx":0002
      TabIndex        =   6
      Top             =   120
      Width           =   2655
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      ItemData        =   "CLOrders.frx":0004
      Left            =   9840
      List            =   "CLOrders.frx":0006
      TabIndex        =   3
      Top             =   120
      Width           =   2055
   End
   Begin VB.ListBox OrdersList 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2430
      ItemData        =   "CLOrders.frx":0008
      Left            =   120
      List            =   "CLOrders.frx":000A
      TabIndex        =   0
      Top             =   1320
      Width           =   11775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   750
      Left            =   10200
      TabIndex        =   1
      Top             =   8040
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrders.frx":000C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   750
      Left            =   120
      TabIndex        =   2
      Top             =   8040
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrders.frx":01EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   735
      Left            =   10200
      TabIndex        =   10
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrders.frx":03CA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   735
      Left            =   5160
      TabIndex        =   11
      Top             =   8040
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrders.frx":05A9
   End
   Begin VB.TextBox txtOrder 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   4680
      Visible         =   0   'False
      Width           =   11655
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   19
      Top             =   720
      Width           =   615
   End
   Begin VB.Label lblNote 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   12
      Top             =   4200
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Label lblRef 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Order #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   8
      Top             =   3840
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Orders"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   885
   End
End
Attribute VB_Name = "frmCLOrders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OrderType As String
Private PatientId As Long
Private StartDate As String
Private EndDate As String
Public AllDatesOn As Boolean

Private Sub cmdDone_Click()
Unload frmCLOrders
End Sub

Private Sub cmdHome_Click()
Unload frmCLOrders
End Sub

Private Sub cmdNotes_Click()
Dim PatId As Long
Dim OrderId As Long
Dim RetrieveCLOrders As CLOrders
If (OrdersList.ListIndex >= 0) Then
    OrderId = OrdersList.ItemData(OrdersList.ListIndex)
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.OrderId = OrderId
    If (RetrieveCLOrders.RetrieveCLOrders) Then
        PatId = RetrieveCLOrders.PatientId
    End If
    If (PatId > 0) Then
        frmNotes.PatientId = PatId
        frmNotes.NoteId = val(Trim(txtOrder.Tag))
        If (OrderType = "G") Then
            frmNotes.NoteId = val(Trim(txtANote.Tag))
        End If
        frmNotes.AppointmentId = 0
        frmNotes.SystemReference = ""
        frmNotes.SetTo = "C"
        frmNotes.MaintainOn = True
        If (frmNotes.LoadNotes) Then
            frmNotes.Show 1
        End If
    End If
End If
End Sub

Private Sub cmdPost_Click()
Dim OrderId As Long
If (OrdersList.ListIndex >= 0) Then
    OrderId = OrdersList.ItemData(OrdersList.ListIndex)
    If (SetCLDetail(OrderId, OrderType, Trim(txtRef.Text), Trim(txtNote.Text))) Then
        Call DisplayOrder(OrderId)
    End If
End If
End Sub

Private Sub Form_Load()
PatientId = 0
AllDatesOn = False
StartDate = ""
Call FormatTodaysDate(StartDate, False)
EndDate = ""
Call FormatTodaysDate(EndDate, False)
txtPat.Text = ""
Call LoadOrdersList(True)
End Sub

Private Sub LoadOrdersList(Init As Boolean)
Dim i As Integer
Dim TheDate As String, Ref As String
Dim Temp As String, CLMan As String
Dim APhone As String, PatName As String
Dim AStatus As String, DisplayItem As String
Dim OpenBalance As Single
Dim RetCodes As PracticeCodes
Dim RetrievePatient As Patient
Dim RetrieveCLOrders As CLOrders
Dim RetrieveCLInventory As CLInventory
Dim RetrievePCInventory As PCInventory
Dim RS As Recordset
txtOrder.Visible = False
txtFrame.Visible = False
txtLensOD.Visible = False
txtLensOS.Visible = False
txtANote.Visible = False
txtShipping.Visible = False
Set RetrieveCLOrders = New CLOrders
If (Init) Then
    If (lstStatus.ListCount < 1) Then
        lstStatus.Clear
        Set RetCodes = New PracticeCodes
        If (OrderType = "C") Then
            RetCodes.ReferenceType = "CLOrderStatus"
        Else
            RetCodes.ReferenceType = "PCOrderStatus"
        End If
        If (RetCodes.FindCode > 0) Then
            i = 1
            While (RetCodes.SelectCode(i))
                If (Trim(Left(RetCodes.ReferenceCode, 20)) <> "Dispensed") And (Trim(Left(RetCodes.ReferenceCode, 20)) <> "Auto Order") Then
                    lstStatus.AddItem Trim(RetCodes.ReferenceCode)
                    lstStatus.ItemData(lstStatus.NewIndex) = val(Trim(RetCodes.ReferenceAlternateCode))
                End If
                i = i + 1
            Wend
        End If
        Set RetCodes = Nothing
    End If
    Label1.Caption = "Spectacle Orders"
    If (OrderType = "C") Then
        If (lstMan.ListCount < 2) Then
            lstMan.Visible = False
            lstMan.Clear
            lstMan.AddItem "Any Manufacturer"
            lstMan.ItemData(0) = 0
            Set RetrieveCLInventory = New CLInventory
            If (RetrieveCLInventory.FindCLManufacturers > 0) Then
                i = 1
                While (RetrieveCLInventory.SelectCLManufacturers(i))
                    lstMan.AddItem Trim(RetrieveCLInventory.Manufacturer)
                    i = i + 1
                Wend
            End If
            Set RetrieveCLInventory = Nothing
            lstMan.Visible = True
        End If
        Label1.Caption = "Contact Lens Orders"
    Else
        lstMan.Visible = False
    End If
End If
If (Trim(StartDate) <> "") And (Trim(EndDate) <> "") Then
    TheDate = StartDate
    Call FormatTodaysDate(TheDate, False)
    StartDate = TheDate
    TheDate = EndDate
    Call FormatTodaysDate(TheDate, False)
    EndDate = TheDate
Else
    TheDate = ""
    Call FormatTodaysDate(TheDate, False)
    StartDate = TheDate
    EndDate = TheDate
End If
CLMan = ""
If (lstMan.ListIndex > 0) Then
    CLMan = Trim(lstMan.List(lstMan.ListIndex))
End If
Ref = ""
If (PatientId > 0) Then
    AllDatesOn = True
    RetrieveCLOrders.Criteria = "PatientId = " + Trim(Str(PatientId)) + " "
    Ref = "And "
    RetrieveCLOrders.Criteria = RetrieveCLOrders.Criteria + "And OrderDate <= '" + Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2) + "' "
    RetrieveCLOrders.OrderBy = "OrderDate DESC"
Else
    If (lstStatus.ListIndex >= 0) Then
        RetrieveCLOrders.Criteria = "Status = " + Trim(Str(lstStatus.ItemData(lstStatus.ListIndex))) + " "
    Else
        If Not (AllDatesOn) Then
            RetrieveCLOrders.Criteria = RetrieveCLOrders.Criteria + Ref + "Status <> 5 "
        End If
    End If
    If Not (AllDatesOn) Then
        RetrieveCLOrders.Criteria = RetrieveCLOrders.Criteria + "And OrderDate <= '" + Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2) + "' And OrderDate >= '" + Mid(EndDate, 7, 4) + Mid(EndDate, 1, 2) + Mid(EndDate, 4, 2) + "' "
        RetrieveCLOrders.OrderBy = "OrderDate"
    Else
        RetrieveCLOrders.Criteria = RetrieveCLOrders.Criteria + "And OrderDate <= '" + Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2) + "' "
        RetrieveCLOrders.OrderBy = "OrderDate DESC"
    End If
End If
If (OrderType = "C") Then
    RetrieveCLOrders.FindCLOrders
Else
    RetrieveCLOrders.FindPCOrders
End If
i = 1
OrdersList.Clear
While (RetrieveCLOrders.SelectCLOrders(i))
    If (OrderType = "C") Then
        Set RetrieveCLInventory = New CLInventory
        RetrieveCLInventory.Criteria = "InventoryID = " + CStr(RetrieveCLOrders.InventoryId)
        RetrieveCLInventory.FindCLInventory
        'If (RetrieveCLInventory.SelectCLInventory(1)) Then
            If (Trim(RetrieveCLInventory.Manufacturer) = Trim(CLMan)) Or (Trim(CLMan) = "") Then
                GoSub GetName
                AStatus = "        "
                If (RetrieveCLOrders.Status = 1) Then
                    AStatus = "Ordered "
                ElseIf (RetrieveCLOrders.Status = 2) Then
                    AStatus = "Auto    "
                ElseIf (RetrieveCLOrders.Status = 4) Then
                    AStatus = "Allocate"
                ElseIf (RetrieveCLOrders.Status = 5) Then
                    AStatus = "Filled  "
                ElseIf (RetrieveCLOrders.Status = 10) Then
                    AStatus = "Manual "
                End If
                DisplayItem = Space(98)
                Mid(DisplayItem, 1, Len(PatName)) = PatName
                Mid(DisplayItem, 30, 10) = Mid(RetrieveCLOrders.OrderDate, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderDate, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderDate, 1, 4)
                Mid(DisplayItem, 42, 14) = APhone
                Mid(DisplayItem, 56, 2) = RetrieveCLOrders.Eye
                Mid(DisplayItem, 59, 5) = Trim(Str(RetrieveCLOrders.Qty))
                OpenBalance = GetOpenBalance(RetrieveCLOrders.ReceivableId)
               
                Call DisplayDollarAmount(Trim(Str(OpenBalance)), Temp)
                Mid(DisplayItem, 65, 7) = "$" + Trim(Temp)
                Mid(DisplayItem, 73, 8) = AStatus
                If (Trim(RetrieveCLOrders.OrderComplete) <> "") Then
                    Mid(DisplayItem, 82, 10) = Mid(RetrieveCLOrders.OrderComplete, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 1, 4)
                End If
                Mid(DisplayItem, 93, 5) = Trim(Str(RetrieveCLInventory.Qty))
                OrdersList.AddItem DisplayItem
                OrdersList.ItemData(OrdersList.NewIndex) = RetrieveCLOrders.OrderId
            End If
        'End If
        Set RetrieveCLInventory = Nothing
    Else
        Set RetrievePCInventory = New PCInventory
        RetrievePCInventory.Criteria = "InventoryID = " + CStr(RetrieveCLOrders.InventoryId)
        RetrievePCInventory.FindPCInventory
        'If (RetrievePCInventory.SelectPCInventory(1)) Then
            GoSub GetName
            AStatus = Space(16)
            If (RetrieveCLOrders.Status = 1) Then
                AStatus = "Ordered         "
            ElseIf (RetrieveCLOrders.Status = 3) Then
                AStatus = "Ordered         "
            ElseIf (RetrieveCLOrders.Status = 4) Then
                AStatus = "Ordered         "
            ElseIf (RetrieveCLOrders.Status = 5) Then
                AStatus = "Dispensed       "
            ElseIf (RetrieveCLOrders.Status = 6) Then
                AStatus = "Received        "
            ElseIf (RetrieveCLOrders.Status = 7) Then
                AStatus = "Ready for Pickup"
            ElseIf (RetrieveCLOrders.Status = 8) Then
                AStatus = "Called Patient  "
            ElseIf (RetrieveCLOrders.Status = 10) Then
                AStatus = "To Be Ordered   "
            End If
            DisplayItem = Space(98)
            Mid(DisplayItem, 1, Len(PatName)) = PatName
            Mid(DisplayItem, 30, 10) = Mid(RetrieveCLOrders.OrderDate, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderDate, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderDate, 1, 4)
            Mid(DisplayItem, 42, 14) = APhone
            OpenBalance = GetOpenBalance(RetrieveCLOrders.ReceivableId)
            Call DisplayDollarAmount(Trim(Str(OpenBalance)), Temp)
            Mid(DisplayItem, 56, 7) = "$" + Trim(Temp)
            Mid(DisplayItem, 66, 16) = AStatus
            If (Trim(RetrieveCLOrders.OrderComplete) <> "") Then
                Mid(DisplayItem, 82, 10) = Mid(RetrieveCLOrders.OrderComplete, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 1, 4)
            End If
            Mid(DisplayItem, 93, 5) = Trim(Str(RetrievePCInventory.Qty))
            OrdersList.AddItem DisplayItem
            OrdersList.ItemData(OrdersList.NewIndex) = RetrieveCLOrders.OrderId
        'End If
        Set RetrievePCInventory = Nothing
    End If
    i = i + 1
Wend
Set RetrieveCLOrders = Nothing
lblRef.Visible = False
txtRef.Visible = False
txtRef.Text = ""
cmdNotes.Visible = False
lblNote.Visible = False
txtNote.Text = ""
txtNote.Visible = False
txtOrder.Visible = False
txtOrder.Text = ""
txtFrame.Visible = False
txtFrame.Text = ""
txtLensOD.Visible = False
txtLensOD.Text = ""
txtLensOS.Visible = False
txtLensOS.Text = ""
txtANote.Visible = False
txtANote.Text = ""
txtShipping.Visible = False
txtShipping.Text = ""
cmdPost.Visible = False
Exit Sub
GetName:
    Set RetrievePatient = New Patient
    RetrievePatient.PatNumber = RetrieveCLOrders.PatientId
    RetrievePatient.Status = ""
    RetrievePatient.FindPatientDirect
    If (RetrievePatient.SelectPatient(1)) Then
        If (Trim(RetrievePatient.FirstName) <> "") Then
            PatName = Trim(RetrievePatient.LastName) + ", " _
                    + Trim(RetrievePatient.FirstName)
        Else
            PatName = Trim(RetrievePatient.LastName)
        End If
        Set RS = GetPatientPrimaryPhoneNumber(RetrieveCLOrders.PatientId)
        If Not RS.EOF Then
            Call DisplayPhone(RS("PhoneNumber"), APhone)
        End If
        Set RS = Nothing
    End If
    Set RetrievePatient = Nothing
    Return
End Sub

Private Sub lstMan_Click()
If (lstMan.ListIndex >= 0) Then
    AllDatesOn = True
    PatientId = 0
    txtPat.Text = ""
    DoEvents
    Call LoadOrdersList(False)
End If
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    AllDatesOn = True
    PatientId = 0
    txtPat.Text = ""
    DoEvents
    Call LoadOrdersList(False)
End If
End Sub

Private Sub OrdersList_Click()
Dim Idx As Integer
Dim OrderId As Long
' this will just display
If (OrdersList.ListIndex >= 0) Then
    Idx = OrdersList.ListIndex
    OrderId = OrdersList.ItemData(Idx)
    If (OrderId > 0) Then
        If (OrdersList.Selected(Idx)) Then
            Call DisplayOrder(OrderId)
        Else
            lblRef.Visible = False
            txtRef.Visible = False
            txtRef.Text = ""
            cmdNotes.Visible = False
            lblNote.Visible = False
            txtNote.Text = ""
            txtNote.Visible = False
            txtOrder.Visible = False
            txtOrder.Text = ""
            txtFrame.Visible = False
            txtFrame.Text = ""
            txtLensOD.Visible = False
            txtLensOD.Text = ""
            txtLensOS.Visible = False
            txtLensOS.Text = ""
            txtANote.Visible = False
            txtANote.Text = ""
            txtShipping.Visible = False
            txtShipping.Text = ""
            cmdPost.Visible = False
        End If
    Else
        lblRef.Visible = False
        txtRef.Visible = False
        txtRef.Text = ""
        cmdNotes.Visible = False
        lblNote.Visible = False
        txtNote.Text = ""
        txtNote.Visible = False
        txtOrder.Visible = False
        txtOrder.Text = ""
        txtFrame.Visible = False
        txtFrame.Text = ""
        txtLensOD.Visible = False
        txtLensOD.Text = ""
        txtLensOS.Visible = False
        txtLensOS.Text = ""
        txtANote.Visible = False
        txtANote.Text = ""
        txtShipping.Visible = False
        txtShipping.Text = ""
        cmdPost.Visible = False
    End If
Else
    lblRef.Visible = False
    txtRef.Visible = False
    txtRef.Text = ""
    cmdNotes.Visible = False
    lblNote.Visible = False
    txtNote.Text = ""
    txtNote.Visible = False
    txtOrder.Visible = False
    txtOrder.Text = ""
    txtFrame.Visible = False
    txtFrame.Text = ""
    txtLensOD.Visible = False
    txtLensOD.Text = ""
    txtLensOS.Visible = False
    txtLensOS.Text = ""
    txtANote.Visible = False
    txtANote.Text = ""
    txtShipping.Visible = False
    txtShipping.Text = ""
    cmdPost.Visible = False
End If
End Sub

Private Sub OrdersList_DblClick()
Dim i As Integer
Dim Idx As Integer
Dim RcvId As Long
Dim OrderId As Long
Dim Temp As String
Dim MyStatus As String
Dim RetrieveCLOrders As CLOrders
Dim ApplTemp As ApplicationTemplates
'
'  Note: the OrdersList listbox is multiselect:
'  multiselect works for status, order and delete only
'  current is used for clinical, demo, financial
'
If (OrdersList.ListIndex >= 0) Then
    If Not (OrdersList.Selected(OrdersList.ListIndex)) Then
        OrdersList.Selected(OrdersList.ListIndex) = True
    End If
End If
If (OrdersList.SelCount > 0) And (OrdersList.ListIndex >= 0) Then
    Idx = OrdersList.ListIndex
    OrderId = OrdersList.ItemData(Idx)
    If (OrderId > 0) Then
        Set RetrieveCLOrders = New CLOrders
        RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
        RetrieveCLOrders.OrderBy = "OrderID"
        
        If OrderType = "G" Then
            RetrieveCLOrders.FindPCOrders
        Else
            RetrieveCLOrders.FindCLOrders
        End If
        
        RcvId = RetrieveCLOrders.ReceivableId
        frmEventMsgs.Header = "Action ?"
        frmEventMsgs.AcceptText = "Clinical"
        frmEventMsgs.RejectText = "Patient Info"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Financial"
        If (OrderType = "G") Then
            frmEventMsgs.Other0Text = "Post Payments"
        End If
        frmEventMsgs.Other1Text = "Status"
        frmEventMsgs.Other2Text = ""
        If (RetrieveCLOrders.Status <> 5) Then
            frmEventMsgs.Other2Text = "Delete Orders"
            If (OrderType = "G") Then
                frmEventMsgs.Other2Text = "Edit/Delete Order(s)"
            End If
        End If
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        If (OrderType = "G") Then
            frmEventMsgs.Other4Text = "Print Spectacle Order/Rcpt"
        End If
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            Call DisplayClinical(OrderId)
        ElseIf (frmEventMsgs.Result = 2) Then
            Call DisplayPatient(OrderId)
        ElseIf (frmEventMsgs.Result = 3) Then
            If (OrderType <> "G") Then
                Call DisplayFinancial(OrderId)
            Else
                Call DisplayPayments(OrderId)
            End If
        ElseIf (frmEventMsgs.Result = 5) Then
            MyStatus = GetCLOrderStatus
            If (Trim(MyStatus) <> "") Then
                For i = 0 To OrdersList.ListCount - 1
                    If (OrdersList.Selected(i)) Then
                        Call ChangeStatusOrder(i, val(Trim(MyStatus)))
                    End If
                Next i
            End If
            Call LoadOrdersList(False)
        ElseIf (frmEventMsgs.Result = 6) Then
            If (OrderType = "G") Then
                frmEventMsgs.Header = "Edit Last Selected Order or Delete All Selected Orders."
                frmEventMsgs.AcceptText = "Edit Order"
                frmEventMsgs.RejectText = "Delete Orders"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 2) Then
                    frmEventMsgs.Header = "All order(s) selected will be deleted, the claims remain for any deleted order(s). Are you sure ?"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Yes"
                    frmEventMsgs.CancelText = "No"
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    If (frmEventMsgs.Result = 2) Then
                        For i = 0 To OrdersList.ListCount - 1
                            If (OrdersList.Selected(i)) Then
                                Call DeleteOrder(i)
                            End If
                        Next i
                        Call LoadOrdersList(True)
                    End If
                ElseIf (frmEventMsgs.Result = 1) Then
                    frmPCOrder.DemoOn = False
                    If (frmPCOrder.LoadActiveOrder(OrderId)) Then
                        frmPCOrder.Show 1
                        Call LoadOrdersList(True)
                        OrdersList.ListIndex = Idx
                    End If
                End If
            Else
                frmEventMsgs.Header = "All order(s) selected will be deleted, the claims remain for any deleted order(s). Are you sure ?"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Delete Orders"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 2) Then
                    For i = 0 To OrdersList.ListCount - 1
                        If (OrdersList.Selected(i)) Then
                            Call DeleteOrder(i)
                        End If
                    Next i
                    Call LoadOrdersList(True)
                End If
            End If
        ElseIf (frmEventMsgs.Result = 8) Then
            Dim ApplList As New ApplicationAIList
            Dim ApptId As Long
            ApptId = ApplList.ApplGetAppt(RcvId)
            Set ApplList = Nothing
            Dim TelerikReporting As New Reporting
            Dim ParamArgs(0) As Variant
            
            ParamArgs(0) = Array("EncounterId", Str(ApptId))
            'Invoke Patient Glasses telerik Report.
            Call TelerikReporting.ViewReport("Glasses Receipt", ParamArgs)
        End If
        Set RetrieveCLOrders = Nothing
    Else
        lblRef.Visible = False
        txtRef.Visible = False
        txtRef.Text = ""
        cmdNotes.Visible = False
        lblNote.Visible = False
        txtNote.Text = ""
        txtNote.Visible = False
        txtOrder.Visible = False
        txtOrder.Text = ""
        txtFrame.Visible = False
        txtFrame.Text = ""
        txtLensOD.Visible = False
        txtLensOD.Text = ""
        txtLensOS.Visible = False
        txtLensOS.Text = ""
        txtANote.Visible = False
        txtANote.Text = ""
        txtShipping.Visible = False
        txtShipping.Text = ""
        cmdPost.Visible = False
    End If
End If
End Sub

Private Function DeleteOrder(Idx As Integer) As Boolean
Dim OrderId As Long
Dim RcvId As Long
Dim Temp As String
Dim ATemp As String
Dim ADate As String
Dim RetRcv As PatientReceivables
Dim RetNote As PatientNotes
Dim RetrieveCLOrders As CLOrders
DeleteOrder = False
If (Idx >= 0) Then
    OrderId = OrdersList.ItemData(Idx)
    If (OrderId > 0) Then
        Set RetrieveCLOrders = New CLOrders
        RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
        RetrieveCLOrders.OrderBy = "OrderID"
        If OrderType = "G" Then
            RetrieveCLOrders.FindPCOrders
        Else
            RetrieveCLOrders.FindCLOrders
        End If
        RcvId = RetrieveCLOrders.ReceivableId
        ADate = ""
        Call FormatTodaysDate(ADate, False)
        ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        ATemp = Trim(Str(RetrieveCLOrders.InventoryId)) + " " + RetrieveCLOrders.Eye + " " + Trim(Str(RetrieveCLOrders.Qty)) + " " + Trim(RetrieveCLOrders.ShipTo) + " "
        ATemp = ATemp + Trim(RetrieveCLOrders.ShipAddress1) + " " + Trim(RetrieveCLOrders.ShipAddress2) + " " + Trim(RetrieveCLOrders.ShipCity) + " "
        ATemp = ATemp + Trim(RetrieveCLOrders.ShipState) + " " + Trim(RetrieveCLOrders.ShipZip)
        RetrieveCLOrders.DeleteCLOrders
        Set RetrieveCLOrders = Nothing
        DeleteOrder = True
        If (RcvId > 0) Then
' plant a billing note for them that says the underlying order was cancelled.
            Set RetRcv = New PatientReceivables
            RetRcv.ReceivableId = RcvId
            If (RetRcv.RetrievePatientReceivable) Then
                Set RetNote = New PatientNotes
                RetNote.NotesId = 0
                If (RetNote.RetrieveNotes) Then
                    RetNote.NotesType = "B"
                    RetNote.NotesAppointmentId = RetRcv.AppointmentId
                    RetNote.NotesPatientId = RetRcv.PatientId
                    RetNote.NotesUser = UserLogin.iId
                    RetNote.NotesText1 = "This Order Was Deleted. " + ATemp
                    RetNote.NotesText2 = ""
                    RetNote.NotesText3 = ""
                    RetNote.NotesText4 = ""
                    RetNote.NotesILPNRef = ""
                    RetNote.NotesOffDate = ""
                    RetNote.NotesCommentOn = False
                    RetNote.NotesAudioOn = False
                    RetNote.NotesHighlight = ""
                    RetNote.NotesSystem = "R" + Trim(RetRcv.ReceivableInvoice)
                    RetNote.NotesDate = ADate
                    RetNote.NotesClaimOn = False
                    RetNote.NotesEye = ""
                    RetNote.NotesCategory = ""
                    Call RetNote.ApplyNotes
                End If
                Set RetNote = Nothing
            End If
            Set RetRcv = Nothing
        End If
    End If
End If
End Function

Private Function PlaceOnOrder(Idx As Integer) As Boolean
Dim OrderId As Long
Dim Temp As String
Dim RetrieveCLOrders As CLOrders
PlaceOnOrder = False
If (Idx >= 0) Then
    OrderId = OrdersList.ItemData(Idx)
    If (OrderId > 0) Then
        Set RetrieveCLOrders = New CLOrders
        RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
        RetrieveCLOrders.OrderBy = "OrderID"
        RetrieveCLOrders.FindCLOrders
        RetrieveCLOrders.Status = 3
        Temp = ""
        Call FormatTodaysDate(Temp, False)
        Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
        RetrieveCLOrders.OrderComplete = Temp
        RetrieveCLOrders.PutCLOrder
        Set RetrieveCLOrders = Nothing
        PlaceOnOrder = True
    End If
End If
End Function

Private Function ChangeStatusOrder(Idx As Integer, AStat As Integer) As Boolean
Dim OrderId As Long
Dim Temp As String
Dim RetrieveCLOrders As CLOrders
ChangeStatusOrder = False
If (Idx >= 0) Then
    OrderId = OrdersList.ItemData(Idx)
    If (OrderId > 0) Then
        Set RetrieveCLOrders = New CLOrders
        RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
        RetrieveCLOrders.OrderBy = "OrderID"
        If OrderType = "G" Then
            RetrieveCLOrders.FindPCOrders
        Else
            RetrieveCLOrders.FindCLOrders
        End If
        RetrieveCLOrders.Status = AStat
        If (AStat = 5) Then
            Temp = ""
            Call FormatTodaysDate(Temp, False)
            Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
            RetrieveCLOrders.OrderComplete = Temp
        End If
        RetrieveCLOrders.PutCLOrder
        Set RetrieveCLOrders = Nothing
        ChangeStatusOrder = True
    End If
End If
End Function

Private Sub txtANote_Click()
lblNote.Visible = False
lblRef.Visible = False
txtFrame.Visible = False
txtFrame.Text = ""
txtLensOD.Visible = False
txtLensOD.Text = ""
txtLensOS.Visible = False
txtLensOS.Text = ""
txtANote.Visible = False
txtANote.Text = ""
txtShipping.Visible = False
txtShipping.Text = ""
txtRef.Text = ""
txtRef.Visible = False
txtNote.Text = ""
txtNote.Visible = False
cmdPost.Visible = False
cmdNotes.Visible = False
End Sub

Private Sub txtOrder_Click()
txtOrder.Visible = False
txtOrder.Text = ""
End Sub

Private Sub txtFrame_Click()
lblNote.Visible = False
lblRef.Visible = False
txtFrame.Visible = False
txtFrame.Text = ""
txtLensOD.Visible = False
txtLensOD.Text = ""
txtLensOS.Visible = False
txtLensOS.Text = ""
txtANote.Visible = False
txtANote.Text = ""
txtShipping.Visible = False
txtShipping.Text = ""
txtRef.Text = ""
txtRef.Visible = False
txtNote.Text = ""
txtNote.Visible = False
cmdPost.Visible = False
cmdNotes.Visible = False
End Sub

Private Sub txtPat_KeyPress(KeyAscii As Integer)
KeyAscii = 0
Call txtPat_Click
End Sub

Private Sub txtPat_Click()
Dim RetPat As Patient
Dim ReturnArguments As Variant

Set ReturnArguments = DisplayPatientSearchScreen(False)
If Not (ReturnArguments Is Nothing) Then PatientId = ReturnArguments.PatientId
If (PatientId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        txtPat.Text = Trim(RetPat.LastName) + ", " + Trim(RetPat.FirstName)
    End If
    Set RetPat = Nothing
    Call LoadOrdersList(False)
End If
End Sub

Private Sub txtShipping_Click()
lblNote.Visible = False
lblRef.Visible = False
txtFrame.Visible = False
txtFrame.Text = ""
txtLensOD.Visible = False
txtLensOD.Text = ""
txtLensOS.Visible = False
txtLensOS.Text = ""
txtANote.Visible = False
txtANote.Text = ""
txtShipping.Visible = False
txtShipping.Text = ""
txtRef.Text = ""
txtRef.Visible = False
txtNote.Text = ""
txtNote.Visible = False
cmdPost.Visible = False
cmdNotes.Visible = False
End Sub

Private Sub txtLensOD_Click()
lblNote.Visible = False
lblRef.Visible = False
txtFrame.Visible = False
txtFrame.Text = ""
txtLensOD.Visible = False
txtLensOD.Text = ""
txtLensOS.Visible = False
txtLensOS.Text = ""
txtANote.Visible = False
txtANote.Text = ""
txtShipping.Visible = False
txtShipping.Text = ""
txtRef.Text = ""
txtRef.Visible = False
txtNote.Text = ""
txtNote.Visible = False
cmdPost.Visible = False
cmdNotes.Visible = False
End Sub

Private Sub txtLensOS_Click()
lblNote.Visible = False
lblRef.Visible = False
txtFrame.Visible = False
txtFrame.Text = ""
txtLensOD.Visible = False
txtLensOD.Text = ""
txtLensOS.Visible = False
txtLensOS.Text = ""
txtANote.Visible = False
txtANote.Text = ""
txtShipping.Visible = False
txtShipping.Text = ""
txtRef.Text = ""
txtRef.Visible = False
txtNote.Text = ""
txtNote.Visible = False
cmdPost.Visible = False
cmdNotes.Visible = False
End Sub

Private Sub DisplayOrder(OrderId As Long)
Dim mOwn As String
Dim mSent As String
Dim mUncut As String
Dim ANoteId As Long
Dim i As Integer, j As Integer
Dim CSZ As String, Temp As String
Dim Addr As String, Temp1 As String
Dim ATemp1 As String, ATemp2 As String
Dim ATemp3 As String, ATemp4 As String
Dim ATemp5 As String, ATemp6 As String
Dim ATempOD As String, ATempOS As String
Dim APhone1 As String, APhone2 As String
Dim PatName As String, AStatus As String
Dim myTmp As String
Dim myA As String, myB As String, myED As String
Dim myDBL As String, MyEye As String, myBrg As String
Dim DisplayItem As String
Dim ODRx As String, OSRx As String
Dim RetPrc As PracticeName
Dim RetrievePatient As Patient
Dim RetrieveCLOrders As CLOrders
Dim RetrieveCLInventory As CLInventory
Dim RetrievePCInventory As PCInventory
Dim RS As Recordset
DisplayItem = ""
If (OrderId > 0) Then
    If (OrderType = "C") Then
        txtFrame.Text = ""
        txtLensOD.Text = ""
        txtLensOS.Text = ""
        txtANote.Text = ""
        txtShipping.Text = ""
        txtOrder.Text = ""
        Set RetrieveCLOrders = New CLOrders
        RetrieveCLOrders.OrderId = OrderId
        If (RetrieveCLOrders.RetrieveCLOrders) Then
            Set RetrievePatient = New Patient
            RetrievePatient.PatNumber = RetrieveCLOrders.PatientId
            RetrievePatient.FindPatientDirect
            If (RetrievePatient.SelectPatient(1)) Then
                PatName = Trim(RetrievePatient.LastName) + ", " _
                        + Trim(RetrievePatient.FirstName)
                Set RS = GetPatientPrimaryPhoneNumber(RetrieveCLOrders.PatientId)
                If Not RS.EOF Then
                    Call DisplayPhone(RS("PhoneNumber"), APhone1)
                    APhone1 = RS("PhoneNumberType") + ": " + APhone1
                End If
                Set RS = Nothing
                Addr = Trim(RetrievePatient.Address)
                CSZ = Trim(RetrievePatient.City) + ", " + Trim(RetrievePatient.State) + " " + Trim(RetrievePatient.Zip)
            End If
            Set RetrievePatient = Nothing
            Set RetrieveCLInventory = New CLInventory
            RetrieveCLInventory.Criteria = "InventoryID = " + CStr(RetrieveCLOrders.InventoryId)
            RetrieveCLInventory.FindCLInventory
            'If (RetrieveCLInventory.SelectCLInventory(1)) Then
                DisplayItem = Space(78)
                Mid(DisplayItem, 1, Len(PatName)) = PatName
                Mid(DisplayItem, 30, 18) = Trim(RetrieveCLInventory.Manufacturer)
                Mid(DisplayItem, 50, 40) = Trim(RetrieveCLInventory.Series)
                DisplayItem = Trim(DisplayItem)
                If (Len(DisplayItem) > 75) Then
                    DisplayItem = Left(DisplayItem, 75)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtOrder.Text = txtOrder.Text + DisplayItem

                DisplayItem = Space(78)
                Mid(DisplayItem, 1, 6 + Len(APhone1)) = APhone1
                If (Trim(RetrieveCLInventory.Tint) <> "") Then
                    Mid(DisplayItem, 50, Len(Trim(RetrieveCLInventory.Tint))) = Trim(RetrieveCLInventory.Tint)
                End If
                DisplayItem = Trim(DisplayItem)
                If (Len(DisplayItem) > 75) Then
                    DisplayItem = Left(DisplayItem, 75)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtOrder.Text = txtOrder.Text + DisplayItem

                Temp = ""
                DisplayItem = Space(78)
                If (Trim(RetrieveCLOrders.OrderComplete) <> "") Then
                    Mid(DisplayItem, 30, 18) = "Filled: " + Mid(RetrieveCLOrders.OrderComplete, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 1, 4)
                Else
                    Mid(DisplayItem, 30, 7) = "Filled:"
                End If
                Mid(DisplayItem, 50, 2) = RetrieveCLOrders.Eye
                Mid(DisplayItem, 54, 5) = Trim(Str(RetrieveCLOrders.Qty))
                DisplayItem = Trim(DisplayItem)
                If (Len(DisplayItem) > 75) Then
                    DisplayItem = Left(DisplayItem, 75)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtOrder.Text = txtOrder.Text + DisplayItem

                If (Trim(RetrieveCLOrders.ShipAddress1) <> "") Then
                    Addr = Trim(RetrieveCLOrders.ShipAddress1)
                    CSZ = Trim(RetrieveCLOrders.ShipCity) + ", " + Trim(RetrieveCLOrders.ShipState) + " " + Trim(RetrieveCLOrders.ShipZip)
                ElseIf (RetrieveCLOrders.ShipTo = "O") Then
                    Set RetPrc = New PracticeName
                    RetPrc.PracticeType = "P"
                    RetPrc.PracticeName = Chr(1)
                    If (RetPrc.FindPractice > 0) Then
                        i = 1
                        Do Until Not (RetPrc.SelectPractice(i))
                            If (Trim(RetPrc.PracticeLocationReference) = "") Then
                                Addr = Trim(RetPrc.PracticeAddress) + " " + Trim(RetPrc.PracticeSuite)
                                CSZ = Trim(RetPrc.PracticeCity) + ", " + Trim(RetPrc.PracticeState) + " " + Trim(RetPrc.PracticeZip)
                                Exit Do
                            End If
                            i = i + 1
                        Loop
                    End If
                    Set RetPrc = Nothing
                End If
                Call GetClinicalPrescription(RetrieveCLOrders.AppointmentId, "", ODRx, OSRx)
                DisplayItem = Space(78)
                Mid(DisplayItem, 1, Len(Addr)) = Addr
                If (Trim(ODRx) <> "") Then
                    Mid(DisplayItem, 30, Len(ODRx)) = ODRx
                End If
                If (Len(DisplayItem) > 75) Then
                    DisplayItem = Left(DisplayItem, 75)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtOrder.Text = txtOrder.Text + DisplayItem

                DisplayItem = Space(78)
                Mid(DisplayItem, 1, Len(CSZ)) = CSZ
                If (Trim(OSRx) <> "") Then
                    Mid(DisplayItem, 30, Len(OSRx)) = OSRx
                End If
                If (Len(DisplayItem) > 75) Then
                    DisplayItem = Left(DisplayItem, 75)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtOrder.Text = txtOrder.Text + DisplayItem

                DisplayItem = Space(78)
                Call GetCLNote(OrderId, OrderType, Temp, ANoteId)
                Temp = Trim(Temp)
                If (Len(Temp) > 40) Then
                    Temp = Left(Temp, 40)
                End If
                Mid(DisplayItem, 1, Len(RetrieveCLOrders.OrderReference)) = Trim(RetrieveCLOrders.OrderReference)
                Mid(DisplayItem, 30, Len(Temp)) = Temp
                DisplayItem = DisplayItem + vbCrLf
                txtOrder.Text = txtOrder.Text + DisplayItem
                txtOrder.Tag = Trim(Str(ANoteId))
            'End If
            Set RetrieveCLInventory = Nothing
        End If
    Else
        txtFrame.Text = ""
        txtLensOD.Text = ""
        txtLensOS.Text = ""
        txtShipping.Text = ""
        txtANote.Text = ""
        txtOrder.Text = ""
        Set RetrieveCLOrders = New CLOrders
        RetrieveCLOrders.OrderId = OrderId
        If (RetrieveCLOrders.RetrieveCLOrders) Then
            Set RetrievePatient = New Patient
            RetrievePatient.PatNumber = RetrieveCLOrders.PatientId
            RetrievePatient.Status = ""
            RetrievePatient.FindPatientDirect
            If (RetrievePatient.SelectPatient(1)) Then
                PatName = Trim(RetrievePatient.LastName) + ", " _
                        + Trim(RetrievePatient.FirstName)
                Call DisplayPhone(RetrievePatient.HomePhone, APhone1)
                Call DisplayPhone(RetrievePatient.BusinessPhone, APhone2)
                Addr = Trim(RetrievePatient.Address)
                CSZ = Trim(RetrievePatient.City) + ", " + Trim(RetrievePatient.State) + " " + Trim(RetrievePatient.Zip)
            End If
            Set RetrievePatient = Nothing
            Set RetrievePCInventory = New PCInventory
            RetrievePCInventory.Criteria = "InventoryID = " + CStr(RetrieveCLOrders.InventoryId)
            RetrievePCInventory.FindPCInventory
            'If (RetrievePCInventory.SelectPCInventory(1)) Then
                DisplayItem = Space(110)
                Mid(DisplayItem, 1, Len(PatName)) = PatName
                Mid(DisplayItem, 30, 8) = "Ordered:"
                Mid(DisplayItem, 38, 10) = Mid(RetrieveCLOrders.OrderDate, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderDate, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderDate, 1, 4)
                If (Trim(RetrieveCLOrders.OrderComplete) <> "") Then
                    Mid(DisplayItem, 50, 10) = "Dispensed:"
                    Mid(DisplayItem, 60, 10) = Mid(RetrieveCLOrders.OrderComplete, 5, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 7, 2) + "/" + Mid(RetrieveCLOrders.OrderComplete, 1, 4)
                End If
                Mid(DisplayItem, 71, 16) = APhone1
                Mid(DisplayItem, 88, Len(RetrieveCLOrders.OrderReference) + 9) = "Order #: " + Trim(RetrieveCLOrders.OrderReference)
                If (Len(DisplayItem) > 110) Then
                    DisplayItem = Left(DisplayItem, 110)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtFrame.Text = txtFrame.Text + DisplayItem
                
                DisplayItem = Space(100)
                Mid(DisplayItem, 1, 20) = RetrievePCInventory.Type_
                Mid(DisplayItem, 22, 25) = RetrievePCInventory.Manufacturer
                Mid(DisplayItem, 50, 25) = RetrievePCInventory.Model
                Mid(DisplayItem, 80, 20) = RetrievePCInventory.PCColor
                If (Len(DisplayItem) > 100) Then
                    DisplayItem = Left(DisplayItem, 100)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtFrame.Text = txtFrame.Text + DisplayItem

                myA = ""
                myB = ""
                myED = ""
                myDBL = ""
                MyEye = ""
                myBrg = ""
                myTmp = ""
                
                
                Select Case Mid(RetrieveCLOrders.ShipZip, 2, 1)
                Case 0
                    mOwn = "Yes"
                Case 1
                    mOwn = "No"
                End Select
                Select Case Mid(RetrieveCLOrders.ShipZip, 3, 1)
                Case 0
                    mSent = "Yes"
                Case 1
                    mSent = "No"
                End Select
                Select Case Mid(RetrieveCLOrders.ShipState, 1, 1)
                Case 0
                    mUncut = "Yes"
                Case 1
                    mUncut = "No"
                End Select
                
                i = InStrPS(RetrieveCLOrders.ShipZip, "&")
                If (i > 0) Then
                    ATemp1 = Trim(Mid(RetrieveCLOrders.ShipZip, i + 1 + 2, Len(RetrieveCLOrders.ShipZip) - i))
                    i = InStrPS(ATemp1, "&")
                    If (i > 0) Then
                        myA = Mid(ATemp1, 1, i - 1)
                        j = i + 1
                        i = InStrPS(j, ATemp1, "&")
                        If (i > 0) Then
                            myB = Mid(ATemp1, j, (i - 1) - (j - 1))
                            j = i + 1
                            i = InStrPS(j, ATemp1, "&")
                            If (i > 0) Then
                                myED = Mid(ATemp1, j, (i - 1) - (j - 1))
                                j = i + 1
                                i = InStrPS(j, ATemp1, "&")
                                If (i > 0) Then
                                    myDBL = Mid(ATemp1, j, (i - 1) - (j - 1))
                                    j = i + 1
                                    i = InStrPS(j, ATemp1, "&")
                                    If (i > 0) Then
                                        MyEye = Mid(ATemp1, j, (i - 1) - (j - 1))
                                        j = i + 1
                                        i = InStrPS(j, ATemp1, "&")
                                        If (i > 0) Then
                                            myBrg = Mid(ATemp1, j, (i - 1) - (j - 1))
                                            j = i + 1
                                            i = InStrPS(j, ATemp1, "&")
                                            If (i > 0) Then
                                                myTmp = Mid(ATemp1, j, (i - 1) - (j - 1))
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                DisplayItem = Space(100)
                Mid(DisplayItem, 1, 7) = "A:" + myA
                Mid(DisplayItem, 9, 8) = "B:" + myB
                Mid(DisplayItem, 17, 9) = "ED:" + myED
                Mid(DisplayItem, 26, 10) = "DBL:" + myDBL
                Mid(DisplayItem, 36, 10) = "EYE:" + MyEye
                Mid(DisplayItem, 46, 10) = "BRG:" + myBrg
                Mid(DisplayItem, 56, 10) = "TMP:" + myB
                Mid(DisplayItem, 66, 15) = "Own Frame:" & mOwn
                Mid(DisplayItem, 81, 15) = "Sent Frame:" & mSent
                If (Len(DisplayItem) > 100) Then
                    DisplayItem = Left(DisplayItem, 100)
                End If
                DisplayItem = DisplayItem + vbCrLf
                txtFrame.Text = txtFrame.Text + DisplayItem
            'End If

' Lens OD Information
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 19) = "OD Lens Information"
            Mid(DisplayItem, 40, 10) = "Uncut:" & mUncut
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem
                
            DisplayItem = Space(78)
            Call GetClinicalPrescription(RetrieveCLOrders.AppointmentId, "", ODRx, OSRx)
            If (Trim(ODRx) <> "") Then
                Mid(DisplayItem, 1, Len(ODRx) + 4) = "OD: " + ODRx
            End If
            If (Len(DisplayItem) > 40) Then
                DisplayItem = Left(DisplayItem, 40)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem
            
            ATemp1 = ""
            ATemp2 = ""
            ATemp3 = ""
            ATemp4 = ""
            ATemp5 = ""
            ATemp6 = ""
            Call GetPCParts(Trim(RetrieveCLOrders.ShipAddress1), "OD", ATemp1, ATemp2, ATemp3, ATemp4, ATemp5, ATemp6)

            ATempOD = ""
            ATempOS = ""
            i = InStrPS(RetrieveCLOrders.ShipCity, "OS:")
            If (i > 0) Then
                ATempOD = Trim(Left(RetrieveCLOrders.ShipCity, i - 1))
                Call ReplaceCharacters(ATempOD, "OD:", "~")
                Call StripCharacters(ATempOD, "~")
                ATempOS = Trim(Mid(RetrieveCLOrders.ShipCity, i, Len(RetrieveCLOrders.ShipCity) - (i - 1)))
                Call ReplaceCharacters(ATempOS, "OS:", "~")
                Call StripCharacters(ATempOS, "~")
            End If
                        
            DisplayItem = Space(78)
            ATempOD = ATempOD + " "
            Mid(DisplayItem, 1, 7) = "PD(DV):"
            j = 1
            i = InStrPS(ATempOD, "&")
            If (i = 0) Then
                i = Len(ATempOD) + 1
            End If
            If (i <= Len(ATempOD) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOD, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp3) + 9) = "Material:" + ATemp3
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem
                
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 7) = "PD(DN):"
            j = i + 1
            i = InStrPS(j, ATempOD, "&")
            If (i = 0) Then
                i = Len(ATempOD) + 1
            End If
            If (i <= Len(ATempOD) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOD, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp5) + 6) = "Brand:" + ATemp5
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem
                
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 7) = "Height:"
            j = i + 1
            i = InStrPS(j, ATempOD, "&")
            If (i = 0) Then
                i = Len(ATempOD) + 1
            End If
            If (i <= Len(ATempOD) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOD, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp4) + 7) = "Option:" + ATemp4
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem
                
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 7) = "BC:    "
            j = i + 1
            i = InStrPS(j, ATempOD, "&")
            If (i = 0) Then
                i = Len(ATempOD) + 1
            End If
            If (i <= Len(ATempOD) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOD, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp6) + 5) = "Type:" + ATemp6
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem

            DisplayItem = Space(78)
            Mid(DisplayItem, 1, Len(ATemp2) + 8) = "Coating:" + ATemp2
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOD.Text = txtLensOD.Text + DisplayItem
            
' Lens OS Information
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 19) = "OS Lens Information"
            Mid(DisplayItem, 40, 10) = "Uncut:" & mUncut
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem
            
            DisplayItem = Space(78)
            If (Trim(OSRx) <> "") Then
                Mid(DisplayItem, 1, Len(OSRx) + 4) = "OS: " + OSRx
            End If
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem
            
            ATemp1 = ""
            ATemp2 = ""
            ATemp3 = ""
            ATemp4 = ""
            ATemp5 = ""
            ATemp6 = ""
            Call GetPCParts(Trim(RetrieveCLOrders.ShipAddress2), "OS", ATemp1, ATemp2, ATemp3, ATemp4, ATemp5, ATemp6)
            
            DisplayItem = Space(78)
            ATempOD = ATempOD + " "
            Mid(DisplayItem, 1, 7) = "PD(DV):"
            j = 1
            i = InStrPS(ATempOS, "&")
            If (i = 0) Then
                i = Len(ATempOS) + 1
            End If
            If (i <= Len(ATempOS) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOS, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp3) + 9) = "Material:" + ATemp3
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem
                
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 7) = "PD(DN):"
            j = i + 1
            i = InStrPS(j, ATempOS, "&")
            If (i = 0) Then
                i = Len(ATempOS) + 1
            End If
            If (i <= Len(ATempOS) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOS, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp5) + 6) = "Brand:" + ATemp5
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem

            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 7) = "Height:"
            j = i + 1
            i = InStrPS(j, ATempOS, "&")
            If (i = 0) Then
                i = Len(ATempOS) + 1
            End If
            If (i <= Len(ATempOS) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOS, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp4) + 7) = "Option:" + ATemp4
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem
                
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 7) = "BC:    "
            j = i + 1
            i = InStrPS(j, ATempOS, "&")
            If (i = 0) Then
                i = Len(ATempOS) + 1
            End If
            If (i <= Len(ATempOS) + 1) And (i > j) Then
                Mid(DisplayItem, 8, (i - 1) - (j - 1)) = Mid(ATempOS, j, (i - 1) - (j - 1))
            End If
            Mid(DisplayItem, 15, Len(ATemp6) + 5) = "Type:" + ATemp6
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem

            DisplayItem = Space(78)
            Mid(DisplayItem, 1, Len(ATemp2) + 8) = "Coating:" + ATemp2
            If (Len(DisplayItem) > 50) Then
                DisplayItem = Left(DisplayItem, 50)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtLensOS.Text = txtLensOS.Text + DisplayItem
            
' Notes
            Set RetPrc = New PracticeName
            DisplayItem = Space(110)
            Call GetCLNote(OrderId, OrderType, Temp, ANoteId)
            Temp = Trim(Temp)
            If (Len(Temp) > 110) Then
                Temp = Left(Temp, 110)
            End If
            Mid(DisplayItem, 1, Len(Temp) + 6) = "Note: " + Temp
            If (Len(DisplayItem) > 110) Then
                DisplayItem = Left(DisplayItem, 110)
            End If
            DisplayItem = DisplayItem + vbCrLf + vbCrLf
            txtANote.Text = txtANote.Text + DisplayItem
            txtANote.Tag = Trim(Str(ANoteId))
                
            DisplayItem = Space(78)
            Mid(DisplayItem, 1, 10) = "Shipping: "
            RetPrc.PracticeType = "P"
            RetPrc.PracticeName = Chr(1)
            If (RetPrc.FindPractice > 0) Then
                i = 1
                Do Until Not (RetPrc.SelectPractice(i))
                    If (Trim(RetPrc.PracticeLocationReference) = "") Then
                        Addr = Trim(RetPrc.PracticeAddress) + " " + Trim(RetPrc.PracticeSuite)
                        CSZ = Trim(RetPrc.PracticeCity) + ", " + Trim(RetPrc.PracticeState) + " " + Trim(RetPrc.PracticeZip)
                        Exit Do
                    End If
                    i = i + 1
                Loop
            End If
            Set RetPrc = Nothing
            Mid(DisplayItem, 11, Len(Addr)) = Addr
            Mid(DisplayItem, 42, Len(CSZ)) = CSZ
            If (Len(DisplayItem) > 75) Then
                DisplayItem = Left(DisplayItem, 75)
            End If
            DisplayItem = DisplayItem + vbCrLf
            txtShipping.Text = txtShipping.Text + DisplayItem
        End If
        Set RetrievePCInventory = Nothing
    End If
End If
If (DisplayItem <> "") Then
    If (OrderType = "C") Then
        txtFrame.Visible = False
        txtLensOD.Visible = False
        txtLensOS.Visible = False
        txtShipping.Visible = False
        txtANote.Visible = False
        txtOrder.Visible = True
    Else
        txtFrame.Visible = True
        txtLensOD.Visible = True
        txtLensOS.Visible = True
        txtShipping.Visible = True
        txtANote.Visible = True
        txtOrder.Visible = False
    End If
    lblRef.Visible = True
    txtRef.Visible = True
    txtRef.Text = ""
    cmdNotes.Visible = True
    lblNote.Visible = True
    txtNote.Text = ""
    txtNote.Visible = True
    cmdPost.Visible = True
End If
End Sub

Private Sub DisplayClinical(OrderId As Long)
Dim RetrieveCLOrders As CLOrders
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.OrderId = OrderId
    If (RetrieveCLOrders.RetrieveCLOrders) Then
        frmReviewAppts.StartDate = ""
        DoEvents
        If (frmReviewAppts.LoadApptsList(RetrieveCLOrders.PatientId, RetrieveCLOrders.AppointmentId, True)) Then
            frmReviewAppts.Show 1
        End If
    End If
    Set RetrieveCLOrders = Nothing
End If
End Sub

Private Sub DisplayPatient(OrderId As Long)
Dim RetrieveCLOrders As CLOrders
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.OrderId = OrderId
    If (RetrieveCLOrders.RetrieveCLOrders) Then
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = RetrieveCLOrders.PatientId
        Call PatientDemographics.DisplayPatientInfoScreen
    End If
    Set RetrieveCLOrders = Nothing
End If
End Sub

Private Sub DisplayFinancial(OrderId As Long)
Dim RetrieveCLOrders As CLOrders
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.OrderId = OrderId
    If (RetrieveCLOrders.RetrieveCLOrders) Then
''''        frmPayments.PatientId = RetrieveCLOrders.PatientId
''''        frmPayments.ReceivableId = RetrieveCLOrders.ReceivableId
''''        If (frmPayments.LoadPayments(False, "")) Then
''''            frmPayments.Show 1
''''        Else
''''            frmEventMsgs.Header = "No Active Financials"
''''            frmEventMsgs.AcceptText = ""
''''            frmEventMsgs.RejectText = "Ok"
''''            frmEventMsgs.CancelText = ""
''''            frmEventMsgs.Other0Text = ""
''''            frmEventMsgs.Other1Text = ""
''''            frmEventMsgs.Other2Text = ""
''''            frmEventMsgs.Other3Text = ""
''''            frmEventMsgs.Other4Text = ""
''''            frmEventMsgs.Show 1
''''        End If
            Dim PatientDemographics As New PatientDemographics
            PatientDemographics.PatientId = RetrieveCLOrders.PatientId
            Call PatientDemographics.DisplayPatientFinancialScreen
            Set PatientDemographics = Nothing
    End If
    Set RetrieveCLOrders = Nothing
End If
End Sub

Private Sub DisplayPayments(OrderId As Long)
Dim PatId As Long
Dim RcvId As Long
Dim RetrieveCLOrders As CLOrders
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.OrderId = OrderId
    If (RetrieveCLOrders.RetrieveCLOrders) Then
        PatId = RetrieveCLOrders.PatientId
        RcvId = RetrieveCLOrders.ReceivableId
    End If
    Set RetrieveCLOrders = Nothing
    If (PatId > 0) Then
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = PatId
        Call PatientDemographics.DisplayPatientFinancialScreen
        Set PatientDemographics = Nothing
    End If
End If
End Sub

Private Function GetCLOrderStatus() As String
Dim RetCode As PracticeCodes
GetCLOrderStatus = ""
frmSelectDialogue.InsurerSelected = ""
If (OrderType = "C") Then
    Call frmSelectDialogue.BuildSelectionDialogue("CLOrderStatus")
Else
    Call frmSelectDialogue.BuildSelectionDialogue("PCOrderStatus")
End If
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Set RetCode = New PracticeCodes
    If (OrderType = "C") Then
        RetCode.ReferenceType = "CLOrderStatus"
    Else
        RetCode.ReferenceType = "PCOrderStatus"
    End If
    RetCode.ReferenceCode = Trim(frmSelectDialogue.Selection)
    If (RetCode.FindCode > 0) Then
        Call RetCode.SelectCode(1)
        GetCLOrderStatus = Trim(RetCode.ReferenceAlternateCode)
    End If
    Set RetCode = Nothing
End If
End Function

Private Function GetOpenBalance(RcvId As Long) As Single
Dim RetRcv As PatientReceivables
GetOpenBalance = 0
If (RcvId > 0) Then
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        GetOpenBalance = RetRcv.ReceivableBalance
    End If
    Set RetRcv = Nothing
End If
End Function

Private Function GetClinicalPrescription(ApptId, myRx As String, ODRx As String, OSRx As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim MySearch As String
Dim RetCln As PatientClinical
GetClinicalPrescription = False
ODRx = ""
OSRx = ""
MySearch = "DISPENSE SPEC"
If (OrderType = "C") Then
    MySearch = "DISPENSE CL"
End If
If (Trim(myRx) <> "") Then
    j = InStrPS(myRx, "-2/")
    If (j > 0) Then
        k = InStrPS(j, myRx, "-3/")
        If (k > 0) Then
            ODRx = Mid(myRx, j + 3, (k - 1) - (j + 2))
            Call ReplaceCharacters(ODRx, "ADD-READING:", "ADD ")
            If (OrderType = "C") Then
                Call ReplaceCharacters(ODRx, "BASE CURVE:", "BC ")
                Call ReplaceCharacters(ODRx, "DIAMETER:", "DI ")
                Call ReplaceCharacters(ODRx, "PERIPH CURVE:", "PC ")
            Else
                Call ReplaceCharacters(ODRx, "ADD-INTERMEDIATE:", "AI ")
                Call ReplaceCharacters(ODRx, "PRISM OF ANGLE 1:", "P1 ")
                Call ReplaceCharacters(ODRx, "PRISM OF ANGLE 2:", "P2 ")
            End If
        End If
    End If
    j = InStrPS(myRx, "-3/")
    If (j > 0) Then
        k = InStrPS(j, myRx, "-4/")
        If (k > 0) Then
            OSRx = Mid(myRx, j + 3, (k - 1) - (j + 2))
            Call ReplaceCharacters(OSRx, "ADD-READING:", "ADD ")
            If (OrderType = "C") Then
                Call ReplaceCharacters(OSRx, "BASE CURVE:", "BC ")
                Call ReplaceCharacters(OSRx, "DIAMETER:", "DI ")
                Call ReplaceCharacters(OSRx, "PERIPH CURVE:", "PC ")
            Else
                Call ReplaceCharacters(OSRx, "ADD-INTERMEDIATE:", "AI ")
                Call ReplaceCharacters(OSRx, "PRISM OF ANGLE 1:", "P1 ")
                Call ReplaceCharacters(OSRx, "PRISM OF ANGLE 2:", "P2 ")
            End If
        End If
    End If
    GetClinicalPrescription = True
ElseIf (ApptId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.AppointmentId = ApptId
    RetCln.ClinicalType = "A"
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        Do Until Not (RetCln.SelectPatientClinical(i))
            If (InStrPS(RetCln.Findings, MySearch) > 0) Then
                j = InStrPS(RetCln.Findings, "-2/")
                If (j > 0) Then
                    k = InStrPS(j, RetCln.Findings, "-3/")
                    If (k > 0) Then
                        ODRx = Mid(RetCln.Findings, j + 3, (k - 1) - (j + 2))
                        Call ReplaceCharacters(ODRx, "ADD-READING:", "ADD ")
                        If (OrderType = "C") Then
                            Call ReplaceCharacters(ODRx, "BASE CURVE:", "BC ")
                            Call ReplaceCharacters(ODRx, "DIAMETER:", "DI ")
                            Call ReplaceCharacters(ODRx, "PERIPH CURVE:", "PC ")
                        Else
                            Call ReplaceCharacters(ODRx, "ADD-INTERMEDIATE:", "AI ")
                            Call ReplaceCharacters(ODRx, "PRISM OF ANGLE 1:", "P1 ")
                            Call ReplaceCharacters(ODRx, "PRISM OF ANGLE 2:", "P2 ")
                        End If
                    End If
                End If
                j = InStrPS(RetCln.Findings, "-3/")
                If (j > 0) Then
                    k = InStrPS(j, RetCln.Findings, "-4/")
                    If (k > 0) Then
                        OSRx = Mid(RetCln.Findings, j + 3, (k - 1) - (j + 2))
                        Call ReplaceCharacters(OSRx, "ADD-READING:", "ADD ")
                        If (OrderType = "C") Then
                            Call ReplaceCharacters(OSRx, "BASE CURVE:", "BC ")
                            Call ReplaceCharacters(OSRx, "DIAMETER:", "DI ")
                            Call ReplaceCharacters(OSRx, "PERIPH CURVE:", "PC ")
                        Else
                            Call ReplaceCharacters(OSRx, "ADD-INTERMEDIATE:", "AI ")
                            Call ReplaceCharacters(OSRx, "PRISM OF ANGLE 1:", "P1 ")
                            Call ReplaceCharacters(OSRx, "PRISM OF ANGLE 2:", "P2 ")
                        End If
                    End If
                End If
                GetClinicalPrescription = True
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetCln = Nothing
End If
End Function

Private Sub GetPCParts(ATemp As String, AEye As String, ATemp1 As String, ATemp2 As String, ATemp3 As String, ATemp4 As String, ATemp5 As String, ATemp6 As String)
Dim i As Integer
Dim j As Integer
Dim z As Integer, z1 As Integer
Dim BSrv As String
Dim ASrv As String
Dim ASrvDesc As String
Dim RetSrv As Service
ATemp1 = ""
ATemp2 = ""
ATemp3 = ""
ATemp4 = ""
ATemp5 = ""
ATemp6 = ""
ATemp = UCase(ATemp)
If (Trim(ATemp) <> "") Then
    i = InStrPS(ATemp, AEye + "L:")
    If (i > 0) Then
        j = InStrPS(ATemp, "&")
        If (j > 0) Then
            ATemp1 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
            ASrv = ATemp1
            GoSub GetSrv
            If (Trim(ASrvDesc) <> "") Then
                ATemp1 = ASrvDesc
            End If
        End If
    End If
    i = InStrPS(ATemp, AEye + "C:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp2 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
            BSrv = ""
            z1 = 1
            z = InStrPS(z1, ATemp2, ",")
            While (z > 0)
                ASrv = Mid(ATemp2, z1, (z - 1) - (z1 - 1))
                GoSub GetSrv
                If (Trim(ASrvDesc) <> "") Then
                    If (Trim(BSrv) <> "") Then
                        BSrv = BSrv + ", " + ASrvDesc
                    Else
                        BSrv = ASrvDesc
                    End If
                End If
                z1 = z + 1
                z = InStrPS(z1, ATemp2, ",")
            Wend
            If (z1 < Len(ATemp2)) Then
                ASrv = Mid(ATemp2, z1, Len(ATemp2) - (z1 - 1))
                GoSub GetSrv
                If (Trim(ASrvDesc) <> "") Then
                    If (Trim(BSrv) <> "") Then
                        BSrv = BSrv + ", " + ASrvDesc
                    Else
                        BSrv = ASrvDesc
                    End If
                End If
            End If
            ATemp2 = BSrv
        End If
    End If
    i = InStrPS(ATemp, AEye + "M:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp3 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
            ASrv = ATemp3
            GoSub GetSrv
            If (Trim(ASrvDesc) <> "") Then
                ATemp3 = ASrvDesc
            End If
        End If
    End If
    i = InStrPS(ATemp, AEye + "P:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp4 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
            ASrv = ATemp4
            GoSub GetSrv
            If (Trim(ASrvDesc) <> "") Then
                ATemp4 = ASrvDesc
            End If
        End If
    End If
    i = InStrPS(ATemp, AEye + "B:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp5 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
            ASrv = ATemp5
            GoSub GetSrv
            If (Trim(ASrvDesc) <> "") Then
                ATemp5 = ASrvDesc
            End If
        End If
    End If
    i = InStrPS(ATemp, AEye + "T:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp6 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
            ASrv = ATemp6
            GoSub GetSrv
            If (Trim(ASrvDesc) <> "") Then
                ATemp6 = ASrvDesc
            End If
        End If
    End If
End If
Exit Sub
GetSrv:
    ASrvDesc = ""
    If (Trim(ASrv) <> "") Then
        Call StripCharacters(ASrv, ",")
        Set RetSrv = New Service
        RetSrv.Service = ASrv
        If (RetSrv.FindService > 0) Then
            If (RetSrv.SelectService(1)) Then
                If (ASrv = RetSrv.Service) Then
                    ASrvDesc = Trim(RetSrv.ServiceDescription)
                End If
            End If
        End If
        Set RetSrv = Nothing
    End If
    If (Trim(ASrvDesc) = "") And (Len(ASrv) > 5) Then
        Set RetSrv = New Service
        RetSrv.Service = Left(ASrv, 5)
        If (RetSrv.FindService > 0) Then
            If (RetSrv.SelectService(1)) Then
                If (ASrv = RetSrv.Service) Then
                    ASrvDesc = Trim(RetSrv.ServiceDescription)
                End If
            End If
        End If
        Set RetSrv = Nothing
    End If
    Return
End Sub

Private Function GetCLNote(OrderId As Long, OType As String, AText As String, ANoteId As Long) As Boolean
Dim i As Integer
Dim RetOrder As CLOrders
Dim RetNote As PatientNotes
GetCLNote = False
AText = ""
ANoteId = 0
If (OrderId > 0) Then
    Set RetOrder = New CLOrders
    RetOrder.OrderId = OrderId
    If (RetOrder.RetrieveCLOrders) Then
        Set RetNote = New PatientNotes
        RetNote.NotesAppointmentId = RetOrder.AppointmentId
        If (OType = "C") Then
            RetNote.NotesSystem = "CLO" + Trim(Str(OrderId))
        Else
            RetNote.NotesSystem = "PCO" + Trim(Str(OrderId))
        End If
        If (RetNote.FindNotes > 0) Then
            i = 1
            While (RetNote.SelectNotes(i))
                AText = AText + " " + Trim(RetNote.NotesText1)
                ANoteId = RetNote.NotesId
                GetCLNote = True
                i = i + 1
            Wend
        End If
        Set RetNote = Nothing
    End If
    Set RetOrder = Nothing
End If
End Function

Private Function SetCLDetail(OId As Long, OType As String, ARef As String, ANote As String) As Boolean
Dim ADate As String
Dim RetNote As PatientNotes
Dim RetOrder As CLOrders
SetCLDetail = False
If (OId > 0) Then
    Set RetOrder = New CLOrders
    RetOrder.OrderId = OId
    If (RetOrder.RetrieveCLOrders) Then
        If (Trim(ARef) <> "") Then
            RetOrder.OrderReference = ARef
            Call RetOrder.PutCLOrder
        End If
    End If
    If (Trim(ANote) <> "") Then
        ADate = ""
        Call FormatTodaysDate(ADate, False)
        ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        Set RetNote = New PatientNotes
        RetNote.NotesId = 0
        If (RetNote.RetrieveNotes) Then
            RetNote.NotesAudioOn = False
            RetNote.NotesAlertMask = ""
            RetNote.NotesCategory = ""
            RetNote.NotesClaimOn = False
            RetNote.NotesCommentOn = False
            RetNote.NotesEye = RetOrder.Eye
            RetNote.NotesHighlight = ""
            RetNote.NotesILPNRef = ""
            RetNote.NotesOffDate = ""
            RetNote.NotesPatientId = RetOrder.PatientId
            RetNote.NotesUser = UserLogin.iId
            RetNote.NotesDate = ADate
            RetNote.NotesType = "C"
            RetNote.NotesText1 = Trim(ANote)
            RetNote.NotesText2 = ""
            RetNote.NotesText3 = ""
            RetNote.NotesText4 = ""
            RetNote.NotesAppointmentId = RetOrder.AppointmentId
            If (OType = "C") Then
                RetNote.NotesSystem = "CLO" + Trim(Str(OId))
            Else
                RetNote.NotesSystem = "PCO" + Trim(Str(OId))
            End If
            Call RetNote.ApplyNotes
            SetCLDetail = True
        End If
        Set RetNote = Nothing
    End If
    Set RetOrder = Nothing
    SetCLDetail = True
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
Private Function GetPatientPrimaryPhoneNumber(ByVal PatientId As Long) As Recordset
Dim strSQL As String
Dim RS As Recordset

strSQL = "DECLARE @employeePhoneType AS INT " & _
" SELECT TOP 1 @employeePhoneType = Id  FROM dbo.PracticeCodeTable pct WHERE pct.Code = 'Employer' AND ReferenceType = 'PhoneTypePatient' " & _
" SELECT @employeePhoneType = ISNULL(@employeePhoneType,0) " & _
" SELECT TOP 1 PatientPhoneNumberTypeId,COALESCE(AreaCode, '') + ExchangeAndSuffix As PhoneNumber,CASE WHEN PatientPhoneNumberTypeId = 7 THEN 'HomePhone' " & _
" WHEN PatientPhoneNumberTypeId = 3 THEN 'CellPhone' " & _
" WHEN PatientPhoneNumberTypeId = 2 THEN 'BusinessPhone' " & _
" WHEN PatientPhoneNumberTypeId = 12 THEN 'EmergencyPhone' " & _
" WHEN PatientPhoneNumberTypeId = 14 THEN 'Fax' " & _
" WHEN PatientPhoneNumberTypeId = 15 THEN 'Beeper' " & _
" WHEN PatientPhoneNumberTypeId = 16 THEN 'Night' " & _
" WHEN PatientPhoneNumberTypeId = 17 THEN 'Unknown' " & _
" WHEN PatientPhoneNumberTypeId = @employeePhoneType THEN 'EmployerPhone' END As PhoneNumberType,* from model.PatientPhoneNumbers " & _
" WHERE PatientId = " & PatientId & " Order by OrdinalId ASC"

Set RS = GetRS(strSQL)
Set GetPatientPrimaryPhoneNumber = RS
End Function
