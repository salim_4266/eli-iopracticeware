VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmTosPos 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtDesc 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   4
      Top             =   6240
      Width           =   5295
   End
   Begin VB.TextBox txtCode 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2760
      MaxLength       =   3
      TabIndex        =   3
      Top             =   6240
      Width           =   855
   End
   Begin VB.ListBox lstStd 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4140
      ItemData        =   "TosPos.frx":0000
      Left            =   6840
      List            =   "TosPos.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   1320
      Width           =   4815
   End
   Begin VB.ListBox lstNew 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4140
      ItemData        =   "TosPos.frx":0004
      Left            =   120
      List            =   "TosPos.frx":0006
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   1320
      Width           =   6495
   End
   Begin VB.TextBox txtTblName 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   3480
      MaxLength       =   16
      TabIndex        =   0
      Top             =   240
      Width           =   4815
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   6
      Top             =   7080
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TosPos.frx":0008
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10200
      TabIndex        =   7
      Top             =   7080
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TosPos.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5280
      TabIndex        =   8
      Top             =   7080
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TosPos.frx":03C6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   990
      Left            =   120
      TabIndex        =   5
      Top             =   5640
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TosPos.frx":05A7
   End
   Begin VB.Label lblSelectCode 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Matching Std Code:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1920
      TabIndex        =   14
      Top             =   5640
      Width           =   8850
   End
   Begin VB.Label lblNew 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "New TOS Codes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   13
      Top             =   960
      Width           =   1860
   End
   Begin VB.Label lblTos 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "TOS Table Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1440
      TabIndex        =   12
      Top             =   360
      Width           =   1890
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3960
      TabIndex        =   11
      Top             =   6240
      Width           =   1290
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1920
      TabIndex        =   10
      Top             =   6240
      Width           =   690
   End
   Begin VB.Label lblStd 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Standard TOS Codes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6840
      TabIndex        =   9
      Top             =   960
      Width           =   2355
   End
End
Attribute VB_Name = "frmTosPos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Public TblType As String
Public TableName As String

Private Sub cmdApply_Click()
Unload frmTosPos
End Sub

Private Sub cmdBack_Click()
Unload frmTosPos
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
If (lstNew.ListIndex >= 0) Then
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplDeleteCode(Trim(TableName), Trim(Left(lstNew.List(lstNew.ListIndex), 30)), True)) Then
        Call LoadDisplay
    End If
    Set ApplTbl = Nothing
Else
    frmEventMsgs.Header = "New Code must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
lstNew.ListIndex = -1
End Sub

Private Sub cmdPost_Click()
Dim Temp As String
Dim ApplTbl As ApplicationTables
If (lstStd.ListIndex < 0) Then
    frmEventMsgs.Header = "Std Code must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    Temp = Trim(Left(lstStd.List(lstStd.ListIndex), 30))
    Set ApplTbl = New ApplicationTables
    Call ApplTbl.ApplPostCode(Trim(txtTblName.Text), Trim(txtCode.Text) + "-" + Trim(txtDesc.Text), Temp, "", True, "", "", "", "", "", "", "", 0)
    If (Trim(TableName) = "") Then
        Call PostTheTableName
        TableName = Trim(txtTblName.Text)
    End If
    lstNew.ListIndex = -1
    lstStd.ListIndex = -1
    lblSelectCode.Caption = "Matching Code:"
    txtCode.Text = ""
    txtDesc.Text = ""
    Set ApplTbl = Nothing
    Call LoadDisplay
End If
End Sub

Public Function LoadDisplay() As Boolean
Dim Temp As String
Dim DisplayText As String
Dim ApplTbl As ApplicationTables
LoadDisplay = False
Set ApplTbl = New ApplicationTables
Temp = "TYPEOFSERVICE"
lblTos.Caption = "New TOS Code"
lblNew.Caption = "New TOS Codes"
lblStd.Caption = "Standard TOS Codes"
If (TblType = "P") Then
    Temp = "PLACEOFSERVICE"
    lblTos.Caption = "New POS Code"
    lblNew.Caption = "New POS Codes"
    lblStd.Caption = "Standard POS Codes"
End If
Set ApplTbl.lstBox = lstStd
Call ApplTbl.ApplLoadCodes(Temp, False, True, True)
txtTblName.Text = Trim(TableName)
If (Trim(TableName) <> "") Then
    Set ApplTbl.lstBox = lstNew
    Call ApplTbl.ApplLoadCodes(Trim(TableName), False, True, True)
    txtTblName.Locked = True
Else
    txtTblName.Locked = False
End If
txtCode.Text = ""
txtDesc.Text = ""
lblSelectCode.Caption = "Matching Std Code:"
cmdDelete.Visible = DeleteOn
LoadDisplay = True
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmTosPos.BorderStyle = 1
    frmTosPos.ClipControls = True
    frmTosPos.Caption = Mid(frmTosPos.Name, 4, Len(frmTosPos.Name) - 3)
    frmTosPos.AutoRedraw = True
    frmTosPos.Refresh
End If
End Sub

Private Sub lstNew_Click()
Dim q As Integer
Dim u As Integer
If (lstNew.ListIndex >= 0) Then
    cmdDelete.Visible = True
    q = InStr(lstNew.List(lstNew.ListIndex), "-")
    If (q <> 0) Then
        txtCode.Text = Trim(Left(lstNew.List(lstNew.ListIndex), q - 1))
        txtDesc.Text = Trim(Mid(lstNew.List(lstNew.ListIndex), q + 1, 32 - q))
        For u = 0 To lstStd.ListCount - 1
            If (UCase(Trim(lstStd.List(u))) = UCase(Trim(Mid(lstNew.List(lstNew.ListIndex), 65, Len(lstNew.List(lstNew.ListIndex)) - 64)))) Then
                lstStd.ListIndex = u
                Exit For
            End If
        Next u
    End If
Else
    cmdDelete.Visible = False
End If
End Sub

Private Sub lstStd_Click()
If (lstStd.ListIndex >= 0) Then
    If (Trim(txtTblName.Text) = "") Then
        frmEventMsgs.Header = "Table Name must be specified"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        lblSelectCode.Caption = "Matching Std Code: " + Trim(lstStd.List(lstStd.ListIndex))
    End If
End If
End Sub

Private Sub txtTblName_KeyPress(KeyAscii As Integer)
Dim Temp As String
Dim ApplTbl As ApplicationTables
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Temp = "TOS"
    If (TblType = "P") Then
        Temp = "POS"
    End If
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplIsCode(Temp, Trim(txtTblName.Text))) Then
        frmEventMsgs.Header = "Table Name Already exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtTblName.Text = ""
        txtTblName.SetFocus
    End If
    Set ApplTbl = Nothing
End If
End Sub

Private Function PostTheTableName() As Boolean
Dim Temp As String
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
Temp = "TOS"
If (TblType = "P") Then
    Temp = "POS"
End If
Call ApplTbl.ApplPostCode(Temp, Trim(txtTblName.Text), "", "", True, "", "", "", "", "", "", "", 0)
Set ApplTbl = Nothing
End Function
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
