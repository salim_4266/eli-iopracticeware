VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmService 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11880
   ClipControls    =   0   'False
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   11880
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox ServiceNDC 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   11
      TabIndex        =   43
      Top             =   2880
      Width           =   2295
   End
   Begin VB.TextBox txtEndDate 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   10
      TabIndex        =   40
      Top             =   3360
      Width           =   1695
   End
   Begin VB.TextBox txtStartDate 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2145
      MaxLength       =   10
      TabIndex        =   39
      Top             =   3360
      Width           =   1695
   End
   Begin VB.TextBox txtNote 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   128
      TabIndex        =   37
      Top             =   2880
      Width           =   2775
   End
   Begin VB.CheckBox chkSrvF 
      BackColor       =   &H0077742D&
      Caption         =   "Allow 0 Amt"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   7440
      TabIndex        =   36
      Top             =   2520
      Width           =   1815
   End
   Begin VB.TextBox txtLoc 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      TabIndex        =   9
      Top             =   4920
      Width           =   3735
   End
   Begin VB.TextBox txtFFee 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   12
      TabIndex        =   15
      Top             =   6480
      Width           =   1695
   End
   Begin VB.TextBox txtPrcFee 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2145
      MaxLength       =   12
      TabIndex        =   2
      Top             =   1440
      Width           =   2295
   End
   Begin VB.TextBox txtDoctor 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2175
      MaxLength       =   128
      TabIndex        =   8
      Top             =   4440
      Width           =   6615
   End
   Begin VB.TextBox txtPlan 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   128
      TabIndex        =   7
      Top             =   3960
      Width           =   6615
   End
   Begin VB.TextBox txtVRU 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   12
      TabIndex        =   3
      Top             =   1440
      Width           =   2055
   End
   Begin VB.TextBox txtPerAllowed 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   3
      TabIndex        =   13
      Top             =   5520
      Width           =   975
   End
   Begin VB.TextBox txtAllowed 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   12
      TabIndex        =   14
      Top             =   6000
      Width           =   1695
   End
   Begin VB.TextBox ServiceCat 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7440
      MaxLength       =   32
      TabIndex        =   5
      Top             =   1920
      Width           =   2640
   End
   Begin VB.ComboBox lstTOS 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      TabIndex        =   6
      Text            =   "lstTOS"
      Top             =   2400
      Width           =   4695
   End
   Begin VB.TextBox txtSDate 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   10
      TabIndex        =   11
      Top             =   6000
      Width           =   1695
   End
   Begin VB.TextBox txtEDate 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   10
      TabIndex        =   12
      Top             =   6480
      Width           =   1695
   End
   Begin VB.TextBox Fee 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   12
      TabIndex        =   10
      Top             =   5520
      Width           =   1695
   End
   Begin VB.TextBox ServiceType 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   1
      TabIndex        =   4
      Top             =   1920
      Width           =   735
   End
   Begin VB.TextBox Description 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2160
      MaxLength       =   64
      TabIndex        =   1
      Top             =   960
      Width           =   6615
   End
   Begin VB.TextBox ServiceCode 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      MaxLength       =   7
      TabIndex        =   0
      Top             =   480
      Width           =   1935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   360
      TabIndex        =   16
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Service.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   9960
      TabIndex        =   18
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Service.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5280
      TabIndex        =   17
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Service.frx":03BE
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   42
      Top             =   3360
      Width           =   1290
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      TabIndex        =   41
      Top             =   3360
      Width           =   1290
   End
   Begin VB.Label lblNote 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Unclassified Service Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4560
      TabIndex        =   38
      Top             =   2880
      Width           =   2760
   End
   Begin VB.Label lblLoc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   375
      TabIndex        =   35
      Top             =   4920
      Width           =   1530
   End
   Begin VB.Label lblFFee 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Facility Fee"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5760
      TabIndex        =   34
      Top             =   6480
      Width           =   1530
   End
   Begin VB.Label lblPrcFee 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Practice Fee"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   375
      TabIndex        =   33
      Top             =   1440
      Width           =   1530
   End
   Begin VB.Label lblDoctor 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   465
      TabIndex        =   32
      Top             =   4440
      Width           =   1440
   End
   Begin VB.Label lblPlan 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   465
      TabIndex        =   31
      Top             =   3960
      Width           =   1440
   End
   Begin VB.Label lblVRU 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "RVU"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5760
      TabIndex        =   30
      Top             =   1440
      Width           =   1530
   End
   Begin VB.Label lblPer 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "% Paid"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5760
      TabIndex        =   29
      Top             =   5520
      Width           =   1530
   End
   Begin VB.Label lblAllowed 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Allowed"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5760
      TabIndex        =   28
      Top             =   6000
      Width           =   1530
   End
   Begin VB.Label lblCat 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Category"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5970
      TabIndex        =   27
      Top             =   1920
      Width           =   1320
   End
   Begin VB.Label lblNDC 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "NDC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   26
      Top             =   2880
      Width           =   1410
   End
   Begin VB.Label lblSrvType 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Type of Service"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   25
      Top             =   2400
      Width           =   1890
   End
   Begin VB.Label lblEDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   615
      TabIndex        =   24
      Top             =   6480
      Width           =   1290
   End
   Begin VB.Label lblSDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   615
      TabIndex        =   23
      Top             =   6000
      Width           =   1290
   End
   Begin VB.Label lblFee 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Schedule Fee"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   135
      TabIndex        =   22
      Top             =   5520
      Width           =   1770
   End
   Begin VB.Label lblDesc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   465
      TabIndex        =   21
      Top             =   960
      Width           =   1440
   End
   Begin VB.Label lblService 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Service"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   585
      TabIndex        =   20
      Top             =   480
      Width           =   1320
   End
   Begin VB.Label lblType 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   585
      TabIndex        =   19
      Top             =   1920
      Width           =   1320
   End
End
Attribute VB_Name = "frmService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Public PreviousPlan As Long
Public TheService As String

Private Sub chkSrvF_Click()
If chkSrvF.Value = 0 Then
    frmEventMsgs.Header = "You are disabling PQRI.  This will result in lower revenues to the practice.  Are you sure?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If frmEventMsgs.Result = 4 Then
         chkSrvF.Value = 1
    End If
End If
End Sub

Private Sub cmdApply_Click()
Dim u As Integer
Dim TheAction As String
Dim Temp As String, ADate As String
Dim AllPlans As Boolean
Dim PostedPracticeOn As Boolean
Dim LocId As Long
Dim PlanId As Long, DrId As Long
Dim LocalDate As ManagedDate
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim RSAllIns As Recordset
Dim VerifyService As Service
Dim RetrieveService As Service
Dim RetFee As PracticeFeeSchedules
Dim ApplTemp As ApplicationTemplates
TheAction = "A"
Set VerifyService = New Service
VerifyService.Service = UCase(Trim(ServiceCode.Text))
VerifyService.ServiceType = ServiceType.Text
If (VerifyService.RetrieveService) Then
    If (TheService = "-") Then
        frmEventMsgs.Header = "Service Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set VerifyService = Nothing
        Exit Sub
    End If
    TheAction = "C"
End If
Set VerifyService = Nothing
If (Trim(txtPrcFee.Text) = "") And (Trim(ServiceType.Text) <> "D") Then
    frmEventMsgs.Header = "Schedule Fee missing"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrcFee.SetFocus
    Exit Sub
End If

If IsDate(txtEndDate.Text) Then
    If IsDate(txtStartDate.Text) Then
        If DateDiff("d", CDate(txtStartDate.Text), CDate(txtEndDate.Text)) < 1 Then
            frmEventMsgs.Header = "End Date must be greater than Start Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
        End If
      End If
End If

PostedPracticeOn = False
Set LocalDate = New ManagedDate
Temp = ServiceCode.Text
Call StripCharacters(Temp, "'")
ServiceCode.Text = Temp
Set RetrieveService = New Service
RetrieveService.Service = UCase(Trim(ServiceCode.Text))
'If (RetrieveService.RetrieveService) Then
RetrieveService.RetrieveService
    RetrieveService.Service = UCase(Trim(ServiceCode.Text))
    RetrieveService.ServiceDescription = Trim(Description.Text)
    RetrieveService.ServiceType = UCase(Trim(ServiceType.Text))
    RetrieveService.ServiceCategory = Trim(ServiceCat.Text)
    RetrieveService.ServiceFee = val(Trim(txtPrcFee.Text))
    RetrieveService.ServiceTOS = Trim(lstTOS.List(lstTOS.ListIndex))
'    RetrieveService.ServicePOS = Trim(lstPOS.List(lstPOS.ListIndex))
    RetrieveService.ServiceNDC = Trim(ServiceNDC.Text)
    RetrieveService.ServiceRVU = val(Trim(txtVRU.Text))
    RetrieveService.ServiceClaimNote = Trim(txtNote.Text)
    RetrieveService.ServiceStartDate = ""
    LocalDate.ExposedDate = txtStartDate.Text
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        RetrieveService.ServiceStartDate = LocalDate.ExposedDate
    End If
    RetrieveService.ServiceEndDate = ""
    LocalDate.ExposedDate = txtEndDate.Text
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        RetrieveService.ServiceEndDate = LocalDate.ExposedDate
    End If
    Set ApplTemp = New ApplicationTemplates
    RetrieveService.ServiceOrderDoc = ApplTemp.ApplIsOrderDocOn(Trim(ServiceCode.Text))
    RetrieveService.ServiceConsultOn = ApplTemp.ApplIsConsultOn(Trim(ServiceCode.Text))
    Set ApplTemp = Nothing
    RetrieveService.ServiceFilter = False
    If (chkSrvF.Value = 1) Then
        RetrieveService.ServiceFilter = True
    End If
    If (RetrieveService.ApplyService) Then
        TheService = RetrieveService.Service
    Else
        frmEventMsgs.Header = "Update Failed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
'End If
Set RetrieveService = Nothing
If (Trim(ServiceType.Text) <> "D") Then
    If (Trim(Fee.Text) <> "") Then
        LocId = -1
        If (Trim(txtLoc.Text) <> "") Then
            LocId = val(Trim(Mid(txtLoc.Text, 57, 5)))
        End If
        PlanId = 0
        AllPlans = False
        If (Trim(txtPlan.Text) <> "") Then
            PlanId = val(Trim(Mid(txtPlan.Text, 57, 5)))
            frmEventMsgs.Header = "Apply Fee to all Insurer's Plans"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Continue"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                AllPlans = True
            End If
        Else
            PlanId = -1
        End If
        If (Trim(txtDoctor.Text) <> "") Then
            DrId = val(Trim(Mid(txtDoctor.Text, 56, 5)))
        Else
            If (PlanId > 0) Then
                DrId = -1
                frmEventMsgs.Header = "Doctor Not Set"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Continue"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result <> 2) Then
                    Exit Sub
                End If
            Else
                If (Trim(txtPerAllowed.Text) <> "") Or (Trim(txtAllowed.Text) <> "") Then
                    frmEventMsgs.Header = "Must have a Doctor or an Insurer"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPlan.SetFocus
                    Exit Sub
                Else
                    Unload frmService
                End If
            End If
        End If
        If (Trim(txtSDate.Text) <> "") Then
            ADate = Mid(txtSDate.Text, 7, 4) + Mid(txtSDate.Text, 1, 2) + Mid(txtSDate.Text, 4, 2)
        End If
        
        If PlanId > 0 Then
'----------------------------------------------------------------------------------------
        'validation
            If (Trim(txtSDate.Text) = "") _
            Or (Trim(txtPerAllowed.Text) = "") _
            Or (Trim(txtAllowed.Text) = "") Then
                frmEventMsgs.Header = "Specify Start Date, % Paid and Allowed"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                txtPlan.SetFocus
                Exit Sub
            End If
        'rec exist
            If (VerifyFee(PlanId, DrId, LocId, ServiceCode.Text, ADate)) Then
                frmEventMsgs.Header = "Fee already assigned, use Fee Schedules to change assigned fees."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                txtPlan.SetFocus
                Exit Sub
            End If
        
            If Not DrId > 0 Then DrId = -1
            
            Call PostFee(PlanId, DrId, LocId, ServiceCode.Text)
            
            If AllPlans Then
                    Set RetPatientFinancialService = New PatientFinancialService
                    RetPatientFinancialService.InsurerId = PlanId
                    Set RS = RetPatientFinancialService.RetrieveInsurer
                    Set RetPatientFinancialService = Nothing
                    If (Not RS Is Nothing And RS.RecordCount > 0) Then
                        Set RetPatientFinancialService = New PatientFinancialService
                        RetPatientFinancialService.InsurerName = Trim(RS("Name"))
                        Set RSAllIns = RetPatientFinancialService.GetInsurerInfoByName(True)
                        If (Not RSAllIns Is Nothing And RSAllIns.RecordCount > 0) Then
                            While (Not RSAllIns.EOF)
                                If Not (VerifyFee(RSAllIns("InsurerId"), DrId, LocId, ServiceCode.Text, ADate)) Then
                                    Call PostFee(RSAllIns("InsurerId"), DrId, LocId, ServiceCode.Text)
                                End If
                                RSAllIns.MoveNext
                            Wend
                        End If
                    End If
                    Set RetPatientFinancialService = Nothing
                    Set RSAllIns = Nothing
            End If
'----------------------------------------------------------------------------------------
        End If
    Else
        If (Trim(txtPlan.Text) <> "") Or (Trim(txtDoctor.Text) <> "") Then
            frmEventMsgs.Header = "Schedule Fee is not Set"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Exit Anyway"
            frmEventMsgs.CancelText = "Remain"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result <> 2) Then
                Exit Sub
            End If
        End If
    End If
End If
If (PlanId > 0) Then
    PreviousPlan = PlanId
End If
Unload frmService
End Sub

Private Sub cmdBack_Click()
Unload frmService
End Sub

Public Function ServiceLoadDisplay(Service As String, TheType As String) As Boolean
On Error GoTo UIError_Label
Dim u As Integer
Dim AUse As Boolean
Dim AEType As String
Dim Temp1 As String, Temp2 As String
Dim Temp3 As String, Temp4 As String
Dim ApplTbl As ApplicationTables
Dim ApplTemp As ApplicationTemplates
Dim RetrieveService As Service
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBoxA = lstTOS
Call ApplTbl.ApplLoadCodesOther("TYPEOFSERVICE", False)
'Set ApplTbl.lstBoxA = lstPOS
Call ApplTbl.ApplLoadCodesOther("PLACEOFSERVICE", False)
TheService = ""
cmdDelete.Visible = DeleteOn
ServiceLoadDisplay = True
Set RetrieveService = New Service
If (Trim(Service) <> "") Then
    If (Service = "-") Then
        RetrieveService.Service = Chr(1)
    Else
        RetrieveService.Service = UCase(Trim(Service))
    End If
    RetrieveService.ServiceType = TheType
    If (RetrieveService.RetrieveService) Then
        ServiceCode.Text = RetrieveService.Service
        If (RetrieveService.Service = Chr(1)) Then
            ServiceCode.Text = ""
        End If
        Description.Text = RetrieveService.ServiceDescription
        ServiceType.Text = RetrieveService.ServiceType
        ServiceCat.Text = RetrieveService.ServiceCategory
        txtPrcFee.Text = Trim(Str(RetrieveService.ServiceFee))
        For u = 0 To lstTOS.ListCount - 1
            If (Trim(RetrieveService.ServiceTOS) = Trim(lstTOS.List(u))) Then
                lstTOS.ListIndex = u
                Exit For
            End If
        Next u
'        For u = 0 To lstPOS.ListCount - 1
'            If (Trim(RetrieveService.ServicePOS) = Trim(lstPOS.List(u))) Then
'                lstPOS.ListIndex = u
'                Exit For
'            End If
'        Next u
        ServiceNDC.Text = Trim(RetrieveService.ServiceNDC)
        txtVRU.Text = Trim(Str(RetrieveService.ServiceRVU))
        txtNote.Text = Trim(RetrieveService.ServiceClaimNote)
        chkSrvF.Value = 0
        If (RetrieveService.ServiceFilter) Then
            chkSrvF.Value = 1
        End If
        chkSrvF.Visible = True
    End If
End If
If (Trim(ServiceType.Text) = "") Then
    ServiceType.Text = TheType
End If
ServiceType.Locked = True
If (Trim(ServiceCode.Text) <> "") Then
    ServiceCode.Locked = True
End If
If (Trim(Fee.Text) = "") Then
    Fee.Text = txtPrcFee.Text
End If
If (PreviousPlan > 0) Then
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetPlanName(PreviousPlan, Temp1, Temp2, Temp3, Temp4, AEType, AUse)
    If (Trim(Temp1) <> "") Then
        Temp2 = Space(56)
        Mid(Temp2, 1, Len(Trim(Temp1))) = Trim(Temp1)
        Temp2 = Temp2 + Trim(Str(PreviousPlan))
        txtPlan.Text = Temp2
    End If
End If
PreviousPlan = 0
TheService = Trim(ServiceCode.Text)
If Len(RetrieveService.ServiceStartDate) = 8 Then
    txtStartDate.Text = Mid(RetrieveService.ServiceStartDate, 5, 2) & "/" & _
                    Mid(RetrieveService.ServiceStartDate, 7, 2) & "/" & _
                    Mid(RetrieveService.ServiceStartDate, 1, 4)
End If
If Len(RetrieveService.ServiceEndDate) = 8 Then
    txtEndDate.Text = Mid(RetrieveService.ServiceEndDate, 5, 2) & "/" & _
                    Mid(RetrieveService.ServiceEndDate, 7, 2) & "/" & _
                    Mid(RetrieveService.ServiceEndDate, 1, 4)
End If
Set RetrieveService = Nothing
If (TheType = "D") Then
    lblService.Caption = "Diagnosis"
    lblPlan.Visible = False
    txtPlan.Visible = False
    lblDoctor.Visible = False
    txtDoctor.Visible = False
    lblFee.Visible = False
    Fee.Visible = False
    lblPer.Visible = False
    txtPerAllowed.Visible = False
    lblAllowed.Visible = False
    txtAllowed.Visible = False
    lblSrvType.Visible = False
    lstTOS.Visible = False
'    lblSrvPlace.Visible = False
'    lstPOS.Visible = False
    lblSDate.Visible = False
    txtSDate.Visible = False
    lblEDate.Visible = False
    txtEDate.Visible = False
    lblFFee.Visible = False
    txtFFee.Visible = False
    chkSrvF.Visible = False
End If
Exit Function
UIError_Label:
    ServiceLoadDisplay = False
    Resume LeaveFast
LeaveFast:
End Function

Private Sub cmdDelete_Click()
Dim Adt As Audit
Dim RetrieveService As New Service
Dim RetSrv As New PatientReceivableService
RetrieveService.Service = UCase(Trim(ServiceCode.Text))
If (RetrieveService.RetrieveService) Then
    If RetSrv.ActivePatientReceivableServices(RetrieveService.Service) Then
        frmEventMsgs.Header = "The service is in use.  Use End Date instead."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        If (RetrieveService.DeleteService) Then
    ' Also delete all items in Fee Schedule Table
            Set Adt = New Audit
            If (IsNumeric(RetrieveService.Service)) Then
                Call Adt.PostAudit("D", Trim$(Str$(UserLogin.iId)), RetrieveService.Service, "PracticeServices")
            Else
                Call Adt.PostAudit("D", Trim$(Str$(UserLogin.iId)), -100, "PracticeServices")
            End If
            Set Adt = Nothing
            TheService = "-"
            Unload frmService
        End If
    End If
End If
Set RetrieveService = Nothing
Set RetSrv = Nothing
End Sub

Private Sub Fee_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Numeric Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmService.BorderStyle = 1
    frmService.ClipControls = True
    frmService.Caption = Mid(frmService.Name, 4, Len(frmService.Name) - 3)
    frmService.AutoRedraw = True
    frmService.Refresh
End If
End Sub

Private Sub lblSrvPlace_Click()

End Sub

'Private Sub lstPOS_Click()
'If (lstPOS.ListIndex >= 0) Then
'    SendKeys "{Home}"
'End If
'End Sub

Private Sub lstTOS_Click()
If (lstTOS.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub ServiceCode_KeyPress(KeyAscii As Integer)
If Not ((IsNumeric(Chr(KeyAscii)) Or IsAlpha(Chr(KeyAscii))) Or (Chr(KeyAscii) = ".")) Then
    frmEventMsgs.Header = "Must be [0-9, A-Z, .]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub



Private Sub txtFFee_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Numeric Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtLoc_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtLoc.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub txtLoc_KeyPress(KeyAscii As Integer)
Call txtLoc_Click
KeyAscii = 0
End Sub

Private Sub txtPrcFee_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Numeric Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtAllowed_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Numeric Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtVRU_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Numeric Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtPerAllowed_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Numeric Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub ServiceCat_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ServiceCategory")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    ServiceCat.Text = Trim(frmSelectDialogue.Selection)
End If
End Sub

Private Sub ServiceCat_KeyPress(KeyAscii As Integer)
Call ServiceCat_Click
KeyAscii = 0
End Sub

Private Sub txtPlan_Click()
Dim ReturnArguments As Object
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim DisplayText As String
Dim Temp As String
Set ReturnArguments = DisplayInsurancePlanSearchScreen
If Not ReturnArguments Is Nothing Then
    If (ReturnArguments.InsurerId > 0) Then
        Set RetPatientFinancialService = New PatientFinancialService
        RetPatientFinancialService.InsurerId = ReturnArguments.InsurerId
        Set RS = RetPatientFinancialService.RetrieveInsurer
        If Not RS Is Nothing And RS.RecordCount > 0 Then
            DisplayText = Space(56)
            Temp = Trim(RS("Name"))
            If (Len(Temp) > 15) Then
                Temp = Left(Temp, 15)
            End If
            If (Trim(RS("PlanName")) <> "") Then
                Temp = Left(RS("Name"), 3) + " " + Left(RS("PlanName"), 11)
            End If
            Mid(DisplayText, 1, Len(Temp)) = Temp
            DisplayText = DisplayText + Trim(Str(ReturnArguments.InsurerId))
            txtPlan.Text = DisplayText
        End If
        Set RetPatientFinancialService = Nothing
    End If
End If
End Sub

Private Sub txtPlan_KeyPress(KeyAscii As Integer)
Call txtPlan_Click
KeyAscii = 0
End Sub

Private Sub txtDoctor_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledDoctor")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtDoctor.Text = frmSelectDialogue.Selection
End If
End Sub

Private Sub txtDoctor_KeyPress(KeyAscii As Integer)
Call txtDoctor_Click
KeyAscii = 0
End Sub
'============================================================================

Private Sub txtStartDate_Validate(Cancel As Boolean)
Call CheckDate(txtStartDate, 13)
End Sub

Private Sub txtStartDate_KeyPress(KeyAscii As Integer)
Call CheckDate(txtStartDate, KeyAscii)
End Sub

Private Sub txtEndDate_Validate(Cancel As Boolean)
Call CheckDate(txtEndDate, 13)
End Sub

Private Sub txtEndDate_KeyPress(KeyAscii As Integer)
Call CheckDate(txtEndDate, KeyAscii)
End Sub












Private Sub txtSDate_Validate(Cancel As Boolean)
'Call txtSDate_KeyPress(13)
Call CheckDate(txtSDate, 13)
End Sub

Private Sub txtSDate_KeyPress(KeyAscii As Integer)
'If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
'    If Not (OkDate(txtSDate.Text)) Then
'        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
'        frmEventMsgs.AcceptText = ""
'        frmEventMsgs.RejectText = "Ok"
'        frmEventMsgs.CancelText = ""
'        frmEventMsgs.Other0Text = ""
'        frmEventMsgs.Other1Text = ""
'        frmEventMsgs.Other2Text = ""
'        frmEventMsgs.Other3Text = ""
'        frmEventMsgs.Other4Text = ""
'        frmEventMsgs.Show 1
'        KeyAscii = 0
'        txtSDate.SetFocus
'        txtSDate.Text = ""
'        SendKeys "{Home}"
'    Else
'        Call UpdateDisplay(txtSDate, "D")
'    End If
'End If
Call CheckDate(txtSDate, KeyAscii)

End Sub

Private Sub txtEDate_Validate(Cancel As Boolean)
'Call txtEDate_KeyPress(13)
Call CheckDate(txtEDate, 13)
End Sub

Private Sub txtEDate_KeyPress(KeyAscii As Integer)
'If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
'    If Not (OkDate(txtEDate.Text)) Then
'        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
'        frmEventMsgs.AcceptText = ""
'        frmEventMsgs.RejectText = "Ok"
'        frmEventMsgs.CancelText = ""
'        frmEventMsgs.Other0Text = ""
'        frmEventMsgs.Other1Text = ""
'        frmEventMsgs.Other2Text = ""
'        frmEventMsgs.Other3Text = ""
'        frmEventMsgs.Other4Text = ""
'        frmEventMsgs.Show 1
'        KeyAscii = 0
'        txtEDate.SetFocus
'        txtEDate.Text = ""
'        SendKeys "{Home}"
'    Else
'        Call UpdateDisplay(txtEDate, "D")
'    End If
'End If
Call CheckDate(txtEDate, KeyAscii)
End Sub

Private Sub CheckDate(txt As TextBox, KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txt.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txt.SetFocus
        txt.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txt, "D")
    End If
End If

End Sub


Private Sub ServiceNDC_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) Then
    If (KeyAscii > 31) And (KeyAscii < 125) Then
        frmEventMsgs.InfoMessage "Valid Values [0123456789]"
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
    If (KeyAscii = 13) Then
        SendKeys "{Tab}"
    End If
End If
End Sub
Private Sub ServiceNDC_Validate(Cancel As Boolean)
    If (Len(ServiceNDC) < 11) Then
        frmEventMsgs.InfoMessage "NDC Must be a 11 digit number"
        frmEventMsgs.Show 1
        ServiceNDC.Text = ""
        Exit Sub
    End If
End Sub
'============================================================================
Private Function PostFee(InsId As Long, DrId As Long, LocId As Long, Srv As String) As Boolean
Dim LocalDate As ManagedDate
Dim RetFee As PracticeFeeSchedules
Set LocalDate = New ManagedDate
Set RetFee = New PracticeFeeSchedules
RetFee.SchServiceId = 0
If (RetFee.RetrieveService) Then
    RetFee.SchService = Trim(Srv)
    RetFee.SchAdjustmentAllowed = val(Trim(txtAllowed.Text))
    RetFee.SchAdjustmentRate = val(Trim(txtPerAllowed.Text))
    RetFee.SchFee = val(Trim(Fee.Text))
    RetFee.SchFacFee = val(Trim(txtFFee.Text))
    RetFee.SchDoctorId = DrId
    RetFee.SchPlanId = InsId
    RetFee.SchLocId = LocId
    RetFee.SchStartDate = ""
    LocalDate.ExposedDate = txtSDate.Text
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        RetFee.SchStartDate = LocalDate.ExposedDate
    End If
    RetFee.SchEndDate = ""
    LocalDate.ExposedDate = txtEDate.Text
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        RetFee.SchEndDate = LocalDate.ExposedDate
    End If
    Call RetFee.ApplyService
End If
Set RetFee = Nothing
Set LocalDate = Nothing
End Function

Private Function VerifyFee(InsId As Long, DrId As Long, LocId As Long, Srv As String, SDate As String) As Boolean
Dim RetFee As PracticeFeeSchedules
VerifyFee = False
If (Trim(Srv) <> "") Then
    Set RetFee = New PracticeFeeSchedules
    RetFee.SchService = Trim(Srv)
    RetFee.SchDoctorId = DrId
    RetFee.SchPlanId = InsId
    RetFee.SchLocId = LocId
    RetFee.SchStartDate = SDate
    If (RetFee.FindServiceDirect > 0) Then
        VerifyFee = True
        If (RetFee.SelectService(1)) Then
            If (RetFee.SchStartDate < SDate) And (Trim(RetFee.SchEndDate) <> "") Then
                VerifyFee = False
            End If
        End If
    End If
    Set RetFee = Nothing
End If
End Function

Private Function VerifyPracticeFee(InsId As Long, Srv As String, SDate As String) As Boolean
Dim RetFee As PracticeFeeSchedules
VerifyPracticeFee = False
If (Trim(Srv) <> "") Then
    Set RetFee = New PracticeFeeSchedules
    RetFee.SchService = Trim(Srv)
    RetFee.SchPlanId = InsId
    RetFee.SchLocId = -1
    RetFee.SchDoctorId = -1
    RetFee.SchStartDate = SDate
    If (RetFee.FindServiceDirect > 0) Then
        VerifyPracticeFee = True
        If (RetFee.SelectService(1)) Then
            If (RetFee.SchStartDate < SDate) And (Trim(RetFee.SchEndDate) <> "") Then
                VerifyPracticeFee = False
            End If
        End If
    End If
    Set RetFee = Nothing
End If
End Function

