VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmCLAdd 
   BackColor       =   &H009B9626&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H009B9626&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox OrdersList 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4350
      Left            =   480
      TabIndex        =   7
      Top             =   2160
      Visible         =   0   'False
      Width           =   11295
   End
   Begin VB.TextBox Quantity 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   3240
      MaxLength       =   6
      TabIndex        =   1
      Top             =   5520
      Width           =   1575
   End
   Begin VB.TextBox SKU 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   3240
      TabIndex        =   0
      Top             =   1560
      Width           =   8535
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdView 
      Height          =   990
      Left            =   6240
      TabIndex        =   12
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLAdd.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAllocate 
      Height          =   990
      Left            =   8160
      TabIndex        =   13
      Top             =   7920
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLAdd.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10080
      TabIndex        =   14
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLAdd.frx":03CA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   360
      TabIndex        =   15
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLAdd.frx":05A9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNew 
      Height          =   990
      Left            =   4320
      TabIndex        =   16
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLAdd.frx":0788
   End
   Begin VB.Label AllocatedNumber 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3240
      TabIndex        =   11
      Top             =   6600
      Visible         =   0   'False
      Width           =   4605
      WordWrap        =   -1  'True
   End
   Begin VB.Label InventoryNumber 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3240
      TabIndex        =   10
      Top             =   7320
      Visible         =   0   'False
      Width           =   4605
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Allocated"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   9
      Top             =   6600
      Visible         =   0   'False
      Width           =   1545
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Inventory"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   8
      Top             =   7320
      Visible         =   0   'False
      Width           =   1545
      WordWrap        =   -1  'True
   End
   Begin VB.Label Description 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3225
      TabIndex        =   6
      Top             =   2640
      Width           =   8565
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Quantity"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   5
      Top             =   5520
      Width           =   1605
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   4
      Top             =   2640
      Width           =   1965
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      BackStyle       =   0  'Transparent
      Caption         =   "Item #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   3
      Top             =   1560
      Width           =   1185
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Add Or Remove an Item"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4080
      TabIndex        =   2
      Top             =   240
      Width           =   4095
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmCLAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OrderType As String
Private InventoryId As Long
Private RetrieveCLInventory As CLInventory
Private RetrievePCInventory As PCInventory
Private RetrieveCLOrders As CLOrders

Private Sub cmdHome_Click()
If (OrderType = "C") Then
    Set RetrieveCLInventory = Nothing
Else
    Set RetrievePCInventory = Nothing
End If
Set RetrieveCLOrders = Nothing
Unload frmCLAdd
End Sub

Private Sub cmdNew_Click()
If (OrderType = "C") Then
    frmCLNew.SKU = ""
    If (InventoryId < 1) Then
        frmCLNew.SKU = SKU.Text
    End If
    frmCLNew.OrderType = "C"
    frmCLNew.InventoryId = InventoryId
    frmCLNew.Show 1
    Call SKU_KeyPress(13)
Else
    frmPCNew.OrderType = "G"
    frmPCNew.InventoryId = InventoryId
    frmPCNew.SKU = SKU.Text
    frmPCNew.Show 1
    Call SKU_KeyPress(13)
End If
End Sub

Private Sub cmdDone_Click()
Dim i As Integer
If (Quantity.Text = "") Then
    If (OrderType = "C") Then
        Set RetrieveCLInventory = Nothing
    Else
        Set RetrievePCInventory = Nothing
    End If
    Set RetrieveCLOrders = Nothing
    Unload frmCLAdd
    Exit Sub
End If
If (Validate) Then
    Label1.Visible = False
    Label2.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    SKU.Visible = False
    Quantity.Visible = False
    Description.Top = 120
    Description.Left = 480
    OrdersList.Visible = True
    If (OrderType = "C") Then
        RetrieveCLInventory.Qty = RetrieveCLInventory.Qty + val(Quantity.Text)
        RetrieveCLInventory.PutCLInventory
    Else
        RetrievePCInventory.Qty = RetrievePCInventory.Qty + val(Quantity.Text)
        RetrievePCInventory.PutPCInventory
    End If
    Quantity.Text = ""
    i = InStrPS(Description.Caption, "Qty:")
    If (i > 0) Then
        If (OrderType = "C") Then
            Description.Caption = Left(Description.Caption, i - 1) + "Qty:" + Trim(Str(RetrieveCLInventory.Qty))
        Else
            Description.Caption = Left(Description.Caption, i - 1) + "Qty:" + Trim(Str(RetrievePCInventory.Qty))
        End If
    Else
        If (OrderType = "C") Then
            Description.Caption = Description.Caption + Chr(10) + Chr(13) + "Qty:" + Trim(Str(RetrieveCLInventory.Qty))
        Else
            Description.Caption = Description.Caption + Chr(10) + Chr(13) + "Qty:" + Trim(Str(RetrievePCInventory.Qty))
        End If
    End If
    If Not (LoadOrdersList) Then
        OrdersList.Visible = False
        cmdView.Text = "Add Items"
        Call cmdView_Click
    Else
        cmdView.Text = "Add Items"
        Call cmdView_Click
    End If
End If
End Sub

Private Sub cmdAllocate_Click()
If (Trim(InventoryNumber.Caption) <> "") Then
    If (Trim(InventoryNumber.Caption) >= 0) Then
        RetrieveCLOrders.Status = 4
        RetrieveCLOrders.PutCLOrder
        If (OrderType = "C") Then
            RetrieveCLInventory.Qty = RetrieveCLInventory.Qty - val(Trim(AllocatedNumber.Caption))
            RetrieveCLInventory.PutCLInventory
        Else
            RetrievePCInventory.Qty = RetrievePCInventory.Qty - val(Trim(AllocatedNumber.Caption))
            RetrievePCInventory.PutPCInventory
        End If
        AllocatedNumber.Visible = False
        InventoryNumber.Visible = False
        Label5.Visible = False
        Label6.Visible = False
        Call cmdView_Click
    Else
        frmEventMsgs.Header = "Inventory quantity is insufficient to fill quantity allocated to order."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    frmEventMsgs.Header = "Select SKU then View Orders"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdView_Click()
If (cmdView.Text = "View  Orders") Then
    AllocatedNumber.Visible = False
    InventoryNumber.Visible = False
    Label5.Visible = False
    Label6.Visible = False
    If (LoadOrdersList) Then
        cmdView.Text = "Add Items"
    Else
        frmEventMsgs.Header = "No Orders Pending"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    AllocatedNumber.Visible = False
    InventoryNumber.Visible = False
    Label5.Visible = False
    Label6.Visible = False
    Label1.Visible = True
    Label2.Visible = True
    Label3.Visible = True
    Label4.Visible = True
    SKU.Visible = True
    Quantity.Visible = True
    Description.Top = 2640
    Description.Left = 3225
    OrdersList.Visible = False
    cmdView.Text = "View  Orders"
    frmEventMsgs.Header = "Another or Done"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Another"
    frmEventMsgs.CancelText = "Done"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        Quantity.Text = ""
        SKU.Text = ""
        Description.Caption = ""
        SKU.SetFocus
    End If
End If
End Sub

Private Function LoadOrdersList() As Boolean
Dim i As Integer
Dim PatName As String
Dim Temp As String
Dim ATemp As String
Dim ADate As String
Dim ACost As String
Dim RetrievePatient As Patient
LoadOrdersList = False
If (Trim(SKU.Text) <> "") Then
    ATemp = ""
    If (OrderType = "C") Then
        Set RetrieveCLInventory = Nothing
        Set RetrieveCLInventory = New CLInventory
        RetrieveCLInventory.FieldSelect = "SKU"
        RetrieveCLInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "' "
        RetrieveCLInventory.OrderBy = "SKU"
        If (RetrieveCLInventory.FindCLInventory) Then
            ATemp = Trim(Str(RetrieveCLInventory.InventoryId))
        End If
    Else
        Set RetrievePCInventory = Nothing
        Set RetrievePCInventory = New PCInventory
        RetrievePCInventory.FieldSelect = "SKU"
        RetrievePCInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "' "
        RetrievePCInventory.OrderBy = "SKU"
        If (RetrievePCInventory.FindPCInventory) Then
            ATemp = Trim(Str(RetrievePCInventory.InventoryId))
        End If
    End If
    If (Trim(ATemp) <> "") Then
        Set RetrieveCLOrders = Nothing
        Set RetrieveCLOrders = New CLOrders
        If (OrderType = "C") Then
            RetrieveCLOrders.Criteria = "InventoryId = " + Trim(ATemp) + " AND (Status = 3 Or Status = 1)"
            RetrieveCLOrders.OrderBy = "InventoryId"
            RetrieveCLOrders.FindCLOrders
        Else
            RetrieveCLOrders.Criteria = "InventoryId = " + Trim(ATemp) + " AND (Status = 3 Or Status = 1)"
            RetrieveCLOrders.OrderBy = "InventoryId"
            RetrieveCLOrders.FindPCOrders
        End If
        i = 1
        OrdersList.Clear
        While (RetrieveCLOrders.SelectCLOrders(i))
            Set RetrievePatient = New Patient
            RetrievePatient.PatNumber = RetrieveCLOrders.PatientId
            If (RetrievePatient.FindPatientDirect) Then
                Call RetrievePatient.SelectPatient(1)
                Temp = Space(80)
                PatName = Trim(RetrievePatient.FirstName) + " " _
                        + Trim(RetrievePatient.MiddleInitial) + " " _
                        + Trim(RetrievePatient.LastName) + " " _
                        + Trim(RetrievePatient.NameRef)
                ADate = RetrieveCLOrders.OrderDate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                If (RetrieveCLOrders.Cost > 0) Then
                    Call DisplayDollarAmount(Trim(Str(RetrieveCLOrders.Cost)), ACost)
                    ACost = "$ " + Trim(ACost)
                Else
                    ACost = "$ 0.00"
                End If
                Mid(Temp, 1, Len(PatName)) = PatName
                Mid(Temp, 20, 10) = ADate
                Mid(Temp, 31, 2) = RetrieveCLOrders.Eye
                Mid(Temp, 40, Len(RetrievePatient.HomePhone)) = RetrievePatient.HomePhone
                Mid(Temp, 60, Len(Trim(Str(RetrieveCLOrders.Qty)))) = Trim(Str(RetrieveCLOrders.Qty))
                Mid(Temp, 67, Len(ACost)) = ACost
                Temp = Temp + Trim(Str(RetrieveCLOrders.OrderId))
                OrdersList.AddItem Temp
            End If
            i = i + 1
            Set RetrievePatient = Nothing
        Wend
        If (OrdersList.ListCount > 0) Then
            OrdersList.Visible = True
            LoadOrdersList = True
        End If
    End If
End If
End Function

Private Sub OrdersList_Click()
Dim Temp As Long
If (OrdersList.ListIndex >= 0) Then
    Set RetrieveCLOrders = Nothing
    Set RetrieveCLOrders = New CLOrders
    Temp = val(Trim(Mid(OrdersList.List(OrdersList.ListIndex), 81, Len(OrdersList.List(OrdersList.ListIndex)) - 80)))
    RetrieveCLOrders.Criteria = "OrderId = " + Trim(Str(Temp)) + " "
    RetrieveCLOrders.OrderBy = "OrderId"
    If (OrderType = "C") Then
        RetrieveCLOrders.FindCLOrders
    Else
        RetrieveCLOrders.FindPCOrders
    End If
    Call RetrieveCLOrders.SelectCLOrders(1)
    AllocatedNumber.Visible = True
    InventoryNumber.Visible = True
    Label5.Visible = True
    Label6.Visible = True
    cmdAllocate.Visible = True
    cmdView.Visible = True
    AllocatedNumber.Caption = RetrieveCLOrders.Qty
    If (OrderType = "G") Then
        InventoryNumber.Caption = Trim(Str(RetrievePCInventory.Qty - val(Trim(AllocatedNumber.Caption))))
    Else
        InventoryNumber.Caption = Trim(Str(RetrieveCLInventory.Qty - val(Trim(AllocatedNumber.Caption))))
    End If
End If
End Sub

Private Function Validate() As Boolean
Validate = True
If (Quantity.Text = "" Or Not IsNumeric(Quantity.Text) Or InStrPS(1, Quantity.Text, ".") <> 0) Then
    Validate = False
    frmEventMsgs.Header = "Invalid Quantity."
ElseIf (Description.Caption = "") Then
    Validate = False
    frmEventMsgs.Header = "No Description."
End If
If Not (Validate) Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Function

Private Sub Quantity_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call cmdDone_Click
End If
End Sub

Private Sub SKU_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    InventoryId = 0
    Description.Caption = ""
    If (OrderType = "C") Then
        Set RetrieveCLInventory = Nothing
        Set RetrieveCLInventory = New CLInventory
        RetrieveCLInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "'"
        RetrieveCLInventory.FindCLInventory
        If (RetrieveCLInventory.CLInventoryTotal > 0) Then
            Call RetrieveCLInventory.SelectCLInventory(1)
            InventoryId = RetrieveCLInventory.InventoryId
            Description.Caption = RetrieveCLInventory.Manufacturer + ", " + RetrieveCLInventory.Series + ", " + RetrieveCLInventory.Type_ + ", " + RetrieveCLInventory.Material _
                                + Chr(10) + Chr(13) + RetrieveCLInventory.Disposable + ", " + RetrieveCLInventory.WearTime + ", " + RetrieveCLInventory.Tint _
                                + Chr(10) + Chr(13) + " SP:" + RetrieveCLInventory.SpherePower + " CY:" + RetrieveCLInventory.CylinderPower + " AX:" + RetrieveCLInventory.Axis _
                                + " AR:" + RetrieveCLInventory.AddReading + " DI:" + RetrieveCLInventory.Diameter + " BC:" + RetrieveCLInventory.BaseCurve + " PC:" + RetrieveCLInventory.PeriphCurve _
                                + Chr(10) + Chr(13) + "Qty:" + Trim(Str(RetrieveCLInventory.Qty))
            Quantity.Text = "1"
            Quantity.SetFocus
        Else
            SKU.Text = ""
            SKU.SetFocus
        End If
    Else
        If (Trim(SKU.Text) <> "") Then
            Set RetrievePCInventory = Nothing
            Set RetrievePCInventory = New PCInventory
            RetrievePCInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "'"
            RetrievePCInventory.FindPCInventory
            If (RetrievePCInventory.PCInventoryTotal > 0) Then
                Call RetrievePCInventory.SelectPCInventory(1)
                InventoryId = RetrievePCInventory.InventoryId
                Description.Caption = Trim(RetrievePCInventory.Manufacturer) + ", " + Trim(RetrievePCInventory.Model) _
                                    + Chr(10) + Chr(13) + Trim(RetrievePCInventory.Type_) + ", " + RetrievePCInventory.PCColor + ", " _
                                    + Chr(10) + Chr(13) + Trim(RetrievePCInventory.LensTint) _
                                    + Chr(10) + Chr(13) + "Qty:" + Trim(Str(RetrievePCInventory.Qty))
                Quantity.Text = "1"
                Quantity.SetFocus
            Else
                SKU.Text = ""
                SKU.SetFocus
            End If
        End If
    End If
End If
End Sub

Private Sub SKU_Validate(Cancel As Boolean)
Dim Temp As String
Temp = SKU.Text
Call SKU_KeyPress(13)
SKU.Text = Temp
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
