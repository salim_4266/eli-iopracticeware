VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmApptInfo 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "ApptInfo"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form3"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstPayment 
      Height          =   915
      Left            =   840
      TabIndex        =   15
      Top             =   2040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   402
      TabIndex        =   4
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptInfo.frx":0000
   End
   Begin VB.ListBox lstReferral 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1230
      ItemData        =   "ApptInfo.frx":01DF
      Left            =   2944
      List            =   "ApptInfo.frx":01E1
      TabIndex        =   6
      Top             =   6120
      Width           =   8650
   End
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1380
      ItemData        =   "ApptInfo.frx":01E3
      Left            =   2944
      List            =   "ApptInfo.frx":01E5
      TabIndex        =   13
      Top             =   3630
      Width           =   8655
   End
   Begin VB.TextBox txtNotes 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      ForeColor       =   &H00FFFFFF&
      Height          =   2175
      Left            =   3000
      MaxLength       =   255
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   1155
      Width           =   8650
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDemo 
      Height          =   990
      Left            =   9720
      TabIndex        =   0
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptInfo.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFinancial 
      Height          =   990
      Left            =   5160
      TabIndex        =   3
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptInfo.frx":03D2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSchedule 
      Height          =   990
      Left            =   7440
      TabIndex        =   1
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptInfo.frx":05BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClinical 
      Height          =   990
      Left            =   2760
      TabIndex        =   2
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptInfo.frx":07AC
   End
   Begin VB.Label lblPatientName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   2944
      TabIndex        =   8
      Top             =   480
      Width           =   4275
   End
   Begin VB.Label lblFinancial 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   2944
      TabIndex        =   7
      Top             =   5400
      Width           =   4275
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Referral Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   403
      TabIndex        =   14
      Top             =   6120
      Width           =   2295
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Financial Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   403
      TabIndex        =   12
      Top             =   5400
      Width           =   2295
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   402
      TabIndex        =   11
      Top             =   3630
      Width           =   2295
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Notes"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   402
      TabIndex        =   10
      Top             =   1155
      Width           =   2295
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   402
      TabIndex        =   9
      Top             =   465
      Width           =   2295
   End
End
Attribute VB_Name = "frmApptInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public CurrentAction As String
Private OrgNotes As String
Private CurrentReferral As Long
Private RetrievePatient As Patient

Private Sub cmdClinical_Click()
frmReviewAppts.StartDate = ""
frmReviewAppts.PatientId = PatientId
If (frmReviewAppts.LoadApptsList) Then
    frmReviewAppts.Show 1
Else
    frmEventMsgs.Header = "No Appointments"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.OtherText = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdDemo_Click()
Call PostNotes
Set RetrievePatient = Nothing
Unload frmApptInfo
End Sub

Private Sub PostNotes()
Set RetrievePatient = Nothing
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PatientId
If (RetrievePatient.RetrievePatient) Then
    RetrievePatient.ProfileComment1 = Trim(txtNotes.Text)
    If Not (RetrievePatient.ApplyPatient) Then
        Call PinpointError("frmApptInfo", "Notes not updated for" + Str(PatientId))
    End If
End If
End Sub

Private Sub LoadAllAppointments(ThePatientId As Long)
Dim k As Integer
Dim PatientName As String
Dim ApptDate As String, ApptTime As String
Dim LocalPrevDate As ManagedDate
Dim RetRef As PatientReferral
Dim RetrievePrevAppt As SchedulerAppointment
Dim RetrievePrevApptType As SchedulerAppointmentType
Set LocalPrevDate = New ManagedDate
Set RetRef = New PatientReferral
Set RetrievePrevAppt = New SchedulerAppointment
Set RetrievePrevApptType = New SchedulerAppointmentType
lstAppts.Clear
RetrievePrevAppt.AppointmentStatus = ""
RetrievePrevAppt.AppointmentPatientId = ThePatientId
If (RetrievePrevAppt.FindAppointment) Then
    k = 1
    While (RetrievePrevAppt.SelectAppointment(k))
        If (Trim(RetrievePrevAppt.AppointmentComments) <> "ADD VIA BILLING") Then
            PatientName = Space(70)
            LocalPrevDate.ExposedDate = RetrievePrevAppt.AppointmentDate
            If (LocalPrevDate.ConvertManagedDateToDisplayDate) Then
                ApptDate = LocalPrevDate.ExposedDate
            End If
            Call ConvertMinutesToTime(RetrievePrevAppt.AppointmentTime, ApptTime)
            Mid(PatientName, 1, 1) = RetrievePrevAppt.AppointmentStatus
            Mid(PatientName, 3, 19) = ApptDate + " " + ApptTime
            RetrievePrevApptType.AppointmentTypeId = RetrievePrevAppt.AppointmentTypeId
            If (RetrievePrevApptType.RetrieveSchedulerAppointmentType) Then
                Mid(PatientName, 24, 16) = Trim(RetrievePrevApptType.AppointmentType)
            End If
            If (RetrievePrevAppt.AppointmentReferralId > 0) Then
                RetRef.PatientId = PatientId
                RetRef.ReferralId = RetrievePrevAppt.AppointmentReferralId
                If (RetRef.RetrievePatientReferral) Then
                    Mid(PatientName, 40, 20) = RetRef.Referral
                End If
            End If
            lstAppts.AddItem PatientName
        End If
        k = k + 1
    Wend
End If
Set LocalPrevDate = Nothing
Set RetRef = Nothing
Set RetrievePrevAppt = Nothing
Set RetrievePrevApptType = Nothing
End Sub

Public Function LoadAppointment() As Boolean
On Error GoTo UI_ErrorHandler
Dim PatientName As String
Dim BalText As String
Dim BalanceDue As Single
LoadAppointment = False
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PatientId
If (RetrievePatient.RetrievePatient) Then
    PatientName = RetrievePatient.FirstName + " " _
                + RetrievePatient.MiddleInitial + " " _
                + RetrievePatient.LastName + " " _
                + RetrievePatient.NameRef
    OrgNotes = Trim(RetrievePatient.ProfileComment1)
    txtNotes.Text = Trim(RetrievePatient.ProfileComment1)
    lblPatientName.Caption = Trim(PatientName)
    Call frmPayment.LoadPayerInvoices(PatientId, lstPayment)
    BalanceDue = frmPayment.PatientBalance
    Call DisplayDollarAmount(Trim(Str(BalanceDue)), BalText)
    lblFinancial.Caption = "Current Patient Balance is $" + BalText
    Call LoadAllAppointments(PatientId)
    lstReferral.ListIndex = LoadReferrals(PatientId)
    LoadAppointment = True
End If
Exit Function
UI_ErrorHandler:
    Set RetrievePatient = Nothing
    LoadAppointment = False
    Call PinpointError("frmApptInfo", Error(Err))
    Resume LeaveFast
LeaveFast:
End Function

Private Sub cmdHome_Click()
CurrentAction = "Home"
Set RetrievePatient = Nothing
Unload frmApptInfo
End Sub

Private Sub cmdSchedule_Click()
Dim TheReferralId As Long
Call PostNotes
If (lstReferral.ListIndex > -1) Then
    TheReferralId = Val(Mid(lstReferral.List(lstReferral.ListIndex), 86, 5))
Else
    TheReferralId = 0
End If
frmScheduler.PatientId = PatientId
frmScheduler.ReferralId = TheReferralId
If (frmScheduler.LoadScheduler) Then
    frmScheduler.CurrentAction = ""
    frmScheduler.Show 1
    CurrentAction = frmScheduler.CurrentAction
    If (frmScheduler.CurrentAction = "Home") Then
        Call cmdHome_Click
    ElseIf (frmScheduler.CurrentAction = "HomeNoChanges") Then
        Set RetrievePatient = Nothing
        Unload frmApptInfo
    Else
        Call LoadAppointment
    End If
End If
End Sub

Private Sub cmdFinancial_Click()
frmFinancials.PatientId = PatientId
frmFinancials.ReferralId = 0
If (frmFinancials.LoadFinancialDisplay) Then
    frmFinancials.CurrentAction = ""
    frmFinancials.Show 1
    If (Left(frmFinancials.CurrentAction, 4) = "Home") Then
        CurrentAction = frmFinancials.CurrentAction
        Set RetrievePatient = Nothing
        Unload frmApptInfo
    End If
Else
    frmEventMsgs.Header = "No Financials"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.OtherText = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Function LoadReferrals(PatientId As Long) As Integer
Dim k As Integer
Dim TheDate As String
Dim Iref As Integer
Dim TheText As String
Dim DisplayText As String
Dim Referral As PatientReferral
Dim LocalDate As ManagedDate
Iref = -1
lstReferral.Clear
lstReferral.List(0) = "Record Referral"
If (PatientId < 1) Then
    Exit Function
End If
Set Referral = New PatientReferral
Set LocalDate = New ManagedDate
Call FormatTodaysDate(TheDate, False)
LocalDate.ExposedDate = TheDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    TheDate = LocalDate.ExposedDate
End If
Referral.PatientId = PatientId
Referral.ReferralDate = ""
Referral.ReferralExpireDate = ""
If (Referral.FindPatientReferral > 0) Then
    k = 1
    While (Referral.SelectPatientReferral(k))
        DisplayText = Space(85)
        Mid(DisplayText, 1, Len(Referral.Referral)) = Trim(Referral.Referral)
        Mid(DisplayText, 18, 2) = Trim(Str(Referral.ReferralVisitsLeft))
        If (Referral.ReferralToId > 0) Then
            Call getReferralTo(Referral.ReferralToId, TheText)
            Mid(DisplayText, 21, 10) = Left(TheText, 10)
        End If
        If (Referral.ReferralFromId > 0) Then
            Call getReferralFrom(Referral.ReferralFromId, TheText)
            Mid(DisplayText, 32, 15) = Left(TheText, 15)
        End If
        If (Referral.ReferralPlan > 0) Then
            If (getInsurer(Referral.ReferralPlan, TheText)) Then
                Mid(DisplayText, 48, 20) = Left(TheText, 20)
            End If
        End If
        If (Referral.ReferralExpireDate < TheDate) Then
            Mid(DisplayText, 70, 1) = "X"
        End If
        DisplayText = DisplayText + Trim(Str(Referral.ReferralId))
        lstReferral.AddItem DisplayText
        k = k + 1
    Wend
End If
Set Referral = Nothing
Set LocalDate = Nothing
LoadReferrals = -1
End Function

Private Sub lstReferral_Click()
Dim TheReferralId As Long
If (lstReferral.ListIndex = 0) Then
    TheReferralId = 0
Else
    TheReferralId = Val(Trim(Mid(lstReferral.List(lstReferral.ListIndex), 86, 5)))
End If
If (TheReferralId = 0) Then
    If (frmPatientReferral.ReferralLoadDisplay(PatientId, TheReferralId)) Then
        frmPatientReferral.CurrentAction = ""
        frmPatientReferral.Show 1
        If (Left(frmPatientReferral.CurrentAction, 4) = "Home") Then
            CurrentAction = frmPatientReferral.CurrentAction
            Call cmdHome_Click
            Exit Sub
        ElseIf (frmPatientReferral.TheReferralId > 0) Then
            lstReferral.ListIndex = LoadReferrals(PatientId)
        End If
    End If
End If
End Sub

Private Sub lstReferral_DblClick()
Dim TheReferralId As Long
If (lstReferral.ListIndex = 0) Then
    TheReferralId = 0
Else
    TheReferralId = Val(Trim(Mid(lstReferral.List(lstReferral.ListIndex), 86, 5)))
End If
If (frmPatientReferral.ReferralLoadDisplay(PatientId, TheReferralId)) Then
    frmPatientReferral.CurrentAction = ""
    frmPatientReferral.Show 1
    If (Left(frmPatientReferral.CurrentAction, 4) = "Home") Then
        CurrentAction = frmPatientReferral.CurrentAction
        Call cmdHome_Click
        Exit Sub
    ElseIf (frmPatientReferral.TheReferralId > 0) Then
        lstReferral.ListIndex = LoadReferrals(PatientId)
    End If
End If
End Sub

Private Function getReferralFrom(PCPId As Long, PCPName As String) As Boolean
Dim TheDocs As PracticeVendors
getReferralFrom = False
PCPName = ""
Set TheDocs = New PracticeVendors
TheDocs.VendorId = PCPId
If (TheDocs.RetrieveVendor) Then
    PCPName = Space(56)
    Mid(PCPName, 1, Len(TheDocs.VendorName)) = TheDocs.VendorName
    PCPName = PCPName + Trim(Str(TheDocs.VendorId))
    getReferralFrom = True
End If
Set TheDocs = Nothing
End Function

Private Function getReferralTo(PCPId As Long, PCPName As String) As Boolean
Dim TheDocs As SchedulerResource
getReferralTo = False
PCPName = ""
Set TheDocs = New SchedulerResource
TheDocs.ResourceId = PCPId
If (TheDocs.RetrieveSchedulerResource) Then
    PCPName = Space(56)
    Mid(PCPName, 1, Len(TheDocs.ResourceName)) = TheDocs.ResourceName
    PCPName = PCPName + Trim(Str(TheDocs.ResourceId))
    getReferralTo = True
End If
Set TheDocs = Nothing
End Function

Private Function getInsurer(InsId As Long, RetString) As Boolean
Dim DisplayIt As String
Dim RetrieveInsurer As Insurer
getInsurer = False
RetString = ""
If (InsId < 1) Then
    Set RetrieveInsurer = New Insurer
    RetrieveInsurer.InsurerId = InsId
    If (RetrieveInsurer.RetrieveInsurer) Then
        getInsurer = True
        DisplayIt = Trim(RetrieveInsurer.InsurerName) + " " _
                  + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                  + Trim(RetrieveInsurer.InsurerGroupName)
        RetString = DisplayIt
    End If
    Set RetrieveInsurer = Nothing
End If
End Function
