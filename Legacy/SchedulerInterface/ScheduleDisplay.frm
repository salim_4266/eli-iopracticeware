VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "SSCALB32.OCX"
Begin VB.Form frmScheduleDisplay 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form5"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form5"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   990
      Left            =   4680
      TabIndex        =   5
      Top             =   6840
      Width           =   1365
      _Version        =   131072
      _ExtentX        =   2408
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   0
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDisplay.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   990
      Left            =   2760
      TabIndex        =   12
      Top             =   6840
      Width           =   1365
      _Version        =   131072
      _ExtentX        =   2408
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   0
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDisplay.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDoctor 
      Height          =   990
      Left            =   240
      TabIndex        =   10
      Top             =   7800
      Width           =   2100
      _Version        =   131072
      _ExtentX        =   3704
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDisplay.frx":03C2
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   3015
      Left            =   8828
      TabIndex        =   9
      Top             =   360
      Width           =   3015
      _Version        =   65537
      _ExtentX        =   5318
      _ExtentY        =   5318
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   14285823
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
   End
   Begin SSCalendarWidgets_B.SSDay SSDay2 
      Height          =   5385
      Left            =   3045
      TabIndex        =   3
      Top             =   360
      Visible         =   0   'False
      Width           =   2850
      _Version        =   65537
      _ExtentX        =   5027
      _ExtentY        =   9499
      _StockProps     =   79
      Caption         =   "SSDay2"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty CaptionFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   8388608
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      DurationFillColor=   16777152
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
   End
   Begin VB.ListBox lstStaff 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4830
      ItemData        =   "ScheduleDisplay.frx":05B1
      Left            =   8828
      List            =   "ScheduleDisplay.frx":05B3
      TabIndex        =   0
      Top             =   3960
      Width           =   3015
   End
   Begin SSCalendarWidgets_B.SSDay SSDay3 
      Height          =   5385
      Left            =   5925
      TabIndex        =   4
      Top             =   360
      Visible         =   0   'False
      Width           =   2850
      _Version        =   65537
      _ExtentX        =   5027
      _ExtentY        =   9499
      _StockProps     =   79
      Caption         =   "SSDay3"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty CaptionFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   8388608
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      DurationFillColor=   16777152
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   6480
      TabIndex        =   11
      Top             =   7800
      Width           =   2100
      _Version        =   131072
      _ExtentX        =   3704
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDisplay.frx":05B5
   End
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   5385
      Left            =   165
      TabIndex        =   13
      Top             =   360
      Visible         =   0   'False
      Width           =   2850
      _Version        =   65537
      _ExtentX        =   5027
      _ExtentY        =   9499
      _StockProps     =   79
      Caption         =   "SSDay1"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty CaptionFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   8388608
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
   End
   Begin VB.Label lblDetails 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Appointment Details"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   360
      TabIndex        =   14
      Top             =   6360
      Visible         =   0   'False
      Width           =   1965
   End
   Begin VB.Label Behind3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Minutes Behind:"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6100
      TabIndex        =   8
      Top             =   5880
      Visible         =   0   'False
      Width           =   2505
   End
   Begin VB.Label Behind2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Minutes Behind:"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   3240
      TabIndex        =   7
      Top             =   5880
      Visible         =   0   'False
      Width           =   2505
   End
   Begin VB.Label Behind1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Minutes Behind:"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   340
      TabIndex        =   6
      Top             =   5880
      Visible         =   0   'False
      Width           =   2505
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   11198
      TabIndex        =   2
      Top             =   3550
      Width           =   645
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Person"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   8828
      TabIndex        =   1
      Top             =   3550
      Width           =   1170
   End
End
Attribute VB_Name = "frmScheduleDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurrentDate As String
Private CurrentListNumber As Integer
Private TotalRooms As Integer
Private TotalDoctors As Integer

Private Sub cmdDoctor_Click()
Dim ResourceId As Long
Dim TheDate As String
If (lstStaff.ListIndex < 0) Then
    frmEventMsgs.Header = "Please Select a Doctor"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.OtherText = ""
    frmEventMsgs.Show 1
Else
    frmScheduleDoctor.InitialApptDate = CurrentDate
    ResourceId = Val(Trim(Mid(lstStaff.List(lstStaff.ListIndex), 57, 5)))
    If (frmScheduleDoctor.LoadDoctorSchedule(ResourceId)) Then
        frmScheduleDoctor.Show 1
    Else
        frmEventMsgs.Header = "No Appointments"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.OtherText = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdExit_Click()
Unload frmScheduleDisplay
End Sub

Private Sub cmdNext_Click()
Dim ResourceId As Long
CurrentListNumber = (Int(CurrentListNumber / 3)) * 3
If (CurrentListNumber < 1) Or (CurrentListNumber >= TotalRooms) Then
    CurrentListNumber = 0
End If
If (CurrentListNumber + 1 <= TotalRooms) Then
    CurrentListNumber = CurrentListNumber + 1
    ResourceId = GetRoomResource(CurrentListNumber)
    If (ResourceId > 0) Then
        Call LoadRoomSchedule(ResourceId, 1)
        Behind1.Caption = "Minutes Behind:" + Str(GetMinutesBehind(1))
        Behind1.Visible = True
    Else
        SSDay1.Visible = False
        Behind1.Visible = False
    End If
Else
    SSDay1.Visible = False
    Behind1.Visible = False
End If
If (CurrentListNumber + 1 <= TotalRooms) Then
    CurrentListNumber = CurrentListNumber + 1
    ResourceId = GetRoomResource(CurrentListNumber)
    If (ResourceId > 0) Then
        Call LoadRoomSchedule(ResourceId, 2)
        Behind2.Caption = "Minutes Behind:" + Str(GetMinutesBehind(2))
        Behind2.Visible = True
    Else
        SSDay2.Visible = False
        Behind2.Visible = False
    End If
Else
    SSDay2.Visible = False
    Behind2.Visible = False
End If
If (CurrentListNumber + 1 <= TotalRooms) Then
    CurrentListNumber = CurrentListNumber + 1
    ResourceId = GetRoomResource(CurrentListNumber)
    If (ResourceId > 0) Then
        Call LoadRoomSchedule(ResourceId, 3)
        Behind3.Caption = "Minutes Behind:" + Str(GetMinutesBehind(3))
        Behind3.Visible = True
    Else
        SSDay3.Visible = False
        Behind3.Visible = False
    End If
Else
    SSDay3.Visible = False
    Behind3.Visible = False
End If
End Sub

Private Sub cmdPrev_Click()
Dim ResourceId As Long
CurrentListNumber = CurrentListNumber - 2
CurrentListNumber = (Int(CurrentListNumber / 3)) * 3 + 1
If (CurrentListNumber < 3) Then
    CurrentListNumber = 0
    Call cmdNext_Click
    Exit Sub
End If
If (CurrentListNumber - 1 > 0) Then
    CurrentListNumber = CurrentListNumber - 1
    ResourceId = GetRoomResource(CurrentListNumber)
    If (ResourceId > 0) Then
        Call LoadRoomSchedule(ResourceId, 3)
        Behind3.Caption = "Minutes Behind:" + Str(GetMinutesBehind(3))
        Behind3.Visible = True
    Else
        SSDay1.Visible = False
        Behind1.Visible = False
    End If
Else
    SSDay1.Visible = False
    Behind1.Visible = False
End If
If (CurrentListNumber - 1 > 0) Then
    CurrentListNumber = CurrentListNumber - 1
    ResourceId = GetRoomResource(CurrentListNumber)
    If (ResourceId > 0) Then
        Call LoadRoomSchedule(ResourceId, 2)
        Behind2.Caption = "Minutes Behind:" + Str(GetMinutesBehind(2))
        Behind2.Visible = True
    Else
        SSDay2.Visible = False
        Behind2.Visible = False
    End If
Else
    SSDay2.Visible = False
    Behind2.Visible = False
End If
If (CurrentListNumber - 1 > 0) Then
    CurrentListNumber = CurrentListNumber - 1
    ResourceId = GetRoomResource(CurrentListNumber)
    If (ResourceId > 0) Then
        Call LoadRoomSchedule(ResourceId, 1)
        Behind3.Caption = "Minutes Behind:" + Str(GetMinutesBehind(1))
        Behind3.Visible = True
    Else
        SSDay3.Visible = False
        Behind3.Visible = False
    End If
Else
    SSDay3.Visible = False
    Behind3.Visible = False
End If
End Sub

Private Function LoadDoctorList() As Boolean
Dim k As Integer
Dim NumberOfAppointments As Integer
Dim TotalResource As Integer
Dim DisplayItem As String
Dim RetrieveResource As SchedulerResource
LoadDoctorList = False
lstStaff.Clear
Set RetrieveResource = New SchedulerResource
RetrieveResource.ResourceName = Chr(1)
RetrieveResource.ResourceType = "S"
TotalDoctors = RetrieveResource.FindResource
If (TotalDoctors > 0) Then
    k = 1
    While (RetrieveResource.SelectResource(k))
        If (RetrieveResource.ResourceType <> "R") And (RetrieveResource.ResourceType <> "O") Then
            DisplayItem = Space(56)
            If (Len(RetrieveResource.ResourceDescription) <> 0) Then
                Mid(DisplayItem, 1, 20) = Left(RetrieveResource.ResourceDescription, 20)
            Else
                Mid(DisplayItem, 1, Len(RetrieveResource.ResourceName)) = RetrieveResource.ResourceName
            End If
            NumberOfAppointments = AppointmentsAssigned(RetrieveResource.ResourceId)
            Mid(DisplayItem, 22, Len(Trim(Str(NumberOfAppointments)))) = Trim(Str(NumberOfAppointments))
            Mid(DisplayItem, 45, Len(Trim(Str(RetrieveResource.ResourceColor)))) = Trim(Str(RetrieveResource.ResourceColor))
            DisplayItem = DisplayItem + Trim(Str(RetrieveResource.ResourceId))
            lstStaff.AddItem DisplayItem
        End If
        k = k + 1
    Wend
    LoadDoctorList = True
End If
Set RetrieveResource = Nothing
End Function

Private Sub LoadRoomSchedule(ResourceId As Long, ListNumber As Integer)
On Error GoTo UI_ErrorHandler
Dim TheColor As Long
Dim PatientName As String
Dim k As Integer, TotalAppts As Integer
Dim SlotName As String, ResourceName As String
Dim BeginTime As String, EndTime As String
Dim TDate As String, ApptDate As String
Dim ApptType As String
Dim RetrievePatient As Patient
Dim LocalDate As ManagedDate
Dim RetrieveAppt As SchedulerAppointment
Dim RetrieveApptSearch As SchedulerAppointment
Dim RetrieveApptType As SchedulerAppointmentType
Dim RetrieveRes As SchedulerResource
If (ListNumber = 1) Then
    SSDay1.Visible = False
    SSDay1.x.Tasks.RemoveAll
ElseIf (ListNumber = 2) Then
    SSDay2.Visible = False
    SSDay2.x.Tasks.RemoveAll
ElseIf (ListNumber = 3) Then
    SSDay3.Visible = False
    SSDay3.x.Tasks.RemoveAll
Else
    Exit Sub
End If
If (ResourceId < 1) Then
    Exit Sub
End If
Set LocalDate = New ManagedDate
Set RetrievePatient = New Patient
Set RetrieveRes = New SchedulerResource
Set RetrieveApptType = New SchedulerAppointmentType
Set RetrieveApptSearch = New SchedulerAppointment
RetrieveRes.ResourceId = ResourceId
If (RetrieveRes.RetrieveSchedulerResource) Then
    ResourceName = RetrieveRes.ResourceDescription
Else
    Set LocalDate = Nothing
    Set RetrievePatient = Nothing
    Set RetrieveRes = Nothing
    Set RetrieveApptType = Nothing
    Set RetrieveApptSearch = Nothing
    Exit Sub
End If
If (ListNumber = 1) Then
    SSDay1.Caption = ResourceName
    SSDay1.Visible = True
ElseIf (ListNumber = 2) Then
    SSDay2.Caption = ResourceName
    SSDay2.Visible = True
Else
    SSDay3.Caption = ResourceName
    SSDay3.Visible = True
End If
Call FormatTodaysDate(CurrentDate, False)
LocalDate.ExposedDate = CurrentDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    ApptDate = LocalDate.ExposedDate
End If
RetrieveApptSearch.ResourceId = ResourceId
RetrieveApptSearch.AppointmentDate = ApptDate
RetrieveApptSearch.AppointmentStatus = "P"
TotalAppts = RetrieveApptSearch.FindAppointmentbyResource
If (TotalAppts > 0) Then
    k = 1
    While (RetrieveApptSearch.SelectAppointment(k))
        TheColor = GetDoctorColor(RetrieveApptSearch.AppointmentId)
        RetrievePatient.PatientId = RetrieveApptSearch.AppointmentPatientId
        If (RetrievePatient.RetrievePatient) Then
            PatientName = RetrievePatient.FirstName + " " _
                        + RetrievePatient.MiddleInitial + " " _
                        + RetrievePatient.LastName + " " _
                        + RetrievePatient.NameRef
        Else
            PatientName = ""
        End If
        Call GetAppointmentType(RetrieveApptSearch.AppointmentTypeId, ApptType)
        Call ConvertMinutesToTime(RetrieveApptSearch.AppointmentTime, BeginTime)
        Call AddTime(BeginTime, RetrieveApptSearch.AppointmentDuration, EndTime)
        SlotName = BeginTime + " - " + RetrieveApptSearch.AppointmentStatus + " " + PatientName + " " + ApptType
        If (ListNumber = 1) Then
            SSDay1.x.Tasks.Add BeginTime, EndTime, SlotName, RGB(TheColor, 0, 0)
        ElseIf (ListNumber = 2) Then
            SSDay2.x.Tasks.Add BeginTime, EndTime, SlotName, RGB(TheColor, 0, 0)
        ElseIf (ListNumber = 3) Then
            SSDay3.x.Tasks.Add BeginTime, EndTime, SlotName, RGB(TheColor, 0, 0)
        End If
        k = k + 1
    Wend
End If
If (ListNumber = 1) Then
    Call PracticeTimes(0, TDate, BeginTime, EndTime)
    SSDay1.TimeBegin = BeginTime
    SSDay1.TimeEnd = EndTime
    Behind1.Caption = "Minutes Behind:" + Str(GetMinutesBehind(1))
    Behind1.Visible = True
ElseIf (ListNumber = 2) Then
    Call PracticeTimes(0, TDate, BeginTime, EndTime)
    SSDay2.TimeBegin = BeginTime
    SSDay2.TimeEnd = EndTime
    Behind2.Caption = "Minutes Behind:" + Str(GetMinutesBehind(2))
    Behind2.Visible = True
ElseIf (ListNumber = 3) Then
    Call PracticeTimes(0, TDate, BeginTime, EndTime)
    SSDay3.TimeBegin = BeginTime
    SSDay3.TimeEnd = EndTime
    Behind3.Caption = "Minutes Behind:" + Str(GetMinutesBehind(3))
    Behind3.Visible = True
End If
Set LocalDate = Nothing
Set RetrievePatient = Nothing
Set RetrieveRes = Nothing
Set RetrieveApptType = Nothing
Set RetrieveApptSearch = Nothing
Exit Sub
UI_ErrorHandler:
    Set LocalDate = Nothing
    Set RetrievePatient = Nothing
    Set RetrieveRes = Nothing
    Set RetrieveApptType = Nothing
    Set RetrieveApptSearch = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Public Function LoadFullSchedule() As Boolean
LoadFullSchedule = False
TotalRooms = GetTotalRooms
If (TotalRooms > 0) Then
    CurrentListNumber = 0
    LoadFullSchedule = LoadDoctorList
    If (LoadFullSchedule) Then
        Call cmdNext_Click
    End If
End If
End Function

Private Sub GetAppointmentType(ApptTypeId As Long, ApptType As String)
Dim RetrieveApptType As SchedulerAppointmentType
Set RetrieveApptType = New SchedulerAppointmentType
RetrieveApptType.AppointmentTypeId = ApptTypeId
If (RetrieveApptType.RetrieveSchedulerAppointmentType) Then
    ApptType = RetrieveApptType.AppointmentType
Else
    ApptType = "INVALID"
End If
Set RetrieveApptType = Nothing
End Sub

Private Function GetTotalRooms() As Long
Dim RetrieveResource As SchedulerResource
GetTotalRooms = 0
Set RetrieveResource = New SchedulerResource
RetrieveResource.ResourceName = Chr(1)
RetrieveResource.ResourceType = "R"
GetTotalRooms = RetrieveResource.FindResource
Set RetrieveResource = Nothing
End Function

Private Function GetRoomResource(ListNumber As Integer) As Long
Dim RetrieveResource As SchedulerResource
GetRoomResource = 0
If (ListNumber > 0) Then
    Set RetrieveResource = New SchedulerResource
    RetrieveResource.ResourceName = Chr(1)
    RetrieveResource.ResourceType = "R"
    TotalRooms = RetrieveResource.FindResource
    If (TotalRooms > 0) Then
        If (RetrieveResource.SelectResource(ListNumber)) Then
            GetRoomResource = RetrieveResource.ResourceId
        End If
    End If
    Set RetrieveResource = Nothing
End If
End Function

Private Function GetMinutesBehind(ListNumber As Integer) As Long
Dim k As Integer, u As Integer
Dim CurTime As Integer, CurApptTime As Integer
Dim CurrentTime As String, ApptTime As String
Dim DisplayText As String
GetMinutesBehind = 0
If (ListNumber > 0) Then
    CurrentTime = ""
    Call FormatTimeNow(CurrentTime)
    CurTime = ConvertTimeToMinutes(CurrentTime)
    ApptTime = ""
    GetMinutesBehind = 0
    If (ListNumber = 1) Then
        If (SSDay1.Tasks.Count < 1) Then
            Exit Function
        End If
        For k = 0 To SSDay1.Tasks.Count - 1
            DisplayText = Trim(SSDay1.Tasks.Item(k).Text)
            If (DisplayText <> "") Then
                u = InStrPS(DisplayText, "-")
                If (Mid(DisplayText, u + 2, 1) = "P") Then
                    ApptTime = Mid(DisplayText, 1, u - 2)
                    k = SSDay1.Tasks.Count
                End If
            End If
        Next k
    ElseIf (ListNumber = 2) Then
        If (SSDay2.Tasks.Count < 1) Then
            Exit Function
        End If
        For k = 0 To SSDay2.Tasks.Count - 1
            DisplayText = Trim(SSDay2.Tasks.Item(k).Text)
            If (DisplayText <> "") Then
                u = InStrPS(DisplayText, "-")
                If (Mid(DisplayText, u + 2, 1) = "P") Then
                    ApptTime = Mid(DisplayText, 1, u - 2)
                    k = SSDay2.Tasks.Count
                End If
            End If
        Next k
    ElseIf (ListNumber = 3) Then
        If (SSDay3.Tasks.Count < 1) Then
            Exit Function
        End If
        For k = 0 To SSDay3.Tasks.Count - 1
            DisplayText = Trim(SSDay3.Tasks.Item(k).Text)
            If (DisplayText <> "") Then
                u = InStrPS(DisplayText, "-")
                If (Mid(DisplayText, u + 2, 1) = "P") Then
                    ApptTime = Mid(DisplayText, 1, u - 2)
                    k = SSDay3.Tasks.Count
                End If
            End If
        Next k
    End If
    If (ApptTime <> "") Then
        CurApptTime = ConvertTimeToMinutes(ApptTime)
        If (CurApptTime >= CurTime) Then
            GetMinutesBehind = 0
        Else
            GetMinutesBehind = (CurTime - CurApptTime)
        End If
    End If
End If
End Function

Private Function GetDoctorColor(ApptId As Long) As Long
Dim RetrieveAppt As SchedulerAppointment
Dim i, DoctorId As Long
GetDoctorColor = 255
Set RetrieveAppt = New SchedulerAppointment
RetrieveAppt.AppointmentId = ApptId
If (RetrieveAppt.RetrieveSchedulerAppointment) Then
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId1)
    If (i > 0) Then
        GetDoctorColor = i
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId2)
    If (i > 0) Then
        GetDoctorColor = i
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId3)
    If (i > 0) Then
        GetDoctorColor = i
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId4)
    If (i > 0) Then
        GetDoctorColor = i
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId5)
    If (i > 0) Then
        GetDoctorColor = i
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId6)
    If (i > 0) Then
        GetDoctorColor = i
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId7)
    If (i > 0) Then
        GetDoctorColor = i
    End If
End If
Set RetrieveAppt = Nothing
End Function

Private Function LocateDoctorColor(ResourceId) As Long
Dim i, StfLen, ListDoctor As Long
LocateDoctorColor = -1
If (ResourceId < 1) Then
    Exit Function
End If
For i = 0 To TotalDoctors - 1
    StfLen = Len(lstStaff.List(i)) - 56
    ListDoctor = Val(Trim(Mid(lstStaff.List(i), 57, StfLen)))
    If (ResourceId = ListDoctor) Then
        LocateDoctorColor = Val(Trim(Mid(lstStaff.List(i), 45, 10)))
        i = TotalDoctors
    End If
Next i
End Function

Private Function AppointmentsAssigned(ResourceId As Long) As Integer
Dim TheDate As String
Dim NewLocal As ManagedDate
Dim TheResource As SchedulerAppointment
AppointmentsAssigned = 0
Set NewLocal = New ManagedDate
Set TheResource = New SchedulerAppointment
TheResource.ResourceId = ResourceId
TheResource.AppointmentStatus = ""
Call FormatTodaysDate(TheDate, False)
NewLocal.ExposedDate = TheDate
If (NewLocal.ConvertDisplayDateToManagedDate) Then
    TheResource.AppointmentDate = NewLocal.ExposedDate
Else
    TheResource.AppointmentDate = ""
End If
TheResource.AppointmentStatus = "P"
AppointmentsAssigned = TheResource.FindAppointmentbyResource
If (AppointmentsAssigned < 0) Then
    AppointmentsAssigned = 0
End If
Set NewLocal = Nothing
Set TheResource = Nothing
End Function

Private Sub lstStaff_DblClick()
    Call cmdDoctor_Click
End Sub

Private Sub SSDay1_DblClick()
lblDetails.Caption = SSDay1.Tasks.Item(SSDay1.TaskSelected).Text
lblDetails.Visible = True
End Sub

Private Sub SSDay2_DblClick()
lblDetails.Caption = SSDay2.Tasks.Item(SSDay2.TaskSelected).Text
lblDetails.Visible = True
End Sub

Private Sub SSDay3_DblClick()
lblDetails.Caption = SSDay3.Tasks.Item(SSDay3.TaskSelected).Text
lblDetails.Visible = True
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
CurrentDate = SelDate
Call FormatTodaysDate(CurrentDate, False)
Call UpdateApptsAssigned(CurrentDate)
CurrentListNumber = 0
Call cmdNext_Click
End Sub

Private Sub UpdateApptsAssigned(TheDate As String)
Dim ResourceId As Long, ApptNumber As Long
Dim q As Integer, LocalText As String
Dim TheAppt As SchedulerAppointment
Dim LocalDate As ManagedDate
Set TheAppt = New SchedulerAppointment
Set LocalDate = New ManagedDate
For q = 0 To lstStaff.ListCount - 1
    ResourceId = Val(Trim(Mid(lstStaff.List(q), 57, 5)))
    LocalDate.ExposedDate = TheDate
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        TheAppt.AppointmentDate = LocalDate.ExposedDate
    End If
    TheAppt.ResourceId = ResourceId
    TheAppt.AppointmentStatus = "P"
    ApptNumber = TheAppt.FindAppointmentbyResource
    If (ApptNumber < 0) Then
        ApptNumber = 0
    End If
    LocalText = lstStaff.List(q)
    Mid(LocalText, 22, 7) = Space(7)
    Mid(LocalText, 22, Len(Trim(Str(ApptNumber)))) = Trim(Str(ApptNumber))
    lstStaff.List(q) = LocalText
Next q
Set TheAppt = Nothing
Set LocalDate = Nothing
End Sub

