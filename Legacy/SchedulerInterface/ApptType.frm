VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmApptType 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form2"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   FillColor       =   &H00FFFFFF&
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form2"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtConcurrent 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11040
      MaxLength       =   4
      TabIndex        =   37
      Top             =   3000
      Width           =   615
   End
   Begin VB.CheckBox chkSurgOn 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Surgery Appt Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   4560
      TabIndex        =   36
      Top             =   3960
      Width           =   2055
   End
   Begin VB.TextBox txtEMBasis 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5880
      MaxLength       =   10
      TabIndex        =   35
      Top             =   2520
      Width           =   2415
   End
   Begin VB.CheckBox chkPlan 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Available In Plan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   9960
      TabIndex        =   33
      Top             =   3480
      Value           =   1  'Checked
      Width           =   1935
   End
   Begin VB.CheckBox chkFv 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "First Visit Indicator"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   9960
      TabIndex        =   32
      Top             =   3720
      Width           =   2055
   End
   Begin VB.TextBox txtMaxRecalls 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11055
      MaxLength       =   4
      TabIndex        =   30
      Top             =   2520
      Width           =   615
   End
   Begin VB.TextBox txtRecall 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11055
      MaxLength       =   4
      TabIndex        =   12
      Top             =   2040
      Width           =   615
   End
   Begin VB.TextBox txtRank1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11040
      MaxLength       =   3
      TabIndex        =   27
      Top             =   4560
      Width           =   615
   End
   Begin VB.TextBox txtRank 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11040
      MaxLength       =   3
      TabIndex        =   7
      Top             =   4080
      Width           =   615
   End
   Begin VB.ListBox lstRooms 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1530
      ItemData        =   "ApptType.frx":0000
      Left            =   375
      List            =   "ApptType.frx":0002
      Style           =   1  'Checkbox
      TabIndex        =   8
      Top             =   6300
      Width           =   3900
   End
   Begin VB.ListBox lstQuestion 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1950
      ItemData        =   "ApptType.frx":0004
      Left            =   4515
      List            =   "ApptType.frx":0017
      TabIndex        =   9
      Top             =   5880
      Width           =   2775
   End
   Begin VB.ListBox lstStaff 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2130
      ItemData        =   "ApptType.frx":0075
      Left            =   7530
      List            =   "ApptType.frx":0077
      Style           =   1  'Checkbox
      TabIndex        =   10
      Top             =   5700
      Width           =   4095
   End
   Begin VB.ListBox lstDur 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   990
      ItemData        =   "ApptType.frx":0079
      Left            =   7560
      List            =   "ApptType.frx":0089
      TabIndex        =   6
      Top             =   4080
      Width           =   1335
   End
   Begin VB.CheckBox chkPc 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Pre-Cert Required"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   4560
      TabIndex        =   4
      Top             =   3480
      Width           =   2055
   End
   Begin VB.CheckBox chkDr 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Doctor sign off Required"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   4560
      TabIndex        =   5
      Top             =   3720
      Width           =   2415
   End
   Begin VB.TextBox txtRecurring 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11055
      MaxLength       =   4
      TabIndex        =   11
      Top             =   1560
      Width           =   615
   End
   Begin VB.TextBox txtDoctorTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11055
      MaxLength       =   4
      TabIndex        =   2
      Top             =   600
      Width           =   615
   End
   Begin VB.TextBox txtTechTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   11055
      MaxLength       =   4
      TabIndex        =   3
      Top             =   1080
      Width           =   615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   360
      TabIndex        =   15
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":00AA
   End
   Begin VB.ListBox lstApptType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2730
      ItemData        =   "ApptType.frx":0289
      Left            =   375
      List            =   "ApptType.frx":028B
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   1080
      Width           =   3900
   End
   Begin VB.TextBox txtApptType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   360
      MaxLength       =   32
      TabIndex        =   1
      Top             =   4440
      Width           =   3900
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   9840
      TabIndex        =   13
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":028D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5160
      TabIndex        =   14
      Top             =   7920
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":046C
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4080
      Top             =   5160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Flags           =   1
   End
   Begin VB.Label lblColor 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      TabIndex        =   40
      Top             =   5040
      Width           =   1980
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Color"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   39
      Top             =   5040
      Width           =   1860
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Number of concurrent Appointments allowed"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      TabIndex        =   38
      Top             =   3000
      Width           =   5895
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "E M Basis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      TabIndex        =   34
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label Label16 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Maximum Recalls"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      TabIndex        =   31
      Top             =   2520
      Width           =   1935
   End
   Begin VB.Label Label15 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Recall Frequency (months between postings)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      TabIndex        =   29
      Top             =   2040
      Width           =   5895
   End
   Begin VB.Label Label14 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Schedule Rank"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   9240
      TabIndex        =   28
      Top             =   4560
      Width           =   1725
   End
   Begin VB.Label Label13 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Di Plan Rank"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   9240
      TabIndex        =   26
      Top             =   4080
      Width           =   1725
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time Between Appts"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   7440
      TabIndex        =   25
      Top             =   3480
      Width           =   1575
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Click on all the people who could be part of this exam"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   7530
      TabIndex        =   24
      Top             =   5070
      Width           =   4095
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Number of minutes this exam takes the technician"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      TabIndex        =   23
      Top             =   1080
      Width           =   5895
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Select the question set for this appointment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   4515
      TabIndex        =   22
      Top             =   5280
      Width           =   2775
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Number of recurring appointments needed"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      TabIndex        =   21
      Top             =   1560
      Width           =   5895
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Appointment Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   390
      TabIndex        =   20
      Top             =   600
      Width           =   3825
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Number of minutes this exam takes the doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      TabIndex        =   19
      Top             =   600
      Width           =   5895
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Click on all the rooms that this exam may be conducted"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   360
      TabIndex        =   18
      Top             =   5640
      Width           =   3900
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Enter the name of the exam type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   375
      TabIndex        =   17
      Top             =   3900
      Width           =   3900
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Setup Appointment Types"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   375
      Left            =   3960
      TabIndex        =   16
      Top             =   120
      Width           =   4095
   End
End
Attribute VB_Name = "frmApptType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AppointmentTypeId As Long
Private RemoveOn As Boolean
Private OrgApptType As String
Private TheResourceId(7) As Long

Private Sub cmdApply_Click()
Dim AptType As String, Aem As String
Dim Dur As String, QSet As String
Dim TheAction As String
Dim iColor As Long
Dim SryOn As Long
Dim Afv As Boolean, Apl As Boolean
Dim PcOn As Boolean, DrOn As Boolean
Dim ApplTbl As ApplicationTables
If (lstRooms.Visible) Then
    Call SetResources
    AptType = Trim(txtApptType.Text)
    If (AppointmentTypeId < 1) Then
        Call ReplaceCharacters(AptType, "/", " ")
        Call ReplaceCharacters(AptType, "-", " ")
        txtApptType.Text = Trim(AptType)
        DoEvents
    End If
    Set ApplTbl = New ApplicationTables
    If (OrgApptType <> UCase(Trim(AptType))) Then
        If (ApplTbl.ApplIsAppointmentType(AptType)) Then
            frmEventMsgs.Header = "Appointment Type Already Exists"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtApptType.SetFocus
            SendKeys "{Home}+{End}"
            Set ApplTbl = Nothing
            Exit Sub
        End If
    End If
    If (lstQuestion.ListIndex < 0) Then
        QSet = Trim(lstQuestion.List(0))
    Else
        QSet = Trim(lstQuestion.List(lstQuestion.ListIndex))
    End If
    Apl = chkPlan.Value
    DrOn = chkDr.Value
    PcOn = chkPc.Value
    Afv = chkFv.Value
    SryOn = 0
    If (chkSurgOn.Value = 1) Then
        SryOn = 1
    End If
    Dur = ""
    If (lstDur.ListIndex > 0) Then
        Dur = Left(lstDur.List(lstDur.ListIndex), 1)
    End If
    Aem = Trim(txtEMBasis.Text)
    TheResourceId(7) = val(Trim(txtConcurrent.Text))
    
    If IsNumeric(lblColor.Caption) Then
        iColor = lblColor.Caption
    Else
        iColor = vbWhite
    End If
    
    If (ApplTbl.ApplPostAppointmentType(AppointmentTypeId, _
                                        UCase(Trim(AptType)), _
                                        Trim(txtDoctorTime.Text), _
                                        Trim(txtTechTime.Text), _
                                        Trim(txtRecurring.Text), _
                                        Dur, _
                                        QSet, _
                                        PcOn, _
                                        DrOn, _
                                        TheResourceId(1), _
                                        TheResourceId(2), _
                                        TheResourceId(3), _
                                        TheResourceId(4), _
                                        TheResourceId(5), _
                                        iColor, _
                                        TheResourceId(7), _
                                        SryOn, _
                                        val(Trim(txtRank.Text)), _
                                        val(Trim(txtRank1.Text)), _
                                        val(Trim(txtRecall.Text)), _
                                        val(Trim(txtMaxRecalls.Text)), _
                                        "", _
                                        Aem, _
                                        Afv, _
                                        Apl)) Then
    Else
        frmEventMsgs.Header = "Update not applied"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
    Set ApplTbl = Nothing
    If (OrgApptType = Trim(txtApptType.Text)) Then
        Call LoadApptTypeList
    Else
        OrgApptType = Trim(txtApptType.Text)
    End If
    Exit Sub
End If
Unload frmApptType
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
If (AppointmentTypeId > 0) Then
    Set ApplTbl = New ApplicationTables
    If Not (ApplTbl.ApplIsAppointmentTypeInUse(AppointmentTypeId)) Then
        If (ApplTbl.ApplDeleteAppointmentType(AppointmentTypeId)) Then
            Unload frmApptType
        End If
    Else
        frmEventMsgs.Header = "Appointment Type is in use. Delete Denied."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
    Set ApplTbl = Nothing
End If
End Sub

Private Sub cmdExit_Click()
Unload frmApptType
End Sub

Public Function LoadApptTypeList() As Boolean
Dim Ref As String
Dim ApplTbl As ApplicationTables
Dim ApplSch As ApplicationScheduler
LoadApptTypeList = False
Set ApplTbl = New ApplicationTables
Set ApplSch = New ApplicationScheduler
Set ApplTbl.lstBox = lstRooms
Call ApplTbl.ApplLoadResource("R", True, True, False, False, False)
Set ApplTbl.lstBox = lstStaff
Call ApplTbl.ApplLoadResource("S", True, True, False, False, False)
Set ApplSch.lstStartTime = lstQuestion
Call ApplSch.LoadQuestionSets
Set ApplTbl.lstBox = lstApptType
Call ApplTbl.ApplLoadApptType(True)
Set ApplTbl = Nothing
Set ApplSch = Nothing
Call RemoveManualClaim
cmdDelete.Visible = False
OrgApptType = ""
RemoveOn = CheckConfigCollection("REMOVEACTIVE") = "T"
cmdDelete.Visible = RemoveOn
Label2.Visible = False
txtApptType.Visible = False
Label18.Visible = False
lblColor.Visible = False
Label6.Visible = True
lstApptType.Enabled = True
lstApptType.Visible = True
Label4.Visible = False
lstRooms.Enabled = False
lstRooms.Visible = False
Label3.Visible = False
lstStaff.Enabled = False
lstStaff.Visible = False
Label5.Visible = False
txtDoctorTime.Enabled = False
txtDoctorTime.Visible = False
Label9.Visible = False
txtTechTime.Enabled = False
txtTechTime.Visible = False
Label7.Visible = False
txtRecurring.Enabled = False
txtRecurring.Visible = False
Label8.Visible = False
lstQuestion.Enabled = False
lstQuestion.Visible = False
chkFv.Enabled = False
chkFv.Visible = False
chkSurgOn.Visible = False
chkSurgOn.Enabled = False
chkPlan.Visible = False
chkPlan.Enabled = False
chkFv.Visible = False
chkPc.Enabled = False
chkPc.Visible = False
chkDr.Enabled = False
chkDr.Visible = False
Label12.Visible = False
lstDur.Visible = False
Label13.Visible = False
txtRank.Visible = False
txtRank.Enabled = False
txtRank.Text = ""
Label14.Visible = False
txtRank1.Visible = False
txtRank1.Enabled = False
txtRank1.Text = ""
Label15.Visible = False
txtRecall.Visible = False
txtRecall.Enabled = False
txtRecall.Text = ""
Label16.Visible = False
txtMaxRecalls.Visible = False
txtMaxRecalls.Enabled = False
txtMaxRecalls.Text = ""
Label10.Visible = False
txtEMBasis.Visible = False
txtEMBasis.Enabled = False
txtEMBasis.Text = ""
Label11.Visible = False
txtConcurrent.Visible = False
txtConcurrent.Enabled = False
txtConcurrent.Text = ""
LoadApptTypeList = True
End Function

Private Sub SetResources()
Dim k, l As Integer
Dim ResourceId As Long
For k = 1 To 6
    TheResourceId(k) = 0
Next k
k = 0
For l = 1 To lstStaff.ListCount - 1
    ResourceId = val(Mid(lstStaff.List(l), 56, Len(lstStaff.List(l)) - 55))
    If (lstStaff.Selected(l)) Then
        k = k + 1
        If (k <= 6) Then
            TheResourceId(k) = ResourceId
        End If
    End If
Next l
For l = 1 To lstRooms.ListCount - 1
    ResourceId = val(Mid(lstRooms.List(l), 56, Len(lstRooms.List(l)) - 55))
    If (lstRooms.Selected(l)) Then
        k = k + 1
        If (k <= 6) Then
            TheResourceId(k) = ResourceId
        End If
    End If
Next l
End Sub

Private Function LoadApptType(ApptTypeId As Long) As Boolean
Dim SomethingSet As Boolean
Dim Ref As String
Dim k As Integer
Dim l As Integer
Dim ApplTbl As ApplicationTables
Dim Afv As Boolean, Apl As Boolean
Dim SryOn As Long
Dim ResourceId As Long, Aem As String
Dim AName As String, Dur As String, Dura As String, TheMRecall As Integer
Dim Period As String, Recur As String, QSet As String, TheRecall As Integer
Dim PcOn As Boolean, DrOn As Boolean, TheRank As Integer, TheRank1 As Integer
Dim ResId(8) As Long
Dim iColor As Long
Dim TheApptType As SchedulerAppointmentType
AppointmentTypeId = ApptTypeId
Set ApplTbl = New ApplicationTables
If (ApplTbl.ApplGetAppointmentType(ApptTypeId, AName, Dur, Dura, Recur, Period, QSet, _
                                   PcOn, DrOn, ResId(1), ResId(2), ResId(3), ResId(4), _
                                   ResId(5), iColor, ResId(7), SryOn, TheRank, _
                                   TheRank1, TheRecall, TheMRecall, "", Aem, Afv, Apl)) Then
    AppointmentTypeId = ApptTypeId
    txtDoctorTime.Text = Dur
    txtTechTime.Text = Dura
    txtRecurring.Text = Recur
    chkSurgOn.Value = SryOn
    If (Afv) Then
        chkFv.Value = 1
    Else
        chkFv.Value = 0
    End If
    If (PcOn) Then
        chkPc.Value = 1
    Else
        chkPc.Value = 0
    End If
    If (DrOn) Then
        chkDr.Value = 1
    Else
        chkDr.Value = 0
    End If
    If (Apl) Then
        chkPlan.Value = 1
    Else
        chkPlan.Value = 0
    End If
    If (Trim(Period) <> "") Then
        For l = 1 To lstDur.ListCount - 1
            If (Left(lstDur.List(l), 1) = Period) Then
                lstDur.ListIndex = l
                Exit For
            End If
        Next l
    Else
        lstDur.ListIndex = 0
    End If
    SomethingSet = False
    For k = 1 To 7
        For l = 1 To lstStaff.ListCount - 1
            ResourceId = val(Mid(lstStaff.List(l), 56, Len(lstStaff.List(l)) - 55))
            If (ResourceId = ResId(k)) Then
                lstStaff.Selected(l) = True
                SomethingSet = True
            End If
        Next l
    Next k
    If Not (SomethingSet) Then
        lstStaff.Selected(0) = True
    End If
    SomethingSet = False
    For k = 1 To 7
        For l = 1 To lstRooms.ListCount - 1
            ResourceId = val(Mid(lstRooms.List(l), 56, Len(lstRooms.List(l)) - 55))
            If (ResourceId = ResId(k)) Then
                lstRooms.Selected(l) = True
                SomethingSet = True
            End If
        Next l
    Next k
    If Not (SomethingSet) Then
        lstRooms.Selected(0) = True
    End If
    For k = 0 To lstQuestion.ListCount - 1
        If (Trim(QSet) = Trim(lstQuestion.List(k))) Then
            lstQuestion.ListIndex = k
        End If
    Next k
    cmdDelete.Visible = RemoveOn
    If (AppointmentTypeId < 1) Then
        cmdDelete.Visible = False
    End If
    txtApptType.Text = UCase(Trim(txtApptType.Text))
    If (lstApptType.ListIndex = 0) Then
        Label6.Visible = False
        lstApptType.Visible = False
    Else
        Label2.Visible = True
        txtApptType.Visible = True
        OrgApptType = txtApptType.Text
    End If
    Label18.Visible = True
    lblColor.Visible = True
    Label5.Visible = True
    txtDoctorTime.Enabled = True
    txtDoctorTime.Visible = True
    Label9.Visible = True
    txtTechTime.Enabled = True
    txtTechTime.Visible = True
    Label7.Visible = True
    txtRecurring.Enabled = True
    txtRecurring.Visible = True
    chkPlan.Enabled = True
    chkPlan.Visible = True
    chkSurgOn.Visible = True
    chkSurgOn.Enabled = True
    chkFv.Enabled = True
    chkFv.Visible = True
    chkPc.Enabled = True
    chkPc.Visible = True
    chkDr.Enabled = True
    chkDr.Visible = True
    Label12.Visible = True
    lstDur.Visible = True
    Label4.Visible = True
    lstRooms.Enabled = True
    lstRooms.Visible = True
    Label3.Visible = True
    lstStaff.Enabled = True
    lstStaff.Visible = True
    Label8.Visible = True
    lstQuestion.Enabled = True
    lstQuestion.Visible = True
    Label13.Visible = True
    txtRank.Visible = True
    txtRank.Enabled = True
    txtRank.Text = Trim(Str(TheRank))
    Label14.Visible = True
    txtRank1.Visible = True
    txtRank1.Enabled = True
    txtRank1.Text = Trim(Str(TheRank1))
    Label15.Visible = True
    txtRecall.Visible = True
    txtRecall.Enabled = True
    txtRecall.Text = Trim(Str(TheRecall))
    Label16.Visible = True
    txtMaxRecalls.Visible = True
    txtMaxRecalls.Enabled = True
    txtMaxRecalls.Text = Trim(Str(TheMRecall))
    Label10.Visible = True
    txtEMBasis.Visible = True
    txtEMBasis.Enabled = True
    txtEMBasis.Text = Trim(Aem)
    Label11.Visible = True
    txtConcurrent.Visible = True
    txtConcurrent.Enabled = True
    txtConcurrent.Text = Trim(Str(ResId(7)))
    If iColor = 0 Then
        lblColor.Caption = vbWhite
    Else
        lblColor.Caption = iColor
    End If
    cmdApply.Text = "Done"
    lstApptType.Enabled = False
    cmdDelete.Visible = RemoveOn
End If
Set ApplTbl = Nothing
End Function

Private Sub ClearControls()
Dim k As Integer
txtDoctorTime.Text = ""
txtTechTime.Text = ""
For k = 1 To lstStaff.ListCount - 1
    lstStaff.Selected(k) = False
Next k
For k = 1 To lstRooms.ListCount - 1
    lstRooms.Selected(k) = False
Next k
RemoveOn = False
cmdDelete.Visible = False
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmApptType.BorderStyle = 1
    frmApptType.ClipControls = True
    frmApptType.Caption = Mid(frmApptType.Name, 4, Len(frmApptType.Name) - 3)
    frmApptType.AutoRedraw = True
    frmApptType.Refresh
End If
End Sub

Private Sub lblColor_Change()
With lblColor
    .BackColor = .Caption
    .ForeColor = .BackColor
End With
End Sub

Private Sub lstApptType_Click()
If (lstApptType.ListIndex = 0) Then
    AppointmentTypeId = -1
    Label2.Visible = True
    txtApptType.Text = ""
    txtApptType.Visible = True
    txtApptType.SetFocus
ElseIf (lstApptType.ListIndex > 0) Then
    Label2.Visible = True
    AppointmentTypeId = val(Mid(lstApptType.List(lstApptType.ListIndex), 57, Len(lstApptType.List(lstApptType.ListIndex)) - 56))
    txtApptType.Text = Left(lstApptType.List(lstApptType.ListIndex), 32)
    txtApptType.Visible = True
    Call LoadApptType(AppointmentTypeId)
End If
End Sub

Private Sub lstApptType_DblClick()
If (lstApptType.ListIndex = 0) Then
    AppointmentTypeId = -1
    Label2.Visible = True
    txtApptType.Text = ""
    txtApptType.Visible = True
    txtApptType.SetFocus
ElseIf (lstApptType.ListIndex > 0) Then
    AppointmentTypeId = val(Mid(lstApptType.List(lstApptType.ListIndex), 57, Len(lstApptType.List(lstApptType.ListIndex)) - 56))
    txtApptType.Text = Left(lstApptType.List(lstApptType.ListIndex), 32)
    Call LoadApptType(AppointmentTypeId)
End If
End Sub

Private Sub lstRooms_Click()
Dim k As Integer
If (lstRooms.ListIndex = 0) Then
    For k = 1 To lstRooms.ListCount - 1
        lstRooms.Selected(k) = False
    Next k
ElseIf (lstRooms.ListIndex > 0) Then
    lstRooms.Selected(0) = False
End If
End Sub

Private Sub lstStaff_Click()
Dim k As Integer
If (lstStaff.ListIndex = 0) Then
    For k = 1 To lstStaff.ListCount - 1
        lstStaff.Selected(k) = False
    Next k
ElseIf (lstStaff.ListIndex > 0) Then
    lstStaff.Selected(0) = False
End If
End Sub

Private Sub lblColor_Click()
CommonDialog1.CancelError = True
On Error GoTo UI_ErrorHandler
CommonDialog1.Flags = &H1&
CommonDialog1.ShowColor
lblColor.Caption = Trim(Str(CommonDialog1.Color))
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:

End Sub

Private Sub txtApptType_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call txtApptType_Validate(False)
End If
End Sub

Private Sub txtApptType_Validate(Cancel As Boolean)
Dim ApplTbl As ApplicationTables
If (OrgApptType <> Trim(txtApptType.Text)) Then
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplIsAppointmentType(txtApptType.Text)) Then
        frmEventMsgs.Header = "Appointment Type Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtApptType.SetFocus
        txtApptType.Text = ""
        SendKeys "{Home}"
    Else
        Call LoadApptType(-1)
    End If
    Set ApplTbl = Nothing
End If
End Sub

Private Sub txtConcurrent_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtTechTime_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtDoctorTime_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtRecurring_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtRank_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtRecall_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtRank1_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtMaxRecalls_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) And (KeyAscii <> 8) Then
    frmEventMsgs.Header = "Enter A Numeric Value [0123456789]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
Public Sub RemoveManualClaim()
Dim i As Integer
For i = lstApptType.ListCount - 1 To 1 Step -1
    If val(Mid(lstApptType.List(i), 57, Len(lstApptType.List(i)) - 56)) = 0 Then
        lstApptType.RemoveItem i
        i = i + 1
    End If
Next
End Sub
