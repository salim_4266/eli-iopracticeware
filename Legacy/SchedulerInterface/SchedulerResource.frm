VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSchedulerResource 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtErxVal 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7980
      MaxLength       =   8
      TabIndex        =   68
      Top             =   3960
      Width           =   1935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdInfo 
      Height          =   990
      Left            =   5160
      TabIndex        =   67
      Top             =   7320
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":0000
   End
   Begin VB.TextBox txtTaxy 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   16
      TabIndex        =   65
      Top             =   3480
      Width           =   1935
   End
   Begin VB.TextBox txtNPI 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   63
      Top             =   3000
      Width           =   1935
   End
   Begin VB.CheckBox chkGo 
      BackColor       =   &H0077742D&
      Caption         =   "Go Direct"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2280
      TabIndex        =   62
      Top             =   6600
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox chkBill 
      BackColor       =   &H0077742D&
      Caption         =   "Doctor Billable"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2280
      TabIndex        =   61
      Top             =   6360
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.TextBox Breaks 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2340
      MaxLength       =   2
      TabIndex        =   22
      Top             =   4440
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox Overload 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2340
      MaxLength       =   2
      TabIndex        =   20
      Top             =   3960
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox txtPOS 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2340
      MaxLength       =   10
      TabIndex        =   25
      Top             =   5880
      Width           =   1575
   End
   Begin VB.TextBox txtFile 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2340
      MaxLength       =   64
      TabIndex        =   24
      Top             =   5400
      Width           =   3135
   End
   Begin VB.TextBox txtSrvCode 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2340
      MaxLength       =   10
      TabIndex        =   23
      Top             =   4920
      Width           =   1575
   End
   Begin VB.TextBox txtFirst 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6240
      MaxLength       =   35
      TabIndex        =   2
      Top             =   600
      Width           =   2895
   End
   Begin VB.TextBox txtLast 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   35
      TabIndex        =   1
      Top             =   600
      Width           =   3375
   End
   Begin VB.TextBox txtPid 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   4
      TabIndex        =   3
      Top             =   600
      Width           =   1935
   End
   Begin VB.TextBox Description 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   64
      TabIndex        =   0
      Top             =   120
      Width           =   7745
   End
   Begin VB.TextBox txtLic 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   32
      TabIndex        =   15
      Top             =   2520
      Width           =   1935
   End
   Begin VB.TextBox txtDea 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   32
      TabIndex        =   12
      Top             =   2040
      Width           =   1935
   End
   Begin VB.TextBox txtUpin 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   6
      TabIndex        =   9
      Top             =   1560
      Width           =   1935
   End
   Begin VB.ListBox lstAff 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2460
      ItemData        =   "SchedulerResource.frx":01EA
      Left            =   5820
      List            =   "SchedulerResource.frx":01EC
      Sorted          =   -1  'True
      TabIndex        =   26
      Top             =   4440
      Width           =   6015
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   975
      Left            =   120
      TabIndex        =   30
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":01EE
   End
   Begin VB.ListBox lstPractice 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   870
      ItemData        =   "SchedulerResource.frx":03CD
      Left            =   5820
      List            =   "SchedulerResource.frx":03CF
      TabIndex        =   14
      Top             =   2520
      Width           =   3315
   End
   Begin VB.TextBox Specialty 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4920
      MaxLength       =   64
      TabIndex        =   19
      Top             =   3480
      Width           =   3255
   End
   Begin VB.TextBox Email 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5820
      MaxLength       =   64
      TabIndex        =   11
      Top             =   2025
      Width           =   3315
   End
   Begin VB.TextBox Suite 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   10
      TabIndex        =   5
      Top             =   1080
      Width           =   1935
   End
   Begin VB.TextBox Phone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   16
      TabIndex        =   10
      Top             =   2025
      Width           =   3135
   End
   Begin VB.TextBox City 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   30
      TabIndex        =   6
      Top             =   1560
      Width           =   3135
   End
   Begin VB.TextBox Address 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   30
      TabIndex        =   4
      Top             =   1080
      Width           =   7750
   End
   Begin VB.TextBox Zip 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7320
      MaxLength       =   10
      TabIndex        =   8
      Top             =   1560
      Width           =   1815
   End
   Begin VB.TextBox State 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5820
      MaxLength       =   2
      TabIndex        =   7
      Top             =   1560
      Width           =   495
   End
   Begin VB.TextBox TaxId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   32
      TabIndex        =   13
      Top             =   2505
      Width           =   3135
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3180
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Color 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   10
      TabIndex        =   18
      Top             =   3480
      Width           =   1695
   End
   Begin VB.TextBox ResourceName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1380
      MaxLength       =   16
      TabIndex        =   16
      Top             =   3000
      Width           =   2655
   End
   Begin VB.TextBox Cost 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4920
      MaxLength       =   2
      TabIndex        =   21
      Top             =   3960
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox ResourceType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5160
      MaxLength       =   1
      TabIndex        =   17
      Top             =   3000
      Width           =   495
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   975
      Left            =   10200
      TabIndex        =   27
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":03D1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5160
      TabIndex        =   29
      Top             =   7320
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":05B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPerm 
      Height          =   990
      Left            =   8400
      TabIndex        =   28
      Top             =   7320
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":0791
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHours 
      Height          =   990
      Left            =   6720
      TabIndex        =   56
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":0977
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMedLoc 
      Height          =   990
      Left            =   3480
      TabIndex        =   57
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":0B5F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdUpdox 
      Height          =   855
      Left            =   120
      TabIndex        =   70
      Top             =   6360
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":0D4C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmNewCrop 
      Height          =   855
      Left            =   4080
      TabIndex        =   71
      Top             =   6240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerResource.frx":0F33
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "e-Rx Validation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5880
      TabIndex        =   69
      Top             =   3960
      Width           =   2040
   End
   Begin VB.Label lblTaxy 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Taxonomy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8280
      TabIndex        =   66
      Top             =   3480
      Width           =   1665
   End
   Begin VB.Label lblNPI 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "NPI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9240
      TabIndex        =   64
      Top             =   3000
      Width           =   705
   End
   Begin VB.Label Label21 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Max. Appt/Hr"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   60
      Top             =   3960
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Calendar Breaks"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   59
      Top             =   4440
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Place Of Service"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   58
      Top             =   5880
      Width           =   2040
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Signature File"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   55
      Top             =   5400
      Width           =   2040
   End
   Begin VB.Label Label31 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   54
      Top             =   4920
      Width           =   2040
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4800
      TabIndex        =   53
      Top             =   600
      Width           =   1335
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   0
      TabIndex        =   52
      Top             =   600
      Width           =   1335
   End
   Begin VB.Label lblResourceId 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resource Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9240
      TabIndex        =   51
      Top             =   120
      Width           =   2655
   End
   Begin VB.Label lblLic 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lic"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9240
      TabIndex        =   50
      Top             =   2520
      Width           =   705
   End
   Begin VB.Label lblDea 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "DEA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9240
      TabIndex        =   49
      Top             =   2040
      Width           =   705
   End
   Begin VB.Label lblUpin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "UPIN"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9240
      TabIndex        =   48
      Top             =   1560
      Width           =   705
   End
   Begin VB.Label lblAffiliations 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Affiliations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   10320
      TabIndex        =   47
      Top             =   3960
      Width           =   1455
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pid"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9240
      TabIndex        =   46
      Top             =   600
      Width           =   615
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Practice"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4620
      TabIndex        =   45
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialty"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3360
      TabIndex        =   44
      Top             =   3480
      Width           =   1410
   End
   Begin VB.Label Label28 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "E-mail"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4620
      TabIndex        =   43
      Top             =   2025
      Width           =   1125
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Suite"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9240
      TabIndex        =   42
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   41
      Top             =   2025
      Width           =   1095
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   40
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6480
      TabIndex        =   39
      Top             =   1560
      Width           =   795
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   38
      Top             =   1560
      Width           =   1095
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4620
      TabIndex        =   37
      Top             =   1560
      Width           =   1140
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Tax Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   36
      Top             =   2505
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   35
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   34
      Top             =   3000
      Width           =   1095
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Overbook %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3120
      TabIndex        =   33
      Top             =   3960
      Visible         =   0   'False
      Width           =   1680
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Color"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   32
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4200
      TabIndex        =   31
      Top             =   3000
      Width           =   855
   End
End
Attribute VB_Name = "frmSchedulerResource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Public TheResourceId As Long
Private Perm As String
Private BreakDown As Integer
Private OrgOverLoad As Integer
Private sTxtPidText As String

Private Sub Breaks_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) Then
    If Not ((KeyAscii = 13) Or (KeyAscii = 10) Or (KeyAscii = 8)) Then
        frmEventMsgs.Header = "Valid Numbers [0123456789]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Breaks.SetFocus
        Breaks.Text = ""
        KeyAscii = 0
        SendKeys "{Home}"
    ElseIf (val(Breaks.Text) <> 0) And (val(Breaks.Text) <> 5) And (val(Breaks.Text) <> 10) And (val(Breaks.Text) <> 15) And (val(Breaks.Text) <> 30) And (val(Breaks.Text) <> 60) Then
        frmEventMsgs.Header = "Breaks Allowed [0, 5, 10, 15, 30, 60]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Breaks.SetFocus
        Breaks.Text = ""
        KeyAscii = 0
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub Breaks_Validate(Cancel As Boolean)
Call Breaks_KeyPress(13)
End Sub

Private Sub cmdHours_Click()
If (TheResourceId > 0) Then
    If (frmResourceTime.LoadTimeSheets(TheResourceId)) Then
        frmResourceTime.Show 1
    End If
End If
End Sub

Private Sub cmdInfo_Click()
If frmPhysicianSpi.LoadInfo Then
    frmPhysicianSpi.Show 1
End If
End Sub

Private Sub cmdMedLoc_Click()
If (frmMedicaidLocations.LoadMedLocList(TheResourceId)) Then
    frmMedicaidLocations.Show 1
End If
End Sub

Private Sub cmdPerm_Click()
frmPermissions.Permissions = Perm
frmPermissions.UserName = Trim(Description.Text)
frmPermissions.UserId = TheResourceId
If (frmPermissions.LoadPermission) Then
    frmPermissions.Show 1
    If (frmPermissions.CurrentAction = "") Then
        Perm = frmPermissions.Permissions
    End If
End If
End Sub

Private Sub cmdDelete_Click()
Dim ResId As Long
Dim Adt As Audit
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
ResId = TheResourceId
If (ApplTbl.ApplDeleteResource(TheResourceId)) Then
    Set Adt = New Audit
    Call Adt.PostAudit("D", Trim$(Str$(UserLogin.iId)), ResId, "Resources")
    Set Adt = Nothing
    Unload frmSchedulerResource
End If
Set ApplTbl = Nothing
End Sub

Private Sub cmdApply_Click()
Dim PostIt As Integer
Dim GoDirect As Boolean
Dim PracticeId As Long
Dim BillOn As Boolean
Dim Temp As String, TheAction As String
Dim ApplTbl As ApplicationTables
Dim SchedAppt As SchedulerAppointment
Dim ApplCal As ApplicationCalendar
Dim RetrieveResource As SchedulerResource
Dim IsDoctorValidated As Boolean
Dim DoctorValdMsg As String
If UCase(ResourceType) = "D" Then
    IsDoctorValidated = True
      DoctorValdMsg = "Please enter "
    If (Trim(txtFirst.Text) = "") Then
        IsDoctorValidated = False
        DoctorValdMsg = DoctorValdMsg + " FirstName"
    End If
    If (Trim(txtLast.Text) = "") Then
        IsDoctorValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "LastName"
        Else
            DoctorValdMsg = DoctorValdMsg + ", LastName"
        End If
    End If
    If (Trim(Address.Text) = "") Then
        IsDoctorValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "Address"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Address"
        End If
    End If
    If (Trim(City.Text) = "") Then
        IsDoctorValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "City"
        Else
            DoctorValdMsg = DoctorValdMsg + ", City"
        End If
    End If
    If (Trim(State.Text) = "") Then
        IsDoctorValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "State"
        Else
            DoctorValdMsg = DoctorValdMsg + ", State"
        End If
    End If
    If (Trim(txtNPI.Text) = "") Then
        IsDoctorValidated = False
         If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "NPI"
        Else
            DoctorValdMsg = DoctorValdMsg + ", NPI"
        End If
    End If
    If (Trim(txtTaxy.Text) = "") Then
        IsDoctorValidated = False
         If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "Taxonomy"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Taxonomy"
        End If
    End If

    If (Trim(Zip.Text) = "") Then
        IsDoctorValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "Zip"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Zip"
        End If
    ElseIf Len(Zip.Text) < 10 Then
         IsDoctorValidated = False
        If DoctorValdMsg = "Please enter " Then
             DoctorValdMsg = " Zipcode is too short"
        Else
            DoctorValdMsg = DoctorValdMsg + ". And Zipcode is too short"
        End If
    End If
    If Not IsDoctorValidated Then
        frmEventMsgs.Header = DoctorValdMsg
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SendKeys "{Home}"
        Exit Sub
    End If
End If

If (Trim(txtLast.Text) = "") And (InStrPS("RE", ResourceType) = 0) Then
    frmEventMsgs.Header = "Please Select a resource last name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtLast.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtFirst.Text) = "") And (InStrPS("RE", ResourceType) = 0) Then
    frmEventMsgs.Header = "Please Select a resource first name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtFirst.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(Description.Text) = "") Then
    frmEventMsgs.Header = "Please Select a resource name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Description.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(ResourceType.Text) = "") Then
    frmEventMsgs.Header = "Please Select a resource type"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    ResourceType.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtPid.Text) = "") And (InStrPS("RE", ResourceType) = 0) Then
    frmEventMsgs.Header = "Please Enter a Pid for the resource"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPid.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If

txtPid.Text = Trim(txtPid.Text)
If Not sTxtPidText = txtPid.Text Then
    Set RetrieveResource = New SchedulerResource
    RetrieveResource.ResourcePid = txtPid.Text
    If RetrieveResource.GetResourcebyPid Then
        frmEventMsgs.Header = "This Pid has already been assigned to another resource.  Please use another."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPid.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
End If

If (Trim(txtSrvCode.Text) = "") And (InStrPS("RE", ResourceType) <> 0) Then
    frmEventMsgs.Header = "Please Enter a Location Code"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSrvCode.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(ResourceName.Text) = "") Then
    frmEventMsgs.Header = "Please Enter a Code for the resource"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    ResourceName.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (lstPractice.ListIndex < 0) And (InStrPS("RE", ResourceType) <> 0) Then
    frmEventMsgs.Header = "Please Select a Practice"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    lstPractice.SetFocus
    Exit Sub
End If

If (ResourceType = "R") And (txtSrvCode.Text = "02") And Zip.Text = "" Then

    frmEventMsgs.Header = "Please select a ZipCode."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        'txtType.SetFocus
        SendKeys "{Home}"
        Exit Sub
    
    


Else
 
    If (ResourceType = "R") And (txtSrvCode.Text = "02") And Len(Zip.Text) < 10 Then
    
    frmEventMsgs.Header = "Zipcode is too short."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Zip.SetFocus
    Exit Sub
    
    End If
End If

BillOn = False
If (chkBill.Value = 1) Then
    BillOn = True
End If
GoDirect = False
If (chkGo.Value = 1) Then
    GoDirect = True
End If
PracticeId = 0
If (lstPractice.ListIndex >= 0) Then
    PracticeId = val(Trim(Mid(lstPractice.List(lstPractice.ListIndex), 70, 5)))
End If
'Check if any Invoices are already billed with old BillingProvider.
If ResourceType.Text = "D" Then
    If CheckIfInvoicesExists(TheResourceId, PracticeId) Then
        frmEventMsgs.InfoMessage "Cannot change provider. Invoices were already billed with current provider."
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If
Set ApplTbl = New ApplicationTables
Set SchedAppt = New SchedulerAppointment
Temp = ResourceName.Text
Call StripCharacters(Temp, "'")
ResourceName.Text = Temp

If ResourceType.Text = "Z" Then
ResourceType.Text = SchedAppt.FindAppointmentResourceType(TheResourceId)
End If

PostIt = ApplTbl.ApplPostResourceRec(TheResourceId, ResourceName.Text, ResourceType.Text, Description.Text, _
                                     txtLast.Text, txtFirst.Text, _
                                     Address.Text, Suite.Text, City.Text, State.Text, Zip.Text, Phone.Text, _
                                     TaxId.Text, Email.Text, Specialty.Text, Color.Text, Left(txtPOS.Text, 2), _
                                     Overload.Text, Cost.Text, txtPid.Text, Perm, txtSrvCode.Text, txtLic.Text, _
                                     txtUpin.Text, txtDea.Text, txtFile.Text, PracticeId, Breaks.Text, BillOn, _
                                     GoDirect, txtNPI.Text, txtTaxy.Text, txtErxVal.Text)
If (PostIt = -1) Then
    frmEventMsgs.Header = "Resource Exists"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
ElseIf (PostIt = -2) Then
    frmEventMsgs.Header = "Update not performed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    If (ResourceType.Text = "D") Then
        Dim ERPClient As New ClinicalIntegration.ERPServices
        ERPClient.Type = "doctor"
        ERPClient.ExternalId = TheResourceId
        ERPClient.DBObject = IdbConnection
        ERPClient.Push
    End If
End If
Set ApplTbl = Nothing
   


If (PostIt = 0) Then
    Unload frmSchedulerResource
End If
End Sub

Private Sub cmdBack_Click()
Unload frmSchedulerResource
End Sub

Public Function ResourceLoadDisplay(ResourceId As Long, TheResourceType As String) As Boolean
Dim PracticeId As Long
Dim i As Integer
Dim NPI As String, RSTaxy As String
Dim RBreak As String, BillOn As Boolean, GoDirect As Boolean
Dim RLast As String, RFirst As String, RFile As String, RPos As String
Dim RName As String, RType As String, RDesc As String, RAddr As String
Dim RSuite As String, RCity As String, RState As String, RZip As String
Dim RPhone As String, RTaxId As String, REMail As String, RSpec As String
Dim RColor As String, ROverl As String, RCost As String, RPid As String
Dim RSCode As String, RLic As String, RUpin As String, RDea As String
Dim eRXVal As String
Dim ApplTbl As ApplicationTables
ResourceLoadDisplay = True
cmdDelete.Visible = False
OrgOverLoad = 0
Perm = ""
BreakDown = SetBreakdown(0)
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
sTxtPidText = ""
cmdUpdox.Visible = False
If (ApplTbl.ApplGetResourceRec(ResourceId, RName, RType, RDesc, RLast, RFirst, RAddr, RSuite, _
                RCity, RState, RZip, RPhone, RTaxId, REMail, RSpec, RColor, RPos, _
                ROverl, RCost, RPid, Perm, RSCode, RLic, RUpin, RDea, RFile, PracticeId, RBreak, BillOn, GoDirect, NPI, RSTaxy, eRXVal)) Then
    TheResourceId = ResourceId
    ResourceName.Text = RName
    ResourceType.Text = RType
    If Trim(UCase(RType)) = "D" And ResourceId <> 0 Then
        cmdUpdox.Visible = True
    End If
    Description.Text = RDesc
    txtLast.Text = RLast
    txtFirst.Text = RFirst
    Address.Text = RAddr
    Suite.Text = RSuite
    City.Text = RCity
    State.Text = RState
    Zip.Text = RZip
    Phone.Text = RPhone
    TaxId.Text = RTaxId
    Email.Text = REMail
    Specialty.Text = RSpec
    Color.Text = RColor
    Overload.Text = ROverl
    Breaks.Text = RBreak
    OrgOverLoad = ROverl
    Cost.Text = RCost
    txtPid.Text = RPid
    sTxtPidText = RPid
    txtSrvCode.Text = RSCode
    txtUpin.Text = RUpin
    txtLic.Text = RLic
    txtDea.Text = RDea
    txtFile.Text = RFile
        txtPOS.Text = RPos
        chkBill.Value = 0
    If (BillOn) Then
        chkBill.Value = 1
    End If
    chkGo.Value = 0
    If (GoDirect) Then
        chkGo.Value = 1
    End If
    txtNPI.Text = NPI
    txtTaxy.Text = RSTaxy
    txtErxVal.Text = eRXVal
    lblResourceId.Caption = "Resource Id:" + Str(ResourceId)
Else
    TheResourceId = 0
    If (UCase(Trim(TheResourceType)) = "R") Then
        ResourceType.Text = "R"
    End If
End If
lstPractice.ListIndex = -1
For i = 0 To lstPractice.ListCount - 1
    If (PracticeId = val(Mid(lstPractice.List(i), 70, 5))) Then
        lstPractice.ListIndex = i
        Exit For
    End If
Next i
If (lstPractice.ListIndex < 0) And (lstPractice.ListCount > 0) Then
    lstPractice.ListIndex = 0
End If
Set ApplTbl.lstBox = lstAff
Call ApplTbl.ApplLoadAffiliations(ResourceId, "R")
If (TheResourceType = "R") Or (TheResourceType = "E") Then
    Label8.Visible = True
        txtPOS.Visible = True
        Label18.Visible = True
    lstPractice.Visible = True
    Label31.Visible = True
    txtSrvCode.Visible = True
    lblUpin.Visible = False
    txtUpin.Visible = False
    Label22.Visible = False
    txtPid.Visible = False
    Label20.Visible = False
    TaxId.Visible = False
    Label28.Visible = False
    Email.Visible = False
    Label17.Visible = False
    Specialty.Visible = False
    Label9.Visible = False
    Color.Visible = False
    ResourceType.Enabled = False
    lblDea.Visible = False
    txtDea.Visible = False
    lblLic.Visible = False
    txtLic.Visible = False
    Label4.Visible = False
    txtLast.Visible = False
    Label5.Visible = False
    txtFirst.Visible = False
    Label6.Visible = False
    txtFile.Visible = False
    lblTaxy.Visible = False
    txtTaxy.Visible = False
    cmdPerm.Visible = False
    cmdInfo.Visible = False
Else
    lblNPI.Visible = True
    txtNPI.Visible = True
    Label4.Visible = True
    txtLast.Visible = True
    Label5.Visible = True
    txtFirst.Visible = True
    lblUpin.Visible = True
    txtUpin.Visible = True
    lblDea.Visible = True
    txtDea.Visible = True
    lblLic.Visible = True
    txtLic.Visible = True
    Label31.Visible = False
    txtSrvCode.Visible = False
    Label8.Visible = False
        txtPOS.Visible = False
        Label6.Visible = True
    txtFile.Visible = True
    lblTaxy.Visible = True
    txtTaxy.Visible = True
    cmdPerm.Visible = True
    cmdInfo.Visible = True
End If
Set ApplTbl = Nothing
End Function

Private Sub cmdUpdox_Click()
    If TheResourceId = 0 Then
        Exit Sub
    End If
    Dim ClinicalManager As New ClinicalIntegration.frmResourceUpdoxRegistration
    ClinicalManager.ResourceId = CStr(TheResourceId)
    ClinicalManager.DBObject = IdbConnection
    ClinicalManager.ShowDialog
End Sub

Private Sub cmNewCrop_Click()
    If TheResourceId = 0 Then
        Exit Sub
    End If
    Dim ImportPHI As New ClinicalIntegration.frmImportPHI
    ImportPHI.ResourceId = CStr(TheResourceId)
    ImportPHI.DBObject = IdbConnection
    ImportPHI.FolderPath = PinPointDirectory
    ImportPHI.ShowDialog
    ImportPHI.Close
    ImportPHI.Dispose


End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmSchedulerResource.BorderStyle = 1
    frmSchedulerResource.ClipControls = True
    frmSchedulerResource.Caption = Mid(frmSchedulerResource.Name, 4, Len(frmSchedulerResource.Name) - 3)
    frmSchedulerResource.AutoRedraw = True
    frmSchedulerResource.Refresh
End If
End Sub

Private Sub Phone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Phone, "P")
End If
End Sub

Private Sub Phone_Validate(Cancel As Boolean)
    Call UpdateDisplay(Phone, "P")
End Sub

Private Sub txtFile_Click()
Call txtFile_KeyPress(13)
End Sub

Private Sub txtFile_KeyPress(KeyAscii As Integer)
Dim strTemp As String
strTemp = OpenSingleFile(CommonDialog1, PinPointDirectory & "Templates\")
If (strTemp <> "") Then
    txtFile.Text = strTemp
End If
KeyAscii = 0
End Sub

Private Sub txtNPI_KeyPress(KeyAscii As Integer)
Dim i As Integer
If (KeyAscii = 13) Then
    If (Len(txtNPI.Text) <> 10) And (Len(txtNPI.Text) <> 0) Then
        frmEventMsgs.Header = "Invalid NPI. Must be all numeric and 10 characters in length."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtNPI.Text = ""
        txtNPI.SetFocus
    Else
        For i = 1 To Len(txtNPI.Text)
            If Not (IsNumeric(Mid(txtNPI.Text, i, 1))) Then
                frmEventMsgs.Header = "Invalid NPI. Must be all numeric and 10 characters in length."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                txtNPI.Text = ""
                txtNPI.SetFocus
                Exit For
            End If
        Next i
    End If
End If
End Sub

Private Sub txtNPI_Validate(Cancel As Boolean)
Call txtNPI_KeyPress(13)
End Sub

Private Sub txtPOS_Click()
Call txtPOS_KeyPress(13)
End Sub

Private Sub txtPOS_KeyPress(KeyAscii As Integer)
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PLACEOFSERVICE")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
        txtPOS.Text = UCase(frmSelectDialogue.Selection)
    End If
End Sub

Private Sub txtSrvCode_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ServiceCode")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtSrvCode.Text = UCase(Left(frmSelectDialogue.Selection, 2))
End If
End Sub

Private Sub txtSrvCode_KeyPress(KeyAscii As Integer)
Call txtSrvCode_Click
KeyAscii = 0
End Sub

Private Sub ResourceType_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ResourceType")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (UCase(Left(frmSelectDialogue.Selection, 1)) <> "R") And (UCase(Left(frmSelectDialogue.Selection, 1)) <> "E") Then
        ResourceType.Text = UCase(Left(frmSelectDialogue.Selection, 1))
    Else
        frmEventMsgs.Header = "Cannot be Room or Equipment"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub ResourceType_KeyPress(KeyAscii As Integer)
Call ResourceType_Click
KeyAscii = 0
End Sub

Private Sub Cost_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    If Not ((KeyAscii = 13) Or (KeyAscii = 10) Or (KeyAscii = 8)) Then
        frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Cost.SetFocus
        SendKeys "{Home}{End}"
        Cost.Text = ""
        KeyAscii = 0
    End If
End If
End Sub

Private Sub OverLoad_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) Then
    If Not ((KeyAscii = 13) Or (KeyAscii = 10) Or (KeyAscii = 8)) Then
        frmEventMsgs.Header = "Valid Numbers [0123456789]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Overload.SetFocus
        Overload.Text = ""
        KeyAscii = 0
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub Overload_Validate(Cancel As Boolean)
Call OverLoad_KeyPress(13)
End Sub

Private Sub Color_Click()
CommonDialog1.CancelError = True
On Error GoTo UI_ErrorHandler
    CommonDialog1.flags = &H1&
    CommonDialog1.ShowColor
Color.Text = Trim(Str(CommonDialog1.Color))
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub Color_KeyPress(KeyAscii As Integer)
Call Color_Click
KeyAscii = 0
End Sub

Private Sub Specialty_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("Specialty")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Specialty.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub Specialty_KeyPress(KeyAscii As Integer)
Call Specialty_Click
KeyAscii = 0
End Sub

Private Sub lstAff_Click()
Dim Temp As String
Dim ApplTbl As ApplicationTables
If (TheResourceId < 1) Then
    frmEventMsgs.Header = "Resource must be saved"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (lstAff.ListIndex = 0) Then
    frmAffiliations.AffiliationId = 0
    frmAffiliations.ResourceId = TheResourceId
    frmAffiliations.ResourceType = "R"
    frmAffiliations.OverrideOn = True
    frmAffiliations.RemoveOn = False
    If (frmAffiliations.LoadAffiliationDisplay) Then
        frmAffiliations.Show 1
        If (frmAffiliations.AffiliationId <> 0) Then
            Set ApplTbl = New ApplicationTables
            Set ApplTbl.lstBox = lstAff
            Call ApplTbl.ApplLoadAffiliations(TheResourceId, "R")
            Set ApplTbl = Nothing
        End If
    End If
ElseIf (lstAff.ListIndex > 0) Then
    frmAffiliations.AffiliationId = val(Trim(Mid(lstAff.List(lstAff.ListIndex), 76, 5)))
    frmAffiliations.ResourceId = TheResourceId
    frmAffiliations.ResourceType = "R"
    frmAffiliations.OverrideOn = True
    frmAffiliations.RemoveOn = CheckConfigCollection("REMOVEACTIVE") = "T"
    If (frmAffiliations.LoadAffiliationDisplay) Then
        frmAffiliations.Show 1
        If (frmAffiliations.AffiliationId <> 0) Then
            Set ApplTbl = New ApplicationTables
            Set ApplTbl.lstBox = lstAff
            Call ApplTbl.ApplLoadAffiliations(TheResourceId, "R")
            Set ApplTbl = Nothing
        End If
    End If
End If
End Sub

Private Sub txtUpin_KeyPress(KeyAscii As Integer)
Dim ApplTemp As ApplicationTemplates
If (KeyAscii = 13) Then
    If (Len(txtUpin.Text) <> 6) And (Len(txtUpin.Text) <> 0) Then
        frmEventMsgs.Header = "Invalid Upin"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtUpin.Text = ""
        txtUpin.SetFocus
    ElseIf (Trim(txtUpin.Text) <> "") Then
        Set ApplTemp = New ApplicationTemplates
        If Not (ApplTemp.ApplIsUPinFormatValid(txtUpin.Text)) Then
            frmEventMsgs.Header = "Invalid Upin"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtUpin.Text = ""
            txtUpin.SetFocus
        End If
        Set ApplTemp = Nothing
    End If
End If
End Sub

Private Sub txtUpin_Validate(Cancel As Boolean)
Call txtUpin_KeyPress(13)
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub

Private Sub Zip_KeyPress(KeyAscii As Integer)

    If (KeyAscii = 13) Or (KeyAscii = 10) Then
        Call UpdateDisplay(Zip, "Z")
    End If
End Sub

Private Sub Zip_Validate(Cancel As Boolean)
    Call UpdateDisplay(Zip, "Z")
End Sub

Private Function CheckIfInvoicesExists(ResourceId As Long, NewPracticeId As Long) As Boolean
On Error GoTo lCheckInvoicesExists_Error

Dim sqlStr As String
Dim RS As Recordset
Dim RetrieveResource  As SchedulerResource
Dim OldPracticeId As Long
CheckIfInvoicesExists = False
Set RetrieveResource = New SchedulerResource
If (ResourceId > 0) Then
    RetrieveResource.ResourceId = ResourceId
    If (RetrieveResource.RetrieveSchedulerResource) Then
        OldPracticeId = RetrieveResource.PracticeId
    End If
End If
If NewPracticeId = OldPracticeId Then
    CheckIfInvoicesExists = False
    Exit Function
End If
sqlStr = " SELECT COUNT(*) FROM model.Invoices i INNER JOIN model.Providers p ON i.BillingProviderId=p.Id WHERE p.UserId=" & ResourceId & " AND p.ServiceLocationId =" & OldPracticeId
Set RS = GetRS(sqlStr)
If RS Is Nothing Then CheckIfInvoicesExists = False
If RS(0) > 0 Then CheckIfInvoicesExists = True
Exit Function
lCheckInvoicesExists_Error:
    LogError "SchedulerResources.frm", "CheckInvoicesExists", Err, Err.Description
End Function
