VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MsFlxGrd.ocx"
Begin VB.Form FApptHistory 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8490
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11880
   FillColor       =   &H0077742D&
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8490
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin MSFlexGridLib.MSFlexGrid flxChangeList 
      Height          =   5775
      Left            =   240
      TabIndex        =   6
      Top             =   1440
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   10186
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorBkg    =   7828525
      ScrollBars      =   2
      AllowUserResizing=   1
      BorderStyle     =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   240
      TabIndex        =   7
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptHistory.frx":0000
   End
   Begin VB.Label lblApptId 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "ApptId"
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   11100
      TabIndex        =   5
      Top             =   1080
      Width           =   555
   End
   Begin VB.Label lblApptIdTitle 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Appointment ID #"
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8400
      TabIndex        =   4
      Top             =   1080
      Width           =   1500
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment History"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   360
      Left            =   4545
      TabIndex        =   0
      Top             =   240
      Width           =   2805
   End
   Begin VB.Label lblPatId 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "PatId"
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   11205
      TabIndex        =   3
      Top             =   720
      Width           =   450
   End
   Begin VB.Label lblPatIdTitle 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Patient ID #"
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8880
      TabIndex        =   2
      Top             =   720
      Width           =   1020
   End
   Begin VB.Label lblLastName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   240
      TabIndex        =   1
      Top             =   840
      Width           =   4260
   End
End
Attribute VB_Name = "FApptHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function LoadHistory(ApptId As Long) As Boolean
Dim i As Long
Dim sLocation As String
Dim sAppDate As String
Dim sAppTime As String
Dim sChangeDate As String
Dim sChangeTime As String
Dim myPatient As Patient
Dim appointment As SchedulerAppointment
Dim AppType As SchedulerAppointmentType
Dim ApptTrack As CAppointmentTrack
Dim Doctor As SchedulerResource
Dim Location As ApplicationAIList
Dim User As SchedulerResource
Dim MyDate As ManagedDate

    On Error GoTo lLoadHistory_Error
    ResetChangeList
    Set appointment = New SchedulerAppointment
    appointment.AppointmentId = ApptId
    lblApptId.Caption = Str$(ApptId)
    If appointment.RetrieveSchedulerAppointment Then
        Set myPatient = New Patient
        myPatient.PatientId = appointment.AppointmentPatientId
        lblPatId.Caption = Str$(appointment.AppointmentPatientId)
        If myPatient.RetrievePatient Then
            lblLastName.Caption = Trim(myPatient.LastName) + ", " + Trim(myPatient.FirstName)
        End If
        Set ApptTrack = New CAppointmentTrack
        ApptTrack.AppointmentId = appointment.AppointmentId
        If ApptTrack.FindApptTrack Then
            i = 1
            With flxChangeList
                While ApptTrack.SelectApptTrack(i)
                    Set User = New SchedulerResource
                    User.ResourceId = ApptTrack.UserId
                    User.RetrieveSchedulerResource
                    Set MyDate = New ManagedDate
                    MyDate.ExposedDate = ApptTrack.ApptTrackDate
                    If MyDate.ConvertManagedDateToDisplayDate Then
                        sChangeDate = MyDate.ExposedDate
                    End If
                    ConvertMinutesToTime ApptTrack.ApptTrackTime, sChangeTime
                    MyDate.ExposedDate = ApptTrack.ApptDate
                    If MyDate.ConvertManagedDateToDisplayDate Then
                        sAppDate = MyDate.ExposedDate
                    End If
                    Set MyDate = Nothing
                    ConvertMinutesToTime ApptTrack.ApptTime, sAppTime
                    Set Doctor = New SchedulerResource
                    Doctor.ResourceId = ApptTrack.ApptDoc
                    Doctor.RetrieveSchedulerResource
                    Set Location = New ApplicationAIList
                    Location.GetLocation ApptTrack.ApptLoc, sLocation
                    Set Location = Nothing
                    Set AppType = New SchedulerAppointmentType
                    AppType.AppointmentTypeId = ApptTrack.ApptType
                    AppType.RetrieveSchedulerAppointmentType
                    .Rows = i + 1
                    .TextMatrix(i, 0) = sChangeDate
                    .TextMatrix(i, 1) = sChangeTime
                    .TextMatrix(i, 2) = User.ResourceName
                    .TextMatrix(i, 3) = ApptTrack.Action
                    '.TextMatrix(i, 4) = Appointment.AppointmentComments
                    .TextMatrix(i, 4) = mySplit(ApptTrack.Comment, 0)
                    .TextMatrix(i, 5) = mySplit(ApptTrack.Comment, 1)
                    .TextMatrix(i, 6) = sAppDate
                    .TextMatrix(i, 7) = sAppTime
                    .TextMatrix(i, 8) = Doctor.ResourceName
                    .TextMatrix(i, 9) = sLocation
                    .TextMatrix(i, 10) = AppType.AppointmentType
                    i = i + 1
                    Set AppType = Nothing
                    Set Doctor = Nothing
                    Set User = Nothing
                Wend
            End With
        End If
        Set ApptTrack = Nothing
        Set myPatient = Nothing
    End If
    Set appointment = Nothing

    Exit Function

lLoadHistory_Error:

    LogError "FApptHistory", "LoadHistory", Err, Err.Description, , , ApptId
End Function

Private Function mySplit(fStr As String, ind As Integer) As String
Dim s As Integer
s = InStrPS(1, fStr, "]")

If ind = 0 Then
    If s > 0 Then
        mySplit = Mid(fStr, 2, s - 2)
    End If
Else
    If s > 0 Then
        If Len(fStr) > s Then
            mySplit = Mid(fStr, s + 1)
        End If
    Else
        mySplit = fStr
    End If
End If

End Function


Private Sub ResetChangeList()
    With flxChangeList
        .Clear
        .Rows = 2
        .Cols = 11
        .FormatString = ("<Date|<Time|<User||<Reason|<Comment|<Appt Date|<Appt Time|<Doctor|<Location|<Appt Type")
        .ColWidth(0) = 950
        .ColWidth(1) = 850
        .ColWidth(2) = 1000
        .ColWidth(3) = 250
        .ColWidth(4) = 1750
        .ColWidth(5) = 1750
        .ColWidth(6) = 950
        .ColWidth(7) = 850
        .ColWidth(8) = 1000
        .ColWidth(9) = 1000
        .ColWidth(10) = 1000
    End With
End Sub

Private Sub cmdHome_Click()
    Unload FApptHistory
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

