VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCollectionSetup 
   BackColor       =   &H0077742D&
   Caption         =   "Collection Setup"
   ClientHeight    =   6000
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6525
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   6000
   ScaleWidth      =   6525
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtBad 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   1
      TabIndex        =   13
      Top             =   3120
      Width           =   615
   End
   Begin VB.TextBox txtSend 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   1
      TabIndex        =   5
      Top             =   2640
      Width           =   615
   End
   Begin VB.TextBox txtKeep 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   1
      TabIndex        =   4
      Top             =   2160
      Width           =   615
   End
   Begin VB.TextBox txtExc 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   1
      TabIndex        =   3
      Top             =   1680
      Width           =   615
   End
   Begin VB.TextBox txtDay 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   4
      TabIndex        =   2
      Top             =   1200
      Width           =   615
   End
   Begin VB.TextBox txtMon 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   1
      TabIndex        =   1
      Top             =   720
      Width           =   615
   End
   Begin VB.TextBox txtMin 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      MaxLength       =   4
      TabIndex        =   0
      Top             =   240
      Width           =   615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   870
      Left            =   1440
      TabIndex        =   6
      Top             =   4800
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CollectionSetup.frx":0000
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Bad Debt Write Off Code:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   14
      Top             =   3120
      Width           =   2040
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Send How Many Letters [1-5]:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   12
      Top             =   2640
      Width           =   2385
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Keep Sending Statements Until Aging Bucket [0-5]:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   11
      Top             =   2160
      Width           =   4140
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Exclude From Standard A/R after Aging Bucket [0-5]:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   10
      Top             =   1680
      Width           =   4215
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Number of Days Since Last Statement:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   9
      Top             =   1200
      Width           =   3180
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Number of Monthly Statements Sent [1-5]:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   8
      Top             =   720
      Width           =   3390
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Minimum Aging from Patient Bill Date:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   240
      TabIndex        =   7
      Top             =   240
      Width           =   3090
   End
End
Attribute VB_Name = "frmCollectionSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdDone_Click()
Dim i As Long
Dim RetCon As PracticeConfigureInterface
Set RetCon = New PracticeConfigureInterface
RetCon.ConfigureInterfaceField = ""
If (RetCon.FindConfigure > 0) Then
    i = 1
    While (RetCon.SelectConfigure(i))
        If (Trim(RetCon.ConfigureInterfaceField) = "MONTHLYMINIMUMAGE") Then
            RetCon.ConfigureInterfaceValue = Trim(txtMin.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MONTHLYALREADYSENT") Then
            RetCon.ConfigureInterfaceValue = Trim(txtMon.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MONTHLYDAYSLASTSENT") Then
            RetCon.ConfigureInterfaceValue = Trim(txtDay.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MONTHLYEXCLUDEAFTER") Then
            RetCon.ConfigureInterfaceValue = Trim(txtExc.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MONTHLYKEEPSENDING") Then
            RetCon.ConfigureInterfaceValue = Trim(txtKeep.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MONTHLYSENDLETTER") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSend.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "BADDEBTWO") Then
            RetCon.ConfigureInterfaceValue = Trim(txtBad.Text)
            Call RetCon.ApplyConfigure
        End If
        i = i + 1
    Wend
End If
Set RetCon = Nothing
LoadConfigCollection
SetGlobalDefaults
Unload frmCollectionSetup
End Sub

Private Sub Form_Load()
Call LoadForm
End Sub

Private Sub LoadForm()
txtMin.Text = CheckConfigCollection("MONTHLYMINIMUMAGE")
txtMon.Text = CheckConfigCollection("MONTHLYALREADYSENT")
txtDay.Text = CheckConfigCollection("MONTHLYDAYSLASTSENT")
txtExc.Text = CheckConfigCollection("MONTHLYEXCLUDEAFTER")
txtKeep.Text = CheckConfigCollection("MONTHLYKEEPSENDING")
txtSend.Text = CheckConfigCollection("MONTHLYSENDLETTER")
txtBad.Text = CheckConfigCollection("BADDEBTWO")
End Sub

Private Sub txtMin_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) Then
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 32) And (KeyAscii <> 0) And (KeyAscii <> vbKeyBack) Then
        frmEventMsgs.Header = "Invalid number."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtMin.Text = ""
        txtMin.SetFocus
    End If
Else
    If ((val(Trim(txtMin.Text)) < 1)) Then
        frmEventMsgs.Header = "Value must be greater than 0."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtMin.Text = ""
        txtMin.SetFocus
    End If
End If
End Sub

Private Sub txtMin_Validate(Cancel As Boolean)
Call txtMin_KeyPress(13)
End Sub

Private Sub txtKeep_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) Then
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 32) And (KeyAscii <> 0) And (KeyAscii <> vbKeyBack) Then
        frmEventMsgs.Header = "Invalid number."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtKeep.Text = ""
        txtKeep.SetFocus
    End If
Else
    If ((val(Trim(txtKeep.Text)) < 1) And (val(Trim(txtKeep.Text)) > 6)) Then
        frmEventMsgs.Header = "Value must be [0-5]."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtKeep.Text = ""
        txtKeep.SetFocus
    End If
End If
End Sub

Private Sub txtKeep_Validate(Cancel As Boolean)
Call txtKeep_KeyPress(13)
End Sub

Private Sub txtDay_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) Then
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 32) And (KeyAscii <> 0) And (KeyAscii <> vbKeyBack) Then
        frmEventMsgs.Header = "Invalid number."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDay.Text = ""
        txtDay.SetFocus
    End If
Else
    If ((val(Trim(txtDay.Text)) < 1)) Then
        frmEventMsgs.Header = "Value must be greater than 0."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDay.Text = ""
        txtDay.SetFocus
    End If
End If
End Sub

Private Sub txtDay_Validate(Cancel As Boolean)
Call txtDay_KeyPress(13)
End Sub

Private Sub txtMon_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) Then
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 32) And (KeyAscii <> 0) And (KeyAscii <> vbKeyBack) Then
        frmEventMsgs.Header = "Invalid number."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtMon.Text = ""
        txtMon.SetFocus
    End If
Else
    If ((val(Trim(txtMon.Text)) < 1) Or (val(Trim(txtMon.Text)) > 5)) Then
        frmEventMsgs.Header = "Value must be [1-5]."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtMon.Text = ""
        txtMon.SetFocus
    End If
End If
End Sub

Private Sub txtMon_Validate(Cancel As Boolean)
Call txtMon_KeyPress(13)
End Sub

Private Sub txtExc_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) Then
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 32) And (KeyAscii <> 0) And (KeyAscii <> vbKeyBack) Then
        frmEventMsgs.Header = "Invalid number."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtExc.Text = ""
        txtExc.SetFocus
    End If
Else
    If ((val(Trim(txtExc.Text)) < 0) Or (val(Trim(txtExc.Text)) > 5)) Then
        frmEventMsgs.Header = "Value must be [5-5]."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtExc.Text = ""
        txtExc.SetFocus
    End If
End If
End Sub

Private Sub txtExc_Validate(Cancel As Boolean)
Call txtExc_KeyPress(13)
End Sub

Private Sub txtSend_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 13) Then
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 32) And (KeyAscii <> 0) And (KeyAscii <> vbKeyBack) Then
        frmEventMsgs.Header = "Invalid number."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSend.Text = ""
        txtSend.SetFocus
    End If
Else
    If ((val(Trim(txtSend.Text)) < 1) Or (val(Trim(txtSend.Text)) > 5)) Then
        frmEventMsgs.Header = "Value must be [1-5]."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSend.Text = ""
        txtSend.SetFocus
    End If
End If
End Sub
Private Sub txtSend_Validate(Cancel As Boolean)
Call txtSend_KeyPress(13)
End Sub
Public Sub FrmClose()
Unload Me

End Sub

