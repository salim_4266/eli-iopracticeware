VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmPractice 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtTax 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8280
      MaxLength       =   16
      TabIndex        =   65
      Top             =   3720
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.TextBox txtNPI 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7800
      MaxLength       =   10
      TabIndex        =   64
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkBill 
      BackColor       =   &H0077742D&
      Caption         =   "Billing Practice"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9840
      TabIndex        =   62
      Top             =   2400
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox txtLocRef 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4920
      MaxLength       =   16
      TabIndex        =   2
      Top             =   870
      Visible         =   0   'False
      Width           =   1800
   End
   Begin VB.ListBox lstAff 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1020
      ItemData        =   "Practice.frx":0000
      Left            =   6840
      List            =   "Practice.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   58
      Top             =   720
      Width           =   5055
   End
   Begin VB.TextBox txtType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   1
      TabIndex        =   1
      Top             =   885
      Visible         =   0   'False
      Width           =   600
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   31
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Practice.frx":0004
   End
   Begin VB.TextBox Suite 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7800
      MaxLength       =   35
      TabIndex        =   5
      Top             =   1845
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.TextBox OtherId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4800
      MaxLength       =   32
      TabIndex        =   14
      Top             =   3765
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.TextBox TaxId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   32
      TabIndex        =   13
      Top             =   3765
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.TextBox Email 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   64
      TabIndex        =   12
      Top             =   3285
      Visible         =   0   'False
      Width           =   5280
   End
   Begin VB.TextBox Fax 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7800
      MaxLength       =   28
      TabIndex        =   11
      Top             =   2805
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox OtherPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4800
      MaxLength       =   28
      TabIndex        =   10
      Top             =   2805
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.TextBox State 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6240
      MaxLength       =   2
      TabIndex        =   7
      Top             =   2325
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox Zip 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7800
      MaxLength       =   10
      TabIndex        =   8
      Top             =   2325
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox PracticeName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   64
      TabIndex        =   3
      Top             =   1365
      Visible         =   0   'False
      Width           =   5295
   End
   Begin VB.TextBox Address 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   35
      TabIndex        =   4
      Top             =   1845
      Visible         =   0   'False
      Width           =   5295
   End
   Begin VB.TextBox City 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   35
      TabIndex        =   6
      Top             =   2325
      Visible         =   0   'False
      Width           =   3240
   End
   Begin VB.TextBox Phone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   28
      TabIndex        =   9
      Top             =   2805
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.ListBox lstStartTime1 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01E3
      Left            =   840
      List            =   "Practice.frx":01E5
      TabIndex        =   15
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime1 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01E7
      Left            =   840
      List            =   "Practice.frx":01E9
      TabIndex        =   16
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStartTime7 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01EB
      Left            =   7920
      List            =   "Practice.frx":01ED
      TabIndex        =   27
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStartTime6 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01EF
      Left            =   6735
      List            =   "Practice.frx":01F1
      TabIndex        =   25
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStartTime5 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01F3
      Left            =   5550
      List            =   "Practice.frx":01F5
      TabIndex        =   23
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStartTime4 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01F7
      Left            =   4380
      List            =   "Practice.frx":01F9
      TabIndex        =   21
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStartTime2 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01FB
      Left            =   2010
      List            =   "Practice.frx":01FD
      TabIndex        =   17
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStartTime3 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":01FF
      Left            =   3195
      List            =   "Practice.frx":0201
      TabIndex        =   19
      Top             =   5040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime7 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":0203
      Left            =   7920
      List            =   "Practice.frx":0205
      TabIndex        =   28
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime6 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":0207
      Left            =   6735
      List            =   "Practice.frx":0209
      TabIndex        =   26
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime5 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":020B
      Left            =   5550
      List            =   "Practice.frx":020D
      TabIndex        =   24
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime4 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":020F
      Left            =   4380
      List            =   "Practice.frx":0211
      TabIndex        =   22
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime3 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":0213
      Left            =   3195
      List            =   "Practice.frx":0215
      TabIndex        =   20
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstEndTime2 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      ItemData        =   "Practice.frx":0217
      Left            =   2010
      List            =   "Practice.frx":0219
      TabIndex        =   18
      Top             =   6300
      Visible         =   0   'False
      Width           =   1095
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2775
      Left            =   9120
      TabIndex        =   29
      Top             =   4680
      Visible         =   0   'False
      Width           =   2775
      _Version        =   65537
      _ExtentX        =   4895
      _ExtentY        =   4895
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      BevelColorFace  =   14285823
      BevelColorShadow=   15794174
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      SelectionType   =   1
      CaptionAlignmentYear=   3
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10080
      TabIndex        =   30
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Practice.frx":021B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5280
      TabIndex        =   59
      Top             =   7560
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Practice.frx":03FA
   End
   Begin VB.ListBox lstPractice 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "Practice.frx":05DB
      Left            =   1455
      List            =   "Practice.frx":05DD
      TabIndex        =   0
      Top             =   210
      Width           =   6360
   End
   Begin VB.Label lblTax 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Taxonomy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      TabIndex        =   66
      Top             =   3720
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lblNPI 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "NPI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      TabIndex        =   63
      Top             =   3240
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location Reference"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   2160
      TabIndex        =   61
      Top             =   885
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label lblUpdateHoliday 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Updating Holidays"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7080
      TabIndex        =   60
      Top             =   4320
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.Label lblAffiliations 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Affiliations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      TabIndex        =   57
      Top             =   240
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   56
      Top             =   885
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Suite"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      TabIndex        =   55
      Top             =   1845
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblPractice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Practice"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   54
      Top             =   210
      Width           =   1095
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Other Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3600
      TabIndex        =   53
      Top             =   3765
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Tax Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   52
      Top             =   3765
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "E-mail"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   51
      Top             =   3285
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      TabIndex        =   50
      Top             =   2805
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3600
      TabIndex        =   49
      Top             =   2805
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4800
      TabIndex        =   48
      Top             =   2325
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   47
      Top             =   2325
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      TabIndex        =   46
      Top             =   2325
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   45
      Top             =   1365
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   44
      Top             =   1845
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label21 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   43
      Top             =   2805
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Start Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   645
      Left            =   120
      TabIndex        =   42
      Top             =   5040
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   645
      Left            =   120
      TabIndex        =   41
      Top             =   6300
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sun"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   840
      TabIndex        =   40
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Mon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   2010
      TabIndex        =   39
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Tues"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   3195
      TabIndex        =   38
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Wed"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   4380
      TabIndex        =   37
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Thurs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   5550
      TabIndex        =   36
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Fri"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   6735
      TabIndex        =   35
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sat"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   7920
      TabIndex        =   34
      Top             =   4680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Practice Hours of Operation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   2850
      TabIndex        =   33
      Top             =   4320
      Visible         =   0   'False
      Width           =   4140
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H008D312C&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Holidays"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   9720
      TabIndex        =   32
      Top             =   4320
      Visible         =   0   'False
      Width           =   1485
   End
End
Attribute VB_Name = "frmPractice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PracticeId As Long
Private RemoveOn As Boolean
Private BreakDown As Integer

Private Sub SetControls(DisplayType As Boolean)
    lblAffiliations.Visible = DisplayType
    lstAff.Visible = DisplayType
    lstPractice.Visible = Not DisplayType
    cmdApply.Visible = DisplayType
    PracticeName.Visible = DisplayType
    txtLocRef.Visible = DisplayType
    Address.Visible = DisplayType
    Suite.Visible = DisplayType
    City.Visible = DisplayType
    State.Visible = DisplayType
    Zip.Visible = DisplayType
    OtherId.Visible = DisplayType
    TaxId.Visible = DisplayType
    Email.Visible = DisplayType
    Fax.Visible = DisplayType
    Phone.Visible = DisplayType
    OtherPhone.Visible = DisplayType
    txtType.Visible = DisplayType
    chkBill.Visible = DisplayType
    
    DisplayType = False     'DisplayType is set to False to fix Bug 4791
    lstStartTime1.Visible = DisplayType
    lstStartTime2.Visible = DisplayType
    lstStartTime3.Visible = DisplayType
    lstStartTime4.Visible = DisplayType
    lstStartTime5.Visible = DisplayType
    lstStartTime6.Visible = DisplayType
    lstStartTime7.Visible = DisplayType
    lstEndTime1.Visible = DisplayType
    lstEndTime2.Visible = DisplayType
    lstEndTime3.Visible = DisplayType
    lstEndTime4.Visible = DisplayType
    lstEndTime5.Visible = DisplayType
    lstEndTime6.Visible = DisplayType
    lstEndTime7.Visible = DisplayType
    Label4.Visible = DisplayType
    Label5.Visible = DisplayType
    Label6.Visible = DisplayType
    Label8.Visible = DisplayType
    Label9.Visible = DisplayType
    Label10.Visible = DisplayType
    Label11.Visible = DisplayType
    Label12.Visible = DisplayType
    Label13.Visible = DisplayType
    Label14.Visible = DisplayType
    Label15.Visible = DisplayType
    Label16.Visible = DisplayType
    SSMonth1.Visible = DisplayType
    DisplayType = True
    
    Label1.Visible = DisplayType
    Label2.Visible = DisplayType
    Label3.Visible = DisplayType
    Label7.Visible = DisplayType
    Label15.Caption = "Practice Hours of Operation"
    Label17.Visible = DisplayType
    Label18.Visible = DisplayType
    Label19.Visible = DisplayType
    Label20.Visible = DisplayType
    Label21.Visible = DisplayType
    Label22.Visible = DisplayType
    lblPractice.Visible = Not (DisplayType)
    Label24.Visible = DisplayType
    Label25.Visible = DisplayType
    Label26.Visible = DisplayType
    lblNPI.Visible = DisplayType
    txtNPI.Visible = DisplayType
    lblTax.Visible = DisplayType
    txtTax.Visible = DisplayType
    cmdDelete.Visible = False
End Sub

Private Sub LoadSelectedPractice(PracticeId As Long)
Dim ABill As Boolean
Dim Ps(7) As String, Pe(7) As String, NPI As String, Tax As String
Dim AName As String, AType As String, AFax As String, AEmail As String
Dim AAddr As String, ACity As String, ASuite As String, ALoc As String
Dim AState As String, AZip As String, ALog As String, AVac As String
Dim APhone1 As String, APhone2 As String, ATaxId As String, AOId1 As String
Dim VacDay As String, Yr As String
Dim l As Integer
Dim k As Integer
Dim ApplTbl As ApplicationTables
    Call SetControls(True)
    If (PracticeId > 0) Then
        Set ApplTbl = New ApplicationTables
        Call ApplTbl.ApplLoadPractice(PracticeId, AName, AType, AAddr, ASuite, _
                                      ACity, AState, AZip, AOId1, ATaxId, _
                                      AEmail, AFax, APhone1, APhone2, ALog, AVac, ALoc, ABill, _
                                      Ps(1), Ps(2), Ps(3), Ps(4), Ps(5), Ps(6), Ps(7), _
                                      Pe(1), Pe(2), Pe(3), Pe(4), Pe(5), Pe(6), Pe(7), NPI, Tax)
        PracticeName.Text = AName
        txtType.Text = AType
        txtLocRef.Text = ALoc
        Address.Text = AAddr
        Suite.Text = ASuite
        City.Text = ACity
        State.Text = AState
        
        
        
        Zip.Text = AZip

        OtherId.Text = AOId1
        TaxId.Text = ATaxId
        Email.Text = AEmail
        Fax.Text = FormatPhoneNumber(AFax, True)
        Phone.Text = FormatPhoneNumber(APhone1, True)
        OtherPhone.Text = FormatPhoneNumber(APhone2, True)
        chkBill.Value = 0
        If ABill Then
            chkBill.Value = 1
        End If
        ALog = 0
        lstStartTime1.ListIndex = GetTime(Trim(Ps(1)), lstStartTime1)
        lstStartTime2.ListIndex = GetTime(Trim(Ps(2)), lstStartTime2)
        lstStartTime3.ListIndex = GetTime(Trim(Ps(3)), lstStartTime3)
        lstStartTime4.ListIndex = GetTime(Trim(Ps(4)), lstStartTime4)
        lstStartTime5.ListIndex = GetTime(Trim(Ps(5)), lstStartTime5)
        lstStartTime6.ListIndex = GetTime(Trim(Ps(6)), lstStartTime6)
        lstStartTime7.ListIndex = GetTime(Trim(Ps(7)), lstStartTime7)
        lstEndTime1.ListIndex = GetTime(Trim(Pe(1)), lstEndTime1)
        lstEndTime2.ListIndex = GetTime(Trim(Pe(2)), lstEndTime2)
        lstEndTime3.ListIndex = GetTime(Trim(Pe(3)), lstEndTime3)
        lstEndTime4.ListIndex = GetTime(Trim(Pe(4)), lstEndTime4)
        lstEndTime5.ListIndex = GetTime(Trim(Pe(5)), lstEndTime5)
        lstEndTime6.ListIndex = GetTime(Trim(Pe(6)), lstEndTime6)
        lstEndTime7.ListIndex = GetTime(Trim(Pe(7)), lstEndTime7)
        SSMonth1.x.SelectedDays.RemoveAll
        If (Trim(AVac) <> "") Then
            k = 1
            l = Len(AVac)
            Yr = Trim(Str(Year(Date)))
            For k = 1 To l Step 4
                VacDay = Mid(AVac, k, 2) + "/" + Mid(AVac, k + 2, 2) + "/" + Yr
                If (IsDate(VacDay)) Then
                    SSMonth1.x.SelectedDays.Add VacDay
                End If
            Next k
        End If
        PracticeName.SetFocus
        SendKeys "{Home}"
        txtType.Text = "P"
        txtType.Locked = True
        txtNPI.Text = NPI
        txtTax.Text = Tax
        Set ApplTbl.lstBox = lstAff
        Call ApplTbl.ApplLoadAffiliations(PracticeId, "P")
        Set ApplTbl = Nothing
    Else
        txtType.Text = "P"
        txtType.Locked = True
    End If
    SSMonth1.ShowCentury = False
End Sub

Public Function PracticeLoadDisplay() As Boolean
Dim ApplTbl As New ApplicationTables
    lstAff.Visible = False
    BreakDown = val(CheckConfigCollection("DURATIONBREAKDOWN", "5"))
    If (BreakDown < 5) Then
        BreakDown = 5
    End If
    Call LoadTime(lstStartTime1, "", "", BreakDown)
    Call LoadTime(lstStartTime2, "", "", BreakDown)
    Call LoadTime(lstStartTime3, "", "", BreakDown)
    Call LoadTime(lstStartTime4, "", "", BreakDown)
    Call LoadTime(lstStartTime5, "", "", BreakDown)
    Call LoadTime(lstStartTime6, "", "", BreakDown)
    Call LoadTime(lstStartTime7, "", "", BreakDown)
    Call LoadTime(lstEndTime1, "", "", BreakDown)
    Call LoadTime(lstEndTime2, "", "", BreakDown)
    Call LoadTime(lstEndTime3, "", "", BreakDown)
    Call LoadTime(lstEndTime4, "", "", BreakDown)
    Call LoadTime(lstEndTime5, "", "", BreakDown)
    Call LoadTime(lstEndTime6, "", "", BreakDown)
    Call LoadTime(lstEndTime7, "", "", BreakDown)
    Set ApplTbl.lstBox = lstPractice
    PracticeLoadDisplay = ApplTbl.ApplLoadPracticeList("P", True)
    lblPractice.Visible = True
    lstPractice.Visible = True
    cmdDelete.Visible = False
    Set ApplTbl = Nothing
End Function

Private Sub cmdApply_Click()
    Dim ABill As Boolean
    Dim i As Integer
    Dim Temp As String, VacDay As String, ILog As String
    Dim ApplTbl As ApplicationTables
    Dim ApplCal As ApplicationCalendar
	Dim IsValidated As Boolean
    Dim DoctorValdMsg As String
    ''
    IsValidated = True
      DoctorValdMsg = "Please enter "
    If (Trim(PracticeName.Text) = "") Then
        IsValidated = False
        DoctorValdMsg = DoctorValdMsg + " Name"
    End If
    If (Trim(Address.Text) = "") Then
        IsValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "Address"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Address"
        End If
    End If
     If (Trim(City.Text) = "") Then
        IsValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "City"
        Else
            DoctorValdMsg = DoctorValdMsg + ", City"
        End If
    End If
    If (Trim(State.Text) = "") Then
        IsValidated = False
         If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + " State"
        Else
            DoctorValdMsg = DoctorValdMsg + ", State"
        End If
        
    End If
    If (Trim(Zip.Text) = "") Then
        IsValidated = False
        
         If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + " Zip"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Zip"
        End If
    End If
    If (Trim(txtNPI.Text) = "") Then
        IsValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "NPI"
        Else
            DoctorValdMsg = DoctorValdMsg + ", NPI"
        End If
    End If
    If (Trim(txtTax.Text) = "") Then
        IsValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "Taxonomy"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Taxonomy"
        End If
    End If
    If (Trim(Phone.Text) = "") Then
        IsValidated = False
        If DoctorValdMsg = "Please enter " Then
            DoctorValdMsg = DoctorValdMsg + "Phone"
        Else
            DoctorValdMsg = DoctorValdMsg + ", Phone"
        End If
    End If
    If Not IsValidated Then
        frmEventMsgs.Header = DoctorValdMsg
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SendKeys "{Home}"
        Exit Sub
    End If
    ''
    If (Trim(PracticeName.Text) = "") And Not (txtType.Visible) Then
        Unload frmPractice
        Exit Sub
    ElseIf (Trim(PracticeName.Text) = "") Then
        frmEventMsgs.Header = "Please select a name"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        PracticeName.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
    If (Trim(txtType.Text) = "") Then
        frmEventMsgs.Header = "Please select a type"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtType.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
    
     Set ApplTbl = New ApplicationTables
    
    If Trim(txtLocRef.Text = "") Then
    If ApplTbl.EmptyLocationReferenceexist() Then
        frmEventMsgs.Header = "Please select a LocationReference"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtType.SetFocus
        SendKeys "{Home}"
        Exit Sub
       
    End If
    
    
    
    End If
    
    If Len(Phone.Text) < 14 Or Len(Phone.Text) > 14 Then
    frmEventMsgs.Header = "Invalid Phone. Must be all numeric and 10 characters in length."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtType.SetFocus
        SendKeys "{Home}"
        Exit Sub
    
    
    End If
    
    
    If Zip.Text = "" Then
    
    frmEventMsgs.Header = "Please select a ZipCode."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtType.SetFocus
        SendKeys "{Home}"
        Exit Sub
    
    
    
    
    Else
    
    
    If Len(Zip.Text) < 10 Then
    frmEventMsgs.Header = "Zipcode is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtType.SetFocus
        SendKeys "{Home}"
        Exit Sub
    
    
    End If
    
    End If
    
    
   
    If (PracticeId < 1) Then
        If (ApplTbl.ApplIsPractice(PracticeName.Text, txtLocRef.Text)) Then
            frmEventMsgs.Header = "Practice Already Exists"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Set ApplTbl = Nothing
            PracticeName.SetFocus
            SendKeys "{Home}+{End}"
            Exit Sub
        End If
    End If
    Temp = PracticeName.Text
    Call StripCharacters(Temp, "'")
    PracticeName.Text = Temp
    Temp = ""
    For i = 0 To SSMonth1.x.SelectedDays.Count - 1
        VacDay = SSMonth1.x.SelectedDays(i).Date
        Call FormatTodaysDate(VacDay, False)
        VacDay = Left(VacDay, 5)
        Call StripCharacters(VacDay, "/")
        Temp = Temp + VacDay
    Next i
    ABill = False
    If (chkBill.Value = 1) Then
        ABill = True
    End If
    ILog = "1"
    If (ApplTbl.ApplPostPractice(PracticeId, PracticeName.Text, Left(Trim(txtType), 1), _
                         Address.Text, Suite.Text, City.Text, State.Text, Zip.Text, _
                         OtherId.Text, TaxId.Text, Email.Text, FormatPhoneNumber(Fax.Text, False), _
                         FormatPhoneNumber(Phone.Text, False), FormatPhoneNumber(OtherPhone.Text, False), _
                         ILog, Temp, txtLocRef.Text, ABill, _
                         Trim(lstStartTime1.List(lstStartTime1.ListIndex)), _
                         Trim(lstStartTime2.List(lstStartTime2.ListIndex)), _
                         Trim(lstStartTime3.List(lstStartTime3.ListIndex)), _
                         Trim(lstStartTime4.List(lstStartTime4.ListIndex)), _
                         Trim(lstStartTime5.List(lstStartTime5.ListIndex)), _
                         Trim(lstStartTime6.List(lstStartTime6.ListIndex)), _
                         Trim(lstStartTime7.List(lstStartTime7.ListIndex)), _
                         Trim(lstEndTime1.List(lstEndTime1.ListIndex)), _
                         Trim(lstEndTime2.List(lstEndTime2.ListIndex)), _
                         Trim(lstEndTime3.List(lstEndTime3.ListIndex)), _
                         Trim(lstEndTime4.List(lstEndTime4.ListIndex)), _
                         Trim(lstEndTime5.List(lstEndTime5.ListIndex)), _
                         Trim(lstEndTime6.List(lstEndTime6.ListIndex)), _
                         Trim(lstEndTime7.List(lstEndTime7.ListIndex)), txtNPI.Text, txtTax.Text)) Then
        If (Label16.Visible) Then
            Set ApplCal = New ApplicationCalendar
            lblUpdateHoliday.Visible = True
            For i = 0 To SSMonth1.x.SelectedDays.Count - 1
                VacDay = SSMonth1.x.SelectedDays(i).Date
                Call FormatTodaysDate(VacDay, False)
                Call ApplCal.SetHolidays(VacDay)
            Next i
            lblUpdateHoliday.Visible = False
            Set ApplCal = Nothing
        End If
    Else
        frmEventMsgs.Header = "Location Reference Required"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
    Set ApplTbl = Nothing
    Unload frmPractice
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As New ApplicationTables
    ApplTbl.ApplDeletePractice PracticeId
    PracticeId = 0
    Set ApplTbl = Nothing
    Unload frmPractice
End Sub

Private Sub cmdBack_Click()
    Unload frmPractice
End Sub

Private Sub Form_Load()
    If UserLogin.HasPermission(epPowerUser) Then
        frmPractice.BorderStyle = 1
        frmPractice.ClipControls = True
        frmPractice.Caption = Mid(frmPractice.Name, 4, Len(frmPractice.Name) - 3)
        frmPractice.AutoRedraw = True
        frmPractice.Refresh
    End If
End Sub

Private Sub Phone_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Or KeyAscii = 10 Then
        Phone.Text = FormatPhoneNumber(Phone.Text, True)
    End If
End Sub

Private Sub Phone_Validate(Cancel As Boolean)
    Phone.Text = FormatPhoneNumber(Phone.Text, True)
End Sub

Private Sub OtherPhone_Keypress(KeyAscii As Integer)
    If KeyAscii = 13 Or KeyAscii = 10 Then
        OtherPhone.Text = FormatPhoneNumber(OtherPhone.Text, True)
    End If
End Sub

Private Sub OtherPhone_Validate(Cancel As Boolean)
    OtherPhone.Text = FormatPhoneNumber(OtherPhone.Text, True)
End Sub

Private Sub Fax_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Or KeyAscii = 10 Then
        Fax.Text = FormatPhoneNumber(Fax.Text, True)
    End If
End Sub

Private Sub Fax_Validate(Cancel As Boolean)
    Fax.Text = FormatPhoneNumber(Fax.Text, True)
End Sub

Private Sub txtNPI_KeyPress(KeyAscii As Integer)
Dim i As Integer
    If (KeyAscii = 13) Then
        If (Len(txtNPI.Text) <> 10) And (Len(txtNPI.Text) <> 0) Then
            frmEventMsgs.Header = "Invalid NPI. Must be all numeric and 10 characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtNPI.Text = ""
            txtNPI.SetFocus
        Else
            For i = 1 To Len(txtNPI.Text)
                If Not (IsNumeric(Mid(txtNPI.Text, i, 1))) Then
                    frmEventMsgs.Header = "Invalid NPI. Must be all numeric and 10 characters in length."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtNPI.Text = ""
                    txtNPI.SetFocus
                    Exit For
                End If
            Next i
        End If
    End If
End Sub

Private Sub txtNPI_Validate(Cancel As Boolean)
    Call txtNPI_KeyPress(13)
End Sub

Private Sub Zip_KeyPress(KeyAscii As Integer)

    If (KeyAscii = 13) Or (KeyAscii = 10) Then
        Call UpdateDisplay(Zip, "Z")
    End If
End Sub

Private Sub Zip_Validate(Cancel As Boolean)
    Call UpdateDisplay(Zip, "Z")
End Sub

Private Sub lstPractice_Click()
    If (lstPractice.ListIndex > 0) Then
        PracticeId = val(Trim(Mid(lstPractice.List(lstPractice.ListIndex), 70, 5)))
        RemoveOn = CheckConfigCollection("REMOVEACTIVE") = "T"
    Else
        PracticeId = 0
        RemoveOn = False
    End If
    Call LoadSelectedPractice(PracticeId)
End Sub

Private Sub lstAff_Click()
Dim ApplTbl As New ApplicationTables
    If (lstAff.ListIndex = 0) Then
        frmAffiliations.AffiliationId = 0
        frmAffiliations.ResourceId = PracticeId
        frmAffiliations.ResourceType = "P"
        frmAffiliations.OverrideOn = True
        frmAffiliations.RemoveOn = RemoveOn
        If (frmAffiliations.LoadAffiliationDisplay) Then
            frmAffiliations.Show 1
            If (frmAffiliations.AffiliationId <> 0) Then
                Set ApplTbl.lstBox = lstAff
                Call ApplTbl.ApplLoadAffiliations(PracticeId, "P")
            End If
        End If
    ElseIf (lstAff.ListIndex > 0) Then
        frmAffiliations.AffiliationId = val(Trim(Mid(lstAff.List(lstAff.ListIndex), 76, 5)))
        frmAffiliations.ResourceId = PracticeId
        frmAffiliations.ResourceType = "P"
        frmAffiliations.OverrideOn = True
        frmAffiliations.RemoveOn = RemoveOn
        If (frmAffiliations.LoadAffiliationDisplay) Then
            frmAffiliations.Show 1
            If (frmAffiliations.AffiliationId <> 0) Then
                Set ApplTbl.lstBox = lstAff
                Call ApplTbl.ApplLoadAffiliations(PracticeId, "P")
            End If
        End If
    End If
    Set ApplTbl = Nothing
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
