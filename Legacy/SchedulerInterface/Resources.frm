VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmResources 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   FillColor       =   &H00FFFFFF&
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdPatientAccess 
      Height          =   990
      Left            =   6480
      TabIndex        =   8
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Resources.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   840
      TabIndex        =   5
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Resources.frx":01E9
   End
   Begin VB.ListBox lstRooms 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5310
      ItemData        =   "Resources.frx":03C8
      Left            =   6533
      List            =   "Resources.frx":03CA
      TabIndex        =   2
      Top             =   1560
      Width           =   4695
   End
   Begin VB.ListBox lstStaff 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5310
      ItemData        =   "Resources.frx":03CC
      Left            =   773
      List            =   "Resources.frx":03CE
      TabIndex        =   1
      Top             =   1560
      Width           =   5055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEdit 
      Height          =   990
      Left            =   3960
      TabIndex        =   6
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Resources.frx":03D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9360
      TabIndex        =   7
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Resources.frx":05AF
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Rooms"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6540
      TabIndex        =   4
      Top             =   1080
      Width           =   1200
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Staff"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   780
      TabIndex        =   3
      Top             =   1080
      Width           =   1200
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Setup  Resources"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   420
      Left            =   4530
      TabIndex        =   0
      Top             =   360
      Width           =   2940
   End
End
Attribute VB_Name = "frmResources"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdDone_Click()
Unload frmResources
End Sub

Private Sub cmdEdit_Click()
Dim Ref As String
Dim ResourceId As Long
Dim TheType As String
If (lstStaff.ListIndex < 0) And (lstRooms.ListIndex < 0) Then
    frmEventMsgs.Header = "Select Staff or Rooms to Edit or Create"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (lstStaff.ListIndex > 0) Then
    ResourceId = val(Mid(lstStaff.List(lstStaff.ListIndex), 57, Len(lstStaff.List(lstStaff.ListIndex)) - 56))
    TheType = "S"
ElseIf (lstRooms.ListIndex > 0) Then
    ResourceId = val(Mid(lstRooms.List(lstRooms.ListIndex), 57, Len(lstRooms.List(lstRooms.ListIndex)) - 56))
    TheType = "R"
Else
    ResourceId = 0
    TheType = "D"
    If (lstRooms.ListIndex = 0) Then
        TheType = "R"
    End If
End If
frmSchedulerResource.DeleteOn = CheckConfigCollection("REMOVEACTIVE") = "T"
If (ResourceId < 1) Then
    frmSchedulerResource.DeleteOn = False
End If
If (frmSchedulerResource.ResourceLoadDisplay(ResourceId, TheType)) Then
    frmSchedulerResource.Show 1
    Call LoadResourceList
End If
End Sub

Private Sub cmdExit_Click()
Unload frmResources
End Sub

Public Function LoadResourceList() As Boolean
Dim ApplTbl As ApplicationTables
LoadResourceList = False
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstRooms
LoadResourceList = ApplTbl.ApplLoadResource("R", False, True, False, False, False)
Set ApplTbl.lstBox = lstStaff
LoadResourceList = ApplTbl.ApplLoadResource("S", False, True, False, True, False)
Set ApplTbl = Nothing
End Function

Private Sub cmdPatientAccess_Click()
Dim objRestricted As EmergencyAccess.frmRestrictedPatients
Set objRestricted = New EmergencyAccess.frmRestrictedPatients
Dim DbHelper As New EmergencyAccess.DbHelper
DbHelper.DbObject = IdbConnection
objRestricted.Show
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmResources.BorderStyle = 1
    frmResources.ClipControls = True
    frmResources.Caption = Mid(frmResources.Name, 4, Len(frmResources.Name) - 3)
    frmResources.AutoRedraw = True
    frmResources.Refresh
End If
If UserLogin.HasPermission(epPatientAccess) Then
    cmdPatientAccess.Visible = True
Else
    cmdPatientAccess.Visible = False
End If
End Sub

Private Sub lstRooms_Click()
Dim CurItem As Integer
CurItem = lstRooms.ListIndex
lstStaff.ListIndex = -1
lstRooms.ListIndex = CurItem
End Sub

Private Sub lstRooms_DblClick()
Dim CurItem As Integer
CurItem = lstRooms.ListIndex
lstStaff.ListIndex = -1
lstRooms.ListIndex = CurItem
Call cmdEdit_Click
End Sub

Private Sub lstStaff_Click()
Dim CurItem As Integer
CurItem = lstStaff.ListIndex
lstRooms.ListIndex = -1
lstStaff.ListIndex = CurItem
End Sub

Private Sub lstStaff_DblClick()
Dim CurItem As Integer
CurItem = lstStaff.ListIndex
lstRooms.ListIndex = -1
lstStaff.ListIndex = CurItem
Call cmdEdit_Click
End Sub
Public Function FrmClose()
Call cmdDone_Click
Unload Me
End Function
