VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmPatientLocator 
   BackColor       =   &H00785211&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00785211&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstDoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PatientLocator.frx":0000
      Left            =   7560
      List            =   "PatientLocator.frx":0002
      TabIndex        =   8
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PatientLocator.frx":0004
      Left            =   9600
      List            =   "PatientLocator.frx":0006
      TabIndex        =   6
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstPayment 
      Height          =   450
      Left            =   4440
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   840
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H00946414&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5700
      ItemData        =   "PatientLocator.frx":0008
      Left            =   240
      List            =   "PatientLocator.frx":000A
      TabIndex        =   1
      Top             =   1440
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   240
      TabIndex        =   2
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientLocator.frx":000C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9720
      TabIndex        =   3
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientLocator.frx":01EB
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   960
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   9724948
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Appointment Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   11
      Top             =   720
      Width           =   1560
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   7560
      TabIndex        =   9
      Top             =   240
      Width           =   570
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9600
      TabIndex        =   7
      Top             =   240
      Width           =   735
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   5400
      TabIndex        =   5
      Top             =   960
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00785211&
      Caption         =   "Patient Locator"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   4620
      TabIndex        =   4
      Top             =   240
      Width           =   2535
   End
End
Attribute VB_Name = "frmPatientLocator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public AppointmentDate As String
Public LocationId As Long
Public DoctorId As Long
Private TriggerOn As Boolean

Private Sub cmdDone_Click()
CurrentAction = ""
Unload frmPatientLocator
End Sub

Private Sub cmdHome_Click()
CurrentAction = "Home"
Unload frmPatientLocator
End Sub

Private Sub Form_Load()
TriggerOn = False
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientLocator.BorderStyle = 1
    frmPatientLocator.ClipControls = True
    frmPatientLocator.Caption = Mid(frmPatientLocator.Name, 4, Len(frmPatientLocator.Name) - 3)
    frmPatientLocator.AutoRedraw = True
    frmPatientLocator.Refresh
End If
End Sub

Private Sub lstAppts_Click()
Dim sType As Boolean
Dim ITemp As Integer
Dim AName As String
Dim Temp As String, IType As String
Dim CPayMethod As String, CPayRef As String
Dim CPayAmount As Single
Dim PatId As Long, ApptId As Long, LocId As Long
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim Valid As Validations
Dim AHostName As String
Dim ActivityId As Long
Dim RetrieveActivity  As PracticeActivity
If Not (TriggerOn) Then
    If (lstAppts.ListIndex >= 0) Then
        ITemp = lstAppts.ListIndex
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
        Call ApplList.ApplGetLastAccess(ApptId, AName)
        Set ApplList = Nothing
        If (AName <> "") Then
            frmEventMsgs.Header = "Last Access: " + Trim(AName) + vbCrLf + "Send Patient to"
        Else
            frmEventMsgs.Header = "Send Patient to"
        End If
        frmEventMsgs.AcceptText = "Print Pre-Appt"
        frmEventMsgs.RejectText = "Patient Info"
        frmEventMsgs.CancelText = "Quit"
        frmEventMsgs.Other0Text = "Cancel Appt"
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "In Chk") > 0) Then
            frmEventMsgs.Other0Text = ""
        End If
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "Disc") <> 0) Then
            frmEventMsgs.Other0Text = ""
            sType = False
            IType = ""
        End If
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "Exam") > 0) Then
            frmEventMsgs.Other0Text = ""
        End If
        sType = False
        IType = ""
        frmEventMsgs.Other1Text = ""
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "Que") <> 0) Or _
           (InStrPS(lstAppts.List(lstAppts.ListIndex), "Unk") <> 0) Then
            frmEventMsgs.Other1Text = "Waiting Room"
        End If
        frmEventMsgs.Other2Text = ""
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "Que") <> 0) Or _
           (InStrPS(lstAppts.List(lstAppts.ListIndex), "Unk") <> 0) Or _
           (InStrPS(lstAppts.List(lstAppts.ListIndex), "Wait") <> 0) Then
            frmEventMsgs.Other2Text = "Check Out"
        End If
        frmEventMsgs.Other3Text = ""
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "Que") <> 0) Or _
           (InStrPS(lstAppts.List(lstAppts.ListIndex), "Unk") <> 0) Or _
           (InStrPS(lstAppts.List(lstAppts.ListIndex), "Wait") <> 0) Then
            frmEventMsgs.Other3Text = "Collect CoPay"
        End If
        If (InStrPS(lstAppts.List(lstAppts.ListIndex), "In Chk") > 0) Then
            frmEventMsgs.Other4Text = "Unlock"
        Else
            frmEventMsgs.Other4Text = ""
        End If
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            lblPrint.Caption = "Printing In Progress"
            lblPrint.Visible = True
            Set ApplList = New ApplicationAIList
            PatId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
            ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
            Set ApplList = Nothing
            Set ApplTemp = New ApplicationTemplates
            Set ApplTemp.lstBox = lstPayment
            Call ApplTemp.PrintTransaction(ApptId, PatId, "C", True, True, "", False, 0, "S", "", 0)
            Set ApplTemp = Nothing
            lblPrint.Visible = False
        ElseIf (frmEventMsgs.Result = 2) Then
            Set ApplList = New ApplicationAIList
            PatId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
            Set ApplList = Nothing
            Dim PatientDemoGraphics As New PatientDemoGraphics
            PatientDemoGraphics.PatientId = PatId
            If PatientDemoGraphics.DisplayPatientInfoScreen Then
                TriggerOn = True
                Call AppointmentLoadList
                If (ITemp <= lstAppts.ListCount - 1) Then
                    lstAppts.ListIndex = ITemp
                End If
            End If
        ElseIf (frmEventMsgs.Result = 3) Then
            Set Valid = New Validations
            If Valid.AppCancel(ApptId) Then
                If CheckConfigCollection("APPTTRACKON") = "T" Then
                    Set ApplList = New ApplicationAIList
                    ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
                    
                    Dim arguments() As Variant
                    Dim ReturnArguments As Variant
                    Dim CancelAppointmentComWrapper As New comWrapper
                    Call CancelAppointmentComWrapper.Create(CancelAppointmentLoadArgumentsType, emptyArgs)
                    
                    Dim wrapper As New comWrapper
                    Call wrapper.Create("System.Collections.Generic.List`1[[System.Int32]]", emptyArgs)
                    
                    Dim AddItemArgs() As Variant
                    AddItemArgs = Array(ApptId)
                    Call wrapper.InvokeMethod("Add", AddItemArgs)
    
                    Dim loadArguments As Object
                    Set loadArguments = CancelAppointmentComWrapper.Instance
                    loadArguments.ListOfAppointmentId = wrapper.Instance
    
                    arguments = Array(loadArguments, True, Nothing)
    
                    Call CancelAppointmentComWrapper.Create(CancelAppointmentViewManagerType, emptyArgs)
                    Set ReturnArguments = CancelAppointmentComWrapper.InvokeMethod("ShowCancelAppointment", arguments)
                Else
                    frmEventMsgs.Header = "Type of Cancel"
                    frmEventMsgs.AcceptText = "Left"
                    frmEventMsgs.RejectText = "No Show"
                    frmEventMsgs.CancelText = "Quit"
                    frmEventMsgs.Other0Text = "Same Day"
                    frmEventMsgs.Other1Text = "Patient Cancel"
                    frmEventMsgs.Other2Text = "Office Cancel"
                    frmEventMsgs.Other3Text = "Cancel Other"
                    frmEventMsgs.Other4Text = "Special Cancel"
                    frmEventMsgs.Show 1
                    If (frmEventMsgs.Result = 1) Then
                        Temp = "F"
                    ElseIf (frmEventMsgs.Result = 2) Then
                        Temp = "N"
                    ElseIf (frmEventMsgs.Result = 3) Then
                        Temp = "S"
                    ElseIf (frmEventMsgs.Result = 5) Then
                        Temp = "Y"
                    ElseIf (frmEventMsgs.Result = 6) Then
                        Temp = "O"
                    ElseIf (frmEventMsgs.Result = 7) Then
                        Temp = "X"
                    ElseIf (frmEventMsgs.Result = 8) Then
                        Temp = "Q"
                    Else
                        Temp = ""
                    End If
                    If (Temp <> "") Then
                        Set ApplList = New ApplicationAIList
                        Call ApplList.ApplCheckOutPatient(lstAppts.List(lstAppts.ListIndex), Temp, False, UserLogin.iId)
                        ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
                        If (ApptId > 0) Then
                            Call ApplList.DeleteSurgeryPlanAppointment(ApptId)
                        End If
                        Set ApplList = Nothing
                    End If
                End If
                TriggerOn = True
                Call AppointmentLoadList
                If (ITemp <= lstAppts.ListCount - 1) Then
                    lstAppts.ListIndex = ITemp
                End If
            End If
            Set Valid = Nothing
        ElseIf (frmEventMsgs.Result = 5) Then
            Set ApplList = New ApplicationAIList
            Call ApplList.ApplSetActivityStatus(lstAppts.List(lstAppts.ListIndex), "M", UserLogin.iId)
            Set ApplList = Nothing
            TriggerOn = True
            Call AppointmentLoadList
            If (ITemp <= lstAppts.ListCount - 1) Then
                lstAppts.ListIndex = ITemp
            End If
        ElseIf (frmEventMsgs.Result = 6) Then
            Set ApplList = New ApplicationAIList
            Call ApplList.ApplSetActivityStatus(lstAppts.List(lstAppts.ListIndex), "H", UserLogin.iId)
            Set ApplList = Nothing
            TriggerOn = True
            Call AppointmentLoadList
            If (ITemp <= lstAppts.ListCount - 1) Then
                lstAppts.ListIndex = ITemp
            End If
        ElseIf (frmEventMsgs.Result = 7) Then
            Set ApplList = New ApplicationAIList
            ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
            If (ApptId > 0) Then
                Call ApplList.ApplGetActivityCopay(ApptId, CPayAmount, CPayMethod, CPayRef, LocId)
                If (frmCollectPayment.LoadCollect(ApptId, CPayAmount, CPayMethod, CPayRef)) Then
                    frmCollectPayment.Show 1
                    CPayAmount = frmCollectPayment.CopayAmount
                    CPayMethod = frmCollectPayment.CopayMethod
                    CPayRef = frmCollectPayment.CopayRef
                    Call ApplList.ApplActivityCopay(ApptId, CPayAmount, CPayMethod, CPayRef, LocId)
                End If
            End If
            Set ApplList = Nothing
            TriggerOn = True
            Call AppointmentLoadList
            If (ITemp <= lstAppts.ListCount - 1) Then
                lstAppts.ListIndex = ITemp
            End If
        ElseIf (frmEventMsgs.Result = 8) Then
            Set ApplList = New ApplicationAIList
            ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
            PatId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
            Set ApplList = Nothing
                    
            'Get Back ActivityId based on AppointmentId and PatientId
            If ApptId > 0 And PatId > 0 Then
                Set RetrieveActivity = New PracticeActivity
                RetrieveActivity.ActivityAppointmentId = ApptId
                RetrieveActivity.ActivityLocationId = -1
                RetrieveActivity.ActivityPatientId = PatId
                RetrieveActivity.ActivityStatus = "U"
                If (RetrieveActivity.FindActivity > 0) Then
                    If (RetrieveActivity.SelectActivity(1)) Then
                        ActivityId = RetrieveActivity.ActivityId
                    End If
                End If
                Set RetrieveActivity = Nothing
            End If
            
            If ActivityId <= 0 Then Exit Sub
            
            'Get the HostName where Patient Chart was opened last time
            AHostName = GetLastAccessHostName(ActivityId)
            If (Trim(AHostName) <> "") Then
                frmEventMsgs.SetButtons "Patient was being checked out on {" & AHostName & "}." & vbCrLf & "If you are sure it is not open on that computer, would you like to put the patient back in check out?", "", "Put back in check out", "Cancel"
                frmEventMsgs.cmdResult(2).Top = 1500
                frmEventMsgs.cmdResult(2).Height = 1310
                frmEventMsgs.cmdResult(2).Width = 1800
                frmEventMsgs.cmdResult(3).Top = 1500
                frmEventMsgs.cmdResult(3).Left = 3800
                frmEventMsgs.cmdResult(3).Width = 1800
                frmEventMsgs.cmdResult(3).Height = 1310
            Else
                frmEventMsgs.SetButtons "would you like to put the patient back in check out? ", "", "Put back in check out", "Cancel"
            End If
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 2) Then
                    'Roll Back Activity Status to 'H'
                    Set RetrieveActivity = New PracticeActivity
                    RetrieveActivity.ActivityId = ActivityId
                    If (RetrieveActivity.RetrieveActivity) Then
                        RetrieveActivity.ActivityStatus = "H"
                        RetrieveActivity.ApplyActivity
                    End If
                    Set RetrieveActivity = Nothing
                    
                    TriggerOn = True
                    Call AppointmentLoadList
                    If (ITemp <= lstAppts.ListCount - 1) Then
                        lstAppts.ListIndex = ITemp
                    End If
                End If
            End If
    End If
Else
    TriggerOn = False
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        LocationId = -1
    Else
        Set ApplList = New ApplicationAIList
        LocationId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
    Call AppointmentLoadList
''''    If (frmPatientLocator.Visible) Then
''''        If (lstAppts.ListCount < 1) Then
''''            frmEventMsgs.Header = "Nothing Outstanding"
''''            frmEventMsgs.AcceptText = ""
''''            frmEventMsgs.RejectText = "Ok"
''''            frmEventMsgs.CancelText = ""
''''            frmEventMsgs.Other0Text = ""
''''            frmEventMsgs.Other1Text = ""
''''            frmEventMsgs.Other2Text = ""
''''            frmEventMsgs.Other3Text = ""
''''            frmEventMsgs.Other4Text = ""
''''            frmEventMsgs.Show 1
''''            LocationId = dbPracticeId
''''            DoEvents
''''            Call AppointmentLoadList
''''            lstLoc.ListIndex = -1
''''        End If
''''    End If
End If
End Sub

Private Sub lstDoc_Click()
Dim ApplList As ApplicationAIList
If (lstDoc.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    DoctorId = ApplList.ApplGetListResourceId(lstDoc.List(lstDoc.ListIndex))
    Set ApplList = Nothing
    Call AppointmentLoadList
''''    If (lstAppts.ListCount < 1) Then
''''        frmEventMsgs.Header = "Nothing Outstanding"
''''        frmEventMsgs.AcceptText = ""
''''        frmEventMsgs.RejectText = "Ok"
''''        frmEventMsgs.CancelText = ""
''''        frmEventMsgs.Other0Text = ""
''''        frmEventMsgs.Other1Text = ""
''''        frmEventMsgs.Other2Text = ""
''''        frmEventMsgs.Other3Text = ""
''''        frmEventMsgs.Other4Text = ""
''''        frmEventMsgs.Show 1
''''        DoctorId = -1
''''        DoEvents
''''        Call AppointmentLoadList
''''        lstDoc.ListIndex = -1
''''    End If
End If
End Sub

Private Sub SSDateCombo1_Change()
If SSDateCombo1.DroppedDown Then Exit Sub
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_Click()
'If (Trim(SSDateCombo1.Date) <> "") Then
'    Call SSDateCombo1_KeyPress(13)
'End If
End Sub

Private Sub SSDateCombo1_CloseUp()
If (Trim(SSDateCombo1.Date) <> "") Then
    AppointmentDate = SSDateCombo1.DateValue
    Call FormatTodaysDate(AppointmentDate, False)
    If (AppointmentDate = "") Then
        SSDateCombo1.Text = ""
    Else
        SSDateCombo1.Text = AppointmentDate
    End If
    Call AppointmentLoadList
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") And (SSDateCombo1.DateValue <> SSDateCombo1.Date) Then
        AppointmentDate = SSDateCombo1.DateValue
        Call FormatTodaysDate(AppointmentDate, False)
        If (AppointmentDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = AppointmentDate
            Call AppointmentLoadList
        End If
    Else
        If (AppointmentDate = "") Then
            SSDateCombo1.Text = ""
        ElseIf (SSDateCombo1.DateValue = "") Then
            SSDateCombo1.Text = AppointmentDate
        Else
            AppointmentDate = SSDateCombo1.DateValue
            Call FormatTodaysDate(AppointmentDate, False)
            AppointmentDate = Mid(AppointmentDate, 1, 2) + Mid(AppointmentDate, 4, 2) + Mid(AppointmentDate, 9, 2)
        End If
        ATemp = AppointmentDate
        If (Len(AppointmentDate) > 10) Then
            ATemp = Mid(AppointmentDate, 11, Len(AppointmentDate) - 10)
        ElseIf (AppointmentDate = "") Then
            Call FormatTodaysDate(AppointmentDate, False)
            ATemp = Mid(AppointmentDate, 1, 2) + Mid(AppointmentDate, 4, 2) + Mid(AppointmentDate, 9, 2)
        End If
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        AppointmentDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(AppointmentDate, False)
        If (AppointmentDate = "") And (SSDateCombo1.DateValue <> SSDateCombo1.Date) Then
            AppointmentDate = SSDateCombo1.Date
            Call FormatTodaysDate(AppointmentDate, False)
            SSDateCombo1.Text = AppointmentDate
        Else
            SSDateCombo1.Text = AppointmentDate
        End If
        Call AppointmentLoadList
    End If
ElseIf (KeyAscii = 27) Then
    Call PrintContent(lstAppts, Label5.Caption + " For " + Trim(SSDateCombo1.DateValue))
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        AppointmentDate = AppointmentDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(AppointmentDate) > 0) Then
        AppointmentDate = Left(AppointmentDate, Len(AppointmentDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(AppointmentDate) < 1) Then
        AppointmentDate = ""
    End If
End If
End Sub

Public Function AppointmentLoadList() As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim LocId As Long
Dim Temp As String
Dim TheDate As String
Dim LocalDate As ManagedDate
Dim ApptList As ApplicationAIList
AppointmentLoadList = False
lstAppts.Clear
If (lstLoc.ListCount < 1) Then
    Set ApptList = New ApplicationAIList
    Set ApptList.ApplList = lstLoc
    Call ApptList.ApplLoadLocation(False, True)
    Set ApptList = Nothing
    LocationId = dbPracticeId
End If
If (lstDoc.ListCount < 1) Then
    Set ApptList = New ApplicationAIList
    Set ApptList.ApplList = lstDoc
    Call ApptList.ApplLoadStaff(False)
    Set ApptList = Nothing
    DoctorId = -1
End If
TheDate = ""
If (Trim(AppointmentDate) <> "") Then
    TheDate = AppointmentDate
End If
Call FormatTodaysDate(TheDate, False)
Set ApptList = New ApplicationAIList
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = TheDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    ApptList.ApplDate = LocalDate.ExposedDate
End If
Set ApptList.ApplList = lstAppts
ApptList.ApplResourceId = 0
ApptList.ApplLocId = LocationId
ApptList.ApplResourceId = DoctorId
Set ApptList.ApplLocList = lstLoc
Call ApptList.ApplRetrievePatientLocatorList
AppointmentLoadList = True
Set LocalDate = Nothing
Set ApptList = Nothing
If (LocationId > -1) And Not (frmPatientLocator.Visible) Then
    For i = 0 To lstLoc.ListCount - 1
        If (LocationId = val(Trim(Mid(lstLoc.List(i), 56, 5)))) Then
            LocId = i
            Exit For
        End If
    Next i
    If (LocId > 0) Then
        lstLoc.ListIndex = LocId
    End If
End If
Exit Function
UIError_Label:
    Set LocalDate = Nothing
    Set ApptList = Nothing
    Resume LeaveFast
LeaveFast:
End Function
Public Function FrmClose()
Call cmdHome_Click
Unload Me
End Function
Private Function GetLastAccessHostName(ByVal ActivityId As Long)
GetLastAccessHostName = ""
Dim sqlStr As String
Dim RS As Recordset

    sqlStr = "Select TOP 1 HostName from AuditEntries WHERE ObjectName='dbo.PracticeActivity' AND KeyValueNumeric=" & ActivityId & " ORDER BY AuditDateTime Desc"

Set RS = GetRS(sqlStr)
If (Not (RS Is Nothing) And (RS.RecordCount > 0)) Then
    GetLastAccessHostName = RS("HostName")
End If

End Function
