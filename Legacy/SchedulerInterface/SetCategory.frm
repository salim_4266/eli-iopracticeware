VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmSetCategory 
   BackColor       =   &H00008000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Set Categories"
   ClientHeight    =   6090
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8370
   ClipControls    =   0   'False
   ForeColor       =   &H00008000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6090
   ScaleWidth      =   8370
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstCat 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5310
      ItemData        =   "SetCategory.frx":0000
      Left            =   120
      List            =   "SetCategory.frx":0002
      TabIndex        =   0
      Top             =   240
      Width           =   6255
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   6480
      TabIndex        =   1
      Top             =   4560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetCategory.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   990
      Left            =   6480
      TabIndex        =   2
      Top             =   360
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetCategory.frx":01E3
   End
End
Attribute VB_Name = "frmSetCategory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ResultOn As Boolean
Public PCat1 As String
Public PCat2 As String
Public PCat3 As String
Public PCat4 As String
Public PCat5 As String
Public PCat6 As String
Public PCat7 As String
Public PCat8 As String
Private TimeArray(96) As String

Private Sub cmdApply_Click()
Dim i As Integer
ResultOn = True
PCat1 = ""
PCat3 = ""
PCat5 = ""
PCat7 = ""
For i = 1 To 48
    PCat1 = PCat1 + Mid(TimeArray(i), 1, 1) + Mid(TimeArray(i), 3, 1) + Mid(TimeArray(i), 5, 1) + Mid(TimeArray(i), 7, 1)
    PCat3 = PCat3 + Mid(TimeArray(i), 9, 1) + Mid(TimeArray(i), 11, 1) + Mid(TimeArray(i), 13, 1) + Mid(TimeArray(i), 15, 1)
    PCat5 = PCat5 + Mid(TimeArray(i), 17, 1) + Mid(TimeArray(i), 19, 1) + Mid(TimeArray(i), 21, 1) + Mid(TimeArray(i), 23, 1)
    PCat7 = PCat7 + Mid(TimeArray(i), 25, 1) + Mid(TimeArray(i), 27, 1) + Mid(TimeArray(i), 29, 1) + Mid(TimeArray(i), 31, 1)
Next i
PCat2 = ""
PCat4 = ""
PCat6 = ""
PCat8 = ""
For i = 49 To 96
    PCat2 = PCat2 + Mid(TimeArray(i), 1, 1) + Mid(TimeArray(i), 3, 1) + Mid(TimeArray(i), 5, 1) + Mid(TimeArray(i), 7, 1)
    PCat4 = PCat4 + Mid(TimeArray(i), 9, 1) + Mid(TimeArray(i), 11, 1) + Mid(TimeArray(i), 13, 1) + Mid(TimeArray(i), 15, 1)
    PCat6 = PCat6 + Mid(TimeArray(i), 17, 1) + Mid(TimeArray(i), 19, 1) + Mid(TimeArray(i), 21, 1) + Mid(TimeArray(i), 23, 1)
    PCat8 = PCat8 + Mid(TimeArray(i), 25, 1) + Mid(TimeArray(i), 27, 1) + Mid(TimeArray(i), 29, 1) + Mid(TimeArray(i), 31, 1)
Next i
Unload frmSetCategory
End Sub

Private Sub cmdClear_Click()
Dim i As Integer
For i = 1 To 96
    Mid(TimeArray(i), 1, 1) = " "
    Mid(TimeArray(i), 3, 1) = " "
    Mid(TimeArray(i), 5, 1) = " "
    Mid(TimeArray(i), 7, 1) = " "
    Mid(TimeArray(i), 9, 1) = " "
    Mid(TimeArray(i), 11, 1) = " "
    Mid(TimeArray(i), 13, 1) = " "
    Mid(TimeArray(i), 15, 1) = " "
    Mid(TimeArray(i), 17, 1) = " "
    Mid(TimeArray(i), 19, 1) = " "
    Mid(TimeArray(i), 21, 1) = " "
    Mid(TimeArray(i), 23, 1) = " "
    Mid(TimeArray(i), 25, 1) = " "
    Mid(TimeArray(i), 27, 1) = " "
    Mid(TimeArray(i), 29, 1) = " "
    Mid(TimeArray(i), 31, 1) = " "
Next i
Call cmdApply_Click
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If (Cancel <> 0) And (UnloadMode <> 1) Then
    ResultOn = False
End If
Unload frmSetCategory
End Sub

Public Function LoadCategory() As Boolean
Dim i As Integer
Dim z As Integer
LoadCategory = False
ResultOn = False
For i = 1 To 96
    TimeArray(i) = Space(32)
Next i
LoadCategory = True
Call LoadTime(lstCat, "12:00 AM", "11:59 PM", 15)
If (Trim(PCat1) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat1, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat1, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 1, 2) = Mid(PCat1, z + 1, 1) + "S"
            End If
            If (Trim(Mid(PCat1, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 3, 2) = Mid(PCat1, z + 2, 1) + "I"
            End If
            If (Trim(Mid(PCat1, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 5, 2) = Mid(PCat1, z + 3, 1) + "L"
            End If
            If (Trim(Mid(PCat1, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 7, 2) = Mid(PCat1, z + 4, 1) + "E"
            End If
        End If
    Next z
End If
If (Trim(PCat3) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat3, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat3, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 9, 2) = Mid(PCat3, z + 1, 1) + "W"
            End If
            If (Trim(Mid(PCat3, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 11, 2) = Mid(PCat3, z + 2, 1) + "X"
            End If
            If (Trim(Mid(PCat3, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 13, 2) = Mid(PCat3, z + 3, 1) + "Y"
            End If
            If (Trim(Mid(PCat3, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 15, 2) = Mid(PCat3, z + 4, 1) + "Z"
            End If
        End If
    Next z
End If
If (Trim(PCat5) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat5, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat5, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 17, 2) = Mid(PCat5, z + 1, 1) + "A"
            End If
            If (Trim(Mid(PCat5, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 19, 2) = Mid(PCat5, z + 2, 1) + "B"
            End If
            If (Trim(Mid(PCat5, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 21, 2) = Mid(PCat5, z + 3, 1) + "C"
            End If
            If (Trim(Mid(PCat5, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 23, 2) = Mid(PCat5, z + 4, 1) + "D"
            End If
        End If
    Next z
End If
If (Trim(PCat7) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat7, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat7, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 25, 2) = Mid(PCat7, z + 1, 1) + "M"
            End If
            If (Trim(Mid(PCat7, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 27, 2) = Mid(PCat7, z + 2, 1) + "N"
            End If
            If (Trim(Mid(PCat7, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 29, 2) = Mid(PCat7, z + 3, 1) + "O"
            End If
            If (Trim(Mid(PCat7, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1), 31, 2) = Mid(PCat7, z + 4, 1) + "P"
            End If
        End If
    Next z
End If

If (Trim(PCat2) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat2, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat2, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 1, 2) = Mid(PCat2, z + 1, 1) + "S"
            End If
            If (Trim(Mid(PCat2, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 3, 2) = Mid(PCat2, z + 2, 1) + "I"
            End If
            If (Trim(Mid(PCat2, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 5, 2) = Mid(PCat2, z + 3, 1) + "L"
            End If
            If (Trim(Mid(PCat2, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 7, 2) = Mid(PCat2, z + 4, 1) + "E"
            End If
        End If
    Next z
End If
If (Trim(PCat4) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat4, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat4, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 9, 2) = Mid(PCat4, z + 1, 1) + "W"
            End If
            If (Trim(Mid(PCat4, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 11, 2) = Mid(PCat4, z + 2, 1) + "X"
            End If
            If (Trim(Mid(PCat4, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 13, 2) = Mid(PCat4, z + 3, 1) + "Y"
            End If
            If (Trim(Mid(PCat4, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 15, 2) = Mid(PCat4, z + 4, 1) + "Z"
            End If
        End If
    Next z
End If
If (Trim(PCat6) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat6, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat6, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 17, 2) = Mid(PCat6, z + 1, 1) + "A"
            End If
            If (Trim(Mid(PCat6, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 19, 2) = Mid(PCat6, z + 2, 1) + "B"
            End If
            If (Trim(Mid(PCat6, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 21, 2) = Mid(PCat6, z + 3, 1) + "C"
            End If
            If (Trim(Mid(PCat6, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 23, 2) = Mid(PCat6, z + 4, 1) + "D"
            End If
        End If
    Next z
End If
If (Trim(PCat8) <> "") Then
    For z = 0 To 191 Step 4
        If (Trim(Mid(PCat8, z + 1, 4)) <> "") Then
            If (Trim(Mid(PCat8, z + 1, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 25, 2) = Mid(PCat8, z + 1, 1) + "M"
            End If
            If (Trim(Mid(PCat8, z + 2, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 27, 2) = Mid(PCat8, z + 2, 1) + "N"
            End If
            If (Trim(Mid(PCat8, z + 3, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 29, 2) = Mid(PCat8, z + 3, 1) + "O"
            End If
            If (Trim(Mid(PCat8, z + 4, 1)) <> "") Then
                Mid(TimeArray((z / 4) + 1 + 48), 31, 2) = Mid(PCat8, z + 4, 1) + "P"
            End If
        End If
    Next z
End If
For z = 1 To 96
    If (Trim(TimeArray(z)) <> "") Then
        lstCat.List(z + 1) = Left(lstCat.List(z + 1), 32) + " " + TimeArray(z)
    End If
Next z
End Function

Private Sub lstCat_Click()
Dim Temp As String
Dim ASlot As String
Dim ATemp As String
Dim ATime As String
Dim TimeSlot As Integer
If (lstCat.ListIndex > 0) Then
    TimeSlot = lstCat.ListIndex
    ATime = Left(lstCat.List(lstCat.ListIndex), 8)
    If (frmCalendar.IsTimeActive(ATime)) Then
        If (Len(lstCat.List(TimeSlot)) > 9) Then
            ASlot = Mid(lstCat.List(TimeSlot), 10, Len(lstCat.List(lstCat.ListIndex)) - 9)
            If (Len(ASlot) < 32) Then
                ASlot = ASlot + Space(32 - Len(ASlot))
            End If
        End If
        If (Trim(ASlot) = "") Then
            ASlot = Space(32)
        End If
        frmNumericPad.NumPad_ACategoryOn = True
        frmNumericPad.NumPad_Field = "Category " + lstCat.List(TimeSlot)
        frmNumericPad.NumPad_Min = 1
        frmNumericPad.NumPad_Max = 9
        frmNumericPad.Show 1
        If Not (frmNumericPad.NumPad_Quit) Then
            If (frmNumericPad.NumPad_Result <> "") And Not (frmNumericPad.NumPad_ChoiceOn9) Then
                Temp = Trim(frmNumericPad.NumPad_Result)
                If (frmNumericPad.NumPad_ChoiceOn1) Then
                    Temp = Temp + "S"
                    Mid(ASlot, 1, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn2) Then
                    Temp = Temp + "I"
                    Mid(ASlot, 3, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn3) Then
                    Temp = Temp + "L"
                    Mid(ASlot, 5, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn4) Then
                    Temp = Temp + "E"
                    Mid(ASlot, 7, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn5) Then
                    Temp = Temp + "W"
                    Mid(ASlot, 9, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn6) Then
                    Temp = Temp + "X"
                    Mid(ASlot, 11, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn7) Then
                    Temp = Temp + "Y"
                    Mid(ASlot, 13, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn8) Then
                    Temp = Temp + "Z"
                    Mid(ASlot, 15, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn10) Then
                    Temp = Temp + "A"
                    Mid(ASlot, 17, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn11) Then
                    Temp = Temp + "B"
                    Mid(ASlot, 19, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn12) Then
                    Temp = Temp + "C"
                    Mid(ASlot, 21, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn13) Then
                    Temp = Temp + "D"
                    Mid(ASlot, 23, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn14) Then
                    Temp = Temp + "M"
                    Mid(ASlot, 25, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn15) Then
                    Temp = Temp + "N"
                    Mid(ASlot, 27, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn16) Then
                    Temp = Temp + "O"
                    Mid(ASlot, 29, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn17) Then
                    Temp = Temp + "P"
                    Mid(ASlot, 31, 2) = Left(Temp, 2)
                Else
                    Temp = ""
                End If
                If (Trim(Temp) <> "") Then
                    lstCat.List(TimeSlot) = Left(lstCat.List(TimeSlot), 8) + " " + ASlot
                    TimeArray(TimeSlot - 1) = ASlot
                End If
            ElseIf (frmNumericPad.NumPad_ChoiceOn9) Then
                lstCat.List(TimeSlot) = Left(lstCat.List(TimeSlot), 8)
                TimeArray(TimeSlot - 1) = Space(32)
            End If
        End If
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdClear_Click
Unload Me
End Sub

