VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPatientChartReqAppts 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstAppDet 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6270
      Left            =   360
      TabIndex        =   0
      Top             =   1320
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   855
      Left            =   360
      TabIndex        =   1
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReqAppts.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   10320
      TabIndex        =   2
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReqAppts.frx":01DF
   End
End
Attribute VB_Name = "frmPatientChartReqAppts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Formon As Boolean
Private Sub cmdDone_Click()
Formon = True
Unload frmPatientChartReqAppts
End Sub

Private Sub cmdHome_Click()
Formon = True
Unload frmPatientChartReqAppts
End Sub
Public Function LoadApptsList(PatId As Long, ApptId As Long, DAccess As Boolean) As Boolean
Dim ASex As String
Dim AName As String
Dim APcp As String
Dim ABirthDate As String
Dim TheDate As String
Dim LocalDate As ManagedDate
Dim ApplList As ApplicationAIList
LoadApptsList = False
Set ApplList = New ApplicationAIList
Set LocalDate = New ManagedDate
DirectAccess = DAccess
lstAppDet.Clear
TriggerItem = ""
ItemSelected = 0
AppointmentId = ApptId
PatientId = PatId
If (PatId > 0) Then
    Call ApplList.ApplGetPatientInfo(PatId, AName, ASex, APcp, ABirthDate)
    TheDate = ""
    If (Trim(StartDate) <> "") Then
        LocalDate.ExposedDate = StartDate
        If (LocalDate.ConvertDisplayDateToManagedDate) Then
            TheDate = LocalDate.ExposedDate
        End If
    Else
        Call FormatTodaysDate(TheDate, False)
        TheDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
    End If
    ApplList.ApplDate = TheDate
    ApplList.ApplPatId = PatId
    ApplList.ApplLocId = -1
    ApplList.ApplResourceId = -1
    ApplList.ApplTransactionStatus = "D"
    Set ApplList.ApplList = lstAppDet
    ApplList.ApplStartAt = 0
    ApplList.ApplReturnCount = 0
    If ApplList.ApplRetrieveReviewApptsList(False) > 0 Then
        LoadApptsList = True
    End If
End If
Set LocalDate = Nothing
Set ApplList = Nothing
End Function
Private Sub lstAppDet_dblClick()
Dim PatId As Long
Dim ApptId As Long
Dim ReferId As Long
Dim AQuestion As String
Dim DateNow As String
Dim RetTrn As PracticeTransactionJournal
Dim ApplList As ApplicationAIList
If (lstAppDet.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
     DateNow = ""
    Call FormatTodaysDate(DateNow, False)
    DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
    If (ApplList.ApplGetStatus(lstAppDet.List(lstAppDet.ListIndex)) = "D") Then
        ApptId = ApplList.ApplGetAppointmentId(lstAppDet.List(lstAppDet.ListIndex))
        PatId = ApplList.ApplGetPatientId(lstAppDet.List(lstAppDet.ListIndex))
        ReferId = ApplList.ApplGetReferralId(lstAppDet.List(lstAppDet.ListIndex))
        AQuestion = ApplList.ApplGetQuestion(lstAppDet.List(lstAppDet.ListIndex))
        Set ApplList = Nothing
        Set RetTrn = New PracticeTransactionJournal
        RetTrn.TransactionJournalId = 0
        If (RetTrn.RetrieveTransactionJournal) Then
            RetTrn.TransactionJournalTypeId = ApptId
            RetTrn.TransactionJournalType = "P"
            RetTrn.TransactionJournalStatus = "P"
            RetTrn.TransactionJournalDate = DateNow
            RetTrn.TransactionJournalRcvrId = UserLogin.iId
            RetTrn.TransactionJournalTime = 0
            RetTrn.TransactionJournalAssign = 0
            'RetTrn.TransactionJournalRemark = DateNow
            Call RetTrn.ApplyTransactionJournal
            TransId = RetTrn.TransactionJournalId
            Set RetTrn = Nothing
            Call cmdDone_Click
            Formon = False
        End If
    Else
        frmEventMsgs.Header = "Appointment did not occur"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        lstAppts.ListIndex = -1
        Set ApplList = Nothing
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
