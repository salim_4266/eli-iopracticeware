VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmSelectObject 
   BackColor       =   &H0077742D&
   Caption         =   "Select Object"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   ClipControls    =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtBillDr 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   840
      Width           =   4455
   End
   Begin VB.ComboBox lstObject 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   4455
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   750
      Left            =   3240
      TabIndex        =   1
      Top             =   2160
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectObject.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   750
      Left            =   120
      TabIndex        =   2
      Top             =   2160
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectObject.frx":01DF
   End
End
Attribute VB_Name = "frmSelectObject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ResultSelection As String
Public Header As String
Private DisplayType As String
Private ReceivableId As Long
Private PatientId As Long
Private InsurerId As Long

Private Sub cmdDone_Click()
If (lstObject.ListIndex >= 0) Then
    If (DisplayType = "I") Then
        ResultSelection = lstObject.List(lstObject.ListIndex)
    ElseIf (DisplayType = "9") Then
        If (lstObject.List(lstObject.ListIndex) = "No Reason") Then
            ResultSelection = "-1"
        Else
            ResultSelection = Trim(Str(lstObject.ListIndex))
        End If
    ElseIf (DisplayType = "B") Then
        ResultSelection = txtBillDr.Text
    End If
End If
Unload frmSelectObject
End Sub

Private Sub cmdQuit_Click()
Unload frmSelectObject
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Unload frmSelectObject
End Sub

Public Function LoadSelectableObject(RcvId As Long, PatId As Long, InsId As Long, PolId1 As Long, PolId2 As Long, InsType As String, DType As String) As Boolean
Dim ApptId As Long
Dim TheDate As String
Dim b1 As Boolean, b2 As Boolean
Dim r1 As String, r2 As String, n1 As String
Dim l1 As String, l2 As String, l3 As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
LoadSelectableObject = False
frmSelectObject.Caption = Header
lstObject.Clear
lstObject.Visible = True
txtBillDr.Visible = False
ResultSelection = ""
DisplayType = DType
If (RcvId > 0) And (PatId > 0) Then
    If (DType = "I") Then
        Set ApplTemp = New ApplicationTemplates
        If (Trim(InsType) = "") Then
            InsType = "M"
        End If
        TheDate = ApplTemp.ApplGetReceivableDate(RcvId, PatId, InsId)
        If (Trim(TheDate) <> "") Then
            Call ApplTemp.ApplGetAllPayers(PatId, n1, InsId, b1, r1, PolId2, b2, r2, InsType, TheDate, lstObject, l1, l2, l3, True, True, True, True)
            If (lstObject.ListCount < 1) Or (InsId <> PolId1) Then
                If (PolId1 <> PatId) Then
                    Call ApplTemp.ApplGetAllPayers(PatId, n1, PolId1, b1, r1, PolId2, b2, r2, InsType, TheDate, lstObject, l1, l2, l3, True, False, True, True)
                Else
                    Call ApplTemp.ApplGetAllPayers(PatId, n1, PolId1, b1, r1, PolId2, b2, r2, InsType, TheDate, lstObject, l1, l2, l3, True, False, True, True)
                End If
                If (lstObject.ListCount < 1) Then
                    Call ApplTemp.ApplGetAllPayers(PatId, n1, PolId2, b2, r2, 0, b1, r1, InsType, TheDate, lstObject, l1, l2, l3, True, False, True, True)
                End If
            End If
        End If
        Set ApplTemp = Nothing
        If (lstObject.ListCount > 0) Then
            LoadSelectableObject = True
        End If
    ElseIf (DType = "B") Then
        txtBillDr.Visible = True
        lstObject.Visible = False
        LoadSelectableObject = True
    ElseIf (DType = "9") Then
        Set ApplList = New ApplicationAIList
        lstObject.AddItem "Litigation"
        lstObject.AddItem "Payer Delays"
        lstObject.AddItem "Eligibility Delays"
        lstObject.AddItem "Rejection or Denial"
        lstObject.AddItem "Admin Delay by State"
        lstObject.AddItem "Interrupted Maternity care"
        lstObject.AddItem "Letter attached"
        lstObject.AddItem "No Reason"
        lstObject.ListIndex = ApplList.ApplGet90Day(RcvId)
        Set ApplList = Nothing
        LoadSelectableObject = True
    End If
End If
End Function

Private Sub txtBillDr_Click()
Call txtBillDr_KeyPress(0)
End Sub

Private Sub txtBillDr_KeyPress(KeyAscii As Integer)
KeyAscii = 0
frmSelectDialogue.InsurerSelected = Trim(Str(InsurerId))
Call frmSelectDialogue.BuildSelectionDialogue("RESOURCEWITHAFF")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtBillDr.Text = frmSelectDialogue.Selection
End If
End Sub
Public Sub FrmClose()
cmdQuit_Click
Unload Me
End Sub

