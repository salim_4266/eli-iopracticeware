VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmListLetters 
   BackColor       =   &H0073702B&
   BorderStyle     =   0  'None
   Caption         =   "Document List"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   9960
      MultiSelect     =   1  'Simple
      TabIndex        =   1
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "ListLetters.frx":0000
      Left            =   4560
      List            =   "ListLetters.frx":000D
      TabIndex        =   13
      Top             =   360
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstPrint 
      Height          =   645
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   1800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   5760
      MultiSelect     =   1  'Simple
      TabIndex        =   3
      Top             =   360
      Width           =   2535
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   8400
      MultiSelect     =   1  'Simple
      TabIndex        =   2
      Top             =   360
      Width           =   1455
   End
   Begin VB.ListBox lstDocs 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6060
      ItemData        =   "ListLetters.frx":0025
      Left            =   240
      List            =   "ListLetters.frx":0027
      MultiSelect     =   1  'Simple
      TabIndex        =   0
      Top             =   1320
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   1095
      Left            =   2880
      TabIndex        =   8
      Top             =   7440
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":0029
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   1680
      TabIndex        =   9
      Top             =   7440
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":0208
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   10080
      TabIndex        =   10
      Top             =   7440
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":03E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLetters 
      Height          =   1110
      Left            =   8520
      TabIndex        =   11
      Top             =   7440
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":05C6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   1095
      Left            =   6960
      TabIndex        =   12
      Top             =   7440
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":07AE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSend 
      Height          =   1110
      Left            =   5520
      TabIndex        =   14
      Top             =   7440
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":099B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1110
      Left            =   240
      TabIndex        =   15
      Top             =   7440
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":0B7E
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   240
      TabIndex        =   16
      Top             =   840
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   7828525
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   375
      Left            =   2640
      TabIndex        =   17
      Top             =   840
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   7828525
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1110
      Left            =   4080
      TabIndex        =   19
      Top             =   7440
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListLetters.frx":0D5D
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   2160
      TabIndex        =   18
      Top             =   840
      Width           =   345
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   240
      TabIndex        =   7
      Top             =   480
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.Label lblPage 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0073702B&
      Caption         =   "Page Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9600
      TabIndex        =   5
      Top             =   8640
      Width           =   1935
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Review Consult Letters"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   2925
   End
End
Attribute VB_Name = "frmListLetters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TransType As String
Public TransStatus As String

Private Const recsPerPage As Integer = 25
Private AutoLoadOn As Boolean
Private TriggerOn As Boolean
Private PendTrans As PendingTrans
Private StartDate As String
Private EndDate As String

Private Sub cmdAll_Click()
Dim k As Integer
Dim TransId As Long
Dim AllOfThem As Long
Dim SDate As String
Dim EDate As String
Dim ADisplay As String
Dim DRFilter As String
Dim LocFilter As String
Dim StatusFilter As String
If (lstDocs.ListCount > 0) Then
    frmEventMsgs.Header = "Do you want to create all the letters ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        AllOfThem = PendTrans.GetAllRecords
        If (AllOfThem > 0) Then
            Call FilterStatus(StatusFilter)
            Call FilterLocs(LocFilter)
            Call FilterDocs(DRFilter)
            SDate = Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2)
            EDate = Mid(EndDate, 7, 4) + Mid(EndDate, 1, 2) + Mid(EndDate, 4, 2)
            PendTrans.PendType = TransType
            Call PendTrans.RetrieveList(AllOfThem, StatusFilter, DRFilter, LocFilter, SDate, EDate)
            While Not (PendTrans.EOF)
                ADisplay = PendTrans.DisplayList
                TransId = val(Trim(Mid(ADisplay, 98, 10)))
                Call MaintainLetter(TransId, TransType, "E", True)
                For k = 0 To 250
                    DoEvents
                Next k
                Call PendTrans.NextRec
            Wend
        End If
    End If
End If
End Sub

Private Sub cmdDisplay_Click()
PendTrans.CurrentPage = 1
Call LoadDocList(False)
End Sub

Private Sub cmdDone_Click()
If (Label1.Caption = "Review Appointments") Then
    Label1.Caption = "Review Consult Letters"
    If (TransType = "F") Then
        Label1.Caption = "Review Referral Letters"
    End If
    cmdLetters.Visible = True
    cmdAll.Visible = True
Else
    Set PendTrans = Nothing
    Unload frmListLetters
End If
End Sub

Private Sub cmdHome_Click()
Set PendTrans = Nothing
Unload frmListLetters
End Sub

Private Sub cmdLetters_Click()
Dim PatId As Long
Dim TotalFound As Integer
Dim ApplLetters As Letters
Dim ReturnArguments As Variant

Set ReturnArguments = DisplayPatientSearchScreen(False)
If Not (ReturnArguments Is Nothing) Then PatId = ReturnArguments.PatientId
If (PatId > 0) Then
    lstDocs.Clear
    Label1.Caption = "Review Appointments"
    cmdLetters.Visible = False
    Set ApplLetters = New Letters
    Set ApplLetters.ApplList = lstDocs
    ApplLetters.ApplDate = ""
    ApplLetters.ApplPatId = PatId
    TotalFound = ApplLetters.ApplRetrieveLettersList
    Set ApplLetters = Nothing
    If (TotalFound < 1) Then
        frmEventMsgs.Header = "No Appointments"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Call cmdDone_Click
    Else
        cmdLetters.Visible = False
        cmdAll.Visible = False
        lblPage.Visible = False
        If (TotalFound <= 25) Then
            cmdPrev.Visible = False
            cmdNext.Visible = False
        Else
            cmdPrev.Visible = True
            cmdNext.Visible = True
        End If
    End If
End If
End Sub

Private Sub cmdNext_Click()
If (PendTrans.LastPage) Then
    PendTrans.CurrentPage = 1
Else
    PendTrans.CurrentPage = PendTrans.CurrentPage + 1
End If
Call LoadDocList(False)
End Sub

Private Sub cmdPrev_Click()
If (PendTrans.CurrentPage = 1) Then
    PendTrans.CurrentPage = PendTrans.MaxPage
Else
    PendTrans.CurrentPage = PendTrans.CurrentPage - 1
End If
Call LoadDocList(False)
End Sub

Private Sub cmdSend_Click()
Dim i As Integer
Dim ErrNum As Integer
Dim AllOn As Boolean
Dim ApplTemp As ApplicationTemplates
AllOn = False
If (lstDocs.ListCount > 0) And (lstType.ListIndex >= 0) Then
    If (lstDocs.SelCount > 0) Then
        frmEventMsgs.Header = "Submit All Selected Items ?"
    Else
        frmEventMsgs.Header = "Submit All Items ?"
        AllOn = True
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Submit"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 4) Then
        Exit Sub
    End If
    If (AllOn) Then
        For i = 0 To lstDocs.ListCount - 1
            lstDocs.Selected(i) = True
        Next i
    End If
    ErrNum = 0
    If (lstType.ListIndex = 1) Then
        ErrNum = SendViaFax("", "P", "")
    End If
    If (ErrNum = 0) Then
        DoEvents
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBox = lstPrint
        Set ApplTemp.lstBoxA = lstDocs
        If (lstType.ListIndex = 0) Then
            ErrNum = ApplTemp.ApplTransmitLetters(AllOn, TransType, "E")
        ElseIf (lstType.ListIndex = 1) Then
            ErrNum = ApplTemp.ApplTransmitLetters(AllOn, TransType, "F")
        ElseIf (lstType.ListIndex = 2) Then
            ErrNum = ApplTemp.ApplPrintLetters(AllOn, TransType, "P")
        Else
            ErrNum = -104
        End If
        Set ApplTemp = Nothing
    End If
    frmEventMsgs.Header = ""
    If (ErrNum = -1) Then
        frmEventMsgs.Header = "Incorrect Configuration Parameters"
    ElseIf (ErrNum = -2) Then
        frmEventMsgs.Header = "Bad To Address"
    ElseIf (ErrNum = -3) Then
        frmEventMsgs.Header = "Cannot attach the file"
    ElseIf (ErrNum = -4) Then
        frmEventMsgs.Header = "Could not connect to SMTP Server"
    ElseIf (ErrNum = -5) Then
        frmEventMsgs.Header = "No From Address"
    ElseIf (ErrNum = -6) Then
        frmEventMsgs.Header = "Could not write message to SMTP Server"
    ElseIf (ErrNum = -7) Then
        frmEventMsgs.Header = "Transmission Failed"
    ElseIf (ErrNum = -100) Then
        frmEventMsgs.Header = "Access not available"
    ElseIf (ErrNum = -102) Then
        frmEventMsgs.Header = "Feature Coming Soon"
    ElseIf (ErrNum = -103) Then
        frmEventMsgs.Header = "No Fax available"
    ElseIf (ErrNum = -104) Then
        frmEventMsgs.Header = "Nothing to transmit"
    ElseIf (ErrNum <> 0) Then
        frmEventMsgs.Header = "General Connectivity Error"
    Else
        frmEventMsgs.Header = "Submit Completed"
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Call LoadDocList(False)
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") Then
        EndDate = SSDateCombo1.DateValue
    Else
        ATemp = EndDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        EndDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(EndDate, False)
        If (EndDate = "") Then
            Call FormatTodaysDate(EndDate, False)
        End If
        SSDateCombo1.Text = EndDate
    End If
    Call FormatTodaysDate(EndDate, False)
    If (EndDate = "") Then
        SSDateCombo1.Text = ""
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        EndDate = EndDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(EndDate) > 0) Then
        EndDate = Left(EndDate, Len(StartDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(EndDate) < 1) Then
        EndDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo1_Validate(Cancel As Boolean)
Call SSDateCombo1_KeyPress(13)
End Sub

Private Sub SSDateCombo2_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo2.Date) <> "") Then
        StartDate = SSDateCombo2.DateValue
    Else
        ATemp = StartDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        StartDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(StartDate, False)
        If (StartDate = "") Then
            Call FormatTodaysDate(StartDate, False)
        End If
        SSDateCombo1.Text = StartDate
    End If
    Call FormatTodaysDate(StartDate, False)
    If (StartDate = "") Then
        SSDateCombo1.Text = ""
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        StartDate = StartDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(StartDate) > 0) Then
        StartDate = Left(StartDate, Len(StartDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(StartDate) < 1) Then
        StartDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo2_Validate(Cancel As Boolean)
Call SSDateCombo2_KeyPress(13)
End Sub

Private Sub Form_Load()
Dim ADate As String
Set PendTrans = New PendingTrans
AutoLoadOn = False
AutoLoadOn = CheckConfigCollection("AUTOLOADDISPLAY") = "T"
lstType.Visible = False
cmdAll.Visible = True
cmdSend.Visible = False
cmdLetters.Visible = True
TransType = Trim(TransType)
If (InStrPS("SF", TransType) = 0) Then
    TransType = "S"
End If
Label1.Caption = "Review Consult Letters"
If (TransType = "F") Then
    Label1.Caption = "Review Referral Letters"
End If
If (TransStatus = "B") Then
    Label1.Caption = "Submit Consult Letters"
    If (TransType = "F") Then
        Label1.Caption = "Submit Referral Letters"
    End If
    cmdAll.Visible = False
    cmdSend.Visible = True
    cmdLetters.Visible = False
    lstType.ListIndex = -1
    lstType.Visible = True
End If
TriggerOn = True
Call LoadStaffList
'lstDr.Selected(0) = True
TriggerOn = True
Call LoadLocList
'lstLoc.Selected(0) = True
TriggerOn = True
Call LoadStatusList
'lstStatus.Selected(0) = True
PendTrans.CurrentPage = 1
ADate = ""
Call FormatTodaysDate(ADate, False)
StartDate = ADate
EndDate = ADate
SSDateCombo1.DateValue = StartDate
SSDateCombo2.DateValue = EndDate
Call LoadDocList(True)
End Sub

Private Sub LoadStaffList()
Dim staffFilter As ListFilters
lstDr.Visible = True
lstDr.Clear
lstDr.AddItem ("All Doctors")
Set staffFilter = New ListFilters
staffFilter.ListType = "R"
Call staffFilter.PopList(lstDr)
Set staffFilter = Nothing
End Sub

Private Sub LoadLocList()
Dim LocFilter As ListFilters
lstLoc.Visible = True
lstLoc.Clear
lstLoc.AddItem ("All Locations")
Set LocFilter = New ListFilters
LocFilter.ListType = "L"
Call LocFilter.PopList(lstLoc)
Set LocFilter = Nothing
End Sub

Private Sub LoadStatusList()
Dim StatusFilter As ListFilters
lstStatus.Visible = True
lstStatus.Clear
lstStatus.AddItem ("All Status")
Set StatusFilter = New ListFilters
StatusFilter.ListType = "S"
Call StatusFilter.PopList(lstStatus)
Set StatusFilter = Nothing
End Sub

Private Function FilterStatus(StatusFilter) As Boolean
Dim i As Integer
StatusFilter = ""
If (lstStatus.Selected(0) Or lstStatus.SelCount = 0) Then
    For i = 1 To lstStatus.ListCount - 1
        StatusFilter = StatusFilter + Mid(lstStatus.List(i), 1, 1) + ","
    Next i
Else
    For i = 1 To lstStatus.ListCount - 1
        If (lstStatus.Selected(i)) Then
            StatusFilter = StatusFilter + Mid(lstStatus.List(i), 1, 1) + ","
        End If
    Next i
End If
If (InStrPS(1, StatusFilter, "N") > 0) Then
    StatusFilter = "'" + StatusFilter + "'"
Else
    StatusFilter = "'" + Mid(StatusFilter, 1, Len(StatusFilter) - 1) + "'"
End If
End Function

Private Function FilterLocs(LocFilter) As Boolean
Dim i As Integer
LocFilter = ""
If (lstLoc.Selected(0) Or lstLoc.SelCount = 0) Then
    For i = 1 To lstLoc.ListCount - 1
        If (Mid(lstLoc.List(i), 21, 1) = "P") Then
            If (Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) = "0") Then
                LocFilter = LocFilter + "0 "
            Else
                LocFilter = LocFilter + Trim(Str(1000 + CLng(Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21))))) + " "
            End If
        ElseIf (Mid(lstLoc.List(i), 21, 1) = "R") Then
            LocFilter = LocFilter + Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) + " "
        End If
    Next i
Else
    For i = 1 To lstLoc.ListCount - 1
        If (lstLoc.Selected(i)) Then
            If (Mid(lstLoc.List(i), 21, 1) = "P") Then
                If (Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) = "0") Then
                    LocFilter = LocFilter + "0 "
                Else
                    LocFilter = LocFilter + Trim(Str(1000 + CLng(Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21))))) + " "
                End If
            ElseIf (Mid(lstLoc.List(i), 21, 1) = "R") Then
                LocFilter = LocFilter + Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) + " "
            End If
        End If
    Next i
End If
LocFilter = "'" + Mid(LocFilter, 1, Len(LocFilter) - 1) + "'"
End Function

Private Function FilterDocs(DRFilter As String) As Boolean
Dim i As Integer
DRFilter = ""
If (lstDr.Selected(0) Or lstDr.SelCount = 0) Then
    For i = 1 To lstDr.ListCount - 1
        DRFilter = DRFilter + Mid(lstDr.List(i), 21, Len(lstDr.List(i)) - 20) + " "
    Next i
Else
    For i = 1 To lstDr.ListCount - 1
        If (lstDr.Selected(i)) Then
            DRFilter = DRFilter + Mid(lstDr.List(i), 21, Len(lstDr.List(i)) - 20) + " "
        End If
    Next i
End If
DRFilter = "'" + Mid(DRFilter, 1, Len(DRFilter) - 1) + "'"
End Function

Private Sub LoadDocList(InitOn As Boolean)
Dim Temp As String
Dim ATemp As String
Dim SDate As String
Dim EDate As String
Dim DRFilter As String
Dim LocFilter As String
Dim StatusFilter As String

Label1.Visible = True
lstStatus.Visible = True
lstLoc.Visible = True
lstDocs.Visible = True
cmdPrev.Visible = True
cmdNext.Visible = True
cmdDone.Visible = True
lblPage.Visible = True

Call FilterDocs(DRFilter)
Call FilterLocs(LocFilter)
Call FilterStatus(StatusFilter)
If (InitOn) Then
    If (AutoLoadOn) Then    ' Set Date and Defaults
        Temp = CheckConfigCollection("REVIEWWINDOW")
        If (Temp <> "") Then
            Call AdjustDate(EndDate, -1 * val(Temp), ATemp)
            SSDateCombo1.DateValue = ATemp
            EndDate = ATemp
        End If
    End If
End If

PendTrans.PendStatus = TransStatus
PendTrans.PendType = TransType
SDate = Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2)
EDate = Mid(EndDate, 7, 4) + Mid(EndDate, 1, 2) + Mid(EndDate, 4, 2)
Call PendTrans.RetrieveList(recsPerPage, StatusFilter, DRFilter, LocFilter, SDate, EDate)
lstDocs.Clear
While Not (PendTrans.EOF)
    lstDocs.AddItem (PendTrans.DisplayList)
    Call PendTrans.NextRec
Wend
lblPage.Caption = ("Page " + Str(PendTrans.CurrentPage) + " of " + Str(PendTrans.MaxPage))
DoEvents
End Sub

Private Sub lstDr_Click()
Dim i As Integer
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        If (lstDr.Selected(0)) Then
            For i = 1 To lstDr.ListCount - 1
                lstDr.Selected(i) = False
            Next i
        End If
    ElseIf (lstDr.ListIndex > 0) Then
        lstDr.Selected(0) = False
    End If
    If Not (TriggerOn) Then
        PendTrans.CurrentPage = 1
        Call LoadDocList(False)
    Else
        TriggerOn = False
    End If
End If
End Sub

Private Sub lstLoc_Click()
Dim i As Integer
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        If (lstLoc.Selected(0)) Then
            For i = 1 To lstLoc.ListCount - 1
                lstLoc.Selected(i) = False
            Next i
        End If
    ElseIf (lstLoc.ListIndex > 0) Then
        lstLoc.Selected(0) = False
    End If
    If Not (TriggerOn) Then
        PendTrans.CurrentPage = 1
        Call LoadDocList(False)
    Else
        TriggerOn = False
    End If
End If
End Sub

Private Sub lstStatus_Click()
Dim i As Integer
If (lstStatus.ListIndex >= 0) Then
    If (lstStatus.ListIndex = 0) Then
        If (lstStatus.Selected(0)) Then
            For i = 1 To lstStatus.ListCount - 1
                lstStatus.Selected(i) = False
            Next i
        End If
    ElseIf (lstStatus.ListIndex > 0) Then
        lstStatus.Selected(0) = False
    End If
    If Not (TriggerOn) Then
        PendTrans.CurrentPage = 1
        Call LoadDocList(False)
    Else
        TriggerOn = False
    End If
End If
End Sub

Private Sub lstDocs_dblClick()
Dim TheRef As String
Dim a3 As String
Dim TransId As Long
Dim p1 As Long
Dim PatId As Long, ApptId As Long
Dim ApplLetters As Letters
If (lstDocs.ListIndex >= 0) Then
    If (TransStatus = "P") Then
        If (Trim(Label1.Caption) <> "Review Appointments") Then
            frmEventMsgs.Header = "Action ?"
            frmEventMsgs.AcceptText = "Send"
            frmEventMsgs.RejectText = "Edit"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = "Delete"
            frmEventMsgs.Other1Text = "Status"
            frmEventMsgs.Other2Text = "Select Recipient"
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = "Patient Info"
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                If (SendLetter(lstDocs.ListIndex, TransType)) Then
                   Call LoadDocList(False)
                End If
            ElseIf (frmEventMsgs.Result = 2) Then
                TransId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 98, 10)))
                Call MaintainLetter(TransId, TransType, "E", False)
            ElseIf (frmEventMsgs.Result = 3) Then
                TransId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 98, 10)))
                Call MaintainLetter(TransId, TransType, "D", False)
            ElseIf (frmEventMsgs.Result = 5) Then
                Call MaintainStatus(lstDocs, TransType)
            ElseIf (frmEventMsgs.Result = 6) Then
                TransId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 98, 10)))
                Call SelectParty(lstDocs.ListIndex, TransType)
            ElseIf (frmEventMsgs.Result = 8) Then
                PatId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 108, 10)))
                Dim PatientDemographics As New PatientDemographics
                PatientDemographics.PatientId = PatId
                If Not PatientDemographics.DisplayPatientInfoScreen Then Exit Sub
            End If
        Else
            TheRef = "F"
            If (TransType = "S") Then
                TheRef = "C"
            End If
            Set ApplLetters = New Letters
            ApptId = ApplLetters.ApplLettersApptId(lstDocs.List(lstDocs.ListIndex))
            PatId = ApplLetters.ApplLettersPatientId(lstDocs.List(lstDocs.ListIndex))
            If (GetDocumentComponents(PatId, TheRef, a3, p1)) Then
                Call ApplLetters.ApplPostLetter(PatId, ApptId, Trim(a3), "", p1, TransType)
            End If
            Set ApplLetters = Nothing
            Label1.Caption = "Review Consult Letters"
            If (TransType = "F") Then
                Label1.Caption = "Review Referral Letters"
            End If
            cmdLetters.Visible = True
            cmdAll.Visible = True
            lblPage.Visible = True
            Call LoadDocList(False)
        End If
    End If
End If
End Sub

Private Function MaintainLetter(TransId As Long, IType As String, ActionType As String, AutoCreateOn As Boolean) As Boolean
Dim u As Integer
Dim TagOn As Boolean
Dim PatId As Long
Dim Temp As String, KTemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
TagOn = True
If (TransId > 0) Then
    Set ApplLetters = New Letters
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    If (ActionType = "E") Then
        Temp = ApplLetters.ApplLettersFile(TransId)
        If (Trim(Temp) = "-") Then
            KTemp = IType
            If (KTemp = "[") Then
                KTemp = "a"
            ElseIf (KTemp = "]") Then
                KTemp = "d"
            End If
            If (FM.IsFileThere(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")) Then
                Call ApplTemp.ApplAdjustTransmitStatus(TransId, "")
                Call TriggerWord(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")
                Call InsertLog("FrmListLetters.frm", "Viewed Letter " & CStr(TransId) & "", LoginCatg.PatientLetterInsert, "TransactionId", CStr(TransId), 0, 0)
            Else
                frmEventMsgs.Header = "This document has already been generated and not yet received in your office. Please try in 15 minutes."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
        ElseIf (Trim(Temp) <> "") Then
            KTemp = IType
            If (KTemp = "[") Then
                KTemp = "a"
            ElseIf (KTemp = "]") Then
                KTemp = "d"
            End If
            If Not (FM.IsFileThere(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")) Then
                If (ApplTemp.ApplIsTransmitStatusLocked(TransId)) Then
                    frmEventMsgs.Header = "This document has already been generated and not yet received in your office. Please try in 15 minutes."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    Exit Function
                Else
                    lblPrint.Caption = "File Creation In Progress " + Trim(Str(TransId))
                    lblPrint.Visible = True
                    DoEvents
                    Call ApplTemp.ApplAdjustTransmitStatus(TransId, "*")
                    Set ApplTemp.lstBox = lstPrint
                    Call InsertLog("FrmListLetters.frm", "Created New Letter " & CStr(TransId) & "", LoginCatg.PatientLetterInsert, "TransactionId", CStr(TransId), 0, 0)
                    Call ApplTemp.PrintTransaction(TransId, 2, KTemp, True, True, Temp, False, 0, "S", "", 0)
                    TagOn = False
                End If
            End If
            lblPrint.Visible = False
            DoEvents
            If Not (AutoCreateOn) Then
                If (TagOn) Then
' if the person who writes the letter re-reads before Unison can synchonize the file, then
' we will have defeated the purpose of this access control tag.
                    Call ApplTemp.ApplAdjustTransmitStatus(TransId, "")
                    Call InsertLog("FrmListLetters.frm", "Viewed Letter " & CStr(TransId) & "", LoginCatg.PatientLettersView, "TransactionId", CStr(TransId), 0, 0)
                End If
                Call TriggerWord(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")
            End If
        End If
    ElseIf (ActionType = "D") Then
        Temp = ApplLetters.ApplLettersFile(TransId)
        Call ApplTemp.ApplPurgeTransaction(TransId, True, False)
        Call LoadDocList(False)
    End If
    Set ApplLetters = Nothing
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function MaintainStatus(AList As ListBox, IType As String) As Boolean
Dim w As Integer
Dim TransId As Long
Dim Temp As String
Dim ATemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("LETTERSTATUS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Temp = Left(frmSelectDialogue.Selection, 1)
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    For w = 0 To AList.ListCount - 1
        If (AList.Selected(w)) Or (AList.ListIndex = w) Then
            TransId = Trim(Mid(AList.List(w), 98, 10))
            If (TransId > 0) Then
                Call ApplList.ApplSetLetterStatus(TransId, Temp)
                ATemp = AList.List(w)
                Mid(ATemp, 1, 1) = Temp
                AList.List(w) = ATemp
            End If
        End If
    Next w
    For w = 0 To AList.ListCount - 1
        AList.Selected(w) = False
    Next w
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function SendLetter(idx As Integer, IType As String) As Boolean
Dim q As Integer
Dim TransId As Long
Dim Temp As String
Dim FileName As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
SendLetter = False
If (idx >= 0) Then
    Set ApplLetters = New Letters
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    TransId = Trim(Mid(lstDocs.List(idx), 98, 10))
    FileName = DocumentDirectory + "Ltrs-" + TransType + Trim(Str(TransId)) + ".doc"
    If Not (FM.IsFileThere(FileName)) Then
        frmEventMsgs.Header = "File must exist before transmitting"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplLetters = Nothing
        Set ApplTemp = Nothing
        Set ApplList = Nothing
        Exit Function
    End If
    frmEventMsgs.Header = "Send Letter How ?"
    frmEventMsgs.AcceptText = "E-Mail"
    frmEventMsgs.RejectText = "Fax"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Print"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (ApplList.ApplIsEmailOkay(TransId)) Then
            Call ApplList.ApplSetTransactionStatus(TransId, "B")
            Call ApplList.ApplSetTransactionAction(TransId, "E")
            SendLetter = True
        Else
            frmEventMsgs.Header = "No e-mail address"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (ApplList.ApplIsFaxOkay(TransId)) Then
            Call ApplList.ApplSetTransactionStatus(TransId, "B")
            Call ApplList.ApplSetTransactionAction(TransId, "F")
            SendLetter = True
        Else
            frmEventMsgs.Header = "No Fax Number"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Call ApplList.ApplSetTransactionStatus(TransId, "B")
        Call ApplList.ApplSetTransactionAction(TransId, "P")
        SendLetter = True
    End If
    Set ApplLetters = Nothing
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function SelectParty(idx As Integer, IType As String) As Boolean
Dim q As Integer
Dim GotIt As Boolean
Dim Temp As String
Dim Ref As String, TempRef As String
Dim ATemp As String, VndName As String
Dim FaxNumber As String, EMailAddress As String
Dim VndId As Long, VndIdOrg As Long, TransId As Long
Dim PermOn As Boolean, IsDoctor As Boolean
Dim ApplList As ApplicationAIList
SelectParty = False
If (idx >= 0) Then
    Set ApplList = New ApplicationAIList
    TransId = Trim(Mid(lstDocs.List(idx), 98, 10))
    GotIt = False
    IsDoctor = False
    frmSelectDialogue.InsurerSelected = ""
    If (IType = "Y") Then
        Call frmSelectDialogue.BuildSelectionDialogue("HOSPITALS")
        frmSelectDialogue.Show 1
        GotIt = frmSelectDialogue.SelectionResult
        If (GotIt) Then
            Temp = Trim(frmSelectDialogue.Selection)
        End If
    ElseIf (IType = "X") Then
        Call frmSelectDialogue.BuildSelectionDialogue("LABS")
        frmSelectDialogue.Show 1
        GotIt = frmSelectDialogue.SelectionResult
        If (GotIt) Then
            Temp = Trim(frmSelectDialogue.Selection)
        End If
    Else
        frmEventMsgs.Header = "Select Party"
        frmEventMsgs.AcceptText = "PCP"
        frmEventMsgs.RejectText = "Referring Doctor"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Select Doctor"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            GotIt = ApplList.ApplGetPartyFromTransaction(TransId, "P", Temp)
        ElseIf (frmEventMsgs.Result = 2) Then
            GotIt = ApplList.ApplGetPartyFromTransaction(TransId, "R", Temp)
        ElseIf (frmEventMsgs.Result = 3) Then
            IsDoctor = True
            Call frmSelectDialogue.BuildSelectionDialogue("PCP")
            frmSelectDialogue.Show 1
            GotIt = frmSelectDialogue.SelectionResult
            If (frmSelectDialogue.Selection = "Create Doctor") Then
                GotIt = False
                frmVendors.TheType = "D"
                frmVendors.DeleteOn = False
                If (frmVendors.VendorLoadDisplay(0, PermOn)) Then
                    frmVendors.Show 1
                    If (frmVendors.TheVendorId > 0) Then
                        Temp = Space(56)
                        Temp = Temp + Trim(Str(frmVendors.TheVendorId))
                        GotIt = True
                    End If
                End If
            Else
                If (GotIt) Then
                    Temp = Trim(frmSelectDialogue.Selection)
                    q = InStrPS(Temp, " ")
                    If (q > 0) Then
                        FaxNumber = Trim(Mid(Temp, q, 55 - (q - 1))) + " " + Left(Temp, q - 1)
                        Temp = FaxNumber + Space(55 - Len(FaxNumber)) + Mid(Temp, 56, Len(Temp) - 55)
                        FaxNumber = ""
                    End If
                End If
            End If
        End If
    End If
    If (GotIt) Then
        VndId = val(Mid(Temp, 57, Len(Temp) - 56))
        If IsDoctor Then VndId = val(Mid(Temp, 56, Len(Temp) - 55))
        Call ApplList.ApplGetVendorTransmitInfo(VndId, EMailAddress, FaxNumber, VndName)
        If (ApplList.ApplGetTransactionReference(TransId, Ref, VndIdOrg)) Then
            Ref = ""
            Call ApplList.ApplSetTransactionReference(TransId, Ref, VndId)
            ATemp = lstDocs.List(idx)
            If (Len(VndName) > 23) Then
                VndName = Left(VndName, 23)
            Else
                VndName = VndName + Space(23 - Len(VndName))
            End If
            Mid(ATemp, 67, 23) = Left(VndName, 23)
            lstDocs.List(idx) = ATemp
        End If
        SelectParty = True
    End If
    Set ApplList = Nothing
End If
Call LoadDocList(False)
End Function

Private Function GetDocumentComponents(PatId As Long, ARef As String, TheResults As String, VndId As Long) As Boolean
Dim i As Integer
Dim q As Integer
Dim PCPId As Long
Dim RefId As Long
Dim PCPName As String
Dim RefName As String
Dim TempText As String
Dim RetPat As Patient
Dim RetVnd As PracticeVendors
GetDocumentComponents = False
TheResults = ""
VndId = 0
PCPName = ""
PCPId = 0
RefName = ""
RefId = 0
If (PatId > 0) And (Trim(ARef) <> "") Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        If (RetPat.PrimaryCarePhysician > 0) Then
            Set RetVnd = New PracticeVendors
            RetVnd.VendorId = RetPat.PrimaryCarePhysician
            If (RetVnd.RetrieveVendor) Then
                PCPName = Trim(RetVnd.VendorName)
                PCPId = RetVnd.VendorId
            End If
            Set RetVnd = Nothing
        ElseIf (RetPat.PCPDr > 0) Then
            Set RetVnd = New PracticeVendors
            RetVnd.VendorId = RetPat.PCPDr
            If (RetVnd.RetrieveVendor) Then
                PCPName = Trim(RetVnd.VendorName)
                PCPId = RetVnd.VendorId
            End If
            Set RetVnd = Nothing
        End If
        If (RetPat.ReferralDr > 0) Then
            Set RetVnd = New PracticeVendors
            RetVnd.VendorId = RetPat.ReferralDr
            If (RetVnd.RetrieveVendor) Then
                RefName = Trim(RetVnd.VendorName)
                RefId = RetVnd.VendorId
            End If
            Set RetVnd = Nothing
        ElseIf (RetPat.ReferringPhysician > 0) Then
            Set RetVnd = New PracticeVendors
            RetVnd.VendorId = RetPat.ReferringPhysician
            If (RetVnd.RetrieveVendor) Then
                RefName = Trim(RetVnd.VendorName)
                RefId = RetVnd.VendorId
            End If
            Set RetVnd = Nothing
        End If
    End If
    Set RetPat = Nothing
    frmLetters.LetterPCP = ""
    frmLetters.LetterPCP = PCPName
    frmLetters.LetterPCPId = PCPId
    frmLetters.LetterRef = ""
    frmLetters.LetterRef = RefName
    frmLetters.LetterRefId = RefId
    If (frmLetters.LoadLetters(ARef, True)) Then
        frmLetters.Show 1
        If (Trim(frmLetters.LetterName) <> "") Then
            TempText = Trim(frmLetters.LetterName)
            i = InStrPS(frmLetters.LetterDestination, "(")
            If (i > 0) Then
                q = InStrPS(i, frmLetters.LetterDestination, ")")
                If (q > 0) Then
                    VndId = val(Mid(frmLetters.LetterDestination, i + 1, (q - 1) - i))
                End If
            End If
            If (Trim(frmLetters.LetterDestinationCCpcp) <> "") Then
                TempText = TempText + Trim(frmLetters.LetterDestinationCCpcp) + ","
            End If
            If (Trim(frmLetters.LetterDestinationCCref) <> "") Then
                TempText = TempText + Trim(frmLetters.LetterDestinationCCref) + ","
            End If
            If (Trim(frmLetters.LetterDestinationCCoth) <> "") Then
                TempText = TempText + Trim(frmLetters.LetterDestinationCCoth) + ","
            End If
            If (Trim(frmLetters.LetterDestinationCCpat) <> "") Then
                TempText = TempText + "(P)"
            End If
            TempText = Trim(TempText)
            If (Trim(TempText) <> "") Then
                If (Mid(TempText, Len(TempText), 1) = ",") Then
                    TempText = Left(TempText, Len(TempText) - 1)
                End If
            End If
            TheResults = Trim(TempText)
            GetDocumentComponents = True
        End If
    End If
End If
End Function
