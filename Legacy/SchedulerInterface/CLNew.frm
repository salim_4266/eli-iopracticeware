VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmCLNew 
   BackColor       =   &H009B9626&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H009B9626&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox WPrice 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   10080
      MaxLength       =   7
      TabIndex        =   37
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox PeriphCurve 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6480
      MaxLength       =   7
      TabIndex        =   16
      Top             =   7200
      Width           =   1335
   End
   Begin VB.TextBox Diameter 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   10080
      MaxLength       =   7
      TabIndex        =   15
      Top             =   6600
      Width           =   1335
   End
   Begin VB.TextBox BaseCurve 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6480
      MaxLength       =   7
      TabIndex        =   14
      Top             =   6600
      Width           =   1335
   End
   Begin VB.TextBox AddReading 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      MaxLength       =   7
      TabIndex        =   13
      Top             =   6600
      Width           =   1335
   End
   Begin VB.TextBox Axis 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   10080
      MaxLength       =   7
      TabIndex        =   12
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox Cylinder 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6480
      MaxLength       =   7
      TabIndex        =   11
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox Sphere 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      MaxLength       =   7
      TabIndex        =   10
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox AType 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   3
      Top             =   1800
      Width           =   8535
   End
   Begin VB.TextBox Tint 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   9
      Top             =   5400
      Width           =   8535
   End
   Begin VB.TextBox WearTime 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   8
      Top             =   4800
      Visible         =   0   'False
      Width           =   8535
   End
   Begin VB.TextBox Disposable 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   7
      Top             =   4200
      Width           =   8535
   End
   Begin VB.TextBox Material 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   6
      Top             =   3600
      Width           =   8535
   End
   Begin VB.TextBox Series 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   5
      Top             =   3000
      Width           =   8535
   End
   Begin VB.TextBox Price 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   5520
      MaxLength       =   7
      TabIndex        =   2
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox Manufacturer 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   4
      Top             =   2400
      Width           =   8535
   End
   Begin VB.TextBox SKU 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   0
      Top             =   600
      Width           =   8535
   End
   Begin VB.TextBox AQuantity 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2880
      MaxLength       =   7
      TabIndex        =   1
      Top             =   1200
      Width           =   1335
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9720
      TabIndex        =   17
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLNew.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   360
      TabIndex        =   18
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLNew.frx":01DF
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Wholesale Price"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      TabIndex        =   38
      Top             =   1200
      Width           =   2940
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label18 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Add Reading"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   450
      Left            =   360
      TabIndex        =   36
      Top             =   6600
      Width           =   2445
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Diameter"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8160
      TabIndex        =   35
      Top             =   6600
      Width           =   1725
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Peripheral Curve"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   450
      Left            =   3360
      TabIndex        =   34
      Top             =   7200
      Width           =   3045
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Base Curve"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      TabIndex        =   33
      Top             =   6600
      Width           =   1965
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Axis"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8880
      TabIndex        =   32
      Top             =   6000
      Width           =   885
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Cylinder"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4920
      TabIndex        =   31
      Top             =   6000
      Width           =   1485
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Sphere"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   30
      Top             =   6000
      Width           =   1485
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   29
      Top             =   1800
      Width           =   1965
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Tint"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   28
      Top             =   5400
      Width           =   1965
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Wear Time"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   27
      Top             =   4800
      Visible         =   0   'False
      Width           =   1965
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Disposable"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   26
      Top             =   4200
      Width           =   1965
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Material"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   25
      Top             =   3600
      Width           =   1440
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Series"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   24
      Top             =   3000
      Width           =   1275
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Price"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      TabIndex        =   23
      Top             =   1200
      Width           =   1005
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Manufacturer"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   22
      Top             =   2400
      Width           =   2355
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Enter a New Item into Inventory"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   450
      Left            =   2520
      TabIndex        =   21
      Top             =   120
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "SKU"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   20
      Top             =   600
      Width           =   1065
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Quantity"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   360
      TabIndex        =   19
      Top             =   1200
      Width           =   1605
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmCLNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OrderType As String
Public InventoryId As Long
Private CLWearTime As String
Private CLManufacturer As String
Private CLDisposable As String
Private CLSeries As String
Private CLType As String
Private CLMaterial As String
Private CLTint As String
Private CLQuantity As String
Private CLPrice As String
Private CLWPrice As String

Private Sub cmdHome_Click()
Unload frmCLNew
End Sub

Private Sub cmdDone_Click()
Dim RetrieveCLInventory As CLInventory
If (SKU.Text <> "") Then
    If (Validate) Then
        If (InventoryId > 0) Then
            Set RetrieveCLInventory = New CLInventory
            RetrieveCLInventory.InventoryId = InventoryId
            If (RetrieveCLInventory.RetrieveCLInventory) Then
                RetrieveCLInventory.SKU = SKU.Text
                RetrieveCLInventory.Manufacturer = CLManufacturer
                RetrieveCLInventory.WearTime = CLWearTime
                RetrieveCLInventory.Qty = val(CLQuantity)
                RetrieveCLInventory.Cost = val(CLPrice)
                RetrieveCLInventory.WCost = val(CLWPrice)
                RetrieveCLInventory.Disposable = CLDisposable
                RetrieveCLInventory.Tint = CLTint
                RetrieveCLInventory.Material = CLMaterial
                RetrieveCLInventory.Type_ = CLType
                RetrieveCLInventory.Series = CLSeries
                RetrieveCLInventory.Axis = Axis.Text
                RetrieveCLInventory.SpherePower = Sphere.Text
                RetrieveCLInventory.CylinderPower = Cylinder.Text
                RetrieveCLInventory.BaseCurve = BaseCurve.Text
                RetrieveCLInventory.Diameter = Diameter.Text
                RetrieveCLInventory.PeriphCurve = PeriphCurve.Text
                RetrieveCLInventory.PutCLInventory
            End If
            Set RetrieveCLInventory = Nothing
        Else
            Set RetrieveCLInventory = New CLInventory
            RetrieveCLInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "'"
            RetrieveCLInventory.FindCLInventory
            If (RetrieveCLInventory.CLInventoryTotal < 1) Then
                RetrieveCLInventory.SKU = SKU.Text
                RetrieveCLInventory.Manufacturer = CLManufacturer
                RetrieveCLInventory.WearTime = CLWearTime
                RetrieveCLInventory.Qty = val(CLQuantity)
                RetrieveCLInventory.Cost = val(CLPrice)
                RetrieveCLInventory.WCost = val(CLWPrice)
                RetrieveCLInventory.Disposable = CLDisposable
                RetrieveCLInventory.Tint = CLTint
                RetrieveCLInventory.Material = CLMaterial
                RetrieveCLInventory.Type_ = CLType
                RetrieveCLInventory.Series = CLSeries
                RetrieveCLInventory.Axis = Axis.Text
                RetrieveCLInventory.SpherePower = Sphere.Text
                RetrieveCLInventory.CylinderPower = Cylinder.Text
                RetrieveCLInventory.BaseCurve = BaseCurve.Text
                RetrieveCLInventory.Diameter = Diameter.Text
                RetrieveCLInventory.PeriphCurve = PeriphCurve.Text
                RetrieveCLInventory.PutCLInventory
            End If
            Set RetrieveCLInventory = Nothing
        End If
        Unload frmCLNew
    End If
End If
End Sub

Private Function Validate() As Boolean
Validate = True
If (SKU.Text = "") Or (CLSeries = "") Or (CLManufacturer = "") Then
    Validate = False
    frmEventMsgs.Header = "SKU, Series and Manufacturer must be entered."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Function

Private Sub AQuantity_Validate(Cancel As Boolean)
Call AQuantity_KeyPress(13)
End Sub

Private Sub AQuantity_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(AQuantity.Text)) Then
        frmEventMsgs.Header = "Invalid Quantity."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        AQuantity.Text = ""
        AQuantity.SetFocus
    Else
        CLQuantity = AQuantity.Text
    End If
End If
End Sub

Private Sub Form_Load()
If (InventoryId > 0) Then
    Call LoadInventory(InventoryId)
Else
    SKU.Text = SKU
End If
End Sub

Private Sub Price_Validate(Cancel As Boolean)
Call Price_KeyPress(13)
End Sub

Private Sub Price_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(Price.Text)) Then
        frmEventMsgs.Header = "Invalid Price."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Price.Text = ""
        Price.SetFocus
    Else
        CLPrice = Price.Text
    End If
End If
End Sub

Private Sub WPrice_Validate(Cancel As Boolean)
Call WPrice_KeyPress(13)
End Sub

Private Sub WPrice_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(WPrice.Text)) Then
        frmEventMsgs.Header = "Invalid Price."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        WPrice.Text = ""
        WPrice.SetFocus
    Else
        CLWPrice = WPrice.Text
    End If
End If
End Sub

Private Sub SKU_KeyPress(KeyAscii As Integer)
Dim RetrieveCLInventory As CLInventory
If (KeyAscii = 13) Then
    Set RetrieveCLInventory = New CLInventory
    RetrieveCLInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "'"
    RetrieveCLInventory.FindCLInventory
    If (RetrieveCLInventory.CLInventoryTotal = 1) Then
        frmEventMsgs.Header = "Already Exists."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SKU.Text = ""
        SKU.SetFocus
    Else
        AQuantity.SetFocus
    End If
    Set RetrieveCLInventory = Nothing
End If
End Sub

Private Sub Manufacturer_Click()
Call Manufacturer_KeyPress(13)
End Sub

Private Sub Manufacturer_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (getFieldValue("CLChoices", Temp)) Then
    CLManufacturer = Temp
    Manufacturer.Text = Temp
End If
End Sub

Private Sub Series_Validate(Cancel As Boolean)
Call Series_KeyPress(13)
End Sub

Private Sub Series_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    CLSeries = Series.Text
End If
End Sub

Private Sub AType_Click()
Call AType_KeyPress(13)
End Sub

Private Sub AType_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (getFieldValue("CLType", Temp)) Then
    CLType = Temp
    AType.Text = Temp
End If
End Sub

Private Sub Material_Click()
Call Material_KeyPress(13)
End Sub

Private Sub Material_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (getFieldValue("CLMaterials", Temp)) Then
    CLMaterial = Temp
    Material.Text = Temp
End If
End Sub

Private Sub Disposable_Click()
Call Disposable_KeyPress(13)
End Sub

Private Sub Disposable_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (getFieldValue("CLDisposable", Temp)) Then
    CLDisposable = Temp
    Disposable.Text = Temp
End If
End Sub

Private Sub WearTime_Click()
Call WearTime_KeyPress(13)
End Sub

Private Sub WearTime_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (getFieldValue("CLWearTime", Temp)) Then
    CLWearTime = Temp
    WearTime.Text = Temp
End If
End Sub

Private Sub Tint_Click()
Call Tint_KeyPress(13)
End Sub

Private Sub Tint_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (getFieldValue("CLTint", Temp)) Then
    CLTint = Temp
    Tint.Text = Temp
End If
End Sub

Private Function getFieldValue(RefName As String, RefValue As String) As Boolean
getFieldValue = False
RefValue = ""
If (Trim(RefName) <> "") Then
    frmSelectDialogue.InsurerSelected = ""
    Call frmSelectDialogue.BuildSelectionDialogue(RefName)
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        RefValue = UCase(frmSelectDialogue.Selection)
        getFieldValue = True
    End If
End If
End Function

Private Function LoadInventory(InvId As Long) As Boolean
Dim RetrieveCLInventory As CLInventory
InventoryId = InvId
If (InvId > 0) Then
    Set RetrieveCLInventory = New CLInventory
    RetrieveCLInventory.InventoryId = InventoryId
    If (RetrieveCLInventory.RetrieveCLInventory) Then
        SKU.Text = RetrieveCLInventory.SKU
        CLManufacturer = RetrieveCLInventory.Manufacturer
        CLWearTime = RetrieveCLInventory.WearTime
        CLQuantity = Trim(Str(RetrieveCLInventory.Qty))
        CLPrice = Trim(Str(RetrieveCLInventory.Cost))
        CLWPrice = Trim(Str(RetrieveCLInventory.WCost))
        CLDisposable = RetrieveCLInventory.Disposable
        CLTint = RetrieveCLInventory.Tint
        CLMaterial = RetrieveCLInventory.Material
        CLType = RetrieveCLInventory.Type_
        CLSeries = RetrieveCLInventory.Series
        Axis.Text = RetrieveCLInventory.Axis
        Sphere.Text = RetrieveCLInventory.SpherePower
        Cylinder.Text = RetrieveCLInventory.CylinderPower
        BaseCurve.Text = RetrieveCLInventory.BaseCurve
        Diameter.Text = RetrieveCLInventory.Diameter
        PeriphCurve.Text = RetrieveCLInventory.PeriphCurve
        
        AType.Text = CLType
        Manufacturer.Text = CLManufacturer
        WearTime.Text = CLWearTime
        Disposable.Text = CLDisposable
        Material.Text = CLMaterial
        Tint.Text = CLTint
        Series.Text = CLSeries
        AQuantity.Text = CLQuantity
        Price.Text = CLPrice
        WPrice.Text = CLWPrice
    End If
    Set RetrieveCLInventory = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

