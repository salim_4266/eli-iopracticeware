VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "SSCALB32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmCalendarNew 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstSch 
      Height          =   645
      Left            =   4320
      TabIndex        =   21
      Top             =   720
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstPractice 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "CalendarNew.frx":0000
      Left            =   120
      List            =   "CalendarNew.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   600
      Width           =   3840
   End
   Begin VB.TextBox txtColor 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   5160
      MaxLength       =   10
      TabIndex        =   0
      Top             =   1560
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   2
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CalendarNew.frx":0004
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2655
      Left            =   9120
      TabIndex        =   3
      Top             =   480
      Width           =   2775
      _Version        =   65537
      _ExtentX        =   4895
      _ExtentY        =   4683
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      BevelColorFace  =   14285823
      BevelColorShadow=   15794174
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10320
      TabIndex        =   4
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CalendarNew.frx":01E3
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6840
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Flags           =   1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdForward 
      Height          =   990
      Left            =   8160
      TabIndex        =   5
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CalendarNew.frx":03C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdVac 
      Height          =   990
      Left            =   2280
      TabIndex        =   6
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CalendarNew.frx":05AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWeekly 
      Height          =   495
      Left            =   1440
      TabIndex        =   7
      Top             =   2520
      Width           =   2475
      _Version        =   131072
      _ExtentX        =   4366
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CalendarNew.frx":0799
   End
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   4215
      Left            =   6480
      TabIndex        =   19
      Top             =   3120
      Width           =   5400
      _Version        =   65537
      _ExtentX        =   9525
      _ExtentY        =   6959
      _StockProps     =   79
      Caption         =   "Current Availability"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty CaptionFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TimeInterval    =   2
      AllowEdit       =   0   'False
      AllowAdd        =   0   'False
      AllowDelete     =   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay2 
      Height          =   4215
      Left            =   120
      TabIndex        =   20
      Top             =   3120
      Width           =   5400
      _Version        =   65537
      _ExtentX        =   9525
      _ExtentY        =   6959
      _StockProps     =   79
      Caption         =   "New Availability"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty CaptionFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TimeInterval    =   2
      AllowEdit       =   0   'False
      AllowAdd        =   0   'False
      AllowDelete     =   0   'False
   End
   Begin VB.Label lblAvail 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Hours of Availability"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   5040
      TabIndex        =   18
      Top             =   600
      Width           =   3660
   End
   Begin VB.Label lblDay 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sun"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   5160
      TabIndex        =   17
      Top             =   1080
      Width           =   3495
   End
   Begin VB.Label lblTemplate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblResource 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resource"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4680
      TabIndex        =   15
      Top             =   120
      Width           =   4215
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Please select a date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   9000
      TabIndex        =   14
      Top             =   120
      Width           =   2775
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Note reason for availability break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   570
      Left            =   4200
      TabIndex        =   13
      Top             =   2520
      Width           =   2415
   End
   Begin VB.Label lblColor 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Color"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4320
      TabIndex        =   12
      Top             =   1560
      Width           =   735
   End
   Begin VB.Label lblSample 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sample"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   7440
      TabIndex        =   11
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label lblHoliday 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Holiday"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7440
      TabIndex        =   10
      Top             =   2520
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblDisplay 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Display"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4320
      TabIndex        =   9
      Top             =   2160
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblVacation 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Vacation"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7440
      TabIndex        =   8
      Top             =   2160
      Visible         =   0   'False
      Width           =   1125
   End
End
Attribute VB_Name = "frmCalendarNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public PracticeId As Long
Public ResourceId As Long
Public ResourceName As String
Public SelectedDate As String
Public BreakDown As Integer

Private TemplateType As String
Private ForwardDeleteOn As Boolean
Private DontWorry As Boolean
Private UpdateOnly As Boolean
Private ExitFast As Boolean
Private SetDate As String
Private ApplyAll As Boolean

Private Sub cmdBack_Click()
CurrentAction = "Home"
Unload frmCalendarNew
End Sub

Private Sub cmdForward_Click()
Dim k As Integer
Dim LclDate As String
Dim DayName As String
Dim ApplCal As ApplicationCalendar
ForwardDeleteOn = False
If (Trim(SetDate) <> "") Then
    DayName = SetDate
    Call FormatTodaysDate(DayName, True)
    k = InStrPS(DayName, ",")
    If (k > 0) Then
        DayName = Trim(Left(DayName, k - 1))
    End If
    frmEventMsgs.Header = "Apply schedule to all " + DayName + "s."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        ForwardDeleteOn = True
        ExitFast = False
        LclDate = ""
        Set ApplCal = New ApplicationCalendar
        Do Until Not (ApplCal.ApplIsDateSetable(SetDate))
            lblDisplay.Caption = "Applying " + SetDate
            lblDisplay.Visible = True
            DoEvents
            Call cmdApply_Click
            Call AdjustDate(SetDate, 7, LclDate)
            SetDate = LclDate
        Loop
        lblDisplay.Visible = False
        Unload frmCalendarNew
    Else
        ExitFast = True
        Call cmdApply_Click
    End If
Else
    frmEventMsgs.Header = "Need to set times"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdApply_Click()
Unload frmCalendarNew
End Sub

Public Function LoadCalendarDisplay() As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim IAdtId As Long, OAdtId As Long
Dim TheName As String
Dim Temp1 As String, Temp2 As String
Dim InTime As String, OutTime As String
Dim TempDate As String, DisplayText As String
Dim PrcAudit As Audit
Dim ApplTbl As ApplicationTables
Dim ApplCal As ApplicationCalendar
Dim ApplSch As ApplicationScheduler
Dim ApplTemp As ApplicationTemplates
LoadCalendarDisplay = True
DontWorry = False
ExitFast = True
ForwardDeleteOn = False
CurrentAction = ""
If (BreakDown < 5) Then
    Call GetGlobalEntry("DURATIONBREAKDOWN", Temp1)
    BreakDown = Val(Temp1)
    If (BreakDown < 5) Then
        BreakDown = 5
    End If
End If
lblResource.Caption = "Calendar for " + Trim(ResourceName)
If (Trim(TemplateType) = "") Then
    TemplateType = "T"
End If
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
Call ApplTbl.ApplLoadPracticeList(TemplateType, True)
Set ApplCal = New ApplicationCalendar
Set ApplCal.lstStartTime = lstPractice
Call ApplCal.SetUpTemplates
lstPractice.Visible = True
TempDate = SelectedDate
Call SetMinMaxDates(Temp1, Temp2)
SSMonth1.MinDate = Temp1
SSMonth1.MaxDate = Temp2
SSMonth1.X.SelectedDays.RemoveAll
SelectedDate = TempDate
If (Trim(SelectedDate) = "") Then
    Call FormatTodaysDate(SelectedDate, False)
End If
lblVacation.Visible = False
Set ApplSch = New ApplicationScheduler
'Set ApplSch.lstStartTime = lstStartTime
Call ApplSch.LoadDoctorSchedule(SelectedDate, ResourceId, SSDay1, lblVacation, lblHoliday, SSMonth1)
Call ApplSch.LoadDoctorCalendar(SelectedDate, ResourceId, lstSch)
Set ApplSch = Nothing
txtColor.Text = ""
ApplCal.BreakDown = BreakDown
ApplCal.CalendarDate = SelectedDate
ApplCal.ResourceId = ResourceId
ApplCal.Location1 = -1
If (ApplCal.LoadCalendarDate) Then
    lblVacation.Visible = ApplCal.Vacation
    lblHoliday.Visible = ApplCal.Holiday
    txtColor.Text = Trim(ApplCal.DisplayColor)
    If (Trim(txtColor.Text) <> "") Then
        lblSample.BackColor = Val(Trim(txtColor.Text))
    End If
    Set ApplTemp = New ApplicationTemplates
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location1, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location1))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location2, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location2))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location3, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location3))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location4, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location4))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location5, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location5))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location6, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location6))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location7, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location7))
    Else
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    Set ApplTemp = Nothing
    Set PrcAudit = New Audit
    Call PrcAudit.GetResourceHours(ResourceId, SelectedDate, InTime, OutTime, IAdtId, OAdtId, 1)
    Set PrcAudit = Nothing
End If
Set ApplCal = Nothing
SSMonth1.ShowCentury = False
SSMonth1.X.SelectedDays.Add SelectedDate
DontWorry = True
Exit Function
UIError_Label:
    LoadCalendarDisplay = False
    Resume LeaveFast
LeaveFast:
End Function

Private Sub cmdVac_Click()
Dim ApplSch As ApplicationScheduler
frmEventMsgs.Header = "Set Vacation ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplSch = New ApplicationScheduler
    Call ApplSch.SetResourceVacation(ResourceId, SetDate)
    Set ApplSch = Nothing
    lblVacation.Visible = True
    DoEvents
End If
End Sub

Private Sub cmdWeekly_Click()
Dim ApplTbl As ApplicationTables
If (TemplateType = "T") Then
    TemplateType = "W"
Else
    TemplateType = "T"
End If
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
Call ApplTbl.ApplLoadPracticeList(TemplateType, True)
Set ApplTbl = Nothing
End Sub

Private Sub Form_Load()
If (frmHome.GetPowerUser) Then
    frmCalendarNew.BorderStyle = 1
    frmCalendarNew.ClipControls = True
    frmCalendarNew.Caption = Mid(frmCalendarNew.Name, 4, Len(frmCalendarNew.Name) - 3)
    frmCalendarNew.AutoRedraw = True
    frmCalendarNew.Refresh
End If
End Sub

Private Sub lblHoliday_Click()
Dim ApplSch As ApplicationScheduler
frmEventMsgs.Header = "Unset Holiday ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplSch = New ApplicationScheduler
    Call ApplSch.UnsetResourceHoliday(ResourceId, SetDate)
    Set ApplSch = Nothing
    lblHoliday.Visible = False
    DoEvents
End If
End Sub

Private Sub lblVacation_Click()
Dim ApplSch As ApplicationScheduler
frmEventMsgs.Header = "Unset Vacation ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplSch = New ApplicationScheduler
    Call ApplSch.UnsetResourceVacation(ResourceId, SetDate)
    Set ApplSch = Nothing
    lblVacation.Visible = False
    DoEvents
End If
End Sub

Private Sub lstPractice_Click()
Dim i As Integer
Dim AName As String
Dim ApplTemp As ApplicationTemplates
Dim ApplCal As ApplicationCalendar
AName = Space(56)
Set ApplCal = New ApplicationCalendar
If (lstPractice.ListIndex > 0) Then
    PracticeId = Val(Trim(Mid(lstPractice.List(lstPractice.ListIndex), 70, 5)))
Else
    PracticeId = 0
    If (frmTemplateNew.LoadTemplateDisplay(TemplateType)) Then
        frmTemplateNew.Show 1
        If (frmTemplateNew.PracticeId > 0) Then
            PracticeId = frmTemplateNew.PracticeId
        End If
    End If
End If
If (PracticeId > 0) Then
'    Set ApplCal.lstStartTime = lstStartTime
    Call ApplCal.LoadTemplate(PracticeId)
    txtColor.Text = ApplCal.DisplayColor
    Set ApplTemp = New ApplicationTemplates
    If (ApplCal.TemplateLocation >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.TemplateLocation, AName)
        If (Len(AName) < 56) Then
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location1 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location1, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location2 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location2, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location3 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location3, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location4 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location4, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location5 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location5, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location6 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location6, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    If (ApplCal.Location7 >= 0) Then
        Call ApplTemp.ApplGetResourceName(0, ApplCal.Location7, AName)
        If (Len(AName) < 56) Then
            If (Trim(AName) = "") Then
                AName = "OFFICE"
            End If
            AName = AName + Space(56 - Len(AName))
        End If
    End If
    Set ApplTemp = Nothing
    Set ApplCal.lstStartTime = lstPractice
    Call ApplCal.SetUpTemplates
End If
Set ApplCal = Nothing
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
Dim TmpDate As String
Dim OrgDate As String
Dim TheDate As String
Dim ApplCal As ApplicationCalendar
OrgDate = OldSelDate
TheDate = SelDate
SetDate = SelDate
Call FormatTodaysDate(TheDate, True)
Call FormatTodaysDate(SetDate, False)
lblDay.Caption = TheDate
If (Trim(SetDate) <> "") And (Trim(OrgDate) <> "") Then
        frmEventMsgs.Header = "Apply changes ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            ExitFast = False
            Call FormatTodaysDate(OrgDate, False)
            TmpDate = SetDate
            SetDate = OrgDate
            Call cmdApply_Click
            SetDate = TmpDate
            ExitFast = True
        End If
End If
SelectedDate = SetDate
If (DontWorry) Then
    Call LoadCalendarDisplay
End If
End Sub

Private Sub txtColor_Click()
CommonDialog1.CancelError = True
On Error GoTo UI_ErrorHandler
CommonDialog1.Flags = &H1&
CommonDialog1.ShowColor
txtColor.Text = Trim(Str(CommonDialog1.Color))
If (Trim(txtColor.Text) <> "") Then
    lblSample.BackColor = Val(Trim(txtColor.Text))
End If
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub txtColor_KeyPress(KeyAscii As Integer)
Call txtColor_Click
KeyAscii = 0
End Sub
