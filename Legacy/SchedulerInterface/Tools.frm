VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{918E6E43-F23A-11D0-901E-0020AF7543C2}#5.0#0"; "fximg50g.ocx"
Begin VB.Form frmTools 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdRImpr 
      Height          =   975
      Left            =   2160
      TabIndex        =   45
      Top             =   4440
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLBD 
      Height          =   975
      Left            =   4080
      TabIndex        =   44
      Top             =   4440
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":01EC
   End
   Begin VB.TextBox txtCln 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      MaxLength       =   10
      TabIndex        =   38
      Top             =   1560
      Visible         =   0   'False
      Width           =   855
   End
   Begin FXIMG50GLib.FXImage FXImage1 
      Height          =   495
      Left            =   840
      TabIndex        =   35
      Top             =   4440
      Visible         =   0   'False
      Width           =   615
      _Version        =   327680
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   1105503
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   1000408065
      ErrInfo         =   -1809338394
   End
   Begin VB.TextBox txtLeft 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1680
      MaxLength       =   10
      TabIndex        =   33
      Top             =   1080
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox txtTop 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1680
      MaxLength       =   10
      TabIndex        =   32
      Top             =   600
      Visible         =   0   'False
      Width           =   855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMed 
      Height          =   975
      Left            =   4080
      TabIndex        =   19
      Top             =   5520
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":03D6
   End
   Begin VB.TextBox txtToDr 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   3720
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.TextBox txtFromDr 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   5
      Top             =   3240
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.TextBox txtClosingDate 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      MaxLength       =   10
      TabIndex        =   3
      Top             =   1560
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.TextBox txtFromIns 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   2
      Top             =   2160
      Visible         =   0   'False
      Width           =   6855
   End
   Begin VB.TextBox txtToIns 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   1
      Top             =   2640
      Visible         =   0   'False
      Width           =   6855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9840
      TabIndex        =   0
      Top             =   7680
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":05C3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReset 
      Height          =   975
      Left            =   2160
      TabIndex        =   4
      Top             =   5520
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":07A2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMergeDr 
      Height          =   975
      Left            =   240
      TabIndex        =   7
      Top             =   3240
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":098B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdVC 
      Height          =   975
      Left            =   240
      TabIndex        =   8
      Top             =   6600
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":0B73
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMergePlans 
      Height          =   975
      Left            =   240
      TabIndex        =   9
      Top             =   2160
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":0D5D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClinicalVerify 
      Height          =   975
      Left            =   240
      TabIndex        =   10
      Top             =   5520
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":0F43
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPay 
      Height          =   975
      Left            =   2160
      TabIndex        =   20
      Top             =   6600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":112D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCosts 
      Height          =   975
      Left            =   4080
      TabIndex        =   21
      Top             =   6600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":131A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRpt 
      Height          =   975
      Left            =   6000
      TabIndex        =   22
      Top             =   5520
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":1506
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPat 
      Height          =   975
      Left            =   6000
      TabIndex        =   23
      Top             =   6600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":16F0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBill 
      Height          =   975
      Left            =   7920
      TabIndex        =   24
      Top             =   5520
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":18DE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCheck 
      Height          =   975
      Left            =   7920
      TabIndex        =   25
      Top             =   6600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":1ACA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDeny 
      Height          =   975
      Left            =   9840
      TabIndex        =   26
      Top             =   5520
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":1CB7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlant 
      Height          =   975
      Left            =   9840
      TabIndex        =   27
      Top             =   6600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":1E9F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImage 
      Height          =   975
      Left            =   6000
      TabIndex        =   28
      Top             =   7680
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2087
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImportClin 
      Height          =   975
      Left            =   2160
      TabIndex        =   29
      Top             =   7680
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":226E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBurn 
      Height          =   975
      Left            =   4080
      TabIndex        =   34
      Top             =   7680
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2458
   End
   Begin FXIMG50GLib.FXImage FXImage2 
      Height          =   495
      Left            =   1680
      TabIndex        =   36
      Top             =   4440
      Visible         =   0   'False
      Width           =   615
      _Version        =   327680
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   1105503
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   1000408065
      ErrInfo         =   -1809338394
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuant 
      Height          =   975
      Left            =   6000
      TabIndex        =   39
      Top             =   4440
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2641
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdInsDes 
      Height          =   975
      Left            =   7920
      TabIndex        =   40
      Top             =   7680
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":282C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEyes 
      Height          =   975
      Left            =   9840
      TabIndex        =   41
      Top             =   4440
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2A1E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRLoc 
      Height          =   975
      Left            =   7920
      TabIndex        =   42
      Top             =   4440
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2C06
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   240
      TabIndex        =   43
      Top             =   7680
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2DF3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRAppt 
      Height          =   975
      Left            =   9840
      TabIndex        =   46
      Top             =   1080
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":2FD2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRRx 
      Height          =   975
      Left            =   240
      TabIndex        =   47
      Top             =   4440
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":31BB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdScanFix 
      Height          =   975
      Left            =   7920
      TabIndex        =   48
      Top             =   3360
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":339E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLLoad 
      Height          =   975
      Left            =   9840
      TabIndex        =   49
      Top             =   3360
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Tools.frx":3581
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Clinical Id"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   37
      Top             =   1560
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblLeft 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Left"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   31
      Top             =   1080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblTop 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Top"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   30
      Top             =   600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblFromDr 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor to Delete"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   18
      Top             =   3240
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label lblToDr 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor To Keep"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   17
      Top             =   3720
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Tools"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4560
      TabIndex        =   16
      Top             =   120
      Width           =   2775
   End
   Begin VB.Label lblClosingDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Closing Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3720
      TabIndex        =   15
      Top             =   1560
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label lblLastDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Closing Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3720
      TabIndex        =   14
      Top             =   1080
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.Label lblTotal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Closing Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   5160
      TabIndex        =   13
      Top             =   600
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.Label lblToIns 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan to Keep"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   12
      Top             =   2640
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label lblFromIns 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan to Delete"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   11
      Top             =   2160
      Visible         =   0   'False
      Width           =   1935
   End
End
Attribute VB_Name = "frmTools"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CloseOn As Boolean
Private ToDoctor As Long
Private FromDoctor As Long
Private ToIns As Long
Private FromIns As Long

Private Sub cmdCLLoad_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "CL Inventory: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.LoadCLInventory(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdEyes_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Eyes: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetImageEyes(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdHome_Click()
Unload frmTools
End Sub

Private Sub cmdImportClin_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Import Clinical Services: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ImportIOClinical(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdBill_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Bill Office: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetReceivableBillOffice(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdCheck_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Checks: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetPaymentCheckRecords(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdCosts_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Fees: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetReceivableFees(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdDeny_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Close Denials: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetReceivableDenials(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdDone_Click()
Dim ApplList As ApplicationAIList
If (CloseOn) Then
    If (Trim(txtClosingDate.Text) <> "") Then
        Set ApplList = New ApplicationAIList
        Call ApplList.ApplSetClosingMonth(Trim(txtClosingDate.Text))
        Set ApplList = Nothing
        Call SetGlobalConfiguration("S")
    End If
End If
Unload frmTools
End Sub

Private Sub cmdImage_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "JPeg Convert: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplConvertJpeg(lblTotal, Val(Trim(txtTop.Text)), Val(Trim(txtLeft.Text)), Val(Trim(txtCln.Text)))
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdBurn_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Re-Burn Images: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplReBurnImages(lblTotal, FXImage1, FXImage2)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdInsDes_Click()
Dim ApplTemp As ApplicationTemplates
frmEventMsgs.Header = "Set Rcv Ins Des: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetInsurerDesignation("", True, lblTotal)
    Set ApplTemp = Nothing
    lblTotal.Visible = False
End If
End Sub

Private Sub cmdLBD_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
txtFromDr.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Bill Date: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetPatientBillDate("", True, lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdMed_Click()
frmFeeUtility.Show 1
End Sub

Private Sub cmdMergeDr_Click()
Dim ApplTemp As ApplicationTemplates
frmEventMsgs.Header = "Ref Dr Merge: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplMergeDoctors(FromDoctor, ToDoctor, lblTotal)
    Set ApplTemp = Nothing
    lblTotal.Visible = False
    txtFromDr.Text = ""
    txtToDr.Text = ""
End If
End Sub

Private Sub cmdMergePlans_Click()
Dim ApplTemp As ApplicationTemplates
If (FromIns > 0) And (ToIns > 0) Then
    frmEventMsgs.Header = "Plan Merge: Are you sure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        Set ApplTemp = New ApplicationTemplates
        lblTotal.Visible = True
        Call ApplTemp.ApplMergePlans(FromIns, ToIns, lblTotal)
        Set ApplTemp = Nothing
        lblTotal.Visible = False
        txtFromIns.Text = ""
        txtToIns.Text = ""
        FromIns = 0
        ToIns = 0
    End If
End If
End Sub

Private Sub cmdClinicalVerify_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Verify Clinical: Record Type ?"
frmEventMsgs.AcceptText = "Tests"
frmEventMsgs.RejectText = "Images"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = "Procedures"
frmEventMsgs.Other1Text = "EM"
frmEventMsgs.Other2Text = "Set Images"
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplClinicalVerify("F", lblTotal)
    Set ApplTemp = Nothing
    txtFromIns.Text = ""
    txtToIns.Text = ""
ElseIf (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplClinicalVerify("I", lblTotal)
    Set ApplTemp = Nothing
    txtFromIns.Text = ""
    txtToIns.Text = ""
ElseIf (frmEventMsgs.Result = 3) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplClinicalVerify("P", lblTotal)
    Set ApplTemp = Nothing
    txtFromIns.Text = ""
    txtToIns.Text = ""
ElseIf (frmEventMsgs.Result = 5) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplClinicalVerify("Z", lblTotal)
    Set ApplTemp = Nothing
    txtFromIns.Text = ""
    txtToIns.Text = ""
ElseIf (frmEventMsgs.Result = 6) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplVerifyCDDrawFiles(lblTotal)
    Set ApplTemp = Nothing
    txtFromIns.Text = ""
    txtToIns.Text = ""
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdPat_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Referral: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetPatientReferralRequired(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdPay_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
txtFromDr.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Pay Date: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetPatientLastPaid(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdPlant_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Plant Dxs: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    DoEvents
    Call ApplTemp.ResetReceivablePlantDxs(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdQuant_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Quantifiers: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplResetQuantifiers(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdReset_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Balances: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetReceivableBalances(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdRAppt_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Appt Ref: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetAppointmentLetterReference(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdRImpr_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Impressions: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetImpressions(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdRLoc_Click()
Dim Ref As Integer
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Type of Reset Locations ?"
frmEventMsgs.AcceptText = "Finding"
frmEventMsgs.RejectText = "Image"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = "Place LC"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result <> 4) Then
    Ref = frmEventMsgs.Result
    frmEventMsgs.Header = "Reset Locations: Are you sure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        If (Ref = 1) Then
            Set ApplTemp = New ApplicationTemplates
            lblTotal.Visible = True
            Call ApplTemp.ApplResetFindingLocations(lblTotal)
            Set ApplTemp = Nothing
        ElseIf (Ref = 2) Then
            Set ApplTemp = New ApplicationTemplates
            lblTotal.Visible = True
            Call ApplTemp.ApplResetImageLocations(lblTotal)
            Set ApplTemp = Nothing
        ElseIf (Ref = 3) Then
            Set ApplTemp = New ApplicationTemplates
            lblTotal.Visible = True
            Call ApplTemp.ApplResetLocations(lblTotal)
            Set ApplTemp = Nothing
        End If
    End If
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdRpt_Click()
Dim Rec As String
Dim PatPaid As Single, ATotal As Single
Dim MedOn As Boolean
Dim GDate As String
Dim InvId As String, InsName As String
Dim Temp As String, ADate As String
Dim ThePat As String, TheAmt As String
Dim MyFile As String, TheFile As String
Dim FName As String, LName As String, APhone As String
Dim Addr As String, CSZ As String, AZip As String
Dim TheBal As Single, PBal As Single, IBal As Single
Dim RetPat As Patient
Dim ApplList As ApplicationAIList
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
MedOn = True
FM.OpenFile SubmissionsDirectory + "TestFile.txt", FileOpenMode.Append, FileAccess.ReadWrite, CLng(101)
FM.CloseFile CLng(101)
FM.Kill SubmissionsDirectory + "TestFile.txt"
ATotal = 0
InsName = ""
MyFile = Dir(SubmissionsDirectory + "VEC*.txt")
While (Trim(MyFile) <> "")
    GDate = Mid(MyFile, 9, 8)
    If (GDate <= "20040819") Then
        Set ApplList = New ApplicationAIList
        FM.OpenFile SubmissionsDirectory + "TestFile.txt", FileOpenMode.Append, FileAccess.ReadWrite, CLng(101)
        TheFile = SubmissionsDirectory + MyFile
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(102)
        While Not (EOF(102))
            Line Input #102, Rec
            If (Left(Rec, 3) = "DA0") Then
                If (InsName = "") Then
                    InsName = Mid(Rec, 36, 20)
                End If
            ElseIf (Left(Rec, 3) = "XA0") Then
                ThePat = Trim(Mid(Rec, 6, 17))
                TheAmt = Trim(Mid(Rec, 127, 7))
                TheAmt = Left(TheAmt, Len(TheAmt) - 2) + "." + Mid(TheAmt, Len(TheAmt) - 1, 2)
                Call DisplayDollarAmount(Trim(Str(Val(TheAmt))), Temp)
                Temp = Trim(Temp)
                InvId = Trim(Mid(Rec, 306, 15))
                If (Val(Trim(Temp)) > 0) Then
                    PatPaid = ApplList.ApplGetPatientPaidbyInvoice(InvId, True)
                    TheBal = ApplList.ApplGetBalancebyInvoice(InvId, PBal, IBal, False, False)
                    If (Val(Temp) <> PatPaid) Then
                        If (InStr(UCase(InsName), "MEDICARE") > 0) And (MedOn) Then
                            ATotal = ATotal + Val(Temp)
                            GoSub GetOthers
                            Print #101, ThePat; vbTab + Temp + vbTab + InvId + vbTab + Trim(InsName) + vbTab + LName + vbTab + FName + vbTab + APhone + vbTab + Trim(Str(TheBal)) + vbTab + Addr + vbTab + CSZ
                            lblTotal.Caption = "Reference: " + ThePat + " " + Temp
                            lblTotal.Visible = True
                            DoEvents
                        End If
                    End If
                End If
                InsName = ""
            End If
        Wend
        FM.CloseFile CLng(102)
        FM.CloseFile CLng(101)
        Set ApplList = Nothing
    End If
    MyFile = Dir
Wend
lblTotal.Visible = False
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
Exit Sub
GetOthers:
    FName = ""
    LName = ""
    APhone = ""
    Addr = ""
    CSZ = ""
    AZip = ""
    Set RetPat = New Patient
    RetPat.PatientId = Val(ThePat)
    If (RetPat.RetrievePatient) Then
        FName = RetPat.FirstName
        LName = RetPat.LastName
        APhone = RetPat.HomePhone
        Addr = RetPat.Address
        Call DisplayZip(RetPat.Zip, AZip)
        CSZ = Trim(RetPat.City) + ", " + Trim(RetPat.State) + " " + AZip
    End If
    Set RetPat = Nothing
    Return
'If (ATotal > 0) Then
'    Open SubmissionsDirectory + "TestFile.txt" For Append As #101
'    Print #101, Trim(Str(ATotal))
'    Close #101
'End If
End Sub

Private Sub cmdRRx_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Reset Rx Records: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ResetRxRecords(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdScanFix_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Scan Fix: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ScanFix(lblTotal)
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub cmdVC_Click()
Dim ApplTemp As ApplicationTemplates
lblFromDr.Visible = False
txtFromDr.Visible = False
lblToDr.Visible = False
txtToDr.Visible = False
lblFromIns.Visible = False
txtFromIns.Visible = False
lblToIns.Visible = False
txtToIns.Visible = False
frmEventMsgs.Header = "Verify Check Out: Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplTemp = New ApplicationTemplates
    lblTotal.Visible = True
    Call ApplTemp.ApplVerifyCheckOut(lblTotal)
    lblTotal.Visible = False
    Set ApplTemp = Nothing
End If
lblFromDr.Visible = True
txtFromDr.Visible = True
lblToDr.Visible = True
txtToDr.Visible = True
lblFromIns.Visible = True
txtFromIns.Visible = True
lblToIns.Visible = True
txtToIns.Visible = True
End Sub

Private Sub Form_Load()
Dim TheDate As String
Dim ApplList As ApplicationAIList
FromDoctor = 0
ToDoctor = 0
If (CloseOn) Then
    Set ApplList = New ApplicationAIList
    TheDate = ApplList.ApplGetClosingMonth
    Set ApplList = Nothing
    lblClosingDate.Visible = True
    txtClosingDate.Text = ""
    txtClosingDate.Visible = True
    lblLastDate.Caption = "Last Date: " + TheDate
    lblLastDate.Visible = True
    lblTotal.Visible = False
    cmdImage.Visible = False
    cmdMergePlans.Visible = False
    cmdMergeDr.Visible = False
    cmdVC.Visible = False
    cmdCosts.Visible = False
    cmdPat.Visible = False
    cmdPay.Visible = False
    cmdLBD.Visible = False
    cmdCheck.Visible = False
    cmdRpt.Visible = False
    cmdBill.Visible = False
    cmdDeny.Visible = False
    cmdClinicalVerify.Visible = False
    cmdMed.Visible = False
    cmdPlant.Visible = False
    cmdBurn.Visible = False
    cmdImportClin.Visible = False
    cmdRLoc.Visible = False
    cmdEyes.Visible = False
    cmdRImpr.Visible = False
    cmdRAppt.Visible = False
    cmdQuant.Visible = False
    cmdInsDes.Visible = False
    cmdRRx.Visible = False
    cmdScanFix.Visible = False
    cmdCLLoad.Visible = False
Else
    cmdReset.Visible = True
    lblTop.Visible = True
    txtTop.Visible = True
    txtTop.Text = ""
    lblLeft.Visible = True
    txtLeft.Visible = True
    txtLeft.Text = ""
    lblToDr.Visible = True
    txtToDr.Visible = True
    txtToDr.Text = ""
    lblFromDr.Visible = True
    txtFromDr.Visible = True
    txtFromDr.Text = ""
    lblToIns.Visible = True
    txtToIns.Visible = True
    txtToIns.Text = ""
    lblFromIns.Visible = True
    txtFromIns.Visible = True
    txtFromIns.Text = ""
    cmdImage.Visible = True
    cmdMergePlans.Visible = True
    cmdMergeDr.Visible = True
    cmdVC.Visible = True
    cmdCosts.Visible = True
    cmdPat.Visible = True
    cmdPay.Visible = True
    cmdLBD.Visible = True
    cmdCheck.Visible = True
    cmdDeny.Visible = True
    cmdRpt.Visible = True
    cmdBill.Visible = True
    cmdClinicalVerify.Visible = True
    cmdMed.Visible = True
    cmdBurn.Visible = True
    cmdPlant.Visible = True
    cmdImportClin.Visible = True
    cmdRLoc.Visible = True
    cmdEyes.Visible = True
    cmdRImpr.Visible = True
    cmdRAppt.Visible = True
    cmdQuant.Visible = True
    cmdInsDes.Visible = True
    cmdRRx.Visible = True
    cmdScanFix.Visible = True
    cmdCLLoad.Visible = True
End If
End Sub

Private Sub txtFromDr_Click()
Dim DisplayText As String
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PCP")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection <> "Create Doctor") Then
        txtFromDr.Text = UCase(frmSelectDialogue.Selection)
        FromDoctor = Val(Trim(Mid(txtFromDr.Text, 57, 5)))
    End If
End If
End Sub

Private Sub txtFromDr_KeyPress(KeyAscii As Integer)
Call txtFromDr_Click
KeyAscii = 0
End Sub

Private Sub txtLeft_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If Not (IsNumeric(Chr(KeyAscii))) Then
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtTop_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If Not (IsNumeric(Chr(KeyAscii))) Then
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtCln_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If Not (IsNumeric(Chr(KeyAscii))) Then
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtToDr_Click()
Dim DisplayText As String
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PCP")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection <> "Create Doctors") Then
        txtToDr.Text = UCase(frmSelectDialogue.Selection)
        ToDoctor = Val(Trim(Mid(txtToDr.Text, 57, 5)))
    End If
End If
End Sub

Private Sub txtToDr_KeyPress(KeyAscii As Integer)
Call txtToDr_Click
KeyAscii = 0
End Sub

Private Sub txtFromIns_Click()
Dim DisplayText As String
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PLAN")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection <> "Create Plan") Then
        txtFromIns.Text = UCase(frmSelectDialogue.Selection)
        FromIns = Val(Trim(Mid(txtFromIns.Text, 57, 5)))
    End If
End If
End Sub

Private Sub txtFromIns_KeyPress(KeyAscii As Integer)
Call txtFromIns_Click
KeyAscii = 0
End Sub

Private Sub txtToIns_Click()
Dim DisplayText As String
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PLAN")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection <> "Create Plan") Then
        txtToIns.Text = UCase(frmSelectDialogue.Selection)
        ToIns = Val(Trim(Mid(txtToIns.Text, 57, 5)))
    End If
End If
End Sub

Private Sub txtToIns_KeyPress(KeyAscii As Integer)
Call txtToIns_Click
KeyAscii = 0
End Sub

Private Sub txtClosingDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtClosingDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtClosingDate.Text = ""
        txtClosingDate.SetFocus
        SendKeys "{Home}"
        KeyAscii = 0
    Else
        Call UpdateDisplay(txtClosingDate, "D")
    End If
End If
End Sub

Private Sub txtClosingDate_Validate(Cancel As Boolean)
Call txtClosingDate_KeyPress(13)
End Sub


