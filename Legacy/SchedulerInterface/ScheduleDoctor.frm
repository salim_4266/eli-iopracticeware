VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "Sscalb32.ocx"
Begin VB.Form frmScheduleDoctor 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form6"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form6"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstSch 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   840
      ItemData        =   "ScheduleDoctor.frx":0000
      Left            =   120
      List            =   "ScheduleDoctor.frx":0002
      MultiSelect     =   1  'Simple
      TabIndex        =   11
      Top             =   840
      Width           =   6255
   End
   Begin VB.ListBox lstStartTime 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   705
      ItemData        =   "ScheduleDoctor.frx":0004
      Left            =   7440
      List            =   "ScheduleDoctor.frx":0006
      MultiSelect     =   1  'Simple
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2175
      Left            =   9240
      TabIndex        =   2
      Top             =   120
      Width           =   2535
      _Version        =   65537
      _ExtentX        =   4471
      _ExtentY        =   3836
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   14285823
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   10320
      TabIndex        =   3
      Top             =   7920
      Width           =   1380
      _Version        =   131072
      _ExtentX        =   2434
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDoctor.frx":0008
   End
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   6045
      Left            =   120
      TabIndex        =   1
      Top             =   1800
      Width           =   11655
      _Version        =   65537
      _ExtentX        =   20558
      _ExtentY        =   8017
      _StockProps     =   79
      Caption         =   "SSDay1"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrSch 
      Height          =   990
      Left            =   8880
      TabIndex        =   9
      Top             =   7920
      Width           =   1380
      _Version        =   131072
      _ExtentX        =   2434
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDoctor.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWeekly 
      Height          =   990
      Left            =   7440
      TabIndex        =   10
      Top             =   7920
      Width           =   1380
      _Version        =   131072
      _ExtentX        =   2434
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ScheduleDoctor.frx":03D1
   End
   Begin VB.Label lblHoliday 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Holiday"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   8040
      TabIndex        =   8
      Top             =   1440
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblVacation 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Vacation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6720
      TabIndex        =   7
      Top             =   1440
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Label lblDetails 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Appointment Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   120
      TabIndex        =   5
      Top             =   8040
      Visible         =   0   'False
      Width           =   7005
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblApptTotal 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Total Appointments:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   2280
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Dr. Jones Schedule for Thursday, January 16, 1999"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5745
   End
End
Attribute VB_Name = "frmScheduleDoctor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public InitialApptDate As String
Public ApptDate As String
Public ApptTime As String
Public TheDoctorId As Long
Private DontWorry As Boolean
Private CurrentSelectedDate As String

Private Sub cmdDrSch_Click()
Dim Br As Integer
Dim Pr As Long
Dim DrName As String
Dim ApplList As ApplicationAIList

Set ApplList = New ApplicationAIList
Call ApplList.ApplGetDoctorInfo(TheDoctorId, DrName, Br, Pr)
frmCalendar.ResourceName = DrName
frmCalendar.ResourceId = TheDoctorId
frmCalendar.SelectedDate = CurrentSelectedDate
frmCalendar.BreakDown = Br
frmCalendar.PracticeId = Pr
If (frmCalendar.LoadCalendarDisplay) Then
    frmCalendar.Show 1
    If (frmCalendar.CurrentAction <> "Home") Then
        If (frmCalendar.SelectedDate <> CurrentSelectedDate) Then
            CurrentSelectedDate = frmCalendar.SelectedDate
        End If
        Call LoadDoctorSchedule(TheDoctorId)
    End If
End If
End Sub

Private Sub cmdExit_Click()
CurrentSelectedDate = ""
Unload frmScheduleDoctor
End Sub

Public Function LoadDoctorSchedule(DoctorId As Long) As Boolean
Dim i As Integer
Dim AptTotal As Integer
Dim TheName As String
Dim TheDate As String
Dim ApplList As ApplicationAIList
Dim ApplSch As ApplicationScheduler
DontWorry = False
TheDate = ""
If (Trim(InitialApptDate) <> "") Then
    TheDate = InitialApptDate
    ApptDate = InitialApptDate
    InitialApptDate = ""
End If
If (Trim(CurrentSelectedDate) <> "") Then
    TheDate = CurrentSelectedDate
    ApptDate = TheDate
End If
Call FormatTodaysDate(TheDate, True)
Set ApplList = New ApplicationAIList
Call ApplList.ApplGetDoctor(DoctorId, TheName)
Label1.Caption = Trim(Left(TheName, 30)) + " Schedule for " + TheDate
Set ApplList = Nothing
Set ApplSch = New ApplicationScheduler
Set ApplSch.lstStartTime = lstStartTime
LoadDoctorSchedule = ApplSch.LoadDoctorSchedule(ApptDate, DoctorId, SSDay1, lblVacation, lblHoliday, SSMonth1)
Call ApplSch.LoadDoctorCalendar(ApptDate, DoctorId, lstSch)
Set ApplSch = Nothing
CurrentSelectedDate = ApptDate
AptTotal = 0
For i = 0 To SSDay1.Tasks.Count - 1
    If (InStrPS(SSDay1.Tasks(i).Text, "*") <> 0) Then
        AptTotal = AptTotal + 1
    End If
Next i
lblApptTotal.Caption = "Total Appointments: " + Trim(Str(AptTotal))
DontWorry = True
TheDoctorId = DoctorId
ApptTime = ""
End Function

Private Sub cmdWeekly_Click()
Dim ADate As String
If (frmScheduleWeekly.LoadDoctorSchedule(TheDoctorId, ADate)) Then
    frmScheduleWeekly.Show 1
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmScheduleDoctor.BorderStyle = 1
    frmScheduleDoctor.ClipControls = True
    frmScheduleDoctor.Caption = Mid(frmScheduleDoctor.Name, 4, Len(frmScheduleDoctor.Name) - 3)
    frmScheduleDoctor.AutoRedraw = True
    frmScheduleDoctor.Refresh
End If
End Sub

Private Sub SSDay1_Click()
Dim k As Integer
Dim Temp As String
Dim ApptId As Long
If (SSDay1.TaskSelected >= 0) Then
    lblDetails.Caption = SSDay1.Tasks.Item(SSDay1.TaskSelected).Text
    lblDetails.Visible = True
    k = InStrPS(lblDetails.Caption, "*")
    If (k > 0) Then
        ApptId = val(Trim(Mid(lblDetails.Caption, k + 1, Len(lblDetails.Caption) - k)))
        Call GetApptFiller(ApptId, Temp)
        lblDetails.Caption = Left(SSDay1.Tasks.Item(SSDay1.TaskSelected).Text, k - 1) + " " + Temp
        lblDetails.Visible = True
    End If
End If
If (SSDay1.TimeSlotIndex >= 0) Then
    ApptTime = SSDay1.TimeFromIndex(SSDay1.TimeSlotIndex)
End If
End Sub

Private Sub SSDay1_DblClick()
Call SSDay1_Click
End Sub

Private Sub SSDay1_TimeBarClick(Time As String)
ApptTime = ""
End Sub

Private Sub SSDay1_TimeBtnClick(Time As String)
If (SSDay1.TimeSlotIndex >= 0) Then
    ApptTime = Time
End If
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
Dim DocId As Long
Dim ADate As String
If (Trim(SelDate) <> "") And (DontWorry) Then
    OldSelDate = SelDate
    ADate = SelDate
    DocId = TheDoctorId
    Call FormatTodaysDate(ADate, False)
    ApptDate = ADate
    If (ADate <> CurrentSelectedDate) Then
        CurrentSelectedDate = ADate
        Call LoadDoctorSchedule(DocId)
    End If
End If
End Sub

Private Sub SSMonth1_SelChanged(SelDate As String, OldSelDate As String, Selected As Integer)
Call SSMonth1_SelChange(SelDate, OldSelDate, Selected, False)
End Sub

Private Function GetApptFiller(ApptId As Long, DisplayIt As String) As Boolean
Dim RetRes As SchedulerResource
Dim RetAppt As SchedulerAppointment
GetApptFiller = False
DisplayIt = ""
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        GetApptFiller = True
        If (Trim(RetAppt.AppointmentSetDate) <> "") Then
            DisplayIt = Mid(RetAppt.AppointmentSetDate, 5, 2) + "/" + Mid(RetAppt.AppointmentSetDate, 7, 2) + "/" + Mid(RetAppt.AppointmentSetDate, 1, 4)
        End If
        If (RetAppt.AppointmentTypeTech > 0) Then
            Set RetRes = New SchedulerResource
            RetRes.ResourceId = RetAppt.AppointmentTypeTech
            If (RetRes.RetrieveSchedulerResource) Then
                DisplayIt = DisplayIt + " " + Trim(RetRes.ResourceName)
            End If
            Set RetRes = Nothing
        End If
    End If
    Set RetAppt = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
