VERSION 5.00
Begin VB.Form frmSplash 
   BorderStyle     =   0  'None
   ClientHeight    =   4515
   ClientLeft      =   210
   ClientTop       =   1365
   ClientWidth     =   6015
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H00E0E0E0&
   Icon            =   "Splash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4515
   ScaleWidth      =   6015
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   4600
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6100
      Begin VB.Image imgLogo 
         Appearance      =   0  'Flat
         Height          =   4500
         Left            =   0
         Picture         =   "Splash.frx":000C
         Top             =   0
         Width           =   6000
      End
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
    (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
    (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_APPWINDOW = &H40000
Private IsShowingWithIcon As Boolean ' Prevents splash screen from being shown multiple times

Public Whom As String

Private Sub Form_KeyPress(KeyAscii As Integer)
   Unload frmSplash
End Sub

Private Sub Frame1_Click()
Unload frmSplash
End Sub

' Shows splash screen with icon in the taskbar.
' Most useful when native VB6 root window (such as frmHome) is hidden (replaced with .NET one)
Public Sub ShowWithIcon()
    ' We can only do Show when there are no modal forms. Checking for Form.Visible on all forms will trigger their loading. So this:
    On Error Resume Next
    
    ' Do following: Me.ShowInTaskbar = True
    SetWindowLong Me.hWnd, GWL_EXSTYLE, (GetWindowLong(Me.hWnd, GWL_EXSTYLE) Or WS_EX_APPWINDOW)
    ' Show it
    Me.Show

End Sub

' Hides splash screen and reverts back ShowInTaskbar state
Public Sub HideWithIcon()
    ' We can only do Hide when Splash is the last visible form. Checking for Form.Visible on all forms will trigger their loading. So this:
    On Error GoTo Quit
        
    ' Hide splash screen now
    Me.Hide
    ' Do following: Me.ShowInTaskbar = False
    SetWindowLong Me.hWnd, GWL_EXSTYLE, (GetWindowLong(Me.hWnd, GWL_EXSTYLE) And Not WS_EX_APPWINDOW)
    ' Unload it, since otherwise it might leave an icon
    Unload Me
Quit:
End Sub
