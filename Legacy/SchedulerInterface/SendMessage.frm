VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmSendMessage 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstTests 
      Height          =   450
      Left            =   8640
      TabIndex        =   29
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstMsgs 
      Height          =   450
      Left            =   7680
      TabIndex        =   28
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstRooms 
      Height          =   450
      Left            =   6720
      TabIndex        =   27
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstStaff 
      Height          =   450
      Left            =   5760
      TabIndex        =   26
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest1 
      Height          =   855
      Left            =   8040
      TabIndex        =   17
      Top             =   960
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg1 
      Height          =   855
      Left            =   4080
      TabIndex        =   0
      Top             =   960
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":01DB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   8040
      TabIndex        =   1
      Top             =   7800
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":03B6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff1 
      Height          =   855
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":05E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff2 
      Height          =   855
      Left            =   240
      TabIndex        =   3
      Top             =   1920
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":07C4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff3 
      Height          =   855
      Left            =   240
      TabIndex        =   4
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":099F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff4 
      Height          =   855
      Left            =   240
      TabIndex        =   5
      Top             =   3840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":0B7A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff6 
      Height          =   855
      Left            =   240
      TabIndex        =   6
      Top             =   5760
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":0D55
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff5 
      Height          =   855
      Left            =   240
      TabIndex        =   7
      Top             =   4800
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":0F30
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff7 
      Height          =   855
      Left            =   240
      TabIndex        =   8
      Top             =   6720
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":110B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreMsgs 
      Height          =   855
      Left            =   4080
      TabIndex        =   9
      Top             =   4800
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":12E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreStaff 
      Height          =   855
      Left            =   240
      TabIndex        =   10
      Top             =   7680
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":14CE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg2 
      Height          =   855
      Left            =   4080
      TabIndex        =   11
      Top             =   1920
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":16B3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg3 
      Height          =   855
      Left            =   4080
      TabIndex        =   12
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":188E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg4 
      Height          =   855
      Left            =   4080
      TabIndex        =   13
      Top             =   3840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":1A69
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom2 
      Height          =   855
      Left            =   4080
      TabIndex        =   14
      Top             =   6720
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":1C44
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom1 
      Height          =   855
      Left            =   4080
      TabIndex        =   15
      Top             =   5760
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":1E1F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest2 
      Height          =   855
      Left            =   8040
      TabIndex        =   18
      Top             =   1920
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":1FFA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest3 
      Height          =   855
      Left            =   8040
      TabIndex        =   19
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":21D5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest4 
      Height          =   855
      Left            =   8040
      TabIndex        =   20
      Top             =   3840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":23B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest6 
      Height          =   855
      Left            =   8040
      TabIndex        =   21
      Top             =   5760
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":258B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest5 
      Height          =   855
      Left            =   8040
      TabIndex        =   22
      Top             =   4800
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":2766
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreRooms 
      Height          =   855
      Left            =   4080
      TabIndex        =   23
      Top             =   7680
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":2941
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreTests 
      Height          =   855
      Left            =   8040
      TabIndex        =   24
      Top             =   6720
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":2B26
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   975
      Left            =   9960
      TabIndex        =   25
      Top             =   7800
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":2D0B
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00AC897B&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Message to a co-worker"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   540
      Left            =   240
      TabIndex        =   16
      Top             =   240
      Width           =   4770
   End
End
Attribute VB_Name = "frmSendMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public ResourceName As String

Private RoomId As Long
Private ReceiverId As Long
Private SenderId As Long
Private TestId As String
Private MessageId As String

Private TotalStaff As Integer
Private CurrentStaffIndex As Integer
Private TotalRooms As Integer
Private CurrentRoomsIndex As Integer
Private TotalTests As Integer
Private CurrentTestsIndex As Integer
Private TotalMessages As Integer
Private CurrentMsgsIndex As Integer

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub ClearButtons(ClearType As String)
If (ClearType = "S") Then
    cmdStaff1.Text = ""
    cmdStaff1.Visible = False
    cmdStaff2.Text = ""
    cmdStaff2.Visible = False
    cmdStaff3.Text = ""
    cmdStaff3.Visible = False
    cmdStaff4.Text = ""
    cmdStaff4.Visible = False
    cmdStaff5.Text = ""
    cmdStaff5.Visible = False
    cmdStaff6.Text = ""
    cmdStaff6.Visible = False
    cmdStaff7.Text = ""
    cmdStaff7.Visible = False
    cmdMoreStaff.Text = "More Staff"
    cmdMoreStaff.Visible = False
ElseIf (ClearType = "M") Then
    cmdMsg1.Text = ""
    cmdMsg1.Visible = False
    cmdMsg2.Text = ""
    cmdMsg2.Visible = False
    cmdMsg3.Text = ""
    cmdMsg3.Visible = False
    cmdMsg4.Text = ""
    cmdMsg4.Visible = False
    cmdMoreMsgs.Text = "More Messages"
    cmdMoreMsgs.Visible = False
ElseIf (ClearType = "T") Then
    cmdTest1.Text = ""
    cmdTest1.Visible = False
    cmdTest2.Text = ""
    cmdTest2.Visible = False
    cmdTest3.Text = ""
    cmdTest3.Visible = False
    cmdTest4.Text = ""
    cmdTest4.Visible = False
    cmdMoreTests.Text = "More Tests"
    cmdMoreTests.Visible = False
ElseIf (ClearType = "R") Then
    cmdRoom1.Text = ""
    cmdRoom1.Visible = False
    cmdRoom2.Text = ""
    cmdRoom2.Visible = False
    cmdMoreRooms.Text = "More Rooms"
    cmdMoreRooms.Visible = False
End If
End Sub

Private Sub cmdQuit_Click()
Unload frmSendMessage
End Sub

Private Sub cmdDone_Click()
If (Trim(MessageId) = "") Then
    frmEventMsgs.Header = "Select a Message"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
ElseIf (ReceiverId < 1) Then
    frmEventMsgs.Header = "Select a Receiver"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If Not (ApplyMessage(MessageId, SenderId, ReceiverId, PatientId, RoomId, TestId)) Then
    frmEventMsgs.Header = "Message not sent"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Unload frmSendMessage
End Sub

Private Sub cmdStaff1_Click()
Call SelectStaff(cmdStaff1, 1)
End Sub

Private Sub cmdStaff2_Click()
Call SelectStaff(cmdStaff2, 2)
End Sub

Private Sub cmdStaff3_Click()
Call SelectStaff(cmdStaff3, 3)
End Sub

Private Sub cmdStaff4_Click()
Call SelectStaff(cmdStaff4, 4)
End Sub

Private Sub cmdStaff5_Click()
Call SelectStaff(cmdStaff5, 5)
End Sub

Private Sub cmdStaff6_Click()
Call SelectStaff(cmdStaff6, 6)
End Sub

Private Sub cmdStaff7_Click()
Call SelectStaff(cmdStaff7, 7)
End Sub

Private Sub SelectStaff(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    ReceiverId = val(Trim(AButton.Tag))
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    ReceiverId = 0
End If
End Sub

Private Sub cmdRoom1_Click()
Call SelectRoom(cmdRoom1, 1)
End Sub

Private Sub cmdRoom2_Click()
Call SelectRoom(cmdRoom2, 2)
End Sub

Private Sub SelectRoom(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    RoomId = val(Trim(AButton.Tag))
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    RoomId = 0
End If
End Sub

Private Sub cmdTest1_Click()
Call SelectTest(cmdTest1, 1)
End Sub

Private Sub cmdTest2_Click()
Call SelectTest(cmdTest2, 2)
End Sub

Private Sub cmdTest3_Click()
Call SelectTest(cmdTest3, 3)
End Sub

Private Sub cmdTest4_Click()
Call SelectTest(cmdTest4, 4)
End Sub

Private Sub SelectTest(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    TestId = Trim(AButton.Text)
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    TestId = ""
End If
End Sub

Private Sub cmdMsg1_Click()
Call SelectMessage(cmdMsg1, 1)
End Sub

Private Sub cmdMsg2_Click()
Call SelectMessage(cmdMsg2, 2)
End Sub

Private Sub cmdMsg3_Click()
Call SelectMessage(cmdMsg3, 3)
End Sub

Private Sub cmdMsg4_Click()
Call SelectMessage(cmdMsg4, 4)
End Sub

Private Sub SelectMessage(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    MessageId = Trim(AButton.Text)
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    MessageId = ""
End If
End Sub

Private Sub cmdMoreMsgs_Click()
CurrentMsgsIndex = CurrentMsgsIndex + 1
If (CurrentMsgsIndex > TotalMessages) Then
    CurrentMsgsIndex = 1
End If
Call LoadMessages
End Sub

Private Sub cmdMoreRooms_Click()
CurrentRoomsIndex = CurrentRoomsIndex + 1
If (CurrentRoomsIndex > TotalRooms) Then
    CurrentRoomsIndex = 1
End If
Call LoadRooms
End Sub

Private Sub cmdMoreStaff_Click()
CurrentStaffIndex = CurrentStaffIndex + 1
If (CurrentStaffIndex > TotalStaff) Then
    CurrentStaffIndex = 1
End If
Call LoadStaff
End Sub

Private Sub cmdMoreTests_Click()
CurrentTestsIndex = CurrentTestsIndex + 1
If (CurrentTestsIndex > TotalTests) Then
    CurrentTestsIndex = 1
End If
Call LoadTests
End Sub

Public Function LoadNotes() As Boolean
Dim i As Integer
LoadNotes = False
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdStaff1.BackColor
BaseTextColor = cmdStaff1.ForeColor
RoomId = 0
TestId = ""
MessageId = ""
ReceiverId = 0
CurrentRoomsIndex = 1
CurrentTestsIndex = 1
CurrentMsgsIndex = 1
CurrentStaffIndex = 1
SenderId = 0
i = InStrPS(ResourceName, "-")
If (i <> 0) Then
    SenderId = val(Mid(ResourceName, 2, i - 2))
End If
Call LoadStaff
Call LoadMessages
Call LoadTests
Call LoadRooms
LoadNotes = True
End Function

Private Function LoadRooms() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim ApplTbl As ApplicationTables
LoadRooms = False
Call ClearButtons("R")
ButtonCnt = 0
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstRooms
Call ApplTbl.ApplLoadResource("R", False, False, False, False, False)
TotalRooms = lstRooms.ListCount
If (TotalRooms > 0) Then
    i = CurrentRoomsIndex
    ButtonCnt = 1
    While (i <= TotalRooms) And (ButtonCnt < 3)
        If (ButtonCnt = 1) Then
            cmdRoom1.Text = Trim(Left(lstRooms.List(i - 1), 30))
            cmdRoom1.Tag = Mid(lstRooms.List(i - 1), 56, 5)
            cmdRoom1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdRoom2.Text = Trim(Left(lstRooms.List(i - 1), 30))
            cmdRoom2.Tag = Mid(lstRooms.List(i - 1), 56, 5)
            cmdRoom2.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentRoomsIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadRooms = True
End If
If (TotalRooms < 3) Then
    cmdMoreRooms.Visible = False
Else
    If (CurrentRoomsIndex < TotalRooms) Then
        cmdMoreRooms.Text = "More Rooms"
        cmdMoreRooms.Visible = True
    Else
        cmdMoreRooms.Text = "Start Over"
        cmdMoreRooms.Visible = True
    End If
End If
Set ApplTbl = Nothing
End Function

Private Function LoadStaff() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim ApplTbl As ApplicationTables
LoadStaff = False
Call ClearButtons("S")
ButtonCnt = 0
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstStaff
Call ApplTbl.ApplLoadResource("S", False, False, False, False, False)
TotalStaff = lstStaff.ListCount
If (TotalStaff > 0) Then
    i = CurrentStaffIndex
    ButtonCnt = 1
    While (i <= TotalStaff) And (ButtonCnt < 8)
        If (val(Mid(lstStaff.List(i - 1), 56, 5)) <> SenderId) Then
            If (ButtonCnt = 1) Then
                cmdStaff1.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff1.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdStaff2.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff2.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdStaff3.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff3.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdStaff4.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff4.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdStaff5.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff5.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdStaff6.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff6.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdStaff7.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff7.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff7.Visible = True
            End If
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
        CurrentStaffIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadStaff = True
End If
If (TotalStaff < 8) Then
    cmdMoreStaff.Visible = False
Else
    If (CurrentStaffIndex < TotalStaff) Then
        cmdMoreStaff.Text = "More Staff"
        cmdMoreStaff.Visible = True
    Else
        cmdMoreStaff.Text = "Start Over"
        cmdMoreStaff.Visible = True
    End If
End If
Set ApplTbl = Nothing
End Function

Private Function LoadMessages() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim ApplTbl As ApplicationTables
LoadMessages = False
Call ClearButtons("M")
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstMsgs
Call ApplTbl.ApplLoadCodes("Message", False, True, True)
TotalMessages = lstMsgs.ListCount
If (TotalMessages > 0) Then
    i = CurrentMsgsIndex
    ButtonCnt = 1
    While (i <= TotalMessages) And (ButtonCnt < 5)
        If (ButtonCnt = 1) Then
            cmdMsg1.Text = Trim(Left(lstMsgs.List(i - 1), 30))
            cmdMsg1.Tag = "1"
            cmdMsg1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdMsg2.Text = Trim(Left(lstMsgs.List(i - 1), 30))
            cmdMsg2.Tag = "2"
            cmdMsg2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdMsg3.Text = Trim(Left(lstMsgs.List(i - 1), 30))
            cmdMsg3.Tag = "3"
            cmdMsg3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdMsg4.Text = Trim(Left(lstMsgs.List(i - 1), 30))
            cmdMsg4.Tag = "4"
            cmdMsg4.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentMsgsIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadMessages = True
End If
If (TotalMessages < 5) Then
    cmdMoreMsgs.Visible = False
Else
    If (CurrentMsgsIndex < TotalMessages) Then
        cmdMoreMsgs.Text = "More"
        cmdMoreMsgs.Visible = True
    Else
        cmdMoreMsgs.Text = "Start Over"
        cmdMoreMsgs.Visible = True
    End If
End If
Set ApplTbl = Nothing
End Function

Private Function LoadTests() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim RetrieveClass As DynamicClass
LoadTests = False
Call ClearButtons("T")
Set RetrieveClass = New DynamicClass
RetrieveClass.QuestionSet = "First Visit"
RetrieveClass.QuestionParty = "T"
TotalTests = RetrieveClass.FindClassForms
If (TotalTests > 0) Then
    i = CurrentTestsIndex
    ButtonCnt = 1
    While (RetrieveClass.SelectClassForm(i)) And (ButtonCnt < 7)
        If (ButtonCnt = 1) Then
            cmdTest1.Text = RetrieveClass.Question
            cmdTest1.Tag = RetrieveClass.Question
            cmdTest1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdTest2.Text = RetrieveClass.Question
            cmdTest2.Tag = RetrieveClass.Question
            cmdTest2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdTest3.Text = RetrieveClass.Question
            cmdTest3.Tag = RetrieveClass.Question
            cmdTest3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdTest4.Text = RetrieveClass.Question
            cmdTest4.Tag = RetrieveClass.Question
            cmdTest4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            cmdTest5.Text = RetrieveClass.Question
            cmdTest5.Tag = RetrieveClass.Question
            cmdTest5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            cmdTest6.Text = RetrieveClass.Question
            cmdTest6.Tag = RetrieveClass.Question
            cmdTest6.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentTestsIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadTests = True
End If
If (TotalTests < 7) Then
    cmdMoreTests.Visible = False
Else
    If (CurrentTestsIndex < TotalTests) Then
        cmdMoreTests.Text = "More Tests"
        cmdMoreTests.Visible = True
    Else
        cmdMoreTests.Text = "Start Over"
        cmdMoreTests.Visible = True
    End If
End If
Set RetrieveClass = Nothing
End Function

Private Function ApplyMessage(TheMessage As String, TheSenderId As Long, TheReceiverId As Long, ThePatientId As Long, TheRoomId As Long, TestText As String) As Boolean
Dim k As Integer
Dim p1 As Long, r1 As String, b1 As Boolean
Dim p2 As Long, r2 As String, b2 As Boolean
Dim NewTime As String, NewMsg As String
Dim NewDate As String, TheDate As String
Dim PatientName As String, RoomName As String
Dim SenderName As String, ReceiverName As String
Dim LocalDate As ManagedDate
Dim ApplTbl As ApplicationTables
Dim ApplTemp As ApplicationTemplates
ApplyMessage = False
Call FormatTodaysDate(NewDate, False)
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = NewDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    NewDate = LocalDate.ExposedDate
End If
Set LocalDate = Nothing
Call FormatTimeNow(NewTime)
Set ApplTemp = New ApplicationTemplates
SenderName = "Scheduler"
Call ApplTemp.ApplGetResourceName(TheSenderId, 0, SenderName)
ReceiverName = ""
Call ApplTemp.ApplGetResourceName(TheReceiverId, 0, ReceiverName)
RoomName = ""
Call ApplTemp.ApplGetResourceName(RoomId, 0, ReceiverName)
PatientName = ""
Call ApplTemp.ApplGetPatientName(PatientId, PatientName, p1, r1, b1, p2, r2, b2, False)
' Format the Message
NewMsg = ReceiverName + ": " + TheMessage
k = InStrPS(NewMsg, "<Sender>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + SenderName + Mid(NewMsg, k + 8, Len(NewMsg) - (k + 7))
End If
k = InStrPS(NewMsg, "<Room>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + RoomName + Mid(NewMsg, k + 6, Len(NewMsg) - (k + 5))
End If
k = InStrPS(NewMsg, "<Patient>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + PatientName + Mid(NewMsg, k + 9, Len(NewMsg) - (k + 8))
End If
k = InStrPS(NewMsg, "<Test>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + TestText + Mid(NewMsg, k + 6, Len(NewMsg) - (k + 5))
End If
Set ApplTbl = New ApplicationTables
ApplyMessage = ApplTbl.ApplPostMessage(TheSenderId, TheReceiverId, NewDate, NewTime, NewMsg)
Set ApplTbl = Nothing
Set ApplTemp = Nothing
End Function
Public Sub FrmClose()
Call cmdQuit_Click
Unload Me
End Sub
