VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPatientPreCerts 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3045
      TabIndex        =   18
      Top             =   4680
      Width           =   2415
   End
   Begin VB.TextBox txtComments 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3045
      MaxLength       =   64
      TabIndex        =   2
      Top             =   4200
      Width           =   7215
   End
   Begin VB.TextBox PreCert 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3045
      MaxLength       =   32
      TabIndex        =   0
      Top             =   3225
      Width           =   5295
   End
   Begin VB.TextBox PreCertDate 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3045
      MaxLength       =   10
      TabIndex        =   1
      Top             =   3720
      Width           =   2415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9825
      TabIndex        =   3
      Top             =   6720
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientPreCerts.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   270
      TabIndex        =   4
      Top             =   6720
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientPreCerts.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSetInsurer 
      Height          =   990
      Left            =   5160
      TabIndex        =   21
      Top             =   6720
      Visible         =   0   'False
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientPreCerts.frx":03BE
   End
   Begin VB.Label lblAptDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Dr"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7200
      TabIndex        =   20
      Top             =   1440
      Width           =   1770
   End
   Begin VB.Label lblDr 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Dr"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7200
      TabIndex        =   19
      Top             =   1080
      Width           =   1770
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Locations"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1710
      TabIndex        =   17
      Top             =   4680
      Width           =   1110
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Comments"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1650
      TabIndex        =   16
      Top             =   4200
      Width           =   1170
   End
   Begin VB.Label lblBirthDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Birth:"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2235
      TabIndex        =   15
      Top             =   1080
      Width           =   585
   End
   Begin VB.Label lblPolicy 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Policy"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7200
      TabIndex        =   14
      Top             =   2520
      Width           =   705
   End
   Begin VB.Label lblClPhone 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Claim Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7200
      TabIndex        =   13
      Top             =   2160
      Width           =   1410
   End
   Begin VB.Label lblPhone 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Primary Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7200
      TabIndex        =   12
      Top             =   1800
      Width           =   1590
   End
   Begin VB.Label lblPrPhone 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Provider Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1110
      TabIndex        =   11
      Top             =   2520
      Width           =   1710
   End
   Begin VB.Label lblElPhone 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Eligibility Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   960
      TabIndex        =   10
      Top             =   2160
      Width           =   1860
   End
   Begin VB.Label lblPcPhone 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "PreCert Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1260
      TabIndex        =   9
      Top             =   1800
      Width           =   1560
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Authorization Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   690
      TabIndex        =   8
      Top             =   3720
      Width           =   2130
   End
   Begin VB.Label lblPatient 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2055
      TabIndex        =   7
      Top             =   720
      Width           =   765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Pre-Cert Reference"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   750
      TabIndex        =   6
      Top             =   3225
      Width           =   2070
   End
   Begin VB.Label lblPlan 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Plan"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2340
      TabIndex        =   5
      Top             =   1440
      Width           =   480
   End
End
Attribute VB_Name = "frmPatientPreCerts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public ThePatientId As Long
Public ThePreCertId As Long
Public ThePreCert As String
Public TheApptId As Long
Private PrimaryInsId As Long
Private PrimaryInsIdNew As Long
Private OtherPatientId1 As Long
Private OtherPatientId2 As Long
Private LocalDate As ManagedDate
Private RetrievePreCert As PatientPreCerts
Private RetrievePatient As Patient

Private Sub cmdDone_Click()
CurrentAction = ""
If (Trim(PreCertDate.Text) <> "") Then
    If Not (OkDate(Trim(PreCertDate.Text))) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        PreCertDate.SetFocus
        PreCertDate.Text = ""
        SendKeys "{Home}"
        Exit Sub
    End If
Else
    frmEventMsgs.Header = "Must select a date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    PreCertDate.SetFocus
    PreCertDate.Text = ""
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtLoc.Text) = "") Then
    frmEventMsgs.Header = "Location must be specified"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtLoc.SetFocus
    Exit Sub
End If
Set RetrievePreCert = New PatientPreCerts
If (ThePreCertId > 0) Then
    RetrievePreCert.PreCertsId = ThePreCertId
Else
    RetrievePreCert.PreCertsId = 0
End If
Call RetrievePreCert.RetrievePatientPreCerts
RetrievePreCert.PatientId = ThePatientId
RetrievePreCert.PreCerts = UCase(Trim(PreCert.Text))
RetrievePreCert.PreCertsDate = ""
LocalDate.ExposedDate = PreCertDate.Text
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    RetrievePreCert.PreCertsDate = LocalDate.ExposedDate
End If
RetrievePreCert.PreCertsInsId = 0
If (Len(lblPlan.Caption) > 75) Then
    RetrievePreCert.PreCertsInsId = val(Trim(Mid(lblPlan.Caption, 76, Len(lblPlan.Caption) - 75)))
End If
RetrievePreCert.PreCertsApptId = TheApptId
RetrievePreCert.PreCertsComments = Trim(txtComments.Text)
RetrievePreCert.PreCertsLocation = val(Trim(Mid(txtLoc.Text, 56, Len(txtLoc.Text) - 55)))
If (RetrievePreCert.ApplyPatientPreCerts) Then
    ThePreCertId = RetrievePreCert.PreCertsId
    ThePreCert = RetrievePreCert.PreCerts
End If
Set LocalDate = Nothing
Set RetrievePatient = Nothing
Set RetrievePreCert = Nothing
Unload frmPatientPreCerts
End Sub

Private Sub cmdHome_Click()
CurrentAction = "Home"
Set LocalDate = Nothing
Set RetrievePatient = Nothing
Set RetrievePreCert = Nothing
Unload frmPatientPreCerts
End Sub

Private Sub cmdSetInsurer_Click()
Dim DisplayText As String
If (PrimaryInsId <> PrimaryInsIdNew) Then
    If (PrimaryInsIdNew > 0) Then
        Call GetInsurer(PrimaryInsIdNew, DisplayText)
        lblPlan.Caption = DisplayText
        PrimaryInsId = PrimaryInsIdNew
    End If
End If
End Sub

Public Function PrecertsLoadDisplay(PatientId As Long, PreCertsId As Long, ApptId As Long) As Boolean
On Error GoTo UIError_Label
Dim RetRes As SchedulerResource
Dim DisplayText As String
PrecertsLoadDisplay = False
ThePatientId = PatientId
ThePreCertId = 0
TheApptId = ApptId
OtherPatientId1 = 0
OtherPatientId2 = 0
PrimaryInsId = 0
PrimaryInsIdNew = 0
cmdSetInsurer.Visible = False
Set LocalDate = New ManagedDate
Set RetrievePreCert = New PatientPreCerts
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PatientId
If (RetrievePatient.RetrievePatient) Then
    lblPatient.Caption = RetrievePatient.FirstName + " " _
                       + RetrievePatient.MiddleInitial + " " _
                       + RetrievePatient.LastName + " " _
                       + RetrievePatient.NameRef
    lblBirthdate.Caption = "Birth: "
    LocalDate.ExposedDate = RetrievePatient.BirthDate
    If (LocalDate.ConvertManagedDateToDisplayDate) Then
        lblBirthdate.Caption = "Birth :" + LocalDate.ExposedDate
    End If
    OtherPatientId1 = RetrievePatient.PolicyPatientId
    OtherPatientId2 = RetrievePatient.SecondPolicyPatientId
    If (PreCertsId > 0) Then
        RetrievePreCert.PreCertsId = PreCertsId
        RetrievePreCert.PreCertsApptId = ApptId
    ElseIf (ApptId > 0) Then
        RetrievePreCert.PreCertsId = 0
        RetrievePreCert.PreCertsApptId = ApptId
    End If
    If (ApptId > 0) Then
        Call GetDr(ApptId)
    End If
    If (RetrievePreCert.RetrievePatientPreCerts) Then
        ThePreCertId = RetrievePreCert.PreCertsId
        ThePreCert = RetrievePreCert.PreCerts
        TheApptId = RetrievePreCert.PreCertsApptId
        If (GetApptType(TheApptId, DisplayText)) Then
            lblPatient.Caption = Trim(lblPatient.Caption) + " for " + Trim(DisplayText)
        End If
        PreCert.Text = RetrievePreCert.PreCerts
        LocalDate.ExposedDate = RetrievePreCert.PreCertsDate
        If (LocalDate.ConvertManagedDateToDisplayDate) Then
            PreCertDate.Text = LocalDate.ExposedDate
        End If
        txtComments.Text = RetrievePreCert.PreCertsComments
        Call GetLocation(RetrievePreCert.PreCertsLocation)
        If (RetrievePreCert.PreCertsInsId > 0) Then
            PrimaryInsId = RetrievePreCert.PreCertsInsId
            If (OtherPatientId1 > 0) Then
                Call GetPrimaryInsurer(OtherPatientId1, PrimaryInsIdNew, DisplayText)
            ElseIf (OtherPatientId2 > 0) Then
                Call GetPrimaryInsurer(OtherPatientId2, PrimaryInsIdNew, DisplayText)
            Else
                Call GetPrimaryInsurer(PatientId, PrimaryInsIdNew, DisplayText)
            End If
            If (PrimaryInsIdNew <> RetrievePreCert.PreCertsInsId) Then
                cmdSetInsurer.Visible = True
            End If
            Call GetInsurer(RetrievePreCert.PreCertsInsId, DisplayText)
            lblPlan.Caption = Trim(DisplayText)
        Else
            If (OtherPatientId1 > 0) Then
                Call GetPrimaryInsurer(OtherPatientId1, PrimaryInsId, DisplayText)
                lblPlan.Caption = Trim(DisplayText)
            ElseIf (OtherPatientId2 > 0) Then
                Call GetPrimaryInsurer(OtherPatientId2, PrimaryInsId, DisplayText)
                lblPlan.Caption = Trim(DisplayText)
            ElseIf (PatientId > 0) Then
                Call GetPrimaryInsurer(PatientId, PrimaryInsId, DisplayText)
                lblPlan.Caption = Trim(DisplayText)
            End If
        End If
        PrecertsLoadDisplay = True
    Else
        If (ApptId > 0) Then
            TheApptId = ApptId
            ThePatientId = PatientId
            PreCert.Text = ""
            PreCertDate.Text = ""
            txtComments.Text = ""
            Call GetLocation(0)
            If (OtherPatientId1 > 0) Then
                Call GetPrimaryInsurer(OtherPatientId1, PrimaryInsId, DisplayText)
            ElseIf (OtherPatientId2 > 0) Then
                Call GetPrimaryInsurer(OtherPatientId2, PrimaryInsId, DisplayText)
            Else
                Call GetPrimaryInsurer(PatientId, PrimaryInsId, DisplayText)
            End If
            If (GetApptType(ApptId, DisplayText)) Then
                lblPatient.Caption = Trim(lblPatient.Caption) + " for " + Trim(DisplayText)
            End If
            PrecertsLoadDisplay = True
        End If
    End If
End If
If (PrimaryInsId < 1) Then
    PrecertsLoadDisplay = False
End If
Exit Function
UIError_Label:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientPreCerts.BorderStyle = 1
    frmPatientPreCerts.ClipControls = True
    frmPatientPreCerts.Caption = Mid(frmPatientPreCerts.Name, 4, Len(frmPatientPreCerts.Name) - 3)
    frmPatientPreCerts.AutoRedraw = True
    frmPatientPreCerts.Refresh
End If
End Sub

Private Sub PreCertDate_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (OkDate(PreCertDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        PreCertDate.SetFocus
        PreCertDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(PreCertDate, "D")
    End If
End If
End Sub

Private Sub PreCertDate_Validate(Cancel As Boolean)
Call PreCertDate_KeyPress(13)
End Sub

Private Sub txtLoc_Click()
Dim DisplayText As String
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtLoc.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub txtLoc_KeyPress(KeyAscii As Integer)
Call txtLoc_Click
KeyAscii = 0
End Sub

Private Function GetPrimaryInsurer(PatId As Long, InsId As Long, RetString As String) As Boolean
Dim DisplayIt As String
Dim RetFin As PatientFinance
Dim RetrieveInsurer As Insurer
GetPrimaryInsurer = False
RetString = ""
InsId = 0
If (PatId > 0) Then
    Set RetrieveInsurer = New Insurer
    Set RetFin = New PatientFinance
    RetFin.PatientId = PatId
    If (RetFin.RetrievePatientFinancialPrimary) Then
        InsId = RetFin.PrimaryInsurerId
        lblPolicy.Caption = "Policy:" + RetFin.PrimaryPerson
        RetrieveInsurer.InsurerId = InsId
        If (RetrieveInsurer.RetrieveInsurer) Then
            GetPrimaryInsurer = True
            RetString = Space(75)
            DisplayIt = Trim(RetrieveInsurer.InsurerName) + " " _
                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                      + Trim(RetrieveInsurer.InsurerGroupName)
            Mid(RetString, 1, Len(DisplayIt)) = DisplayIt
            RetString = RetString + Trim(Str(RetrieveInsurer.InsurerId))
            lblPlan.Caption = RetString
            Call DisplayPhone(RetrieveInsurer.InsurerElPhone, DisplayIt)
            lblElPhone.Caption = "Eligibility Phone:" + DisplayIt
            Call DisplayPhone(RetrieveInsurer.InsurerClPhone, DisplayIt)
            lblClPhone.Caption = "Claim Phone:" + DisplayIt
            Call DisplayPhone(RetrieveInsurer.InsurerPcPhone, DisplayIt)
            lblPcPhone.Caption = "PreCert Phone:" + DisplayIt
            Call DisplayPhone(RetrieveInsurer.InsurerPrPhone, DisplayIt)
            lblPrPhone.Caption = "Provider Phone:" + DisplayIt
            Call DisplayPhone(RetrieveInsurer.InsurerPhone, DisplayIt)
            lblPhone.Caption = "Primary Phone:" + DisplayIt
        End If
    End If
    Set RetFin = Nothing
    Set RetrieveInsurer = Nothing
End If
End Function

Private Function GetInsurer(InsId As Long, RetString As String) As Boolean
Dim DisplayIt As String
Dim RetrieveInsurer As Insurer
GetInsurer = False
RetString = ""
If (InsId > 0) Then
    Set RetrieveInsurer = New Insurer
    RetrieveInsurer.InsurerId = InsId
    If (RetrieveInsurer.RetrieveInsurer) Then
        GetInsurer = True
        RetString = Space(75)
        DisplayIt = Trim(RetrieveInsurer.InsurerName) + " " _
                  + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                  + Trim(RetrieveInsurer.InsurerGroupName)
        Mid(RetString, 1, Len(DisplayIt)) = DisplayIt
        RetString = RetString + Trim(Str(RetrieveInsurer.InsurerId))
        Call DisplayPhone(RetrieveInsurer.InsurerElPhone, DisplayIt)
        lblElPhone.Caption = "Eligibility Phone:" + DisplayIt
        Call DisplayPhone(RetrieveInsurer.InsurerClPhone, DisplayIt)
        lblClPhone.Caption = "Claim Phone:" + DisplayIt
        Call DisplayPhone(RetrieveInsurer.InsurerPcPhone, DisplayIt)
        lblPcPhone.Caption = "PreCert Phone:" + DisplayIt
        Call DisplayPhone(RetrieveInsurer.InsurerPrPhone, DisplayIt)
        lblPrPhone.Caption = "Provider Phone:" + DisplayIt
        Call DisplayPhone(RetrieveInsurer.InsurerPhone, DisplayIt)
        lblPhone.Caption = "Primary Phone:" + DisplayIt
    End If
    Set RetrieveInsurer = Nothing
End If
End Function

Private Function GetApptType(AId As Long, RetString As String) As Boolean
Dim RetAppt As SchedulerAppointment
Dim RetApptType As SchedulerAppointmentType
GetApptType = False
RetString = ""
If (AId > 0) Then
    Set RetApptType = New SchedulerAppointmentType
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = AId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        Set RetApptType = New SchedulerAppointmentType
        RetApptType.AppointmentTypeId = RetAppt.AppointmentTypeId
        If (RetApptType.RetrieveSchedulerAppointmentType) Then
            RetString = Trim(RetApptType.AppointmentType)
            GetApptType = True
        End If
        Set RetApptType = Nothing
    End If
    Set RetAppt = Nothing
End If
End Function

Private Sub GetLocation(LocId As Long)
Dim DisplayText As String
Dim ApplSch As ApplicationScheduler
If (LocId > 0) Then
    Set ApplSch = New ApplicationScheduler
    DisplayText = ApplSch.GetResourceLocation(LocId)
    If (Trim(DisplayText) = "") Then
        DisplayText = Space(56)
        Mid(DisplayText, 1, 7) = "Office-"
        DisplayText = DisplayText + Trim(Str(0))
    End If
    Set ApplSch = Nothing
Else
    DisplayText = Space(56)
    Mid(DisplayText, 1, 7) = "Office-"
    DisplayText = DisplayText + Trim(Str(0))
End If
txtLoc.Text = DisplayText
End Sub

Private Sub GetDr(AptId As Long)
Dim RetAppt As SchedulerAppointment
Dim RetRes As SchedulerResource
Dim LocalDate As ManagedDate
Set LocalDate = New ManagedDate
Set RetRes = New SchedulerResource
Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentId = AptId
If (RetAppt.RetrieveSchedulerAppointment) Then
    LocalDate.ExposedDate = RetAppt.AppointmentDate
    If (LocalDate.ConvertManagedDateToDisplayDate) Then
        lblAptDate.Caption = "Appointment Date:" + LocalDate.ExposedDate
    Else
        lblAptDate.Caption = "Appointment Date:"
    End If
    If (RetAppt.AppointmentResourceId1 > 0) Then
        RetRes.ResourceId = RetAppt.AppointmentResourceId1
        If (RetRes.RetrieveSchedulerResource) Then
            lblDr.Caption = "Doctor:" + Trim(RetRes.ResourceDescription)
        Else
            lblDr.Caption = "Doctor:"
        End If
    End If
End If
Set LocalDate = Nothing
Set RetRes = Nothing
Set RetAppt = Nothing
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

