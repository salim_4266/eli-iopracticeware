VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmExamMonitor 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H006C6928&
   Icon            =   "ExamMonitor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstPayment 
      Height          =   450
      ItemData        =   "ExamMonitor.frx":0442
      Left            =   2400
      List            =   "ExamMonitor.frx":0444
      TabIndex        =   23
      Top             =   720
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox lstAppts 
      Height          =   645
      Left            =   2640
      TabIndex        =   20
      Top             =   1320
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Timer Timer1 
      Interval        =   18000
      Left            =   1680
      Top             =   1440
   End
   Begin VB.ListBox lstMessages 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1710
      ItemData        =   "ExamMonitor.frx":0446
      Left            =   3480
      List            =   "ExamMonitor.frx":0448
      TabIndex        =   19
      Top             =   120
      Width           =   8415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1095
      Left            =   10200
      TabIndex        =   0
      Top             =   7200
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":044A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient1 
      Height          =   855
      Left            =   3480
      TabIndex        =   1
      Top             =   2160
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":0629
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient2 
      Height          =   855
      Left            =   3480
      TabIndex        =   2
      Top             =   3120
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":080C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient3 
      Height          =   855
      Left            =   3480
      TabIndex        =   3
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":09EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient4 
      Height          =   855
      Left            =   3480
      TabIndex        =   4
      Top             =   5040
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":0BD2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient5 
      Height          =   855
      Left            =   3480
      TabIndex        =   5
      Top             =   6000
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":0DB5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1095
      Left            =   4800
      TabIndex        =   6
      Top             =   7200
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":0F98
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPreAppt 
      Height          =   1095
      Left            =   7080
      TabIndex        =   21
      Top             =   7200
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":1177
   End
   Begin VB.Label lblRoom 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Exam Room"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.Label lblResource5 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   480
      TabIndex        =   18
      Top             =   6120
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   480
      TabIndex        =   17
      Top             =   5160
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource3 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   480
      TabIndex        =   16
      Top             =   4200
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   480
      TabIndex        =   15
      Top             =   3240
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   480
      TabIndex        =   14
      Top             =   2280
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblDate 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Date:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   1320
      Width           =   525
   End
   Begin VB.Label lblTime 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Time:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   1680
      Width           =   555
   End
   Begin VB.Label lblPatient1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7560
      TabIndex        =   11
      Top             =   2280
      Visible         =   0   'False
      Width           =   4290
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPatient2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7560
      TabIndex        =   10
      Top             =   3240
      Visible         =   0   'False
      Width           =   4290
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPatient3 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7560
      TabIndex        =   9
      Top             =   4200
      Visible         =   0   'False
      Width           =   4290
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPatient4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7560
      TabIndex        =   8
      Top             =   5160
      Visible         =   0   'False
      Width           =   4290
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPatient5 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7560
      TabIndex        =   7
      Top             =   6120
      Visible         =   0   'False
      Width           =   4290
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmExamMonitor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private ViewingRoom As String
Private TheActivityId As Long
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Private TotalExams As Long
Private CurrentIndex As Integer
Private ThePatientName As String

Private Sub ClearButtons()
cmdPatient1.Enabled = True
cmdPatient1.BackColor = BaseBackColor
cmdPatient1.ForeColor = BaseTextColor
cmdPatient1.Text = ""
cmdPatient1.Tag = ""
cmdPatient1.Visible = False
lblPatient1.Caption = ""
lblPatient1.Visible = False
lblResource1.Caption = ""
lblResource1.Visible = False
cmdPatient2.Enabled = True
cmdPatient2.BackColor = BaseBackColor
cmdPatient2.ForeColor = BaseTextColor
cmdPatient2.Text = ""
cmdPatient2.Tag = ""
cmdPatient2.Visible = False
lblPatient2.Caption = ""
lblPatient2.Visible = False
lblResource2.Caption = ""
lblResource2.Visible = False
cmdPatient3.Enabled = True
cmdPatient3.BackColor = BaseBackColor
cmdPatient3.ForeColor = BaseTextColor
cmdPatient3.Text = ""
cmdPatient3.Tag = ""
cmdPatient3.Visible = False
lblPatient3.Caption = ""
lblPatient3.Visible = False
lblResource3.Caption = ""
lblResource3.Visible = False
cmdPatient4.Enabled = True
cmdPatient4.BackColor = BaseBackColor
cmdPatient4.ForeColor = BaseTextColor
cmdPatient4.Text = ""
cmdPatient4.Tag = ""
cmdPatient4.Visible = False
lblPatient4.Caption = ""
lblPatient4.Visible = False
lblResource4.Caption = ""
lblResource4.Visible = False
cmdPatient5.Enabled = True
cmdPatient5.BackColor = BaseBackColor
cmdPatient5.ForeColor = BaseTextColor
cmdPatient5.Text = ""
cmdPatient5.Tag = ""
cmdPatient5.Visible = False
lblPatient5.Caption = ""
lblPatient5.Visible = False
lblResource5.Caption = ""
lblResource5.Visible = False
End Sub

Public Function LoadExamRoom(Init As Boolean, TheRoom As String) As Boolean
Dim p As Integer
Dim k As Integer
Dim TheLabel As Label
Dim OthLabel As Label
Dim TheButton As fpBtn
Dim Temp As String
Dim TheDate As String
Dim TheTime As String
Dim ApptDate As String
Dim LocalDate As ManagedDate
Dim ApplList As ApplicationAIList
LoadExamRoom = False
If (Init) Then
    ThePatientName = ""
    TheActivityId = 0
    CurrentIndex = 1
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = cmdHome.BackColor
    BaseTextColor = cmdHome.ForeColor
End If
ViewingRoom = TheRoom
If (ViewingRoom = "W") Then
    lblRoom.Caption = "Waiting for Questionnaire"
ElseIf (ViewingRoom = "M") Then
    lblRoom.Caption = "Waiting Room"
Else
    lblRoom.Caption = "Exam Room"
End If
Call ClearButtons
Call FormatTodaysDate(TheDate, True)
Call FormatTimeNow(TheTime)
lblDate.Caption = TheDate
If (Left(TheTime, 1) = "0") Then
    lblTime.Caption = Mid(TheTime, 2, Len(TheTime) - 1)
Else
    lblTime.Caption = TheTime
End If
Call FormatTodaysDate(ApptDate, False)
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = ApptDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    ApptDate = LocalDate.ExposedDate
End If
Set LocalDate = Nothing
Set ApplList = New ApplicationAIList
ApplList.ApplDate = Chr(1)
If CheckConfigCollection("USETODAYONLY") = "T" Then
    ApplList.ApplDate = ApptDate
End If
Set ApplList.ApplList = lstAppts
ApplList.ApplResourceId = 0
ApplList.ApplLocId = dbPracticeId
TotalExams = ApplList.ApplRetrieveRoomList(ViewingRoom)
If (TotalExams > 0) Then
    For p = CurrentIndex To CurrentIndex + 4
        If (p <= lstAppts.ListCount) Then
            k = p - (CurrentIndex - 1)
            If (k = 1) Then
                Set OthLabel = lblPatient1
                Set TheLabel = lblResource1
                Set TheButton = cmdPatient1
            ElseIf (k = 2) Then
                Set OthLabel = lblPatient2
                Set TheLabel = lblResource2
                Set TheButton = cmdPatient2
            ElseIf (k = 3) Then
                Set OthLabel = lblPatient3
                Set TheLabel = lblResource3
                Set TheButton = cmdPatient3
            ElseIf (k = 4) Then
                Set OthLabel = lblPatient4
                Set TheLabel = lblResource4
                Set TheButton = cmdPatient4
            ElseIf (k = 5) Then
                Set OthLabel = lblPatient5
                Set TheLabel = lblResource5
                Set TheButton = cmdPatient5
            End If
            Call ApplList.ApplAcquirePatient(lstAppts.List(p - 1), TheLabel, OthLabel, TheButton, p)
            Set TheLabel = Nothing
            Set OthLabel = Nothing
            Set TheButton = Nothing
        End If
    Next p
End If
CurrentIndex = p - 1
If (TotalExams < 6) Then
    cmdMore.Visible = False
Else
    If (CurrentIndex - 1 > 5) Then
        cmdMore.Text = "Start Over"
    Else
        cmdMore.Text = "More"
    End If
    cmdMore.Visible = True
End If
If (CurrentIndex > 0) Then
    LoadExamRoom = True
End If
Set ApplList.ApplList = lstMessages
Call ApplList.ApplLoadMessageBox(0)
Set ApplList = Nothing
End Function

Private Sub cmdHome_Click()
Unload frmExamMonitor
End Sub

Private Sub cmdMore_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > TotalExams) Then
    CurrentIndex = 1
End If
Call LoadExamRoom(False, ViewingRoom)
End Sub

Private Sub cmdPreAppt_Click()
Dim q As Integer
Dim ApptId As Long
Dim PatId As Long
Dim Temp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Timer1.Enabled = False
If (TheActivityId > 0) Then
    Set ApplList = New ApplicationAIList
    Call WhatsActive(q)
    ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(q - 1))
    PatId = ApplList.ApplGetPatientId(lstAppts.List(q - 1))
    Set ApplList = Nothing
    Set ApplTemp = New ApplicationTemplates
    Set ApplTemp.lstBox = lstPayment
    Call ApplTemp.PrintTransaction(ApptId, PatId, "C", True, True, "", False, 0, "S", "", 0)
    Set ApplTemp = Nothing
Else
    frmEventMsgs.Header = "Must select a patient"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Timer1.Enabled = True
End Sub

Private Sub cmdPatient1_Click()
Call SelectPatient(cmdPatient1, 1)
End Sub

Private Sub cmdPatient2_Click()
Call SelectPatient(cmdPatient2, 2)
End Sub

Private Sub cmdPatient3_Click()
Call SelectPatient(cmdPatient3, 3)
End Sub

Private Sub cmdPatient4_Click()
Call SelectPatient(cmdPatient4, 4)
End Sub

Private Sub cmdPatient5_Click()
Call SelectPatient(cmdPatient5, 5)
End Sub

Private Sub TurnOn()
cmdPatient1.Enabled = True
cmdPatient2.Enabled = True
cmdPatient3.Enabled = True
cmdPatient4.Enabled = True
cmdPatient5.Enabled = True
End Sub

Private Sub TurnOff(Ref As Integer)
If (Ref <> 1) Then
    cmdPatient1.Enabled = False
End If
If (Ref <> 2) Then
    cmdPatient2.Enabled = False
End If
If (Ref <> 3) Then
    cmdPatient3.Enabled = False
End If
If (Ref <> 4) Then
    cmdPatient4.Enabled = False
End If
If (Ref <> 5) Then
    cmdPatient5.Enabled = False
End If
End Sub

Private Sub SelectPatient(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    Call TurnOff(Ref)
    TheActivityId = val(Trim(AButton.Tag))
    ThePatientName = AButton.Text
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    Call TurnOn
    TheActivityId = 0
    ThePatientName = ""
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmExamMonitor.BorderStyle = 1
    frmExamMonitor.ClipControls = True
    frmExamMonitor.Caption = Mid(frmExamMonitor.Name, 4, Len(frmExamMonitor.Name) - 3)
    frmExamMonitor.AutoRedraw = True
    frmExamMonitor.Refresh
End If
End Sub

Private Sub lstMessages_DblClick()
Dim ApplList As ApplicationAIList
If (lstMessages.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplSendMessageBox(lstMessages.List(lstMessages.ListIndex))
    Set ApplList = Nothing
End If
End Sub

Private Function WhatsActive(Ref As Integer) As Integer
WhatsActive = 0
Ref = 0
If (cmdPatient1.BackColor <> BaseBackColor) Then
    WhatsActive = 1
    Ref = val(lblPatient1.Tag)
ElseIf (cmdPatient2.BackColor <> BaseBackColor) Then
    WhatsActive = 2
    Ref = val(lblPatient2.Tag)
ElseIf (cmdPatient3.BackColor <> BaseBackColor) Then
    WhatsActive = 3
    Ref = val(lblPatient3.Tag)
ElseIf (cmdPatient4.BackColor <> BaseBackColor) Then
    WhatsActive = 4
    Ref = val(lblPatient4.Tag)
ElseIf (cmdPatient5.BackColor <> BaseBackColor) Then
    WhatsActive = 5
    Ref = val(lblPatient5.Tag)
End If
End Function

Private Sub Timer1_Timer()
frmExamMonitor.Enabled = False
Call LoadExamRoom(True, ViewingRoom)
frmExamMonitor.Enabled = True
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
