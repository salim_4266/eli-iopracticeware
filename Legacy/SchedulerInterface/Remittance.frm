VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmRemittance 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      BackColor       =   &H006C6928&
      Height          =   2775
      Left            =   2160
      TabIndex        =   14
      Top             =   2400
      Visible         =   0   'False
      Width           =   7575
      Begin VB.ComboBox PaymentMethodCombo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   240
         Width           =   3015
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdCancel 
         Height          =   615
         Left            =   2400
         TabIndex        =   15
         Top             =   1920
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "Remittance.frx":0000
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdSubmit 
         Height          =   615
         Left            =   4150
         TabIndex        =   16
         Top             =   1920
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "Remittance.frx":01E1
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
         Height          =   375
         Left            =   4200
         TabIndex        =   20
         Top             =   960
         Width           =   3015
         _Version        =   65537
         _ExtentX        =   5318
         _ExtentY        =   661
         _StockProps     =   93
         ForeColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1999/1/1"
         MaxDate         =   "2050/12/31"
         EditMode        =   0
         ShowCentury     =   -1  'True
      End
      Begin VB.Label Label1 
         BackColor       =   &H006C6928&
         Caption         =   "Select the payment date:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   3615
      End
      Begin VB.Label Label1 
         BackColor       =   &H006C6928&
         Caption         =   "Select the payment method:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   3975
      End
   End
   Begin VB.ListBox lstReport 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   7470
      ItemData        =   "Remittance.frx":03C0
      Left            =   120
      List            =   "Remittance.frx":03C2
      TabIndex        =   9
      Top             =   1080
      Visible         =   0   'False
      Width           =   11775
   End
   Begin VB.PictureBox PicStatus 
      BackColor       =   &H006C6928&
      Height          =   855
      Left            =   4320
      ScaleHeight     =   795
      ScaleWidth      =   3675
      TabIndex        =   11
      Top             =   2280
      Visible         =   0   'False
      Width           =   3735
      Begin VB.Label lblClaim 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H006C6928&
         Caption         =   "1 of 10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005ED2F0&
         Height          =   330
         Left            =   2160
         TabIndex        =   13
         Top             =   240
         Width           =   1395
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H006C6928&
         Caption         =   "Process Claim"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H005ED2F0&
         Height          =   330
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   1875
      End
   End
   Begin VB.ListBox lstPLB 
      Height          =   840
      Left            =   1320
      TabIndex        =   10
      Top             =   120
      Visible         =   0   'False
      Width           =   2655
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   1
      Top             =   7440
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Remittance.frx":03C4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10080
      TabIndex        =   2
      Top             =   7440
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Remittance.frx":05A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   990
      Left            =   5400
      TabIndex        =   6
      Top             =   7440
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Remittance.frx":0782
   End
   Begin VB.ListBox lstAcks 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5895
      ItemData        =   "Remittance.frx":096D
      Left            =   120
      List            =   "Remittance.frx":096F
      TabIndex        =   3
      Top             =   1080
      Width           =   11775
   End
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5970
      ItemData        =   "Remittance.frx":0971
      Left            =   120
      List            =   "Remittance.frx":0973
      TabIndex        =   0
      Top             =   1080
      Width           =   11775
   End
   Begin VB.Label lblDisp 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "For File "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   240
      Left            =   120
      TabIndex        =   8
      Top             =   360
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label lblWho 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "For File "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   240
      Left            =   5280
      TabIndex        =   7
      Top             =   840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Process Claim Remittances"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   330
      Left            =   4050
      TabIndex        =   5
      Top             =   240
      Width           =   3555
   End
   Begin VB.Label lblFile 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "For File "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   720
   End
End
Attribute VB_Name = "frmRemittance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private AckDate As String
Private HipaaOn As Boolean
Private RejectFile As String
Private ResultsFile As String
Private SecondaryAdjustmentsFile As String
Public clms As Integer


Private Sub cmdCancel_Click()
Frame1.Visible = False
Frame1.Enabled = False
lstAcks.Enabled = True
cmdPost.Enabled = True
End Sub

Private Sub cmdDone_Click()
If (lstAcks.Visible) Then
    lstAcks.Visible = False
    lstAppts.Visible = True
Else
    Unload frmRemittance
End If
End Sub

Private Sub cmdHome_Click()
Unload frmRemittance
End Sub

Public Function LoadRemittance() As Boolean
Dim AFile As String
Dim MyFile As String
LoadRemittance = False
AckDate = ""
HipaaOn = False
lstAcks.Clear
lstAcks.Visible = False
lstAppts.Clear
lblFile.Visible = False
MyFile = RemitDirectory + "*.*"
AFile = Dir(MyFile)
While (AFile <> "")
    If (InStrPS(AFile, "Result") = 0) And (InStrPS(AFile, "Reject") = 0) Then
        lstAppts.AddItem AFile
    End If
    AFile = Dir
Wend
If (lstAppts.ListCount > 0) Then
    LoadRemittance = True
End If
lstPLB.Clear
cmdPost.Enabled = True
cmdDone.Enabled = True
cmdHome.Enabled = True
End Function

Private Sub cmdPost_Click()
If (lstAcks.ListCount > 0) Then
    Frame1.Visible = True
    Frame1.Enabled = True
    lstAcks.Enabled = False
    cmdPost.Enabled = False
    Call LoadPaymentMethod
End If
End Sub

Private Sub cmdSubmit_Click()
Dim PaymentMethod As String
Dim PaymentDate As String
Frame1.Visible = False
Frame1.Enabled = False
PaymentMethod = PaymentMethodCombo.Text
PaymentDate = SSDateCombo1.Text
If (Trim(PaymentMethod) <> "" And OkDate(PaymentDate)) Then
        DoEvents
        cmdPost.Enabled = False
        cmdDone.Enabled = False
        cmdHome.Enabled = False
        If (HipaaOn) Then
            'Call ProcessRemitHipaa
            PaymentMethod = Trim(Mid(PaymentMethod, InStrPS(1, PaymentMethod, "-") + 1, Len(PaymentMethod)))
            Dim PmntBill As New CPaymentsBilling
            Call PmntBill.ProcFile(RemitDirectory & Mid(lblFile.Caption, 17), PaymentMethod, PaymentDate)
            Set PmntBill = Nothing
            lstAcks.Visible = False
            lstAppts.Visible = True
            cmdPost.Enabled = True
            cmdDone.Enabled = True
            cmdHome.Enabled = True
            Call LoadRemittance
        Else
            Call ProcessRemit
        End If
End If
Frame1.Visible = False
Frame1.Enabled = False
lstAcks.Enabled = True
End Sub

Private Sub lstAppts_Click()
Dim Idx As Integer
If (lstAppts.ListIndex >= 0) Then
    If Not (VerifyCompletedFile(RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))) Then
        HipaaOn = False
        lblFile.Caption = "Processing File " + Trim(lstAppts.List(lstAppts.ListIndex))
        lblFile.Visible = True
        If (FM.IsFileThere(RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))) Then
            Idx = VerifyFileType(RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))
            If (Idx = 1) Then
                HipaaOn = True
'''''''                If Not (LoadRemitFileHipaa(RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))) Then
                Dim PayBill As New CPaymentsBilling
                Dim CAFile As String
                Dim ClstPLB As New Collection
                Dim ClstAcks As New Collection
                CAFile = RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex))
                lstPLB.Clear
                lstAcks.Clear
                clms = 0
                If PayBill.LoadRemitFileHipaa(CAFile, ClstPLB, ClstAcks, lblWho, clms) Then
                    Dim l
                    For Each l In ClstPLB
                        lstPLB.AddItem l
                    Next
                    For Each l In ClstAcks
                        lstAcks.AddItem l
                    Next
                    lstAcks.Visible = True
                    lstAppts.Visible = False
                    AckDate = "20" + Left(lstAcks.List(1), 6)
                Else
                    frmEventMsgs.Header = "No Remittance Transactions."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
                Set PayBill = Nothing
            ElseIf (Idx = 2) Then
                HipaaOn = False
                If Not (LoadRemitFile(RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))) Then
                    frmEventMsgs.Header = "No Remittance Transactions."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
            End If
        End If
    Else
        frmEventMsgs.Header = "File already processed."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Function LoadRemitFile(AFile As String) As Boolean
Dim FileNum As Long
Dim Rec As String
Dim Temp As String
Dim TheDate As String
Dim DisplayText As String
Dim NSFArray(50) As String
On Error GoTo LeaveFast
LoadRemitFile = False
lstAcks.Clear
If (FM.IsFileThere(AFile)) Then
    Rec = ""
    FileNum = FreeFile
    FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
    While Not (EOF(FileNum))
        Line Input #FileNum, Rec
        If (Trim(Rec) <> "") Then
            If (Left(Rec, 3) = "100") Then
' File Header (Only 1)
                Erase NSFArray
                NSFArray(1) = Mid(Rec, 4, 10)
                NSFArray(2) = Mid(Rec, 14, 16)
                NSFArray(3) = Mid(Rec, 39, 16)
                NSFArray(4) = Mid(Rec, 61, 33)
                NSFArray(5) = Mid(Rec, 94, 8)
                NSFArray(6) = ""
                NSFArray(7) = ""
                NSFArray(8) = ""
                NSFArray(9) = ""
                lblWho.Caption = "P:" + Trim(NSFArray(2)) + " - " + Trim(NSFArray(4))
                lblWho.Visible = True
            ElseIf (Left(Rec, 3) = "200") Then
' Batch Header (Only 1)
                Erase NSFArray
                NSFArray(1) = Mid(Rec, 4, 10)
                NSFArray(2) = Mid(Rec, 14, 1)   ' source of payment
                NSFArray(3) = Mid(Rec, 15, 15)  ' neic
                NSFArray(4) = Mid(Rec, 30, 4)   ' batch #
                NSFArray(5) = Mid(Rec, 34, 33)
                NSFArray(6) = Mid(Rec, 67, 15)  ' provider #
                NSFArray(7) = Mid(Rec, 82, 15)  ' check no
                NSFArray(8) = Mid(Rec, 97, 8)   ' check date
                NSFArray(9) = Mid(Rec, 105, 8)
                NSFArray(10) = Mid(Rec, 113, 15)
                NSFArray(11) = Mid(Rec, 128, 15)
                NSFArray(12) = Mid(Rec, 143, 15)
                NSFArray(13) = Mid(Rec, 158, 15)
                NSFArray(14) = Mid(Rec, 173, 1)
                NSFArray(15) = Mid(Rec, 174, 3)
                NSFArray(16) = Mid(Rec, 177, 3)
                NSFArray(17) = Mid(Rec, 180, 2)
                NSFArray(18) = Mid(Rec, 182, 1) ' assignment indicator
                DisplayText = Space(75)
                Mid(DisplayText, 1, 11) = "Batch Start"
                DisplayText = DisplayText + "P"
                lstAcks.AddItem DisplayText
                DisplayText = Space(75)
                Mid(DisplayText, 1, 1) = NSFArray(2)
                Mid(DisplayText, 3, 15) = NSFArray(3)
                Mid(DisplayText, 20, 4) = NSFArray(4)
                Mid(DisplayText, 25, 15) = NSFArray(6)
                Mid(DisplayText, 41, 15) = NSFArray(7)
                Mid(DisplayText, 56, 8) = NSFArray(8)
                Mid(DisplayText, 65, 8) = NSFArray(9)
                Mid(DisplayText, 75, 1) = NSFArray(18)
                DisplayText = DisplayText + "P"
                lstAcks.AddItem DisplayText
            ElseIf (Left(Rec, 3) = "400") Then
' Patient Claim Data (1 per Claim)
                Erase NSFArray
                NSFArray(1) = Mid(Rec, 4, 10)
                NSFArray(2) = Mid(Rec, 14, 17)  ' pat control #
                NSFArray(3) = Mid(Rec, 31, 15)  ' check #
                NSFArray(4) = Mid(Rec, 46, 8)   ' check date
                NSFArray(5) = Mid(Rec, 54, 20)  ' group
                NSFArray(6) = Mid(Rec, 74, 25)  ' insurer id
                NSFArray(7) = Mid(Rec, 99, 1)
                NSFArray(8) = Mid(Rec, 100, 20)
                NSFArray(9) = Mid(Rec, 120, 12)
                NSFArray(10) = Mid(Rec, 132, 1)
                NSFArray(11) = Mid(Rec, 133, 12)
                NSFArray(12) = Mid(Rec, 145, 20)    ' pat last name
                NSFArray(13) = Mid(Rec, 165, 12)    ' pat first name
                NSFArray(14) = Mid(Rec, 177, 1)
                NSFArray(15) = Mid(Rec, 178, 1)
                NSFArray(16) = Mid(Rec, 179, 8)
                NSFArray(17) = Mid(Rec, 187, 1)
                NSFArray(18) = Mid(Rec, 188, 2) ' claim status
                NSFArray(19) = Mid(Rec, 190, 10)
                NSFArray(20) = Mid(Rec, 200, 17)
                NSFArray(21) = Mid(Rec, 217, 17)
                NSFArray(22) = Mid(Rec, 234, 25)
                NSFArray(23) = Mid(Rec, 259, 1)
                DisplayText = Space(75)
                Mid(DisplayText, 1, 25) = NSFArray(6)
                Mid(DisplayText, 27, 20) = NSFArray(5)
                Mid(DisplayText, 49, 15) = NSFArray(3)
                Mid(DisplayText, 65, 2) = NSFArray(18)
                If (NSFArray(18) = "00") Then
                    Temp = "Control "
                ElseIf (NSFArray(18) = "01") Then
                    Temp = "Reversal"
                ElseIf (NSFArray(18) = "05") Or (NSFArray(18) = "06") Or _
                       (NSFArray(18) = "07") Or (NSFArray(18) = "08") Or _
                       (NSFArray(18) = "09") Then
                    Temp = "Transfer"
                ElseIf (NSFArray(18) = "10") Then
                    Temp = "Progress"
                ElseIf (NSFArray(18) = "20") Then
                    Temp = "Develpmt"
                ElseIf (NSFArray(18) = "30") Then
                    Temp = "Review  "
                ElseIf (NSFArray(18) = "40") Then
                    Temp = "Srvs Rvw"
                ElseIf (NSFArray(18) = "50") Then
                    Temp = "Paid    "
                ElseIf (NSFArray(18) = "60") Then
                    Temp = "Paid 2nd"
                ElseIf (NSFArray(18) = "70") Then
                    Temp = "Paid 3rd"
                ElseIf (NSFArray(18) = "80") Then
                    Temp = "No Paymt"
                ElseIf (NSFArray(18) = "90") Then
                    Temp = "Rejected"
                Else
                    Temp = Space(8)
                End If
                Mid(DisplayText, 67, 8) = Temp
                DisplayText = DisplayText + "C"
                lstAcks.AddItem DisplayText
            ElseIf (Left(Rec, 3) = "450") Then
' Service Line - Part 1
                Erase NSFArray
                NSFArray(1) = Mid(Rec, 4, 10)
                NSFArray(2) = Mid(Rec, 14, 17)
                NSFArray(3) = Mid(Rec, 31, 17)  ' Line Item
                NSFArray(4) = Mid(Rec, 48, 3)   ' Service Line #
                NSFArray(5) = Mid(Rec, 51, 2)   ' Line Item Status
                NSFArray(6) = Mid(Rec, 53, 8)   ' From Date
                NSFArray(7) = Mid(Rec, 61, 8)   ' To Date
                NSFArray(8) = Mid(Rec, 69, 8)
                NSFArray(9) = Mid(Rec, 77, 15)
                NSFArray(10) = Mid(Rec, 92, 2)
                NSFArray(11) = Mid(Rec, 94, 2)
                NSFArray(12) = Mid(Rec, 96, 5)  ' Service
                NSFArray(13) = Mid(Rec, 101, 2) ' Modifier 1
                NSFArray(14) = Mid(Rec, 103, 2) ' Modifier 2
                NSFArray(15) = Mid(Rec, 105, 2) ' Modifier 3
                NSFArray(16) = Mid(Rec, 107, 4) ' Units
                NSFArray(17) = Mid(Rec, 111, 7) ' Line Charge
                NSFArray(18) = Mid(Rec, 118, 7)
                NSFArray(19) = Mid(Rec, 125, 7)
                NSFArray(20) = Mid(Rec, 132, 7)
                NSFArray(21) = Mid(Rec, 139, 7)
                NSFArray(22) = Mid(Rec, 146, 7)
                NSFArray(23) = Mid(Rec, 153, 7)
                NSFArray(24) = Mid(Rec, 160, 7)
                NSFArray(25) = Mid(Rec, 167, 7)
                NSFArray(26) = Mid(Rec, 174, 7) ' Adjustment Amt
                NSFArray(27) = Mid(Rec, 181, 7) ' Payment Amt
                NSFArray(28) = Mid(Rec, 188, 7)
                NSFArray(29) = Mid(Rec, 195, 7)
                NSFArray(30) = Mid(Rec, 202, 7)
                NSFArray(31) = Mid(Rec, 209, 7)
                NSFArray(32) = Mid(Rec, 216, 7)
                NSFArray(33) = Mid(Rec, 223, 5)
                NSFArray(34) = Mid(Rec, 228, 1)
                NSFArray(35) = Mid(Rec, 229, 15)
                NSFArray(36) = Mid(Rec, 244, 15)
                NSFArray(37) = Mid(Rec, 259, 6)
                NSFArray(38) = Mid(Rec, 265, 6)
                NSFArray(39) = Mid(Rec, 271, 6)
                NSFArray(40) = Mid(Rec, 277, 6)
                NSFArray(41) = Mid(Rec, 283, 6)
                NSFArray(42) = Mid(Rec, 289, 6)
                NSFArray(43) = Mid(Rec, 295, 6)
                NSFArray(44) = Mid(Rec, 301, 17)
                NSFArray(45) = Mid(Rec, 318, 2)
                DisplayText = Space(75)
                Mid(DisplayText, 1, 17) = NSFArray(2)
                Mid(DisplayText, 19, 3) = NSFArray(4)
                Mid(DisplayText, 23, 8) = NSFArray(6)
                Mid(DisplayText, 32, 5) = NSFArray(12)
                Mid(DisplayText, 37, 2) = NSFArray(13)
                Mid(DisplayText, 39, 2) = NSFArray(14)
                Mid(DisplayText, 41, 2) = NSFArray(15)
                Mid(DisplayText, 43, 2) = NSFArray(45)
                Mid(DisplayText, 46, 4) = NSFArray(16)
                Mid(DisplayText, 51, 7) = NSFArray(17)
                Mid(DisplayText, 59, 7) = NSFArray(26)
                Mid(DisplayText, 67, 7) = NSFArray(27)
                Mid(DisplayText, 11, 6) = NSFArray(37)
                DisplayText = DisplayText + "S1"
                lstAcks.AddItem DisplayText
            ElseIf (Left(Rec, 3) = "451") Then
' Service Line - Part 2
                Erase NSFArray
                NSFArray(1) = Mid(Rec, 4, 10)
                NSFArray(2) = Mid(Rec, 14, 17)
                NSFArray(3) = Mid(Rec, 31, 17)
                NSFArray(4) = Mid(Rec, 48, 3)
                NSFArray(5) = Mid(Rec, 51, 17)
                NSFArray(6) = Mid(Rec, 68, 7)
                NSFArray(7) = Mid(Rec, 75, 7)   ' Patient owes
                NSFArray(8) = Mid(Rec, 82, 5)
                NSFArray(9) = Mid(Rec, 87, 7)
                NSFArray(10) = Mid(Rec, 94, 7)
                NSFArray(11) = Mid(Rec, 101, 7)
                NSFArray(12) = Mid(Rec, 108, 7)
                NSFArray(13) = Mid(Rec, 115, 7)
                NSFArray(14) = Mid(Rec, 122, 58)
                NSFArray(15) = Mid(Rec, 180, 7) ' Patient Paid
                NSFArray(16) = Mid(Rec, 187, 4)
                DisplayText = Space(75)
                Mid(DisplayText, 1, 17) = NSFArray(2)
                Mid(DisplayText, 19, 3) = NSFArray(4)
                Mid(DisplayText, 23, 7) = NSFArray(7)
                Mid(DisplayText, 31, 7) = NSFArray(15)
                DisplayText = DisplayText + "S2"
'                lstAcks.AddItem DisplayText
            ElseIf (Left(Rec, 3) = "500") Then
                Erase NSFArray
                DisplayText = Space(75)
                Mid(DisplayText, 1, 9) = "Claim End"
                DisplayText = DisplayText + "X"
                lstAcks.AddItem DisplayText
            ElseIf (Left(Rec, 3) = "700") Then
' Provider Adjustment (Not Claim Related, Only 1)
            ElseIf (Left(Rec, 3) = "800") Then
' Batch Trailer (Only 1)
            ElseIf (Left(Rec, 3) = "900") Then
' File Trailer (Only 1)
                DisplayText = Space(75)
                Mid(DisplayText, 1, 9) = "Batch End"
                DisplayText = DisplayText + "Z"
                lstAcks.AddItem DisplayText
            End If
        End If
    Wend
    If (Trim(Rec) <> "") Then
        lstAcks.AddItem Rec
    End If
    FM.CloseFile CLng(FileNum)
    If (lstAcks.ListCount > 0) Then
        lstAcks.Visible = True
        lstAppts.Visible = False
        AckDate = "20" + Left(lstAcks.List(1), 6)
        LoadRemitFile = True
    End If
End If
ByeBye:
Exit Function
LeaveFast:
    FM.CloseFile CLng(FileNum)
    If (lstAcks.ListCount > 0) Then
        lstAcks.Visible = True
        lstAppts.Visible = False
        LoadRemitFile = True
    End If
    Resume ByeBye
End Function

'''''''Private Function LoadRemitFileHipaa(AFile As String) As Boolean
'''''''Dim MaxCnt As Long
'''''''Dim CurCnt As Long
'''''''Dim FileNum As Long
'''''''Dim q As Integer
'''''''Dim i As Integer, j As Integer
'''''''Dim p As Integer, z As Integer
'''''''Dim Rec As String, Tp As String
'''''''Dim Temp As String, TheDate As String
'''''''
'''''''Dim TId As String
'''''''Dim BatchNumber As String
'''''''Dim PayerRef As String, PayerMeth As String
'''''''Dim PayerDate As String, PayerRsn As String
'''''''
'''''''Dim PatLName As String, PatFName As String
'''''''Dim PatIdRef As String, PatIdNumber As String
'''''''Dim InsdLName As String, InsdFName As String
'''''''Dim InsdIdRef As String, InsdIdNumber As String
'''''''
'''''''Dim CheckAmount As String
'''''''Dim PlanName As String, PlanAddress As String
'''''''Dim PlanCity As String, PlanState As String
'''''''Dim PlanZip As String, PlanId As String
'''''''
'''''''Dim ClmDate As String, ClmPatId As String
'''''''Dim ClmSta As String, ClmTmp As String
'''''''Dim ClmCharge As String, ClmPayAmt As String
'''''''Dim ClmPatRsp As String, ClmAplId As String
'''''''Dim ClmAmtRef As String, ClmAmt As String
'''''''Dim ClmQtyRef As String, ClmQty As String
'''''''Dim ClmAdjCode As String
'''''''Dim ClmAdjRsn As String, ClmAdjAmt As String
'''''''
'''''''Dim SrvAllCnt As Integer
'''''''Dim SrvOn As Boolean, SrvCnt As Integer
'''''''Dim SrvDate(30) As String, SrvCode(30) As String, SrvModifier(30) As String
'''''''Dim SrvChg(30) As String, SrvAllow(30) As String
'''''''Dim SrvPay(30) As String, SrvQty(30) As String
'''''''Dim SrvAdj(30) As String, SrvRsn(30) As String, SrvAdjAmt(30) As String
'''''''Dim SrvPatAmt(30) As String, SrvPatRsn(30) As String
'''''''
'''''''Dim PostPayerOn As Boolean
'''''''Dim DisplayText As String
'''''''Dim NSFArray(50) As String
'''''''
'''''''On Error GoTo LeaveFast
'''''''LoadRemitFileHipaa = False
'''''''lstPLB.Clear
'''''''lstAcks.Clear
'''''''If (FM.IsFileThere(AFile)) Then
'''''''    Rec = ""
'''''''    PostPayerOn = False
'''''''    FileNum = FreeFile
'''''''    CurCnt = 0
'''''''    MaxCnt = FM.GetFileLength(AFile)
'''''''    Open AFile For Binary As #FileNum
'''''''    While (CurCnt < MaxCnt)
'''''''        GoSub GetNextRecord
'''''''        If (Trim(Rec) <> "") Then
'''''''            If (Left(Rec, 3) = "ISA") Then
'''''''                GoSub InitAll
'''''''                DisplayText = Space(75)
'''''''                Mid(DisplayText, 1, 11) = "Batch Start"
'''''''                DisplayText = DisplayText + "Y"
'''''''                lstAcks.AddItem DisplayText
'''''''            ElseIf (Left(Rec, 2) = "GS") Then
'''''''                GoSub ParseRecord
'''''''                lblWho.Caption = ""
'''''''                lblWho.Caption = "P:" + Trim(NSFArray(3)) + " - " + Trim(NSFArray(4))
'''''''                lblWho.Visible = True
'''''''            ElseIf (Left(Rec, 2) = "ST") Then
'''''''                GoSub ParseRecord
'''''''                BatchNumber = NSFArray(3)
'''''''            ElseIf (Left(Rec, 3) = "BPR") Then
'''''''                GoSub ParseRecord
'''''''                PayerMeth = ""
'''''''                If (NSFArray(5) = "CHK") Then
'''''''                    PayerMeth = "K"
'''''''                ElseIf (NSFArray(5) = "ACH") Or (NSFArray(5) = "FWT") Or (NSFArray(5) = "BOP") Then
'''''''                    PayerMeth = "W"
'''''''                End If
'''''''                PayerDate = NSFArray(17)
'''''''                CheckAmount = NSFArray(3)
'''''''            ElseIf (Left(Rec, 3) = "TRN") Then
'''''''                GoSub ParseRecord
'''''''                PayerRef = Trim(NSFArray(3))
'''''''                If (Len(PayerRef) > 24) Then
'''''''                    PayerRef = Mid(PayerRef, Len(PayerRef) - 24, 24)
'''''''                End If
'''''''            ElseIf (Left(Rec, 3) = "REF") Then
'''''''                GoSub ParseRecord
'''''''                If (NSFArray(2) = "EV") Then
'''''''
'''''''                ElseIf (NSFArray(2) = "F2") Or (NSFArray(2) = "VP") Then
'''''''
'''''''                ElseIf (NSFArray(2) = "2U") Then
'''''''                    If (Trim(PayerRef) = "") Then
'''''''                        PayerRef = NSFArray(3)
'''''''                    End If
'''''''                ElseIf (InStrPS("0B1A1B1C1D1E1F1G1HD3G2N5PQTJ", NSFArray(2)) > 0) Then
'''''''
'''''''                ElseIf (InStrPS("1L1W9A9CA6BBCEEAF8G1G3IGSY", NSFArray(2)) > 0) Then
'''''''
'''''''                ElseIf (InStrPS("1S6RBBE9G1G3LURB", NSFArray(2)) > 0) Then
'''''''
'''''''                ElseIf (InStrPS("1A1B1C1D1G1H1JSYTJ", NSFArray(2)) > 0) Or (NSFArray(2) = "HPI") Then
'''''''
'''''''                End If
'''''''            ElseIf (Left(Rec, 3) = "DTM") Then
'''''''                GoSub ParseRecord
'''''''                If (NSFArray(2) = "405") Or (NSFArray(2) = "050") Then
'''''''                    ClmDate = NSFArray(3)
'''''''                ElseIf (NSFArray(2) = "036") Or (NSFArray(2) = "232") Or (NSFArray(2) = "233") Then
'''''''
'''''''                ElseIf (NSFArray(2) = "472") Or (NSFArray(2) = "150") Or (NSFArray(2) = "151") Then
'''''''                    SrvDate(SrvCnt) = NSFArray(3)
'''''''                End If
'''''''            ElseIf (Left(Rec, 2) = "N1") Then
'''''''                GoSub ParseRecord
'''''''                If (NSFArray(2) = "PR") Then
'''''''                    PlanName = NSFArray(3)
'''''''                    PlanId = NSFArray(5)
'''''''                    GoSub GetNextRecord
'''''''                    GoSub ParseRecord
'''''''                    If (Left(Rec, 2) = "N3") Then
'''''''                        PlanAddress = NSFArray(2)
'''''''                    End If
'''''''                    GoSub GetNextRecord
'''''''                    GoSub ParseRecord
'''''''                    If (Left(Rec, 2) = "N4") Then
'''''''                        PlanCity = NSFArray(2)
'''''''                        PlanState = NSFArray(3)
'''''''                        PlanZip = NSFArray(4)
'''''''                    End If
'''''''                ElseIf (NSFArray(2) = "PE") Then
'''''''                    GoSub GetNextRecord
'''''''                    GoSub ParseRecord
'''''''                    If (Left(Rec, 2) = "N3") Then
'''''''
'''''''                    End If
'''''''                    GoSub GetNextRecord
'''''''                    GoSub ParseRecord
'''''''                    If (Left(Rec, 2) = "N4") Then
'''''''
'''''''                    End If
'''''''                End If
'''''''            ElseIf (Left(Rec, 3) = "NM1") Then
'''''''                GoSub ParseRecord
'''''''                If (NSFArray(2) = "QC") Then
'''''''                    PatLName = NSFArray(4)
'''''''                    PatFName = NSFArray(5)
'''''''                    PatIdRef = NSFArray(9)
'''''''                    PatIdNumber = NSFArray(10)
'''''''                    GoSub SetupPat
'''''''                ElseIf (NSFArray(2) = "IL") Then
'''''''                    InsdLName = NSFArray(4)
'''''''                    InsdFName = NSFArray(5)
'''''''                    InsdIdRef = NSFArray(9)
'''''''                    InsdIdNumber = NSFArray(10)
'''''''                    GoSub SetUpInsured
'''''''                ElseIf (NSFArray(2) = "74") Then
'''''''
'''''''                ElseIf (NSFArray(2) = "TT") Then
'''''''
'''''''                ElseIf (NSFArray(2) = "PR") Then
'''''''
'''''''                End If
'''''''            ElseIf (Left(Rec, 3) = "PER") Then
'''''''' No action
'''''''            ElseIf (Left(Rec, 3) = "TS3") Then
'''''''' No action
'''''''            ElseIf (Left(Rec, 3) = "TS2") Then
'''''''' No action
'''''''            ElseIf (Left(Rec, 2) = "LX") Then
'''''''                GoSub ParseRecord
'''''''                TId = NSFArray(2)
'''''''                GoSub SetupPayor
'''''''            ElseIf (Left(Rec, 3) = "CLP") Then
'''''''                If Not (PostPayerOn) Then
'''''''                    GoSub SetupPayor
'''''''                End If
'''''''                GoSub SetupServices
'''''''                GoSub InitClaimSrv
'''''''                GoSub ParseRecord
'''''''                ClmPatId = NSFArray(2)
'''''''                ClmSta = NSFArray(3)
'''''''                If (ClmSta = "1") Or (ClmSta = "2") Or (ClmSta = "3") Then
'''''''                    ClmSta = "0" + ClmSta
'''''''                    ClmTmp = "Paid   "
'''''''                ElseIf (ClmSta = "4") Then
'''''''                    ClmSta = "0" + ClmSta
'''''''                    ClmTmp = "Denied "
'''''''                ElseIf (ClmSta = "5") Then
'''''''                    ClmSta = "0" + ClmSta
'''''''                    ClmTmp = "Pend   "
'''''''                ElseIf (ClmSta = "13") Or (ClmSta = "15") Or (ClmSta = "16") Or (ClmSta = "17") Then
'''''''                    ClmTmp = "Suspend"
'''''''                ElseIf (ClmSta = "19") Or (ClmSta = "20") Or (ClmSta = "21") Then
'''''''                    ClmTmp = "Prc/Fwd"
'''''''                ElseIf (ClmSta = "22") Then
'''''''                    ClmTmp = "Reverse"
'''''''                ElseIf (ClmSta = "23") Then
'''''''                    ClmTmp = "Refused"
'''''''                ElseIf (ClmSta = "25") Or (ClmSta = "27") Then
'''''''                    ClmTmp = "Review "
'''''''                End If
'''''''                ClmCharge = NSFArray(4)
'''''''                ClmPayAmt = NSFArray(5)
'''''''                ClmPatRsp = NSFArray(6)
'''''''                ClmAplId = NSFArray(8)
'''''''                GoSub SetupClaim
'''''''            ElseIf (Left(Rec, 3) = "CAS") Then
'''''''                GoSub ParseRecord
'''''''                If Not (SrvOn) Or (SrvCnt < 1) Then
'''''''                    ClmAdjCode = NSFArray(2)
'''''''                    ClmAdjRsn = NSFArray(3)
'''''''                    ClmAdjAmt = NSFArray(4)
'''''''                Else
'''''''                    If (NSFArray(2) = "CO") Or (NSFArray(2) = "OA") Or (NSFArray(2) = "PI") Or (NSFArray(2) = "CR") Then
'''''''                        If (Trim(SrvAdj(SrvCnt)) <> "") Or (Trim(SrvAdjAmt(SrvCnt)) <> "") Or (Trim(SrvAdjAmt(SrvCnt)) <> "") Then
'''''''                            SrvCnt = SrvCnt + 1
'''''''                            SrvCode(SrvCnt) = SrvCode(SrvCnt - 1)
'''''''                            SrvModifier(SrvCnt) = SrvModifier(SrvCnt - 1)
'''''''                            Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                            SrvDate(SrvCnt) = SrvDate(SrvCnt - 1)
'''''''                        End If
'''''''                        SrvAdj(SrvCnt) = NSFArray(2)
'''''''                        SrvRsn(SrvCnt) = NSFArray(3)
'''''''                        SrvAdjAmt(SrvCnt) = NSFArray(4)
'''''''                        If (Trim(NSFArray(6)) <> "") And (Trim(NSFArray(7)) <> "") Then
'''''''                            SrvCnt = SrvCnt + 1
'''''''                            SrvCode(SrvCnt) = SrvCode(SrvCnt - 1)
'''''''                            SrvModifier(SrvCnt) = SrvModifier(SrvCnt - 1)
'''''''                            Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                            SrvDate(SrvCnt) = SrvDate(SrvCnt - 1)
'''''''                            SrvAdj(SrvCnt) = NSFArray(2)
'''''''                            SrvRsn(SrvCnt) = NSFArray(6)
'''''''                            SrvAdjAmt(SrvCnt) = NSFArray(7)
'''''''                            SrvPatAmt(SrvCnt) = ""
'''''''                            SrvPatRsn(SrvCnt) = ""
'''''''                            If (Trim(NSFArray(9)) <> "") And (Trim(NSFArray(10)) <> "") Then
'''''''                                SrvCnt = SrvCnt + 1
'''''''                                SrvCode(SrvCnt) = SrvCode(SrvCnt - 1)
'''''''                                SrvModifier(SrvCnt) = SrvModifier(SrvCnt - 1)
'''''''                                Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                                SrvDate(SrvCnt) = SrvDate(SrvCnt - 1)
'''''''                                SrvAdj(SrvCnt) = NSFArray(2)
'''''''                                SrvRsn(SrvCnt) = NSFArray(9)
'''''''                                SrvAdjAmt(SrvCnt) = NSFArray(10)
'''''''                                SrvPatAmt(SrvCnt) = ""
'''''''                                SrvPatRsn(SrvCnt) = ""
'''''''                            End If
'''''''                        End If
'''''''                    ElseIf (NSFArray(2) = "PR") Then
'''''''' for more than 1 PR
'''''''                        If (Trim(SrvAdj(SrvCnt)) <> "") Or (Trim(SrvAdjAmt(SrvCnt)) <> "") Then
'''''''                            SrvCnt = SrvCnt + 1
'''''''                            SrvCode(SrvCnt) = SrvCode(SrvCnt - 1)
'''''''                            SrvModifier(SrvCnt) = SrvModifier(SrvCnt - 1)
'''''''                            Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                            SrvDate(SrvCnt) = SrvDate(SrvCnt - 1)
'''''''                            SrvAdj(SrvCnt) = NSFArray(2)
'''''''                        End If
'''''''                        SrvPatRsn(SrvCnt) = NSFArray(3)
'''''''                        SrvPatAmt(SrvCnt) = NSFArray(4)
'''''''                        If (Trim(NSFArray(6)) <> "") And (Trim(NSFArray(7)) <> "") Then
'''''''                            SrvCnt = SrvCnt + 1
'''''''                            SrvCode(SrvCnt) = SrvCode(SrvCnt - 1)
'''''''                            SrvModifier(SrvCnt) = SrvModifier(SrvCnt - 1)
'''''''                            Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                            SrvDate(SrvCnt) = SrvDate(SrvCnt - 1)
'''''''                            SrvAdj(SrvCnt) = "PR"
'''''''                            SrvPatAmt(SrvCnt) = NSFArray(7)
'''''''                            SrvPatRsn(SrvCnt) = NSFArray(6)
'''''''                            SrvRsn(SrvCnt) = ""
'''''''                            SrvAdjAmt(SrvCnt) = ""
'''''''                            If (Trim(NSFArray(9)) <> "") And (Trim(NSFArray(10)) <> "") Then
'''''''                                SrvCnt = SrvCnt + 1
'''''''                                SrvCode(SrvCnt) = SrvCode(SrvCnt - 1)
'''''''                                SrvModifier(SrvCnt) = SrvModifier(SrvCnt - 1)
'''''''                                Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                                SrvDate(SrvCnt) = SrvDate(SrvCnt - 1)
'''''''                                SrvAdj(SrvCnt) = "PR"
'''''''                                SrvPatRsn(SrvCnt) = NSFArray(9)
'''''''                                SrvPatAmt(SrvCnt) = NSFArray(10)
'''''''                                SrvRsn(SrvCnt) = ""
'''''''                                SrvAdjAmt(SrvCnt) = ""
'''''''                            End If
'''''''                        End If
'''''''                    End If
'''''''                End If
'''''''            ElseIf (Left(Rec, 3) = "MIA") Then
'''''''                GoSub ParseRecord
'''''''            ElseIf (Left(Rec, 3) = "MOA") Then
'''''''                GoSub ParseRecord
'''''''            ElseIf (Left(Rec, 3) = "AMT") Then
'''''''                GoSub ParseRecord
'''''''                If Not (SrvOn) Then
'''''''                    ClmAmt = NSFArray(3)
'''''''                Else
'''''''                    If (NSFArray(2) = "B6") Then
'''''''                        SrvAllCnt = SrvAllCnt + 1
'''''''                        SrvAllow(SrvAllCnt) = NSFArray(3)
'''''''                    End If
'''''''                End If
'''''''            ElseIf (Left(Rec, 3) = "QTY") Then
'''''''                GoSub ParseRecord
'''''''                If Not (SrvOn) Then
'''''''                    ClmQty = NSFArray(3)
'''''''                End If
'''''''            ElseIf (Left(Rec, 2) = "LQ") Then
'''''''                GoSub ParseRecord
'''''''                PayerRsn = NSFArray(3)
'''''''            ElseIf (Left(Rec, 3) = "SVC") Then
'''''''                GoSub ParseRecord
'''''''                SrvOn = True
'''''''                SrvCnt = SrvCnt + 1
'''''''                If (Trim(NSFArray(7)) = "") Then
'''''''                    SrvCode(SrvCnt) = Mid(NSFArray(2), 4, 5)
'''''''                    SrvModifier(SrvCnt) = Mid(NSFArray(2), 9, 12)
'''''''                    Call StripCharacters(SrvModifier(SrvCnt), "<")
'''''''                    Call StripCharacters(SrvModifier(SrvCnt), ":")
'''''''                    Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                Else    ' where bundling was done or payer is correcting the modifiers, assumes the second one was our original
'''''''                    SrvCode(SrvCnt) = Mid(NSFArray(7), 4, 5)
'''''''                    SrvModifier(SrvCnt) = Mid(NSFArray(7), 9, 12)
'''''''                    Call StripCharacters(SrvModifier(SrvCnt), "<")
'''''''                    Call StripCharacters(SrvModifier(SrvCnt), ":")
'''''''                    Call StripCharacters(SrvModifier(SrvCnt), ">")
'''''''                End If
'''''''                SrvChg(SrvCnt) = NSFArray(3)
'''''''                SrvPay(SrvCnt) = NSFArray(4)
'''''''                SrvQty(SrvCnt) = NSFArray(6)
'''''''            ElseIf (Left(Rec, 3) = "PLB") Then
'''''''                GoSub ParseRecord
'''''''                DisplayText = Space(75)
'''''''                Mid(DisplayText, 1, Len(NSFArray(2))) = NSFArray(2)
'''''''                Mid(DisplayText, 31, 2) = Left(NSFArray(4), 2)
'''''''                Mid(DisplayText, 34, Len(NSFArray(4)) - 3) = Mid(NSFArray(4), Len(NSFArray(3)) - 3)
'''''''                Mid(DisplayText, 55, Len(NSFArray(5))) = NSFArray(5)
'''''''                lstPLB.AddItem DisplayText
'''''''                If (Trim(NSFArray(6)) <> "") Then
'''''''                    DisplayText = Space(75)
'''''''                    Mid(DisplayText, 1, Len(NSFArray(2))) = NSFArray(2)
'''''''                    Mid(DisplayText, 31, 2) = Left(NSFArray(6), 2)
'''''''                    Mid(DisplayText, 34, Len(NSFArray(6)) - 3) = Mid(NSFArray(6), Len(NSFArray(6)) - 3)
'''''''                    Mid(DisplayText, 55, Len(NSFArray(7))) = NSFArray(7)
'''''''                    lstPLB.AddItem DisplayText
'''''''                End If
'''''''                If (Trim(NSFArray(8)) <> "") Then
'''''''                    DisplayText = Space(75)
'''''''                    Mid(DisplayText, 1, Len(NSFArray(2))) = NSFArray(2)
'''''''                    Mid(DisplayText, 31, 2) = Left(NSFArray(8), 2)
'''''''                    Mid(DisplayText, 34, Len(NSFArray(8)) - 3) = Mid(NSFArray(8), Len(NSFArray(8)) - 3)
'''''''                    Mid(DisplayText, 55, Len(NSFArray(9))) = NSFArray(9)
'''''''                    lstPLB.AddItem DisplayText
'''''''                End If
'''''''            ElseIf (Left(Rec, 2) = "SE") Then
'''''''                GoSub SetupServices
'''''''                GoSub InitClaimSrv
'''''''                DisplayText = Space(75)
'''''''                Mid(DisplayText, 1, 9) = "Batch End"
'''''''                DisplayText = DisplayText + "Z"
'''''''                lstAcks.AddItem DisplayText
'''''''            End If
'''''''        End If
'''''''    Wend
'''''''    If (Trim(Rec) <> "") Then
'''''''        lstAcks.AddItem Rec
'''''''    End If
'''''''    Close #FileNum
'''''''    If (lstAcks.ListCount > 0) Then
'''''''        lstAcks.Visible = True
'''''''        lstAppts.Visible = False
'''''''        AckDate = "20" + Left(lstAcks.List(1), 6)
'''''''        LoadRemitFileHipaa = True
'''''''    End If
'''''''End If
'''''''ByeBye:
'''''''Exit Function
'''''''SetupPayor:
'''''''    DisplayText = Space(80)
'''''''    Mid(DisplayText, 1, 3) = "PBY"
'''''''    Mid(DisplayText, 5, 8) = BatchNumber
'''''''    Mid(DisplayText, 14, 8) = PayerDate
'''''''    Mid(DisplayText, 23, 1) = PayerMeth
'''''''    Mid(DisplayText, 25, 24) = PayerRef
'''''''    Mid(DisplayText, 50, 16) = PlanName
'''''''    Mid(DisplayText, 66, 10) = CheckAmount
'''''''    DisplayText = DisplayText + "B"
'''''''    lstAcks.AddItem DisplayText
'''''''    PostPayerOn = True
'''''''    Return
'''''''SetupClaim:
'''''''    DisplayText = Space(80)
'''''''    Mid(DisplayText, 1, 3) = "CLM"
'''''''    Mid(DisplayText, 5, 8) = ClmDate
'''''''    Mid(DisplayText, 14, 12) = ClmPatId
'''''''    Mid(DisplayText, 27, 10) = ClmSta + "-" + ClmTmp
'''''''    Call DisplayDollarAmount(ClmCharge, Temp)
'''''''    Mid(DisplayText, 38, 7) = Trim(Temp)
'''''''    Call DisplayDollarAmount(ClmPayAmt, Temp)
'''''''    Mid(DisplayText, 46, 7) = Trim(Temp)
'''''''    Call DisplayDollarAmount(ClmPatRsp, Temp)
'''''''    Mid(DisplayText, 54, 7) = Trim(Temp)
'''''''    Mid(DisplayText, 62, 13) = ClmAplId
'''''''    DisplayText = DisplayText + "C"
'''''''    lstAcks.AddItem DisplayText
'''''''    Return
'''''''SetupPat:
'''''''    DisplayText = Space(80)
'''''''    Mid(DisplayText, 1, 3) = "PAT"
'''''''    Mid(DisplayText, 5, 16) = PatLName
'''''''    Mid(DisplayText, 22, 14) = PatFName
'''''''    Mid(DisplayText, 38, 2) = PatIdRef
'''''''    Mid(DisplayText, 41, 32) = PatIdNumber
'''''''    DisplayText = DisplayText + "P"
'''''''    lstAcks.AddItem DisplayText
'''''''    Return
'''''''SetUpInsured:
'''''''    DisplayText = Space(80)
'''''''    Mid(DisplayText, 1, 3) = "INS"
'''''''    Mid(DisplayText, 5, 16) = InsdLName
'''''''    Mid(DisplayText, 22, 14) = InsdFName
'''''''    Mid(DisplayText, 38, 2) = InsdIdRef
'''''''    Mid(DisplayText, 41, 32) = InsdIdNumber
'''''''    DisplayText = DisplayText + "I"
'''''''    lstAcks.AddItem DisplayText
'''''''    Return
'''''''SetupServices:
'''''''    q = 0
'''''''    For z = 1 To SrvCnt
'''''''        DisplayText = Space(80)
'''''''        Mid(DisplayText, 1, 3) = "SVC"
'''''''        Mid(DisplayText, 5, 8) = SrvDate(z)
'''''''        Mid(DisplayText, 15, 5) = SrvCode(z)
'''''''        Mid(DisplayText, 21, 8) = SrvModifier(z)
'''''''        Call DisplayDollarAmount(SrvChg(z), Temp)
'''''''        Mid(DisplayText, 30, 7) = Trim(Temp)
'''''''        Call DisplayDollarAmount(SrvPay(z), Temp)
'''''''        Mid(DisplayText, 38, 7) = Trim(Temp)
'''''''        Mid(DisplayText, 46, 4) = Trim(SrvQty(z))
'''''''        Mid(DisplayText, 51, 2) = SrvAdj(z)
'''''''        Mid(DisplayText, 54, 5) = SrvRsn(z)
'''''''        Call DisplayDollarAmount(SrvAdjAmt(z), Temp)
'''''''        Mid(DisplayText, 60, 7) = Trim(Temp)
'''''''        Call DisplayDollarAmount(SrvPatAmt(z), Temp)
'''''''        Mid(DisplayText, 68, 7) = Trim(Temp)
'''''''        Mid(DisplayText, 76, 5) = SrvPatRsn(z)
'''''''        DisplayText = DisplayText + "S"
'''''''        If (SrvCode(z) <> SrvCode(z - 1)) Then
'''''''            q = q + 1
'''''''            DisplayText = DisplayText + " " + SrvAllow(q)
'''''''        End If
'''''''        lstAcks.AddItem DisplayText
'''''''    Next z
'''''''    If (SrvCnt > 0) Then
'''''''        DisplayText = Space(80)
'''''''        Mid(DisplayText, 1, 9) = "Claim End"
'''''''        DisplayText = DisplayText + "X"
'''''''        lstAcks.AddItem DisplayText
'''''''    End If
'''''''    Return
'''''''InitAll:
'''''''    TId = ""
'''''''    BatchNumber = ""
'''''''    PayerRef = ""
'''''''    PayerMeth = ""
'''''''    PayerDate = ""
'''''''    PayerRsn = ""
'''''''    PatLName = ""
'''''''    PatFName = ""
'''''''    PatIdRef = ""
'''''''    PatIdNumber = ""
'''''''    InsdLName = ""
'''''''    InsdFName = ""
'''''''    InsdIdRef = ""
'''''''    InsdIdNumber = ""
'''''''    PlanName = ""
'''''''    PlanAddress = ""
'''''''    PlanCity = ""
'''''''    PlanState = ""
'''''''    PlanZip = ""
'''''''    PlanId = ""
'''''''    ClmDate = ""
'''''''    ClmPatId = ""
'''''''    ClmSta = ""
'''''''    ClmTmp = ""
'''''''    ClmCharge = ""
'''''''    ClmPayAmt = ""
'''''''    ClmPatRsp = ""
'''''''    ClmAplId = ""
'''''''    ClmAmtRef = ""
'''''''    ClmAmt = ""
'''''''    ClmQtyRef = ""
'''''''    ClmQty = ""
'''''''    ClmAdjCode = ""
'''''''    ClmAdjRsn = ""
'''''''    ClmAdjAmt = ""
'''''''    SrvOn = False
'''''''    SrvCnt = 0
'''''''    SrvAllCnt = 0
'''''''    Erase SrvDate
'''''''    Erase SrvCode
'''''''    Erase SrvModifier
'''''''    Erase SrvChg
'''''''    Erase SrvPay
'''''''    Erase SrvQty
'''''''    Erase SrvAdj
'''''''    Erase SrvRsn
'''''''    Erase SrvAdjAmt
'''''''    Erase SrvPatAmt
'''''''    Erase SrvPatRsn
'''''''    Return
'''''''InitClaimSrv:
'''''''    ClmPatId = ""
'''''''    ClmSta = ""
'''''''    ClmTmp = ""
'''''''    ClmCharge = ""
'''''''    ClmPayAmt = ""
'''''''    ClmPatRsp = ""
'''''''    ClmAplId = ""
'''''''    ClmAmtRef = ""
'''''''    ClmAmt = ""
'''''''    ClmQtyRef = ""
'''''''    ClmQty = ""
'''''''    ClmAdjCode = ""
'''''''    ClmAdjRsn = ""
'''''''    ClmAdjAmt = ""
'''''''    SrvOn = False
'''''''    SrvCnt = 0
'''''''    SrvAllCnt = 0
'''''''    Erase SrvDate
'''''''    Erase SrvCode
'''''''    Erase SrvModifier
'''''''    Erase SrvChg
'''''''    Erase SrvPay
'''''''    Erase SrvQty
'''''''    Erase SrvAdj
'''''''    Erase SrvRsn
'''''''    Erase SrvAdjAmt
'''''''    Erase SrvPatAmt
'''''''    Erase SrvPatRsn
'''''''    Return
'''''''ParseRecord:
'''''''    Erase NSFArray
'''''''    p = 0
'''''''    j = 1
'''''''    i = InStrPS(Rec, "*")
'''''''    While (i > 0)
'''''''        p = p + 1
'''''''        NSFArray(p) = Mid(Rec, j, (i - 1) - (j - 1))
'''''''        j = i + 1
'''''''        i = InStrPS(j, Rec, "*")
'''''''    Wend
'''''''    p = p + 1
'''''''    NSFArray(p) = Mid(Rec, j, Len(Rec) - (j - 1))
'''''''    Return
'''''''GetNextRecord:
'''''''    Tp = ""
'''''''    Rec = ""
'''''''    Do Until (Tp = "~") Or EOF(FileNum)
'''''''        CurCnt = CurCnt + 1
'''''''        Tp = Input(1, #FileNum)
'''''''        If (Tp <> "~") Then
'''''''            Rec = Rec + Tp
'''''''        End If
'''''''        If (CurCnt > MaxCnt) Then
'''''''            Exit Do
'''''''        End If
'''''''    Loop
'''''''    Call StripCharacters(Rec, Chr(13))
'''''''    Call StripCharacters(Rec, Chr(10))
'''''''    If (Left(Rec, 10) = Space(10)) Then
'''''''        Rec = Trim(Rec)
'''''''        If (Asc(Left(Rec, 1)) > 129) Or (Asc(Left(Rec, 1)) < 32) Then
'''''''            Rec = Mid(Rec, 2, Len(Rec) - 1)
'''''''        End If
'''''''    End If
'''''''    Return
'''''''LeaveFast:
'''''''    Close #FileNum
'''''''    If (lstAcks.ListCount > 0) Then
'''''''        lstAcks.Visible = True
'''''''        lstAppts.Visible = False
'''''''        LoadRemitFileHipaa = True
'''''''    End If
'''''''    Resume ByeBye
'''''''End Function

Private Function ProcessRemit() As Boolean
Dim SrvItem As Long
Dim PatId As Long, InsId As Long
Dim i As Long, RcvId As Long
Dim p As Integer, z As Integer
Dim ADate As String, Temp As String
Dim AFile As String, BFile As String
Dim ClmSta As String, InvId As String
Dim ThePat As String, TheGroup As String
Dim TheNeic As String, TheBatch As String
Dim TheCheck As String, TheCheckDate As String
Dim Mod1 As String, Mod2 As String, Mod3 As String, Mod4 As String
Dim Qnty As String, PAmt As String, AAmt As String, PRsn As String
Dim Srv As String, SLine As String, SDate As String, Chrg As String
Dim ClaimRef(20) As String
Dim ProcComp As Boolean
Dim RetFin As PatientFinance
Dim RetRcv As PatientReceivables
Dim RetPay As PatientReceivablePayment
Dim RetSrv As PatientReceivableService
ProcessRemit = True
ProcComp = False
If (lstAcks.ListCount > 2) Then
    For i = 1 To lstAcks.ListCount - 1
        If (Left(lstAcks.List(i), 9) = "Claim End") Then
            ProcComp = True
            GoSub ProcClaim
            p = 0
            Erase ClaimRef
        Else
            p = p + 1
            ClaimRef(p) = lstAcks.List(i)
        End If
    Next i
End If
If (ProcComp) Then
    frmEventMsgs.Header = "Remittances Posted"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
' rename original
    Temp = ""
    Call FormatTodaysDate(Temp, False)
    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
    AFile = RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex))
    i = InStrPS(AFile, ".")
    If (i > 0) Then
        BFile = Left(AFile, i - 1) + "-" + Temp + Mid(AFile, i, Len(AFile) - (i - 1))
    Else
        BFile = AFile + "-" + Temp + ".txt"
    End If
    If (FM.IsFileThere(BFile)) Then
        FM.Kill BFile
        For i = 0 To 1000
            DoEvents        ' spin
        Next i
    End If
    FM.Name AFile, BFile
    lstAppts.ListIndex = -1
    lstAppts.Visible = True
    lstAcks.Visible = False
    Call LoadRemittance
End If
Exit Function
ProcClaim:
    TheNeic = Trim(Mid(lstAcks.List(1), 3, 15))
    TheBatch = Trim(Mid(lstAcks.List(1), 20, 4))
    TheCheck = Trim(Mid(lstAcks.List(1), 41, 15))
    TheCheckDate = Trim(Mid(lstAcks.List(1), 56, 8))
    ThePat = Trim(Mid(ClaimRef(1), 1, 25))
    TheGroup = Trim(Mid(ClaimRef(1), 27, 20))
    ClmSta = Trim(Mid(ClaimRef(1), 65, 2))
    ADate = Trim(Mid(ClaimRef(2), 23, 8))
    Set RetFin = New PatientFinance
    RetFin.PrimaryPerson = ThePat
    If (RetFin.RetrievePatientbyPolicy(1) > 0) Then
        PatId = RetFin.PatientId
        InsId = RetFin.PrimaryInsurerId
    End If
    Set RetFin = Nothing
    If (PatId > 0) And (InsId > 0) Then
        GoSub GetRcv
        If (RcvId > 0) And (Trim(InvId) <> "") Then
            For z = 2 To p - 1
                Srv = Trim(Mid(ClaimRef(z), 32, 5))
                SLine = Trim(Mid(ClaimRef(z), 19, 3))
                SDate = Trim(Mid(ClaimRef(z), 23, 8))
                Mod1 = Trim(Mid(ClaimRef(z), 37, 2))
                Mod2 = Trim(Mid(ClaimRef(z), 39, 2))
                Mod3 = Trim(Mid(ClaimRef(z), 41, 2))
                Mod4 = Trim(Mid(ClaimRef(z), 43, 2))
                Qnty = Trim(Mid(ClaimRef(z), 46, 4))
                PAmt = Trim(Mid(ClaimRef(z), 59, 7))
                PAmt = Left(PAmt, Len(PAmt) - 2) + "." + Mid(PAmt, Len(PAmt) - 1, 2)
                AAmt = Trim(Mid(ClaimRef(z), 67, 7))
                AAmt = Left(AAmt, Len(AAmt) - 2) + "." + Mid(AAmt, Len(AAmt) - 1, 2)
                Chrg = Trim(Mid(ClaimRef(z), 51, 7))
                PRsn = Trim(Mid(ClaimRef(z), 11, 6))
                If (PAmt > 0) Or (AAmt > 0) Then
                    GoSub GetSrvId
                End If
                If (SrvItem > 0) Then
                    If (PAmt > 0) Then
                        Set RetPay = New PatientReceivablePayment
                        RetPay.PaymentId = 0
                        If (RetPay.RetrievePatientReceivablePayments) Then
                            RetPay.PaymentAmount = Round(val(Trim(PAmt)), 2)
                            RetPay.PaymentType = "P"
                            RetPay.ReceivableId = RcvId
                            RetPay.PaymentFinancialType = "C"
                            RetPay.PaymentCheck = TheCheck
                            RetPay.PaymentCommentOn = False
                            RetPay.PaymentDate = TheCheckDate
                            RetPay.PaymentAssignBy = 0
                            RetPay.PaymentPayerType = "I"
                            RetPay.PaymentRefType = "K"
                            RetPay.PaymentRefId = TheBatch
                            RetPay.PaymentService = Srv
                            RetPay.PaymentServiceItem = SrvItem
                            RetPay.PaymentPayerId = InsId
                            RetPay.PaymentReason = PRsn
                        End If
                        Set RetPay = Nothing
                    End If
                    If (AAmt > 0) Then
                        Set RetPay = New PatientReceivablePayment
                        RetPay.PaymentId = 0
                        If (RetPay.RetrievePatientReceivablePayments) Then
                            RetPay.PaymentAmount = Round(val(Trim(AAmt)), 2)
                            RetPay.PaymentType = "+"
                            RetPay.ReceivableId = RcvId
                            RetPay.PaymentFinancialType = "C"
                            RetPay.PaymentCheck = TheCheck
                            RetPay.PaymentCommentOn = False
                            RetPay.PaymentDate = TheCheckDate
                            RetPay.PaymentAssignBy = 0
                            RetPay.PaymentPayerType = "I"
                            RetPay.PaymentRefType = "K"
                            RetPay.PaymentRefId = TheBatch
                            RetPay.PaymentService = Srv
                            RetPay.PaymentServiceItem = SrvItem
                            RetPay.PaymentPayerId = InsId
                            RetPay.PaymentReason = PRsn
                        End If
                        Set RetPay = Nothing
                    End If
                    If (ClmSta <> "50") And (ClmSta <> "60") And (ClmSta <> "70") Then
                        Set RetPay = New PatientReceivablePayment
                        RetPay.PaymentId = 0
                        If (RetPay.RetrievePatientReceivablePayments) Then
                            RetPay.PaymentAmount = 0
                            RetPay.PaymentType = "D"
                            RetPay.ReceivableId = RcvId
                            RetPay.PaymentFinancialType = "C"
                            RetPay.PaymentCheck = TheCheck
                            RetPay.PaymentCommentOn = False
                            RetPay.PaymentDate = TheCheckDate
                            RetPay.PaymentAssignBy = 0
                            RetPay.PaymentPayerType = "I"
                            RetPay.PaymentRefType = "K"
                            RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                            RetPay.PaymentService = Srv
                            RetPay.PaymentServiceItem = SrvItem
                            RetPay.PaymentPayerId = InsId
                            RetPay.PaymentReason = PRsn
                        End If
                        Set RetPay = Nothing
                    End If
                End If
                lblDisp.Caption = "Processing Srv " + Srv
                lblDisp.Visible = True
                DoEvents
            Next z
        End If
    End If
    lblDisp.Visible = False
    Return
GetRcv:
    RcvId = 0
    InvId = ""
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableInvoiceDate = ADate
    RetRcv.PatientId = PatId
    RetRcv.InsurerId = InsId
    If (RetRcv.FindPatientReceivable > 0) Then
        If (RetRcv.SelectPatientReceivable(1)) Then
            RcvId = RetRcv.ReceivableId
            InvId = RetRcv.ReceivableInvoice
        End If
    End If
    Set RetRcv = Nothing
    Return
GetSrvId:
    SrvItem = 0
    Set RetSrv = New PatientReceivableService
    RetSrv.Service = Srv
    RetSrv.Invoice = InvId
    RetSrv.ServiceDate = ADate
    If (RetSrv.FindPatientReceivableService > 0) Then
        If (RetSrv.SelectPatientReceivableService(1)) Then
            SrvItem = RetSrv.ItemId
        End If
    End If
    Set RetSrv = Nothing
    Return
End Function

Private Function ProcessRemitHipaa() As Boolean
Dim AutoOn As Boolean
Dim MedOn1 As Boolean, MedOn2 As Boolean
Dim ProcComp As Boolean, GoodRcv As Boolean
Dim MedicareOn As Boolean, SecondaryOn As Boolean
Dim DenyOn As Boolean, Replay As Boolean
Dim VfyPst As Boolean, VfyCon As Boolean
Dim zy As Integer
Dim h1 As Integer, k As Integer
Dim q1 As Integer, MyEye As Integer
Dim u As Integer, w As Integer
Dim p As Integer, z As Integer
Dim PInd As Integer, MaxChecks As Integer
Dim z1 As Integer, z2 As Integer, zz As Integer
Dim IRef As Long, IRef2 As Integer, ITot As Long
Dim i As Long, RefId As Long
Dim p1 As Long, NewInsd As Long
Dim TempIns As Long, RecId As Long, ApptId As Long
Dim RcvId As Long, PatId As Long, InsId As Long
Dim SrvItem As Long, FinId As Long, TrnId As Long
Dim AllowAmt As Single
Dim PBal As Single, IBal As Single
Dim TheBal As Single, PrAmt As Single
Dim LclInvId As String
Dim InsType As String, MyReason As String
Dim ATemp As String, TType As String
Dim ADate As String, Temp As String
Dim AFile As String, BFile As String
Dim VType As String, InvIdA As String
Dim ClmSta As String, InvId As String
Dim EOBDate As String, InvDt As String
Dim PatIdRef As String, ThePat As String
Dim TheNeic As String, TheBatch As String
Dim TheCheck As String, TheCheckDate As String
Dim UTemp As String, TheAppealNumber As String
Dim ARec As String, a1 As String, a2 As String
Dim CChk As String, CAmt As String, CPty As String
Dim InvDate As String, Temp1 As String, ZType As String
Dim Srv As String, SDate As String, Chrg As String, ATyp As String
Dim Mod1 As String, Mod2 As String, Mod3 As String, Mod4 As String
Dim Qnty As String, PAmt As String, AAmt As String, PRsn As String
Dim XRsn As String, XAmt As String
Dim TotalAdjs As Long, TotalRevs As Long
Dim TotalPays As Long, TotalDenys As Long
Dim TotalRecs As Long, TotalReject As Long
Dim TotalSrvReject As Long, TotalPayOther As Long
Dim TotalSrvDenys As Long, TotalNotPosted As Long
Dim TotalSrvRejectAmt As Single, TotalSrvPatResp As Long
Dim TotalPrAmount As Single, TotalRevAmount As Single
Dim TotalPayAmount As Single, TotalAdjAmount As Single
Dim TotalProviderAdjs As Single, TotalNotPostedAmt As Single
Dim CheckParty(40) As String
Dim CheckNumber(40) As String
Dim CheckAmount(40) As String
Dim ClaimRef(40) As String
Dim RetIns As Insurer
Dim RetFin As PatientFinance
Dim RetRcv As PatientReceivables
Dim RetRcvA As PatientReceivables
Dim RetPay As PatientReceivablePayment
Dim RetSrv As PatientReceivableService
Dim RetCode As PracticeCodes
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
ProcessRemitHipaa = True
Temp = Trim(Mid(lblWho.Caption, 3, Len(lblWho.Caption) - 2))
Call StripCharacters(Temp, " ")
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "TRANSMITTYPE"
RetCode.ReferenceAlternateCode = Trim(Temp)
If (RetCode.FindCode > 0) Then
    If (RetCode.SelectCode(1)) Then
        TType = Left(RetCode.ReferenceCode, 1)
    End If
End If
Set RetCode = Nothing
RejectFile = ""
Temp = ""
Call FormatTodaysDate(Temp, False)
Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
AFile = RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex))
i = InStrPS(AFile, ".")
If (i > 0) Then
    BFile = Left(AFile, i - 1) + "-" + Temp + "-Reject" + Mid(AFile, i, Len(AFile) - (i - 1))
    RejectFile = BFile
    BFile = Left(AFile, i - 1) + "-" + Temp + "-Adjust" + Mid(AFile, i, Len(AFile) - (i - 1))
    SecondaryAdjustmentsFile = BFile
    BFile = Left(AFile, i - 1) + "-" + Temp + "-Result" + Mid(AFile, i, Len(AFile) - (i - 1))
    ResultsFile = BFile
Else
    BFile = AFile + "-" + Temp + "-Reject.txt"
    RejectFile = BFile
    BFile = AFile + "-" + Temp + "-Adjust.txt"
    SecondaryAdjustmentsFile = BFile
    BFile = AFile + "-" + Temp + "-Result.txt"
    ResultsFile = BFile
End If
ProcComp = False
If (lstAcks.ListCount > 2) Then
    RefId = 1
    MaxChecks = 0
    Erase CheckAmount
    Erase CheckParty
    TotalRecs = 0
    TotalReject = 0
    TotalSrvReject = 0
    TotalPays = 0
    TotalAdjs = 0
    TotalRevs = 0
    TotalDenys = 0
    TotalSrvDenys = 0
    TotalPayOther = 0
    TotalPrAmount = 0
    TotalPayAmount = 0
    TotalAdjAmount = 0
    TotalRevAmount = 0
    TotalSrvRejectAmt = 0
    TotalSrvPatResp = 0
    TotalNotPosted = 0
    TotalProviderAdjs = 0
    TotalNotPostedAmt = 0
    MaxChecks = MaxChecks + 1
    CheckAmount(MaxChecks) = Trim(Mid(lstAcks.List(1), 66, 10))
    CheckParty(MaxChecks) = Trim(Mid(lstAcks.List(1), 50, 15))
    CheckNumber(MaxChecks) = Trim(Mid(lstAcks.List(1), 25, 24))
    MyEye = 2
    For i = 0 To lstAcks.ListCount - 1
        If (Left(lstAcks.List(i), 3) = "PBY") Then
            MyEye = i
            Exit For
        End If
    Next i
    For i = MyEye To lstAcks.ListCount - 1
        If (Left(lstAcks.List(i), 9) = "Claim End") Then
            ProcComp = True
            GoSub ProcClaim
            GoSub BatchBalance
            p = 0
            Erase ClaimRef
        ElseIf (Left(lstAcks.List(i), 9) = "Batch End") Then
        
        ElseIf (Left(lstAcks.List(i), 3) = "PBY") Then
            RefId = i
            Replay = False
            CChk = Trim(Mid(lstAcks.List(RefId), 25, 24))
            CPty = Trim(Mid(lstAcks.List(RefId), 50, 15))
            CAmt = Trim(Mid(lstAcks.List(RefId), 66, 10))
            For z2 = 1 To MaxChecks
                If (CChk = CheckNumber(z2)) And (CPty = CheckParty(z2)) And (CAmt = CheckAmount(z2)) Then
                    Replay = True
                    Exit For
                End If
            Next z2
            If Not (Replay) Then
                MaxChecks = MaxChecks + 1
                CheckAmount(MaxChecks) = Trim(Mid(lstAcks.List(RefId), 66, 10))
                CheckParty(MaxChecks) = Trim(Mid(lstAcks.List(RefId), 50, 15))
                CheckNumber(MaxChecks) = Trim(Mid(lstAcks.List(RefId), 25, 24))
            End If
        Else
            p = p + 1
            ClaimRef(p) = lstAcks.List(i)
        End If
    Next i
End If
If (ProcComp) Then
    frmEventMsgs.Header = "Remittances Posted"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Results"
    frmEventMsgs.CancelText = "Done"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
' rename original
    Temp = ""
    Call FormatTodaysDate(Temp, False)
    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
    AFile = RemitDirectory + Trim(lstAppts.List(lstAppts.ListIndex))
    i = InStrPS(AFile, ".")
    If (i > 0) Then
        BFile = Left(AFile, i - 1) + "-" + Temp + Mid(AFile, i, Len(AFile) - (i - 1))
    Else
        BFile = AFile + "-" + Temp + ".txt"
    End If
    If (FM.IsFileThere(BFile)) Then
        FM.Kill BFile
        For i = 0 To 1000
            DoEvents        ' spin
        Next i
    End If
    FM.Name AFile, BFile
    If (frmEventMsgs.Result = 2) Then
        GoSub DisplayResults
    Else
        lstAppts.ListIndex = -1
        lstAppts.Visible = True
        lstAcks.Visible = False
        Call LoadRemittance
    End If
Else
    GoSub DisplayResults
End If
Exit Function
DisplayResults:
    lstReport.Clear
    lstReport.AddItem "Close Results View"
    lstReport.AddItem ""
    lstReport.AddItem "Print Results View"
    Temp = ""
    Call FormatTodaysDate(Temp, False)
    Temp1 = ""
    Call FormatTimeNow(Temp1)
    lstReport.AddItem "Remittances for " + Temp + " at " + Temp1
    lstReport.AddItem ""
    For z = 1 To MaxChecks
        lstReport.AddItem "Check Number = " + CheckNumber(z)
        lstReport.AddItem "Check Party  = " + CheckParty(z)
        lstReport.AddItem "Check Amount = " + CheckAmount(z)
    Next z
    lstReport.AddItem ""
    lstReport.AddItem "Total Transactions        = " + Trim(Str(TotalRecs))
    lstReport.AddItem "Total Reject Transactions = " + Trim(Str(TotalReject))
    lstReport.AddItem ""
    lstReport.AddItem "Total Denials Transactions = " + Trim(Str(TotalDenys))
    lstReport.AddItem "Total Service Denials      = " + Trim(Str(TotalSrvDenys))
    lstReport.AddItem "Total Payments to Others   = " + Trim(Str(TotalPayOther))
    lstReport.AddItem "Total Patient Responsible  = " + Trim(Str(TotalSrvPatResp))
    lstReport.AddItem ""
    lstReport.AddItem "Total Payments = " + Trim(Str(TotalPays))
    lstReport.AddItem "Total Amount   = " + Trim(Str(TotalPayAmount))
    lstReport.AddItem ""
    lstReport.AddItem "Total Adjustments = " + Trim(Str(TotalAdjs))
    lstReport.AddItem "Total Amount      = " + Trim(Str(TotalAdjAmount))
    lstReport.AddItem ""
    lstReport.AddItem "Total Reversals = " + Trim(Str(TotalRevs))
    lstReport.AddItem "Total Amount    = " + Trim(Str(TotalRevAmount))
    lstReport.AddItem ""
    lstReport.AddItem "Total Reject Service Transactions = " + Trim(Str(TotalSrvReject))
    lstReport.AddItem "Total Amount                      = " + Trim(Str(TotalSrvRejectAmt))
    lstReport.AddItem ""
    lstReport.AddItem "Total Duplicates = " + Trim(Str(TotalNotPosted))
    lstReport.AddItem "Total Duplicates Amt = " + Trim(Str(TotalNotPostedAmt))
    lstReport.AddItem ""
    lstReport.AddItem "Provider Adjustments"
    For z = 0 To lstPLB.ListCount - 1
        lstReport.AddItem lstPLB.List(z)
        TotalProviderAdjs = TotalProviderAdjs + Round(val(Trim(Mid(lstPLB.List(z), 55, 10))), 2)
    Next z
    lstReport.AddItem "Total PLB = " + Trim(Str(TotalProviderAdjs))
    lstReport.AddItem ""
    If (Trim(RejectFile) <> "") Then
        If (FM.IsFileThere(RejectFile)) Then
            lstReport.AddItem ""
            lstReport.AddItem "-------------------"
            lstReport.AddItem "Rejected Items List"
            lstReport.AddItem "-------------------"
            FM.OpenFile RejectFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(102)
            While Not (EOF(102))
                Line Input #102, ARec
                lstReport.AddItem ARec
            Wend
            FM.CloseFile CLng(102)
        End If
    End If
    If (Trim(SecondaryAdjustmentsFile) <> "") Then
        If (FM.IsFileThere(SecondaryAdjustmentsFile)) Then
            lstReport.AddItem ""
            lstReport.AddItem "--------------------------------"
            lstReport.AddItem "Secondary Adjustments Items List"
            lstReport.AddItem "--------------------------------"
            FM.OpenFile SecondaryAdjustmentsFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(102)
            While Not (EOF(102))
                Line Input #102, ARec
                lstReport.AddItem ARec
            Wend
            FM.CloseFile CLng(102)
        End If
    End If
' Always retain results
    FM.OpenFile ResultsFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(102)
    For z = 1 To lstReport.ListCount - 1
        Print #102, lstReport.List(z)
    Next z
    FM.CloseFile CLng(102)
    If (FM.IsFileThere(RejectFile)) Then
        FM.Kill RejectFile
    End If
    If (FM.IsFileThere(SecondaryAdjustmentsFile)) Then
        FM.Kill SecondaryAdjustmentsFile
    End If
    lstAcks.Visible = False
    lstAppts.Visible = False
    lstReport.Visible = True
    Return
BatchBalance:
    If (Trim(InvId) <> "") And (RcvId > 0) Then
        Set ApplList = New ApplicationAIList
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplClearBalanceTransactions(RcvId)
        If Not (DenyOn) Then
            TheBal = ApplList.ApplGetBalancebyInvoice(InvId, PBal, IBal, False, False)
            If (TheBal > 0) Then
                Call ApplList.ApplIsInsurerAutoCrossOver(InsId, MedOn1)
                RecId = RcvId
                Call ApplTemp.ApplGetInvoice(RcvId, a1, InvDt, InsId, ApptId)
                PatId = ApplTemp.ApplGetPatientbyReceivable(RcvId)
                Call ApplTemp.ApplGetInsuranceType(ApptId, ATemp)
                TempIns = 0
                zz = 0
                Call ApplTemp.ApplGetNextPayerPartyRef(PatId, InvId, InvDt, ATemp, InsId, zz)
                If (zz > 0) Then
                    zz = zz + 1
                    Call ApplTemp.ApplGetNextPayerParty(PatId, InvId, InvDt, ATemp, False, NewInsd, TempIns, a1, p1, zz)
                    If (TempIns > 0) Then
                        RcvId = ApplTemp.ApplBillSecondary(RcvId, InsId, TempIns, False, True)
                        NewInsd = TempIns
                        TempIns = InsId
                    End If
                End If
                
                
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                If (RcvId > 0) And (TempIns > 0) Then
                    AutoOn = False
                    If (zz = 2) Then
                        If (MedOn1) And (ApplList.ApplIsInsurerAutoCrossOver(TempIns, MedOn2)) Then
                            AutoOn = True
                        End If
                    End If
                    If (ApplTemp.IsTransactionPending(RcvId, "I", TrnId, a1, a2)) Then
                        If (AutoOn) Then
                            TrnId = ApplTemp.BatchTransaction(RcvId, "I", Trim(TheBal), "B", True, 0, True, True, False)
                            Call ApplList.ApplSetTransactionStatus(TrnId, "S")
                        Else
                            Call ApplTemp.BatchTransaction(RcvId, "I", Trim(TheBal), a1, True, 0, False, False, False)
                        End If
                    Else
                        If (ApplTemp.ApplIsInsurerPlanPaper(TempIns)) Then
                            If (AutoOn) Then
                                TrnId = ApplTemp.BatchTransaction(RcvId, "I", Trim(TheBal), "B", True, 0, True, True, False)
                                Call ApplList.ApplSetTransactionStatus(TrnId, "S")
                            Else
                                Call ApplTemp.BatchTransaction(RcvId, "I", Trim(TheBal), "P", True, 0, False, True, False)
                            End If
                        Else
                            TrnId = ApplTemp.BatchTransaction(RcvId, "I", Trim(TheBal), "B", True, 0, AutoOn, False, False)
                        End If
                    End If
                Else
                    If (RcvId > 0) Then
                        RecId = RcvId
                    Else
                        RcvId = RecId
                    End If
                    Call ApplTemp.BatchTransaction(RcvId, "P", Trim(Str(TheBal)), "P", True, 0, False, False, False)
                End If
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                
                
                Call ApplList.ApplSetReceivableBalance(InvId, "", True)
            End If
        End If
        If (Trim(InvId) <> "") Then
            Call ApplTemp.ApplReconcilePatientStatements(InvId)
        End If
        Set ApplList = Nothing
        Set ApplTemp = New ApplicationTemplates
    End If
    Return
ProcClaim:
    TotalRecs = TotalRecs + 1
    RcvId = 0
    DenyOn = False
    Temp = ""
    Call FormatTodaysDate(Temp, False)
    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
    TheCheckDate = Temp
    TheCheck = Trim(Mid(lstAcks.List(RefId), 25, 24))
    TheBatch = Trim(Mid(lstAcks.List(RefId), 5, 8))
    EOBDate = Trim(Mid(lstAcks.List(RefId), 14, 8))
    If (Left(ClaimRef(1), 3) = "CLM") Then
        PatIdRef = Trim(Mid(ClaimRef(1), 14, 12))
        q1 = InStrPS(PatIdRef, "-")
        If (q1 > 0) Then
            PatIdRef = Mid(PatIdRef, q1 + 1, Len(PatIdRef) - q1)
        End If
        q1 = InStrPS(PatIdRef, " ")
        If (q1 > 0) Then
            PatIdRef = Trim(Mid(PatIdRef, q1 + 1, Len(PatIdRef) - q1))
        End If
        ClmSta = Trim(Mid(ClaimRef(1), 27, 2))
        TheAppealNumber = Trim(Mid(ClaimRef(1), 62, 13))
        ThePat = Trim(Mid(ClaimRef(2), 41, 32))
'        q1 = InStrPS(ThePat, " A")
'        If (q1 > 0) Then
'            ThePat = Mid(ThePat, q1 + 1, Len(ThePat) - q1)
'        End If
        w = 3
        If (Left(ClaimRef(3), 3) = "INS") Then
            w = 4
        End If
        ADate = Trim(Mid(ClaimRef(w), 5, 8))
    End If
' This is due to the stupidity of United Healthcare ...
    If (InStrPS(UCase(CPty), "UNITEDHEALTHCAR") > 0) Then
        If (Len(ThePat) = 16) Then
            ThePat = Mid(ThePat, 6, Len(ThePat) - 5)
            If (Mid(ThePat, Len(ThePat) - 2, 2) = "000") Then
                ThePat = Left(ThePat, Len(ThePat) - 3)
            End If
            If (Mid(ThePat, Len(ThePat) - 1, 2) = "00") Then
                ThePat = Left(ThePat, Len(ThePat) - 2)
            End If
        End If
    End If
    k = 1
    Set RetFin = New PatientFinance
    RetFin.PrimaryPerson = ThePat
    RetFin.PrimaryStartDate = ADate
    RetFin.FinancialStatus = ""
    IRef = RetFin.RetrievePatientbyPolicy(k)
    If (IRef > 1) Then
        PatId = RetFin.PatientId
        InsId = RetFin.PrimaryInsurerId
        GoSub GetRcv
        If (RcvId < 1) Then
            Set RetFin = Nothing
            Set RetFin = New PatientFinance
            RetFin.PrimaryPerson = ThePat
            RetFin.PrimaryStartDate = ADate
            RetFin.FinancialStatus = ""
            Call RetFin.RetrievePatientbyPolicy(k + 1)
        End If
    End If
    If (TType = "G") Then
        Do Until (IRef < 1)
            Set RetIns = New Insurer
            RetIns.InsurerId = RetFin.PrimaryInsurerId
            If (RetIns.RetrieveInsurer) Then
                If (TType = Left(RetIns.InsurerTFormat, 1)) Then
                    Set RetIns = Nothing
                    Exit Do
                Else
                    k = k + 1
                    Set RetFin = Nothing
                    Set RetFin = New PatientFinance
                    RetFin.PrimaryPerson = ThePat
                    RetFin.PrimaryStartDate = ADate
                    RetFin.FinancialStatus = ""
                    IRef = RetFin.RetrievePatientbyPolicy(k)
                End If
            End If
            Set RetIns = Nothing
        Loop
    End If
    If (IRef > 0) Then
        PatId = RetFin.PatientId
        InsId = RetFin.PrimaryInsurerId
        Set ApplTemp = New ApplicationTemplates
        MedicareOn = ApplTemp.ApplIsInsurerMedicare(InsId, False)
        Set ApplTemp = Nothing
    Else
        q1 = 2
        h1 = InStrPS(ThePat, " A")
        If (h1 < 1) Then
            If (Left(ThePat, 1) = "A") Then
                h1 = 1
                q1 = 1
            End If
        End If
        If (h1 > 0) Then
            ThePat = Trim(Mid(ThePat, h1 + q1, Len(ThePat) - (h1 + (q1 - 1))))
            Set RetFin = Nothing
            Set RetFin = New PatientFinance
            RetFin.PrimaryPerson = ThePat
            RetFin.PrimaryStartDate = ADate
            RetFin.FinancialStatus = ""
            If (RetFin.RetrievePatientbyPolicy(1) > 0) Then
                Set RetIns = New Insurer
                RetIns.InsurerId = RetFin.PrimaryInsurerId
                If (RetIns.RetrieveInsurer) Then
                    If (UCase(Trim(RetIns.InsurerName)) = "UNITED HEALTH CARE") Or (UCase(Trim(RetIns.InsurerName)) = "UNITED HEALTHCARE") Then
                        PatId = RetFin.PatientId
                        InsId = RetFin.PrimaryInsurerId
                        Set ApplTemp = New ApplicationTemplates
                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(InsId, False)
                        Set ApplTemp = Nothing
                    End If
                    If (UCase(Trim(RetIns.InsurerName)) = "UNITED HEALTHCARE EMPIRE") Then
                        PatId = RetFin.PatientId
                        InsId = RetFin.PrimaryInsurerId
                        Set ApplTemp = New ApplicationTemplates
                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(InsId, False)
                        Set ApplTemp = Nothing
                    End If
                End If
                Set RetIns = Nothing
            End If
        End If
    End If
    If (PatId <> val(PatIdRef)) Then
        If (Len(PatIdRef) <= 7) Then
            PatId = val(PatIdRef)
        Else
            PatId = 0
        End If
    End If
    PrAmt = 0
    w = 3
    If (Left(ClaimRef(3), 3) = "INS") Then
        w = 4
    End If
    If (Left(ClaimRef(1), 3) = "SVC") Then
        w = 1
    End If
    Set RetFin = Nothing
    If (PatId > 0) And (InsId > 0) Then
        GoSub GetRcv
        If (RcvId > 0) And (Trim(InvId) <> "") Then
            For z = w To p
                Srv = Trim(Mid(ClaimRef(z), 15, 5))
                SDate = Trim(Mid(ClaimRef(z), 5, 8))
                Mod1 = Trim(Mid(ClaimRef(z), 21, 2))
                Mod2 = Trim(Mid(ClaimRef(z), 23, 2))
                Mod3 = Trim(Mid(ClaimRef(z), 25, 2))
                Mod4 = Trim(Mid(ClaimRef(z), 27, 2))
                Chrg = Trim(Mid(ClaimRef(z), 30, 7))
                PAmt = Trim(Mid(ClaimRef(z), 38, 7))
                Qnty = Trim(Mid(ClaimRef(z), 46, 4))
                ATyp = Trim(Mid(ClaimRef(z), 51, 2))
                PRsn = Trim(Mid(ClaimRef(z), 54, 5))
                AAmt = Trim(Mid(ClaimRef(z), 60, 7))
                PrAmt = PrAmt + Round(val(Trim(Mid(ClaimRef(z), 68, 7))), 2)
                XAmt = Trim(Mid(ClaimRef(z), 68, 7))
                XRsn = Trim(Mid(ClaimRef(z), 76, 5))
                AllowAmt = Round(val(Trim(Mid(ClaimRef(z), 82, Len(ClaimRef(z)) - 81))), 2)
                GoSub GetSrvId
                If (SrvItem > 0) Then
                    If (ClmSta <> "01") And (ClmSta <> "02") And (ClmSta <> "03") And (ClmSta <> "19") And (ClmSta <> "20") And (ClmSta <> "21") And (ClmSta <> "22") Then
                        DenyOn = True
                        TotalDenys = TotalDenys + 1
                        VType = "D"
                        ZType = "I"
                        GoSub VerifyPayment
                        If (VfyPst) Then
                            Set RetPay = New PatientReceivablePayment
                            RetPay.PaymentId = 0
                            If (RetPay.RetrievePatientReceivablePayments) Then
                                RetPay.PaymentAmount = 0
                                RetPay.PaymentType = "D"
                                RetPay.ReceivableId = RcvId
                                RetPay.PaymentFinancialType = ""
                                RetPay.PaymentCheck = TheCheck
                                RetPay.PaymentCommentOn = False
                                RetPay.PaymentDate = TheCheckDate
                                RetPay.PaymentAssignBy = 0
                                RetPay.PaymentPayerType = "I"
                                RetPay.PaymentRefType = "K"
                                RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                RetPay.PaymentService = Srv
                                RetPay.PaymentServiceItem = SrvItem
                                RetPay.PaymentPayerId = InsId
                                RetPay.PaymentReason = PRsn
                                RetPay.PaymentAppealNumber = TheAppealNumber
                                RetPay.PaymentEOBDate = EOBDate
                                Call RetPay.ApplyPatientReceivablePayments
                            End If
                            Set RetPay = Nothing
                        Else
                            TotalNotPosted = TotalNotPosted + 1
                        End If
                    ElseIf (ClmSta = "22") Then ' Reverse
                        TotalRevs = TotalRevs + 1
                        TotalRevAmount = TotalRevAmount + Round(Abs(val(Trim(PAmt))), 2)
                        VType = "U"
                        ZType = "I"
                        GoSub VerifyPayment
                        If (VfyPst) Then
                            Set RetPay = New PatientReceivablePayment
                            If (val(Trim(PAmt)) <> 0) Then
                                RetPay.PaymentId = 0
                                If (RetPay.RetrievePatientReceivablePayments) Then
                                    RetPay.PaymentAmount = Round(Abs(val(Trim(PAmt))), 2)
                                    RetPay.PaymentType = "U"
                                    RetPay.ReceivableId = RcvId
                                    RetPay.PaymentFinancialType = "D"
                                    RetPay.PaymentCheck = TheCheck
                                    RetPay.PaymentCommentOn = False
                                    RetPay.PaymentDate = TheCheckDate
                                    RetPay.PaymentAssignBy = 0
                                    RetPay.PaymentPayerType = "I"
                                    RetPay.PaymentRefType = "K"
                                    RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                    RetPay.PaymentService = Srv
                                    RetPay.PaymentServiceItem = SrvItem
                                    RetPay.PaymentPayerId = InsId
                                    RetPay.PaymentReason = PRsn
                                    RetPay.PaymentAppealNumber = TheAppealNumber
                                    RetPay.PaymentEOBDate = EOBDate
                                    Call RetPay.ApplyPatientReceivablePayments
                                End If
                                Set RetPay = Nothing
                            End If
                            
                            If ((PRsn = "42") Or (PRsn = "45") Or (PRsn = "2")) And (val(Trim(AAmt)) <> 0) Then
                                Set RetPay = New PatientReceivablePayment
                                RetPay.PaymentId = 0
                                If (RetPay.RetrievePatientReceivablePayments) Then
                                    RetPay.PaymentAmount = Round(Abs(val(Trim(AAmt))), 2)
                                    RetPay.ReceivableId = RcvId
                                    RetPay.PaymentType = "U"
                                    RetPay.PaymentFinancialType = "D"
                                    If (PRsn = "2") Then
                                        RetPay.PaymentFinancialType = ""
                                        RetPay.PaymentType = "|"
                                    End If
                                    RetPay.PaymentCheck = TheCheck
                                    RetPay.PaymentCommentOn = False
                                    RetPay.PaymentDate = TheCheckDate
                                    RetPay.PaymentAssignBy = 0
                                    RetPay.PaymentPayerType = "I"
                                    RetPay.PaymentRefType = "K"
                                    RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                    RetPay.PaymentService = Srv
                                    RetPay.PaymentServiceItem = SrvItem
                                    RetPay.PaymentPayerId = InsId
                                    RetPay.PaymentReason = PRsn
                                    RetPay.PaymentAppealNumber = TheAppealNumber
                                    RetPay.PaymentEOBDate = EOBDate
                                    Call RetPay.ApplyPatientReceivablePayments
                                End If
                                Set RetPay = Nothing
                            End If
                        Else
                            TotalNotPosted = TotalNotPosted + 1
                        End If
                    Else
                        If (val(Trim(PAmt)) > 0) Then
                            TotalPays = TotalPays + 1
                            TotalPayAmount = TotalPayAmount + Round(val(Trim(PAmt)), 2)
                            VType = "P"
                            ZType = "I"
                            GoSub VerifyPayment
                            If (VfyPst) Then
                                Set RetPay = New PatientReceivablePayment
                                RetPay.PaymentId = 0
                                If (RetPay.RetrievePatientReceivablePayments) Then
                                    RetPay.PaymentAmount = Round(val(Trim(PAmt)), 2)
                                    RetPay.PaymentType = "P"
                                    RetPay.ReceivableId = RcvId
                                    RetPay.PaymentFinancialType = "C"
                                    RetPay.PaymentCheck = TheCheck
                                    RetPay.PaymentCommentOn = False
                                    RetPay.PaymentDate = TheCheckDate
                                    RetPay.PaymentAssignBy = 0
                                    RetPay.PaymentRefType = "K"
                                    RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                    RetPay.PaymentService = Srv
                                    RetPay.PaymentServiceItem = SrvItem
                                    RetPay.PaymentPayerType = "I"
                                    RetPay.PaymentPayerId = InsId
                                    RetPay.PaymentReason = PRsn
                                    RetPay.PaymentAppealNumber = TheAppealNumber
                                    RetPay.PaymentEOBDate = EOBDate
                                    Call RetPay.ApplyPatientReceivablePayments
                                    If (AllowAmt > 0) Then
                                        Set ApplTemp = New ApplicationTemplates
                                        Call ApplTemp.ApplPostSrvAllowed(SrvItem, AllowAmt)
                                        Set ApplTemp = Nothing
                                    End If
                                End If
                                Set RetPay = Nothing
                            Else
                                TotalNotPosted = TotalNotPosted + 1
                                TotalNotPostedAmt = TotalNotPostedAmt + Round(val(Trim(PAmt)), 2)
                                GoSub PostSrvDuplicate
                            End If
                        End If
                        If (val(Trim(AAmt)) <> 0) And (InsType = "T") Then       ' 45 is for BCBS
                            If (PRsn = "42") Or (PRsn = "59") Or (PRsn = "B6") Or (PRsn = "45") Or (PRsn = "A2") Or (PRsn = "58") Then
                                TotalAdjs = TotalAdjs + 1
                                TotalAdjAmount = TotalAdjAmount + Round(val(Trim(AAmt)), 2)
                                VType = "X"
                                ZType = "I"
                                GoSub VerifyPayment
                                GoSub VerifyContractual
                                If (VfyPst) And (VfyCon) Then
                                    Set RetPay = New PatientReceivablePayment
                                    RetPay.PaymentId = 0
                                    If (RetPay.RetrievePatientReceivablePayments) Then
                                        RetPay.PaymentAmount = Round(val(Trim(AAmt)), 2)
                                        RetPay.PaymentType = "X"
                                        RetPay.ReceivableId = RcvId
                                        RetPay.PaymentFinancialType = "C"
                                        RetPay.PaymentCheck = TheCheck
                                        RetPay.PaymentCommentOn = False
                                        RetPay.PaymentDate = TheCheckDate
                                        RetPay.PaymentAssignBy = 0
                                        RetPay.PaymentPayerType = "I"
                                        RetPay.PaymentRefType = "K"
                                        RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                        RetPay.PaymentService = Srv
                                        RetPay.PaymentServiceItem = SrvItem
                                        RetPay.PaymentPayerId = InsId
                                        RetPay.PaymentReason = PRsn
                                        RetPay.PaymentAppealNumber = TheAppealNumber
                                        RetPay.PaymentEOBDate = EOBDate
                                        Call RetPay.ApplyPatientReceivablePayments
                                    End If
                                Else
                                    TotalNotPosted = TotalNotPosted + 1
                                End If
                            ElseIf (ATyp = "OA") Then
                                If (PRsn = "100") Then
                                    DenyOn = True
                                    VType = "0"
                                    TotalPayOther = TotalPayOther + 1
                                Else ' e.g. 23, 161, 116
                                    If Not (IsOADenial(PRsn)) Then
                                        DenyOn = True
                                        VType = "D"
                                        TotalSrvDenys = TotalSrvDenys + 1
                                    Else
                                        VType = "]"
                                    End If
                                End If
                                ZType = "I"
                                GoSub VerifyPayment
                                If (VfyPst) Then
                                    Set RetPay = New PatientReceivablePayment
                                    RetPay.PaymentId = 0
                                    If (RetPay.RetrievePatientReceivablePayments) Then
                                        RetPay.PaymentAmount = Round(val(Trim(AAmt)), 2)
                                        RetPay.PaymentType = VType
                                        RetPay.ReceivableId = RcvId
                                        RetPay.PaymentFinancialType = ""
                                        RetPay.PaymentCheck = TheCheck
                                        RetPay.PaymentCommentOn = False
                                        RetPay.PaymentDate = TheCheckDate
                                        RetPay.PaymentAssignBy = 0
                                        RetPay.PaymentPayerType = "I"
                                        RetPay.PaymentRefType = "K"
                                        RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                        RetPay.PaymentService = Srv
                                        RetPay.PaymentServiceItem = SrvItem
                                        RetPay.PaymentPayerId = InsId
                                        RetPay.PaymentReason = PRsn
                                        RetPay.PaymentAppealNumber = TheAppealNumber
                                        RetPay.PaymentEOBDate = EOBDate
                                        Call RetPay.ApplyPatientReceivablePayments
                                    End If
                                    Set RetPay = Nothing
                                Else
                                    TotalNotPosted = TotalNotPosted + 1
                                    DenyOn = False
                                End If
                            Else
                                TotalSrvDenys = TotalSrvDenys + 1
                                VType = "D"
                                If (ATyp = "PR") Then
                                    If (IsPRDenial(XRsn, True)) Then
                                        VType = "D"
                                        DenyOn = True
                                    ElseIf (IsPRDenial(XRsn, False)) Then
                                        VType = "Q"
                                    End If
                                End If
                                ZType = "I"
                                GoSub VerifyPayment
                                If (VfyPst) Then
                                    DenyOn = True
                                    Set RetPay = New PatientReceivablePayment
                                    RetPay.PaymentId = 0
                                    If (RetPay.RetrievePatientReceivablePayments) Then
                                        RetPay.PaymentAmount = Round(val(Trim(AAmt)), 2)
                                        RetPay.PaymentType = "D"
                                        RetPay.ReceivableId = RcvId
                                        RetPay.PaymentFinancialType = ""
                                        RetPay.PaymentCheck = TheCheck
                                        RetPay.PaymentCommentOn = False
                                        RetPay.PaymentDate = TheCheckDate
                                        RetPay.PaymentAssignBy = 0
                                        RetPay.PaymentPayerType = "I"
                                        RetPay.PaymentRefType = "K"
                                        RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                        RetPay.PaymentService = Srv
                                        RetPay.PaymentServiceItem = SrvItem
                                        RetPay.PaymentPayerId = InsId
                                        RetPay.PaymentReason = PRsn
                                        RetPay.PaymentAppealNumber = TheAppealNumber
                                        RetPay.PaymentEOBDate = EOBDate
                                        Call RetPay.ApplyPatientReceivablePayments
                                    End If
                                    Set RetPay = Nothing
                                Else
                                    TotalNotPosted = TotalNotPosted + 1
                                End If
                            End If
                            Set RetPay = Nothing
                        End If
                        If (val(Trim(XAmt)) > 0) And (InsType = "T") Then
                            If (Trim(XRsn) <> "") Then
                                TotalSrvPatResp = TotalSrvPatResp + 1
                                If (val(Trim(XRsn)) = 1) Then
                                    VType = "!"
                                ElseIf (val(Trim(XRsn)) = 2) Then
                                    VType = "|"
                                ElseIf (val(Trim(XRsn)) = 3) Then   ' BCBS
                                    VType = "|"
                                Else
                                    VType = "?"
                                End If
                                If (IsPRDenial(XRsn, True)) Then
                                    VType = "D"
                                    DenyOn = True
                                ElseIf (IsPRDenial(XRsn, False)) Then
                                    VType = "Q"
                                End If
                                ZType = "I"
                                GoSub VerifyPayment
                                If (VfyPst) Then
                                    Set RetPay = New PatientReceivablePayment
                                    RetPay.PaymentId = 0
                                    If (RetPay.RetrievePatientReceivablePayments) Then
                                        RetPay.PaymentAmount = Round(val(Trim(XAmt)), 2)
                                        If (val(Trim(XRsn)) = 1) Then
                                            RetPay.PaymentType = "!"
                                        ElseIf (val(Trim(XRsn)) = 2) Then
                                            RetPay.PaymentType = "|"
                                        ElseIf (val(Trim(XRsn)) = 3) Then   ' BCBS
                                            RetPay.PaymentType = "|"
                                        Else
                                            RetPay.PaymentType = "?"
                                        End If
                                        RetPay.ReceivableId = RcvId
                                        RetPay.PaymentFinancialType = ""
                                        RetPay.PaymentCheck = TheCheck
                                        RetPay.PaymentCommentOn = False
                                        RetPay.PaymentDate = TheCheckDate
                                        RetPay.PaymentAssignBy = 0
                                        RetPay.PaymentPayerType = "I"
                                        RetPay.PaymentRefType = "K"
                                        RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
                                        RetPay.PaymentService = Srv
                                        RetPay.PaymentServiceItem = SrvItem
                                        RetPay.PaymentPayerId = InsId
                                        RetPay.PaymentReason = XRsn
                                        RetPay.PaymentAppealNumber = TheAppealNumber
                                        RetPay.PaymentEOBDate = EOBDate
                                        Call RetPay.ApplyPatientReceivablePayments
                                    End If
                                    Set RetPay = Nothing
                                Else
                                    TotalNotPosted = TotalNotPosted + 1
                                End If
                            End If
                        End If
                    End If
                    Set ApplList = New ApplicationAIList
                    Call ApplList.ApplSetReceivableBalance(Trim(InvId), "", True)
                    Set ApplList = Nothing
                Else
                    TotalSrvReject = TotalSrvReject + 1
                    TotalSrvRejectAmt = TotalSrvRejectAmt + Round(val(Trim(PAmt)), 2) + Round(val(Trim(AAmt)), 2)
                    MyReason = "No Service Record"
                    GoSub PostSrvRejects
                    lblDisp.Caption = "Processing Service Rejected"
                    lblDisp.Visible = True
                    DoEvents
                End If
                lblDisp.Caption = "Processing Srv " + Srv
                lblDisp.Visible = True
                DoEvents
            Next z
            TotalPrAmount = TotalPrAmount + PrAmt
        Else
            TotalReject = TotalReject + 1
            MyReason = "No Receivable Present - " + Trim(Str(PatId)) + "-" + Trim(Str(InsId))
            GoSub PostRejects
            lblDisp.Caption = "Processing Service Rejected"
            lblDisp.Visible = True
            DoEvents
        End If
    Else
        TotalReject = TotalReject + 1
        MyReason = "No Patient or Insurer - " + Trim(Str(PatId)) + "-" + Trim(Str(InsId))
        GoSub PostRejects
        lblDisp.Caption = "Processing Claim Rejected"
        lblDisp.Visible = True
        DoEvents
    End If
    lblDisp.Visible = False
    Return
VfySelectSrv:
    GoodRcv = False
    Srv = Trim(Mid(ClaimRef(w), 15, 5))
    Mod1 = Trim(Mid(ClaimRef(w), 21, 2))
    Mod2 = Trim(Mid(ClaimRef(w), 23, 2))
    Mod3 = Trim(Mid(ClaimRef(w), 25, 2))
    Mod4 = Trim(Mid(ClaimRef(w), 27, 2))
    GoSub GetSrvId
    If (SrvItem > 0) Then
        GoodRcv = True
    End If
    Return
GetRcv:
    RcvId = 0
    InvId = ""
    InvDate = ""
    SecondaryOn = False
    InsType = ""
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableInvoiceDate = ADate
    RetRcv.PatientId = PatId
    RetRcv.InsurerId = InsId
    ITot = RetRcv.FindPatientReceivable
    If (ITot > 1) Then
        IRef2 = 1
        Do Until Not (RetRcv.SelectPatientReceivable(IRef2))
            RcvId = RetRcv.ReceivableId
            InvId = RetRcv.ReceivableInvoice
            InvDate = RetRcv.ReceivableInvoiceDate
            InsType = RetRcv.ReceivableInsurerDesignation
            If (RetRcv.ReceivableInsurerDesignation <> "T") Then
                SecondaryOn = True
            End If
            GoodRcv = False
            GoSub VfySelectSrv
            If (GoodRcv) Then
                Exit Do
            End If
            RcvId = 0
            InvId = ""
            InvDate = ""
            IRef2 = IRef2 + 1
        Loop
    ElseIf (ITot = 1) Then
        Call RetRcv.SelectPatientReceivable(1)
        RcvId = RetRcv.ReceivableId
        InvId = RetRcv.ReceivableInvoice
        InvDate = RetRcv.ReceivableInvoiceDate
        InsType = RetRcv.ReceivableInsurerDesignation
        If (RetRcv.ReceivableInsurerDesignation <> "T") Then
            SecondaryOn = True
        End If
    End If
    Set RetRcv = Nothing
    Return
GetRcvA:
    InvIdA = ""
    Set RetRcvA = New PatientReceivables
    RetRcvA.ReceivableId = RcvId
    If (RetRcvA.RetrievePatientReceivable) Then
        InvIdA = RetRcvA.ReceivableInvoice
    End If
    Set RetRcvA = Nothing
    Return
GetSrvId:
    SrvItem = 0
    Set RetSrv = New PatientReceivableService
    RetSrv.Service = Srv
    RetSrv.Invoice = InvId
    RetSrv.ServiceDate = ADate
    If (RetSrv.FindPatientReceivableService > 0) Then
        UTemp = Trim(Mod1 + Mod2 + Mod3 + Mod4)
        u = 1
        Do Until Not (RetSrv.SelectPatientReceivableService(u))
            If (Trim(RetSrv.ServiceModifier) = UTemp) Then
                SrvItem = RetSrv.ItemId
                Exit Do
            End If
            u = u + 1
        Loop
    Else
        Set RetSrv = Nothing
        Set RetSrv = New PatientReceivableService
        RetSrv.Service = Trim(Srv)
        RetSrv.Invoice = InvId
        RetSrv.ServiceDate = ADate
        If (RetSrv.FindPatientReceivableServiceAny > 0) Then
            UTemp = Trim(Mod1 + Mod2 + Mod3 + Mod4)
            u = 1
            Do Until Not (RetSrv.SelectPatientReceivableService(u))
                If (Trim(RetSrv.ServiceModifier) = UTemp) And (Srv = Left(RetSrv.Service, Len(Srv))) And (Len(Srv) = 5) Then
                    Srv = RetSrv.Service
                    SrvItem = RetSrv.ItemId
                    Exit Do
                End If
                u = u + 1
            Loop
        End If
        Set RetSrv = Nothing
    End If
    Return
PostSkippedAdjustments:
    FM.OpenFile SecondaryAdjustmentsFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(102)
    Print #102, "Check #:"; TheCheck; " Date:"; TheCheckDate; " Appeal #:"; TheAppealNumber
    If (p > 2) Then
        For zz = 1 To 2
            Print #102, ClaimRef(zz)
        Next zz
        If (z > 0) And (z <= p) Then
            Print #102, ClaimRef(z)
        End If
    End If
    FM.CloseFile CLng(102)
    Return
PostRejects:
    FM.OpenFile RejectFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(102)
    Print #102, "Check #:"; TheCheck; " Date:"; TheCheckDate; " Appeal #:"; TheAppealNumber
    If (Trim(MyReason) <> "") Then
        Print #102, "Reject Reason: "; MyReason
    End If
    For z = 1 To p
        Print #102, ClaimRef(z)
    Next z
    FM.CloseFile CLng(102)
    Return
PostSrvDuplicate:
    FM.OpenFile RejectFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(102)
    Print #102, "-----------Service Duplicate"
    Print #102, "Check #:"; TheCheck; " Date:"; TheCheckDate; " Appeal #:"; TheAppealNumber
    For zz = 1 To 2
        Print #102, ClaimRef(zz)
    Next zz
    If (z > 0) And (z <= p) Then
        Print #102, ClaimRef(z)
    End If
    Print #102, "-------------------------"
    FM.CloseFile CLng(102)
    Return
PostSrvRejects:
    FM.OpenFile RejectFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(102)
    Print #102, "-----------Service Reject"
    Print #102, "Check #:"; TheCheck; " Date:"; TheCheckDate; " Appeal #:"; TheAppealNumber
    If (Trim(MyReason) <> "") Then
        Print #102, "Reject Reason: "; MyReason
    End If
    For zz = 1 To 2
        Print #102, ClaimRef(zz)
    Next zz
    If (z > 0) And (z <= p) Then
        Print #102, ClaimRef(z)
    End If
    Print #102, "-------------------------"
    FM.CloseFile CLng(102)
    Return
VerifyPayment:
    VfyPst = True
    Set RetPay = New PatientReceivablePayment
    RetPay.PaymentType = VType
    RetPay.ReceivableId = RcvId
    RetPay.PaymentCheck = TheCheck
    RetPay.PaymentRefType = "K"
    RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
    RetPay.PaymentPayerType = ZType
    RetPay.PaymentPayerId = InsId
    RetPay.PaymentService = Srv
    RetPay.PaymentServiceItem = SrvItem
    RetPay.PaymentAppealNumber = TheAppealNumber
    RetPay.PaymentReason = PRsn
    If (RetPay.FindPatientReceivablePaymentsSpecial > 0) Then
        VfyPst = False
    End If
    Set RetPay = Nothing
    Return
VerifyContractual:
    VfyCon = True
    Set RetPay = New PatientReceivablePayment
    RetPay.PaymentType = "X"
    RetPay.ReceivableId = 0
    RetPay.PaymentCheck = TheCheck
    RetPay.PaymentRefType = "K"
    RetPay.PaymentRefId = TheBatch + "-" + Trim(ClmSta)
    RetPay.PaymentPayerType = "I"
    RetPay.PaymentPayerId = InsId
    RetPay.PaymentService = Srv
    RetPay.PaymentServiceItem = SrvItem
    RetPay.PaymentAppealNumber = TheAppealNumber
    RetPay.PaymentReason = PRsn
    If (RetPay.FindPatientReceivablePayments > 0) Then
        z1 = 1
        Do Until Not (RetPay.SelectPatientReceivablePayments(z1))
            If (Round(val(Trim(AAmt)), 2) = RetPay.PaymentAmount) Then
                GoSub GetRcvA
                If (Trim(InvIdA) = Trim(InvId)) And (TheAppealNumber = Trim(RetPay.PaymentAppealNumber)) Then
                    VfyCon = False
                    Exit Do
                End If
            End If
            z1 = z1 + 1
        Loop
    End If
    Set RetPay = Nothing
' do skip verify check on if you need to
    If (VfyCon) Then
        If (MedicareOn) And (SecondaryOn) Then
            VfyCon = False
            GoSub PostSkippedAdjustments
        End If
    End If
    Return
End Function

Private Function VerifyFileType(FileName As String) As Integer
Dim Tp As String
VerifyFileType = -1
If (FM.IsFileThere(FileName)) Then
    FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    If (LOF(101) > 10) Then
        Tp = Input(10, #101)
        If (Left(Tp, 3) = "ISA") Then
            VerifyFileType = 1
        ElseIf (Left(Tp, 3) = "100") Then
            VerifyFileType = 2
        End If
    End If
    FM.CloseFile CLng(101)
End If
End Function

Private Function VerifyCompletedFile(FileName As String) As Boolean
VerifyCompletedFile = False
If InStrPS(FileName, "-200") > 0 _
Or InStrPS(FileName, "-201") > 0 Then
    VerifyCompletedFile = True
End If
End Function

Private Sub lstReport_Click()
Dim z As Integer
Dim ApplTemp As ApplicationTemplates
If (lstReport.ListIndex = 0) Then
    lstAppts.ListIndex = -1
    lstAppts.Visible = True
    lstAcks.Visible = False
    lstReport.Clear
    lstReport.Visible = False
    Call LoadRemittance
ElseIf (lstReport.ListIndex = 1) Then
    Set ApplTemp = New ApplicationTemplates
    Set ApplTemp.lstBox = lstReport
    Call ApplTemp.PrintProof(-1, "RemittanceResults", "")
    Set ApplTemp = Nothing
End If
End Sub

Private Function IsOADenial(ARsn As String) As Boolean
Dim RetCode As PracticeCodes
IsOADenial = True
If (Trim(ARsn) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "ERAIGNOREOA"
    RetCode.ReferenceCode = ARsn
    If (RetCode.FindCode < 1) Then
        IsOADenial = False
    End If
    Set RetCode = Nothing
End If
End Function

Private Function IsPRDenial(ARsn As String, IType As Boolean) As Boolean
Dim RetCode As PracticeCodes
IsPRDenial = True
If (Trim(ARsn) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "ERADENIALPR"
    If (IType) Then
        RetCode.ReferenceType = "ERAIGNOREPR"
    End If
    RetCode.ReferenceCode = ARsn
    If (RetCode.FindCode < 1) Then
        IsPRDenial = False
    End If
    Set RetCode = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

Private Function LoadPaymentMethod()
Dim r As Integer
Dim DisplayItem As String
Dim RetCode As New PracticeCodes
RetCode.ReferenceType = "PAYABLETYPE"
RetCode.ReferenceCode = ""
    If (RetCode.FindCodePartial > 0) Then
        r = 1
        While (RetCode.SelectCode(r))
            DisplayItem = Trim(Mid(RetCode.ReferenceCode, 1, InStrPS(1, RetCode.ReferenceCode, "-") - 1)) _
            & "- " & myTrim(RetCode.ReferenceCode, 1)
            PaymentMethodCombo.AddItem DisplayItem
            r = r + 1
        Wend
    End If
    
    PaymentMethodCombo.ListIndex = -1
    
    For r = 0 To PaymentMethodCombo.ListCount - 1
        If (Trim(Mid(PaymentMethodCombo.List(r), 1, InStrPS(1, PaymentMethodCombo.List(r), "-") - 1)) = "Check") Then
            PaymentMethodCombo.ListIndex = r
            Exit For
        End If
    Next
    
    If PaymentMethodCombo.ListIndex = -1 Then
        If PaymentMethodCombo.ListCount > 0 Then PaymentMethodCombo.ListIndex = 0
    End If
Set RetCode = Nothing
End Function

