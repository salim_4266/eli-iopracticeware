VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmApptSlots 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkType 
      BackColor       =   &H00A95911&
      Caption         =   "Do Only Doctor Resources"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   1560
      TabIndex        =   4
      Top             =   5760
      Value           =   1  'Checked
      Width           =   4575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   6000
      TabIndex        =   2
      Top             =   4320
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptSlots.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStart 
      Height          =   990
      Left            =   1440
      TabIndex        =   3
      Top             =   4320
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptSlots.frx":01E1
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Establishing Resource "
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4080
      TabIndex        =   1
      Top             =   2880
      Visible         =   0   'False
      Width           =   2595
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00A95911&
      Caption         =   "Establish All Appointment Slots for Schedulable Resources"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   975
      Left            =   3000
      TabIndex        =   0
      Top             =   1200
      Width           =   4695
   End
End
Attribute VB_Name = "frmApptSlots"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExit_Click()
Unload frmApptSlots
End Sub

Private Sub cmdStart_Click()
Dim IType As Boolean
Dim ApplCal As ApplicationCalendar
IType = True
If (IType = 0) Then
    IType = False
End If
Set ApplCal = New ApplicationCalendar
If (ApplCal.EstablishSlots(Label2, IType)) Then
    frmEventMsgs.Header = "Appointment Slots Setup Completed"
Else
    frmEventMsgs.Header = "Appointment Slots Setup Failed"
End If
Set ApplCal = Nothing
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
Unload frmApptSlots
End Sub

Private Sub Form_Load()
If (frmHome.GetPowerUser) Then
    frmApptSlots.BorderStyle = 1
    frmApptSlots.ClipControls = True
    frmApptSlots.Caption = Mid(frmApptSlots.Name, 4, Len(frmApptSlots.Name) - 3)
    frmApptSlots.AutoRedraw = True
    frmApptSlots.Refresh
End If
End Sub
