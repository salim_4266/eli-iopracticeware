VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMessages 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch 
      Height          =   615
      Left            =   4680
      TabIndex        =   27
      Top             =   1200
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Messages.frx":0000
   End
   Begin VB.TextBox txtFilter 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4800
      MaxLength       =   64
      TabIndex        =   26
      Top             =   720
      Visible         =   0   'False
      Width           =   1260
   End
   Begin VB.TextBox txtRank 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   2280
      MaxLength       =   5
      TabIndex        =   24
      Top             =   6240
      Width           =   1020
   End
   Begin VB.CheckBox chkFU 
      BackColor       =   &H0077742D&
      Caption         =   "FollowUp"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4800
      TabIndex        =   23
      Top             =   3360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtOrder 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4680
      MaxLength       =   5
      TabIndex        =   21
      Top             =   6240
      Width           =   780
   End
   Begin VB.CheckBox chkSig 
      BackColor       =   &H0077742D&
      Caption         =   "Signature"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4800
      TabIndex        =   20
      Top             =   2880
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtLtr1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   128
      TabIndex        =   7
      Top             =   7680
      Visible         =   0   'False
      Width           =   9180
   End
   Begin VB.CheckBox chkExc 
      BackColor       =   &H0077742D&
      Caption         =   "Exclude"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4800
      TabIndex        =   5
      Top             =   3120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtLtr 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   128
      TabIndex        =   6
      Top             =   7200
      Visible         =   0   'False
      Width           =   9180
   End
   Begin VB.TextBox txtAltMsg 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      MaxLength       =   64
      TabIndex        =   3
      Top             =   5760
      Width           =   5220
   End
   Begin VB.TextBox txtNote 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000007&
      Height          =   2895
      Left            =   5520
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   4200
      Width           =   6255
   End
   Begin VB.TextBox txtMessage 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      MaxLength       =   64
      TabIndex        =   2
      Top             =   4800
      Width           =   5220
   End
   Begin VB.ListBox lstRefType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   3540
      ItemData        =   "Messages.frx":01E1
      Left            =   240
      List            =   "Messages.frx":01E3
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   600
      Width           =   4380
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   750
      Left            =   240
      TabIndex        =   10
      Top             =   8160
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Messages.frx":01E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   750
      Left            =   9960
      TabIndex        =   8
      Top             =   8160
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Messages.frx":03C4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   750
      Left            =   5280
      TabIndex        =   9
      Top             =   8160
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Messages.frx":05A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFormat 
      Height          =   615
      Left            =   4680
      TabIndex        =   19
      Top             =   2160
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Messages.frx":0784
   End
   Begin VB.ListBox lstRefCode 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   3540
      ItemData        =   "Messages.frx":0969
      Left            =   6240
      List            =   "Messages.frx":096B
      TabIndex        =   1
      Top             =   600
      Width           =   5580
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4800
      Top             =   3720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Flags           =   1
   End
   Begin VB.Label lblRank 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Rank"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   25
      Top             =   6240
      Width           =   1980
   End
   Begin VB.Label lblOrder 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Test Order"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3360
      TabIndex        =   22
      Top             =   6240
      Width           =   1260
   End
   Begin VB.Label lblLtr1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Other Ltr Translation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   18
      Top             =   7680
      Visible         =   0   'False
      Width           =   2340
   End
   Begin VB.Label lblLtr 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Letter Translation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   17
      Top             =   7200
      Visible         =   0   'False
      Width           =   2340
   End
   Begin VB.Label lblAltMsg 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Enter the Alternate Reference Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   16
      Top             =   5280
      Width           =   5220
   End
   Begin VB.Label lblNote 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   5520
      TabIndex        =   15
      Top             =   3840
      Width           =   540
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Codes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   8160
      TabIndex        =   14
      Top             =   240
      Width           =   720
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Setup Messages"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   375
      Left            =   3660
      TabIndex        =   13
      Top             =   120
      Width           =   4095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Enter the Reference Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   12
      Top             =   4320
      Width           =   5220
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Reference Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   11
      Top             =   240
      Width           =   1650
   End
End
Attribute VB_Name = "frmMessages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CodesOn As Boolean
Public ServicesOn As Boolean
Public DeleteOn As Boolean
Private SigOn As Boolean
Private LettersOn As Boolean
Private OrgCode As String
Private TheCodeFormat As String

Private Sub cmdApply_Click()
Dim ARank As Long
Dim ItemId As Long
Dim Temp1 As String, Temp2 As String, Temp3 As String
Dim m As Integer
Dim ApplTbl As ApplicationTables
If (lstRefType.ListIndex < 0) And (Trim(txtMessage.Text) = "") Then
    Unload frmMessages
    Exit Sub
End If
If (lstRefType.ListIndex < 0) Then
    frmEventMsgs.Header = "Must select a Reference Type"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (Trim(txtMessage.Text) = "") Then
    frmEventMsgs.Header = "Must Enter a Code"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If


Set ApplTbl = New ApplicationTables
  
Temp1 = "F"
If (chkExc.Value = 1) Then
    Temp1 = "T"
End If
Temp2 = "F"
If (chkSig.Value = 1) Then
    Temp2 = "T"
End If
Temp3 = ""
If (chkFU.Value = 1) Then
    Temp3 = "F"
End If
ARank = val(Trim(txtRank.Text))

If (lstRefCode.ListIndex > 0) Then
    ItemId = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 383))
End If

If (Trim(OrgCode) <> "") Then
    If (ApplTbl.ApplIsCode(lstRefType.List(lstRefType.ListIndex), Trim(txtMessage.Text), ItemId)) Then
        frmEventMsgs.Header = "Code Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        Exit Sub
    End If
Else

If (ApplTbl.ApplDuplicateCode(lstRefType.List(lstRefType.ListIndex), Trim(txtMessage.Text))) Then
        If (UCase(Trim(lstRefType.List(lstRefType.ListIndex))) = "PAYABLETYPE") Then
            frmEventMsgs.Header = "Reference Codes must have unique last letters.  Please use a code with a different last letter."
        Else
            frmEventMsgs.Header = "Reference Codes must have unique first letters.  Please use a code with a different first letter."
        End If
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        Exit Sub
    End If

    If (ApplTbl.ApplIsCode(lstRefType.List(lstRefType.ListIndex), Trim(txtMessage.Text))) Then
        frmEventMsgs.Header = "Code Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        Exit Sub
    End If
    If (ApplTbl.ApplIsPaymentCodeExists(lstRefType.List(lstRefType.ListIndex), _
    Trim(txtMessage.Text), Mid(Trim(txtMessage.Text), Len(Trim(txtMessage.Text)), 1)) _
    And lstRefType.List(lstRefType.ListIndex) = "PaymentType") Then
        frmEventMsgs.Header = "Payment Type Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        Exit Sub
    End If
End If

If Not (ApplTbl.ApplPostCode(lstRefType.List(lstRefType.ListIndex), Trim(txtMessage.Text), Trim(txtAltMsg.Text), Trim(txtNote.Text), CodesOn, Temp2, Temp1, Trim(txtLtr.Text), Trim(txtLtr1.Text), TheCodeFormat, Trim(txtOrder.Text), Temp3, ARank, ItemId)) Then
    frmEventMsgs.Header = "Update not applied"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
lblNote.Visible = False
txtNote.Visible = False
txtNote.Text = ""
lblRank.Visible = False
txtRank.Text = ""
txtRank.Visible = False
lblAltMsg.Visible = False
txtAltMsg.Visible = False
txtAltMsg.Text = ""
lblLtr.Visible = False
txtLtr.Visible = False
txtLtr.Text = ""
lblLtr1.Visible = False
txtLtr1.Text = ""
txtLtr1.Visible = False
lblOrder.Visible = False
txtOrder.Text = ""
txtOrder.Visible = False
chkExc.Caption = "Exclusion"
chkExc.Visible = False
chkExc.Value = 0
chkSig.Caption = "Signature"
chkSig.Visible = False
chkSig.Value = 0
chkFU.Visible = False
chkFU.Value = 0

m = lstRefCode.TopIndex
lstRefType_Click
lstRefCode.TopIndex = m

'lstRefType.ListIndex = -1
'lstRefCode.Clear
'lstRefCode.AddItem " Create a new code"
txtMessage.Text = ""
OrgCode = ""
Set ApplTbl = Nothing
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
If (lstRefType.ListIndex < 0) Then
    frmEventMsgs.Header = "Must select a Reference Type"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (Trim(txtMessage.Text) = "") Then
    frmEventMsgs.Header = "Must Enter a Code"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
Set ApplTbl = New ApplicationTables
If (Trim(OrgCode) <> "") Then
    Call ApplTbl.ApplDeleteCode(lstRefType.List(lstRefType.ListIndex), OrgCode, CodesOn)
End If
lstRefType.ListIndex = -1
lstRefCode.Clear
lstRefCode.AddItem " Create a new code"
txtMessage.Text = ""
lblRank.Visible = False
txtRank.Text = ""
txtRank.Visible = False
lblNote.Visible = False
txtNote.Visible = False
txtNote.Text = ""
lblAltMsg.Visible = False
txtAltMsg.Visible = False
txtAltMsg.Text = ""
lblLtr.Visible = False
txtLtr.Visible = False
txtLtr.Text = ""
lblLtr1.Visible = False
txtLtr1.Text = ""
txtLtr1.Visible = False
lblOrder.Visible = False
txtOrder.Text = ""
txtOrder.Visible = False
chkExc.Caption = "Exclusion"
chkExc.Visible = False
chkExc.Value = 0
chkSig.Caption = "Signature"
chkSig.Visible = False
chkSig.Value = 0
chkFU.Visible = False
chkFU.Value = 0
lstRefType.ListIndex = -1
lstRefCode.Clear
lstRefCode.AddItem " Create a new code"
txtMessage.Text = ""
OrgCode = ""
Set ApplTbl = Nothing
End Sub

Private Sub cmdExit_Click()
Unload frmMessages
End Sub

Private Sub cmdFormat_Click()
Dim MyForm As Boolean
MyForm = False
If (lstRefType.List(lstRefType.ListIndex) = "FacilityAdmission") Then
    MyForm = True
End If
If (frmSetFormat.LoadFormat(Trim(lstRefCode.List(lstRefCode.ListIndex)), TheCodeFormat, MyForm)) Then
    frmSetFormat.Show 1
    If (Trim(frmSetFormat.TheFormat) <> "") Then
        TheCodeFormat = frmSetFormat.TheFormat
    End If
End If
End Sub

Private Sub cmdSearch_Click()
    lblAltMsg.Visible = False
    txtAltMsg.Text = ""
    txtAltMsg.Visible = False
    lblRank.Visible = False
    txtRank.Text = ""
    txtRank.Visible = False
    lblLtr.Visible = False
    txtLtr.Text = ""
    txtLtr.Visible = False
    lblLtr1.Visible = False
    txtLtr1.Text = ""
    txtLtr1.Visible = False
    lblOrder.Visible = False
    txtOrder.Text = ""
    txtOrder.Visible = False
    chkExc.Caption = "Exclusion"
    chkExc.Value = 0
    chkExc.Visible = False
    chkFU.Value = 0
    chkFU.Visible = False
    chkSig.Caption = "Signature"
    chkSig.Value = 0
    chkSig.Visible = False
    lblNote.Visible = False
    txtNote.Visible = False
    txtNote.Text = ""
    txtMessage.Text = ""

    FilteredLstRefCode

End Sub

Private Sub Form_Load()
Dim Temp As String
Dim InternalOn As Boolean
If UserLogin.HasPermission(epPowerUser) Then
    frmMessages.BorderStyle = 1
    frmMessages.ClipControls = True
    frmMessages.Caption = Mid(frmMessages.Name, 4, Len(frmMessages.Name) - 3)
    frmMessages.AutoRedraw = True
    frmMessages.Refresh
End If
TheCodeFormat = ""
SigOn = False
LettersOn = False
InternalOn = CheckConfigCollection("INTERNAL") = "T"
lstRefType.Clear
cmdDelete.Visible = DeleteOn
If (CodesOn) Then
    lstRefType.AddItem "Appt Cancel Reasons"
    lstRefType.AddItem "MyProcedures"
    lstRefType.AddItem "MyDiagnosis"
    lstRefType.AddItem "ChiefComplaints"
    lstRefType.AddItem "ERAIgnoreOA"
    lstRefType.AddItem "ERAIgnorePR"
    lstRefType.AddItem "ERADenialPR"
    lstRefType.AddItem "PaymentType"
    lstRefType.AddItem "PaymentReason"
    lstRefType.AddItem "MedicareSecondary"
    lstRefType.AddItem "AutoAlertType"
    lstRefType.AddItem "BusinessClass"
    lstRefType.AddItem "CPTClass"
    lstRefType.AddItem "PlanType"
    lstRefType.AddItem "Specialty"
    lstRefType.AddItem "Relationship"
    lstRefType.AddItem "ScanType"
    lstRefType.AddItem "PatientAdmin"
    lstRefType.AddItem "ClinicalFollowUpStatus"
    
    lstRefType.AddItem "Rx Request Status"
    lstRefType.AddItem "Drug Families"
    
    lstRefType.AddItem "CLOrderStatus"
    lstRefType.AddItem "CLChoices"
    lstRefType.AddItem "CLMaterials"
    lstRefType.AddItem "CLTint"
    lstRefType.AddItem "CLWearTime"
    lstRefType.AddItem "CLSeries"
    lstRefType.AddItem "CLDisposable"
    lstRefType.AddItem "CLType"

'    lstRefType.AddItem "PCManufacturers"
'    lstRefType.AddItem "PCModel"
'    lstRefType.AddItem "PCColor"
'    lstRefType.AddItem "PCSize"            ' Size (was converted to a text input box - NOT USED)
'    lstRefType.AddItem "PCTint"            ' NOT USED
    
    lstRefType.AddItem "PCChoices"
    lstRefType.AddItem "PCOrderStatus"
    lstRefType.AddItem "PCType"             ' Type
    lstRefType.AddItem "PCBrand"            ' Lens Brand
    lstRefType.AddItem "PCVCode"            ' Lens Type (e.g. bifocal)
    lstRefType.AddItem "PCMaterial"         ' Glass, Poly
    lstRefType.AddItem "PCCoating"          ' Scratch Resistent
    lstRefType.AddItem "PCOptions"          ' Brown, Gray (use to be PCPhotochromatic)
        
    lstRefType.AddItem "SurgeryType"
    lstRefType.AddItem "SurgeryStatus"
    lstRefType.AddItem "SurgeryCaseType"
    lstRefType.AddItem "SurgicalInstrumentation"
    lstRefType.AddItem "SurgicalResources"
    lstRefType.AddItem "SurgicalMedicalClearance"
    lstRefType.AddItem "SurgicalAnesthesiaType"
    lstRefType.AddItem "SurgicalIOL"
    lstRefType.AddItem "SurgicalMedsAndSupplies"
    lstRefType.AddItem "SurgicalTransportation"
    lstRefType.AddItem "SurgicalPreOpMeds"
    lstRefType.AddItem "AccessAds"
    lstRefType.AddItem "NameReference"
    lstRefType.AddItem "ConfirmStatus"
    lstRefType.AddItem "NoteCategory"
    lstRefType.AddItem "Zones"
    lstRefType.AddItem "ZonesAlternatives"
    lstRefType.AddItem "Zones02"
    lstRefType.AddItem "Zones02Alternatives"
    lstRefType.AddItem "Zones08"
    lstRefType.AddItem "Zones08Alternatives"
    lstRefType.AddItem "Quantifiers"
    lstRefType.AddItem "QuantifiersClarity"
    lstRefType.AddItem "QuantifiersColor"
    lstRefType.AddItem "QuantifiersDepth"
    lstRefType.AddItem "QuantifiersDescriptors"
    lstRefType.AddItem "QuantifiersGrade"
    lstRefType.AddItem "QuantifiersHardness"
    lstRefType.AddItem "QuantifiersLids"
    lstRefType.AddItem "QuantifiersLength"
    lstRefType.AddItem "QuantifiersLocation"
    lstRefType.AddItem "QuantifiersMobility"
    lstRefType.AddItem "QuantifiersProgression"
    lstRefType.AddItem "QuantifiersShape"
    lstRefType.AddItem "QuantifiersSize"
    lstRefType.AddItem "USSTATES"
    If (InternalOn) Then
        lstRefType.AddItem "LetterStatus"
        lstRefType.AddItem "ContactType"
        lstRefType.AddItem "EmployeeStatus"
        lstRefType.AddItem "PayableType"
        lstRefType.AddItem "PracticeType"
        lstRefType.AddItem "VendorType"
        lstRefType.AddItem "ResourceType"
        lstRefType.AddItem "QuestionSets"
        lstRefType.AddItem "SubFormat"
        lstRefType.AddItem "TransmitType"
        lstRefType.AddItem "InsurerRef"
        lstRefType.AddItem "InsurerPType"
        lstRefType.AddItem "AppointmentStatus"
    End If
ElseIf (ServicesOn) Then
    lstRefType.AddItem "TypeOfService"
    lstRefType.AddItem "PlaceOfService"
    lstRefType.AddItem "ServiceMods"
    lstRefType.AddItem "ServiceCategory"
    If (InternalOn) Then
        lstRefType.AddItem "ServiceCode"
    End If
Else
    LettersOn = True
    lstRefType.AddItem "Chart"
    lstRefType.AddItem "SystemNormal"
    lstRefType.AddItem "ImpressionNotes"
    lstRefType.AddItem "ImpressionLinkPlanNotes"
    lstRefType.AddItem "ImpressionLinkDiscussion"
    lstRefType.AddItem "SpecialInstructions"
    lstRefType.AddItem "DelinquentBillingNotes"
    lstRefType.AddItem "TestNotes"
    lstRefType.AddItem "RedNotes"
    lstRefType.AddItem "Message"
    lstRefType.AddItem "RxNotes"
    lstRefType.AddItem "SurgeryNotes"
    lstRefType.AddItem "DemographicsNotes"
    lstRefType.AddItem "BillingNotes"
    lstRefType.AddItem "Action"
    lstRefType.AddItem "External Tests"
    lstRefType.AddItem "ExternalTestLinks"
    lstRefType.AddItem "OfficeProcedures"
    lstRefType.AddItem "Ticklers"
    lstRefType.AddItem "AppealLetters"
    lstRefType.AddItem "DenialLetters"
    lstRefType.AddItem "ConsultationLetters"
    lstRefType.AddItem "CollectionLetters"
    lstRefType.AddItem "ReferralLetters"
    lstRefType.AddItem "ExternalLetters"
    lstRefType.AddItem "MiscellaneousLetters"
    lstRefType.AddItem "FacilityAdmission"
End If
lblAltMsg.Visible = False
txtAltMsg.Text = ""
txtAltMsg.Visible = False
lblRank.Visible = False
txtRank.Text = ""
txtRank.Visible = False
lblLtr.Visible = False
txtLtr.Text = ""
txtLtr.Visible = False
lblLtr1.Visible = False
txtLtr1.Text = ""
txtLtr1.Visible = False
lblOrder.Visible = False
txtOrder.Text = ""
txtOrder.Visible = False
chkExc.Caption = "Exclusion"
chkExc.Value = 0
chkExc.Visible = False
chkFU.Value = 0
chkFU.Visible = False
chkSig.Caption = "Signature"
chkSig.Value = 0
chkSig.Visible = False
lblNote.Visible = False
txtNote.Visible = False
txtNote.Text = ""
lstRefType.ListIndex = -1
lstRefCode.Clear
lstRefCode.AddItem " Create a new code"
txtMessage.Text = ""
OrgCode = ""
End Sub

Private Sub lstRefType_Click()
Dim ApplTbl As ApplicationTables
    
txtAltMsg.ForeColor = txtMessage.ForeColor
txtAltMsg.BackColor = txtMessage.BackColor

txtFilter.Visible = False
cmdSearch.Visible = False
If (lstRefType.ListIndex > -1) Then
    Set ApplTbl = New ApplicationTables
    Set ApplTbl.lstBox = lstRefCode
    Select Case lstRefType.List(lstRefType.ListIndex)
    Case "ImpressionLinkDiscussion", "ImpressionLinkPlanNotes"
        txtFilter.Visible = True
        cmdSearch.Visible = True
        FilteredLstRefCode
    Case Else
        Call ApplTbl.ApplLoadCodes(lstRefType.List(lstRefType.ListIndex), True, True, True)
    End Select
    txtMessage.Text = ""
    lblAltMsg.Visible = False
    txtAltMsg.Text = ""
    txtAltMsg.Visible = False
    lblRank.Visible = False
    txtRank.Text = ""
    txtRank.Visible = False
    lblLtr.Visible = False
    txtLtr.Text = ""
    txtLtr.Visible = False
    lblLtr1.Visible = False
    txtLtr1.Text = ""
    txtLtr1.Visible = False
    lblOrder.Visible = False
    txtOrder.Text = ""
    txtOrder.Visible = False
    lblNote.Visible = False
    txtNote.Visible = False
    txtNote.Text = ""
    cmdDelete.Visible = False
    cmdFormat.Visible = False
    chkExc.Caption = "Exclusion"
    If (Left(lstRefType.List(lstRefType.ListIndex), 10) = "Quantifier") Then
        chkExc.Caption = "Category"
    End If
    chkExc.Visible = False
    chkSig.Caption = "Signature"
    If (Left(lstRefType.List(lstRefType.ListIndex), 10) = "Quantifier") Then
        chkSig.Caption = "Trigger Pad"
    End If
    chkSig.Visible = False
    chkFU.Visible = False
    Set ApplTbl = Nothing
    
    
End If
End Sub

Private Sub FilteredLstRefCode()
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As New PracticeCodes
Dim GoodToGo As Boolean
Dim Fltr As String
Dim Codes() As String
Dim c As Integer
Dim Delim As Integer

Fltr = Trim(txtFilter.Text)
lstRefCode.Clear
lstRefCode.AddItem " Create a new code"
RetrieveCode.ReferenceType = Trim(lstRefType.List(lstRefType.ListIndex))
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        
        GoodToGo = True
        If Fltr = "" Then
        Else
            If RetrieveCode.ReferenceAlternateCode = "" Then
            Else
                c = 0
                ReDim Codes(0) As String
                Codes(0) = RetrieveCode.ReferenceAlternateCode
                Do
                    c = c + 1
                    ReDim Preserve Codes(c)
                    
                    Delim = InStrPS(1, Codes(0), ",")
                    If Delim > 0 Then
                        Codes(c) = (Mid(Codes(0), 1, Delim - 1))
                        Codes(0) = Trim(Mid(Codes(0), Delim + 1))
                    Else
                        Codes(c) = Codes(0)
                        Exit Do
                    End If
                Loop
                
                For c = 1 To UBound(Codes)
                    If Fltr = Mid(Codes(c), 1, Len(Fltr)) Then
                        GoodToGo = True
                        Exit For
                    Else
                        GoodToGo = False
                    End If
                Next
            End If
        End If
        
        If GoodToGo Then
            DisplayText = Space(512)
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceCode))) = Trim(RetrieveCode.ReferenceCode)
            Mid(DisplayText, 65, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
            Mid(DisplayText, 128, Len(Trim(RetrieveCode.LetterLanguage))) = Trim(RetrieveCode.LetterLanguage)
            Mid(DisplayText, 240, 7) = Trim(Str(RetrieveCode.Rank))
            Mid(DisplayText, 248, 1) = " "
            If (RetrieveCode.FollowUp = "F") Then
                Mid(DisplayText, 248, 1) = "F"
            End If
            Mid(DisplayText, 249, Len(Trim(RetrieveCode.TestOrder))) = Trim(RetrieveCode.TestOrder)
            Mid(DisplayText, 254, 1) = "F"
            If (RetrieveCode.Signature = "T") Then
                Mid(DisplayText, 254, 1) = "T"
            End If
            Mid(DisplayText, 255, 1) = "F"
            If (RetrieveCode.Exclusion = "T") Then
                Mid(DisplayText, 255, 1) = "T"
            End If
            Mid(DisplayText, 256, Len(Trim(RetrieveCode.LetterLanguage1))) = Trim(RetrieveCode.LetterLanguage1)
            Mid(DisplayText, 383, Len(Trim(RetrieveCode.ItemId))) = Trim(RetrieveCode.ItemId)
            lstRefCode.AddItem DisplayText
        End If
        k = k + 1
    Loop
End If
Set RetrieveCode = Nothing

End Sub


Private Sub lstRefType_DblClick()
lstRefType.ListIndex = -1
lstRefCode.Clear
lstRefCode.AddItem " Create a new code"
lblRank.Visible = False
txtRank.Text = ""
txtRank.Visible = False
txtMessage.Text = ""
lblAltMsg.Visible = False
txtAltMsg.Text = ""
txtAltMsg.Visible = False
lblLtr.Visible = False
txtLtr.Text = ""
txtLtr.Visible = False
lblLtr1.Visible = False
txtLtr1.Text = ""
txtLtr1.Visible = False
lblOrder.Visible = False
txtOrder.Text = ""
txtOrder.Visible = False
cmdFormat.Visible = False
chkFU.Visible = False
chkExc.Caption = "Exclusion"
If (Left(lstRefType.List(lstRefType.ListIndex), 10) = "Quantifier") Then
    chkExc.Caption = "Category"
End If
chkExc.Visible = False
chkSig.Caption = "Signature"
If (Left(lstRefType.List(lstRefType.ListIndex), 10) = "Quantifier") Then
    chkSig.Caption = "Trigger Pad"
End If
chkSig.Visible = False
End Sub

Private Sub lstRefCode_Click()
Dim FileNum As Integer
Dim ItemId As Long
Dim Temp As String, Ref As String, Code As String
Dim TempText As String
Dim ApplTbl As ApplicationTables
lblOrder.Visible = False
txtOrder.Visible = False
If (lstRefCode.ListIndex = 0) _
And (Mid(lstRefCode.List(0), 1, 1) = " ") Then
    txtMessage.Text = ""
    lblAltMsg.Visible = True
    txtAltMsg.Text = ""
    txtNote.Text = ""
    txtOrder.Text = ""
    txtAltMsg.Visible = True
    lblRank.Visible = True
    txtRank.Text = ""
    txtRank.Visible = True
    cmdDelete.Visible = False
    If Not (CodesOn) Then
        If (lstRefType.List(lstRefType.ListIndex) = "Chart") Or _
           (lstRefType.List(lstRefType.ListIndex) = "Private") Or _
           (lstRefType.List(lstRefType.ListIndex) = "BillingNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ImpressionNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ImpressionLinkPlanNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ImpressionLinkDiscussion") Or _
           (lstRefType.List(lstRefType.ListIndex) = "DemographicsNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "TestNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "DelinquentBillingNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "RxNotes") Then
            lblNote.Visible = True
            txtNote.Visible = True
        ElseIf (lstRefType.List(lstRefType.ListIndex) = "Action") Or _
               (lstRefType.List(lstRefType.ListIndex) = "Test") Then
            lblLtr.Visible = True
            txtLtr.Text = ""
            txtLtr.Visible = True
            chkExc.Value = 0
            chkExc.Visible = True
        ElseIf (InStrPS(UCase(lstRefType.List(lstRefType.ListIndex)), "LETTERS") <> 0) Or _
               (InStrPS(UCase(lstRefType.List(lstRefType.ListIndex)), "ADMISSION") <> 0) Then
            cmdFormat.Visible = True
            chkSig.Value = 0
            chkSig.Visible = True
        End If
        If (lstRefType.List(lstRefType.ListIndex) = "External Tests") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ExternalTestLinks") Or _
           (lstRefType.List(lstRefType.ListIndex) = "Office Procedures") Or _
           (lstRefType.List(lstRefType.ListIndex) = "SurgeryType") Then
            lblOrder.Visible = True
            txtOrder.Visible = True
            chkFU.Value = 0
            chkFU.Visible = True
        End If
    End If
    If (InStrPS(UCase(lstRefType.List(lstRefType.ListIndex)), "QUANTIFIERS") <> 0) Then
        cmdFormat.Visible = False
        chkSig.Value = 0
        chkSig.Visible = True
        chkExc.Value = 0
        chkExc.Visible = True
    End If
Else
    lblNote.Visible = False
    txtNote.Visible = False
    txtNote.Text = ""
    cmdDelete.Visible = True
    lblRank.Visible = True
    txtRank.Text = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 240, 8))
    txtRank.Visible = True
    txtMessage.Text = Trim(Left(lstRefCode.List(lstRefCode.ListIndex), 64))
    lblAltMsg.Visible = True
    txtAltMsg.Text = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 65, 63))
    txtAltMsg.Visible = True
    lblLtr.Visible = True
    txtLtr.Text = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 128, 112))
    txtLtr.Visible = True
    lblLtr1.Visible = True
    txtLtr1.Text = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 256, 127))
    txtLtr1.Visible = True
    lblOrder.Visible = True
    txtOrder.Text = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 249, 5))
    txtOrder.Visible = True
    chkExc.Value = 0
    If (Mid(lstRefCode.List(lstRefCode.ListIndex), 255, 1) = "T") Then
        chkExc.Value = 1
    End If
    chkExc.Visible = True
    chkSig.Value = 0
    If (Mid(lstRefCode.List(lstRefCode.ListIndex), 254, 1) = "T") Then
        chkSig.Value = 1
    End If
    chkSig.Visible = True
    chkFU.Value = 0
    If (Mid(lstRefCode.List(lstRefCode.ListIndex), 248, 1) = "F") Then
        chkFU.Value = 1
    End If
    chkExc.Visible = True
    Set ApplTbl = New ApplicationTables
    Ref = Trim(Left(lstRefType.List(lstRefType.ListIndex), 64))
    Code = Trim(Left(lstRefCode.List(lstRefCode.ListIndex), 64))
    ItemId = Trim(Mid(lstRefCode.List(lstRefCode.ListIndex), 383))
    Call ApplTbl.ApplGetCodeFormat(Ref, Code, TheCodeFormat, ItemId)
    Set ApplTbl = Nothing
    If Not (CodesOn) Then
        If (lstRefType.List(lstRefType.ListIndex) = "Chart") Or _
           (lstRefType.List(lstRefType.ListIndex) = "Private") Or _
           (lstRefType.List(lstRefType.ListIndex) = "BillingNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ImpressionNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ImpressionLinkPlanNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "ImpressionLinkDiscussion") Or _
           (lstRefType.List(lstRefType.ListIndex) = "DemographicsNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "TestNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "DelinquentBillingNotes") Or _
           (lstRefType.List(lstRefType.ListIndex) = "RxNotes") Then
            Temp = ImpressionNotesDirectory + Trim(txtMessage.Text) + ".txt"
            If (FM.IsFileThere(Temp)) Then
                FileNum = FreeFile
                FM.OpenFile Temp, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
                While Not (EOF(FileNum))
                    Line Input #FileNum, TempText
                    txtNote.Text = Trim(TempText) + Chr(13) + Chr(10) + txtNote.Text
                Wend
                FM.CloseFile CLng(FileNum)
                If (Left(txtNote.Text, 2) = vbCrLf) Then
                    Temp = txtNote.Text
                    Mid(Temp, 1, 2) = "  "
                    txtNote.Text = Trim(Temp)
                End If
            Else
                txtNote.Text = ""
            End If
            lblNote.Visible = True
            txtNote.Visible = True
        ElseIf (InStrPS(UCase(lstRefType.List(lstRefType.ListIndex)), "LETTERS") <> 0) Or _
               (InStrPS(UCase(lstRefType.List(lstRefType.ListIndex)), "ADMISSION") <> 0) Then
            cmdFormat.Visible = True
        End If
    End If
    If (lstRefType.List(lstRefType.ListIndex) = "External Tests") Or _
       (lstRefType.List(lstRefType.ListIndex) = "ExternalTestLinks") Or _
       (lstRefType.List(lstRefType.ListIndex) = "OfficeProcedures") Or _
       (lstRefType.List(lstRefType.ListIndex) = "SurgeryType") Then
        lblOrder.Visible = True
        txtOrder.Visible = True
        chkFU.Visible = True
    Else
        lblOrder.Visible = False
        txtOrder.Visible = False
        chkFU.Visible = True
    End If
End If
OrgCode = Trim(txtMessage.Text)

If lstRefType.List(lstRefType.ListIndex) = "AccessAds" Then
    lblLtr.Visible = True
    txtLtr.Visible = True
    lblRank.Visible = True
    txtRank.Visible = True
Else
    lblAltMsg.Caption = "Enter the Alternate Reference Code"
End If
If (lstRefType.List(lstRefType.ListIndex) = "Drug Families") Then
    chkSig.Visible = False
    chkExc.Visible = False
    chkFU.Visible = False
    lblLtr.Visible = False
    txtLtr.Visible = False
    lblLtr1.Visible = False
    txtLtr1.Visible = False
    cmdDelete.Visible = False
End If


txtMessage.SetFocus
SendKeys "{Home}"
End Sub

Private Sub lstRefCode_DblClick()
lstRefCode.ListIndex = -1
txtMessage.Text = ""
lblRank.Visible = False
txtRank.Visible = False
txtRank.Text = ""
lblAltMsg.Visible = False
txtAltMsg.Text = ""
txtAltMsg.Visible = False
lblLtr.Visible = False
txtLtr.Text = ""
txtLtr.Visible = False
lblLtr1.Visible = False
txtLtr1.Text = ""
txtLtr1.Visible = False
lblOrder.Visible = False
txtOrder.Text = ""
txtOrder.Visible = False
chkExc.Value = 0
chkExc.Visible = False
chkSig.Value = 0
chkSig.Visible = False
End Sub


Private Sub txtAltMsg_KeyPress(KeyAscii As Integer)
If (lstRefType.ListIndex > 0) Then
    If (lstRefType.List(lstRefType.ListIndex) = "") Then
        KeyAscii = 0
    End If
End If
End Sub


Private Sub txtFilter_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case vbKeyReturn
    Call cmdSearch_Click
End Select
End Sub


Private Sub txtRank_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) Then
    If (KeyAscii <> 13) And (KeyAscii <> 10) And (KeyAscii <> 8) Then
        frmEventMsgs.Header = "Valid Numeric Characters [0123456789]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
