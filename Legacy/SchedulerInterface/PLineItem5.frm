VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPLineItem5 
   BackColor       =   &H006C6928&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstIns 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   105
      TabIndex        =   76
      Top             =   600
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Frame frPmnt 
      BackColor       =   &H80000009&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   8430
      Index           =   0
      Left            =   120
      TabIndex        =   37
      Top             =   480
      Width           =   11790
      Begin VB.TextBox txtC 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5790
         MaxLength       =   64
         TabIndex        =   38
         Top             =   3240
         Visible         =   0   'False
         Width           =   5805
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H006C6928&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   8535
         Index           =   2
         Left            =   11640
         TabIndex        =   39
         Top             =   0
         Width           =   615
      End
      Begin VB.Frame frPmnt 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   5775
         Index           =   1
         Left            =   60
         TabIndex        =   56
         Top             =   1320
         Width           =   11655
         Begin VB.ListBox lstP 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1740
            Left            =   120
            TabIndex        =   72
            Top             =   240
            Visible         =   0   'False
            Width           =   1935
         End
         Begin VB.Frame frmask 
            BackColor       =   &H80000009&
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   525
            Index           =   1
            Left            =   7875
            TabIndex        =   73
            Top             =   600
            Width           =   1035
            Begin VB.Label Label3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000000&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Session"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   120
               TabIndex        =   74
               Top             =   0
               Visible         =   0   'False
               Width           =   735
            End
         End
         Begin VB.Frame frmask 
            BackColor       =   &H80000009&
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   525
            Index           =   0
            Left            =   2370
            TabIndex        =   57
            Top             =   600
            Width           =   2340
            Begin VB.Label lblWB 
               Alignment       =   2  'Center
               BackColor       =   &H000000FF&
               Caption         =   "Disbursed amount is greater than payment!"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   495
               Left            =   120
               TabIndex        =   58
               Top             =   0
               Visible         =   0   'False
               Width           =   2175
            End
         End
         Begin VB.PictureBox pTxt1 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   3120
            ScaleHeight     =   855
            ScaleWidth      =   1695
            TabIndex        =   65
            Top             =   1680
            Visible         =   0   'False
            Width           =   1695
            Begin VB.CommandButton dd1 
               Caption         =   "q"
               BeginProperty Font 
                  Name            =   "Wingdings"
                  Size            =   6
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   720
               TabIndex        =   66
               Top             =   0
               Width           =   255
            End
            Begin VB.TextBox txt1 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   45
               TabIndex        =   0
               Text            =   "Text1"
               Top             =   15
               Width           =   975
            End
         End
         Begin VB.CommandButton PT 
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   11325
            TabIndex        =   64
            Top             =   960
            Width           =   210
         End
         Begin VB.PictureBox lstPT 
            BackColor       =   &H00C0FFFF&
            Height          =   3375
            Left            =   8040
            ScaleHeight     =   3315
            ScaleWidth      =   3195
            TabIndex        =   60
            Top             =   1200
            Visible         =   0   'False
            Width           =   3255
            Begin VB.VScrollBar VS 
               Height          =   3255
               LargeChange     =   300
               Left            =   2940
               SmallChange     =   15
               TabIndex        =   61
               Top             =   0
               Width           =   255
            End
            Begin VB.Label lblHelp 
               BackColor       =   &H00C0FFFF&
               Caption         =   "Label1"
               Height          =   3015
               Left            =   60
               TabIndex        =   62
               Top             =   0
               Width           =   2655
            End
         End
         Begin VB.Frame FrMaskTemp 
            BackColor       =   &H80000009&
            BorderStyle     =   0  'None
            Caption         =   "FrMaskTemp"
            Height          =   525
            Left            =   5790
            TabIndex        =   59
            Top             =   600
            Width           =   2460
            Begin VB.Label lblWB1 
               Alignment       =   2  'Center
               BackColor       =   &H000000FF&
               Caption         =   " Service Balance is Less than $0!"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   495
               Left            =   120
               TabIndex        =   80
               Top             =   0
               Visible         =   0   'False
               Width           =   1695
            End
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   2085
            Index           =   3
            Left            =   0
            TabIndex        =   63
            Top             =   3240
            Visible         =   0   'False
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   3678
            _Version        =   393216
            Rows            =   1
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   -2147483643
            ForeColorSel    =   -2147483640
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            Enabled         =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   525
            Index           =   0
            Left            =   0
            TabIndex        =   67
            Top             =   0
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   4485
            Index           =   2
            Left            =   0
            TabIndex        =   68
            Top             =   1200
            Width           =   12000
            _ExtentX        =   21167
            _ExtentY        =   7911
            _Version        =   393216
            Rows            =   1
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   -2147483643
            ForeColorSel    =   -2147483640
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            Enabled         =   0   'False
            FocusRect       =   2
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   525
            Index           =   1
            Left            =   0
            TabIndex        =   70
            Top             =   600
            Width           =   7575
            _ExtentX        =   13361
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.PictureBox sf 
            Height          =   255
            Left            =   3120
            ScaleHeight     =   195
            ScaleWidth      =   195
            TabIndex        =   69
            Top             =   600
            Width           =   255
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmntInfo 
            Height          =   525
            Left            =   0
            TabIndex        =   75
            Top             =   0
            Visible         =   0   'False
            Width           =   6405
            _ExtentX        =   11298
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            Enabled         =   0   'False
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H006C6928&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   1215
         Index           =   0
         Left            =   0
         TabIndex        =   54
         Top             =   0
         Width           =   11895
         Begin MSFlexGridLib.MSFlexGrid lstInfo 
            Height          =   975
            Left            =   0
            TabIndex        =   55
            Top             =   120
            Width           =   11655
            _ExtentX        =   20558
            _ExtentY        =   1720
            _Version        =   393216
            Rows            =   1
            Cols            =   4
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   7104808
            GridColor       =   8388608
            AllowBigSelection=   0   'False
            ScrollTrack     =   -1  'True
            Enabled         =   0   'False
            FocusRect       =   2
            HighLight       =   0
            ScrollBars      =   0
            AllowUserResizing=   1
            BorderStyle     =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H006C6928&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   1350
         Index           =   1
         Left            =   0
         TabIndex        =   40
         Top             =   7080
         Width           =   11895
         Begin VB.OptionButton opt 
            BackColor       =   &H006C6928&
            Caption         =   "Pat. Open"
            ForeColor       =   &H8000000E&
            Height          =   255
            Index           =   0
            Left            =   3720
            TabIndex        =   79
            Top             =   120
            Value           =   -1  'True
            Width           =   1095
         End
         Begin VB.OptionButton opt 
            BackColor       =   &H006C6928&
            Caption         =   "All Open"
            ForeColor       =   &H8000000E&
            Height          =   255
            Index           =   1
            Left            =   3720
            TabIndex        =   78
            Top             =   360
            Width           =   1095
         End
         Begin VB.OptionButton opt 
            BackColor       =   &H006C6928&
            Caption         =   "All"
            ForeColor       =   &H8000000E&
            Height          =   255
            Index           =   2
            Left            =   3720
            TabIndex        =   77
            Top             =   600
            Width           =   1095
         End
         Begin VB.Frame frS 
            BackColor       =   &H006C6928&
            BorderStyle     =   0  'None
            Caption         =   "Frame2"
            Height          =   975
            Left            =   7680
            TabIndex        =   41
            Top             =   120
            Width           =   1575
            Begin fpBtnAtlLibCtl.fpBtn fpBtnS 
               Height          =   495
               Index           =   0
               Left            =   0
               TabIndex        =   42
               Top             =   0
               Width           =   735
               _Version        =   131072
               _ExtentX        =   1296
               _ExtentY        =   873
               Enabled         =   -1  'True
               MousePointer    =   0
               Object.TabStop         =   -1  'True
               GrayAreaColor   =   11098385
               BorderShowDefault=   -1  'True
               ButtonType      =   0
               NoPointerFocus  =   0   'False
               Value           =   0   'False
               GroupID         =   0
               GroupSelect     =   0
               DrawFocusRect   =   2
               DrawFocusRectCell=   -1
               GrayAreaPictureStyle=   0
               Static          =   0   'False
               BackStyle       =   1
               AutoSize        =   0
               AutoSizeOffsetTop=   0
               AutoSizeOffsetBottom=   0
               AutoSizeOffsetLeft=   0
               AutoSizeOffsetRight=   0
               DropShadowOffsetX=   3
               DropShadowOffsetY=   3
               DropShadowType  =   0
               DropShadowColor =   0
               Redraw          =   -1  'True
               ButtonDesigner  =   "PLineItem5.frx":0000
            End
            Begin fpBtnAtlLibCtl.fpBtn fpBtnS 
               Height          =   495
               Index           =   1
               Left            =   0
               TabIndex        =   43
               Top             =   480
               Width           =   735
               _Version        =   131072
               _ExtentX        =   1296
               _ExtentY        =   873
               Enabled         =   -1  'True
               MousePointer    =   0
               Object.TabStop         =   -1  'True
               GrayAreaColor   =   11098385
               BorderShowDefault=   -1  'True
               ButtonType      =   0
               NoPointerFocus  =   0   'False
               Value           =   0   'False
               GroupID         =   0
               GroupSelect     =   0
               DrawFocusRect   =   2
               DrawFocusRectCell=   -1
               GrayAreaPictureStyle=   0
               Static          =   0   'False
               BackStyle       =   1
               AutoSize        =   0
               AutoSizeOffsetTop=   0
               AutoSizeOffsetBottom=   0
               AutoSizeOffsetLeft=   0
               AutoSizeOffsetRight=   0
               DropShadowOffsetX=   3
               DropShadowOffsetY=   3
               DropShadowType  =   0
               DropShadowColor =   0
               Redraw          =   -1  'True
               ButtonDesigner  =   "PLineItem5.frx":01DC
            End
            Begin fpBtnAtlLibCtl.fpBtn fpBtnS 
               Height          =   495
               Index           =   2
               Left            =   840
               TabIndex        =   44
               Top             =   480
               Width           =   735
               _Version        =   131072
               _ExtentX        =   1296
               _ExtentY        =   873
               Enabled         =   -1  'True
               MousePointer    =   0
               Object.TabStop         =   -1  'True
               GrayAreaColor   =   11098385
               BorderShowDefault=   -1  'True
               ButtonType      =   0
               NoPointerFocus  =   0   'False
               Value           =   0   'False
               GroupID         =   0
               GroupSelect     =   0
               DrawFocusRect   =   2
               DrawFocusRectCell=   -1
               GrayAreaPictureStyle=   0
               Static          =   0   'False
               BackStyle       =   1
               AutoSize        =   0
               AutoSizeOffsetTop=   0
               AutoSizeOffsetBottom=   0
               AutoSizeOffsetLeft=   0
               AutoSizeOffsetRight=   0
               DropShadowOffsetX=   3
               DropShadowOffsetY=   3
               DropShadowType  =   0
               DropShadowColor =   0
               Redraw          =   -1  'True
               ButtonDesigner  =   "PLineItem5.frx":03B8
            End
            Begin fpBtnAtlLibCtl.fpBtn fpBtnS 
               Height          =   495
               Index           =   3
               Left            =   840
               TabIndex        =   45
               Top             =   0
               Width           =   735
               _Version        =   131072
               _ExtentX        =   1296
               _ExtentY        =   873
               Enabled         =   -1  'True
               MousePointer    =   0
               Object.TabStop         =   -1  'True
               GrayAreaColor   =   11098385
               BorderShowDefault=   -1  'True
               ButtonType      =   0
               NoPointerFocus  =   0   'False
               Value           =   0   'False
               GroupID         =   0
               GroupSelect     =   0
               DrawFocusRect   =   2
               DrawFocusRectCell=   -1
               GrayAreaPictureStyle=   0
               Static          =   0   'False
               BackStyle       =   1
               AutoSize        =   0
               AutoSizeOffsetTop=   0
               AutoSizeOffsetBottom=   0
               AutoSizeOffsetLeft=   0
               AutoSizeOffsetRight=   0
               DropShadowOffsetX=   3
               DropShadowOffsetY=   3
               DropShadowType  =   0
               DropShadowColor =   0
               Redraw          =   -1  'True
               ButtonDesigner  =   "PLineItem5.frx":0594
            End
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   0
            Left            =   0
            TabIndex        =   46
            Top             =   120
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":0770
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   1
            Left            =   10560
            TabIndex        =   47
            Top             =   120
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":094F
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn1 
            Height          =   990
            Left            =   4920
            TabIndex        =   48
            Top             =   120
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":0B2E
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn2 
            Height          =   990
            Left            =   6120
            TabIndex        =   49
            Top             =   120
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1746
            Enabled         =   0   'False
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":0D16
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn4 
            Height          =   990
            Left            =   1200
            TabIndex        =   50
            Top             =   120
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1746
            Enabled         =   0   'False
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":0EFF
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn5 
            Height          =   990
            Left            =   2520
            TabIndex        =   51
            Top             =   120
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":10EB
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtnAF 
            Height          =   990
            Left            =   9480
            TabIndex        =   52
            Top             =   120
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":12DA
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn3 
            Height          =   990
            Left            =   4920
            TabIndex        =   53
            Top             =   120
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PLineItem5.frx":14BE
         End
      End
      Begin VB.PictureBox picMask 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   8070
         ScaleHeight     =   225
         ScaleWidth      =   1410
         TabIndex        =   71
         Top             =   1530
         Width           =   1410
      End
   End
   Begin VB.ComboBox lstMethod 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   6840
      Style           =   2  'Dropdown List
      TabIndex        =   36
      Top             =   960
      Width           =   2415
   End
   Begin VB.ListBox lstDisb 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4860
      Left            =   1800
      TabIndex        =   5
      Top             =   2280
      Visible         =   0   'False
      Width           =   9015
   End
   Begin VB.TextBox txtPatient 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   64
      TabIndex        =   1
      Top             =   480
      Width           =   3975
   End
   Begin VB.TextBox txtAmount 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6840
      MaxLength       =   10
      TabIndex        =   3
      Top             =   480
      Width           =   2055
   End
   Begin VB.TextBox txtCheck 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   20
      TabIndex        =   2
      Top             =   960
      Width           =   3975
   End
   Begin VB.TextBox txtDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9720
      MaxLength       =   10
      TabIndex        =   4
      Top             =   960
      Width           =   2055
   End
   Begin VB.TextBox txtPayAmt 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   10
      TabIndex        =   12
      Top             =   1440
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.TextBox txtAdj 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6120
      MaxLength       =   64
      TabIndex        =   11
      Top             =   1440
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.TextBox txtAdjAmt 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   10
      TabIndex        =   10
      Top             =   2400
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CheckBox chkInclude 
      BackColor       =   &H0077742D&
      Caption         =   "Include comment on statements"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5040
      TabIndex        =   8
      Top             =   1800
      Visible         =   0   'False
      Width           =   3975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdVIns 
      Height          =   990
      Left            =   9240
      TabIndex        =   6
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":16A6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFinData 
      Height          =   990
      Left            =   3960
      TabIndex        =   7
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":188E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdComment 
      Height          =   990
      Left            =   9240
      TabIndex        =   9
      Top             =   7920
      Visible         =   0   'False
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":1A77
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   4215
      Left            =   120
      TabIndex        =   13
      Top             =   2880
      Visible         =   0   'False
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   7435
      _Version        =   393216
      Cols            =   11
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   14
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":1C5D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10560
      TabIndex        =   15
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":1E3C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisburse 
      Height          =   990
      Left            =   5280
      TabIndex        =   16
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":201B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   990
      Left            =   2640
      TabIndex        =   17
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":21FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdView 
      Height          =   990
      Left            =   7920
      TabIndex        =   18
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":23E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   990
      Left            =   6600
      TabIndex        =   19
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":25D2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChkReset 
      Height          =   990
      Left            =   1440
      TabIndex        =   20
      Top             =   7920
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PLineItem5.frx":27BF
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid2 
      Height          =   3855
      Left            =   7440
      TabIndex        =   21
      Top             =   3000
      Visible         =   0   'False
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   6800
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   11098385
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Method"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   5760
      TabIndex        =   35
      Top             =   960
      Width           =   945
   End
   Begin VB.Label lblPatient 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   34
      Top             =   480
      Width           =   945
   End
   Begin VB.Label lblAmount 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5760
      TabIndex        =   33
      Top             =   480
      Width           =   945
   End
   Begin VB.Label lblCheck 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Check #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   32
      Top             =   960
      Width           =   945
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Open Item Payments"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   31
      Top             =   120
      Width           =   2685
   End
   Begin VB.Label lblAmountLeft 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Amount Remaining to disburse"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   30
      Top             =   1920
      Visible         =   0   'False
      Width           =   3510
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Payment Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9720
      TabIndex        =   29
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label lblAdj 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Comment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5010
      TabIndex        =   28
      Top             =   1440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblTPay 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Paid"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   7440
      TabIndex        =   27
      Top             =   7200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblTPaid 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   7440
      TabIndex        =   26
      Top             =   7560
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label lblTAdj 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Adjustments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   9240
      TabIndex        =   25
      Top             =   7200
      Visible         =   0   'False
      Width           =   2085
   End
   Begin VB.Label lblTAdjs 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   9240
      TabIndex        =   24
      Top             =   7560
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label lblPrint 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading ..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   10560
      TabIndex        =   23
      Top             =   1920
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblInst 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Use Arrow Keys to move from cell to cell"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   6480
      TabIndex        =   22
      Top             =   2280
      Visible         =   0   'False
      Width           =   5325
   End
End
Attribute VB_Name = "frmPLineItem5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public BatchOn As Boolean
Public PatientId As Long
Public InsurerId As Long

Private RightArrowOn As Boolean
Private PreviousCol As Integer
Private PreviousRow As Integer
Private MedicareOn As Boolean
Private TransitionInsurerOn As Boolean
Private UserId As Long
Private GetCheckOn As Boolean
Private FirstTimeSet As Boolean
Private PaymentMethod As String
Private AdjustmentCode As String
Private AllOn As Boolean
Private CheckId As Long
Private PatPol1 As Long
Private PatPol2 As Long
Private PatPolId As Long
Private CurrentRow As Integer
Private CurrentCol As Integer
Private RunningCheckBalance As String

Dim bySys As Boolean
Dim mCheckExist As Long

Dim CheckAmt As Single
Dim PaidCheckAmt  As Single
Dim AdjCheckAmt As Single
Dim RemCheckAmt As Single

Dim mKeyCode As Integer
Dim OptView As Integer

Private Sub ShowOpen()
Dim r As Integer
Dim h As Integer
Dim vis As Boolean
With lstPmnt(2)
    For r = 1 To .Rows - 1
        h = .RowHeight(0)
        If Not AllOn Then
            If lstPmnt(3).TextMatrix(r, 7) = "T" Then
                h = 0
            End If
        End If
        .RowHeight(r) = h
        If h > 0 Then vis = True
    Next
    .Enabled = vis
End With

End Sub

Private Sub cmdChkReset_Click()
Dim Temp As String
Dim ADate As String
Dim TheResult As Single
Dim TheAmt As Single
Dim ApplTemp As ApplicationTemplates
If (InsurerId > 0) And (Trim(txtCheck.Text) <> "") And (Trim(txtDate.Text) <> "") Then
    TheAmt = 0
    ADate = Trim(txtDate.Text)
    ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
    Set ApplTemp = New ApplicationTemplates
    TheAmt = ApplTemp.ApplGetPaymentsAgainstCheck(InsurerId, Trim(txtCheck.Text), ADate)
    Set ApplTemp = Nothing
    TheResult = val(Trim(txtAmount.Text)) - TheAmt
    Call DisplayDollarAmount(Trim(Str(TheResult)), RunningCheckBalance)
    RunningCheckBalance = Trim(RunningCheckBalance)
    Call MoneyLeft(0)
    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
    lblAmountLeft.Tag = Trim(RunningCheckBalance)
    lblAmountLeft.Visible = True
End If
End Sub

Private Sub cmdFinData_Click()
Dim AInsId As Long
Dim RcvId As Long
Dim TheDate As String, InvDt As String
Dim ApplTemp As ApplicationTemplates
Dim PatientDemographics As PatientDemographics
If (PatientId > 0) Then
    If (CurrentRow > 0) And (MSFlexGrid1.Visible) Then
        RcvId = 0
        Set ApplTemp = New ApplicationTemplates
        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 1)) <> "") Then
            RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 1)), AInsId, InvDt)
        End If
''''''''        frmPayments.AccessType = "FD"
''''''''        frmPayments.Whom = True
''''''''        frmPayments.CurrentAction = "BP"
''''''''        frmPayments.PatientId = PatientId
''''''''        frmPayments.ReceivableId = RcvID
''''''''        If (frmPayments.LoadPayments(True, "")) Then
''''''''            frmPayments.Show 1
''''''''            If (frmPayments.CurrentAction <> "Home") And (frmPayments.CurrentAction <> "BP") Then
''''''''                Call LoadPayments(PatientId, InsurerId, AllOn, True, False)
''''''''            End If
''''''''        Else
''''''''            frmEventMsgs.Header = "No Financial Data"
''''''''            frmEventMsgs.AcceptText = ""
''''''''            frmEventMsgs.RejectText = "Ok"
''''''''            frmEventMsgs.CancelText = ""
''''''''            frmEventMsgs.Other0Text = ""
''''''''            frmEventMsgs.Other1Text = ""
''''''''            frmEventMsgs.Other2Text = ""
''''''''            frmEventMsgs.Other3Text = ""
''''''''            frmEventMsgs.Other4Text = ""
''''''''            frmEventMsgs.Show 1
''''''''        End If
        Set PatientDemographics = New PatientDemographics
        PatientDemographics.PatientId = PatientId
        Call PatientDemographics.DisplayPatientFinancialScreen
        Set PatientDemographics = Nothing
        Call LoadPayments(PatientId, InsurerId, AllOn, True, False)
    Else
'''''''        frmPayments.AccessType = "FD"
'''''''        frmPayments.Whom = True
'''''''        frmPayments.CurrentAction = "BP"
'''''''        frmPayments.PatientId = PatientId
'''''''        frmPayments.ReceivableId = 0
'''''''        If (frmPayments.LoadPayments(True, "")) Then
'''''''            frmPayments.Show 1
'''''''            If (frmPayments.CurrentAction <> "Home") And (frmPayments.CurrentAction <> "BP") Then
'''''''                Call LoadPayments(PatientId, InsurerId, AllOn, True, False)
'''''''            End If
'''''''        Else
'''''''            frmEventMsgs.Header = "No Financial Data"
'''''''            frmEventMsgs.AcceptText = ""
'''''''            frmEventMsgs.RejectText = "Ok"
'''''''            frmEventMsgs.CancelText = ""
'''''''            frmEventMsgs.Other0Text = ""
'''''''            frmEventMsgs.Other1Text = ""
'''''''            frmEventMsgs.Other2Text = ""
'''''''            frmEventMsgs.Other3Text = ""
'''''''            frmEventMsgs.Other4Text = ""
'''''''            frmEventMsgs.Show 1
'''''''        End If
        Set PatientDemographics = New PatientDemographics
        PatientDemographics.PatientId = PatientId
        Call PatientDemographics.DisplayPatientFinancialScreen
        Set PatientDemographics = Nothing
        Call LoadPayments(PatientId, InsurerId, AllOn, True, False)
        End If
Else
    frmEventMsgs.Header = "Select a patient"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdHome_Click()
MyPracticeRepositoryCmd.CommandType = adCmdText
Unload frmLineItem
End Sub

Private Sub cmdApply_Click()
If (Trim(txtCheck.Text) <> "") Then
    Call txtPatient_Click
Else
    MyPracticeRepositoryCmd.CommandType = adCmdText
    Unload frmLineItem
End If
End Sub

Private Sub cmdAll_Click()
If (PatientId > 0) And (InsurerId > 0) Then
    AllOn = Not (AllOn)
    If (AllOn) Then
        cmdAll.Text = "Show Only Open"
    Else
        cmdAll.Text = "Show All Items"
    End If
    Call LoadPayments(PatientId, InsurerId, AllOn, True, False)
    lblInst.Visible = True
    MSFlexGrid1.Visible = True
    MSFlexGrid1.SetFocus
    MSFlexGrid1.RowSel = 1
    CurrentRow = 1
End If
End Sub

Private Sub cmdComment_Click()
If (PatientId > 0) And (InsurerId > 0) And (CurrentRow > 0) Then
    If Not (lblAdj.Visible) Then
        lblAdj.Visible = True
        txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
        txtAdj.Visible = True
        chkInclude.Visible = True
        If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
            chkInclude.Value = 1
        Else
            chkInclude.Value = 0
        End If
        txtAdj.SetFocus
    Else
        lblAdj.Visible = False
        txtAdj.Visible = False
        chkInclude.Visible = False
    End If
End If
End Sub

Private Sub cmdDisburse_Click()
''''''''''Dim IResult As Integer
''''''''''Dim RcvId As Long
''''''''''Dim TempId As Long
''''''''''Dim TheDate As String
''''''''''Dim ApplTemp As ApplicationTemplates
''''''''''Dim InsName(6) As String
''''''''''InsName(1) = ""
''''''''''InsName(2) = ""
''''''''''InsName(3) = ""
''''''''''InsName(4) = ""
''''''''''InsName(5) = ""
''''''''''InsName(6) = ""
''''''''''If Not (MSFlexGrid1.Visible) Then
''''''''''    lblPrint.Caption = "Loading..."
''''''''''    lblPrint.Visible = True
''''''''''    If (PatInsId <> InsurerId) And (PatInsId > 0) Then
''''''''''        If (TransitionInsurerOn) Then
''''''''''            TempId = PatInsId
''''''''''        Else
''''''''''            TempId = InsurerId
''''''''''' this means use the current insurer as the paying insurer
''''''''''            PatInsId = InsurerId
''''''''''        End If
''''''''''    Else
''''''''''        TempId = InsurerId
''''''''''    End If
''''''''''    IResult = LoadPayments(PatientId, TempId, AllOn, True, False)
''''''''''    If (IResult = 0) Then
''''''''''        cmdComment.Visible = True
''''''''''        cmdView.Visible = True
''''''''''        cmdAll.Visible = True
''''''''''        cmdChkReset.Visible = True
''''''''''        txtAmount.Locked = True
''''''''''        txtCheck.Locked = True
''''''''''        lblTPay.Visible = True
''''''''''        lblTPaid.Visible = True
''''''''''        lblTAdj.Visible = True
''''''''''        lblTAdjs.Visible = True
''''''''''        lblInst.Visible = True
''''''''''        lblRsn.Visible = False
''''''''''        MSFlexGrid1.Visible = True
''''''''''        MSFlexGrid1.SetFocus
''''''''''        MSFlexGrid1.RowSel = 1
''''''''''        MSFlexGrid1.ColSel = 8
''''''''''        CurrentRow = 1
''''''''''        cmdDisburse.SetFocus
''''''''''        RightArrowOn = False
''''''''''    ElseIf (IResult = -2) Then
''''''''''        frmEventMsgs.Header = "Patient does not have this payer, Override ?"
''''''''''        frmEventMsgs.AcceptText = ""
''''''''''        frmEventMsgs.RejectText = "Ok"
''''''''''        frmEventMsgs.CancelText = "No"
''''''''''        frmEventMsgs.Other0Text = ""
''''''''''        frmEventMsgs.Other1Text = ""
''''''''''        frmEventMsgs.Other2Text = ""
''''''''''        frmEventMsgs.Other3Text = ""
''''''''''        frmEventMsgs.Other4Text = ""
''''''''''        frmEventMsgs.Show 1
''''''''''        If (frmEventMsgs.Result = 2) Then
''''''''''            Set ApplTemp = New ApplicationTemplates
''''''''''            Call ApplTemp.ApplGetBatchPayers(PatientId, InsName(1), InsName(2), InsName(3), InsName(4), InsName(5), InsName(6))
''''''''''            Set ApplTemp = Nothing
''''''''''            frmEventMsgs.Header = "Override with which Patient Insurer ?"
''''''''''            frmEventMsgs.AcceptText = "Cancel"
''''''''''            frmEventMsgs.RejectText = Trim(Left(InsName(1), 30))
''''''''''            frmEventMsgs.CancelText = Trim(Left(InsName(2), 30))
''''''''''            frmEventMsgs.Other0Text = Trim(Left(InsName(3), 30))
''''''''''            frmEventMsgs.Other1Text = Trim(Left(InsName(4), 30))
''''''''''            frmEventMsgs.Other2Text = Trim(Left(InsName(5), 30))
''''''''''            frmEventMsgs.Other3Text = Trim(Left(InsName(6), 30))
''''''''''            frmEventMsgs.Other4Text = ""
''''''''''            frmEventMsgs.Show 1
''''''''''            If (frmEventMsgs.Result = 1) Then
''''''''''                RightArrowOn = False
''''''''''                InsurerId = 0
''''''''''                txtInsurer.Text = ""
''''''''''                txtInsurer.SetFocus
''''''''''            ElseIf (frmEventMsgs.Result = 2) Then
''''''''''                PatInsId = Val(Mid(InsName(1), 56, Len(InsName(1)) - 55))
''''''''''                InsurerId = PatInsId
''''''''''                txtAmount.Locked = False
''''''''''                txtCheck.Locked = False
''''''''''                cmdDisburse.Text = "Disburse"
''''''''''                cmdDisburse.SetFocus
''''''''''                RightArrowOn = False
''''''''''            ElseIf (frmEventMsgs.Result = 3) Then
''''''''''                PatInsId = Val(Mid(InsName(3), 56, Len(InsName(3)) - 55))
''''''''''                InsurerId = PatInsId
''''''''''                txtAmount.Locked = False
''''''''''                txtCheck.Locked = False
''''''''''                cmdDisburse.Text = "Disburse"
''''''''''                cmdDisburse.SetFocus
''''''''''                RightArrowOn = False
''''''''''            ElseIf (frmEventMsgs.Result = 4) Then
''''''''''                PatInsId = Val(Mid(InsName(2), 56, Len(InsName(2)) - 55))
''''''''''                InsurerId = PatInsId
''''''''''                txtAmount.Locked = False
''''''''''                txtCheck.Locked = False
''''''''''                cmdDisburse.Text = "Disburse"
''''''''''                cmdDisburse.SetFocus
''''''''''                RightArrowOn = False
''''''''''            ElseIf (frmEventMsgs.Result = 5) Then
''''''''''                PatInsId = Val(Mid(InsName(4), 56, Len(InsName(4)) - 55))
''''''''''                InsurerId = PatInsId
''''''''''                txtAmount.Locked = False
''''''''''                txtCheck.Locked = False
''''''''''                cmdDisburse.Text = "Disburse"
''''''''''                cmdDisburse.SetFocus
''''''''''                RightArrowOn = False
''''''''''            ElseIf (frmEventMsgs.Result = 6) Then
''''''''''                PatInsId = Val(Mid(InsName(5), 56, Len(InsName(5)) - 55))
''''''''''                InsurerId = PatInsId
''''''''''                txtAmount.Locked = False
''''''''''                txtCheck.Locked = False
''''''''''                cmdDisburse.Text = "Disburse"
''''''''''                cmdDisburse.SetFocus
''''''''''                RightArrowOn = False
''''''''''            ElseIf (frmEventMsgs.Result = 7) Then
''''''''''                PatInsId = Val(Mid(InsName(6), 56, Len(InsName(6)) - 55))
''''''''''                InsurerId = PatInsId
''''''''''                txtAmount.Locked = False
''''''''''                txtCheck.Locked = False
''''''''''                cmdDisburse.Text = "Disburse"
''''''''''                cmdDisburse.SetFocus
''''''''''                RightArrowOn = False
''''''''''            End If
''''''''''        End If
''''''''''    ElseIf (IResult = -1) Then
''''''''''        frmEventMsgs.Header = "Patient has no open items."
''''''''''        frmEventMsgs.AcceptText = ""
''''''''''        frmEventMsgs.RejectText = "Ok"
''''''''''        frmEventMsgs.CancelText = ""
''''''''''        frmEventMsgs.Other0Text = ""
''''''''''        frmEventMsgs.Other1Text = ""
''''''''''        frmEventMsgs.Other2Text = ""
''''''''''        frmEventMsgs.Other3Text = ""
''''''''''        frmEventMsgs.Other4Text = ""
''''''''''        frmEventMsgs.Show 1
''''''''''        cmdAll.Visible = True
''''''''''    ElseIf (IResult = -3) Then
''''''''''        frmEventMsgs.Header = "Patient has unassigned payments."
''''''''''        frmEventMsgs.AcceptText = "By Pass"
''''''''''        frmEventMsgs.RejectText = "Financial"
''''''''''        frmEventMsgs.CancelText = "Cancel"
''''''''''        frmEventMsgs.Other0Text = "Fix"
''''''''''        frmEventMsgs.Other1Text = ""
''''''''''        frmEventMsgs.Other2Text = ""
''''''''''        frmEventMsgs.Other3Text = ""
''''''''''        frmEventMsgs.Other4Text = ""
''''''''''        frmEventMsgs.Show 1
''''''''''        If (frmEventMsgs.Result = 1) Then
''''''''''' Already Loaded just display as is
''''''''''            cmdComment.Visible = True
''''''''''            cmdView.Visible = True
''''''''''            cmdAll.Visible = True
''''''''''            cmdChkReset.Visible = True
''''''''''            txtAmount.Locked = True
''''''''''            txtCheck.Locked = True
''''''''''            lblTPay.Visible = True
''''''''''            lblTPaid.Visible = True
''''''''''            lblTAdj.Visible = True
''''''''''            lblTAdjs.Visible = True
''''''''''            lblInst.Visible = True
''''''''''            lblRsn.Visible = False
''''''''''            MSFlexGrid1.Visible = True
''''''''''            MSFlexGrid1.SetFocus
''''''''''            MSFlexGrid1.RowSel = 1
''''''''''            MSFlexGrid1.ColSel = 8
''''''''''            CurrentRow = 1
''''''''''            cmdDisburse.SetFocus
''''''''''            RightArrowOn = False
''''''''''        ElseIf (frmEventMsgs.Result = 2) Then
''''''''''            frmPayments.AccessType = "FD"
''''''''''            frmPayments.Whom = True
''''''''''            frmPayments.CurrentAction = "BP"
''''''''''            frmPayments.PatientId = PatientId
''''''''''            frmPayments.ReceivableId = 0
''''''''''            If (frmPayments.LoadPayments(True, "")) Then
''''''''''                frmPayments.Show 1
''''''''''            End If
''''''''''            frmPaymentsTotal.AccessType = "FD"
''''''''''            frmPaymentsTotal.Whom = True
''''''''''            frmPaymentsTotal.CurrentAction = "BP"
''''''''''            frmPaymentsTotal.PatientId = PatientId
''''''''''            frmPaymentsTotal.ReceivableId = 0
''''''''''            If frmPaymentsTotal.LoadPaymentsTotal("") Then
''''''''''                frmPaymentsTotal.Show 1
''''''''''            End If
''''''''''        ElseIf (frmEventMsgs.Result = 3) Then
''''''''''            If (frmReviewClaim.LoadClaims(PatientId, "U", True)) Then
''''''''''                frmReviewClaim.Show 1
''''''''''            End If
''''''''''        End If
''''''''''        cmdAll.Visible = True
''''''''''    ElseIf (IResult = -4) Then
''''''''''        frmEventMsgs.Header = "Patient has payments linked to deleted services."
''''''''''        frmEventMsgs.AcceptText = "By Pass"
''''''''''        frmEventMsgs.RejectText = "Financial"
''''''''''        frmEventMsgs.CancelText = "Cancel"
''''''''''        frmEventMsgs.Other0Text = "Fix"
''''''''''        frmEventMsgs.Other1Text = ""
''''''''''        frmEventMsgs.Other2Text = ""
''''''''''        frmEventMsgs.Other3Text = ""
''''''''''        frmEventMsgs.Other4Text = ""
''''''''''        frmEventMsgs.Show 1
''''''''''        If (frmEventMsgs.Result = 1) Then
''''''''''' Already Loaded just display as is
''''''''''            cmdComment.Visible = True
''''''''''            cmdView.Visible = True
''''''''''            cmdAll.Visible = True
''''''''''            cmdChkReset.Visible = True
''''''''''            txtAmount.Locked = True
''''''''''            txtCheck.Locked = True
''''''''''            lblTPay.Visible = True
''''''''''            lblTPaid.Visible = True
''''''''''            lblTAdj.Visible = True
''''''''''            lblTAdjs.Visible = True
''''''''''            lblInst.Visible = True
''''''''''            lblRsn.Visible = False
''''''''''            MSFlexGrid1.Visible = True
''''''''''            MSFlexGrid1.SetFocus
''''''''''            MSFlexGrid1.RowSel = 1
''''''''''            MSFlexGrid1.ColSel = 8
''''''''''            CurrentRow = 1
''''''''''            cmdDisburse.SetFocus
''''''''''            RightArrowOn = False
''''''''''        ElseIf (frmEventMsgs.Result = 2) Then
''''''''''            frmPayments.AccessType = "FD"
''''''''''            frmPayments.Whom = True
''''''''''            frmPayments.CurrentAction = "BP"
''''''''''            frmPayments.PatientId = PatientId
''''''''''            frmPayments.ReceivableId = 0
''''''''''            If (frmPayments.LoadPayments(True, "")) Then
''''''''''                frmPayments.Show 1
''''''''''            End If
''''''''''            frmPaymentsTotal.AccessType = "FD"
''''''''''            frmPaymentsTotal.Whom = True
''''''''''            frmPaymentsTotal.CurrentAction = "BP"
''''''''''            frmPaymentsTotal.PatientId = PatientId
''''''''''            frmPaymentsTotal.ReceivableId = 0
''''''''''            If frmPaymentsTotal.LoadPaymentsTotal("") Then
''''''''''                frmPaymentsTotal.Show 1
''''''''''            End If
''''''''''        ElseIf (frmEventMsgs.Result = 3) Then
''''''''''            If (frmReviewClaim.LoadClaims(PatientId, "D", True)) Then
''''''''''                frmReviewClaim.Show 1
''''''''''            End If
''''''''''        End If
''''''''''        cmdAll.Visible = True
''''''''''    End If
''''''''''    lblPrint.Visible = False
''''''''''End If
End Sub

Private Sub cmdLegend_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PAYMENTTYPEVIEWONLY")
frmSelectDialogue.Show 1
End Sub

Private Sub cmdRsn_Click()
''''''''''If (PatientId > 0) And (InsurerId > 0) And (CurrentRow > 0) Then
''''''''''    If Not (lblRsn.Visible) Then
''''''''''        lblRsn.Visible = True
''''''''''        txtRsn.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 3))
''''''''''        txtRsn.Visible = True
''''''''''        txtRsn.SetFocus
''''''''''    Else
''''''''''        lblRsn.Visible = False
''''''''''        txtRsn.Visible = False
''''''''''    End If
''''''''''End If
End Sub

Private Sub cmdView_Click()
Dim ApplTemp As ApplicationTemplates
If Not (lstDisb.Visible) Then
    frmEventMsgs.Header = "Disbursements"
    frmEventMsgs.AcceptText = "View"
    frmEventMsgs.RejectText = "Print"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBox = lstDisb
        Call ApplTemp.ApplLoadCheckDisplay(Trim(txtCheck.Text), "I", CheckId)
        Set ApplTemp = Nothing
        lstDisb.Visible = True
    ElseIf (frmEventMsgs.Result = 2) Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBox = lstDisb
        Call ApplTemp.ApplLoadCheckDisplay(Trim(txtCheck.Text), "I", CheckId)
        Set ApplTemp = Nothing
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBox = lstDisb
        Call ApplTemp.PrintProof(0, Trim(txtCheck.Text), "")
        Set ApplTemp = Nothing
        lstDisb.Visible = False
    End If
Else
    lstDisb.Visible = False
End If
End Sub

Private Sub cmdVIns_Click()
If (PatientId > 0) Then
    frmViewFinancial.PatientId = PatientId
    frmViewFinancial.ThePolicyHolder1 = PatPol1
    frmViewFinancial.ThePolicyHolder2 = PatPol2
    If (frmViewFinancial.FinancialLoadDisplay(PatientId, PatPol1, PatPol2, "M")) Then
        frmViewFinancial.Show 1
    End If
End If
End Sub


Private Sub DD1Click()

If dd1.Caption = "q" Then
    With lstP
        .Clear
        .AddItem "Close list"
        Select Case lstPmntInd
        Case 0
            Select Case lstPmnt(0).col
            Case 1
                Dim r As Integer
                Dim RetCode As New PracticeCodes
                RetCode.ReferenceType = "PAYABLETYPE"
                RetCode.ReferenceCode = ""
                    If (RetCode.FindCodePartial > 0) Then
                        r = 1
                        While (RetCode.SelectCode(r))
                            .AddItem Trim(Mid(RetCode.ReferenceCode, 1, InStrPS(1, RetCode.ReferenceCode, "-") - 1)) _
                            & Space(200) & myTrim(RetCode.ReferenceCode, 1)
                            r = r + 1
                        Wend
                    End If
                Set RetCode = Nothing
                .Selected(0) = True
            End Select
        Case 2
            With txtC
                .Text = lstPmnt(2).TextMatrix(lstPmnt(2).Row, 15)
                .Top = pTxt1.Top + 1560
                .Left = pTxt1.Left + dd1.Left + 330 - .Width
                .Visible = True
                .SetFocus
            End With
            Exit Sub
        End Select
        DoEvents
        .Left = 0
        .Top = lstPmnt(0).Top + lstPmnt(0).RowPos(lstPmnt(0).Row) + lstPmnt(0).RowHeight(0)
        .Height = frPmnt(1).Height - .Top + 90
        .Width = frPmnt(1).Width - .Left
        .Visible = True
        .SetFocus
    End With
Else
    txt1.SetFocus
End If

End Sub

Private Sub dd1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
DD1Click
If dd1.Caption = "q" Then
    dd1.Caption = "p"
Else
    dd1.Caption = "q"
End If

End Sub

Private Sub Form_Load()

Dim ADate As String
If UserLogin.HasPermission(epPowerUser) Then
    Me.BorderStyle = 1
    Me.ClipControls = True
    Me.Caption = Mid(Me.Name, 4, Len(Me.Name) - 3)
    Me.AutoRedraw = True
    Me.Refresh
End If
UserId = UserLogin.iId
AllOn = False
''''''TransitionInsurerOn = False
CheckId = 0
mCheckExist = 0
RunningCheckBalance = ""
AdjustmentCode = "X"
ADate = ""
Call FormatTodaysDate(ADate, False)
txtDate.Text = ADate
cmdComment.Visible = False
cmdAll.Text = "Show All Items"
cmdAll.Visible = False
cmdChkReset.Visible = False
lstDisb.Clear
lstDisb.Visible = False
lblInst.Visible = False
'frmLineItem.KeyPreview = True

pTxt1.Height = lstPmnt(0).RowHeight(1) - 15

Dim cntl As Control
For Each cntl In Me
    On Error Resume Next
    cntl.TabStop = False
Next
initPmnt
lstPmnt_MouseDown 0, 0, 0, 0, 0

txt1.Text = "Check                    "
lstPmnt(0).TextMatrix(1, 1) = "Check                    "
lstPmntInfo.TextMatrix(1, 1) = "K"

lstIns.Width = 11655

End Sub

Private Sub fpBtn_Click(Index As Integer)
Static Notfirsttime As Boolean

If Notfirsttime Then Exit Sub
Notfirsttime = True


Select Case Index
Case 0
    Unload Me
Case 1
    Dim i As Integer, g As Integer
    Dim currAptID As String
    Dim RcvId As String
    Dim RS As Recordset
    Dim SQL As String
    
    If CompletedPayment Then
        For i = 1 To lstPmnt(2).Rows - 1
            If Not IsNumeric(lstPmnt(3).TextMatrix(i, 6)) Then
                SQL = _
                "Select * FROM PatientReceivables " & _
                "where Invoice = '" & lstPmnt(3).TextMatrix(i, 5) & "'" & _
                "and PatientId = " & PatientId & _
                " and Invoicedate = " & Format(lstPmnt(2).TextMatrix(i, 1), "yyyymmdd")
                Set RS = GetRS(SQL)
                If RS.RecordCount = 0 Then
                    MsgBox "RCVID ERROR"
                Else
                    RcvId = RS("ReceivableId")
                    lstPmnt(3).TextMatrix(i, 6) = RcvId
                End If
            End If
        Next
        For i = 1 To lstPmnt(2).Rows - 1
            currAptID = lstPmnt(3).TextMatrix(i, 4)
            If Entered(currAptID) Then ' SKIP INVOICES THAT HAVE NO PAYMENT/ADJ ENTRIES !!!!!!!!!!!!!!!
    '====================================================
                frmBillServices.HiddenMode = True

                If frmBillServices.LoadServices(lstPmnt(3).TextMatrix(i, 6), CLng(currAptID)) Then
                    With frmBillServices
                        g = 1
                        Do While currAptID = lstPmnt(3).TextMatrix(i, 4)
                            If lstPmnt(2).TextMatrix(i, 13) = "" _
                            Or lstPmnt(2).TextMatrix(i, 14) = "" Then
                                .lstSrvs1.TextMatrix(g, 6) = "X"
                            Else
                                .lstSrvs1.TextMatrix(g, 4) = _
                                lstPmnt(2).TextMatrix(i, 11)
                                .lstSrvs1.TextMatrix(g, 5) = _
                                lstPmnt(2).TextMatrix(i, 13)
                                .lstSrvs1.TextMatrix(g, 6) = _
                                lstPmnt(2).TextMatrix(i, 14)
                            End If
                            g = g + 1
                            i = i + 1
                            If i = lstPmnt(2).Rows Then Exit Do
                        Loop
                        '.Show 1
                        Call .BatchOut_5(CStr(currAptID))
                        i = i - 1
                    End With
                End If
                frmBillServices.HiddenMode = False
                Unload frmBillServices
    '====================================================
            End If
        Next
                
        
        Call CompleteCurrentPatient_5(PatientId)
        lstPmnt(3).Rows = 1
        lstPmnt(2).Rows = 1
        lstPmnt(1).TextMatrix(1, 0) = ""
        lstPmnt(1).col = 0
        lstPmnt_MouseDown 0, 0, 0, 0, 0
        
        pTxt1.Visible = False
        lstPmnt(2).Enabled = False
        'Call Disburse(False)
    End If
End Select
Notfirsttime = False

End Sub

Private Function CompletedPayment() As Boolean
Dim i As Integer
Dim s As Double
frmEventMsgs.Header = ""
If Trim(lstPmnt(0).TextMatrix(1, 2)) = 0 Then
    For i = 1 To lstPmnt(2).Rows - 1
        s = s + lstPmnt(2).TextMatrix(i, 7)
    Next
    If Not s = 0 Then
        frmEventMsgs.Header = "Please enter payment amount."
        lstPmnt(0).col = 2
    End If
Else
    If Trim(lstPmnt(0).TextMatrix(1, 3)) = "" And myTrim(lstPmnt(0).TextMatrix(1, 1), 1) = "K" Then
        frmEventMsgs.Header = "Please enter a check number."
        lstPmnt(0).col = 3
    End If
End If

If Not frmEventMsgs.Header = "" Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    
    If lstPmnt(0).Enabled Then
        lstPmnt(0).SetFocus
    End If
    Exit Function
End If
If lstPmnt(0).TextMatrix(1, 0) = "Office" Then
    For i = 1 To lstPmnt(2).Rows - 1
        If Not lstPmnt(2).TextMatrix(i, 7) = 0 Then
            frmEventMsgs.Header = "Office not valid for payments"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Function
        End If
    Next
End If
If lstPmnt(0).TextMatrix(1, 0) = "Patient" Then
    For i = 1 To lstPmnt(2).Rows - 1
        If (lstPmnt(2).TextMatrix(i, 8) = 0 _
        And lstPmnt(2).TextMatrix(i, 9) = 0 _
        And lstPmnt(2).TextMatrix(i, 10)) = 0 Then
        Else
            If _
                  (lstPmnt(2).TextMatrix(i, 17) = 0 And (lstPmnt(2).TextMatrix(i, 19) = "") _
            Or Not lstPmnt(2).TextMatrix(i, 17) = 0 And lstPmnt(2).TextMatrix(i, 19) = "V") Then
            Else
                frmEventMsgs.Header = "Adjustments cannot be from the patient. Choose a different party."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Exit Function
            End If
        End If
    Next
End If

Dim checkOK As Boolean
For i = 1 To lstPmnt(2).Rows - 1
    checkOK = False
    If lstPmnt(2).TextMatrix(i, 17) = 0 Then
        If lstPmnt(2).TextMatrix(i, 19) = "" Then checkOK = True
    Else
        If Not lstPmnt(2).TextMatrix(i, 19) = "" Then checkOK = True
    End If
    
    If Not checkOK Then
        frmEventMsgs.Header = "Other Adj requires amount and type"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Function
    End If
Next

Dim c As Integer
For i = 1 To lstPmnt(2).Rows - 1
    For c = 13 To 14
        If lstPmnt(2).TextMatrix(1, c) = "?" Then
            frmEventMsgs.Header = "Complete Billing."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
    
            DoEvents
            lstPmnt(2).Row = i
            lstPmnt(2).col = c
            editCellPmnt 2
            Exit Function
        End If
    Next
Next

CompletedPayment = True

End Function

Private Sub fpBtn1_Click()
fpBtn1.Visible = False
lstIns.Clear
DisplayInsuranceScreen (PatientId)
fpBtn1.Visible = True
End Sub


Private Sub fpBtn2_Click()
Dim PatientDemographics As New PatientDemographics
PatientDemographics.PatientId = PatientId
Call PatientDemographics.DisplayPatientFinancialScreen
Set PatientDemographics = Nothing
Disburse AllOn
pTxt1.Visible = False
End Sub

Private Sub fpBtn4_Click()
frmEventMsgs.Header = "Disbursements"
frmEventMsgs.AcceptText = "View"
frmEventMsgs.RejectText = "Print"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If frmEventMsgs.Result = 1 Then
    lstDisb.Clear
    Call frmDisbursements.LoadDisbursements(lstPmnt(0).TextMatrix(1, 2), _
    lstPmnt(0).TextMatrix(1, 3), lstPmnt(0).TextMatrix(1, 5), "Patient")
    frmDisbursements.Show 1
    Unload frmDisbursements
ElseIf frmEventMsgs.Result = 2 Then
    lstDisb.Clear
    Call frmDisbursements.LoadDisbursements(lstPmnt(0).TextMatrix(1, 2), _
    lstPmnt(0).TextMatrix(1, 3), lstPmnt(0).TextMatrix(1, 5), "Patient")
    If (lstDisb.ListCount > 0) Then
        Dim ApplTemp As New ApplicationTemplates
        Set ApplTemp.lstBox = lstDisb
        Call ApplTemp.PrintProof(0, lstPmnt(0).TextMatrix(1, 3), 0)
    End If
ElseIf frmEventMsgs.Result = 4 Then
    Exit Sub
End If

End Sub

Private Sub fpBtn5_Click()
Dim Temp As String
Dim ADate As String
Dim TheResult As Single
Dim TheAmt As Single
Dim ApplTemp As ApplicationTemplates
If (InsurerId > 0) And (Trim(txtCheck.Text) <> "") And (Trim(txtDate.Text) <> "") Then
    TheAmt = 0
    ADate = Trim(txtDate.Text)
    ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
    Set ApplTemp = New ApplicationTemplates
    TheAmt = ApplTemp.ApplGetPaymentsAgainstCheck(InsurerId, Trim(txtCheck.Text), ADate)
    Set ApplTemp = Nothing
    TheResult = val(Trim(txtAmount.Text)) - TheAmt
    Call DisplayDollarAmount(Trim(Str(TheResult)), RunningCheckBalance)
    RunningCheckBalance = Trim(RunningCheckBalance)
    Call MoneyLeft(0)
    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
    lblAmountLeft.Tag = Trim(RunningCheckBalance)
    lblAmountLeft.Visible = True
End If

End Sub

Private Sub fpBtnAF_Click()
Dim i As Integer
Dim r(1) As Integer
Dim currAppt As String
For i = 1 To lstPmnt(2).Rows - 1
    If Not currAppt = lstPmnt(3).TextMatrix(i, 4) Then
        currAppt = lstPmnt(3).TextMatrix(i, 4)
        If Entered(currAppt) Then
            r(0) = i
            Do While currAppt = lstPmnt(3).TextMatrix(i, 4)
                r(1) = i
                i = i + 1
                If i = lstPmnt(2).Rows Then Exit Do
            Loop
            Call frmPaymentsTotal.NextBill(lstPmnt(3).TextMatrix(r(0), 5), CStr(PatientId), myTrim(lstPmnt(0).TextMatrix(1, 1), 1), lstPmnt(2), r)
            i = i - 1
        End If
    End If
Next

lstPmnt(2).col = 1
editCellPmnt 2
End Sub

Private Function Entered(ApptId As String) As Boolean
Dim r As Integer
Dim f As Integer
Dim e As Boolean

For r = 1 To lstPmnt(2).Rows - 1
    If lstPmnt(3).TextMatrix(r, 4) = ApptId Then
        For f = 7 To 10
            e = Not (lstPmnt(2).TextMatrix(r, f) = 0)
            If e Then GoTo EndFunction
        Next
        If Not e Then
            e = Not (lstPmnt(2).TextMatrix(r, 17) = 0)
            If e Then GoTo EndFunction
        End If
    End If
Next

EndFunction:
Entered = e
End Function


Private Function BilledParty(AppId As Long, SrvId As Long) As String

Dim RS As ADODB.Recordset
Dim ApplList As New ApplicationAIList
Set RS = ApplList.getBilledParty(AppId, SrvId)
Set ApplList = Nothing

If RS.EOF Then Exit Function

If RS("Transactionref") = "P" Then
    BilledParty = "P"
Else
    BilledParty = RS("InsurerID")
End If
End Function


Private Sub fpBtnS_Click(Index As Integer)
If lstPmnt(2).Rows = 1 Then Exit Sub

Dim s As Integer
Dim tRow As Integer
Select Case Index
Case 0
    tRow = lstPmnt(2).TopRow - 1
Case 1
    tRow = lstPmnt(2).TopRow - 1
Case 2
    tRow = lstPmnt(2).Rows - 1
Case 3
    tRow = 1
End Select
If tRow < 1 Then tRow = 1

If lstPmnt(2).TopRow = tRow Then
    If pTxt1.Visible Then txt1.SetFocus
Else
    pTxt1.Visible = False
    lstPmnt(2).TopRow = tRow
End If
End Sub

Private Sub lstDisb_Click()
''''''''''Dim Amt As String
''''''''''Dim Temp As String
''''''''''Dim AmtLeft As String
''''''''''Dim InsName As String
''''''''''Dim ChkNo As String
''''''''''Dim ApplTemp As ApplicationTemplates
''''''''''If (GetCheckOn) Then
''''''''''    If (lstDisb.ListIndex > 0) Then
''''''''''        Set ApplTemp = New ApplicationTemplates
''''''''''        cmdApply.Enabled = True
''''''''''        cmdHome.Enabled = True
''''''''''        cmdDisburse.Enabled = True
''''''''''        cmdView.Enabled = True
''''''''''        txtAmount.Enabled = True
''''''''''        txtPatient.Enabled = True
''''''''''        txtInsurer.Enabled = True
''''''''''        txtCheck.Enabled = True
''''''''''        txtDate.Enabled = True
''''''''''        GetCheckOn = False
''''''''''        CheckId = Val(Trim(Mid(lstDisb.List(lstDisb.ListIndex), 56, Len(lstDisb.List(lstDisb.ListIndex)))))
''''''''''        If Not (ApplTemp.ApplGetTheCheck(CheckId, InsName, ChkNo, Amt, AmtLeft)) Then
''''''''''            txtCheck.SetFocus
''''''''''        Else
''''''''''            txtInsurer.Text = InsName
''''''''''            InsurerId = Val(Mid(InsName, 56, Len(InsName) - 55))
''''''''''            RunningCheckBalance = Trim(AmtLeft)
''''''''''            Call DisplayDollarAmount(Trim(Str(Amt)), Temp)
''''''''''            txtAmount.Text = Trim(Temp)
''''''''''            lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(Str(AmtLeft))
''''''''''            lblAmountLeft.Tag = Trim(Str(AmtLeft))
''''''''''            txtPatient.SetFocus
''''''''''        End If
''''''''''        Set ApplTemp = Nothing
''''''''''        lstDisb.Visible = False
''''''''''    ElseIf (lstDisb.ListIndex = 0) Then
''''''''''        cmdApply.Enabled = True
''''''''''        cmdHome.Enabled = True
''''''''''        cmdDisburse.Enabled = True
''''''''''        cmdView.Enabled = True
''''''''''        txtAmount.Enabled = True
''''''''''        txtPatient.Enabled = True
''''''''''        txtInsurer.Enabled = True
''''''''''        txtCheck.Enabled = True
''''''''''        txtDate.Enabled = True
''''''''''        txtCheck.SetFocus
''''''''''        GetCheckOn = False
''''''''''        lstDisb.Visible = False
''''''''''    End If
''''''''''End If
End Sub

Private Sub lstIns_DblClick()
If lstIns.ListIndex = 0 Then lstIns_LostFocus
End Sub

Private Sub lstIns_LostFocus()
lstIns.Visible = False
fpBtn1.Visible = True
editCellPmnt 2
End Sub

Private Sub lstP_DblClick()

If Not lstP.ListIndex = 0 Then
    'FromCredit False
    If lstPmntInd = 0 _
    And lstPmnt(0).col = 1 Then
        txt1.Text = Mid(lstP.Text, 1, 25)
        lstPmnt(0).TextMatrix(1, 1) = txt1.Text
        lstPmntInfo.TextMatrix(1, 1) = myTrim(lstP.Text, 1)
    Else
        txt1.Text = lstP.Text
    End If
End If
txt1.SetFocus

End Sub

Private Sub lstP_LostFocus()
lstP.Visible = False
End Sub

Private Sub lstPmnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim ReturnArguments As Variant
Select Case Index
Case 0
    If lstPmnt(Index).col = 0 Then
            Set ReturnArguments = DisplayPatientSearchScreen
            If Not (ReturnArguments Is Nothing) Then
                lstPmnt(1).TextMatrix(1, 2) = "0.00"
                lstPmnt(1).TextMatrix(1, 3) = "0.00"
                LoadPatient 0, ReturnArguments.PatientId
            End If
        lstPmnt(0).col = 1
    End If
Case 1
    If lstPmnt(Index).col = 0 Then
        Dim e As Integer
        e = LstCompleted
        Select Case e
        Case 0
            Set ReturnArguments = DisplayPatientSearchScreen
            If Not (ReturnArguments Is Nothing) Then
                lstPmnt(1).TextMatrix(1, 2) = "0.00"
                lstPmnt(1).TextMatrix(1, 3) = "0.00"
                LoadPatient 1, ReturnArguments.PatientId
                Call Disburse(False)
            End If
        Case 1
            lstPmnt(0).col = 0
            lstPmnt_MouseDown 0, 0, 0, 0, 0
        Case Else
            lstPmnt(0).col = e - 1
            editCellPmnt (0)
        End Select
    End If
    If lstPmnt(2).Rows > 1 Then
        editCellPmnt 2
    Else
        editCellPmnt lstPmntInd
    End If
End Select
If lstPmnt(1).TextMatrix(1, 0) = "" Then
    fpBtn2.Enabled = False
    fpBtn1.Enabled = False
Else
    fpBtn2.Enabled = True
    fpBtn1.Enabled = True
End If
If Not Trim(lstPmnt(0).TextMatrix(1, 3)) = "" And Not Trim(lstPmnt(0).TextMatrix(1, 5)) = "" And Not Trim(lstPmnt(0).TextMatrix(1, 2) = "0.00") Then
    fpBtn4.Enabled = True
Else
    fpBtn4.Enabled = False
End If
End Sub

Private Sub lstPmnt_EnterCell(Index As Integer)
    editCellPmnt Index
End Sub

Private Sub lstPmnt_GotFocus(Index As Integer)
    editCellPmnt Index
End Sub

Private Sub opt_Click(Index As Integer)
If Not lstPmnt(1).Enabled Then Exit Sub

OptView = Index
lstPmnt(1).TextMatrix(1, 6) = "0.00"
lstPmnt(1).TextMatrix(1, 7) = "0.00"
Call Disburse(False)
editCellPmnt 2
End Sub

Private Sub PT_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Not lstPmnt(2).Enabled Then Exit Sub

If lstPT.Visible Then
    lstPT.Visible = False
    lstPmnt(2).col = lstPmnt(2).Cols - 1
    lstPmnt(2).SetFocus
Else
    lblHelp.Caption = ""
    Dim k As Integer
    Dim RetrieveCode As New PracticeCodes
    RetrieveCode.ReferenceType = "paymenttype"
    If (RetrieveCode.FindCode > 0) Then
        k = 1
        Do Until (Not (RetrieveCode.SelectCode(k)))
            If Trim(RetrieveCode.ReferenceCode) Like "*&" Then
                lblHelp.Caption = lblHelp.Caption & Trim(RetrieveCode.ReferenceCode) & "&" & vbNewLine
            Else
                lblHelp.Caption = lblHelp.Caption & Trim(RetrieveCode.ReferenceCode) & vbNewLine
            End If
            k = k + 1
        Loop
    End If
    Set RetrieveCode = Nothing
    VS.SmallChange = 15 * 14
    VS.LargeChange = VS.SmallChange * 10
    lblHelp.Height = (k - 1) * VS.SmallChange
    VS.Max = (k - 1 - 18) * VS.SmallChange
    lstPT.Visible = True
    VS.SetFocus
End If
End Sub

Private Sub sf_GotFocus()
pTxt1.Visible = False
End Sub

Private Sub txt1_GotFocus()
dd1.Caption = "q"
If txtC.Visible Then txtC.Visible = False
End Sub

Private Sub txt1_KeyDown(KeyCode As Integer, Shift As Integer)
mKeyCode = 0

Dim FGC As MSFlexGrid
Set FGC = lstPmnt(lstPmntInd)


Dim ExitSub As Boolean
Select Case KeyCode
Case vbKeyTab, vbKeyReturn
    If FGC.Index = 2 Then
        If FGC.col = 6 Then
            If KeyCode = vbKeyTab Then mKeyCode = vbKeyTab
        End If
    End If
    
    Call txt1_Validate(ExitSub)
    If ExitSub Then Exit Sub
Case vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
    '''If txtLocked Then KeyCode = 0
Case vbKeyPageDown, vbKeyPageUp
    Call txt1_Validate(ExitSub)
    If ExitSub Then Exit Sub
    Dim nextI As Integer
    nextI = KeyCode - 34
    If nextI = 0 Then nextI = 1
    nextI = lstPmntInd + nextI
    Select Case nextI
    Case 3
        nextI = 0
    Case -1
        nextI = 2
    End Select
    
    If nextI = 1 Then
        lstPmnt_MouseDown 0, 0, 0, 0, 0
        Exit Sub
    Else
        If lstPmnt(nextI).Enabled Then lstPmnt(nextI).SetFocus
    End If
Case Else
    KeyCode = 0
End Select

Dim c As Integer, r As Integer
c = FGC.col
r = FGC.Row
Dim nextC As Integer, nextR As Integer
With FGC
    Select Case KeyCode
    Case vbKeyTab
        bySys = True
        nextR = .Row
        If Shift = 0 Then
            nextC = .col + 1
        Else
            nextC = .col - 1
        End If
        
        If lstPmntInd = 1 Then
            If nextC < 6 Then
                nextC = -1
            End If
        End If
        
        Select Case nextC
        Case .Cols
            nextC = 0
            nextR = .Row + 1
        Case Is < 0
            nextC = .Cols - 1
            nextR = .Row - 1
        End Select
        
        Select Case nextR
        Case .Rows
            nextR = 1
        Case 0
            nextR = .Rows - 1
        End Select
        
        Call SkipCell1(lstPmntInd, nextC, nextR, Shift)
        .col = nextC
        .Row = nextR
        bySys = False
        editCellPmnt lstPmntInd
    Case vbKeyReturn
        
        txt1.Text = .Text
        If dd1.Visible Then
            '''DD1Click
            dd1.Caption = "p"
        Else
            bySys = False
            If lstPmntInd = 2 Then
                .col = 6
                nextR = r + (Shift + 1 - (Shift * 3))
            End If
            Select Case nextR
            Case 0
                nextR = .Rows - 1
            Case .Rows
                nextR = 1
            End Select
            .Row = nextR
            bySys = False
            editCellPmnt lstPmntInd
        End If
    End Select
End With
End Sub


Private Sub txt1_Validate(Cancel As Boolean)
Dim StxtValid As Boolean
Dim orgTxt As String
Dim corrTxt As String
Dim n(5) As Double
Dim Temp As String
Dim r As Integer
Dim k As Integer
Dim FGC As MSFlexGrid
Set FGC = lstPmnt(lstPmntInd)

If FGC.Text = txt1.Text Then
    If FGC.Index = 2 And FGC.col = 6 And mKeyCode = vbKeyTab Then
    Else
        Exit Sub
    End If
End If

orgTxt = FGC.Text
StxtValid = txt1Valid(corrTxt)

If orgTxt = corrTxt Then
    If FGC.Index = 2 And FGC.col = 6 And mKeyCode = vbKeyTab Then
    Else
        Exit Sub
    End If
End If

If StxtValid Then
    FGC.Text = corrTxt
Else
    txt1.Text = FGC.Text
    txt1.SelStart = 0
    txt1.SelLength = Len(txt1.Text)
    txt1.SetFocus
    Cancel = True
    Exit Sub
End If

Select Case lstPmntInd
Case 2
    With lstPmnt(2)
        Select Case .col
        Case 6, 7, 8, 17, 19
            '--------------------------
'            If .col = 6 And mKeyCode = vbKeyTab Then
'                If Not .Text = 0 Then
'                    mKeyCode = -1
'
'                    For r = 0 To 4
'                        n(r) = 0
'                    Next
'                    If IsNumeric(.TextMatrix(.Row, 4)) Then n(0) = .TextMatrix(.Row, 4) 'Charge
'                    If IsNumeric(.TextMatrix(.Row, 5)) Then n(1) = .TextMatrix(.Row, 5) 'Prev Bal
'                    If IsNumeric(.TextMatrix(.Row, 6)) Then n(2) = .TextMatrix(.Row, 6) 'Allowed
'
'                    Dim AdjAlowed As Double
'                    Dim PlanId As String
'                    PlanId = myTrim(lstPmnt(0).TextMatrix(1, 1), 1)
'                    If IsNumeric(PlanId) Then
'                        Dim RetFee As New PracticeFeeSchedules
'                        RetFee.SchService = .TextMatrix(.Row, 2)
'                        RetFee.SchPlanId = PlanId
'                        If RetFee.FindServiceDirect > 0 Then
'                            k = 1
'                            Dim apptDte As String
'                            apptDte = Format(.TextMatrix(.Row, 1), "yyyymmdd")
'                            Do While RetFee.SelectService(k)
'                                If Not apptDte < RetFee.SchStartDate Then
'                                    If Not apptDte > RetFee.SchEndDate _
'                                    Or RetFee.SchEndDate = "" Then
'                                        If IsNumeric(RetFee.SchAdjustmentRate) Then
'                                            If n(0) < n(2) Then
'                                                AdjAlowed = n(0) * RetFee.SchAdjustmentRate / 100
'                                            Else
'                                                AdjAlowed = n(2) * RetFee.SchAdjustmentRate / 100
'                                            End If
'                                            Exit Do
'                                        End If
'                                    End If
'                                End If
'                                k = k + 1
'                            Loop
'                        End If
'                        Set RetFee = Nothing
'                    End If
'
'                    If AdjAlowed = 0 Then
'                        If n(0) = n(1) Then
'                            If n(0) < n(2) Then
'                                n(3) = n(0)
'                            Else
'                                n(3) = n(2)
'                            End If
'                        Else
'                            n(3) = n(2) - (n(0) - n(1))
'                        End If
'                    Else
'                        n(3) = AdjAlowed
'                    End If
'
'                    If n(3) > n(1) Then
'                        n(3) = n(1)
'                    End If
'
'                    If n(3) < 0 Then
'                        n(3) = 0
'                    End If
'                    Temp = n(3)
'                    Call DisplayDollarAmount(Temp, Temp)
'                    .TextMatrix(.Row, 7) = Trim(Temp)           '<payment
'
'                    n(4) = n(0) - n(2)
'                    If n(4) < 0 Then n(4) = 0
'                    Temp = n(4)
'                    Call DisplayDollarAmount(Temp, Temp)
'                    .TextMatrix(.Row, 8) = Trim(Temp)
'                End If
'                GoSub calcRemainingAmt
'                mKeyCode = 0
'            End If
            '--------------------------
            For r = 0 To 4
                n(r) = 0
            Next
            If IsNumeric(.TextMatrix(.Row, 5)) Then n(0) = .TextMatrix(.Row, 5)
            If IsNumeric(.TextMatrix(.Row, 7)) Then n(1) = .TextMatrix(.Row, 7)
            If IsNumeric(.TextMatrix(.Row, 8)) Then n(2) = .TextMatrix(.Row, 8)
            If IsNumeric(.TextMatrix(.Row, 17)) Then
                n(3) = .TextMatrix(.Row, 17)
                If n(3) = 0 _
                Or .TextMatrix(.Row, 19) = "" Then
                    n(3) = 0
                Else
                    Dim RetrieveCode As New PracticeCodes
                    RetrieveCode.ReferenceType = "paymenttype"
                    If (RetrieveCode.FindCode > 0) Then
                        k = 1
                        Do Until (Not (RetrieveCode.SelectCode(k)))
                            If .TextMatrix(.Row, 19) = myTrim(RetrieveCode.ReferenceCode, 1) Then
                                Exit Do
                            End If
                            k = k + 1
                        Loop
                    End If

                    Select Case RetrieveCode.ReferenceAlternateCode
                    Case "C"
                        n(3) = -n(3)
                    Case "D"
                    Case Else
                        n(3) = 0
                    End Select
                    Set RetrieveCode = Nothing
                End If
            End If
            n(4) = n(0) - n(1) - n(2) + n(3)

            Temp = n(4)
            Call DisplayDollarAmount(Temp, Temp)
            .TextMatrix(.Row, 11) = Trim(Temp)
            Select Case .col
            Case 7, 8, 17, 19
                GoSub calcRemainingAmt
            End Select
        End Select
        Select Case .col
        Case 6, 7, 8, 9, 10, 17, 19
            Call checkGroup(lstPmnt(3).TextMatrix(lstPmnt(2).Row, 4))
        End Select
    End With

End Select
Exit Sub
'=============================
calcRemainingAmt:

For r = 0 To 5
    n(r) = 0
Next
With lstPmnt(2)
    For r = 1 To .Rows - 1
        If IsNumeric(.TextMatrix(r, 7)) Then
            n(1) = n(1) + .TextMatrix(r, 7)
        End If
        If IsNumeric(.TextMatrix(r, 8)) Then
            n(2) = n(2) + .TextMatrix(r, 8)
        End If
        If IsNumeric(.TextMatrix(r, 17)) _
        And Not .TextMatrix(r, 19) = "" Then
            n(3) = .TextMatrix(r, 17) * CreditDebit(.TextMatrix(r, 19))
            Select Case .TextMatrix(r, 19)
            Case "U", "R"
                n(4) = n(4) + n(3)
            Case Else
                n(5) = n(5) + n(3)
            End Select
        End If
    Next

    '1
    n(0) = RemCheckAmt - Round(n(1), 2) - Round(n(4), 2)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmnt(1).TextMatrix(1, 2) = Trim(Temp)
    lblWB.Visible = (CDbl(Trim(Temp)) < 0)
    '2
    n(0) = PaidCheckAmt + Round(n(1), 2) + Round(n(4), 2)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmnt(1).TextMatrix(1, 3) = Trim(Temp)
    '3
    n(0) = AdjCheckAmt + n(2) + n(5)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmnt(1).TextMatrix(1, 4) = Trim(Temp)
    '--------
    '4
    n(0) = Round(n(1), 2) + Round(n(4), 2)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmnt(1).TextMatrix(1, 6) = Trim(Temp)
    '5
    n(0) = n(2) + n(5)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmnt(1).TextMatrix(1, 7) = Trim(Temp)
    
    CheckServiceBallance
End With


Return
End Sub

Private Sub CheckServiceBallance()
Dim r As Integer
Dim Worn As Boolean

With lstPmnt(2)
    For r = 1 To .Rows - 1
        If .TextMatrix(r, 7) = 0 _
        And .TextMatrix(r, 8) = 0 _
        And .TextMatrix(r, 17) = 0 Then
        Else
            If .TextMatrix(r, 11) < 0 Then
                Worn = True
                Exit For
            End If
        End If
    Next
End With

lblWB1.Visible = Worn
End Sub



Private Function CreditDebit(PaymentType As String) As Integer
Dim SQL As String
Dim RS As Recordset

SQL = _
"select AlternateCode  from PracticeCodeTable " & _
"where ReferenceType = 'PaymentType' " & _
"and Right(Code, 1) = '" & PaymentType & "'"
Set RS = GetRS(SQL)

Select Case RS("AlternateCode")
Case "C"
    CreditDebit = 1
Case "D"
    CreditDebit = -1
Case Else
    CreditDebit = 0
End Select
End Function

Private Function checkGroup(ApptId As String)
Dim r As Integer
Dim v As String
If Entered(ApptId) Then v = "?"

For r = 1 To lstPmnt(2).Rows - 1
    If lstPmnt(3).TextMatrix(r, 4) = ApptId Then
        lstPmnt(2).TextMatrix(r, 13) = v
        lstPmnt(2).TextMatrix(r, 14) = v
    End If
Next
End Function


Private Function txt1Valid(corrTxt As String) As Boolean
Dim FtxtValid As Boolean
corrTxt = Trim(txt1.Text)

Select Case lstPmntInd
Case 0
    Select Case lstPmnt(0).col
    Case 2
            If Trim(corrTxt) = "" Then corrTxt = 0
            If IsNumeric(corrTxt) Then
                Call DisplayDollarAmount(corrTxt, corrTxt)
                FtxtValid = True
            End If
    Case 4
            FtxtValid = OkDate(corrTxt)
            If FtxtValid Then
                FtxtValid = EditableDate(CDate(corrTxt))
            End If
    Case 5
            corrTxt = Trim(corrTxt)
            If corrTxt = "" Then
                FtxtValid = True
            Else
                FtxtValid = OkDate(corrTxt)
            End If
    Case Else
            FtxtValid = True
    End Select
Case 2
    Select Case lstPmnt(2).col
    Case 6, 7, 8, 9, 10, 17
            If Trim(corrTxt) = "" Then corrTxt = 0
            If IsNumeric(corrTxt) Then
                Call DisplayDollarAmount(corrTxt, corrTxt)
                FtxtValid = True
            End If
    Case 13
            Select Case UCase(Trim(corrTxt))
            Case "", "P"
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            Case Else
                If IsNumeric(Trim(corrTxt)) Then
                    corrTxt = Trim(corrTxt)
                    FtxtValid = True
                End If
            End Select
    Case 14
            Select Case UCase(Trim(corrTxt))
            Case "", "B", "P", "W", "A", "U"
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            End Select
    Case 16
            Select Case UCase(Trim(corrTxt))
            Case "", "Y"
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            End Select
    Case 19
            Select Case UCase(Trim(corrTxt))
            Case ""
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            Case Else
                Dim k As Integer
                Dim RetrieveCode As New PracticeCodes
                RetrieveCode.ReferenceType = "paymenttype"
                If (RetrieveCode.FindCode > 0) Then
                    k = 1
                    Do Until (Not (RetrieveCode.SelectCode(k)))
                        If UCase(Trim(corrTxt)) = myTrim(RetrieveCode.ReferenceCode, 1) Then
                            corrTxt = UCase(corrTxt)
                            FtxtValid = True
                            Exit Do
                        End If
                        k = k + 1
                    Loop
                End If
                Set RetrieveCode = Nothing
            End Select
    Case Else
        FtxtValid = True
    End Select
End Select

If Not FtxtValid Then
    With frmEventMsgs
        .Header = "Invalid value"
        .AcceptText = ""
        .RejectText = "OK"
        .CancelText = ""
        .Other0Text = ""
        .Other1Text = ""
        .Other2Text = ""
        .Other3Text = ""
        .Other4Text = ""
        .Show 1
    End With
End If

corrTxt = Trim(corrTxt)
txt1Valid = FtxtValid
End Function


Private Sub txtC_GotFocus()
txtC.SelStart = 0
txtC.SelLength = Len(txtC.Text)
End Sub

Private Sub txtC_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case vbKeyEscape
    txt1.SetFocus
Case vbKeyReturn, vbKeyTab
    lstPmnt(2).TextMatrix(lstPmnt(2).Row, 12) = Trim(txtC.Text)
    If Trim(txtC.Text) = "" Then
        txt1.Text = ""
    Else
        txt1.Text = txtC.Text
    End If
    lstPmnt(2).TextMatrix(lstPmnt(2).Row, 15) = txt1.Text
    txt1.SetFocus
End Select
End Sub

Private Sub txtCheck_Validate(Cancel As Boolean)
If (Trim(txtCheck.Text) <> "") Then
    Call txtCheck_KeyPress(13)
End If
End Sub

Private Sub txtCheck_KeyPress(KeyAscii As Integer)
'''''''''''Dim Amt As String
'''''''''''Dim ins As String
'''''''''''Dim Temp As String
'''''''''''Dim AmtLeft As String
'''''''''''Dim CheckDate As String
'''''''''''Dim DateGood As Boolean
'''''''''''Dim ApplTemp As ApplicationTemplates
'''''''''''If (KeyAscii = 13) Or (KeyAscii = 10) Then
'''''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''''    ins = Trim(Left(txtInsurer.Text, 30))
'''''''''''    If (ApplTemp.ApplIsInsurerCheckOpen(ins, Trim(txtCheck.Text), Amt, AmtLeft, CheckId, CheckDate, DateGood)) Then
'''''''''''        If (Trim(ins) <> "") Then
'''''''''''            frmEventMsgs.Header = "Check In Use - " + CheckDate
'''''''''''            frmEventMsgs.AcceptText = ""
'''''''''''            frmEventMsgs.RejectText = "Continue"
'''''''''''            frmEventMsgs.CancelText = "Cancel"
'''''''''''            frmEventMsgs.Other0Text = ""
'''''''''''            frmEventMsgs.Other1Text = ""
'''''''''''            frmEventMsgs.Other2Text = ""
'''''''''''            frmEventMsgs.Other3Text = ""
'''''''''''            frmEventMsgs.Other4Text = ""
'''''''''''            frmEventMsgs.Show 1
'''''''''''            If (frmEventMsgs.Result <> 2) Then
'''''''''''                txtCheck.Text = ""
'''''''''''                txtCheck.SetFocus
'''''''''''                Set ApplTemp = Nothing
'''''''''''                Exit Sub
'''''''''''            End If
'''''''''''        End If
'''''''''''        If Not (DateGood) Then
'''''''''''            frmEventMsgs.Header = "Batch is Closed."
'''''''''''            frmEventMsgs.AcceptText = ""
'''''''''''            frmEventMsgs.RejectText = "Ok"
'''''''''''            frmEventMsgs.CancelText = ""
'''''''''''            frmEventMsgs.Other0Text = ""
'''''''''''            frmEventMsgs.Other1Text = ""
'''''''''''            frmEventMsgs.Other2Text = ""
'''''''''''            frmEventMsgs.Other3Text = ""
'''''''''''            frmEventMsgs.Other4Text = ""
'''''''''''            frmEventMsgs.Show 1
'''''''''''            txtCheck.Text = ""
'''''''''''            txtCheck.SetFocus
'''''''''''            Set ApplTemp = Nothing
'''''''''''            Exit Sub
'''''''''''        End If
'''''''''''        If (Val(Trim(AmtLeft)) = 0) Or (Val(Trim(Amt)) < 0) Then
'''''''''''            If (Trim(ins) = "") Then
'''''''''''                If Not (GetCorrectCheck(txtCheck.Text, CheckId)) Then
'''''''''''                    Call cmdView_Click
'''''''''''                    txtCheck.Text = ""
'''''''''''                    txtCheck.SetFocus
'''''''''''                    Exit Sub
'''''''''''                End If
'''''''''''            Else
'''''''''''                frmEventMsgs.Header = "Check Already Closed"
'''''''''''                frmEventMsgs.AcceptText = ""
'''''''''''                frmEventMsgs.RejectText = "Ok"
'''''''''''                frmEventMsgs.CancelText = ""
'''''''''''                frmEventMsgs.Other0Text = ""
'''''''''''                frmEventMsgs.Other1Text = ""
'''''''''''                frmEventMsgs.Other2Text = ""
'''''''''''                frmEventMsgs.Other3Text = ""
'''''''''''                frmEventMsgs.Other4Text = ""
'''''''''''                frmEventMsgs.Show 1
'''''''''''                txtCheck.Text = ""
'''''''''''                txtCheck.SetFocus
'''''''''''            End If
'''''''''''        Else
'''''''''''            If (Len(ins) > 30) Then
'''''''''''                txtInsurer.Text = ins
'''''''''''                InsurerId = Val(Mid(ins, 56, Len(ins) - 55))
'''''''''''            End If
'''''''''''            RunningCheckBalance = Trim(AmtLeft)
'''''''''''            Call DisplayDollarAmount(Trim(Str(Amt)), Temp)
'''''''''''            txtAmount.Text = Trim(Temp)
'''''''''''            lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
'''''''''''            lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''''''            txtDate.Text = Mid(CheckDate, 5, 2) + "/" + Mid(CheckDate, 7, 2) + "/" + Left(CheckDate, 4)
'''''''''''            txtPatient.SetFocus
'''''''''''        End If
'''''''''''    Else
'''''''''''        txtPatient.SetFocus
'''''''''''    End If
'''''''''''    Set ApplTemp = Nothing
'''''''''''End If
End Sub

Private Sub txtPatient_Click()
''''''''Dim i As Integer
''''''''Dim PatId As Long
''''''''Dim VerifyOn As Boolean
''''''''Dim TriggerDisburse As Boolean
''''''''Dim b1 As Boolean, b2 As Boolean
''''''''Dim ATemp As String
''''''''Dim IName As String
''''''''Dim InsName(6) As String
''''''''Dim r1 As String, r2 As String
''''''''Dim Temp As String, AName As String
''''''''Dim ApplTemp As ApplicationTemplates
''''''''VerifyOn = False
''''''''TriggerDisburse = False
''''''''IName = ""
''''''''InsName(1) = ""
''''''''InsName(2) = ""
''''''''InsName(3) = ""
''''''''InsName(4) = ""
''''''''InsName(5) = ""
''''''''InsName(6) = ""
''''''''If (InsurerId > 0) And (PatientId > 0) Then
''''''''    VerifyOn = False
''''''''    For i = 1 To MSFlexGrid1.Rows - 1
''''''''        If (Val(Trim(MSFlexGrid1.TextMatrix(i, 6))) <> 0) Then
''''''''            If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
''''''''                VerifyOn = True
''''''''                Exit For
''''''''            End If
''''''''        End If
''''''''        If (Val(Trim(MSFlexGrid1.TextMatrix(i, 7))) <> 0) Then
''''''''            If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
''''''''                VerifyOn = True
''''''''                Exit For
''''''''            End If
''''''''        End If
''''''''        If (Trim(MSFlexGrid1.TextMatrix(i, 12)) <> "") Then
''''''''            If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
''''''''                VerifyOn = True
''''''''                Exit For
''''''''            End If
''''''''        End If
''''''''    Next i
''''''''    If (VerifyOn) Then
''''''''        Call CompleteCurrentPatient(PatientId)
''''''''    End If
''''''''    TriggerDisburse = MSFlexGrid1.Visible
''''''''    MSFlexGrid1.Visible = False
''''''''    lblInst.Visible = False
''''''''    cmdDisburse.Text = "Disburse"
''''''''    cmdAll.Visible = False
''''''''    cmdChkReset.Visible = False
''''''''    cmdComment.Visible = False
''''''''    lblTPay.Visible = False
''''''''    lblTPaid.Visible = False
''''''''    lblTAdj.Visible = False
''''''''    lblTAdjs.Visible = False
''''''''    txtAllow.Text = ""
''''''''    txtPayAmt.Text = ""
''''''''    txtAdjAmt.Text = ""
''''''''    lblAdj.Visible = False
''''''''    txtAdj.Text = ""
''''''''    txtAdj.Visible = False
''''''''    chkInclude.Value = 0
''''''''    chkInclude.Visible = False
''''''''    txtPatient.Text = ""
''''''''End If
''''''''frmPatientSchedulerSearch.PatientNumberOn = True
''''''''frmPatientSchedulerSearch.AddOn = False
''''''''frmPatientSchedulerSearch.Show 1
''''''''frmPatientSchedulerSearch.PatientNumberOn = False
''''''''If (frmPatientSchedulerSearch.PatientId > 0) Then
''''''''    PatId = frmPatientSchedulerSearch.PatientId
''''''''    Unload frmPatientSchedulerSearch
''''''''    Set ApplTemp = New ApplicationTemplates
''''''''    Call ApplTemp.ApplGetPatientName(PatId, AName, PatPol1, r1, b1, PatPol2, r2, b2, False)
''''''''    ATemp = ApplTemp.ApplVerifyPatient(PatPol1, 0)
''''''''    If (ATemp <> "") Then
''''''''        frmEventMsgs.Header = ATemp
''''''''        frmEventMsgs.AcceptText = ""
''''''''        frmEventMsgs.RejectText = "Fix It"
''''''''        frmEventMsgs.CancelText = "Cancel"
''''''''        frmEventMsgs.Other0Text = ""
''''''''        frmEventMsgs.Other1Text = ""
''''''''        frmEventMsgs.Other2Text = ""
''''''''        frmEventMsgs.Other3Text = ""
''''''''        frmEventMsgs.Other4Text = ""
''''''''        frmEventMsgs.Show 1
''''''''        If (frmEventMsgs.Result = 2) Then
''''''''            If (frmPatient.PatientLoadDisplay(PatPol1, False)) Then
''''''''                frmPatient.Show 1
''''''''                ATemp = ApplTemp.ApplVerifyPatient(PatPol1, 0)
''''''''                If (ATemp <> "") Then
''''''''                    frmEventMsgs.Header = ATemp
''''''''                    frmEventMsgs.AcceptText = ""
''''''''                    frmEventMsgs.RejectText = "Ok"
''''''''                    frmEventMsgs.CancelText = ""
''''''''                    frmEventMsgs.Other0Text = ""
''''''''                    frmEventMsgs.Other1Text = ""
''''''''                    frmEventMsgs.Other2Text = ""
''''''''                    frmEventMsgs.Other3Text = ""
''''''''                    frmEventMsgs.Other4Text = ""
''''''''                    frmEventMsgs.Show 1
''''''''                    PatientId = 0
''''''''                    txtPatient.Text = ""
''''''''                    txtPatient.SetFocus
''''''''            Exit Sub
''''''''                End If
''''''''            End If
''''''''        Else
''''''''            PatientId = 0
''''''''            txtPatient.Text = ""
''''''''            txtPatient.SetFocus
''''''''            Exit Sub
''''''''        End If
''''''''    End If
''''''''    If (InsurerId > 0) Then
''''''''        If (ApplTemp.ApplIsInsurerMatch(PatId, PatPol1, PatPol2, InsurerId, PatInsId, PatPolId)) Then
''''''''            PatientId = PatId
''''''''            txtPatient.Text = AName
''''''''            txtAmount.SetFocus
''''''''            If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                cmdDisburse.Visible = True
''''''''                cmdDisburse.SetFocus
''''''''            End If
''''''''        Else
''''''''            frmEventMsgs.Header = "Patient does not currently have this payer, Override ?"
''''''''            frmEventMsgs.AcceptText = ""
''''''''            frmEventMsgs.RejectText = "Ok"
''''''''            frmEventMsgs.CancelText = "No"
''''''''            frmEventMsgs.Other0Text = ""
''''''''            frmEventMsgs.Other1Text = ""
''''''''            frmEventMsgs.Other2Text = ""
''''''''            frmEventMsgs.Other3Text = ""
''''''''            frmEventMsgs.Other4Text = ""
''''''''            frmEventMsgs.Show 1
''''''''            If (frmEventMsgs.Result = 2) Then
''''''''                Call ApplTemp.ApplGetBatchPayers(PatId, InsName(1), InsName(2), InsName(3), InsName(4), InsName(5), InsName(6))
''''''''                Call ApplTemp.AddPreviousPayers(PatId, InsurerId, IName, False)
''''''''                If (Trim(InsName(6)) = "") Then
''''''''                    InsName(6) = IName
''''''''                End If
''''''''                If (InsName(1) = "") Then
''''''''                    frmEventMsgs.Header = "No Insurers"
''''''''                    frmEventMsgs.AcceptText = ""
''''''''                    frmEventMsgs.RejectText = "Ok"
''''''''                    frmEventMsgs.CancelText = ""
''''''''                    frmEventMsgs.Other0Text = ""
''''''''                    frmEventMsgs.Other1Text = ""
''''''''                    frmEventMsgs.Other2Text = ""
''''''''                    frmEventMsgs.Other3Text = ""
''''''''                    frmEventMsgs.Other4Text = ""
''''''''                    frmEventMsgs.Show 1
''''''''                    PatientId = 0
''''''''                    txtPatient.SetFocus
''''''''                    Exit Sub
''''''''                End If
''''''''                frmEventMsgs.Header = "Override with which Patient Insurer ?"
''''''''                frmEventMsgs.AcceptText = "Cancel"
''''''''                frmEventMsgs.RejectText = Trim(Left(InsName(1), 30))
''''''''                frmEventMsgs.CancelText = Trim(Left(InsName(2), 30))
''''''''                frmEventMsgs.Other0Text = Trim(Left(InsName(3), 30))
''''''''                frmEventMsgs.Other1Text = Trim(Left(InsName(4), 30))
''''''''                frmEventMsgs.Other2Text = Trim(Left(InsName(5), 30))
''''''''                frmEventMsgs.Other3Text = Trim(Left(InsName(6), 30))
''''''''                frmEventMsgs.Other4Text = ""
''''''''                frmEventMsgs.Show 1
''''''''                If (frmEventMsgs.Result = 1) Then
''''''''                    PatientId = 0
''''''''                    txtPatient.SetFocus
''''''''                ElseIf (frmEventMsgs.Result = 2) Then
''''''''                    If (Len(InsName(1)) > 75) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(1), 77, Len(InsName(1)) - 76))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    ElseIf (Len(InsName(1)) > 56) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(1), 56, Len(InsName(1)) - 55))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    End If
''''''''                ElseIf (frmEventMsgs.Result = 4) Then
''''''''                    If (Len(InsName(2)) > 75) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(2), 77, Len(InsName(2)) - 76))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    ElseIf (Len(InsName(2)) > 56) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(2), 56, Len(InsName(2)) - 55))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    End If
''''''''                ElseIf (frmEventMsgs.Result = 3) Then
''''''''                    If (Len(InsName(3)) > 75) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(3), 77, Len(InsName(3)) - 76))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    ElseIf (Len(InsName(3)) > 56) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(3), 56, Len(InsName(3)) - 55))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    End If
''''''''                ElseIf (frmEventMsgs.Result = 5) Then
''''''''                    If (Len(InsName(4)) > 75) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(4), 77, Len(InsName(4)) - 76))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    ElseIf (Len(InsName(4)) > 56) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(4), 56, Len(InsName(4)) - 55))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    End If
''''''''                ElseIf (frmEventMsgs.Result = 6) Then
''''''''                    If (Len(InsName(5)) > 75) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(5), 77, Len(InsName(5)) - 76))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    ElseIf (Len(InsName(5)) > 56) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(5), 56, Len(InsName(5)) - 55))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    End If
''''''''                ElseIf (frmEventMsgs.Result = 7) Then
''''''''                    If (Len(InsName(6)) > 75) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(6), 77, Len(InsName(6)) - 76))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    ElseIf (Len(InsName(6)) > 56) Then
''''''''                        TransitionInsurerOn = True
''''''''                        PatInsId = Val(Mid(InsName(6), 56, Len(InsName(6)) - 55))
''''''''                        PatientId = PatId
''''''''                        txtPatient.Text = AName
''''''''                        txtAmount.SetFocus
''''''''                        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''                            cmdDisburse.Visible = True
''''''''                            cmdDisburse.SetFocus
''''''''                        End If
''''''''                        MedicareOn = ApplTemp.ApplIsInsurerMedicare(PatInsId, False)
''''''''                    End If
''''''''                End If
''''''''            Else
''''''''                PatientId = 0
''''''''                txtPatient.Text = ""
''''''''                txtPatient.SetFocus
''''''''            End If
''''''''        End If
''''''''    Else
''''''''        PatientId = PatId
''''''''        txtPatient.Text = AName
''''''''        txtAmount.SetFocus
''''''''        If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") And (Trim(txtInsurer.Text) <> "") Then
''''''''            cmdDisburse.Visible = True
''''''''            cmdDisburse.SetFocus
''''''''        End If
''''''''    End If
''''''''    Set ApplTemp = Nothing
''''''''    If (TriggerDisburse) And (PatientId > 0) Then
''''''''        Call cmdDisburse_Click
''''''''        TriggerDisburse = False
''''''''    End If
''''''''    frmPatientSchedulerSearch.PatientNumberOn = False
''''''''Else
''''''''    frmPatientSchedulerSearch.PatientNumberOn = False
''''''''    Unload frmPatientSchedulerSearch
''''''''    txtPatient.SetFocus
''''''''End If
End Sub

Private Sub txtPatient_KeyPress(KeyAscii As Integer)
Call txtPatient_Click
KeyAscii = 0
End Sub

Private Sub txtAmount_KeyPress(KeyAscii As Integer)
'''''''''Dim Temp As String
'''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
'''''''''    If Not (IsCurrency(Chr(KeyAscii))) Then
'''''''''        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''    End If
'''''''''Else
'''''''''    Call DisplayDollarAmount(txtAmount.Text, Temp)
'''''''''    txtAmount.Text = Trim(Temp)
'''''''''    RunningCheckBalance = Trim(txtAmount.Text)
'''''''''    txtInsurer.SetFocus
'''''''''End If
End Sub

Private Sub txtAmount_Validate(Cancel As Boolean)
Call txtAmount_KeyPress(13)
End Sub

Private Sub txtInsurer_Click()
'''''''''Dim i As Integer
'''''''''Dim PatId As Long
'''''''''Dim TriggerDisburse As Boolean
'''''''''Dim b1 As Boolean, b2 As Boolean
'''''''''Dim r1 As String, r2 As String
'''''''''Dim Temp As String, AName As String
'''''''''Dim InsName(6) As String
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''InsName(1) = ""
'''''''''InsName(2) = ""
'''''''''InsName(3) = ""
'''''''''InsName(4) = ""
'''''''''InsName(5) = ""
'''''''''InsName(6) = ""
'''''''''frmSelectDialogue.InsurerSelected = ""
'''''''''Call frmSelectDialogue.BuildSelectionDialogue("INSURERONLY")
'''''''''frmSelectDialogue.Show 1
'''''''''If (frmSelectDialogue.SelectionResult) Then
'''''''''    If (Len(frmSelectDialogue.Selection) > 56) Then
'''''''''        txtInsurer.Text = frmSelectDialogue.Selection
'''''''''        InsurerId = Val(Trim(Mid(frmSelectDialogue.Selection, 56, Len(frmSelectDialogue.Selection) - 55)))
'''''''''        Set ApplTemp = New ApplicationTemplates
'''''''''        If (ApplTemp.ApplIsInsurerMatch(PatientId, PatPol1, PatPol2, InsurerId, PatInsId, PatPolId)) Then
'''''''''            MedicareOn = ApplTemp.ApplIsInsurerMedicare(InsurerId, False)
'''''''''            txtAmount.Locked = False
'''''''''            txtCheck.Locked = False
'''''''''            cmdDisburse.Text = "Disburse"
'''''''''            txtCheck.SetFocus
'''''''''        Else
'''''''''            If (PatientId > 0) Then
'''''''''                frmEventMsgs.Header = "Patient does not have this payer, Override ?"
'''''''''                frmEventMsgs.AcceptText = ""
'''''''''                frmEventMsgs.RejectText = "Ok"
'''''''''                frmEventMsgs.CancelText = "No"
'''''''''                frmEventMsgs.Other0Text = ""
'''''''''                frmEventMsgs.Other1Text = ""
'''''''''                frmEventMsgs.Other2Text = ""
'''''''''                frmEventMsgs.Other3Text = ""
'''''''''                frmEventMsgs.Other4Text = ""
'''''''''                frmEventMsgs.Show 1
'''''''''                If (frmEventMsgs.Result = 2) Then
'''''''''                    Call ApplTemp.ApplGetBatchPayers(PatientId, InsName(1), InsName(2), InsName(3), InsName(4), InsName(5), InsName(6))
'''''''''                    frmEventMsgs.Header = "Override with which Patient Insurer ?"
'''''''''                    frmEventMsgs.AcceptText = "Cancel"
'''''''''                    frmEventMsgs.RejectText = Trim(Left(InsName(1), 30))
'''''''''                    frmEventMsgs.CancelText = Trim(Left(InsName(2), 30))
'''''''''                    frmEventMsgs.Other0Text = Trim(Left(InsName(3), 30))
'''''''''                    frmEventMsgs.Other1Text = Trim(Left(InsName(4), 30))
'''''''''                    frmEventMsgs.Other2Text = Trim(Left(InsName(5), 30))
'''''''''                    frmEventMsgs.Other3Text = Trim(Left(InsName(6), 30))
'''''''''                    frmEventMsgs.Other4Text = ""
'''''''''                    frmEventMsgs.Show 1
'''''''''                    If (frmEventMsgs.Result = 1) Then
'''''''''                        InsurerId = 0
'''''''''                        txtInsurer.Text = ""
'''''''''                        txtInsurer.SetFocus
'''''''''                    ElseIf (frmEventMsgs.Result = 2) Then
'''''''''                        If (Len(InsName(1)) > 75) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(1), 77, Len(InsName(1)) - 76))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        ElseIf (Len(InsName(1)) > 56) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(1), 56, Len(InsName(1)) - 55))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        End If
'''''''''                    ElseIf (frmEventMsgs.Result = 4) Then
'''''''''                        If (Len(InsName(2)) > 75) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(2), 77, Len(InsName(2)) - 76))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        ElseIf (Len(InsName(2)) > 56) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(2), 56, Len(InsName(2)) - 55))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        End If
'''''''''                    ElseIf (frmEventMsgs.Result = 3) Then
'''''''''                        If (Len(InsName(3)) > 75) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(3), 77, Len(InsName(3)) - 76))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        ElseIf (Len(InsName(3)) > 56) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(3), 56, Len(InsName(3)) - 55))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        End If
'''''''''                    ElseIf (frmEventMsgs.Result = 5) Then
'''''''''                        If (Len(InsName(4)) > 75) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(4), 77, Len(InsName(4)) - 76))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        ElseIf (Len(InsName(4)) > 56) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(4), 56, Len(InsName(4)) - 55))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        End If
'''''''''                    ElseIf (frmEventMsgs.Result = 6) Then
'''''''''                        If (Len(InsName(5)) > 75) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(5), 77, Len(InsName(5)) - 76))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        ElseIf (Len(InsName(5)) > 56) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(5), 56, Len(InsName(5)) - 55))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        End If
'''''''''                    ElseIf (frmEventMsgs.Result = 7) Then
'''''''''                        If (Len(InsName(6)) > 75) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(6), 77, Len(InsName(6)) - 76))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        ElseIf (Len(InsName(6)) > 56) Then
'''''''''                            TransitionInsurerOn = True
'''''''''                            PatInsId = Val(Mid(InsName(6), 56, Len(InsName(6)) - 55))
'''''''''                            txtAmount.Locked = False
'''''''''                            txtCheck.Locked = False
'''''''''                            cmdDisburse.Text = "Disburse"
'''''''''                        End If
'''''''''                    End If
'''''''''                End If
'''''''''            End If
'''''''''            txtCheck.SetFocus
'''''''''        End If
'''''''''    Else
'''''''''        txtInsurer.Text = ""
'''''''''    End If
'''''''''    Set ApplTemp = Nothing
'''''''''End If
End Sub

Private Sub txtInsurer_KeyPress(KeyAscii As Integer)
Call txtInsurer_Click
KeyAscii = 0
End Sub

Private Sub txtDate_KeyPress(KeyAscii As Integer)
Dim ADate As String
Dim UrDate As String
Dim EditWindow As String
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDate.SetFocus
        txtDate.Text = ""
        SendKeys "{Home}"
    Else
        If (Trim(txtDate.Text) <> "") Then
            Call UpdateDisplay(txtDate, "D")
            EditWindow = CheckConfigCollection("EDITWINDOW")
            If (Trim(EditWindow) <> "") Then
                If (InStrPS(EditWindow, "/") = 0) Then
                    EditWindow = Mid(EditWindow, 1, 2) + "/" + Mid(EditWindow, 3, 2) + "/" + Mid(EditWindow, 5, 4)
                End If
                ADate = Mid(EditWindow, 7, 4) + Mid(EditWindow, 1, 2) + Mid(EditWindow, 4, 2)
                UrDate = Mid(txtDate.Text, 7, 4) + Mid(txtDate.Text, 1, 2) + Mid(txtDate.Text, 4, 2)
                If (ADate >= UrDate) Then
                    frmEventMsgs.Header = "Date cannot be older than the close date"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    KeyAscii = 0
                    txtDate.Text = ""
                    txtDate.SetFocus
                End If
            End If
        Else
            Call UpdateDisplay(txtDate, "D")
        End If
    End If
End If
End Sub

Private Sub txtDate_Validate(Cancel As Boolean)
If (Trim(txtDate.Text) <> "") Then
    Call txtDate_KeyPress(13)
End If
End Sub

Private Sub MSFlexGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
RightArrowOn = False
MSFlexGrid1.AllowBigSelection = False
If (KeyCode = vbKeyRight) And (Shift = 0) Then
    RightArrowOn = True
    CurrentRow = MSFlexGrid1.RowSel
    CurrentCol = MSFlexGrid1.ColSel
ElseIf (KeyCode = vbKeyDown) And (Shift = 0) Then
    RightArrowOn = True
    CurrentRow = MSFlexGrid1.RowSel
    CurrentCol = MSFlexGrid1.ColSel
ElseIf (KeyCode = vbKeyUp) And (Shift = 0) Then
    RightArrowOn = True
    CurrentRow = MSFlexGrid1.RowSel
    CurrentCol = MSFlexGrid1.ColSel
ElseIf (KeyCode = vbKeyLeft) And (Shift = 0) Then
    CurrentRow = MSFlexGrid1.RowSel
    CurrentCol = MSFlexGrid1.ColSel
End If
End Sub

Private Sub MSFlexGrid1_KeyPress(KeyAscii As Integer)
''''''''If (CurrentRow <> MSFlexGrid1.RowSel) Then
''''''''    CurrentRow = MSFlexGrid1.RowSel
''''''''End If
''''''''MSFlexGrid1.AllowBigSelection = True
''''''''MSFlexGrid1.Highlight = flexHighlightAlways
''''''''MSFlexGrid1.AllowBigSelection = False
''''''''CurrentCol = MSFlexGrid1.ColSel
''''''''If (MSFlexGrid1.TextMatrix(CurrentRow, 1) <> "") Then
''''''''    If (KeyAscii = 13) Or (KeyAscii = 10) Then
''''''''        If ((CurrentCol > 4) And (CurrentCol < 8)) Or (CurrentCol = 9) Then
''''''''            Call MsFlexGrid1_LeaveCell
''''''''            Call MsFlexGrid1_EnterCell
''''''''        Else
''''''''            Call MSFlexGrid1_Click
''''''''        End If
''''''''    Else
''''''''        If (CurrentCol > 4) And (CurrentCol < 8) Or (CurrentCol = 9) Or (CurrentCol = 12) Then
''''''''            If (KeyAscii >= 32) And (KeyAscii < 125) And (KeyAscii <> 39) And (KeyAscii <> 37) Then
''''''''                If (CurrentCol = 5) Then
''''''''                    Call txtAllow_KeyPress(KeyAscii)
''''''''                ElseIf (CurrentCol = 6) Then
''''''''                    Call txtPayAmt_KeyPress(KeyAscii)
''''''''                ElseIf (CurrentCol = 7) Then
''''''''                    Call txtAdjAmt_KeyPress(KeyAscii)
''''''''                ElseIf (CurrentCol = 9) Then
''''''''                    Call txtRsp_KeyPress(KeyAscii)
''''''''                ElseIf (CurrentCol = 12) Then
''''''''                    Call txtDeny_KeyPress(KeyAscii)
''''''''                End If
''''''''                If (KeyAscii <> 0) Then
''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) + Chr(KeyAscii)
''''''''                End If
''''''''            ElseIf (KeyAscii = 8) Or (KeyAscii = 126) Then
''''''''                If (Len(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) > 1) Then
''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = Left(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol), Len(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) - 1)
''''''''                Else
''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
''''''''                End If
''''''''            Else
''''''''                Exit Sub
''''''''            End If
''''''''            If (CurrentCol = 5) Then
''''''''                txtAllow.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''            ElseIf (CurrentCol = 6) Then
''''''''                txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''            ElseIf (CurrentCol = 7) Then
''''''''                txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''            ElseIf (CurrentCol = 9) Then
''''''''                txtRsp.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''            ElseIf (CurrentCol = 12) Then
''''''''                txtDeny.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''            End If
''''''''        End If
''''''''    End If
''''''''Else
''''''''    KeyAscii = 0
''''''''End If
End Sub

Private Sub MSFlexGrid1_Click()
''''''''''Dim i As Integer
''''''''''Dim Amt As Single, Chg As Single, ATotal As Single
''''''''''Dim Paid As Single, Adjs As Single
''''''''''Dim Temp As String
''''''''''Dim ProcessBalance As Boolean
''''''''''ProcessBalance = False
''''''''''lblAdj.Visible = False
''''''''''txtAdj.Visible = False
''''''''''chkInclude.Visible = False
''''''''''MSFlexGrid1.AllowBigSelection = False
''''''''''If (MSFlexGrid1.ColSel <> 14) And (MSFlexGrid1.ColSel <> 5) And (MSFlexGrid1.ColSel <> 0) Then
''''''''''    Call MsFlexGrid1_EnterCell
''''''''''    Exit Sub
''''''''''ElseIf (MSFlexGrid1.ColSel = 5) Then
''''''''''    Exit Sub
''''''''''End If
''''''''''If (MSFlexGrid1.Row > 0) And (Trim(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, 1)) <> "") Then
''''''''''    If (CurrentRow > 0) Then
''''''''''        If (Trim(txtAllow.Text) <> "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 5) = Trim(txtAllow.Text)
''''''''''            ProcessBalance = True
''''''''''        End If
''''''''''        If (Trim(txtPayAmt.Text) <> "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
''''''''''            ProcessBalance = True
''''''''''        End If
''''''''''        If (Trim(txtAdjAmt.Text) <> "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 7) = Trim(txtAdjAmt.Text)
''''''''''            ProcessBalance = True
''''''''''        End If
''''''''''        If (Trim(txtRsp.Text) <> "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 9) = Trim(txtRsp.Text)
''''''''''        End If
''''''''''        If (Trim(txtDeny.Text) <> "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 12) = Trim(txtDeny.Text)
''''''''''        End If
''''''''''        If (Trim(txtAdj.Text) <> "") Then
''''''''''            MSFlexGrid2.TextMatrix(CurrentRow, 1) = Trim(txtAdj.Text)
''''''''''            MSFlexGrid2.TextMatrix(CurrentRow, 2) = "F"
''''''''''            If (chkInclude.Value = 1) Then
''''''''''                MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T"
''''''''''            End If
''''''''''        End If
''''''''''        If (ProcessBalance) And (CurrentCol <> 5) Then
''''''''''            Chg = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 3)))
''''''''''            Paid = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)))
''''''''''            Adjs = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)))
''''''''''            ATotal = Chg - (Paid + Adjs)
''''''''''            Call DisplayDollarAmount(Trim(Str(ATotal)), Temp)
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 8) = Trim(Temp)
''''''''''        End If
''''''''''    End If
''''''''''    If (Val(Trim(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, 4))) > 0) Then
''''''''''        CurrentRow = MSFlexGrid1.Row
''''''''''        txtAdj.Text = ""
''''''''''        chkInclude.Value = 0
''''''''''        txtAllow.Text = ""
''''''''''        txtPayAmt.Text = ""
''''''''''        txtAdjAmt.Text = ""
''''''''''        txtRsp.Text = ""
''''''''''        txtDeny.Text = ""
''''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 5)) <> "") Then
'''''''''''            txtAllow.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 5))
'''''''''''            FirstTimeSet = True
'''''''''''            If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)) = "") And Not (ProcessBalance) Then
'''''''''''                Call CalculateFields(CurrentRow)
'''''''''''            End If
''''''''''        End If
''''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)) <> "") Then
''''''''''            txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6))
''''''''''            FirstTimeSet = True
''''''''''        End If
''''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)) <> "") Then
''''''''''            txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))
''''''''''            FirstTimeSet = True
''''''''''        End If
''''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 9)) <> "") Then
''''''''''            txtRsp.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 9))
''''''''''        End If
''''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 12)) <> "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 12) = UCase(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 12)))
''''''''''            txtDeny.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 12))
''''''''''        End If
''''''''''        If (txtAllow.Visible) Then
''''''''''            txtAllow.SetFocus
''''''''''        End If
''''''''''' Display Comment
''''''''''        lblAdj.Visible = True
''''''''''        txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
''''''''''        txtAdj.Visible = True
''''''''''        chkInclude.Visible = True
''''''''''        If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
''''''''''            chkInclude.Value = 1
''''''''''        Else
''''''''''            chkInclude.Value = 0
''''''''''        End If
''''''''''    Else
''''''''''        frmEventMsgs.Header = "No Open Balance"
''''''''''        frmEventMsgs.AcceptText = ""
''''''''''        frmEventMsgs.RejectText = "OK"
''''''''''        frmEventMsgs.CancelText = ""
''''''''''        frmEventMsgs.Other0Text = ""
''''''''''        frmEventMsgs.Other1Text = ""
''''''''''        frmEventMsgs.Other2Text = ""
''''''''''        frmEventMsgs.Other3Text = ""
''''''''''        frmEventMsgs.Other4Text = ""
''''''''''        frmEventMsgs.Show 1
''''''''''    End If
''''''''''Else
''''''''''    txtAdj.Text = ""
''''''''''    txtAllow.Text = ""
''''''''''    txtPayAmt.Text = ""
''''''''''    txtAdjAmt.Text = ""
''''''''''    txtRsp.Text = ""
''''''''''    txtDeny.Text = ""
''''''''''End If
''''''''''Call SetDisburseRemaining
End Sub

Private Sub MsFlexGrid1_EnterCell()
''''''''''Dim RowNow As Integer
''''''''''Dim ColNow As Integer
''''''''''If (MSFlexGrid1.TextMatrix(CurrentRow, 1) = "") Then
''''''''''    Exit Sub
''''''''''End If
''''''''''MSFlexGrid1.AllowBigSelection = False
''''''''''CurrentCol = MSFlexGrid1.ColSel
''''''''''CurrentRow = MSFlexGrid1.RowSel
''''''''''' Display Comment
''''''''''lblAdj.Visible = True
''''''''''txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
''''''''''txtAdj.Visible = True
''''''''''chkInclude.Visible = True
''''''''''If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
''''''''''    chkInclude.Value = 1
''''''''''Else
''''''''''    chkInclude.Value = 0
''''''''''End If
''''''''''If (CurrentCol = 5) Then
''''''''''    If (RightArrowOn) And (PreviousCol = 5) Then
''''''''''        ColNow = CurrentCol
''''''''''        RowNow = CurrentRow
''''''''''        CurrentCol = PreviousCol
''''''''''        CurrentRow = PreviousRow
''''''''''        If (Trim(txtAllow.Text) = "") Then
''''''''''            txtAllow.Text = MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)
''''''''''        End If
''''''''''        Call txtAllow_KeyPress(13)
''''''''''        If (Trim(txtAllow.Text) = "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
''''''''''        End If
''''''''''        CurrentCol = ColNow
''''''''''        CurrentRow = RowNow
''''''''''        RightArrowOn = False
''''''''''    End If
''''''''''    txtAllow.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''''    FirstTimeSet = True
''''''''''ElseIf (CurrentCol = 6) Then
''''''''''    If (RightArrowOn) And (PreviousCol = 5) Then
''''''''''        ColNow = CurrentCol
''''''''''        RowNow = CurrentRow
''''''''''        CurrentCol = PreviousCol
''''''''''        CurrentRow = PreviousRow
''''''''''        If (Trim(txtAllow.Text) = "") Then
''''''''''            txtAllow.Text = MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)
''''''''''        End If
''''''''''        Call txtAllow_KeyPress(13)
''''''''''        If (Trim(txtAllow.Text) = "") Then
''''''''''            MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
''''''''''        End If
''''''''''        CurrentCol = ColNow
''''''''''        CurrentRow = RowNow
''''''''''        RightArrowOn = False
''''''''''    End If
''''''''''    txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''''    FirstTimeSet = True
''''''''''ElseIf (CurrentCol = 7) Then
''''''''''    txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''''    FirstTimeSet = True
''''''''''ElseIf (CurrentCol = 9) Then
''''''''''    txtRsp.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
''''''''''ElseIf (CurrentCol = 12) Then
''''''''''    txtDeny.Text = MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)
''''''''''    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = UCase(Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)))
''''''''''ElseIf (CurrentCol = 0) Then
''''''''''    MSFlexGrid1_Click
''''''''''End If
End Sub

Private Sub MsFlexGrid1_LeaveCell()
'''''''''If (MSFlexGrid1.TextMatrix(CurrentRow, 1) = "") Then
'''''''''    Exit Sub
'''''''''End If
'''''''''MSFlexGrid1.AllowBigSelection = True
'''''''''CurrentCol = MSFlexGrid1.ColSel
'''''''''CurrentRow = MSFlexGrid1.RowSel
'''''''''' Display Comment
'''''''''lblAdj.Visible = True
'''''''''txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
'''''''''txtAdj.Visible = True
'''''''''chkInclude.Visible = True
'''''''''If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
'''''''''    chkInclude.Value = 1
'''''''''Else
'''''''''    chkInclude.Value = 0
'''''''''End If
'''''''''If (CurrentCol = 5) Then
'''''''''    RightArrowOn = True
'''''''''    PreviousRow = CurrentRow
'''''''''    PreviousCol = CurrentCol
'''''''''ElseIf (CurrentCol = 6) Then
'''''''''    Call txtPayAmt_KeyPress(13)
'''''''''    If (Trim(txtPayAmt.Text) = "") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
'''''''''    End If
'''''''''ElseIf (CurrentCol = 7) Then
'''''''''    Call txtAdjAmt_KeyPress(13)
'''''''''    If (Trim(txtAdjAmt.Text) = "") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
'''''''''    End If
'''''''''ElseIf (CurrentCol = 9) Then
'''''''''    Call txtRsp_KeyPress(13)
'''''''''    If (Trim(txtRsp.Text) = "") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
'''''''''    End If
'''''''''ElseIf (CurrentCol = 12) Then
'''''''''    Call txtDeny_KeyPress(13)
'''''''''    If (Trim(txtDeny.Text) = "D") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = "D"
'''''''''    End If
'''''''''    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = UCase(Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)))
'''''''''End If
'''''''''MSFlexGrid1.Highlight = flexHighlightAlways
'''''''''MSFlexGrid1.AllowBigSelection = False
End Sub

Private Sub txtDeny_Validate(Cancel As Boolean)
''''''''If (Trim(txtDeny.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 12))) Then
''''''''    Call txtDeny_KeyPress(13)
''''''''End If
End Sub

Private Sub txtDeny_KeyPress(KeyAscii As Integer)
''''''''Dim AdjCode As String
''''''''Dim RefCode As String
''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
''''''''    txtDeny.Text = UCase(Chr(KeyAscii))
''''''''    KeyAscii = Asc(txtDeny.Text)
''''''''    If Not (GetAdjType(txtDeny.Text, AdjCode, RefCode)) Then
'''''''''    If (txtDeny.Text <> "D") And (txtDeny.Text <> " ") And (txtDeny.Text <> "") Then
''''''''        frmEventMsgs.Header = "Must be a valid Adjustment Type."
''''''''        frmEventMsgs.AcceptText = ""
''''''''        frmEventMsgs.RejectText = "Ok"
''''''''        frmEventMsgs.CancelText = ""
''''''''        frmEventMsgs.Other0Text = ""
''''''''        frmEventMsgs.Other1Text = ""
''''''''        frmEventMsgs.Other2Text = ""
''''''''        frmEventMsgs.Other3Text = ""
''''''''        frmEventMsgs.Other4Text = ""
''''''''        frmEventMsgs.Show 1
''''''''        KeyAscii = 0
''''''''    Else
''''''''        txtDeny.Text = UCase(txtDeny.Text)
''''''''        If (CurrentRow > 0) Then
''''''''            If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)) = "") Then
''''''''                If (Trim(RefCode) = "") Then
''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, 6) = "0.00"
''''''''                End If
''''''''            End If
''''''''            If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)) = "") Then
''''''''                If (Trim(RefCode) = "") Then
''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, 7) = "0.00"
''''''''                End If
''''''''            End If
''''''''            Call CalculateBalance(CurrentRow)
''''''''        End If
''''''''    End If
''''''''End If
End Sub

Private Sub txtAllow_Validate(Cancel As Boolean)
'''''If (Trim(txtAllow.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 5))) Then
'''''    Call txtAllow_KeyPress(13)
'''''End If
End Sub

Private Sub txtAllow_KeyPress(KeyAscii As Integer)
'''''''''Dim Amt As Single
'''''''''Dim Temp As String
'''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
'''''''''    If Not (IsCurrency(Chr(KeyAscii))) Then
'''''''''        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''    Else
'''''''''        If (FirstTimeSet) Then
'''''''''            txtAllow.Text = ""
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 5) = ""
'''''''''            FirstTimeSet = False
'''''''''        End If
'''''''''    End If
'''''''''Else
'''''''''    If (CurrentRow > 0) And (Trim(txtAllow.Text) <> "") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, 5) = Trim(txtAllow.Text)
'''''''''        If Not (CalculateFields(CurrentRow)) Then
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 5) = Temp
'''''''''            frmEventMsgs.Header = "Insufficient funds to apply"
'''''''''            frmEventMsgs.AcceptText = ""
'''''''''            frmEventMsgs.RejectText = "Ok"
'''''''''            frmEventMsgs.CancelText = ""
'''''''''            frmEventMsgs.Other0Text = ""
'''''''''            frmEventMsgs.Other1Text = ""
'''''''''            frmEventMsgs.Other2Text = ""
'''''''''            frmEventMsgs.Other3Text = ""
'''''''''            frmEventMsgs.Other4Text = ""
'''''''''            frmEventMsgs.Show 1
'''''''''            txtAllow.Text = ""
'''''''''            If (txtAllow.Visible) Then
'''''''''                txtAllow.SetFocus
'''''''''            End If
'''''''''        Else
'''''''''            If (txtPayAmt.Visible) Then
'''''''''                txtPayAmt.SetFocus
'''''''''            End If
'''''''''        End If
'''''''''    End If
'''''''''End If
End Sub

Private Sub txtPayAmt_Validate(Cancel As Boolean)
If (Trim(txtPayAmt.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6))) Then
    Call txtPayAmt_KeyPress(13)
End If
End Sub

Private Sub txtPayAmt_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    Else
        If (FirstTimeSet) Then
            txtPayAmt.Text = ""
            MSFlexGrid1.TextMatrix(CurrentRow, 6) = ""
            FirstTimeSet = False
        End If
    End If
Else
    Call DisplayDollarAmount(txtPayAmt.Text, Temp)
    txtPayAmt.Text = Trim(Temp)
    If (MoneyLeft(CurrentRow) < val(Trim(txtPayAmt.Text))) Then
        frmEventMsgs.Header = "Insufficient funds to apply"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPayAmt.Text = ""
        If (txtPayAmt.Visible) Then
            txtPayAmt.SetFocus
        End If
    End If
    If Not (VerifyAgainstOpenBalance(CurrentRow, val(Trim(txtPayAmt.Text)), val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))))) And (val(Trim(txtPayAmt.Text)) <> 0) Then
        frmEventMsgs.Header = "Payment exceeds Open Balance"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Apply"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 4) Then
            txtPayAmt.Text = ""
            If (txtPayAmt.Visible) Then
                txtPayAmt.SetFocus
            End If
        Else
            If (CurrentRow > 0) Then
                MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
                Call CalculateBalance(CurrentRow)
            End If
            If (txtAdjAmt.Visible) Then
                txtAdjAmt.SetFocus
            End If
        End If
    Else
        If (CurrentRow > 0) Then
            MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
            Call CalculateBalance(CurrentRow)
        End If
        If (txtAdjAmt.Visible) Then
            txtAdjAmt.SetFocus
        End If
    End If
End If
End Sub

Private Sub txtAdjAmt_Validate(Cancel As Boolean)
If (Trim(txtAdjAmt.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))) Then
    Call txtAdjAmt_KeyPress(13)
End If
End Sub

Private Sub txtAdjAmt_KeyPress(KeyAscii As Integer)
'''''''''Dim Temp As String
'''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
'''''''''    If Not (IsCurrency(Chr(KeyAscii))) Then
'''''''''        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''    Else
'''''''''        If (FirstTimeSet) Then
'''''''''            txtAdjAmt.Text = ""
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 7) = ""
'''''''''            FirstTimeSet = False
'''''''''        End If
'''''''''    End If
'''''''''Else
'''''''''    Call DisplayDollarAmount(txtAdjAmt.Text, Temp)
'''''''''    txtAdjAmt.Text = Trim(Temp)
'''''''''    If (CurrentRow > 0) Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, 7) = Trim(txtAdjAmt.Text)
'''''''''        Call CalculateBalance(CurrentRow)
'''''''''    End If
'''''''''    If (txtAllow.Visible) Then
'''''''''        txtAllow.SetFocus
'''''''''    End If
'''''''''End If
End Sub

Private Sub txtAdj_Validate(Cancel As Boolean)
Call txtAdj_KeyPress(13)
End Sub

Private Sub txtAdj_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If (CurrentRow > 0) Then
        MSFlexGrid2.TextMatrix(CurrentRow, 1) = Trim(txtAdj.Text)
    End If
    KeyAscii = 0
End If
End Sub

Private Sub chkInclude_Click()
If (CurrentRow > 0) Then
    MSFlexGrid2.TextMatrix(CurrentRow, 2) = "F"
    If (chkInclude.Value = 1) Then
        MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T"
    End If
End If
End Sub

Private Sub txtRsn_Click()
'''''''Dim z As Integer
'''''''frmSelectDialogue.InsurerSelected = ""
'''''''Call frmSelectDialogue.BuildSelectionDialogue("PaymentReason")
'''''''frmSelectDialogue.Show 1
'''''''If (frmSelectDialogue.SelectionResult) Then
'''''''    z = InStrPS(frmSelectDialogue.Selection, "-")
'''''''    If (z > 0) Then
'''''''        txtRsn.Text = Trim(Left(frmSelectDialogue.Selection, z - 1))
'''''''    Else
'''''''        txtRsn.Text = Trim(frmSelectDialogue.Selection)
'''''''    End If
'''''''    MSFlexGrid2.TextMatrix(CurrentRow, 3) = Trim(txtRsn.Text)
'''''''End If
End Sub

Private Sub txtRsn_KeyPress(KeyAscii As Integer)
Call txtRsn_Click
End Sub

Private Sub txtRsp_KeyPress(KeyAscii As Integer)
''''''Dim ApplTemp As ApplicationTemplates
''''''If (KeyAscii <> 13) Then
''''''    If (KeyAscii = Asc("p")) Then
''''''        KeyAscii = Asc("P")
''''''    End If
''''''    If ((KeyAscii < 49) Or (KeyAscii > 54)) And (KeyAscii <> Asc("P")) Then
''''''        frmEventMsgs.Header = "Valid Values are [123456P]"
''''''        frmEventMsgs.AcceptText = ""
''''''        frmEventMsgs.RejectText = "Ok"
''''''        frmEventMsgs.CancelText = ""
''''''        frmEventMsgs.Other0Text = ""
''''''        frmEventMsgs.Other1Text = ""
''''''        frmEventMsgs.Other2Text = ""
''''''        frmEventMsgs.Other3Text = ""
''''''        frmEventMsgs.Other4Text = ""
''''''        frmEventMsgs.Show 1
''''''        KeyAscii = 0
''''''        txtRsp.Text = ""
''''''        If (txtRsp.Visible) Then
''''''            txtRsp.SetFocus
''''''        End If
''''''    Else
''''''        txtRsp.Text = ""
''''''        MSFlexGrid1.TextMatrix(CurrentRow, 9) = ""
''''''        Set ApplTemp = New ApplicationTemplates
''''''        If Not (ApplTemp.ApplIsValidNextParty(PatPol1, PatPol2, Chr(KeyAscii))) Then
''''''            frmEventMsgs.Header = "Not a valid party"
''''''            frmEventMsgs.AcceptText = ""
''''''            frmEventMsgs.RejectText = "Ok"
''''''            frmEventMsgs.CancelText = ""
''''''            frmEventMsgs.Other0Text = ""
''''''            frmEventMsgs.Other1Text = ""
''''''            frmEventMsgs.Other2Text = ""
''''''            frmEventMsgs.Other3Text = ""
''''''            frmEventMsgs.Other4Text = ""
''''''            frmEventMsgs.Show 1
''''''            KeyAscii = 0
''''''            txtRsp.Text = ""
''''''            If (txtRsp.Visible) Then
''''''                txtRsp.SetFocus
''''''            End If
''''''        End If
''''''    End If
''''''Else
''''''    Set ApplTemp = New ApplicationTemplates
''''''    If Not (ApplTemp.ApplIsValidNextParty(PatPol1, PatPol2, txtRsp.Text)) Then
''''''        frmEventMsgs.Header = "Not a valid party"
''''''        frmEventMsgs.AcceptText = ""
''''''        frmEventMsgs.RejectText = "Ok"
''''''        frmEventMsgs.CancelText = ""
''''''        frmEventMsgs.Other0Text = ""
''''''        frmEventMsgs.Other1Text = ""
''''''        frmEventMsgs.Other2Text = ""
''''''        frmEventMsgs.Other3Text = ""
''''''        frmEventMsgs.Other4Text = ""
''''''        frmEventMsgs.Show 1
''''''        KeyAscii = 0
''''''        txtRsp.Text = ""
''''''        If (txtRsp.Visible) Then
''''''            txtRsp.SetFocus
''''''        End If
''''''    End If
''''''End If
End Sub

Private Sub txtRsp_Validate(Cancel As Boolean)
'''''''If ((txtRsp.Text) <> "") Then
'''''''    Call txtRsp_KeyPress(13)
'''''''End If
End Sub

Private Function LoadPayments(PatId As Long, InsId As Long, AllOn As Boolean, Reload As Boolean, BiOn As Boolean) As Integer
'''''''Dim Bs As Boolean
'''''''Dim UAOn1 As Boolean, UAOn2 As Boolean
'''''''Dim DPOn1 As Boolean, DPOn2 As Boolean
'''''''Dim TempLoad As Integer
'''''''Dim CurRow As Integer
'''''''Dim ApplList As ApplicationAIList
'''''''LoadPayments = -1
'''''''FirstTimeSet = False
'''''''GetCheckOn = False
'''''''If (PatId > 0) And (InsId > 0) Then
'''''''    CurRow = CurrentRow
'''''''    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
'''''''    lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''    lblAmountLeft.Visible = True
'''''''    lblAdj.Visible = False
'''''''    txtAdj.Visible = False
'''''''    chkInclude.Value = 0
'''''''    chkInclude.Visible = False
'''''''    Set ApplList = New ApplicationAIList
'''''''    Set ApplList.ApplGrid = MSFlexGrid1
'''''''    Set ApplList.ApplGrid1 = MSFlexGrid2
'''''''    If (Reload) Then
'''''''        lblPrint.Caption = "Loading Grid Entries"
'''''''        lblPrint.Visible = True
'''''''        Bs = MSFlexGrid1.Visible
'''''''        MSFlexGrid1.Visible = False
'''''''        DoEvents
'''''''        If (InsId > 0) And (InsId = InsurerId) Then
'''''''            LoadPayments = ApplList.ApplLoadPaymentsbyServices(PatientId, PatPol1, InsurerId, AllOn, UAOn1, DPOn1, BiOn)
'''''''            If (LoadPayments <> 0) Then
'''''''                If (PatPol2 > 0) Then
'''''''                    TempLoad = ApplList.ApplLoadPaymentsbyServices(PatientId, PatPol2, InsurerId, AllOn, UAOn2, DPOn2, BiOn)
'''''''                    If (TempLoad >= 0) Then
'''''''                        LoadPayments = TempLoad
'''''''                    End If
'''''''                End If
'''''''            End If
'''''''            If Not (TransitionInsurerOn) Then
'''''''                PatInsId = InsurerId
'''''''            End If
'''''''        ElseIf (InsId > 0) And (InsId <> InsurerId) Then
'''''''            LoadPayments = ApplList.ApplLoadPaymentsbyServices(PatientId, PatPol1, InsId, AllOn, UAOn1, DPOn1, BiOn)
'''''''            If (LoadPayments <> 0) Then
'''''''                If (PatPol2 > 0) Then
'''''''                    TempLoad = ApplList.ApplLoadPaymentsbyServices(PatientId, PatPol2, InsId, AllOn, UAOn2, DPOn2, BiOn)
'''''''                    If (TempLoad >= 0) Then
'''''''                        LoadPayments = TempLoad
'''''''                    End If
'''''''                End If
'''''''            End If
'''''''            If Not (TransitionInsurerOn) Then
'''''''                PatInsId = InsurerId
'''''''            End If
'''''''        End If
'''''''        lblPrint.Visible = False
'''''''        MSFlexGrid1.Visible = Bs
'''''''    End If
'''''''    Call ApplList.ApplGetAdjustmentCode(InsurerId, AdjustmentCode)
'''''''    Set ApplList = Nothing
'''''''    Call SetGridTotals
'''''''    cmdAll.Visible = False
'''''''    If (LoadPayments = 0) Then
'''''''        cmdAll.Visible = True
'''''''    End If
'''''''    cmdChkReset.Visible = cmdAll.Visible
'''''''    CurrentRow = CurRow
'''''''    If (CurrentRow < 0) Or (CurrentRow > 12) Or (CurrentRow > MSFlexGrid1.Row) Then
'''''''        CurrentRow = 1
'''''''    End If
'''''''    MSFlexGrid1.RowSel = CurrentRow
'''''''    If (UAOn1) Or (UAOn2) Then
'''''''        LoadPayments = -3
'''''''    ElseIf (DPOn1) Or (DPOn2) Then
'''''''        LoadPayments = -4
'''''''    End If
'''''''End If
End Function

Private Function MoneyLeft_5(chkAmt As String) As Single
Dim i As Integer
Dim Amt As Single
Amt = 0
MoneyLeft_5 = 0
For i = 1 To lstPmnt(2).Rows - 1
    Amt = Amt + (lstPmnt(2).TextMatrix(i, 7))
Next i
MoneyLeft_5 = chkAmt - Amt
End Function


Private Function MoneyLeft(ARow As Integer) As Single
Dim i As Integer
Dim Amt As Single
Amt = 0
MoneyLeft = 0
If (MSFlexGrid1.Visible) Then
    For i = 0 To MSFlexGrid1.Rows - 1
        If (i <> ARow) Then
            Amt = Amt + val(Trim(MSFlexGrid1.TextMatrix(i, 6)))
        End If
    Next i
End If
MoneyLeft = Int(((val(Trim(RunningCheckBalance)) - Amt) + 0.004) * 100) / 100
End Function

Private Function VerifyAgainstOpenBalance(ARow As Integer, Amt1 As Single, Amt2 As Single) As Boolean
VerifyAgainstOpenBalance = False
If (val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))) >= (Amt1 + Amt2)) Then
    VerifyAgainstOpenBalance = True
End If
End Function

Private Function CompleteCurrentPatient(PatId As Long) As Boolean
'''''''Dim zz As Integer
'''''''Dim z As Integer
'''''''Dim i As Integer, j As Integer
'''''''Dim InsId1 As Long
'''''''Dim TempInsId1 As Long
'''''''Dim InsrId As Long, TrnId As Long
'''''''Dim ItemId As Long, p1 As Long
'''''''Dim RcvId As Long, InsId As Long
'''''''Dim RecId As Long, TempIns As Long
'''''''Dim ComOn As Boolean, MedOn As Boolean
'''''''Dim IAmt(6) As Single
'''''''Dim k As Single, PAmt As Single
'''''''Dim InvDt As String
'''''''Dim a1 As String, a2 As String
'''''''Dim Cmt As String, st As String
'''''''Dim Srv As String, Amt As String
'''''''Dim CurInvId As String, Rsn As String
'''''''Dim SendTo As String, InvId As String
'''''''Dim AdjCode As String, RefCode As String
'''''''Dim ApplTemp As ApplicationTemplates
'''''''Dim ApplList As ApplicationAIList
'''''''CompleteCurrentPatient = False
'''''''' doing Adjustment Type
'''''''If (PatId > 0) Then
'''''''    CurInvId = ""
'''''''    k = MoneyLeft(0)
'''''''    If (MSFlexGrid1.Visible) Then
'''''''        lblPrint.Caption = "Posting ..."
'''''''        lblPrint.Visible = True
'''''''        DoEvents
'''''''        Call DisplayDollarAmount(Trim(Str(k)), RunningCheckBalance)
'''''''        lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''        lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(lblAmountLeft.Tag)
'''''''        Set ApplTemp = New ApplicationTemplates
'''''''' Write the Check Record
'''''''        Call ApplTemp.ApplPostCheckRecord(CheckId, InsurerId, "I", txtAmount.Text, RunningCheckBalance, txtCheck.Text, txtDate.Text, txtDate.Text)
'''''''        For i = 1 To MSFlexGrid1.Rows - 1
'''''''' Payments
'''''''            ComOn = False
'''''''            If (Val(Trim(MSFlexGrid1.TextMatrix(i, 6))) <> 0) Then
'''''''                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), PatInsId, InvDt)
'''''''                    If (RcvId > 0) Then
'''''''                        ItemId = Val(Trim(MSFlexGrid2.TextMatrix(i, 4)))
'''''''                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
'''''''                        Amt = Trim(MSFlexGrid1.TextMatrix(i, 6))
'''''''                        Rsn = Trim(MSFlexGrid2.TextMatrix(i, 3))
'''''''                        Cmt = Trim(MSFlexGrid2.TextMatrix(i, 1))
'''''''                        If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
'''''''                            ComOn = True
'''''''                        End If
'''''''                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "I", PatInsId, Amt, txtDate.Text, "P", "K", "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, "", "", 0)
'''''''                        If (Val(MSFlexGrid1.TextMatrix(i, 5)) > 0) Then
'''''''                            Call ApplTemp.ApplPostSrvAllowed(ItemId, MSFlexGrid1.TextMatrix(i, 5))
'''''''                        End If
'''''''                    End If
'''''''                End If
'''''''            End If
'''''''' Adjustments
'''''''            If (Val(Trim(MSFlexGrid1.TextMatrix(i, 7))) <> 0) Then
'''''''                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), PatInsId, InvDt)
'''''''                    If (RcvId > 0) Then
'''''''                        ItemId = Val(Trim(MSFlexGrid2.TextMatrix(i, 4)))
'''''''                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
'''''''                        Amt = MSFlexGrid1.TextMatrix(i, 7)
'''''''                        Rsn = Trim(MSFlexGrid2.TextMatrix(i, 3))
'''''''                        If Not (ComOn) Then
'''''''                            Cmt = MSFlexGrid2.TextMatrix(i, 1)
'''''''                            If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
'''''''                                ComOn = True
'''''''                            End If
'''''''                        Else ' because it was posted with the payment
'''''''                            ComOn = False
'''''''                            Cmt = ""
'''''''                        End If
'''''''                        If (Trim(MSFlexGrid1.TextMatrix(i, 12)) = "") Then
'''''''                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "I", PatInsId, Amt, txtDate.Text, AdjustmentCode, "K", "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, "", "", 0)
'''''''                        Else
'''''''                            If (GetAdjType(MSFlexGrid1.TextMatrix(i, 12), AdjCode, RefCode)) Then
'''''''                                If (Trim(RefCode) <> "") Then
'''''''                                    Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "I", PatInsId, Amt, txtDate.Text, AdjCode, "K", "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, "", "", 0)
'''''''                                End If
'''''''                            End If
'''''''                        End If
'''''''                    End If
'''''''                End If
'''''''            End If
'''''''' Denials
'''''''            If (Trim(MSFlexGrid1.TextMatrix(i, 12)) = "D") Then
'''''''                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), PatInsId, InvDt)
'''''''                    If (RcvId > 0) Then
'''''''                        ItemId = Val(Trim(MSFlexGrid2.TextMatrix(i, 4)))
'''''''                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
'''''''                        Amt = 0
'''''''                        Rsn = Trim(MSFlexGrid2.TextMatrix(i, 3))
'''''''                        Cmt = MSFlexGrid2.TextMatrix(i, 1)
'''''''                        ComOn = False
'''''''                        If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
'''''''                            ComOn = True
'''''''                        End If
'''''''                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "I", PatInsId, Amt, txtDate.Text, "D", "", "", "", Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, "", "", 0)
'''''''                    End If
'''''''                End If
'''''''            End If
'''''''        Next i
'''''''
'''''''' Queue to Send if Okay to do this
'''''''        lblPrint.Caption = "Batching ..."
'''''''        lblPrint.Visible = True
'''''''        Erase IAmt
'''''''        PAmt = 0
'''''''        InsId = 0
'''''''        InsrId = 0
'''''''        CurInvId = ""
'''''''        SendTo = Space(7)
'''''''        Set ApplList = New ApplicationAIList
'''''''        For i = 1 To MSFlexGrid1.Rows - 1
'''''''            RecId = 0
'''''''            InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''            If (CurInvId = "") Then
'''''''                CurInvId = InvId
'''''''            ElseIf (InvId <> CurInvId) Then
'''''''                RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(CurInvId, InsId, InvDt)
'''''''                If (Mid(SendTo, 1, 1) = "1") Then
'''''''                    If (IAmt(1) > 0) Then
'''''''                        If (ApplTemp.IsTransactionPending(RcvId, "I", TrnId, a1, a2)) Then
'''''''                            p1 = ApplTemp.BatchTransaction(RcvId, "I", Trim(Str(IAmt(1))), "B", True, 0, False, True, False)
'''''''                        Else
'''''''                            p1 = ApplTemp.BatchTransaction(RcvId, "I", Trim(Str(IAmt(1))), "B", True, 0, False, False, False)
'''''''                        End If
'''''''                    End If
'''''''                End If
'''''''                For z = 2 To 6
'''''''                    If (Mid(SendTo, z, 1) <> " ") Then
'''''''                        If (IAmt(z) > 0) Then
'''''''                            zz = z
'''''''                            Call ApplTemp.ApplGetNextPayerParty(PatientId, CurInvId, InvDt, "M", False, InsId, TempIns, a1, p1, zz)
'''''''                            If (TempIns > 0) Then
'''''''                                RecId = RcvId
''''''''                                InsId1 = InsurerId
'''''''                                If (TempInsId1 = PatientId) Then
'''''''                                    TempInsId1 = 0
'''''''                                End If
'''''''                                InsId1 = PatInsId
'''''''                                RcvId = ApplTemp.ApplBillSecondary(RcvId, InsId1, TempInsId1, True, True)
'''''''                                If (RcvId > 0) Then
'''''''                                    If (TempIns > 0) Then
'''''''                                        If (RecId = RcvId) Then
'''''''                                            RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(CurInvId, InsId1, InvDt)
'''''''                                        End If
'''''''                                    End If
'''''''                                    Call ApplTemp.IsTransactionPending(RecId, "I", TrnId, a1, a2)
'''''''                                    If (TrnId > 0) Then
'''''''                                        Call ApplList.ApplSetAllRcvTransStatus(RecId, "I", "S", "", "", False)
'''''''                                    End If
'''''''                                    If (ApplTemp.ApplIsInsurerPlanPaper(TempIns)) Then
'''''''                                        p1 = ApplTemp.BatchTransaction(RcvId, "I", Trim(Str(IAmt(z))), "P", True, 0, False, True, False)
'''''''                                    Else
'''''''                                        p1 = ApplTemp.BatchTransaction(RcvId, "I", Trim(Str(IAmt(z))), "B", True, 0, False, False, False)
'''''''                                    End If
'''''''                                End If
'''''''                            End If
'''''''                        End If
'''''''                    End If
'''''''                Next z
'''''''                If (Mid(SendTo, 7, 1) = "P") Then
'''''''                    If (PAmt > 0) Then
'''''''                        If (RecId > 0) Then
'''''''                            RcvId = RecId
'''''''                        End If
'''''''                        Call ApplTemp.BatchTransaction(RcvId, "P", Trim(Str(PAmt)), "P", False, 0, False, False, False)
'''''''                    End If
'''''''                End If
'''''''                Call ApplList.ApplSetReceivableBalance(CurInvId, "", True)
'''''''                Call ApplList.ApplCloseZeroedReceivables(CurInvId)
'''''''                Call ApplTemp.ApplReconcilePatientStatements(CurInvId)
'''''''                Erase IAmt
'''''''                PAmt = 0
'''''''                InsId = 0
'''''''                InsrId = 0
'''''''                CurInvId = InvId
'''''''                SendTo = Space(7)
'''''''            End If
'''''''            If (Val(Trim(MSFlexGrid1.TextMatrix(i, 8))) > 0) And ((Val(Trim(MSFlexGrid1.TextMatrix(i, 6))) > 0) Or (Val(Trim(MSFlexGrid1.TextMatrix(i, 7))) > 0)) Or (UCase(MSFlexGrid1.TextMatrix(i, 12)) = "D") Then
'''''''                st = Trim(MSFlexGrid1.TextMatrix(i, 9))
'''''''                If (st = "P") Then
'''''''                    Mid(SendTo, 7, 1) = "P"
'''''''                Else
'''''''                    Mid(SendTo, Val(st), 1) = st
'''''''                End If
'''''''                If (st = "P") Then
'''''''                    PAmt = PAmt + Val(Trim(MSFlexGrid1.TextMatrix(i, 8)))
'''''''                Else
'''''''                    IAmt(Val(st)) = IAmt(Val(st)) + Val(Trim(MSFlexGrid1.TextMatrix(i, 8)))
'''''''                End If
'''''''            End If
'''''''        Next i
'''''''        Set ApplList = Nothing
'''''''    End If
'''''''    Set ApplTemp = Nothing
'''''''    lblPrint.Visible = False
'''''''    TransitionInsurerOn = False
'''''''    CompleteCurrentPatient = True
'''''''End If
End Function

Private Function CompleteCurrentPatient_5(PatId As Long) As Boolean

Dim InsurerId As String
Dim CheckNumber As String
Dim CheckAmount As String
Dim CheckDate As String

InsurerId = "Patient"
CheckNumber = Trim(lstPmnt(0).TextMatrix(1, 3))
CheckAmount = Trim(lstPmnt(0).TextMatrix(1, 2))
CheckDate = Trim(lstPmnt(0).TextMatrix(1, 4))


Dim CheckId As Long
Dim RunningCheckBalance As String


Dim pcnt As Integer
Dim d As Integer, ARef As Integer
Dim w As Integer, w1 As Integer
Dim zz As Integer, p As Integer
Dim z As Integer, ICol As Integer
Dim i As Integer, j As Integer
Dim SP As Integer, scnt As Integer

Dim ComOn As Boolean, MedOn As Boolean
Dim AppealOn As Boolean
Dim XOver1 As Boolean
Dim XOver2 As Boolean

Dim k As Single
Dim JAmt As String
' represents amount for all 4 status APBU
Dim PAmt(4) As Single
Dim PSrv(12) As String
Dim IAmt(12, 4) As Single
Dim ISrv(12, 12, 12) As String
Dim SrvCnt(12, 12) As Integer

Dim LocalBal As Single
Dim myCreditAmt As Single
Dim PBal As Single, IBal As Single

Dim PolHldId As String
Dim RcvIdBillParty As String
Dim RcvIdUna As String
Dim RcvId1 As String
Dim TempInsId1 As String
Dim a1 As String, a2 As String
Dim Cmt As String, st As String
Dim Srv As String, Amt As String
Dim ItemId As Long, p1 As String
Dim myRef As String, Temp As String
Dim RcvId As Long, InsId As String
Dim CurInvId As String, Rsn As String
Dim SendTo As String, InvId As String
Dim InsrId As Long, TrnId As String
Dim InsId1 As String, AplNum As String
Dim RecId As String, TempIns As String
Dim InvDt As String, EOBDate As String
Dim AdjCode As String, RefCode As String
Dim PaymentMethod As String
Dim PType As String

Dim RetSrv As PatientReceivableService
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim InsurerId1 As Long
Dim dte As String

Dim AFee As Single
Dim ChargesByFee As Single

CompleteCurrentPatient_5 = False
If (Trim(PatId) <> "") Then
    EOBDate = Trim(lstPmnt(0).TextMatrix(1, 5))
    CurInvId = ""
        DoEvents
        Call DisplayDollarAmount(Trim(Str(MoneyLeft_5(CheckAmount))), RunningCheckBalance)
        Set ApplTemp = New ApplicationTemplates
' Write the Check Record
        
        If mCheckExist = 0 Then
            Call ApplTemp.ApplPostCheckRecord_5(CheckId, CStr(PatId), "P", Trim(Str(CheckAmount)), RunningCheckBalance, CheckNumber, CheckDate, EOBDate)
            mCheckExist = CheckId
        Else
            CheckId = mCheckExist
        End If
        
        PaymentMethod = myTrim(lstPmntInfo.TextMatrix(1, 1), 1)

        For i = 1 To lstPmnt(2).Rows - 1
            AFee = lstPmnt(2).TextMatrix(i, 6)
            ChargesByFee = ChargesByFee + lstPmnt(2).TextMatrix(i, 6)
            If Not skipRow(i) Then
                RcvId = lstPmnt(3).TextMatrix(i, 6)
                InvId = lstPmnt(3).TextMatrix(i, 5)
                If UCase(lstPmnt(2).TextMatrix(i, 19)) = "V" Then
                    dte = Format(Now, "mm/dd/yyyy")
                Else
                    dte = ""
                End If
    ' Payments
                ItemId = lstPmnt(2).TextMatrix(i, 0)
                Srv = Trim(lstPmnt(2).TextMatrix(i, 2))
                ComOn = False
                If (val(Trim(lstPmnt(2).TextMatrix(i, 7))) <> 0) Then
                    If (Trim(lstPmnt(2).TextMatrix(i, 2)) <> "") Then
                        If (Trim(RcvId) <> "") Then
                            Amt = Trim(lstPmnt(2).TextMatrix(i, 7))
                            Rsn = ""
                            Cmt = Trim(lstPmnt(2).TextMatrix(i, 15))
                            If (Trim(lstPmnt(2).TextMatrix(i, 16)) <> "") Then
                                ComOn = True
                            End If
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "P", PatientId, Amt, CheckDate, "P", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
    ' Contractual Adjustment
                If (val(Trim(lstPmnt(2).TextMatrix(i, 8))) <> 0) Then
                    If (Trim(lstPmnt(2).TextMatrix(i, 2)) <> "") Then
                        If (Trim(RcvId) <> "") Then
                            Amt = lstPmnt(2).TextMatrix(i, 8)
                            Rsn = "45" ' when is it A2 ?
                            Cmt = ""
                            If Not (ComOn) Then
                                Cmt = lstPmnt(2).TextMatrix(i, 15)
                                If (Trim(lstPmnt(2).TextMatrix(i, 16)) <> "") Then
                                    ComOn = True
                                End If
                            End If
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "O", InsurerId1, Amt, CheckDate, "X", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
    ' CoPay/CoIns
                If (val(Trim(lstPmnt(2).TextMatrix(i, 9))) <> 0) Then
                    If (Trim(lstPmnt(2).TextMatrix(i, 2)) <> "") Then
                        If (Trim(RcvId) <> "") Then
                            Amt = lstPmnt(2).TextMatrix(i, 9)
                            Rsn = "2"
                            Cmt = ""
                            If Not (ComOn) Then
                                Cmt = lstPmnt(2).TextMatrix(i, 15)
                                If (Trim(lstPmnt(2).TextMatrix(i, 16)) <> "") Then
                                    ComOn = True
                                End If
                            End If
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "O", InsurerId1, Amt, CheckDate, "|", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
    ' Deductible
                If (val(Trim(lstPmnt(2).TextMatrix(i, 10))) <> 0) Then
                    If (Trim(lstPmnt(2).TextMatrix(i, 2)) <> "") Then
                        If (Trim(RcvId) <> "") Then
                            Amt = lstPmnt(2).TextMatrix(i, 10)
                            Rsn = "1"
                            Cmt = ""
                            If Not (ComOn) Then
                                Cmt = lstPmnt(2).TextMatrix(i, 15)
                                If (Trim(lstPmnt(2).TextMatrix(i, 16)) <> "") Then
                                    ComOn = True
                                End If
                            End If
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "O", InsurerId1, Amt, CheckDate, "!", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
    ' Other1/2/3 Adjustments/Info and any additional
                GoSub PostOtherAdjs
            End If
        Next i

    'Removed because PatientReceivables is a view and this update is
    'unnecessary since ChargesByFee is a calculated column.
    '' update ChargesByFee
    'Dim RS As Recordset
    'Dim SQL As String
    'SQL = "update PatientReceivables set ChargesByFee = " & ChargesByFee & _
    '      " where Invoice = '" & InvId & "'"
    'Set RS = GetRS(SQL)
    
    Set ApplTemp = Nothing
    CompleteCurrentPatient_5 = True
End If
Exit Function

PostOtherAdjs:
        If (val(Trim(lstPmnt(2).TextMatrix(i, 17))) <> 0) Then
            If (Trim(lstPmnt(2).TextMatrix(i, 2)) <> "") Then
                If (Trim(RcvId) <> "") Then
                    Amt = lstPmnt(2).TextMatrix(i, 17)
                    Rsn = Trim(lstPmnt(2).TextMatrix(i, 18))
                    AdjCode = UCase(lstPmnt(2).TextMatrix(i, 19))
                    If (AdjCode = "W") Then
                        AdjCode = "8"       ' general writeoff
                    End If
                    If (AdjCode = "R") Then
                        AdjCode = "U"       ' general reversal
                    End If
                    If (AdjCode = "I") Then
                        AdjCode = "]"       ' general Informational
                    End If
                    If (AdjCode = "D") Then
                        AdjCode = "D"       ' general Denial
                    End If
' Watch this because as the visible grid arrangement changes this may need to be adjusted to get appeal number in the second grid
                    AplNum = lstPmnt(2).TextMatrix(i, 11)
                    Cmt = ""
                    If Not (ComOn) Then
                        Cmt = lstPmnt(2).TextMatrix(i, 15)
                        If (Trim(lstPmnt(2).TextMatrix(i, 16)) <> "") Then
                            ComOn = True
                        End If
                    End If
                    Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "O", InsurerId1, Amt, CheckDate, AdjCode, PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, "", CheckId, "", "", _
                    AFee, True)
                End If
            End If
        End If
    Return
End Function

Private Function skipRow(r As Integer) As Boolean
'If lstPmnt(2).TextMatrix(r, 7) = 0 _
'Then
'    skipRow = True
'End If
End Function

Private Function CalculateFields(ARow As Integer) As Boolean
Dim Bal As Single
Dim Adj As Single
Dim Paid As Single
Dim Allow As Single
Dim Cover As Single
Dim Charge As Single
Dim Temp1 As String
Dim Temp2 As String
CalculateFields = True
If ((Trim(MSFlexGrid1.TextMatrix(ARow, 6)) = "") And (Trim(MSFlexGrid1.TextMatrix(ARow, 7)) = "") Or _
    (val(Trim(MSFlexGrid1.TextMatrix(ARow, 6))) = 0) And (val(Trim(MSFlexGrid1.TextMatrix(ARow, 7))) = 0)) Then
    If (ARow > 0) Then
        Cover = val(Trim(MSFlexGrid2.TextMatrix(ARow, 0)))
        If (Cover <= 0) Then
            Cover = 1
        Else
            Cover = Cover / 100
        End If
        Allow = val(Trim(MSFlexGrid1.TextMatrix(ARow, 5)))
        Charge = val(Trim(MSFlexGrid1.TextMatrix(ARow, 3)))
        If (Allow > 0) And (Charge >= val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))) Then
            If (Charge >= Allow) Then
                Adj = Charge - Allow
                If (Adj < 0) Then
                    Adj = 0
                End If
                Paid = (Int(((Allow * Cover) + 0.005) * 100) / 100)
                If (Charge <> val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))) Then
                    Paid = val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))) - Adj
                End If
            Else
                Paid = Int(((Charge * Cover) + 0.004) * 100) / 100
                Adj = 0
            End If
        Else
            Paid = 0
            Adj = 0
        End If
        Call DisplayDollarAmount(Trim(Str(Paid)), Temp1)
        Call DisplayDollarAmount(Trim(Str(Adj)), Temp2)
        Bal = MoneyLeft(ARow)
' removed to allow calculator function on cell
' is covered display of negative balances in check amount
' and if movement out of field occurs.
        txtPayAmt.Text = Trim(Temp1)
        MSFlexGrid1.TextMatrix(ARow, 6) = Trim(txtPayAmt.Text)
        txtAdjAmt.Text = Trim(Temp2)
        MSFlexGrid1.TextMatrix(ARow, 7) = Trim(txtAdjAmt.Text)
        Call CalculateBalance(ARow)
    End If
End If
End Function

Private Function CalculateBalance(ARow As Integer) As Boolean
Dim Bal As Single
Dim Adj As Single
Dim Paid As Single
Dim PrevBal As Single
Dim Temp1 As String
CalculateBalance = False
PrevBal = val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))
Paid = val(Trim(MSFlexGrid1.TextMatrix(ARow, 6)))
Adj = val(Trim(MSFlexGrid1.TextMatrix(ARow, 7)))
Bal = ((PrevBal - (Paid + Adj)) * 100) / 100
Call DisplayDollarAmount(Trim(Str(Bal)), Temp1)
MSFlexGrid1.TextMatrix(ARow, 8) = Trim(Temp1)
Call SetDisburseRemaining
CalculateBalance = True
End Function

Private Function SetGridTotals() As Boolean
Dim i As Integer
Dim p As Single, a As Single
Dim Temp1 As String, Temp2 As String
p = 0
a = 0
For i = 1 To MSFlexGrid1.Rows - 1
    p = p + val(Trim(MSFlexGrid1.TextMatrix(i, 6)))
    a = a + val(Trim(MSFlexGrid1.TextMatrix(i, 7)))
Next i
Call DisplayDollarAmount(Trim(Str(p)), Temp1)
Call DisplayDollarAmount(Trim(Str(a)), Temp2)
lblTPay.Visible = True
lblTPaid.Caption = Trim(Temp1)
lblTAdj.Visible = True
lblTAdjs.Caption = Trim(Temp2)
End Function

Private Function SetDisburseRemaining() As Boolean
Dim Amt As Single
Dim Temp As String
SetDisburseRemaining = True
Amt = MoneyLeft(0)
lblAmountLeft.Tag = Trim(Str(Amt))
Call DisplayDollarAmount(lblAmountLeft.Tag, Temp)
lblAmountLeft.Tag = Trim(Temp)
lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(lblAmountLeft.Tag)
Call SetGridTotals
End Function

Private Function GetCorrectCheck(TheCheck As String, CheckId As Long) As Boolean
'Dim ApplTemp As ApplicationTemplates
'GetCheckOn = False
'GetCorrectCheck = False
'If (Trim(TheCheck) <> "") Then
'    Set ApplTemp = New ApplicationTemplates
'    Set ApplTemp.lstBox = lstDisb
'    If (ApplTemp.ApplGetAllChecks(TheCheck)) Then
'        GetCorrectCheck = True
'        lstDisb.Visible = True
'        cmdApply.Enabled = False
'        cmdHome.Enabled = False
'        cmdDisburse.Enabled = False
'        cmdView.Enabled = False
'        txtAmount.Enabled = False
'        txtPatient.Enabled = False
'        txtInsurer.Enabled = False
'        txtCheck.Enabled = False
'        txtDate.Enabled = False
'        GetCheckOn = True
'    End If
'End If
End Function

Private Function GetAdjType(ACode As String, AAdj As String, ARef As String) As Boolean
Dim i As Integer
Dim p As Integer
Dim TstCode As String
Dim RetCode As PracticeCodes
GetAdjType = False
If (Trim(ACode) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "PAYMENTTYPE"
    If (RetCode.FindCode > 0) Then
        i = 1
        Do Until Not (RetCode.SelectCode(i))
            p = InStrPS(RetCode.ReferenceCode, "-")
            If (p > 0) Then
                TstCode = Trim(Mid(RetCode.ReferenceCode, p + 1, Len(RetCode.ReferenceCode) - p))
                If (TstCode = ACode) Then
                    AAdj = ACode
                    ARef = Trim(RetCode.ReferenceAlternateCode)
                    GetAdjType = True
                    Exit Do
                End If
            End If
            i = i + 1
        Loop
    End If
    Set RetCode = Nothing
End If
End Function



'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
'==========================================
Private Sub frPmnt_Visible(stat As Boolean)

frPmnt(0).Visible = stat
DoEvents
stat = Not stat
'cmdDone.Enabled = stat
'cmdMClm.Enabled = stat
'cmdNotes.Enabled = stat
If stat Then
    'FromCredit False
    'lstSrvs1.SetFocus
Else
    lstPmnt(0).col = 0
    initPmnt
    'fpPmnt(1).Visible = Not EditMode
    'picMask.Visible = EditMode
End If


End Sub

Private Sub initPmnt()
lblWB.Visible = False


lstInfo.FormatString = "Major Med" + vbTab _
            + "Vision" + vbTab _
            + "Ambulatory" + vbTab _
            + "Workers Comp" + vbTab

lstInfo.ColWidth(0) = 300 * 15
lstInfo.ColWidth(1) = 170 * 15
lstInfo.ColWidth(2) = 170 * 15
lstInfo.ColWidth(3) = 170 * 15



Dim DisplayText  As String
Dim j As Integer
DisplayText = "Payer" + vbTab _
            + "Method" + vbTab _
            + "Amt" + vbTab _
            + "Check or Ref #" + vbTab _
            + "Pay Date" + vbTab _
            + "Check Date"
lstPmnt(0).FormatString = DisplayText

With lstPmnt(0)
For j = 0 To .Cols - 1
    Select Case j
    Case 0
        .ColWidth(j) = 157 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 1
        .ColWidth(j) = 156 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 2
        .ColWidth(j) = 70 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 3
        .ColWidth(j) = 225 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 4
        .ColWidth(j) = 80 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 5
        .ColAlignment(j) = flexAlignRightCenter
        .ColWidth(j) = 80 * 15
    End Select
    Next j
    .Width = 769 * 15
    .Row = 0
    For j = 0 To .Cols - 1
        .col = j
        .CellAlignment = 1
    Next j
    .Row = 1
    .col = 0
End With

With lstPmnt(1)
DisplayText = "Patient" + vbTab + _
              "" + vbTab + _
              "Remaining" + vbTab + _
              "Paid" + vbTab + _
              "Adjust." + vbTab + _
              "" + vbTab + _
              "Paid" + vbTab + _
              "Adjust."
.FormatString = DisplayText

For j = 0 To .Cols - 1
    Select Case j
    Case 0
        .ColWidth(j) = 157 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 1
        .ColWidth(j) = 157 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 2, 3, 4, 5, 6, 7
        .ColWidth(j) = 70 * 15
        .ColAlignment(j) = flexAlignRightCenter
    End Select
    Next j
    .Row = 0
    Dim w As Integer
    For j = 0 To .Cols - 1
        .col = j
        .CellAlignment = 1
        w = w + .ColWidth(j)
    Next j
    .Row = 1
    .col = 0
    .Width = w
End With

'------------
With lstPmnt(2)
    DisplayText = "itemid" + vbTab _
            + "Srv Date" + vbTab _
            + "Service" + vbTab _
            + "Mo" + vbTab _
            + "Charge" + vbTab _
            + "Prv Bal" + vbTab _
            + "Allowed" + vbTab _
            + "Payment" + vbTab _
            + "CntrWO" + vbTab _
            + "CoIns" + vbTab _
            + "Deduct" + vbTab _
            + "Balance" + vbTab _
            + vbTab _
            + "N" + vbTab _
            + "A" + vbTab _
            + "Cmnt" + vbTab _
            + "P" + vbTab _
            + "OthAdj" + vbTab _
            + "Rsn" + vbTab _
            + "D"
    .FormatString = DisplayText
    
    For j = 0 To .Cols - 1
    Select Case j
    Case 0
        .ColWidth(j) = 0
    Case 1
        .ColWidth(j) = 75 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 2
        .ColWidth(j) = 55 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 3
        .ColWidth(j) = 20 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 4
        .ColWidth(j) = 56 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 5, 11
        .ColWidth(j) = 61 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 6, 7, 8, 11
        .ColWidth(j) = 55 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 17
        .ColWidth(j) = 55 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 9, 10
        .ColWidth(j) = 50 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 12
        .ColWidth(j) = 0
    Case 13, 14
        .ColWidth(j) = 15 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 15
        .ColWidth(j) = 30 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 16
        .ColWidth(j) = 15 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 18
        .ColWidth(j) = 30 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 19
        .ColWidth(j) = 15 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    End Select
    Next j
    .Row = 0
    For j = 0 To .Cols - 1
        .col = j
        .CellAlignment = 1
    Next j
End With

lstPmnt(3).FormatString = "Cvr  " + vbTab + "RefId" + vbTab + "Comment" + vbTab + "Reason" + vbTab + "appid  " + vbTab + _
                        "invoiceid   " + vbTab + "receivable" _
                        + vbTab + "ZeroBal" + vbTab + "Entered"


Dim r As Integer
    With lstPmnt(0)
        .TextMatrix(1, 0) = ""
        .TextMatrix(1, 1) = ""
        .TextMatrix(1, 2) = "0.00"
        .TextMatrix(1, 3) = ""
        .TextMatrix(1, 4) = Format(Now, "mm/dd/yyyy")
        .TextMatrix(1, 5) = ""
    End With
    With lstPmnt(1)
        .TextMatrix(1, 0) = ""
        .TextMatrix(1, 1) = ""
        .TextMatrix(1, 2) = "0.00"
        .TextMatrix(1, 3) = "0.00"
        .TextMatrix(1, 4) = "0.00"
        .TextMatrix(1, 5) = ""
        .TextMatrix(1, 6) = "0.00"
        .TextMatrix(1, 7) = "0.00"

    End With

'initPmntSrvs RetPay
'Call lstPmnt_Click(0)
'Set RetPay = Nothing

End Sub

''''Private Sub initPmntSrvs(Optional RetPay As PatientReceivablePayment)
''''Dim r As Integer, r1 As Integer, i As Integer
''''With lstPmnt(1)
''''    .Rows = 1
''''    If EditMode Then
''''        r = lstSrvs1.Row
''''        .Rows = 2
''''
''''        Set RetPay = New PatientReceivablePayment
''''        RetPay.PaymentId = lstSrvs2.TextMatrix(r, 2)
''''        If (RetPay.RetrievePatientReceivablePayments) Then
''''            For r1 = lstSrvs1.Row To 0 Step -1
''''                If lstSrvs2.TextMatrix(r1, 0) = "R" Then
''''                    .TextMatrix(1, 1) = lstSrvs2.TextMatrix(r1, 9)
''''                    Exit For
''''                End If
''''            Next
''''            For r1 = r1 + 1 To lstSrvs1.Rows - 1
''''                Select Case lstSrvs2.TextMatrix(r1, 0)
''''                Case "R"
''''                    Exit For
''''                Case "S"
''''                    If lstSrvs1.TextMatrix(r1, 2) = RetPay.PaymentService Then
''''                        .TextMatrix(1, 0) = RetPay.PaymentServiceItem
''''                        .TextMatrix(1, 2) = RetPay.PaymentService
''''                        .TextMatrix(1, 3) = lstSrvs1.TextMatrix(r1, 6)
''''                        .TextMatrix(1, 4) = lstSrvs1.TextMatrix(r1, 7)
''''                        .TextMatrix(1, 5) = lstSrvs1.TextMatrix(r1, 9)
''''                        .TextMatrix(1, 6) = lstSrvs2.TextMatrix(r1, 10)
''''
''''                        .TextMatrix(1, 7) = "0.00"
''''                        .TextMatrix(1, 8) = "0.00"
''''                        .TextMatrix(1, 9) = "0.00"
''''                        .TextMatrix(1, 10) = "0.00"
''''                        .TextMatrix(1, 17) = "0.00"
''''                        .TextMatrix(1, 19) = ""
''''                        Select Case RetPay.PaymentType
''''                        Case "P"
''''                            i = 7
''''                        Case "X"
''''                            i = 8
''''                        Case "|"
''''                            i = 9
''''                        Case "!"
''''                            i = 10
''''                        Case Else
''''                            i = 17
''''                            .TextMatrix(1, 19) = RetPay.PaymentType
''''                        End Select
''''                        .TextMatrix(1, i) = Format(RetPay.PaymentAmount, "#####0.00")
''''
''''                        .TextMatrix(1, 11) = lstSrvs1.TextMatrix(r1, 9)
''''                        .TextMatrix(1, 13) = ""
''''                        .TextMatrix(1, 14) = ""
''''                        .TextMatrix(1, 15) = RetPay.PaymentRefId
''''                        If RetPay.PaymentCommentOn Then
''''                            .TextMatrix(1, 16) = "Y"
''''                        Else
''''                            .TextMatrix(1, 16) = ""
''''                        End If
''''                        .TextMatrix(1, 18) = RetPay.PaymentReason
''''                    End If
''''                End Select
''''            Next
''''        End If
''''        Set RetPay = Nothing
''''    Else
''''        For r = lstSrvs1.Row + 1 To lstSrvs1.Rows - 1
''''            Select Case lstSrvs2.TextMatrix(r, 0)
''''            Case "S"
''''                r1 = .Rows
''''                .Rows = .Rows + 1
''''                .TextMatrix(r1, 0) = lstSrvs2.TextMatrix(r, 1)
''''                .TextMatrix(r1, 1) = lstSrvs1.TextMatrix(r, 0)
''''                .TextMatrix(r1, 2) = lstSrvs1.TextMatrix(r, 2)
''''                .TextMatrix(r1, 3) = lstSrvs1.TextMatrix(r, 6)
''''                .TextMatrix(r1, 4) = lstSrvs1.TextMatrix(r, 7)
''''                .TextMatrix(r1, 5) = lstSrvs1.TextMatrix(r, 9)
''''                .TextMatrix(r1, 6) = lstSrvs2.TextMatrix(r, 10)
''''                .TextMatrix(r1, 7) = "0.00"
''''                .TextMatrix(r1, 8) = "0.00"
''''                .TextMatrix(r1, 9) = "0.00"
''''                .TextMatrix(r1, 10) = "0.00"
''''                .TextMatrix(r1, 11) = lstSrvs1.TextMatrix(r, 9)
''''                .TextMatrix(r1, 13) = ""
''''                .TextMatrix(r1, 14) = ""
''''                .TextMatrix(r1, 15) = ""
''''                .TextMatrix(r1, 16) = ""
''''                .TextMatrix(r1, 17) = "0.00"
''''                .TextMatrix(r1, 18) = ""
''''                .TextMatrix(r1, 19) = ""
''''            Case "R"
''''                Exit For
''''            End Select
''''        Next
''''    End If
''''End With
''''lstPmnt(1).Height = lstPmnt(1).Rows * lstPmnt(1).RowHeight(0)
''''lstPmnt(2).Top = lstPmnt(1).Top + lstPmnt(1).Height + 75
''''End Sub

Private Sub editCellPmnt(i As Integer)
Dim FGC As MSFlexGrid
Set FGC = lstPmnt(i)

Dim c As Integer, r As Integer
c = FGC.col
r = FGC.Row

If i = 0 And c = 0 Then Exit Sub
If i = 1 Then Exit Sub
If FGC.ColWidth(c) = 0 Then Exit Sub

pTxt1.Top = FGC.Top + FGC.RowPos(r) + 15
pTxt1.Left = FGC.Left + FGC.ColPos(c) + 15
pTxt1.Width = FGC.ColWidth(c) - 30
pTxt1.Visible = True
bySys = True
txt1.MaxLength = 0
txt1.Text = FGC.Text
txt1.Width = pTxt1.Width - 45
If FGC.ColAlignment(c) = flexAlignRightCenter Then
    txt1.Alignment = 1
Else
    txt1.Alignment = 0
End If
bySys = False

dd1.Caption = "q"
dd1.Visible = False
Select Case i
Case 0
    Select Case c
    Case 1
        dd1.Left = pTxt1.Width - dd1.Width + 15
        dd1.Visible = True
    End Select
Case 2
    Select Case c
    Case 15
        dd1.Left = pTxt1.Width - dd1.Width + 15
        dd1.Visible = True
    End Select
    Select Case c
    Case 13, 14, 16, 19
        txt1.MaxLength = 1
    End Select
End Select


Call SkipCell1(i, c, r)
If Not (FGC.col = c And FGC.Row = r) Then
    FGC.col = c
    FGC.Row = r
    Exit Sub
End If


pTxt1.Visible = True
If Not dd1.Visible Then
    txt1.SelStart = 0
    txt1.SelLength = Len(txt1.Text)
End If
On Error Resume Next
txt1.SetFocus

End Sub

Private Sub SkipCell1(i As Integer, nC As Integer, nR As Integer, _
                     Optional sShift As Integer = 0)

    Select Case i
    Case 0
        Select Case nC
        Case Is < 1
            If sShift = 0 Then
                nC = 1
            Else
                nC = lstPmnt(1).Cols - 1
            End If
        End Select
    Case 1
        If Not nC = 0 Then nC = 0
    Case 2
        Select Case nC
        Case Is < 6
            If sShift = 0 Then
                nC = 6
            Else
                nC = lstPmnt(1).Cols - 1
            End If
        Case 11, 12
            If sShift = 0 Then
                nC = 13
            Else
                nC = 10
            End If
        End Select
    End Select
End Sub

Private Function LstCompleted() As Integer
Dim msg As String

Select Case True
Case Trim(lstPmnt(0).TextMatrix(1, 0)) = ""
    msg = "Please select an Insurer."
    LstCompleted = 1
Case Trim(lstPmnt(0).TextMatrix(1, 3)) = ""
    If lstPmntInfo.TextMatrix(1, 1) = "K" Then
        msg = "Please enter a check number."
        LstCompleted = 4
    End If
End Select

If msg = "" Then
    If lstPmntInfo.TextMatrix(1, 1) = "K" Then
        Dim RetPay As New PatientReceivablePayment
        Dim mPaymentDate As String
        mPaymentDate = Format(lstPmnt(0).TextMatrix(1, 4), "yyyymmdd")
        Dim InsurerId As String
        Dim PayerType As String
        InsurerId = lstPmntInfo.TextMatrix(1, 0)
        'get payer info
        If IsNumeric(InsurerId) Then
            PayerType = "I"
        Else
            Select Case InsurerId
            Case "Patient"
                InsurerId = PatientId
                PayerType = "P"
            Case "Office"
                InsurerId = 0
                PayerType = "O"
            End Select
        End If
        mCheckExist = RetPay.CheckExist(CDbl(lstPmnt(0).TextMatrix(1, 2)), _
                                  lstPmnt(0).TextMatrix(1, 3), _
                           mPaymentDate, _
                           Format(lstPmnt(0).TextMatrix(1, 5), "yyyymmdd"), PayerType)
                     
        If mCheckExist = 0 Then
'''            If lstPmnt(0).TextMatrix(1, 2) = 0 Then
'''                msg = "Please enter payment amount."
'''                LstCompleted = 3
'''            End If
        Else
            If lstPmnt(0).Enabled Then
            
                msg = "Check from " & _
                      RetPay.getPayerName(RetPay.PaymentPayerType, RetPay.PaymentPayerId) & _
                      " already in use.  Continue with this batch?"
                LstCompleted = 5
            End If
        End If
        Set RetPay = Nothing
    End If
End If

If Not msg = "" Then
    frmEventMsgs.Header = msg
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    If LstCompleted = 5 Then
        frmEventMsgs.CancelText = "New batch with diff check #"
    Else
        frmEventMsgs.CancelText = ""
    End If
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
DoEvents
If LstCompleted = 5 Then
    If frmEventMsgs.Result = 2 Then
        lstPmnt(0).TextMatrix(1, 4) = Mid(mPaymentDate, 5, 2) & "/" & Mid(mPaymentDate, 7, 2) & "/" & Mid(mPaymentDate, 1, 4)
        LstCompleted = 0
    End If
End If
End Function

Private Function lstPmntInd() As Integer
Select Case pTxt1.Top
Case Is > lstPmnt(2).Top
    lstPmntInd = 2
Case Is > lstPmnt(1).Top
    lstPmntInd = 1
Case Else
    lstPmntInd = 0
End Select
End Function

Public Sub LoadPatient(lst As Integer, PatId As Long)
Dim b1 As Boolean, b2 As Boolean
Dim InsName(6) As String
Dim r1 As String, r2 As String
Dim Temp As String, AName As String
Dim IName As String
Dim ApplTemp As New ApplicationTemplates

Call ApplTemp.ApplGetPatientName(PatId, AName, PatPol1, r1, b1, PatPol2, r2, b2, False)

VerifyPatient:
Temp = ApplTemp.ApplVerifyPatient(PatPol1, 0)

If (Temp <> "") Then
    frmEventMsgs.Header = Temp
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Fix It"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.result = 2) Then
        Dim PatientDemographics As New PatientDemographics
        If (PatPol1 > 0) Then
            PatientDemographics.PatientId = PatPol1
            Call PatientDemographics.DisplayPatientInfoScreen
            GoTo VerifyPatient
        End If
    End If
End If

lstPmnt(lst).TextMatrix(1, 0) = AName & Space(100) & PatId
If lst = 1 Then
    PatientId = PatId
    frmPaymentsTotal.loadlstInfo "", PatPol1, lstInfo
End If
Set ApplTemp = Nothing
End Sub

Private Function getPaidAmt(ChkId As Long) As Single
If ChkId = 0 Then Exit Function

Dim SQL As String
Dim CR(1) As String
Dim Tot(1) As Single
Dim i As Integer
Dim RS As Recordset

SQL = _
"select * From PatientReceivablePayments " & _
"Where PaymentBatchCheckId = " & ChkId & _
"and PaymentType in "

CR(0) = "('P')"
CR(1) = "('U', 'R')"

For i = 0 To 1
    Set RS = GetRS(SQL & CR(i))
    Do Until RS.EOF
    Tot(i) = Tot(i) + RS("paymentamount")
        RS.MoveNext
    Loop
    Set RS = Nothing
Next

getPaidAmt = Tot(0) - Tot(1)
End Function

Private Function getAdjAmt(ChkId As Long) As Single
If ChkId = 0 Then Exit Function

Dim SQL As String
Dim CR(1) As String
Dim Tot(1) As Single
Dim i As Integer
Dim RS As Recordset

SQL = _
"select * From PatientReceivablePayments " & _
"Where PaymentBatchCheckId = " & ChkId & _
"and PaymentType not in ('P', 'R', 'U', 'V') " & _
"and PaymentFinancialType = "

CR(0) = "('C')"
CR(1) = "('D')"

For i = 0 To 1
    Set RS = GetRS(SQL & CR(i))
    Do Until RS.EOF
    Tot(i) = Tot(i) + RS("paymentamount")
        RS.MoveNext
    Loop
    Set RS = Nothing
Next

getAdjAmt = Tot(0) - Tot(1)
End Function


Private Sub loadTotals()

CheckAmt = lstPmnt(0).TextMatrix(1, 2)
PaidCheckAmt = getPaidAmt(mCheckExist)
RemCheckAmt = CheckAmt - PaidCheckAmt
AdjCheckAmt = getAdjAmt(mCheckExist)

lstPmnt(1).TextMatrix(1, 2) = Format(RemCheckAmt, "#####0.00")
lstPmnt(1).TextMatrix(1, 3) = Format(PaidCheckAmt, "#####0.00")
lstPmnt(1).TextMatrix(1, 4) = Format(AdjCheckAmt, "#####0.00")
lstPmnt(1).TextMatrix(1, 6) = "0.00"
lstPmnt(1).TextMatrix(1, 7) = "0.00"
End Sub


Private Sub Disburse(OverrideOn As Boolean)
loadTotals
Dim res As Boolean
res = LoadPayments_5(PatientId)
lstPmnt(2).Enabled = res
If res Then
    lstPmnt(0).Enabled = False
    lstPmnt(2).col = 7
End If
End Sub

Private Function LoadPayments_5(PatId As Long) As Integer
Dim lst1 As MSFlexGrid
Set lst1 = lstPmnt(2)
lst1.Rows = 1

Dim lst2 As MSFlexGrid
Set lst2 = lstPmnt(3)
lst2.Rows = 1

Dim newLine As String

Dim RS As Recordset
Dim SQL As String

SQL = getSQL(PatId)
Set RS = GetRS(SQL)

Do Until RS.EOF
    newLine = RS("ItemId") & vbTab & _
              Mid(RS("ServiceDate"), 5, 2) & "/" & Mid(RS("ServiceDate"), 7, 2) & "/" & Mid(RS("ServiceDate"), 1, 4) & vbTab & _
              RS("Service") & vbTab & _
              RS("Modifier") & vbTab & _
              myCCur(RS("Charge")) & vbTab & _
              myCCur(RS("PreviousBalance")) & vbTab & _
              "0.00" & vbTab & _
              "0.00" & vbTab & _
              "0.00" & vbTab & _
              "0.00" & vbTab & _
              "0.00" & vbTab & _
              myCCur(RS("PreviousBalance")) & vbTab & _
              "" & vbTab & _
              "" & vbTab & _
              "" & vbTab & _
              "" & vbTab & _
              "" & vbTab & _
              "0.00" & vbTab & _
              "" & vbTab & _
              ""
    lst1.AddItem newLine
    
    newLine = "?" & vbTab & _
              "?" & vbTab & _
              "<>comment" & vbTab & _
              "<>reason" & vbTab & _
              RS("AppointmentId") & vbTab & _
              RS("Invoice") & vbTab & _
              RS("ReceivableId") & vbTab & _
              "?" & vbTab
    lst2.AddItem newLine
    
    
    RS.MoveNext
Loop

LoadPayments_5 = (lst1.Rows > 1)

End Function

Private Function getSQL(PatId As Long) As String
Dim fSQL As String
Dim rcv As String
rcv = " '' as ReceivableId, "

Select Case OptView
Case 0
    fSQL = "EXEC dbo.GetPatientPaymentsForPatient " & PatId
Case 1
    fSQL = _
        " and OpenBalance > 0 "
Case 2
End Select

Select Case OptView
Case 1, 2
fSQL = " select distinct prs.ServiceDate, prs.ItemId, prs.Service, prs.Modifier, " & _
        " prs.Quantity, prs.Charge * prs.Quantity as Charge, prs.FeeCharge , Pr.AppointmentId, prs.Invoice, " & _
        rcv & _
        "  (prs.Charge * prs.Quantity) - Sum( " & _
        "         Case PaymentFinancialType " & _
        "              when 'C' then PaymentAmount " & _
        "              when 'D' then - PaymentAmount " & _
        "              else 0 " & _
        "              end ) as PreviousBalance " & _
        " from PatientReceivableServices prs " & _
        " inner join PatientReceivables pr on pr.Invoice = prs.Invoice " & _
        " left outer join PatientReceivablePayments prp on prp.PaymentServiceItem = prs.ItemId " & _
        " Where Pr.PatientId = " & PatId & _
        " and prs.Status = 'A' " & _
        fSQL & _
        " group by prs.servicedate, prs.ItemId, prs.Service, prs.Modifier, prs.Quantity, prs.Charge,  prs.FeeCharge, pr.AppointmentId, prs.Invoice, pr.ReceivableId " & _
        " Order by prs.ServiceDate desc, prs.Invoice, Charge desc"
End Select

getSQL = fSQL
End Function


Private Sub VS_Change()
lblHelp.Top = -VS.Value
End Sub

Private Sub VS_LostFocus()
lstPT.Visible = False
End Sub

Private Sub VS_Scroll()
Call VS_Change
End Sub
Public Sub FrmClose()
Call fpBtn_Click(0)
Unload Me
End Sub








'oldform
'''''''''Option Explicit
'''''''''Public BatchOn As Boolean
'''''''''Public PatientId As Long
'''''''''
'''''''''Private UserId As Long
'''''''''Private GetCheckOn As Boolean
'''''''''Private FirstTimeSet As Boolean
'''''''''Private PaymentMethod As String
'''''''''Private AdjustmentCode As String
'''''''''Private AllOn As Boolean
'''''''''Private CheckId As Long
'''''''''Private PatPol1 As Long
'''''''''Private PatPol2 As Long
'''''''''Private PatPolId As Long
'''''''''Private CurrentRow As Integer
'''''''''Private CurrentCol As Integer
'''''''''Private RunningCheckBalance As String
'''''''''
'''''''''Dim bySys As Boolean
'''''''''Dim mCheckExist As Long
'''''''''
'''''''''Dim mKeyCode As Integer
'''''''''
'''''''''
'''''''''Private Sub cmdChkReset_Click()
'''''''''Dim ADate As String
'''''''''Dim TheAmt As Single
'''''''''Dim TheResult As Single
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If (Trim(txtCheck.Text) <> "") And (Trim(txtDate.Text) <> "") Then
'''''''''    TheAmt = 0
'''''''''    ADate = Trim(txtDate.Text)
'''''''''    ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''    TheAmt = ApplTemp.ApplGetPaymentsAgainstCheck(0, Trim(txtCheck.Text), ADate)
'''''''''    Set ApplTemp = Nothing
'''''''''    TheResult = Val(Trim(txtAmount.Text)) - TheAmt
'''''''''    RunningCheckBalance = Trim(Str(TheResult))
'''''''''    Call MoneyLeft(0)
'''''''''    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
'''''''''    lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''''    lblAmountLeft.Visible = True
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdFinData_Click()
'''''''''Dim AInsId As Long
'''''''''Dim RcvId As Long
'''''''''Dim TheDate As String, InvDt As String
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If (PatientId > 0) Then
'''''''''    If (CurrentRow > 0) And (MSFlexGrid1.Visible) Then
'''''''''        RcvId = 0
'''''''''        Set ApplTemp = New ApplicationTemplates
'''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 1)) <> "") Then
'''''''''            RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 1)), AInsId, InvDt)
'''''''''        End If
'''''''''''''''        frmPayments.AccessType = "FD"
'''''''''''''''        frmPayments.Whom = True
'''''''''''''''        frmPayments.CurrentAction = "BP"
'''''''''''''''        frmPayments.PatientId = PatientId
'''''''''''''''        frmPayments.ReceivableId = RcvID
'''''''''''''''        If (frmPayments.LoadPayments(True, "")) Then
'''''''''''''''            frmPayments.Show 1
'''''''''''''''            If (frmPayments.CurrentAction <> "Home") And (frmPayments.CurrentAction <> "BP") Then
'''''''''''''''                Call LoadPayments(PatientId, AllOn, True)
'''''''''''''''            End If
'''''''''''''''        Else
'''''''''''''''            frmEventMsgs.Header = "No Financial Data"
'''''''''''''''            frmEventMsgs.AcceptText = ""
'''''''''''''''            frmEventMsgs.RejectText = "Ok"
'''''''''''''''            frmEventMsgs.CancelText = ""
'''''''''''''''            frmEventMsgs.Other0Text = ""
'''''''''''''''            frmEventMsgs.Other1Text = ""
'''''''''''''''            frmEventMsgs.Other2Text = ""
'''''''''''''''            frmEventMsgs.Other3Text = ""
'''''''''''''''            frmEventMsgs.Other4Text = ""
'''''''''''''''            frmEventMsgs.Show 1
'''''''''''''''        End If
'''''''''        frmPaymentsTotal.AccessType = "FD"
'''''''''        frmPaymentsTotal.Whom = True
'''''''''        frmPaymentsTotal.CurrentAction = "BP"
'''''''''        frmPaymentsTotal.PatientId = PatientId
'''''''''        frmPaymentsTotal.ReceivableId = RcvId
'''''''''        If frmPaymentsTotal.LoadPaymentsTotal("") Then
'''''''''            frmPaymentsTotal.Show 1
'''''''''            If (frmPaymentsTotal.CurrentAction <> "Home") And (frmPaymentsTotal.CurrentAction <> "BP") Then
'''''''''                Call LoadPayments(PatientId, AllOn, True)
'''''''''            End If
'''''''''        End If
'''''''''        Set ApplTemp = Nothing
'''''''''    Else
'''''''''''''''        frmPayments.AccessType = "FD"
'''''''''''''''        frmPayments.Whom = True
'''''''''''''''        frmPayments.CurrentAction = "BP"
'''''''''''''''        frmPayments.PatientId = PatientId
'''''''''''''''        frmPayments.ReceivableId = 0
'''''''''''''''        If (frmPayments.LoadPayments(True, "")) Then
'''''''''''''''            frmPayments.Show 1
'''''''''''''''            If (frmPayments.CurrentAction <> "Home") And (frmPayments.CurrentAction <> "BP") Then
'''''''''''''''                Call LoadPayments(PatientId, AllOn, True)
'''''''''''''''            End If
'''''''''''''''        Else
'''''''''''''''            frmEventMsgs.Header = "No Financial Data"
'''''''''''''''            frmEventMsgs.AcceptText = ""
'''''''''''''''            frmEventMsgs.RejectText = "Ok"
'''''''''''''''            frmEventMsgs.CancelText = ""
'''''''''''''''            frmEventMsgs.Other0Text = ""
'''''''''''''''            frmEventMsgs.Other1Text = ""
'''''''''''''''            frmEventMsgs.Other2Text = ""
'''''''''''''''            frmEventMsgs.Other3Text = ""
'''''''''''''''            frmEventMsgs.Other4Text = ""
'''''''''''''''            frmEventMsgs.Show 1
'''''''''''''''        End If
'''''''''        frmPaymentsTotal.AccessType = "FD"
'''''''''        frmPaymentsTotal.Whom = True
'''''''''        frmPaymentsTotal.CurrentAction = "BP"
'''''''''        frmPaymentsTotal.PatientId = PatientId
'''''''''        frmPaymentsTotal.ReceivableId = 0
'''''''''        If frmPaymentsTotal.LoadPaymentsTotal("") Then
'''''''''            frmPaymentsTotal.Show 1
'''''''''            If (frmPaymentsTotal.CurrentAction <> "Home") And (frmPaymentsTotal.CurrentAction <> "BP") Then
'''''''''                Call LoadPayments(PatientId, AllOn, True)
'''''''''            End If
'''''''''        End If
'''''''''    End If
'''''''''Else
'''''''''    frmEventMsgs.Header = "Select a patient"
'''''''''    frmEventMsgs.AcceptText = ""
'''''''''    frmEventMsgs.RejectText = "Ok"
'''''''''    frmEventMsgs.CancelText = ""
'''''''''    frmEventMsgs.Other0Text = ""
'''''''''    frmEventMsgs.Other1Text = ""
'''''''''    frmEventMsgs.Other2Text = ""
'''''''''    frmEventMsgs.Other3Text = ""
'''''''''    frmEventMsgs.Other4Text = ""
'''''''''    frmEventMsgs.Show 1
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdHome_Click()
'''''''''MyPracticeRepositoryCmd.CommandType = adCmdText
'''''''''Unload frmPLineItem
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdApply_Click()
'''''''''If (MSFlexGrid1.Visible) Then
'''''''''    Call txtPatient_Click
'''''''''Else
'''''''''    MyPracticeRepositoryCmd.CommandType = adCmdText
'''''''''    Unload frmPLineItem
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdAll_Click()
'''''''''If (PatientId > 0) Then
'''''''''    AllOn = Not (AllOn)
'''''''''    If (AllOn) Then
'''''''''        cmdAll.Text = "Show Only Open"
'''''''''    Else
'''''''''        cmdAll.Text = "Show All Items"
'''''''''    End If
'''''''''    Call LoadPayments(PatientId, AllOn, True)
'''''''''    lblInst.Visible = True
'''''''''    MSFlexGrid1.Visible = True
'''''''''    MSFlexGrid1.SetFocus
'''''''''    MSFlexGrid1.RowSel = 1
'''''''''    CurrentRow = 1
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdComment_Click()
'''''''''If (PatientId > 0) And (CurrentRow > 0) Then
'''''''''    If Not (lblAdj.Visible) Then
'''''''''        lblAdj.Visible = True
'''''''''        txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
'''''''''        txtAdj.Visible = True
'''''''''        chkInclude.Visible = True
'''''''''        If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
'''''''''            chkInclude.Value = 1
'''''''''        Else
'''''''''            chkInclude.Value = 0
'''''''''        End If
'''''''''    End If
'''''''''    txtAdj.SetFocus
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdDisburse_Click()
'''''''''Dim RcvId As Long
'''''''''Dim TheDate As String
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If Not (MSFlexGrid1.Visible) Then
'''''''''    If (txtPatient.Text = "") Then
'''''''''        frmEventMsgs.Header = "Patient is missing."
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        txtPatient.SetFocus
'''''''''        Exit Sub
'''''''''    End If
'''''''''    If (Val(Trim(txtAmount.Text)) <= 0) Then
'''''''''        frmEventMsgs.Header = "Amount must be greater than zero."
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        txtAmount.SetFocus
'''''''''        Exit Sub
'''''''''    End If
'''''''''    If (Trim(txtDate.Text) = "") Then
'''''''''        frmEventMsgs.Header = "Date is missing."
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        txtDate.SetFocus
'''''''''        Exit Sub
'''''''''    End If
'''''''''    If (lstMethod.ListIndex < 0) Then
'''''''''        frmEventMsgs.Header = "Payment Method is missing."
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        lstMethod.SetFocus
'''''''''        Exit Sub
'''''''''    End If
'''''''''    If (Mid(lstMethod.List(lstMethod.ListIndex), 40, 1) <> "C") And (Trim(txtCheck.Text) = "") Then
'''''''''        frmEventMsgs.Header = "Check or Payment Reference is missing."
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        txtCheck.SetFocus
'''''''''        Exit Sub
'''''''''    End If
'''''''''    lblPrint.Caption = "Loading..."
'''''''''    lblPrint.Visible = True
'''''''''    If (LoadPayments(PatientId, AllOn, True)) Then
'''''''''        cmdComment.Visible = False
'''''''''        cmdPrint.Visible = True
'''''''''        cmdView.Visible = True
'''''''''        cmdAll.Visible = True
'''''''''        cmdChkReset.Visible = True
'''''''''        txtAmount.Locked = True
'''''''''        txtCheck.Locked = True
'''''''''        lblTPay.Visible = True
'''''''''        lblTPaid.Visible = True
'''''''''        lblTAdj.Visible = True
'''''''''        lblTAdjs.Visible = True
'''''''''        lblInst.Visible = True
'''''''''        MSFlexGrid1.Visible = True
'''''''''        MSFlexGrid1.SetFocus
'''''''''        MSFlexGrid1.RowSel = 1
'''''''''        CurrentRow = 1
'''''''''    Else
'''''''''        frmEventMsgs.Header = "No Items available for posting"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        cmdComment.Visible = False
'''''''''        cmdPrint.Visible = False
'''''''''        cmdView.Visible = False
'''''''''        cmdAll.Visible = False
'''''''''        cmdChkReset.Visible = False
'''''''''    End If
'''''''''    lblPrint.Visible = False
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdPrint_Click()
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If Not (lstDisb.Visible) Then
'''''''''    Call cmdView_Click
'''''''''End If
'''''''''If (lstDisb.ListCount > 0) Then
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''    Set ApplTemp.lstBox = lstDisb
'''''''''    Call ApplTemp.PrintProof(0, Trim(txtCheck.Text), "")
'''''''''    Set ApplTemp = Nothing
'''''''''    lstDisb.Visible = False
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdView_Click()
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If Not (lstDisb.Visible) Then
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''    Set ApplTemp.lstBox = lstDisb
'''''''''    Call ApplTemp.ApplLoadCheckDisplay(Trim(txtCheck.Text), "P", CheckId)
'''''''''    Set ApplTemp = Nothing
'''''''''    lstDisb.Visible = True
'''''''''Else
'''''''''    lstDisb.Visible = False
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub cmdVIns_Click()
'''''''''If (PatientId > 0) Then
'''''''''    frmViewFinancial.PatientId = PatientId
'''''''''    frmViewFinancial.ThePolicyHolder1 = PatPol1
'''''''''    frmViewFinancial.ThePolicyHolder2 = PatPol2
'''''''''    If (frmViewFinancial.FinancialLoadDisplay(PatientId, PatPol1, PatPol2, "M")) Then
'''''''''        frmViewFinancial.Show 1
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub DD1Click()
'''''''''
'''''''''If dd1.Caption = "q" Then
'''''''''    With lstP
'''''''''        .Clear
'''''''''        .AddItem "Close list"
'''''''''        Select Case lstPmntInd
'''''''''        Case 0
'''''''''            Select Case lstPmnt(0).col
'''''''''            Case 1
'''''''''                Dim r As Integer
'''''''''                Dim RetCode As New PracticeCodes
'''''''''                RetCode.ReferenceType = "PAYABLETYPE"
'''''''''                RetCode.ReferenceCode = ""
'''''''''                    If (RetCode.FindCodePartial > 0) Then
'''''''''                        r = 1
'''''''''                        While (RetCode.SelectCode(r))
'''''''''                            .AddItem Trim(Mid(RetCode.ReferenceCode, 1, InStrPS(1, RetCode.ReferenceCode, "-") - 1)) _
'''''''''                            & Space(200) & myTrim(RetCode.ReferenceCode, 1)
'''''''''                            r = r + 1
'''''''''                        Wend
'''''''''                    End If
'''''''''                Set RetCode = Nothing
'''''''''                .Selected(0) = True
'''''''''            End Select
'''''''''        Case 1
'''''''''            With txtC
'''''''''                .Text = lstPmnt(2).TextMatrix(lstPmnt(2).Row, 15)
'''''''''                .Top = pTxt1.Top + 1560
'''''''''                .Left = pTxt1.Left + dd1.Left + 330 - .Width
'''''''''                .Visible = True
'''''''''                .SetFocus
'''''''''            End With
'''''''''            Exit Sub
'''''''''        End Select
'''''''''        DoEvents
'''''''''        .Left = 0
'''''''''        .Top = lstPmnt(0).Top + lstPmnt(0).RowPos(lstPmnt(0).Row) + lstPmnt(0).RowHeight(0)
'''''''''        .Height = frPmnt(1).Height - .Top + 90
'''''''''        .Width = frPmnt(1).Width - .Left
'''''''''        .Visible = True
'''''''''        .SetFocus
'''''''''    End With
'''''''''Else
'''''''''    txt1.SetFocus
'''''''''End If
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub dd1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'''''''''DD1Click
'''''''''If dd1.Caption = "q" Then
'''''''''    dd1.Caption = "p"
'''''''''Else
'''''''''    dd1.Caption = "q"
'''''''''End If
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub Form_Load()
'''''''''
'''''''''Dim ADate As String
'''''''''If UserLogin.HasPermission(epPowerUser) Then
'''''''''    frmLineItem.BorderStyle = 1
'''''''''    frmLineItem.ClipControls = True
'''''''''    frmLineItem.Caption = Mid(frmLineItem.Name, 4, Len(frmLineItem.Name) - 3)
'''''''''    frmLineItem.AutoRedraw = True
'''''''''    frmLineItem.Refresh
'''''''''End If
'''''''''UserId = UserLogin.iId
'''''''''AllOn = False
'''''''''''''''TransitionInsurerOn = False
'''''''''CheckId = 0
'''''''''RunningCheckBalance = ""
'''''''''AdjustmentCode = "X"
'''''''''ADate = ""
'''''''''Call FormatTodaysDate(ADate, False)
'''''''''txtDate.Text = ADate
'''''''''cmdComment.Visible = False
'''''''''cmdAll.Text = "Show All Items"
'''''''''cmdAll.Visible = False
'''''''''cmdChkReset.Visible = False
'''''''''lstDisb.Clear
'''''''''lstDisb.Visible = False
'''''''''lblInst.Visible = False
''''''''''frmLineItem.KeyPreview = True
'''''''''
'''''''''pTxt1.Height = lstPmnt(0).RowHeight(1) - 15
'''''''''
'''''''''Dim cntl As control
'''''''''For Each cntl In Me
'''''''''    On Error Resume Next
'''''''''    cntl.TabStop = False
'''''''''Next
'''''''''initPmnt
'''''''''lstPmnt_MouseDown 0, 0, 0, 0, 0
'''''''''
'''''''''txt1.Text = "Check                    "
'''''''''lstPmnt(0).TextMatrix(1, 1) = "Check                    "
'''''''''lstPmntInfo.TextMatrix(1, 1) = "K"
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub initPmnt()
'''''''''lblWB.Visible = False
'''''''''
'''''''''
'''''''''lstInfo.FormatString = "Major Med" + vbTab _
'''''''''            + "Vision" + vbTab _
'''''''''            + "Ambulatory" + vbTab _
'''''''''            + "Workers Comp" + vbTab
'''''''''
'''''''''lstInfo.ColWidth(0) = 300 * 15
'''''''''lstInfo.ColWidth(1) = 170 * 15
'''''''''lstInfo.ColWidth(2) = 170 * 15
'''''''''lstInfo.ColWidth(3) = 170 * 15
'''''''''
'''''''''
'''''''''
'''''''''Dim DisplayText  As String
'''''''''Dim j As Integer
'''''''''DisplayText = "Payer" + vbTab _
'''''''''            + "Method" + vbTab _
'''''''''            + "Amt" + vbTab _
'''''''''            + "Check or Ref #" + vbTab _
'''''''''            + "Pay Date" + vbTab _
'''''''''            + "EOB Date"
'''''''''lstPmnt(0).FormatString = DisplayText
'''''''''
'''''''''With lstPmnt(0)
'''''''''For j = 0 To .Cols - 1
'''''''''    Select Case j
'''''''''    Case 0
'''''''''        .ColWidth(j) = 157 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 1
'''''''''        .ColWidth(j) = 156 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 2
'''''''''        .ColWidth(j) = 70 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 3
'''''''''        .ColWidth(j) = 225 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 4
'''''''''        .ColWidth(j) = 80 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 5
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''        .ColWidth(j) = 80 * 15
'''''''''    End Select
'''''''''    Next j
'''''''''    .Width = 769 * 15
'''''''''    .Row = 0
'''''''''    For j = 0 To .Cols - 1
'''''''''        .col = j
'''''''''        .CellAlignment = 1
'''''''''    Next j
'''''''''    .Row = 1
'''''''''    .col = 0
'''''''''End With
'''''''''
'''''''''With lstPmnt(1)
'''''''''DisplayText = "Patient" + vbTab + _
'''''''''              "" + vbTab + _
'''''''''              "Remaining" + vbTab + _
'''''''''              "Paid" + vbTab + _
'''''''''              "Adjust." + vbTab + _
'''''''''              "" + vbTab + _
'''''''''              "Paid" + vbTab + _
'''''''''              "Adjust."
'''''''''.FormatString = DisplayText
'''''''''
'''''''''For j = 0 To .Cols - 1
'''''''''    Select Case j
'''''''''    Case 0
'''''''''        .ColWidth(j) = 157 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 1
'''''''''        .ColWidth(j) = 157 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 2, 3, 4, 5, 6, 7
'''''''''        .ColWidth(j) = 70 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    End Select
'''''''''    Next j
'''''''''    .Row = 0
'''''''''    Dim w As Integer
'''''''''    For j = 0 To .Cols - 1
'''''''''        .col = j
'''''''''        .CellAlignment = 1
'''''''''        w = w + .ColWidth(j)
'''''''''    Next j
'''''''''    .Row = 1
'''''''''    .col = 0
'''''''''    .Width = w
'''''''''End With
'''''''''
''''''''''------------
'''''''''With lstPmnt(2)
'''''''''    DisplayText = "itemid" + vbTab _
'''''''''            + "Srv Date" + vbTab _
'''''''''            + "Service" + vbTab _
'''''''''            + "Mo" + vbTab _
'''''''''            + "Charge" + vbTab _
'''''''''            + "Prv Bal" + vbTab _
'''''''''            + "Allowed" + vbTab _
'''''''''            + "Payment" + vbTab _
'''''''''            + "CntrWO" + vbTab _
'''''''''            + "CoIns" + vbTab _
'''''''''            + "Deduct" + vbTab _
'''''''''            + "Balance" + vbTab _
'''''''''            + vbTab _
'''''''''            + "N" + vbTab _
'''''''''            + "A" + vbTab _
'''''''''            + "Cmnt" + vbTab _
'''''''''            + "P" + vbTab _
'''''''''            + "OthAdj" + vbTab _
'''''''''            + "Rsn" + vbTab _
'''''''''            + "D"
'''''''''    .FormatString = DisplayText
'''''''''
'''''''''    For j = 0 To .Cols - 1
'''''''''    Select Case j
'''''''''    Case 0
'''''''''        .ColWidth(j) = 0
'''''''''    Case 1
'''''''''        .ColWidth(j) = 75 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 2
'''''''''        .ColWidth(j) = 55 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 3
'''''''''        .ColWidth(j) = 20 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 4
'''''''''        .ColWidth(j) = 56 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 5, 11
'''''''''        .ColWidth(j) = 61 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 6, 7, 8, 11
'''''''''        .ColWidth(j) = 55 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 17
'''''''''        .ColWidth(j) = 55 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 9, 10
'''''''''        .ColWidth(j) = 50 * 15
'''''''''        .ColAlignment(j) = flexAlignRightCenter
'''''''''    Case 12
'''''''''        .ColWidth(j) = 0
'''''''''    Case 13, 14
'''''''''        .ColWidth(j) = 15 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 15
'''''''''        .ColWidth(j) = 30 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 16
'''''''''        .ColWidth(j) = 15 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 18
'''''''''        .ColWidth(j) = 30 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    Case 19
'''''''''        .ColWidth(j) = 15 * 15
'''''''''        .ColAlignment(j) = flexAlignLeftCenter
'''''''''    End Select
'''''''''    Next j
'''''''''    .Row = 0
'''''''''    For j = 0 To .Cols - 1
'''''''''        .col = j
'''''''''        .CellAlignment = 1
'''''''''    Next j
'''''''''End With
'''''''''
'''''''''Dim r As Integer
'''''''''    With lstPmnt(0)
'''''''''        .TextMatrix(1, 0) = ""
'''''''''        .TextMatrix(1, 1) = ""
'''''''''        .TextMatrix(1, 2) = "0.00"
'''''''''        .TextMatrix(1, 3) = ""
'''''''''        .TextMatrix(1, 4) = Format(Now, "mm/dd/yyyy")
'''''''''        .TextMatrix(1, 5) = ""
'''''''''    End With
'''''''''    With lstPmnt(1)
'''''''''        .TextMatrix(1, 0) = ""
'''''''''        .TextMatrix(1, 1) = ""
'''''''''        .TextMatrix(1, 2) = "0.00"
'''''''''        .TextMatrix(1, 3) = "0.00"
'''''''''        .TextMatrix(1, 4) = "0.00"
'''''''''        .TextMatrix(1, 5) = ""
'''''''''        .TextMatrix(1, 6) = "0.00"
'''''''''        .TextMatrix(1, 7) = "0.00"
'''''''''
'''''''''    End With
'''''''''
''''''''''initPmntSrvs RetPay
''''''''''Call lstPmnt_Click(0)
''''''''''Set RetPay = Nothing
'''''''''
'''''''''End Sub
'''''''''
'''''''''
'''''''''
'''''''''
'''''''''Private Sub fpBtn_Click(Index As Integer)
'''''''''Select Case Index
'''''''''Case 0
'''''''''    Unload Me
'''''''''Case 1
''''''''''    Dim i As Integer, g As Integer
''''''''''    Dim currAptID As String
''''''''''
''''''''''
''''''''''    If CompletedPayment Then
''''''''''        For i = 1 To lstPmnt(2).Rows - 1
''''''''''            currAptID = lstPmnt(3).TextMatrix(i, 4)
''''''''''            If Entered(currAptID) Then ' SKIP INVOICES THAT HAVE NO PAYMENT/ADJ ENTRIES !!!!!!!!!!!!!!!
''''''''''    '====================================================
''''''''''                frmBillServices.HiddenMode = True
''''''''''                If frmBillServices.LoadServices( _
''''''''''                            lstPmnt(3).TextMatrix(i, 6), _
''''''''''                            CLng(currAptID) _
''''''''''                            ) Then
''''''''''                    With frmBillServices
''''''''''                        g = 1
''''''''''                        Do While currAptID = lstPmnt(3).TextMatrix(i, 4)
''''''''''                            If lstPmnt(2).TextMatrix(i, 13) = "" _
''''''''''                            Or lstPmnt(2).TextMatrix(i, 14) = "" Then
''''''''''                                .lstSrvs1.TextMatrix(g, 6) = "X"
''''''''''                            Else
''''''''''                                .lstSrvs1.TextMatrix(g, 4) = _
''''''''''                                lstPmnt(2).TextMatrix(i, 11)
''''''''''                                .lstSrvs1.TextMatrix(g, 5) = _
''''''''''                                lstPmnt(2).TextMatrix(i, 13)
''''''''''                                .lstSrvs1.TextMatrix(g, 6) = _
''''''''''                                lstPmnt(2).TextMatrix(i, 14)
''''''''''                            End If
''''''''''                            g = g + 1
''''''''''                            i = i + 1
''''''''''                            If i = lstPmnt(2).Rows Then Exit Do
''''''''''                        Loop
''''''''''                        '.Show 1
''''''''''                        Call .BatchOut_5(CStr(currAptID))
''''''''''                        i = i - 1
''''''''''                    End With
''''''''''                End If
''''''''''                frmBillServices.HiddenMode = False
''''''''''                Unload frmBillServices
''''''''''    '====================================================
''''''''''            End If
''''''''''        Next
''''''''''
''''''''''
''''''''''        Call CompleteCurrentPatient_5(PatientId)
''''''''''        lstPmnt(3).Rows = 1
''''''''''        lstPmnt(2).Rows = 1
''''''''''        lstPmnt(1).TextMatrix(1, 0) = ""
''''''''''        lstPmnt(1).col = 0
''''''''''        lstPmnt_Click 1
''''''''''
''''''''''
''''''''''        'Disburse True
''''''''''    End If
'''''''''End Select
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub fpBtn2_Click()
'''''''''frmPaymentsTotal.PatientId = PatientId
'''''''''frmPaymentsTotal.ReceivableId = 0
'''''''''frmPaymentsTotal.Updated = False
'''''''''If frmPaymentsTotal.LoadPaymentsTotal("") Then
'''''''''    frmPaymentsTotal.fpBtnPmnt.Visible = False
'''''''''    frmPaymentsTotal.Show 1
'''''''''    frmPaymentsTotal.fpBtnPmnt.Visible = True
'''''''''    If frmPaymentsTotal.Updated Then
'''''''''        frmPaymentsTotal.Updated = False
'''''''''        Unload frmPaymentsTotal
'''''''''        Disburse AllOn
'''''''''        pTxt1.Visible = False
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub lstDisb_Click()
'''''''''Dim Amt As String
'''''''''Dim Temp As String
'''''''''Dim AmtLeft As String
'''''''''Dim InsName As String
'''''''''Dim ChkNo As String
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If (GetCheckOn) Then
'''''''''    If (lstDisb.ListIndex > 0) Then
'''''''''        Set ApplTemp = New ApplicationTemplates
'''''''''        cmdApply.Enabled = True
'''''''''        cmdHome.Enabled = True
'''''''''        cmdDisburse.Enabled = True
'''''''''        cmdPrint.Enabled = True
'''''''''        cmdView.Enabled = True
'''''''''        txtAmount.Enabled = True
'''''''''        txtPatient.Enabled = True
'''''''''        txtCheck.Enabled = True
'''''''''        txtDate.Enabled = True
'''''''''        GetCheckOn = False
'''''''''        CheckId = Val(Trim(Mid(lstDisb.List(lstDisb.ListIndex), 56, Len(lstDisb.List(lstDisb.ListIndex)))))
'''''''''        If Not (ApplTemp.ApplGetTheCheck(CheckId, InsName, ChkNo, Amt, AmtLeft)) Then
'''''''''            txtCheck.SetFocus
'''''''''        Else
'''''''''            RunningCheckBalance = Trim(AmtLeft)
'''''''''            Call DisplayDollarAmount(Trim(Str(Amt)), Temp)
'''''''''            txtAmount.Text = Trim(Temp)
'''''''''            lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(Str(AmtLeft))
'''''''''            lblAmountLeft.Tag = Trim(Str(AmtLeft))
'''''''''            txtPatient.SetFocus
'''''''''        End If
'''''''''        Set ApplTemp = Nothing
'''''''''        lstDisb.Visible = False
'''''''''    ElseIf (lstDisb.ListIndex = 0) Then
'''''''''        cmdApply.Enabled = True
'''''''''        cmdHome.Enabled = True
'''''''''        cmdDisburse.Enabled = True
'''''''''        cmdPrint.Enabled = True
'''''''''        cmdView.Enabled = True
'''''''''        txtAmount.Enabled = True
'''''''''        txtPatient.Enabled = True
'''''''''        txtCheck.Enabled = True
'''''''''        txtDate.Enabled = True
'''''''''        txtCheck.SetFocus
'''''''''        GetCheckOn = False
'''''''''        lstDisb.Visible = False
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub lstMethod_Click()
'''''''''If (lstMethod.ListIndex >= 0) Then
'''''''''    PaymentMethod = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub lstP_DblClick()
'''''''''
'''''''''If Not lstP.ListIndex = 0 Then
'''''''''    'FromCredit False
'''''''''    If lstPmntInd = 0 _
'''''''''    And lstPmnt(0).col = 1 Then
'''''''''        txt1.Text = Mid(lstP.Text, 1, 25)
'''''''''        lstPmnt(0).TextMatrix(1, 1) = txt1.Text
'''''''''        lstPmntInfo.TextMatrix(1, 1) = myTrim(lstP.Text, 1)
'''''''''    Else
'''''''''        txt1.Text = lstP.Text
'''''''''    End If
'''''''''End If
'''''''''txt1.SetFocus
'''''''''
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub lstP_KeyDown(KeyCode As Integer, Shift As Integer)
'''''''''Select Case KeyCode
'''''''''Case vbKeyReturn
'''''''''    Call lstP_DblClick
'''''''''Case vbKeyEscape, vbKeyTab
'''''''''    txt1.SetFocus
'''''''''End Select
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub lstP_LostFocus()
'''''''''lstP.Visible = False
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub lstPmnt_EnterCell(Index As Integer)
'''''''''    editCellPmnt Index
'''''''''End Sub
'''''''''
'''''''''Private Sub lstPmnt_GotFocus(Index As Integer)
'''''''''    editCellPmnt Index
'''''''''End Sub
'''''''''
'''''''''
'''''''''Private Function LstCompleted() As Integer
''''''''''Exit Function
'''''''''
'''''''''Dim msg As String
'''''''''
'''''''''Select Case True
'''''''''Case Trim(lstPmnt(0).TextMatrix(1, 0)) = ""
'''''''''    msg = "Please select an Payer."
'''''''''    LstCompleted = 1
'''''''''Case Trim(lstPmnt(0).TextMatrix(1, 3)) = ""
'''''''''    msg = "Please enter a check number."
'''''''''    LstCompleted = 4
'''''''''End Select
'''''''''
'''''''''If msg = "" Then
'''''''''    If lstPmnt(0).Enabled Then
'''''''''        Dim RetPay As New PatientReceivablePayment
'''''''''        Dim mPaymentDate As String
'''''''''        mPaymentDate = Format(lstPmnt(0).TextMatrix(1, 4), "yyyymmdd")
'''''''''
'''''''''        mCheckExist = RetPay.CheckExist(CDbl(lstPmnt(0).TextMatrix(1, 2)), _
'''''''''                                  lstPmnt(0).TextMatrix(1, 3), _
'''''''''                           mPaymentDate, _
'''''''''                           Format(lstPmnt(0).TextMatrix(1, 5), "yyyymmdd"))
'''''''''
'''''''''        If mCheckExist = 0 Then
''''''''''''            If lstPmnt(0).TextMatrix(1, 2) = 0 Then
''''''''''''                msg = "Please enter payment amount."
''''''''''''                LstCompleted = 3
''''''''''''            End If
'''''''''        Else
'''''''''            msg = "Check from " & _
'''''''''                  RetPay.getPayerName(RetPay.PaymentPayerType, RetPay.PaymentPayerId) & _
'''''''''                  " already in use.  Continue with this batch?"
'''''''''            LstCompleted = 5
'''''''''        End If
'''''''''        Set RetPay = Nothing
'''''''''
'''''''''    End If
'''''''''End If
'''''''''
'''''''''
'''''''''If Not msg = "" Then
'''''''''    frmEventMsgs.Header = msg
'''''''''    frmEventMsgs.AcceptText = ""
'''''''''    frmEventMsgs.RejectText = "Ok"
'''''''''    If LstCompleted = 5 Then
'''''''''        frmEventMsgs.CancelText = "New batch with diff check #"
'''''''''    Else
'''''''''        frmEventMsgs.CancelText = ""
'''''''''    End If
'''''''''    frmEventMsgs.Other0Text = ""
'''''''''    frmEventMsgs.Other1Text = ""
'''''''''    frmEventMsgs.Other2Text = ""
'''''''''    frmEventMsgs.Other3Text = ""
'''''''''    frmEventMsgs.Other4Text = ""
'''''''''    frmEventMsgs.Show 1
'''''''''End If
'''''''''DoEvents
'''''''''If LstCompleted = 5 Then
'''''''''    If frmEventMsgs.Result = 2 Then
'''''''''        lstPmnt(0).TextMatrix(1, 4) = Mid(mPaymentDate, 5, 2) & "/" & Mid(mPaymentDate, 7, 2) & "/" & Mid(mPaymentDate, 1, 4)
'''''''''        LstCompleted = 0
'''''''''    End If
'''''''''End If
'''''''''End Function
'''''''''
'''''''''
'''''''''Private Sub Disburse(OverrideOn As Boolean)
'''''''''Dim res As Boolean
'''''''''res = LoadPayments_5(PatientId, True, True)
'''''''''lstPmnt(2).Enabled = res
'''''''''If res Then
'''''''''    lstPmnt(0).Enabled = False
'''''''''    lstPmnt(2).col = 6
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Function LoadPayments_5(PatId As Long, AllOn As Boolean, Reload As Boolean) As Boolean
'''''''''Dim Bs As Boolean
'''''''''Dim CurRow As Integer
'''''''''Dim ApplList As ApplicationAIList
'''''''''LoadPayments_5 = False
'''''''''FirstTimeSet = False
'''''''''GetCheckOn = False
'''''''''If (PatId > 0) Then
'''''''''    CurRow = CurrentRow
'''''''''    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
'''''''''    lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''''    lblAmountLeft.Visible = True
'''''''''    lblAdj.Visible = False
'''''''''    txtAdj.Visible = False
'''''''''    chkInclude.Value = 0
'''''''''    chkInclude.Visible = False
'''''''''    Set ApplList = New ApplicationAIList
'''''''''    Set ApplList.ApplGrid = lstPmnt(2)
'''''''''    Set ApplList.ApplGrid1 = lstPmnt(3)
'''''''''    If (Reload) Then
'''''''''        lblPrint.Caption = "Loading Grid Entries"
'''''''''        lblPrint.Visible = True
'''''''''        Bs = MSFlexGrid1.Visible
'''''''''        MSFlexGrid1.Visible = False
'''''''''        DoEvents
'''''''''        LoadPayments_5 = ApplList.ApplLoadPaymentsbyServicesForPatients_5(PatId, PatPol1, AllOn)
'''''''''
'''''''''        lblPrint.Visible = False
'''''''''        MSFlexGrid1.Visible = Bs
'''''''''    End If
''''''''''    AdjustmentCode = "O"
'''''''''    AdjustmentCode = "X"
'''''''''    Set ApplList = Nothing
'''''''''    Call SetGridTotals
'''''''''    cmdAll.Visible = LoadPayments_5
'''''''''    cmdChkReset.Visible = cmdAll.Visible
'''''''''    CurrentRow = CurRow
'''''''''    If (CurrentRow < 0) Or (CurrentRow > 12) Or (CurrentRow > MSFlexGrid1.Row) Then
'''''''''        CurrentRow = 1
'''''''''    End If
'''''''''    MSFlexGrid1.RowSel = CurrentRow
'''''''''End If
'''''''''End Function
'''''''''
'''''''''Private Function MoneyLeft(ARow As Integer) As Single
'''''''''Dim i As Integer
'''''''''Dim Amt As Single
'''''''''Amt = 0
'''''''''MoneyLeft = 0
'''''''''If (MSFlexGrid1.Visible) Then
'''''''''    For i = 0 To MSFlexGrid1.Rows - 1
'''''''''        If (i <> ARow) Then
'''''''''            Amt = Amt + Val(Trim(MSFlexGrid1.TextMatrix(i, 6)))
'''''''''        End If
'''''''''    Next i
'''''''''End If
'''''''''MoneyLeft = Int(((Val(Trim(RunningCheckBalance)) - Amt) + 0.004) * 100) / 100
'''''''''End Function
'''''''''
'''''''''
'''''''''Public Sub LoadPatient(lst As Integer)
'''''''''Dim PatId As Long
'''''''''Dim b1 As Boolean, b2 As Boolean
'''''''''Dim InsName(6) As String
'''''''''Dim r1 As String, r2 As String
'''''''''Dim Temp As String, AName As String
'''''''''Dim IName As String
'''''''''Dim ApplTemp As New ApplicationTemplates
'''''''''
'''''''''PatId = frmPatientSchedulerSearch.PatientId
'''''''''Call ApplTemp.ApplGetPatientName(PatId, AName, PatPol1, r1, b1, PatPol2, r2, b2, False)
'''''''''
'''''''''VerifyPatient:
'''''''''Temp = ApplTemp.ApplVerifyPatient(PatPol1, 0)
'''''''''
'''''''''If (Temp <> "") Then
'''''''''    frmEventMsgs.Header = Temp
'''''''''    frmEventMsgs.AcceptText = ""
'''''''''    frmEventMsgs.RejectText = "Fix It"
'''''''''    frmEventMsgs.CancelText = "Cancel"
'''''''''    frmEventMsgs.Other0Text = ""
'''''''''    frmEventMsgs.Other1Text = ""
'''''''''    frmEventMsgs.Other2Text = ""
'''''''''    frmEventMsgs.Other3Text = ""
'''''''''    frmEventMsgs.Other4Text = ""
'''''''''    frmEventMsgs.Show 1
'''''''''    If (frmEventMsgs.Result = 2) Then
'''''''''        If (frmPatient.PatientLoadDisplay(PatPol1, False)) Then
'''''''''            frmPatient.Show 1
'''''''''            GoTo VerifyPatient
'''''''''        End If
'''''''''    Else
'''''''''        GoTo ExitSub
'''''''''    End If
'''''''''End If
'''''''''
'''''''''lstPmnt(lst).TextMatrix(1, 0) = AName & Space(100) & PatId
'''''''''If lst = 1 Then
'''''''''    PatientId = PatId
'''''''''End If
'''''''''ExitSub:
'''''''''
'''''''''Set ApplTemp = Nothing
'''''''''End Sub
'''''''''
'''''''''
'''''''''Private Function lstPmntInd() As Integer
'''''''''Select Case pTxt1.Top
'''''''''Case Is > lstPmnt(2).Top
'''''''''    lstPmntInd = 2
'''''''''Case Is > lstPmnt(1).Top
'''''''''    lstPmntInd = 1
'''''''''Case Else
'''''''''    lstPmntInd = 0
'''''''''End Select
'''''''''End Function
'''''''''
'''''''''
'''''''''Private Sub editCellPmnt(i As Integer)
'''''''''
'''''''''Dim fgc As MSFlexGrid
'''''''''Set fgc = lstPmnt(i)
'''''''''
'''''''''Dim c As Integer, r As Integer
'''''''''c = fgc.col
'''''''''r = fgc.Row
'''''''''
'''''''''If i = 0 And c = 0 Then Exit Sub
'''''''''If i = 1 Then Exit Sub
'''''''''If fgc.ColWidth(c) = 0 Then Exit Sub
'''''''''
'''''''''pTxt1.Top = fgc.Top + fgc.RowPos(r) + 15
'''''''''pTxt1.Left = fgc.Left + fgc.ColPos(c) + 15
'''''''''pTxt1.Width = fgc.ColWidth(c) - 30
'''''''''pTxt1.Visible = True
'''''''''bySys = True
'''''''''txt1.MaxLength = 0
'''''''''txt1.Text = fgc.Text
'''''''''txt1.Width = pTxt1.Width - 45
'''''''''If fgc.ColAlignment(c) = flexAlignRightCenter Then
'''''''''    txt1.Alignment = 1
'''''''''Else
'''''''''    txt1.Alignment = 0
'''''''''End If
'''''''''bySys = False
'''''''''
'''''''''dd1.Caption = "q"
'''''''''dd1.Visible = False
'''''''''Select Case i
'''''''''Case 0
'''''''''    Select Case c
'''''''''    Case 1
'''''''''        dd1.Left = pTxt1.Width - dd1.Width + 15
'''''''''        dd1.Visible = True
'''''''''    End Select
'''''''''Case 2
'''''''''    Select Case c
'''''''''    Case 15
'''''''''        dd1.Left = pTxt1.Width - dd1.Width + 15
'''''''''        dd1.Visible = True
'''''''''    End Select
'''''''''    Select Case c
'''''''''    Case 13, 14, 16, 19
'''''''''        txt1.MaxLength = 1
'''''''''    End Select
'''''''''End Select
'''''''''
'''''''''
'''''''''Call SkipCell1(i, c, r)
'''''''''If Not (fgc.col = c And fgc.Row = r) Then
'''''''''    fgc.col = c
'''''''''    fgc.Row = r
'''''''''    Exit Sub
'''''''''End If
'''''''''
'''''''''
'''''''''pTxt1.Visible = True
'''''''''If Not dd1.Visible Then
'''''''''    txt1.SelStart = 0
'''''''''    txt1.SelLength = Len(txt1.Text)
'''''''''End If
'''''''''On Error Resume Next
'''''''''txt1.SetFocus
'''''''''End Sub
'''''''''
'''''''''Private Sub SkipCell1(i As Integer, nC As Integer, nR As Integer, _
'''''''''                     Optional sShift As Integer = 0)
'''''''''
'''''''''    Select Case i
'''''''''    Case 0
'''''''''        Select Case nC
'''''''''        Case Is < 1
'''''''''            If sShift = 0 Then
'''''''''                nC = 1
'''''''''            Else
'''''''''                nC = lstPmnt(1).Cols - 1
'''''''''            End If
'''''''''        End Select
'''''''''    Case 1
'''''''''        If Not nC = 0 Then nC = 0
'''''''''    Case 2
'''''''''        Select Case nC
'''''''''        Case Is < 6
'''''''''            If sShift = 0 Then
'''''''''                nC = 6
'''''''''            Else
'''''''''                nC = lstPmnt(1).Cols - 1
'''''''''            End If
'''''''''        Case 11, 12
'''''''''            If sShift = 0 Then
'''''''''                nC = 13
'''''''''            Else
'''''''''                nC = 10
'''''''''            End If
'''''''''        End Select
'''''''''    End Select
'''''''''End Sub
'''''''''
'''''''''
'''''''''Private Sub lstPmnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'''''''''Select Case Index
'''''''''Case 0
'''''''''    If lstPmnt(Index).col = 0 Then
'''''''''            frmPatientSchedulerSearch.PatientNumberOn = True
'''''''''            frmPatientSchedulerSearch.AddOn = True
'''''''''            frmPatientSchedulerSearch.Header = "Select A Patient"
'''''''''            frmPatientSchedulerSearch.Show 1
'''''''''            If (Trim(frmPatientSchedulerSearch.PatientId) <> 0) Then
'''''''''                LoadPatient 0
'''''''''            End If
'''''''''            Unload frmPatientSchedulerSearch
'''''''''        lstPmnt(0).col = 1
'''''''''    End If
'''''''''Case 1
'''''''''    If lstPmnt(Index).col = 0 Then
'''''''''        Dim e As Integer
'''''''''        e = LstCompleted
'''''''''        Select Case e
'''''''''        Case 0
'''''''''            frmPatientSchedulerSearch.PatientNumberOn = True
'''''''''            frmPatientSchedulerSearch.AddOn = True
'''''''''            frmPatientSchedulerSearch.Header = "Select A Patient"
'''''''''            frmPatientSchedulerSearch.Show 1
'''''''''            If (Trim(frmPatientSchedulerSearch.PatientId) <> 0) Then
'''''''''                LoadPatient 1
'''''''''                Call Disburse(False)
'''''''''            End If
'''''''''            Unload frmPatientSchedulerSearch
'''''''''        Case 1
'''''''''            lstPmnt(0).col = 0
'''''''''            lstPmnt_MouseDown 0, 0, 0, 0, 0
'''''''''        Case Else
'''''''''            lstPmnt(0).col = e - 1
'''''''''            editCellPmnt (0)
'''''''''        End Select
'''''''''    End If
'''''''''    If lstPmnt(2).Rows > 1 Then
'''''''''        editCellPmnt 2
'''''''''    Else
'''''''''        editCellPmnt lstPmntInd
'''''''''    End If
'''''''''End Select
'''''''''End Sub
'''''''''
'''''''''Private Sub txt1_GotFocus()
'''''''''dd1.Caption = "q"
'''''''''If txtC.Visible Then txtC.Visible = False
'''''''''End Sub
'''''''''
'''''''''Private Sub txt1_KeyDown(KeyCode As Integer, Shift As Integer)
'''''''''mKeyCode = 0
'''''''''
'''''''''Dim fgc As MSFlexGrid
'''''''''Set fgc = lstPmnt(lstPmntInd)
'''''''''
'''''''''
'''''''''Dim ExitSub As Boolean
'''''''''Select Case KeyCode
'''''''''Case vbKeyTab, vbKeyReturn
'''''''''    If fgc.Index = 1 Then
'''''''''        If fgc.col = 6 Then
'''''''''            If KeyCode = vbKeyTab Then mKeyCode = vbKeyTab
'''''''''        End If
'''''''''    End If
'''''''''
'''''''''    Call txt1_Validate(ExitSub)
'''''''''    If ExitSub Then Exit Sub
'''''''''Case vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
'''''''''    If dd1.Visible Then KeyCode = 0
'''''''''Case vbKeyPageDown, vbKeyPageUp
'''''''''    Call txt1_Validate(ExitSub)
'''''''''    If ExitSub Then Exit Sub
'''''''''    Dim nextI As Integer
'''''''''    nextI = KeyCode - 34
'''''''''    If nextI = 0 Then nextI = 1
'''''''''    nextI = lstPmntInd + nextI
'''''''''    Select Case nextI
'''''''''    Case 3
'''''''''        nextI = 0
'''''''''    Case -1
'''''''''        nextI = 2
'''''''''    End Select
'''''''''    lstPmnt(nextI).SetFocus
'''''''''Case Else
'''''''''    KeyCode = 0
'''''''''End Select
'''''''''
'''''''''Dim c As Integer, r As Integer
'''''''''c = fgc.col
'''''''''r = fgc.Row
'''''''''Dim nextC As Integer, nextR As Integer
'''''''''With fgc
'''''''''    Select Case KeyCode
'''''''''    Case vbKeyTab
'''''''''        bySys = True
'''''''''        nextR = .Row
'''''''''        If Shift = 0 Then
'''''''''            nextC = .col + 1
'''''''''        Else
'''''''''            nextC = .col - 1
'''''''''        End If
'''''''''
'''''''''        If lstPmntInd = 1 Then
'''''''''            If nextC < 6 Then
'''''''''                nextC = -1
'''''''''            End If
'''''''''        End If
'''''''''
'''''''''        Select Case nextC
'''''''''        Case .Cols
'''''''''            nextC = 0
'''''''''            nextR = .Row + 1
'''''''''        Case Is < 0
'''''''''            nextC = .Cols - 1
'''''''''            nextR = .Row - 1
'''''''''        End Select
'''''''''
'''''''''        Select Case nextR
'''''''''        Case .Rows
'''''''''            nextR = 1
'''''''''        Case 0
'''''''''            nextR = .Rows - 1
'''''''''        End Select
'''''''''
'''''''''        Call SkipCell1(lstPmntInd, nextC, nextR, Shift)
'''''''''        .col = nextC
'''''''''        .Row = nextR
'''''''''        bySys = False
'''''''''        editCellPmnt lstPmntInd
'''''''''    Case vbKeyReturn
'''''''''        txt1.Text = .Text
'''''''''        If dd1.Visible Then
'''''''''            DD1Click
'''''''''            dd1.Caption = "p"
'''''''''        Else
'''''''''            bySys = False
'''''''''            If lstPmntInd = 1 Then
'''''''''                .col = 6
'''''''''                nextR = r + (Shift + 1 - (Shift * 3))
'''''''''            End If
'''''''''            Select Case nextR
'''''''''            Case 0
'''''''''                nextR = .Rows - 1
'''''''''            Case .Rows
'''''''''                nextR = 1
'''''''''            End Select
'''''''''            .Row = nextR
'''''''''            bySys = False
'''''''''            editCellPmnt lstPmntInd
'''''''''        End If
'''''''''
'''''''''    End Select
'''''''''End With
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub txt1_KeyPress(KeyAscii As Integer)
'''''''''If dd1.Visible Then KeyAscii = 0
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub txt1_Validate(Cancel As Boolean)
'''''''''
'''''''''Dim StxtValid As Boolean
'''''''''Dim orgTxt As String
'''''''''Dim corrTxt As String
'''''''''Dim n(5) As Double
'''''''''Dim Temp As String
'''''''''Dim r As Integer
'''''''''Dim k As Integer
'''''''''Dim fgc As MSFlexGrid
'''''''''Set fgc = lstPmnt(lstPmntInd)
'''''''''
'''''''''If fgc.Text = txt1.Text Then
'''''''''    If lstPmntInd = 1 And fgc.col = 6 And mKeyCode = vbKeyTab Then
'''''''''    Else
'''''''''        Exit Sub
'''''''''    End If
'''''''''End If
'''''''''
'''''''''orgTxt = fgc.Text
'''''''''StxtValid = txt1Valid(corrTxt)
'''''''''
'''''''''If orgTxt = corrTxt Then
'''''''''    If fgc.Index = 1 And fgc.col = 6 And mKeyCode = vbKeyTab Then
'''''''''    Else
'''''''''        Exit Sub
'''''''''    End If
'''''''''End If
'''''''''
'''''''''If StxtValid Then
'''''''''    fgc.Text = corrTxt
'''''''''''''''    If lstPmntInd = 0 And Not fgc.col = 4 _
'''''''''''''''    Or lstPmntInd = 2 Then
'''''''''''''''        FromCredit False
'''''''''''''''    End If
'''''''''Else
'''''''''    txt1.Text = fgc.Text
'''''''''    txt1.SelStart = 0
'''''''''    txt1.SelLength = Len(txt1.Text)
'''''''''    txt1.SetFocus
'''''''''    Cancel = True
'''''''''    Exit Sub
'''''''''End If
'''''''''
'''''''''Select Case lstPmntInd
'''''''''Case 0
'''''''''        Select Case lstPmnt(0).col
'''''''''        Case 2, 3, 4
'''''''''            mCheckExist = -1
'''''''''        End Select
'''''''''
'''''''''        Select Case lstPmnt(0).col
'''''''''        Case 2
'''''''''            Call calcRemainingAmt
'''''''''        End Select
'''''''''Case 2
'''''''''    With lstPmnt(2)
'''''''''        Select Case .col
'''''''''        Case 6, 7, 8, 17, 19
'''''''''            '--------------------------
'''''''''            If .col = 6 And mKeyCode = vbKeyTab Then
'''''''''                If Not .Text = 0 Then
'''''''''                    mKeyCode = -1
'''''''''                    For r = 0 To 4
'''''''''                        n(r) = 0
'''''''''                    Next
'''''''''                    If IsNumeric(.TextMatrix(.Row, 4)) Then n(0) = .TextMatrix(.Row, 4) 'Charge
'''''''''                    If IsNumeric(.TextMatrix(.Row, 5)) Then n(1) = .TextMatrix(.Row, 5) 'Prev Bal
'''''''''                    If IsNumeric(.TextMatrix(.Row, 6)) Then n(2) = .TextMatrix(.Row, 6) 'Allowed
'''''''''
'''''''''                    Dim AdjAlowed As Double
'''''''''                    Dim PlanId As String
'''''''''                    PlanId = lstPmntInfo.TextMatrix(1, 0)
'''''''''                    If IsNumeric(PlanId) Then
'''''''''                        Dim RetFee As New PracticeFeeSchedules
'''''''''                        RetFee.SchService = .TextMatrix(.Row, 2)
'''''''''                        RetFee.SchPlanId = PlanId
'''''''''                        If RetFee.FindServiceDirect > 0 Then
'''''''''                            k = 1
'''''''''                            Dim apptDte As String
'''''''''                            'apptDte = Format(lstSrvs2.TextMatrix(lstSrvs1.Row, 9), "yyyymmdd")
'''''''''                            Do While RetFee.SelectService(k)
'''''''''                                If Not apptDte < RetFee.SchStartDate Then
'''''''''                                    If Not apptDte > RetFee.SchEndDate _
'''''''''                                    Or RetFee.SchEndDate = "" Then
'''''''''                                        If IsNumeric(RetFee.SchAdjustmentRate) Then
'''''''''                                            AdjAlowed = n(2) * RetFee.SchAdjustmentRate / 100
'''''''''                                            Exit Do
'''''''''                                        End If
'''''''''                                    End If
'''''''''                                End If
'''''''''                                k = k + 1
'''''''''                            Loop
'''''''''                        End If
'''''''''                        Set RetFee = Nothing
'''''''''                    End If
'''''''''
'''''''''                    If AdjAlowed = 0 Then
'''''''''                        If n(0) = n(1) Then
'''''''''                            If n(0) < n(2) Then
'''''''''                                n(3) = n(0)
'''''''''                            Else
'''''''''                                n(3) = n(2)
'''''''''                            End If
'''''''''                        Else
'''''''''                            n(3) = n(2) - (n(0) - n(1))
'''''''''                        End If
'''''''''                    Else
'''''''''                        n(3) = AdjAlowed
'''''''''                    End If
'''''''''
'''''''''                    If n(3) > n(1) Then
'''''''''                        n(3) = n(1)
'''''''''                    End If
'''''''''
'''''''''                    If n(3) < 0 Then
'''''''''                        n(3) = 0
'''''''''                    End If
'''''''''                    Temp = n(3)
'''''''''                    Call DisplayDollarAmount(Temp, Temp)
'''''''''                    .TextMatrix(.Row, 7) = Trim(Temp)           '<payment
'''''''''
'''''''''                    n(4) = n(0) - n(2)
'''''''''                    If n(4) < 0 Then n(4) = 0
'''''''''                    Temp = n(4)
'''''''''                    Call DisplayDollarAmount(Temp, Temp)
'''''''''                    .TextMatrix(.Row, 8) = Trim(Temp)
'''''''''                End If
'''''''''                Call calcRemainingAmt
'''''''''                mKeyCode = 0
'''''''''            End If
'''''''''            '--------------------------
'''''''''            For r = 0 To 4
'''''''''                n(r) = 0
'''''''''            Next
'''''''''            If IsNumeric(.TextMatrix(.Row, 5)) Then n(0) = .TextMatrix(.Row, 5)
'''''''''            If IsNumeric(.TextMatrix(.Row, 7)) Then n(1) = .TextMatrix(.Row, 7)
'''''''''            If IsNumeric(.TextMatrix(.Row, 8)) Then n(2) = .TextMatrix(.Row, 8)
'''''''''            If IsNumeric(.TextMatrix(.Row, 17)) Then
'''''''''                n(3) = .TextMatrix(.Row, 17)
'''''''''                If n(3) = 0 _
'''''''''                Or .TextMatrix(.Row, 19) = "" Then
'''''''''                    n(3) = 0
'''''''''                Else
'''''''''                    Dim RetrieveCode As New PracticeCodes
'''''''''                    RetrieveCode.ReferenceType = "paymenttype"
'''''''''                    If (RetrieveCode.FindCode > 0) Then
'''''''''                        k = 1
'''''''''                        Do Until (Not (RetrieveCode.SelectCode(k)))
'''''''''                            If .TextMatrix(.Row, 19) = myTrim(RetrieveCode.ReferenceCode, 1) Then
'''''''''                                Exit Do
'''''''''                            End If
'''''''''                            k = k + 1
'''''''''                        Loop
'''''''''                    End If
'''''''''
'''''''''                    Select Case RetrieveCode.ReferenceAlternateCode
'''''''''                    Case "C"
'''''''''                        n(3) = -n(3)
'''''''''                    Case "D"
'''''''''                    Case Else
'''''''''                        n(3) = 0
'''''''''                    End Select
'''''''''                    Set RetrieveCode = Nothing
'''''''''                End If
'''''''''            End If
'''''''''            n(4) = n(0) - n(1) - n(2) + n(3)
'''''''''
'''''''''            Temp = n(4)
'''''''''            Call DisplayDollarAmount(Temp, Temp)
'''''''''            .TextMatrix(.Row, 11) = Trim(Temp)
'''''''''
'''''''''            lblWB.Visible = False
'''''''''            For r = 1 To .Rows - 1
'''''''''                If .TextMatrix(r, 11) < 0 Then
'''''''''                   lblWB.Visible = True
'''''''''                   Exit For
'''''''''                End If
'''''''''            Next
'''''''''            Select Case .col
'''''''''            Case 7, 8, 17, 19
'''''''''                Call calcRemainingAmt
'''''''''            End Select
'''''''''        End Select
'''''''''        Select Case .col
'''''''''        Case 6, 7, 8, 9, 10, 17, 19
'''''''''            'Call checkGroup
'''''''''        End Select
'''''''''    End With
'''''''''Case 2
''''''''''        Select Case lstPmnt(2).col
''''''''''        Case 0
''''''''''            GoSub calcRemainingAmt
''''''''''        End Select
'''''''''End Select
'''''''''
'''''''''End Sub
'''''''''
'''''''''Private Sub calcRemainingAmt()
'''''''''Dim n(5) As Single
'''''''''Dim r As Integer
'''''''''Dim Temp As String
'''''''''For r = 0 To 5
'''''''''    n(r) = 0
'''''''''Next
'''''''''With lstPmnt(2)
'''''''''    For r = 1 To .Rows - 1
'''''''''        If IsNumeric(.TextMatrix(r, 7)) Then
'''''''''            n(1) = n(1) + .TextMatrix(r, 7)
'''''''''        End If
'''''''''        If IsNumeric(.TextMatrix(r, 8)) Then
'''''''''            n(2) = n(2) + .TextMatrix(r, 8)
'''''''''        End If
'''''''''        If IsNumeric(.TextMatrix(r, 17)) _
'''''''''        And Not .TextMatrix(r, 19) = "" Then
'''''''''            n(3) = .TextMatrix(r, 17) * CreditDebit(.TextMatrix(r, 19))
'''''''''            Select Case .TextMatrix(r, 19)
'''''''''            Case "U", "R"
'''''''''                n(4) = n(4) + n(3)
'''''''''            Case Else
'''''''''                n(5) = n(5) + n(3)
'''''''''            End Select
'''''''''        End If
'''''''''    Next
'''''''''
'''''''''    '4
'''''''''    n(0) = Round(n(1), 2) + Round(n(4), 2)
'''''''''    Call DisplayDollarAmount(Round(n(0), 2), Temp)
'''''''''    lstPmnt(1).TextMatrix(1, 6) = Trim(Temp)
'''''''''    '5
'''''''''    n(0) = n(2) + n(5)
'''''''''    Call DisplayDollarAmount(Round(n(0), 2), Temp)
'''''''''    lstPmnt(1).TextMatrix(1, 7) = Trim(Temp)
'''''''''End With
'''''''''End Sub
'''''''''
'''''''''
'''''''''Private Function CreditDebit(PaymentType As String) As Integer
'''''''''Dim SQL As String
'''''''''Dim RS As Recordset
'''''''''
'''''''''SQL = _
'''''''''"select AlternateCode  from PracticeCodeTable " & _
'''''''''"where ReferenceType = 'PaymentType' " & _
'''''''''"and Right(Code, 1) = '" & PaymentType & "'"
'''''''''Set RS = GetRS(SQL)
'''''''''
'''''''''Select Case RS("AlternateCode")
'''''''''Case "C"
'''''''''    CreditDebit = 1
'''''''''Case "D"
'''''''''    CreditDebit = -1
'''''''''Case Else
'''''''''    CreditDebit = 0
'''''''''End Select
'''''''''End Function
'''''''''
'''''''''
'''''''''Private Function txt1Valid(corrTxt As String) As Boolean
'''''''''Dim FtxtValid As Boolean
'''''''''corrTxt = Trim(txt1.Text)
'''''''''
'''''''''Select Case lstPmntInd
'''''''''Case 0
'''''''''    Select Case lstPmnt(0).col
'''''''''    Case 2
'''''''''            If Trim(corrTxt) = "" Then corrTxt = 0
'''''''''            If IsNumeric(corrTxt) Then
'''''''''                Call DisplayDollarAmount(corrTxt, corrTxt)
'''''''''                FtxtValid = True
'''''''''            End If
'''''''''    Case 4
'''''''''            If Not corrTxt = "" Then FtxtValid = OkDate(corrTxt)
'''''''''    Case 5
'''''''''            corrTxt = Trim(corrTxt)
'''''''''            If corrTxt = "" Then
'''''''''                FtxtValid = True
'''''''''            Else
'''''''''                FtxtValid = OkDate(corrTxt)
'''''''''            End If
'''''''''    Case Else
'''''''''            FtxtValid = True
'''''''''    End Select
'''''''''Case 2
'''''''''    Select Case lstPmnt(2).col
'''''''''    Case 6, 7, 8, 9, 10, 17
'''''''''            If Trim(corrTxt) = "" Then corrTxt = 0
'''''''''            If IsNumeric(corrTxt) Then
'''''''''                Call DisplayDollarAmount(corrTxt, corrTxt)
'''''''''                FtxtValid = True
'''''''''            End If
'''''''''    Case 13
'''''''''            Select Case UCase(Trim(corrTxt))
'''''''''            Case "", "P"
'''''''''                corrTxt = UCase(corrTxt)
'''''''''                FtxtValid = True
'''''''''            Case Else
'''''''''                If IsNumeric(Trim(corrTxt)) Then
'''''''''                    lstP.Clear
'''''''''                    'Call GetInsRecs(PatientId, "", lstP)
'''''''''                    Select Case CInt(Trim(corrTxt))
'''''''''                    Case 1 To lstP.ListCount - 2
'''''''''                        FtxtValid = True
'''''''''                    End Select
'''''''''                End If
'''''''''            End Select
'''''''''    Case 14
'''''''''            Select Case UCase(Trim(corrTxt))
'''''''''            Case "", "B", "P", "W", "A", "U"
'''''''''                corrTxt = UCase(corrTxt)
'''''''''                FtxtValid = True
'''''''''            End Select
'''''''''    Case 16
'''''''''            Select Case UCase(Trim(corrTxt))
'''''''''            Case "", "Y"
'''''''''                corrTxt = UCase(corrTxt)
'''''''''                FtxtValid = True
'''''''''            End Select
'''''''''    Case 19
'''''''''            Select Case UCase(Trim(corrTxt))
'''''''''            Case ""
'''''''''                corrTxt = UCase(corrTxt)
'''''''''                FtxtValid = True
'''''''''            Case Else
'''''''''                Dim k As Integer
'''''''''                Dim RetrieveCode As New PracticeCodes
'''''''''                RetrieveCode.ReferenceType = "paymenttype"
'''''''''                If (RetrieveCode.FindCode > 0) Then
'''''''''                    k = 1
'''''''''                    Do Until (Not (RetrieveCode.SelectCode(k)))
'''''''''                        If UCase(Trim(corrTxt)) = myTrim(RetrieveCode.ReferenceCode, 1) Then
'''''''''                            corrTxt = UCase(corrTxt)
'''''''''                            FtxtValid = True
'''''''''                            Exit Do
'''''''''                        End If
'''''''''                        k = k + 1
'''''''''                    Loop
'''''''''                End If
'''''''''                Set RetrieveCode = Nothing
'''''''''            End Select
'''''''''    Case Else
'''''''''        FtxtValid = True
'''''''''    End Select
'''''''''End Select
'''''''''
'''''''''If Not FtxtValid Then
'''''''''    With frmEventMsgs
'''''''''        .Header = "Invalid value"
'''''''''        .AcceptText = ""
'''''''''        .RejectText = "OK"
'''''''''        .CancelText = ""
'''''''''        .Other0Text = ""
'''''''''        .Other1Text = ""
'''''''''        .Other2Text = ""
'''''''''        .Other3Text = ""
'''''''''        .Other4Text = ""
'''''''''        .Show 1
'''''''''    End With
'''''''''End If
'''''''''
'''''''''corrTxt = Trim(corrTxt)
'''''''''txt1Valid = FtxtValid
'''''''''End Function
'''''''''
'''''''''
'''''''''
'''''''''Private Sub txtCheck_Validate(Cancel As Boolean)
'''''''''If (Trim(txtCheck.Text) <> "") Then
'''''''''    Call txtCheck_KeyPress(13)
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtCheck_KeyPress(KeyAscii As Integer)
'''''''''Dim Amt As String
'''''''''Dim AmtLeft As String
'''''''''Dim CheckDate As String
'''''''''Dim Temp As String
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''If (Trim(txtPatient.Text) = "") Then
'''''''''    frmEventMsgs.Header = "Select Patient First"
'''''''''    frmEventMsgs.AcceptText = ""
'''''''''    frmEventMsgs.RejectText = "Ok"
'''''''''    frmEventMsgs.CancelText = ""
'''''''''    frmEventMsgs.Other0Text = ""
'''''''''    frmEventMsgs.Other1Text = ""
'''''''''    frmEventMsgs.Other2Text = ""
'''''''''    frmEventMsgs.Other3Text = ""
'''''''''    frmEventMsgs.Other4Text = ""
'''''''''    frmEventMsgs.Show 1
'''''''''    txtCheck.Text = ""
'''''''''    txtPatient.SetFocus
'''''''''    KeyAscii = 0
'''''''''    Exit Sub
'''''''''End If
'''''''''If (KeyAscii = 13) Or (KeyAscii = 10) Then
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''    If (ApplTemp.ApplIsPatientCheckOpen(PatientId, Trim(txtCheck.Text), Amt, AmtLeft, CheckId, CheckDate)) Then
'''''''''        If (Val(Trim(AmtLeft)) = 0) Or (Val(Trim(Amt)) < 0) Then
'''''''''            frmEventMsgs.Header = "Check Already In Closed"
'''''''''            frmEventMsgs.AcceptText = "Print"
'''''''''            frmEventMsgs.RejectText = "View"
'''''''''            frmEventMsgs.CancelText = "Cancel"
'''''''''            frmEventMsgs.Other0Text = ""
'''''''''            frmEventMsgs.Other1Text = ""
'''''''''            frmEventMsgs.Other2Text = ""
'''''''''            frmEventMsgs.Other3Text = ""
'''''''''            frmEventMsgs.Other4Text = ""
'''''''''            frmEventMsgs.Show 1
'''''''''            If (frmEventMsgs.Result = 1) Then
'''''''''                Call cmdPrint_Click
'''''''''                txtCheck.Text = ""
'''''''''                txtCheck.SetFocus
'''''''''            ElseIf (frmEventMsgs.Result = 2) Then
'''''''''                Call cmdView_Click
'''''''''                txtCheck.Text = ""
'''''''''                txtCheck.SetFocus
'''''''''            Else
'''''''''                txtCheck.Text = ""
'''''''''                txtCheck.SetFocus
'''''''''            End If
'''''''''        Else
'''''''''            RunningCheckBalance = Trim(AmtLeft)
'''''''''            Call DisplayDollarAmount(Trim(Str(Amt)), Temp)
'''''''''            txtAmount.Text = Trim(Temp)
'''''''''            Call DisplayDollarAmount(Trim(Str(AmtLeft)), Temp)
'''''''''            lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(Temp)
'''''''''            lblAmountLeft.Tag = Trim(Str(AmtLeft))
'''''''''            txtDate.Text = Mid(CheckDate, 5, 2) + "/" + Mid(CheckDate, 7, 2) + "/" + Left(CheckDate, 4)
'''''''''            txtAmount.SetFocus
'''''''''        End If
'''''''''    Else
'''''''''        txtAmount.SetFocus
'''''''''    End If
'''''''''    Set ApplTemp = Nothing
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtPatient_Click()
'''''''''Dim i As Integer
'''''''''Dim PatId As Long
'''''''''Dim VerifyOn As Boolean
'''''''''Dim r1 As String, r2 As String
'''''''''Dim Temp As String, AName As String
'''''''''Dim b1 As Boolean, b2 As Boolean
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''Dim TriggerDisburse As Boolean
'''''''''VerifyOn = False
'''''''''TriggerDisburse = False
'''''''''If (PatientId > 0) Then
'''''''''    VerifyOn = False
'''''''''    For i = 1 To MSFlexGrid1.Rows - 1
'''''''''        If (Val(Trim(MSFlexGrid1.TextMatrix(i, 6))) <> 0) Then
'''''''''            If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''''                VerifyOn = True
'''''''''                Exit For
'''''''''            End If
'''''''''        End If
'''''''''        If (Val(Trim(MSFlexGrid1.TextMatrix(i, 7))) <> 0) Then
'''''''''            If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''''                VerifyOn = True
'''''''''                Exit For
'''''''''            End If
'''''''''        End If
'''''''''    Next i
'''''''''    If (VerifyOn) Then
'''''''''        Call CompleteCurrentPatient(PatientId)
'''''''''    End If
'''''''''    TriggerDisburse = MSFlexGrid1.Visible
'''''''''    MSFlexGrid1.Visible = False
'''''''''    lblInst.Visible = False
'''''''''    cmdDisburse.Text = "Disburse"
'''''''''    cmdAll.Visible = False
'''''''''    cmdChkReset.Visible = False
'''''''''    cmdComment.Visible = False
'''''''''    lblTPay.Visible = False
'''''''''    lblTPaid.Visible = False
'''''''''    lblTAdj.Visible = False
'''''''''    lblTAdjs.Visible = False
'''''''''    txtPayAmt.Text = ""
'''''''''    txtAdjAmt.Text = ""
'''''''''    lblAdj.Visible = False
'''''''''    txtAdj.Text = ""
'''''''''    txtAdj.Visible = False
'''''''''    chkInclude.Value = 0
'''''''''    chkInclude.Visible = False
'''''''''    txtPatient.Text = ""
'''''''''End If
'''''''''frmPatientSchedulerSearch.AddOn = False
'''''''''frmPatientSchedulerSearch.Show 1
'''''''''If (frmPatientSchedulerSearch.PatientId > 0) Then
'''''''''    PatId = frmPatientSchedulerSearch.PatientId
'''''''''    Unload frmPatientSchedulerSearch
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''    Call ApplTemp.ApplGetPatientName(PatId, AName, PatPol1, r1, b1, PatPol2, r2, b2, False)
'''''''''    PatientId = PatId
'''''''''    txtPatient.Text = AName
'''''''''    If (Trim(txtCheck.Text) <> "") Then
'''''''''        txtAmount.SetFocus
'''''''''    Else
'''''''''        txtCheck.SetFocus
'''''''''    End If
'''''''''    If (Trim(txtAmount.Text) <> "") And (Trim(txtDate.Text) <> "") Then
'''''''''        cmdDisburse.Visible = True
'''''''''        cmdDisburse.SetFocus
'''''''''    End If
'''''''''    Set ApplTemp = Nothing
'''''''''    If (TriggerDisburse) And (PatientId > 0) Then
'''''''''        Call cmdDisburse_Click
'''''''''        TriggerDisburse = False
'''''''''    End If
'''''''''Else
'''''''''    Unload frmPatientSchedulerSearch
'''''''''    txtPatient.SetFocus
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtPatient_KeyPress(KeyAscii As Integer)
'''''''''Call txtPatient_Click
'''''''''KeyAscii = 0
'''''''''End Sub
'''''''''
'''''''''Private Sub txtAmount_KeyPress(KeyAscii As Integer)
'''''''''Dim Temp As String
'''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
'''''''''    If Not (IsCurrency(Chr(KeyAscii))) Then
'''''''''        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''    End If
'''''''''Else
'''''''''    Call DisplayDollarAmount(txtAmount.Text, Temp)
'''''''''    txtAmount.Text = Trim(Temp)
'''''''''    If (Val(Trim(RunningCheckBalance)) <= 0) Then
'''''''''        RunningCheckBalance = Trim(txtAmount.Text)
'''''''''    End If
'''''''''    txtDate.SetFocus
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtAmount_Validate(Cancel As Boolean)
'''''''''Call txtAmount_KeyPress(13)
'''''''''End Sub
'''''''''
'''''''''Private Sub txtDate_KeyPress(KeyAscii As Integer)
'''''''''If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
'''''''''    If Not (OkDate(txtDate.Text)) Then
'''''''''        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''        txtDate.SetFocus
'''''''''        txtDate.Text = ""
'''''''''        SendKeys "{Home}"
'''''''''    Else
'''''''''        Call UpdateDisplay(txtDate, "D")
'''''''''        txtDate.SetFocus
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtDate_Validate(Cancel As Boolean)
'''''''''If (Trim(txtDate.Text) <> "") Then
'''''''''    Call txtDate_KeyPress(13)
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub MSFlexGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
'''''''''MSFlexGrid1.AllowBigSelection = False
'''''''''If (KeyCode = vbKeyRight) And (Shift = 0) Then
'''''''''    CurrentRow = MSFlexGrid1.RowSel
'''''''''    CurrentCol = MSFlexGrid1.ColSel
'''''''''ElseIf (KeyCode = vbKeyLeft) And (Shift = 0) Then
'''''''''    CurrentRow = MSFlexGrid1.RowSel
'''''''''    CurrentCol = MSFlexGrid1.ColSel
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub MSFlexGrid1_KeyPress(KeyAscii As Integer)
'''''''''If (CurrentRow <> MSFlexGrid1.RowSel) Then
'''''''''    CurrentRow = MSFlexGrid1.RowSel
'''''''''End If
'''''''''MSFlexGrid1.AllowBigSelection = True
'''''''''MSFlexGrid1.Highlight = flexHighlightAlways
'''''''''MSFlexGrid1.AllowBigSelection = False
'''''''''CurrentCol = MSFlexGrid1.ColSel
'''''''''If (MSFlexGrid1.TextMatrix(CurrentRow, 1) <> "") Then
'''''''''    If (KeyAscii = 13) Or (KeyAscii = 10) Then
'''''''''        If ((CurrentCol > 4) And (CurrentCol < 8)) Then
'''''''''            Call MsFlexGrid1_LeaveCell
'''''''''            Call MsFlexGrid1_EnterCell
'''''''''        Else
'''''''''            Call MSFlexGrid1_Click
'''''''''        End If
'''''''''    Else
'''''''''        If (CurrentCol > 4) And (CurrentCol < 8) Then
'''''''''            If (KeyAscii >= 32) And (KeyAscii < 125) And (KeyAscii <> 39) And (KeyAscii <> 37) Then
'''''''''                If (CurrentCol = 6) Then
'''''''''                    Call txtPayAmt_KeyPress(KeyAscii)
'''''''''                ElseIf (CurrentCol = 7) Then
'''''''''                    Call txtAdjAmt_KeyPress(KeyAscii)
'''''''''                End If
'''''''''                If (KeyAscii <> 0) Then
'''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) + Chr(KeyAscii)
'''''''''                End If
'''''''''            ElseIf (KeyAscii = 8) Or (KeyAscii = 126) Then
'''''''''                If (Len(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) > 1) Then
'''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = Left(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol), Len(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol)) - 1)
'''''''''                Else
'''''''''                    MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
'''''''''                End If
'''''''''            Else
'''''''''                Exit Sub
'''''''''            End If
'''''''''            If (CurrentCol = 6) Then
'''''''''                txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
'''''''''            ElseIf (CurrentCol = 7) Then
'''''''''                txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
'''''''''            End If
'''''''''        End If
'''''''''    End If
'''''''''Else
'''''''''    KeyAscii = 0
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub MSFlexGrid1_Click()
'''''''''Dim i As Integer
'''''''''Dim Amt As Single, Chg As Single, ATotal As Single
'''''''''Dim Paid As Single, Adjs As Single
'''''''''Dim Temp As String
'''''''''Dim ProcessBalance As Boolean
'''''''''ProcessBalance = False
'''''''''lblAdj.Visible = False
'''''''''txtAdj.Visible = False
'''''''''chkInclude.Visible = False
'''''''''MSFlexGrid1.AllowBigSelection = False
'''''''''If (MSFlexGrid1.ColSel <> 13) And (MSFlexGrid1.ColSel <> 0) Then
'''''''''    Call MsFlexGrid1_EnterCell
'''''''''    Exit Sub
'''''''''End If
'''''''''If (MSFlexGrid1.Row > 0) And (Trim(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, 1)) <> "") Then
'''''''''    If (CurrentRow > 0) Then
'''''''''        If (Trim(txtPayAmt.Text) <> "") Then
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
'''''''''            ProcessBalance = True
'''''''''            FirstTimeSet = True
'''''''''        End If
'''''''''        If (Trim(txtAdjAmt.Text) <> "") Then
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 7) = Trim(txtAdjAmt.Text)
'''''''''            ProcessBalance = True
'''''''''            FirstTimeSet = True
'''''''''        End If
'''''''''        If (Trim(txtAdj.Text) <> "") Then
'''''''''            MSFlexGrid2.TextMatrix(CurrentRow, 1) = Trim(txtAdj.Text)
'''''''''            MSFlexGrid2.TextMatrix(CurrentRow, 2) = "F"
'''''''''            If (chkInclude.Value = 1) Then
'''''''''                MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T"
'''''''''            End If
'''''''''        End If
'''''''''        If (ProcessBalance) Then
'''''''''            Chg = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 3)))
'''''''''            If (Chg <> Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 4)))) Then
'''''''''                Chg = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 4)))
'''''''''            End If
'''''''''            Paid = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)))
'''''''''            Adjs = Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)))
'''''''''            ATotal = Chg - (Paid + Adjs)
'''''''''            Call DisplayDollarAmount(Trim(Str(ATotal)), Temp)
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 8) = Trim(Temp)
'''''''''        End If
'''''''''    End If
'''''''''    If (Val(Trim(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, 4))) > 0) Then
'''''''''        CurrentRow = MSFlexGrid1.Row
'''''''''        txtAdj.Text = ""
'''''''''        chkInclude.Value = 0
'''''''''        txtPayAmt.Text = ""
'''''''''        txtAdjAmt.Text = ""
'''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6)) <> "") Then
'''''''''            txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6))
'''''''''        End If
'''''''''        If (Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7)) <> "") Then
'''''''''            txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))
'''''''''        End If
'''''''''' Display Comment
'''''''''        lblAdj.Visible = True
'''''''''        txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
'''''''''        txtAdj.Visible = True
'''''''''        chkInclude.Visible = True
'''''''''        If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
'''''''''            chkInclude.Value = 1
'''''''''        Else
'''''''''            chkInclude.Value = 0
'''''''''        End If
'''''''''    Else
'''''''''        frmEventMsgs.Header = "No Open Balance"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "OK"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''    End If
'''''''''Else
'''''''''    txtAdj.Text = ""
'''''''''    txtPayAmt.Text = ""
'''''''''    txtAdjAmt.Text = ""
'''''''''End If
'''''''''Call SetDisburseRemaining
'''''''''End Sub
'''''''''
'''''''''Private Sub MsFlexGrid1_EnterCell()
'''''''''If (MSFlexGrid1.TextMatrix(CurrentRow, 1) = "") Then
'''''''''    Exit Sub
'''''''''End If
'''''''''MSFlexGrid1.AllowBigSelection = False
'''''''''CurrentCol = MSFlexGrid1.ColSel
'''''''''CurrentRow = MSFlexGrid1.RowSel
'''''''''' Display Comment
'''''''''lblAdj.Visible = True
'''''''''txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
'''''''''txtAdj.Visible = True
'''''''''chkInclude.Visible = True
'''''''''If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
'''''''''    chkInclude.Value = 1
'''''''''Else
'''''''''    chkInclude.Value = 0
'''''''''End If
'''''''''If (CurrentCol = 6) Then
'''''''''    FirstTimeSet = True
'''''''''    txtPayAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
'''''''''ElseIf (CurrentCol = 7) Then
'''''''''    FirstTimeSet = True
'''''''''    txtAdjAmt.Text = Trim(MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol))
'''''''''ElseIf (CurrentCol = 0) Then
'''''''''    MSFlexGrid1_Click
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub MsFlexGrid1_LeaveCell()
'''''''''If (MSFlexGrid1.TextMatrix(CurrentRow, 1) = "") Then
'''''''''    Exit Sub
'''''''''End If
'''''''''MSFlexGrid1.AllowBigSelection = True
'''''''''CurrentCol = MSFlexGrid1.ColSel
'''''''''CurrentRow = MSFlexGrid1.RowSel
'''''''''' Display Comment
'''''''''lblAdj.Visible = True
'''''''''txtAdj.Text = Trim(MSFlexGrid2.TextMatrix(CurrentRow, 1))
'''''''''txtAdj.Visible = True
'''''''''chkInclude.Visible = True
'''''''''If (MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T") Then
'''''''''    chkInclude.Value = 1
'''''''''Else
'''''''''    chkInclude.Value = 0
'''''''''End If
'''''''''If (CurrentCol = 6) Then
'''''''''    Call txtPayAmt_KeyPress(13)
'''''''''    If (Trim(txtPayAmt.Text) = "") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
'''''''''    End If
'''''''''ElseIf (CurrentCol = 7) Then
'''''''''    Call txtAdjAmt_KeyPress(13)
'''''''''    If (Trim(txtAdjAmt.Text) = "") Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, CurrentCol) = ""
'''''''''    End If
'''''''''End If
'''''''''MSFlexGrid1.Highlight = flexHighlightAlways
'''''''''MSFlexGrid1.AllowBigSelection = False
'''''''''End Sub
'''''''''
'''''''''Private Sub txtPayAmt_Validate(Cancel As Boolean)
'''''''''If (Trim(txtPayAmt.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 6))) Then
'''''''''    Call txtPayAmt_KeyPress(13)
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtPayAmt_KeyPress(KeyAscii As Integer)
'''''''''Dim Temp As String
'''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
'''''''''    If Not (IsCurrency(Chr(KeyAscii))) Then
'''''''''        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''    Else
'''''''''        If (FirstTimeSet) Then
'''''''''            txtPayAmt.Text = ""
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 6) = ""
'''''''''            FirstTimeSet = False
'''''''''        End If
'''''''''    End If
'''''''''Else
'''''''''    Call DisplayDollarAmount(txtPayAmt.Text, Temp)
'''''''''    txtPayAmt.Text = Trim(Temp)
'''''''''    If (MoneyLeft(CurrentRow) < Val(Trim(txtPayAmt.Text))) Then
'''''''''        frmEventMsgs.Header = "Insufficient funds to apply"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        txtPayAmt.Text = ""
'''''''''        If (txtPayAmt.Visible) Then
'''''''''            txtPayAmt.SetFocus
'''''''''        End If
'''''''''    End If
'''''''''    If Not (VerifyAgainstOpenBalance(CurrentRow, Val(Trim(txtPayAmt.Text)), Val(Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))))) And (Val(Trim(txtPayAmt.Text)) <> 0) Then
'''''''''        frmEventMsgs.Header = "Payment exceeds Open Balance"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Apply"
'''''''''        frmEventMsgs.CancelText = "Cancel"
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        If (frmEventMsgs.Result = 4) Then
'''''''''            txtPayAmt.Text = ""
'''''''''            If (txtPayAmt.Visible) Then
'''''''''                txtPayAmt.SetFocus
'''''''''            End If
'''''''''        Else
'''''''''            If (CurrentRow > 0) Then
'''''''''                MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
'''''''''                Call CalculateBalance(CurrentRow)
'''''''''            End If
'''''''''            If (txtAdjAmt.Visible) Then
'''''''''                txtAdjAmt.SetFocus
'''''''''            End If
'''''''''        End If
'''''''''    Else
'''''''''        If (CurrentRow > 0) Then
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 6) = Trim(txtPayAmt.Text)
'''''''''            Call CalculateBalance(CurrentRow)
'''''''''        End If
'''''''''        If (txtAdjAmt.Visible) Then
'''''''''            txtAdjAmt.SetFocus
'''''''''        End If
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtAdjAmt_Validate(Cancel As Boolean)
'''''''''If (Trim(txtAdjAmt.Text) <> Trim(MSFlexGrid1.TextMatrix(CurrentRow, 7))) Then
'''''''''    Call txtAdjAmt_KeyPress(13)
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtAdjAmt_KeyPress(KeyAscii As Integer)
'''''''''Dim Temp As String
'''''''''If (KeyAscii <> 13) And (KeyAscii <> 10) Then
'''''''''    If Not (IsCurrency(Chr(KeyAscii))) Then
'''''''''        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
'''''''''        frmEventMsgs.AcceptText = ""
'''''''''        frmEventMsgs.RejectText = "Ok"
'''''''''        frmEventMsgs.CancelText = ""
'''''''''        frmEventMsgs.Other0Text = ""
'''''''''        frmEventMsgs.Other1Text = ""
'''''''''        frmEventMsgs.Other2Text = ""
'''''''''        frmEventMsgs.Other3Text = ""
'''''''''        frmEventMsgs.Other4Text = ""
'''''''''        frmEventMsgs.Show 1
'''''''''        KeyAscii = 0
'''''''''    Else
'''''''''        If (FirstTimeSet) Then
'''''''''            txtAdjAmt.Text = ""
'''''''''            MSFlexGrid1.TextMatrix(CurrentRow, 7) = ""
'''''''''            FirstTimeSet = False
'''''''''        End If
'''''''''    End If
'''''''''Else
'''''''''    Call DisplayDollarAmount(txtAdjAmt.Text, Temp)
'''''''''    txtAdjAmt.Text = Trim(Temp)
'''''''''    If (CurrentRow > 0) Then
'''''''''        MSFlexGrid1.TextMatrix(CurrentRow, 7) = Trim(txtAdjAmt.Text)
'''''''''        Call CalculateBalance(CurrentRow)
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub txtAdj_Validate(Cancel As Boolean)
'''''''''Call txtAdj_KeyPress(13)
'''''''''End Sub
'''''''''
'''''''''Private Sub txtAdj_KeyPress(KeyAscii As Integer)
'''''''''If (KeyAscii = 13) Or (KeyAscii = 10) Then
'''''''''    If (CurrentRow > 0) Then
'''''''''        MSFlexGrid2.TextMatrix(CurrentRow, 1) = Trim(txtAdj.Text)
'''''''''    End If
'''''''''    KeyAscii = 0
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Sub chkInclude_Click()
'''''''''If (CurrentRow > 0) Then
'''''''''    MSFlexGrid2.TextMatrix(CurrentRow, 2) = "F"
'''''''''    If (chkInclude.Value = 1) Then
'''''''''        MSFlexGrid2.TextMatrix(CurrentRow, 2) = "T"
'''''''''    End If
'''''''''End If
'''''''''End Sub
'''''''''
'''''''''Private Function LoadPayments(PatId As Long, AllOn As Boolean, Reload As Boolean) As Boolean
'''''''''Dim Bs As Boolean
'''''''''Dim CurRow As Integer
'''''''''Dim ApplList As ApplicationAIList
'''''''''LoadPayments = False
'''''''''FirstTimeSet = False
'''''''''GetCheckOn = False
'''''''''If (PatId > 0) Then
'''''''''    CurRow = CurrentRow
'''''''''    lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(RunningCheckBalance)
'''''''''    lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''''    lblAmountLeft.Visible = True
'''''''''    lblAdj.Visible = False
'''''''''    txtAdj.Visible = False
'''''''''    chkInclude.Value = 0
'''''''''    chkInclude.Visible = False
'''''''''    Set ApplList = New ApplicationAIList
'''''''''    Set ApplList.ApplGrid = MSFlexGrid1
'''''''''    Set ApplList.ApplGrid1 = MSFlexGrid2
'''''''''    If (Reload) Then
'''''''''        lblPrint.Caption = "Loading Grid Entries"
'''''''''        lblPrint.Visible = True
'''''''''        Bs = MSFlexGrid1.Visible
'''''''''        MSFlexGrid1.Visible = False
'''''''''        DoEvents
'''''''''        LoadPayments = ApplList.ApplLoadPaymentsbyServicesForPatients(PatientId, PatPol1, AllOn)
'''''''''        lblPrint.Visible = False
'''''''''        MSFlexGrid1.Visible = Bs
'''''''''    End If
''''''''''    AdjustmentCode = "O"
'''''''''    AdjustmentCode = "X"
'''''''''    Set ApplList = Nothing
'''''''''    Call SetGridTotals
'''''''''    cmdAll.Visible = LoadPayments
'''''''''    cmdChkReset.Visible = cmdAll.Visible
'''''''''    CurrentRow = CurRow
'''''''''    If (CurrentRow < 0) Or (CurrentRow > 12) Or (CurrentRow > MSFlexGrid1.Row) Then
'''''''''        CurrentRow = 1
'''''''''    End If
'''''''''    MSFlexGrid1.RowSel = CurrentRow
'''''''''End If
'''''''''End Function
'''''''''
'''''''''Private Function VerifyAgainstOpenBalance(ARow As Integer, Amt1 As Single, Amt2 As Single) As Boolean
'''''''''VerifyAgainstOpenBalance = False
'''''''''If (Val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))) >= (Amt1 + Amt2)) Then
'''''''''    VerifyAgainstOpenBalance = True
'''''''''End If
'''''''''End Function
'''''''''
'''''''''Private Function CompleteCurrentPatient(PatId As Long) As Boolean
'''''''''Dim ComOn As Boolean
'''''''''Dim z As Integer
'''''''''Dim i As Integer, j As Integer
'''''''''Dim p1 As Long, ItemId As Long
'''''''''Dim RcvId As Long, InsrId As Long, AInsId As Long
'''''''''Dim k As Single, PAmt As Single
'''''''''Dim InvDt As String
'''''''''Dim Srv As String, Amt As String, Cmt As String
'''''''''Dim InvId As String, CurInvId As String
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''Dim ApplList As ApplicationAIList
'''''''''CompleteCurrentPatient = False
'''''''''If (PatId > 0) Then
'''''''''    If (Trim(PaymentMethod) = "") Then
'''''''''        PaymentMethod = "K"
'''''''''    End If
'''''''''    CurInvId = ""
'''''''''    k = MoneyLeft(0)
'''''''''    If (MSFlexGrid1.Visible) Then
'''''''''' Payments
'''''''''        ComOn = False
'''''''''        lblPrint.Caption = "Posting ..."
'''''''''        lblPrint.Visible = True
'''''''''        DoEvents
'''''''''        Call DisplayDollarAmount(Trim(Str(k)), RunningCheckBalance)
'''''''''        lblAmountLeft.Tag = Trim(RunningCheckBalance)
'''''''''        lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(lblAmountLeft.Tag)
'''''''''        Set ApplTemp = New ApplicationTemplates
'''''''''        Call ApplTemp.ApplPostCheckRecord(CheckId, PatientId, "P", txtAmount.Text, RunningCheckBalance, txtCheck.Text, txtDate.Text, txtDate.Text)
'''''''''        For i = 1 To MSFlexGrid1.Rows - 1
'''''''''            If (Val(Trim(MSFlexGrid1.TextMatrix(i, 6))) <> 0) Then
'''''''''                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''''                    AInsId = 0
'''''''''                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''''                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), AInsId, InvDt)
'''''''''                    If (RcvId > 0) Then
'''''''''                        ItemId = Val(Trim(MSFlexGrid2.TextMatrix(i, 3)))
'''''''''                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
'''''''''                        j = InStrPS(Srv, " ")
'''''''''                        If (j > 0) Then
'''''''''                            Srv = Left(Srv, j - 1)
'''''''''                        End If
'''''''''                        Amt = Trim(MSFlexGrid1.TextMatrix(i, 6))
'''''''''                        Cmt = Trim(MSFlexGrid2.TextMatrix(i, 1))
'''''''''                        If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
'''''''''                            ComOn = True
'''''''''                        End If
'''''''''                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "P", AInsId, Amt, txtDate.Text, "P", PaymentMethod, "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, "", CheckId, "", "", 0)
'''''''''                    End If
'''''''''                End If
'''''''''            End If
'''''''''' Adjustments
'''''''''            If (Val(Trim(MSFlexGrid1.TextMatrix(i, 7))) <> 0) Then
'''''''''                If (Trim(MSFlexGrid1.TextMatrix(i, 1)) <> "") Then
'''''''''                    AInsId = 0
'''''''''                    InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''''                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(Trim(MSFlexGrid1.TextMatrix(i, 1)), AInsId, InvDt)
'''''''''                    If (RcvId > 0) Then
'''''''''                        ItemId = Val(Trim(MSFlexGrid2.TextMatrix(i, 3)))
'''''''''                        Srv = Trim(MSFlexGrid1.TextMatrix(i, 2))
'''''''''                        j = InStrPS(Srv, " ")
'''''''''                        If (j > 0) Then
'''''''''                            Srv = Left(Srv, j - 1)
'''''''''                        End If
'''''''''                        Amt = MSFlexGrid1.TextMatrix(i, 7)
'''''''''                        Cmt = MSFlexGrid2.TextMatrix(i, 1)
'''''''''                        If (Trim(MSFlexGrid2.TextMatrix(i, 2)) = "T") Then
'''''''''                            ComOn = True
'''''''''                        End If
'''''''''                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, "P", AInsId, Amt, txtDate.Text, AdjustmentCode, PaymentMethod, "", txtCheck.Text, Cmt, Srv, ItemId, ComOn, UserId, False, "", CheckId, "", "", 0)
'''''''''                    End If
'''''''''                End If
'''''''''            End If
'''''''''        Next i
'''''''''' Queue to Send if Okay to do this
'''''''''        lblPrint.Caption = "Batching ..."
'''''''''        lblPrint.Visible = True
'''''''''        PAmt = 0
'''''''''        InsrId = 0
'''''''''        CurInvId = ""
'''''''''        Set ApplList = New ApplicationAIList
'''''''''        For i = 1 To MSFlexGrid1.Rows - 1
'''''''''            InvId = Trim(MSFlexGrid1.TextMatrix(i, 1))
'''''''''            If (CurInvId = "") Then
'''''''''                CurInvId = InvId
'''''''''            ElseIf (InvId <> CurInvId) Then
'''''''''                If (PAmt > 0) Then
'''''''''                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(CurInvId, AInsId, InvDt)
'''''''''                    Call ApplTemp.BatchTransaction(RcvId, "P", Trim(Str(PAmt)), "P", False, 0, False, False, False)
'''''''''                    Call ApplList.ApplSetReceivableBalance(CurInvId, "", True)
'''''''''                End If
'''''''''                PAmt = 0
'''''''''                InsrId = 0
'''''''''                CurInvId = InvId
'''''''''            End If
'''''''''            If (Val(Trim(MSFlexGrid1.TextMatrix(i, 8))) > 0) And ((Val(Trim(MSFlexGrid1.TextMatrix(i, 6))) > 0) Or (Val(Trim(MSFlexGrid1.TextMatrix(i, 7))) > 0)) Then
'''''''''                PAmt = PAmt + Val(Trim(MSFlexGrid1.TextMatrix(i, 8)))
''''''''''            Else
''''''''''                PAmt = PAmt + Val(Trim(MSFlexGrid1.TextMatrix(i, 4)))
'''''''''            End If
'''''''''        Next i
'''''''''        Set ApplList = Nothing
'''''''''    End If
'''''''''    Set ApplTemp = Nothing
'''''''''    lblPrint.Visible = False
'''''''''    CompleteCurrentPatient = True
'''''''''End If
'''''''''End Function
'''''''''
'''''''''Private Function CalculateFields(ARow As Integer) As Boolean
'''''''''Dim Bal As Single
'''''''''Dim Adj As Single
'''''''''Dim Allow As Single
'''''''''Dim Paid As Single
'''''''''Dim Cover As Single
'''''''''Dim Charge As Single
'''''''''Dim Temp1 As String
'''''''''Dim Temp2 As String
'''''''''CalculateFields = True
'''''''''If ((Trim(MSFlexGrid1.TextMatrix(ARow, 6)) = "") And (Trim(MSFlexGrid1.TextMatrix(ARow, 7)) = "") Or _
'''''''''   (Val(Trim(MSFlexGrid1.TextMatrix(ARow, 6))) = 0) And (Val(Trim(MSFlexGrid1.TextMatrix(ARow, 7))) = 0)) Then
'''''''''    If (ARow > 0) Then
'''''''''        Cover = Val(Trim(MSFlexGrid2.TextMatrix(ARow, 0)))
'''''''''        If (Cover <= 0) Then
'''''''''            Cover = 1
'''''''''        Else
'''''''''            Cover = Cover / 100
'''''''''        End If
'''''''''        Allow = 0
'''''''''        Charge = Val(Trim(MSFlexGrid1.TextMatrix(ARow, 3)))
'''''''''        If (Allow > 0) And (Charge >= Val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))) Then
'''''''''            If (Charge >= Allow) Then
'''''''''                Paid = (Int(((Allow * Cover) + 0.005) * 100) / 100) - (Charge - Val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))))
'''''''''                Adj = (Val(Trim(MSFlexGrid1.TextMatrix(ARow, 4))) - Paid) - (Int(((Allow * (1 - Cover)) + 0.005) * 100) / 100)
'''''''''            Else
'''''''''                Paid = Int(((Charge * Cover) + 0.004) * 100) / 100
'''''''''                Adj = 0
'''''''''            End If
'''''''''        Else
'''''''''            Paid = 0
'''''''''            Adj = 0
'''''''''        End If
'''''''''        Call DisplayDollarAmount(Trim(Str(Paid)), Temp1)
'''''''''        Call DisplayDollarAmount(Trim(Str(Adj)), Temp2)
'''''''''        Bal = MoneyLeft(ARow)
'''''''''' removed to allow calculator function on cell
'''''''''' is covered display of negative balances in check amount
'''''''''' and if movement out of field occurs.
'''''''''        txtPayAmt.Text = Trim(Temp1)
'''''''''        MSFlexGrid1.TextMatrix(ARow, 6) = Trim(txtPayAmt.Text)
'''''''''        txtAdjAmt.Text = Trim(Temp2)
'''''''''        MSFlexGrid1.TextMatrix(ARow, 7) = Trim(txtAdjAmt.Text)
'''''''''        Call CalculateBalance(ARow)
'''''''''    End If
'''''''''End If
'''''''''End Function
'''''''''
'''''''''Private Function CalculateBalance(ARow As Integer) As Boolean
'''''''''Dim Bal As Single
'''''''''Dim Adj As Single
'''''''''Dim Paid As Single
'''''''''Dim PrevBal As Single
'''''''''Dim Temp1 As String
'''''''''CalculateBalance = False
'''''''''PrevBal = Val(Trim(MSFlexGrid1.TextMatrix(ARow, 4)))
'''''''''Paid = Val(Trim(MSFlexGrid1.TextMatrix(ARow, 6)))
'''''''''Adj = Val(Trim(MSFlexGrid1.TextMatrix(ARow, 7)))
'''''''''Bal = ((PrevBal - (Paid + Adj)) * 100) / 100
'''''''''Call DisplayDollarAmount(Trim(Str(Bal)), Temp1)
'''''''''MSFlexGrid1.TextMatrix(ARow, 8) = Trim(Temp1)
'''''''''Call SetDisburseRemaining
'''''''''CalculateBalance = True
'''''''''End Function
'''''''''
'''''''''Private Function SetGridTotals() As Boolean
'''''''''Dim i As Integer
'''''''''Dim p As Single, a As Single
'''''''''Dim Temp1 As String, Temp2 As String
'''''''''p = 0
'''''''''a = 0
'''''''''For i = 1 To MSFlexGrid1.Rows - 1
'''''''''    p = p + Val(Trim(MSFlexGrid1.TextMatrix(i, 6)))
'''''''''    a = a + Val(Trim(MSFlexGrid1.TextMatrix(i, 7)))
'''''''''Next i
'''''''''Call DisplayDollarAmount(Trim(Str(p)), Temp1)
'''''''''Call DisplayDollarAmount(Trim(Str(a)), Temp2)
'''''''''lblTPay.Visible = True
'''''''''lblTPaid.Caption = Trim(Temp1)
'''''''''lblTAdj.Visible = True
'''''''''lblTAdjs.Caption = Trim(Temp2)
'''''''''End Function
'''''''''
'''''''''Private Function SetDisburseRemaining() As Boolean
'''''''''Dim Amt As Single
'''''''''Dim Temp As String
'''''''''SetDisburseRemaining = True
'''''''''Amt = MoneyLeft(0)
'''''''''lblAmountLeft.Tag = Trim(Str(Amt))
'''''''''Call DisplayDollarAmount(lblAmountLeft.Tag, Temp)
'''''''''lblAmountLeft.Tag = Trim(Temp)
'''''''''lblAmountLeft.Caption = "Amount remaining to disburse $ " + Trim(lblAmountLeft.Tag)
'''''''''Call SetGridTotals
'''''''''End Function
'''''''''
'''''''''Private Function GetCorrectCheck(TheCheck As String, CheckId As Long) As Boolean
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''GetCheckOn = False
'''''''''GetCorrectCheck = False
'''''''''If (Trim(TheCheck) <> "") Then
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''    Set ApplTemp.lstBox = lstDisb
'''''''''    If (ApplTemp.ApplGetAllChecks(TheCheck)) Then
'''''''''        GetCorrectCheck = True
'''''''''        lstDisb.Visible = True
'''''''''        cmdApply.Enabled = False
'''''''''        cmdHome.Enabled = False
'''''''''        cmdDisburse.Enabled = False
'''''''''        cmdPrint.Enabled = False
'''''''''        cmdView.Enabled = False
'''''''''        txtAmount.Enabled = False
'''''''''        txtPatient.Enabled = False
'''''''''        txtCheck.Enabled = False
'''''''''        txtDate.Enabled = False
'''''''''        GetCheckOn = True
'''''''''    End If
'''''''''End If
'''''''''End Function
'''''''''
