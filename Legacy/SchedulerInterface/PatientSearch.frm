VERSION 5.00
Begin VB.Form frmPatientSchedulerSearch 
   BackColor       =   &H00FF0000&
   BorderStyle     =   0  'None
   Caption         =   "PatientSearch"
   ClientHeight    =   9000
   ClientLeft      =   120
   ClientTop       =   690
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ForeColor       =   &H00FF0000&
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox FirstName 
      BackColor       =   &H00FF8080&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4920
      MaxLength       =   16
      TabIndex        =   12
      Top             =   960
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.CommandButton cmdBack 
      BackColor       =   &H00FFFF00&
      Caption         =   "Back"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   7560
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2400
      UseMaskColor    =   -1  'True
      Width           =   1575
   End
   Begin VB.ComboBox PatientList 
      BackColor       =   &H00FF8080&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   360
      TabIndex        =   7
      Top             =   3360
      Visible         =   0   'False
      Width           =   8775
   End
   Begin VB.CommandButton cmdSearch 
      BackColor       =   &H00FFFF00&
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   7560
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5760
      UseMaskColor    =   -1  'True
      Width           =   1575
   End
   Begin VB.TextBox DateofBirth 
      BackColor       =   &H00FF8080&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      MaxLength       =   10
      TabIndex        =   3
      Top             =   2160
      Width           =   2655
   End
   Begin VB.TextBox SocialSecurity 
      BackColor       =   &H00FF8080&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      MaxLength       =   12
      TabIndex        =   4
      Top             =   2760
      Width           =   2655
   End
   Begin VB.TextBox HomePhone 
      BackColor       =   &H00FF8080&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      MaxLength       =   16
      TabIndex        =   2
      Top             =   1560
      Width           =   2655
   End
   Begin VB.TextBox LastName 
      BackColor       =   &H00FF8080&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      MaxLength       =   16
      TabIndex        =   1
      Top             =   960
      Width           =   2655
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H00FF8080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Search For Patient"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   345
      Left            =   600
      TabIndex        =   11
      Top             =   360
      Width           =   1920
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Home Phone"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   600
      TabIndex        =   10
      Top             =   1680
      Width           =   1275
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H80000010&
      BackStyle       =   0  'Transparent
      Caption         =   "SS #"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   600
      TabIndex        =   9
      Top             =   2880
      Width           =   450
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date of Birth"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   600
      TabIndex        =   8
      Top             =   2280
      Width           =   1320
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   600
      TabIndex        =   0
      Top             =   1080
      Width           =   1110
   End
End
Attribute VB_Name = "frmPatientSchedulerSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AllowAdd As Boolean
Public PatientId As Long
Dim CurrentItem As Long
Dim TotalMatches As Long
Dim Paging As Boolean
Dim PatientListActive As Boolean
Dim PatientSelectActive As Boolean
Dim BDate As ManagedDate
Dim PatientFind As PatientSearch

Private Sub cmdBack_Click()
On Error GoTo UIError_Label
If (PatientListActive) Then
    PatientListActive = False
    PatientSelectActive = False
    PatientList.Enabled = False
    PatientList.Visible = False
    cmdSearch.Visible = True
    cmdSearch.Enabled = True
    DateofBirth.Enabled = True
    DateofBirth.Text = ""
    HomePhone.Enabled = True
    HomePhone.Text = ""
    SocialSecurity.Enabled = True
    SocialSecurity.Text = ""
    LastName.Enabled = True
    LastName.Text = ""
    Set PatientFind = Nothing
Else
    If (PatientSelectActive) Then
        PatientListActive = False
        PatientSelectActive = False
        PatientList.Enabled = False
        PatientList.Visible = False
        FirstName.Enabled = True
        FirstName.Visible = True
        DateofBirth.Enabled = True
        HomePhone.Enabled = True
        SocialSecurity.Enabled = True
        LastName.Enabled = True
        cmdBack.Caption = "Done"
    Else
        cmdSearch.Visible = True
        cmdSearch.Enabled = True
        Set BDate = Nothing
        Set PatientFind = Nothing
        Unload frmPatientSearch
    End If
End If
Exit Sub
UIError_Label:
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub cmdSearch_Click()
On Error GoTo UIError_Label
Dim PatientLastName As String
Dim PatientPhone As String
Dim PatientSS As String
Dim Results As Long
TotalMatches = 0
PatientListActive = False
PatientSelectActive = False
PatientLastName = UCase(RTrim(LTrim(LastName.Text)))
PatientSS = UCase(RTrim(LTrim(SocialSecurity.Text)))
PatientPhone = UCase(RTrim(LTrim(HomePhone.Text)))
Set BDate = New ManagedDate
Set PatientFind = New PatientSearch
BDate.ExposedDate = UCase(RTrim(LTrim(DateofBirth.Text)))
If (PatientLastName <> "") Then
    PatientFind.LastName = PatientLastName
End If
If (PatientSS <> "") Then
    Call StripCharacters(PatientSS, "-")
    PatientFind.SocialSecurity = PatientSS
End If
If (BDate.ExposedDate <> "") And Not (BDate.ConvertDisplayDateToManagedDate) Then
    MsgBox "Dates are entered as MM/DD/YYYY"
    SocialSecurity.Text = ""
    DateofBirth.Text = ""
    HomePhone.Text = ""
    LastName.Text = ""
    Set PatientFind = Nothing
    Set BDate = Nothing
    Exit Sub
Else
    PatientFind.BirthDate = BDate.ExposedDate
End If
If (PatientPhone <> "") Then
    Call StripCharacters(PatientPhone, "-")
    Call StripCharacters(PatientPhone, "(")
    Call StripCharacters(PatientPhone, ")")
    PatientFind.Phone = PatientPhone
End If
If (PatientLastName = "") And (PatientSS = "") And (BDate.ExposedDate = "") And (PatientPhone = "") Then
    Results = -1
Else
    Results = PatientFind.FindPatient
    If (Results <> 0) Then
        TotalMatches = Results
    Else
        MsgBox "No Matches Found"
    End If
End If
If (Results = -1) Then
    If (AllowAdd) Then
        If (MsgBox("Create New Patient ?", vbExclamation + vbYesNo) = vbYes) Then
            PatientId = 0
        Else
            PatientId = -1
        End If
        PatientListActive = False
        PatientSelectActive = False
        Call cmdBack_Click
    Else
        MsgBox "No Match Found"
    End If
Else
    PatientListActive = True
    CurrentItem = 1
    If (PatientSelection(CurrentItem)) Then
        PatientList.Enabled = True
        PatientList.Visible = True
        cmdSearch.Visible = False
        LastName.Enabled = False
        DateofBirth.Enabled = False
        HomePhone.Enabled = False
        SocialSecurity.Enabled = False
    Else
        MsgBox "No Items Loaded"
    End If
End If
Exit Sub
UIError_Label:
    Resume LeaveFast
LeaveFast:
    PatientList.Visible = False
    PatientList.Enabled = False
    SocialSecurity.Text = ""
    DateofBirth.Text = ""
    HomePhone.Text = ""
    LastName.Text = ""
    cmdSearch.Visible = True
    Set BDate = Nothing
    Set PatientFind = Nothing
End Sub

Private Function PatientSelection(Item As Long) As Boolean
On Error GoTo UIError_Label
Dim DisplayItem As String
Dim i As Integer
PatientSelection = False
If (Item < 1) Then
    Exit Function
End If
For i = Item To Item + 4
    If (PatientFind.SelectPatient(i)) Then
        DisplayItem = Space(67)
        Mid(DisplayItem, 1, Len(PatientFind.LastName)) = PatientFind.LastName
        If (Len(PatientFind.FirstName) <> 0) Then
            Mid(DisplayItem, 15, Len(PatientFind.FirstName)) = PatientFind.FirstName
        End If
        If (Len(PatientFind.MiddleInitial) <> 0) Then
            Mid(DisplayItem, 25, Len(PatientFind.MiddleInitial)) = PatientFind.MiddleInitial
        End If
        If (Len(PatientFind.NameRef) <> 0) Then
            Mid(DisplayItem, 26, Len(PatientFind.NameRef)) = PatientFind.NameRef
        End If
        If (Len(PatientFind.SocialSecurity) <> 0) Then
            Mid(DisplayItem, 28, Len(PatientFind.SocialSecurity)) = " " + PatientFind.SocialSecurity
        End If
        If (Len(PatientFind.Phone) <> 0) Then
            Mid(DisplayItem, 38, Len(PatientFind.Phone)) = PatientFind.Phone
        End If
        If (Len(PatientFind.BirthDate) <> 0) Then
            BDate.ExposedDate = PatientFind.BirthDate
            If (BDate.ConvertManagedDateToDisplayDate) Then
                Mid(DisplayItem, 49, Len(BDate.ExposedDate)) = BDate.ExposedDate
            End If
        End If
        PatientList.List(i - Item) = DisplayItem
        PatientSelection = True
    Else
        PatientList.List(i - Item) = ""
    End If
Next i
PatientList.Text = ""
Set BDate = Nothing
Exit Function
UIError_Label:
    Resume LeaveFast
LeaveFast:
    Set BDate = Nothing
End Function

Private Sub Form_Load()
PatientId = -1
End Sub

Private Sub PatientList_Click()
On Error GoTo UIError_Label
Dim Item As Integer
Dim BDate As ManagedDate
Set BDate = New ManagedDate
If (Paging) Then
    Paging = False
    Exit Sub
End If
Item = CurrentItem + PatientList.ListIndex
If (PatientFind.SelectPatient(Item)) Then
    PatientListActive = False
    PatientSelectActive = True
    PatientId = PatientFind.PatientId
    Call cmdBack_Click
    FirstName.Text = PatientFind.FirstName + " " + PatientFind.MiddleInitial + " " + PatientFind.NameRef
    LastName.Text = PatientFind.LastName
    SocialSecurity.Text = PatientFind.SocialSecurity
    HomePhone.Text = PatientFind.Phone
    BDate.ExposedDate = PatientFind.BirthDate
    If (BDate.ConvertManagedDateToDisplayDate) Then
        DateofBirth.Text = BDate.ExposedDate
    Else
        DateofBirth.Text = ""
    End If
End If
Exit Sub
UIError_Label:
    Resume LeaveFast
LeaveFast:
Set BDate = Nothing
End Sub

Private Sub PatientList_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo UIError_Label
Dim Check As Boolean
Paging = True
If (KeyCode = vbKeyPageDown) And (Shift = 0) Then
    If (CurrentItem + 5) <= TotalMatches Then
        CurrentItem = CurrentItem + 5
        Check = PatientSelection(CurrentItem)
    End If
ElseIf (KeyCode = vbKeyPageUp) And (Shift = 0) Then
    If (CurrentItem - 5) < 1 Then
        CurrentItem = 1
    Else
        CurrentItem = CurrentItem - 5
    End If
    Check = PatientSelection(CurrentItem)
Else
    Paging = False
End If
Exit Sub
UIError_Label:
    Resume LeaveFast
LeaveFast:
End Sub
