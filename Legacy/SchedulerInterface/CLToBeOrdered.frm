VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmCLToBeOrdered 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "PatientSearch"
   ClientHeight    =   9000
   ClientLeft      =   120
   ClientTop       =   690
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Moveable        =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox OrdersList 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6240
      ItemData        =   "CLToBeOrdered.frx":0000
      Left            =   360
      List            =   "CLToBeOrdered.frx":0002
      TabIndex        =   1
      Top             =   960
      Width           =   11415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   360
      TabIndex        =   2
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLToBeOrdered.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9840
      TabIndex        =   3
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLToBeOrdered.frx":01E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOrder 
      Height          =   990
      Left            =   7200
      TabIndex        =   4
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLToBeOrdered.frx":03C2
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "To Be Ordered"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   510
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   3015
   End
End
Attribute VB_Name = "frmCLToBeOrdered"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OrderType As String

Private Sub cmdDone_Click()
Unload frmCLToBeOrdered
End Sub

Private Sub cmdHome_Click()
Unload frmCLToBeOrdered
End Sub

Private Sub Form_Load()
Dim RetrieveCLInventory As CLInventory
Dim DisplayItem As String
Dim i As Integer
Dim RetrieveCLOrders As CLOrders
Set RetrieveCLOrders = New CLOrders
RetrieveCLOrders.Criteria = "Status = 3 "
RetrieveCLOrders.OrderBy = "OrderDate"
RetrieveCLOrders.FindCLOrders
OrdersList.Clear
i = 1
While (RetrieveCLOrders.SelectCLOrders(i))
    Set RetrieveCLInventory = New CLInventory
    RetrieveCLInventory.Criteria = "InventoryID = " + Trim(Str(RetrieveCLOrders.InventoryId))
    RetrieveCLInventory.OrderBy = "InventoryID"
    RetrieveCLInventory.FindCLInventory
    DisplayItem = Space(78)
    Mid(DisplayItem, 1, 20) = RetrieveCLInventory.Manufacturer
    Mid(DisplayItem, 25, 40) = RetrieveCLInventory.Series
    Mid(DisplayItem, 65, 10) = Trim(Str(RetrieveCLOrders.Qty))
    DisplayItem = DisplayItem + Trim(Str(RetrieveCLOrders.OrderId))
    OrdersList.AddItem DisplayItem
    DisplayItem = Space(78)
    Mid(DisplayItem, 1, 15) = RetrieveCLInventory.Type_
    Mid(DisplayItem, 17, 15) = RetrieveCLInventory.Material
    Mid(DisplayItem, 34, 15) = RetrieveCLInventory.Disposable
    Mid(DisplayItem, 50, 15) = RetrieveCLInventory.WearTime
    Mid(DisplayItem, 65, 13) = RetrieveCLInventory.Tint
    DisplayItem = DisplayItem + Trim(Str(RetrieveCLOrders.OrderId))
    OrdersList.AddItem DisplayItem
    DisplayItem = Space(78)
    OrdersList.AddItem DisplayItem
    OrdersList.AddItem ("")
    Set RetrieveCLInventory = Nothing
    i = i + 1
Wend
Set RetrieveCLOrders = Nothing
End Sub

Private Sub cmdOrder_Click()
Dim i As Integer
Dim OrderId As Long
Dim Temp As String
Dim RetrieveCLOrders As CLOrders
If (OrdersList.ListIndex >= 0) Then
    Exit Sub
    frmEventMsgs.Header = "Update Orders ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        For i = 0 To OrdersList.ListCount - 1 Step 4
            OrderId = Val(Mid(OrdersList.List(i), 79, Len(OrdersList.List(i)) - 78))
            If (OrderId > 0) Then
                Set RetrieveCLOrders = New CLOrders
                RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
                RetrieveCLOrders.OrderBy = "OrderID"
                RetrieveCLOrders.FindCLOrders
                RetrieveCLOrders.Status = 3
                Temp = ""
                Call FormatTodaysDate(Temp, False)
                Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                RetrieveCLOrders.OrderComplete = Temp
                RetrieveCLOrders.PutCLOrder
                Set RetrieveCLOrders = Nothing
            End If
        Next i
        Call Form_Load
    End If
End If
End Sub

