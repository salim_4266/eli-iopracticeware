VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmFeeSchedule 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtFFee 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   9840
      MaxLength       =   12
      TabIndex        =   7
      Top             =   5760
      Width           =   1695
   End
   Begin VB.TextBox txtLoc 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   6120
      TabIndex        =   6
      Top             =   6720
      Width           =   3735
   End
   Begin VB.TextBox txtDoctor 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   1320
      MaxLength       =   64
      TabIndex        =   17
      Top             =   480
      Width           =   4695
   End
   Begin VB.TextBox txtAllowed 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   6120
      MaxLength       =   12
      TabIndex        =   5
      Top             =   6240
      Width           =   2295
   End
   Begin VB.TextBox txtPerAllowed 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   6120
      MaxLength       =   3
      TabIndex        =   4
      Top             =   5760
      Width           =   975
   End
   Begin VB.TextBox Fee 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2280
      MaxLength       =   12
      TabIndex        =   1
      Top             =   5760
      Width           =   1695
   End
   Begin VB.TextBox txtEDate 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2280
      MaxLength       =   10
      TabIndex        =   3
      Top             =   6720
      Width           =   1695
   End
   Begin VB.TextBox txtSDate 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2280
      MaxLength       =   10
      TabIndex        =   2
      Top             =   6240
      Width           =   1695
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   240
      TabIndex        =   8
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FeeSchedule.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10080
      TabIndex        =   10
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FeeSchedule.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5520
      TabIndex        =   9
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FeeSchedule.frx":03BE
   End
   Begin VB.ListBox lstFeeSrv 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4620
      ItemData        =   "FeeSchedule.frx":059F
      Left            =   360
      List            =   "FeeSchedule.frx":05A1
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   960
      Width           =   11175
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Facility Fee"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8280
      TabIndex        =   20
      Top             =   5760
      Width           =   1410
   End
   Begin VB.Label lblLoc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      TabIndex        =   19
      Top             =   6720
      Width           =   1530
   End
   Begin VB.Label lblDoctor 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   360
      TabIndex        =   18
      Top             =   480
      Width           =   795
   End
   Begin VB.Label lblService 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Service"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   360
      TabIndex        =   16
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Allowed"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      TabIndex        =   15
      Top             =   6240
      Width           =   1530
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "% Paid"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      TabIndex        =   14
      Top             =   5760
      Width           =   1530
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Fee"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1440
      TabIndex        =   13
      Top             =   5760
      Width           =   690
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   840
      TabIndex        =   12
      Top             =   6240
      Width           =   1290
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   825
      TabIndex        =   11
      Top             =   6720
      Width           =   1290
   End
End
Attribute VB_Name = "frmFeeSchedule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Private TheService As String
Private TheDoctor As Long

Private Sub cmdApply_Click()
Dim LocalDate As ManagedDate
Dim RetFee As PracticeFeeSchedules
If (lstFeeSrv.ListIndex >= 0) Then
    Set LocalDate = New ManagedDate
    Set RetFee = New PracticeFeeSchedules
    RetFee.SchServiceId = val(Trim(Mid(lstFeeSrv.List(lstFeeSrv.ListIndex), 100, Len(lstFeeSrv.List(lstFeeSrv.ListIndex)) - 99)))
    If (RetFee.RetrieveService) Then
        RetFee.SchFee = Round(val(Trim(Fee.Text)), 2)
        RetFee.SchFacFee = Round(val(Trim(txtFFee.Text)), 2)
        RetFee.SchAdjustmentAllowed = Round(val(Trim(txtAllowed.Text)), 2)
        RetFee.SchAdjustmentRate = Round(val(Trim(txtPerAllowed.Text)), 2)
        RetFee.SchStartDate = ""
        LocalDate.ExposedDate = txtSDate.Text
        If (LocalDate.ConvertDisplayDateToManagedDate) Then
            RetFee.SchStartDate = LocalDate.ExposedDate
        End If
        RetFee.SchEndDate = ""
        LocalDate.ExposedDate = txtEDate.Text
        If (LocalDate.ConvertDisplayDateToManagedDate) Then
            RetFee.SchEndDate = LocalDate.ExposedDate
        End If
        RetFee.SchLocId = val(Trim(Mid(txtLoc.Text, 57, 5)))
        If (Left(txtLoc.Text, 7) = "OFFICE ") Then
            RetFee.SchLocId = 0
        End If
        Call RetFee.ApplyService
    End If
    Set LocalDate = Nothing
    Set RetFee = Nothing
    Call FeeLoadDisplay(TheService, TheDoctor)
End If
End Sub

Private Sub cmdBack_Click()
Unload frmFeeSchedule
End Sub

Private Sub cmdDelete_Click()
Dim RetFee As PracticeFeeSchedules
If (lstFeeSrv.ListIndex >= 0) Then
    Set RetFee = New PracticeFeeSchedules
    RetFee.SchServiceId = val(Trim(Mid(lstFeeSrv.List(lstFeeSrv.ListIndex), 100, Len(lstFeeSrv.List(lstFeeSrv.ListIndex)) - 99)))
    If (RetFee.RetrieveService) Then
        Call RetFee.DeleteService
    End If
    Set RetFee = Nothing
    Call FeeLoadDisplay(TheService, TheDoctor)
Else
    frmEventMsgs.Header = "Select a Fee "
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Public Function FeeLoadDisplay(Service As String, DrId As Long) As Boolean
Dim k As Integer
Dim LocName As String
Dim ResName As String
Dim ResCode As String
Dim DisplayText As String
Dim RetSrv As Service
Dim RetFee As PracticeFeeSchedules
FeeLoadDisplay = False
txtDoctor.Text = ""
Fee.Text = ""
txtFFee.Text = ""
txtSDate.Text = ""
txtEDate.Text = ""
txtAllowed.Text = ""
txtPerAllowed.Text = ""
txtLoc.Text = ""
cmdDelete.Visible = False
lstFeeSrv.Clear
Set RetSrv = New Service
RetSrv.Service = Trim(Service)
RetSrv.ServiceType = "P"
If (RetSrv.RetrieveService) Then
    lblService.Caption = "Service: " + Trim(Service) + " " + Trim(RetSrv.ServiceDescription)
Else
    Set RetSrv = Nothing
    Exit Function
End If
TheService = Service
TheDoctor = DrId
If (DrId > 0) Then
    Call GetDoctorName(DrId, ResName, ResCode)
    DisplayText = Space(56)
    Mid(DisplayText, 1, Len(ResName)) = ResName
    DisplayText = DisplayText + Trim(Str(DrId))
    txtDoctor.Text = DisplayText
End If
Set RetFee = New PracticeFeeSchedules
RetFee.SchService = Service
RetFee.SchDoctorId = DrId
RetFee.SchPlanId = 0
RetFee.SchLocId = 0
If (RetFee.FindServiceDirect > 0) Then
    k = 1
    While (RetFee.SelectService(k))
        DisplayText = Space(100)
        Call GetDoctorName(RetFee.SchDoctorId, ResName, ResCode)
        Mid(DisplayText, 1, Len(ResCode)) = ResCode
        Call GetPlanName(RetFee.SchPlanId, ResName)
        Mid(DisplayText, 11, Len(ResName)) = ResName
        Call GetLocName(RetFee.SchLocId, LocName)
        If (Len(LocName) > 10) Then
            LocName = Left(LocName, 10)
        End If
        Mid(DisplayText, 60, Len(LocName)) = LocName
        ResName = Trim(Str(RetFee.SchFee))
        Mid(DisplayText, 71, Len(ResName)) = ResName
        DisplayText = DisplayText + Trim(Str(RetFee.SchServiceId))
        lstFeeSrv.AddItem DisplayText
        k = k + 1
    Wend
End If
Set RetSrv = Nothing
Set RetFee = Nothing
If (lstFeeSrv.ListCount > 0) Then
    FeeLoadDisplay = True
End If
End Function

Private Sub Fee_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmFeeSchedule.BorderStyle = 1
    frmFeeSchedule.ClipControls = True
    frmFeeSchedule.Caption = Mid(frmFeeSchedule.Name, 4, Len(frmFeeSchedule.Name) - 3)
    frmFeeSchedule.AutoRedraw = True
    frmFeeSchedule.Refresh
End If
End Sub

Private Sub txtFFee_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtLoc_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtLoc.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub txtLoc_KeyPress(KeyAscii As Integer)
Call txtLoc_Click
KeyAscii = 0
End Sub

Private Sub txtPerAllowed_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtVRU_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtAllowed_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Characters [0123456789.-]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub lstFeeSrv_Click()
Dim LocName As String
Dim LocalDate As ManagedDate
Dim RetFee As PracticeFeeSchedules
If (lstFeeSrv.ListIndex >= 0) Then
    Set LocalDate = New ManagedDate
    Set RetFee = New PracticeFeeSchedules
    RetFee.SchServiceId = val(Trim(Mid(lstFeeSrv.List(lstFeeSrv.ListIndex), 100, Len(lstFeeSrv.List(lstFeeSrv.ListIndex)) - 99)))
    If (RetFee.RetrieveService) Then
        Fee.Text = Trim(Str(RetFee.SchFee))
        txtFFee.Text = Trim(Str(RetFee.SchFacFee))
        txtAllowed.Text = Trim(Str(RetFee.SchAdjustmentAllowed))
        txtPerAllowed.Text = Trim(Str(RetFee.SchAdjustmentRate))
        txtSDate.Text = ""
        LocalDate.ExposedDate = RetFee.SchStartDate
        If (LocalDate.ConvertManagedDateToDisplayDate) Then
            txtSDate.Text = LocalDate.ExposedDate
        End If
        txtEDate.Text = ""
        LocalDate.ExposedDate = RetFee.SchEndDate
        If (LocalDate.ConvertManagedDateToDisplayDate) Then
            txtEDate.Text = LocalDate.ExposedDate
        End If
        Call GetLocName(RetFee.SchLocId, LocName)
        If (Trim(LocName) <> "") Then
            txtLoc.Text = LocName + Space(56 - Len(LocName)) + Trim(Str(RetFee.SchLocId))
        End If
    End If
    Set LocalDate = Nothing
    Set RetFee = Nothing
End If
cmdDelete.Visible = DeleteOn
End Sub

Private Sub txtSDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtSDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSDate.SetFocus
        txtSDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSDate, "D")
    End If
End If
End Sub

Private Sub txtEDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtEDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtEDate.SetFocus
        txtEDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtEDate, "D")
    End If
End If
End Sub

Private Sub txtDoctor_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledDoctor")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtDoctor.Text = frmSelectDialogue.Selection
    TheDoctor = val(Trim(Mid(txtDoctor.Text, 56, 5)))
    Call FeeLoadDisplay(TheService, TheDoctor)
End If
End Sub

Private Sub txtDoctor_KeyPress(KeyAscii As Integer)
Call txtDoctor_Click
KeyAscii = 0
End Sub

Private Function GetDoctorName(ResId As Long, ResName As String, ResCode As String) As Boolean
Dim RetDr As SchedulerResource
GetDoctorName = False
Set RetDr = New SchedulerResource
RetDr.ResourceId = ResId
If (RetDr.RetrieveSchedulerResource) Then
    GetDoctorName = True
    ResName = Trim(RetDr.ResourceDescription)
    ResCode = Trim(RetDr.ResourceName)
End If
Set RetDr = Nothing
End Function

Private Function GetPlanName(PlnId As Long, PlnName As String) As Boolean
Dim RetPatientFinancialService As PatientFinancialService
Dim RSIns As Recordset
GetPlanName = False
If (PlnId > 0) Then
    Set RetPatientFinancialService = New PatientFinancialService
    RetPatientFinancialService.InsurerId = PlnId
    Set RSIns = RetPatientFinancialService.GetInsurerDetails
    If (Not RSIns Is Nothing And RSIns.RecordCount > 0) Then
        PlnName = Trim(RSIns("Name")) + " " _
                    + Trim(RSIns("InsurerAddress")) + " " _
                    + Trim(RSIns("InsurerCity"))
        PlnName = Trim(PlnName)
        GetPlanName = True
    End If
    Set RetPatientFinancialService = Nothing
ElseIf (PlnId = -1) Then
    GetPlanName = True
    PlnName = "DEFAULT"
End If
End Function

Private Function GetLocName(LocId As Long, LocName As String) As Boolean
Dim RetLoc As SchedulerResource
Dim RetPrc As PracticeName
GetLocName = False
If (LocId > 0) And (LocId < 1000) Then
    Set RetLoc = New SchedulerResource
    RetLoc.ResourceId = LocId
    If (RetLoc.RetrieveSchedulerResource) Then
        GetLocName = True
        LocName = Trim(RetLoc.ResourceDescription)
    End If
    Set RetLoc = Nothing
ElseIf (LocId > 1000) Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeId = LocId - 1000
    If (RetPrc.RetrievePracticeName) Then
        GetLocName = True
        LocName = "OFFICE-" + Trim(RetPrc.PracticeLocationReference)
    End If
    Set RetPrc = Nothing
Else
    GetLocName = True
    LocName = "OFFICE"
End If
End Function
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub

