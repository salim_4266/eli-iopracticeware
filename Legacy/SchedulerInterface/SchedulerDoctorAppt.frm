VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "SSCALB32.OCX"
Begin VB.Form frmSchedulerDoctorAppt 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstStartTime 
      Height          =   1815
      Left            =   4320
      MultiSelect     =   1  'Simple
      TabIndex        =   8
      Top             =   480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstStaff 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2055
      ItemData        =   "SchedulerDoctorAppt.frx":0000
      Left            =   120
      List            =   "SchedulerDoctorAppt.frx":0002
      TabIndex        =   3
      Top             =   600
      Width           =   2775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   870
      Left            =   10320
      TabIndex        =   0
      Top             =   8040
      Width           =   1500
      _Version        =   131072
      _ExtentX        =   2646
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerDoctorAppt.frx":0004
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2295
      Left            =   6120
      TabIndex        =   1
      Top             =   480
      Width           =   3255
      _Version        =   65537
      _ExtentX        =   5741
      _ExtentY        =   4048
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   14285823
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
   End
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   5145
      Left            =   120
      TabIndex        =   2
      Top             =   2880
      Width           =   5775
      _Version        =   65537
      _ExtentX        =   10186
      _ExtentY        =   6959
      _StockProps     =   79
      Caption         =   "First Selected Doctor"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   5.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
   End
   Begin SSCalendarWidgets_B.SSDay SSDay2 
      Height          =   5145
      Left            =   6120
      TabIndex        =   6
      Top             =   2880
      Width           =   5775
      _Version        =   65537
      _ExtentX        =   10186
      _ExtentY        =   6959
      _StockProps     =   79
      Caption         =   "Second Selected Doctor"
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   5.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   14285823
      BevelColorShadow=   8388608
      BevelColorHighlight=   0
      BevelColorFrame =   8421504
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      DurationFillColor=   16777152
      EditBackColor   =   16777215
      EditForeColor   =   8388608
      BevelType       =   0
      TimeInterval    =   0
      AllowEdit       =   0   'False
      AllowDelete     =   0   'False
   End
   Begin VB.Label lblHoliday2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Holiday"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   9600
      TabIndex        =   13
      Top             =   2160
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblHoliday1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Holiday"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3240
      TabIndex        =   12
      Top             =   2160
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblVacation2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Vacation Day"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   9600
      TabIndex        =   11
      Top             =   2520
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.Label lblVacation1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Vacation Day"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3240
      TabIndex        =   10
      Top             =   2520
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor Schedules"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   285
      Left            =   5010
      TabIndex        =   9
      Top             =   120
      Width           =   2085
   End
   Begin VB.Label lblDetails 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Appointment Details"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   8640
      Visible         =   0   'False
      Width           =   1965
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Person"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   810
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   2280
      TabIndex        =   4
      Top             =   240
      Width           =   645
   End
End
Attribute VB_Name = "frmSchedulerDoctorAppt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public InitialApptDate As String
Private ApptDate As String
Private CurrentListId As Integer

Private Sub cmdExit_Click()
ApptDate = ""
Unload frmSchedulerDoctorAppt
End Sub

Public Function LoadDoctorList() As Boolean
Dim ApplList As ApplicationAIList
LoadDoctorList = False
Set ApplList = New ApplicationAIList
Set ApplList.ApplList = lstStaff
ApplList.ApplDate = InitialApptDate
Call ApplList.ApplLoadStaff(True)
Set ApplList = Nothing
CurrentListId = 0
LoadDoctorList = True
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmSchedulerDoctorAppt.BorderStyle = 1
    frmSchedulerDoctorAppt.ClipControls = True
    frmSchedulerDoctorAppt.Caption = Mid(frmSchedulerDoctorAppt.Name, 4, Len(frmSchedulerDoctorAppt.Name) - 3)
    frmSchedulerDoctorAppt.AutoRedraw = True
    frmSchedulerDoctorAppt.Refresh
End If
End Sub

Private Sub lstStaff_Click()
Dim DocId As Long
Dim ApplList As ApplicationAIList
Dim ApplSch As ApplicationScheduler
If (lstStaff.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    DocId = ApplList.ApplGetListResourceId(lstStaff.List(lstStaff.ListIndex))
    Set ApplList = Nothing
    If (DocId > 0) Then
        If (val(Trim(SSDay1.Tag)) <> DocId) And _
           (val(Trim(SSDay2.Tag)) <> DocId) Then
            CurrentListId = CurrentListId + 1
            If (CurrentListId > 2) Then
                CurrentListId = 1
            End If
            If (CurrentListId = 1) Then
                Set ApplSch = New ApplicationScheduler
                Set ApplSch.lstStartTime = lstStartTime
                Call ApplSch.LoadDoctorSchedule(ApptDate, DocId, SSDay1, lblVacation1, lblHoliday1, SSMonth1)
                Set ApplSch = Nothing
            ElseIf (CurrentListId = 2) Then
                Set ApplSch = New ApplicationScheduler
                Set ApplSch.lstStartTime = lstStartTime
                Call ApplSch.LoadDoctorSchedule(ApptDate, DocId, SSDay2, lblVacation2, lblHoliday2, SSMonth1)
                Set ApplSch = Nothing
            End If
        End If
    End If
    lstStaff.ListIndex = -1
End If
End Sub

Private Sub SSDay1_DblClick()
If (SSDay1.TaskSelected >= 0) Then
    If Not (lblDetails.Visible) Then
        lblDetails.Caption = SSDay1.Tasks.Item(SSDay1.TaskSelected).Text
        lblDetails.Visible = True
    Else
        lblDetails.Visible = False
    End If
End If
End Sub

Private Sub SSDay2_DblClick()
If (SSDay2.TaskSelected >= 0) Then
    If Not (lblDetails.Visible) Then
        lblDetails.Caption = SSDay2.Tasks.Item(SSDay2.TaskSelected).Text
        lblDetails.Visible = True
    Else
        lblDetails.Visible = False
    End If
End If
End Sub

Private Sub SSMonth1_Click()
Dim DocId As Long
Dim SelDate As String
Dim ApplList As ApplicationAIList
Dim ApplSch As ApplicationScheduler
SelDate = SSMonth1.Date
Call FormatTodaysDate(SelDate, False)
If (Trim(SelDate) <> "") Or (ApptDate = "") Or (ApptDate <> SelDate) Then
    ApptDate = SelDate
    Call FormatTodaysDate(ApptDate, False)
    DocId = val(Trim(SSDay1.Tag))
    If (DocId > 0) Then
        Set ApplSch = New ApplicationScheduler
        Set ApplSch.lstStartTime = lstStartTime
        Call ApplSch.LoadDoctorSchedule(ApptDate, DocId, SSDay1, lblVacation1, lblHoliday1, SSMonth1)
        Set ApplSch = Nothing
    End If
    DocId = val(Trim(SSDay2.Tag))
    If (DocId > 0) Then
        Set ApplSch = New ApplicationScheduler
        Set ApplSch.lstStartTime = lstStartTime
        Call ApplSch.LoadDoctorSchedule(ApptDate, DocId, SSDay2, lblVacation2, lblHoliday2, SSMonth1)
        Set ApplSch = Nothing
    End If
    Set ApplList = New ApplicationAIList
    ApplList.ApplDate = ApptDate
    Set ApplList.ApplList = lstStaff
    Call ApplList.ApplLoadStaff(True)
    Set ApplList = Nothing
End If
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
