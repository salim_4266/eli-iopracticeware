VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmPatientChartReq 
   BackColor       =   &H0073702B&
   BorderStyle     =   0  'None
   Caption         =   "Patient Chart Request"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1095
      Left            =   5040
      TabIndex        =   16
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReq.frx":0000
   End
   Begin VB.ListBox lstTemp 
      Height          =   2790
      Left            =   960
      TabIndex        =   15
      Top             =   2880
      Visible         =   0   'False
      Width           =   7095
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   9960
      MultiSelect     =   1  'Simple
      TabIndex        =   13
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PatientChartReq.frx":01EA
      Left            =   8400
      List            =   "PatientChartReq.frx":01EC
      MultiSelect     =   1  'Simple
      TabIndex        =   12
      Top             =   360
      Width           =   1455
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PatientChartReq.frx":01EE
      Left            =   5760
      List            =   "PatientChartReq.frx":01F0
      MultiSelect     =   1  'Simple
      TabIndex        =   11
      Top             =   360
      Width           =   2535
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H006C6928&
      Height          =   4695
      Left            =   3480
      TabIndex        =   6
      Top             =   2040
      Visible         =   0   'False
      Width           =   4095
      Begin fpBtnAtlLibCtl.fpBtn cmdCancel 
         Height          =   615
         Left            =   480
         TabIndex        =   9
         Top             =   3840
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "PatientChartReq.frx":01F2
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdSubmit 
         Height          =   615
         Left            =   2280
         TabIndex        =   8
         Top             =   3840
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "PatientChartReq.frx":03D3
      End
      Begin SSCalendarWidgets_A.SSMonth SSMonth1 
         Height          =   3015
         Left            =   480
         TabIndex        =   7
         Top             =   720
         Width           =   3015
         _Version        =   65537
         _ExtentX        =   5318
         _ExtentY        =   5318
         _StockProps     =   76
      End
      Begin VB.Label Label1 
         BackColor       =   &H006C6928&
         Caption         =   "Select Date:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   480
         TabIndex        =   10
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.ListBox lstPtnChartReqList 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6270
      Left            =   240
      TabIndex        =   0
      Top             =   1320
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1110
      Left            =   240
      TabIndex        =   1
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReq.frx":05B4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   10200
      TabIndex        =   2
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReq.frx":0793
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   2760
      TabIndex        =   3
      Top             =   7680
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReq.frx":0972
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   1095
      Left            =   5040
      TabIndex        =   4
      Top             =   7680
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReq.frx":0B51
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNewReq 
      Height          =   1110
      Left            =   7680
      TabIndex        =   5
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChartReq.frx":0D30
   End
   Begin VB.Label Label2 
      BackColor       =   &H0077742D&
      Caption         =   "Patient Chart Requests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   14
      Top             =   240
      Width           =   2925
   End
End
Attribute VB_Name = "frmPatientChartReq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TransType As String
Public TransStatus As String
Private DateValue As String
Private Result As String
Private StartDate As String
Private EndDate As String
Private PendTrans As PendingTrans
Private Const recsPerPage As Integer = 25
Private AutoLoadOn As Boolean
Private TriggerOn As Boolean
Private ResourceId As Long
Private ResourceLocId As Long
Dim PatId As Long
Dim Status As String
Public FirstTime As Boolean
Dim DRFilter As String
Dim StatusFilter As String
Dim LocFilter As String
Private Sub cmdCancel_Click()
lstPtnChartReqList.Enabled = True
Frame1.Visible = False
Frame1.Enabled = False
DateValue = ""
End Sub

Private Sub cmdDisplay_Click()
Dim i As Integer
Dim StatusItems() As String
Dim DRItems() As String
Dim LocItems() As String
TriggerOn = False
lstPtnChartReqList.Clear
If Not StatusFilter = "" Then
    If Not Trim(StatusFilter) = "-1" Then
        StatusItems = Split(StatusFilter, ",")
        For i = 0 To UBound(StatusItems) - 1
            Status = StatusItems(i)
            LoadStatus (True)
        Next
    Else
        LoadAllFilters
    End If
End If
If Not Trim(DRFilter) = "-1" Then
        lstPtnChartReqList.Clear
        DRItems = Split(Trim(DRFilter), ",")
        For i = 0 To UBound(DRItems) - 1
            ResourceId = DRItems(i)
            LoadStaff (True)
        Next
    Else
        LoadAllFilters
End If
If Not Trim(LocFilter) = "-1" Then
    lstPtnChartReqList.Clear
    LocItems = Split(Trim(LocFilter), ",")
    For i = 0 To UBound(LocItems) - 1
            ResourceLocId = LocItems(i)
            LoadLocation
        Next
Else
    LoadAllFilters
End If
End Sub

Private Sub cmdDone_Click()
Unload frmPatientChartReq
End Sub

Private Sub cmdHome_Click()
Unload frmPatientChartReq
End Sub

Private Sub cmdNewReq_Click()
Dim PatId As Long
Dim DoAgain As Boolean
DoAgain = True
Dim ReturnArguments As Variant
While DoAgain
    PatId = 0
    Set ReturnArguments = DisplayPatientSearchScreen
    If Not (ReturnArguments Is Nothing) Then
        PatId = ReturnArguments.PatientId
    End If
If PatId > 0 Then
    If frmPatientChartReqAppts.LoadApptsList(PatId, 0, True) = True Then
        frmPatientChartReqAppts.Show 1
        DoAgain = frmPatientChartReqAppts.Formon
        'Unload frmPatientChartReqAppts
    Else
        frmEventMsgs.Header = "No Appointments"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoAgain = True
    End If
Else
    DoAgain = False
End If
Wend
Call Form_Load
'Call LoadAllFilters
End Sub

Private Sub cmdSubmit_Click()
lstPtnChartReqList.Enabled = True
Frame1.Visible = False
Frame1.Enabled = False
DateValue = SSMonth1.DateValue
Dim ADate As String
Dim TransId As String
Dim RetTrans As PracticeTransactionJournal
Call FormatTodaysDate(DateValue, False)
ADate = Mid(DateValue, 7, 4) + Mid(DateValue, 1, 2) + Mid(DateValue, 4, 2)
If Result = "1" Then
       If lstPtnChartReqList.ListIndex >= 0 Then
            TransId = val(Trim(Mid(lstPtnChartReqList.List(lstPtnChartReqList.ListIndex), 80, 20)))
            If TransId > 0 Then
                Set RetTrans = New PracticeTransactionJournal
                RetTrans.TransactionJournalId = TransId
                If (RetTrans.RetrieveTransactionJournal) Then
                    RetTrans.TransactionJournalDate = ADate
                    RetTrans.ApplyTransactionJournal
                End If
            End If
       End If
ElseIf Result = "5" Then
     If lstPtnChartReqList.ListIndex >= 0 Then
            TransId = val(Trim(Mid(lstPtnChartReqList.List(lstPtnChartReqList.ListIndex), 80, 20)))
            If TransId > 0 Then
                Set RetTrans = New PracticeTransactionJournal
                RetTrans.TransactionJournalId = TransId
                If (RetTrans.RetrieveTransactionJournal) Then
                    RetTrans.TransactionJournalRemark = ADate
                    RetTrans.ApplyTransactionJournal
                End If
            End If
       End If
End If
Call Form_Load
End Sub

Private Sub Form_Load()
Dim ADate As String
Frame1.Visible = False
Dim i As Integer
Dim AppId As Long
Dim DateNow As String
Dim DisplayItem As String
Set PendTrans = New PendingTrans
AutoLoadOn = False
AutoLoadOn = CheckConfigCollection("AUTOLOADDISPLAY") = "T"
TransType = "P"
If FirstTime Then
    Status = "P"
    ResourceId = -1
    ResourceLocId = -1
    DRFilter = "-1"
    LocFilter = "-1"
End If
TransType = Trim(TransType)
If (InStrPS("SF", TransType) = 0) Then
    'TransType = "S"
End If
lstPtnChartReqList.Clear
lstTemp.Clear
Dim RetTrn As PracticeTransactionJournal
Dim appointment As New SchedulerAppointment
Dim myPatient As New Patient
Set RetTrn = New PracticeTransactionJournal
RetTrn.TransactionJournalType = "P"
RetTrn.TransactionJournalTypeId = 0
RetTrn.TransactionJournalStatus = ""
RetTrn.TransactionJournalAction = ""
If (RetTrn.FindTransactionJournal > 0) Then
    i = 1
    While (RetTrn.SelectTransactionJournal(i))
        DisplayItem = Space(140)
        Mid(DisplayItem, 1, 1) = RetTrn.TransactionJournalStatus
        DateNow = Mid(RetTrn.TransactionJournalDate, 7, 4) + Mid(RetTrn.TransactionJournalDate, 1, 2) + Mid(RetTrn.TransactionJournalDate, 4, 2)
        Mid(DisplayItem, 5, Len(RetTrn.TransactionJournalDate)) = RetTrn.TransactionJournalDate
        If Not RetTrn.TransactionJournalRemark = "" Then
            Mid(DisplayItem, 15, Len(RetTrn.TransactionJournalRemark)) = RetTrn.TransactionJournalRemark
        End If
        AppId = RetTrn.TransactionJournalTypeId
        appointment.AppointmentId = AppId
        If appointment.RetrieveSchedulerAppointment = True Then
            myPatient.PatientId = appointment.AppointmentPatientId
            If myPatient.RetrievePatient = True Then
                Mid(DisplayItem, 25, Len(myPatient.LastName)) = Trim(myPatient.LastName)
                Mid(DisplayItem, 45, Len(myPatient.FirstName)) = Trim(myPatient.FirstName)
                Mid(DisplayItem, 100, 115) = myPatient.PatientId
            End If
            Mid(DisplayItem, 65, Len(appointment.AppointmentDate)) = appointment.AppointmentDate
            Mid(DisplayItem, 80, 20) = RetTrn.TransactionJournalId
            'old Mid(DisplayItem, 120, 10) = RetTrn.TransactionJournalRcvrId
            Mid(DisplayItem, 120, 10) = appointment.AppointmentResourceId1
            Mid(DisplayItem, 130, 10) = appointment.AppointmentResourceId2
        End If
        lstPtnChartReqList.AddItem Trim(DisplayItem)
        lstTemp.AddItem Trim(DisplayItem)
        i = i + 1
    Wend
End If
Set RetTrn = Nothing
TriggerOn = True
Call LoadStatusList
TriggerOn = True
Call LoadLocList
TriggerOn = True
Call LoadStaffList
PendTrans.CurrentPage = 1
ADate = ""
Call FormatTodaysDate(ADate, False)
If Not lstStatus.List(1) = "" Then
    lstStatus.Selected(1) = True
End If
Call cmdDisplay_Click
lstPtnChartReqList.Enabled = True
FirstTime = False
End Sub


Private Sub lstLoc_Click()
Dim i As Integer
If (lstLoc.ListIndex >= 0) Then
    If lstLoc.ListIndex = 0 Then
    If (lstLoc.Selected(0)) Then
            For i = 1 To lstLoc.ListCount - 1
                lstLoc.Selected(i) = False
            Next i
            LocFilter = -1
            TriggerOn = True
    End If
    Else
        If lstLoc.Selected(0) = True Then
            lstLoc.Selected(0) = False
            TriggerOn = True
        End If
        LocFilter = FilterLocs("")
    End If
End If
End Sub

Private Sub lstDr_Click()
Dim i As Integer
If lstDr.ListIndex >= 0 Then
    If (lstDr.ListIndex = 0) Then
            If (lstDr.Selected(0)) Then
                For i = 1 To lstDr.ListCount - 1
                    lstDr.Selected(i) = False
                Next i
                ResourceId = -1
                TriggerOn = True
                DRFilter = "-1"
            End If
    ElseIf (lstDr.ListIndex > 0) Then
            TriggerOn = False
            If lstDr.Selected(0) = True Then
                lstDr.Selected(0) = False
                TriggerOn = True
            End If
            DRFilter = FilterDocs("")
    End If
End If
End Sub
Private Sub lstStatus_Click()
Dim i As Integer
If lstStatus.ListIndex >= 0 Then
    If (lstStatus.ListIndex = 0) Then
        If (lstStatus.Selected(0)) Then
            For i = 1 To lstStatus.ListCount - 1
                lstStatus.Selected(i) = False
            Next i
            Status = -1
            StatusFilter = -1
            TriggerOn = True
        End If
    ElseIf (lstStatus.ListIndex > 0) Then
        lstStatus.Selected(0) = False
        StatusFilter = FilterStatus("")
    End If
End If
End Sub
Private Function FilterLocs(LocFilter) As String
Dim i As Integer
FilterLocs = ""
If (lstLoc.Selected(0) Or lstLoc.SelCount = 0) Then
    
Else
    For i = 1 To lstLoc.ListCount - 1
        If (lstLoc.Selected(i)) Then
            FilterLocs = FilterLocs + Trim(Mid(lstLoc.List(i), 56, Len(lstLoc.List(i)) - 55)) + ","
        End If
    Next i
End If
End Function

Private Function FilterDocs(DRFilter As String) As String
Dim i As Integer
FilterDocs = ""
If (lstDr.Selected(0) Or lstDr.SelCount = 0) Then
    
Else
    For i = 1 To lstDr.ListCount - 1
        If (lstDr.Selected(i)) Then
            FilterDocs = FilterDocs + Mid(lstDr.List(i), 21, Len(lstDr.List(i)) - 20) + ","
        End If
    Next i
End If
End Function
Private Function FilterStatus(StatusFilter) As String
Dim i As Integer
FilterStatus = ""
If (lstStatus.Selected(0) Or lstStatus.SelCount = 0) Then
    For i = 1 To lstStatus.ListCount - 1
        FilterStatus = FilterStatus + Mid(lstStatus.List(i), 1, 1) + ","
    Next i
Else
    For i = 1 To lstStatus.ListCount - 1
        If (lstStatus.Selected(i)) Then
            FilterStatus = FilterStatus + Mid(lstStatus.List(i), 1, 1) + ","
        End If
    Next i
End If
If (InStrPS(1, StatusFilter, "N") > 0) Then
    FilterStatus = "'" + FilterStatus + "'"
Else
End If
End Function
Public Sub LoadAllFilters()
Dim i As Integer
    If DRFilter = "-1" And LocFilter = "-1" And StatusFilter = "-1" Then
        lstPtnChartReqList.Clear
        For i = lstTemp.ListCount - 1 To 0 Step -1
            lstPtnChartReqList.AddItem (lstTemp.List(i))
        Next i
        Exit Sub
    End If
End Sub
Public Sub LoadStatus(ByVal Flag As Boolean)
If Flag Then
    If TriggerOn = True Or FirstTime = True Then
        lstPtnChartReqList.Clear
    End If
    Dim i As Integer
    For i = lstTemp.ListCount - 1 To 0 Step -1
       If Trim(Status) = Trim(Mid(lstTemp.List(i), 1, 2)) Then
            lstPtnChartReqList.AddItem (lstTemp.List(i))
       End If
    Next i
Else
    If TriggerOn Then
        lstPtnChartReqList.Clear
    End If
    For i = lstPtnChartReqList.ListCount - 1 To 0 Step -1
        If Status = Trim(Mid(lstPtnChartReqList.List(i), 1, 2)) Then
          lstPtnChartReqList.RemoveItem (i)
        End If
    Next i
    For i = 0 To lstStatus.ListCount - 1
        If lstStatus.Selected(i) = True Then
            Exit Sub
        End If
    Next i
    Status = "-1"
    TriggerOn = True
    LoadAllFilters
End If
End Sub
Public Sub LoadStaff(ByVal Flag As Boolean)
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim ItemFlag As Boolean
Dim StatusItems() As String
StatusItems = Split(StatusFilter, ",")
If Flag Then
    If TriggerOn = True Or FirstTime = True Then
        'lstPtnChartReqList.Clear
    End If
    For i = lstTemp.ListCount - 1 To 0 Step -1
        ItemFlag = False
       If ResourceId = val(Trim(Mid(lstTemp.List(i), 120, 10))) Then
            For k = 0 To UBound(StatusItems) - 1
                       Status = StatusItems(k)
                       If Trim(Status) = Trim(Mid(lstTemp.List(i), 1, 2)) Then
                          ItemFlag = True
                        End If
            Next
            If Status = "-1" Then
                ItemFlag = True
            End If
            If ItemFlag Then
                lstPtnChartReqList.AddItem (lstTemp.List(i))
            End If
        Else
            'lstPtnChartReqList.RemoveItem()
       End If
    Next i
Else
    If TriggerOn Then
        'lstPtnChartReqList.Clear
    End If
    
    For i = lstPtnChartReqList.ListCount - 1 To 0 Step -1
           If ResourceId = val(Trim(Mid(lstPtnChartReqList.List(i), 120, 10))) Then
                lstPtnChartReqList.RemoveItem (i)
           End If
    Next i
    For j = 0 To lstStatus.ListCount - 1
        If lstStatus.Selected(j) = True Then
            For i = 0 To lstTemp.ListCount - 1
                If Status = Trim(Mid(lstTemp.List(i), 1, 2)) Then
                  lstPtnChartReqList.AddItem (lstTemp.List(i))
                End If
            Next i
        End If
    Next j
End If
End Sub
Public Sub LoadLocation()
Dim i As Integer
Dim j As Integer
Dim ItemFlag As Boolean
Dim k As Integer
Dim StatusItems() As String
Dim DRItems() As String
StatusItems = Split(StatusFilter, ",")
DRItems = Split(Trim(DRFilter), ",")
        
For i = lstTemp.ListCount - 1 To 0 Step -1
        ItemFlag = False
       If ResourceLocId = val(Trim(Mid(lstTemp.List(i), 130, 10))) Then
            For j = 0 To UBound(DRItems) - 1
                        ResourceId = DRItems(j)
                        If Trim(ResourceId) = val(Trim(Mid(lstTemp.List(i), 120, 10))) Then
                            For k = 0 To UBound(StatusItems) - 1
                                Status = StatusItems(k)
                                If Trim(Status) = Trim(Mid(lstTemp.List(i), 1, 2)) Then
                                    ItemFlag = True
                                End If
                            Next
                            If Status = "-1" Then
                                ItemFlag = True
                            End If
                        End If
                    Next
                    If ResourceId = "-1" Then
                            For k = 0 To UBound(StatusItems) - 1
                                Status = StatusItems(k)
                                If Trim(Status) = Trim(Mid(lstTemp.List(i), 1, 2)) Then
                                    ItemFlag = True
                                End If
                            Next
                    End If
                    If Status = "-1" And ResourceId = "-1" Then
                        ItemFlag = True
                    End If
            'Next j
            If ItemFlag Then
                lstPtnChartReqList.AddItem (lstTemp.List(i))
            End If
        Else
            'lstPtnChartReqList.RemoveItem()
       End If
    Next i
End Sub
Private Sub lstPtnChartReqList_dblClick()
    Dim TransId As Long
    Dim DateNow As String
    DateNow = ""
    Call FormatTodaysDate(DateNow, False)
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Request Date"
    frmEventMsgs.RejectText = ""
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Status"
    frmEventMsgs.Other1Text = "Sent Date"
    frmEventMsgs.Other2Text = "Delete"
    frmEventMsgs.Other3Text = "Patient Info"
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        lstPtnChartReqList.Enabled = False
        Frame1.Visible = True
        Frame1.Enabled = True
        Result = 1
    ElseIf frmEventMsgs.Result = 6 Then
        TransId = val(Trim(Mid(lstPtnChartReqList.List(lstPtnChartReqList.ListIndex), 80, 20)))
        If TransId > 0 Then
            Dim ApplTemp As New ApplicationTemplates
            Call ApplTemp.ApplPurgeTransaction(TransId, True, False)
            lstTemp.RemoveItem (lstPtnChartReqList.ListIndex)
            lstPtnChartReqList.RemoveItem (lstPtnChartReqList.ListIndex)
        End If
    ElseIf frmEventMsgs.Result = 5 Then
        lstPtnChartReqList.Enabled = False
        Frame1.Visible = True
        Frame1.Enabled = True
        Result = 5
    ElseIf frmEventMsgs.Result = 3 Then
        TransId = val(Trim(Mid(lstPtnChartReqList.List(lstPtnChartReqList.ListIndex), 80, 20)))
        frmSelectDialogue.BuildSelectionDialogue ("PatientChartReqStatus")
        frmSelectDialogue.Show 1
        If frmSelectDialogue.SelectionResult Then
            Dim RetTrans As PracticeTransactionJournal
            Set RetTrans = New PracticeTransactionJournal
            RetTrans.TransactionJournalId = TransId
            If (RetTrans.RetrieveTransactionJournal) Then
                RetTrans.TransactionJournalStatus = Mid(frmSelectDialogue.Selection, 1, 1)
                If RetTrans.TransactionJournalStatus = "S" Then
                    If RetTrans.TransactionJournalRemark = "" Then
                        DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
                        RetTrans.TransactionJournalRemark = DateNow
                    End If
                End If
                RetTrans.ApplyTransactionJournal
                Call Form_Load
            End If
        End If
    ElseIf frmEventMsgs.Result = 7 Then
        PatId = val(Trim(Mid(lstPtnChartReqList.List(lstPtnChartReqList.ListIndex), 100, 15)))
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = PatId
        Call PatientDemographics.DisplayPatientInfoScreen
        Set PatientDemographics = Nothing
    End If
End Sub

Private Sub LoadStaffList()
Dim ApplList As ApplicationAIList
lstDr.Visible = True
lstDr.Clear
Set ApplList = New ApplicationAIList
Set ApplList.ApplList = lstDr
Call ApplList.ApplLoadStaff(False)
lstDr.List(0) = ("All Doctors")
End Sub

Private Sub LoadLocList()
Dim ApplList As ApplicationAIList
lstLoc.Visible = True
lstLoc.Clear
Set ApplList = New ApplicationAIList
Set ApplList.ApplList = lstLoc
Call ApplList.ApplLoadLocation(False, True)
End Sub

Private Sub LoadStatusList()
Dim ApplList As ApplicationAIList
lstStatus.Visible = True
lstStatus.Clear
Set ApplList = New ApplicationAIList
Call ApplList.ApplGetCodesbyList("PatientChartReqStatus", False, True, lstStatus)
lstStatus.List(0) = "All Status"
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
