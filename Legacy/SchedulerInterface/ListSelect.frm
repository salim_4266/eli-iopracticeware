VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form FListSelect 
   BackColor       =   &H00FCFCFC&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "ListSelect.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":305A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   1095
      Left            =   225
      TabIndex        =   1
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":323B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   1095
      Left            =   225
      TabIndex        =   3
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":341C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   1095
      Left            =   225
      TabIndex        =   4
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":35FD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   1095
      Left            =   225
      TabIndex        =   5
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":37DE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   1095
      Left            =   225
      TabIndex        =   6
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":39BF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   6600
      TabIndex        =   7
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":3BA0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   1095
      Left            =   4200
      TabIndex        =   8
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":3D7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   1095
      Left            =   4200
      TabIndex        =   9
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":3F60
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   1095
      Left            =   4200
      TabIndex        =   10
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":4141
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   1095
      Left            =   4200
      TabIndex        =   11
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":4322
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   1095
      Left            =   4200
      TabIndex        =   12
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":4503
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   1095
      Left            =   4200
      TabIndex        =   13
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":46E4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt13 
      Height          =   1095
      Left            =   8160
      TabIndex        =   14
      Top             =   480
      Width           =   3600
      _Version        =   131072
      _ExtentX        =   6350
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":48C5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt14 
      Height          =   1110
      Left            =   8160
      TabIndex        =   15
      Top             =   1680
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":4AA6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt15 
      Height          =   1110
      Left            =   8160
      TabIndex        =   16
      Top             =   2880
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":4C87
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt16 
      Height          =   1110
      Left            =   8160
      TabIndex        =   17
      Top             =   4080
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":4E68
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt17 
      Height          =   1095
      Left            =   8160
      TabIndex        =   18
      Top             =   5280
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":5049
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt18 
      Height          =   1110
      Left            =   8160
      TabIndex        =   19
      Top             =   6480
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":522A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   240
      TabIndex        =   20
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":540B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10200
      TabIndex        =   21
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":55EA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   4200
      TabIndex        =   22
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":57C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAnother 
      Height          =   990
      Left            =   2640
      TabIndex        =   23
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":59B6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurrRv 
      Height          =   990
      Left            =   8160
      TabIndex        =   25
      Top             =   7680
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":5B9C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   8160
      TabIndex        =   24
      Top             =   7680
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListSelect.frx":5D89
   End
   Begin VB.Label lblBirthdate 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Birthdate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   9720
      TabIndex        =   28
      Top             =   120
      Visible         =   0   'False
      Width           =   945
   End
   Begin VB.Label lblPatientName 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   5520
      TabIndex        =   27
      Top             =   120
      Visible         =   0   'False
      Width           =   630
   End
   Begin VB.Label lblPatientAge 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Age"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8760
      TabIndex        =   26
      Top             =   120
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select An Appointment Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   3210
   End
End
Attribute VB_Name = "FListSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public DeleteOn As Boolean
Public ApptTypeName As String
Public ApptTypeId As Long
Public ApptPeriod As String
Public lstBox As ListBox
Public NoteOn As Boolean

Private BaseBackColor As Long
Private BaseForeColor As Long
Private MaxItems As Integer
Private CurrentAdmin As Integer
Private AdminList(40) As String
Private DisplayType As String
Private CurrentIndex As Integer
Private TheComplaintCategory As String
Private TotalAppts As Long
Private fUseResourceName As Boolean

Private Const SetForeColor As Long = 0
Private Const SetBackColor As Long = 14745312

Private Sub cmdCurrRv_Click()
ApptTypeName = "CURRENTRV"
ApptTypeId = 0
ApptPeriod = ""
Unload FListSelect
End Sub

Private Sub cmdNotes_Click()
frmNotes.NoteId = 0
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.SystemReference = "8"
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    NoteOn = frmNotes.NoteOn
    Call cmdDone_Click
End If
End Sub

Private Sub cmdAnother_Click()
PatientId = 0
AppointmentId = 0
ApptTypeId = -3
ApptTypeName = ""
Unload FListSelect
End Sub

Private Sub cmdDone_Click()
Dim i As Integer
If Not (cmdQuit.Visible) Then
    ApptTypeId = -1
    ApptTypeName = ""
End If
If (CurrentAdmin > 0) Then
    For i = 1 To CurrentAdmin
        If (Trim(AdminList(i)) <> "") Then
            ApptTypeName = ApptTypeName + Trim(AdminList(i)) + "-f/"
        End If
    Next i
End If
PatientId = 0
AppointmentId = 0
Unload FListSelect
End Sub

Private Sub cmdQuit_Click()
PatientId = 0
AppointmentId = 0
ApptTypeId = -1
ApptTypeName = ""
Unload FListSelect
End Sub

Private Sub cmdDelete_Click()
frmEventMsgs.Header = "Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Delete"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    DeleteOn = True
    ApptTypeId = -1
    ApptTypeName = ""
    If (UCase(cmdDelete.Text) = UCase("De-Select ALL")) Then
        ApptTypeId = -2
    End If
    PatientId = 0
    AppointmentId = 0
    Unload FListSelect
End If
End Sub

Private Sub cmdMore_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > TotalAppts) Then
    CurrentIndex = 1
End If
Call LoadAppt(DisplayType, False)
End Sub

Private Sub cmdAppt1_Click()
Call SetItem(cmdAppt1)
End Sub

Private Sub cmdAppt2_Click()
Call SetItem(cmdAppt2)
End Sub

Private Sub cmdAppt3_Click()
Call SetItem(cmdAppt3)
End Sub

Private Sub cmdAppt4_Click()
Call SetItem(cmdAppt4)
End Sub

Private Sub cmdAppt5_Click()
Call SetItem(cmdAppt5)
End Sub

Private Sub cmdAppt6_Click()
Call SetItem(cmdAppt6)
End Sub

Private Sub cmdAppt7_Click()
Call SetItem(cmdAppt7)
End Sub

Private Sub cmdAppt8_Click()
Call SetItem(cmdAppt8)
End Sub

Private Sub cmdAppt9_Click()
Call SetItem(cmdAppt9)
End Sub

Private Sub cmdAppt10_Click()
Call SetItem(cmdAppt10)
End Sub

Private Sub cmdAppt11_Click()
Call SetItem(cmdAppt11)
End Sub

Private Sub cmdAppt12_Click()
Call SetItem(cmdAppt12)
End Sub

Private Sub cmdAppt13_Click()
Call SetItem(cmdAppt13)
End Sub

Private Sub cmdAppt14_Click()
Call SetItem(cmdAppt14)
End Sub

Private Sub cmdAppt15_Click()
Call SetItem(cmdAppt15)
End Sub

Private Sub cmdAppt16_Click()
Call SetItem(cmdAppt16)
End Sub

Private Sub cmdAppt17_Click()
Call SetItem(cmdAppt17)
End Sub

Private Sub cmdAppt18_Click()
Call SetItem(cmdAppt18)
End Sub

Private Sub ClearAppt()
cmdAppt1.Text = ""
cmdAppt1.BackColor = BaseBackColor
cmdAppt1.ForeColor = BaseForeColor
cmdAppt1.AlignTextH = fpAlignTextHCenter
cmdAppt1.AlignTextV = fpAlignTextVCenter
cmdAppt1.Visible = False
cmdAppt2.Text = ""
cmdAppt2.BackColor = BaseBackColor
cmdAppt2.ForeColor = BaseForeColor
cmdAppt2.AlignTextH = fpAlignTextHCenter
cmdAppt2.AlignTextV = fpAlignTextVCenter
cmdAppt2.Visible = False
cmdAppt3.Text = ""
cmdAppt3.BackColor = BaseBackColor
cmdAppt3.ForeColor = BaseForeColor
cmdAppt3.AlignTextH = fpAlignTextHCenter
cmdAppt3.AlignTextV = fpAlignTextVCenter
cmdAppt3.Visible = False
cmdAppt4.Text = ""
cmdAppt4.BackColor = BaseBackColor
cmdAppt4.ForeColor = BaseForeColor
cmdAppt4.AlignTextH = fpAlignTextHCenter
cmdAppt4.AlignTextV = fpAlignTextVCenter
cmdAppt4.Visible = False
cmdAppt5.Text = ""
cmdAppt5.BackColor = BaseBackColor
cmdAppt5.ForeColor = BaseForeColor
cmdAppt5.AlignTextH = fpAlignTextHCenter
cmdAppt5.AlignTextV = fpAlignTextVCenter
cmdAppt5.Visible = False
cmdAppt6.Text = ""
cmdAppt6.BackColor = BaseBackColor
cmdAppt6.ForeColor = BaseForeColor
cmdAppt6.AlignTextH = fpAlignTextHCenter
cmdAppt6.AlignTextV = fpAlignTextVCenter
cmdAppt6.Visible = False
cmdAppt7.Text = ""
cmdAppt7.BackColor = BaseBackColor
cmdAppt7.ForeColor = BaseForeColor
cmdAppt7.AlignTextH = fpAlignTextHCenter
cmdAppt7.AlignTextV = fpAlignTextVCenter
cmdAppt7.Visible = False
cmdAppt8.Text = ""
cmdAppt8.BackColor = BaseBackColor
cmdAppt8.ForeColor = BaseForeColor
cmdAppt8.AlignTextH = fpAlignTextHCenter
cmdAppt8.AlignTextV = fpAlignTextVCenter
cmdAppt8.Visible = False
cmdAppt9.Text = ""
cmdAppt9.BackColor = BaseBackColor
cmdAppt9.ForeColor = BaseForeColor
cmdAppt9.AlignTextH = fpAlignTextHCenter
cmdAppt9.AlignTextV = fpAlignTextVCenter
cmdAppt9.Visible = False
cmdAppt10.Text = ""
cmdAppt10.BackColor = BaseBackColor
cmdAppt10.ForeColor = BaseForeColor
cmdAppt10.AlignTextH = fpAlignTextHCenter
cmdAppt10.AlignTextV = fpAlignTextVCenter
cmdAppt10.Visible = False
cmdAppt11.Text = ""
cmdAppt11.BackColor = BaseBackColor
cmdAppt11.ForeColor = BaseForeColor
cmdAppt11.AlignTextH = fpAlignTextHCenter
cmdAppt11.AlignTextV = fpAlignTextVCenter
cmdAppt11.Visible = False
cmdAppt12.Text = ""
cmdAppt12.BackColor = BaseBackColor
cmdAppt12.ForeColor = BaseForeColor
cmdAppt12.AlignTextH = fpAlignTextHCenter
cmdAppt12.AlignTextV = fpAlignTextVCenter
cmdAppt12.Visible = False
cmdAppt13.Text = ""
cmdAppt13.BackColor = BaseBackColor
cmdAppt13.ForeColor = BaseForeColor
cmdAppt13.AlignTextH = fpAlignTextHCenter
cmdAppt13.AlignTextV = fpAlignTextVCenter
cmdAppt13.Visible = False
cmdAppt14.Text = ""
cmdAppt14.BackColor = BaseBackColor
cmdAppt14.ForeColor = BaseForeColor
cmdAppt14.AlignTextH = fpAlignTextHCenter
cmdAppt14.AlignTextV = fpAlignTextVCenter
cmdAppt14.Visible = False
cmdAppt15.Text = ""
cmdAppt15.BackColor = BaseBackColor
cmdAppt15.ForeColor = BaseForeColor
cmdAppt15.AlignTextH = fpAlignTextHCenter
cmdAppt15.AlignTextV = fpAlignTextVCenter
cmdAppt15.Visible = False
cmdAppt16.Text = ""
cmdAppt16.BackColor = BaseBackColor
cmdAppt16.ForeColor = BaseForeColor
cmdAppt16.AlignTextH = fpAlignTextHCenter
cmdAppt16.AlignTextV = fpAlignTextVCenter
cmdAppt16.Visible = False
cmdAppt17.Text = ""
cmdAppt17.BackColor = BaseBackColor
cmdAppt17.ForeColor = BaseForeColor
cmdAppt17.AlignTextH = fpAlignTextHCenter
cmdAppt17.AlignTextV = fpAlignTextVCenter
cmdAppt17.Visible = False
cmdAppt18.Text = ""
cmdAppt18.BackColor = BaseBackColor
cmdAppt18.ForeColor = BaseForeColor
cmdAppt18.AlignTextH = fpAlignTextHCenter
cmdAppt18.AlignTextV = fpAlignTextVCenter
cmdAppt18.Visible = False
cmdMore.Text = "More"
cmdMore.Visible = False
End Sub

Private Sub SetItem(AButton As fpBtn)
Dim i As Integer
Dim j As Integer
Dim p As Integer
Dim UTemp1 As Long
Dim UTemp2 As String
Dim UTemp3 As String
Dim ITemp As String
Dim TheEyes As String
If (DisplayType = "v") Then
    TheComplaintCategory = Trim(AButton.Text)
    UTemp1 = Val(str(AButton.Tag))
    UTemp2 = AButton.Text + TheEyes
    UTemp3 = AButton.ToolTipText
    CurrentIndex = 1
    If Not (LoadAppt("V", True)) Then
        ApptTypeId = UTemp1
        ApptTypeName = UTemp2
        ApptPeriod = UTemp3
        Unload FListSelect
    End If
ElseIf (DisplayType <> "Z") And (DisplayType <> "&") And (DisplayType <> "C") And (DisplayType <> "O") And (DisplayType <> "B") And (DisplayType <> "W") Then
    If (DisplayType = "V") Or (DisplayType = "J") Then
        TheEyes = ""
        Call GetEyes(TheEyes)
    End If
    If (DisplayType = "J") Then
        ApptTypeId = 0
        If (TheEyes = " (OD)") Then
            ApptTypeId = 1
        ElseIf (TheEyes = " (OS)") Then
            ApptTypeId = 2
        End If
        ApptTypeName = AButton.Tag
        ApptPeriod = AButton.ToolTipText
    ElseIf (DisplayType = "{") Then
        ApptTypeId = Val(str(AButton.Tag))
'        ApptTypeName = AButton.Text
        ApptPeriod = AButton.Text
    Else
        ApptTypeId = Val(str(AButton.Tag))
        If fUseResourceName Then
            ApptTypeName = AButton.ToolTipText & TheEyes
            ApptPeriod = AButton.Text
        Else
            ApptTypeName = AButton.Text + TheEyes
            ApptPeriod = AButton.ToolTipText
        End If
    End If
    Unload FListSelect
Else
    If (AButton.BackColor = BaseBackColor) Then
        If (MaxItems > 0) Then
            If (CurrentAdmin + 1 > MaxItems) Then
                frmEventMsgs.Header = "Too Many Selections"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Exit Sub
            End If
        End If
        AButton.BackColor = SetBackColor
        AButton.ForeColor = SetForeColor
        CurrentAdmin = CurrentAdmin + 1
        TheEyes = ""
        If (DisplayType = "C") Then
            Call GetEyes(TheEyes)
        End If
        AdminList(CurrentAdmin) = AButton.Text + TheEyes
    Else
        AButton.BackColor = BaseBackColor
        AButton.ForeColor = BaseForeColor
        For i = 1 To CurrentAdmin
            j = InStrPS(AdminList(i), "(")
            If (j > 0) Then
                ITemp = Left(AdminList(i), j - 1)
            Else
                ITemp = AdminList(i)
            End If
            If (UCase(Trim(AButton.Text)) = UCase(Trim(ITemp))) Then
                For p = i To CurrentAdmin - 1
                    AdminList(p) = AdminList(p + 1)
                Next p
                AdminList(CurrentAdmin) = ""
            End If
        Next i
        CurrentAdmin = CurrentAdmin - 1
        If (CurrentAdmin < 1) Then
            CurrentAdmin = 1
        End If
    End If
End If
End Sub

Public Function LoadAppt(TheType As String, Init As Boolean, Optional fUseNames As Boolean = False) As Boolean
Dim i As Integer
Dim BreakPoint As Integer, ButtonCnt As Integer
Dim DisplayText As String
Dim RetrieveReferral As PracticeVendors
LoadAppt = False
MaxItems = 0
BreakPoint = 19
ApptPeriod = ""
DisplayType = TheType
Call ClearAppt
DeleteOn = False
NoteOn = False
cmdQuit.Visible = True
cmdMore.Visible = False
cmdNotes.Visible = False
cmdCurrRv.Visible = False
cmdDelete.Visible = False
cmdDone.Visible = False
cmdAnother.Visible = False
fUseResourceName = fUseNames

If (TheType = "D") Or (TheType = "H") Or (TheType = "R") Or (TheType = "Q") Then
    lblField.Caption = "Select a Doctor"
    If (TheType = "H") Then
        lblField.Caption = "Select a Hospital"
    ElseIf (TheType = "R") Then
        lblField.Caption = "Select a Referring Doctor"
    End If
    Set RetrieveReferral = New PracticeVendors
    RetrieveReferral.VendorName = ""
    If (Trim(ApptTypeName) <> "") Then
        RetrieveReferral.VendorLastName = Trim(ApptTypeName)
    Else
        RetrieveReferral.VendorLastName = Chr(1)
    End If
    RetrieveReferral.VendorType = TheType
    If (TheType = "Q") Then
        RetrieveReferral.VendorType = "D"
    End If
    If (TheType <> "H") Then
        TotalAppts = RetrieveReferral.FindVendorbyLastNameT
    Else
        TotalAppts = RetrieveReferral.FindVendorT
    End If
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveReferral.SelectVendor(i)) And (ButtonCnt < 19) And (i <= TotalAppts)
            DisplayText = Trim(RetrieveReferral.VendorLastName) + " " + Trim(RetrieveReferral.VendorTitle) + " " + Trim(RetrieveReferral.VendorFirstName)
            If (TheType = "H") Or (TheType = "Q") Then
                DisplayText = Trim(RetrieveReferral.VendorName) + vbCrLf + Trim(RetrieveReferral.VendorAddress) + vbCrLf + Trim(RetrieveReferral.VendorCity)
            End If
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = Trim(str(RetrieveReferral.VendorId))
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetrieveReferral = Nothing
    End If
End If

If (CurrentIndex <= TotalAppts) And (TotalAppts > 18) Then
    cmdMore.Text = "More"
    cmdMore.Visible = True
Else
    cmdMore.Text = "Beginning of List"
    cmdMore.Visible = True
End If
If (TotalAppts < BreakPoint - 1) Then
    cmdMore.Visible = False
End If
End Function

Private Sub Form_Load()
CurrentIndex = 1
BaseBackColor = cmdAppt1.BackColor
BaseForeColor = cmdAppt1.ForeColor
End Sub

Public Function GetAdminItem(Ref As Integer, Item As String) As Boolean
GetAdminItem = False
Item = ""
If (Ref > 0) And (Ref < 40) Then
    Item = Trim(AdminList(Ref))
    If (Item <> "") Then
        GetAdminItem = True
    End If
End If
End Function

Private Function GetEyes(AEye As String) As Boolean
GetEyes = True
AEye = ""
frmEventMsgs.Header = "Which Eye ?"
frmEventMsgs.AcceptText = "OD"
frmEventMsgs.RejectText = "OS"
frmEventMsgs.CancelText = "Not Needed"
frmEventMsgs.Other0Text = "OU"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    AEye = " (OD)"
ElseIf (frmEventMsgs.Result = 2) Then
    AEye = " (OS)"
ElseIf (frmEventMsgs.Result = 3) Then
    AEye = " (OU)"
End If
End Function
Public Function FrmClose()
Call cmdQuit_Click
  Unload Me
End Function

