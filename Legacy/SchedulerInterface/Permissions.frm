VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPermissions 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Permissions"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkOpt70 
      BackColor       =   &H0077742D&
      Caption         =   "Cancel Appointment From Exam"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   56
      Top             =   3600
      Width           =   4575
   End
   Begin VB.CheckBox chkOpt69 
      BackColor       =   &H0077742D&
      Caption         =   "Send eRx"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   44
      Top             =   2640
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt110 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Patient Access"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8880
      TabIndex        =   53
      Top             =   4440
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt109 
      BackColor       =   &H0077742D&
      Caption         =   "Exam CheckOut"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   52
      Top             =   4080
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt108 
      BackColor       =   &H0077742D&
      Caption         =   "Follow Up And Surgery"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5640
      TabIndex        =   51
      Top             =   4080
      Width           =   3135
   End
   Begin VB.CheckBox chkOpt106 
      BackColor       =   &H0077742D&
      Caption         =   "Past Visits"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   50
      Top             =   6960
      Width           =   1695
   End
   Begin VB.CheckBox chkOpt105 
      BackColor       =   &H0077742D&
      Caption         =   "Plan"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   49
      Top             =   6600
      Width           =   1455
   End
   Begin VB.CheckBox chkOpt104 
      BackColor       =   &H0077742D&
      Caption         =   "Findings"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   48
      Top             =   6240
      Width           =   1455
   End
   Begin VB.CheckBox chkOpt103 
      BackColor       =   &H0077742D&
      Caption         =   "Meds"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   47
      Top             =   5880
      Width           =   1455
   End
   Begin VB.CheckBox chkOpt102 
      BackColor       =   &H0077742D&
      Caption         =   "History,Allergies"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   46
      Top             =   5520
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt101 
      BackColor       =   &H0077742D&
      Caption         =   "Exam Elements"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   45
      Top             =   5170
      Width           =   1935
   End
   Begin VB.CheckBox chkOpt100 
      BackColor       =   &H0077742D&
      Caption         =   "Open Chart"
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8880
      TabIndex        =   54
      Top             =   4800
      Width           =   1815
   End
   Begin VB.CheckBox chkOpt65 
      BackColor       =   &H0077742D&
      Caption         =   "Set/Clear Alerts"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5640
      TabIndex        =   43
      Top             =   2640
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt25 
      BackColor       =   &H0077742D&
      Caption         =   "Patient Notes"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5640
      TabIndex        =   42
      Top             =   2280
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt24 
      BackColor       =   &H0077742D&
      Caption         =   "Practice Notes"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5640
      TabIndex        =   41
      Top             =   1920
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt23 
      BackColor       =   &H0077742D&
      Caption         =   "Contacts"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5640
      TabIndex        =   40
      Top             =   1560
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt64 
      BackColor       =   &H0077742D&
      Caption         =   "DI Edit Previous Visit"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5640
      TabIndex        =   39
      Top             =   1200
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt55 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Other Favorites"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   38
      Top             =   6960
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt62 
      BackColor       =   &H0077742D&
      Caption         =   "Set Power User"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   37
      Top             =   3000
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt54 
      BackColor       =   &H0077742D&
      Caption         =   "Maintain Billing Notes"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   36
      Top             =   2640
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt53 
      BackColor       =   &H0077742D&
      Caption         =   "Allow Aggr Insr Changes"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   35
      Top             =   6600
      Width           =   2535
   End
   Begin VB.CheckBox chkOpt22 
      BackColor       =   &H0077742D&
      Caption         =   "Patient Locator"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   34
      Top             =   4080
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt21 
      BackColor       =   &H0077742D&
      Caption         =   "Write Presciptions"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   33
      Top             =   1920
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt51 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Diagnosis"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   27
      Top             =   6240
      Width           =   2895
   End
   Begin VB.CheckBox chkOpt49 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Services"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   25
      Top             =   5520
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt50 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Service Codes"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   26
      Top             =   5880
      Width           =   3135
   End
   Begin VB.CheckBox chkOpt47 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Type of Service"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   23
      Top             =   4800
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt48 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Place of Service"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   24
      Top             =   5160
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt46 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Referring Doctors"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   22
      Top             =   6960
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt44 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Fee Schedules"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   20
      Top             =   6240
      Width           =   2895
   End
   Begin VB.CheckBox chkOpt45 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Codes"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   21
      Top             =   6600
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt42 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Favorites"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   18
      Top             =   5520
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt43 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Notes, Actions, Tests"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   19
      Top             =   5880
      Width           =   3135
   End
   Begin VB.CheckBox chkOpt40 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Vendors"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   16
      Top             =   4800
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt41 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Plans"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   17
      Top             =   5160
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt38 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Configuration"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   15
      Top             =   6600
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt35 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Appt Types"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   14
      Top             =   5520
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt34 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Resources"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   13
      Top             =   5160
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt6 
      BackColor       =   &H0077742D&
      Caption         =   "Patient Clinical Data"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   3000
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt15 
      BackColor       =   &H0077742D&
      Caption         =   "Clinical Exchange"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   11
      Top             =   3000
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt14 
      BackColor       =   &H0077742D&
      Caption         =   "Book Payables"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   10
      Top             =   2640
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt12 
      BackColor       =   &H0077742D&
      Caption         =   "Submit"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   9
      Top             =   1920
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt10 
      BackColor       =   &H0077742D&
      Caption         =   "Office Monitor"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3120
      TabIndex        =   8
      Top             =   1200
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt9 
      BackColor       =   &H0077742D&
      Caption         =   "Exam Room"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   4080
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt8 
      BackColor       =   &H0077742D&
      Caption         =   "Waiting Room"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   3720
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt7 
      BackColor       =   &H0077742D&
      Caption         =   "Waiting for Questionaire"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   3360
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt5 
      BackColor       =   &H0077742D&
      Caption         =   "Review"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   2640
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt33 
      BackColor       =   &H0077742D&
      Caption         =   "Setup Practice"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   12
      Top             =   4800
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt2 
      BackColor       =   &H0077742D&
      Caption         =   "Check Out"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   1560
      Width           =   2295
   End
   Begin VB.CheckBox chkOpt1 
      BackColor       =   &H0077742D&
      Caption         =   "Check In"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   1200
      Width           =   2295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   360
      TabIndex        =   29
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Permissions.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   9840
      TabIndex        =   28
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Permissions.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   3120
      TabIndex        =   55
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Permissions.frx":03BE
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Option Access"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   8880
      TabIndex        =   32
      Top             =   840
      Width           =   1485
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "General Functions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   120
      TabIndex        =   31
      Top             =   840
      Width           =   1830
   End
   Begin VB.Label lblSetup 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Setup Permissions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   120
      TabIndex        =   30
      Top             =   4440
      Width           =   1845
   End
   Begin VB.Label lblPerms 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Select Permissions for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2640
      TabIndex        =   0
      Top             =   480
      Width           =   2475
   End
End
Attribute VB_Name = "frmPermissions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public Permissions As String
Public UserName As String
Public UserId As Long

Public Function LoadPermission() As Boolean
Dim i As Integer
LoadPermission = True
lblPerms.Caption = "Select Permissions for " + UserName
For i = 1 To Len(Permissions)
    If (Mid(Permissions, i, 1) <> "0") And (Mid(Permissions, i, 1) <> " ") Then
        If (i = 1) Then
            chkOpt1.Value = 1
        ElseIf (i = 2) Then
            chkOpt2.Value = 1
        ElseIf (i = 5) Then
            chkOpt5.Value = 1
        ElseIf (i = 6) Then
            chkOpt6.Value = 1
        ElseIf (i = 7) Then
            chkOpt7.Value = 1
        ElseIf (i = 8) Then
            chkOpt8.Value = 1
        ElseIf (i = 9) Then
            chkOpt9.Value = 1
        ElseIf (i = 10) Then
            chkOpt10.Value = 1
        ElseIf (i = 12) Then
            chkOpt12.Value = 1
        ElseIf (i = 14) Then
            chkOpt14.Value = 1
        ElseIf (i = 15) Then
            chkOpt15.Value = 1
        ElseIf (i = 21) Then
            chkOpt21.Value = 1
        ElseIf (i = 22) Then
            chkOpt22.Value = 1
        ElseIf (i = 23) Then
            chkOpt23.Value = 1
        ElseIf (i = 24) Then
            chkOpt24.Value = 1
        ElseIf (i = 25) Then
            chkOpt25.Value = 1
        ElseIf (i = 33) Then
            chkOpt33.Value = 1
        ElseIf (i = 34) Then
            chkOpt34.Value = 1
        ElseIf (i = 35) Then
            chkOpt35.Value = 1
        ElseIf (i = 38) Then
            chkOpt38.Value = 1
        ElseIf (i = 40) Then
            chkOpt40.Value = 1
        ElseIf (i = 41) Then
            chkOpt41.Value = 1
        ElseIf (i = 42) Then
            chkOpt42.Value = 1
        ElseIf (i = 43) Then
            chkOpt43.Value = 1
        ElseIf (i = 44) Then
            chkOpt44.Value = 1
        ElseIf (i = 45) Then
            chkOpt45.Value = 1
        ElseIf (i = 46) Then
            chkOpt46.Value = 1
        ElseIf (i = 47) Then
            chkOpt47.Value = 1
        ElseIf (i = 48) Then
            chkOpt48.Value = 1
        ElseIf (i = 49) Then
            chkOpt49.Value = 1
        ElseIf (i = 50) Then
            chkOpt50.Value = 1
        ElseIf (i = 51) Then
            chkOpt51.Value = 1
        ElseIf (i = 53) Then
            chkOpt53.Value = 1
        ElseIf (i = 54) Then
            chkOpt54.Value = 1
        ElseIf (i = 55) Then
            chkOpt55.Value = 1
        ElseIf (i = 62) Then
            chkOpt62.Value = 1
        ElseIf (i = 64) Then
            chkOpt64.Value = 1
        ElseIf (i = 65) Then
            chkOpt65.Value = 1
        ElseIf i = 69 Then
            chkOpt69.Value = 1
        ElseIf i = 70 Then
            chkOpt70.Value = 1
        'Changes Made for Access Control
        ElseIf (i = 100) Then
            chkOpt100.Value = 1
        ElseIf (i = 101) Then
            chkOpt101.Value = 1
        ElseIf (i = 102) Then
            chkOpt102.Value = 1
        ElseIf (i = 103) Then
            chkOpt103.Value = 1
        ElseIf (i = 104) Then
            chkOpt104.Value = 1
        ElseIf (i = 105) Then
            chkOpt105.Value = 1
        ElseIf (i = 106) Then
            chkOpt106.Value = 1
        ElseIf (i = 108) Then
            chkOpt108.Value = 1
        ElseIf (i = 109) Then
            chkOpt109.Value = 1
        ElseIf (i = 110) Then
            chkOpt110.Value = 1
        End If
    End If
Next i
End Function

Private Sub cmdApply_Click()
Dim Temp As String
CurrentAction = ""
Temp = String(128, "0")
If (chkOpt1.Value = 1) Then
    Mid(Temp, 1, 1) = "1"
End If
If (chkOpt2.Value = 1) Then
    Mid(Temp, 2, 1) = "1"
End If
If (chkOpt5.Value = 1) Then
    Mid(Temp, 5, 1) = "1"
End If
If (chkOpt6.Value = 1) Then
    Mid(Temp, 6, 1) = "1"
End If
If (chkOpt7.Value = 1) Then
    Mid(Temp, 7, 1) = "1"
End If
If (chkOpt8.Value = 1) Then
    Mid(Temp, 8, 1) = "1"
End If
If (chkOpt9.Value = 1) Then
    Mid(Temp, 9, 1) = "1"
End If
If (chkOpt10.Value = 1) Then
    Mid(Temp, 10, 1) = "1"
End If
If (chkOpt12.Value = 1) Then
    Mid(Temp, 12, 1) = "1"
End If
If (chkOpt14.Value = 1) Then
    Mid(Temp, 14, 1) = "1"
End If
If (chkOpt15.Value = 1) Then
    Mid(Temp, 15, 1) = "1"
End If
If (chkOpt21.Value = 1) Then
    Mid(Temp, 21, 1) = "1"
End If
If (chkOpt22.Value = 1) Then
    Mid(Temp, 22, 1) = "1"
End If
If (chkOpt23.Value = 1) Then
    Mid(Temp, 23, 1) = "1"
End If
If (chkOpt24.Value = 1) Then
    Mid(Temp, 24, 1) = "1"
End If
If (chkOpt25.Value = 1) Then
    Mid(Temp, 25, 1) = "1"
End If
If (chkOpt33.Value = 1) Then
    Mid(Temp, 33, 1) = "1"
End If
If (chkOpt34.Value = 1) Then
    Mid(Temp, 34, 1) = "1"
End If
If (chkOpt35.Value = 1) Then
    Mid(Temp, 35, 1) = "1"
End If
If (chkOpt38.Value = 1) Then
    Mid(Temp, 38, 1) = "1"
End If
If (chkOpt40.Value = 1) Then
    Mid(Temp, 40, 1) = "1"
End If
If (chkOpt41.Value = 1) Then
    Mid(Temp, 41, 1) = "1"
End If
If (chkOpt42.Value = 1) Then
    Mid(Temp, 42, 1) = "1"
End If
If (chkOpt43.Value = 1) Then
    Mid(Temp, 43, 1) = "1"
End If
If (chkOpt44.Value = 1) Then
    Mid(Temp, 44, 1) = "1"
End If
If (chkOpt45.Value = 1) Then
    Mid(Temp, 45, 1) = "1"
End If
If (chkOpt46.Value = 1) Then
    Mid(Temp, 46, 1) = "1"
End If
If (chkOpt47.Value = 1) Then
    Mid(Temp, 47, 1) = "1"
End If
If (chkOpt48.Value = 1) Then
    Mid(Temp, 48, 1) = "1"
End If
If (chkOpt49.Value = 1) Then
    Mid(Temp, 49, 1) = "1"
End If
If (chkOpt50.Value = 1) Then
    Mid(Temp, 50, 1) = "1"
End If
If (chkOpt51.Value = 1) Then
    Mid(Temp, 51, 1) = "1"
End If
If (chkOpt53.Value = 1) Then
    Mid(Temp, 53, 1) = "1"
End If
If (chkOpt54.Value = 1) Then
    Mid(Temp, 54, 1) = "1"
End If
If (chkOpt55.Value = 1) Then
    Mid(Temp, 55, 1) = "1"
End If
If (chkOpt62.Value = 1) Then
    Mid(Temp, 62, 1) = "1"
End If
If (chkOpt64.Value = 1) Then
    Mid(Temp, 64, 1) = "1"
End If
If (chkOpt65.Value = 1) Then
    Mid(Temp, 65, 1) = "1"
End If
If chkOpt69.Value = 1 Then
    Mid$(Temp, 69, 1) = "1"
End If
If chkOpt70.Value = 1 Then
    Mid$(Temp, 70, 1) = "1"
End If
'Change Made for Access Control
If (chkOpt100.Value = 1) Then
    Mid(Temp, 100, 1) = "1"
End If
If (chkOpt101.Value = 1) Then
    Mid(Temp, 101, 1) = "1"
End If
If (chkOpt102.Value = 1) Then
    Mid(Temp, 102, 1) = "1"
End If
If (chkOpt103.Value = 1) Then
    Mid(Temp, 103, 1) = "1"
End If
If (chkOpt104.Value = 1) Then
    Mid(Temp, 104, 1) = "1"
End If
If (chkOpt105.Value = 1) Then
    Mid(Temp, 105, 1) = "1"
End If
If (chkOpt106.Value = 1) Then
    Mid(Temp, 106, 1) = "1"
End If
If (chkOpt108.Value = 1) Then
    Mid(Temp, 108, 1) = "1"
End If
If (chkOpt109.Value = 1) Then
    Mid(Temp, 109, 1) = "1"
End If
If (chkOpt110.Value = 1) Then
    Mid(Temp, 110, 1) = "1"
End If
Permissions = Temp
Unload frmPermissions
End Sub

Private Sub cmdBack_Click()
CurrentAction = "Home"
Unload frmPermissions
End Sub

Private Sub cmdMore_Click()
On Error GoTo lcmdMore_Click

Dim PermissionsComWrapper As New comWrapper
Dim loadArguments As Object
Dim arguments() As Variant

Call PermissionsComWrapper.Create(PermissionLoadArgumentsType, emptyArgs)
Set loadArguments = PermissionsComWrapper.Instance
loadArguments.UserId = UserId
arguments = Array(loadArguments)
Call PermissionsComWrapper.Create(PermissionsViewManagerType, emptyArgs)
Call PermissionsComWrapper.InvokeMethod("ShowPermissionsView", arguments)

Exit Sub

lcmdMore_Click:
    LogError "frmPermissions", "cmdMore_Click", Err, Err.Description
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPermissions.BorderStyle = 1
    frmPermissions.ClipControls = True
    frmPermissions.Caption = Mid(frmPermissions.Name, 4, Len(frmPermissions.Name) - 3)
    frmPermissions.AutoRedraw = True
    frmPermissions.Refresh
End If
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
