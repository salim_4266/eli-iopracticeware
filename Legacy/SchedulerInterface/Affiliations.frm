VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmAffiliations 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkOrgOverride 
      BackColor       =   &H0077742D&
      Caption         =   "Override Organization Name"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1320
      TabIndex        =   13
      Top             =   3840
      Width           =   3735
   End
   Begin VB.TextBox txtOPin 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1680
      MaxLength       =   32
      TabIndex        =   11
      Top             =   2640
      Width           =   4575
   End
   Begin VB.TextBox txtLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1680
      MaxLength       =   75
      TabIndex        =   9
      Top             =   3120
      Width           =   4560
   End
   Begin VB.CheckBox chkOverride 
      BackColor       =   &H0077742D&
      Caption         =   "Override"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1320
      TabIndex        =   6
      Top             =   3600
      Width           =   1575
   End
   Begin VB.TextBox txtPin 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1680
      MaxLength       =   32
      TabIndex        =   2
      Top             =   2160
      Width           =   4575
   End
   Begin VB.TextBox txtPlan 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1680
      MaxLength       =   75
      TabIndex        =   0
      Top             =   1680
      Width           =   4560
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   1
      Top             =   7800
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Affiliations.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10080
      TabIndex        =   3
      Top             =   7830
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Affiliations.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemove 
      Height          =   990
      Left            =   4680
      TabIndex        =   8
      Top             =   7800
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Affiliations.frx":03BE
   End
   Begin VB.Label lblAff2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Other Pin"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   360
      TabIndex        =   12
      Top             =   2640
      Width           =   1215
   End
   Begin VB.Label lblLoc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   480
      TabIndex        =   10
      Top             =   3120
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Affiliations"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3000
      TabIndex        =   7
      Top             =   360
      Width           =   4335
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   480
      TabIndex        =   5
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label lblAffiliations 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pin"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   720
      TabIndex        =   4
      Top             =   2160
      Width           =   855
   End
End
Attribute VB_Name = "frmAffiliations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AffiliationId As Long
Public ResourceId As Long
Public LocationId As Long
Public ResourceType As String
Public OverrideOn As Boolean
Public OrgOverrideOn As Boolean
Public RemoveOn As Boolean
Private PlanId As Long
Private LocId As Long

Public Function LoadAffiliationDisplay() As Boolean
Dim Temp1 As String, Temp2 As String
Dim Temp3 As String, Temp4 As String
Dim TheName As String, LocName As String
Dim ApplTbl As ApplicationTables
Dim ApplTemp As ApplicationTemplates
LoadAffiliationDisplay = False
PlanId = 0
txtPlan.Text = ""
txtPin.Text = ""
chkOverride.Value = 0
chkOrgOverride.Value = 0
cmdRemove.Visible = RemoveOn
If (AffiliationId > 0) Then
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplGetAffiliations(AffiliationId, Temp1, TheName, PlanId, LocId, OverrideOn, Temp4, OrgOverrideOn)) Then
        txtPin.Text = Temp1
        txtOPin.Text = Temp4
        txtPlan.Text = TheName
        If (OverrideOn) Then
            chkOverride.Value = 1
        Else
            chkOverride.Value = 0
        End If
        If (OrgOverrideOn) Then
            chkOrgOverride.Value = 1
        Else
            chkOrgOverride.Value = 0
        End If
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplGetResourceName(0, LocId, LocName)
        Set ApplTemp = Nothing
        Temp2 = Space(56)
        Mid(Temp2, 1, Len(LocName)) = LocName
        Temp2 = Temp2 + Trim(Str(LocId))
        txtLoc.Text = Temp2
    End If
    chkOverride.Visible = True
    chkOrgOverride.Visible = True
    Set ApplTbl = Nothing
    LoadAffiliationDisplay = True
Else
    LoadAffiliationDisplay = True
    cmdRemove.Visible = False
    RemoveOn = False
End If
End Function

Private Sub cmdApply_Click()
Dim Temp As String
Dim Temp1 As String
Dim OverrideOn As Boolean
Dim ApplTbl As ApplicationTables
If (Trim(txtPin.Text) = "") Then
    frmEventMsgs.Header = "Enter Pin"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPin.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
Temp = txtPin.Text
Call StripCharacters(Temp, "'")
txtPin.Text = Temp
If (Trim(txtPlan.Text) = "") Then
    frmEventMsgs.Header = "Select Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPlan.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
Temp1 = txtOPin.Text
Call StripCharacters(Temp1, "'")
Set ApplTbl = New ApplicationTables
If (AffiliationId < 1) Then
    If (ApplTbl.ApplIsAffiliation(ResourceId, ResourceType, PlanId, LocId)) Then
        frmEventMsgs.Header = "Affiliation Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        txtPlan.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
End If
OverrideOn = False
If (chkOverride.Value = 1) Then
    OverrideOn = True
End If
OrgOverrideOn = False
If (chkOrgOverride.Value = 1) Then
    OrgOverrideOn = True
End If
If Not (ApplTbl.ApplPostAffiliation(AffiliationId, ResourceId, PlanId, ResourceType, Temp, LocId, OverrideOn, Temp1, OrgOverrideOn)) Then
    frmEventMsgs.Header = "Update not performed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set ApplTbl = Nothing
Unload frmAffiliations
End Sub

Private Sub cmdBack_Click()
Unload frmAffiliations
End Sub

Private Sub cmdRemove_Click()
Dim ApplTbl As ApplicationTables
If (AffiliationId < 1) Then
    frmEventMsgs.Header = "Must be an existing Affiliation"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    Set ApplTbl = New ApplicationTables
    Call ApplTbl.ApplDeleteAffiliation(AffiliationId)
    Set ApplTbl = Nothing
    Unload frmAffiliations
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmAffiliations.BorderStyle = 1
    frmAffiliations.ClipControls = True
    frmAffiliations.Caption = Mid(frmAffiliations.Name, 4, Len(frmAffiliations.Name) - 3)
    frmAffiliations.AutoRedraw = True
    frmAffiliations.Refresh
End If
End Sub

Private Sub txtLoc_Click()
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtLoc.Text = frmSelectDialogue.Selection
    LocId = val(Trim(Mid(frmSelectDialogue.Selection, 56, Len(frmSelectDialogue.Selection) - 55)))
End If
End Sub

Private Sub txtLoc_KeyPress(KeyAscii As Integer)
Call txtLoc_Click
KeyAscii = 0
End Sub

Private Sub txtPlan_Click()
Dim TheText As String
Dim ReturnArguments As Object
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Set ReturnArguments = DisplayInsurancePlanSearchScreen
If Not ReturnArguments Is Nothing Then
    If (ReturnArguments.InsurerId > 0) Then
        Set RetPatientFinancialService = New PatientFinancialService
        RetPatientFinancialService.InsurerId = ReturnArguments.InsurerId
        Set RS = RetPatientFinancialService.RetrieveInsurer
        If Not RS Is Nothing And RS.RecordCount > 0 Then
            txtPlan.Text = RS("Name")
            PlanId = ReturnArguments.InsurerId
        End If
        Set RetPatientFinancialService = Nothing
    End If
End If
End Sub

Private Sub txtPlan_KeyPress(KeyAscii As Integer)
Call txtPlan_Click
KeyAscii = 0
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
