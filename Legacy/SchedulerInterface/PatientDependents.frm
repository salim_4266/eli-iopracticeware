VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPatientDependents 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkDel23 
      BackColor       =   &H0077742D&
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   37
      Top             =   6360
      Width           =   1455
   End
   Begin VB.CheckBox chkDel22 
      BackColor       =   &H0077742D&
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   36
      Top             =   5880
      Width           =   1455
   End
   Begin VB.CheckBox chkDel21 
      BackColor       =   &H0077742D&
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   35
      Top             =   5400
      Width           =   1455
   End
   Begin VB.CheckBox chkDel13 
      BackColor       =   &H0077742D&
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   34
      Top             =   2760
      Width           =   1455
   End
   Begin VB.CheckBox chkDel12 
      BackColor       =   &H0077742D&
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   33
      Top             =   2280
      Width           =   1455
   End
   Begin VB.CheckBox chkDel11 
      BackColor       =   &H0077742D&
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   32
      Top             =   1800
      Width           =   1455
   End
   Begin VB.TextBox txtPH2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2160
      MaxLength       =   35
      TabIndex        =   15
      Top             =   4320
      Width           =   5295
   End
   Begin VB.TextBox txtMem12 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      MaxLength       =   32
      TabIndex        =   13
      Top             =   2280
      Width           =   1455
   End
   Begin VB.TextBox txtMem11 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      MaxLength       =   32
      TabIndex        =   12
      Top             =   1800
      Width           =   1455
   End
   Begin VB.TextBox txtPH1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2160
      MaxLength       =   35
      TabIndex        =   11
      Top             =   645
      Width           =   5295
   End
   Begin VB.TextBox txtMem21 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      MaxLength       =   32
      TabIndex        =   10
      Top             =   5400
      Width           =   1455
   End
   Begin VB.TextBox txtMem22 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      MaxLength       =   32
      TabIndex        =   9
      Top             =   5880
      Width           =   1455
   End
   Begin VB.TextBox txtMem13 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      MaxLength       =   32
      TabIndex        =   7
      Top             =   2760
      Width           =   1455
   End
   Begin VB.TextBox txtMem23 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6000
      MaxLength       =   32
      TabIndex        =   6
      Top             =   6360
      Width           =   1455
   End
   Begin VB.TextBox txtSub13 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   10
      TabIndex        =   5
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox txtSub11 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1800
      Width           =   1335
   End
   Begin VB.TextBox txtSub12 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   10
      TabIndex        =   3
      Top             =   2280
      Width           =   1335
   End
   Begin VB.TextBox txtSub23 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   2
      Top             =   6360
      Width           =   1335
   End
   Begin VB.TextBox txtSub21 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   1
      Top             =   5400
      Width           =   1335
   End
   Begin VB.TextBox txtSub22 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8520
      MaxLength       =   10
      TabIndex        =   0
      Top             =   5880
      Width           =   1335
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9840
      TabIndex        =   8
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDependents.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   405
      TabIndex        =   14
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDependents.frx":01DF
   End
   Begin VB.Line Line1 
      BorderColor     =   &H000000C0&
      X1              =   120
      X2              =   11880
      Y1              =   4080
      Y2              =   4080
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6000
      TabIndex        =   31
      Top             =   1440
      Width           =   1305
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   480
      TabIndex        =   30
      Top             =   1440
      Width           =   2865
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy Holder"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   29
      Top             =   645
      Width           =   2025
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Financial Responsibility for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   3450
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy Holder"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   27
      Top             =   4320
      Width           =   2025
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   480
      TabIndex        =   26
      Top             =   5040
      Width           =   2865
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Policy #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6000
      TabIndex        =   25
      Top             =   5040
      Width           =   1305
   End
   Begin VB.Label lblPatientName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3600
      TabIndex        =   24
      Top             =   120
      Width           =   3885
   End
   Begin VB.Label lblIns12 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   23
      Top             =   2280
      Width           =   4215
   End
   Begin VB.Label lblIns11 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   22
      Top             =   1800
      Width           =   4215
   End
   Begin VB.Label lblIns21 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   21
      Top             =   5400
      Width           =   4215
   End
   Begin VB.Label lblIns22 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   20
      Top             =   5880
      Width           =   4215
   End
   Begin VB.Label lblIns13 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   19
      Top             =   2760
      Width           =   4215
   End
   Begin VB.Label lblIns23 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   18
      Top             =   6360
      Width           =   4215
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Subscriber"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8400
      TabIndex        =   17
      Top             =   1440
      Width           =   1305
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Subscriber"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8520
      TabIndex        =   16
      Top             =   5040
      Width           =   1305
   End
End
Attribute VB_Name = "frmPatientDependents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private DependentId(6) As Long
Private PatientId As Long
Private PolicyHolder1 As Long
Private FirstInsurerId1 As Long
Private FirstInsurerId2 As Long
Private FirstInsurerId3 As Long
Private PolicyHolder2 As Long
Private SecondInsurerId1 As Long
Private SecondInsurerId2 As Long
Private SecondInsurerId3 As Long
Private InsurerType As String

Private Sub chkDel11_Click()
If (Trim(txtMem11.Text) <> "") Or (Trim(txtSub11.Text) <> "") Then
    Call DeleteDependent(DependentId(1))
    Call FinancialLoadDisplay(PatientId, PolicyHolder1, PolicyHolder2, InsurerType)
End If
End Sub

Private Sub chkDel12_Click()
If (Trim(txtMem12.Text) <> "") Or (Trim(txtSub12.Text) <> "") Then
    Call DeleteDependent(DependentId(2))
    Call FinancialLoadDisplay(PatientId, PolicyHolder1, PolicyHolder2, InsurerType)
End If
End Sub

Private Sub chkDel13_Click()
If (Trim(txtMem13.Text) <> "") Or (Trim(txtSub13.Text) <> "") Then
    Call DeleteDependent(DependentId(3))
    Call FinancialLoadDisplay(PatientId, PolicyHolder1, PolicyHolder2, InsurerType)
End If
End Sub

Private Sub chkDel21_Click()
If (Trim(txtMem21.Text) <> "") Or (Trim(txtSub21.Text) <> "") Then
    Call DeleteDependent(DependentId(4))
    Call FinancialLoadDisplay(PatientId, PolicyHolder1, PolicyHolder2, InsurerType)
End If
End Sub

Private Sub chkDel22_Click()
If (Trim(txtMem22.Text) <> "") Or (Trim(txtSub22.Text) <> "") Then
    Call DeleteDependent(DependentId(5))
    Call FinancialLoadDisplay(PatientId, PolicyHolder1, PolicyHolder2, InsurerType)
End If
End Sub

Private Sub chkDel23_Click()
If (Trim(txtMem23.Text) <> "") Or (Trim(txtSub23.Text) <> "") Then
    Call DeleteDependent(DependentId(6))
    Call FinancialLoadDisplay(PatientId, PolicyHolder1, PolicyHolder2, InsurerType)
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientFinancial.BorderStyle = 1
    frmPatientFinancial.ClipControls = True
    frmPatientFinancial.Caption = Mid(frmPatientFinancial.Name, 4, Len(frmPatientFinancial.Name) - 3)
    frmPatientFinancial.AutoRedraw = True
    frmPatientFinancial.Refresh
End If
End Sub

Private Sub cmdDone_Click()
Dim RetDep As PatientFinanceDependents
If (PolicyHolder1 > 0) Then
    If (verifyPost(1)) Then
        If (FirstInsurerId1 > 0) Then
            Set RetDep = New PatientFinanceDependents
            RetDep.DependentId = DependentId(1)
            If (RetDep.RetrievePatientFinancialDependents) Then
                RetDep.DependentPatientId = PatientId
                RetDep.DependentPolicyHolderId = PolicyHolder1
                RetDep.DependentInsurerId = FirstInsurerId1
                RetDep.DependentMember = Trim(txtMem11.Text)
                RetDep.DependentSubscriber = Trim(txtSub11.Text)
                Call RetDep.ApplyPatientFinancialDependents
                Set RetDep = Nothing
            End If
        End If
        If (FirstInsurerId1 > 0) Then
            Set RetDep = New PatientFinanceDependents
            RetDep.DependentId = DependentId(2)
            If (RetDep.RetrievePatientFinancialDependents) Then
                RetDep.DependentPatientId = PatientId
                RetDep.DependentPolicyHolderId = PolicyHolder1
                RetDep.DependentInsurerId = FirstInsurerId2
                RetDep.DependentMember = Trim(txtMem12.Text)
                RetDep.DependentSubscriber = Trim(txtSub12.Text)
                Call RetDep.ApplyPatientFinancialDependents
                Set RetDep = Nothing
            End If
        End If
        If (FirstInsurerId3 > 0) Then
            Set RetDep = New PatientFinanceDependents
            RetDep.DependentId = DependentId(3)
            If (RetDep.RetrievePatientFinancialDependents) Then
                RetDep.DependentPatientId = PatientId
                RetDep.DependentPolicyHolderId = PolicyHolder1
                RetDep.DependentInsurerId = FirstInsurerId3
                RetDep.DependentMember = Trim(txtMem13.Text)
                RetDep.DependentSubscriber = Trim(txtSub13.Text)
                Call RetDep.ApplyPatientFinancialDependents
                Set RetDep = Nothing
            End If
        End If
    Else
        Exit Sub
    End If
End If
If (PolicyHolder2 > 0) Then
    If (verifyPost(2)) Then
        If (SecondInsurerId1 > 0) And (Trim(txtMem21.Text) <> "") Then
            Set RetDep = New PatientFinanceDependents
            RetDep.DependentId = DependentId(4)
            If (RetDep.RetrievePatientFinancialDependents) Then
                RetDep.DependentPatientId = PatientId
                RetDep.DependentPolicyHolderId = PolicyHolder2
                RetDep.DependentInsurerId = SecondInsurerId1
                RetDep.DependentMember = Trim(txtMem21.Text)
                RetDep.DependentSubscriber = Trim(txtSub21.Text)
                Call RetDep.ApplyPatientFinancialDependents
                Set RetDep = Nothing
            End If
        End If
        If (SecondInsurerId2 > 0) And (Trim(txtMem22.Text) <> "") Then
            Set RetDep = New PatientFinanceDependents
            RetDep.DependentId = DependentId(5)
            If (RetDep.RetrievePatientFinancialDependents) Then
                RetDep.DependentPatientId = PatientId
                RetDep.DependentPolicyHolderId = PolicyHolder2
                RetDep.DependentInsurerId = SecondInsurerId2
                RetDep.DependentMember = Trim(txtMem22.Text)
                RetDep.DependentSubscriber = Trim(txtSub22.Text)
                Call RetDep.ApplyPatientFinancialDependents
                Set RetDep = Nothing
            End If
        End If
        If (SecondInsurerId3 > 0) And (Trim(txtMem23.Text) <> "") Then
            Set RetDep = New PatientFinanceDependents
            RetDep.DependentId = DependentId(6)
            If (RetDep.RetrievePatientFinancialDependents) Then
                RetDep.DependentPatientId = PatientId
                RetDep.DependentPolicyHolderId = PolicyHolder2
                RetDep.DependentInsurerId = SecondInsurerId3
                RetDep.DependentMember = Trim(txtMem13.Text)
                RetDep.DependentSubscriber = Trim(txtSub13.Text)
                Call RetDep.ApplyPatientFinancialDependents
                Set RetDep = Nothing
            End If
        End If
    Else
        Exit Sub
    End If
End If
Unload frmPatientDependents
End Sub

Private Sub cmdHome_Click()
Unload frmPatientDependents
End Sub

Public Function FinancialLoadDisplay(PatId As Long, FirstPolicyHolderId As Long, SecondPolicyHolderId As Long, InsType As String) As Boolean
Dim RetrieveAnotherPatient As Patient
Dim RetrieveInsurer As Insurer
Dim RetDep As PatientFinanceDependents
FinancialLoadDisplay = True
chkDel11.Value = 0
chkDel12.Value = 0
chkDel13.Value = 0
chkDel21.Value = 0
chkDel22.Value = 0
chkDel23.Value = 0
Set RetrieveAnotherPatient = New Patient
PatientId = PatId
InsurerType = InsType
RetrieveAnotherPatient.PatientId = PatId
If (RetrieveAnotherPatient.RetrievePatient) Then
    lblPatientName.Caption = RetrieveAnotherPatient.FirstName + " " _
                           + RetrieveAnotherPatient.MiddleInitial + " " _
                           + RetrieveAnotherPatient.LastName + " " _
                           + RetrieveAnotherPatient.NameRef
End If
Set RetrieveAnotherPatient = Nothing
Erase DependentId
txtPH1.Text = ""
txtPH2.Text = ""
txtPH1.Locked = True
txtPH2.Locked = True
lblIns11.Caption = ""
lblIns12.Caption = ""
lblIns13.Caption = ""
txtMem11.Text = ""
txtMem11.Enabled = False
txtMem12.Text = ""
txtMem12.Enabled = False
txtMem13.Text = ""
txtMem13.Enabled = False
txtSub11.Text = ""
txtSub11.Enabled = False
txtSub12.Text = ""
txtSub12.Enabled = False
txtSub13.Text = ""
txtSub13.Enabled = False

lblIns21.Caption = ""
lblIns22.Caption = ""
lblIns23.Caption = ""
txtMem21.Text = ""
txtMem21.Enabled = False
txtMem22.Text = ""
txtMem22.Enabled = False
txtMem23.Text = ""
txtMem23.Enabled = False
txtSub21.Text = ""
txtSub21.Enabled = False
txtSub22.Text = ""
txtSub22.Enabled = False
txtSub23.Text = ""
txtSub23.Enabled = False
' Policy Holder 1 Insurer Stuff
PolicyHolder1 = FirstPolicyHolderId
PolicyHolder2 = SecondPolicyHolderId
FirstInsurerId1 = 0
FirstInsurerId2 = 0
FirstInsurerId3 = 0
Set RetDep = New PatientFinanceDependents
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
If (PolicyHolder1 <> 0) And (PolicyHolder1 <> PatId) Then
    RetrieveAnotherPatient.PatientId = PolicyHolder1
    If (RetrieveAnotherPatient.RetrievePatient) Then
        txtPH1.Text = RetrieveAnotherPatient.FirstName + " " _
                    + RetrieveAnotherPatient.MiddleInitial + " " _
                    + RetrieveAnotherPatient.LastName + " " _
                    + RetrieveAnotherPatient.NameRef
        txtPH1.Locked = True
        Call GetActiveFinancial(PolicyHolder1, FirstInsurerId1, FirstInsurerId2, FirstInsurerId3, InsType)
        If (FirstInsurerId1 > 0) Then
            RetrieveInsurer.InsurerId = FirstInsurerId1
            If (RetrieveInsurer.RetrieveInsurer) Then
                lblIns11.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                 + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                 + Trim(RetrieveInsurer.InsurerGroupName)
                txtMem11.Enabled = True
                txtSub11.Enabled = True
                txtMem11.Locked = True
                txtSub11.Locked = True
                RetDep.DependentPatientId = PatientId
                RetDep.DependentInsurerId = FirstInsurerId1
                RetDep.DependentPolicyHolderId = PolicyHolder1
                If (RetDep.FindPatientFinancialDependents > 0) Then
                    If (RetDep.SelectPatientFinancialDependents(1)) Then
                        DependentId(1) = RetDep.DependentId
                        txtMem11.Text = RetDep.DependentMember
                        txtSub11.Text = RetDep.DependentSubscriber
                    End If
                End If
            End If
        End If
        If (FirstInsurerId2 > 0) Then
            RetrieveInsurer.InsurerId = FirstInsurerId2
            If (RetrieveInsurer.RetrieveInsurer) Then
                lblIns12.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                 + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                 + Trim(RetrieveInsurer.InsurerGroupName)
                                  txtMem12.Locked = True
                 txtMem12.Locked = True
                txtSub12.Locked = True
                txtMem12.Enabled = True
                txtSub12.Enabled = True
                RetDep.DependentPatientId = PatientId
                RetDep.DependentInsurerId = FirstInsurerId2
                RetDep.DependentPolicyHolderId = PolicyHolder1
                If (RetDep.FindPatientFinancialDependents > 0) Then
                    If (RetDep.SelectPatientFinancialDependents(1)) Then
                        DependentId(2) = RetDep.DependentId
                        txtMem12.Text = RetDep.DependentMember
                        txtSub12.Text = RetDep.DependentSubscriber
                    End If
                End If
            End If
        End If
        If (FirstInsurerId3 > 0) Then
            RetrieveInsurer.InsurerId = FirstInsurerId1
            If (RetrieveInsurer.RetrieveInsurer) Then
                lblIns13.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                 + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                 + Trim(RetrieveInsurer.InsurerGroupName)
                txtMem13.Locked = True
                txtSub13.Locked = True
                txtMem13.Enabled = True
                txtSub13.Enabled = True
                
                RetDep.DependentPatientId = PatientId
                RetDep.DependentInsurerId = FirstInsurerId3
                RetDep.DependentPolicyHolderId = PolicyHolder1
                If (RetDep.FindPatientFinancialDependents > 0) Then
                    If (RetDep.SelectPatientFinancialDependents(1)) Then
                        DependentId(3) = RetDep.DependentId
                        txtMem13.Text = RetDep.DependentMember
                        txtSub13.Text = RetDep.DependentSubscriber
                    End If
                End If
            End If
        End If
    End If
End If

' Policy Holder 2 Insurer Stuff
SecondInsurerId1 = 0
SecondInsurerId2 = 0
SecondInsurerId3 = 0
If (PolicyHolder2 <> 0) And (PolicyHolder2 <> PatId) Then
    Call GetActiveFinancial(PolicyHolder2, SecondInsurerId1, SecondInsurerId2, SecondInsurerId3, InsType)
    RetrieveAnotherPatient.PatientId = PolicyHolder2
    If (RetrieveAnotherPatient.RetrievePatient) Then
        txtPH2.Text = RetrieveAnotherPatient.FirstName + " " _
                    + RetrieveAnotherPatient.MiddleInitial + " " _
                    + RetrieveAnotherPatient.LastName + " " _
                    + RetrieveAnotherPatient.NameRef
        txtPH2.Locked = True
        If (SecondInsurerId1 > 0) Then
            RetrieveInsurer.InsurerId = SecondInsurerId1
            If (RetrieveInsurer.RetrieveInsurer) Then
                lblIns21.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                 + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                 + Trim(RetrieveInsurer.InsurerGroupName)
                txtMem21.Enabled = True
                txtSub21.Enabled = True
                txtMem21.Locked = True
                txtSub21.Locked = True
                RetDep.DependentPatientId = PatientId
                RetDep.DependentInsurerId = SecondInsurerId1
                RetDep.DependentPolicyHolderId = PolicyHolder2
                If (RetDep.FindPatientFinancialDependents > 0) Then
                    If (RetDep.SelectPatientFinancialDependents(1)) Then
                        DependentId(4) = RetDep.DependentId
                        txtMem21.Text = RetDep.DependentMember
                        txtSub21.Text = RetDep.DependentSubscriber
                    End If
                End If
            End If
        End If
        If (SecondInsurerId2 > 0) Then
            RetrieveInsurer.InsurerId = SecondInsurerId2
            If (RetrieveInsurer.RetrieveInsurer) Then
                lblIns22.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                 + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                 + Trim(RetrieveInsurer.InsurerGroupName)
                txtMem22.Enabled = True
                txtSub22.Enabled = True
                 txtMem22.Locked = True
                txtSub22.Locked = True
                RetDep.DependentPatientId = PatientId
                RetDep.DependentInsurerId = SecondInsurerId2
                RetDep.DependentPolicyHolderId = PolicyHolder2
                If (RetDep.FindPatientFinancialDependents > 0) Then
                    If (RetDep.SelectPatientFinancialDependents(1)) Then
                        DependentId(5) = RetDep.DependentId
                        txtMem22.Text = RetDep.DependentMember
                        txtSub22.Text = RetDep.DependentSubscriber
                    End If
                End If
            End If
        End If
        If (SecondInsurerId3 > 0) Then
            RetrieveInsurer.InsurerId = SecondInsurerId3
            If (RetrieveInsurer.RetrieveInsurer) Then
                lblIns23.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                 + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                 + Trim(RetrieveInsurer.InsurerGroupName)
                txtMem23.Enabled = True
                txtSub23.Enabled = True
                txtMem23.Locked = True
                txtSub23.Locked = True
                RetDep.DependentPatientId = PatientId
                RetDep.DependentInsurerId = FirstInsurerId1
                RetDep.DependentPolicyHolderId = PolicyHolder2
                If (RetDep.FindPatientFinancialDependents > 0) Then
                    If (RetDep.SelectPatientFinancialDependents(1)) Then
                        DependentId(6) = RetDep.DependentId
                        txtMem23.Text = RetDep.DependentMember
                        txtSub23.Text = RetDep.DependentSubscriber
                    End If
                End If
            End If
        End If
    End If
End If
Set RetDep = Nothing
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
End Function

Private Function GetActiveFinancial(PolicyHolderid As Long, FirstCurrent As Long, SecondCurrent As Long, ThirdCurrent As Long, InsType As String) As Boolean
Dim i As Integer
Dim FinRec As PatientFinance
Set FinRec = New PatientFinance
GetActiveFinancial = False
FirstCurrent = 0
SecondCurrent = 0
ThirdCurrent = 0
If (PolicyHolderid > 0) Then
    FinRec.PatientId = PolicyHolderid
    FinRec.PrimaryInsurerId = 0
    FinRec.financialid = 0
    FinRec.PrimaryStartDate = ""
    FinRec.PrimaryEndDate = ""
    FinRec.PrimaryInsType = InsType
    FinRec.FinancialStatus = "C"
    If (FinRec.FindPatientFinancial > 0) Then
        i = 1
        While (FinRec.SelectPatientFinancial(i))
            If (FinRec.PrimaryPIndicator = 1) Then
                FirstCurrent = FinRec.PrimaryInsurerId
            ElseIf (FinRec.PrimaryPIndicator = 2) Then
                SecondCurrent = FinRec.PrimaryInsurerId
            ElseIf (FinRec.PrimaryPIndicator = 3) Then
                ThirdCurrent = FinRec.PrimaryInsurerId
            End If
            i = i + 1
        Wend
        GetActiveFinancial = True
    End If
End If
Set FinRec = Nothing
End Function

Private Function verifyPost(IType As Integer) As Boolean
verifyPost = True
If (IType = 1) Then
    If (PatientId <> PolicyHolder1) Then
        If (Trim(txtMem11.Text) = "") And (Trim(txtMem12.Text) = "") And (Trim(txtMem13.Text) = "") And _
           (Trim(txtSub11.Text) = "") And (Trim(txtSub12.Text) = "") And (Trim(txtSub13.Text) = "") Then
            verifyPost = False
        End If
    End If
ElseIf (IType = 2) Then
    If (PatientId <> PolicyHolder2) Then
        If (Trim(txtMem21.Text) = "") And (Trim(txtMem22.Text) = "") And (Trim(txtMem23.Text) = "") And _
           (Trim(txtSub21.Text) = "") And (Trim(txtSub22.Text) = "") And (Trim(txtSub23.Text) = "") Then
            verifyPost = False
        End If
    End If
End If
If Not (verifyPost) Then
    frmEventMsgs.Header = "No Member Id is Present"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Continue"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        verifyPost = True
    End If
End If
End Function

Private Function DeleteDependent(Id As Long) As Boolean
Dim RetDep As PatientFinanceDependents
If (Id > 0) Then
    Set RetDep = New PatientFinanceDependents
    RetDep.DependentId = Id
    If (RetDep.RetrievePatientFinancialDependents) Then
        RetDep.DeletePatientFinancialDependents
    End If
    Set RetDep = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

