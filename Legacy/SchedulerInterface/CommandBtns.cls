VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommandBtns"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub DiagnosisSearch(lst As ListBox)
Dim TheDiscipline As String
Dim SearchString As String

start:
frmAlphaPad.NumPad_Field = "Diagnosis (ICD)"
frmAlphaPad.lblLegend.Caption = "LookUp Legend" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
                                "=O Ophthalmic Codes" & Chr(10) & Chr(13) & _
                                "=A Any Codes" & Chr(10) & Chr(13) & _
                                "=M My Codes" & Chr(10)
frmAlphaPad.lblLegend.Visible = True

frmAlphaPad.NumPad_Result = ""
frmAlphaPad.NumPad_Quit = False
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
lst.Clear

SearchString = Trim(frmAlphaPad.NumPad_Result)
If Len(SearchString) = 0 Or frmAlphaPad.NumPad_Quit Then Exit Sub

TheDiscipline = "OPHT"
If Mid(SearchString, 1, 1) = "=" Then
    If Len(SearchString) > 1 Then
        Select Case UCase(Mid(SearchString, 2, 1))
        Case "O"
        Case "A"
            TheDiscipline = ""
        Case "M"
            Call MySearch(lst, "MYDIAGNOSIS") '"MYPROCEDURES" "MYDIAGNOSIS"
            Exit Sub
        Case Else
            GoTo WrongSelection
        End Select
        SearchString = Trim(Mid(SearchString, 3))
    Else
        GoTo WrongSelection
    End If
End If

Dim ApplClaim As New ApplicationClaims
Call ApplClaim.LoadDiagnosis(lst, TheDiscipline, SearchString, True, True)
Set ApplClaim = Nothing
If Not lst.List(0) = "Close Diagnosis List" Then lst.AddItem "Close Diagnosis List", 0

Dim POS As Integer

If SearchString = "" Then
    POS = 1
Else
    POS = 10
End If

'Call FGCSort(lst, pos)
Call RSSort(lst, POS)
Exit Sub

WrongSelection:
frmEventMsgs.Header = "Invalid entry"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
GoTo start

End Sub

Private Sub MySearch(lst As ListBox, RefType As String)
Dim Offset As Integer
If RefType = "MYDIAGNOSIS" Then
    lst.AddItem "Close Diagnosis List"
    Offset = 10
Else
    lst.AddItem "Close Procedures List"
    Offset = 14
End If

Dim i As Integer
Dim DisplayText As String
Dim RetCode As New PracticeCodes
RetCode.ReferenceType = RefType
    If (RetCode.FindCode > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            DisplayText = Space(Offset)
            Mid(DisplayText, 1, Offset) = Left(RetCode.ReferenceAlternateCode, Offset)
            DisplayText = DisplayText + Trim(RetCode.ReferenceCode)
            lst.AddItem DisplayText
            i = i + 1
        Wend
    End If
Set RetCode = Nothing
End Sub


Public Sub ServiceSearch(lst As ListBox, lstSort As ListBox, _
ApptDate As String, PatientId As Long, ApptId As Long)

Dim InsrId As Long
Dim InsId As Long
Dim Copay As Single
Dim IType As String
Dim LocId As Long
Dim SearchString As String

start:
frmAlphaPad.NumPad_Field = "Procedures (CPT)"
frmAlphaPad.lblLegend.Caption = "LookUp Legend" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
                                "=A Any Codes" & Chr(10) & Chr(13) & _
                                "=M My Codes" & Chr(10)
frmAlphaPad.lblLegend.Visible = True
frmAlphaPad.NumPad_Result = ""
frmAlphaPad.NumPad_Quit = False
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1

lst.Clear


SearchString = Trim(frmAlphaPad.NumPad_Result)
If Len(SearchString) = 0 Or frmAlphaPad.NumPad_Quit Then Exit Sub
    
If Mid(SearchString, 1, 1) = "=" Then
    If Len(SearchString) > 1 Then
        Select Case UCase(Mid(SearchString, 2, 1))
        Case "A"
            SearchString = Trim(Mid(SearchString, 3))
        Case "M"
            Call MySearch(lst, "MYPROCEDURES") '"MYPROCEDURES" "MYDIAGNOSIS"
            Exit Sub
        Case Else
            GoTo WrongSelection
        End Select
    Else
        GoTo WrongSelection
    End If
End If
    
lst.AddItem "Close Procedures List"

Dim ApplList As New ApplicationAIList
Call ApplList.ApplGetInsurance(PatientId, ApptDate, InsrId, InsId, Copay, IType, True)
Set ApplList = Nothing

Dim RetApt As New SchedulerAppointment
RetApt.AppointmentId = ApptId
If (RetApt.RetrieveSchedulerAppointment) Then
    LocId = RetApt.AppointmentResourceId3
End If
Set RetApt = Nothing

Dim ApplClaim As New ApplicationClaims
Dim POS As Integer
Call ApplClaim.LoadProcedures(lst, lstSort, InsId, ApptDate, SearchString, LocId)
Set ApplClaim = Nothing
If (lst.ListCount > 1) Then
    If SearchString = "" Then
        POS = 1
    Else
        POS = 25
    End If
    Call RSSort(lst, POS)
Else
    frmEventMsgs.Header = "No matches found"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Exit Sub

WrongSelection:
frmEventMsgs.Header = "Invalid entry"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
GoTo start

End Sub

Public Sub More(ReceivableId As Long)
Dim ApptDate As String
Dim MedRef As String
Dim BillOff As Long
With frmAddInfo
    Dim RetrieveReceivables As New PatientReceivables
    RetrieveReceivables.ReceivableId = ReceivableId
    If (RetrieveReceivables.RetrievePatientReceivable) Then
            .AccidentType = RetrieveReceivables.ReceivableAccidentType
            .AccidentState = RetrieveReceivables.ReceivableAccidentState
            .AdmissionDate = RetrieveReceivables.ReceivableHospitalAdmission
            .DismissalDate = RetrieveReceivables.ReceivableHospitalDismissal
            .Disability = RetrieveReceivables.ReceivableDisability
            .RsnType = RetrieveReceivables.ReceivableRsnType
            .RsnDate = RetrieveReceivables.ReceivableRsnDate
            .ConsDate = RetrieveReceivables.ReceivableConsDate
            .PrevCond = RetrieveReceivables.ReceivablePrevCond
            .ExternalRefInfo = RetrieveReceivables.ReceivableExternalRefInfo
            ApptDate = RetrieveReceivables.ReceivableInvoiceDate
    End If
    Set RetrieveReceivables = Nothing
    Call .LoadAddInfo
    .Show 1

    Dim ApplClaim As New ApplicationClaims
    
    Dim SQL As String
    SQL = _
    "exec dbo.GetBillingOfficeForReceivable " & ReceivableId
    Dim RS As Recordset
    Set RS = GetRS(SQL)
    Do Until RS.EOF
        BillOff = RS("BillingOffice")
        Call ApplClaim.UpdateReceivable(RS("ReceivableId"), ApptDate, .AccidentType, .AccidentState, .AdmissionDate, _
                                .DismissalDate, .Disability, .RsnType, .RsnDate, .ConsDate, .PrevCond, _
                                .ExternalRefInfo, BillOff)
        RS.MoveNext
    Loop
        
    Set ApplClaim = Nothing
End With
End Sub

Public Sub PatientInfo(PatId As Long)
Dim PatientDemographics As New PatientDemographics
PatientDemographics.PatientId = PatId
Call PatientDemographics.DisplayPatientInfoScreen
Set PatientDemographics = Nothing
End Sub

Public Sub ClinicalInfo(PatId As Long, ApptId As Long)
    If (ApptId > 0) Then
        If (frmReviewAppts.LoadApptsList(PatId, ApptId, True)) Then
            frmReviewAppts.Show 1
        End If
    End If
End Sub

Public Sub InsuranceInfo(PatientId As Long, _
                         PolicyHolderid As Long, _
                         PolicyHolderIdRel As String, _
                         PolicyHolderIdBill As Boolean, _
                         SecondPolicyHolderId As Long, _
                         SecondPolicyHolderIdRel As String, _
                         SecondPolicyHolderIdBill As Boolean)

Call DisplayInsuranceScreen(PatientId)

End Sub

Public Sub DoctorsSearch(lst As ListBox, i As Integer)
Dim DisplayText As String
Dim k As Integer, n As Integer

lst.Clear
lst.AddItem "Close Doctors List", 0

Select Case i
Case 3
    lst.AddItem "Clear", 1
    GoTo Vendors
End Select


Dim RetDr As New SchedulerResource
Dim RS As ADODB.Recordset
Set RS = CreateAdoRecordset
Dim a() As String
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyDoctor) Then
    k = 1
    While (RetDr.SelectResource(k))
        'If RetDr.ResourceBillable Then
            DisplayText = Space(120)
            Mid(DisplayText, 1, 30) = Left(RetDr.ResourceName, 30)
            Mid(DisplayText, 101, 10) = Trim(Str(RetDr.ResourceId))
            n = n + 1
            ReDim Preserve a(n)
            a(n) = DisplayText
        'End If
        k = k + 1
    Wend
End If
Set RetDr = Nothing

'Call BubbleSort(a)
For n = 1 To UBound(a)
    If Not a(n) = a(n - 1) Then lst.AddItem a(n)
Next
Exit Sub

Vendors:
Dim TotalDocs As Integer
Dim TheDocs As New PracticeVendors

TheDocs.VendorType = "D"
TheDocs.VendorLastName = ""
TheDocs.VendorFirstName = ""
TotalDocs = TheDocs.FindVendorbyLastName
If (TotalDocs > 0) Then
    k = 1
    While (TheDocs.SelectVendor(k))
        DisplayText = Space(120)
        Mid(DisplayText, 1, 30) = Left(TheDocs.VendorLastName + " " + TheDocs.VendorFirstName, 29)
        Mid(DisplayText, 31, 35) = Left(TheDocs.VendorAddress, 34)
        Mid(DisplayText, 66, 25) = Left(TheDocs.VendorCity, 24)
        Mid(DisplayText, 91, 10) = Trim(TheDocs.VendorState)
        Mid(DisplayText, 101, 10) = Trim(Str(TheDocs.VendorId))
        lst.AddItem DisplayText
        k = k + 1
    Wend
End If
Set TheDocs = Nothing

End Sub

Public Sub Referral(lst As ListBox, txt As TextBox, PatientId As Long, TDate As String, AppointmentId As Long, CurrentRefId As Long)
    Dim i As Integer
    Dim ApplTbl As New ApplicationTables
    Set ApplTbl.lstBox = lst
    Call ApplTbl.ApplLoadReferrals(PatientId, TDate)
    Set ApplTbl = Nothing
    
    lst.RemoveItem (0)
    
    Dim RetApt As New SchedulerAppointment
    RetApt.AppointmentId = AppointmentId
    If (RetApt.RetrieveSchedulerAppointment) Then
        CurrentRefId = RetApt.AppointmentReferralId
        Call RetApt.ApplySchedulerAppointment
    End If
    Set RetApt = Nothing

    For i = 0 To lst.ListCount - 1
        lst.Selected(i) = (myTrim(lst.List(i), 1) = CStr(CurrentRefId))
    Next
    lst.AddItem "Close Referral List", 0
    lst.AddItem "Record Referral"
    
End Sub

Public Sub EditReferral(lst As ListBox, txt As TextBox, PatId As Long, ApptId As Long, LclRef As Long)
Dim i As Integer
If lst.ListIndex = lst.ListCount - 1 Then GoTo AddEdit

Dim RefId As Long
RefId = myTrim(lst.Text, 1)

With frmEventMsgs
    If Trim(Mid(lst.Text, 1, 17)) = txt.Text Then
        .Header = "Unset the Referral ?"
        .AcceptText = "UnSet"
    Else
        .Header = "Set the Referral ?"
        .AcceptText = "Set"
    End If
    .RejectText = "Edit"
    .CancelText = "Cancel"
    .Other0Text = ""
    .Other1Text = ""
    .Other2Text = ""
    .Other3Text = ""
    .Other4Text = ""
    .Show 1
            
    Select Case .Result
    Case 1
        Dim ReferId As Long
        If .AcceptText = "Set" Then
            ReferId = myTrim(lst.Text, 1)
            GoSub SetUnset
            txt.Text = Trim(Mid(lst.Text, 1, 17))
        Else
            GoSub SetUnset
            txt.Text = ""
        End If
        lst.Visible = False
        txt.SetFocus
    Case 2
        GoTo AddEdit
    Case 4
        For i = 1 To lst.ListCount - 2
            lst.Selected(i) = (Trim(Mid(lst.List(i), 1, 17)) = txt.Text)
        Next
        lst.Visible = False
        txt.SetFocus
    End Select
            
End With
Exit Sub

AddEdit:
Dim PatientDemographics As New PatientDemographics
Call PatientDemographics.DisplayReferralsScreen(ApptId, PatId, RefId)
Set PatientDemographics = Nothing
If myTrim(txt.Text) = myTrim(lst.Text) Then
                Dim a As String
    txt.Text = NewRef(PatId, RefId)
End If
lst.Visible = False
txt.SetFocus
Exit Sub

SetUnset:
Dim RetApt As New SchedulerAppointment
RetApt.AppointmentId = ApptId
If (RetApt.RetrieveSchedulerAppointment) Then
    RetApt.AppointmentReferralId = ReferId
    Call RetApt.ApplySchedulerAppointment
End If
Set RetApt = Nothing

Dim ApplSch As ApplicationScheduler
Set ApplSch = New ApplicationScheduler
If LclRef > 0 Then
'unset: LclRef = curr
    Call ApplSch.SetReferralCount(LclRef, PatId)
End If
'set: ReferId = new
Call ApplSch.SetReferralCount(ReferId, PatId)
Set ApplSch = Nothing

Return
End Sub

Public Sub PreCert(PatientId As Long, AppointmentId As Long)
Dim PreCText As String
Dim ThePre As Long
ThePre = 0
Dim ApplSch As New ApplicationScheduler
Call ApplSch.GetPreCert(PatientId, AppointmentId, ThePre, PreCText)
Set ApplSch = Nothing
If (ThePre < 1) Then
    ThePre = 0
End If
    
    'Call SetPreCert(AppointmentId, Prec)

Dim PreCertId As Long
Dim ReturnArguments As Object
Dim PatientDemographics As New PatientDemographics
Set ReturnArguments = PatientDemographics.DisplayPreAuthorizationScreen(AppointmentId, ThePre)
Set PatientDemographics = Nothing

If Not ReturnArguments Is Nothing Then
    PreCertId = ReturnArguments.AuthorizationId
End If

If PreCertId > 0 Then
    Set ApplSch = New ApplicationScheduler
    If (ApplSch.SetPreCert(AppointmentId, PreCertId)) Then
        'lblPreCert.Visible = True
    Else
        frmEventMsgs.Header = "Failed to set pre-cert"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
    Set ApplSch = Nothing
End If
End Sub

Public Sub Location(lst As ListBox)
Dim k As Integer
Dim DisplayText As String
lst.Clear
lst.AddItem "Close Locations List"

    Dim RetPrc As New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        k = 1
        While (RetPrc.SelectPractice(k))
            DisplayText = Space(100)
            If (Trim(RetPrc.PracticeLocationReference) <> "") Then
                Mid(DisplayText, 1, (Len(Trim(RetPrc.PracticeLocationReference)) + 9)) = Trim(RetPrc.PracticeLocationReference) + " - Office"
                DisplayText = DisplayText + Trim(Str(1000 + RetPrc.PracticeId))
            Else
                Mid(DisplayText, 1, 6) = "Office"
                DisplayText = DisplayText + Trim(Str(0))
            End If
            lst.AddItem DisplayText
            k = k + 1
        Wend
    End If
    Set RetPrc = Nothing


Dim TheDocs As New SchedulerResource
TheDocs.ResourceName = Chr(1)
If (TheDocs.FindResource > 0) Then
    k = 1
    While (TheDocs.SelectResource(k))
        DisplayText = Space(100)
        Mid(DisplayText, 1, Len(Trim(TheDocs.ResourceName))) = Trim(TheDocs.ResourceName)
        Mid(DisplayText, 17, Len(Trim(TheDocs.ResourceDescription))) = Trim(TheDocs.ResourceDescription)
        If (Trim(DisplayText) <> "") Then
            If (TheDocs.ResourceServiceCode = "02") Then
                DisplayText = DisplayText + Trim(Str(TheDocs.ResourceId))
                lst.AddItem DisplayText
            End If
        End If
        k = k + 1
    Wend
End If
Set TheDocs = Nothing

End Sub

Public Sub over90(lst As ListBox, over90() As String, ReceivableId As Long)
lst.Clear
lst.AddItem "Close List"

Dim i As Integer
For i = 0 To UBound(over90)
    lst.AddItem over90(i)
Next
Dim ApplList As New ApplicationAIList
lst.ListIndex = ApplList.ApplGet90Day(ReceivableId)
Set ApplList = Nothing
End Sub

Public Sub Notes(PatientId As Long, RcvId As Long, Optional SrvId As String = "", Optional chkClaim As Boolean = False)
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim IPlan As Long
Dim ApptId As Long
Dim MyEye As String
Dim Temp As String
Dim ADate As String, AName As String
Dim InvId As String
Dim i As Integer

InvId = ""
frmNotes.NoteId = 0
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = 0
frmNotes.CurrentAction = ""
frmNotes.SetTo = "B"
frmNotes.MaintainOn = UserLogin.HasPermission(epBillingNotes)
    
Set ApplList = New ApplicationAIList
ApptId = ApplList.ApplGetAppt(RcvId)
Set ApplList = Nothing
Set ApplTemp = New ApplicationTemplates
Call ApplTemp.ApplGetInvoice(RcvId, InvId, ADate, IPlan, ApptId)
Set ApplTemp = Nothing
frmNotes.EyeContext = ""
frmNotes.AppointmentId = ApptId
frmNotes.SystemReference = "R" + InvId

If Not SrvId = "" Then
    Dim RetrieveSrv As New PatientReceivableService
    RetrieveSrv.Invoice = InvId
    If (RetrieveSrv.FindPatientReceivableService > 0) Then
        i = 1
        Do While (RetrieveSrv.SelectPatientReceivableService(i))
            If (RetrieveSrv.ServiceStatus <> "X") Then
                If SrvId = Trim(RetrieveSrv.Service) Then
                    MyEye = Trim(RetrieveSrv.ServiceModifier)
                    Exit Do
                End If
            End If
            i = i + 1
        Loop
    End If
    Set RetrieveSrv = Nothing
    
    If (InStrPS(MyEye, "RT") > 0) Then
        MyEye = "OD"
    ElseIf (InStrPS(MyEye, "LT") > 0) Then
        MyEye = "OS"
    ElseIf (InStrPS(MyEye, "50") > 0) Then
        MyEye = "OU"
    Else
        MyEye = ""
    End If
    frmNotes.EyeContext = MyEye
    frmNotes.SystemReference = frmNotes.SystemReference + "C" + SrvId
End If

If (frmNotes.LoadNotes) Then
    frmNotes.chkClaim.Visible = chkClaim
    frmNotes.Show 1
End If

End Sub

Private Function NewRef(PatientId As Long, RefId As Long) As String
Dim fNewRef As String
Dim a As String, i As Integer, l As Long
Dim ApplTbl As New ApplicationTables
Call ApplTbl.ApplGetReferral(PatientId, RefId, fNewRef, i, i, a, a, a, l, l, l)
Set ApplTbl = Nothing

NewRef = fNewRef
End Function

Public Sub RSSort(ByRef lst As ListBox, Optional POS As Integer = 1)
Dim i As Long
Dim RS As ADODB.Recordset
Set RS = New ADODB.Recordset
With RS
    .ActiveConnection = Nothing
    .CursorLocation = adUseClient
    .LockType = adLockBatchOptimistic
    If POS = 1 Then
        .Fields.Append "col1", adChar, 1
    Else
        .Fields.Append "col1", adChar, POS - 1
    End If
    .Fields.Append "col2", adChar, 200
    .Open
    
    For i = 1 To lst.ListCount - 1
        .AddNew
        If POS > 1 Then
            .Fields("col1") = Mid(lst.List(i), 1, POS - 1)
        End If
        .Fields("col2") = Mid(lst.List(i), POS)
        .Update
    Next
    
    .Sort = "col2"
    .MoveFirst
    i = 0
    Do Until .EOF
        i = i + 1
        If POS = 1 Then
            lst.List(i) = .Fields("col2")
        Else
            lst.List(i) = .Fields("col1") & .Fields("col2")
        End If
        .MoveNext
    Loop
End With
Set RS = Nothing
End Sub

