VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmDisposition 
   BackColor       =   &H00785211&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00785211&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdERx 
      Height          =   870
      Left            =   6240
      TabIndex        =   35
      Top             =   6600
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":0000
   End
   Begin VB.ComboBox lstRef 
      BackColor       =   &H00946414&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      ItemData        =   "Disposition.frx":01E4
      Left            =   2160
      List            =   "Disposition.frx":01E6
      Style           =   2  'Dropdown List
      TabIndex        =   30
      Top             =   960
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.ComboBox lstInsType 
      BackColor       =   &H00946414&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "Disposition.frx":01E8
      Left            =   7920
      List            =   "Disposition.frx":01F8
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   3480
      Width           =   1935
   End
   Begin VB.ListBox lstPrint 
      Height          =   450
      Left            =   10920
      Sorted          =   -1  'True
      TabIndex        =   23
      Top             =   1440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox txtAmt 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      MaxLength       =   10
      TabIndex        =   1
      Top             =   2520
      Width           =   1575
   End
   Begin VB.ComboBox lstMethod 
      Appearance      =   0  'Flat
      BackColor       =   &H00946414&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "Disposition.frx":022E
      Left            =   9600
      List            =   "Disposition.frx":0230
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   2520
      Width           =   2295
   End
   Begin VB.TextBox txtPayRef 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      MaxLength       =   20
      TabIndex        =   3
      Top             =   3000
      Width           =   3975
   End
   Begin VB.ListBox lstActions 
      BackColor       =   &H00946414&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   6060
      ItemData        =   "Disposition.frx":0232
      Left            =   120
      List            =   "Disposition.frx":0234
      TabIndex        =   0
      Top             =   1440
      Width           =   5895
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   12
      Top             =   7800
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":0236
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFinancial 
      Height          =   990
      Left            =   9360
      TabIndex        =   6
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":0415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppts 
      Height          =   990
      Left            =   6120
      TabIndex        =   8
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":05FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClinical 
      Height          =   990
      Left            =   1200
      TabIndex        =   11
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":07E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   990
      Left            =   2520
      TabIndex        =   10
      Top             =   7800
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":09CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFinRes 
      Height          =   990
      Left            =   7560
      TabIndex        =   7
      Top             =   7800
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":0BAD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChgs 
      Height          =   990
      Left            =   3600
      TabIndex        =   9
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":0DA0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCalcMedicare 
      Height          =   750
      Left            =   9960
      TabIndex        =   5
      Top             =   4080
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":0F84
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDemo 
      Height          =   990
      Left            =   4800
      TabIndex        =   28
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":1178
   End
   Begin MSFlexGridLib.MSFlexGrid lstInfo 
      Height          =   1095
      Left            =   4800
      TabIndex        =   33
      Top             =   240
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   1931
      _Version        =   393216
      Rows            =   4
      Cols            =   6
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7885329
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      Enabled         =   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   0
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10800
      TabIndex        =   13
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disposition.frx":135F
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Check-out Screen for: "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   120
      TabIndex        =   34
      Top             =   120
      Width           =   2580
   End
   Begin VB.Label Label43 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Pharmacy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   6240
      TabIndex        =   32
      Top             =   6840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label lblInsurer2 
      BackColor       =   &H0077742D&
      Caption         =   "Insurer:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   270
      Left            =   6120
      TabIndex        =   31
      Top             =   600
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.Label lblAlert 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      Caption         =   "Alert ON"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   29
      Top             =   6120
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label lblInsurer1 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Insurer:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6120
      TabIndex        =   27
      Top             =   240
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.Label lblPatBal 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Balance:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   6840
      TabIndex        =   26
      Top             =   4440
      Width           =   945
   End
   Begin VB.Label lblInsType 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Insurance Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6105
      TabIndex        =   25
      Top             =   3480
      Width           =   1560
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   6240
      TabIndex        =   24
      Top             =   1920
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label lblAmt 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Amount:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6885
      TabIndex        =   22
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label lblMethod 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Method:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   9660
      TabIndex        =   21
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label lblBalance 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Balance:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6840
      TabIndex        =   20
      Top             =   4080
      Width           =   915
   End
   Begin VB.Label lblPayRef 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Payment Ref:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6345
      TabIndex        =   19
      Top             =   3000
      Width           =   1395
   End
   Begin VB.Label lblReferRef 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Ref:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6240
      TabIndex        =   18
      Top             =   5520
      Width           =   420
   End
   Begin VB.Label lblRefer 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Referral Remaining:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6240
      TabIndex        =   17
      Top             =   5040
      Width           =   2070
   End
   Begin VB.Label lblCopay 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Copay:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   10320
      TabIndex        =   16
      Top             =   480
      Visible         =   0   'False
      Width           =   750
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Disposition"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   15
      Top             =   960
      Width           =   1185
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   60
   End
End
Attribute VB_Name = "frmDisposition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ReferenceDate As String
Public ActivityId As Long
Public PatientId As Long
Public AppointmentId As Long
Public ReferralId As Long
Public InsType As String
Public CurrentAction As String

Private CLSeparateOrderIdOD As Long
Private CLSeparateInvoiceOD As Boolean
Private CLSeparateOrderIdOS As Long
Private CLSeparateInvoiceOS As Boolean
Private FirstTimeIn As Boolean
Private DirectOn As Boolean
Private TriggerOn As Boolean
Private BusinessLogicOn As Boolean
Private UserId As Long
Private ReceivableId As Long
Private FromPostCharges As Boolean
Private PaymentPosted As Boolean
Private PolicyHolderid As Long
Private PolicyHolderIdRel As String
Private PolicyHolderIdBill As Boolean
Private SecondPolicyHolderId As Long
Private SecondPolicyHolderIdRel As String
Private SecondPolicyHolderIdBill As Boolean
Private Const ChkDoneInProgress As Integer = 0
Private Const ChkDone As Integer = 1
Private Const ChkOut As Integer = 2

Private SelectedDrug As String
Private DrugList As Collection
Private PostBillingDiagnosis As Boolean

Private Sub cmdCalcMedicare_Click()
Dim Temp As String
Dim Amt As Single
Dim ApplList As ApplicationAIList
If (ReceivableId < 1) Then
    frmEventMsgs.Header = "Post Charges First."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    Set ApplList = New ApplicationAIList
    If (ApplList.ApplCalculateMedicareBalance(ReceivableId, Amt)) Then
        Call DisplayDollarAmount(Trim(Str(Amt)), Temp)
        lblPatBal.Caption = "Patient Balance: $" + Trim(Temp)
        lblPatBal.Visible = True
    End If
    Set ApplList = Nothing
End If
End Sub

Private Sub cmdChgs_Click()
Dim RcvId As Long
Dim BillOff As Long
Dim Amt As Single
Dim myAmt As String
Dim PType As String, PAmt As String, PRef As String
Dim InvId As String, Temp As String, TheDate As String
Dim ApplList As ApplicationAIList
Dim ApplClaim As ApplicationClaims
Dim ApplTemp As ApplicationTemplates
TheDate = ""
If (Trim(ReferenceDate) <> "") Then
    TheDate = ReferenceDate
Else
    Call FormatTodaysDate(TheDate, False)
End If
If (lstInsType.ListIndex >= 0) Then
    InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
End If
If (Trim(InsType) = "") Then
    InsType = "M"
End If
If Not Validate() Then Exit Sub
myAmt = txtAmt.Text
If (ReceivableId < 1) Then
    Set ApplList = New ApplicationAIList
    Set ApplClaim = New ApplicationClaims
    PAmt = ""
    PType = ""
    PRef = ""
    If (Trim(txtAmt.Text) <> "") Then
        PAmt = Trim(txtAmt.Text)
        PRef = Trim(txtPayRef.Text)
        If (lstMethod.ListIndex >= 0) Then
            PType = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
        ElseIf (Trim(PRef) <> "") Then
            PType = "K"
        Else
            PType = "C"
        End If
    End If
    Call ApplClaim.SetApptInsType(AppointmentId, InsType)
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetPatientBillingOffice(AppointmentId, BillOff)
    Set ApplTemp = Nothing
    ReceivableId = ApplList.ApplPostReceivable(AppointmentId, PatientId, ActivityId, TheDate, InsType, PAmt, PType, PRef, DirectOn, UserId, PaymentPosted, BillOff, BusinessLogicOn, True, 0)
    If (ReceivableId > 0) Then
        InvId = ApplList.FormatInvoiceNumber(PatientId, AppointmentId)
        lstPrint.Clear
        Call ApplClaim.ArrangeServicesOrder(InvId, lstPrint)
        RcvId = ReceivableId
        
        'Display .NET Edit and Bill Screen
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = PatientId
        Call PatientDemographics.DisplayInvoiceEditAndBillScreen(ReceivableId)
        
        ' covers the case where services are posted after a payment has been entered.
        Amt = ApplClaim.IsUnassignedPayments(ReceivableId)
        If (Amt > 0) Then
            Call ApplClaim.PostRemainingPaymentToOtherServices(RcvId, "", Trim(Str(Amt)), "", "", PatientId, "", 0, UserId)
            PaymentPosted = True
        End If
        Call LoadDispositionDisplay(False)
        PAmt = myAmt
        txtAmt.Text = PAmt
        txtPayRef.Text = PRef
        ReceivableId = RcvId
    Else
        frmEventMsgs.Header = "Posting Charges not available"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
    Set ApplList = Nothing
    Set ApplClaim = Nothing
Else
    frmEventMsgs.Header = "Posting Charges not available"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub WriteG8553Record(sInvoice As String, sServiceDate As String)
Dim oReceivableService As New PatientReceivableService
    On Error GoTo lWriteG8553Record_Error

    With oReceivableService
        .ItemId = 0
        .Invoice = sInvoice
        If .RetrievePatientReceivableService Then
            .Invoice = sInvoice
            .Service = "G8553"
            .ServiceModifier = ""
            .ServiceDate = sServiceDate
            .ServiceCharge = 0
            .ServiceFeeCharge = 0
            .ServiceTOS = ""
            .ServicePOS = ""
            .ServiceOrderDoc = False
            .ServiceConsultOn = False
            .ServiceQuantity = 1
            .ServiceECode = False
            .ServiceCreateDate = Format(Now, "yyyymmdd")
            .ServiceLinkedDiag = ""
            .ApplyPatientReceivableService
        End If
    End With
    Set oReceivableService = Nothing

    Exit Sub

lWriteG8553Record_Error:

    LogError "frmDisposition", "WriteG8553Record", Err, Err.Description
End Sub

Private Sub cmdClinical_Click()
lblPrint.Caption = "Loading Appointment Details"
lblPrint.Visible = True
DoEvents
frmReviewAppts.StartDate = ""
If (frmReviewAppts.LoadApptsList(PatientId, AppointmentId, True)) Then
    frmReviewAppts.Show 1
End If
lblPrint.Caption = "Print"
lblPrint.Visible = False
DoEvents
End Sub

Private Sub cmdDemo_Click()
Dim Temp As String
Dim PatName As String
Dim InsType As String
Dim InsTypeA As String
Dim Copay As String
Dim TheCopay As String
Dim l1 As String, l2 As String, l3 As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates

Dim PatientDemographics As New PatientDemographics
If (PatientId > 0) Then
    PatientDemographics.PatientId = PatientId
    If Not PatientDemographics.DisplayPatientInfoScreen Then Exit Sub
    PolicyHolderid = 0
    PolicyHolderIdRel = ""
    PolicyHolderIdBill = False
    SecondPolicyHolderId = 0
    SecondPolicyHolderIdRel = ""
    SecondPolicyHolderIdBill = False
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetPatientPolicyInfo(PatientId, PatName, PolicyHolderid, PolicyHolderIdRel, PolicyHolderIdBill, SecondPolicyHolderId, SecondPolicyHolderIdRel, SecondPolicyHolderIdBill)
    Call ApplList.ApplGetApptInsType(AppointmentId, InsType)

    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetAllPayers(PatientId, PatName, PolicyHolderid, PolicyHolderIdBill, PolicyHolderIdRel, SecondPolicyHolderId, SecondPolicyHolderIdBill, SecondPolicyHolderIdRel, InsTypeA, "", lstRef, l1, l2, l3, True, True, False, True)
    Set ApplTemp = Nothing
    lblInsurer1.Caption = "Primary:" + l1
    lblInsurer2.Caption = "Second:" + l2
    
    Copay = ""
    TheCopay = ""
    If (Trim(PolicyHolderid) > 0) Then
        TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, PolicyHolderid, Temp, InsType)))
        If (TheCopay = "-1") Then
            TheCopay = ""
        End If
    End If
    If (Trim(TheCopay) = "") Then
        If (Trim(SecondPolicyHolderId) > 0) Then
            TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, SecondPolicyHolderId, Temp, InsType)))
            If (TheCopay = "-1") Then
                TheCopay = "0"
            End If
        Else
            TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, 0, Temp, InsType)))
            If (TheCopay = "-1") Then
                TheCopay = "0"
            End If
        End If
    End If
    If (val(TheCopay) > 0) Then
        Call DisplayDollarAmount(TheCopay, Copay)
        lblCopay.Caption = "Copay: $" + Copay
    Else
        lblCopay.Caption = "Copay: None Required"
    End If
    Set ApplList = Nothing
End If
End Sub

Private Sub cmdDone_Click()
Dim PrimMedOn As Boolean
Dim z As Integer, i As Integer
Dim BillOff As Long, RcvId As Long
Dim IPlan As Long, ApptId As Long
Dim Srv As String, SrvItem As Long
Dim myAmt As String
Dim Mods As String
Dim TodaysDate As String, Temp1 As String
Dim ApptDate As String, InvId As String
Dim IDate As String, PType As String
Dim CloseDate As String, TheDate As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim ApplClaim As ApplicationClaims
Dim oDate As CIODateTime
PType = ""
Call FormatTodaysDate(TodaysDate, False)
TodaysDate = Mid(TodaysDate, 7, 4) + Mid(TodaysDate, 1, 2) + Mid(TodaysDate, 4, 2)
TheDate = ""
If (Trim(ReferenceDate) <> "") Then
    TheDate = ReferenceDate
Else
    Call FormatTodaysDate(TheDate, False)
End If
Set oDate = New CIODateTime
oDate.SetDateUnknown CheckConfigCollection("EDITWINDOW")
CloseDate = oDate.GetIODate
Set oDate = Nothing
ApptDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
If (lstInsType.ListIndex >= 0) Then
    InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
End If
If (Trim(InsType) = "") Then
    InsType = "M"
End If
PType = ""
If (Not FromPostCharges) Then
    If Not Validate() Then Exit Sub
    PType = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
End If
Set ApplClaim = New ApplicationClaims
Set ApplList = New ApplicationAIList
Set ApplTemp = New ApplicationTemplates
If (ReceivableId < 1) Then
    Call ApplTemp.ApplGetPatientBillingOffice(AppointmentId, BillOff)
    InvId = ApplList.FormatInvoiceNumber(PatientId, AppointmentId)
    If Not (ApplTemp.ApplIsInvoicePresent(InvId)) Then
        Call ApplClaim.SetApptInsType(AppointmentId, InsType)
        RcvId = ApplList.ApplPostReceivable(AppointmentId, PatientId, ActivityId, TheDate, InsType, txtAmt.Text, PType, txtPayRef.Text, DirectOn, UserId, PaymentPosted, BillOff, BusinessLogicOn, True, InvId)
    End If
    lstPrint.Clear
    Call ApplClaim.ArrangeServicesOrder(InvId, lstPrint)
Else
    RcvId = ReceivableId
    Call ApplTemp.ApplGetInvoice(RcvId, InvId, Temp1, IPlan, AppointmentId)
    If IsInvoiceeRx Then
        'WriteG8553Record InvId, Format(TheDate, "yyyymmdd")
    End If
    If (Trim(txtAmt.Text) <> "") Then
        If Not (PaymentPosted) Then
            Call ApplClaim.ApplGetServiceToSetPayment(RcvId, True, Trim(txtAmt.Text), Srv, Mods, SrvItem)
            PrimMedOn = ApplTemp.ApplIsInsurerMedicare(IPlan, False)
            If Not (PrimMedOn) Then
                PrimMedOn = ApplTemp.ApplIsInsurerMedicareNJ(IPlan, False)
            End If
            If Not (PrimMedOn) Then
                PrimMedOn = ApplTemp.ApplIsInsurerMedicarePA(IPlan)
            End If
            If Not (PrimMedOn) Then
                PrimMedOn = ApplTemp.ApplIsInsurerMedicareGHI(IPlan)
            End If
            myAmt = txtAmt.Text
            If (ApptDate < CloseDate) Then
                Call ApplList.PostClaimPaymentsExcludedByInsurer(PatientId, RcvId, TodaysDate, myAmt, PType, txtPayRef.Text, UserId)
                PaymentPosted = ApplList.PostClaimPayments(PatientId, RcvId, TodaysDate, myAmt, PType, txtPayRef.Text, UserId, True, PrimMedOn, True)
            Else
                Call ApplList.PostClaimPaymentsExcludedByInsurer(PatientId, RcvId, ApptDate, myAmt, PType, txtPayRef.Text, UserId)
                PaymentPosted = ApplList.PostClaimPayments(PatientId, RcvId, ApptDate, myAmt, PType, txtPayRef.Text, UserId, True, PrimMedOn, True)
            End If
            Call ApplList.ApplSetReceivableBalance(InvId, "", True)
        End If
        If (DirectOn) Then
            Call ApplTemp.BatchTransaction(RcvId, "I", "", "B", DirectOn, 0, False, False, False)
        End If
    End If
End If

Call VerifyActions("SEND A CONSULT")
Call VerifyActions("REFER PATIENT TO ANOTHER")
Call VerifyActions("SCHEDULE SURGERY")
Call VerifyActions("APPOINTMENT")
If (Trim(txtAmt.Text) <> "") And (Not FromPostCharges) Then
    frmEventMsgs.Header = "Print Receipt ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        lblPrint.Caption = "Printing In Progress"
        lblPrint.Visible = True
        DoEvents
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppt(RcvId)
        Set ApplList = Nothing
        
        Dim TelerikReporting As New Reporting
        Dim ParamArgs(0) As Variant
                
        ParamArgs(0) = Array("EncounterId", Str(ApptId))
        'Invoke PatientReceipt telerik Report.
        Call TelerikReporting.ViewReport("Patient Receipt", ParamArgs)
        Set ApplList = Nothing
        lblPrint.Visible = False
    End If
End If

'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Dim RetCon As New PracticeConfigureInterface
Dim RetTyp As New SchedulerAppointmentType
Dim RetrieveReceiv As New PatientReceivables

If RetCon.getFieldValue("ASC") = "T" Then
    If RetTyp.getAppTypeId(AppointmentId) = 1 Then
        If CanCreateSecondClaim(AppointmentId) Then
    
            Dim InvoiceId As String
            Dim DocId As Long
            Dim RoomId As Long
            Dim ApptTypeId As Long
            Dim PreCertId As Long
            
            ' appointment
            Dim RetApt As New SchedulerAppointment
            RetApt.AppointmentId = AppointmentId
            If RetApt.RetrieveSchedulerAppointment Then
                DocId = RetApt.AppointmentResourceId1
                RoomId = RetApt.AppointmentResourceId2
                ApptTypeId = RetApt.AppointmentTypeId
                ApptDate = RetApt.AppointmentDate
                InsType = RetApt.AppointmentInsType
                ReferralId = RetApt.AppointmentReferralId
                PreCertId = RetApt.AppointmentPreCertId
            End If
            Set RetApt = Nothing
            Dim ApptIdNew As Long
            ApptIdNew = ApplClaim.BuildAppointment(PatientId, DocId, RoomId, ApptTypeId, ApptDate, InsType, ReferralId, PreCertId, False, False, True)
            
            'receivable
            InvoiceId = ApplList.FormatInvoiceNumber(PatientId, ApptIdNew)
            Dim RetRecOrg As New PatientReceivables
            Dim RetrieveRec As New PatientReceivables
            RetRecOrg.ReceivableId = RcvId
            If RetRecOrg.RetrievePatientReceivable Then
                If Not RetRecOrg.ReceivableLastPayDate = "" Then
                    RetRecOrg.ReceivableLastPayDate = ""
                    RetRecOrg.ApplyPatientReceivable
                End If
                RetrieveRec.ReceivableId = 0
                If (RetrieveRec.RetrievePatientReceivable) Then
                    RetrieveRec.AppointmentId = ApptIdNew
                    RetrieveRec.ReceivableType = "I"
                    RetrieveRec.PatientId = PatientId
                    RetrieveRec.ReceivableInvoice = InvoiceId
                    RetrieveRec.ReceivableInsurerDesignation = "T"
                    RetrieveRec.ReceivableInvoiceDate = RetRecOrg.ReceivableInvoiceDate
                    RetrieveRec.InsuredId = RetRecOrg.InsuredId
                    RetrieveRec.InsurerId = RetRecOrg.InsurerId
                    RetrieveRec.ReceivableBillOffice = RetRecOrg.ReceivableBillOffice
                    RetrieveRec.ReceivableBillToDr = RetRecOrg.ReceivableBillToDr
                    RetrieveRec.ReceivableAmount = RetRecOrg.ReceivableAmount
                    RetrieveRec.ReceivableFeeAmount = RetRecOrg.ReceivableFeeAmount
                    RetrieveRec.ReceivableRefDr = RetRecOrg.ReceivableRefDr
                    RetrieveRec.ReceivableBalance = RetRecOrg.ReceivableBalance
                    RetrieveRec.ReceivableUnAllocated = RetRecOrg.ReceivableUnAllocated
                    RetrieveRec.ReceivableExternalRefInfo = RetRecOrg.ReceivableExternalRefInfo
                    RetrieveRec.ApplyPatientReceivable
                End If
            End If
            Set RetRecOrg = Nothing
            Set RetrieveRec = Nothing
            
            'Diagnosis
            Dim RetClinicalOrg As New PatientClinical
            Dim RetrieveClinical As New PatientClinical
            
            If RetClinicalOrg.getDiagnosisByRcvId(RcvId) Then
                i = 1
                Do While RetClinicalOrg.SelectPatientClinical(i)
                    RetrieveClinical.ClinicalId = 0
                    If (RetrieveClinical.RetrievePatientClinical) Then
                        RetrieveClinical.ClinicalType = "K"
                        RetrieveClinical.AppointmentId = ApptIdNew
                        RetrieveClinical.PatientId = PatientId
                        RetrieveClinical.EyeContext = ""
                        RetrieveClinical.ImageDescriptor = ""
                        RetrieveClinical.ImageInstructions = ""
                        RetrieveClinical.Findings = RetClinicalOrg.Findings
                        RetrieveClinical.Symptom = RetClinicalOrg.Symptom
                        Call RetrieveClinical.ApplyPatientClinical
                    End If
                    i = i + 1
                Loop
            End If
            Set RetClinicalOrg = Nothing
            Set RetrieveClinical = Nothing
            
            'BillingDiagnosis
            Set ApplList = New ApplicationAIList
            Call ApplList.CreateBillingDiagnosis(ApptIdNew, True)
            Call ApplList.CreateBillingDiagnosisIcd10(ApptIdNew, True)
            Set ApplList = Nothing
            
            'Services
            
            Dim RetSrvOrg As New PatientReceivableService
            Dim RetrieveService As New PatientReceivableService
            
            If RetSrvOrg.GetServiceByRcvId(RcvId) Then
                i = 1
                Do While RetSrvOrg.SelectPatientReceivableService(i)
                    RetrieveService.ItemId = 0
                    If (RetrieveService.RetrievePatientReceivableService) Then
                        RetrieveService.ServiceExternalRefInfo = RetSrvOrg.ServiceExternalRefInfo
                        RetrieveService.Invoice = InvoiceId
                        RetrieveService.Service = RetSrvOrg.Service
                        RetrieveService.ServiceDate = RetSrvOrg.ServiceDate
                        RetrieveService.ServiceQuantity = RetSrvOrg.ServiceQuantity
                        RetrieveService.ServiceCharge = RetSrvOrg.ServiceCharge
                        RetrieveService.ServiceFeeCharge = RetSrvOrg.ServiceFeeCharge
                        RetrieveService.ServiceTOS = RetSrvOrg.ServiceTOS
                        RetrieveService.ServicePOS = RetSrvOrg.ServicePOS
                        RetrieveService.ServicePOS = RetSrvOrg.ServicePOS
                        RetrieveService.ServiceModifier = RetSrvOrg.ServiceModifier
                        RetrieveService.ServiceLinkedDiag = RetSrvOrg.ServiceLinkedDiag
                        RetrieveService.ServiceOrder = RetSrvOrg.ServiceOrder
                        RetrieveService.ServiceOrderDoc = RetSrvOrg.ServiceOrderDoc
                        RetrieveService.ServiceConsultOn = RetSrvOrg.ServiceConsultOn
                        RetrieveService.ServiceCreateDate = RetSrvOrg.ServiceCreateDate
                        RetrieveService.ServiceExternalRefInfo = RetSrvOrg.ServiceExternalRefInfo
                        Call RetrieveService.ApplyPatientReceivableService
                    End If
                    i = i + 1
                Loop
            End If
            
            'cleanup
            Call RetSrvOrg.CleanUpServices(InvId)
            
                   
            
            Set RetSrvOrg = Nothing
            Set RetrieveService = Nothing
        Else
            RetrieveReceiv.ReceivableId = RcvId
            If RetrieveReceiv.RetrievePatientReceivable Then
                If Not RetrieveReceiv.ReceivableLastPayDate = "" Then
                    RetrieveReceiv.ReceivableLastPayDate = ""
                    RetrieveReceiv.ApplyPatientReceivable
                End If
            End If
        End If
    Else
        RetrieveReceiv.ReceivableId = RcvId
        If RetrieveReceiv.RetrievePatientReceivable Then
            If Not RetrieveReceiv.ReceivableLastPayDate = "" Then
                RetrieveReceiv.ReceivableLastPayDate = ""
                RetrieveReceiv.ApplyPatientReceivable
            End If
        End If
    End If
End If

Set RetCon = Nothing
Set RetTyp = Nothing
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


Set ApplList = Nothing
Set ApplTemp = Nothing
Set ApplClaim = Nothing

Dim RCPManager As New ClinicalIntegration.frmRCPServices
RCPManager.jsonNumber = "07"
RCPManager.ExternalId = CStr(AppointmentId)
RCPManager.ResourceId = CStr(UserLogin.iId)
RCPManager.jsonDirectoryPath = CStr(PinPointDirectory)
RCPManager.DBObject = IdbConnection
RCPManager.showDialog

If (Not FromPostCharges) Then
    CurrentAction = ""
    Unload frmDisposition
End If
 
End Sub

Private Function CheckeRxOutput(fReset As Boolean) As String
Dim iCount As Integer
Dim iPosition As Integer
Dim sFirstPart As String
Dim sSecondPart As String
Dim oClinical As New PatientClinical
    
    On Error GoTo lCheckeRxOutput_Error

    With oClinical
        .ClinicalType = "A"
        .AppointmentId = AppointmentId
        .PatientId = PatientId
        .Symptom = ""
        .Findings = ""
        .EyeContext = ""
        .ImageDescriptor = ""
        .ImageInstructions = ""
        If .FindPatientClinical > 0 Then
            iCount = 1
            While .SelectPatientClinical(iCount)
                iPosition = InStrPS(1, .Findings, "-9/")
                If iPosition > 0 Then
                    If InStrPS(1, .Findings, "RX-8/") Then
                        CheckeRxOutput = Mid$(.Findings, iPosition - 1, 1)
                        If .ImageInstructions = "" Then
                            If CheckeRxOutput = "9" Then
                                CheckeRxOutput = ""
                            End If
                        End If
                        If fReset Then
                            ' Remove "x-9/" where x is anything.
                            sFirstPart = Mid$(.Findings, 1, iPosition - 2)
                            sSecondPart = Mid$(.Findings, iPosition + 3)
                            .Findings = sFirstPart & sSecondPart
                            .ApplyPatientClinical
                        End If
                    End If
                End If
                iCount = iCount + 1
            Wend
        End If
    End With
    Set oClinical = Nothing
    Exit Function

lCheckeRxOutput_Error:

    LogError "frmDisposition", "CheckeRxOutput", Err, Err.Description, , PatientId, AppointmentId

End Function

Private Sub cmdERx_Click()
Dim clsERX As New CeRx
    If UserLogin.HasPermission(EPermissions.epeRx) Then
        With clsERX
            .strPatientID = PatientId
            .strAppointmentId = AppointmentId
            .ComposeeRx
        End With
    Else
        frmEventMsgs.InfoMessage "Not permissioned for eRx"
        frmEventMsgs.Show 1
    End If
    Set clsERX = Nothing
End Sub

Private Sub cmdFinRes_Click()
Dim InsTypeA As String
Dim Temp As String, Copay As String
Dim l1 As String, l2 As String, l3 As String
Dim TheCopay As String, InsType As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim IsDisplayed As Boolean
Dim RetPatient As Patient
If (PatientId > 0) Then
    InsType = "M"
    If (lstInsType.ListIndex >= 0) Then
        InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    End If
    If (PolicyHolderid < 1) Then
        PolicyHolderid = PatientId
    End If
    
    IsDisplayed = DisplayInsuranceScreen(PatientId)
    
    If IsDisplayed Then
        Set RetPatient = New Patient
        RetPatient.PatientId = PatientId
        
        If RetPatient.RetrievePatient Then
            If (SecondPolicyHolderId <> RetPatient.SecondPolicyPatientId) Or _
               (SecondPolicyHolderIdRel <> RetPatient.SecondRelationship And RetPatient.SecondRelationship <> "") Or _
               (SecondPolicyHolderIdBill <> RetPatient.SecondBillParty) Then
                SecondPolicyHolderId = RetPatient.SecondPolicyPatientId
                SecondPolicyHolderIdRel = RetPatient.SecondRelationship
                SecondPolicyHolderIdBill = RetPatient.SecondBillParty
            End If
            If (PolicyHolderid <> RetPatient.PolicyPatientId) Or _
               (PolicyHolderIdRel <> RetPatient.Relationship And RetPatient.Relationship <> "") Or _
               (PolicyHolderIdBill <> RetPatient.BillParty) Then
                PolicyHolderid = RetPatient.PolicyPatientId
                PolicyHolderIdRel = RetPatient.Relationship
                PolicyHolderIdBill = RetPatient.BillParty
            End If
            Copay = ""
            TheCopay = ""
            Set ApplList = New ApplicationAIList
            If (Trim(PolicyHolderid) > 0) Then
                TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, PolicyHolderid, Temp, InsType)))
                If (TheCopay = "-1") Then
                    TheCopay = ""
                End If
            End If
            If (Trim(TheCopay) = "") Then
                If (Trim(SecondPolicyHolderId) > 0) Then
                    TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, SecondPolicyHolderId, Temp, InsType)))
                    If (TheCopay = "-1") Then
                        TheCopay = "0"
                    End If
                Else
                    TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, 0, Temp, InsType)))
                    If (TheCopay = "-1") Then
                        TheCopay = "0"
                    End If
                End If
            End If
            Set ApplList = Nothing
            
            Set ApplTemp = New ApplicationTemplates
            Call ApplTemp.ApplGetAllPayers(PatientId, Temp, PolicyHolderid, PolicyHolderIdBill, PolicyHolderIdRel, SecondPolicyHolderId, SecondPolicyHolderIdBill, SecondPolicyHolderIdRel, InsTypeA, "", lstRef, l1, l2, l3, True, True, False, True)
            lblInsurer1.Caption = "Primary:" + l1
            lblInsurer2.Caption = "Second:" + l2
            Set ApplTemp = Nothing
            
            If (val(TheCopay) > 0) Then
                Call DisplayDollarAmount(TheCopay, Copay)
                lblCopay.Caption = "Copay: $" + Copay
            Else
                lblCopay.Caption = "Copay: None Required"
            End If
            lblAmt.Visible = True
            txtAmt.Visible = True
            lblPayRef.Visible = True
            txtPayRef.Visible = True
            lblMethod.Visible = True
            lstMethod.Visible = True
            Set ApplList = Nothing
        End If
        Set RetPatient = Nothing
    End If
End If
End Sub

Private Sub cmdHome_Click()
Dim ApplList As ApplicationAIList
Set ApplList = New ApplicationAIList
If (ReceivableId > 0) Then
    Call VerifyActions("SEND A CONSULT")
    Call VerifyActions("REFER PATIENT TO ANOTHER")
    Call VerifyActions("SCHEDULE SURGERY")
    Call VerifyActions("APPOINTMENT")
    Call ApplList.SetActivityDoneInProgress(ActivityId, ChkDone, UserLogin.iId)
Else
    CurrentAction = "Home"
    Call ApplList.SetActivityDoneInProgress(ActivityId, ChkOut, UserLogin.iId)
End If
Set ApplList = Nothing
Unload frmDisposition
End Sub

Private Sub cmdRx_Click()
Dim ApplTemp As ApplicationTemplates
If (lstActions.ListIndex >= 0) Then
    If (UCase(Left(lstActions.List(lstActions.ListIndex), 2)) = "RX") Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBoxA = lstActions
        Set ApplTemp.lstBox = lstPrint
        lblPrint.Visible = True
        Call ApplTemp.ApplApptDrugPrint(lstActions.ListIndex, PatientId, AppointmentId)
        lblPrint.Visible = False
        Set ApplTemp = Nothing
    ElseIf (UCase(Left(lstActions.List(lstActions.ListIndex), 8)) = "DISPENSE") Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBoxA = lstActions
        Set ApplTemp.lstBox = lstPrint
        lblPrint.Visible = True
        If (InStrPS(UCase(lstActions.List(lstActions.ListIndex)), "SPECTACLE") <> 0) Then
            Call ApplTemp.ApplApptEyeWearPrint(lstActions.ListIndex, PatientId, AppointmentId, False, lstActions.ItemData(lstActions.ListIndex))
        Else
            Call ApplTemp.ApplApptEyeWearPrint(lstActions.ListIndex, PatientId, AppointmentId, True, lstActions.ItemData(lstActions.ListIndex))
        End If
        lblPrint.Visible = False
        Set ApplTemp = Nothing
    ElseIf (UCase(Left(lstActions.List(lstActions.ListIndex), 7)) = "WRITTEN") Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBoxA = lstActions
        Set ApplTemp.lstBox = lstPrint
        lblPrint.Visible = True
        If (InStrPS(lstActions.List(lstActions.ListIndex + 2), "-INSTRUCTIONS") > 0) Then
            Call ApplTemp.ApplWrittenInstructionsPrint(lstActions.ListIndex + 2)
        ElseIf (InStrPS(lstActions.List(lstActions.ListIndex + 1), "-INSTRUCTIONS") > 0) Then
            Call ApplTemp.ApplWrittenInstructionsPrint(lstActions.ListIndex + 1)
        End If
        lblPrint.Visible = False
        Set ApplTemp = Nothing
    Else
        frmEventMsgs.Header = "Select an RX or Written Instructions"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    frmEventMsgs.Header = "Select an RX or Written Instructions"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdAppts_Click()
If PatientId > 0 Then
    Dim arguments() As Variant
    Dim PatientInfoComWrapper As New comWrapper
    Call PatientInfoComWrapper.Create(PatientInfoLoadArgumentsType, emptyArgs)
    
    Dim loadArguments As Object
    Set loadArguments = PatientInfoComWrapper.Instance
    loadArguments.PatientId = PatientId
    loadArguments.TabToOpenInt = 0
    arguments = Array(loadArguments)
    
    Call PatientInfoComWrapper.Create(PatientInfoViewManagerType, emptyArgs)
    Call PatientInfoComWrapper.InvokeMethod("ShowPatientInfo", arguments)
End If
End Sub

Private Sub cmdFinancial_Click()
Dim PatientDemographics As New PatientDemographics
PatientDemographics.PatientId = PatientId
Call PatientDemographics.DisplayPatientFinancialScreen
Set PatientDemographics = Nothing
End Sub

Private Sub Form_Activate()
If (lblAlert.Visible) Then
    If (FirstTimeIn) Then
        Call lblAlert_Click
        FirstTimeIn = False
    End If
End If
End Sub

Private Sub Form_Load()
FirstTimeIn = True
If UserLogin.HasPermission(epPowerUser) Then
    frmDisposition.BorderStyle = 1
    frmDisposition.ClipControls = True
    frmDisposition.Caption = Mid(frmDisposition.Name, 4, Len(frmDisposition.Name) - 3)
    frmDisposition.AutoRedraw = True
    frmDisposition.Refresh
End If
End Sub



Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatCheckout, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatCheckout)) Then
        lblAlert.Caption = "Chk Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub lstInsType_Click()
Dim i As Integer
Dim OrgInsType As String
Dim TheCopay As String, Copay As String, Temp As String
Dim ApplList As ApplicationAIList
If Not (TriggerOn) Then
    If (lstInsType.ListIndex >= 0) Then
        OrgInsType = InsType
        InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
        Copay = ""
        Set ApplList = New ApplicationAIList
        TheCopay = ""
        If (Trim(PolicyHolderid) > 0) Then
            TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, PolicyHolderid, Temp, InsType)))
            If (TheCopay = "-1") Then
                TheCopay = ""
            End If
        End If
        If (Trim(TheCopay) = "") Or (Temp = "") Then
            If (Trim(SecondPolicyHolderId) > 0) Then
                TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, SecondPolicyHolderId, Temp, InsType)))
                If (TheCopay = "-1") Then
                    TheCopay = "0"
                End If
            Else
                TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, 0, Temp, InsType)))
                If (TheCopay = "-1") Then
                    TheCopay = "0"
                End If
            End If
        End If
        Set ApplList = Nothing
        If (Trim(Temp) = "") Then
            frmEventMsgs.Header = "No Insurers with that type"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            InsType = OrgInsType
            For i = 0 To lstInsType.ListCount
                If (Left(lstInsType.List(i), 1) = InsType) Then
                    lstInsType.ListIndex = i
                    Exit For
                End If
            Next i
        End If
    End If
End If
End Sub

Private Sub txtAmt_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 10) And (KeyAscii <> 13) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
Else
    KeyAscii = 0
End If
End Sub

Public Function LoadDispositionDisplay(Init As Boolean) As Boolean
On Error GoTo UI_ErrorHandler
Dim PlnId As Long, ReferDr As Long
Dim i As Integer, VisitsLeft As Integer
Dim b1 As Boolean
Dim TrialsOn As Boolean
Dim CPay As Single
Dim CMeth As String, CRef As String
Dim Balance As Single, PBal As Single, IBal As Single
Dim BalText As String, Temp As String, Copay As String
Dim ReferName As String
Dim TheCopay As String, ADate As String
Dim RefNum As String, PatName As String
Dim a1 As String, a2 As String, a3 As String
Dim a4 As String, a5 As String
Dim InsTypeA As String
Dim l1 As String, l2 As String, l3 As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim ERxOn As Boolean
LoadDispositionDisplay = False
TriggerOn = False
CLSeparateOrderIdOD = 0
CLSeparateInvoiceOD = False
CLSeparateOrderIdOS = 0
CLSeparateInvoiceOS = False
TrialsOn = CheckConfigCollection("INVENTORYON") = "T"
ADate = ""
Call FormatTodaysDate(ADate, False)
If (Init) Then
    ReceivableId = 0
    DirectOn = CheckConfigCollection("DIRECTQUEUE") = "T"
    BusinessLogicOn = CheckConfigCollection("BUSLOGON") = "T"
    UserId = UserLogin.iId
    PaymentPosted = False
End If
FromPostCharges = False
Set ApplTemp = New ApplicationTemplates
Set ApplList = New ApplicationAIList
If (lstMethod.ListIndex < 0) Then
    Call ApplList.ApplGetCodes("PAYABLETYPE", True, lstMethod)
End If
PolicyHolderid = 0
PolicyHolderIdRel = ""
PolicyHolderIdBill = False
SecondPolicyHolderId = 0
SecondPolicyHolderIdRel = ""
SecondPolicyHolderIdBill = False
Call ApplList.ApplGetPatientPolicyInfo(PatientId, PatName, PolicyHolderid, PolicyHolderIdRel, PolicyHolderIdBill, SecondPolicyHolderId, SecondPolicyHolderIdRel, SecondPolicyHolderIdBill)
Label1.Caption = Trim(PatName)
Set ApplList.ApplList = lstActions
Call ApplList.ApplGetClinicalActions(AppointmentId, PatientId, ADate, TrialsOn, ERxOn)
cmdERx.Enabled = ERxOn
'ErxCERT
cmdERx.Enabled = True
lblRefer.Visible = False
lblReferRef.Visible = False
If (ReferralId < 1) Then
    Call ApplList.ApplGetAppointmentInfo(AppointmentId, PatientId, a1, a2, a3, a4, ReferralId, b1, a5)
End If
If (ReferralId > 0) Then
    If (ApplList.ApplGetReferralInfo(ReferralId, PatientId, RefNum, VisitsLeft, ReferDr)) Then
        ReferName = ""
        If (ReferDr > 0) Then
            Call ApplList.ApplGetExternalDoctor(ReferDr, ReferName, False)
        End If
        lblRefer.Caption = "Referral Remaining: " + Trim(Str(VisitsLeft))
        lblRefer.Visible = True
        If (Trim(ReferName) <> "") Then
            lblReferRef.Caption = "Referral #: " + Trim(RefNum) + " from " + Trim(ReferName)
        Else
            lblReferRef.Caption = "Referral #: " + Trim(RefNum)
        End If
        lblReferRef.Visible = True
    End If
End If
InsType = a4
If (a4 = "") Then
    Call ApplList.ApplGetApptInsType(AppointmentId, InsType)
End If
Copay = ""
TheCopay = ""
If (Trim(PolicyHolderid) > 0) Then
    TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, PolicyHolderid, Temp, InsType)))
    If (TheCopay = "-1") Then
        TheCopay = ""
    End If
End If
If (Trim(TheCopay) = "") Or (Temp = "") Then
    If (Trim(SecondPolicyHolderId) > 0) Then
        TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, SecondPolicyHolderId, Temp, InsType)))
        If (TheCopay = "-1") Then
            TheCopay = "0"
        End If
    Else
        TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatientId, 0, Temp, InsType)))
        If (TheCopay = "-1") Then
            TheCopay = "0"
        End If
    End If
End If
If (val(TheCopay) > 0) Then
    Call DisplayDollarAmount(TheCopay, Copay)
    lblCopay.Caption = "Copay: $" + Copay
Else
    lblCopay.Caption = "Copay: None Required"
End If

Call ApplTemp.ApplGetAllPayers(PatientId, PatName, PolicyHolderid, PolicyHolderIdBill, PolicyHolderIdRel, SecondPolicyHolderId, SecondPolicyHolderIdBill, SecondPolicyHolderIdRel, InsTypeA, "", lstRef, l1, l2, l3, True, True, False, True)
lblInsurer1.Caption = "Primary:" + Trim(l1)
lblInsurer2.Caption = "Second:" + Trim(l2)
    
TriggerOn = True
For i = 0 To lstInsType.ListCount - 1
    If (Left(lstInsType.List(i), 1) = InsType) Then
        lstInsType.ListIndex = i
        Exit For
    End If
Next i
TriggerOn = False

If (lstInsType.ListIndex < 0) Then
    lstInsType.ListIndex = 0
End If
Call ApplList.ApplGetActivityCopay(AppointmentId, CPay, CMeth, CRef)
lblAmt.Visible = True
txtAmt.Text = ""
If (CPay <> 0) Then
    txtAmt.Text = Trim(Str(CPay))
End If
txtAmt.Visible = True
lblPayRef.Visible = True
txtPayRef.Text = CRef
txtPayRef.Visible = True
If (Trim(CMeth) <> "") Then
    For i = 0 To lstMethod.ListCount - 1
        If (CMeth = Mid(lstMethod.List(i), 40, 1)) Then
            lstMethod.ListIndex = i
            Exit For
        End If
    Next i
End If
lblMethod.Visible = True
lstMethod.Visible = True
'PBal = ApplList.ApplGetBalancebyPatient(PatientId)
Call ApplList.ApplGetAllBalancebyPatient(PatientId, PBal, 0, True)
Call DisplayDollarAmount(Trim(Str(PBal)), BalText)
lblBalance.Caption = "Patient Balance: $ " + BalText
lblPatBal.Visible = False
Set ApplTemp = New ApplicationTemplates
PlnId = ApplTemp.ApplGetPatientPrimaryPlanId(PatientId)
If (PlnId > 0) Then
    cmdCalcMedicare.Visible = ApplTemp.ApplIsInsurerMedicare(PlnId, False)
    If (PatientId <> PolicyHolderid) Then
        If Not (ApplTemp.ApplIsPlanDependentAllowed(PlnId)) Then
            cmdCalcMedicare.Visible = False
        End If
    End If
End If
If (IsAlertPresent(PatientId, EAlertType.eatCheckout)) Then
    lblAlert.Caption = "Chk Alert ON"
    lblAlert.Visible = True
End If
Set ApplList = Nothing
Set ApplTemp = Nothing
LoadDispositionDisplay = True
'-------------------------
    Dim lst As New Collection
    Set lst = GetInsLst(PatientId, Format(Now, "yyyymmdd"), True)
    Dim l As Integer
    Dim IName As String
    Dim IType As String
    Dim ICopay As String
    
    Dim TM(3) As String
    Dim tV(3) As String
    Dim tA(3) As String
    TM(0) = 0
    tV(0) = 0
    tA(0) = 0
    For l = 1 To lst.Count
        IName = Trim(Mid(lst(l), 1, 76))
        IType = Trim(Mid(lst(l), 77, 1))
        ICopay = Format(myTrim(lst(l), 1), "##0.00")
        Select Case IType
        Case "M"
            TM(0) = CInt(TM(0)) + 1
            If CInt(TM(0)) < 4 Then
                TM(CInt(TM(0))) = IName & vbTab & ICopay
            End If
        Case "V"
            tV(0) = CInt(tV(0)) + 1
            If CInt(tV(0)) < 4 Then
                tV(CInt(tV(0))) = IName & vbTab & ICopay
            End If
        Case "A"
            tA(0) = CInt(tA(0)) + 1
            If CInt(tA(0)) < 4 Then
                tA(CInt(tA(0))) = IName & vbTab & ICopay
            End If
        End Select
    Next
    
    Dim disp As String
    lstInfo.FormatString = "Major Med|CoPay|Vision|CoPay|Ambulatory|CoPay"
    lstInfo.Rows = 1
    For l = 1 To 3
        If TM(l) = "" Then TM(l) = vbTab
        If tV(l) = "" Then tV(l) = vbTab
        If tA(l) = "" Then tA(l) = vbTab
        disp = TM(l) & vbTab & tV(l) & vbTab & tA(l)
        If Not disp = vbTab & vbTab & vbTab & vbTab & vbTab Then
            lstInfo.AddItem disp
        End If
    Next
    lstInfo.ColWidth(0) = 2400
    lstInfo.ColWidth(2) = 1500
    lstInfo.ColWidth(4) = lstInfo.ColWidth(2)
    lstInfo.ColWidth(1) = 600
    lstInfo.ColWidth(3) = lstInfo.ColWidth(1)
    lstInfo.ColWidth(5) = lstInfo.ColWidth(1)
    lstInfo.Width = lstInfo.ColWidth(0) + lstInfo.ColWidth(2) + lstInfo.ColWidth(4) + _
                    (lstInfo.ColWidth(1) * 3)
    lstInfo.Left = txtPayRef.Left + txtPayRef.Width - lstInfo.Width

Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub lstActions_Click()
Dim Ref As Integer
Dim u As Integer, k As Integer
Dim ProceedOn As Boolean
Dim Temp As String
Dim ACom As String, ARv As String
Dim ARef As String, ARem As String
Dim TDate As String, NDate As String
Dim LocalDate As ManagedDate
Dim NumDays As Integer
Dim ApplList As ApplicationAIList
Dim RcvId As Long
Dim BillOff As Long
Dim InvId As String
Dim TheDate As String
Dim PType As String, PAmt As String, PRef As String
Dim ApplClaim As ApplicationClaims
Dim ApplTemp As ApplicationTemplates
Dim RetSCln As PatientClinicalSurgeryPlan

If (lstActions.ListIndex >= 0) Then
    If (InStrPS(UCase(lstActions.List(lstActions.ListIndex)), "SCHEDULE APP") <> 0) Then
        Call DisplayAppointmentSearchScreen(False)
    ElseIf (InStrPS(UCase(lstActions.List(lstActions.ListIndex)), "SCHEDULE SURGERY") <> 0) Then
        Call DisplayAppointmentSearchScreen(True)
    ElseIf ((InStrPS(UCase(lstActions.List(lstActions.ListIndex)), "DISPENSE CL") <> 0) _
         Or (InStrPS(UCase(lstActions.List(lstActions.ListIndex)), "ORDER TRIAL") <> 0)) Then
        ProceedOn = CheckConfigCollection("INVENTORYON") = "T"
        If (ProceedOn) Then
' gotta save the receivable to allow for Joined.
            TheDate = ""
            If (Trim(ReferenceDate) <> "") Then
                TheDate = ReferenceDate
            Else
                Call FormatTodaysDate(TheDate, False)
            End If
            If (lstInsType.ListIndex >= 0) Then
                InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
            End If
            If (Trim(InsType) = "") Then
                InsType = "M"
            End If
            If (ReceivableId < 1) Then
                Set ApplList = New ApplicationAIList
                Set ApplClaim = New ApplicationClaims
                PAmt = ""
                PType = ""
                PRef = ""
                If (Trim(txtAmt.Text) <> "") Then
                    PAmt = Trim(txtAmt.Text)
                    PRef = Trim(txtPayRef.Text)
                    If (lstMethod.ListIndex >= 0) Then
                        PType = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
                    ElseIf (Trim(PRef) <> "") Then
                        PType = "K"
                    Else
                        PType = "C"
                    End If
                End If
                Call ApplClaim.SetApptInsType(AppointmentId, InsType)
                Set ApplTemp = New ApplicationTemplates
                Call ApplTemp.ApplGetPatientBillingOffice(AppointmentId, BillOff)
                Set ApplTemp = Nothing
                ReceivableId = ApplList.ApplPostReceivable(AppointmentId, PatientId, ActivityId, TheDate, InsType, PAmt, PType, PRef, DirectOn, UserId, PaymentPosted, BillOff, BusinessLogicOn, True, 0)
                If (ReceivableId > 0) Then
                    InvId = ApplList.FormatInvoiceNumber(PatientId, AppointmentId)
                    lstPrint.Clear
                    Call ApplClaim.ArrangeServicesOrder(InvId, lstPrint)
                End If
            End If
            CLSeparateOrderIdOD = 0
            CLSeparateInvoiceOD = False
            CLSeparateOrderIdOS = 0
            CLSeparateInvoiceOS = False
            If ((Trim(lstActions.List(lstActions.ListIndex + 6)) <> "-") And (Trim(lstActions.List(lstActions.ListIndex + 6)) <> "") And (InStrPS(lstActions.List(lstActions.ListIndex + 6), "Order OD") > 0)) Or _
               ((Trim(lstActions.List(lstActions.ListIndex + 7)) <> "-") And (Trim(lstActions.List(lstActions.ListIndex + 7)) <> "") And (InStrPS(lstActions.List(lstActions.ListIndex + 7), "Order OS") > 0)) Then
                frmCLOrder.PatientId = PatientId
                frmCLOrder.AppointmentId = AppointmentId
                frmCLOrder.OrderIdOD = 0
                frmCLOrder.OrderIdOS = 0
                frmCLOrder.InventoryIdOD = Trim(lstActions.ItemData(lstActions.ListIndex + 6))
                frmCLOrder.InventoryIdOS = Trim(lstActions.ItemData(lstActions.ListIndex + 7))
                frmCLOrder.ClnId = Trim(lstActions.ItemData(lstActions.ListIndex))
                frmCLOrder.Show 1
                If (frmCLOrder.OrderIdOD > 0) Then
                    CLSeparateOrderIdOD = frmCLOrder.OrderIdOD
                End If
                If (frmCLOrder.OrderIdOS > 0) Then
                    CLSeparateOrderIdOS = frmCLOrder.OrderIdOS
                End If
            End If
        End If
    End If
End If
End Sub

Private Sub VerifyActions(ActionType As String)
Dim DoctorId As Long
Dim VndId As Long
Dim i As Integer, u As Integer
Dim a As Integer, j As Integer
Dim NumDays As Integer, NumDaysRange1 As Integer
Dim Temp As String, Ref As Integer
Dim ARem As String, ARef As String
Dim LclDate As String, Vndr As String
Dim TDate As String, ADate As String
Dim ApptType As String, Dura As String, Dr As String
Dim ApplList As ApplicationAIList
Dim ApplSch As ApplicationScheduler
Dim RetAppt As SchedulerAppointment
ActionType = UCase(Trim(ActionType))
If (ActionType <> "") Then
    Vndr = ""
    VndId = 0
    Call FormatTodaysDate(TDate, False)
    Set ApplSch = New ApplicationScheduler
    Set ApplList = New ApplicationAIList
    For i = 0 To lstActions.ListCount - 1
        If (InStrPS(UCase(lstActions.List(i)), ActionType) <> 0) Then
            If (ActionType = "SEND A CONSULT") Then
                GoSub ConsultOn
            ElseIf (ActionType = "REFER PATIENT TO ANOTHER") Then
                GoSub ReferOn
            ElseIf (ActionType = "SCHEDULE SURGERY") Then
                GoSub FacilityOn
            ElseIf (ActionType = "APPOINTMENT") Then
                GoSub ApptOn
            End If
        End If
    Next i
    Set ApplSch = Nothing
    Set ApplList = Nothing
End If
Exit Sub
ConsultOn:
    Vndr = ""
    VndId = 0
    u = InStrPS(6, lstActions.List(i + 1), "/")
    If (u > 0) Then
        ARem = "/Ltr/" + Trim(Left(lstActions.List(i + 1), u - 1))
        ARef = Trim(Mid(lstActions.List(i + 1), u + 1, Len(lstActions.List(i + 1)) - u))
        j = InStrPS(ARef, "(")
        If (j > 0) Then
            a = InStrPS(j, ARef, ")")
            If (a > 0) Then
                Vndr = Mid(ARef, j + 1, (a - 1) - j)
                VndId = val(Trim(Vndr))
            End If
        End If
    Else
        ARem = "/Ltr/" + Trim(lstActions.List(i + 1))
        ARef = ""
        If (Left(lstActions.List(i + 2), 5) = Space(5)) Then
            ARef = Trim(lstActions.List(i + 2))
            j = InStrPS(ARef, "(")
            If (j > 0) Then
                a = InStrPS(j, ARef, ")")
                If (a > 0) Then
                    Vndr = Trim(Mid(ARef, j + 1, (a - 1) - j))
                    VndId = val(Vndr)
                End If
            End If
        End If
    End If
    If (Left(lstActions.List(i + 3), 5) = Space(5)) Then
        If ("(" + Vndr + ")" <> Trim(Left(lstActions.List(i + 3), 74))) Then
            ARem = ARem + Trim(Left(lstActions.List(i + 3), 75))
        End If
    End If
    Call ApplList.ApplSetNewTransaction(AppointmentId, "S", TDate, ARef, ARem, VndId)
    Return
ReferOn:
    u = InStrPS(6, lstActions.List(i + 1), "/")
    If (u > 0) Then
        ARem = "/Ltr/" + Trim(Left(lstActions.List(i + 1), u - 1))
        ARef = Trim(Mid(lstActions.List(i + 1), u + 1, Len(lstActions.List(i + 1)) - u))
        If (Trim(ARef) = "") Then
            If (Left(lstActions.List(i + 2), 5) = "     ") Then
                ARef = Trim(lstActions.List(i + 2))
                j = InStrPS(ARef, "(")
                If (j > 0) Then
                    a = InStrPS(j, ARef, ")")
                    If (a > 0) Then
                        Vndr = Mid(ARef, j + 1, (a - 1) - j)
                        VndId = val(Trim(Vndr))
                    End If
                End If
                Call StripCharacters(ARef, "/")
                i = i + 1
            Else
                ARef = ""
            End If
        End If
    Else
        ARem = "/Ltr/" + Trim(lstActions.List(i + 1))
        If (Left(lstActions.List(i + 2), 5) = "     ") Then
            ARef = Trim(lstActions.List(i + 2))
            j = InStrPS(ARef, "(")
            If (j > 0) Then
                a = InStrPS(j, ARef, ")")
                If (a > 0) Then
                    Vndr = Trim(Mid(ARef, j + 1, (a - 1) - j))
                    VndId = val(Vndr)
                End If
            End If
            i = i + 1
        Else
            ARef = ""
        End If
    End If
    If (Left(lstActions.List(i + 3), 5) = Space(5)) Then
        If ("(" + Vndr + ")" <> Trim(Left(lstActions.List(i + 3), 74))) Then
            ARem = ARem + Trim(Left(lstActions.List(i + 3), 75))
        End If
    End If
    Call ApplList.ApplSetNewTransaction(AppointmentId, "F", TDate, ARef, ARem, VndId)
    Return
FacilityOn:
    u = InStrPS(6, lstActions.List(i + 1), "/")
    If (u > 0) Then
        ARem = "/Ltr/" + Trim(Left(lstActions.List(i + 1), u - 1))
        ARef = Trim(Mid(lstActions.List(i + 1), u + 1, Len(lstActions.List(i + 1)) - u))
        j = InStrPS(ARef, "(")
        If (j > 0) Then
            a = InStrPS(j, ARef, ")")
            If (a > 0) Then
                Vndr = Mid(ARef, j + 1, (a - 1) - j)
                VndId = val(Trim(Vndr))
            End If
        End If
    Else
        ARem = "/Ltr/" + Trim(lstActions.List(i + 1))
        ARef = Trim(lstActions.List(i + 4)) + "/" + Trim(lstActions.List(i + 3)) + "/"
        j = InStrPS(ARef, "(")
        If (j > 0) Then
            a = InStrPS(j, ARef, ")")
            If (a > 0) Then
                Vndr = Mid(ARef, j + 1, (a - 1) - j)
                VndId = val(Trim(Vndr))
            End If
        End If
        i = i + 2
    End If
    Call ApplList.ApplSetNewTransaction(AppointmentId, "Y", TDate, ARef, ARem, VndId)
    Return
ApptOn:
    NumDaysRange1 = 0
    ADate = ""
    LclDate = ""
    ApptType = Trim(lstActions.List(i + 1))
    Dura = Trim(lstActions.List(i + 2))
    Dr = Trim(lstActions.List(i + 3))
    If CheckConfigCollection("AUTORECALL") = "T" Then
        u = InStrPS(Dura, " ")
        If (u = 0) Then
            Ref = 1
        Else
            Ref = val(Trim(Left(Dura, u)))
        End If
        If (InStrPS(Dura, "STAT") > 0) Then
            NumDays = 0
            NumDaysRange1 = 0
        ElseIf (InStrPS(Dura, "WEEK") > 0) Then
            NumDays = 7
            NumDaysRange1 = 1
            If (Ref > 52) Then
                Ref = 52
            End If
        ElseIf (InStrPS(Dura, "DAY") > 0) Then
            NumDays = 1
            NumDaysRange1 = 0
            If (Ref > 5) Then
                NumDaysRange1 = 1
            End If
        ElseIf (InStrPS(Dura, "MONTH") > 0) Then
            NumDays = 30
            NumDaysRange1 = 1
            If (Ref > 24) Then
                Ref = 24
            End If
        ElseIf (InStrPS(Dura, "YEAR") > 0) Then
            NumDays = 365
            NumDaysRange1 = 1
            If (Ref > 20) Then
                Ref = 20
            End If
        Else
            NumDays = 1
            NumDaysRange1 = 1
        End If
        NumDays = NumDays * Ref
        Call AdjustDate(TDate, NumDays, ADate)
        ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        If (NumDaysRange1 > 0) Then
            Call AdjustDate(TDate, NumDaysRange1, LclDate)
            LclDate = Mid(LclDate, 7, 4) + Mid(LclDate, 1, 2) + Mid(LclDate, 4, 2)
            If (Not (ApplSch.IsAppointmentThere(PatientId, LclDate, ApptType))) _
            And (Not (ApplSch.IsRecallThere(PatientId, LclDate, ApptType))) Then
                Set RetAppt = New SchedulerAppointment
                RetAppt.AppointmentId = AppointmentId
                If (RetAppt.RetrieveSchedulerAppointment) Then
                    DoctorId = RetAppt.AppointmentResourceId1
                End If
                Set RetAppt = Nothing
                Call ApplSch.ApplSetCheckOutRecall(PatientId, ApptType, Dura, Dr, ADate, DoctorId)
            End If
        End If
    End If
    Return
End Sub

Private Function IsInvoiceeRx() As Boolean
    If CheckeRxOutput(True) = "9" Then
        IsInvoiceeRx = True
    End If
End Function

Private Function DoHL7Interop() As Boolean

End Function
Private Function CanCreateSecondClaim(ByVal AppointmentId As Long) As Boolean
 CanCreateSecondClaim = False
 Dim oPractice As New PracticeName
 Dim Resource As New SchedulerResource
 Dim RetApt As New SchedulerAppointment
 Dim i As Integer
 Dim ResourceId2 As Long

 oPractice.PracticeType = "P"
 oPractice.PracticeLocationReference = ""
 If oPractice.getPracticeId Then
    If Resource.GetFacilityServiceLocation() Then
       RetApt.AppointmentId = AppointmentId
       If RetApt.RetrieveSchedulerAppointment Then
           ResourceId2 = RetApt.AppointmentResourceId2
       End If
       i = 1
       While (Resource.SelectResource(i))
           If ResourceId2 = Resource.ResourceId And ResourceId2 > 0 And Resource.ResourceId > 0 Then
               CanCreateSecondClaim = False
               Exit Function
           End If
           i = i + 1
       Wend
       CanCreateSecondClaim = True
    End If
 End If
 Set oPractice = Nothing
End Function

Private Sub DisplayAppointmentSearchScreen(ByVal IsSurgery As Boolean)
On Error GoTo lDisplayAppointmentSearchScreen_Error

Dim ACom As String, ARv As String
Dim ARef As String, ARem As String
Dim TDate As String, NDate As String
Dim LocalDate As ManagedDate
Dim u As Integer, k As Integer
Dim Ref As Integer
Dim NumDays As Integer
Dim Temp As String
Dim RetAppt As SchedulerAppointment
Dim RetApptType As SchedulerAppointmentType

ACom = ""
ARv = ""
Call FormatTodaysDate(TDate, False)
If IsSurgery Then
    ARef = UCase(Trim(lstActions.List(lstActions.ListIndex + 3)))
Else
    ARef = UCase(Trim(lstActions.List(lstActions.ListIndex + 2)))
End If
u = InStrPS(ARef, "<")
If (u > 0) Then
    k = InStrPS(u + 1, ARef, "<")
    If (k > 0) Then
        ARv = Mid(ARef, k + 1, (Len(ARef) - 1) - k)
        ARv = Mid(ARv, 2, Len(ARv) - 1)
        ACom = Mid(ARef, u + 1, (k - 1) - u)
    Else
        ARv = ""
        ACom = Mid(ARef, u + 1, (Len(ARef) - 1) - u)
        If (Left(ACom, 1) = "^") Then
            ARv = Mid(ACom, 2, Len(ACom) - 1)
            ACom = ""
        End If
    End If
    ARef = Left(ARef, u - 1)
End If
Call ReplaceCharacters(ARef, "OD ", " ")
Call ReplaceCharacters(ARef, "OS ", " ")
Call ReplaceCharacters(ARef, "OU ", " ")
ARef = Trim(ARef)
u = InStrPS(ARef, " ")
If (u = 0) Then
    Ref = 1
Else
    If (u < 5) Then
        Ref = val(Trim(Left(ARef, u)))
    End If
End If
If (InStrPS(ARef, "STAT") > 0) Then
NumDays = 0
ElseIf (InStrPS(ARef, "WEEK") > 0) Then
NumDays = 7
If (Ref > 52) Then
    Ref = 52
End If
ElseIf (InStrPS(ARef, "DAY") > 0) Then
NumDays = 1
ElseIf (InStrPS(ARef, "MONTH") > 0) Then
NumDays = 30
If (Ref > 24) Then
    Ref = 24
End If
ElseIf (InStrPS(ARef, "YEAR") > 0) Then
NumDays = 365
If (Ref > 20) Then
    Ref = 20
End If
Else
NumDays = 1
End If
NumDays = NumDays * Ref
Call AdjustDate(TDate, NumDays, NDate)
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = NDate

Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentId = AppointmentId
If (RetAppt.RetrieveSchedulerAppointment) Then
    Dim arguments() As Variant
    Dim AppointmentSearchWrapper As New comWrapper
    Call AppointmentSearchWrapper.Create(AppointmentSearchLoadArgumentsType, emptyArgs)
    
    Dim loadArguments As Object
    Set loadArguments = AppointmentSearchWrapper.Instance
    
    loadArguments.PatientId = PatientId
    
    Set RetApptType = New SchedulerAppointmentType
    
    If IsSurgery Then
        RetApptType.AppointmentType = "Surgery"
    Else
        RetApptType.AppointmentType = Trim(Left(lstActions.List(lstActions.ListIndex + 1), 32))
    End If
    
    If (RetApptType.FindAppointmentType > 0) Then
        If (RetApptType.SelectAppointmentType(1)) Then
            loadArguments.AppointmentTypeId = RetApptType.AppointmentTypeId
        End If
    End If
    
    If IsSurgery Then
        If InStrPS(1, lstActions.List(lstActions.ListIndex + 4), "(") > 0 Then
            Temp = Trim(Left(lstActions.List(lstActions.ListIndex + 4), InStrPS(1, lstActions.List(lstActions.ListIndex + 4), "(") - 1))
        Else
            Temp = Trim(Left(lstActions.List(lstActions.ListIndex + 4), 32))
        End If
        loadArguments.LocationId = GetServiceLocationId(Temp, True)
    Else
        Call GetLocName(RetAppt.AppointmentResourceId2, Temp)
        loadArguments.LocationId = GetServiceLocationId(Temp, False)
    End If
    
    If LocalDate.ConvertDisplayDateToManagedDate Then
        Temp = Mid(LocalDate.ExposedDate, 5, 2) + "/" + Mid(LocalDate.ExposedDate, 7, 2) + "/" + Left(LocalDate.ExposedDate, 4)
        loadArguments.StartDate = CDate(Temp)
    Else
        loadArguments.StartDate = ""
    End If
    
    Dim RetRes As New SchedulerResource
    If IsSurgery Then
        k = InStrPS(1, lstActions.List(lstActions.ListIndex + 4), "(")
        If k > 0 Then
            RetRes.ResourceName = Trim(Mid(lstActions.List(lstActions.ListIndex + 4), k + 1, InStrPS(1, lstActions.List(lstActions.ListIndex + 4), ")") - (k + 1)))
        Else
            RetRes.ResourceName = Trim(Left(lstActions.List(lstActions.ListIndex + 4), 32))
        End If
    Else
        RetRes.ResourceName = Trim(Left(lstActions.List(lstActions.ListIndex + 3), 32))
    End If
    If (RetRes.FindResource > 0) Then
        If (RetRes.SelectResource(1)) Then
            loadArguments.ResourceId = RetRes.ResourceId
        End If
    End If
    loadArguments.ReasonForVisit = ARv
    loadArguments.Comment = ACom
    arguments = Array(loadArguments)
    
    Call AppointmentSearchWrapper.Create(AppointmentSearchViewManagerType, emptyArgs)
    Call AppointmentSearchWrapper.InvokeMethod("ShowAppointmentSearch", arguments)
End If
Set LocalDate = Nothing
Set RetAppt = Nothing
Exit Sub
lDisplayAppointmentSearchScreen_Error:
    LogError "frmDisposition", "DisplayAppointmentSearchScreen", Err, Err.Description
End Sub

Private Function GetServiceLocationId(ByVal Filter As String, ByVal IsSurgery As Boolean)
GetServiceLocationId = -1
Dim SQL As String
Dim RS As Recordset
If IsSurgery Then
    SQL = "Select TOP 1 Id From Model.ServiceLocations Where Name= '" & Filter & "' "
Else
    SQL = "Select TOP 1 Id From Model.ServiceLocations Where ShortName= '" & Filter & "' "
End If
Set RS = GetRS(SQL)
If RS.RecordCount > 0 Then
   GetServiceLocationId = RS("Id")
End If
End Function
Private Function GetLocName(LocId As Long, LocName As String) As Boolean
Dim RetLoc As SchedulerResource
Dim RetPrc As PracticeName
GetLocName = False
If (LocId > 0) And (LocId < 1000) Then
    Set RetLoc = New SchedulerResource
    RetLoc.ResourceId = LocId
    If (RetLoc.RetrieveSchedulerResource) Then
        GetLocName = True
        LocName = Trim(RetLoc.ResourceName)
    End If
    Set RetLoc = Nothing
ElseIf (LocId > 1000) Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeId = LocId - 1000
    If (RetPrc.RetrievePracticeName) Then
        GetLocName = True
        LocName = "OFFICE-" + Trim(RetPrc.PracticeLocationReference)
    End If
    Set RetPrc = Nothing
Else
    GetLocName = True
    LocName = "OFFICE"
End If
End Function
Private Function Validate() As Boolean
Validate = False
If (lstMethod.ListIndex >= 0) Then
    If (Trim(txtAmt.Text) = "") Then
        frmEventMsgs.Header = "Please Enter the Payment Amount"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        lstMethod.ListIndex = -1
        txtAmt.SetFocus
        Exit Function
    End If
    If (Mid(lstMethod.List(lstMethod.ListIndex), 40, 1) <> "C") And (Trim(txtPayRef.Text) = "") Then
        frmEventMsgs.Header = "Please Enter the Payment Reference #"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPayRef.SetFocus
        Exit Function
    End If
Else
    If (Trim(txtAmt.Text) <> "") Then
        frmEventMsgs.Header = "Please Enter the Payment Method"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        lstMethod.ListIndex = -1
        lstMethod.SetFocus
        Exit Function
    End If
End If
Validate = True
End Function
