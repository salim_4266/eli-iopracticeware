VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPaymentsTotal 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   ClientHeight    =   8850
   ClientLeft      =   -30
   ClientTop       =   -315
   ClientWidth     =   15810
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8850
   ScaleWidth      =   15810
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdVisits 
      Height          =   990
      Index           =   1
      Left            =   7800
      TabIndex        =   88
      Top             =   7680
      Visible         =   0   'False
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":0000
   End
   Begin VB.ListBox lstVisits 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3000
      Left            =   7800
      MultiSelect     =   1  'Simple
      TabIndex        =   87
      Top             =   4680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstPrint1 
      Height          =   690
      ItemData        =   "PaymentsTotal.frx":01F4
      Left            =   12480
      List            =   "PaymentsTotal.frx":01FB
      TabIndex        =   84
      Top             =   840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame frEditClaim 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   6495
      Left            =   1920
      TabIndex        =   9
      Top             =   1320
      Visible         =   0   'False
      Width           =   11895
      Begin VB.Frame frInEditClame 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   4935
         Left            =   120
         TabIndex        =   25
         Top             =   120
         Width           =   11535
         Begin VB.ListBox lstDiagDet 
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1950
            Left            =   0
            TabIndex        =   89
            Top             =   0
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.ListBox lstDelSrvs 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   5160
            MultiSelect     =   1  'Simple
            TabIndex        =   29
            Top             =   1680
            Visible         =   0   'False
            Width           =   1575
         End
         Begin MSFlexGridLib.MSFlexGrid lstASrv1 
            Height          =   1575
            Left            =   0
            TabIndex        =   40
            Top             =   3000
            Visible         =   0   'False
            Width           =   10215
            _ExtentX        =   18018
            _ExtentY        =   2778
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.PictureBox pTxt 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   0
            ScaleHeight     =   855
            ScaleWidth      =   1695
            TabIndex        =   32
            Top             =   2760
            Width           =   1695
            Begin VB.CommandButton DD 
               Caption         =   "q"
               BeginProperty Font 
                  Name            =   "Wingdings"
                  Size            =   6
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   720
               TabIndex        =   33
               Top             =   0
               Width           =   255
            End
            Begin VB.TextBox txt 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   45
               TabIndex        =   0
               Text            =   "Text1"
               Top             =   15
               Width           =   975
            End
         End
         Begin VB.ListBox lstDelDiag 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   690
            Left            =   0
            MultiSelect     =   1  'Simple
            TabIndex        =   39
            Top             =   1800
            Visible         =   0   'False
            Width           =   10335
         End
         Begin VB.ListBox lstTemp 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1950
            Left            =   7920
            TabIndex        =   38
            Top             =   1200
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.ListBox lstSearch 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1950
            Left            =   120
            TabIndex        =   27
            Top             =   1080
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.ListBox lstDD 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1950
            Left            =   1800
            MultiSelect     =   1  'Simple
            TabIndex        =   26
            Top             =   1080
            Visible         =   0   'False
            Width           =   1575
         End
         Begin MSFlexGridLib.MSFlexGrid lstDiagSave 
            Height          =   885
            Left            =   2280
            TabIndex        =   30
            Top             =   2760
            Visible         =   0   'False
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   1561
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstASrv 
            Height          =   1215
            Left            =   0
            TabIndex        =   34
            Top             =   1200
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   2143
            _Version        =   393216
            Cols            =   10
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstGen 
            Height          =   525
            Left            =   0
            TabIndex        =   36
            Top             =   0
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstGenSave 
            Height          =   765
            Left            =   4680
            TabIndex        =   37
            Top             =   2880
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   1349
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstGen1 
            Height          =   525
            Left            =   0
            TabIndex        =   28
            Top             =   480
            Visible         =   0   'False
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   10
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstDiag 
            Height          =   405
            Left            =   0
            TabIndex        =   35
            Top             =   600
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   714
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstASrvSave 
            Height          =   1575
            Left            =   0
            TabIndex        =   31
            Top             =   3240
            Visible         =   0   'False
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   2778
            _Version        =   393216
            Cols            =   10
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame frBtnBar 
         BackColor       =   &H0077742D&
         BorderStyle     =   0  'None
         Height          =   1095
         Left            =   0
         TabIndex        =   12
         Top             =   5400
         Width           =   11895
         Begin fpBtnAtlLibCtl.fpBtn fpBtnExit 
            Height          =   990
            Left            =   0
            TabIndex        =   13
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":020A
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtnSave 
            Height          =   990
            Left            =   10800
            TabIndex        =   14
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":03E9
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   4
            Left            =   4800
            TabIndex        =   15
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":05C8
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   11
            Left            =   7800
            TabIndex        =   16
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":07AA
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   10
            Left            =   960
            TabIndex        =   17
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":098F
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   12
            Left            =   1920
            TabIndex        =   18
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":0B76
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   13
            Left            =   8760
            TabIndex        =   19
            Top             =   105
            Visible         =   0   'False
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":0D61
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   14
            Left            =   2880
            TabIndex        =   20
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":0F4A
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   15
            Left            =   9720
            TabIndex        =   21
            Top             =   105
            Visible         =   0   'False
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":1130
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   7
            Left            =   5760
            TabIndex        =   22
            Top             =   105
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":1319
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   9
            Left            =   6840
            TabIndex        =   23
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":1507
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn 
            Height          =   990
            Index           =   0
            Left            =   3840
            TabIndex        =   80
            Top             =   105
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":16E7
         End
      End
   End
   Begin VB.Frame frPmnt 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   6135
      Index           =   0
      Left            =   600
      TabIndex        =   48
      Top             =   120
      Visible         =   0   'False
      Width           =   11775
      Begin VB.TextBox txtC 
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5790
         MaxLength       =   64
         TabIndex        =   59
         Top             =   3360
         Visible         =   0   'False
         Width           =   5805
      End
      Begin VB.Frame frPmnt 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   3375
         Index           =   1
         Left            =   60
         TabIndex        =   52
         Top             =   60
         Width           =   11655
         Begin VB.ListBox lstP 
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1740
            Left            =   960
            TabIndex        =   54
            Top             =   1320
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.Frame frMask 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   225
            Index           =   1
            Left            =   3105
            TabIndex        =   85
            Top             =   1470
            Width           =   900
         End
         Begin VB.PictureBox lstPT 
            BackColor       =   &H00C0FFFF&
            Height          =   3375
            Left            =   8040
            ScaleHeight     =   3315
            ScaleWidth      =   3195
            TabIndex        =   63
            Top             =   1200
            Visible         =   0   'False
            Width           =   3255
            Begin VB.VScrollBar VS 
               Height          =   3255
               LargeChange     =   300
               Left            =   2940
               SmallChange     =   15
               TabIndex        =   65
               Top             =   0
               Width           =   255
            End
            Begin VB.Label lblHelp 
               BackColor       =   &H00C0FFFF&
               Caption         =   "Label1"
               Height          =   3015
               Left            =   60
               TabIndex        =   64
               Top             =   0
               Width           =   2655
            End
         End
         Begin VB.Frame Frame1 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Caption         =   "FrMaskTemp"
            Height          =   580
            Left            =   4680
            TabIndex        =   82
            Top             =   540
            Width           =   4210
         End
         Begin VB.Frame frMask 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   225
            Index           =   0
            Left            =   8910
            TabIndex        =   81
            Top             =   1470
            Width           =   435
         End
         Begin VB.PictureBox pTxt1 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   3120
            ScaleHeight     =   855
            ScaleWidth      =   1695
            TabIndex        =   55
            Top             =   1680
            Width           =   1695
            Begin VB.CommandButton dd1 
               Caption         =   "q"
               BeginProperty Font 
                  Name            =   "Wingdings"
                  Size            =   6
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   720
               TabIndex        =   56
               Top             =   0
               Width           =   255
            End
            Begin VB.TextBox txt1 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   45
               TabIndex        =   1
               Text            =   "Text1"
               Top             =   15
               Width           =   975
            End
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   525
            Index           =   2
            Left            =   4695
            TabIndex        =   66
            Top             =   2880
            Width           =   6840
            _ExtentX        =   12065
            _ExtentY        =   926
            _Version        =   393216
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.CommandButton PT 
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   11325
            TabIndex        =   62
            Top             =   960
            Width           =   210
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   525
            Index           =   0
            Left            =   0
            TabIndex        =   53
            Top             =   0
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmnt 
            Height          =   2325
            Index           =   1
            Left            =   0
            TabIndex        =   57
            Top             =   1200
            Width           =   11535
            _ExtentX        =   20346
            _ExtentY        =   4101
            _Version        =   393216
            Rows            =   5
            Cols            =   7
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   -2147483643
            ForeColorSel    =   -2147483640
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            Enabled         =   -1  'True
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSFlexGridLib.MSFlexGrid lstPmntInfo 
            Height          =   525
            Left            =   4695
            TabIndex        =   58
            Top             =   600
            Width           =   6405
            _ExtentX        =   11298
            _ExtentY        =   926
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            BackColorFixed  =   12632256
            BackColorSel    =   16777215
            ForeColorSel    =   0
            BackColorBkg    =   16777215
            GridColor       =   8388608
            AllowBigSelection=   -1  'True
            ScrollTrack     =   -1  'True
            Enabled         =   0   'False
            FocusRect       =   2
            ScrollBars      =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblWB1 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   "Disbursed amount is greater than payment!"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   495
            Left            =   240
            TabIndex        =   90
            Top             =   600
            Visible         =   0   'False
            Width           =   2415
         End
         Begin VB.Label lblWB 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   " Service balance is less than zero"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   495
            Left            =   2760
            TabIndex        =   60
            Top             =   600
            Visible         =   0   'False
            Width           =   1815
         End
      End
      Begin VB.Frame frBtnBar1 
         BackColor       =   &H0077742D&
         BorderStyle     =   0  'None
         Height          =   1815
         Left            =   0
         TabIndex        =   49
         Top             =   4920
         Width           =   11775
         Begin fpBtnAtlLibCtl.fpBtn fpPmnt 
            Height          =   990
            Index           =   2
            Left            =   10800
            TabIndex        =   69
            Top             =   120
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":18CD
         End
         Begin fpBtnAtlLibCtl.fpBtn fpPmnt 
            Height          =   990
            Index           =   0
            Left            =   0
            TabIndex        =   50
            Top             =   120
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":1AAC
         End
         Begin fpBtnAtlLibCtl.fpBtn fpPmnt 
            Height          =   990
            Index           =   1
            Left            =   9720
            TabIndex        =   51
            Top             =   120
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1746
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   11098385
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "PaymentsTotal.frx":1C8B
         End
      End
   End
   Begin VB.ListBox lstPrint 
      Height          =   690
      ItemData        =   "PaymentsTotal.frx":1E6F
      Left            =   12360
      List            =   "PaymentsTotal.frx":1E71
      Sorted          =   -1  'True
      TabIndex        =   78
      Top             =   0
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.PictureBox piclblAlert 
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2640
      ScaleHeight     =   255
      ScaleWidth      =   1935
      TabIndex        =   71
      Top             =   600
      Visible         =   0   'False
      Width           =   1935
      Begin VB.Label lblAlert 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         Caption         =   "Alert ON"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         TabIndex        =   72
         Top             =   0
         Width           =   1455
      End
   End
   Begin VB.ListBox lstSrv 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   1800
      TabIndex        =   70
      Top             =   120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.ListBox lstCrd 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   1320
      TabIndex        =   67
      Top             =   120
      Visible         =   0   'False
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtn1 
      Height          =   990
      Left            =   120
      TabIndex        =   61
      Top             =   7680
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":1E73
   End
   Begin MSFlexGridLib.MSFlexGrid lstSrvs2 
      Height          =   2775
      Left            =   120
      TabIndex        =   2
      Top             =   3360
      Visible         =   0   'False
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   4895
      _Version        =   393216
      Rows            =   0
      Cols            =   20
      FixedRows       =   0
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   12648447
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox lstSort 
      ForeColor       =   &H00C0FFC0&
      Height          =   480
      ItemData        =   "PaymentsTotal.frx":2052
      Left            =   6720
      List            =   "PaymentsTotal.frx":2054
      Sorted          =   -1  'True
      TabIndex        =   24
      Top             =   960
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.ListBox lstSearch_old 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2700
      ItemData        =   "PaymentsTotal.frx":2056
      Left            =   4920
      List            =   "PaymentsTotal.frx":205D
      TabIndex        =   8
      Top             =   1560
      Visible         =   0   'False
      Width           =   5415
   End
   Begin MSFlexGridLib.MSFlexGrid lstInfo 
      Height          =   975
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   1720
      _Version        =   393216
      Rows            =   1
      Cols            =   4
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      Enabled         =   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   0
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10920
      TabIndex        =   4
      Top             =   7680
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":2070
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMClm 
      Height          =   510
      Left            =   2520
      TabIndex        =   5
      Top             =   8160
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   900
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":224F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   5880
      TabIndex        =   6
      Top             =   7680
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":2436
   End
   Begin MSFlexGridLib.MSFlexGrid lstPat 
      Height          =   270
      Left            =   120
      TabIndex        =   7
      Top             =   240
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   476
      _Version        =   393216
      Rows            =   1
      Cols            =   11
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16777215
      ForeColor       =   0
      BackColorFixed  =   16777215
      ForeColorFixed  =   0
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   16777215
      GridColor       =   0
      AllowBigSelection=   0   'False
      Enabled         =   -1  'True
      FocusRect       =   0
      HighLight       =   0
      ScrollBars      =   0
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid lstBSrv 
      Height          =   1575
      Left            =   840
      TabIndex        =   11
      Top             =   1200
      Visible         =   0   'False
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   2778
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   16777215
      GridColor       =   8388608
      AllowBigSelection=   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   0
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid lstSrvs1 
      Height          =   5655
      Left            =   120
      TabIndex        =   10
      Top             =   1680
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   9975
      _Version        =   393216
      Rows            =   0
      Cols            =   25
      FixedRows       =   0
      FixedCols       =   0
      ForeColor       =   0
      BackColorFixed  =   16777215
      ForeColorFixed  =   0
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   2
      HighLight       =   0
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnBill 
      Height          =   990
      Left            =   3840
      TabIndex        =   41
      Top             =   7680
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":2616
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnView 
      Height          =   495
      Left            =   8880
      TabIndex        =   42
      Top             =   7680
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":27F5
   End
   Begin VB.CheckBox chkGroupView 
      BackColor       =   &H0077742D&
      Caption         =   "Invoice"
      ForeColor       =   &H8000000E&
      Height          =   255
      Index           =   0
      Left            =   9840
      TabIndex        =   43
      Tag             =   "Invoice"
      Top             =   7650
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.CheckBox chkGroupView 
      BackColor       =   &H0077742D&
      Caption         =   "Services"
      ForeColor       =   &H8000000E&
      Height          =   255
      Index           =   1
      Left            =   9840
      TabIndex        =   44
      Tag             =   "Services"
      Top             =   7860
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.CheckBox chkGroupView 
      BackColor       =   &H0077742D&
      Caption         =   "Pmts, Adj"
      ForeColor       =   &H8000000E&
      Height          =   255
      Index           =   2
      Left            =   9840
      TabIndex        =   45
      Tag             =   "Payments"
      Top             =   8100
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.CheckBox chkGroupView 
      BackColor       =   &H0077742D&
      Caption         =   "Billing"
      ForeColor       =   &H8000000E&
      Height          =   255
      Index           =   3
      Left            =   9840
      TabIndex        =   46
      Tag             =   "Billing"
      Top             =   8340
      Width           =   1095
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnPmnt 
      Height          =   990
      Left            =   4800
      TabIndex        =   47
      Top             =   7680
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":29D4
   End
   Begin VB.ComboBox cmbTemp 
      Height          =   330
      Left            =   120
      TabIndex        =   68
      Text            =   "Combo1"
      Top             =   960
      Width           =   975
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnD 
      Height          =   495
      Index           =   10
      Left            =   1080
      TabIndex        =   73
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":2BBB
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnD 
      Height          =   495
      Index           =   12
      Left            =   1080
      TabIndex        =   74
      Top             =   8160
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":2DA3
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnD 
      Height          =   495
      Index           =   14
      Left            =   2520
      TabIndex        =   75
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":2F8B
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnPrint 
      Height          =   990
      Left            =   6840
      TabIndex        =   76
      Top             =   7680
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":3171
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnOld 
      Height          =   495
      Left            =   8880
      TabIndex        =   79
      Top             =   8160
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":3351
   End
   Begin VB.ListBox List1 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Left            =   1680
      TabIndex        =   83
      Top             =   0
      Visible         =   0   'False
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdVisits 
      Height          =   990
      Index           =   0
      Left            =   7800
      TabIndex        =   86
      Top             =   7680
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PaymentsTotal.frx":352F
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   4440
      TabIndex        =   77
      Top             =   0
      Visible         =   0   'False
      Width           =   3315
   End
   Begin VB.Menu mnuGridMain 
      Caption         =   "Options"
      Index           =   0
      Visible         =   0   'False
      Begin VB.Menu mnuGridSrvActions 
         Caption         =   "Service Actions"
         Begin VB.Menu mnuServiceNote 
            Caption         =   "Service Note"
         End
         Begin VB.Menu mnuAddService 
            Caption         =   "Services"
         End
         Begin VB.Menu mnuAddPayment 
            Caption         =   "Payments"
         End
         Begin VB.Menu mnuDiagnosis 
            Caption         =   "Diagnosis"
         End
      End
      Begin VB.Menu mnuGridClmActions 
         Caption         =   "Claim Actions"
         Begin VB.Menu mnuClaimNote 
            Caption         =   "Claim Note"
         End
         Begin VB.Menu mnuBillParty 
            Caption         =   "Bill Party"
            Begin VB.Menu mnuBillPartyPatient 
               Caption         =   "Patient"
            End
            Begin VB.Menu mnuBillPartyPayer 
               Caption         =   "Payer"
            End
            Begin VB.Menu mnuBillPartyNext 
               Caption         =   "Next Payer"
            End
            Begin VB.Menu mnuBillPartySelect 
               Caption         =   "Choose Payer"
            End
         End
         Begin VB.Menu mnuPrint 
            Caption         =   "Print"
            Begin VB.Menu mnuPrintClaim 
               Caption         =   "Claim"
            End
            Begin VB.Menu mnuPrintProof 
               Caption         =   "Proof"
            End
            Begin VB.Menu mnuPrintStatement 
               Caption         =   "Statement"
            End
            Begin VB.Menu mnuPrintReceipt 
               Caption         =   "Receipt"
            End
            Begin VB.Menu mnuPrintAggregate 
               Caption         =   "Aggregate"
            End
            Begin VB.Menu mnuPrintHistory 
               Caption         =   "History"
            End
            Begin VB.Menu mnuPrintHistoryDetails 
               Caption         =   "History Details"
            End
         End
         Begin VB.Menu mnuAddInfo 
            Caption         =   "Additional Info"
         End
         Begin VB.Menu mnuReferralDoctor 
            Caption         =   "Set Referral Doctor"
         End
         Begin VB.Menu mnuBillOffice 
            Caption         =   "Set Bill Office"
         End
         Begin VB.Menu mnuSetBillDoctor 
            Caption         =   "Set Bill Doctor"
         End
         Begin VB.Menu mnuSetInsurer 
            Caption         =   "Set Insurer"
         End
         Begin VB.Menu mnuSetDesignation 
            Caption         =   "Set Designation"
         End
         Begin VB.Menu mnuSetOver90 
            Caption         =   "Set Over 90"
         End
         Begin VB.Menu mnuShow 
            Caption         =   "Display"
            Begin VB.Menu mnuShowDemo 
               Caption         =   "Demo"
            End
            Begin VB.Menu mnuShowClinical 
               Caption         =   "Clinical"
            End
            Begin VB.Menu mnuShowInsurers 
               Caption         =   "Insurers"
            End
         End
      End
   End
End
Attribute VB_Name = "frmPaymentsTotal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public ReceivableId As Long
Public CurrentAction As String
Public InvAllocatedBalance As Single
Public AllocBalance As Single
Public UnallocBalance As Single
Public PatientBalance As Single
Public InsurerBalance As Single
Public Whom As Boolean
Public DisplayHow As String
Public AccessType As String


Public EditClaimMode As Boolean

Private IName(4, 6) As String
Private IPtr(4, 6) As Long

Private Const InvBackColor As Single = &H4000&
Private Const InvForeColor As Single = &H80000004
Private Const PayBackColor As Single = &HC0E0FF
Private Const PayIBackColor As Single = &H80FFFF
Private Const PayIForeColor As Single = &H0&
Private Const SrvBackColor As Single = &HCF9C&
Private Const SrvForeColor As Single = &H0&
Private Const PayPBackColor As Single = &HC0FFFF
Private Const PayPForeColor As Single = &H0
Private Const ClmTBackColor As Single = &HFFFFC0
Private Const TranBackColor As Single = &HC0FFC0
Private Const ClmTForeColor As Single = &H0&
Private Const HdrBackColor As Single = &HC0C0C0
Private Const HdrForeColor As Single = &H0&

Private SrvRow As Integer
Private SrvCol As Integer
Private SrvLoadOn As Boolean
Private SrvFieldOrg As String
Private SrvFirstKeyStroke As Boolean

Private DiaRow As Integer
Private DiaCol As Integer
Private DiaLoadOn As Boolean
Private DiaFieldOrg As String
Private DiaFirstKeyStroke As Boolean

Private TriggerOn As Boolean
Private MyAction As Integer
Private ResourceType As Integer
Private UserId As Long
Private TotalFound As Integer
Private RequireCheck As Boolean
Private InvDate As String
Private InsType As String
Private MaxInvoices As Integer
Private PaymentId As Long
Private Invoicedate As String
Private PayerType As String
Private PaymentType As String
Private TheCheckId As Long
Private PolicyHolderid As Long
Private PolicyHolderIdRel As String
Private PolicyHolderIdBill As Boolean
Private SecondPolicyHolderId As Long
Private SecondPolicyHolderIdRel As String
Private SecondPolicyHolderIdBill As Boolean
Private CurrRefId As Long

Private currentFGC As MSFlexGrid
Private FGCcol As New Collection
Private over90(10) As String
Dim bySys As Boolean

Public BillServices As Boolean

Private ManualClaim As Boolean
Private FromCreditInd As Boolean
Private FromCreditID As String
Public FirstTimeIn As Boolean

Dim EditMode As Boolean

Public InvId As String
Public OpenBal As String
Public Updated As Boolean

Dim mCheckExist As Long

Dim mKeyCode As Integer
Public PatRunsUsed As Long
Dim pat As New Patient
Dim EditCellFlag As Boolean

Public Function LoadPaymentsTotal(InvDt As String) As Boolean

If UserLogin.HasPermission(epPatientFinancials) Then

Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1




    LoadPaymentsTotal = False
    Exit Function
End If











' new logic switch
Dim NewLogic As Boolean
NewLogic = True


On Error GoTo UI_ErrorHandler
Dim OpticalMedicalOn As Boolean

Dim TotalBalance As Single

Dim u As Integer, g1 As Integer
Dim p As Integer

Dim RcvId As Long
'dim ai As Long
'Dim i1 As Long, i2 As Long, i3 As Long
'Dim i4 As Long, i5 As Long, i6 As Long

Dim DisplayText As String
Dim TempP As String, TempI As String
Dim TempB As String, TempU As String
Dim TempC As String

Dim Temp As String
Dim TheDate As String
'Dim InsName As String
Dim PatName As String
Dim PatDOB As String
Dim IColors(24) As Single

'Dim RetIns As Insurer
'Dim RetFin As PatientFinance
Dim RetPat As Patient

Dim LocalDate As ManagedDate

Dim ApplList As ApplicationAIList
'Dim ApplTemp As ApplicationTemplates

Set ApplList = New ApplicationAIList
'Set ApplTemp = New ApplicationTemplates

over90(0) = " 1 Eligibility"
over90(1) = " 2 Litigation"
over90(2) = " 3 Authorization Delays"
over90(3) = " 4 Delay in cerifying Provider"
over90(4) = " 5 Delay in Supplying Billing Forms"
over90(5) = " 6 Delay in Delivery"
over90(6) = " 7 Third Party Processing Delay"
over90(7) = " 8 Delay in Eligibility"
over90(8) = " 9 Original Reject"
over90(9) = "10 Administration Delay"
over90(10) = "11 Other"

LoadPaymentsTotal = True
MyAction = 0
TriggerOn = False
IColors(1) = &HC0C0FF
IColors(2) = &HC0E0FF
IColors(3) = &HC0FFFF
IColors(4) = &HC0FFC0
IColors(5) = &HFFFFC0
IColors(6) = &HFFC0C0
IColors(7) = &HFFC0FF
IColors(8) = &HE0E0E0
IColors(9) = &H8080FF
IColors(10) = &H80C0FF
IColors(11) = &H80FFFF
IColors(12) = &H80FF80
IColors(13) = &HFFFF80
IColors(14) = &HFF8080
IColors(15) = &HFF80FF
IColors(16) = &HC0C0C0

UserId = UserLogin.iId
RequireCheck = Not (CheckConfigCollection("REQUIRECHECK") = "F")
ResourceType = 1
If CheckConfigCollection("OPTICALMEDICALON") = "T" Then
    OpticalMedicalOn = True
    If (UserLogin.sType = "Q") Or (UserLogin.sType = "U") Or (UserLogin.sType = "Y") Then
        ResourceType = 2
    End If
End If
If (CurrentAction <> "") Then
    ResourceType = 0
End If
InsType = ""
InvDate = InvDt
TheDate = ""
If (Trim(InvDate) <> "") Then
    Set LocalDate = New ManagedDate
    LocalDate.ExposedDate = InvDate
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        TheDate = LocalDate.ExposedDate
    End If
    Set LocalDate = Nothing
End If
PolicyHolderid = 0
PolicyHolderIdRel = ""
PolicyHolderIdBill = False
SecondPolicyHolderId = 0
SecondPolicyHolderIdRel = ""
SecondPolicyHolderIdBill = False

Set RetPat = New Patient
RetPat.PatientId = PatientId
If (RetPat.RetrievePatient) Then
    PatName = Trim(RetPat.LastName) + ", " + Trim(RetPat.FirstName)
    PatDOB = Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
End If

'lstInfo.Clear
lstInfo.Rows = 1
Dim RetRcv As New PatientReceivables
RetRcv.PatientId = PatientId
RetRcv.ReceivableId = RetRcv.GetDummyReceivable
If (RetRcv.RetrievePatientReceivable) Then
    TempC = -RetRcv.ReceivableBalance
End If
Set RetRcv = Nothing



PatientBalance = 0
InsurerBalance = 0
TotalBalance = ApplList.ApplGetAllBalancebyPatient(PatientId, PatientBalance, InsurerBalance, True)
UnallocBalance = TotalBalance - (PatientBalance + InsurerBalance) + TempC

Call DisplayDollarAmount(Trim(Str(PatientBalance)), TempP)
Call DisplayDollarAmount(Trim(Str(InsurerBalance)), TempI)
Call DisplayDollarAmount(Trim(Str(UnallocBalance)), TempU)
Call DisplayDollarAmount(Trim(Str(TotalBalance)), TempB)
Call DisplayDollarAmount(TempC, TempC)


DisplayText = Trim(PatName) + vbTab _
            + Trim(Str(PatientId)) + vbTab _
            + PatDOB + vbTab _
            + "Ins" + ": " + Trim(TempI) + vbTab _
            + "Pat" + ": " + Trim(TempP) + vbTab _
            + "UnBill" + ": " + Trim(TempU) + vbTab _
            + "Crdt" + ": " + Trim(TempC) + vbTab _
            + "Total" + ": " + Trim(TempB)
lstPat.AddItem DisplayText, 0
lstPat.Rows = 1
p = 2
GoSub SetColumnWidths

lstPat.ColAlignment(3) = 7
lstPat.ColAlignment(4) = 7
lstPat.ColAlignment(5) = 7
lstPat.ColAlignment(6) = 7
lstPat.ColAlignment(7) = 7
'lstpat.CellBackColor =

Erase IName
DisplayText = "Major Med" + vbTab _
            + "Vision" + vbTab _
            + "Ambulatory" + vbTab _
            + "Workers Comp" + vbTab
lstInfo.FormatString = DisplayText

Call loadlstInfo(TheDate)
p = 0
Dim c As Integer
For c = 0 To 3
lstInfo.ColAlignment(c) = flexAlignLeftCenter
Next

'For p = 1 To 4
'    If (p = 1) Then
'        Call ApplTemp.ApplGetAllPatientPayers(PatientId, "M", TheDate, i1, i2, i3, i4, i5, i6, False)
'    ElseIf (p = 2) Then
'        Call ApplTemp.ApplGetAllPatientPayers(PatientId, "V", TheDate, i1, i2, i3, i4, i5, i6, False)
'    ElseIf (p = 3) Then
'        Call ApplTemp.ApplGetAllPatientPayers(PatientId, "A", TheDate, i1, i2, i3, i4, i5, i6, False)
'    ElseIf (p = 4) Then
'        Call ApplTemp.ApplGetAllPatientPayers(PatientId, "W", TheDate, i1, i2, i3, i4, i5, i6, False)
'    End If
'    If (i1 > 0) Then
'        ai = i1
'        GoSub GetInsName
'        IName(p, 1) = InsName
'        IPtr(p, 1) = i1
'    End If
'    If (i2 > 0) Then
'        ai = i2
'        GoSub GetInsName
'        IName(p, 2) = InsName
'        IPtr(p, 2) = i2
'    End If
'    If (i3 > 0) Then
'        ai = i3
'        GoSub GetInsName
'        IName(p, 3) = InsName
'        IPtr(p, 3) = i3
'    End If
'    If (i4 > 0) Then
'        ai = i4
'        GoSub GetInsName
'        IName(p, 4) = InsName
'        IPtr(p, 4) = i4
'    End If
'    If (i5 > 0) Then
'        ai = i5
'        GoSub GetInsName
'        IName(p, 5) = InsName
'        IPtr(p, 5) = i5
'    End If
'    If (i6 > 0) Then
'        ai = i6
'        GoSub GetInsName
'        IName(p, 6) = InsName
'        IPtr(p, 6) = i6
'    End If
'Next p
'For p = 1 To 6
'    If (Trim(IName(1, p)) <> "") Or (Trim(IName(2, p)) <> "") Or (Trim(IName(3, p)) <> "") Or (Trim(IName(4, p)) <> "") Then
'        DisplayText = IName(1, p) + vbTab _
'                    + IName(2, p) + vbTab _
'                    + IName(3, p) + vbTab _
'                    + IName(4, p) + vbTab
'        lstInfo.AddItem DisplayText
'    End If
'Next p
'p = 0


GoSub SetColumnWidths
lstSrvs1.Visible = False
Set ApplList = New ApplicationAIList
Set ApplList.ApplGrid = lstSrvs1
Set ApplList.ApplGrid1 = lstSrvs2

If NewLogic Then
    Call ApplList.ApplLoadFinanciaData_5(lstSrvs1, lstSrvs2, PatientId, InvId, OpenBal, True)
    
    Dim NumVisits As Integer
    NumVisits = UBound(AllVisits)
    If NumDispVisits > NumVisits Then
        p = NumVisits
    Else
        p = NumDispVisits
    End If
    cmdVisits(0).Text = "Showing " & p & " of        " & NumVisits
    lstVisits.Clear
    For p = 1 To NumVisits
        lstVisits.AddItem AllVisits(p)
    Next
    p = lstVisits.ListCount
    lstVisits.Width = 1210
    If p > 25 Then
        p = 25
        lstVisits.Width = lstVisits.Width + 240
    End If
    lstVisits.Height = (p * 14 + 4) * 15
    lstVisits.Top = cmdVisits(0).Top - lstVisits.Height
Else
    Call ApplList.ApplLoadFinanciaData(PatientId, InvId, OpenBal, True, False, 1)
End If

Set ApplList = Nothing

p = 1
GoSub SetColumnWidths

If NewLogic Then
    For p = 0 To lstSrvs2.Rows - 1
        Select Case lstSrvs2.TextMatrix(p, 0)
        Case "R"
            Call SetLineColor(p, InvBackColor, InvForeColor, lstSrvs1)
        Case "S"
            Call SetLineColor(p, SrvBackColor, SrvForeColor, lstSrvs1)
        Case "P"
            If UCase((lstSrvs1.TextMatrix(p, 1)) = "PATIENT") Then
                Call SetLineColor(p, PayPBackColor, PayPForeColor, lstSrvs1)
            Else
                If lstSrvs2.TextMatrix(p, 7) = "" Then
                    Call SetLineColor(p, PayBackColor, PayIForeColor, lstSrvs1)
                Else
                    Call SetLineColor(p, PayIBackColor, PayIForeColor, lstSrvs1)
                End If
            End If
        Case "Date"
            Call SetLineColor(p, lstSrvs1.BackColorFixed, &H808080, lstSrvs1)
        Case "h1", "h2"
            Call SetLineColor(p, lstSrvs1.BackColorFixed, lstSrvs1.ForeColorFixed, lstSrvs1)
        Case "TR"
            Call SetLineColor(p, TranBackColor, ClmTForeColor, lstSrvs1)
        Case "T"
            Call SetLineColor(p, ClmTBackColor, ClmTForeColor, lstSrvs1)
        Case "X"
            Call SetLineColor(p, vbRed, InvForeColor, lstSrvs1)
'        Case "Y"
'            Call SetLineColor(p, &HC0&, InvForeColor, lstSrvs1)
        Case "Y", "NS"
            Call SetLineColor(p, &H808080, ClmTForeColor, lstSrvs1)
        End Select
    Next
Else
    For p = 0 To lstSrvs2.Rows - 1
        If (val(Trim(lstSrvs2.TextMatrix(p, 1))) > 0) Then
            If (lstSrvs2.TextMatrix(p, 0) = "R") Then
                Call SetLineColor(p, InvBackColor, InvForeColor, lstSrvs1)
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "S") Then
                Call SetLineColor(p, SrvBackColor, SrvForeColor, lstSrvs1)
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "P") Then
                If UCase((lstSrvs1.TextMatrix(p, 1)) = "PATIENT") Then
                    Call SetLineColor(p, PayPBackColor, PayPForeColor, lstSrvs1)
                Else
                    If lstSrvs2.TextMatrix(p, 7) = "" Then
                        Call SetLineColor(p, PayBackColor, PayIForeColor, lstSrvs1)
                    Else
                        Call SetLineColor(p, PayIBackColor, PayIForeColor, lstSrvs1)
                    End If
                End If
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "X") Then
                Call SetLineColor(p, vbRed, InvForeColor, lstSrvs1)
            End If
        Else
            If (lstSrvs2.TextMatrix(p, 0) = "Date") Then
                Call SetLineColor(p, lstSrvs1.BackColorFixed, &H808080, lstSrvs1)
                'Call SetLineColor(p, lstSrvs1.BackColorFixed, lstSrvs1.ForeColorFixed, lstSrvs1)
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "S Doc") Then
                Call SetLineColor(p, lstSrvs1.BackColorFixed, lstSrvs1.ForeColorFixed, lstSrvs1)
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "T") Then
                Call SetLineColor(p, ClmTBackColor, ClmTForeColor, lstSrvs1)
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "TR") Then
                Call SetLineColor(p, TranBackColor, ClmTForeColor, lstSrvs1)
            ElseIf (lstSrvs2.TextMatrix(p, 0) = "Y") Then
                Call SetLineColor(p, &HC0&, InvForeColor, lstSrvs1)
            End If
        End If
    Next p
End If

Call setView
Call setRowsHeight

lstSrvs1.Visible = True

If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
    piclblAlert.Visible = True
End If

RcvId = ReceivableId
ReceivableId = RcvId

LoadPaymentsTotal = True
lstSrvs1.Row = 1
lstSrvs1.col = 0



Exit Function
SetColumnWidths:
    If (p = 0) Then
        lstInfo.Row = 0
        lstInfo.RowSel = 0
        For g1 = 0 To lstInfo.Cols - 1
            lstInfo.col = 1
            lstInfo.ColSel = 1
            If (g1 = 0) Then
                lstInfo.ColWidth(g1) = 300 * 15
            Else
                lstInfo.ColWidth(g1) = 170 * 15
            End If
        Next g1
    ElseIf (p = 1) Then
        lstSrvs1.Row = 0
        lstSrvs1.RowSel = 0
        For g1 = 0 To lstSrvs1.Cols - 1
            lstSrvs1.col = 1
            lstSrvs1.ColSel = 1
            Select Case g1
            Case 0
                lstSrvs1.ColWidth(g1) = 64 * 15
            Case 1
                lstSrvs1.ColWidth(g1) = 150 * 15
            Case 2
                lstSrvs1.ColWidth(g1) = 50 * 15
            Case 3
                lstSrvs1.ColWidth(g1) = 100 * 15
            Case 4, 5
                lstSrvs1.ColWidth(g1) = 40 * 15
            Case 6, 7, 8, 9
                lstSrvs1.ColWidth(g1) = 63 * 15
            Case 10
                lstSrvs1.ColWidth(g1) = 15 * 15
            Case 11
                lstSrvs1.ColWidth(g1) = 49 * 15
            Case Else
                lstSrvs1.ColWidth(g1) = 0
            End Select
        Next g1
        lstSrvs1.ColAlignment(1) = flexAlignLeftCenter
        lstSrvs1.ColAlignment(2) = flexAlignLeftCenter
        lstSrvs1.ColAlignment(3) = flexAlignLeftCenter
        lstSrvs1.ColAlignment(6) = flexAlignRightCenter
        lstSrvs1.ColAlignment(7) = flexAlignRightCenter
        lstSrvs1.ColAlignment(8) = flexAlignRightCenter
        lstSrvs1.ColAlignment(9) = flexAlignRightCenter

        lstSrvs1.FixedRows = 1
        lstSrvs1.AllowUserResizing = flexResizeColumns
    ElseIf (p = 2) Then
        lstPat.Row = 0
        lstPat.RowSel = 0
        For g1 = 0 To lstPat.Cols - 1
            lstPat.col = g1
            lstPat.ColSel = g1
'            Select Case g1
'            Case 0
'                lstPat.ColWidth(g1) = 195 * 15
'            Case 1
'                lstPat.ColWidth(g1) = 100 * 15
'            Case 2
'                lstPat.ColWidth(g1) = 70 * 15
'            Case 3, 5, 7, 9
'                lstPat.ColWidth(g1) = 40 * 15
'            Case Else
'                lstPat.ColWidth(g1) = 64 * 15
'            End select
            Select Case g1
            Case 0
                lstPat.ColWidth(g1) = 195 * 15
            Case 1
                lstPat.ColWidth(g1) = 50 * 15
            Case 2
                lstPat.ColWidth(g1) = 70 * 15
'            Case 3, 4, 5, 6
'                lstPat.ColWidth(g1) = 95 * 15
            Case 3
                lstPat.ColWidth(g1) = 90 * 15
            Case 4
                lstPat.ColWidth(g1) = 90 * 15
            Case 5
                lstPat.ColWidth(g1) = 100 * 15
            Case 6
                lstPat.ColWidth(g1) = 95 * 15
            Case 7
                lstPat.ColWidth(g1) = 88 * 15
            End Select
        Next g1
    End If
    Return
'GetInsName:
'    InsName = ""
'    If (ai > 0) Then
'        Set RetFin = New PatientFinance
'        RetFin.FinancialId = ai
'        If (RetFin.RetrievePatientFinance) Then
'            If (RetFin.PrimaryInsurerId > 0) Then
'                Set RetIns = New Insurer
'                RetIns.InsurerId = RetFin.PrimaryInsurerId
'                If (RetIns.RetrieveInsurer) Then
'                    InsName = Trim(RetIns.InsurerName)
'                End If
'                Set RetIns = Nothing
'            End If
'        End If
'        Set RetFin = Nothing
'    End If
'    Return
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Sub loadlstInfo(TheDate As String, _
Optional pat As Long, Optional lst As MSFlexGrid)

Erase IName

If lst Is Nothing Then
    pat = PatientId
    Set lst = lstInfo
End If


Dim i1 As Long, i2 As Long, i3 As Long
Dim i4 As Long, i5 As Long, i6 As Long
Dim ai As Long
Dim InsName As String
Dim DisplayText As String
Dim p As Integer

Dim ApplTemp As New ApplicationTemplates
Dim RetFin As New PatientFinance
Dim RetIns As New Insurer

MyPracticeRepository.BeginCachingScope
            On Error GoTo Finally
MyPracticeRepository.BeginBatch

For p = 1 To 4
    If (p = 1) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "M", TheDate, i1, i2, i3, i4, i5, i6, False)
    ElseIf (p = 2) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "V", TheDate, i1, i2, i3, i4, i5, i6, False)
    ElseIf (p = 3) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "A", TheDate, i1, i2, i3, i4, i5, i6, False)
    ElseIf (p = 4) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "W", TheDate, i1, i2, i3, i4, i5, i6, False)
    End If
    If (i1 > 0) Then
        ai = i1
        GoSub GetInsName
        IName(p, 1) = InsName
        IPtr(p, 1) = i1
    End If
    If (i2 > 0) Then
        ai = i2
        GoSub GetInsName
        IName(p, 2) = InsName
        IPtr(p, 2) = i2
    End If
    If (i3 > 0) Then
        ai = i3
        GoSub GetInsName
        IName(p, 3) = InsName
        IPtr(p, 3) = i3
    End If
    If (i4 > 0) Then
        ai = i4
        GoSub GetInsName
        IName(p, 4) = InsName
        IPtr(p, 4) = i4
    End If
    If (i5 > 0) Then
        ai = i5
        GoSub GetInsName
        IName(p, 5) = InsName
        IPtr(p, 5) = i5
    End If
    If (i6 > 0) Then
        ai = i6
        GoSub GetInsName
        IName(p, 6) = InsName
        IPtr(p, 6) = i6
    End If
Next p
MyPracticeRepository.ExecuteBatch

For p = 1 To 4
    If (p = 1) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "M", TheDate, i1, i2, i3, i4, i5, i6, False)
    ElseIf (p = 2) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "V", TheDate, i1, i2, i3, i4, i5, i6, False)
    ElseIf (p = 3) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "A", TheDate, i1, i2, i3, i4, i5, i6, False)
    ElseIf (p = 4) Then
        Call ApplTemp.ApplGetAllPatientPayers(pat, "W", TheDate, i1, i2, i3, i4, i5, i6, False)
    End If
    If (i1 > 0) Then
        ai = i1
        GoSub GetInsName
        IName(p, 1) = InsName
        IPtr(p, 1) = i1
    End If
    If (i2 > 0) Then
        ai = i2
        GoSub GetInsName
        IName(p, 2) = InsName
        IPtr(p, 2) = i2
    End If
    If (i3 > 0) Then
        ai = i3
        GoSub GetInsName
        IName(p, 3) = InsName
        IPtr(p, 3) = i3
    End If
    If (i4 > 0) Then
        ai = i4
        GoSub GetInsName
        IName(p, 4) = InsName
        IPtr(p, 4) = i4
    End If
    If (i5 > 0) Then
        ai = i5
        GoSub GetInsName
        IName(p, 5) = InsName
        IPtr(p, 5) = i5
    End If
    If (i6 > 0) Then
        ai = i6
        GoSub GetInsName
        IName(p, 6) = InsName
        IPtr(p, 6) = i6
    End If
Next p
GoSub FinallyOnSuccess

lst.Rows = 1
For p = 1 To 6
    If (Trim(IName(1, p)) <> "") Or (Trim(IName(2, p)) <> "") Or (Trim(IName(3, p)) <> "") Or (Trim(IName(4, p)) <> "") Then
        DisplayText = IName(1, p) + vbTab _
                    + IName(2, p) + vbTab _
                    + IName(3, p) + vbTab _
                    + IName(4, p) + vbTab
        lst.AddItem DisplayText
    End If
Next p

Set ApplTemp = Nothing
Set RetFin = Nothing
Set RetIns = Nothing

Exit Sub

GetInsName:
    InsName = ""
    If (ai > 0) Then
        Set RetFin = New PatientFinance
        RetFin.FinancialId = ai
        If (RetFin.RetrievePatientFinance) Then
            If (RetFin.PrimaryInsurerId > 0) Then
                Set RetIns = New Insurer
                RetIns.InsurerId = RetFin.PrimaryInsurerId
                If (RetIns.RetrieveInsurer) Then
                    InsName = Trim(RetIns.InsurerName)
                End If
                Set RetIns = Nothing
            End If
        End If
        Set RetFin = Nothing
    End If
    Return
    
FinallyOnSuccess:
    MyPracticeRepository.CancelBatch
    MyPracticeRepository.EndCachingScope
    Return
Finally:
    MyPracticeRepository.CancelBatch
    MyPracticeRepository.EndCachingScope


End Sub

Private Sub chkGroupView_Click(Index As Integer)
    If bySys Then Exit Sub
    
    Select Case Index
    Case 0
        SrvGrid.ClaimHeader = chkGroupView(0).Value
        SrvGrid.InvoiceInformation = chkGroupView(0).Value
        SrvGrid.ClaimTotal = chkGroupView(0).Value
        SrvGrid.Invoice = chkGroupView(0).Value
    Case 1
        SrvGrid.ServiceHeader = chkGroupView(1).Value
        SrvGrid.Code = chkGroupView(1).Value
        SrvGrid.Services = chkGroupView(1).Value
    Case 2
        SrvGrid.InsurancePayments = chkGroupView(2).Value
        SrvGrid.PatientPayments = chkGroupView(2).Value
        SrvGrid.InsuranceAdjustments = chkGroupView(2).Value
        SrvGrid.NonInsuranceAdjustments = chkGroupView(2).Value
        SrvGrid.InfoTransactions = chkGroupView(2).Value
        SrvGrid.Payments = chkGroupView(2).Value
    Case 3
        SrvGrid.BillingTransactionsClaimsQueued = chkGroupView(3).Value
        SrvGrid.BillingTransactionsClaimsSent = chkGroupView(3).Value
        SrvGrid.BillingTransactionsClaimsClosed = chkGroupView(3).Value
        SrvGrid.BillingTransactionsAppeal = chkGroupView(3).Value
        SrvGrid.BillingTransactionsAppealClosed = chkGroupView(3).Value
        SrvGrid.BillingTransactionsPatientStatementQueued = chkGroupView(3).Value
        SrvGrid.BillingTransactionsPatientStatementSent = chkGroupView(3).Value
        SrvGrid.BillingTransactionsPatientStatementClosed = chkGroupView(3).Value
        SrvGrid.BillingTransactionsMonthlyStatement = chkGroupView(3).Value
        SrvGrid.BillingTransactionOrphaned = chkGroupView(3).Value
        SrvGrid.Billing = chkGroupView(3).Value
    End Select


setRowsHeight
On Error Resume Next
lstSrvs1.SetFocus
End Sub


Private Sub cmdDone_Click()
If PatRunsUsed = 1 Then
Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
End If

Dim r As Integer
For r = 0 To lstSrvs1.Rows - 1
    If lstSrvs2.TextMatrix(r, 0) = "T" Then
        If IsNumeric(lstSrvs1.TextMatrix(r, 11)) Then
            If CSng(lstSrvs1.TextMatrix(r, 11)) > 0 Then
                frmEventMsgs.Header = "You have unbilled services. Complete billing now?"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Yes"
                frmEventMsgs.CancelText = "No"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                DoEvents
                If frmEventMsgs.Result = 2 Then
                    Exit Sub
                Else
                    Exit For
                End If
            End If
        End If
    End If
Next

Unload frmPaymentsTotal
End Sub

Private Sub cmdMClm_Click()
If UserLogin.HasPermission(epManualClaimEntry) Then
    Dim PatientDemographics As New PatientDemographics
    PatientDemographics.PatientId = PatientId
    Call PatientDemographics.DisplayInvoiceEditAndBillScreen(0)
    Updated = True
    Dim memR As Integer
    memR = lstSrvs1.TopRow
    LoadPaymentsTotal ""
    If lstSrvs1.Rows > 1 Then
        If Not memR < lstSrvs1.Rows Then memR = lstSrvs1.Rows - 1
        If Not memR = 0 Then lstSrvs1.TopRow = memR
    End If
'            Call LoadPaymentsTotal(Invoicedate)
Else
    frmEventMsgs.Header = "User is not permissioned."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
End If
End Sub

Private Sub DD1Click()
Dim itm As String

If dd1.Caption = "q" Then
    With lstP
        .Clear
        .AddItem "Close list"
        Select Case lstPmntInd
        Case 0
            Select Case lstPmnt(0).col
            Case 0
                Call GetInsRecs(PatientId, "", lstP)
            Case 1
                Dim r As Integer
                Dim RetCode As New PracticeCodes
                RetCode.ReferenceType = "PAYABLETYPE"
                RetCode.ReferenceCode = ""
                    If (RetCode.FindCodePartial > 0) Then
                        r = 1
                        While (RetCode.SelectCode(r))
                            
                            itm = Trim(Mid(RetCode.ReferenceCode, 1, InStrPS(1, RetCode.ReferenceCode, "-") - 1)) _
                            & Space(200) & myTrim(RetCode.ReferenceCode, 1)
                            If IsNumeric(lstPmntInfo.TextMatrix(1, 0)) _
                            And UCase(Mid(itm, 1, 4)) = "CASH" Then
                            Else
                                .AddItem itm
                            End If
                            r = r + 1
                        Wend
                    End If
                Set RetCode = Nothing
                .Selected(0) = True
            End Select
        Case 1
            With txtC
                .Text = lstPmnt(1).TextMatrix(lstPmnt(1).Row, 15)
                .Top = pTxt1.Top + 300
                '.Left = pTxt1.Left + dd1.Left + 330 - .Width
                .Visible = True
                .SetFocus
            End With
            Exit Sub
        End Select
        DoEvents
        .Left = 0
        .Top = lstPmnt(0).Top + lstPmnt(0).RowPos(lstPmnt(0).Row) + lstPmnt(0).RowHeight(0)
        .Height = frPmnt(1).Height - .Top + 90
        .Width = frPmnt(1).Width - .Left
        .Visible = True
        .SetFocus
    End With
Else
    txt1.SetFocus
End If
End Sub

Private Sub DDClick()

Select Case DD.Caption
Case "q"
    Dim CmdBtns As New CommandBtns
    With CmdBtns
    
    Dim l As Integer
        Select Case currentFGC.Name
        Case "lstGen"
            Select Case lstGen.col
            Case 1, 2, 3
                Call CmdBtns.DoctorsSearch(lstSearch, lstGen.col)
            Case 4
                Call CmdBtns.Referral(lstSearch, txt, PatientId, _
                lstGen.TextMatrix(1, 0), _
                lstSrvs2.TextMatrix(lstSrvs1.Row, 3), CurrRefId)
            Case 5
            Dim PreCText As String
                If ConvertRcvId < 1 Then
            '    lblPreCert.Visible = Not (lblPreCert.Visible)
            '    If (lblPreCert.Visible) Then
                    frmEventMsgs.Header = "PreCerts Requested After Posting Of Claim"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    DoEvents
            '    End If
                Else
                    Call CmdBtns.PreCert(PatientId, lstSrvs2.TextMatrix(lstSrvs1.Row, 3))
                    Dim ApplSch As New ApplicationScheduler
                    Call ApplSch.GetPreCert(PatientId, lstSrvs2.TextMatrix(lstSrvs1.Row, 3), 0, PreCText)
                    Set ApplSch = Nothing
                    txt.Text = PreCText
                End If
                GoTo ExitSub
            Case 6, 7
                Call CmdBtns.Location(lstSearch)  '"ScheduledDoctor" "AnyDoctor"
            Case 8
                Call CmdBtns.over90(lstSearch, over90, ConvertRcvId)
            Case 9
                Call CmdBtns.Notes(PatientId, ConvertRcvId)
                Dim ApplList As New ApplicationAIList
                txt.Text = ApplList.GetNotes(PatientId, lstSrvs2.TextMatrix(lstSrvs1.Row, 3))
                Set ApplList = Nothing
                GoTo ExitSub
            End Select
            
        Case "lstDiag"
            Call .DiagnosisSearch(lstSearch)
            If Not lstSearch.ListCount > 1 Then GoTo ExitSub
        Case "lstASrv"
            Select Case lstASrv.col
            Case 0
                Call .ServiceSearch(lstSearch, lstSort, _
                                    Mid(lstGen.TextMatrix(1, 0), 7, 4) + Mid(lstGen.TextMatrix(1, 0), 1, 2) + Mid(lstGen.TextMatrix(1, 0), 4, 2), _
                                    PatientId, lstSrvs2.TextMatrix(lstSrvs1.Row, 3))
                If Not lstSearch.ListCount > 1 Then GoTo ExitSub
            Case 2
                With lstDD
                    .Clear
                    For l = 0 To 7
                        If Not lstDiag.TextMatrix(1, l) = "" Then
                            .AddItem lstDiag.TextMatrix(1, l)
                            If InStrPS(1, txt.Text, CStr(l + 1)) > 0 Then
                                .Selected(l) = True
                            End If
                        End If
                    Next
                    .Width = lstDiag.ColWidth(0)
                    .Left = pTxt.Left - .Width
                    .Height = (.ListCount * 15 + 3) * 15
                    .Top = pTxt.Top
                    Do Until .Top + .Height < frInEditClame.Height
                        .Top = .Top - 15
                    Loop
                    .Visible = True
                    .SetFocus
                    Exit Sub
                End With
            Case 10
                If Not lstASrv.TextMatrix(lstASrv.Row, 0) = "" Then
                    Call CmdBtns.Notes(PatientId, _
                    ConvertRcvId, _
                    lstASrv.TextMatrix(lstASrv.Row, 0), _
                    True)
                End If
                Set ApplList = New ApplicationAIList
                Dim Notes As Collection
                txt.Text = ApplList.GetNotes(PatientId, _
                           lstSrvs2.TextMatrix(lstSrvs1.Row, 3), _
                           lstASrv.TextMatrix(lstASrv.Row, 0), Notes)
                
                Set ApplList = Nothing
                GoTo ExitSub
            End Select
        End Select
        
    End With
    Set CmdBtns = Nothing
    Call ShowSearchList
Case "p"
    lstDD.Visible = False
    lstSearch.Visible = False
    txt.SetFocus
End Select

Exit Sub
ExitSub:
DD.Caption = "p"
txt.SetFocus

End Sub

Private Sub SrvsDetails(Optional POS As String = "none", _
                        Optional UnitChg As String = "none", _
                        Optional Allow As String = "none")

If UnitChg = "none" And Allow = "none" Then GoTo getPOS

Dim InsrId As Long
Dim InsId As Long
Dim Copay As Single
Dim IType As String

Dim ApptDate As String
ApptDate = Mid(lstGen.TextMatrix(1, 0), 7, 4) + Mid(lstGen.TextMatrix(1, 0), 1, 2) + Mid(lstGen.TextMatrix(1, 0), 4, 2)
Dim ApptId As Long
ApptId = lstSrvs2.TextMatrix(lstSrvs1.Row, 3)
Dim CRT As String
CRT = lstASrv.TextMatrix(lstASrv.Row, 0)
Dim LocId As Long
LocId = lstGen1.TextMatrix(1, 6)

Dim ApplList As New ApplicationAIList
Call ApplList.ApplGetInsurance(PatientId, ApptDate, InsrId, InsId, Copay, IType, True)
Set ApplList = Nothing

Dim ApplClaim As New ApplicationClaims
Call ApplClaim.FindProcedureByCRT(InsId, ApptDate, CRT, LocId, _
                                  UnitChg, Allow)

If POS = "none" Then Exit Sub
getPOS:
Dim ApplClm As New ApplicationClaims
Call ApplClm.ApplGetLocationPOS(lstGen1.TextMatrix(1, 6), "", POS)
Set ApplClm = Nothing

End Sub



Private Sub cmdVisits_Click(Index As Integer)
If Index = 0 Then

    If cmdVisits(0).Text = "More" Then
        frmPaymentsTotal.InvId = ""
        frmPaymentsTotal.LoadPaymentsTotal ""
    Else
        cmdVisits(1).Visible = True
        cmdVisits(1).Text = cmdVisits(0).Text
        lstVisits.Visible = True
        lstVisits.SetFocus
    End If
Else
    cmdVisits(1).Visible = False
    lstVisits.Visible = False
    cmdVisits(0).SetFocus
End If

End Sub

Private Sub DD_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
DDClick
If DD.Caption = "q" Then
    DD.Caption = "p"
Else
    DD.Caption = "q"
End If
End Sub


Private Sub dd1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
DD1Click
If dd1.Caption = "q" Then
    dd1.Caption = "p"
Else
    dd1.Caption = "q"
End If
End Sub

Private Sub Form_Activate()
If FirstTimeIn Then
    FirstTimeIn = False
    If lblAlert.Visible Then
        Call lblAlert_Click
    End If
    If EditClaimMode Then
        EditClaim
        Exit Sub
    End If
End If


On Error Resume Next
Select Case True
Case frPmnt(0).Visible
    txt1.SetFocus
Case frEditClaim.Visible
    txt.SetFocus
Case Else
    lstSrvs1.SetFocus
End Select


End Sub

Private Sub Form_Load()
'temp !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
OpenBal = "A"
'temp !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

If CheckConfigCollection("DISPLAYOLDFINANCIAL") = "T" Then
    fpBtnView.Height = cmdDone.Height / 2
Else
    fpBtnView.Height = cmdDone.Height
End If

FirstTimeIn = True
With FGCcol
    .Add lstGen
    .Add lstDiag
    .Add lstASrv
End With

Dim DisplayText As String
Dim j As Integer
resizeFrame
pTxt.Height = lstASrv.RowHeight(1) - 15
pTxt1.Height = lstASrv.RowHeight(1) - 15

'load gen
DisplayText = "DOS" + vbTab _
            + "Sched Doctor" + vbTab _
            + "Billing Doctor" + vbTab _
            + "Refering Doctor" + vbTab _
            + "Referral" + vbTab _
            + "Pre-cert" + vbTab _
            + "Location" + vbTab _
            + "Attrib To" + vbTab _
            + "Ov90" + vbTab _
            + "N"
lstGen.FormatString = DisplayText
For j = 0 To lstGen.Cols - 1
    Select Case j
    Case 0
        lstGen.ColWidth(j) = 80 * 15
    Case 1, 2, 3
        lstGen.ColWidth(j) = 90 * 15
    Case 4
        lstGen.ColWidth(j) = 118 * 15
    Case 5
        lstGen.ColWidth(j) = 80 * 15
    Case 6
        lstGen.ColWidth(j) = 80 * 15
    Case 7
        lstGen.ColWidth(j) = 70 * 15
    Case 8
        lstGen.ColWidth(j) = 35 * 15
    Case 9
        lstGen.ColWidth(j) = 35 * 15
    End Select
Next j
For j = 0 To lstGen.Cols - 1
    lstGen.ColAlignment(j) = flexAlignLeftCenter
Next j

'load diagn
DisplayText = "Diag 1" + vbTab _
            + "Diag 2" + vbTab _
            + "Diag 3" + vbTab _
            + "Diag 4" + vbTab _
            + "Diag 5" + vbTab _
            + "Diag 6" + vbTab _
            + "Diag 7" + vbTab _
            + "Diag 8" + vbTab _
            + "Diag 9" + vbTab _
            + "Diag 10" + vbTab _
            + "Diag 11" + vbTab _
            + "Diag 12"
lstDiag.FormatString = DisplayText
For j = 0 To lstDiag.Cols - 1
    lstDiag.ColWidth(j) = 64 * 15
    lstDiag.ColAlignment(j) = flexAlignLeftCenter
Next j
'lstDiag.RowHeight(2) = 0
'load services
DisplayText = "Service" + vbTab _
            + "Mods" + vbTab _
            + "Dx Links" + vbTab _
            + "POS" + vbTab _
            + "Unit Chg" + vbTab _
            + "Allowed" + vbTab _
            + "Units" + vbTab _
            + "Amount" + vbTab _
            + "Open Bal" + vbTab _
            + "Description" + vbTab _
            + "N"
lstASrv.FormatString = DisplayText
For j = 0 To lstASrv.Cols - 1
    Select Case j
    Case 3
        lstASrv.ColWidth(j) = 50 * 15
    Case 4, 5, 7, 8
        lstASrv.ColWidth(j) = 80 * 15
    Case 6
        lstASrv.ColWidth(j) = 40 * 15
    Case 9
        lstASrv.ColWidth(j) = 143 * 15
    Case 10
        lstASrv.ColWidth(j) = 35 * 15
    Case Else
        lstASrv.ColWidth(j) = 60 * 15
    End Select
Next j
'----------------------------------------
'load lstPmnt
DisplayText = "From" + vbTab _
            + "Method" + vbTab _
            + "Amt" + vbTab _
            + "Check or Ref #" + vbTab _
            + "Pay Date" + vbTab _
            + "EOB Date"
lstPmnt(0).FormatString = DisplayText

With lstPmnt(0)
For j = 0 To .Cols - 1
    Select Case j
    Case 0
        .ColWidth(j) = 188 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 1
        .ColWidth(j) = 125 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 2
        .ColWidth(j) = 70 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 3
        .ColWidth(j) = 225 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 4
        .ColWidth(j) = 80 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 5
        .ColAlignment(j) = flexAlignRightCenter
        .ColWidth(j) = 80 * 15
    End Select
    Next j
    .Row = 0
    For j = 0 To .Cols - 1
        .col = j
        .CellAlignment = 1
    Next j
    .Row = 1
    .col = 0
End With

'------------

With lstPmntInfo
    DisplayText = vbTab + vbTab _
                + "" + vbTab _
                + "" + vbTab _
                + "" + vbTab _
                + "" + vbTab _
                + "Paid" + vbTab _
                + "Adjust."
    .FormatString = DisplayText

    For j = 0 To .Cols - 1
    Select Case j
    Case 0, 1
        .ColWidth(j) = 0
    Case 2, 3, 4, 5, 6, 7
        .ColWidth(j) = 70 * 15
    End Select
    Next j
    .Row = 0
    Dim w As Integer
    For j = 0 To .Cols - 1
        .col = j
        .CellAlignment = 1
        w = w + .ColWidth(j)
    Next j
    .Row = 1
    .col = 0
    .Width = w
End With
'------------

With lstPmnt(1)
    DisplayText = vbTab _
            + "Srv Date" + vbTab _
            + "Service" + vbTab _
            + "Mo" + vbTab _
            + "Charge" + vbTab _
            + "Prv Bal" + vbTab _
            + "Allowed" + vbTab _
            + "Payment" + vbTab _
            + "CntrWO" + vbTab _
            + "CoIns" + vbTab _
            + "Deduct" + vbTab _
            + "Balance" + vbTab _
            + vbTab _
            + "N" + vbTab _
            + "A" + vbTab _
            + "Cmnt" + vbTab _
            + "P" + vbTab _
            + "OthAdj" + vbTab _
            + "Rsn" + vbTab _
            + "D"
    .FormatString = DisplayText
    
    For j = 0 To .Cols - 1
    Select Case j
    Case 0
        .ColWidth(j) = 0
    Case 1
        .ColWidth(j) = 75 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 2
        .ColWidth(j) = 55 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 3
        .ColWidth(j) = 20 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 4
        .ColWidth(j) = 56 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 5, 11
        .ColWidth(j) = 61 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 6, 7, 8, 11
        .ColWidth(j) = 55 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 17
        .ColWidth(j) = 55 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 9, 10
        .ColWidth(j) = 50 * 15
        .ColAlignment(j) = flexAlignRightCenter
    Case 12
        .ColWidth(j) = 0
    Case 13, 14
        .ColWidth(j) = 15 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 15
        .ColWidth(j) = 30 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 16
        .ColWidth(j) = 15 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 18
        .ColWidth(j) = 30 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    Case 19
        .ColWidth(j) = 15 * 15
        .ColAlignment(j) = flexAlignLeftCenter
    End Select
    Next j
    .Row = 0
    For j = 0 To .Cols - 1
        .col = j
        .CellAlignment = 1
    Next j
    .Row = 1
    .col = 6
End With
'----------------------------------------
With lstPmnt(2)
    .FormatString = "Credit" & vbTab & "Comments"

    .ColWidth(0) = 70 * 15
    .ColWidth(1) = 385 * 15
    .ColAlignment(0) = flexAlignRightCenter
    .col = 0
    .Row = 0
    .CellAlignment = flexAlignLeftCenter
    .Row = 1
End With
'----------------------------------------
For j = 0 To lstASrv.Cols - 1
    Select Case j
    Case 0, 1, 9, 10, 11, 12
        lstASrv.ColAlignment(j) = flexAlignLeftCenter
    Case Else
        lstASrv.ColAlignment(j) = flexAlignRightCenter
    End Select
Next j
lstASrv.Row = 0
For j = 0 To lstASrv.Cols - 1
    lstASrv.col = j
    lstASrv.CellAlignment = 1
Next j

Dim cntl As Control
For Each cntl In Me
    On Error Resume Next
    cntl.TabStop = False
Next
Set currentFGC = lstASrv


setView
End Sub


Private Sub fpBtn_Click(Index As Integer)
Dim CmdBtns As New CommandBtns

With CmdBtns
    Select Case Index
    Case 0 'Doctor Info
        If currentFGC.Name = "lstGen" Then
            Select Case lstGen.col
            Case 1, 2, 3
                Dim DocId As Long
                DocId = lstGen1.TextMatrix(1, lstGen.col)
                If DocId > 0 Then
                    If lstGen.col = 3 Then
                        If (frmVendors.VendorLoadDisplay(DocId, False)) Then
                            frmVendors.Show 1
                        End If
                    Else
                        If (frmSchedulerResource.ResourceLoadDisplay(DocId, "T")) Then
                            frmSchedulerResource.Show 1
                        End If
                    End If
                End If
            End Select
        End If
    Case 4 'More...
        Call .More(ConvertRcvId)
    Case 9 'Patient Info
        Call cmdNotes_Click
    Case 10 'Patient Info
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = PatientId
        Call PatientDemographics.DisplayPatientInfoScreen
    Case 11 'Diag. Info
        With lstDiagDet
            .Clear
            Dim i As Integer
            Dim d As String
            Dim RetDiag As New DiagnosisMasterPrimary
            For i = 0 To 11
                d = lstDiag.TextMatrix(1, i)
                If d = "" Then Exit For
                d = d & Space(10)
                d = Mid(d, 1, 10)
                .AddItem d & RetDiag.GetDiagName(Trim(d))
            Next
            Set RetDiag = Nothing
            .Left = 0
            .Top = lstDiag.Top + (lstDiag.RowHeight(0) * 2)
            .Height = frInEditClame.Height - .Top + 90
            .Width = frInEditClame.Width - .Left
            .Visible = True
            .SetFocus
        End With
    Case 12 'Clinical Info
    PatRunsUsed = pat.GetPatientAccess(PatientId)
            If PatRunsUsed = -1 Then
                 frmEventMsgs.Header = " Access Blocked"
                 frmEventMsgs.AcceptText = ""
                 frmEventMsgs.RejectText = "Ok"
                 frmEventMsgs.CancelText = ""
                 frmEventMsgs.Other0Text = ""
                 frmEventMsgs.Other1Text = ""
                 frmEventMsgs.Other2Text = ""
                 frmEventMsgs.Other3Text = ""
                 frmEventMsgs.Other4Text = ""
                 frmEventMsgs.Show 1
                Exit Sub
                End If
        If lstSrvs2.TextMatrix(lstSrvs1.Row, 0) = "R" Then
            If val(Trim(lstSrvs2.TextMatrix(lstSrvs1.Row, 3))) > 0 Then
                Call InsertLog("FrmReviewAppts.frm", "Opened Patient Chart  " & CStr(PatientId) & "", LoginCatg.EncounterOpen, "", "", 0, val(Trim(lstSrvs2.TextMatrix(lstSrvs1.Row, 3))))
            End If
            Call .ClinicalInfo(PatientId, val(Trim(lstSrvs2.TextMatrix(lstSrvs1.Row, 3))))
        Else
            frmReviewAppts.StartDate = ""
            If (frmReviewAppts.LoadApptsList(PatientId, 0, False)) Then
                frmReviewAppts.Show 1
            Else
                frmEventMsgs.Header = "No Appointments Listed"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
        End If
        If PatRunsUsed = 1 Then
        Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
        End If
    Case 14 ' Insurance Info
        Dim RetPat As New Patient
        RetPat.PatientId = PatientId
        If (RetPat.RetrievePatient) Then
            PolicyHolderid = RetPat.PolicyPatientId
            'SecondPolicyHolderId = RetPat.SecondPolicyPatientId
        End If
        Set RetPat = Nothing
        If (PolicyHolderid < 1) Then PolicyHolderid = PatientId
        Dim RetrievePatient As New Patient
        RetrievePatient.PatientId = PatientId
        If RetrievePatient.RetrievePatient Then
            PolicyHolderIdRel = RetrievePatient.Relationship
            PolicyHolderIdBill = RetrievePatient.BillParty
            SecondPolicyHolderId = RetrievePatient.SecondPolicyPatientId
            SecondPolicyHolderIdRel = RetrievePatient.SecondRelationship
            SecondPolicyHolderIdBill = RetrievePatient.SecondBillParty
        End If
        Set RetrievePatient = Nothing
        Call .InsuranceInfo(PatientId, _
                         PolicyHolderid, _
                         PolicyHolderIdRel, _
                         PolicyHolderIdBill, _
                         SecondPolicyHolderId, _
                         SecondPolicyHolderIdRel, _
                         SecondPolicyHolderIdBill)
    End Select
End With
'If PatRunsUsed = 1 Then
'Call Pat.GetPatientUpdate(PatientId, Pat.runscmplted)
'End If
Set CmdBtns = Nothing


End Sub

Private Sub ShowSearchList()
DoEvents
With lstSearch
    If .ListCount > 1 Then
        Select Case currentFGC.Name
        Case "lstGen", "lstDiag"
            .Left = 0
            .Top = currentFGC.Top + currentFGC.RowPos(currentFGC.Row) + currentFGC.RowHeight(0)
        Case "lstASrv"
            .Left = currentFGC.Left + currentFGC.ColPos(1)
            .Top = currentFGC.Top + currentFGC.RowHeight(0)
        End Select
        .Height = frInEditClame.Height - .Top + 90
        .Width = frInEditClame.Width - .Left
        .Visible = True
        .SetFocus
    Else
        'DDClick
        txt.SetFocus
    End If
End With

End Sub


Private Sub fpBtn1_Click()
If PatRunsUsed = 1 Then
Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
End If
    Unload frmPaymentsTotal
End Sub

Private Function ConvertRcvId() As Long
Dim RetRcv As New PatientReceivables
Dim SQL As String
Dim RS As Recordset
Dim fRcvId As String
Dim Verified As Boolean

Select Case lstSrvs2.TextMatrix(lstSrvs1.Row, 0)
Case "R"
    SQL = _
    "select receivableid from PatientReceivables " & _
    "where invoice = '" & lstSrvs2.TextMatrix(lstSrvs1.Row, 10) & "'"
    
    Set RS = GetRS(SQL)
    If Not RS.EOF Then
        fRcvId = RS("receivableid")
    End If
Case "P"
    fRcvId = lstSrvs2.TextMatrix(lstSrvs1.Row, 6)
Case Else
    fRcvId = lstSrvs2.TextMatrix(lstSrvs1.Row, 1)
    If Not IsNumeric(fRcvId) Then
        SQL = _
        "select receivableid from PatientReceivables " & _
        "where invoice = '" & lstSrvs2.TextMatrix(lstSrvs1.Row, 10) & "'"
        
        Set RS = GetRS(SQL)
        If Not RS.EOF Then
            fRcvId = RS("receivableid")
        End If
    End If
End Select

'verify
If IsNumeric(fRcvId) Then
    RetRcv.ReceivableId = fRcvId
    If (RetRcv.RetrievePatientReceivable) Then
        If RetRcv.SelectPatientReceivable(1) Then
        Else
            MsgBox "Error 201  Can't find receivable.  Please call IO at 212-844-0105"
            fRcvId = 0
        End If
    End If
End If

ConvertRcvId = fRcvId
End Function
Private Function ConvertInsurerId() As Long

Dim fInsurerId As String
fInsurerId = lstSrvs2.TextMatrix(lstSrvs1.Row, 2)
If Not IsNumeric(fInsurerId) Then
    Dim SQL As String
    Dim RS As Recordset
    SQL = _
    "select insurerid from PatientReceivables " & _
    "where invoice = '" & lstSrvs2.TextMatrix(lstSrvs1.Row, 10) & "'"
    
    Set RS = GetRS(SQL)
    fInsurerId = RS("insurerid")
End If

ConvertInsurerId = fInsurerId
End Function


Private Sub fpBtnBill_Click()
If lstSrvs1.Rows < 2 Then Exit Sub
If Not lstSrvs2.TextMatrix(lstSrvs1.Row, 0) = "R" Then Exit Sub

'========================================
Dim ATemp As String
Dim ApplList As New ApplicationAIList
Dim ApplTemp As New ApplicationTemplates
frmEventMsgs.Header = ""
Dim RcvId As Long
RcvId = ConvertRcvId

'If Not ApplList.anyPositiveServices(lstSrvs2.TextMatrix(lstSrvs1.Row, 10)) Then
'    frmEventMsgs.Header = "No balance to bill."
'    GoTo exitValidation
'End If

ATemp = ApplTemp.ApplVerifyPatient(PatientId, 0)
If ATemp <> "" Then
    frmEventMsgs.Header = ATemp
    GoTo exitValidation
End If

If Not (ApplTemp.ApplIsUPinValid(RcvId)) Then
    frmEventMsgs.Header = "Upin is invalid."
    GoTo exitValidation
End If


If Not (frmPendingClaims.IsClaimBillable(RcvId, ATemp, True)) Then
    frmEventMsgs.Header = ATemp
    GoTo exitValidation
End If
'========================================

exitValidation:
Set ApplList = Nothing
Set ApplTemp = Nothing

If frmEventMsgs.Header = "" Then
    If (frmBillServices.LoadServices(CStr(RcvId), lstSrvs2.TextMatrix(lstSrvs1.Row, 3))) Then
        BillServices = False
        frmBillServices.Show 1
        If BillServices Then
            Updated = True
            Dim memR As Integer
            memR = lstSrvs1.TopRow
            LoadPaymentsTotal ""
            If lstSrvs1.Rows > 1 Then
                If Not memR < lstSrvs1.Rows Then memR = lstSrvs1.Rows - 1
                If Not memR = 0 Then lstSrvs1.TopRow = memR
            End If
        End If
    End If
Else
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
End If
End Sub

Private Sub fpBtnD_Click(Index As Integer)
Call fpBtn_Click(Index)
If PatRunsUsed = 1 Then
Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
End If
End Sub
Private Sub fpBtnExit_Click()
If PatRunsUsed = 1 Then
Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
End If
If changedFGC(lstDiag, lstDiagSave) _
Or changedFGC(lstASrv, lstASrvSave) _
Or changedFGC(lstGen1, lstGenSave) Then
    With frmEventMsgs
        .Header = "Changes You made will not be saved."
        .AcceptText = "Exit"
        .RejectText = "Save and Exit"
        .CancelText = ""
        .Other0Text = ""
        .Other1Text = ""
        .Other2Text = ""
        .Other3Text = ""
        .Other4Text = ""
        .Show 1
        
        Select Case .Result
        Case 2
            Call fpBtnSave_Click
        Case 4
            Exit Sub
        End Select
        
    End With
End If
frEditClaim_Visible False
End Sub
Private Sub fpBtnOld_Click()
frmPayments.CurrentAction = ""
frmPayments.PatientId = PatientId
If (frmPayments.LoadPayments(False, "")) Then
    frmPayments.AccessType = ""
    frmPayments.Whom = False
    frmPayments.CurrentAction = ""
    frmPayments.fromNew = True
    frmPayments.Show 1
Else
    frmEventMsgs.Header = "No financial data for this patient"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
End If
End Sub

Private Sub fpBtnPmnt_Click()
lblWB1.Visible = False
If lstSrvs1.Rows > 0 Then
    If lstSrvs1.Rows < 2 Then Exit Sub
    If Not lstSrvs2.TextMatrix(lstSrvs1.Row, 0) = "R" Then Exit Sub
End If
EditMode = False
lstPmnt(0).Enabled = True
lstPmnt(1).Enabled = True
lstPmnt(2).Enabled = True
pTxt1.Visible = True
fpPmnt(2).Enabled = True
EditCellFlag = False
frPmnt_Visible True
fpPmnt(1).Enabled = (lstPmnt(1).Rows > 1)
End Sub

Private Sub fpBtnPrint_Click()
If lstSrvs1.Rows = 0 Then Exit Sub


Dim Ub92On As Boolean
Dim OptOn As Boolean
Dim p1 As Long
Dim PlnId As Long, ApptId As Long
Dim ITemp As Integer
Dim IType As Integer, TType As Integer
Dim InvId As String, InvDt As String
Dim RptA As String
Dim SSId As String
Dim Temp As String
Dim MyType As String
Dim DateToday As String
Dim PatName As String
Dim RptName As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim Statement As CPatientStatement
Dim SQL As String
Dim RS As Recordset
Dim ParamArgs() As Variant
Dim TelerikReporting As Reporting

p1 = 0

Dim RcvId As Long
If lstSrvs2.TextMatrix(lstSrvs1.Row, 0) = "R" Then
    RcvId = ConvertRcvId
    OptOn = CheckConfigCollection("OPTICALMEDICALON") = "T"
    Ub92On = CheckConfigCollection("UB92ON") = "T"
    PatName = Trim(lstPat.TextMatrix(0, 0))
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetPatientSS(PatientId, SSId)
    Set ApplTemp = Nothing
    frmEventMsgs.Header = "Purpose ?"
    frmEventMsgs.AcceptText = "Claim"
    frmEventMsgs.RejectText = "Proof"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Patient Statement"
    frmEventMsgs.Other1Text = "Patient Receipt"
    frmEventMsgs.Other2Text = "Patient Aggregate"
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = "Acct Hst Details"
    frmEventMsgs.Show 1
    DoEvents
'=============================================================================

    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    If (frmEventMsgs.Result = 1) Then
        MyType = "I"
        If (Ub92On) Then
            frmEventMsgs.Header = ""
            If (frmEventMsgs.Result = 1) Then
                frmEventMsgs.Header = "How ?"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "UB92"
                frmEventMsgs.CancelText = "HCFA"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                DoEvents
                If (frmEventMsgs.Result = 2) Then
                    MyType = "U"
                End If
            End If
        End If
'===================================================
' get receivable
'===================================================
        Dim i As Integer
        Dim c As Integer
        Dim SrvColl As New Collection
        Dim aBPID(7) As String
        Dim aInsuredId(7) As String
        Dim BPID As String
        Dim InsuredId As String
        Dim BPName As String
        Dim RetSrv As New PatientReceivableService
        Dim ApplSch As New ApplicationScheduler
        RetSrv.Invoice = lstSrvs2.TextMatrix(lstSrvs1.Row, 10)
        If (RetSrv.FindPatientReceivableService > 0) Then
            i = 1
            Do While RetSrv.SelectPatientReceivableService(i)
                InsuredId = "?"
                BPID = BilledParty_5(lstSrvs2.TextMatrix(lstSrvs1.Row, 3), RetSrv.ItemId, InsuredId)
                If IsNumeric(BPID) Then
                    Call ApplSch.GetInsurer(CLng(BPID), BPName)
                    If Not ItemExist(SrvColl, BPID) Then
                        aBPID(c) = BPID
                        aInsuredId(c) = InsuredId
                        SrvColl.Add BPName, BPID
                        c = c + 1
                        If c > 7 Then Exit Do
                    End If
                End If
                i = i + 1
            Loop
        End If
        Set RetSrv = Nothing
        Set ApplSch = Nothing
        
        Select Case c
        Case 0
            Set ApplList = New ApplicationAIList
            If ApplList.anyPositiveServices(lstSrvs2.TextMatrix(lstSrvs1.Row, 10)) Then
                frmEventMsgs.Header = "Bill insurer before printing"
            Else
                frmEventMsgs.Header = "No balance to print."
            End If
            GoTo ExitSub
        Case 1
        Case Else
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            For i = 1 To c
                Select Case i
                Case 1
                    frmEventMsgs.AcceptText = SrvColl(i)
                Case 2
                    frmEventMsgs.RejectText = SrvColl(i)
                Case 3
                    frmEventMsgs.Other0Text = SrvColl(i)
                Case 4
                    frmEventMsgs.Other1Text = SrvColl(i)
                Case 5
                    frmEventMsgs.Other2Text = SrvColl(i)
                Case 6
                    frmEventMsgs.Other3Text = SrvColl(i)
                Case 7
                    frmEventMsgs.Other4Text = SrvColl(i)
                End Select
            Next
        End Select
        
        Dim RetRcv As New PatientReceivables
        If c = 1 Then
            RetRcv.InsurerId = aBPID(0)
            RetRcv.InsuredId = aInsuredId(0)
        Else
            frmEventMsgs.Header = "Select Billed Party"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Show 1
            DoEvents
            frmEventMsgs.Header = ""
            If frmEventMsgs.Result = 4 Then GoTo ExitSub
            
            If frmEventMsgs.Result < 4 Then
                RetRcv.InsurerId = aBPID(frmEventMsgs.Result - 1)
                RetRcv.InsuredId = aInsuredId(frmEventMsgs.Result - 1)
            Else
                RetRcv.InsurerId = aBPID(frmEventMsgs.Result - 2)
                RetRcv.InsuredId = aInsuredId(frmEventMsgs.Result - 2)
            End If
        End If
            
        RetRcv.AppointmentId = 0
        RetRcv.PatientId = PatientId
        RetRcv.PatientId = 0
        RetRcv.ReceivableInvoice = lstSrvs2.TextMatrix(lstSrvs1.Row, 10)
        RetRcv.ReceivableInvoiceDate = ""
        RetRcv.ReceivableStatus = ""
        RetRcv.ReceivableType = ""
        If RetRcv.FindPatientReceivable = 1 Then
            If RetRcv.SelectPatientReceivable(1) Then
                RcvId = RetRcv.ReceivableId
            End If
        Else
            frmEventMsgs.Header = "Error #200:  Please call IO at 212-844-0105"
            GoTo ExitSub
        End If
        Set RetRcv = Nothing
        
'check for an active claim
        SQL = _
        " select * " & _
        " from PracticeTransactionJournal ptj " & _
        " inner join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId " & _
        " where ptj.TransactionType = 'R' " & _
        " and ptj.TransactionRef = 'I' " & _
        " and ptj.TransactionStatus <> 'Z' " & _
        " and pr.ReceivableId = " & RcvId

        Set RS = GetRS(SQL)
        If Not RS.RecordCount > 0 Then
            frmEventMsgs.Header = "Please bill an insurer before printing the claim."
            GoTo ExitSub
        End If
'===================================================
        
'        ITemp = frmPayments.IsPrintTransactional
        frmEventMsgs.Header = ""
'        If (ITemp = 1) Then
'            p1 = ApplTemp.BatchTransaction(RcvId, "I", "", "P", False, 0, False, False, False)
'            If (p1 > 0) Then
'                Call ApplList.ApplSetTransactionStatus(p1, "S")
'                lblPrint.Caption = "Printing"
'                lblPrint.Visible = True
'                DoEvents
'                Set ApplTemp.lstBox = lstPrint
'                If (ApplTemp.PrintTransaction(RcvId, PatientId, MyType, True, True, "", False, 0, "S", "", ResourceType)) Then
'                    frmEventMsgs.Header = "Claim Done"
'                Else
'                    frmEventMsgs.Header = "Claim Not Done"
'                End If
'                lblPrint.Visible = False
'            Else
'                frmEventMsgs.Header = ""
'            End If
'        ElseIf (ITemp = 2) Then
            lblPrint.Caption = "Printing"
            lblPrint.Visible = True
            Set ApplTemp.lstBox = lstPrint
            If (ApplTemp.PrintTransaction(RcvId, PatientId, MyType, True, True, "", False, 0, "S", "", ResourceType)) Then
                frmEventMsgs.Header = "Claim Done"
            Else
                frmEventMsgs.Header = "Claim Not Done"
            End If
            lblPrint.Visible = False
'        End If
    ElseIf (frmEventMsgs.Result = 2) Then
''''''        If (lstPayment.ListIndex >= 0) Then
''''''            SSId = SSId + " Invoice:" + (Mid(lstPayment.List(lstPayment.ListIndex), 14, 12))
''''''        End If
''''''        If (lstDetails.ListCount > 0) Then
''''''            Set ApplTemp.lstBox = lstPayment
''''''            Call ApplTemp.PrintProof(RcvId, PatName, SSId)
''''''            Set ApplTemp.lstBox = lstDetails
''''''            Call ApplTemp.PrintProof(RcvId, PatName, SSId)
''''''            If (lstTrans.ListCount > 0) Then
''''''                Set ApplTemp.lstBox = lstTrans
''''''                Call ApplTemp.PrintProof(RcvId, PatName, SSId)
''''''            End If
''''''            frmEventMsgs.Header = ""
''''''        Else
''''''            frmEventMsgs.Header = "Trigger claim details"
''''''        End If
        frmEventMsgs.Header = ""
        Dim Header As String
        Dim IDate As String * 11
        Dim ICode As String * 8
        Dim Idx As String * 7
        Dim iPARTY As String * 15
        Dim iTRANSACTION As String * 19
        Dim iAMOUNT As String * 13
        Dim tAmt As String
        Header = lstPat.TextMatrix(0, 1) & " Invoice:" & lstSrvs2.TextMatrix(lstSrvs1.Row, 10)
        With lstPrint1
            .Clear
            .AddItem ""
            .AddItem "DATE       CODE    DX     PARTY          TRANSACTION        AMOUNT"
            Dim r As Integer
            r = lstSrvs1.Row
            Dim ValidTran As Boolean
            Do Until r = lstSrvs1.Rows - 1
                IDate = ""
                ICode = ""
                Idx = ""
                iPARTY = ""
                iTRANSACTION = ""
                iAMOUNT = ""

            Select Case lstSrvs2.TextMatrix(r, 0)
            Case "S"
                .AddItem ""
                IDate = lstSrvs1.TextMatrix(r, 0)
                ICode = lstSrvs1.TextMatrix(r, 2)
                Idx = lstSrvs1.TextMatrix(r, 4)
                 tAmt = Trim(lstSrvs1.TextMatrix(r, 7))
                iAMOUNT = Space(Len(iAMOUNT) - Len(tAmt)) & tAmt
            Case "TR"
                ValidTran = False
                Select Case True
                Case InStrPS(1, (lstSrvs1.TextMatrix(r, 3)), " Snt") > 0
                    ValidTran = True
                    iTRANSACTION = lstSrvs1.TextMatrix(r, 3)
                Case lstSrvs1.TextMatrix(r, 3) = "Claim Clsd E"
                    ValidTran = True
                    iTRANSACTION = "Claim Snt E"
                Case lstSrvs1.TextMatrix(r, 3) = "Claim Clsd P"
                    ValidTran = True
                    iTRANSACTION = "Claim Snt P"
                End Select
                If ValidTran Then
                    IDate = lstSrvs1.TextMatrix(r, 0)
                    iPARTY = lstSrvs1.TextMatrix(r, 1)
                    tAmt = Trim(lstSrvs1.TextMatrix(r, 7))
                    iAMOUNT = Space(Len(iAMOUNT) - Len(tAmt)) & tAmt
                End If
            Case "T"
                .AddItem ""
                tAmt = Trim(lstSrvs1.TextMatrix(r, 7))
                .AddItem "TOTAL CLAIM: " & Space(60 - Len(tAmt)) & tAmt
                Exit Do
            End Select
                If Not Trim(IDate) = "" Then
                    .AddItem IDate & ICode & Idx & iPARTY & iTRANSACTION & iAMOUNT
                End If
                r = r + 1
            Loop
            
            Set ApplTemp.lstBox = lstPrint1
            Call ApplTemp.PrintProof(1000, PatName, Header)
        End With

    ElseIf (frmEventMsgs.Result = 3) Then 'Patient Statement
        'ITemp = frmPayments.IsPrintTransactional
        ITemp = 2
        frmEventMsgs.Header = ""
        If ITemp > 0 Then
            SQL = _
                " Select * " & _
                " from PracticeTransactionJournal ptj " & _
                " inner join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId " & _
                " where pr.Invoice = '" & lstSrvs2.TextMatrix(lstSrvs1.Row, 10) & "' " & _
                " and ptj.TransactionType = 'R' and ptj.TransactionStatus <> 'Z' and ptj.TransactionRef = 'P' "
            Set RS = GetRS(SQL)
            If RS.EOF Then
                Set ApplList = New ApplicationAIList
                If ApplList.anyPositiveServices(lstSrvs2.TextMatrix(lstSrvs1.Row, 10)) Then
                    frmEventMsgs.Header = "Bill patient before printing"
                Else
                    frmEventMsgs.Header = "No balance to print."
                End If
                GoTo ExitSub
            End If
            
            ReDim ParamArgs(1) As Variant
            
            ParamArgs(0) = Array("PatientId", Str(lstPat.TextMatrix(0, 1)))
            ParamArgs(1) = Array("EncounterId", Str(lstSrvs2.TextMatrix(lstSrvs1.Row, 3)))
            Set TelerikReporting = New Reporting
            'Invoke Patient Statement telerik Report.
            Call TelerikReporting.ViewReport("Patient Statement", ParamArgs)
            Set TelerikReporting = Nothing
            lblPrint.Visible = False
            DoEvents
        End If
    ElseIf (frmEventMsgs.Result = 5) Then
        frmEventMsgs.Header = ""
        lblPrint.Caption = "Printing"
        lblPrint.Visible = True
        DoEvents
        
        ReDim ParamArgs(0) As Variant
        ApptId = lstSrvs2.TextMatrix(lstSrvs1.Row, 3)
        
        ParamArgs(0) = Array("EncounterId", Str(ApptId))
        Set TelerikReporting = New Reporting
        'Invoke PatientReceipt telerik Report.
        Call TelerikReporting.ViewReport("Patient Receipt", ParamArgs)
        Set TelerikReporting = Nothing
        lblPrint.Visible = False
    ElseIf (frmEventMsgs.Result = 6) Then ' Aggregate Statements
        frmEventMsgs.Header = ""
        lblPrint.Caption = "Printing"
        lblPrint.Visible = True
        DoEvents
        
        ReDim ParamArgs(0) As Variant
            
        ParamArgs(0) = Array("PatientId", Str(lstPat.TextMatrix(0, 1)))
        Set TelerikReporting = New Reporting
        'Invoke Patient Statement telerik Report.
        Call TelerikReporting.ViewReport("Patient Statement", ParamArgs)
        Set TelerikReporting = Nothing
        lblPrint.Visible = False
        DoEvents
    ElseIf (frmEventMsgs.Result = 8) Then
        frmEventMsgs.Header = ""
        lblPrint.Caption = "Printing"
        lblPrint.Visible = True
        DoEvents
        'Patient Account History
        Set TelerikReporting = New Reporting
        ReDim ParamArgs(0) As Variant
        
        'We will do  whole history of Patient.Start and End Dates are optional.Null by default.
        ParamArgs(0) = Array("PatientId", Str(PatientId))
                
        'Invoke Account History Details telerik Report.
        Call TelerikReporting.ViewReport("Patient Account History", ParamArgs)
        Set TelerikReporting = Nothing
        lblPrint.Visible = False
    Else
        frmEventMsgs.Header = ""
    End If
    
ExitSub:
    
    Set ApplTemp = Nothing
    Set ApplList = Nothing
    If (Trim(frmEventMsgs.Header) <> "") Then
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoEvents
    End If
End If
End Sub

Private Sub fpBtnSave_Click()

Dim Changed As Boolean

Changed = changedFGC(lstDiag, lstDiagSave)
If Not Changed Then Changed = changedFGC(lstASrv, lstASrvSave)
If Not Changed Then Changed = changedFGC(lstGen1, lstGenSave)
If Not Changed Then GoTo endsub:

If lstDiag.TextMatrix(1, 0) = "" Then
    frmEventMsgs.Header = "Enter at least one diagnosis"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
    lstDiag.SetFocus
    Exit Sub
End If
If lstASrv.Rows = 2 Then
    frmEventMsgs.Header = "Enter at least one service"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
    lstASrv.SetFocus
    Exit Sub
End If

fpBtnSave.Enabled = False

If PatRunsUsed = 1 Then
Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
End If

Dim i As Integer
Dim ApplClaim As New ApplicationClaims

'save gen
'==============
Dim ApptId As Long
ApptId = lstSrvs2.TextMatrix(lstSrvs1.Row, 3)
Dim ApptDate As String
ApptDate = lstGen1.TextMatrix(1, 0)
Dim DocId As Long
DocId = lstGen1.TextMatrix(1, 1)
Dim RoomId As Long
RoomId = lstGen1.TextMatrix(1, 6)

Dim ReferralId As Long
Dim ApptTypeId As Long
Dim InsType As String
Dim RetApt As New SchedulerAppointment
RetApt.AppointmentId = ApptId
If (RetApt.RetrieveSchedulerAppointment) Then
    ReferralId = RetApt.AppointmentReferralId
    ApptTypeId = RetApt.AppointmentTypeId
    InsType = RetApt.AppointmentInsType
End If
Set RetApt = Nothing

Call ApplClaim.UpdateAppointment(ApptId, DocId, RoomId, ApptTypeId, InsType, ReferralId, ApptDate)
'--------------

Dim RcvId As Long
RcvId = ConvertRcvId
Dim RefDrId As Long
RefDrId = lstGen1.TextMatrix(1, 3)
Dim BillDrId As Long
BillDrId = lstGen1.TextMatrix(1, 2)
Dim BillOff As Long
BillOff = lstGen1.TextMatrix(1, 7)

Call ApplClaim.SetReceivableCosts(RcvId, RefDrId, BillDrId, False, BillOff, "")

Dim AccidentType As String
Dim AccidentState As String
Dim AdmissionDate As String
Dim DismissalDate As String
Dim Disability As String
Dim RsnType As String
Dim RsnDate As String
Dim ConsDate As String
Dim PrevCond As String
Dim MedRef As String
Dim RetrieveReceivables As New PatientReceivables
RetrieveReceivables.ReceivableId = RcvId
If (RetrieveReceivables.RetrievePatientReceivable) Then
        AccidentType = RetrieveReceivables.ReceivableAccidentType
        AccidentState = RetrieveReceivables.ReceivableAccidentState
        AdmissionDate = RetrieveReceivables.ReceivableHospitalAdmission
        DismissalDate = RetrieveReceivables.ReceivableHospitalDismissal
        Disability = RetrieveReceivables.ReceivableDisability
        RsnType = RetrieveReceivables.ReceivableRsnType
        RsnDate = RetrieveReceivables.ReceivableRsnDate
        ConsDate = RetrieveReceivables.ReceivableConsDate
        PrevCond = RetrieveReceivables.ReceivablePrevCond
End If

Call ApplClaim.UpdateReceivable(RcvId, ApptDate, AccidentType, AccidentState, AdmissionDate, _
                                DismissalDate, Disability, RsnType, RsnDate, ConsDate, PrevCond, _
                                MedRef, BillOff)
'------------------------
Call ApplClaim.UpdateOtherReceivableDates(RcvId, ApptDate)

'save diag
'========================
lstTemp.Clear
lstTemp.AddItem ""
For i = 0 To 11
    If lstDiag.TextMatrix(1, i) = "" Then
    Else
        lstTemp.AddItem lstDiag.TextMatrix(1, i) & _
        Space(74 - Len(lstDiag.TextMatrix(1, i))) & _
        lstDiag.TextMatrix(2, i)
    End If
Next
Call ApplClaim.PostReceivableDiags(lstTemp, ApptId, PatientId, "d")
Call ApplClaim.PostPurgedDiags(lstDelDiag)
Call ApplClaim.ApplEliminate0001Diag(ApptId)


'save srvs
'========================
Dim InvoiceId As String
InvoiceId = lstSrvs2.TextMatrix(lstSrvs1.Row, 10)
Dim InsurerId As Long
InsurerId = ConvertInsurerId
Dim Srv As String
Dim SrvItem As Long
Dim itm As String

With lstASrv
lstTemp.Clear
lstTemp.AddItem ""
For i = 1 To .Rows - 1
    If Not .TextMatrix(i, 0) = "" And _
    lstASrv1.TextMatrix(i, 1) = "n" Then
        itm = .TextMatrix(i, 0) & Space(75)
        itm = Left(itm, 7) & .TextMatrix(i, 3) & Space(75)
        itm = Left(itm, 9) & .TextMatrix(i, 1) & Space(75)
        itm = Left(itm, 18) & .TextMatrix(i, 6) & Space(75)
        itm = Left(itm, 24) & .TextMatrix(i, 4) & Space(75)
        itm = Left(itm, 35) & .TextMatrix(i, 5) & Space(75)
        itm = Left(itm, 45) & .TextMatrix(i, 2) & Space(75)
        itm = Left(itm, 49) & .TextMatrix(i, 9) & Space(75)
        itm = Left(itm, 74) & "p"
        lstTemp.AddItem itm
    End If
Next
End With

Call ApplClaim.PostReceivableServices(lstTemp, InvoiceId, ApptDate, "p", _
                                      InsurerId, DocId, RoomId, Srv, SrvItem)

For i = 0 To lstDelSrvs.ListCount - 1
    lstDelSrvs.List(i) = Space(74) & "S" & lstDelSrvs.List(i)
Next
Call ApplClaim.PostPurgedServices(lstDelSrvs)
Call ApplClaim.ReplacePayments(RcvId, lstDelSrvs, lstTemp)
Call ApplClaim.ArrangeServicesOrder(InvoiceId, lstSort)

Call ApplClaim.SetReceivableCosts(RcvId, RefDrId, BillDrId, False, BillOff, "")

Set ApplClaim = Nothing

'=======================
Dim ApplTemp As New ApplicationTemplates
Call ApplTemp.ApplClearZeroBalanceTransactions(RcvId)
Set ApplTemp = Nothing
'=======================

'Call CopyFGC(lstGen1, lstGenSave)
'Call CopyFGC(lstDiag, lstDiagSave)
'Call CopyFGC(lstASrv, lstASrvSave)

DoEvents
lstSrvs1.Visible = False

' autoreattach
'---------------------------------
Dim ApplList As New ApplicationAIList
Dim RS(2) As ADODB.Recordset
Set RS(0) = ApplList.getUnattachedPayments(ApptId)

Do Until RS(0).EOF
    Set RS(1) = Nothing
    Set RS(1) = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandText = _
                "SELECT distinct ps.itemid, ps.service, ps.modifier " & _
                "FROM PatientReceivables pr " & _
                "JOIN PatientReceivableServices ps ON pr.Invoice=ps.Invoice " & _
                "WHERE ps.Status <> 'X' and pr.appointmentid = " & ApptId & _
                " and ps.service = '" & RS(0)("paymentservice") & "' "
    Call RS(1).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    
    If RS(1).RecordCount > 1 Then
        RS(1).Filter = "modifier = '" & RS(0)("modifier") & "'"
    End If
    If RS(1).RecordCount = 1 Then

        Set RS(2) = Nothing
        Set RS(2) = CreateAdoRecordset
        MyPracticeRepositoryCmd.CommandText = _
                    "update patientreceivablepayments " & _
                    " Set PaymentServiceItem = " & RS(1)("itemid") & _
                    " Where PaymentId = " & RS(0)("paymentid")
        Call RS(2).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    End If
    
    RS(0).MoveNext
Loop
'--------------
Set RS(0) = ApplList.getUnattachedBills(ApptId)
    Do Until RS(0).EOF
        Set RS(1) = Nothing
        Set RS(1) = CreateAdoRecordset
        MyPracticeRepositoryCmd.CommandText = _
                    "SELECT distinct ps.itemid, ps.service , ps.modifier " & _
                    "FROM PatientReceivables pr " & _
                    "JOIN PatientReceivableServices ps ON pr.Invoice=ps.Invoice " & _
                    "WHERE ps.Status <> 'X' and pr.appointmentid = " & ApptId & _
                    " and ps.service = '" & RS(0)("service") & "' "
        Call RS(1).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
        
        If RS(1).RecordCount > 1 Then
            RS(1).Filter = "modifier = '" & RS(0)("modifier") & "'"
        End If
        If RS(1).RecordCount = 1 Then
            Set RS(2) = Nothing
            Set RS(2) = CreateAdoRecordset
            MyPracticeRepositoryCmd.CommandText = _
                        "update servicetransactions " & _
                        " Set serviceid = " & RS(1)("itemid") & _
                        " Where Id = '" & RS(0)("id") & "'"
            Call RS(2).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
        End If
        
        Set RS(2) = Nothing
        Set RS(2) = CreateAdoRecordset
        MyPracticeRepositoryCmd.CommandText = _
                    "select * from practicetransactionjournal " & _
                    "Where transactionid = '" & RS(0)("transactionid") & "'"
        Call RS(2).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
        
        If Not RS(2)("TransactionStatus") = "Z" Then
            Dim Transaction As New PracticeTransactionJournal
            Call Transaction.CloseTransportAction(RS(0)("transactionid"), RS(2)("TransactionStatus"))
            Set Transaction = Nothing
        
            Set RS(2) = Nothing
            Set RS(2) = CreateAdoRecordset
            MyPracticeRepositoryCmd.CommandText = _
                        "update practicetransactionjournal " & _
                        " Set transactionstatus = 'Z' " & _
                        " Where transactionid = '" & RS(0)("transactionid") & "'"
            Call RS(2).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
        End If


        RS(0).MoveNext
    Loop

'Set ApplList = Nothing
'---------------------------------

'updatercv
'Dim ApplList As New ApplicationAIList

Call ApplList.UpdateRcv(RcvId)
Set ApplList = Nothing


Dim memR As Integer
memR = lstSrvs1.TopRow
LoadPaymentsTotal ""
If lstSrvs1.Rows > 1 Then
    If Not memR < lstSrvs1.Rows Then memR = lstSrvs1.Rows - 1
    If Not memR = 0 Then lstSrvs1.TopRow = memR
End If
fpBtnSave.Enabled = True

endsub:
frEditClaim_Visible False
Updated = True
End Sub

Private Sub fpBtnView_Click()
frmSetting.Show 1
End Sub

Private Sub fpPmnt_Click(Index As Integer)

If Index = 2 And EditMode Then
    frmEventMsgs.Header = "Close Previous Billing Transaction(s)?"
    frmEventMsgs.AcceptText = "Exit, No change"
    frmEventMsgs.RejectText = "Save && Close Prev. Billing"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
    Select Case frmEventMsgs.Result
    Case 1
        Index = 0
    Case 4
        Exit Sub
    End Select
End If

Static Notfirsttime As Boolean

If Notfirsttime Then Exit Sub
Notfirsttime = True

Select Case Index
Case 1
    If CompletedPayment(1) Then
        Dim RR(1) As Integer
        RR(0) = 1
        RR(1) = lstPmnt(1).Rows - 1
        Call NextBill(lstSrvs2.TextMatrix(lstSrvs1.Row, 10), lstPat.TextMatrix(0, 1), lstPmntInfo.TextMatrix(1, 0), lstPmnt(1), RR)
        lstPmnt(1).col = 1
        editCellPmnt 1

    End If
Case 2
    Dim RcvId As Long
    RcvId = ConvertRcvId
    Dim i As Integer
    If EditMode Then
        If Not CompletedPayment(3) Then GoTo ExitSub
        Dim RetPay As PatientReceivablePayment
        'Do not allow posting of any payment type other than Payments into the On Account area
        If Not lstPmnt(0).TextMatrix(1, 0) = "" Then
            Set RetPay = New PatientReceivablePayment
            RetPay.PaymentId = lstSrvs2.TextMatrix(lstSrvs1.Row, 2)
            If (RetPay.RetrievePatientReceivablePayments) Then
                If RetPay.PaymentType <> "P" And lstPmnt(2).TextMatrix(1, 0) <> "0.00" Then
                    frmEventMsgs.InfoMessage "Only Payments can be posted as an On Account Credit"
                    frmEventMsgs.Show 1
                    DoEvents
                    GoTo ExitSub
                End If
            End If
        End If
        'delete payment row
        Set RetPay = New PatientReceivablePayment
        RetPay.PaymentId = lstSrvs2.TextMatrix(lstSrvs1.Row, 2)
        If (RetPay.RetrievePatientReceivablePayments) Then
            Call RetPay.KillPatientReceivablePayments
        End If
        Set RetPay = Nothing
        '------------------
        For i = lstSrvs1.Row To 0 Step -1
            If lstSrvs2.TextMatrix(i, 0) = "R" Then
                lstSrvs1.Row = i
                Exit For
            End If
        Next
        
        Dim ApplList As New ApplicationAIList
        ' get rid of all Insurer Transactions for this invoice
        Call ApplList.ApplSetAllInvTransStatus_5(RcvId, "I", "Z", "", "", False)
        ' get rid of all Patient Transactions for this invoice
        Call ApplList.ApplSetAllInvTransStatus_5(RcvId, "P", "Z", "", "", False)
        
        'updatercv
        Call ApplList.UpdateRcv(RcvId)
        Set ApplList = Nothing

    Else
        If Not CompletedPayment(2) Then
            DoEvents
            GoTo ExitSub
        End If
        If lstSrvs1.Rows > 0 Then
            frmBillServices.HiddenMode = True
            If (frmBillServices.LoadServices(CStr(RcvId), lstSrvs2.TextMatrix(lstSrvs1.Row, 3))) Then
                With frmBillServices
                    For i = 1 To lstPmnt(1).Rows - 1
        
                    If lstPmnt(1).TextMatrix(i, 13) = "" _
                    Or lstPmnt(1).TextMatrix(i, 14) = "" Then
                        .lstSrvs1.TextMatrix(i, 6) = "X"
                    Else
                        .lstSrvs1.TextMatrix(i, 4) = _
                        lstPmnt(1).TextMatrix(i, 11)
        
                        .lstSrvs1.TextMatrix(i, 5) = _
                        lstPmnt(1).TextMatrix(i, 13)
        
                        .lstSrvs1.TextMatrix(i, 6) = _
                        lstPmnt(1).TextMatrix(i, 14)
                    End If
                    Next
                    '.Show 1
                    Call .BatchOut_5(lstSrvs2.TextMatrix(lstSrvs1.Row, 3))
                End With
                frmBillServices.HiddenMode = False
                Unload frmBillServices
            End If
        End If
    End If
    
    If Not CompleteCurrentPatient_5 Then GoTo ExitSub
    If Not PostCredit Then GoTo ExitSub
    
    Dim memR As Integer
    memR = lstSrvs1.TopRow
    LoadPaymentsTotal ""
    If lstSrvs1.Rows > 1 Then
        If Not memR < lstSrvs1.Rows Then memR = lstSrvs1.Rows - 1
        If Not memR = 0 Then lstSrvs1.TopRow = memR
    End If
End Select

Updated = True
If Not Index = 1 Then frPmnt_Visible False


ExitSub:
Notfirsttime = False
End Sub

Private Function CompletedPayment(ind As Integer) As Boolean
Dim i As Integer
Dim s As Double
Dim Temp As String

If Not Entered Then
    frmEventMsgs.Header = "Nothing entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
    Exit Function
End If


frmEventMsgs.Header = ""
If Trim(lstPmnt(0).TextMatrix(1, 2)) = 0 Then
    For i = 1 To lstPmnt(1).Rows - 1
        s = s + lstPmnt(1).TextMatrix(i, 7)
    Next
    If Not s = 0 Then
        frmEventMsgs.Header = "Please enter payment amount."
        lstPmnt(0).col = 2
    End If
Else
    If myTrim(lstPmnt(0).TextMatrix(1, 1), 1) = "K" Then
        If Trim(lstPmnt(0).TextMatrix(1, 3)) = "" Then
            frmEventMsgs.Header = "Please enter a check number."
            lstPmnt(0).col = 3
        Else
            If mCheckExist = -1 Then
                Dim RetPay As New PatientReceivablePayment
                Dim mPaymentDate As String
                mPaymentDate = Format(lstPmnt(0).TextMatrix(1, 4), "yyyymmdd")
                Dim InsurerId As String
                Dim PayerType As String
                InsurerId = lstPmntInfo.TextMatrix(1, 0)
                'get payer info
                If IsNumeric(InsurerId) Then
                    PayerType = "I"
                Else
                    Select Case InsurerId
                    Case "Patient"
                        InsurerId = PatientId
                        PayerType = "P"
                    Case "Office"
                        InsurerId = 0
                        PayerType = "O"
                    End Select
                End If
                If RetPay.CheckExist(CDbl(lstPmnt(0).TextMatrix(1, 2)), _
                                          lstPmnt(0).TextMatrix(1, 3), _
                                          mPaymentDate, _
                                          Format(lstPmnt(0).TextMatrix(1, 5), "yyyymmdd"), PayerType) > 0 Then
                    
                    If EditMode Then
                        mCheckExist = RetPay.PaymentId
                    Else
                        frmEventMsgs.Header = "Check from " & _
                                               RetPay.getPayerName(RetPay.PaymentPayerType, RetPay.PaymentPayerId) & _
                                              " already in use.  Continue with this batch?"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = "New batch with diff check #"
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        DoEvents
                        If frmEventMsgs.Result = 2 Then
                            mCheckExist = RetPay.PaymentId
                            lstPmnt(0).TextMatrix(1, 4) = Mid(mPaymentDate, 5, 2) & "/" & Mid(mPaymentDate, 7, 2) & "/" & Mid(mPaymentDate, 1, 4)
                            frmEventMsgs.Header = ""
                        Else
                            lstPmnt(0).SetFocus
                            Set RetPay = Nothing
                            Exit Function
                        End If
                    End If
                Else
                    mCheckExist = 0
                End If
                Set RetPay = Nothing
            End If
        End If
    Else
        mCheckExist = 0
    End If
End If

If Not frmEventMsgs.Header = "" Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
    lstPmnt(0).SetFocus
    Exit Function
End If

s = 0

For i = 1 To lstPmnt(1).Rows - 1
    s = s + lstPmnt(1).TextMatrix(i, 7)
Next

If IsNumeric(lstPmnt(0).TextMatrix(1, 2)) And IsNumeric(s) Then
    Temp = CDbl(lstPmnt(0).TextMatrix(1, 2)) - CDbl(s)
    If CDbl(Temp) < 0 Then
        lblWB1.Visible = True
    Else
        lblWB1.Visible = False
    End If
End If


If lstPmnt(0).TextMatrix(1, 0) = "Office" Then
    For i = 1 To lstPmnt(1).Rows - 1
        If Not lstPmnt(1).TextMatrix(i, 7) = 0 Then
            frmEventMsgs.Header = "Office not valid for payments"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoEvents
            Exit Function
        End If
    Next
End If
If lstPmnt(0).TextMatrix(1, 0) = "Patient" Then
    For i = 1 To lstPmnt(1).Rows - 1
        If (lstPmnt(1).TextMatrix(i, 8) = 0 _
        And lstPmnt(1).TextMatrix(i, 9) = 0 _
        And lstPmnt(1).TextMatrix(i, 10)) = 0 Then
        Else
            If _
                  (lstPmnt(1).TextMatrix(i, 17) = 0 And (lstPmnt(1).TextMatrix(i, 19) = "") _
            Or Not lstPmnt(1).TextMatrix(i, 17) = 0 And lstPmnt(1).TextMatrix(i, 19) = "V") Then
            Else
                frmEventMsgs.Header = "Adjustments cannot be from the patient. Choose a different party."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                DoEvents
                Exit Function
            End If
        End If
    Next
End If

Dim checkOK As Boolean
For i = 1 To lstPmnt(1).Rows - 1
    checkOK = False
    If lstPmnt(1).TextMatrix(i, 17) = 0 Then
        If lstPmnt(1).TextMatrix(i, 19) = "" Then checkOK = True
    Else
        If Not lstPmnt(1).TextMatrix(i, 19) = "" Then checkOK = True
    End If
    
    If Not checkOK Then
        frmEventMsgs.Header = "Other Adj requires amount and type"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoEvents
        Exit Function
    End If
Next

If lstPmnt(0).TextMatrix(1, 0) = "Office" Then
    For i = 1 To lstPmnt(1).Rows - 1
        If lstPmnt(1).TextMatrix(i, 17) <> 0 And lstPmnt(1).TextMatrix(i, 19) = "P" Then
            frmEventMsgs.InfoMessage "Office not valid for payments"
            frmEventMsgs.Show 1
            DoEvents
            Exit Function
        End If
    Next
End If

If lstPmnt(0).TextMatrix(1, 0) = "Office" Then
    If Not lstPmnt(2).TextMatrix(1, 0) = 0 Then
        frmEventMsgs.InfoMessage "Office not valid for payments"
        frmEventMsgs.Show 1
        DoEvents
        Exit Function
    End If
End If

If ind > 1 Then
    Dim c As Integer
    For i = 1 To lstPmnt(1).Rows - 1
        For c = 13 To 14
            If lstPmnt(1).TextMatrix(1, c) = "?" Then
                frmEventMsgs.Header = "Complete Billing."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                DoEvents
                lstPmnt(1).Row = i
                lstPmnt(1).col = 19
                lstPmnt(1).SetFocus
                Exit Function
            End If
        Next
    Next
End If

CompletedPayment = True

End Function

Public Sub NextBill(Invoice As String, PatientId As String, InsurerId As String, lstSrv As MSFlexGrid, RowsRange() As Integer)
Dim Clm As ClaimInfo
Dim Srv As ServiceInfo
Dim i As Integer
Dim CollClm As New Collection
Dim CollSrv As New Collection
Dim CollSrvS As New Collection
Dim PmntBill As New CPaymentsBilling
Dim SQL As String
Dim RS As Recordset

Clm.PatientId = PatientId
Clm.InsurerId = InsurerId
'----------------------------------------------------------
'get ReceivableId
'----------------------------------------------------------
Dim ServiceDate As String
ServiceDate = lstSrv.TextMatrix(RowsRange(0), 1)
Dim ReceivableId As String
SQL = _
"Select * FROM PatientReceivables " & _
"where Invoice = '" & Invoice & "'" & _
"and PatientId = " & PatientId & _
" and Invoicedate = " & Format(ServiceDate, "yyyymmdd")
Set RS = GetRS(SQL)
If Not RS.RecordCount = 0 Then
    ReceivableId = RS("ReceivableId")
End If
'----------------------------------------------------------
Set CollClm = ClmToColl(Clm)

For i = RowsRange(0) To RowsRange(1)
    Srv.ServiceDate = ServiceDate
    Srv.Amount(1) = lstSrv.TextMatrix(i, 7)
    Srv.Amount(2) = lstSrv.TextMatrix(i, 8)
    Srv.Amount(3) = lstSrv.TextMatrix(i, 9)
    Srv.Amount(4) = lstSrv.TextMatrix(i, 10)
    Srv.Amount(5) = lstSrv.TextMatrix(i, 17)
    Srv.PaymentType(5) = lstSrv.TextMatrix(i, 19)
    Srv.ItemId = lstSrv.TextMatrix(i, 0)
    Srv.FrwBal = lstSrv.TextMatrix(i, 11)
    Srv.Service = lstSrv.TextMatrix(i, 2)
    Srv.ReceivableId = ReceivableId
    Srv.Invoice = Invoice
    
    
    Set CollSrv = Nothing
    Set CollSrv = New Collection
    Set CollSrv = SrvToColl(Srv)
    CollSrvS.Add CollSrv
Next

Call PmntBill.getForwardBalanceRules(CollClm, CollSrvS)
For i = 1 To CollSrvS.Count
     lstSrv.TextMatrix(RowsRange(0) + i - 1, 13) = CollSrvS(i)("Nx")
     lstSrv.TextMatrix(RowsRange(0) + i - 1, 14) = CollSrvS(i)("Ac")
Next
Set PmntBill = Nothing
End Sub

Private Function BilledParty(SrvId As Long) As String
Dim AppId As Long
AppId = lstSrvs2.TextMatrix(lstSrvs1.Row, 3)

Dim RS As ADODB.Recordset
Dim ApplList As New ApplicationAIList
Set RS = ApplList.getBilledParty(AppId, SrvId)
Set ApplList = Nothing

If RS.EOF Then Exit Function

If RS("Transactionref") = "P" Then
    BilledParty = "P"
Else
    BilledParty = RS("InsurerID")
End If
End Function

Private Function PostCredit() As Boolean

Dim CreditAmt As String
Dim CreditCmt As String
CreditAmt = lstPmnt(2).TextMatrix(1, 0)
CreditCmt = Trim(lstPmnt(2).TextMatrix(1, 1))

If CreditAmt = 0 And Not FromCreditInd Then
    PostCredit = True
    Exit Function
End If

Dim InsurerId As String
Dim CheckNumber As String
Dim PayDate As String
Dim PaymentMethod As String
Dim EOBDate As String
InsurerId = lstPmntInfo.TextMatrix(1, 0)
CheckNumber = Trim(lstPmnt(0).TextMatrix(1, 3))
PayDate = Trim(lstPmnt(0).TextMatrix(1, 4))
PaymentMethod = myTrim(lstPmnt(0).TextMatrix(1, 1), 1)
EOBDate = Trim(lstPmnt(0).TextMatrix(1, 5))


Dim RcvId As Long
Dim PType As String
Dim RcvOpenBalance As Double

'find receivable. if not exist create one
Dim RetRcv As New PatientReceivables
RetRcv.PatientId = PatientId
Do
    RcvId = RetRcv.GetDummyReceivable
    If RcvId = 0 Then
        RetRcv.ReceivableType = "S"
        RetRcv.InsurerId = 99999
        Call RetRcv.ApplyPatientReceivable
    Else
        Exit Do
    End If
Loop

'get payer info
If IsNumeric(InsurerId) Then
    PType = "I"
Else
    Select Case InsurerId
    Case "Patient"
        InsurerId = PatientId
        PType = "P"
    Case "Office"
        InsurerId = 0
        PType = "O"
    End Select
End If

Dim RetPay As New PatientReceivablePayment
'close prev credit
If FromCreditInd Then
    Dim RS As ADODB.Recordset
    RetPay.PaymentId = FromCreditID
    Set RS = RetPay.CreditListByID
    Do Until RS.EOF
        If RetPay.RetrievePatientReceivablePayments Then
            RcvOpenBalance = RetPay.PaymentAmount
            RetPay.PaymentType = ""
            Call RetPay.ApplyPatientReceivablePayments
        End If
        RS.MoveNext
    Loop
End If

'post credit
If CreditAmt > 0 Then
    RetPay.PaymentId = 0
    If RetPay.RetrievePatientReceivablePayments Then
        RcvOpenBalance = RcvOpenBalance - CreditAmt
        RetPay.ReceivableId = RcvId
        RetPay.PaymentPayerId = InsurerId
        RetPay.PaymentPayerType = PType
        RetPay.PaymentType = "P"
        RetPay.PaymentAmount = CreditAmt
        RetPay.PaymentDate = Format(PayDate, "yyyymmdd")
        RetPay.PaymentCheck = CheckNumber
        RetPay.PaymentRefId = CreditCmt
        RetPay.PaymentRefType = PaymentMethod
        RetPay.PaymentService = 0
        RetPay.PaymentCommentOn = False
        RetPay.PaymentFinancialType = "C"
        RetPay.PaymentServiceItem = 0
        RetPay.PaymentAssignBy = UserId
        RetPay.PaymentEOBDate = Format(EOBDate, "yyyymmdd")
        If IsNumeric(InsurerId) And PType = "I" Then
            RetPay.PaymentAppealNumber = CStr(RetRcv.PatientId)
        End If
        Call RetPay.ApplyPatientReceivablePayments
    End If
End If

'update openbalance
RetRcv.ReceivableId = RcvId
If RetRcv.RetrievePatientReceivable Then
    RetRcv.ReceivableBalance = RetRcv.ReceivableBalance + RcvOpenBalance
    Call RetRcv.ApplyPatientReceivable
End If
Set RetRcv = Nothing

Set RetPay = Nothing

PostCredit = True
End Function

Private Function CompleteCurrentPatient_5() As Boolean

Dim PatId As String
Dim InsurerId As String
Dim CheckNumber As String
Dim CheckAmount As String
Dim CheckDate As String

PatId = CStr(PatientId)
InsurerId = lstPmntInfo.TextMatrix(1, 0)
CheckNumber = Trim(lstPmnt(0).TextMatrix(1, 3))
CheckAmount = Trim(lstPmnt(0).TextMatrix(1, 2))
CheckDate = Trim(lstPmnt(0).TextMatrix(1, 4))


Dim CheckId As Long
Dim RunningCheckBalance As String


Dim pcnt As Integer
Dim d As Integer, ARef As Integer
Dim w As Integer, w1 As Integer
Dim zz As Integer, p As Integer
Dim z As Integer, ICol As Integer
Dim i As Integer, j As Integer
Dim SP As Integer, scnt As Integer

Dim ComOn As Boolean, MedOn As Boolean
Dim AppealOn As Boolean
Dim XOver1 As Boolean
Dim XOver2 As Boolean

Dim k As Single
Dim JAmt As String
' represents amount for all 4 status APBU
Dim PAmt(4) As Single
Dim PSrv(12) As String
Dim IAmt(12, 4) As Single
Dim ISrv(12, 12, 12) As String
Dim SrvCnt(12, 12) As Integer

Dim LocalBal As Single
Dim myCreditAmt As Single
Dim PBal As Single, IBal As Single

Dim PolHldId As String
Dim RcvIdBillParty As String
Dim RcvIdUna As String
Dim RcvId1 As String
Dim TempInsId1 As String
Dim a1 As String, a2 As String
Dim Cmt As String, st As String
Dim Srv As String, Amt As String
Dim ItemId As Long, p1 As String
Dim myRef As String, Temp As String
Dim RcvId As Long, InsId As String
Dim CurInvId As String, Rsn As String
Dim SendTo As String, InvId As String
Dim InsrId As Long, TrnId As String
Dim InsId1 As String, AplNum As String
Dim RecId As String, TempIns As String
Dim InvDt As String, EOBDate As String
Dim AdjCode As String, RefCode As String
Dim PaymentMethod As String
Dim PType As String

Dim RetSrv As PatientReceivableService
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim InsurerId1 As Long

Dim AFee As Single
Dim ChargesByFee As Single

CompleteCurrentPatient_5 = False
If (Trim(PatId) <> "") Then
    EOBDate = Trim(lstPmnt(0).TextMatrix(1, 5))
    CurInvId = ""
    If (lstPmnt(1).Visible) Then
        DoEvents
        Call DisplayDollarAmount(Trim(Str(MoneyLeft(CheckAmount))), RunningCheckBalance)
        Set ApplTemp = New ApplicationTemplates
        Set ApplList = New ApplicationAIList
        PaymentMethod = myTrim(lstPmnt(0).TextMatrix(1, 1), 1)

        If IsNumeric(InsurerId) Then
            Call ApplTemp.ApplGetInsured(PatientId, CLng(InsurerId), InsrId)
            RcvId = frmBillServices.getRcvId(CLng(InsurerId), lstSrvs2.TextMatrix(lstSrvs1.Row, 3))
            PType = "I"
        Else
            InsrId = PatientId
            If lstSrvs1.Rows > 0 Then RcvId = ConvertRcvId
            Select Case InsurerId
            Case "Patient"
                InsurerId1 = PatientId
                PType = "P"
            Case "Office"
                InsurerId1 = 0
                PType = "O"
            End Select
        End If
        'if receivable not exist create one
        If RcvId = 0 Then
            Dim newRcvID As Long
            If lstSrvs1.Rows > 0 Then
                Call ApplTemp.ApplBuildReceivable(ConvertRcvId, CLng(InsurerId), InsrId, 0, "", newRcvID)
            Else
                Call ApplTemp.ApplBuildReceivable(0, 0, InsrId, 0, "", newRcvID)
            End If
            RcvId = newRcvID
        End If

' Write the Check Record
        If mCheckExist = 0 Then
            If IsNumeric(InsurerId) Then
                Call ApplTemp.ApplPostCheckRecord_5(CheckId, CStr(InsurerId), "I", Trim(Str(CheckAmount)), RunningCheckBalance, CheckNumber, CheckDate, EOBDate)
            Else
                Call ApplTemp.ApplPostCheckRecord_5(CheckId, CStr(InsurerId1), PType, Trim(Str(CheckAmount)), RunningCheckBalance, CheckNumber, CheckDate, EOBDate)
            End If
        Else
            CheckId = mCheckExist
        End If
        If lstSrvs1.Rows > 0 Then InvId = lstSrvs2.TextMatrix(lstSrvs1.Row, 10)
        
        For i = 1 To lstPmnt(1).Rows - 1
            AFee = lstPmnt(1).TextMatrix(i, 6)
            ChargesByFee = ChargesByFee + lstPmnt(1).TextMatrix(i, 6)
            Dim dte As String
            If UCase(lstPmnt(1).TextMatrix(i, 19)) = "V" Then dte = Format(Now, "mm/dd/yyyy")
' Payments
            ItemId = Trim(lstPmnt(1).TextMatrix(i, 0))
            Srv = Trim(lstPmnt(1).TextMatrix(i, 2))
            ComOn = False
            If (val(Trim(lstPmnt(1).TextMatrix(i, 7))) <> 0) Then
                If (Trim(lstPmnt(1).TextMatrix(i, 2)) <> "") Then
                    If (Trim(RcvId) <> "") Then
                        Amt = Trim(lstPmnt(1).TextMatrix(i, 7))
                        Rsn = ""
                        Cmt = Trim(lstPmnt(1).TextMatrix(i, 15))
                        If (Trim(lstPmnt(1).TextMatrix(i, 16)) <> "") Then
                            ComOn = True
                        End If
                        If IsNumeric(InsurerId) Then
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, CLng(InsurerId), Amt, CheckDate, "P", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                            If (Trim(lstPmnt(1).TextMatrix(i, 6)) <> 0) Then
                                Temp = lstPmnt(1).TextMatrix(i, 6)
                                w1 = InStrPS(Temp, ".")
                                If (w1 > 0) Then
                                    w1 = InStrPS(w1 + 1, Temp, ".")
                                    If (w1 > 0) Then
                                        Temp = Left(Temp, w1 - 1)
                                    End If
                                End If
                                Call ApplTemp.ApplPostSrvAllowed(ItemId, val(Temp))
                            End If
                        Else
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, InsurerId1, Amt, CheckDate, "P", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
            Else
                If (PType = "P") Then
                    Call ApplList.ApplSetReceivableBalance(Trim(InvId), CheckDate, True)
                Else
                    Call ApplList.ApplSetReceivableBalance(Trim(InvId), "", True)
                End If
            End If
' Contractual Adjustment
            If (val(Trim(lstPmnt(1).TextMatrix(i, 8))) <> 0) Then
                If (Trim(lstPmnt(1).TextMatrix(i, 2)) <> "") Then
                    If (Trim(RcvId) <> "") Then
                        Amt = lstPmnt(1).TextMatrix(i, 8)
                        Rsn = "45" ' when is it A2 ?
                        Cmt = ""
                        If Not (ComOn) Then
                            Cmt = lstPmnt(1).TextMatrix(i, 15)
                            If (Trim(lstPmnt(1).TextMatrix(i, 16)) <> "") Then
                                ComOn = True
                            End If
                        End If
                        If IsNumeric(InsurerId) Then
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, CLng(InsurerId), Amt, CheckDate, "X", "K", dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        Else
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, InsurerId1, Amt, CheckDate, "X", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
            End If
' CoPay/CoIns
            If (val(Trim(lstPmnt(1).TextMatrix(i, 9))) <> 0) Then
                If (Trim(lstPmnt(1).TextMatrix(i, 2)) <> "") Then
                    If (Trim(RcvId) <> "") Then
                        Amt = lstPmnt(1).TextMatrix(i, 9)
                        Rsn = "2"
                        Cmt = ""
                        If Not (ComOn) Then
                            Cmt = lstPmnt(1).TextMatrix(i, 15)
                            If (Trim(lstPmnt(1).TextMatrix(i, 16)) <> "") Then
                                ComOn = True
                            End If
                        End If
                        If IsNumeric(InsurerId) Then
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, CLng(InsurerId), Amt, CheckDate, "|", "K", dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        Else
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, InsurerId1, Amt, CheckDate, "|", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                    End If
                End If
            End If
' Deductible
            If (val(Trim(lstPmnt(1).TextMatrix(i, 10))) <> 0) Then
                If (Trim(lstPmnt(1).TextMatrix(i, 2)) <> "") Then
                    If (Trim(RcvId) <> "") Then
                        Amt = lstPmnt(1).TextMatrix(i, 10)
                        Rsn = "1"
                        Cmt = ""
                        If Not (ComOn) Then
                            Cmt = lstPmnt(1).TextMatrix(i, 15)
                            If (Trim(lstPmnt(1).TextMatrix(i, 16)) <> "") Then
                                ComOn = True
                            End If
                        End If
                        If IsNumeric(InsurerId) Then
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, CLng(InsurerId), Amt, CheckDate, "!", "K", dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        Else
                            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, InsurerId1, Amt, CheckDate, "!", PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, "", _
                            AFee, True)
                        End If
                        
                    End If
                End If
            End If
' Other1/2/3 Adjustments/Info and any additional
            GoSub PostOtherAdjs
        Next i
                
    End If
    Set ApplTemp = Nothing
    Set ApplList = Nothing

'Removed because PatientReceivables is a view and this update is
'unnecessary since ChargesByFee is a calculated column.
'' update ChargesByFee
'    Dim RS As Recordset
'    Dim SQL As String
'    SQL = "update PatientReceivables set ChargesByFee = " & ChargesByFee & _
'          " where Invoice = '" & InvId & "'"
'    Set RS = GetRS(SQL)
    
    CompleteCurrentPatient_5 = True
End If
Exit Function

PostOtherAdjs:
        If (val(Trim(lstPmnt(1).TextMatrix(i, 17))) <> 0) Then
            If (Trim(lstPmnt(1).TextMatrix(i, 2)) <> "") Then
                If (Trim(RcvId) <> "") Then
                    Amt = lstPmnt(1).TextMatrix(i, 17)
                    Rsn = Trim(lstPmnt(1).TextMatrix(i, 18))
                    AdjCode = UCase(lstPmnt(1).TextMatrix(i, 19))
                    'I think this is code we added when we were hard coding _
                    additional payment types for the ERAs and were trying _
                    to standardize some of the types. _
                    We handled this w a database update; _
                    should not be part of the dynamic code. kb
                    'If (AdjCode = "W") Then
                    '    AdjCode = "8"       ' general writeoff
                    'End If
                    'If (AdjCode = "R") Then
                    '    AdjCode = "U"       ' general reversal
                    'End If
                    'If (AdjCode = "I") Then
                    '    AdjCode = "]"       ' general Informational
                    'End If
                    'If (AdjCode = "D") Then
                    '    AdjCode = "D"       ' general Denial
                    'End If
' Watch this because as the visible grid arrangement changes this may need to be adjusted to get appeal number in the second grid
                    AplNum = lstPmnt(1).TextMatrix(i, 11)
                    Cmt = ""
                    If Not (ComOn) Then
                        Cmt = lstPmnt(1).TextMatrix(i, 15)
                        If (Trim(lstPmnt(1).TextMatrix(i, 16)) <> "") Then
                            ComOn = True
                        End If
                    End If
                    If IsNumeric(InsurerId) Then
                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, CLng(InsurerId), Amt, CheckDate, AdjCode, "K", dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, Rsn, CheckId, EOBDate, AplNum, _
                        AFee, True)
                    Else
                        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, InvId, PType, InsurerId1, Amt, CheckDate, AdjCode, PaymentMethod, dte, CheckNumber, Cmt, Srv, ItemId, ComOn, UserId, False, "", CheckId, "", "", _
                        AFee, True)
                    End If
                End If
            End If
        End If
    Return
    
End Function

Private Function MoneyLeft(chkAmt As String) As Single
Dim i As Integer
Dim Amt As Single
Amt = 0
MoneyLeft = 0
For i = 1 To lstPmnt(1).Rows - 1
    Amt = Amt + (lstPmnt(1).TextMatrix(i, 7))
Next i
MoneyLeft = chkAmt - Amt
End Function




Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatFinancial, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
        piclblAlert.Visible = True
    Else
        piclblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub AddDiagnosisGrid()
Dim DisplayText As String, DisplayText2 As String
Dim RetrieveClinical As New PatientClinical
Dim i As Integer, q As Integer
Dim NewId As String, z As String

DiaLoadOn = True
lstDiag.Rows = 1

With RetrieveClinical
    .EyeContext = ""
    .Symptom = ""
    .Findings = ""
    .PatientId = PatientId
    .AppointmentId = lstSrvs2.TextMatrix(lstSrvs1.Row, 3)
    .ClinicalType = "BK"
    .ClinicalStatus = "-A"
    If (.FindPatientClinical11 > 0) Then
        i = 1
        While (.SelectPatientClinical(i)) And (q < 12)
            NewId = Trim(.Findings)
            z = InStrPS(NewId, ".")
            If (z > 0) Then
                z = InStrPS(z + 1, NewId, ".")
                If (z > 0) Then
                    NewId = Left(NewId, z - 1)
                End If
            End If
            If (val(Trim(.Symptom)) > 0) And (val(Trim(.Symptom)) < 25) Then
                If Not DisplayText = "" Then
                    DisplayText = DisplayText + vbTab
                    DisplayText2 = DisplayText2 + vbTab
                End If
                DisplayText = DisplayText + NewId
                DisplayText2 = DisplayText2 + CStr(.ClinicalType) + CStr(.ClinicalId)
                q = q + 1
            End If
            i = i + 1
        Wend
    End If
End With
Set RetrieveClinical = Nothing

lstDiag.AddItem DisplayText, 1
lstDiag.AddItem DisplayText2, 2
lstDiag.Height = lstDiag.RowHeight(0) + lstDiag.RowHeight(1)
DiaLoadOn = False
DiaFieldOrg = ""
DiaFirstKeyStroke = True
End Sub

Private Sub AddGenGrid()
Dim ThePre As Long
Dim PreCText As String
Dim PreId As Long
Dim ApplSch As New ApplicationScheduler
Call ApplSch.GetPreCert(PatientId, lstSrvs2.TextMatrix(lstSrvs1.Row, 3), PreId, PreCText)
Set ApplSch = Nothing

Dim over90i As Integer, over90 As String
Dim ApplList As New ApplicationAIList
Dim RcvId As Long
RcvId = ConvertRcvId
over90i = ApplList.ApplGet90Day(RcvId)
If Not over90i < 0 Then over90 = over90i + 1
Set ApplList = Nothing


Dim DisplayText As String
Dim r As Integer
r = lstSrvs1.Row
lstGen.Rows = 1
lstGen1.Rows = 1
DisplayText = lstSrvs2.TextMatrix(r, 9) + vbTab _
            + lstSrvs1.TextMatrix(r, 0) + vbTab _
            + lstSrvs1.TextMatrix(r, 1) + vbTab _
            + lstSrvs1.TextMatrix(r, 2) + vbTab _
            + lstSrvs1.TextMatrix(r, 3) + vbTab _
            + PreCText + vbTab _
            + lstSrvs1.TextMatrix(r, 4) + vbTab _
            + lstSrvs1.TextMatrix(r, 5) + vbTab _
            + CStr(over90) + vbTab _
            + lstSrvs1.TextMatrix(r, 10)

lstGen.AddItem DisplayText, 1

Dim i As Long
Set ApplList = New ApplicationAIList
With ApplList
    If .GetFinancialData(PatientId, "", "A", True, False, 1) > 0 Then
        i = 1
        Do While (.LoadFinancialData(i))
            If .ApplRcvId = RcvId Then
                DisplayText = Mid(lstGen.TextMatrix(1, 0), 7, 4) + Mid(lstGen.TextMatrix(1, 0), 1, 2) + Mid(lstGen.TextMatrix(1, 0), 4, 2) + vbTab _
                            + CStr(.ApplSchDrId) + vbTab _
                            + CStr(.ApplBillDrId) + vbTab _
                            + CStr(.ApplRefDrId) + vbTab _
                            + lstSrvs1.TextMatrix(r, 3) + vbTab _
                            + PreCText + vbTab _
                            + CStr(.ApplLocId) + vbTab _
                            + CStr(.ApplAttId) + vbTab
            Exit Do
            End If
            i = i + 1
        Loop
    End If
End With
Set ApplList = Nothing
lstGen1.AddItem DisplayText, 1

End Sub

Private Sub lstASrv_GotFocus()
editCell lstASrv
End Sub


Private Sub lstCrd_DblClick()
lstCrd.Visible = False

If lstCrd.ListIndex = 0 Then Exit Sub
If Not frPmnt(0).Visible Then Exit Sub
EditCellFlag = True
Dim RetPay As New PatientReceivablePayment
RetPay.PaymentId = myTrim(lstCrd.Text, 1)
If RetPay.RetrievePatientReceivablePayments Then
    lstPmnt(0).TextMatrix(1, 0) = Trim(Mid(lstCrd.Text, 1, 20))
    lstPmnt(0).TextMatrix(1, 1) = Trim(Mid(lstCrd.Text, 23, 5)) & Space(200) & RetPay.PaymentRefType
    lstPmnt(0).TextMatrix(1, 2) = Trim(Mid(lstCrd.Text, 30, 8))
    
    lstPmnt(2).TextMatrix(1, 0) = lstPmnt(0).TextMatrix(1, 2)
    
    lstPmnt(0).TextMatrix(1, 3) = Trim(Mid(lstCrd.Text, 40, 19))
    lstPmnt(0).TextMatrix(1, 4) = Trim(Mid(lstCrd.Text, 61, 10))
    If RetPay.PaymentEOBDate = "" Then
        lstPmnt(0).TextMatrix(1, 5) = ""
    Else
        lstPmnt(0).TextMatrix(1, 5) = Mid(RetPay.PaymentEOBDate, 5, 2) & "/" & _
                                      Mid(RetPay.PaymentEOBDate, 7, 2) & "/" & _
                                      Mid(RetPay.PaymentEOBDate, 1, 4)
    End If
    lstPmnt(2).TextMatrix(1, 1) = Trim(Mid(lstCrd.Text, 73, 36))
    Select Case RetPay.PaymentPayerType
    Case "I"
        lstPmntInfo.TextMatrix(1, 0) = RetPay.PaymentPayerId
    Case "P"
        lstPmntInfo.TextMatrix(1, 0) = "Patient"
    Case "O"
        lstPmntInfo.TextMatrix(1, 0) = "Office"
    End Select
    lstPmnt(0).col = 4
    lstPmnt(0).SetFocus
    FromCredit True
End If
Set RetPay = Nothing

End Sub

Private Sub FromCredit(stat As Boolean)
FromCreditInd = stat
If FromCreditInd Then
    lstPat.CellBackColor = vbRed
    FromCreditID = myTrim(lstCrd.Text, 1)
Else
    lstPat.CellBackColor = vbWhite
    FromCreditID = ""
End If
End Sub


Private Sub lstCrd_LostFocus()
lstCrd.Visible = False
End Sub

Private Sub lstDD_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case vbKeyReturn, vbKeyEscape, vbKeyTab
    If KeyCode = vbKeyEscape Then txt.Text = currentFGC.Text
    lstDD.Visible = False
    DD.Caption = "q"
    txt.SetFocus
End Select

End Sub

Private Sub lstDD_KeyUp(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case vbKeySpace
    Call lstDD_MouseDown(0, 0, 0, 0)
End Select

End Sub

Private Sub lstDD_LostFocus()
lstDD.Visible = False

End Sub

Private Sub lstDD_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim i As Integer, s As Integer
Dim newV As String
Select Case currentFGC.Name
Case "lstASrv"

    If lstDD.SelCount > 4 Then
        frmEventMsgs.Header = "Can't link service to more than 4 diagnoses"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = ""
        frmEventMsgs.CancelText = "OK"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
    newV = txt.Text
    For i = 0 To lstDD.ListCount - 1
        If lstDD.Selected(i) Then
            s = InStrPS(1, txt.Text, CStr(i + 1))
            If s = 0 Then
                newV = newV & CStr(i + 1)
            End If
        Else
            s = InStrPS(1, txt.Text, CStr(i + 1))
            If s > 0 Then
                newV = Mid(newV, 1, s - 1) & Mid(newV, s + 1)
            End If
        End If
    Next
    
    txt.Text = newV
End Select
End Sub

Private Sub lstDiag_GotFocus()
editCell lstDiag
End Sub

Private Sub lstDiag_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Exit Sub




If Button = vbRightButton Then
    DragControl lstDiag
End If
End Sub

Private Sub lstDiag_Click()
Exit Sub

Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstDiag.RowSel
CurrentCol = lstDiag.ColSel
If (CurrentRow > 0) And (CurrentCol <> 0) Then
    If (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Exit") Then
        If (CurrentRow = lstDiag.Rows - 1) Then
            lstDiag.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    ElseIf (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Search") Then
        If (CurrentRow = lstDiag.Rows - 1) Then
            If (DiaRow > 0) And (DiaRow < lstDiag.Rows - 1) Then
                Call TriggerSearch("D")
            End If
        End If
    ElseIf (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Clear") Then
        If (CurrentRow = lstDiag.Rows - 1) Then
            If (DiaRow > 0) And (DiaRow < lstDiag.Rows - 1) Then
                lstDiag.TextMatrix(DiaRow, DiaCol) = ""
                DiaRow = 0
                DiaCol = 0
            End If
        End If
    ElseIf (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Save") Then
' Save Editting
        If (CurrentRow = lstDiag.Rows - 1) Then
            lstDiag.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    Else
        DiaRow = lstDiag.RowSel
        DiaCol = lstDiag.ColSel
    End If
End If
End Sub

Private Sub lstDiag_EnterCell()
editCell lstDiag
Exit Sub



Dim g As Integer
Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstDiag.RowSel
CurrentCol = lstDiag.ColSel
If (CurrentRow > 0) And (CurrentCol <> 0) Then
    If (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Exit") Then
        If (CurrentRow = lstDiag.Rows) Then
            lstDiag.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    ElseIf (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Search") Then
    
    ElseIf (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Clear") Then
        If (CurrentRow = lstDiag.Rows) Then
            If (DiaRow > 0) Then
                lstDiag.TextMatrix(DiaRow, DiaCol) = ""
                DiaRow = 0
                DiaCol = 0
            End If
        End If
    ElseIf (lstDiag.TextMatrix(CurrentRow, CurrentCol) = "Save") Then
' Save Editting
        If (CurrentRow = lstDiag.Rows) Then
            lstDiag.Visible = False
            'enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    Else
        DiaRow = lstDiag.RowSel
        DiaCol = lstDiag.ColSel
        DiaFieldOrg = lstDiag.TextMatrix(DiaRow, DiaCol)
    End If
End If
End Sub

Private Sub lstDiag_KeyPress(KeyAscii As Integer)
Exit Sub





Dim Temp As String
Dim CurrentRow As Integer
Dim CurrentCol As Integer
SrvRow = lstDiag.RowSel
SrvCol = lstDiag.ColSel
CurrentRow = lstDiag.RowSel
CurrentCol = lstDiag.ColSel
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call lstDiag_LeaveCell
Else
    If (CurrentCol <> 0) Then
        If (DiaFirstKeyStroke) Then
            Temp = lstDiag.TextMatrix(CurrentRow, CurrentCol)
            DiaFieldOrg = Temp
            lstDiag.TextMatrix(CurrentRow, CurrentCol) = ""
            DiaFirstKeyStroke = False
        End If
        Temp = lstDiag.TextMatrix(CurrentRow, CurrentCol)
        If (KeyAscii = vbKeyBack) Then
            If (Len(Temp) > 1) Then
                Temp = Left(Temp, Len(Temp) - 1)
            Else
                Temp = ""
            End If
        Else
            Temp = Temp + Chr(KeyAscii)
        End If
        lstDiag.TextMatrix(CurrentRow, CurrentCol) = UCase(Temp)
    End If
End If
End Sub

Private Sub lstDiag_LeaveCell()
Exit Sub


Dim Temp As String
Dim t1 As String, t2 As String
Dim t3 As String, t4 As String
Dim t5 As String, t6 As String, t7 As String
Dim CurrentRow As Integer
Dim CurrentCol As Integer
Dim ApplTemp As ApplicationTemplates
CurrentRow = lstDiag.RowSel
CurrentCol = lstDiag.ColSel
If Not (DiaLoadOn) And (CurrentRow <> lstDiag.Rows - 1) And (CurrentCol <> 0) Then
    If (CurrentRow > 0) Then
        Temp = Trim(lstDiag.TextMatrix(CurrentRow, CurrentCol))
        If (Trim(Temp) <> "") Then
            Set ApplTemp = New ApplicationTemplates
            If Not (ApplTemp.ApplGetDiag(Temp, t1, t2, t3, t4, t5, t6, t7)) Then
                MsgBox "Invalid Diagnosis"
                lstDiag.TextMatrix(CurrentRow, CurrentCol) = DiaFieldOrg
                lstDiag.ColSel = CurrentCol
            Else
                lstDiag.TextMatrix(CurrentRow, CurrentCol) = Temp
            End If
            Set ApplTemp = Nothing
        End If
    End If
End If
End Sub

Private Sub AddServiceGrid(st As Integer, ed As Integer)
Dim i As Integer
Dim p As Integer
Dim j As Integer
Dim cnt As Integer
Dim MyQty As Integer
Dim myAmt As Single
Dim MyFee As Single
Dim MyChg As Single
Dim Temp As String
Dim Temp1 As String
Dim EmptyText As String
Dim FinalText As String
Dim DisplayText As String
'lstInfo.Enabled = False

SrvLoadOn = True
cnt = 0
lstBSrv.Clear
lstASrv.Rows = 1
lstASrv1.Rows = 1
  
For i = st To ed
    If (Trim(lstSrvs2.TextMatrix(i, 0)) = "S") Then
        MyChg = val(Trim(lstSrvs1.TextMatrix(i, 4)))
        MyQty = val(Trim(Mid(lstSrvs1.TextMatrix(i, 5), 5, Len(lstSrvs1.TextMatrix(i, 6)))))
        myAmt = MyChg * MyQty
        MyFee = val(Trim(lstSrvs2.TextMatrix(i, 10)))
        Call DisplayDollarAmount(Trim(Str(myAmt)), Temp)
        Call DisplayDollarAmount(Trim(Str(MyFee)), Temp1)
        DisplayText = lstSrvs1.TextMatrix(i, 2) + vbTab _
                    + lstSrvs1.TextMatrix(i, 6) + vbTab _
                    + lstSrvs2.TextMatrix(i, 7) + vbTab _
                    + lstSrvs2.TextMatrix(i, 8) + vbTab _
                    + lstSrvs2.TextMatrix(i, 9) + vbTab _
                    + lstSrvs2.TextMatrix(i, 10) + vbTab _
                    + lstSrvs1.TextMatrix(i, 5) + vbTab _
                    + lstSrvs1.TextMatrix(i, 7) + vbTab _
                    + lstSrvs1.TextMatrix(i, 9) + vbTab _
                    + lstSrvs2.TextMatrix(i, 11) + vbTab _
                    + lstSrvs1.TextMatrix(i, 10) + vbTab _
                    + "" + vbTab _
                    + "" + vbTab
        cnt = cnt + 1
        lstASrv.AddItem DisplayText, cnt
        
        DisplayText = lstSrvs2.TextMatrix(i, 1)
        lstASrv1.AddItem DisplayText, cnt
' additional info
'        DisplayText = lstSrvs2.TextMatrix(i, 1)
'        Dim RetrieveReceivables As New PatientReceivables
'        RetrieveReceivables.ReceivableId = DisplayText
'        If (RetrieveReceivables.RetrievePatientReceivable) Then
'            DisplayText = DisplayText + vbTab _
'                    + RetrieveReceivables.ReceivableAccidentType + vbTab _
'                    + RetrieveReceivables.ReceivableAccidentState + vbTab _
'                    + RetrieveReceivables.ReceivableHospitalAdmission + vbTab _
'                    + RetrieveReceivables.ReceivableHospitalDismissal + vbTab _
'                    + RetrieveReceivables.ReceivableDisability + vbTab _
'                    + RetrieveReceivables.ReceivableRsnType + vbTab _
'                    + RetrieveReceivables.ReceivableRsnDate + vbTab _
'                    + RetrieveReceivables.ReceivableConsDate + vbTab _
'                    + RetrieveReceivables.ReceivablePrevCond
'        End If
'        Set RetrieveReceivables = Nothing
                                    
    End If
Next i
EmptyText = "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + "" + vbTab _
          + ""
cnt = cnt + 1
lstASrv.AddItem EmptyText, lstASrv.Rows
lstASrv1.AddItem "", lstASrv1.Rows
lstBSrv.AddItem Trim(Str(0)), lstBSrv.Rows

lstASrv.Rows = cnt + 1
SrvLoadOn = False
SrvFieldOrg = ""
SrvFirstKeyStroke = True
End Sub

Private Sub lstASrv_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Exit Sub

If Button = vbRightButton Then
    DragControl lstASrv
End If
End Sub

Private Sub lstASrv_Click()
Exit Sub





Dim g As Integer
Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstASrv.RowSel
CurrentCol = lstASrv.ColSel
If (CurrentRow > 0) Then
    If (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Exit") Then
        If (CurrentRow = lstASrv.Rows - 1) Then
            lstASrv.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    ElseIf (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Search") Then
        If (CurrentRow = lstASrv.Rows - 1) Then
            If (SrvRow > 0) And (SrvRow < lstASrv.Rows - 1) Then
                Call TriggerSearch("P")
            End If
        End If
    ElseIf (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Clear") Then
        If (CurrentRow = lstASrv.Rows - 1) Then
            If (SrvRow > 0) And (SrvRow < lstASrv.Rows - 1) Then
                For g = 0 To lstASrv.Cols - 1
                    lstASrv.TextMatrix(SrvRow, g) = ""
                Next g
                SrvRow = 0
                SrvCol = 0
            End If
        End If
    ElseIf (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Save") Then
' Save Editting
        If (CurrentRow = lstASrv.Rows - 1) Then
            lstASrv.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    Else
        SrvRow = lstASrv.RowSel
        SrvCol = lstASrv.ColSel
        If (CurrentCol = 8) Then
            Call TriggerServiceNote(SrvRow)
        End If
    End If
End If
End Sub

Private Sub lstASrv_EnterCell()
editCell lstASrv
Exit Sub

Dim g As Integer
Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstASrv.RowSel
CurrentCol = lstASrv.ColSel
If (CurrentRow > 0) Then
    If (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Exit") Then
        If (CurrentRow = lstASrv.Rows) Then
            lstASrv.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    ElseIf (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Search") Then
    
    ElseIf (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Clear") Then
        If (CurrentRow = lstASrv.Rows) Then
            If (SrvRow > 0) Then
                For g = 0 To lstASrv.Cols - 1
                    lstASrv.TextMatrix(SrvRow, g) = ""
                Next g
                SrvRow = 0
                SrvCol = 0
            End If
        End If
    ElseIf (lstASrv.TextMatrix(CurrentRow, CurrentCol) = "Save") Then
' Save Editting
        If (CurrentRow = lstASrv.Rows) Then
            lstASrv.Visible = False
            'lstInfo.Enabled = True
            lstSrvs1.Enabled = True
            cmdDone.Enabled = True
            cmdMClm.Enabled = True
            cmdNotes.Enabled = True
        End If
    Else
        SrvRow = lstASrv.RowSel
        SrvCol = lstASrv.ColSel
        SrvFieldOrg = lstASrv.TextMatrix(SrvRow, SrvCol)
    End If
End If
End Sub

Private Sub lstASrv_KeyPress(KeyAscii As Integer)
Exit Sub



Dim Temp As String
Dim CurrentRow As Integer
Dim CurrentCol As Integer
SrvRow = lstASrv.RowSel
SrvCol = lstASrv.ColSel
CurrentRow = lstASrv.RowSel
CurrentCol = lstASrv.ColSel
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call lstASrv_LeaveCell
Else
    If (SrvFirstKeyStroke) Then
        Temp = lstASrv.TextMatrix(CurrentRow, CurrentCol)
        SrvFieldOrg = Temp
        lstASrv.TextMatrix(CurrentRow, CurrentCol) = ""
        SrvFirstKeyStroke = False
    End If
    Temp = lstASrv.TextMatrix(CurrentRow, CurrentCol)
    If (KeyAscii = vbKeyBack) Then
        If (Len(Temp) > 1) Then
            Temp = Left(Temp, Len(Temp) - 1)
        Else
            Temp = ""
        End If
    Else
        Temp = Temp + Chr(KeyAscii)
    End If
    lstASrv.TextMatrix(CurrentRow, CurrentCol) = UCase(Temp)
End If
End Sub

Private Sub lstASrv_LeaveCell()
Exit Sub



Dim InsId As Long
Dim Cost As Single
Dim AChg As Single
Dim AFee As Single
Dim APOS As String
Dim Temp As String
Dim Temp1 As String
Dim EmptyText As String
Dim CurrentRow As Integer
Dim CurrentCol As Integer
Dim ApplList As ApplicationAIList
CurrentRow = lstASrv.RowSel
CurrentCol = lstASrv.ColSel
If Not (SrvLoadOn) And (CurrentRow <> lstASrv.Rows - 1) Then
    If (CurrentRow > 0) Then
        If (CurrentCol = 0) Then
' verify service
            Temp = Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol))
            Set ApplList = New ApplicationAIList
            If Not (ApplList.ApplVerifyService(Temp, AChg, APOS)) Then
                MsgBox "Invalid Service"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                lstASrv.ColSel = CurrentCol
            Else
                If (SrvFieldOrg = "") And (Trim(Temp) <> "") Then
                    Call ApplList.ApplVerifyFee(Temp, InsId, AFee)
                    Call DisplayDollarAmount(Trim(Str(AChg)), Temp)
                    Call DisplayDollarAmount(Trim(Str(AFee)), Temp1)
                    lstASrv.TextMatrix(CurrentRow, 1) = ""
                    lstASrv.TextMatrix(CurrentRow, 2) = ""
                    lstASrv.TextMatrix(CurrentRow, 3) = ""
                    lstASrv.TextMatrix(CurrentRow, 4) = Trim(Temp)
                    lstASrv.TextMatrix(CurrentRow, 5) = Trim(Temp1)
                    lstASrv.TextMatrix(CurrentRow, 6) = "1"
                    lstASrv.TextMatrix(CurrentRow, 7) = Trim(Temp)
                    lstASrv.TextMatrix(CurrentRow, 8) = ""
                    lstASrv.TextMatrix(CurrentRow, 9) = ""
                    Call InsertNewServiceLine(CurrentRow + 1)
                End If
            End If
            Set ApplList = Nothing
        ElseIf (CurrentCol = 1) Then
' verify Mods
            Temp = Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol))
            Set ApplList = New ApplicationAIList
            If Not (ApplList.ApplVerifyModifier(Temp)) Then
                MsgBox "Invalid Modifier"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                lstASrv.ColSel = CurrentCol
            End If
            Set ApplList = Nothing
        ElseIf (CurrentCol = 2) Then
' verify Links
            If Not (IsNumeric(lstASrv.TextMatrix(CurrentRow, CurrentCol))) And (Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol)) <> "") Then
                MsgBox "Invalid Link Diagnosis"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = ""
                lstASrv.ColSel = CurrentCol
            Else
                If ((InStrPS(lstASrv.TextMatrix(CurrentRow, CurrentCol), "7") <> 0) Or _
                    (InStrPS(lstASrv.TextMatrix(CurrentRow, CurrentCol), "8") <> 0) Or _
                    (InStrPS(lstASrv.TextMatrix(CurrentRow, CurrentCol), "9") <> 0) Or _
                    (InStrPS(lstASrv.TextMatrix(CurrentRow, CurrentCol), "0") <> 0)) And _
                    (Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol)) <> "") Then
                    MsgBox "Invalid Link Diagnosis"
                    lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                    lstASrv.ColSel = CurrentCol
                End If
            End If
        ElseIf (CurrentCol = 3) Then
' verify POS
            Temp = Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol))
            Set ApplList = New ApplicationAIList
            If Not (ApplList.ApplVerifyPOS(Temp)) Then
                MsgBox "Invalid Place Of Service"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                lstASrv.ColSel = CurrentCol
            End If
            Set ApplList = Nothing
        ElseIf (CurrentCol = 4) Then
' verify Charge
            If Not (IsNumeric(lstASrv.TextMatrix(CurrentRow, CurrentCol))) And (Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol)) <> "") Then
                MsgBox "Invalid Charge Amount"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                lstASrv.ColSel = CurrentCol
            Else
                Cost = val(Trim(lstASrv.TextMatrix(CurrentRow, 4)))
                Call DisplayDollarAmount(Trim(Str(Cost)), Temp)
                lstASrv.TextMatrix(CurrentRow, 4) = Trim(Temp)
                
                Cost = val(Trim(lstASrv.TextMatrix(CurrentRow, 4))) * val(Trim(lstASrv.TextMatrix(CurrentRow, 6)))
                Call DisplayDollarAmount(Trim(Str(Cost)), Temp)
                lstASrv.TextMatrix(CurrentRow, 7) = Trim(Temp)
            End If
        ElseIf (CurrentCol = 5) Then
' verify Allowable
            If Not (IsNumeric(lstASrv.TextMatrix(CurrentRow, CurrentCol))) And (Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol)) <> "") Then
                MsgBox "Invalid Fee Amount"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                lstASrv.ColSel = CurrentCol
            Else
                Cost = val(Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol)))
                Call DisplayDollarAmount(Trim(Str(Cost)), Temp)
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = Trim(Temp)
            End If
        ElseIf (CurrentCol = 6) Then
' verify Qty
            If Not (IsNumeric(lstASrv.TextMatrix(CurrentRow, CurrentCol))) And (Trim(lstASrv.TextMatrix(CurrentRow, CurrentCol)) <> "") Then
                MsgBox "Invalid Quantity"
                lstASrv.TextMatrix(CurrentRow, CurrentCol) = SrvFieldOrg
                lstASrv.ColSel = CurrentCol
            Else
                Cost = val(Trim(lstASrv.TextMatrix(CurrentRow, 4))) * val(Trim(lstASrv.TextMatrix(CurrentRow, 6)))
                Call DisplayDollarAmount(Trim(Str(Cost)), Temp)
                lstASrv.TextMatrix(CurrentRow, 7) = Trim(Temp)
            End If
        End If
    End If
End If
End Sub




Private Sub lstDiagDet_KeyPress(KeyAscii As Integer)
lstDiagDet.Visible = False
End Sub

Private Sub lstDiagDet_LostFocus()
lstDiagDet.Visible = False
End Sub

Private Sub lstDiagDet_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
lstDiagDet.Visible = False
End Sub

Private Sub lstP_DblClick()

If Not lstP.ListIndex = 0 Then
    FromCredit False
    If lstPmntInd = 0 _
    And lstPmnt(0).col = 0 Then
        txt1.Text = Mid(lstP.Text, 1, 25)
        If Not lstPmnt(0).TextMatrix(1, 0) = txt1.Text Then
            lstPmnt(0).TextMatrix(1, 1) = "Check" & Space(200) & "K"
        End If
        
        lstPmnt(0).TextMatrix(1, 0) = txt1.Text
        lstPmntInfo.TextMatrix(1, 0) = myTrim(lstP.Text, 1)

    Else
        txt1.Text = lstP.Text
    End If
End If
txt1.SetFocus

End Sub

Private Sub lstP_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case vbKeyReturn
    Call lstP_DblClick
Case vbKeyEscape, vbKeyTab
    txt1.SetFocus
End Select

End Sub

Private Sub lstP_LostFocus()
lstP.Visible = False
End Sub

Private Sub lstPat_DblClick()
If lstPat.col = 6 Then
    
    Dim CL As ADODB.Recordset
    Dim RetPay As New PatientReceivablePayment
    Set CL = RetPay.CreditListByPatient(PatientId)
    Set RetPay = Nothing
    
    Dim ln(6) As String * 50
    Dim LnD As String
    LnD = "  "
    
    Dim ApplList As New ApplicationAIList
    cmbTemp.Clear
    Call ApplList.ApplGetCodes("PAYABLETYPE", True, cmbTemp)
    Set ApplList = Nothing
    Dim PType As New Collection
    Dim p As Integer
    For p = 0 To cmbTemp.ListCount - 1
        PType.Add cmbTemp.List(p), myTrim(cmbTemp.List(p), 1)
    Next
    
    With lstCrd
        .Clear
''''''        .AddItem "----+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2"
        .AddItem "Close list"
        
        Dim InsRec As New Insurer
        Do Until CL.EOF
            Select Case CL("PayerType")
            Case "P"
                ln(0) = "Patient"
            Case "O"
                ln(0) = "Office"
            Case "I"
                InsRec.InsurerId = Trim(CL("PayerID"))
                If InsRec.RetrieveInsurer Then
                    ln(0) = InsRec.InsurerName
                End If

            End Select
            
            ln(1) = PType(CL("PaymentRefType"))
            ln(2) = Format(CL("PaymentAmount"), "#####0.00")
            ln(2) = Space(8 - Len(Trim(ln(2)))) & ln(2)
            If (CL("PaymentCheck") <> "" And Not IsNull(CL("PaymentCheck"))) Then
                ln(3) = CL("PaymentCheck")
            Else
                ln(3) = ""
            End If
            ln(4) = Mid(CL("PaymentDate"), 5, 2) & "/" & Mid(CL("PaymentDate"), 7, 2) & "/" & Mid(CL("PaymentDate"), 1, 4)
            ln(5) = CL("PaymentRefID")
            ln(6) = CL("PaymentID")
            .AddItem Mid(ln(0), 1, 20) & LnD & _
                     Mid(ln(1), 1, 5) & LnD & _
                     Mid(ln(2), 1, 8) & LnD & _
                     Mid(ln(3), 1, 19) & LnD & _
                     Mid(ln(4), 1, 10) & LnD & _
                     Mid(ln(5), 1, 36) & _
                     Space(20) & Trim(ln(6))
            CL.MoveNext
        Loop
        Set InsRec = Nothing
        
        .Left = lstPat.Left
        .Width = lstPat.Width
        .Top = lstPat.Top + lstPat.Height
        .ListIndex = 0
        .Visible = True
        .SetFocus
    End With
End If
End Sub

Private Sub lstPmnt_EnterCell(Index As Integer)
editCellPmnt Index
End Sub

Private Sub lstPmnt_GotFocus(Index As Integer)
editCellPmnt Index
End Sub


Private Sub lstSearch_DblClick()
If lstSearch.ListIndex > 0 Then
    txt.Text = myTrim(lstSearch.Text)
    If currentFGC.Name = "lstGen" Then
        
        Select Case lstGen.col
        Case 3
            If lstSearch.ListIndex = 1 Then
                txt.Text = ""
                lstGen1.TextMatrix(1, lstGen.col) = 0
            Else
                lstGen1.TextMatrix(1, lstGen.col) = myTrim(lstSearch.Text, 1)
            End If
        Case 1, 2, 5, 6, 7
            lstGen1.TextMatrix(1, lstGen.col) = myTrim(lstSearch.Text, 1)
        Case 8
            Dim ApplList As New ApplicationAIList
            Dim RetRcv As New PatientReceivables
            Dim i As Integer
            If RetRcv.GetPatientReceivableByInvoice(lstSrvs2.TextMatrix(lstSrvs1.Row, 10)) Then
                i = 1
                While RetRcv.SelectPatientReceivable(i)
                    Call ApplList.ApplSet90Day(RetRcv.ReceivableId, txt.Text)
                    i = i + 1
                Wend
            End If
            Set ApplList = Nothing
            Set RetRcv = Nothing
        End Select
    End If
End If

If DD.Visible Then
    DDClick
    DD.Caption = "q"
Else
    lstSearch.Visible = False
    txt.SetFocus
End If
End Sub

Private Sub lstGen_EnterCell()
editCell lstGen

End Sub

Private Sub lstGen_GotFocus()
editCell lstGen

End Sub

Private Sub lstSearch_GotFocus()
Select Case currentFGC.Name
Case "lstGen"
    Dim i As Integer
    Select Case lstGen.col
    Case 4
    Case 8
        If IsNumeric(txt.Text) Then
            i = txt.Text
        Else
            i = 1
        End If
        lstSearch.Selected(i) = True
    Case Else
        With lstSearch
            For i = 1 To .ListCount - 1
                .Selected(i) = (myTrim(.List(i), 1) = lstGen1.TextMatrix(1, lstGen.col))
            Next
        End With
    End Select
    Exit Sub
End Select
lstSearch.Selected(0) = True
End Sub


Private Sub lstSearch_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case vbKeyReturn
    If currentFGC.Name = "lstGen" _
    And currentFGC.col = 4 _
    And lstSearch.ListIndex > 0 Then
        Call lstSearch_MouseUp(0, 0, 0, 0)
    Else
        Call lstSearch_DblClick
    End If
Case vbKeyEscape, vbKeyTab
    DD.Caption = "q"
    lstSearch.Visible = False
    txt.SetFocus
End Select

End Sub

Private Sub lstSearch_LostFocus()
lstSearch.Visible = False
End Sub

Private Sub lstSearch_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Select Case currentFGC.Name
Case "lstGen"
    Select Case lstGen.col
    Case 4
        If lstSearch.ListIndex > 0 Then
            Dim CmdBtns As New CommandBtns
            Call CmdBtns.EditReferral(lstSearch, txt, PatientId, lstSrvs2.TextMatrix(lstSrvs1.Row, 3), CurrRefId)
            Set CmdBtns = Nothing
        End If
    End Select
End Select
End Sub

Private Sub lstSearch_old_Click()
Dim Temp As String
If (lstSearch_old.ListIndex = 0) Then
    lstSearch_old.Visible = False
Else
    If (Left(lstSearch_old.List(0), 1) = "Z") Then
        If (lstSearch_old.ListIndex > 0) Then
            Temp = Trim(Left(lstSearch_old.List(lstSearch_old.ListIndex), 10))
            lstASrv.TextMatrix(SrvRow, 0) = Temp
            lstASrv.RowSel = SrvRow
            lstASrv.ColSel = 0
            Call lstASrv_KeyPress(13)
            lstSearch_old.Visible = False
        End If
    Else
        If (lstSearch_old.ListIndex > 0) Then
            Temp = Trim(Left(lstSearch_old.List(lstSearch_old.ListIndex), 9))
            lstDiag.TextMatrix(1, DiaCol) = Temp
            lstDiag.RowSel = 1
            lstDiag.ColSel = DiaCol
            Call lstDiag_KeyPress(13)
            lstSearch_old.Visible = False
        End If
    End If
End If
End Sub

Private Sub lstSearch_Validate(Cancel As Boolean)
'lstSearch.Visible = False
End Sub

Private Sub lstSrv_DblClick()

Select Case lstSrv.ListIndex
Case 0
Case Else
    Dim RetPay As New PatientReceivablePayment
    RetPay.PaymentId = lstSrvs2.TextMatrix(lstSrvs1.Row, 2)
    If (RetPay.RetrievePatientReceivablePayments) Then
        RetPay.PaymentService = myTrim(lstSrv.List(lstSrv.ListIndex))
        RetPay.PaymentServiceItem = myTrim(lstSrv.List(lstSrv.ListIndex), 1)
        Call RetPay.ApplyPatientReceivablePayments
    End If
    Set RetPay = Nothing
    Dim memR As Integer
    memR = lstSrvs1.TopRow
    LoadPaymentsTotal ""
    If lstSrvs1.Rows > 1 Then
        If Not memR < lstSrvs1.Rows Then memR = lstSrvs1.Rows - 1
        If Not memR = 0 Then lstSrvs1.TopRow = memR
    End If
    Updated = True
End Select
lstSrv.Visible = False
End Sub

Private Sub lstSrv_LostFocus()
lstSrv.Visible = False
End Sub

Private Sub lstSrvs1_Click()
Exit Sub

Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstSrvs1.RowSel
CurrentCol = lstSrvs1.ColSel
If (CurrentCol = 0) Then
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call mnuAddService_Click
    ElseIf (lstSrvs2.TextMatrix(CurrentRow, 0) = "P") Then
        Call mnuAddPayment_Click
    End If
ElseIf (CurrentCol = 3) Then
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call mnuSetBillDoctor_Click
    End If
ElseIf (CurrentCol = 5) Then
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call mnuBillOffice_Click
    End If
ElseIf (CurrentCol = 6) Then
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call mnuReferralDoctor_Click
    End If
ElseIf (CurrentCol = 8) Or (CurrentCol = 9) Or (CurrentCol = 10) Or (CurrentCol = 11) Then
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call mnuDiagnosis_Click
    End If
End If
End Sub

Private Sub lstSrvs1_DblClick()
Select Case lstSrvs1.col
Case 0
    Select Case lstSrvs2.TextMatrix(lstSrvs1.Row, 0)
    Case "R"
        Call EditClaim
    Case "P"
        Call EditPayment
    End Select
Case 2
    Select Case lstSrvs2.TextMatrix(lstSrvs1.Row, 0)
    Case "P", "X"
        Dim r As Integer
        For r = lstSrvs1.Row To 0 Step -1
            If lstSrvs2.TextMatrix(r, 0) = "R" Then
                Exit For
            End If
        Next
        lstSrv.Clear
        lstSrv.AddItem "Close list"
        For r = r + 1 To lstSrvs1.Rows - 1
            Select Case lstSrvs2.TextMatrix(r, 0)
            Case "R"
                Exit For
            Case "S"
                lstSrv.AddItem lstSrvs1.TextMatrix(r, 2) & Space(50) & lstSrvs2.TextMatrix(r, 1)
            End Select
        Next
        
        With lstSrv
            .Height = (.ListCount * 15 + 3) * 15
            .Top = lstSrvs1.RowPos(lstSrvs1.Row) + lstSrvs1.Top
            .Left = lstSrvs1.ColPos(2) + lstSrvs1.ColWidth(2) + lstSrvs1.Left
            .ListIndex = 0
            .Visible = True
            .SetFocus
        End With
    End Select
End Select


End Sub

Private Sub EditPayment()
Dim Test As Boolean
Test = EditableDate(CDate(lstSrvs1.Text))


EditMode = True
frPmnt_Visible True

lstPmnt(0).Enabled = Test
lstPmnt(1).Enabled = Test
lstPmnt(2).Enabled = Test
pTxt1.Visible = Test
fpPmnt(2).Enabled = Test
End Sub


Private Sub lstSrvs1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim CurrentRow As Integer
If (Button = 2) Then

Exit Sub
    CurrentRow = lstSrvs1.RowSel
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "S") Then
        Call Me.PopupMenu(mnuGridSrvActions, -1, lstSrvs1.Left + X + 200, lstSrvs1.Top + Y)
    ElseIf (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call Me.PopupMenu(mnuGridClmActions, -1, lstSrvs1.Left + X + 200, lstSrvs1.Top + Y)
    End If
End If
End Sub


Private Sub lstVisits_Click()
NumDispVisits = lstVisits.ListIndex + 1
cmdVisits_Click (1)
frmPaymentsTotal.LoadPaymentsTotal ""
End Sub

Private Sub lstVisits_LostFocus()
cmdVisits_Click (1)
End Sub

Private Sub mnuAddInfo_Click()
Dim AccidentType As String
Dim AccidentState  As String
Dim Disability  As String
Dim AdmissionDate  As String
Dim DismissalDate  As String
Dim RsnDate  As String
Dim RsnType  As String
Dim ConsDate  As String
Dim PrevCond  As String
Dim MedRef  As String
frmAddInfo.AccidentType = AccidentType
frmAddInfo.AccidentState = AccidentState
frmAddInfo.Disability = Disability
frmAddInfo.AdmissionDate = AdmissionDate
frmAddInfo.DismissalDate = DismissalDate
frmAddInfo.RsnDate = RsnDate
frmAddInfo.RsnType = RsnType
frmAddInfo.ConsDate = ConsDate
frmAddInfo.PrevCond = PrevCond
frmAddInfo.ExternalRefInfo = MedRef
Call frmAddInfo.LoadAddInfo
frmAddInfo.Show 1
AccidentType = frmAddInfo.AccidentType
AccidentState = frmAddInfo.AccidentState
Disability = frmAddInfo.Disability
AdmissionDate = frmAddInfo.AdmissionDate
DismissalDate = frmAddInfo.DismissalDate
RsnDate = frmAddInfo.RsnDate
RsnType = frmAddInfo.RsnType
ConsDate = frmAddInfo.ConsDate
PrevCond = frmAddInfo.PrevCond
MedRef = frmAddInfo.ExternalRefInfo
End Sub

Private Sub mnuAddPayment_Click()
MsgBox "add payment"
End Sub

Private Sub mnuAddService_Click()
'Dim u As Integer
'Dim st As Integer
'Dim ed As Integer
'Dim CurrentRow As Integer
'CurrentRow = lstSrvs1.RowSel
'If (CurrentRow >= 0) Then
'    st = CurrentRow
'
'    ed = 0
'    For u = CurrentRow + 1 To lstSrvs2.Rows - 1
'        If (lstSrvs2.TextMatrix(u, 0) = "T") Then
'            ed = u - 1
'            Exit For
'        End If
'    Next u
'    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
'        Call AddServiceGrid(st, ed)
'    End If
'End If
End Sub

Private Sub cmdNotes_Click()
Dim RcvId As Long, ApptId As Long
Dim Temp As String
frmNotes.NoteId = 0
frmNotes.CurrentAction = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = 0
frmNotes.SetTo = "B"
frmNotes.SystemReference = ""
frmNotes.MaintainOn = UserLogin.HasPermission(epBillingNotes)
frmNotes.EyeContext = ""

Dim ApplList As New ApplicationAIList

If (frmNotes.LoadNotes) Then
    frmNotes.chkClaim.Visible = False
    frmNotes.Show 1
    If (ApplList.ApplIsBillingNotesOn(PatientId, "")) Then
        Temp = "-R-"
    End If
    Set ApplList = Nothing
    If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
        piclblAlert.Visible = True
    End If
End If

Set ApplList = Nothing
End Sub

Private Sub mnuClaimNote_Click()
Dim CurrentRow As Integer
Dim RcvId As Long, ApptId As Long
Dim Temp As String, InvId As String
Dim ApplList As ApplicationAIList
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    RcvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 6)))
    ApptId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 7)))
    InvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 8)))
    frmNotes.NoteId = 0
    frmNotes.CurrentAction = ""
    frmNotes.PatientId = PatientId
    frmNotes.SetTo = "B"
    frmNotes.EyeContext = ""
    frmNotes.AppointmentId = ApptId
    frmNotes.SystemReference = "R" + InvId
    frmNotes.MaintainOn = UserLogin.HasPermission(epBillingNotes)
    If (frmNotes.LoadNotes) Then
        frmNotes.Show 1
        Set ApplList = New ApplicationAIList
        If (ApplList.ApplIsBillingNotesOn(PatientId, "R" + Trim(InvId))) Then
            Temp = "-I-"
        End If
        Set ApplList = Nothing
        If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
            piclblAlert.Visible = True
        End If
    End If
End If
End Sub

Private Sub mnuDiagnosis_Click()
Dim u As Integer
Dim st As Integer
Dim CurrentRow As Integer
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    st = 0
    For u = CurrentRow To u Step -1
        If (lstSrvs2.TextMatrix(u, 0) = "R") Then
            st = u
            Exit For
        End If
    Next u
    Call AddDiagnosisGrid
End If
End Sub

Private Sub mnuServiceNote_Click()
Dim CurrentRow As Integer
Dim RcvId As Long, ApptId As Long
Dim MyEye As String, Temp As String
Dim InvId As String, SrvId As String
Dim ApplList As ApplicationAIList
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    RcvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 6)))
    ApptId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 7)))
    InvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 8)))
    SrvId = Trim(lstSrvs1.TextMatrix(CurrentRow, 0))
    MyEye = Trim(lstSrvs1.TextMatrix(CurrentRow, 1))
    If (InStrPS(MyEye, "RT") > 0) Then
        MyEye = "OD"
    ElseIf (InStrPS(MyEye, "LT") > 0) Then
        MyEye = "OS"
    ElseIf (InStrPS(MyEye, "50") > 0) Then
        MyEye = "OU"
    Else
        MyEye = ""
    End If
    frmNotes.NoteId = 0
    frmNotes.CurrentAction = ""
    frmNotes.PatientId = PatientId
    frmNotes.AppointmentId = 0
    frmNotes.SetTo = "B"
    frmNotes.AppointmentId = ApptId
    frmNotes.EyeContext = MyEye
    frmNotes.SystemReference = "R" + InvId + "C" + SrvId
    frmNotes.MaintainOn = UserLogin.HasPermission(epBillingNotes)
    If (frmNotes.LoadNotes) Then
        frmNotes.Show 1
        Set ApplList = New ApplicationAIList
        If (ApplList.ApplIsBillingNotesOn(PatientId, "R" + Trim(InvId) + "C" + Trim(SrvId))) Then
            Temp = "-S-"
        End If
        Set ApplList = Nothing
        If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
            piclblAlert.Visible = True
        End If
    End If
End If
End Sub

Private Sub mnuReferralDoctor_Click()
Dim CurrentRow As Integer
Dim RcvId As Long
Dim Ref As String
Dim ResId As Long
Dim RetVnd As PracticeVendors
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    RcvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 1)))
    frmSelectDialogue.InsurerSelected = ""
    Call frmSelectDialogue.BuildSelectionDialogue("REFERREDFROM")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        If (Left(frmSelectDialogue.Selection, 6) <> "Create") Then
            Ref = UCase(frmSelectDialogue.Selection)
            ResId = val(Mid(Ref, 57, Len(Ref) - 56))
            Set ApplTemp = New ApplicationTemplates
            Call ApplTemp.ApplSetRefDoctor(RcvId, ResId)
            Set ApplTemp = Nothing
            Set ApplList = New ApplicationAIList
            Set RetVnd = New PracticeVendors
            RetVnd.VendorId = ResId
            If (RetVnd.RetrieveVendor) Then
                lstSrvs1.TextMatrix(CurrentRow, 6) = Trim(RetVnd.VendorLastName)
            End If
            Set RetVnd = Nothing
        End If
    End If
End If
End Sub

Private Sub mnuBillOffice_Click()
Dim CurrentRow As Integer
Dim RcvId As Long
Dim Ref As String
Dim ResId As Long
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    RcvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 1)))
    frmSelectDialogue.InsurerSelected = ""
    Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        Ref = UCase(frmSelectDialogue.Selection)
        ResId = val(Mid(Ref, 57, Len(Ref) - 56))
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplSetBillOffice(RcvId, ResId)
        Set ApplTemp = Nothing
        Set ApplList = New ApplicationAIList
        Call ApplList.GetLocation(ResId, Ref)
        lstSrvs1.TextMatrix(CurrentRow, 5) = Trim(Ref)
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub mnuSetBillDoctor_Click()
Dim CurrentRow As Integer
Dim RcvId As Long
Dim PlanId As Long
Dim Ref As String
Dim ResId As Long
Dim RetRes As SchedulerResource
Dim ApplTemp As ApplicationTemplates
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    RcvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 1)))
    PlanId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 2)))
    frmSelectDialogue.InsurerSelected = Trim(Str(PlanId))
    Call frmSelectDialogue.BuildSelectionDialogue("RESOURCEWITHAFF")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        Ref = frmSelectDialogue.Selection
        ResId = val(Mid(Ref, 57, Len(Ref) - 56))
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplSetBillToDoctor(RcvId, ResId)
        Set ApplTemp = Nothing
        Set RetRes = New SchedulerResource
        RetRes.ResourceId = ResId
        If (RetRes.RetrieveSchedulerResource) Then
            lstSrvs1.TextMatrix(CurrentRow, 3) = Trim(RetRes.ResourceName)
        End If
        Set RetRes = Nothing
    End If
End If
End Sub


Private Sub mnuShowClinical_Click()
Dim RcvId As Long
Dim ApptId As Long
Dim CurrentRow As Integer
CurrentRow = lstSrvs1.RowSel
If (CurrentRow >= 0) Then
    RcvId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 1)))
    If (RcvId > 0) Then
        ApptId = val(Trim(lstSrvs2.TextMatrix(CurrentRow, 3)))
        If (ApptId > 0) Then
            If (frmReviewAppts.LoadApptsList(PatientId, ApptId, True)) Then
                frmReviewAppts.Show 1
            End If
        End If
    End If
End If
End Sub

Private Sub mnuShowDemo_Click()
Dim PatientDemographics As New PatientDemographics
PatientDemographics.PatientId = PatientId
Call PatientDemographics.DisplayPatientInfoScreen
Set PatientDemographics = Nothing
End Sub

Private Sub mnuShowInsurers_Click()
Call PatientInsurerData
End Sub

Private Sub PatientInsurerData()
Dim ApplList As ApplicationAIList
If (PatientId > 0) Then
    If (PolicyHolderid < 1) Then
        PolicyHolderid = PatientId
    End If
    frmPatientFinancial.ThePolicyHolder1 = PolicyHolderid
    frmPatientFinancial.ThePolicyHolder1Rel = PolicyHolderIdRel
    frmPatientFinancial.ThePolicyHolder1Bill = PolicyHolderIdBill
    frmPatientFinancial.ThePolicyHolder2 = SecondPolicyHolderId
    frmPatientFinancial.ThePolicyHolder2Rel = SecondPolicyHolderIdRel
    frmPatientFinancial.ThePolicyHolder2Bill = SecondPolicyHolderIdBill
    If (frmPatientFinancial.FinancialLoadDisplay(PatientId, PolicyHolderid, SecondPolicyHolderId, "M", True, True)) Then
        frmPatientFinancial.CurrentAction = ""
        frmPatientFinancial.Show 1
        If (InStrPS(frmPatientFinancial.CurrentAction, "Home") = 0) Then
            SecondPolicyHolderId = frmPatientFinancial.ThePolicyHolder2
            SecondPolicyHolderIdRel = frmPatientFinancial.ThePolicyHolder2Rel
            SecondPolicyHolderIdBill = frmPatientFinancial.ThePolicyHolder2Bill
            PolicyHolderid = frmPatientFinancial.ThePolicyHolder1
            PolicyHolderIdRel = frmPatientFinancial.ThePolicyHolder1Rel
            PolicyHolderIdBill = frmPatientFinancial.ThePolicyHolder1Bill
            Dim memR As Integer
            memR = lstSrvs1.TopRow
            LoadPaymentsTotal ""
            If lstSrvs1.Rows > 1 Then
                If Not memR < lstSrvs1.Rows Then memR = lstSrvs1.Rows - 1
                If Not memR = 0 Then lstSrvs1.TopRow = memR
            End If
'            Call LoadPaymentsTotal(Invoicedate)
        End If
    End If
End If
End Sub

Private Sub SetLineColor(Idx As Integer, BColor As Single, FColor As Single, ALst As MSFlexGrid)
Dim g1 As Integer
If (Idx >= 0) Then
    ALst.Row = Idx
    ALst.RowSel = Idx
    For g1 = 0 To ALst.Cols - 1
        ALst.col = g1
        ALst.ColSel = g1
        ALst.CellBackColor = BColor
        ALst.CellForeColor = FColor
        If BColor = SrvBackColor Then
            ALst.CellFontBold = True
            'If g1 = 2 Then ALst.CellBackColor = 0
        End If
    Next g1
End If
End Sub

Private Sub AutoFitFlexGrid(ALst As MSFlexGrid)
Dim r As Long
Dim c As Long
Dim cell_wid As Single
Dim col_wid As Single
For c = 0 To ALst.Cols - 1
    col_wid = 0
    For r = 0 To ALst.Rows - 1
        cell_wid = TextWidth(ALst.TextMatrix(r, c))
        If col_wid < cell_wid Then col_wid = cell_wid
    Next r
    ALst.ColWidth(c) = col_wid + 120
Next c
End Sub

Private Sub TriggerSearch(AType As String)
Dim ApplClaim As ApplicationClaims
lstSearch_old.Clear
If (AType = "D") Then
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_Field = "Diagnosis (ICD)"
    frmAlphaPad.NumPad_Quit = False
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) And (Trim(frmAlphaPad.NumPad_Result) <> "") Then
        Set ApplClaim = New ApplicationClaims
        Call ApplClaim.LoadDiagnosis(lstSearch_old, "", Trim(frmAlphaPad.NumPad_Result), True, True)
        Set ApplClaim = Nothing
        If (lstSearch_old.ListCount > 1) Then
            lstSearch_old.Visible = True
        End If
    End If
Else
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_Field = "Procedures (CPT)"
    frmAlphaPad.NumPad_Quit = False
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) And (Trim(frmAlphaPad.NumPad_Result) <> "") Then
        lstSearch_old.AddItem "ZZZZZZ    Close Procedures"
        Set ApplClaim = New ApplicationClaims
        Call ApplClaim.LoadProcedures(lstSearch_old, lstSort, 0, "", Trim(frmAlphaPad.NumPad_Result), 0)
        Set ApplClaim = Nothing
        If (lstSearch_old.ListCount > 1) Then
            lstSearch_old.Visible = True
        End If
    End If
End If
End Sub

Private Sub InsertNewServiceLine(Idx As Integer)
Dim i As Integer
Dim j As Integer
If (Idx > 0) Then
    lstASrv.Rows = lstASrv.Rows + 1
    lstBSrv.Rows = lstBSrv.Rows + 1
    For i = lstASrv.Rows - 1 To Idx Step -1
        If (i = Idx) Then
            For j = 0 To lstASrv.Cols - 1
                lstASrv.TextMatrix(i, j) = ""
                If (j = 0) Then
                    lstBSrv.TextMatrix(i, j) = Trim(Str(0))
                End If
            Next j
        Else
            For j = 0 To lstASrv.Cols - 1
                lstASrv.TextMatrix(i, j) = lstASrv.TextMatrix(i - 1, j)
                If (j = 0) Then
                    lstBSrv.TextMatrix(i, j) = lstBSrv.TextMatrix(i - 1, j)
                End If
            Next j
        End If
    Next i
End If
End Sub

Private Sub TriggerServiceNote(CRow As Integer)
If (CRow >= 0) Then
    lstSrvs1.RowSel = val(Trim(lstBSrv.TextMatrix(CRow, 0)))
    Call mnuServiceNote_Click
    lstASrv.TextMatrix(CRow, 8) = "*"
End If
End Sub


Public Sub EditClaim()
If lstSrvs2.Rows = 0 Then Exit Sub
    
Dim u As Integer
Dim st As Integer
Dim ed As Integer
Dim CurrentRow As Integer
CurrentRow = lstSrvs1.RowSel
lstDelDiag.Clear
lstDelSrvs.Clear
fpBtnSave.Enabled = True

If (CurrentRow >= 0) Then
    st = CurrentRow
    
    ed = 0
    For u = CurrentRow + 1 To lstSrvs2.Rows - 1
        If (lstSrvs2.TextMatrix(u, 0) = "T") Then
            ed = u - 1
            Exit For
        End If
    Next u
    If (lstSrvs2.TextMatrix(CurrentRow, 0) = "R") Then
        Call AddGenGrid
        Call AddDiagnosisGrid
        Call AddServiceGrid(st, ed)
    End If

End If

Dim ACom As String
Dim ApplList As New ApplicationAIList
Call ApplList.ApplGetAppointmentInfo(lstSrvs2.TextMatrix(lstSrvs1.Row, 3), _
                                     0, "", "", "", "", 0, False, ACom)
Set ApplList = Nothing
ManualClaim = (ACom = "ADD VIA BILLING")

CopyFGC lstGen1, lstGenSave
CopyFGC lstDiag, lstDiagSave
CopyFGC lstASrv, lstASrvSave

frEditClaim_Visible True
DoEvents
lstASrv.col = 0
lstASrv.Row = 1
editCell lstASrv

txt.SetFocus
End Sub

Private Sub CopyFGC(src As MSFlexGrid, dst As MSFlexGrid)
Dim r As Integer, c As Integer
dst.Cols = src.Cols
dst.Rows = src.Rows
For r = 0 To src.Rows - 1
    For c = 0 To src.Cols - 1
        dst.TextMatrix(r, c) = src.TextMatrix(r, c)
    Next
Next
End Sub

Private Function changedFGC(src As MSFlexGrid, dst As MSFlexGrid) As Boolean
changedFGC = False
Dim r As Integer, c As Integer

If src.Rows = dst.Rows Then
    For r = 0 To src.Rows - 1
        For c = 0 To src.Cols - 1
            If Not dst.TextMatrix(r, c) = src.TextMatrix(r, c) Then
                changedFGC = True
                Exit Function
            End If
        Next
    Next
Else
    changedFGC = True
End If
End Function


Private Sub frEditClaim_Visible(stat As Boolean)
frEditClaim.Visible = stat
stat = Not stat
lstSrvs1.Enabled = stat
cmdDone.Enabled = stat
cmdMClm.Enabled = stat
cmdNotes.Enabled = stat
If stat Then
    If EditClaimMode Then
        Unload Me
    Else
        If lstSrvs1.Visible Then
            lstSrvs1.SetFocus
        End If
    End If
Else
    txt.SetFocus
End If

End Sub
Private Sub frPmnt_Visible(stat As Boolean)

frPmnt(0).Visible = stat
DoEvents
stat = Not stat
cmdDone.Enabled = stat
cmdMClm.Enabled = stat
cmdNotes.Enabled = stat
If stat Then
    FromCredit False
    If lstSrvs1.Visible Then lstSrvs1.SetFocus
Else
    lstPmnt(0).col = 0
    initPmnt
    fpPmnt(1).Visible = Not EditMode
    frmask(0).Visible = EditMode
    frmask(1).Visible = EditMode
End If


End Sub

Private Sub initPmnt()
lblWB.Visible = False
Dim r As Integer
If EditMode Then
    Dim RetPay As New PatientReceivablePayment
    RetPay.PaymentId = lstSrvs2.TextMatrix(lstSrvs1.Row, 2)
    If (RetPay.FindPatientReceivablePayments > 0) Then
        If RetPay.SelectPatientReceivablePayments(1) Then
            With lstPmnt(0)
                lstP.Clear
                Call GetInsRecs(PatientId, "", lstP)
                Select Case RetPay.PaymentPayerType
                Case "P"
                    .TextMatrix(1, 0) = "Patient"
                Case "O"
                    .TextMatrix(1, 0) = "Office"
                Case "I"
                .TextMatrix(1, 0) = RetPay.PaymentPayerId
                    For r = 2 To lstP.ListCount - 1
                        If CStr(RetPay.PaymentPayerId) = myTrim(lstP.List(r), 1) Then
                            .TextMatrix(1, 0) = lstP.List(r)
                            Exit For
                        End If
                    Next
                End Select
                Dim RetCode As New PracticeCodes
                RetCode.ReferenceType = "PAYABLETYPE"
                    If (RetCode.FindCodePartial > 0) Then
                        r = 1
                        Do While (RetCode.SelectCode(r))
                            If RetPay.PaymentRefType = myTrim(RetCode.ReferenceCode, 1) Then
                                .TextMatrix(1, 1) = Trim(Mid(RetCode.ReferenceCode, 1, InStrPS(1, RetCode.ReferenceCode, "-") - 1)) _
                                & Space(200) & myTrim(RetCode.ReferenceCode, 1)
                                Exit Do
                            End If
                            r = r + 1
                        Loop
                    End If
                Set RetCode = Nothing
                
                Dim a As Double
                mCheckExist = RetPay.PaymentBatchCheckId
                a = RetPay.getCheckAmt(mCheckExist)
                .TextMatrix(1, 2) = Format(a, "#####0.00")
                .TextMatrix(1, 3) = RetPay.PaymentCheck
                .TextMatrix(1, 4) = Format(Mid(RetPay.PaymentDate, 1, 4) & "/" & _
                                           Mid(RetPay.PaymentDate, 5, 2) & "/" & _
                                           Mid(RetPay.PaymentDate, 7, 2), "mm/dd/yyyy")
                If RetPay.PaymentEOBDate = "" Then
                     .TextMatrix(1, 5) = ""
                Else
                    .TextMatrix(1, 5) = Format(Mid(RetPay.PaymentEOBDate, 1, 4) & "/" & _
                                               Mid(RetPay.PaymentEOBDate, 5, 2) & "/" & _
                                               Mid(RetPay.PaymentEOBDate, 7, 2), "mm/dd/yyyy")
                End If
            End With
            
            With lstPmntInfo
                .TextMatrix(1, 0) = myTrim(lstPmnt(0).TextMatrix(1, 0), 1)
                .TextMatrix(1, 1) = ""
                .TextMatrix(1, 2) = "0.00"
                .TextMatrix(1, 3) = "0.00"
                .TextMatrix(1, 4) = "0.00"
                .TextMatrix(1, 6) = "0.00"
                .TextMatrix(1, 7) = "0.00"
            End With
        End If
    End If
Else
    With lstPmnt(0)
        .TextMatrix(1, 0) = "Patient"
        .TextMatrix(1, 1) = "Check" & Space(200) & "K"
        .TextMatrix(1, 2) = "0.00"
        .TextMatrix(1, 3) = ""
        .TextMatrix(1, 4) = Format(Now, "mm/dd/yyyy")
        .TextMatrix(1, 5) = ""
    End With
    With lstPmntInfo
        .TextMatrix(1, 0) = "Patient"
        .TextMatrix(1, 1) = ""
        .TextMatrix(1, 2) = "0.00"
        .TextMatrix(1, 3) = "0.00"
        .TextMatrix(1, 4) = "0.00"
        .TextMatrix(1, 6) = "0.00"
        .TextMatrix(1, 7) = "0.00"
    End With
    mCheckExist = 0
End If
With lstPmnt(2)
    .TextMatrix(1, 0) = "0.00"
    .TextMatrix(1, 1) = ""
End With

initPmntSrvs RetPay
editCellPmnt 0
Set RetPay = Nothing

End Sub

Private Sub initPmntSrvs(Optional RetPay As PatientReceivablePayment)
Dim r As Integer, r1 As Integer, i As Integer
Dim n(2) As Single
Dim k As Integer
With lstPmnt(1)
    .Rows = 1
    If EditMode Then
        r = lstSrvs1.Row
        .Rows = 2
        
        Set RetPay = New PatientReceivablePayment
        RetPay.PaymentId = lstSrvs2.TextMatrix(r, 2)
        If (RetPay.RetrievePatientReceivablePayments) Then
            For r1 = lstSrvs1.Row To 0 Step -1
                If lstSrvs2.TextMatrix(r1, 0) = "R" Then
                    .TextMatrix(1, 1) = lstSrvs2.TextMatrix(r1, 9)
                    Exit For
                End If
            Next
            For r1 = lstSrvs1.Row To 0 Step -1
                If lstSrvs2.TextMatrix(r1, 0) = "S" Then
                    .TextMatrix(1, 0) = RetPay.PaymentServiceItem
                    .TextMatrix(1, 2) = RetPay.PaymentService
                    .TextMatrix(1, 3) = lstSrvs1.TextMatrix(r1, 6)
                    .TextMatrix(1, 4) = lstSrvs1.TextMatrix(r1, 7)
                    .TextMatrix(1, 6) = myCCur(lstSrvs2.TextMatrix(r, 10))
                    .TextMatrix(1, 7) = "0.00"
                    .TextMatrix(1, 8) = "0.00"
                    .TextMatrix(1, 9) = "0.00"
                    .TextMatrix(1, 10) = "0.00"
                    .TextMatrix(1, 17) = "0.00"
                    .TextMatrix(1, 19) = ""
                    
                    n(0) = lstSrvs1.TextMatrix(r1, 9)
                    Select Case RetPay.PaymentType
                    Case "P"
                        i = 7
                        n(1) = RetPay.PaymentAmount
                    Case "X"
                        i = 8
                        n(1) = RetPay.PaymentAmount
                    Case "|"
                        i = 9
                    Case "!"
                        i = 10
                    Case Else
                        i = 17
                        .TextMatrix(1, 19) = RetPay.PaymentType
                        n(1) = RetPay.PaymentAmount
                        Dim RetrieveCode As New PracticeCodes
                        RetrieveCode.ReferenceType = "paymenttype"
                        If (RetrieveCode.FindCode > 0) Then
                            k = 1
                            Do Until (Not (RetrieveCode.SelectCode(k)))
                                If RetPay.PaymentType = myTrim(RetrieveCode.ReferenceCode, 1) Then
                                    Exit Do
                                End If
                                k = k + 1
                            Loop
                        End If
                        
                        Select Case RetrieveCode.ReferenceAlternateCode
                        Case "C"
                        Case "D"
                            n(1) = -n(1)
                        Case Else
                            n(1) = 0
                        End Select
                        Set RetrieveCode = Nothing
                    End Select
                    n(2) = n(0) + n(1)
                    
                    .TextMatrix(1, i) = myCCur(RetPay.PaymentAmount)
                    .TextMatrix(1, 5) = myCCur(CStr(n(2)))
                    .TextMatrix(1, 11) = lstSrvs1.TextMatrix(r1, 9)
                    .TextMatrix(1, 13) = ""
                    .TextMatrix(1, 14) = ""
                    .TextMatrix(1, 15) = RetPay.PaymentRefId
                    If RetPay.PaymentCommentOn Then
                        .TextMatrix(1, 16) = "Y"
                    Else
                        .TextMatrix(1, 16) = ""
                    End If
                    .TextMatrix(1, 18) = RetPay.PaymentReason
                    Exit For
                End If
            Next
        End If
        Set RetPay = Nothing
        Call calcRemainingAmt
    Else
        For r = lstSrvs1.Row + 1 To lstSrvs1.Rows - 1
            Select Case lstSrvs2.TextMatrix(r, 0)
            Case "S"
                r1 = .Rows
                .Rows = .Rows + 1
                .TextMatrix(r1, 0) = lstSrvs2.TextMatrix(r, 1)
                .TextMatrix(r1, 1) = lstSrvs1.TextMatrix(r, 0)
                .TextMatrix(r1, 2) = lstSrvs1.TextMatrix(r, 2)
                .TextMatrix(r1, 3) = lstSrvs1.TextMatrix(r, 6)
                .TextMatrix(r1, 4) = lstSrvs1.TextMatrix(r, 7)
                .TextMatrix(r1, 5) = lstSrvs1.TextMatrix(r, 9)
                .TextMatrix(r1, 6) = lstSrvs2.TextMatrix(r, 10)
                .TextMatrix(r1, 7) = "0.00"
                .TextMatrix(r1, 8) = "0.00"
                .TextMatrix(r1, 9) = "0.00"
                .TextMatrix(r1, 10) = "0.00"
                .TextMatrix(r1, 11) = lstSrvs1.TextMatrix(r, 9)
                .TextMatrix(r1, 13) = ""
                .TextMatrix(r1, 14) = ""
                .TextMatrix(r1, 15) = ""
                .TextMatrix(r1, 16) = ""
                .TextMatrix(r1, 17) = "0.00"
                .TextMatrix(r1, 18) = ""
                .TextMatrix(r1, 19) = ""
            Case "R"
                Exit For
            End Select
        Next
    End If
End With
lstPmnt(1).Height = lstPmnt(1).Rows * lstPmnt(1).RowHeight(0)
lstPmnt(2).Top = lstPmnt(1).Top + lstPmnt(1).Height + 75
End Sub

Private Sub resizeFrame()
frEditClaim.Left = lstSrvs1.Left
frEditClaim.Width = lstSrvs1.Width
frEditClaim.Top = lstSrvs1.Top
frEditClaim.Height = cmdDone.Height + cmdDone.Top - frEditClaim.Top

frBtnBar.Top = frEditClaim.Height - frBtnBar.Height

frInEditClame.Top = 60
frInEditClame.Left = 60
frInEditClame.Width = frEditClaim.Width - 120
frInEditClame.Height = frEditClaim.Height - frBtnBar.Height - 120

lstASrv.Height = frInEditClame.Height - lstASrv.Top

frPmnt(0).Left = frEditClaim.Left
frPmnt(0).Width = frEditClaim.Width
frPmnt(0).Top = frEditClaim.Top
frPmnt(0).Height = frEditClaim.Height
frPmnt(1).Top = frInEditClame.Top
frPmnt(1).Left = frInEditClame.Left
frPmnt(1).Width = frInEditClame.Width
frPmnt(1).Height = frInEditClame.Height - 120
lstPmnt(1).Height = frPmnt(1).Height - lstPmnt(1).Top

frBtnBar1.Top = 5880
lstPT.Height = lstPmnt(1).Height - 90
VS.Height = lstPT.Height - 60
End Sub


Private Sub editCell(FGC As MSFlexGrid)
If frEditClaim.Visible = False Then Exit Sub
If bySys = True Then Exit Sub

If Not currentFGC.Name = FGC.Name Then Set currentFGC = FGC
Dim c As Integer, r As Integer
c = FGC.col
r = FGC.Row

pTxt.Top = FGC.Top + FGC.RowPos(r) + 15
pTxt.Left = FGC.Left + FGC.ColPos(c) + 15
pTxt.Width = FGC.ColWidth(c) - 30
bySys = True
txt.Text = FGC.Text
txt.Width = pTxt.Width - 45
If FGC.ColAlignment(c) = flexAlignRightCenter Then
    txt.Alignment = 1
Else
    txt.Alignment = 0
End If
bySys = False


DD.Caption = "q"
txt.MaxLength = 0
Select Case currentFGC.Name
Case "lstGen"
    Select Case c
    Case 1, 2, 3, 4, 5, 6, 7, 8, 9
        DD.Left = pTxt.Width - DD.Width + 15
        DD.Visible = True
    Case Else
        DD.Visible = False
    End Select
Case "lstDiag"
        DD.Left = pTxt.Width - DD.Width + 15
        DD.Visible = True
Case "lstASrv"
    Select Case c
    Case 0, 10
        DD.Left = pTxt.Width - DD.Width + 15
        DD.Visible = True
    Case 2
        DD.Left = 0
        DD.Visible = True
    Case Else
        DD.Visible = False
    End Select
    
    If c = 9 Then txt.MaxLength = 16
Case Else
    DD.Visible = False
End Select

'fpBtn(0).Enabled = (currentFGC.Name = "lstDiag")
'fpBtn(1).Enabled = (currentFGC.Name = "lstASrv" And lstASrv.Col = 0)

Call SkipCell(c, r)
If Not (FGC.col = c And FGC.Row = r) Then
    FGC.col = c
    FGC.Row = r
    Exit Sub
End If


pTxt.Visible = True
If Not DD.Visible _
Or (DD.Visible And _
    (currentFGC.Name = "lstDiag" Or _
    (currentFGC.Name = "lstASrv" And lstASrv.col = 0))) _
                    Then
    txt.SelStart = 0
    txt.SelLength = Len(txt.Text)
End If
txt.SetFocus
End Sub

Private Sub editCellPmnt(i As Integer)
Dim FGC As MSFlexGrid
Dim Temp As String
Set FGC = lstPmnt(i)

Dim c As Integer, r As Integer
c = FGC.col
r = FGC.Row

If FGC.ColWidth(c) = 0 Then Exit Sub

If i = 2 And (c = 0 Or c = 1) And r = 1 And EditCellFlag Then
    Exit Sub
End If


pTxt1.Top = FGC.Top + FGC.RowPos(r) + 15
pTxt1.Left = FGC.Left + FGC.ColPos(c) + 15
pTxt1.Width = FGC.ColWidth(c) - 30
bySys = True
txt1.MaxLength = 0
txt1.Text = FGC.Text
''If c = 6 And r > 0 And i = 1 Then
''    fgc.Text = "asdf"
''End If
txt1.Width = pTxt1.Width - 45
If FGC.ColAlignment(c) = flexAlignRightCenter Then
    txt1.Alignment = 1
Else
    txt1.Alignment = 0
End If

bySys = False


dd1.Caption = "q"
Select Case i
Case 0
    Select Case c
    Case 0, 1
        dd1.Left = pTxt1.Width - dd1.Width + 15
        dd1.Visible = True
    Case Else
        dd1.Visible = False
    End Select
Case 1
    Select Case c
    Case 15
        dd1.Left = pTxt1.Width - dd1.Width + 15
        dd1.Visible = True
    Case Else
        dd1.Visible = False
    End Select
    Select Case c
    Case 13, 14, 16, 19
        txt1.MaxLength = 1
    End Select
Case Else
    dd1.Visible = False
End Select


Call SkipCell1(c, r)
If Not (FGC.col = c And FGC.Row = r) Then
    FGC.col = c
    FGC.Row = r
    Exit Sub
End If


pTxt1.Visible = True
If Not dd1.Visible Then
    txt1.SelStart = 0
    txt1.SelLength = Len(txt1.Text)
End If
On Error Resume Next
txt1.SetFocus
End Sub



Private Sub PT_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If lstPT.Visible Then
    lstPT.Visible = False
    lstPmnt(1).col = lstPmnt(1).Cols - 1
    lstPmnt(1).SetFocus
Else
    lblHelp.Caption = ""
    Dim k As Integer
    Dim RetrieveCode As New PracticeCodes
    RetrieveCode.ReferenceType = "paymenttype"
    If (RetrieveCode.FindCode > 0) Then
        k = 1
        Do Until (Not (RetrieveCode.SelectCode(k)))
            If Trim(RetrieveCode.ReferenceCode) Like "*&" Then
                lblHelp.Caption = lblHelp.Caption & Trim(RetrieveCode.ReferenceCode) & "&" & vbNewLine
            Else
                lblHelp.Caption = lblHelp.Caption & Trim(RetrieveCode.ReferenceCode) & vbNewLine
            End If
            k = k + 1
        Loop
    End If
    Set RetrieveCode = Nothing
    VS.SmallChange = 15 * 14
    VS.LargeChange = VS.SmallChange * 10
    lblHelp.Height = (k - 1) * VS.SmallChange
    VS.Max = (k - 1 - 18) * VS.SmallChange
    lstPT.Visible = True
    VS.SetFocus
End If
End Sub

Private Sub txt_GotFocus()
DD.Caption = "q"
'lstSearch.Visible = False
End Sub

Private Sub txt_KeyDown(KeyCode As Integer, Shift As Integer)

Dim ExitSub As Boolean
Select Case KeyCode
Case vbKeyTab, vbKeyReturn
    Call txt_Validate(ExitSub)
    If ExitSub Then Exit Sub
Case vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
    If txtLocked Then KeyCode = 0
Case vbKeyPageDown, vbKeyPageUp
    Dim i, n As Integer
    For i = 1 To FGCcol.Count
        If FGCcol.Item(i).Name = currentFGC.Name Then
            If KeyCode = vbKeyPageDown Then
                n = i + 1
            Else
                n = i - 1
            End If
            Select Case n
            Case 1 To FGCcol.Count
            Case Else
                n = i
            End Select
            FGCcol.Item(n).SetFocus
            Exit For
        End If
    Next
Case Else
    KeyCode = 0
End Select

Dim c As Integer, r As Integer
c = currentFGC.col
r = currentFGC.Row
Dim nextC As Integer, nextR As Integer
With currentFGC
    Select Case KeyCode
    Case vbKeyTab
        bySys = True
        If currentFGC.Name = "lstASrv" Then
            If lstASrv.col = 0 _
            And lstASrv.Row = lstASrv.Rows - 1 _
            And lstASrv.TextMatrix(lstASrv.Rows - 1, 0) = "" Then
                If Shift = 0 Then
                    lstASrv.col = 0
                    lstASrv.Row = 1
                Else
                    lstASrv.col = lstASrv.Cols - 1
                    lstASrv.Row = lstASrv.Row - 1
                End If
                bySys = False
                editCell currentFGC
                Exit Sub
            End If
        End If
        Call NextCell(nextC, nextR, Shift)
        .Row = nextR
        .col = nextC
        bySys = False
        editCell currentFGC
    Case vbKeyReturn
        txt.Text = .Text
        If DD.Visible Then
            DDClick
            DD.Caption = "p"
        Else
            bySys = False
            If lstPmntInd = 2 Then
                .col = 0
                nextR = r + (Shift + 1 - (Shift * 3))
            End If
            Select Case nextR
            Case 0
                nextR = .Rows - 1
            Case .Rows
                nextR = 1
            End Select
            .Row = nextR
            bySys = False
            editCellPmnt lstPmntInd
        End If
    End Select
End With

End Sub


Private Sub NextCell(nC As Integer, nR As Integer, sShift As Integer)
Dim c As Integer, r As Integer

    With currentFGC
        If sShift = 0 Then
            If .col = .Cols - 1 Then
                nC = 0
                If .Row = .Rows - 1 Then
                    nR = 1
                Else
                    nR = .Row + 1
                End If
            Else
                nC = .col + 1
                nR = .Row
            End If
        Else
            If .col = 0 Then
                nC = .Cols - 1
                If .Row = 1 Then
                    nR = .Rows - 1
                Else
                    nR = .Row - 1
                End If
            Else
                nC = .col - 1
                nR = .Row
            End If
        End If
    End With
    
    Call SkipCell(nC, nR, sShift)
End Sub
    
Private Sub SkipCell(nC As Integer, nR As Integer, _
                     Optional sShift As Integer = 0)

    Select Case currentFGC.Name
    Case "lstGen"
        If Not ManualClaim Then
            With lstGen
                Select Case nC
                Case 0, 1
                    If sShift = 0 Then
                        nC = 2
                    Else
                        nC = .Cols - 1
                    End If
                End Select
            End With
        End If
    Case "lstDiag"
        With lstDiag
            If .Text = "" Then
                Dim prevC As Integer
                If .TextMatrix(nR, nC) = "" Then
                    For prevC = nC - 1 To 0 Step -1
                        If .TextMatrix(nR, prevC) = "" Then
                            nC = nC - 1
                        Else
                            Exit For
                        End If
                    Next
                End If
            End If
        End With
    Case "lstASrv"
        If lstASrv.TextMatrix(nR, 0) = "" Then
            If nC = 0 Then
                If nR = lstASrv.Rows - 1 Then
                    'nR = 1
                Else
                    nR = lstASrv.Rows - 1
                End If
            Else
                If sShift = 0 Then
                    nC = 0
                    nR = lstASrv.Rows - 1
                Else
                    nC = 10
                End If
            End If
        Else
            If sShift = 0 Then
                Select Case nC
                Case 7, 8
                        nC = 9
                End Select
            Else
                Select Case nC
                Case 7, 8
                        nC = 6
                End Select
            End If
        End If
    End Select
End Sub

Private Sub SkipCell1(nC As Integer, nR As Integer, _
                     Optional sShift As Integer = 0)

    Select Case lstPmntInd
    Case 1
        Select Case nC
        Case Is < 6
            If sShift = 0 Then
                nC = 6
            Else
                nC = lstPmnt(1).Cols - 1
            End If
        Case 11, 12
            If sShift = 0 Then
                nC = 13
            Else
                nC = 10
            End If
        End Select
    End Select
End Sub

Private Sub txt_KeyPress(KeyAscii As Integer)
If txtLocked Then KeyAscii = 0
End Sub

Private Sub txt_Validate(Cancel As Boolean)

Dim StxtValid As Boolean
Dim orgTxt As String
Dim orgC As Integer
Dim corrTxt As String
Dim Temp As String, Temp1 As String
Dim n(3) As String

If currentFGC.Text = txt.Text Then
    Exit Sub
Else
    orgTxt = currentFGC.Text
    orgC = currentFGC.col + 1
    StxtValid = txtValid(corrTxt)
    If StxtValid Then
        currentFGC.Text = corrTxt
    Else
        txt.Text = currentFGC.Text
        txt.SelStart = 0
        txt.SelLength = Len(txt.Text)
        txt.SetFocus
        Cancel = True
        Exit Sub
    End If
End If

Select Case currentFGC.Name
Case "lstGen"
    Select Case currentFGC.col
    Case 0
        lstGen1.TextMatrix(1, 0) = Format(corrTxt, "yyyymmdd")
    Case 4, 9
        lstGen1.TextMatrix(1, currentFGC.col) = lstGen.TextMatrix(1, currentFGC.col)
    End Select
Case "lstDiag"
'reorganize diag
    Dim r As Integer, c As Integer, i As Integer
    Dim b As String, newV As String
    If corrTxt = "" Then
        With lstDiag
            For c = .col To .Cols - 2
                .TextMatrix(1, c) = .TextMatrix(1, c + 1)
                .TextMatrix(2, c) = .TextMatrix(2, c + 1)
            Next
            .TextMatrix(1, .Cols - 1) = ""
            .TextMatrix(2, .Cols - 1) = ""
            .col = 0
        End With
    End If
'reorganize lnk
    With lstASrv
        Select Case True
        Case corrTxt = ""
            For r = 1 To .Rows - 2
                newV = ""
                For i = 1 To Len(.TextMatrix(r, 2))
                    b = (Mid(.TextMatrix(r, 2), i, 1))
                    Select Case CInt(b)
                    Case orgC
                        b = ""
                    Case Is > orgC
                        b = CInt(b) - 1
                        If b = 0 Then b = ""
                    End Select
                    newV = newV & b
                Next
                If Not .TextMatrix(r, 2) = newV Then
                    lstASrv1.TextMatrix(r, 1) = "n"
                    lstDelSrvs.AddItem lstASrv1.TextMatrix(r, 0)
                End If
                .TextMatrix(r, 2) = newV
            Next
'        Case Not orgTxt = corrTxt
'            For r = 1 To .Rows - 2
'                For i = 1 To Len(.TextMatrix(r, 2))
'                    b = Mid(.TextMatrix(r, 2), i, 1)
'                    If b = CStr(orgC) Then
'                        .TextMatrix(r, 2) = ""
'                    End If
'                Next
'            Next
        End Select
    End With
    
Case "lstASrv"
    With lstASrv
        Select Case .col
        Case 0, 1, 2, 3, 4, 5, 6, 9
            lstASrv1.TextMatrix(.Row, 1) = "n"
            If Not lstASrv1.TextMatrix(.Row, 0) = "" Then
                lstDelSrvs.AddItem lstASrv1.TextMatrix(.Row, 0)
            End If
        End Select
        
        Select Case .col
        Case 0
'add/remove/update service
            If .Text = "" Then
                If Not .Row = .Rows - 1 Or .Row = 14 Then
                    lstASrv1.RemoveItem .Row
                    .RemoveItem .Row
                    Cancel = True
                    editCell lstASrv
                    If .Rows = 14 Then
                        If Not .TextMatrix(13, 0) = "" Then
                            .Rows = 15
                        End If
                    End If
                End If
            Else
                If .Row = .Rows - 1 Then
                    If .Rows < 15 Then
                        lstASrv1.Rows = .Rows + 1
                        .Rows = .Rows + 1
                    End If
                End If
                
                Dim p(2) As String
                Call SrvsDetails(p(0), p(1), p(2))
                .TextMatrix(.Row, 1) = ""
                .TextMatrix(.Row, 2) = ""
                .TextMatrix(.Row, 3) = p(0)
                .TextMatrix(.Row, 4) = p(1)
                .TextMatrix(.Row, 5) = p(2)
                .TextMatrix(.Row, 6) = 1
                .TextMatrix(.Row, 7) = p(1)
                .TextMatrix(.Row, 8) = p(1)
                .TextMatrix(.Row, 10) = ""
                
                Dim RS As Recordset
                Set RS = GetRS("select * from PracticeServices where code = '" & _
                         .TextMatrix(.Row, 0) & "'")
                .TextMatrix(.Row, 9) = RS("description")

            End If
        Case 4, 6
'calculate ammount
            n(0) = 0
            n(1) = 0
            n(2) = 0
            n(3) = 0
            If IsNumeric(.TextMatrix(.Row, 4)) Then n(0) = .TextMatrix(.Row, 4)
            If IsNumeric(.TextMatrix(.Row, 6)) Then n(1) = .TextMatrix(.Row, 6)
            If IsNumeric(.TextMatrix(.Row, 7)) Then n(2) = .TextMatrix(.Row, 7)
            If IsNumeric(.TextMatrix(.Row, 8)) Then n(3) = .TextMatrix(.Row, 8)
            Temp = n(0) * n(1)
            Call DisplayDollarAmount(Temp, Temp)
            Temp1 = Temp - n(2) + n(3)
            Call DisplayDollarAmount(Temp1, Temp1)
            .TextMatrix(.Row, 7) = Temp
            .TextMatrix(.Row, 8) = Temp1
            
        End Select
    End With
End Select

End Sub


Private Sub txt1_Validate(Cancel As Boolean)
Dim StxtValid As Boolean
Dim orgTxt As String
Dim corrTxt As String
Dim n(5) As Double
Dim Temp As String
Dim r As Integer
Dim k As Integer
Dim FGC As MSFlexGrid
Set FGC = lstPmnt(lstPmntInd)

If FGC.Text = txt1.Text Then
    If lstPmntInd = 1 And FGC.col = 6 And mKeyCode = vbKeyTab Then
    Else
        Exit Sub
    End If
End If

orgTxt = FGC.Text
StxtValid = txt1Valid(corrTxt)

If orgTxt = corrTxt Then
    If FGC.Index = 1 And FGC.col = 6 And mKeyCode = vbKeyTab Then
    Else
        Exit Sub
    End If
End If
If FGC.Text <> "0.00" And FGC.col = 0 And FGC.Row = 1 Then Exit Sub
If StxtValid Then
    FGC.Text = corrTxt
    If lstPmntInd = 0 And Not FGC.col = 4 _
    Or lstPmntInd = 2 Then
        FromCredit False
    End If
Else
    txt1.Text = FGC.Text
    txt1.SelStart = 0
    txt1.SelLength = Len(txt1.Text)
    txt1.SetFocus
    Cancel = True
    Exit Sub
End If

Select Case lstPmntInd
Case 0
        Select Case lstPmnt(0).col
        Case 2, 3, 4
            mCheckExist = -1
        End Select

        Select Case lstPmnt(0).col
        Case 2
            Call calcRemainingAmt
        End Select
Case 1
    With lstPmnt(1)
        Select Case .col
        Case 6, 7, 8, 17, 19
            '--------------------------
            If .col = 6 And mKeyCode = vbKeyTab Then
                If Not .Text = 0 Then
                    mKeyCode = -1
                    For r = 0 To 4
                        n(r) = 0
                    Next
                    If IsNumeric(.TextMatrix(.Row, 4)) Then n(0) = .TextMatrix(.Row, 4) 'Charge
                    If IsNumeric(.TextMatrix(.Row, 5)) Then n(1) = .TextMatrix(.Row, 5) 'Prev Bal
                    If IsNumeric(.TextMatrix(.Row, 6)) Then n(2) = .TextMatrix(.Row, 6) 'Allowed
                    
                    Dim AdjAlowed As Double
                    Dim PlanId As String
                    PlanId = lstPmntInfo.TextMatrix(1, 0)
                    If IsNumeric(PlanId) Then
                        Dim RetFee As New PracticeFeeSchedules
                        RetFee.SchService = .TextMatrix(.Row, 2)
                        RetFee.SchPlanId = PlanId
                        If RetFee.FindServiceDirect > 0 Then
                            k = 1
                            Dim apptDte As String
                            apptDte = Format(lstSrvs2.TextMatrix(lstSrvs1.Row, 9), "yyyymmdd")
                            Do While RetFee.SelectService(k)
                                If Not apptDte < RetFee.SchStartDate Then
                                    If Not apptDte > RetFee.SchEndDate _
                                    Or RetFee.SchEndDate = "" Then
                                        If IsNumeric(RetFee.SchAdjustmentRate) Then
                                            AdjAlowed = n(2) * RetFee.SchAdjustmentRate / 100
                                            Exit Do
                                        End If
                                    End If
                                End If
                                k = k + 1
                            Loop
                        End If
                        Set RetFee = Nothing
                    End If
                    
                    If AdjAlowed = 0 Then
                        If n(0) = n(1) Then
                            If n(0) < n(2) Then
                                n(3) = n(0)
                            Else
                                n(3) = n(2)
                            End If
                        Else
                            n(3) = n(2) - (n(0) - n(1))
                        End If
                    Else
                        n(3) = AdjAlowed
                    End If
                                    
                    If n(3) > n(1) Then
                        n(3) = n(1)
                    End If
                    
                    If n(3) < 0 Then
                        n(3) = 0
                    End If
                    Temp = n(3)
                    Call DisplayDollarAmount(Temp, Temp)
                    .TextMatrix(.Row, 7) = Trim(Temp)           '<payment
                    
                    n(4) = n(0) - n(2)
                    If n(4) < 0 Then n(4) = 0
                    Temp = n(4)
                    Call DisplayDollarAmount(Temp, Temp)
                    .TextMatrix(.Row, 8) = Trim(Temp)
                End If
                Call calcRemainingAmt
                mKeyCode = 0
            End If
            '--------------------------
            For r = 0 To 4
                n(r) = 0
            Next
            If IsNumeric(.TextMatrix(.Row, 5)) Then n(0) = .TextMatrix(.Row, 5)
            If IsNumeric(.TextMatrix(.Row, 7)) Then n(1) = .TextMatrix(.Row, 7)
            If IsNumeric(.TextMatrix(.Row, 8)) Then n(2) = .TextMatrix(.Row, 8)
            If IsNumeric(.TextMatrix(.Row, 17)) Then
                n(3) = .TextMatrix(.Row, 17)
                If n(3) = 0 _
                Or .TextMatrix(.Row, 19) = "" Then
                    n(3) = 0
                Else
                    Dim RetrieveCode As New PracticeCodes
                    RetrieveCode.ReferenceType = "paymenttype"
                    If (RetrieveCode.FindCode > 0) Then
                        k = 1
                        Do Until (Not (RetrieveCode.SelectCode(k)))
                            If .TextMatrix(.Row, 19) = myTrim(RetrieveCode.ReferenceCode, 1) Then
                                Exit Do
                            End If
                            k = k + 1
                        Loop
                    End If
                    
                    Select Case RetrieveCode.ReferenceAlternateCode
                    Case "C"
                        n(3) = -n(3)
                    Case "D"
                    Case Else
                        n(3) = 0
                    End Select
                    Set RetrieveCode = Nothing
                End If
            End If
            n(4) = n(0) - n(1) - n(2) + n(3)
            
            Temp = n(4)
            Call DisplayDollarAmount(Temp, Temp)
            .TextMatrix(.Row, 11) = Trim(Temp)
            
            lblWB.Visible = False
            For r = 1 To .Rows - 1
                If .TextMatrix(r, 11) < 0 Then
                   lblWB.Visible = True
                   Exit For
                End If
            Next
            Select Case .col
            Case 7, 8, 17, 19
                Call calcRemainingAmt
            End Select
        End Select
        Select Case .col
        Case 6, 7, 8, 9, 10, 17, 19
            Call checkGroup
        End Select
    End With
Case 2
'        Select Case lstPmnt(2).col
'        Case 0
'            GoSub calcRemainingAmt
'        End Select
End Select

End Sub

Private Sub calcRemainingAmt()
Dim n(5) As Single
Dim r As Integer
Dim Temp As String
For r = 0 To 5
    n(r) = 0
Next
With lstPmnt(1)
    For r = 1 To .Rows - 1
        If IsNumeric(.TextMatrix(r, 7)) Then
            n(1) = n(1) + .TextMatrix(r, 7)
        End If
        If IsNumeric(.TextMatrix(r, 8)) Then
            n(2) = n(2) + .TextMatrix(r, 8)
        End If
        If IsNumeric(.TextMatrix(r, 17)) _
        And Not .TextMatrix(r, 19) = "" Then
            n(3) = .TextMatrix(r, 17) * CreditDebit(.TextMatrix(r, 19))
            Select Case .TextMatrix(r, 19)
            Case "U", "R"
                n(4) = n(4) + n(3)
            Case Else
                n(5) = n(5) + n(3)
            End Select
        End If
    Next
    
    '4
    n(0) = Round(n(1), 2) + Round(n(4), 2)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmntInfo.TextMatrix(1, 6) = Trim(Temp)
    '5
    n(0) = n(2) + n(5)
    Call DisplayDollarAmount(Round(n(0), 2), Temp)
    lstPmntInfo.TextMatrix(1, 7) = Trim(Temp)
    
    If FromCreditInd Then
        n(0) = lstPmnt(0).TextMatrix(1, 2) - n(1)
        Call DisplayDollarAmount(Round(n(0), 2), Temp)
        lstPmnt(2).TextMatrix(1, 0) = Temp
    End If
    
End With
End Sub

Private Function CreditDebit(PaymentType As String) As Integer
Dim SQL As String
Dim RS As Recordset

SQL = _
"select AlternateCode  from PracticeCodeTable " & _
"where ReferenceType = 'PaymentType' " & _
"and Right(Code, 1) = '" & PaymentType & "'"
Set RS = GetRS(SQL)

Select Case RS("AlternateCode")
Case "C"
    CreditDebit = 1
Case "D"
    CreditDebit = -1
Case Else
    CreditDebit = 0
End Select
End Function

Private Sub checkGroup()
Dim r As Integer
Dim v As String
With lstPmnt(1)
    If Not EditMode Then
        If Entered Then v = "?"
    End If
    For r = 1 To .Rows - 1
        .TextMatrix(r, 13) = v
        .TextMatrix(r, 14) = v
    Next
End With
End Sub

Private Function Entered() As Boolean
Dim r As Integer, f As Integer
Dim e As Boolean
For r = 1 To lstPmnt(1).Rows - 1
    For f = 7 To 10
        e = Not (lstPmnt(1).TextMatrix(r, f) = 0)
        If Not e Then
            e = Not (lstPmnt(1).TextMatrix(r, 17) = 0)
        End If
        If e Then Exit For
    Next
    If e Then Exit For
Next

If Not e Then
    e = Not (lstPmnt(2).TextMatrix(1, 0) = 0)
End If

Entered = e
End Function


Private Function txt1Valid(corrTxt As String) As Boolean
Dim FtxtValid As Boolean
corrTxt = Trim(txt1.Text)

Select Case lstPmntInd
Case 0
    Select Case lstPmnt(0).col
    Case 2
            If Trim(corrTxt) = "" Then corrTxt = 0
            If IsNumeric(corrTxt) Then
                Call DisplayDollarAmount(corrTxt, corrTxt)
                FtxtValid = True
            End If
    Case 4
            FtxtValid = OkDate(corrTxt)
            If FtxtValid Then FtxtValid = EditableDate(CDate(corrTxt))
    Case 5
            corrTxt = Trim(corrTxt)
            If corrTxt = "" Then
                FtxtValid = True
            Else
                FtxtValid = OkDate(corrTxt)
            End If
    Case Else
            FtxtValid = True
    End Select
Case 1
    Select Case lstPmnt(1).col
    Case 6, 7, 8, 9, 10, 17
            If Trim(corrTxt) = "" Then corrTxt = 0
            If IsNumeric(corrTxt) Then
                Call DisplayDollarAmount(corrTxt, corrTxt)
                FtxtValid = True
            End If
    Case 13
            Select Case UCase(Trim(corrTxt))
            Case "", "P"
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            Case Else
                If IsNumeric(Trim(corrTxt)) Then
                    lstP.Clear
                    Call GetInsRecs(PatientId, "", lstP)
                    Select Case CInt(Trim(corrTxt))
                    Case 1 To lstP.ListCount - 2
                        FtxtValid = True
                    End Select
                End If
            End Select
    Case 14
            Select Case UCase(Trim(corrTxt))
            Case "", "B", "P", "W", "A", "U"
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            End Select
    Case 16
            Select Case UCase(Trim(corrTxt))
            Case "", "Y"
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            End Select
    Case 19
            Select Case UCase(Trim(corrTxt))
            Case ""
                corrTxt = UCase(corrTxt)
                FtxtValid = True
            Case Else
                Dim k As Integer
                Dim RetrieveCode As New PracticeCodes
                RetrieveCode.ReferenceType = "paymenttype"
                If (RetrieveCode.FindCode > 0) Then
                    k = 1
                    Do Until (Not (RetrieveCode.SelectCode(k)))
                        If UCase(Trim(corrTxt)) = myTrim(RetrieveCode.ReferenceCode, 1) Then
                            corrTxt = UCase(corrTxt)
                            FtxtValid = True
                            Exit Do
                        End If
                        k = k + 1
                    Loop
                End If
                Set RetrieveCode = Nothing
            End Select
    Case Else
        FtxtValid = True
    End Select
Case 2
    Select Case lstPmnt(2).col
    Case 0
        If Trim(corrTxt) = "" Then corrTxt = 0
        If IsNumeric(corrTxt) Then
            Call DisplayDollarAmount(corrTxt, corrTxt)
            FtxtValid = True
        End If
    Case Else
        FtxtValid = True
    End Select
End Select

If Not FtxtValid Then
    With frmEventMsgs
        .Header = "Invalid value"
        .AcceptText = ""
        .RejectText = "OK"
        .CancelText = ""
        .Other0Text = ""
        .Other1Text = ""
        .Other2Text = ""
        .Other3Text = ""
        .Other4Text = ""
        .Show 1
    End With
End If

corrTxt = Trim(corrTxt)
txt1Valid = FtxtValid
End Function


Private Function txtValid(corrTxt As String) As Boolean
Dim FtxtValid As Boolean
corrTxt = Trim(txt.Text)

Select Case currentFGC.Name
Case "lstGen"
    With lstGen
        Select Case .col
        Case 0
                Call OkDate(corrTxt)
                If IsDate(corrTxt) Then
                    If Not DateDiff("d", Now, corrTxt) > 0 Then
                        FtxtValid = True
                    End If
                End If
        Case Else
                FtxtValid = True
        End Select
    End With
Case "lstDiag"
'verify diag
    If corrTxt = "" Then
        FtxtValid = True
    Else
        Dim RetrieveDiagnosis As New DiagnosisMasterPrimary
        RetrieveDiagnosis.PrimaryBilling = True
        RetrieveDiagnosis.PrimaryNextLevelDiagnosis = corrTxt
        If RetrieveDiagnosis.VerifyDiagnosis Then
            FtxtValid = True
        Else
            Dim RetDiag As New Service
            RetDiag.Service = corrTxt
            RetDiag.ServiceType = "D"
            If (RetDiag.FindDiagnosis > 0) Then
                FtxtValid = True
            End If
            Set RetDiag = Nothing
        End If
        Set RetrieveDiagnosis = Nothing
    End If
    If FtxtValid Then
        If Not lstDiag.TextMatrix(2, lstDiag.col) = "" Then
            lstDelDiag.AddItem Space(74) & lstDiag.TextMatrix(2, lstDiag.col)
        End If
        lstDiag.TextMatrix(2, lstDiag.col) = "d"
    End If
'==========
Case "lstASrv"
    Dim ApplList As New ApplicationAIList
    Dim AChg As Single
    Dim APOS As String
    Dim tmp As String
    
    Select Case lstASrv.col
    Case 0
'verify service
        If corrTxt = "" Then
            FtxtValid = True
        Else
            FtxtValid = ApplList.ApplVerifyService(corrTxt, AChg, APOS)
            If FtxtValid Then
                corrTxt = UCase(corrTxt)
                lstASrv1.TextMatrix(lstASrv.Row, 2) = "C"
            End If
        End If
    Case 1
'verify modifier
            corrTxt = UCase(corrTxt)
            FtxtValid = ApplList.ApplVerifyModifier(corrTxt)
            If lstASrv1.TextMatrix(lstASrv.Row, 2) = "C" Then
                Call SrvsDetails(, , tmp)
                If corrTxt = "50" Then
                    tmp = tmp * 2
                Else
                    tmp = tmp * lstASrv.TextMatrix(lstASrv.Row, 6)
                End If
                lstASrv.TextMatrix(lstASrv.Row, 5) = myCCur(tmp)
            End If
    Case 3
'verify POS
            If corrTxt = "" Then
                Call SrvsDetails(corrTxt)
                FtxtValid = True
            Else
                FtxtValid = ApplList.ApplVerifyPOS(corrTxt)
            End If
    Case 2
'verify Links
            FtxtValid = (IsNumeric(corrTxt) Or corrTxt = "")
    Case 4
'verify Charge
            FtxtValid = (IsNumeric(corrTxt) Or corrTxt = "")
            If corrTxt = "" Then
                Call SrvsDetails(, corrTxt)
                FtxtValid = True
            Else
                FtxtValid = IsNumeric(corrTxt)
                Call DisplayDollarAmount(corrTxt, corrTxt)
            End If
    Case 5
'verify Allowable
            FtxtValid = (IsNumeric(corrTxt) Or corrTxt = "")
            If corrTxt = "" Then
                Call SrvsDetails(, , corrTxt)
                If lstASrv.TextMatrix(lstASrv.Row, 1) = "50" Then
                    corrTxt = corrTxt * 2
                Else
                    corrTxt = corrTxt * lstASrv.TextMatrix(lstASrv.Row, 6)
                End If
                corrTxt = myCCur(corrTxt)
                FtxtValid = True
                lstASrv1.TextMatrix(lstASrv.Row, 2) = "C"
            Else
                FtxtValid = IsNumeric(corrTxt)
                Call DisplayDollarAmount(corrTxt, corrTxt)
                lstASrv1.TextMatrix(lstASrv.Row, 2) = ""
            End If
    Case 6
'verify Qty
            If Trim(corrTxt) = "" Then
                corrTxt = 1
            End If
            If IsNumeric(corrTxt) Then
                corrTxt = CInt(corrTxt)
                If corrTxt < 1 Then corrTxt = 1
            End If
            FtxtValid = IsNumeric(corrTxt)
            If FtxtValid _
            And lstASrv1.TextMatrix(lstASrv.Row, 2) = "C" Then
                If Not lstASrv.TextMatrix(lstASrv.Row, 1) = "50" Then
                    Call SrvsDetails(, , tmp)
                    tmp = tmp * corrTxt
                    lstASrv.TextMatrix(lstASrv.Row, 5) = myCCur(tmp)
                End If
            End If
    Case Else
        FtxtValid = True
    End Select
    Set ApplList = Nothing
Case Else
    FtxtValid = True
End Select

If Not FtxtValid Then
    With frmEventMsgs
        .Header = "Invalid value"
        .AcceptText = ""
        .RejectText = "OK"
        .CancelText = ""
        .Other0Text = ""
        .Other1Text = ""
        .Other2Text = ""
        .Other3Text = ""
        .Other4Text = ""
        .Show 1
    End With
End If

corrTxt = Trim(corrTxt)
txtValid = FtxtValid
End Function

Private Function txtLocked() As Boolean
If frEditClaim.Visible And DD.Visible Then
    Select Case currentFGC.Name
    Case "lstDiag"
    Case "lstASrv"
        If Not lstASrv.col = 0 Then txtLocked = True
    Case Else
        txtLocked = True
    End Select
End If
If frPmnt(0).Visible And dd1.Visible Then
    If txt1.Top < lstPmnt(1).Top Then
        Select Case lstPmnt(0).col
        Case 0, 1
            txtLocked = True
        End Select
    End If
End If
End Function

Public Sub setRowsHeight()
If lstSrvs2.Rows < 1 Then Exit Sub

Dim r As Integer
Dim h(1) As Long
h(0) = 0
h(1) = lstSrvs2.RowHeight(0)
With lstSrvs1
    For r = 0 To .Rows - 1
        Select Case lstSrvs2.TextMatrix(r, 0)
        Case "S Doc"
            .RowHeight(r) = h(-CInt(SrvGrid.ClaimHeader))
        Case "R"
            .RowHeight(r) = h(-CInt(SrvGrid.InvoiceInformation))
        Case "Date"
            .RowHeight(r) = h(-CInt(SrvGrid.ServiceHeader))
        Case "S"
            .RowHeight(r) = h(-CInt(SrvGrid.Code))
        Case "T"
            .RowHeight(r) = h(-CInt(SrvGrid.ClaimTotal))
        
        Case "TR"
            Select Case Left(.TextMatrix(r, 3), 8)
            Case "Claim Qd", "Claim Wt"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsClaimsQueued))
            Case "Claim Sn"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsClaimsSent))
            Case "Claim Cl"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsClaimsClosed))
            Case "Claim Ap"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsAppeal))
            'Case "Claim Wt"
            '    .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsAppealClosed))
            End Select
            Select Case Left(.TextMatrix(r, 3), 12)
            Case "Statement Qd"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsPatientStatementQueued))
            Case "Statement Sn"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsPatientStatementSent))
            Case "Statement Cl"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsPatientStatementClosed))
            Case "Monthly Stmt"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionsMonthlyStatement))
            End Select
        
        Case "P"
            If lstSrvs2.TextMatrix(r, 12) = "P" Then
                If UCase(lstSrvs2.TextMatrix(r, 5)) = "I" Then
                    .RowHeight(r) = h(-CInt(SrvGrid.InsurancePayments))
                Else
                    .RowHeight(r) = h(-CInt(SrvGrid.PatientPayments))
                End If
            Else
                If lstSrvs2.TextMatrix(r, 13) = "" Then
                    .RowHeight(r) = h(-CInt(SrvGrid.InfoTransactions))
                Else
                    If UCase(lstSrvs2.TextMatrix(r, 5)) = "I" Then
                        .RowHeight(r) = h(-CInt(SrvGrid.InsuranceAdjustments))
                    Else
                        .RowHeight(r) = h(-CInt(SrvGrid.NonInsuranceAdjustments))
                    End If
                End If
            End If
            
        Case "Y", "NS"
                .RowHeight(r) = h(-CInt(SrvGrid.BillingTransactionOrphaned))
        End Select
    Next
End With

End Sub

Public Sub setView()
bySys = True
chkGroupView(0).Value = SrvGrid.Invoice
chkGroupView(1).Value = SrvGrid.Services
chkGroupView(2).Value = SrvGrid.Payments
chkGroupView(3).Value = SrvGrid.Billing
bySys = False
End Sub


Private Sub txt1_GotFocus()
dd1.Caption = "q"
If txtC.Visible Then txtC.Visible = False
End Sub

Private Sub txt1_KeyDown(KeyCode As Integer, Shift As Integer)
mKeyCode = 0

Dim FGC As MSFlexGrid
Set FGC = lstPmnt(lstPmntInd)


Dim ExitSub As Boolean
Select Case KeyCode
Case vbKeyTab, vbKeyReturn
    If FGC.Index = 1 Then
        If FGC.col = 6 Then
            If KeyCode = vbKeyTab Then mKeyCode = vbKeyTab
        End If
    End If

    Call txt1_Validate(ExitSub)
    If ExitSub Then Exit Sub
Case vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
    If txtLocked Then KeyCode = 0
Case vbKeyPageDown, vbKeyPageUp
    Call txt1_Validate(ExitSub)
    If ExitSub Then Exit Sub
    Dim nextI As Integer
    nextI = KeyCode - 34
    If nextI = 0 Then nextI = 1
    nextI = lstPmntInd + nextI
    Select Case nextI
    Case 3
        nextI = 0
    Case -1
        nextI = 2
    End Select
    lstPmnt(nextI).SetFocus
Case Else
    KeyCode = 0
End Select

Dim c As Integer, r As Integer
c = FGC.col
r = FGC.Row
Dim nextC As Integer, nextR As Integer
With FGC
    Select Case KeyCode
    Case vbKeyTab
        bySys = True
        nextR = .Row
        If Shift = 0 Then
            nextC = .col + 1
        Else
            nextC = .col - 1
        End If
        
        If lstPmntInd = 1 Then
            If nextC < 6 Then
                nextC = -1
            End If
        End If
        
        Select Case nextC
        Case .Cols
            nextC = 0
            nextR = .Row + 1
        Case Is < 0
            nextC = .Cols - 1
            nextR = .Row - 1
        End Select
        
        Select Case nextR
        Case .Rows
            nextR = 1
        Case 0
            nextR = .Rows - 1
        End Select
        
        Call SkipCell1(nextC, nextR, Shift)
        .col = nextC
        .Row = nextR
        bySys = False
        editCellPmnt lstPmntInd
    Case vbKeyReturn
        txt1.Text = .Text
        If dd1.Visible Then
            DD1Click
            dd1.Caption = "p"
        Else
            bySys = False
            If lstPmntInd = 1 Then
                .col = 6
                nextR = r + (Shift + 1 - (Shift * 3))
            End If
            Select Case nextR
            Case 0
                nextR = .Rows - 1
            Case .Rows
                nextR = 1
            End Select
            .Row = nextR
            bySys = False
            editCellPmnt lstPmntInd
        End If

    End Select
End With

End Sub

Private Sub txt1_KeyPress(KeyAscii As Integer)
If txtLocked Then KeyAscii = 0
End Sub

Public Sub GetInsRecs(PatId As Long, ADate As String, lst As ListBox)
Dim InsLst As New Collection
With lst
    .AddItem "Patient"
    .AddItem "Office"
    Set InsLst = GetInsLst(PatId, ADate)
    Dim i As Integer
    For i = 1 To InsLst.Count
        .AddItem InsLst(i)
    Next
End With
End Sub

'''''Public Sub GetInsRecs(PatId As Long, ADate As String, lst As ListBox)
'''''
'''''Dim i As Integer, p As Integer
'''''Dim PolId(2) As Long
'''''Dim InsInfo(4) As String
'''''Dim IType(1) As String
'''''Dim DisplayText As String
'''''Dim Temp1 As String, Temp2 As String
'''''
'''''Dim RetPat As New Patient
'''''Dim RetFin As New PatientFinance
'''''Dim ApplTemp As New ApplicationTemplates
'''''Dim RetrievePlan As New Insurer
'''''Dim rs As ADODB.Recordset
'''''Set rs = CreateAdoRecordset
'''''
'''''With lst
'''''    .AddItem "Patient"
'''''    .AddItem "Office"
'''''
'''''
'''''    RetPat.PatientId = PatId
'''''    If (RetPat.RetrievePatient) Then
'''''        PolId(0) = PatId
'''''        If Not RetPat.PolicyPatientId = PolId(0) Then
'''''            PolId(1) = RetPat.PolicyPatientId
'''''        End If
'''''        If Not RetPat.SecondPolicyPatientId = PolId(0) Then
'''''            PolId(2) = RetPat.SecondPolicyPatientId
'''''        End If
'''''
'''''        IType(0) = RetPat.Relationship
'''''        If Trim(IType(0)) = "" Then IType(0) = "Y"
'''''        IType(1) = RetPat.SecondRelationship
'''''        If Trim(IType(1)) = "" Then IType(1) = "Y"
'''''    End If
'''''
'''''
'''''    Dim r As Long
'''''    rs.ActiveConnection = Nothing
'''''    rs.CursorLocation = adUseClient
'''''    rs.LockType = adLockBatchOptimistic
'''''    rs.Fields.Append "c0", adChar, 1
'''''    rs.Fields.Append "c1", adChar, 2
'''''    rs.Fields.Append "c2", adChar, 1
'''''    rs.Fields.Append "c3", adChar, 8
'''''    rs.Fields.Append "c4", adChar, 9
'''''    rs.Fields.Append "c5", adChar, 3
'''''    rs.Fields.Append "c6", adChar, 75
'''''    rs.Fields.Append "c7", adChar, 10
'''''    rs.Open
'''''
'''''    For p = 0 To 2
'''''        If Not PolId(p) > 0 Then GoTo EndFor
'''''        If p > 0 Then
'''''            RetrievePlan.InsurerId = PolId(p)
'''''            If RetrievePlan.RetrieveInsurer Then
'''''                If RetrievePlan.OutOfPocket = 1 Then GoTo EndFor
'''''            End If
'''''        End If
'''''
'''''        RetFin.PatientId = PolId(p)
'''''        RetFin.PrimaryInsurerId = 0
'''''        RetFin.PrimaryPIndicator = 0
'''''        RetFin.PrimaryStartDate = ""
'''''        RetFin.PrimaryEndDate = ""
'''''        RetFin.PrimaryInsType = ""
'''''        RetFin.FinancialStatus = ""
'''''        If (RetFin.FindPatientFinancial > 0) Then
'''''            i = 1
'''''            While (RetFin.SelectPatientFinancial(i))
'''''                Call ApplTemp.ApplGetPlanName(RetFin.PrimaryInsurerId, _
'''''                InsInfo(0), InsInfo(1), InsInfo(2), InsInfo(3), InsInfo(4), False)
'''''
'''''                If Not ADate = "" Then
'''''                    If (Not ADate < RetFin.PrimaryStartDate) _
'''''                    And (Not ADate > RetFin.PrimaryEndDate Or _
'''''                         RetFin.PrimaryEndDate = "") Then
'''''                    Else
'''''                        GoTo endloop
'''''                    End If
'''''                End If
'''''
'''''                rs.AddNew
'''''                '-------------
'''''                rs.Fields("c0") = p
'''''                '-------------
'''''                If (Trim(RetFin.PrimaryInsType) = "") Then
'''''                    Temp1 = "M"
'''''                Else
'''''                    Temp1 = RetFin.PrimaryInsType
'''''                End If
'''''
'''''                Select Case Temp1
'''''                Case "M"
'''''                    Temp1 = "1" & Temp1
'''''                Case "V"
'''''                    Temp1 = "2" & Temp1
'''''                Case "A"
'''''                    Temp1 = "3" & Temp1
'''''                Case "W"
'''''                    Temp1 = "4" & Temp1
'''''                End Select
'''''                rs.Fields("c1") = Temp1
'''''                '-------------
'''''                If (RetFin.PatientId = RetPat.PolicyPatientId) Then
'''''                    rs.Fields("c2") = IType(0)
'''''                Else
'''''                    rs.Fields("c2") = IType(1)
'''''                End If
'''''                '-------------
'''''                rs.Fields("c3") = RetFin.PrimaryStartDate
'''''                '-------------
'''''                Temp1 = RetFin.PrimaryEndDate
'''''                If Temp1 = "" Then
'''''                    Temp1 = "3"
'''''                Else
'''''                    If Temp1 < Format(Now, "yyyymmdd") Then
'''''                        Temp1 = "1" & Temp1
'''''                    Else
'''''                        Temp1 = "2" & Temp1
'''''                    End If
'''''                End If
'''''                rs.Fields("c4") = Temp1
'''''                '-------------
'''''                rs.Fields("c5") = RetFin.PrimaryPIndicator
'''''                '-------------
'''''                rs.Fields("c6") = Trim(InsInfo(1))
'''''                '-------------
'''''                rs.Fields("c7") = RetFin.PrimaryInsurerId
'''''                rs.Update
'''''endloop:
'''''                i = i + 1
'''''            Wend
'''''        End If
'''''EndFor:
'''''    Next
'''''
'''''
'''''    rs.Sort = "c1, c0, c4 desc, c5, c6"
'''''
'''''    Do Until rs.EOF
'''''        Temp1 = Mid(rs.Fields("c3"), 5, 2) + "/" + _
'''''                Mid(rs.Fields("c3"), 7, 2) + "/" + _
'''''                Mid(rs.Fields("c3"), 1, 4)
'''''
'''''        Temp2 = Trim(Mid(rs.Fields("c4"), 2, 8))
'''''        If Not Temp2 = "" Then
'''''            Temp2 = " - " & _
'''''                    Mid(Temp2, 5, 2) + "/" + _
'''''                    Mid(Temp2, 7, 2) + "/" + _
'''''                    Mid(Temp2, 1, 4)
'''''        End If
'''''
'''''        DisplayText = Space(150)
'''''        Mid(DisplayText, 1, 100) = rs.Fields("c6") & " " & _
'''''                                Mid(rs.Fields("c1"), 2, 1) & " " & _
'''''                                rs.Fields("c2") & " " & _
'''''                                Temp1 & Temp2
'''''        Mid(DisplayText, 100, 10) = rs.Fields("c7")
'''''        .AddItem DisplayText
'''''        rs.MoveNext
'''''    Loop
'''''    For i = .ListCount - 1 To 1 Step -1
'''''        If lstPmntInfo.TextMatrix(1, 0) = myTrim(lstP.List(i), 1) Then
'''''            Exit For
'''''        End If
'''''    Next
'''''    .Selected(i) = True
'''''End With
'''''
'''''Set rs = Nothing
'''''Set RetPat = Nothing
'''''Set RetFin = Nothing
'''''Set ApplTemp = Nothing
'''''Set RetrievePlan = Nothing
'''''End Sub

Private Function lstPmntInd() As Integer
Select Case pTxt1.Top
Case Is > lstPmnt(2).Top
    lstPmntInd = 2
Case Is > lstPmnt(1).Top
    lstPmntInd = 1
Case Else
    lstPmntInd = 0
End Select
End Function
Private Sub txtC_GotFocus()
txtC.SelStart = 0
txtC.SelLength = Len(txtC.Text)
End Sub
Private Sub txtC_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case vbKeyEscape
    txt1.SetFocus
Case vbKeyReturn, vbKeyTab
    lstPmnt(1).TextMatrix(lstPmnt(1).Row, 12) = Trim(txtC.Text)
    If Trim(txtC.Text) = "" Then
        txt1.Text = ""
    Else
        txt1.Text = txtC.Text
    End If
    lstPmnt(1).TextMatrix(lstPmnt(1).Row, 15) = txt1.Text
    txt1.SetFocus
End Select
End Sub
Private Sub VS_Change()
lblHelp.Top = -VS.Value
End Sub
Private Sub VS_LostFocus()
lstPT.Visible = False
End Sub
Private Sub VS_Scroll()
Call VS_Change
End Sub
Public Sub FrmClose()
 Call fpBtn1_Click
 Unload Me
End Sub

