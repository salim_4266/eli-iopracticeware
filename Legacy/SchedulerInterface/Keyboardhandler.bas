Attribute VB_Name = "Keyboardhandler"
Option Explicit

Public Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long
Public Declare Function UnhookWindowsHookEx Lib "user32" _
  (ByVal hHook As Long) As Long

Private Declare Sub CopyMemory Lib "kernel32" _
   Alias "RtlMoveMemory" _
  (pDest As Any, _
   pSource As Any, _
   ByVal cb As Long)

Private Declare Function CallNextHookEx Lib "user32" _
   (ByVal hHook As Long, _
   ByVal nCode As Long, _
   ByVal wParam As Long, _
   ByVal lParam As Long) As Long


Private Type KBDLLHOOKSTRUCT
  vkCode As Long
  scanCode As Long
  flags As Long
  Time As Long
  dwExtraInfo As Long
End Type

' Low-Level Keyboard Constants
Private Const HC_ACTION = 0

' Virtual Keys
Public Const VK_CONTROL = &H11
Public Const VK_ALT = &H12
Public Const VK_SHIFT = &H10


Private Const WH_KEYBOARD_LL = 13&
Public KeyboardHandle As Long


Public KeyboardHook As KeyboardHook

Public Function IsHooked(ByRef Hookstruct As KBDLLHOOKSTRUCT) _
            As Boolean
   
  If (KeyboardHook Is Nothing) Then
    IsHooked = False
    Exit Function
  End If
  
  ' Capture ctrl alt shift H in any Sequence
  If ((CBool(GetAsyncKeyState(VK_CONTROL))) _
        And (CBool(GetAsyncKeyState(VK_ALT))) And (CBool(GetAsyncKeyState(VK_SHIFT))) _
        And (CBool(GetAsyncKeyState(vbKeyH)))) Then
        'To Invoke the home screen
    IsHooked = KeyboardHook.InvokeHomeScreen()
    Exit Function
  End If
End Function
Public Function KeyboardCallback(ByVal Code As Long, _
  ByVal wParam As Long, ByVal lParam As Long) As Long

  Static Hookstruct As KBDLLHOOKSTRUCT

  If (Code = HC_ACTION) Then
    ' Copy the keyboard data out of the lParam (which is a pointer)
    Call CopyMemory(Hookstruct, ByVal lParam, Len(Hookstruct))

    If (IsHooked(Hookstruct)) Then
      KeyboardCallback = 1
      Exit Function
    End If

  End If

  KeyboardCallback = CallNextHookEx(KeyboardHandle, _
    Code, wParam, lParam)

End Function

Public Sub HookKeyboard()
  KeyboardHandle = SetWindowsHookEx( _
    WH_KEYBOARD_LL, AddressOf KeyboardCallback, _
    App.hInstance, 0&)
End Sub
Private Function Hooked()
  Hooked = KeyboardHandle <> 0
End Function

Public Sub UnhookKeyboard()
  If (Hooked) Then
    Call UnhookWindowsHookEx(KeyboardHandle)
  End If
End Sub
