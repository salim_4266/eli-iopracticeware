VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmOtherFavorites 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtType 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7440
      MaxLength       =   3
      TabIndex        =   17
      Top             =   840
      Width           =   1095
   End
   Begin VB.TextBox txtRank 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      MaxLength       =   3
      TabIndex        =   15
      Top             =   840
      Width           =   1095
   End
   Begin VB.ListBox lstSysDiags 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4620
      ItemData        =   "OtherFavorites.frx":0000
      Left            =   7080
      List            =   "OtherFavorites.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   11
      Top             =   1680
      Width           =   4815
   End
   Begin VB.TextBox txtNew 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      MaxLength       =   64
      TabIndex        =   9
      Top             =   6720
      Width           =   9375
   End
   Begin VB.ListBox lstSystem 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4620
      ItemData        =   "OtherFavorites.frx":0004
      Left            =   120
      List            =   "OtherFavorites.frx":0006
      TabIndex        =   2
      Top             =   1680
      Width           =   2295
   End
   Begin VB.ListBox lstDiags 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4620
      ItemData        =   "OtherFavorites.frx":0008
      Left            =   2520
      List            =   "OtherFavorites.frx":000A
      TabIndex        =   1
      Top             =   1680
      Width           =   4455
   End
   Begin VB.TextBox txtTemplate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      MaxLength       =   64
      TabIndex        =   0
      Top             =   840
      Width           =   5775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10320
      TabIndex        =   3
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "OtherFavorites.frx":000C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   4
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "OtherFavorites.frx":01EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   990
      Left            =   5160
      TabIndex        =   13
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "OtherFavorites.frx":03CA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   2520
      TabIndex        =   14
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "OtherFavorites.frx":05A9
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7440
      TabIndex        =   18
      Top             =   480
      Width           =   510
   End
   Begin VB.Label lblRank 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Rank"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6120
      TabIndex        =   16
      Top             =   480
      Width           =   540
   End
   Begin VB.Label lblSysDiags 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Finding"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7080
      TabIndex        =   12
      Top             =   1320
      Width           =   870
   End
   Begin VB.Label lblNew 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "To enter a new phrase, type the phrase and then click on 'Post'"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1560
      TabIndex        =   10
      Top             =   6360
      Width           =   7065
   End
   Begin VB.Label lblHeader 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Setup Other Favorites"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3960
      TabIndex        =   8
      Top             =   120
      Width           =   2415
   End
   Begin VB.Label lblSystem 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Part of Exam"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   7
      Top             =   1320
      Width           =   1395
   End
   Begin VB.Label lblDiags 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Phrase"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2520
      TabIndex        =   6
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label lblTemplate 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Template"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   1035
   End
End
Attribute VB_Name = "frmOtherFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private DeleteOn As Boolean
Private PostOn As Boolean
Private TheTemplate As String
Private TheType As String
Private fNew As Boolean

Private Sub cmdApply_Click()
    If PostOn Then
        SetupCodes
    End If
    Unload frmOtherFavorites
End Sub

Private Sub cmdBack_Click()
    Unload frmOtherFavorites
End Sub

Private Sub cmdDelete_Click()
Dim i As Integer
Dim Temp As String
Dim ApplTbl As ApplicationTables
    If Trim$(TheTemplate) <> "" Then
        If lstDiags.ListIndex < 0 Then
            frmEventMsgs.SetButtons "Are you sure?", "", "Yes", "No"
            frmEventMsgs.Show 1
            If frmEventMsgs.Result = 2 Then
                Set ApplTbl = New ApplicationTables
                For i = 0 To lstDiags.ListCount - 1
                    Temp = Trim$(Mid$(lstDiags.List(i), 81, 6))
                    ApplTbl.ApplDeleteCode Trim$(TheTemplate), "SYSTEMNORMAL" + Temp, False
                Next i
                ApplTbl.ApplDeleteCode "CLINICALTEMPLATES", Trim$(TheTemplate), False
                Set ApplTbl = Nothing
                Unload frmOtherFavorites
            End If
        Else
            Set ApplTbl = New ApplicationTables
            Temp = Trim$(Mid$(lstDiags.List(lstDiags.ListIndex), 81, 6))
            ApplTbl.ApplDeleteCode Trim$(TheTemplate), "SYSTEMNORMAL" + Temp, False
            Set ApplTbl = Nothing
            lstDiags.RemoveItem lstDiags.ListIndex
            txtNew.Text = ""
            lstSystem.ListIndex = -1
            lstDiags.ListIndex = -1
            lstSysDiags.Clear
        End If
    End If
End Sub

Private Sub cmdPost_Click()
    If txtNew.Visible Then
        txtNew_KeyPress 13
        txtNew.Text = ""
        txtNew.Locked = False
        PostTheTemplate Trim$(txtTemplate.Text), Trim$(txtRank.Text), TheType
    ElseIf Not txtNew.Visible Then
        txtTemplate_KeyPress 13
    End If
End Sub

Public Function LoadOtherFavorites(IName As String, sRank As String, sType As String, fNewTemplate As Boolean) As Boolean
Dim DisplayText As String
    fNew = fNewTemplate
    DeleteOn = CheckConfigCollection("REMOVEACTIVE") = "T"
    PostOn = False
    TheType = sType
    TheTemplate = IName
    lstDiags.Clear
    lstSystem.Clear
    lblNew.Visible = True
    txtNew.Visible = True
    txtNew.Text = ""
    DisplayText = "External" + Space(42) + "A"
    lstSystem.AddItem DisplayText
    DisplayText = "Pupils" + Space(44) + "B"
    lstSystem.AddItem DisplayText
    DisplayText = "EOM" + Space(47) + "C"
    lstSystem.AddItem DisplayText
    DisplayText = "VF" + Space(48) + "D"
    lstSystem.AddItem DisplayText
    DisplayText = "Lids Lachrymal" + Space(36) + "E"
    lstSystem.AddItem DisplayText
    DisplayText = "Conj/Sclera" + Space(39) + "F"
    lstSystem.AddItem DisplayText
    DisplayText = "Cornea" + Space(44) + "G"
    lstSystem.AddItem DisplayText
    DisplayText = "AC" + Space(48) + "H"
    lstSystem.AddItem DisplayText
    DisplayText = "Iris" + Space(46) + "I"
    lstSystem.AddItem DisplayText
    DisplayText = "Lens" + Space(46) + "J"
    lstSystem.AddItem DisplayText
    DisplayText = "Vitreous" + Space(42) + "K"
    lstSystem.AddItem DisplayText
    DisplayText = "ON" + Space(48) + "L"
    lstSystem.AddItem DisplayText
    DisplayText = "BV" + Space(48) + "M"
    lstSystem.AddItem DisplayText
    DisplayText = "Macula" + Space(44) + "N"
    lstSystem.AddItem DisplayText
    DisplayText = "Retina" + Space(44) + "O"
    lstSystem.AddItem DisplayText
    DisplayText = "Periph" + Space(44) + "P"
    lstSystem.AddItem DisplayText
    DisplayText = "Systemic" + Space(42) + "Q"
    lstSystem.AddItem DisplayText
    DisplayText = "Visual" + Space(40) + "R"
    lstSystem.AddItem DisplayText
    lblHeader.Caption = "Setup Clinical Template"
    If Trim$(IName) <> "" Then
        LoadOtherFavorites = LoadList(IName)
        lstSystem.Visible = True
        lblSystem.Visible = True
        lstDiags.Visible = True
        lblDiags.Visible = True
        lblSysDiags.Visible = True
        lstSysDiags.Visible = True
        lblNew.Visible = True
        txtNew.Visible = True
        lblTemplate.Caption = "Template"
        txtTemplate.Text = IName
        txtRank.Text = sRank
        txtType.Text = sType
        txtTemplate.Locked = True
        txtNew.Locked = False
        cmdDelete.Visible = DeleteOn
    Else
        lstSystem.Visible = False
        lblSystem.Visible = False
        lstDiags.Visible = False
        lblDiags.Visible = False
        lblSysDiags.Visible = False
        lstSysDiags.Visible = False
        lblNew.Visible = False
        txtNew.Visible = False
        lblTemplate.Caption = "Template (Hit <Enter> to Create)"
        txtTemplate.Text = ""
        txtTemplate.Locked = False
        txtNew.Locked = False
        cmdDelete.Visible = False
        LoadOtherFavorites = True
    End If
End Function

Private Sub Form_Load()
    If UserLogin.HasPermission(epPowerUser) Then
        frmOtherFavorites.BorderStyle = 1
        frmOtherFavorites.ClipControls = True
        frmOtherFavorites.Caption = Mid$(frmOtherFavorites.Name, 4, Len(frmOtherFavorites.Name) - 3)
        frmOtherFavorites.AutoRedraw = True
        frmOtherFavorites.Refresh
    End If
End Sub

Private Sub lstDiags_Click()
Dim Temp As Integer
Dim TheSys As String
    If lstDiags.ListIndex >= 0 Then
        TheSys = Mid$(lstDiags.List(lstDiags.ListIndex), 81, 1)
        If TheSys >= "A" And TheSys <= "R" Then
            Temp = lstDiags.ListIndex
            lstSystem.ListIndex = Asc(TheSys) - 65
            lstDiags.ListIndex = Temp
        End If
        txtNew.Text = Trim$(lstDiags.List(lstDiags.ListIndex))
        txtNew.Locked = True
    End If
End Sub

Private Sub lstSysDiags_Click()
    If lstSysDiags.ListIndex >= 0 Then
        txtNew.Text = Trim$(lstSysDiags.List(lstSysDiags.ListIndex))
        txtNew.Locked = True
    End If
End Sub

Private Sub lstSystem_Click()
    If lstSystem.ListIndex >= 0 Then
        LoadDiagsList Mid$(lstSystem.List(lstSystem.ListIndex), Len(lstSystem.List(lstSystem.ListIndex)), 1)
        txtNew.Text = ""
        txtNew.Locked = False
    End If
End Sub

Private Sub txtNew_KeyPress(KeyAscii As Integer)
Dim Temp As String
Dim DisplayText As String
    If KeyAscii = 13 Then
        DisplayText = Space(86)
        If lstDiags.ListIndex >= 0 Then
            Temp = Mid$(lstDiags.List(lstDiags.ListIndex), 81, 1)
            Temp = Left$(lstSystem.List(Asc(Temp) - 65), 2)
            Mid$(DisplayText, 1, 5) = Temp & " - "
            If lstSysDiags.ListIndex < 0 Then
                Mid$(DisplayText, 6, 1) = "*"
                Mid$(DisplayText, 16, Len(Trim$(txtNew.Text))) = Trim$(txtNew.Text)
            Else
                Mid$(DisplayText, 6, Len(Trim$(txtNew.Text))) = Trim$(txtNew.Text)
            End If
            Mid$(DisplayText, 81, 6) = Mid$(lstDiags.List(lstDiags.ListIndex), 81, 6)
            lstDiags.List(lstDiags.ListIndex) = DisplayText
            lstDiags.ListIndex = -1
            lstSystem.ListIndex = -1
            lstSysDiags.ListIndex = -1
            lstSysDiags.Clear
            PostOn = True
        ElseIf lstSystem.ListIndex >= 0 Then
            Temp = Left$(lstSystem.List(lstSystem.ListIndex), 2)
            If Left$(lstSystem.List(lstSystem.ListIndex), 4) = "Conj" Then
                Temp = "Cj"
            End If
            Mid$(DisplayText, 1, 5) = Temp & " - "
            Temp = Chr(65 + lstSystem.ListIndex) & ":" & Trim$(Str$(lstDiags.ListCount + 1))
            If Len(Trim$(Temp)) < 6 Then
                Temp = Temp & Space(6 - Len(Temp))
            End If
            If lstSysDiags.ListIndex < 0 Then
                Mid$(DisplayText, 6, 1) = "*"
                Mid$(DisplayText, 16, Len(Trim$(txtNew.Text))) = Trim$(txtNew.Text)
            Else
                Mid$(DisplayText, 6, Len(Trim$(txtNew.Text))) = Trim$(txtNew.Text)
            End If
            Mid$(DisplayText, 81, 6) = Temp
            lstDiags.AddItem DisplayText
            lstDiags.ListIndex = -1
            lstSystem.ListIndex = -1
            lstSysDiags.ListIndex = -1
            lstSysDiags.Clear
            PostOn = True
        End If
    End If
End Sub

Private Sub txtTemplate_KeyPress(KeyAscii As Integer)
Dim Temp As String
Dim ApplTbl As ApplicationTables
    If KeyAscii = 13 Or KeyAscii = 10 Then
        Temp = "CLINICALTEMPLATES"
        Set ApplTbl = New ApplicationTables
        If ApplTbl.ApplIsCode(Temp, Trim$(txtTemplate.Text)) Then
            frmEventMsgs.InfoMessage "Template already exists"
            frmEventMsgs.Show 1
            txtTemplate.Text = ""
            txtTemplate.SetFocus
        Else
            txtType_Click
            PostTheTemplate Trim$(txtTemplate.Text), Trim$(txtRank.Text), TheType
        End If
        Set ApplTbl = Nothing
    End If
End Sub

Private Function SetupCodes() As Boolean
Dim i As Integer
Dim Ref As String
Dim Ref1 As String
Dim Ref2 As String
Dim ApplTbl As New ApplicationTables
    Ref = UCase$(Trim$(txtTemplate.Text))
    For i = 0 To lstDiags.ListCount - 1
        If Trim$(lstDiags.List(i)) <> "" Then
            Ref1 = "SYSTEMNORMAL" & Trim$(Mid$(lstDiags.List(i), 81, 6))
            Ref2 = Mid$(lstDiags.List(i), 6, 56) & Mid$(lstDiags.List(i), 81, 6)
            ApplTbl.ApplPostCode Ref, Ref1, Ref2, "", True, "", "", "", "", "", "", "", 0
        End If
    Next i
    Set ApplTbl = Nothing
    LoadOtherFavorites UCase$(Trim$(txtTemplate.Text)), Trim$(txtRank.Text), Trim$(txtType.Text), False
End Function

Private Sub LoadDiagsList(TheSys As String)
Dim i As Integer
Dim DisplayText As String
Dim RetDiag As New DiagnosisMasterPrimary
    lstSysDiags.Clear
    RetDiag.PrimarySystem = TheSys
    RetDiag.PrimaryDiagnosis = ""
    RetDiag.PrimaryNextLevelDiagnosis = ""
    RetDiag.PrimaryRank = 0
    RetDiag.PrimaryLevel = 0
    RetDiag.PrimaryBilling = False
    'If RetDiag.FindPrimary() > 0 Then
    If RetDiag.FindPrimaryFiltered() > 0 Then
        i = 1
        While RetDiag.SelectPrimary(i)
            'If Not IsPrimaryThere(Trim$(RetDiag.PrimaryNextLevelDiagnosis)) Then
                DisplayText = Space(75)
                Mid$(DisplayText, 1, Len(Trim$(RetDiag.PrimaryNextLevelDiagnosis))) = Trim$(RetDiag.PrimaryNextLevelDiagnosis)
                Mid$(DisplayText, 11, Len(Trim$(RetDiag.PrimaryLingo))) = Trim$(RetDiag.PrimaryLingo)
                lstSysDiags.AddItem DisplayText
            'End If
            i = i + 1
        Wend
    End If
    Set RetDiag = Nothing
    lstDiags.ListIndex = -1
End Sub

Private Function LoadList(TheName As String) As Boolean
Dim i As Integer, ARk As Long
Dim Temp As String, Temp1 As String, TempC As String
Dim Temp2 As String, Temp3 As String, AMask As String
Dim DisplayText As String, AOrder As String, AFl As String
Dim ApplTbl As New ApplicationTables
    LoadList = False
    lstDiags.Clear
    Set ApplTbl.lstBox = lstDiags
    LoadList = ApplTbl.ApplLoadCodes(Trim$(TheName), False, False, True)
    ApplTbl.ApplGetCode "CLINICALTEMPLATES", Trim$(TheName), TheType, TempC, Temp1, Temp2, Temp3, AMask, AOrder, AFl, ARk
    For i = 0 To lstDiags.ListCount - 1
        DisplayText = Space(86)
        Temp = Mid$(lstDiags.List(i), 57, 1)
        Temp = Left$(lstSystem.List(Asc(Temp) - 65), 2)
        Mid$(DisplayText, 1, 5) = Temp & " - "
        Mid$(DisplayText, 6, 56) = Trim$(Left$(lstDiags.List(i), 56))
        Mid$(DisplayText, 81, 6) = Mid$(lstDiags.List(i), 57, 6)
        lstDiags.List(i) = DisplayText
    Next i
    txtNew.Text = ""
    Set ApplTbl = Nothing
    lstDiags.ListIndex = -1
    LoadList = True
End Function
'
'Private Function IsPrimaryThere(ADiag As String) As Boolean
'If ADiag = "360.52" Then
'    MsgBox ""
'End If
'Dim i As Integer
'    For i = 0 To lstSysDiags.ListCount - 1
'        If ADiag = Trim$(Mid$(lstSysDiags.List(i), 7, 10)) Then
'            IsPrimaryThere = True
'            Exit For
'        End If
'    Next i
'End Function

Private Function IsPrimaryThere(ADiag As String) As Boolean
Dim i As Integer
    For i = 0 To lstSysDiags.ListCount - 1
        If ADiag = Trim$(Mid$(lstSysDiags.List(i), 7, 10)) Then
            IsPrimaryThere = True
            Exit For
        End If
    Next i
End Function

Private Function PostTheTemplate(sTemplateName As String, sRank As String, sType As String) As Boolean
Dim ApplTbl As New ApplicationTables
    If sType = "" Then
        sType = SetType
    End If
    ApplTbl.ApplPostCode "CLINICALTEMPLATES", sTemplateName, sType, "", True, "", "", "", "", "", "", "", val(sRank)
    Set ApplTbl = Nothing
End Function

Private Sub txtType_Click()
    TheType = SetType
End Sub

Private Function SetType() As String
    frmEventMsgs.SetButtons "Select Template Type", "Status Post", "Favorite Findings", "Cancel"
    frmEventMsgs.Show 1
    If frmEventMsgs.Result = 1 Then
        SetType = "S"
    ElseIf frmEventMsgs.Result = 2 Then
        SetType = "F"
    Else
        SetType = TheType
    End If
    txtType.Text = SetType
End Function
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
