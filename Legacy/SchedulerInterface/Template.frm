VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmTemplate 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1710
      ItemData        =   "Template.frx":0000
      Left            =   7320
      List            =   "Template.frx":0002
      TabIndex        =   15
      Top             =   3600
      Visible         =   0   'False
      Width           =   2955
   End
   Begin VB.CheckBox chkOnlyDate 
      BackColor       =   &H00A95911&
      Caption         =   "Applies to only date selected"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7320
      TabIndex        =   14
      Top             =   6480
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.TextBox txtColor 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8160
      MaxLength       =   10
      TabIndex        =   12
      Top             =   2280
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.ListBox lstStartTime 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3630
      ItemData        =   "Template.frx":0004
      Left            =   6120
      List            =   "Template.frx":0006
      MultiSelect     =   1  'Simple
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox PracticeName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1455
      MaxLength       =   64
      TabIndex        =   2
      Top             =   2235
      Visible         =   0   'False
      Width           =   5295
   End
   Begin VB.ListBox lstPractice 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "Template.frx":0008
      Left            =   1455
      List            =   "Template.frx":000A
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   120
      Width           =   5280
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   0
      Top             =   6990
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Template.frx":000C
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2775
      Left            =   360
      TabIndex        =   4
      Top             =   3240
      Visible         =   0   'False
      Width           =   2775
      _Version        =   65537
      _ExtentX        =   4895
      _ExtentY        =   4895
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      BevelColorFace  =   14285823
      BevelColorShadow=   15794174
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10080
      TabIndex        =   5
      Top             =   6990
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Template.frx":01EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5280
      TabIndex        =   6
      Top             =   6990
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Template.frx":03CA
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   9960
      Top             =   2280
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Flags           =   1
   End
   Begin VB.Label lblLoc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   7320
      TabIndex        =   16
      Top             =   3240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lblColor 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Color"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6960
      TabIndex        =   13
      Top             =   2280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblAvail 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H008D312C&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Hours of Availability"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   360
      TabIndex        =   11
      Top             =   2880
      Visible         =   0   'False
      Width           =   4140
   End
   Begin VB.Label lblDay 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sun"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   4800
      TabIndex        =   10
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
   End
   Begin VB.Label lblSTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Block Time"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   645
      Left            =   4800
      TabIndex        =   9
      Top             =   3240
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblName 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   8
      Top             =   2235
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblTemplate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   255
      TabIndex        =   7
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PracticeId As Long
Private SetDate As String
Private TheDate As String
Private BreakDown As Integer

Private Sub SetControls(DisplayType As Boolean)
Dim i As Integer
Dim Temp As String
Dim RemoveOn As Boolean
lblTemplate.Visible = Not DisplayType
lstPractice.Visible = Not DisplayType
cmdApply.Visible = DisplayType
lblName.Visible = DisplayType
PracticeName.Visible = DisplayType
lblDay.Visible = DisplayType
lblAvail.Visible = DisplayType
lblSTime.Visible = DisplayType
lstStartTime.Visible = DisplayType
SSMonth1.Visible = DisplayType
txtColor.Visible = DisplayType
lblColor.Visible = DisplayType
chkOnlyDate.Visible = DisplayType
lblLoc.Visible = DisplayType
lstLoc.Visible = DisplayType
If (DisplayType) Then
    RemoveOn = False
    Call GetGlobalEntry("REMOVEACTIVE", Temp)
    If (Temp = "T") Then
        RemoveOn = True
    End If
    cmdDelete.Visible = RemoveOn
    If (PracticeId < 1) Then
        cmdDelete.Visible = False
    End If
Else
    cmdDelete.Visible = False
End If
For i = 0 To lstStartTime.ListCount - 1
    lstStartTime.Selected(i) = False
Next i
End Sub

Private Sub LoadTemplate(PracticeId As Long)
Dim TheLoc As Long
Dim ODate As Boolean
Dim i As Integer, l As Integer
Dim k As Integer, z As Integer
Dim VacDay As String, Temp As String
Dim Yr As String, PrCity As String
Dim PCat1 As String, PCat2 As String
Dim PName As String, PColor As String, PVac As String
Dim Ps1 As String, Ps2 As String, Ps3 As String, Ps4 As String, Ps5 As String, Ps6 As String, Ps7 As String
Dim Pe1 As String, Pe2 As String, Pe3 As String, Pe4 As String, Pe5 As String, Pe6 As String, Pe7 As String
Dim ApplTbl As ApplicationTables
Dim ApplSch As ApplicationScheduler
If (PracticeId >= 0) Then
    Set ApplSch = New ApplicationScheduler
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplGetTemplate(PracticeId, PName, PrCity, PColor, PVac, ODate, Ps1, Ps2, Ps3, Ps4, Ps5, Ps6, Ps7, Pe1, Pe2, Pe3, Pe4, Pe5, Pe6, Pe7, TheLoc, PCat1, PCat2)) Then
        PracticeName.Text = PName
        txtColor.Text = Trim(PColor)
        SSMonth1.X.SelectedDays.RemoveAll
        If (Trim(PVac) <> "") Then
            k = 1
            l = Len(PVac)
            Yr = Trim(Str(Year(Date)))
            VacDay = Mid(PVac, k, 2) + "/" + Mid(PVac, k + 2, 2) + "/" + Yr
            If (IsDate(VacDay)) Then
                SSMonth1.X.SelectedDays.Add VacDay
            End If
        End If
        Call SetControls(True)
        i = GetTime(Trim(Ps1), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Ps2), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Ps3), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Ps4), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Ps5), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Ps6), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Ps7), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe1), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe2), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe3), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe4), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe5), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe6), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(Pe7), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        chkOnlyDate.Value = 0
        If (ODate) Then
            chkOnlyDate.Value = 1
        End If
        Set ApplSch.lstStartTime = lstLoc
        Call ApplSch.LoadLocList
        For z = 0 To lstLoc.ListCount - 1
            If (TheLoc = Val(Mid(lstLoc.List(z), 57, Len(lstLoc.List(z)) - 56))) Then
                lstLoc.ListIndex = z
                Exit For
            End If
        Next z
    End If
    PracticeName.SetFocus
    SendKeys "{Home}"
    Set ApplTbl = Nothing
    Set ApplSch = Nothing
End If
SSMonth1.ShowCentury = False
End Sub

Public Function LoadTemplateDisplay() As Boolean
Dim Temp As String
Dim ApplTbl As ApplicationTables
Call GetGlobalEntry("DURATIONBREAKDOWN", Temp)
BreakDown = Val(Temp)
If (BreakDown < 5) Then
    BreakDown = 15
End If
Call SetControls(False)
Call LoadTime(lstStartTime, "", "", BreakDown)
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
LoadTemplateDisplay = ApplTbl.ApplLoadPracticeList("T", True)
lstPractice.Visible = True
Set ApplTbl = Nothing
End Function

Private Sub cmdBack_Click()
Unload frmTemplate
End Sub

Private Sub cmdDelete_Click()
Dim Adt As Audit
Dim ApplTbl As ApplicationTables
If (PracticeId > 0) Then
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplDeletePractice(PracticeId)) Then
        Set Adt = New Audit
        Call Adt.PostAudit("D", Trim(Str(frmHome.GetUserId)), PracticeId, "PracticeName")
        Set Adt = Nothing
        PracticeId = 0
    End If
    Set ApplTbl = Nothing
    Unload frmTemplate
End If
End Sub

Private Sub cmdApply_Click()
Dim VacDay As String
Dim dd As String
Dim mm As String
Dim k As Integer
Dim i As Integer
Dim Adt As Audit
Dim TheLoc As Long
Dim TheAction As String
Dim ODate As Boolean
Dim PrCity As String
Dim Ps1 As String, Ps2 As String, Ps3 As String, Ps4 As String, Ps5 As String, Ps6 As String, Ps7 As String
Dim Pe1 As String, Pe2 As String, Pe3 As String, Pe4 As String, Pe5 As String, Pe6 As String, Pe7 As String
Dim ApplTbl As ApplicationTables
If (Trim(PracticeName.Text) = "") Then
    Unload frmTemplate
    Exit Sub
ElseIf (Trim(PracticeName.Text) = "") Then
    frmEventMsgs.Header = "Please select a name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    PracticeName.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
Set ApplTbl = New ApplicationTables
If (PracticeId < 1) Then
    If (ApplTbl.ApplIsTemplate(PracticeName.Text)) Then
        frmEventMsgs.Header = "Template Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        PracticeName.SetFocus
        SendKeys "{Home}+{End}"
        Exit Sub
    End If
End If
TheAction = "C"
If (PracticeId < 1) Then
    TheAction = "A"
End If
ODate = False
If (chkOnlyDate.Value = 1) Then
    ODate = True
End If
TheLoc = 0
If (lstLoc.ListIndex >= 0) Then
    TheLoc = Val(Mid(lstLoc.List(lstLoc.ListIndex), 57, Len(lstLoc.List(lstLoc.ListIndex)) - 56))
End If
VacDay = SetDate
VacDay = Left(VacDay, Len(VacDay) - 4)
Call StripCharacters(VacDay, "/")
For i = 0 To lstStartTime.ListCount - 1
    If (lstStartTime.Selected(i)) Then
        k = k + 1
        If (k = 1) Then
            Ps1 = Trim(lstStartTime.List(i))
        ElseIf (k = 2) Then
            Pe1 = Trim(lstStartTime.List(i))
        ElseIf (k = 3) Then
            Ps2 = Trim(lstStartTime.List(i))
        ElseIf (k = 4) Then
            Pe2 = Trim(lstStartTime.List(i))
        ElseIf (k = 5) Then
            Ps3 = Trim(lstStartTime.List(i))
        ElseIf (k = 6) Then
            Pe3 = Trim(lstStartTime.List(i))
        ElseIf (k = 7) Then
            Ps4 = Trim(lstStartTime.List(i))
        ElseIf (k = 8) Then
            Pe4 = Trim(lstStartTime.List(i))
        ElseIf (k = 9) Then
            Ps5 = Trim(lstStartTime.List(i))
        ElseIf (k = 10) Then
            Pe5 = Trim(lstStartTime.List(i))
        ElseIf (k = 11) Then
            Ps6 = Trim(lstStartTime.List(i))
        ElseIf (k = 12) Then
            Pe6 = Trim(lstStartTime.List(i))
        ElseIf (k = 13) Then
            Ps7 = Trim(lstStartTime.List(i))
        ElseIf (k = 14) Then
            Pe7 = Trim(lstStartTime.List(i))
        End If
    End If
Next i
If (ApplTbl.ApplPostTemplate(PracticeId, PracticeName.Text, PrCity, txtColor.Text, VacDay, ODate, Ps1, Ps2, Ps3, Ps4, Ps5, Ps6, Ps7, Pe1, Pe2, Pe3, Pe4, Pe5, Pe6, Pe7, TheLoc, "", "")) Then
    Set Adt = New Audit
    Call Adt.PostAudit(TheAction, Trim(Str(frmHome.GetUserId)), PracticeId, "PracticeName")
    Set Adt = Nothing
Else
    frmEventMsgs.Header = "Update Not Performed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Set ApplTbl = Nothing
    PracticeName.SetFocus
    SendKeys "{Home}+{End}"
End If
Set ApplTbl = Nothing
Call LoadTemplateDisplay
End Sub

Private Sub Form_Load()
If (frmHome.GetPowerUser) Then
    frmTemplate.BorderStyle = 1
    frmTemplate.ClipControls = True
    frmTemplate.Caption = Mid(frmTemplate.Name, 4, Len(frmTemplate.Name) - 3)
    frmTemplate.AutoRedraw = True
    frmTemplate.Refresh
End If
End Sub

Private Sub lstPractice_Click()
If (lstPractice.ListIndex > 0) Then
    PracticeId = Val(Trim(Mid(lstPractice.List(lstPractice.ListIndex), 70, 5)))
    Call LoadTemplate(PracticeId)
ElseIf (lstPractice.ListIndex = 0) Then
    PracticeId = 0
    Call LoadTemplate(PracticeId)
End If
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
TheDate = SelDate
SetDate = SelDate
Call FormatTodaysDate(TheDate, True)
Call FormatTodaysDate(SetDate, False)
lblDay.Caption = TheDate
End Sub

Private Sub txtColor_Click()
CommonDialog1.CancelError = True
On Error GoTo UI_ErrorHandler
CommonDialog1.Flags = &H1&
CommonDialog1.ShowColor
txtColor.Text = Trim(Str(CommonDialog1.Color))
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub txtColor_KeyPress(KeyAscii As Integer)
Call txtColor_Click
KeyAscii = 0
End Sub

