VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCLOrder 
   BackColor       =   &H0077742D&
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Order CL"
   ClientHeight    =   8610
   ClientLeft      =   2055
   ClientTop       =   435
   ClientWidth     =   8295
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8610
   ScaleWidth      =   8295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox QuantityOS 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   21
      Top             =   2760
      Width           =   1575
   End
   Begin VB.ListBox InventoryListOS 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2070
      ItemData        =   "CLOrder.frx":0000
      Left            =   120
      List            =   "CLOrder.frx":0002
      TabIndex        =   20
      Top             =   3240
      Width           =   8055
   End
   Begin VB.ListBox InventoryListOD 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2070
      ItemData        =   "CLOrder.frx":0004
      Left            =   120
      List            =   "CLOrder.frx":0006
      TabIndex        =   17
      Top             =   480
      Width           =   8055
   End
   Begin VB.TextBox ShipToZip 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6720
      TabIndex        =   14
      Top             =   6840
      Width           =   1455
   End
   Begin VB.TextBox ShipToState 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   5760
      TabIndex        =   13
      Top             =   6840
      Width           =   855
   End
   Begin VB.TextBox ShipToCity 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2280
      TabIndex        =   12
      Top             =   6840
      Width           =   3375
   End
   Begin VB.TextBox ShipToAddress2 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6360
      TabIndex        =   11
      Top             =   6240
      Width           =   1815
   End
   Begin VB.TextBox ShipToAddress1 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2280
      TabIndex        =   10
      Top             =   6240
      Width           =   3975
   End
   Begin VB.TextBox ShipToName 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2280
      TabIndex        =   9
      Top             =   5640
      Width           =   5895
   End
   Begin VB.OptionButton OptionOther 
      BackColor       =   &H0077742D&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   8
      Top             =   6480
      Width           =   375
   End
   Begin VB.OptionButton OptionHome 
      BackColor       =   &H0077742D&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   7
      Top             =   6120
      Width           =   375
   End
   Begin VB.OptionButton OptionOffice 
      BackColor       =   &H0077742D&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   6
      Top             =   5760
      Width           =   375
   End
   Begin VB.TextBox QuantityOD 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   0
      Top             =   0
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   15
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrder.frx":0008
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   6360
      TabIndex        =   16
      Top             =   7560
      Visible         =   0   'False
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrder.frx":01E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFill 
      Height          =   990
      Left            =   2160
      TabIndex        =   19
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrder.frx":03D3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHold 
      Height          =   990
      Left            =   4200
      TabIndex        =   24
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLOrder.frx":05BC
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   5760
      TabIndex        =   25
      Top             =   0
      Width           =   75
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Quantity"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   1200
      TabIndex        =   23
      Top             =   2760
      Width           =   1020
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "OS"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   22
      Top             =   2760
      Width           =   420
   End
   Begin VB.Label lblEye 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "OD"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   18
      Top             =   0
      Width           =   435
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Other"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   480
      TabIndex        =   5
      Top             =   6480
      Width           =   705
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Home"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   480
      TabIndex        =   4
      Top             =   6120
      Width           =   765
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Office"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   480
      TabIndex        =   3
      Top             =   5760
      Width           =   780
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Ship To:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   2
      Top             =   5400
      Width           =   1065
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Quantity"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   1200
      TabIndex        =   1
      Top             =   0
      Width           =   1020
   End
End
Attribute VB_Name = "frmCLOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public WearType As String
Public InventoryIdOD As String
Public InventoryIdOS As String
Public PatientId As Long
Public AppointmentId As Long
Public OrderIdOD As Long
Public OrderIdOS As Long
Public ReorderOn As Boolean
Public JoinedOn As Boolean
Public ClnId As Long
Private InventoryCostOD As Single
Private InventoryCostOS As Single
Private mCriteria As String

Private Type ODOST
    InvId As Long
    SP As String
    CY As String
    Ax As String
    AR As String
    BC As String
    DI As String
    Pc As String
End Type


Private Sub cmdHome_Click()
OrderIdOD = 0
OrderIdOS = 0
Unload frmCLOrder
End Sub

Private Sub cmdFill_Click()
Dim Temp As String
Dim TheFile As String
Dim CLOrder As CLOrders
If (Validate) Then
    If (Trim(QuantityOD.Text) <> "") Then
        Call PostOrders("OD", 1, True)
    End If
    If (Trim(QuantityOS.Text) <> "") Then
        Call PostOrders("OS", 1, True)
    End If
    If (OrderIdOD > 0) Or (OrderIdOS > 0) Then
        If (FillOrder(False, 5)) Then
            frmEventMsgs.Header = "Collect Fee ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                If (JoinedOn) Then
''''''                    frmPayments.PatientId = PatientId
''''''                    frmPayments.ReceivableId = 0
''''''                    If (frmPayments.LoadPayments(True, "")) Then
''''''                        frmPayments.Show 1
''''''                    End If
                    Dim PatientDemographics As New PatientDemographics
                    PatientDemographics.PatientId = PatientId
                    Call PatientDemographics.DisplayPatientFinancialScreen
                    Set PatientDemographics = Nothing
                Else
                    If (OrderIdOD > 0) Then
                        Call PayOrder(OrderIdOD)
                    ElseIf (OrderIdOS > 0) Then
                        Call PayOrder(OrderIdOS)
                    End If
                End If
            End If
            Unload frmCLOrder
        Else
            frmEventMsgs.Header = "Order was not filled."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
   End If
End If
End Sub

Private Sub cmdHold_Click()
Dim RcvId As Long
Dim Temp As String
Dim CLOrder As CLOrders
If (Validate) Then
    If (Trim(QuantityOD.Text) <> "") Then
        Call PostOrders("OD", 10, False)
    End If
    If (Trim(QuantityOS.Text) <> "") Then
        Call PostOrders("OS", 10, False)
    End If
    If (OrderIdOD > 0) Or (OrderIdOS > 0) Then
        If (FillOrder(True, 10)) Then
            frmEventMsgs.Header = "Collect Fee ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                If (JoinedOn) Then
''''''                    frmPayments.PatientId = PatientId
''''''                    frmPayments.ReceivableId = 0
''''''                    If (frmPayments.LoadPayments(True, "")) Then
''''''                        frmPayments.Show 1
''''''                    End If
                    Dim PatientDemographics As New PatientDemographics
                    PatientDemographics.PatientId = PatientId
                    Call PatientDemographics.DisplayPatientFinancialScreen
                    Set PatientDemographics = Nothing
                Else
                    If (OrderIdOD > 0) Then
                        Call PayOrder(OrderIdOD)
                    ElseIf (OrderIdOS > 0) Then
                        Call PayOrder(OrderIdOD)
                    End If
                End If
            End If
        End If
    End If
    Unload frmCLOrder
End If
End Sub

Private Sub cmdDone_Click()
Dim Temp As String
Dim CLOrder As CLOrders
If (Validate) Then
    If (Trim(QuantityOD.Text) <> "") Then
        Call PostOrders("OD", 2, False)
    End If
    If (Trim(QuantityOS.Text) <> "") Then
        Call PostOrders("OS", 2, False)
    End If
    If (OrderIdOD > 0) Or (OrderIdOS > 0) Then
        If (FillOrder(True, 2)) Then
            frmEventMsgs.Header = "Collect Fee ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                If (JoinedOn) Then
''''''                    frmPayments.PatientId = PatientId
''''''                    frmPayments.ReceivableId = 0
''''''                    If (frmPayments.LoadPayments(True, "")) Then
''''''                        frmPayments.Show 1
''''''                    End If
                    Dim PatientDemographics As New PatientDemographics
                    PatientDemographics.PatientId = PatientId
                    Call PatientDemographics.DisplayPatientFinancialScreen
                    Set PatientDemographics = Nothing
                Else
                    If (OrderIdOD > 0) Then
                        Call PayOrder(OrderIdOD)
                    ElseIf (OrderIdOS > 0) Then
                        Call PayOrder(OrderIdOD)
                    End If
                End If
            End If
        End If
    End If
    Unload frmCLOrder
End If
End Sub

Private Function Validate() As Boolean
Validate = True
If (Trim(InventoryIdOD) = "") And (Trim(InventoryIdOS) = "") Then
    Validate = False
    frmEventMsgs.Header = "Inventory ID Required."
ElseIf (PatientId < 1) Then
    Validate = False
ElseIf ((QuantityOD.Text = "" Or Not IsNumeric(QuantityOD.Text) Or InStrPS(1, QuantityOD.Text, ".") <> 0 Or InStrPS(1, QuantityOD.Text, "-") <> 0)) And _
       ((QuantityOS.Text = "" Or Not IsNumeric(QuantityOS.Text) Or InStrPS(1, QuantityOS.Text, ".") <> 0 Or InStrPS(1, QuantityOS.Text, "-") <> 0)) Then
    Validate = False
    frmEventMsgs.Header = "Quantity Required."
ElseIf (OptionHome.Value = False And OptionOffice.Value = False And OptionOther.Value = False) Then
    Validate = False
    frmEventMsgs.Header = "Need ShipTo Instructions."
ElseIf (OptionOther.Value = True) Then
    If (ShipToName.Text = "" Or ShipToAddress1.Text = "" Or ShipToCity.Text = "" Or ShipToState.Text = "" Or ShipToZip.Text = "") Then
        Validate = False
        frmEventMsgs.Header = "Need ShipTo Address."
    End If
End If
If Not (Validate) Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Function

Private Sub Form_Load()
Dim CLOrder As CLOrders
JoinedOn = False
If (WearType = "PC") Then
    frmCLOrder.Caption = "Order Spectacles"
Else
    WearType = "CL"
    frmCLOrder.Caption = "Order CL"
End If
If (OrderIdOD > 0) Then
    Set CLOrder = New CLOrders
    CLOrder.Criteria = "OrderID = " + Trim(Str(OrderIdOD))
    If (WearType = "PC") Then
        Call CLOrder.FindPCOrders
    Else
        Call CLOrder.FindCLOrders
    End If
    InventoryIdOD = CLOrder.InventoryId
    PatientId = CLOrder.PatientId
    QuantityOD.Text = Trim(Str(CLOrder.Qty))
    InventoryCostOD = CLOrder.Cost
    If (CLOrder.ShipTo = "Home") Then
        OptionHome.Value = True
        Call LoadShipToFromPatient
    ElseIf (CLOrder.ShipTo = "Office") Then
        OptionOffice.Value = True
        Call LoadShipToFromOffice
    Else
        OptionOther.Value = True
        ShipToAddress1.Text = CLOrder.ShipAddress1
        ShipToAddress2.Text = CLOrder.ShipAddress2
        ShipToCity.Text = CLOrder.ShipCity
        ShipToState.Text = CLOrder.ShipState
        ShipToZip.Text = CLOrder.ShipZip
    End If
    Set CLOrder = Nothing
End If
If (OrderIdOS > 0) Then
    Set CLOrder = New CLOrders
    CLOrder.Criteria = "OrderID = " + Trim(Str(OrderIdOS))
    If (WearType = "PC") Then
        Call CLOrder.FindPCOrders
    Else
        Call CLOrder.FindCLOrders
    End If
    InventoryIdOS = CLOrder.InventoryId
    PatientId = CLOrder.PatientId
    QuantityOS.Text = Trim(Str(CLOrder.Qty))
    InventoryCostOS = CLOrder.Cost
    If (CLOrder.ShipTo = "Home") Then
        OptionHome.Value = True
        Call LoadShipToFromPatient
    ElseIf (CLOrder.ShipTo = "Office") Then
        OptionOffice.Value = True
    Else
        OptionOther.Value = True
        ShipToAddress1.Text = CLOrder.ShipAddress1
        ShipToAddress2.Text = CLOrder.ShipAddress2
        ShipToCity.Text = CLOrder.ShipCity
        ShipToState.Text = CLOrder.ShipState
        ShipToZip.Text = CLOrder.ShipZip
    End If
    Set CLOrder = Nothing
End If
If (Trim(InventoryIdOD) <> "") Then
    Call LoadItems("InventoryID = " + Trim(InventoryIdOD) + " ", True)
End If
If (Trim(InventoryIdOS) <> "") Then
    Call LoadItems("InventoryID = " + Trim(InventoryIdOS) + " ", False)
End If
Call SetVisibleItems
End Sub

Private Sub LoadShipToFromPatient()
Dim Patient As Patient
If (PatientId > 0) Then
    Set Patient = New Patient
    Patient.PatNumber = PatientId
    If (Patient.FindPatientDirect > 0) Then
        Call Patient.SelectPatient(1)
        ShipToName.Text = Patient.FirstName + " " + Patient.LastName
        If (OptionHome.Value) Then
            ShipToAddress1.Text = Patient.Address
            ShipToAddress2.Text = Patient.Suite
            ShipToCity.Text = Patient.City
            ShipToState.Text = Patient.State
            ShipToZip.Text = Patient.Zip
        Else
            ShipToAddress1.Text = ""
            ShipToAddress2.Text = ""
            ShipToCity.Text = ""
            ShipToState.Text = ""
            ShipToZip.Text = ""
        End If
    Else
        ShipToName.Text = ""
        ShipToAddress1.Text = ""
        ShipToAddress2.Text = ""
        ShipToCity.Text = ""
        ShipToState.Text = ""
        ShipToZip.Text = ""
    End If
    Set Patient = Nothing
End If
End Sub

Private Sub LoadShipToFromOffice()
Dim i As Integer
Dim RetPrc As PracticeName
Dim Patient As Patient
If (PatientId > 0) Then
    Set Patient = New Patient
    Patient.PatNumber = PatientId
    If (Patient.FindPatientDirect > 0) Then
        Call Patient.SelectPatient(1)
        ShipToName.Text = Patient.FirstName + " " + Patient.LastName
    End If
    Set Patient = Nothing
End If
Set RetPrc = New PracticeName
RetPrc.PracticeType = "P"
RetPrc.PracticeName = Chr(1)
If (RetPrc.FindPractice > 0) Then
    i = 1
    Do Until Not (RetPrc.SelectPractice(i))
        If (Trim(RetPrc.PracticeLocationReference) = "") Then
            If (Trim(ShipToName.Text) = "") Then
                ShipToName.Text = Trim(RetPrc.PracticeName)
            End If
            ShipToAddress1.Text = Trim(RetPrc.PracticeAddress)
            ShipToAddress2.Text = Trim(RetPrc.PracticeSuite)
            ShipToCity.Text = Trim(RetPrc.PracticeCity)
            ShipToState.Text = Trim(RetPrc.PracticeState)
            ShipToZip.Text = Trim(RetPrc.PracticeZip)
            Exit Do
        End If
        i = i + 1
    Loop
End If
Set RetPrc = Nothing
End Sub

Private Sub getValues(OD As Boolean, ODOS As ODOST)
Dim RS As Recordset
Set RS = GetRS("select findingdetail , patientid, appointmentid from patientclinical where clinicalid = " & ClnId)

Dim RX As String
If Not RS.EOF Then
    RX = RS.Fields(0)
    If PatientId = 0 Then PatientId = RS.Fields(1)
    If AppointmentId = 0 Then AppointmentId = RS.Fields(2)
End If
Dim i(3) As Integer
If OD Then
    i(0) = InStrPS(1, RX, "-2/")
    i(1) = InStrPS(1, RX, "-3/")
    i(2) = InStrPS(1, RX, "-5/")
    i(3) = InStrPS(1, RX, "-6/")
Else
    i(0) = InStrPS(1, RX, "-3/")
    i(1) = InStrPS(1, RX, "-4/")
    i(2) = InStrPS(1, RX, "-6/")
    i(3) = InStrPS(1, RX, "-7/")
End If

Dim tmp(3) As String
If Not RX = "" Then tmp(0) = Trim(Mid(RX, i(2) + 3, i(3) - i(2) - 3))
If tmp(0) = "" Then tmp(0) = 0
ODOS.InvId = tmp(0)
tmp(0) = ""

If Not RX = "" Then
    RX = Trim(Mid(RX, i(0) + 3, i(1) - i(0) - 3))
End If

Dim BrP As Integer
Dim b As Integer
Dim n As Integer
Dim BrWord(3) As String
Dim NWord(3) As String
NWord(0) = "D OR N:"
NWord(1) = "VA NEAR:"

BrWord(0) = "PERIPH CURVE:"
BrWord(1) = "DIAMETER:"
BrWord(2) = "BASE CURVE:"
BrWord(3) = "ADD-READING:"


For b = 0 To 3
    n = -1
    Select Case b
    Case 0
        n = b
    Case 3
        n = 1
    End Select
    If Not n < 0 Then
        BrP = InStrPS(1, RX, NWord(n))
        If BrP > 0 Then
            RX = Trim(Mid(RX, 1, BrP - 1))
        End If
    End If
    
    BrP = InStrPS(1, RX, BrWord(b))
    If BrP > 0 Then
        tmp(b) = (Mid(RX, BrP + Len(BrWord(b))))
        RX = Trim(Mid(RX, 1, BrP - 1))
    End If
Next

ODOS.Pc = Trim(tmp(0))
ODOS.DI = Trim(tmp(1))
ODOS.BC = Trim(tmp(2))
ODOS.AR = Trim(tmp(3))

For b = 0 To 3
    tmp(b) = ""
Next
b = 0

Do Until RX = "" Or b > 3
    BrP = InStrPS(1, RX, " ")
    If BrP > 0 Then
        tmp(b) = Trim(Mid(RX, 1, BrP - 1))
        RX = Trim(Mid(RX, BrP + 1))
        b = b + 1
    Else
        If Len(RX) > 1 Then
            tmp(b) = RX
            RX = ""
        End If
    End If
Loop

ODOS.SP = tmp(0)
If tmp(2) = "" Then
    If tmp(1) = "" Then
        ODOS.CY = ""
        ODOS.Ax = ""
    Else
        If UCase(Left(tmp(1), 1)) = "X" Then
            ODOS.CY = ""
            ODOS.Ax = tmp(1)
        Else
            ODOS.CY = tmp(1)
            ODOS.Ax = ""
        End If
    End If
Else
    ODOS.CY = tmp(1)
    ODOS.Ax = tmp(2)
End If

End Sub

Private Sub getComments(CommOD As String, CommOS As String)

Dim RS As Recordset
Set RS = GetRS("select * from patientclinical where clinicalid = " & ClnId)

Dim Delim As Integer
Dim img(2) As String
img(0) = Trim(RS("ImageDescriptor"))
Select Case img(0)
Case "^OD:^OS:"
    img(0) = ""
End Select
If Not img(0) = "" Then
    If Len(img(0)) > 3 Then
        Select Case Mid(img(0), 2, 3)
        Case "OD:"
            img(1) = Trim(Mid(img(0), 5))
            Delim = InStrPS(1, img(1), "OS:")
            If Delim > 0 Then
                img(2) = Trim(Mid(img(1), Delim + 3))
                img(1) = Trim(Left(img(1), Delim - 1))
            End If
            If Right(img(1), 3) = "-&-" Then
                img(1) = Left(img(1), Len(img(1)) - 3)
            End If
        Case "OS:"
            img(2) = Mid(img(0), 5)
        Case Else
            img(1) = Mid(img(0), 2)
        End Select
    End If
End If

CommOD = img(1)
CommOS = img(2)

End Sub


Private Function LoadItems(Criteria As String, IWho As Boolean) As Boolean

Dim RetrieveCLInventory As CLInventory
Dim RetrievePCInventory As PCInventory
Dim Temp As String
Dim DisplayItem As String
If (IWho) Then
    InventoryListOD.Visible = True
    InventoryListOD.Clear
Else
    InventoryListOS.Visible = True
    InventoryListOS.Clear
End If
If (WearType = "PC") Then
    Set RetrievePCInventory = New PCInventory
    RetrievePCInventory.Criteria = Criteria
    RetrievePCInventory.OrderBy = "InventoryID"
    LoadItems = RetrievePCInventory.FindPCInventory
    If (LoadItems) Then
        RetrievePCInventory.SelectPCInventory (1)
        DisplayItem = Space(86)
        Mid(DisplayItem, 1, 6) = Trim(Str(RetrievePCInventory.Qty))
        If (IWho) Then
            InventoryCostOD = RetrievePCInventory.Cost
        Else
            InventoryCostOS = RetrievePCInventory.Cost
        End If
        If (RetrieveCLInventory.Cost < 0) Then
            Mid(DisplayItem, 10, 10) = "$0.00"
        Else
            Call DisplayDollarAmount(Trim(Str(RetrievePCInventory.Cost)), Temp)
            Mid(DisplayItem, 10, 10) = Trim(Temp)
        End If
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 15) = RetrievePCInventory.Manufacturer
        Mid(DisplayItem, 30, 15) = RetrievePCInventory.Model
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 15) = RetrievePCInventory.Type_
        Mid(DisplayItem, 30, 15) = RetrievePCInventory.PCColor
        Mid(DisplayItem, 45, 15) = Trim(Str(RetrievePCInventory.PCSize))
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 15) = RetrievePCInventory.LensCoating
        Mid(DisplayItem, 30, 15) = RetrievePCInventory.LensTint
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 10) = "PD:" + Trim(Str(RetrievePCInventory.PDD))
        Mid(DisplayItem, 30, 10) = "PN:" + Trim(Str(RetrievePCInventory.PDN))
        Mid(DisplayItem, 45, 10) = "CG:" + Trim(Str(RetrievePCInventory.CEGHeight))
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
    End If
    Set RetrievePCInventory = Nothing
Else
    Dim ODOS As ODOST
    getValues IWho, ODOS
    Dim CommOD As String
    Dim CommOS As String
    Call getComments(CommOD, CommOS)
    
    Set RetrieveCLInventory = New CLInventory
    If Right(Criteria, 4) = "= 0 " Then
        Criteria = "InventoryID = " & ODOS.InvId & " "
    End If
    mCriteria = Criteria
    RetrieveCLInventory.Criteria = mCriteria
    RetrieveCLInventory.OrderBy = "InventoryID"
    LoadItems = RetrieveCLInventory.FindCLInventory
    If (LoadItems) Then
        RetrieveCLInventory.SelectCLInventory (1)
        DisplayItem = Space(86)
        Mid(DisplayItem, 1, 6) = Trim(Str(RetrieveCLInventory.Qty))
        If (IWho) Then
            InventoryCostOD = RetrieveCLInventory.Cost
        Else
            InventoryCostOS = RetrieveCLInventory.Cost
        End If
        If (RetrieveCLInventory.Cost < 0) Then
            Mid(DisplayItem, 10, 10) = "$0.00"
        Else
            Call DisplayDollarAmount(Trim(Str(RetrieveCLInventory.Cost)), Temp)
            Mid(DisplayItem, 10, 10) = Trim(Temp)
        End If
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 40) = RetrieveCLInventory.Series
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 40) = RetrieveCLInventory.Manufacturer
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 15) = RetrieveCLInventory.Type_
        Mid(DisplayItem, 30, 15) = RetrieveCLInventory.Material
        Mid(DisplayItem, 45, 15) = RetrieveCLInventory.Disposable
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
'        Mid(DisplayItem, 10, 15) = RetrieveCLInventory.WearTime
'        Mid(DisplayItem, 30, 15) = RetrieveCLInventory.Tint
        If (IWho) Then
            DisplayItem = Space(9) & CommOD
            InventoryListOD.AddItem DisplayItem
        Else
            DisplayItem = Space(9) & CommOS
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 10) = "SP:" + ODOS.SP 'RetrieveCLInventory.SpherePower
        Mid(DisplayItem, 30, 10) = "CY:" + ODOS.CY 'RetrieveCLInventory.CylinderPower
        Mid(DisplayItem, 45, 10) = "AX:" + ODOS.Ax 'RetrieveCLInventory.Axis
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 10) = "AR:" + ODOS.AR 'RetrieveCLInventory.AddReading
        Mid(DisplayItem, 30, 10) = "BC:" + ODOS.BC 'RetrieveCLInventory.BaseCurve
        Mid(DisplayItem, 45, 10) = "DI:" + ODOS.DI 'RetrieveCLInventory.Diameter
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
        DisplayItem = Space(86)
        Mid(DisplayItem, 10, 10) = "PC:" + ODOS.Pc 'RetrieveCLInventory.PeriphCurve
        If (IWho) Then
            InventoryListOD.AddItem DisplayItem
        Else
            InventoryListOS.AddItem DisplayItem
        End If
    End If
    Set RetrieveCLInventory = Nothing
End If
End Function

Private Sub SetVisibleItems()
If (Trim(InventoryListOD.ListCount) = 0) Then
    lblEye.Visible = False
    Label2.Visible = False
    QuantityOD.Visible = False
Else
    lblEye.Visible = True
    Label2.Visible = True
    QuantityOD.Visible = True
End If
If (Trim(InventoryListOS.ListCount) = 0) Then
    Label3.Visible = False
    Label7.Visible = False
    QuantityOS.Visible = False
Else
    Label3.Visible = True
    Label7.Visible = True
    QuantityOS.Visible = True
End If
End Sub

Private Function PayOrder(OrderId As Long) As Boolean
Dim RcvId As Long
Dim RetrieveCLOrders As CLOrders
PayOrder = False
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
    RetrieveCLOrders.OrderBy = "OrderID"
    RetrieveCLOrders.FindCLOrders
    RcvId = RetrieveCLOrders.ReceivableId
    Set RetrieveCLOrders = Nothing
    If (RcvId > 0) Then
        PayOrder = True
        If (frmQuickPay.LoadPayments(RcvId)) Then
            frmQuickPay.Show 1
        End If
    End If
End If
End Function

Private Sub PostOrders(AEye As String, AStatus As Integer, FillOn As Boolean)
Dim Temp As String
Dim CLOrder As CLOrders
Dim OrderId As Long
Dim AQty As Single
Dim InvCost As Single
Dim InvId As Long
Set CLOrder = New CLOrders
If (AEye = "OD") Then
    OrderId = OrderIdOD
    AQty = val(Trim(QuantityOD.Text))
    InvId = InventoryIdOD
    InvCost = InventoryCostOD
Else
    OrderId = OrderIdOS
    AQty = val(Trim(QuantityOS.Text))
    InvId = InventoryIdOS
    InvCost = InventoryCostOS
End If
If (OrderId > 0) Then
    CLOrder.Criteria = "OrderID = " + CStr(OrderId)
    If (WearType = "PC") Then
        CLOrder.FindPCOrders
    Else
        CLOrder.FindCLOrders
    End If
    Call CLOrder.SelectCLOrders(1)
    If (OptionHome.Value = True) Then
        CLOrder.ShipTo = "H"
    ElseIf (OptionOffice.Value = True) Then
        CLOrder.ShipTo = "O"
    Else
        CLOrder.ShipTo = "X"
        CLOrder.ShipAddress1 = ShipToAddress1.Text
        CLOrder.ShipAddress2 = ShipToAddress2.Text
        CLOrder.ShipCity = ShipToCity.Text
        CLOrder.ShipState = ShipToState.Text
        CLOrder.ShipZip = ShipToZip.Text
    End If
    CLOrder.Eye = AEye
    CLOrder.Qty = AQty
    CLOrder.AppointmentId = AppointmentId
    If (FillOn) Then
        Temp = ""
        Call FormatTodaysDate(Temp, False)
        Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
        CLOrder.OrderComplete = Temp
    End If
    CLOrder.Status = AStatus
    CLOrder.PutCLOrder
Else
    CLOrder.Criteria = "OrderID = 0"
    If (WearType = "PC") Then
        CLOrder.FindPCOrders
    Else
        CLOrder.FindCLOrders
    End If
    CLOrder.OrderType = "C"
    If (WearType = "PC") Then
        CLOrder.OrderType = "G"
    End If
    CLOrder.Eye = AEye
    CLOrder.InventoryId = InvId
    CLOrder.Qty = AQty
    CLOrder.Cost = InvCost
    CLOrder.PatientId = PatientId
    CLOrder.AppointmentId = AppointmentId
    If (OptionHome.Value) Then
        CLOrder.ShipTo = "H"
    ElseIf (OptionOffice.Value) Then
        CLOrder.ShipTo = "O"
    Else
        CLOrder.ShipTo = "X"
        CLOrder.ShipAddress1 = ShipToAddress1.Text
        CLOrder.ShipAddress2 = ShipToAddress2.Text
        CLOrder.ShipCity = ShipToCity.Text
        CLOrder.ShipState = ShipToState.Text
        CLOrder.ShipZip = ShipToZip.Text
    End If
    Temp = ""
    Call FormatTodaysDate(Temp, False)
    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
    CLOrder.OrderDate = Temp
    CLOrder.OrderComplete = ""
    If (FillOn) Then
        CLOrder.OrderComplete = Temp
    End If
    CLOrder.Status = AStatus
    CLOrder.PutCLOrder
    If (AEye = "OD") Then
        OrderIdOD = CLOrder.OrderId
    Else
        OrderIdOS = CLOrder.OrderId
    End If
End If
Set CLOrder = Nothing
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Call cmdHome_Click
End Sub

Private Sub OptionOffice_Click()
Call LoadShipToFromOffice
End Sub

Private Sub OptionOther_Click()
Call LoadShipToFromPatient
End Sub

Private Sub OptionHome_Click()
Call LoadShipToFromPatient
End Sub

Private Function FillOrder(OrderOn As Boolean, AStat As Integer) As Boolean
Dim Action As Boolean
Dim AdjustInventory As Boolean
Dim PatId As Long, OrderId As Long
Dim RcvId As Long, ApptId As Long
Dim Temp As String
Dim RetRcv As PatientReceivables
Dim RetrieveCLOrders As CLOrders
Dim RetrieveCLInventory As CLInventory
Dim RetrievePCInventory As PCInventory
FillOrder = False
Action = False
AdjustInventory = False
If (OptionOffice.Value) Then
    AdjustInventory = True
End If
If (OrderIdOD > 0) Then
    OrderId = OrderIdOD
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
    RetrieveCLOrders.OrderBy = "OrderID"
    RetrieveCLOrders.FindCLOrders
    ApptId = RetrieveCLOrders.AppointmentId
    GoSub PostSrvs
    Set RetrieveCLOrders = Nothing
    GoSub OtherSrvs
ElseIf (OrderIdOS > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderIdOS)) + " "
    RetrieveCLOrders.OrderBy = "OrderID"
    RetrieveCLOrders.FindCLOrders
    ApptId = RetrieveCLOrders.AppointmentId
    GoSub PostSrvs
    Set RetrieveCLOrders = Nothing
End If

If (Action) Then
    FillOrder = True
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        PatId = RetRcv.PatientId
    End If
    Set RetRcv = Nothing
    Dim PatientDemographics As New PatientDemographics
    PatientDemographics.PatientId = PatId
    Call PatientDemographics.DisplayInvoiceEditAndBillScreen(RcvId)
End If
Exit Function
OtherSrvs:
    If (OrderIdOS > 0) Then
        OrderId = OrderIdOS
        If (OrderId > 0) Then
            Set RetrieveCLOrders = New CLOrders
            RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
            RetrieveCLOrders.OrderBy = "OrderID"
            RetrieveCLOrders.FindCLOrders
            If (ApptId = RetrieveCLOrders.AppointmentId) And (RetrieveCLOrders.AppointmentId > 0) Then
                GoSub PostOtherSrvs
            End If
            Set RetrieveCLOrders = Nothing
        End If
    End If
    Return
PostOtherSrvs:
    If (RetrieveCLOrders.OrderType = "C") Then
        Set RetrieveCLInventory = New CLInventory
        'RetrieveCLInventory.Criteria = "InventoryID = " + Trim(Str(RetrieveCLOrders.InventoryId))
        RetrieveCLInventory.FindCLInventory
        'If (RetrieveCLInventory.SelectCLInventory(1)) Then
            If (AddToClaim(RcvId, RetrieveCLOrders.Cost, RetrieveCLOrders.Qty, RetrieveCLOrders.Eye)) Then
                If (AdjustInventory) Then
                    RetrieveCLInventory.Qty = RetrieveCLInventory.Qty - RetrieveCLOrders.Qty
                    RetrieveCLInventory.PutCLInventory
                End If
                RetrieveCLOrders.Status = AStat
                RetrieveCLOrders.ReceivableId = RcvId
                If Not (OrderOn) Then
                    RetrieveCLOrders.Status = 5
                    Temp = ""
                    Call FormatTodaysDate(Temp, False)
                    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                    RetrieveCLOrders.OrderComplete = Temp
                End If
                RetrieveCLOrders.PutCLOrder
                Action = True
            End If
        'End If
        Set RetrieveCLInventory = Nothing
    Else
        Set RetrievePCInventory = New PCInventory
        RetrievePCInventory.Criteria = "InventoryID = " + Trim(Str(RetrieveCLOrders.InventoryId))
        RetrievePCInventory.FindPCInventory
        If (RetrievePCInventory.SelectPCInventory(1)) Then
            If (AddToClaim(RcvId, RetrieveCLOrders.Cost, RetrieveCLOrders.Qty, RetrieveCLOrders.Eye)) Then
                If (AdjustInventory) Then
                    RetrievePCInventory.Qty = RetrievePCInventory.Qty - RetrieveCLOrders.Qty
                    RetrievePCInventory.PutPCInventory
                End If
                RetrieveCLOrders.Status = AStat
                RetrieveCLOrders.ReceivableId = RcvId
                If Not (OrderOn) Then
                    RetrieveCLOrders.Status = 5
                    Temp = ""
                    Call FormatTodaysDate(Temp, False)
                    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                    RetrieveCLOrders.OrderComplete = Temp
                End If
                RetrieveCLOrders.PutCLOrder
                Action = True
            End If
        End If
        Set RetrievePCInventory = Nothing
    End If
    Return
PostSrvs:
    If (RetrieveCLOrders.OrderType = "C") Then
        Set RetrieveCLInventory = New CLInventory
        'RetrieveCLInventory.Criteria = "InventoryID = " + Trim(Str(RetrieveCLOrders.InventoryId))
        RetrieveCLInventory.Criteria = mCriteria
        RetrieveCLInventory.FindCLInventory
        'If (RetrieveCLInventory.SelectCLInventory(1)) Then
            If (CreateManualClaim(RetrieveCLOrders.PatientId, RetrieveCLOrders.OrderDate, RetrieveCLOrders.AppointmentId, RetrieveCLOrders.Cost, RetrieveCLOrders.Qty, RcvId, RetrieveCLOrders.Eye)) Then
                If (AdjustInventory) Then
                    RetrieveCLInventory.Qty = RetrieveCLInventory.Qty - RetrieveCLOrders.Qty
                    RetrieveCLInventory.PutCLInventory
                End If
                RetrieveCLOrders.ReceivableId = RcvId
                RetrieveCLOrders.Status = AStat
                If Not (OrderOn) Then
                    RetrieveCLOrders.Status = 5
                    Temp = ""
                    Call FormatTodaysDate(Temp, False)
                    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                    RetrieveCLOrders.OrderComplete = Temp
                End If
                RetrieveCLOrders.PutCLOrder
                Action = True
            End If
        'End If
        Set RetrieveCLInventory = Nothing
    Else
        Set RetrievePCInventory = New PCInventory
        RetrievePCInventory.Criteria = "InventoryID = " + Trim(Str(RetrieveCLOrders.InventoryId))
        RetrievePCInventory.FindPCInventory
        If (RetrievePCInventory.SelectPCInventory(1)) Then
            If (CreateManualClaim(RetrieveCLOrders.PatientId, RetrieveCLOrders.OrderDate, RetrieveCLOrders.AppointmentId, RetrieveCLOrders.Cost, RetrieveCLOrders.Qty, RcvId, RetrieveCLOrders.Eye)) Then
                If (AdjustInventory) Then
                    RetrievePCInventory.Qty = RetrievePCInventory.Qty - RetrieveCLOrders.Qty
                    RetrievePCInventory.PutPCInventory
                End If
                RetrieveCLOrders.Status = AStat
                RetrieveCLOrders.ReceivableId = RcvId
                If Not (OrderOn) Then
                    RetrieveCLOrders.Status = 5
                    Temp = ""
                    Call FormatTodaysDate(Temp, False)
                    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                    RetrieveCLOrders.OrderComplete = Temp
                End If
                RetrieveCLOrders.PutCLOrder
                Action = True
            End If
        End If
        Set RetrievePCInventory = Nothing
    End If
    Return
End Function

Private Function CreateManualClaim(PatId As Long, ApptDate As String, ApptId As Long, ACost As Single, AQuant As Long, ARcvId As Long, AEye As String) As Boolean
Dim MyAppt As Long
Dim RcvId As Long
Dim RcvId1 As Long
Dim ADate As String
Dim RetAppt As SchedulerAppointment
Dim ApplTemp As ApplicationTemplates
Dim ApplClm As ApplicationClaims
CreateManualClaim = False
ARcvId = 0
If (PatId > 0) And (Trim(ApptDate) <> "") Then
    JoinedOn = False
'''    If Not (ReorderOn) Then
'''        frmEventMsgs.Header = "Separate or Joined Claim ?"
'''        frmEventMsgs.AcceptText = "Separate"
'''        frmEventMsgs.RejectText = "Joined"
'''        frmEventMsgs.CancelText = "Cancel"
'''        frmEventMsgs.Other0Text = ""
'''        frmEventMsgs.Other1Text = ""
'''        frmEventMsgs.Other2Text = ""
'''        frmEventMsgs.Other3Text = ""
'''        frmEventMsgs.Other4Text = ""
'''        frmEventMsgs.Show 1
'''    Else
'''        frmEventMsgs.Result = 1
'''    End If
'''    If (frmEventMsgs.Result = 1) Then
        If (Not ReorderOn) Then
            Set ApplTemp = New ApplicationTemplates
            RcvId = ApplTemp.ApplGetReceivableIdByApptId(ApptId, PatId, ADate)
            Set ApplTemp = Nothing
            If (SetupReceivableAndService(0, ApptId, RcvId1, ADate, ACost, AQuant, AEye)) Then
                ARcvId = RcvId1
                If (RcvId1 > 0) Then
                    CreateManualClaim = True
                End If
            End If
        Else    ' Reorders require a Manual Claim method
            Call FormatTodaysDate(ADate, False)
            ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = ApptId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                Set ApplClm = New ApplicationClaims
                MyAppt = ApplClm.BuildAppointment(PatId, RetAppt.AppointmentResourceId1, RetAppt.AppointmentResourceId2, RetAppt.AppointmentTypeId, ADate, RetAppt.AppointmentInsType, 0, 0, True, False)
                If (MyAppt > 0) Then
                    If (SetupReceivableAndService(0, MyAppt, RcvId1, ADate, ACost, AQuant, AEye)) Then
                        ARcvId = RcvId1
                        If (RcvId1 > 0) Then
                            CreateManualClaim = True
                        End If
                    End If
                End If
                Set ApplClm = Nothing
            End If
            Set RetAppt = Nothing
        End If
'''    ElseIf (frmEventMsgs.Result = 2) Then
'''        JoinedOn = True
'''        Set ApplTemp = New ApplicationTemplates
'''        RcvId = ApplTemp.ApplGetReceivableIdByApptId(ApptId, PatId, ADate)
'''        Set ApplTemp = Nothing
'''        If (RcvId > 0) And (Trim(ADate) <> "") Then
'''            If (SetupReceivableAndService(RcvId, ApptId, RcvId1, ADate, ACost, AQuant, AEye)) Then
'''                ARcvId = RcvId1
'''                If (RcvId1 > 0) Then
'''                    CreateManualClaim = True
'''                End If
'''            End If
'''        End If
'''    End If
End If
End Function

Private Function SetupReceivableAndService(RcvId As Long, ApptId As Long, RcvId1 As Long, AptDate As String, ACost As Single, AQuant As Long, AEye As String) As Boolean
Dim Copay As Single
Dim ApptIdNew As Long
Dim InsurerId As Long, InsuredId As Long
Dim InsType As String, InvoiceId As String
Dim ApplClaim As ApplicationClaims
Dim ApplSch As ApplicationScheduler
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim RetrieveClinical As PatientClinical
Dim RetrieveService As PatientReceivableService
Dim RetRcv As PatientReceivables
Dim RetAppt As SchedulerAppointment
SetupReceivableAndService = False
InsType = ""
InvoiceId = ""
Copay = 0
RcvId1 = 0
ApptIdNew = 0
InsurerId = 0
InsuredId = 0
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If Not (RetAppt.RetrieveSchedulerAppointment) Then
        Set RetAppt = Nothing
        Exit Function
    End If
Else
    Exit Function
End If
If (RcvId < 1) Then
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetInsurance(RetAppt.AppointmentPatientId, RetAppt.AppointmentDate, InsuredId, InsurerId, Copay, InsType, True)
    Set ApplList = Nothing
    Set ApplClaim = New ApplicationClaims
    ApptIdNew = ApplClaim.BuildAppointment(RetAppt.AppointmentPatientId, RetAppt.AppointmentResourceId1, RetAppt.AppointmentResourceId2, RetAppt.AppointmentTypeId, _
                Format(Now, "yyyymmdd"), RetAppt.AppointmentInsType, RetAppt.AppointmentReferralId, 0, True, False) 'Made PrecertId to 0 To fix 2284.
    If (ApptIdNew > 0) Then
        Call ApplClaim.BuildReceivable(RcvId1, RetAppt.AppointmentPatientId, InsuredId, InsurerId, 0, RetAppt.AppointmentResourceId1, ApptIdNew, RetAppt.AppointmentDate, "", "", "", "", "", "", "", "", "", "", InvoiceId, 0, "I", False)
        If (RcvId1 > 0) Then
            Set RetrieveClinical = New PatientClinical
            RetrieveClinical.ClinicalId = 0
            Call RetrieveClinical.RetrievePatientClinical
            RetrieveClinical.ClinicalType = "K"
            RetrieveClinical.AppointmentId = ApptIdNew
            RetrieveClinical.PatientId = RetAppt.AppointmentPatientId
            RetrieveClinical.EyeContext = ""
            RetrieveClinical.ImageDescriptor = ""
            RetrieveClinical.ImageInstructions = ""
            RetrieveClinical.Findings = "367.9"
            RetrieveClinical.Symptom = "1"
            Call RetrieveClinical.ApplyPatientClinical
            Set RetrieveClinical = Nothing
            GoSub SetSrv
            Call ApplClaim.SetReceivableCosts(RcvId1, RetAppt.AppointmentReferralId, RetAppt.AppointmentResourceId1, False, 0, "")
        End If
    End If
    Set ApplClaim = Nothing
    SetupReceivableAndService = True
Else
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        InvoiceId = RetRcv.ReceivableInvoice
        GoSub SetSrv
        Set ApplClaim = New ApplicationClaims
        Call ApplClaim.SetReceivableCosts(RcvId, RetAppt.AppointmentReferralId, RetRcv.ReceivableBillToDr, False, RetRcv.ReceivableBillOffice, "")
        Set ApplClaim = Nothing
        RcvId1 = RcvId
        SetupReceivableAndService = True
    End If
End If
Set RetAppt = Nothing
Exit Function
SetSrv:
    Set RetrieveService = New PatientReceivableService
    RetrieveService.ItemId = 0
    Call RetrieveService.RetrievePatientReceivableService
    RetrieveService.Invoice = InvoiceId
    RetrieveService.Service = "92326"
    RetrieveService.ServiceDate = RetAppt.AppointmentDate
    RetrieveService.ServiceQuantity = AQuant
    RetrieveService.ServiceCharge = ACost
    RetrieveService.ServiceFeeCharge = ACost
    RetrieveService.ServiceTOS = ""
    RetrieveService.ServicePOS = ""
    RetrieveService.ServiceModifier = ""
    If (AEye = "OS") Then
        RetrieveService.ServiceModifier = "LT"
    ElseIf (AEye = "OD") Then
        RetrieveService.ServiceModifier = "RT"
    End If
    RetrieveService.ServiceLinkedDiag = ""
    RetrieveService.ServiceOrderDoc = False
    RetrieveService.ServiceConsultOn = False
    RetrieveService.ServiceCreateDate = Format(Now, "yyyymmdd")
    Call RetrieveService.ApplyPatientReceivableService
    Set RetrieveService = Nothing
    Return
End Function

Private Function AddToClaim(RcvId As Long, ACost As Single, AQuant As Long, AEye As String) As Boolean
Dim ApplClaim As ApplicationClaims
Dim RetrieveService As PatientReceivableService
Dim RetRcv As PatientReceivables
AddToClaim = False
If (RcvId > 0) Then
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        Set RetrieveService = New PatientReceivableService
        RetrieveService.ItemId = 0
        Call RetrieveService.RetrievePatientReceivableService
        RetrieveService.Invoice = RetRcv.ReceivableInvoice
        RetrieveService.Service = "92326"
        RetrieveService.ServiceDate = RetRcv.ReceivableInvoiceDate
        RetrieveService.ServiceQuantity = AQuant
        RetrieveService.ServiceCharge = ACost
        RetrieveService.ServiceFeeCharge = ACost
        RetrieveService.ServiceTOS = ""
        RetrieveService.ServicePOS = ""
        RetrieveService.ServiceModifier = ""
        If (AEye = "OS") Then
            RetrieveService.ServiceModifier = "LT"
        ElseIf (AEye = "OD") Then
            RetrieveService.ServiceModifier = "RT"
        End If
        RetrieveService.ServiceLinkedDiag = ""
        RetrieveService.ServiceOrderDoc = False
        RetrieveService.ServiceConsultOn = False
        Call RetrieveService.ApplyPatientReceivableService
        Set RetrieveService = Nothing
        Set ApplClaim = New ApplicationClaims
        Call ApplClaim.SetReceivableCosts(RcvId, 0, RetRcv.ReceivableBillToDr, False, RetRcv.ReceivableBillOffice, "")
        Set ApplClaim = Nothing
        AddToClaim = True
    End If
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

