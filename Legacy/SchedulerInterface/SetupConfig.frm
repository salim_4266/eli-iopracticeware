VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSetupConfig 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtAutoLogOff 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   142
      Tag             =   "D-OTHERPRT"
      Top             =   7440
      Width           =   1335
   End
   Begin VB.CheckBox chkERX 
      BackColor       =   &H00808000&
      Caption         =   "e-Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   141
      Tag             =   "D-INSERTTAB"
      Top             =   5040
      Width           =   1335
   End
   Begin VB.CheckBox chkDxOdrImp 
      BackColor       =   &H00808000&
      Caption         =   "Dx Odr Imp"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1320
      TabIndex        =   140
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   6960
      Width           =   1455
   End
   Begin VB.CheckBox chkNewWear 
      BackColor       =   &H00808000&
      Caption         =   "New Wear"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   7800
      TabIndex        =   139
      Tag             =   "D-PRINT"
      Top             =   7440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CheckBox chkMUON 
      BackColor       =   &H00808000&
      Caption         =   "MU On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   138
      Tag             =   "D-PRINT"
      Top             =   3480
      Width           =   1695
   End
   Begin VB.CheckBox chkMUChkIn 
      BackColor       =   &H00808000&
      Caption         =   "MU Checkin"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8520
      TabIndex        =   137
      Tag             =   "S-REQUIRESS"
      Top             =   3360
      Width           =   1575
   End
   Begin VB.CheckBox chkInsBat 
      BackColor       =   &H00808000&
      Caption         =   "Ins Batch New"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   7680
      TabIndex        =   136
      Tag             =   "D-PRINT"
      Top             =   120
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox chkAging 
      BackColor       =   &H00808000&
      Caption         =   "Export Aging On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   135
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   840
      Width           =   1935
   End
   Begin VB.CheckBox chkCollOpt 
      BackColor       =   &H00808000&
      Caption         =   "Collect Optical On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9840
      TabIndex        =   134
      Tag             =   "D-PRINT"
      Top             =   3120
      Width           =   2415
   End
   Begin VB.CheckBox chkNpi 
      BackColor       =   &H00808000&
      Caption         =   "NPI Required"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   133
      Tag             =   "D-PRINT"
      Top             =   2760
      Width           =   1695
   End
   Begin VB.CheckBox chkNExp 
      BackColor       =   &H00808000&
      Caption         =   "Export New"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   132
      Tag             =   "D-PRINT"
      Top             =   600
      Width           =   1455
   End
   Begin VB.CheckBox chkCheckIn 
      BackColor       =   &H00808000&
      Caption         =   "Require CheckIn"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   131
      Tag             =   "S-REMOVEACTIVE"
      Top             =   1080
      Width           =   2055
   End
   Begin VB.TextBox txtGCopies 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   1
      TabIndex        =   129
      Tag             =   "S-REVIEWWINDOW"
      Top             =   2400
      Width           =   615
   End
   Begin VB.CheckBox chkgform 
      BackColor       =   &H00808000&
      Caption         =   "Glasses Form"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   128
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   1320
      Width           =   1815
   End
   Begin VB.CheckBox chkOptMed 
      BackColor       =   &H00808000&
      Caption         =   "Opt/Med On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8160
      TabIndex        =   127
      Tag             =   "D-PRINT"
      Top             =   3120
      Width           =   1695
   End
   Begin VB.CheckBox chkZips 
      BackColor       =   &H00808000&
      Caption         =   "Zip On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   126
      Tag             =   "D-PRINT"
      Top             =   3240
      Width           =   1455
   End
   Begin VB.CheckBox chkUb 
      BackColor       =   &H00808000&
      Caption         =   "UB92 On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   7680
      TabIndex        =   125
      Tag             =   "D-PRINT"
      Top             =   360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CheckBox chkDLoad 
      BackColor       =   &H00808000&
      Caption         =   "Auto Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   124
      Tag             =   "D-PRINT"
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CheckBox chkExport 
      BackColor       =   &H00808000&
      Caption         =   "Loc/Doc Export On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   123
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   3000
      Width           =   2295
   End
   Begin VB.CheckBox chkRx1 
      BackColor       =   &H00808000&
      Caption         =   "Print Wear"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1320
      TabIndex        =   122
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   6720
      Width           =   1335
   End
   Begin VB.TextBox txtPrinter6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7800
      TabIndex        =   120
      Tag             =   "D-PRINTER3"
      Top             =   8400
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CheckBox chkTD 
      BackColor       =   &H00808000&
      Caption         =   "Test Devices"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   119
      Tag             =   "S-USETODAYONLY"
      Top             =   3240
      Width           =   1695
   End
   Begin VB.CheckBox chkGlass 
      BackColor       =   &H00808000&
      Caption         =   "Glasses On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   118
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   3000
      Width           =   1695
   End
   Begin VB.CheckBox chkPatPay 
      BackColor       =   &H00808000&
      Caption         =   "Rpt Pat Paid"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8160
      TabIndex        =   117
      Tag             =   "D-PRINT"
      Top             =   2880
      Width           =   1575
   End
   Begin VB.TextBox txtLimit 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9120
      MaxLength       =   5
      TabIndex        =   115
      Tag             =   "S-STMTRANGE"
      Top             =   1560
      Width           =   735
   End
   Begin VB.CheckBox chkNQTest 
      BackColor       =   &H00808000&
      Caption         =   "NQ Test On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   114
      Tag             =   "D-PRINT"
      Top             =   1560
      Width           =   1695
   End
   Begin VB.CheckBox chkIOrder 
      BackColor       =   &H00808000&
      Caption         =   "Img Odr"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   113
      Tag             =   "D-PRINT"
      Top             =   6960
      Width           =   1215
   End
   Begin VB.CheckBox chkBBL 
      BackColor       =   &H00808000&
      Caption         =   "Bus Log On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   112
      Tag             =   "D-PRINT"
      Top             =   1560
      Width           =   1575
   End
   Begin VB.CheckBox chkUseRv 
      BackColor       =   &H00808000&
      Caption         =   "Use RV"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   111
      Tag             =   "D-PRINT"
      Top             =   6720
      Width           =   1095
   End
   Begin VB.CheckBox chkNewQ 
      BackColor       =   &H00808000&
      Caption         =   "New Quantifiers"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   110
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   3360
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CheckBox chkBackup 
      BackColor       =   &H00808000&
      Caption         =   "Backup Scratch"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   109
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   7680
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CheckBox chkRset 
      BackColor       =   &H00808000&
      Caption         =   "Reset Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   108
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   7440
      Width           =   1935
   End
   Begin VB.CheckBox chkNoHighs 
      BackColor       =   &H00808000&
      Caption         =   "Delay Highlights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   107
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   7200
      Width           =   1935
   End
   Begin VB.CheckBox chkPC 
      BackColor       =   &H00808000&
      Caption         =   "Inventory On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   106
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   360
      Width           =   1695
   End
   Begin VB.CheckBox chkMyRx 
      BackColor       =   &H00808000&
      Caption         =   "Default Prev Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   105
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   6960
      Width           =   1815
   End
   Begin VB.CheckBox chkJpg 
      BackColor       =   &H00808000&
      Caption         =   "Use JPG"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   104
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   6720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CheckBox chkLineD 
      BackColor       =   &H00808000&
      Caption         =   "Draw Line"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   103
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   6240
      Width           =   1335
   End
   Begin VB.CheckBox chkTaskManager 
      BackColor       =   &H00808000&
      Caption         =   "Task Manager"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   102
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   6480
      Width           =   1815
   End
   Begin VB.CheckBox chkSign 
      BackColor       =   &H00808000&
      Caption         =   "Signature On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   101
      Tag             =   "D-PRINT"
      Top             =   600
      Width           =   1695
   End
   Begin VB.CheckBox chkBillDr 
      BackColor       =   &H00808000&
      Caption         =   "Require Bill Dr"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8160
      TabIndex        =   100
      Tag             =   "D-PRINT"
      Top             =   2640
      Width           =   1695
   End
   Begin VB.CheckBox chkCal 
      BackColor       =   &H00808000&
      Caption         =   "New Calendar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   99
      Tag             =   "D-PRINT"
      Top             =   2040
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox chkReqRef 
      BackColor       =   &H00808000&
      Caption         =   "Require Referral"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9840
      TabIndex        =   98
      Tag             =   "S-ALLCHECKOUT"
      Top             =   2160
      Width           =   1935
   End
   Begin VB.CheckBox chkWI 
      BackColor       =   &H00808000&
      Caption         =   "Print Written"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   97
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   5760
      Width           =   1695
   End
   Begin VB.CheckBox chkRV 
      BackColor       =   &H00808000&
      Caption         =   "RV not CC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   96
      Tag             =   "D-PRINT"
      Top             =   6000
      Width           =   1455
   End
   Begin VB.CheckBox chkCC 
      BackColor       =   &H00808000&
      Caption         =   "Require CC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   95
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   5520
      Width           =   1455
   End
   Begin VB.TextBox txtSlotDays 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      MaxLength       =   4
      TabIndex        =   93
      Tag             =   "S-DAYS"
      Top             =   2400
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtSlotSelects 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   2
      TabIndex        =   91
      Tag             =   "S-SELECTIONS"
      Top             =   2400
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtSlug 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1680
      MaxLength       =   2
      TabIndex        =   89
      Tag             =   "S-SLUG"
      Top             =   1800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkDILog 
      BackColor       =   &H00808000&
      Caption         =   "ChkOut Logout"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   88
      Tag             =   "D-PRINT"
      Top             =   5280
      Width           =   2175
   End
   Begin VB.CheckBox chkRef1 
      BackColor       =   &H00808000&
      Caption         =   "Autofill Ref Dr"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   87
      Tag             =   "S-INTERNAL"
      Top             =   2280
      Width           =   1695
   End
   Begin VB.CheckBox chkHipaa 
      BackColor       =   &H00808000&
      Caption         =   "Debug Hipaa"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   86
      Tag             =   "D-PRINT"
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CheckBox chkTaxId 
      BackColor       =   &H00808000&
      Caption         =   "Use Dr Tax Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8160
      TabIndex        =   85
      Tag             =   "S-AUTORECALL"
      Top             =   2160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CheckBox chkRecall 
      BackColor       =   &H00808000&
      Caption         =   "Auto Recall"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   84
      Tag             =   "S-AUTORECALL"
      Top             =   2520
      Width           =   1575
   End
   Begin VB.TextBox txtRRate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      MaxLength       =   5
      TabIndex        =   82
      Tag             =   "S-TESTFILE1"
      Top             =   6360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkTab 
      BackColor       =   &H00808000&
      Caption         =   "Insert Tab"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8760
      TabIndex        =   81
      Tag             =   "D-INSERTTAB"
      Top             =   5040
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox chkRx 
      BackColor       =   &H00808000&
      Caption         =   "Auto Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   80
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   4800
      Width           =   1095
   End
   Begin VB.TextBox txtOtherDI 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7800
      TabIndex        =   78
      Tag             =   "D-OTHERPRT"
      Top             =   7920
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.TextBox txtPrinter1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   73
      Tag             =   "D-PRINTER1"
      Top             =   7920
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox txtPrinter2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   72
      Tag             =   "D-PRINTER2"
      Top             =   8400
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox txtPrinter3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   71
      Tag             =   "D-PRINTER3"
      Top             =   7920
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.TextBox txtPrinter4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   70
      Tag             =   "D-PRINTER4"
      Top             =   8400
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CheckBox chkSetImpr 
      BackColor       =   &H00808000&
      Caption         =   "Impressions On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   69
      Tag             =   "D-SETIMPRESSIONS"
      Top             =   4080
      Width           =   1935
   End
   Begin VB.TextBox txtDICopies 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   67
      Tag             =   "D-PROCLINKTYPE"
      Top             =   6360
      Width           =   375
   End
   Begin VB.CheckBox chkAllCheck 
      BackColor       =   &H00808000&
      Caption         =   "Checkout All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   66
      Tag             =   "S-ALLCHECKOUT"
      Top             =   120
      Width           =   1695
   End
   Begin VB.CheckBox chkDiToday 
      BackColor       =   &H00808000&
      Caption         =   "View Today"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   65
      Tag             =   "S-USETODAYONLY"
      Top             =   3600
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CheckBox chkSiToday 
      BackColor       =   &H00808000&
      Caption         =   "View Today"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8160
      TabIndex        =   64
      Tag             =   "D-USETODAYONLY"
      Top             =   1920
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox chkDraw 
      BackColor       =   &H00808000&
      Caption         =   "Print Images"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   28
      Tag             =   "D-PRINTDRAWIMAGE"
      Top             =   4320
      Width           =   1575
   End
   Begin VB.TextBox txtPLink 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      MaxLength       =   1
      TabIndex        =   25
      Tag             =   "D-PROCLINKTYPE"
      Top             =   6360
      Width           =   375
   End
   Begin VB.CheckBox chkQuery 
      BackColor       =   &H00808000&
      Caption         =   "Query Batch"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4680
      TabIndex        =   6
      Tag             =   "D-PRINT"
      Top             =   2760
      Width           =   1575
   End
   Begin VB.CheckBox chkCheckNumber 
      BackColor       =   &H00808000&
      Caption         =   "Require Check #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2760
      TabIndex        =   3
      Tag             =   "D-PRINT"
      Top             =   600
      Width           =   2055
   End
   Begin VB.CheckBox chkDirect 
      BackColor       =   &H00808000&
      Caption         =   "Direct Queue"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9840
      TabIndex        =   5
      Tag             =   "S-PRINTCHECKIN"
      Top             =   2400
      Width           =   1575
   End
   Begin VB.CheckBox chkAudit 
      BackColor       =   &H00808000&
      Caption         =   "Allow Audit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2760
      TabIndex        =   7
      Tag             =   "S-REMOVEACTIVE"
      Top             =   840
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox txtCalDate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   10
      TabIndex        =   2
      Tag             =   "S-CALENDARDATE"
      Top             =   1800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox chkNames 
      BackColor       =   &H00808000&
      Caption         =   "View Names"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   27
      Tag             =   "D-VIEWNAMES"
      Top             =   3840
      Width           =   1935
   End
   Begin VB.TextBox txtTrans 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   3
      TabIndex        =   0
      Tag             =   "S-TXTYPE"
      Top             =   600
      Width           =   855
   End
   Begin VB.TextBox txtSys98 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   60
      Tag             =   "D-SYSTEMNORMAL98"
      Top             =   6960
      Width           =   3495
   End
   Begin VB.TextBox txtSys99 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   59
      Tag             =   "D-SYSTEMNORMAL99"
      Top             =   6960
      Width           =   3495
   End
   Begin VB.TextBox txtSys18 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   56
      Tag             =   "D-SYSTEMNORMAL9"
      Top             =   6600
      Width           =   3495
   End
   Begin VB.TextBox txtSys17 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   55
      Tag             =   "D-SYSTEMNORMAL8"
      Top             =   6240
      Width           =   3495
   End
   Begin VB.TextBox txtSys16 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   54
      Tag             =   "D-SYSTEMNORMAL7"
      Top             =   5880
      Width           =   3495
   End
   Begin VB.TextBox txtSys15 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   53
      Tag             =   "D-SYSTEMNORMAL6"
      Top             =   5520
      Width           =   3495
   End
   Begin VB.TextBox txtSys14 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   52
      Tag             =   "D-SYSTEMNORMAL5"
      Top             =   5160
      Width           =   3495
   End
   Begin VB.TextBox txtSys13 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   51
      Tag             =   "D-SYSTEMNORMAL4"
      Top             =   4800
      Width           =   3495
   End
   Begin VB.TextBox txtSys12 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   50
      Tag             =   "D-SYSTEMNORMAL3"
      Top             =   4440
      Width           =   3495
   End
   Begin VB.TextBox txtSys11 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   49
      Tag             =   "D-SYSTEMNORMAL2"
      Top             =   4080
      Width           =   3495
   End
   Begin VB.TextBox txtSys10 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6480
      MaxLength       =   32
      TabIndex        =   48
      Tag             =   "D-SYSTEMNORMAL1"
      Top             =   3720
      Width           =   3495
   End
   Begin VB.TextBox txtSys9 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   47
      Tag             =   "D-SYSTEMNORMAL9"
      Top             =   6600
      Width           =   3495
   End
   Begin VB.TextBox txtSys8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   46
      Tag             =   "D-SYSTEMNORMAL8"
      Top             =   6240
      Width           =   3495
   End
   Begin VB.TextBox txtSys7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   45
      Tag             =   "D-SYSTEMNORMAL7"
      Top             =   5880
      Width           =   3495
   End
   Begin VB.TextBox txtSys6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   44
      Tag             =   "D-SYSTEMNORMAL6"
      Top             =   5520
      Width           =   3495
   End
   Begin VB.TextBox txtSys5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   43
      Tag             =   "D-SYSTEMNORMAL5"
      Top             =   5160
      Width           =   3495
   End
   Begin VB.TextBox txtSys4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   42
      Tag             =   "D-SYSTEMNORMAL4"
      Top             =   4800
      Width           =   3495
   End
   Begin VB.TextBox txtSys3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   41
      Tag             =   "D-SYSTEMNORMAL3"
      Top             =   4440
      Width           =   3495
   End
   Begin VB.TextBox txtSys2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   40
      Tag             =   "D-SYSTEMNORMAL2"
      Top             =   4080
      Width           =   3495
   End
   Begin VB.TextBox txtSys1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      MaxLength       =   32
      TabIndex        =   39
      Tag             =   "D-SYSTEMNORMAL1"
      Top             =   3720
      Width           =   3495
   End
   Begin VB.TextBox txtTestShort7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   24
      Tag             =   "S-TESTSHORT7"
      Top             =   5760
      Width           =   1575
   End
   Begin VB.TextBox txtTestShort6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   22
      Tag             =   "S-TESTSHORT6"
      Top             =   5400
      Width           =   1575
   End
   Begin VB.TextBox txtTestShort5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   20
      Tag             =   "S-TESTSHORT5"
      Top             =   5040
      Width           =   1575
   End
   Begin VB.TextBox txtTestShort4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   35
      Tag             =   "S-TESTSHORT4"
      Top             =   4680
      Width           =   1575
   End
   Begin VB.TextBox txtTestShort3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   16
      Tag             =   "S-TESTSHORT3"
      Top             =   4320
      Width           =   1575
   End
   Begin VB.TextBox txtTestShort2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   14
      Tag             =   "S-TESTSHORT2"
      Top             =   3960
      Width           =   1575
   End
   Begin VB.TextBox txtTestShort1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      MaxLength       =   16
      TabIndex        =   12
      Tag             =   "S-TESTSHORT1"
      Top             =   3600
      Width           =   1575
   End
   Begin VB.TextBox txtTestOrder7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   23
      Tag             =   "S-TESTFILE7"
      Top             =   5760
      Width           =   855
   End
   Begin VB.TextBox txtTestOrder6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   21
      Tag             =   "S-TESTFILE6"
      Top             =   5400
      Width           =   855
   End
   Begin VB.TextBox txtTestOrder5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   19
      Tag             =   "S-TESTFILE5"
      Top             =   5040
      Width           =   855
   End
   Begin VB.TextBox txtTestOrder4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   17
      Tag             =   "S-TESTFILE4"
      Top             =   4680
      Width           =   855
   End
   Begin VB.TextBox txtTestOrder3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   15
      Tag             =   "S-TESTFILE3"
      Top             =   4320
      Width           =   855
   End
   Begin VB.TextBox txtTestOrder2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   13
      Tag             =   "S-TESTFILE2"
      Top             =   3960
      Width           =   855
   End
   Begin VB.TextBox txtTestOrder1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   4
      TabIndex        =   11
      Tag             =   "S-TESTFILE1"
      Top             =   3600
      Width           =   855
   End
   Begin VB.TextBox txtPorts 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9000
      MaxLength       =   5
      TabIndex        =   8
      Tag             =   "S-STMTRANGE"
      Top             =   480
      Width           =   735
   End
   Begin VB.CheckBox chkInternal 
      BackColor       =   &H00808000&
      Caption         =   "Allow Internal"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6360
      TabIndex        =   4
      Tag             =   "S-INTERNAL"
      Top             =   2760
      Width           =   1695
   End
   Begin VB.TextBox txtSMTP 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   11040
      MaxLength       =   2
      TabIndex        =   10
      Tag             =   "S-SMTPADDRESS"
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox txtSubmit 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10920
      MaxLength       =   4
      TabIndex        =   9
      Tag             =   "S-SUBMITID"
      Top             =   1560
      Width           =   855
   End
   Begin VB.TextBox txtReviewWindow 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      MaxLength       =   3
      TabIndex        =   1
      Tag             =   "S-REVIEWWINDOW"
      Top             =   1200
      Width           =   855
   End
   Begin VB.CheckBox chkDIPrint 
      BackColor       =   &H00808000&
      Caption         =   "Print Chart"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   26
      Tag             =   "D-PRINT"
      Top             =   4560
      Width           =   1335
   End
   Begin VB.CheckBox chkPrint 
      BackColor       =   &H00808000&
      Caption         =   "Allow Printing"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   29
      Tag             =   "P-PRINT"
      Top             =   7560
      Visible         =   0   'False
      Width           =   1695
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   32
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupConfig.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10200
      TabIndex        =   33
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupConfig.frx":01DF
   End
   Begin VB.Label Label30 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Auto Log Off"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4560
      TabIndex        =   143
      Top             =   7440
      Width           =   1080
   End
   Begin VB.Label Label31 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Glasses Cpys"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   720
      TabIndex        =   130
      Top             =   2160
      Width           =   1245
   End
   Begin VB.Label Label29 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Prt 5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   7320
      TabIndex        =   121
      Top             =   8400
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label Label28 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Stmt Limit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   8160
      TabIndex        =   116
      Top             =   1560
      Width           =   930
   End
   Begin VB.Label Label26 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Slot Cnt"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   2040
      TabIndex        =   94
      Top             =   2160
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label Label25 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Slots"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   120
      TabIndex        =   92
      Top             =   2160
      Visible         =   0   'False
      Width           =   360
   End
   Begin VB.Label Label24 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Slug "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1680
      TabIndex        =   90
      Top             =   1560
      Visible         =   0   'False
      Width           =   360
   End
   Begin VB.Label Label23 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Rotate Rate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   1680
      TabIndex        =   83
      Top             =   6120
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.Label Label22 
      BackColor       =   &H00808000&
      Caption         =   "Other DI Print File"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   7800
      TabIndex        =   79
      Top             =   7680
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label21 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Prt 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   1800
      TabIndex        =   77
      Top             =   8400
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label Label20 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Prt 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   1800
      TabIndex        =   76
      Top             =   7920
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Prt 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   4560
      TabIndex        =   75
      Top             =   7920
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Prt 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   4560
      TabIndex        =   74
      Top             =   8400
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Copies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   120
      TabIndex        =   68
      Top             =   6120
      Width           =   660
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Proc Link"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   840
      TabIndex        =   63
      Top             =   6120
      Width           =   870
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   120
      X2              =   11880
      Y1              =   3240
      Y2              =   3240
   End
   Begin VB.Label lblCalDate 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Sch Start"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   120
      TabIndex        =   62
      Top             =   1560
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Default Tx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1080
      TabIndex        =   61
      Top             =   360
      Width           =   735
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "System Normal 10-18"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6480
      TabIndex        =   58
      Top             =   3480
      Width           =   1890
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "System Normal 1-9"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   2880
      TabIndex        =   57
      Top             =   3480
      Width           =   1680
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Stmt Range"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   7800
      TabIndex        =   38
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "MC/MD Rel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   9840
      TabIndex        =   37
      Top             =   480
      Width           =   1140
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Submit Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   9960
      TabIndex        =   36
      Top             =   1560
      Width           =   870
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Review"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   120
      TabIndex        =   34
      Top             =   960
      Width           =   555
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Doctor Setup"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   31
      Top             =   3360
      Width           =   1230
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Patient Setup"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   30
      Top             =   7200
      Width           =   1650
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Scheduler Setup"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Width           =   1590
   End
End
Attribute VB_Name = "frmSetupConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdApply_Click()
Dim i As Long
Dim TestName(7) As String
Dim RetCon As New PracticeConfigureInterface
RetCon.ConfigureInterfaceField = ""
If (RetCon.FindConfigure > 0) Then
    i = 1
    MyPracticeRepository.BeginCachingScope
    On Error GoTo Finally
    MyPracticeRepository.BeginBatch
    While (RetCon.SelectConfigure(i))
        If (Trim(RetCon.ConfigureInterfaceField) = "MUCHECKIN") Then
            If (chkMUChkIn.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTOLOADDISPLAY") Then
            If (chkDLoad.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INSBATCHNEW") Then
            If (chkInsBat.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "EXPORTAGINGON") Then
            If (chkAging.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "COLLECTIONOPTICALON") Then
            If (chkCollOpt.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NPIREQUIRED") Then
            If (chkNpi.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWEXPORT") Then
            If (chkNExp.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIREDATCHECKIN") Then
            If (chkCheckIn.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "GLASSESFORM2") Then
            If (chkgform.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ZIPLOOKUPON") Then
            If (chkZips.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "EXPORTRECALLDOCLOCON") Then
            If (chkExport.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "OPTICALMEDICALON") Then
            If (chkOptMed.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "UB92ON") Then
            If (chkUb.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RPTPATPAY") Then
            If (chkPatPay.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NQTESTON") Then
            If (chkNQTest.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "BUSLOGON") Then
            If (chkBBL.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SIGNON") Then
            If (chkSign.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIREREFERRALTOBILL") Then
            If (chkReqRef.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INVENTORYON") Then
            If (chkPC.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "GLASSON") Then
            If (chkGlass.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIREBILLDR") Then
            If (chkBillDr.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWCALENDAR") Then
            If (chkCal.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ALLCHECK") Then
            If (chkAllCheck.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTOREFDR") Then
            If (chkRef1.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DEBUGHIPAA") Then
            If (chkHipaa.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTORECALL") Then
            If (chkRecall.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "STATEMENTLOWERLIMIT") Then
            RetCon.ConfigureInterfaceValue = Trim(txtLimit.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "GLASSESCOPIES") Then
            RetCon.ConfigureInterfaceValue = Trim(txtGCopies.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ROTATERATE") Then
            RetCon.ConfigureInterfaceValue = Trim(txtRRate.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REVIEWWINDOW") Then
            RetCon.ConfigureInterfaceValue = Trim(txtReviewWindow.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SLUG") Then
            If (val(Trim(txtSlug.Text)) < 5) Then
                txtSlug.Text = "5"
            ElseIf (val(Trim(txtSlug.Text)) > 60) Then
                txtSlug.Text = "60"
            End If
            RetCon.ConfigureInterfaceValue = Trim(txtSlug.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SLOTSELECTIONS") Then
            If (val(Trim(txtSlotSelects.Text)) > 10) Then
                txtSlotSelects.Text = "10"
            ElseIf (val(Trim(txtSlotSelects.Text)) < 1) Then
                txtSlotSelects.Text = "1"
            End If
            RetCon.ConfigureInterfaceValue = Trim(txtSlotSelects.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SLOTDAYS") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSlotDays.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SUBMITID") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSubmit.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "USETODAYONLY") Then
            If (chkSiToday.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INTERNAL") Then
            If (chkInternal.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DIRECTQUEUE") Then
            If (chkDirect.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "QUERYBATCH") Then
            If (chkQuery.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIRECHECK") Then
            If (chkCheckNumber.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "CALENDARDATE") Then
            RetCon.ConfigureInterfaceValue = Trim(txtCalDate.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "STATEMENTRANGE") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPorts.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RELEASEMONTH") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSMTP.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TXTYPE") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTrans.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUDIT") Then
            If (chkAudit.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINT") Then
            If (chkDIPrint.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DXORDERIMPRESSIONS") Then
            If (chkDxOdrImp.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DRAWLINEON") Then
            If (chkLineD.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTDEVICEON") Then
            If (chkTD.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "IMAGEORDER") Then
            If (chkIOrder.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DORV") Then
            If (chkUseRv.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DELAYHIGHLIGHTS") Then
            If (chkNoHighs.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWQUANTIFIERS") Then
            If (chkNewQ.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RESETON") Then
            If (chkRset.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
                'ElseIf (Trim(RetCon.ConfigureInterfaceField) = "BACKUPON") Then
                '    If (chkBackup.Value = 1) Then
                '        RetCon.ConfigureInterfaceValue = "T"
                '    Else
                '        RetCon.ConfigureInterfaceValue = "F"
                '    End If
                '    Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTWRITTEN") Then
            If (chkWI.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "USEJPG") Then
            If (chkJpg.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TASKSENABLED") Then
            If (chkTaskManager.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DEFPREVRX") Then
            If (chkMyRx.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "VIEWNAMES") Then
            If (chkNames.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RVON") Then
            If (chkRV.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIRECC") Then
            If (chkCC.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "CHECKOUTLOG") Then
            If (chkDILog.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INSERTTAB") Then
            If (chkTab.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "USETODAYONLY") Then
            If (chkDiToday.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTDIRX") Then
            If (chkRx.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ERX") Then
            If (chkERX.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTDIWEAR") Then
            If (chkRx1.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SETIMPRESSIONS") Then
            If (chkSetImpr.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTDRAWIMAGE") Then
            If (chkDraw.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MUON") Then
            If (chkMUON.Value = 1) Then
                RetCon.ConfigureInterfaceValue = "T"
            Else
                RetCon.ConfigureInterfaceValue = "F"
            End If
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTOLOGOFF") Then
            RetCon.ConfigureInterfaceValue = Trim(txtAutoLogOff.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "OTHERDI") Then
            RetCon.ConfigureInterfaceValue = Trim(txtOtherDI.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER1") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPrinter1.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER2") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPrinter2.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER3") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPrinter3.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER4") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPrinter4.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER6") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPrinter6.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE1") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder1.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder1.Text), TestName(1))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE2") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder2.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder2.Text), TestName(2))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE3") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder3.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder3.Text), TestName(3))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE4") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder4.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder4.Text), TestName(4))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE5") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder5.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder5.Text), TestName(5))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE6") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder6.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder6.Text), TestName(6))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE7") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestOrder7.Text)
            Call RetCon.ApplyConfigure
            Call SetTestQuestion(Trim(txtTestOrder7.Text), TestName(7))
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG1") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(1))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG2") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(2))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG3") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(3))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG4") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(4))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG5") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(5))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG6") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(6))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTLONG7") Then
            RetCon.ConfigureInterfaceValue = Trim(TestName(7))
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT1") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort1.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT2") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort2.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT3") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort3.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT4") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort4.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT5") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort5.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT6") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort6.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT7") Then
            RetCon.ConfigureInterfaceValue = Trim(txtTestShort7.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL1") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys1.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL2") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys2.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL3") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys3.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL4") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys4.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL5") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys5.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL6") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys6.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL7") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys7.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL8") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys8.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL9") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys9.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL10") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys10.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL11") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys11.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL12") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys12.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL13") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys13.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL14") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys14.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL15") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys15.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL16") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys16.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL17") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys17.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL18") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys18.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL98") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys98.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL99") Then
            RetCon.ConfigureInterfaceValue = Trim(txtSys99.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PROCLINKTYPE") Then
            RetCon.ConfigureInterfaceValue = Trim(txtPLink.Text)
            Call RetCon.ApplyConfigure
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "COPIES") Then
            RetCon.ConfigureInterfaceValue = Trim(txtDICopies.Text)
            Call RetCon.ApplyConfigure
        End If
        i = i + 1
    Wend
    MyPracticeRepository.ExecuteBatch
    
    GoSub FinallyOnSuccess
End If
Set RetCon = Nothing
LoadConfigCollection
SetGlobalDefaults
Unload frmSetupConfig
Exit Sub

FinallyOnSuccess:
    MyPracticeRepository.CancelBatch
    MyPracticeRepository.EndCachingScope
    Return
    
Finally:
    MyPracticeRepository.CancelBatch
    MyPracticeRepository.EndCachingScope
    Set RetCon = Nothing
    Unload frmSetupConfig
End Sub

Private Sub cmdBack_Click()
Unload frmSetupConfig
End Sub

Public Function LoadConfigs() As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Long
Dim RetCon As New PracticeConfigureInterface
RetCon.ConfigureInterfaceField = ""
If (RetCon.FindConfigure > 0) Then
    i = 1
    While (RetCon.SelectConfigure(i))
        If (Trim(RetCon.ConfigureInterfaceField) = "MUCHECKIN") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkMUChkIn.Value = 1
            Else
                chkMUChkIn.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWDOCTORSCHEDULE") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                'chkNSch.Value = 1
            Else
                'chkNSch.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INSBATCHNEW") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkInsBat.Value = 1
            Else
                chkInsBat.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "EXPORTAGINGON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkAging.Value = 1
            Else
                chkAging.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "COLLECTIONOPTICALON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkCollOpt.Value = 1
            Else
                chkCollOpt.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NPIREQUIRED") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkNpi.Value = 1
            Else
                chkNpi.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIREDATCHECKIN") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkCheckIn.Value = 1
            Else
                chkCheckIn.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "GLASSESFORM2") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkgform.Value = 1
            Else
                chkgform.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "OPTICALMEDICALON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkOptMed.Value = 1
            Else
                chkOptMed.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ZIPLOOKUPON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkZips.Value = 1
            Else
                chkZips.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "UB92ON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkUb.Value = 1
            Else
                chkUb.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTOLOADDISPLAY") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDLoad.Value = 1
            Else
                chkDLoad.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "EXPORTRECALLDOCLOCON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkExport.Value = 1
            Else
                chkExport.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RPTPATPAY") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkPatPay.Value = 1
            Else
                chkPatPay.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NQTESTON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkNQTest.Value = 1
            Else
                chkNQTest.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "BUSLOGON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkBBL.Value = 1
            Else
                chkBBL.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INVENTORYON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkPC.Value = 1
            Else
                chkPC.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "GLASSON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkGlass.Value = 1
            Else
                chkGlass.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SIGNON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkSign.Value = 1
            Else
                chkSign.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIREREFERRALTOBILL") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkReqRef.Value = 1
            Else
                chkReqRef.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIREBILLDR") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkBillDr.Value = 1
            Else
                chkBillDr.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWCALENDAR") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkCal.Value = 1
            Else
                chkCal.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DEBUGHIPAA") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkHipaa.Value = 1
            Else
                chkHipaa.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTOREFDR") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkRef1.Value = 1
            Else
                chkRef1.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ALLCHECK") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkAllCheck.Value = 1
            Else
                chkAllCheck.Value = 0
            End If
'        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DOCTAXID") Then
'            If (RetCon.ConfigureInterfaceValue = "T") Then
'                chkTaxId.Value = 1
'            Else
'                chkTaxId.Value = 0
'            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTORECALL") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkRecall.Value = 1
            Else
                chkRecall.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "GLASSESCOPIES") Then
            txtGCopies.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "STATEMENTLOWERLIMIT") Then
            txtLimit.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ROTATERATE") Then
            txtRRate.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REVIEWWINDOW") Then
            txtReviewWindow.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SLUG") Then
            txtSlug.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SLOTSELECTIONS") Then
            txtSlotSelects.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SLOTDAYS") Then
            txtSlotDays.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SUBMITID") Then
            txtSubmit.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INTERNAL") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkInternal.Value = 1
            Else
                chkInternal.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "CALENDARDATE") Then
            txtCalDate.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "STATEMENTRANGE") Then
            txtPorts.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RELEASEMONTH") Then
            txtSMTP.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TXTYPE") Then
            txtTrans.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "USETODAYONLY") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkSiToday.Value = 1
            Else
                chkSiToday.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUDIT") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkAudit.Value = 1
            Else
                chkAudit.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIRECHECK") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkCheckNumber.Value = 1
            Else
                chkCheckNumber.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DIRECTQUEUE") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDirect.Value = 1
            Else
                chkDirect.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "QUERYBATCH") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkQuery.Value = 1
            Else
                chkQuery.Value = 0
            End If
        End If
        If (Trim(RetCon.ConfigureInterfaceField) = "PRINT") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDIPrint.Value = 1
            Else
                chkDIPrint.Value = 0
            End If
'        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWEYEWEAR") Then
'            If (RetCon.ConfigureInterfaceValue = "T") Then
'                chkNewWear.Value = 1
'            Else
'                chkNewWear.Value = 0
'            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "MUON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkMUON.Value = 1
            Else
                chkMUON.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DXORDERIMPRESSIONS") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDxOdrImp.Value = 1
            Else
                chkDxOdrImp.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DRAWLINEON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkLineD.Value = 1
            Else
                chkLineD.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "IMAGEORDER") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkIOrder.Value = 1
            Else
                chkIOrder.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DORV") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkUseRv.Value = 1
            Else
                chkUseRv.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWQUANTIFIERS") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkNewQ.Value = 1
            Else
                chkNewQ.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTDEVICEON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkTD.Value = 1
            Else
                chkTD.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DELAYHIGHLIGHTS") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkNoHighs.Value = 1
            Else
                chkNoHighs.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RESETON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkRset.Value = 1
            Else
                chkRset.Value = 0
            End If
                'ElseIf (Trim(RetCon.ConfigureInterfaceField) = "BACKUPON") Then
                '    If (RetCon.ConfigureInterfaceValue = "T") Then
                '        chkBackup.Value = 1
                '    Else
                '        chkBackup.Value = 0
                '    End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "USEJPG") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkJpg.Value = 1
            Else
                chkJpg.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTWRITTEN") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkWI.Value = 1
            Else
                chkWI.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "DEFPREVRX") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkMyRx.Value = 1
            Else
                chkMyRx.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "VIEWNAMES") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkNames.Value = 1
            Else
                chkNames.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TASKSENABLED") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkTaskManager.Value = 1
            Else
                chkTaskManager.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "RVON") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkRV.Value = 1
            Else
                chkRV.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "REQUIRECC") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkCC.Value = 1
            Else
                chkCC.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "CHECKOUTLOG") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDILog.Value = 1
            Else
                chkDILog.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTDIRX") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkRx.Value = 1
            Else
                chkRx.Value = 0
            End If
        
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "ERX") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkERX.Value = 1
            Else
                chkERX.Value = 0
            End If
        
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTDIWEAR") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkRx1.Value = 1
            Else
                chkRx1.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "INSERTTAB") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkTab.Value = 1
            Else
                chkTab.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTDRAWIMAGE") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDraw.Value = 1
            Else
                chkDraw.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "USETODAYONLY") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkDiToday.Value = 1
            Else
                chkDiToday.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SETIMPRESSIONS") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkSetImpr.Value = 1
            Else
                chkSetImpr.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "NEWEXPORT") Then
            If (RetCon.ConfigureInterfaceValue = "T") Then
                chkNExp.Value = 1
            Else
                chkNExp.Value = 0
            End If
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "OTHERDI") Then
            txtOtherDI.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER1") Then
            txtPrinter1.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER2") Then
            txtPrinter2.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER3") Then
            txtPrinter3.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER4") Then
            txtPrinter4.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PRINTER6") Then
            txtPrinter6.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE1") Then
            txtTestOrder1.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE2") Then
            txtTestOrder2.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE3") Then
            txtTestOrder3.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE4") Then
            txtTestOrder4.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE5") Then
            txtTestOrder5.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE6") Then
            txtTestOrder6.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTFILE7") Then
            txtTestOrder7.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT1") Then
            txtTestShort1.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT2") Then
            txtTestShort2.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT3") Then
            txtTestShort3.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT4") Then
            txtTestShort4.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT5") Then
            txtTestShort5.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT6") Then
            txtTestShort6.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "TESTSHORT7") Then
            txtTestShort7.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL1") Then
            txtSys1.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL2") Then
            txtSys2.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL3") Then
            txtSys3.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL4") Then
            txtSys4.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL5") Then
            txtSys5.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL6") Then
            txtSys6.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL7") Then
            txtSys7.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL8") Then
            txtSys8.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL9") Then
            txtSys9.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL10") Then
            txtSys10.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL11") Then
            txtSys11.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL12") Then
            txtSys12.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL13") Then
            txtSys13.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL14") Then
            txtSys14.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL15") Then
            txtSys15.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL16") Then
            txtSys16.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL17") Then
            txtSys17.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL18") Then
            txtSys18.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL98") Then
            txtSys98.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "SYSTEMNORMAL99") Then
            txtSys99.Text = Trim(RetCon.ConfigureInterfaceValue)
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "PROCLINKTYPE") Then
            txtPLink.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "COPIES") Then
            txtDICopies.Text = RetCon.ConfigureInterfaceValue
        ElseIf (Trim(RetCon.ConfigureInterfaceField) = "AUTOLOGOFF") Then
            txtAutoLogOff.Text = RetCon.ConfigureInterfaceValue
        End If
        i = i + 1
    Wend
End If
Set RetCon = Nothing
LoadConfigs = True
Exit Function
UI_ErrorHandler:
    Set RetCon = Nothing
End Function

Private Sub txtCalDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtCalDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtCalDate.Text = ""
        txtCalDate.SetFocus
        SendKeys "{Home}"
        KeyAscii = 0
    Else
        Call UpdateDisplay(txtCalDate, "D")
    End If
End If
End Sub

Private Sub txtCalDate_Validate(Cancel As Boolean)
Call txtCalDate_KeyPress(13)
End Sub

Private Sub txtPorts_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) Then
    MsgBox "Valid Values [0123456789]"
    KeyAscii = 0
    txtPorts.Text = ""
    txtPorts.SetFocus
End If
End Sub

Private Sub txtReviewWindow_KeyPress(KeyAscii As Integer)
If Not (IsNumeric(Chr(KeyAscii))) Then
    MsgBox "Valid Values [0123456789]"
    KeyAscii = 0
    txtReviewWindow.Text = ""
    txtReviewWindow.SetFocus
End If
End Sub

Private Sub txtTrans_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("TransmitType")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtTrans.Text = UCase(Left(frmSelectDialogue.Selection, 1))
End If
End Sub

Private Sub txtTrans_KeyPress(KeyAscii As Integer)
Call txtTrans_Click
KeyAscii = 0
End Sub

Private Function SetTestQuestion(TheOrder As String, TheName As String) As Boolean
Dim RetQues As DynamicClass
TheName = ""
SetTestQuestion = False
If (Trim(TheOrder) <> "") Then
    Set RetQues = New DynamicClass
    RetQues.QuestionOrder = Trim(TheOrder)
    RetQues.question = ""
    RetQues.QuestionParty = "T"
    RetQues.QuestionFamily = 0
    RetQues.QuestionSet = "First Visit"
    If (RetQues.FindClassForms > 0) Then
        If (RetQues.SelectClassForm(1)) Then
            TheName = Trim(RetQues.question)
            SetTestQuestion = True
        End If
    End If
    Set RetQues = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
