VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmAppointmentSetCancel 
   BackColor       =   &H0077742D&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Appointment"
   ClientHeight    =   8160
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7335
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   7335
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstCType 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2670
      ItemData        =   "AppointmentSetCancel.frx":0000
      Left            =   240
      List            =   "AppointmentSetCancel.frx":0002
      TabIndex        =   4
      Top             =   1560
      Width           =   1995
   End
   Begin VB.ListBox lstCReason 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5310
      ItemData        =   "AppointmentSetCancel.frx":0004
      Left            =   2280
      List            =   "AppointmentSetCancel.frx":0006
      TabIndex        =   2
      Top             =   1560
      Width           =   4875
   End
   Begin VB.TextBox txtNote 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      MaxLength       =   64
      TabIndex        =   1
      Top             =   720
      Width           =   6975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   5160
      TabIndex        =   0
      Top             =   7200
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AppointmentSetCancel.frx":0008
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   855
      Left            =   120
      TabIndex        =   8
      Top             =   7200
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AppointmentSetCancel.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAHst 
      Height          =   855
      Left            =   2640
      TabIndex        =   9
      Top             =   7200
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AppointmentSetCancel.frx":03D3
   End
   Begin VB.Label lblReason 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Reason"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   270
      Left            =   2280
      TabIndex        =   7
      Top             =   1200
      Width           =   810
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   270
      Left            =   240
      TabIndex        =   6
      Top             =   1200
      Width           =   525
   End
   Begin VB.Label lblAppt 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Date/Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   270
      Left            =   240
      TabIndex        =   5
      Top             =   120
      Width           =   2475
   End
   Begin VB.Label lblPreCert 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   510
   End
End
Attribute VB_Name = "frmAppointmentSetCancel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private AppointmentId As Long
Private PatientId As Long
Public respond As Integer
Public RsnNote As String


Public Function LoadForm(ApptId As Long, Optional Rsch As Boolean) As Boolean
Dim TDate As String
Dim PatName As String
Dim ApptTime As String
Dim RetPat As Patient
Dim RetAppt As SchedulerAppointment
LoadForm = False
If (ApptId > 0) Then
    AppointmentId = ApptId
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        PatientId = RetAppt.AppointmentPatientId
        Set RetPat = New Patient
        RetPat.PatientId = PatientId
        If (RetPat.RetrievePatient) Then
            PatName = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
        End If
        TDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
        Call ConvertMinutesToTime(RetAppt.AppointmentTime, ApptTime)
        lblAppt.Caption = "Date: " + TDate + " Time: " + ApptTime + " for " + PatName
        If Rsch Then
            lstCType.AddItem "Reschedule"
            lstCType.Selected(0) = True
        Else
            lstCType.AddItem "Left"
            lstCType.AddItem "No Show"
            lstCType.AddItem "Same Day"
            lstCType.AddItem "Patient"
            lstCType.AddItem "Office"
            lstCType.AddItem "Other"
            lstCType.AddItem "Special"
            lstCType.Tag = "FNSYOXQ"
        End If
        LoadForm = True
    End If
End If
End Function

Private Sub cmdAHst_Click()
FApptHistory.LoadHistory AppointmentId
FApptHistory.Show 1
End Sub

Private Sub cmdDone_Click()

If lstCType.List(0) = "Reschedule" And lstCType.ListCount = 1 Then
    respond = 1
    If Not lstCReason.ListIndex < 0 Then
        RsnNote = "[" & lstCReason.List(lstCReason.ListIndex) & "]"
    End If
    RsnNote = RsnNote & txtNote.Text
    Unload frmAppointmentSetCancel
    Exit Sub
End If


Dim p As Integer
Dim Sts As String
Dim ARef As String
Dim Proceed As Boolean
Dim ApplSch As ApplicationScheduler
Dim ApplList As ApplicationAIList
If (lstCType.ListIndex < 0) Then
    frmEventMsgs.Header = "Please Select a Cancellation Type."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    ARef = ""
    p = lstCType.ListIndex + 1
    Sts = Mid(lstCType.Tag, p, 1)
    If (lstCReason.ListIndex >= 0) Then
        ARef = "[" + Trim(lstCReason.List(lstCReason.ListIndex)) + "]"
    End If
    Set ApplSch = New ApplicationScheduler
    Call ApplSch.CancelAppointment(AppointmentId, Sts, ARef + txtNote.Text, UserLogin.iId, True)
    Set ApplSch = Nothing
    Set ApplList = New ApplicationAIList
    Call ApplList.DeleteSurgeryPlanAppointment(AppointmentId)
    Set ApplList = Nothing
    Unload frmAppointmentSetCancel
End If
End Sub

Private Sub cmdHome_Click()
respond = -1
Unload frmAppointmentSetCancel
End Sub

Private Sub lstCType_Click()
Dim u As Integer
Dim p As Integer
Dim AType As String
Dim RetCode As PracticeCodes
Dim ApplTbl As ApplicationTables
lstCReason.Clear
If (lstCType.ListIndex >= 0) Then
    p = lstCType.ListIndex + 1
    If lstCType.List(0) = "Reschedule" And lstCType.ListCount = 1 Then
        AType = "R"
    Else
        AType = Mid(lstCType.Tag, p, 1)
    End If
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "Appt Cancel Reasons"
    RetCode.ReferenceAlternateCode = AType
    If (RetCode.FindAlternateCode > 0) Then
        u = 1
        While (RetCode.SelectCode(u))
            lstCReason.AddItem Trim(RetCode.ReferenceCode)
            u = u + 1
        Wend
    End If
    Set RetCode = Nothing
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
