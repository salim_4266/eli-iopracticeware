VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmNewInsured 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox EmpPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   16
      TabIndex        =   52
      Top             =   3600
      Width           =   2055
   End
   Begin VB.CheckBox chkSigP1 
      BackColor       =   &H0077742D&
      Caption         =   "Signature on file"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3600
      TabIndex        =   49
      Top             =   5040
      Width           =   2055
   End
   Begin VB.CheckBox chkAsgP1 
      BackColor       =   &H0077742D&
      Caption         =   "Assignment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5880
      TabIndex        =   48
      Top             =   5040
      Width           =   1575
   End
   Begin VB.TextBox SigDate 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   10
      TabIndex        =   47
      Top             =   5040
      Width           =   1575
   End
   Begin VB.TextBox txtSuite 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7320
      MaxLength       =   8
      TabIndex        =   7
      Top             =   1200
      Width           =   1815
   End
   Begin VB.TextBox Marital 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   1
      TabIndex        =   11
      Top             =   1200
      Width           =   495
   End
   Begin VB.TextBox FirstName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   14
      TabIndex        =   0
      Top             =   720
      Width           =   1935
   End
   Begin VB.TextBox Address 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   6
      Top             =   1200
      Width           =   4575
   End
   Begin VB.TextBox City 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   8
      Top             =   1680
      Width           =   4575
   End
   Begin VB.TextBox State 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7320
      MaxLength       =   2
      TabIndex        =   9
      Top             =   1680
      Width           =   495
   End
   Begin VB.TextBox Zip 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8640
      MaxLength       =   10
      TabIndex        =   10
      Top             =   1680
      Width           =   1575
   End
   Begin VB.TextBox HomePhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   16
      TabIndex        =   12
      Top             =   2175
      Width           =   2055
   End
   Begin VB.TextBox WorkPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   16
      TabIndex        =   13
      Top             =   2640
      Width           =   2055
   End
   Begin VB.TextBox Gender 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   1
      TabIndex        =   4
      Top             =   720
      Width           =   375
   End
   Begin VB.TextBox BirthDate 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   5
      Top             =   720
      Width           =   1455
   End
   Begin VB.TextBox SSN 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6240
      MaxLength       =   11
      TabIndex        =   15
      Top             =   2160
      Width           =   2175
   End
   Begin VB.TextBox Employer 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   17
      Top             =   3615
      Width           =   4695
   End
   Begin VB.TextBox EmployerAddress 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   18
      Top             =   4080
      Width           =   4695
   End
   Begin VB.TextBox EmployerCity 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   19
      Top             =   4560
      Width           =   4695
   End
   Begin VB.TextBox EmployerState 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7560
      MaxLength       =   2
      TabIndex        =   20
      Top             =   4560
      Width           =   495
   End
   Begin VB.TextBox EmployerZip 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8880
      MaxLength       =   10
      TabIndex        =   21
      Top             =   4560
      Width           =   1575
   End
   Begin VB.TextBox Occupation 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   16
      TabIndex        =   16
      Top             =   3120
      Width           =   4095
   End
   Begin VB.TextBox MiddleInitial 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3840
      MaxLength       =   1
      TabIndex        =   1
      Top             =   720
      Width           =   375
   End
   Begin VB.TextBox LastName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4320
      MaxLength       =   16
      TabIndex        =   2
      Top             =   720
      Width           =   2775
   End
   Begin VB.TextBox NameRef 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7200
      MaxLength       =   4
      TabIndex        =   3
      Top             =   720
      Width           =   495
   End
   Begin VB.TextBox CellPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6240
      MaxLength       =   16
      TabIndex        =   14
      Top             =   2640
      Width           =   2055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10320
      TabIndex        =   22
      Top             =   6960
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NewInsured.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   44
      Top             =   6960
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NewInsured.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAssign 
      Height          =   990
      Left            =   7440
      TabIndex        =   45
      Top             =   6960
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NewInsured.frx":03BE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdInsured 
      Height          =   990
      Left            =   2640
      TabIndex        =   46
      Top             =   6960
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NewInsured.frx":05AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAddr 
      Height          =   990
      Left            =   5040
      TabIndex        =   51
      Top             =   6960
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NewInsured.frx":079D
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Employer Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6720
      TabIndex        =   53
      Top             =   3600
      Width           =   1920
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Signature Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   50
      Top             =   5040
      Width           =   1680
   End
   Begin VB.Label Label12 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   43
      Top             =   1680
      Width           =   540
   End
   Begin VB.Label Label32 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4320
      TabIndex        =   42
      Top             =   240
      Width           =   1260
   End
   Begin VB.Label Label31 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1800
      TabIndex        =   41
      Top             =   240
      Width           =   1260
   End
   Begin VB.Label Label13 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   40
      Top             =   4560
      Width           =   585
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6600
      TabIndex        =   39
      Top             =   4560
      Width           =   900
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   8160
      TabIndex        =   38
      Top             =   4560
      Width           =   660
   End
   Begin VB.Label Label19 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6480
      TabIndex        =   37
      Top             =   1680
      Width           =   780
   End
   Begin VB.Label Label21 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   7920
      TabIndex        =   36
      Top             =   1680
      Width           =   660
   End
   Begin VB.Label Label26 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Suite"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6480
      TabIndex        =   35
      Top             =   1200
      Width           =   780
   End
   Begin VB.Label Label29 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Marital"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9240
      TabIndex        =   34
      Top             =   1200
      Width           =   990
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insured"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   33
      Top             =   720
      Width           =   1140
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   32
      Top             =   1200
      Width           =   1140
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Home Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   31
      Top             =   2175
      Width           =   1560
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Work Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   30
      Top             =   2640
      Width           =   1560
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Gender"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   7800
      TabIndex        =   29
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Birth Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   9240
      TabIndex        =   28
      Top             =   720
      Width           =   1185
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Soc Security"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4320
      TabIndex        =   27
      Top             =   2160
      Width           =   1740
   End
   Begin VB.Label Label14 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Employer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   26
      Top             =   3600
      Width           =   1200
   End
   Begin VB.Label Label15 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   25
      Top             =   4080
      Width           =   1140
   End
   Begin VB.Label Label16 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Occupation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   24
      Top             =   3120
      Width           =   1380
   End
   Begin VB.Label Label17 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Cell Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4320
      TabIndex        =   23
      Top             =   2640
      Width           =   1560
   End
End
Attribute VB_Name = "frmNewInsured"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private miPolicyPatientId As Long
Private miPatientId As Long
Private WhichPath As Boolean
Private OriginalPolicyHolderId As Long
Private OriginalPolicyHolderIdRel As String
Private OriginalPolicyHolderIdBill As Boolean
Private PolicyHolderid As Long
Private PolicyHolderIdRel As String
Private PolicyHolderIdBill As Boolean
Private OriginalSecondPolicyHolderId As Long
Private OriginalSecondPolicyHolderIdRel As String
Private OriginalSecondPolicyHolderIdBill As Boolean
Private SecondPolicyHolderId As Long
Private SecondPolicyHolderIdRel As String
Private SecondPolicyHolderIdBill As Boolean
Private PolicyHolderExists As Boolean

Public Property Let PatientId(ByVal lPatientId As Long)
    miPatientId = lPatientId
End Property

Public Property Get PolicyPatientId() As Long
    PolicyPatientId = miPolicyPatientId
End Property

Private Sub chkSigP1_Click()
Dim TheDate As String
If (Trim(SigDate.Text) = "") Then
    TheDate = ""
    Call FormatTodaysDate(TheDate, False)
    SigDate.Text = TheDate
End If
End Sub

Private Sub cmdAssign_Click()
Dim PatId As Long
Dim ReturnArguments As Variant
If (miPolicyPatientId < 1) Then
    If Not (ValidateRequiredFields) Then
        Exit Sub
    End If
    If Not (ApplyChanges("I")) Then
        Exit Sub
    End If
End If
If (miPolicyPatientId > 0) Then
    PatId = 0
    Set ReturnArguments = DisplayPatientSearchScreen(False)
    If Not (ReturnArguments Is Nothing) Then PatId = ReturnArguments.PatientId
    If (PatId > 0) Then
' if Patient already has any insurers then set to open location
        If Not (SetPolicyHolder(PatId, miPolicyPatientId)) Then
            frmEventMsgs.Header = "Patient already has an Insuring Party"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End If
End Sub

Private Sub cmdAddr_Click()
Dim RetPat As Patient
If (miPatientId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = miPatientId
    If (RetPat.RetrievePatient) Then
        Address.Text = Trim(RetPat.Address)
        txtSuite.Text = Trim(RetPat.Suite)
        City.Text = Trim(RetPat.City)
        State.Text = Trim(RetPat.State)
        Zip.Text = Trim(RetPat.Zip)
        HomePhone.Text = Trim(RetPat.HomePhone)
    End If
    Set RetPat = Nothing
ElseIf (miPolicyPatientId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = miPolicyPatientId
    If (RetPat.RetrievePatient) Then
        Address.Text = Trim(RetPat.Address)
        txtSuite.Text = Trim(RetPat.Suite)
        City.Text = Trim(RetPat.City)
        State.Text = Trim(RetPat.State)
        Zip.Text = Trim(RetPat.Zip)
        HomePhone.Text = Trim(RetPat.HomePhone)
    End If
    Set RetPat = Nothing
Else
    frmEventMsgs.Header = "Must Assign a Patient"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdHome_Click()
Unload frmNewInsured
End Sub

Private Sub cmdDone_Click()
If (ValidateRequiredFields) Then
    If (ApplyChanges("I")) Then
        Unload frmNewInsured
    End If
End If
End Sub

Private Sub cmdInsured_Click()
Dim RetPat As Patient
Dim IsDisplayed As Boolean
If (miPolicyPatientId < 1) Then
    If Not (ValidateRequiredFields) Then
        Exit Sub
    End If
    If Not (ApplyChanges("I")) Then
        Exit Sub
    End If
End If
If (miPolicyPatientId > 0) Then
    IsDisplayed = DisplayInsuranceScreen(miPolicyPatientId)
    If IsDisplayed Then
        Call PatientLoadDisplay(miPolicyPatientId, WhichPath, "")
    End If
Else
    frmEventMsgs.Header = "Please Apply New Insuring Party"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub EmployerZip_KeyPress(KeyAscii As Integer)
Dim ACity As String
Dim AState As String
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(EmployerZip, "Z")
    If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
        Call GetCityStatebyZip(EmployerZip.Text, ACity, AState)
        If (Trim(ACity) <> "") Then
            EmployerCity.Text = ACity
        End If
        If (Trim(AState) <> "") Then
            EmployerState.Text = AState
        End If
    End If
End If
End Sub

Private Sub EmployerZip_Validate(Cancel As Boolean)
Dim ACity As String
Dim AState As String
Call UpdateDisplay(EmployerZip, "Z")
If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
    Call GetCityStatebyZip(EmployerZip.Text, ACity, AState)
    If (Trim(ACity) <> "") Then
        EmployerCity.Text = ACity
    End If
    If (Trim(AState) <> "") Then
        EmployerState.Text = AState
    End If
End If
End Sub

Private Sub Gender_Change()
If (Gender.Text <> "") Then
    If (UCase(Gender.Text) <> "M") And (UCase(Gender.Text) <> "F") Then
        frmEventMsgs.Header = "Must be [M]ale or [F]emale."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Gender.Text = ""
        Gender.SetFocus
    Else
        Gender.Text = UCase(Gender.Text)
    End If
End If
End Sub

Private Sub Gender_Click()
Call Gender_Change
End Sub

Private Sub Gender_KeyPress(KeyAscii As Integer)
Call Gender_Change
End Sub

Private Sub NameRef_Click()
Call frmSelectDialogue.BuildSelectionDialogue("NameReference")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    NameRef.Text = ""
    NameRef.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub NameRef_KeyPress(KeyAscii As Integer)
Call NameRef_Click
KeyAscii = 0
End Sub

Private Sub Marital_Click()
Call frmSelectDialogue.BuildSelectionDialogue("Marital")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Marital.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub Marital_KeyPress(KeyAscii As Integer)
Call Marital_Click
KeyAscii = 0
End Sub

Private Sub CellPhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(CellPhone, "P")
End If
End Sub

Private Sub CellPhone_Validate(Cancel As Boolean)
    Call UpdateDisplay(CellPhone, "P")
End Sub

Private Sub EmpPhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(EmpPhone, "P")
End If
End Sub

Private Sub EmpPhone_Validate(Cancel As Boolean)
    Call UpdateDisplay(EmpPhone, "P")
End Sub

Private Sub HomePhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(HomePhone, "P")
End If
End Sub

Private Sub HomePhone_Validate(Cancel As Boolean)
    Call UpdateDisplay(HomePhone, "P")
End Sub

Private Sub WorkPhone_Keypress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(WorkPhone, "P")
End If
End Sub

Private Sub WorkPhone_Validate(Cancel As Boolean)
    Call UpdateDisplay(WorkPhone, "P")
End Sub

Private Sub SSN_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(SSN, "S")
End If
End Sub

Private Sub SSN_Validate(Cancel As Boolean)
    Call UpdateDisplay(SSN, "S")
End Sub

Private Sub Zip_KeyPress(KeyAscii As Integer)
Dim ACity As String
Dim AState As String
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Zip, "Z")
    If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
        Call GetCityStatebyZip(Zip.Text, ACity, AState)
        If (Trim(ACity) <> "") Then
            City.Text = ACity
        End If
        If (Trim(AState) <> "") Then
            State.Text = AState
        End If
    End If
End If
End Sub

Private Sub Zip_Validate(Cancel As Boolean)
Dim ACity As String
Dim AState As String
Call UpdateDisplay(Zip, "Z")
If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
    Call GetCityStatebyZip(Zip.Text, ACity, AState)
    If (Trim(ACity) <> "") Then
        City.Text = ACity
    End If
    If (Trim(AState) <> "") Then
        State.Text = AState
    End If
End If
End Sub

Private Sub BirthDate_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (OkDate(BirthDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        BirthDate.SetFocus
        BirthDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(BirthDate, "D")
        If (GetAge(BirthDate.Text) < 0) Then
            frmEventMsgs.Header = "Future Dates not allowed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            BirthDate.Text = ""
            BirthDate.SetFocus
            SendKeys "{Home}"
            KeyAscii = 0
        ElseIf (GetAge(BirthDate.Text) > 125) Then
            frmEventMsgs.Header = "Birth Dates over 125 years not allowed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Accept"
            frmEventMsgs.CancelText = "Correct"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result <> 2) Then
                BirthDate.Text = ""
                BirthDate.SetFocus
                SendKeys "{Home}"
                KeyAscii = 0
            End If
        End If
    End If
End If
End Sub

Private Sub BirthDate_Validate(Cancel As Boolean)
If Not (OkDate(BirthDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    BirthDate.Text = ""
    BirthDate.SetFocus
    SendKeys "{Home}"
Else
    Call UpdateDisplay(BirthDate, "D")
End If
End Sub

Public Function PatientLoadDisplay(PatientId As Long, TheOne As Boolean, AName As String, Optional ByVal CreateNew As Boolean) As Boolean
On Error GoTo UIError_Label
Dim Temp As String
Dim TempPatientId As Long
Dim LocalDate As ManagedDate
Dim RetrievePatient As Patient
PatientLoadDisplay = True
WhichPath = TheOne
cmdInsured.Visible = TheOne
cmdAssign.Visible = TheOne
    miPolicyPatientId = 0
    PolicyHolderid = 0
    PolicyHolderIdRel = ""
    PolicyHolderIdBill = False
    OriginalPolicyHolderId = 0
    OriginalPolicyHolderIdRel = ""
    OriginalPolicyHolderIdBill = False
    SecondPolicyHolderId = 0
    SecondPolicyHolderIdRel = ""
    SecondPolicyHolderIdBill = False
    OriginalSecondPolicyHolderId = 0
    OriginalSecondPolicyHolderIdRel = ""
    OriginalSecondPolicyHolderIdBill = False
    TempPatientId = PatientId
If PatientId > 0 Then
    If AName = "" And CreateNew Then
        PolicyHolderid = PatientId
        PatientId = 0
        PolicyHolderExists = False
    Else
        PolicyHolderExists = True
    End If
Else
    PolicyHolderExists = False
End If
Set LocalDate = New ManagedDate
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PatientId
If (RetrievePatient.RetrievePatient) Then
    If (Trim(AName) <> "") And (PatientId < 1) Then
        LastName.Text = Trim(AName)
    Else
        LastName.Text = RetrievePatient.LastName
    End If
    FirstName.Text = RetrievePatient.FirstName
    MiddleInitial.Text = RetrievePatient.MiddleInitial
    NameRef.Text = RetrievePatient.NameRef
    Address.Text = RetrievePatient.Address
    txtSuite.Text = RetrievePatient.Suite
    City.Text = RetrievePatient.City
    State.Text = RetrievePatient.State
    Marital.Text = RetrievePatient.Marital
    LocalDate.ExposedDate = RetrievePatient.BirthDate
    If (LocalDate.ConvertManagedDateToDisplayDate) Then
        BirthDate.Text = LocalDate.ExposedDate
    End If
    Temp = ""
    Call DisplayZip(RetrievePatient.Zip, Temp)
    Zip.Text = Temp
    Temp = ""
    Call DisplaySocialSecurity(RetrievePatient.SocialSecurity, Temp)
    SSN.Text = Temp
    Temp = ""
    Call DisplayPhone(RetrievePatient.HomePhone, Temp)
    HomePhone.Text = Temp
    Temp = ""
    Call DisplayPhone(RetrievePatient.CellPhone, Temp)
    CellPhone.Text = Temp
    Temp = ""
    Call DisplayPhone(RetrievePatient.BusinessPhone, Temp)
    WorkPhone.Text = Temp
    Gender.Text = RetrievePatient.Gender
    Occupation.Text = RetrievePatient.Occupation
    Employer.Text = RetrievePatient.BusinessName
    Temp = ""
    Call DisplayPhone(RetrievePatient.EmployerPhone, Temp)
    EmpPhone.Text = Temp
    EmployerAddress.Text = RetrievePatient.BusinessAddress
    EmployerCity.Text = RetrievePatient.BusinessCity
    EmployerState.Text = RetrievePatient.BusinessState
    Temp = ""
    Call DisplayZip(RetrievePatient.BusinessZip, Temp)
    EmployerZip.Text = Temp
    If (RetrievePatient.FinancialAssignment) Then
        chkAsgP1.Value = 1
    Else
        chkAsgP1.Value = 0
    End If
    If (RetrievePatient.FinancialSignature) Then
        chkSigP1.Value = 1
    Else
        chkSigP1.Value = 0
    End If
    SigDate.Text = ""
    LocalDate.ExposedDate = RetrievePatient.FinancialSignatureDate
    If (LocalDate.ConvertManagedDateToDisplayDate) Then
        SigDate.Text = LocalDate.ExposedDate
    End If
    PolicyHolderid = RetrievePatient.PolicyPatientId
    PolicyHolderIdRel = RetrievePatient.Relationship
    PolicyHolderIdBill = RetrievePatient.BillParty
    OriginalPolicyHolderId = PolicyHolderid
    OriginalPolicyHolderIdRel = RetrievePatient.Relationship
    OriginalPolicyHolderIdBill = RetrievePatient.BillParty
    SecondPolicyHolderId = RetrievePatient.SecondPolicyPatientId
    SecondPolicyHolderIdRel = RetrievePatient.SecondRelationship
    SecondPolicyHolderIdBill = RetrievePatient.SecondBillParty
    OriginalSecondPolicyHolderId = SecondPolicyHolderId
    OriginalSecondPolicyHolderIdRel = RetrievePatient.SecondRelationship
    OriginalSecondPolicyHolderIdBill = RetrievePatient.SecondBillParty
    miPolicyPatientId = PatientId
End If
If (miPolicyPatientId < 1) Then
    If CheckConfigCollection("ASSIGNON") = "T" Then
        chkAsgP1.Value = 1
    Else
        chkAsgP1.Value = 0
    End If
    If CheckConfigCollection("SIGNON") = "T" Then
        chkSigP1.Value = 1
    Else
        chkSigP1.Value = 0
    End If
End If
PatientId = TempPatientId
Set RetrievePatient = Nothing
Set LocalDate = Nothing
Exit Function
UIError_Label:
    PatientLoadDisplay = False
    Set RetrievePatient = Nothing
    Set LocalDate = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Sub SigDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(SigDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SigDate.Text = ""
        SigDate.SetFocus
        SendKeys "{Home}"
        KeyAscii = 0
    Else
        Call UpdateDisplay(SigDate, "D")
    End If
End If
End Sub

Private Sub SigDate_Validate(Cancel As Boolean)
If Not (OkDate(SigDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    SigDate.Text = ""
    SigDate.SetFocus
    SendKeys "{Home}"
Else
    Call UpdateDisplay(SigDate, "D")
End If
End Sub

Private Function ApplyChanges(IType As String) As Boolean
On Error GoTo UIError_Label
Dim Temp As String
Dim RequireSS As Boolean
Dim LocalDate As ManagedDate
Dim VerifyPatient As Patient
Dim PatientFind As Patient
Dim RetrievePatient As Patient
Dim ThePatientId As Long
ApplyChanges = True
RequireSS = Not (CheckConfigCollection("REQUIRESS") = "F")
RequireSS = False
Temp = LastName.Text
Call StripCharacters(LastName.Text, "'")
LastName.Text = Temp
Temp = FirstName.Text
Call StripCharacters(FirstName.Text, "'")
FirstName.Text = Temp
If (miPolicyPatientId < 1) Then
    Set VerifyPatient = New Patient
    Temp = SSN.Text
    Call StripCharacters(Temp, "-")
    If (RequireSS) Then
        VerifyPatient.SocialSecurity = Trim(Temp)
        VerifyPatient.LastName = ""
        VerifyPatient.FirstName = ""
        VerifyPatient.HomePhone = ""
        If (VerifyPatient.CheckPatient > 0) Then
            If (RequireSS) Then
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Set VerifyPatient = Nothing
                SSN.SetFocus
                SendKeys "{Home}"
                Exit Function
            Else
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Continue"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 4) Then
                    Set VerifyPatient = Nothing
                    SSN.SetFocus
                    SendKeys "{Home}"
                    Exit Function
                End If
            End If
        End If
    End If
    If (RequireSS) Then
        VerifyPatient.SocialSecurity = ""
    Else
        VerifyPatient.SocialSecurity = Trim(Temp)
    End If
    VerifyPatient.MiddleInitial = Trim(MiddleInitial.Text)
    VerifyPatient.LastName = Trim(LastName.Text)
    VerifyPatient.FirstName = Trim(FirstName.Text)
    VerifyPatient.HomePhone = Trim(HomePhone.Text)
    Set LocalDate = New ManagedDate
    LocalDate.ExposedDate = BirthDate.Text
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        VerifyPatient.BirthDate = LocalDate.ExposedDate
    Else
        VerifyPatient.BirthDate = ""
    End If
    Set LocalDate = Nothing
    If (VerifyPatient.CheckPatient > 0) Then
        frmEventMsgs.Header = "Patient Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set VerifyPatient = Nothing
        Exit Function
    End If
    Set VerifyPatient = Nothing
End If
Set LocalDate = New ManagedDate
Set PatientFind = New Patient
PatientFind.LastName = UCase(Trim(LastName.Text))
PatientFind.FirstName = UCase(Trim(FirstName.Text))
PatientFind.MiddleInitial = UCase(Trim(MiddleInitial.Text))
Temp = SSN.Text
Call StripCharacters(Temp, "-")
PatientFind.SocialSecurity = UCase(Trim(Temp))
LocalDate.ExposedDate = BirthDate.Text
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    PatientFind.BirthDate = LocalDate.ExposedDate
Else
    PatientFind.BirthDate = ""
End If
If (PatientFind.CheckPatient > 0) And (miPolicyPatientId < 1) Then
    frmEventMsgs.Header = "Already Exists"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    LastName.SetFocus
    SendKeys "{Home}"
    Set PatientFind = Nothing
    Exit Function
End If

Set RetrievePatient = New Patient
RetrievePatient.PatientId = miPolicyPatientId
Call RetrievePatient.RetrievePatient
RetrievePatient.LastName = UCase(Trim(LastName.Text))
RetrievePatient.FirstName = UCase(Trim(FirstName.Text))
RetrievePatient.MiddleInitial = UCase(Trim(MiddleInitial.Text))
RetrievePatient.NameRef = UCase(Trim(NameRef.Text))
RetrievePatient.Address = Trim(Address)
RetrievePatient.City = Trim(City.Text)
RetrievePatient.State = Trim(State.Text)
RetrievePatient.Zip = Trim(Zip.Text)
Temp = HomePhone.Text
Call StripCharacters(Temp, "(")
Call StripCharacters(Temp, ")")
Call StripCharacters(Temp, "-")
RetrievePatient.HomePhone = Trim(Temp)
Temp = WorkPhone.Text
Call StripCharacters(Temp, "(")
Call StripCharacters(Temp, ")")
Call StripCharacters(Temp, "-")
RetrievePatient.BusinessPhone = Trim(Temp)
RetrievePatient.Marital = UCase(Trim(Marital.Text))
RetrievePatient.Gender = UCase(Trim(Gender.Text))
LocalDate.ExposedDate = BirthDate.Text
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    RetrievePatient.BirthDate = LocalDate.ExposedDate
End If
Temp = CellPhone.Text
Call StripCharacters(Temp, "(")
Call StripCharacters(Temp, ")")
Call StripCharacters(Temp, "-")
RetrievePatient.CellPhone = Trim(Temp)
Temp = SSN.Text
Call StripCharacters(Temp, "-")
RetrievePatient.SocialSecurity = Trim(Temp)
RetrievePatient.Occupation = Trim(Occupation.Text)
RetrievePatient.BusinessName = Trim(Employer.Text)
Temp = EmpPhone.Text
Call StripCharacters(Temp, "(")
Call StripCharacters(Temp, ")")
Call StripCharacters(Temp, "-")
RetrievePatient.EmployerPhone = Trim(Temp)
RetrievePatient.BusinessAddress = Trim(EmployerAddress.Text)
RetrievePatient.BusinessCity = Trim(EmployerCity.Text)
RetrievePatient.BusinessState = Trim(EmployerState.Text)
RetrievePatient.BusinessZip = Trim(EmployerZip.Text)
If PolicyHolderExists Then
    RetrievePatient.PolicyPatientId = PolicyHolderid
    RetrievePatient.BillParty = PolicyHolderIdBill
    If (Trim(PolicyHolderIdRel) = "") Then
        RetrievePatient.Relationship = "Y"
        RetrievePatient.BillParty = True
    End If
    RetrievePatient.SecondPolicyPatientId = SecondPolicyHolderId
    RetrievePatient.SecondBillParty = SecondPolicyHolderIdBill
    If (Trim(SecondPolicyHolderIdRel) = "") Then
        RetrievePatient.Relationship = "Y"
        RetrievePatient.SecondBillParty = True
    End If
End If
RetrievePatient.BulkMailing = True
RetrievePatient.FinancialAssignment = False
If (chkAsgP1.Value = 1) Then
    RetrievePatient.FinancialAssignment = True
End If
RetrievePatient.FinancialSignature = False
If (chkSigP1.Value = 1) Then
    RetrievePatient.FinancialSignature = True
End If
LocalDate.ExposedDate = SigDate.Text
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    RetrievePatient.FinancialSignatureDate = LocalDate.ExposedDate
End If
If (miPolicyPatientId < 1) Then
    RetrievePatient.Status = IType
End If
Call RetrievePatient.ApplyPatient

'Updates Relationship,PolicyPatientId to fix 2854 and also not to mess up 2461.
ThePatientId = RetrievePatient.PatientId
Set RetrievePatient = Nothing
Set RetrievePatient = New Patient
RetrievePatient.PatientId = ThePatientId
If (RetrievePatient.RetrievePatient) Then
    RetrievePatient.PolicyPatientId = ThePatientId
    RetrievePatient.Relationship = "Y"
End If
Call RetrievePatient.ApplyPatient

If (miPolicyPatientId < 1) Then
    miPolicyPatientId = RetrievePatient.PatientId
End If
Set LocalDate = Nothing
Set PatientFind = Nothing
Set RetrievePatient = Nothing
ApplyChanges = True
Exit Function
UIError_Label:
    Set LocalDate = Nothing
    Set PatientFind = Nothing
    Set RetrievePatient = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Function SetPolicyHolder(PID As Long, PtId As Long) As Boolean
Dim RetrievePatient As Patient
SetPolicyHolder = True
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PID
If (RetrievePatient.RetrievePatient) Then
    If (RetrievePatient.PolicyPatientId < 0) Then
        RetrievePatient.PolicyPatientId = PtId
        Call RetrievePatient.ApplyPatient
        SetPolicyHolder = True
    ElseIf (RetrievePatient.SecondPolicyPatientId < 0) Then
        RetrievePatient.SecondPolicyPatientId = PtId
        Call RetrievePatient.ApplyPatient
        SetPolicyHolder = True
    End If
End If
Set RetrievePatient = Nothing
End Function

Private Function ValidateRequiredFields() As Boolean
Dim RequireField As Boolean
ValidateRequiredFields = False
If (Trim(FirstName.Text) = "") Then
    frmEventMsgs.Header = "Please Enter First Name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    FirstName.SetFocus
    SendKeys "{Home}"
    Exit Function
End If
If (Trim(LastName.Text) = "") Then
    frmEventMsgs.Header = "Please Enter Last Name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    LastName.SetFocus
    SendKeys "{Home}"
    Exit Function
End If
RequireField = CheckConfigCollection("POLHLDSSN") = "T"
If (RequireField) And (Trim(SSN.Text) = "") Then
    frmEventMsgs.Header = "Please Enter Social Security Number"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    SSN.SetFocus
    SendKeys "{Home}"
    Exit Function
End If
If CheckConfigCollection("REQUIREDOB") = "T" Then
    If (Trim(BirthDate.Text) = "") Then
        frmEventMsgs.Header = "Please Enter Birth Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        BirthDate.SetFocus
        SendKeys "{Home}"
        Exit Function
    End If
End If
If CheckConfigCollection("REQUIREHOLDER") = "T" Then
    If (Trim(BirthDate.Text) = "") Then
        frmEventMsgs.Header = "Please Enter Birth Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        BirthDate.SetFocus
        SendKeys "{Home}"
        Exit Function
    End If
    If (Trim(Address.Text) = "") Then
        frmEventMsgs.Header = "Please Enter Address"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Address.SetFocus
        SendKeys "{Home}"
        Exit Function
    End If
    If (Trim(City.Text) = "") Then
        frmEventMsgs.Header = "Please Enter City"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        City.SetFocus
        SendKeys "{Home}"
        Exit Function
    End If
    If (Trim(State.Text) = "") Then
        frmEventMsgs.Header = "Please Enter State"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        State.SetFocus
        SendKeys "{Home}"
        Exit Function
    End If
    If (Trim(Zip.Text) = "") Then
        frmEventMsgs.Header = "Please Enter Zip"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Zip.SetFocus
        SendKeys "{Home}"
        Exit Function
    End If
End If
RequireField = CheckConfigCollection("REQUIREGENDER") = "T"
If (RequireField) And (Trim(Gender.Text) = "") Then
    frmEventMsgs.Header = "Please Enter Gender"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Gender.SetFocus
    SendKeys "{Home}"
    Exit Function
End If
RequireField = CheckConfigCollection("REQUIREHOMEPHONE") = "T"
If (RequireField) And (Trim(HomePhone.Text) = "") Then
    frmEventMsgs.Header = "Please Enter Home Phone"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    HomePhone.SetFocus
    SendKeys "{Home}"
    Exit Function
End If
ValidateRequiredFields = True
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
