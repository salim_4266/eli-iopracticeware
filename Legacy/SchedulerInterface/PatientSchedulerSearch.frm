VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPatientSchedulerSearch 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "PatientSearch"
   ClientHeight    =   9000
   ClientLeft      =   120
   ClientTop       =   690
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Moveable        =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox OldPatId 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   16
      TabIndex        =   6
      Top             =   2040
      Width           =   3135
   End
   Begin VB.TextBox Dob 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7920
      MaxLength       =   10
      TabIndex        =   7
      Top             =   2040
      Width           =   3375
   End
   Begin VB.TextBox PatId 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7920
      MaxLength       =   10
      TabIndex        =   5
      Top             =   1560
      Width           =   3375
   End
   Begin VB.TextBox LastName 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   16
      TabIndex        =   0
      Top             =   600
      Width           =   3135
   End
   Begin VB.TextBox HomePhone 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   16
      TabIndex        =   2
      Top             =   1080
      Width           =   3135
   End
   Begin VB.TextBox SocialSecurity 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7920
      MaxLength       =   12
      TabIndex        =   3
      Top             =   1080
      Width           =   3375
   End
   Begin VB.TextBox PolicyId 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1800
      MaxLength       =   32
      TabIndex        =   4
      Top             =   1560
      Width           =   3135
   End
   Begin VB.TextBox FirstName 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7920
      MaxLength       =   16
      TabIndex        =   1
      Top             =   600
      Width           =   3375
   End
   Begin VB.ListBox lstPatient 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4080
      ItemData        =   "PatientSchedulerSearch.frx":0000
      Left            =   300
      List            =   "PatientSchedulerSearch.frx":0002
      TabIndex        =   14
      Top             =   2760
      Visible         =   0   'False
      Width           =   11055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   1080
      Left            =   360
      TabIndex        =   9
      Top             =   7080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientSchedulerSearch.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReset 
      Height          =   1080
      Left            =   2640
      TabIndex        =   8
      Top             =   7080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientSchedulerSearch.frx":01E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStatus 
      Height          =   1080
      Left            =   9600
      TabIndex        =   19
      Top             =   7080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientSchedulerSearch.frx":03C3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPHolder 
      Height          =   1080
      Left            =   7440
      TabIndex        =   20
      Top             =   7080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientSchedulerSearch.frx":05A5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1080
      Left            =   5040
      TabIndex        =   23
      Top             =   7080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientSchedulerSearch.frx":0791
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Prior Pat No."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   360
      TabIndex        =   22
      Top             =   2160
      Width           =   1350
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Date of Birth"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6480
      TabIndex        =   21
      Top             =   2160
      Width           =   1305
   End
   Begin VB.Label lblLoad 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading ..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5040
      TabIndex        =   18
      Top             =   2400
      Visible         =   0   'False
      Width           =   1470
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6165
      TabIndex        =   17
      Top             =   1680
      Width           =   1620
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6630
      TabIndex        =   16
      Top             =   720
      Width           =   1155
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Policy Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   840
      TabIndex        =   15
      Top             =   1680
      Width           =   870
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Search For Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   4560
      TabIndex        =   13
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Home Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   360
      TabIndex        =   12
      Top             =   1200
      Width           =   1365
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "SS #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7260
      TabIndex        =   11
      Top             =   1200
      Width           =   525
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   570
      TabIndex        =   10
      Top             =   720
      Width           =   1140
   End
End
Attribute VB_Name = "frmPatientSchedulerSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Header As String
Public AddOn As Boolean
Public PatientNumberOn As Boolean
Public TheLastName As String
Public TheFirstName As String

Private AnyOn As Boolean
Private PatientStatus As String
Private PatientOldNo As String
Private PatientPolicyId As String
Private PatientLastName As String
Private PatientFirstName As String
Private PatientPhone As String
Private PatientSS As String
Private PatientDOB As String
Private Const MaxReturned As Integer = 50
Private m_PatientId As Long
Public pat As New Patient
Public PatRunsUsed As Long

Property Get PatientId() As Long
      PatientId = m_PatientId
End Property
Property Let PatientId(Value As Long)
    m_PatientId = Value
    CommonLib.ActivePatientId = m_PatientId
End Property

Private Sub cmdExit_Click()
' Clear active patient in context
CommonLib.ActivePatientId = 0

TheLastName = Trim(LastName.Text)
TheFirstName = Trim(FirstName.Text)
If (Header = "Select Policy Holder") Then
    Unload frmPatientSchedulerSearch
Else
    frmPatientSchedulerSearch.Hide
End If
End Sub

Private Sub cmdPHolder_Click()
PatientId = -1
TheLastName = ""
TheFirstName = ""
If (Header = "Select Policy Holder") Then
    Unload frmPatientSchedulerSearch
Else
    frmPatientSchedulerSearch.Hide
End If
DoEvents
End Sub

Private Sub cmdPrev_Click()
Dim Temp As Long
Dim Temp1 As Long
Dim ApplTemp As ApplicationTemplates
Call cmdReset_Click
Temp = frmHome.GetPreviousPatientId
If (Temp > 0) Then
    PatId.Text = Trim(Str(Temp))
    Call PatId_KeyPress(13)
End If
End Sub
Private Sub cmdReset_Click()
OldPatId.Text = ""
OldPatId.Enabled = True
PatId.Text = ""
PatId.Enabled = True
PolicyId.Text = ""
PolicyId.Enabled = True
LastName.Text = ""
LastName.Enabled = True
FirstName.Text = ""
FirstName.Enabled = True
SocialSecurity.Text = ""
SocialSecurity.Enabled = True
HomePhone.Text = ""
HomePhone.Enabled = True
Dob.Text = ""
Dob.Enabled = True
lstPatient.Clear
lstPatient.Visible = False
lblLoad.Visible = False
PatientId = 0
LastName.SetFocus
End Sub

Private Sub cmdStatus_Click()
If (cmdStatus.Text = "Active") Then
    PatientStatus = "A"
    cmdStatus.Text = "See All"
    Call cmdReset_Click
Else
    PatientStatus = ""
    cmdStatus.Text = "Active"
    Call cmdReset_Click
End If
End Sub

Private Sub LocatePatients()
On Error GoTo UIError_Label
Dim i As Integer
Dim Temp As String
Dim DisplayItem As String
Dim Results As Long
Dim OResults As Long
Dim BDate As ManagedDate
Dim PatientFind As Patient
Dim PatFin As PatientFinance
Dim PatDep As PatientFinanceDependents
lstPatient.Clear
If (AddOn) Then
    lstPatient.AddItem "Create New Patient"
End If
Set BDate = New ManagedDate
Set PatientFind = New Patient
Set PatFin = New PatientFinance
PatientDOB = ""
BDate.ExposedDate = Trim(Dob.Text)
If (BDate.ConvertDisplayDateToManagedDate) Then
    PatientDOB = BDate.ExposedDate
End If
PatientPolicyId = Trim(PolicyId.Text)
PatientLastName = UCase(Trim(LastName.Text))
PatientFirstName = UCase(Trim(FirstName.Text))
PatientSS = UCase(Trim(SocialSecurity.Text))
PatientPhone = UCase(Trim(HomePhone.Text))
PatientOldNo = Trim(OldPatId.Text)
Call StripCharacters(PatientPolicyId, "'")
Call StripCharacters(PatientPhone, "'")
Call StripCharacters(PatientSS, "'")
Call StripCharacters(PatientLastName, "'")
Call StripCharacters(PatientFirstName, "'")
If (PatientSS <> "") Then
    Call StripCharacters(PatientSS, "-")
    PatientFind.SocialSecurity = PatientSS
End If
If (PatientDOB <> "") Then
    PatientFind.BirthDate = PatientDOB
End If
If (PatientPhone <> "") Then
    Call StripCharacters(PatientPhone, "-")
    Call StripCharacters(PatientPhone, "(")
    Call StripCharacters(PatientPhone, ")")
    PatientFind.HomePhone = Trim(PatientPhone)
End If
If (PatientFirstName <> "") Then
    PatientFind.FirstName = PatientFirstName
End If
PatientFind.LastName = PatientLastName
PatientFind.Status = ""
If (AnyOn) Then
    PatientFind.Status = PatientStatus
End If
If (PatId.Text <> "") Then
    PatientFind.PatNumber = val(Trim(PatId.Text))
    Results = PatientFind.FindPatientDirect
    If (Results > 0) Then
        GoSub DisplayList
    End If
ElseIf (OldPatId.Text <> "") Then
    PatientFind.OldPatientNo = Trim(OldPatId.Text)
    Results = PatientFind.FindPatientDirect
    If (Results > 0) Then
        GoSub DisplayList
    End If
ElseIf (PatientLastName <> "") Or (PatientSS <> "") Or (PatientPhone <> "") Or (PatientFirstName <> "") Or (PatientDOB <> "") Then
    Results = PatientFind.FindPatient
    PatientNumberOn = False
    If (Results > 0) Then
        GoSub DisplayList
    End If
ElseIf (PatientPolicyId <> "") Then
    PatientNumberOn = False
    PatFin.PrimaryPerson = PatientPolicyId
    OResults = PatFin.RetrievePatientbyPolicy(1)
    If (OResults > 0) Then
        PatientFind.PatientId = PatFin.PatientId
        Results = PatientFind.FindPatientbyPolicyId
        If (Results > 0) Then
            GoSub DisplayList
        End If
        If (OResults > 1) Then
            Set PatFin = Nothing
            Set PatFin = New PatientFinance
            PatFin.PrimaryPerson = PatientPolicyId
            Results = PatFin.RetrievePatientbyPolicy(2)
            PatientFind.PatientId = PatFin.PatientId
            Results = PatientFind.FindPatientbyPolicyId
            If (Results > 0) Then
                GoSub DisplayList
            End If
        End If
    Else
        Set PatDep = New PatientFinanceDependents
        PatDep.DependentMember = PatientPolicyId
        If (PatDep.FindPatientFinancialDependentsbyPolicy > 0) Then
            If (PatDep.SelectPatientFinancialDependents(1)) Then
                If (Trim(PatientPolicyId) = Trim(PatDep.DependentMember)) Then
                    PatientId = PatDep.DependentPatientId
                    frmPatientSchedulerSearch.Hide
                End If
            End If
        End If
        Set PatDep = Nothing
        If (Results > 0) Then
            GoSub DisplayList
        End If
    End If
End If

lstPatient.ListIndex = -1
lstPatient.Enabled = True
lstPatient.Visible = True
PolicyId.Enabled = False
LastName.Enabled = False
FirstName.Enabled = False
HomePhone.Enabled = False
Dob.Enabled = False
SocialSecurity.Enabled = False
PatId.Enabled = False
OldPatId.Enabled = False
Set BDate = Nothing
Set PatFin = Nothing
Set PatientFind = Nothing
If (Results = 1) And (PatientNumberOn) Then
    DoEvents
    If (PatientId <> -1) Then
        Call frmHome.SetPreviousPatientId(PatientId)
    End If
    frmPatientSchedulerSearch.Hide
End If
Exit Sub
DisplayList:
    If (Results > 0) Then
        i = 1
        While (PatientFind.SelectPatient(i)) And (i < MaxReturned + 1)
            DisplayItem = Space(75)
            Mid(DisplayItem, 1, Len(PatientFind.LastName)) = PatientFind.LastName
            If (Len(PatientFind.FirstName) <> 0) Then
                Mid(DisplayItem, 15, Len(PatientFind.FirstName)) = PatientFind.FirstName
            End If
            If (Len(PatientFind.MiddleInitial) <> 0) Then
                Mid(DisplayItem, 25, Len(PatientFind.MiddleInitial)) = PatientFind.MiddleInitial
            End If
            If (Len(PatientFind.NameRef) <> 0) Then
                Mid(DisplayItem, 26, Len(PatientFind.NameRef)) = PatientFind.NameRef
            End If
            If (PatientFind.PatientId > 0) Then
                Mid(DisplayItem, 28, Len(Trim(Str(PatientFind.PatientId)))) = Trim(Str(PatientFind.PatientId))
            End If
            If (Len(PatientFind.SocialSecurity) <> 0) Then
                Call DisplaySocialSecurity(PatientFind.SocialSecurity, Temp)
                Mid(DisplayItem, 35, Len(Temp)) = Temp
            End If
            If (Len(PatientFind.HomePhone) <> 0) Then
                Call DisplayPhone(PatientFind.HomePhone, Temp)
                Mid(DisplayItem, 47, Len(Temp)) = Temp
            End If
            If (Len(PatientFind.BirthDate) <> 0) Then
                BDate.ExposedDate = PatientFind.BirthDate
                If (BDate.ConvertManagedDateToDisplayDate) Then
                    Mid(DisplayItem, 61, Len(BDate.ExposedDate)) = BDate.ExposedDate
                End If
            End If
            DisplayItem = DisplayItem + Trim(Str(PatientFind.PatientId))
            lstPatient.AddItem DisplayItem
            i = i + 1
            If (Results = 1) And (PatientNumberOn) Then
                PatientId = PatientFind.PatientId
            End If
        Wend
    End If
    Return
UIError_Label:
    lstPatient.Visible = False
    lstPatient.Enabled = False
    PolicyId.Text = ""
    SocialSecurity.Text = ""
    HomePhone.Text = ""
    LastName.Text = ""
    Dob.Text = ""
    OldPatId.Text = ""
    Set BDate = Nothing
    Set PatFin = Nothing
    Set PatientFind = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub Form_Load()
Dim Temp As String
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientSchedulerSearch.BorderStyle = 1
    frmPatientSchedulerSearch.ClipControls = True
    frmPatientSchedulerSearch.Caption = Mid(frmPatientSchedulerSearch.Name, 4, Len(frmPatientSchedulerSearch.Name) - 3)
    frmPatientSchedulerSearch.AutoRedraw = True
    frmPatientSchedulerSearch.Refresh
End If
PatientId = 0
If (Trim(Header) <> "") Then
    Label5.Caption = Header
End If
PatientStatus = "A"
AnyOn = CheckConfigCollection("OPERATEANY", "T") = "T"
cmdStatus.Text = "See All"
cmdStatus.Visible = AnyOn
cmdPHolder.Visible = False
If (Header = "Select Policy Holder") Then
    cmdPHolder.Visible = True
End If
Dob.Text = ""
LastName.Text = ""
FirstName.Text = ""
HomePhone.Text = ""
SocialSecurity.Text = ""
PolicyId.Text = ""
PatId.Text = ""
OldPatId.Text = ""
lstPatient.Visible = False
lstPatient.Clear
If (PatientNumberOn) Then
    PatId.TabIndex = 0
Else
    PatId.TabIndex = 5
End If
End Sub

Private Sub Dob_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (OkDate(Dob.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Dob.Text = ""
        Dob.SetFocus
        SendKeys "{Home}"
        KeyAscii = 0
    Else
        Call UpdateDisplay(Dob, "D")
        If (GetAge(Dob.Text) < 0) Then
            frmEventMsgs.Header = "Future Dates not allowed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Dob.Text = ""
            Dob.SetFocus
            SendKeys "{Home}"
            KeyAscii = 0
        Else
            Call LocatePatients
        End If
    End If
End If
End Sub

Private Sub FirstName_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call LocatePatients
End If
End Sub

Private Sub HomePhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call LocatePatients
End If
End Sub


Private Sub LastName_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call LocatePatients
End If
End Sub

Private Sub PatId_KeyPress(KeyAscii As Integer)

Select Case KeyAscii
Case vbKeyBack, 3, 22, 24 ' ctrl + "c", ctrl + "v", ctrl + "x"
Case vbKeyReturn
    Call LocatePatients
Case Else
    If Not IsNumeric(Chr(KeyAscii)) Then
        frmEventMsgs.Header = "Must be numeric"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
End Select

End Sub

Private Sub PolicyId_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call LocatePatients
End If
End Sub

Private Sub OldPatId_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call LocatePatients
End If
End Sub

Private Sub SocialSecurity_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call LocatePatients
End If
End Sub

Private Sub lstPatient_DblClick()
If (lstPatient.ListIndex >= 0) Then
    If (AddOn) Then
        If (lstPatient.ListIndex = 0) Then
            PatientId = -1
        Else
            lblLoad.Visible = True
            PatientId = val(Mid(lstPatient.List(lstPatient.ListIndex), 76, Len(lstPatient.List(lstPatient.ListIndex)) - 75))
        End If
    Else
        lblLoad.Visible = True
        PatientId = val(Mid(lstPatient.List(lstPatient.ListIndex), 76, Len(lstPatient.List(lstPatient.ListIndex)) - 75))
    End If
    DoEvents
    TheLastName = Trim(LastName.Text)
    TheFirstName = Trim(FirstName.Text)
    If (PatientId <> -1) Then
        Call frmHome.SetPreviousPatientId(PatientId)
        If PatientId > 0 Then Call InsertLog("frmPatientSchedulerSearch.frm", "Searched Patient " & PatientId, LoginCatg.Query, "", "", 0, 0)
    End If
    frmPatientSchedulerSearch.Hide
End If
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
