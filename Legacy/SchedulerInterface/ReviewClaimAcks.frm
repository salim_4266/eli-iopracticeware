VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmReviewClaimAcks 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6240
      ItemData        =   "ReviewClaimAcks.frx":0000
      Left            =   480
      List            =   "ReviewClaimAcks.frx":0002
      TabIndex        =   0
      Top             =   840
      Width           =   11055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   480
      TabIndex        =   1
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewClaimAcks.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9720
      TabIndex        =   2
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewClaimAcks.frx":01E3
   End
   Begin VB.ListBox lstAcks 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6240
      ItemData        =   "ReviewClaimAcks.frx":03C2
      Left            =   480
      List            =   "ReviewClaimAcks.frx":03C4
      TabIndex        =   5
      Top             =   840
      Width           =   11055
   End
   Begin VB.Label lblFile 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "For File "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   240
      Left            =   480
      TabIndex        =   4
      Top             =   600
      Width           =   720
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Review Claim Acknowledgements"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   330
      Left            =   3915
      TabIndex        =   3
      Top             =   240
      Width           =   4305
   End
End
Attribute VB_Name = "frmReviewClaimAcks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private AckDate As String

Private Sub cmdDone_Click()
If (lstAcks.Visible) Then
    lstAcks.Visible = False
    lstAppts.Visible = True
Else
    Unload frmReviewClaimAcks
End If
End Sub

Private Sub cmdHome_Click()
Unload frmReviewClaimAcks
End Sub

Public Function LoadAcks() As Boolean
Dim AFile As String
Dim MyFile As String
LoadAcks = False
AckDate = ""
lstAcks.Clear
lstAcks.Visible = False
lstAppts.Clear
lblFile.Visible = False
MyFile = SubmissionsAcksDirectory + "*.*"
AFile = Dir(MyFile)
While (AFile <> "")
    lstAppts.AddItem AFile
    AFile = Dir
Wend
If (lstAppts.ListCount > 0) Then
    LoadAcks = True
End If
End Function

Private Sub lstAcks_Click()
Dim RcvId As Long
Dim PatId As Long
Dim IRef As String
Dim ApplList As ApplicationAIList
If (lstAcks.ListIndex >= 0) Then
    IRef = ""
    If (Left(lstAcks.List(lstAcks.ListIndex), 3) = "AK2") Then
        IRef = Mid(lstAcks.List(lstAcks.ListIndex), 9, 9)
        IRef = Trim(Str(val(IRef)))
        If (IRef <> "") Then
            Set ApplList = New ApplicationAIList
            Call ApplList.ApplGetRcvPatbyTrans(AckDate, IRef, RcvId, PatId)
            Set ApplList = Nothing
            If (RcvId > 0) And (PatId > 0) Then
''''''                frmPayments.AccessType = ""
''''''                frmPayments.Whom = True
''''''                frmPayments.PatientId = PatId
''''''                frmPayments.ReceivableId = RcvID
''''''                frmPayments.DisplayHow = ""
''''''                If (frmPayments.LoadPayments(True, "")) Then
''''''                    frmPayments.Show 1
''''''                End If
                Dim PatientDemographics As New PatientDemographics
                PatientDemographics.PatientId = PatId
                Call PatientDemographics.DisplayPatientFinancialScreen
                Set PatientDemographics = Nothing
            End If
        End If
    End If
End If
End Sub

Private Sub lstAppts_Click()
If (lstAppts.ListIndex >= 0) Then
    lblFile.Caption = "Processing File " + Trim(lstAppts.List(lstAppts.ListIndex))
    lblFile.Visible = True
    If (FM.IsFileThere(SubmissionsAcksDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))) Then
        If Not (LoadAcksFile(SubmissionsAcksDirectory + Trim(lstAppts.List(lstAppts.ListIndex)))) Then
            frmEventMsgs.Header = "No Rejected Transactions."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End If
End Sub

Private Function LoadAcksFile(AFile As String) As Boolean
Dim FileNum As Long
Dim Rec As String
Dim Temp As String
Dim TheDate As String
Dim InputChar As String * 1
On Error GoTo LeaveFast
LoadAcksFile = False
lstAcks.Clear
If (FM.IsFileThere(AFile)) Then
    Rec = ""
    FileNum = FreeFile
    FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
    While Not (EOF(FileNum))
        InputChar = Input(1, #FileNum)
        If (InputChar = "~") Then
            Rec = Rec + InputChar
            If (Left(Rec, 3) <> "AK5") And (Left(Rec, 3) <> "AK3") And (Left(Rec, 3) <> "AK4") Then
                lstAcks.AddItem Rec
            Else
                Temp = lstAcks.List(lstAcks.ListCount - 1)
                Temp = Temp + Rec
                lstAcks.List(lstAcks.ListCount - 1) = Temp
            End If
            Rec = ""
        ElseIf (Len(Rec) > 68) Then
            Rec = Rec + InputChar
            lstAcks.AddItem Rec
            Rec = ""
        Else
            If (InputChar <> Chr(10)) And (InputChar <> Chr(13)) Then
                Rec = Rec + InputChar
            End If
        End If
    Wend
    If (Trim(Rec) <> "") Then
        If (Left(Rec, 3) <> "AK5") And (Left(Rec, 3) <> "AK3") And (Left(Rec, 3) <> "AK4") Then
            lstAcks.AddItem Rec
        Else
            Temp = lstAcks.List(lstAcks.ListCount - 1)
            Temp = Temp + Rec
            lstAcks.List(lstAcks.ListCount - 1) = Temp
        End If
    End If
    FM.CloseFile CLng(FileNum)
    If (lstAcks.ListCount > 0) Then
        lstAcks.Visible = True
        lstAppts.Visible = False
        AckDate = "20" + Left(lstAcks.List(1), 6)
        LoadAcksFile = True
    End If
End If
ByeBye:
Exit Function
LeaveFast:
    FM.CloseFile CLng(FileNum)
    If (lstAcks.ListCount > 0) Then
        lstAcks.Visible = True
        lstAppts.Visible = False
        LoadAcksFile = True
    End If
    Resume ByeBye
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
