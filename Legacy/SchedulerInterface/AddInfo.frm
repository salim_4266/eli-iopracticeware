VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmAddInfo 
   BackColor       =   &H00A95911&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Additional Info"
   ClientHeight    =   6960
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4680
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6960
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtMedRef 
      Height          =   375
      Left            =   240
      MaxLength       =   16
      TabIndex        =   9
      Top             =   5400
      Width           =   4215
   End
   Begin VB.TextBox txtConsDate 
      Height          =   375
      Left            =   240
      MaxLength       =   10
      TabIndex        =   8
      Top             =   4560
      Width           =   1815
   End
   Begin VB.TextBox txtRsnDate 
      Height          =   375
      Left            =   2640
      MaxLength       =   10
      TabIndex        =   6
      Top             =   2880
      Width           =   1815
   End
   Begin VB.ComboBox lstPrev 
      Height          =   315
      ItemData        =   "AddInfo.frx":0000
      Left            =   240
      List            =   "AddInfo.frx":000A
      TabIndex        =   7
      Text            =   "Combo4"
      Top             =   3720
      Width           =   2295
   End
   Begin VB.ComboBox lstRsn 
      Height          =   315
      ItemData        =   "AddInfo.frx":0017
      Left            =   240
      List            =   "AddInfo.frx":0024
      TabIndex        =   5
      Text            =   "Combo3"
      Top             =   2880
      Width           =   2295
   End
   Begin VB.ComboBox lstDis 
      Height          =   315
      ItemData        =   "AddInfo.frx":0045
      Left            =   240
      List            =   "AddInfo.frx":0052
      TabIndex        =   4
      Text            =   "Combo2"
      Top             =   2040
      Width           =   2295
   End
   Begin VB.TextBox txtDisDate 
      Height          =   375
      Left            =   2640
      MaxLength       =   10
      TabIndex        =   3
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox txtAdmDate 
      Height          =   375
      Left            =   240
      MaxLength       =   10
      TabIndex        =   2
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox txtAccState 
      Height          =   375
      Left            =   2640
      MaxLength       =   2
      TabIndex        =   1
      Top             =   480
      Width           =   615
   End
   Begin VB.ComboBox lstAcc 
      Height          =   315
      ItemData        =   "AddInfo.frx":0076
      Left            =   240
      List            =   "AddInfo.frx":0083
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   480
      Width           =   2295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   870
      Left            =   480
      TabIndex        =   10
      Top             =   6000
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AddInfo.frx":009E
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Claim Reference Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   20
      Top             =   5040
      Width           =   2640
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Previous Condition"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   19
      Top             =   3360
      Width           =   2160
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "First Occurrence Date (HCFA Only)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   18
      Top             =   4200
      Width           =   3915
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Reason Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2640
      TabIndex        =   17
      Top             =   2520
      Width           =   1440
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Reason Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   16
      Top             =   2520
      Width           =   1470
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Disability"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   15
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Discharge  Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2640
      TabIndex        =   14
      Top             =   840
      Width           =   1815
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Admission Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   13
      Top             =   840
      Width           =   1800
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Accident State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2640
      TabIndex        =   12
      Top             =   120
      Width           =   1650
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Accident"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   11
      Top             =   120
      Width           =   1020
   End
End
Attribute VB_Name = "frmAddInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AccidentType As String
Public AccidentState As String
Public AdmissionDate As String
Public DismissalDate As String
Public RsnType As String
Public RsnDate As String
Public Disability As String
Public PrevCond As String
Public ConsDate As String
Public ExternalRefInfo As String

Public Sub LoadAddInfo()
lstAcc.Text = ""
lstRsn.Text = ""
lstDis.Text = ""
lstPrev.Text = ""
Call LoadList(lstAcc, AccidentType)
Call LoadList(lstRsn, RsnType)
Call LoadList(lstDis, Disability)
Call LoadList(lstPrev, PrevCond)
txtAccState.Text = AccidentState
txtRsnDate.Text = RsnDate
txtAdmDate.Text = AdmissionDate
txtDisDate.Text = DismissalDate
txtConsDate.Text = ConsDate
txtMedRef.Text = ExternalRefInfo
End Sub

Private Sub LoadList(AList As ComboBox, Code As String)
Dim i As Integer
For i = 0 To AList.ListCount - 1
    If (Code = Left(AList.List(i), 1)) Then
        AList.ListIndex = i
        Exit For
    End If
Next i
End Sub

Private Sub cmdDone_Click()
If (lstAcc.ListIndex > -1) Then
    AccidentType = Left(lstAcc.List(lstAcc.ListIndex), 1)
End If
If (lstRsn.ListIndex > -1) Then
    RsnType = Left(lstRsn.List(lstRsn.ListIndex), 1)
End If
If (lstDis.ListIndex > -1) Then
    Disability = Left(lstDis.List(lstDis.ListIndex), 1)
End If
If (lstPrev.ListIndex > -1) Then
    PrevCond = Left(lstPrev.List(lstPrev.ListIndex), 1)
End If
AccidentState = txtAccState.Text
RsnDate = txtRsnDate.Text
AdmissionDate = txtAdmDate.Text
DismissalDate = txtDisDate.Text
ConsDate = txtConsDate.Text
ExternalRefInfo = txtMedRef.Text
Unload frmAddInfo
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmAddInfo.BorderStyle = 1
    frmAddInfo.ClipControls = True
    frmAddInfo.Caption = Mid(frmAddInfo.Name, 4, Len(frmAddInfo.Name) - 3)
    frmAddInfo.AutoRedraw = True
    frmAddInfo.Refresh
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
Unload frmAddInfo
End Sub

Private Sub txtAccState_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If Not (OkDate(txtAccState.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtAccState.SetFocus
        txtAccState.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtAccState, "D")
    End If
End If
End Sub

Private Sub txtAdmDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (KeyAscii = 10)) Then
    If Not (OkDate(txtAdmDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtAdmDate.SetFocus
        txtAdmDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtAdmDate, "D")
    End If
End If
End Sub

Private Sub txtAdmDate_Validate(Cancel As Boolean)
If Not (OkDate(txtAdmDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtAdmDate.SetFocus
    txtAdmDate.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtAdmDate, "D")
End If
End Sub

Private Sub txtConsDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtConsDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtConsDate.SetFocus
        txtConsDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtConsDate, "D")
    End If
End If
End Sub

Private Sub txtConsDate_Validate(Cancel As Boolean)
If Not (OkDate(txtConsDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtConsDate.SetFocus
    txtConsDate.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtConsDate, "D")
End If
End Sub

Private Sub txtDisDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtDisDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDisDate.SetFocus
        txtDisDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtDisDate, "D")
    End If
End If
End Sub

Private Sub txtDisDate_Validate(Cancel As Boolean)
If Not (OkDate(txtDisDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtDisDate.SetFocus
    txtDisDate.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtDisDate, "D")
End If
End Sub

Private Sub txtRsnDate_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    If Not (OkDate(txtRsnDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtRsnDate.SetFocus
        txtRsnDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtRsnDate, "D")
    End If
End If
End Sub

Private Sub txtRsnDate_Validate(Cancel As Boolean)
If Not (OkDate(txtRsnDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtRsnDate.SetFocus
    txtRsnDate.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtRsnDate, "D")
End If
End Sub
Public Sub FrmClose()
Unload Me
End Sub
