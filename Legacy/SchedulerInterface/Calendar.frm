VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCalendar 
   Appearance      =   0  'Flat
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtGblNote 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   4080
      MaxLength       =   128
      TabIndex        =   58
      Top             =   2040
      Width           =   4815
   End
   Begin VB.TextBox txtDrNote 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   4080
      MaxLength       =   128
      TabIndex        =   56
      Top             =   2640
      Width           =   4815
   End
   Begin VB.ListBox lstTemp 
      Height          =   840
      ItemData        =   "Calendar.frx":0000
      Left            =   2760
      List            =   "Calendar.frx":0002
      MultiSelect     =   1  'Simple
      TabIndex        =   55
      Top             =   120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtLoc1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   48
      Top             =   3720
      Width           =   2055
   End
   Begin VB.TextBox txtLoc2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   47
      Top             =   4200
      Width           =   2055
   End
   Begin VB.TextBox txtLoc3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   46
      Top             =   4680
      Width           =   2055
   End
   Begin VB.TextBox txtLoc4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   45
      Top             =   5160
      Width           =   2055
   End
   Begin VB.TextBox txtLoc5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   44
      Top             =   5640
      Width           =   2055
   End
   Begin VB.TextBox txtLoc6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   43
      Top             =   6120
      Width           =   2055
   End
   Begin VB.TextBox txtLoc7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      Locked          =   -1  'True
      MaxLength       =   128
      TabIndex        =   42
      Top             =   6600
      Width           =   2055
   End
   Begin VB.TextBox txtLocation1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   41
      Top             =   3720
      Width           =   2415
   End
   Begin VB.TextBox txtLocation2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   40
      Top             =   4200
      Width           =   2415
   End
   Begin VB.TextBox txtLocation3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   39
      Top             =   4680
      Width           =   2415
   End
   Begin VB.TextBox txtLocation4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   38
      Top             =   5160
      Width           =   2415
   End
   Begin VB.TextBox txtLocation5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   37
      Top             =   5640
      Width           =   2415
   End
   Begin VB.TextBox txtLocation6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   36
      Top             =   6120
      Width           =   2415
   End
   Begin VB.TextBox txtLocation7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      MaxLength       =   128
      TabIndex        =   35
      Top             =   6600
      Width           =   2415
   End
   Begin VB.TextBox txtPur7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   30
      Top             =   6600
      Width           =   2055
   End
   Begin VB.TextBox txtPur6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   29
      Top             =   6120
      Width           =   2055
   End
   Begin VB.TextBox txtPur5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   28
      Top             =   5640
      Width           =   2055
   End
   Begin VB.TextBox txtPur4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   27
      Top             =   5160
      Width           =   2055
   End
   Begin VB.TextBox txtPur3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   26
      Top             =   4680
      Width           =   2055
   End
   Begin VB.TextBox txtPur2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   25
      Top             =   4200
      Width           =   2055
   End
   Begin VB.TextBox txtPur1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9840
      Locked          =   -1  'True
      MaxLength       =   32
      TabIndex        =   24
      Top             =   3720
      Width           =   2055
   End
   Begin VB.TextBox txtPurpose7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   23
      Top             =   6600
      Width           =   2415
   End
   Begin VB.TextBox txtPurpose6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   22
      Top             =   6120
      Width           =   2415
   End
   Begin VB.TextBox txtPurpose5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   21
      Top             =   5640
      Width           =   2415
   End
   Begin VB.TextBox txtPurpose4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   20
      Top             =   5160
      Width           =   2415
   End
   Begin VB.TextBox txtPurpose3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   19
      Top             =   4680
      Width           =   2415
   End
   Begin VB.TextBox txtPurpose2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   18
      Top             =   4200
      Width           =   2415
   End
   Begin VB.TextBox txtColor 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   5400
      MaxLength       =   10
      TabIndex        =   15
      Top             =   1440
      Width           =   1575
   End
   Begin VB.TextBox txtPurpose1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      MaxLength       =   32
      TabIndex        =   13
      Top             =   3720
      Width           =   2415
   End
   Begin VB.ListBox lstCurTime 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3630
      ItemData        =   "Calendar.frx":0004
      Left            =   6480
      List            =   "Calendar.frx":0006
      MultiSelect     =   1  'Simple
      TabIndex        =   9
      Top             =   3720
      Width           =   1095
   End
   Begin VB.ListBox lstPractice 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "Calendar.frx":0008
      Left            =   120
      List            =   "Calendar.frx":000A
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   600
      Width           =   3840
   End
   Begin VB.ListBox lstStartTime 
      Appearance      =   0  'Flat
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3630
      ItemData        =   "Calendar.frx":000C
      Left            =   120
      List            =   "Calendar.frx":000E
      MultiSelect     =   1  'Simple
      TabIndex        =   0
      Top             =   3720
      Width           =   1095
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   2
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Calendar.frx":0010
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2655
      Left            =   9000
      TabIndex        =   3
      Top             =   480
      Width           =   2775
      _Version        =   65537
      _ExtentX        =   4895
      _ExtentY        =   4683
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      BevelColorFace  =   14285823
      BevelColorShadow=   15794174
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10320
      TabIndex        =   4
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Calendar.frx":01EF
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   7080
      Top             =   1440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Flags           =   1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdForward 
      Height          =   990
      Left            =   8160
      TabIndex        =   33
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Calendar.frx":03CE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdVac 
      Height          =   990
      Left            =   2280
      TabIndex        =   51
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Calendar.frx":05B6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWeekly 
      Height          =   495
      Left            =   1440
      TabIndex        =   53
      Top             =   2520
      Width           =   2475
      _Version        =   131072
      _ExtentX        =   4366
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Calendar.frx":07A5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCat 
      Height          =   990
      Left            =   5280
      TabIndex        =   54
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Calendar.frx":098E
   End
   Begin VB.Label lblGblNote 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Global Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   4080
      TabIndex        =   59
      Top             =   1800
      Width           =   1020
   End
   Begin VB.Label lblDrNote 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Doctor Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   4080
      TabIndex        =   57
      Top             =   2400
      Width           =   1035
   End
   Begin VB.Label lblVacation 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Vacation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3960
      TabIndex        =   52
      Top             =   600
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Select Locations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   525
      Left            =   2760
      TabIndex        =   50
      Top             =   3120
      Width           =   1125
   End
   Begin VB.Label lblCLoc 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current Locations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   525
      Left            =   7680
      TabIndex        =   49
      Top             =   3120
      Width           =   1125
   End
   Begin VB.Label lblDisplay 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Display"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1440
      TabIndex        =   34
      Top             =   120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblHoliday 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Holiday"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3960
      TabIndex        =   32
      Top             =   960
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Reason for availability break"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   570
      Left            =   9840
      TabIndex        =   31
      Top             =   3120
      Width           =   2055
   End
   Begin VB.Label lblSample 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sample"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   7680
      TabIndex        =   17
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label lblColor 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Color"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   4560
      TabIndex        =   16
      Top             =   1440
      Width           =   645
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Note reason for availability break"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   570
      Left            =   3960
      TabIndex        =   14
      Top             =   3120
      Width           =   2415
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Please select a date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   9000
      TabIndex        =   12
      Top             =   120
      Width           =   2775
   End
   Begin VB.Label lblResource 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resource"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3840
      TabIndex        =   11
      Top             =   120
      Width           =   5055
   End
   Begin VB.Label lblCTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current Availability"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   525
      Left            =   6480
      TabIndex        =   10
      Top             =   3120
      Width           =   1125
   End
   Begin VB.Label lblTemplate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblSTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Select Availability Start/End Times"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   525
      Left            =   120
      TabIndex        =   7
      Top             =   3120
      Width           =   1965
   End
   Begin VB.Label lblDay 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sun"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   5280
      TabIndex        =   6
      Top             =   1080
      Width           =   3255
   End
   Begin VB.Label lblAvail 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Hours of Availability"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   5280
      TabIndex        =   5
      Top             =   600
      Width           =   3300
   End
End
Attribute VB_Name = "frmCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public CalendarId As Long
Public PracticeId As Long
Public ResourceId As Long
Public ResourceName As String
Public SelectedDate As String
Public BreakDown As Integer
Public ATemplate As String

Private GblNoteId As Long
Private GblNote As String
Private GblDbDate As String
Private AllowByPass As Boolean
Private TemplateType As String
Private ForwardDeleteOn As Boolean
Private DontWorry As Boolean
Private UpdateOnly As Boolean
Private ExitFast As Boolean
Private SetDate As String
Private ApplyAll As Boolean
Private CurrentCategory1 As String
Private CurrentCategory2 As String
Private CurrentCategory3 As String
Private CurrentCategory4 As String
Private CurrentCategory5 As String
Private CurrentCategory6 As String
Private CurrentCategory7 As String
Private CurrentCategory8 As String

Private Sub cmdBack_Click()
CurrentAction = "Home"
Unload frmCalendar
End Sub

Private Sub cmdCat_Click()
frmSetCategory.PCat1 = CurrentCategory1
frmSetCategory.PCat2 = CurrentCategory2
frmSetCategory.PCat3 = CurrentCategory3
frmSetCategory.PCat4 = CurrentCategory4
frmSetCategory.PCat5 = CurrentCategory5
frmSetCategory.PCat6 = CurrentCategory6
frmSetCategory.PCat7 = CurrentCategory7
frmSetCategory.PCat8 = CurrentCategory8
If (frmSetCategory.LoadCategory) Then
    frmSetCategory.Show 1
    If (frmSetCategory.ResultOn) Then
        AllowByPass = True
        CurrentCategory1 = frmSetCategory.PCat1
        CurrentCategory2 = frmSetCategory.PCat2
        CurrentCategory3 = frmSetCategory.PCat3
        CurrentCategory4 = frmSetCategory.PCat4
        CurrentCategory5 = frmSetCategory.PCat5
        CurrentCategory6 = frmSetCategory.PCat6
        CurrentCategory7 = frmSetCategory.PCat7
        CurrentCategory8 = frmSetCategory.PCat8
    End If
End If
End Sub

Private Sub cmdForward_Click()
Dim k As Integer
Dim LclDate As String
Dim DayName As String
Dim ApplCal As ApplicationCalendar
ForwardDeleteOn = False
If (Trim(SetDate) <> "") Then
    DayName = SetDate
    Call FormatTodaysDate(DayName, True)
    k = InStrPS(DayName, ",")
    If (k > 0) Then
        DayName = Trim(Left(DayName, k - 1))
    End If
    If (lstStartTime.SelCount < 2) Then
        frmEventMsgs.Header = "No Schedule Set Up"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SSMonth1.SetFocus
        Exit Sub
    End If
    frmEventMsgs.Header = "Apply schedule to all " + DayName + "s."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        ForwardDeleteOn = True
        ExitFast = False
        LclDate = ""
        Set ApplCal = New ApplicationCalendar
        Do Until Not (ApplCal.ApplIsDateSetable(SetDate))
            lblDisplay.Caption = "Applying " + SetDate
            lblDisplay.Visible = True
            DoEvents
            Call cmdApply_Click
            Call AdjustDate(SetDate, 7, LclDate)
            SetDate = LclDate
        Loop
        lblDisplay.Visible = False
        Unload frmCalendar
    Else
        ExitFast = True
        Call cmdApply_Click
    End If
Else
    frmEventMsgs.Header = "Need to set times"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    lstStartTime.ListIndex = -1
    lstStartTime.SetFocus
End If
End Sub

Private Sub cmdApply_Click()
Dim i As Integer
Dim ADate As String
Dim CurrentRecordActive As Boolean
Dim LocalDate As ManagedDate
Dim ApplCal As ApplicationCalendar
CurrentAction = ""
If (ATemplate = "S") Then
    Unload frmCalendar
    Exit Sub
End If
If (Trim(SetDate) = "") Then
    frmEventMsgs.Header = "Please Select a date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    SSMonth1.SetFocus
    Exit Sub
End If
' post global notes always
Call frmNotes.SetGlobalNote(val(Trim(txtGblNote.Tag)), GblDbDate, Trim(txtGblNote.Text))
If (CalendarId > 0) Then
    Set ApplCal = New ApplicationCalendar
    Call ApplCal.SetDoctorNote(CalendarId, Trim(txtDrNote.Text))
    Set ApplCal = Nothing
End If
If (lstStartTime.SelCount < 1) Then
    If (AllowByPass) Or ((Trim(txtDrNote.Text) <> "") And (CalendarId > 0)) Then
        txtPurpose1.Text = txtPur1.Text
        txtPurpose2.Text = txtPur2.Text
        txtPurpose3.Text = txtPur3.Text
        txtPurpose4.Text = txtPur4.Text
        txtPurpose5.Text = txtPur5.Text
        txtPurpose6.Text = txtPur6.Text
        txtPurpose7.Text = txtPur7.Text
        txtLocation1.Text = txtLoc1.Text
        txtLocation2.Text = txtLoc2.Text
        txtLocation3.Text = txtLoc3.Text
        txtLocation4.Text = txtLoc4.Text
        txtLocation5.Text = txtLoc5.Text
        txtLocation6.Text = txtLoc6.Text
        txtLocation7.Text = txtLoc7.Text
        For i = 1 To lstCurTime.ListCount - 1
            lstStartTime.Selected(i) = lstCurTime.Selected(i)
        Next i
    Else
        If (lblHoliday.Visible) Or (lblVacation.Visible) Or (CalendarId > 0) Then
            Unload frmCalendar
        Else
            frmEventMsgs.Header = "Schedule not setup"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            SSMonth1.SetFocus
        End If
        Exit Sub
    End If
End If
CurrentRecordActive = False
For i = 0 To lstCurTime.ListCount - 1
    If (lstCurTime.Selected(i)) Then
        CurrentRecordActive = True
        Exit For
    End If
Next i
ADate = ""
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = SetDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    ADate = LocalDate.ExposedDate
End If
Set LocalDate = Nothing
Set ApplCal = New ApplicationCalendar
If (lstStartTime.Selected(0)) Then
    If Not (ForwardDeleteOn) Then
        frmEventMsgs.Header = "Purge this Calendar Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmEventMsgs.Result = 2
    End If
    If (frmEventMsgs.Result = 2) Then
        ApplCal.ResourceId = ResourceId
        ApplCal.CalendarDate = ADate
        Call ApplCal.PurgeCalendarDate
        If Not (ForwardDeleteOn) Then
            ExitFast = True
        End If
    Else
        lstStartTime.SetFocus
        ExitFast = False
    End If
Else
    If (lstStartTime.SelCount < 2) Then
        ExitFast = True
        If (CurrentRecordActive) Then
            ApplCal.CalendarDate = ADate
            ApplCal.ResourceId = ResourceId
            ApplCal.DisplayColor = Trim(txtColor.Text)
            If (Trim(txtLocation1.Text) <> "") Then
                ApplCal.Location1 = val(Trim(Mid(txtLocation1.Text, 57, Len(txtLocation1.Text) - 56)))
                If (Left(txtLocation1.Text, 6) = "Office") Then
                    ApplCal.Location1 = 0
                End If
            ElseIf (Trim(txtLoc1.Text) <> "") Then
                ApplCal.Location1 = val(Trim(Mid(txtLoc1.Text, 57, Len(txtLoc1.Text) - 56)))
            Else
                ApplCal.Location1 = -1
            End If
            If (Trim(txtLocation2.Text) <> "") Then
                ApplCal.Location2 = val(Trim(Mid(txtLocation2.Text, 57, Len(txtLocation2.Text) - 56)))
                If (Left(txtLocation2.Text, 6) = "Office") Then
                    ApplCal.Location2 = 0
                End If
            ElseIf (Trim(txtLoc2.Text) <> "") Then
                ApplCal.Location2 = val(Trim(Mid(txtLoc2.Text, 57, Len(txtLoc2.Text) - 56)))
            Else
                ApplCal.Location2 = -1
            End If
            If (Trim(txtLocation3.Text) <> "") Then
                ApplCal.Location3 = val(Trim(Mid(txtLocation3.Text, 57, Len(txtLocation3.Text) - 56)))
                If (Left(txtLocation3.Text, 6) = "Office") Then
                    ApplCal.Location3 = 0
                End If
            ElseIf (Trim(txtLoc3.Text) <> "") Then
                ApplCal.Location3 = val(Trim(Mid(txtLoc3.Text, 57, Len(txtLoc3.Text) - 56)))
            Else
                ApplCal.Location3 = -1
            End If
            If (Trim(txtLocation4.Text) <> "") Then
                ApplCal.Location4 = val(Trim(Mid(txtLocation4.Text, 57, Len(txtLocation4.Text) - 56)))
                If (Left(txtLocation4.Text, 6) = "Office") Then
                    ApplCal.Location4 = 0
                End If
            ElseIf (Trim(txtLoc4.Text) <> "") Then
                ApplCal.Location4 = val(Trim(Mid(txtLoc4.Text, 57, Len(txtLoc4.Text) - 56)))
            Else
                ApplCal.Location4 = -1
            End If
            If (Trim(txtLocation5.Text) <> "") Then
                ApplCal.Location5 = val(Trim(Mid(txtLocation5.Text, 57, Len(txtLocation5.Text) - 56)))
                If (Left(txtLocation5.Text, 6) = "Office") Then
                    ApplCal.Location5 = 0
                End If
            ElseIf (Trim(txtLoc5.Text) <> "") Then
                ApplCal.Location5 = val(Trim(Mid(txtLoc5.Text, 57, Len(txtLoc5.Text) - 56)))
            Else
                ApplCal.Location5 = -1
            End If
            If (Trim(txtLocation6.Text) <> "") Then
                ApplCal.Location6 = val(Trim(Mid(txtLocation6.Text, 57, Len(txtLocation6.Text) - 56)))
                If (Left(txtLocation6.Text, 6) = "Office") Then
                    ApplCal.Location6 = 0
                End If
            ElseIf (Trim(txtLoc6.Text) <> "") Then
                ApplCal.Location6 = val(Trim(Mid(txtLoc6.Text, 57, Len(txtLoc6.Text) - 56)))
            Else
                ApplCal.Location6 = -1
            End If
            If (Trim(txtLocation7.Text) <> "") Then
                ApplCal.Location7 = val(Trim(Mid(txtLocation7.Text, 57, Len(txtLocation7.Text) - 56)))
                If (Left(txtLocation7.Text, 6) = "Office") Then
                    ApplCal.Location7 = 0
                End If
            ElseIf (Trim(txtLoc7.Text) <> "") Then
                ApplCal.Location7 = val(Trim(Mid(txtLoc7.Text, 57, Len(txtLoc7.Text) - 56)))
            Else
                ApplCal.Location7 = -1
            End If
            If (Trim(txtPurpose1.Text) <> "") Then
                ApplCal.Purpose1 = txtPurpose1.Text
            Else
                ApplCal.Purpose1 = txtPur1.Text
            End If
            If (Trim(txtPurpose2.Text) <> "") Then
                ApplCal.Purpose2 = txtPurpose2.Text
            Else
                ApplCal.Purpose2 = txtPur2.Text
            End If
            If (Trim(txtPurpose3.Text) <> "") Then
                ApplCal.Purpose3 = txtPurpose3.Text
            Else
                ApplCal.Purpose3 = txtPur3.Text
            End If
            If (Trim(txtPurpose4.Text) <> "") Then
                ApplCal.Purpose4 = txtPurpose4.Text
            Else
                ApplCal.Purpose4 = txtPur4.Text
            End If
            If (Trim(txtPurpose5.Text) <> "") Then
                ApplCal.Purpose5 = txtPurpose5.Text
            Else
                ApplCal.Purpose5 = txtPur5.Text
            End If
            If (Trim(txtPurpose6.Text) <> "") Then
                ApplCal.Purpose6 = txtPurpose6.Text
            Else
                ApplCal.Purpose6 = txtPur6.Text
            End If
            If (Trim(txtPurpose7.Text) <> "") Then
                ApplCal.Purpose7 = txtPurpose7.Text
            Else
                ApplCal.Purpose7 = txtPur7.Text
            End If
            ApplCal.Holiday = lblHoliday.Visible
            ApplCal.Vacation = lblVacation.Visible
            ApplCal.CalendarCategory1 = CurrentCategory1
            ApplCal.CalendarCategory2 = CurrentCategory2
            ApplCal.CalendarCategory3 = CurrentCategory3
            ApplCal.CalendarCategory4 = CurrentCategory4
            ApplCal.CalendarCategory5 = CurrentCategory5
            ApplCal.CalendarCategory6 = CurrentCategory6
            ApplCal.CalendarCategory7 = CurrentCategory7
            ApplCal.CalendarCategory8 = CurrentCategory8
            ApplCal.CalendarComment = Trim(txtDrNote.Text)
            Set ApplCal.lstStartTime = lstCurTime
            Call ApplCal.ApplPostCalendar
        ElseIf (Trim(txtDrNote.Text) <> "") Then
            ApplCal.CalendarDate = ADate
            ApplCal.ResourceId = ResourceId
            ApplCal.DisplayColor = Trim(txtColor.Text)
            If (Trim(txtLocation1.Text) <> "") Then
                ApplCal.Location1 = val(Trim(Mid(txtLocation1.Text, 57, Len(txtLocation1.Text) - 56)))
                If (Left(txtLocation1.Text, 6) = "Office") Then
                    ApplCal.Location1 = 0
                End If
            ElseIf (Trim(txtLoc1.Text) <> "") Then
                ApplCal.Location1 = val(Trim(Mid(txtLoc1.Text, 57, Len(txtLoc1.Text) - 56)))
            Else
                ApplCal.Location1 = -1
            End If
            If (Trim(txtLocation2.Text) <> "") Then
                ApplCal.Location2 = val(Trim(Mid(txtLocation2.Text, 57, Len(txtLocation2.Text) - 56)))
                If (Left(txtLocation2.Text, 6) = "Office") Then
                    ApplCal.Location2 = 0
                End If
            ElseIf (Trim(txtLoc2.Text) <> "") Then
                ApplCal.Location2 = val(Trim(Mid(txtLoc2.Text, 57, Len(txtLoc2.Text) - 56)))
            Else
                ApplCal.Location2 = -1
            End If
            If (Trim(txtLocation3.Text) <> "") Then
                ApplCal.Location3 = val(Trim(Mid(txtLocation3.Text, 57, Len(txtLocation3.Text) - 56)))
                If (Left(txtLocation3.Text, 6) = "Office") Then
                    ApplCal.Location3 = 0
                End If
            ElseIf (Trim(txtLoc3.Text) <> "") Then
                ApplCal.Location3 = val(Trim(Mid(txtLoc3.Text, 57, Len(txtLoc3.Text) - 56)))
            Else
                ApplCal.Location3 = -1
            End If
            If (Trim(txtLocation4.Text) <> "") Then
                ApplCal.Location4 = val(Trim(Mid(txtLocation4.Text, 57, Len(txtLocation4.Text) - 56)))
                If (Left(txtLocation4.Text, 6) = "Office") Then
                    ApplCal.Location4 = 0
                End If
            ElseIf (Trim(txtLoc4.Text) <> "") Then
                ApplCal.Location4 = val(Trim(Mid(txtLoc4.Text, 57, Len(txtLoc4.Text) - 56)))
            Else
                ApplCal.Location4 = -1
            End If
            If (Trim(txtLocation5.Text) <> "") Then
                ApplCal.Location5 = val(Trim(Mid(txtLocation5.Text, 57, Len(txtLocation5.Text) - 56)))
                If (Left(txtLocation5.Text, 6) = "Office") Then
                    ApplCal.Location5 = 0
                End If
            ElseIf (Trim(txtLoc5.Text) <> "") Then
                ApplCal.Location5 = val(Trim(Mid(txtLoc5.Text, 57, Len(txtLoc5.Text) - 56)))
            Else
                ApplCal.Location5 = -1
            End If
            If (Trim(txtLocation6.Text) <> "") Then
                ApplCal.Location6 = val(Trim(Mid(txtLocation6.Text, 57, Len(txtLocation6.Text) - 56)))
                If (Left(txtLocation6.Text, 6) = "Office") Then
                    ApplCal.Location6 = 0
                End If
            ElseIf (Trim(txtLoc6.Text) <> "") Then
                ApplCal.Location6 = val(Trim(Mid(txtLoc6.Text, 57, Len(txtLoc6.Text) - 56)))
            Else
                ApplCal.Location6 = -1
            End If
            If (Trim(txtLocation7.Text) <> "") Then
                ApplCal.Location7 = val(Trim(Mid(txtLocation7.Text, 57, Len(txtLocation7.Text) - 56)))
                If (Left(txtLocation7.Text, 6) = "Office") Then
                    ApplCal.Location7 = 0
                End If
            ElseIf (Trim(txtLoc7.Text) <> "") Then
                ApplCal.Location7 = val(Trim(Mid(txtLoc7.Text, 57, Len(txtLoc7.Text) - 56)))
            Else
                ApplCal.Location7 = -1
            End If
            If (Trim(txtPurpose1.Text) <> "") Then
                ApplCal.Purpose1 = txtPurpose1.Text
            Else
                ApplCal.Purpose1 = txtPur1.Text
            End If
            If (Trim(txtPurpose2.Text) <> "") Then
                ApplCal.Purpose2 = txtPurpose2.Text
            Else
                ApplCal.Purpose2 = txtPur2.Text
            End If
            If (Trim(txtPurpose3.Text) <> "") Then
                ApplCal.Purpose3 = txtPurpose3.Text
            Else
                ApplCal.Purpose3 = txtPur3.Text
            End If
            If (Trim(txtPurpose4.Text) <> "") Then
                ApplCal.Purpose4 = txtPurpose4.Text
            Else
                ApplCal.Purpose4 = txtPur4.Text
            End If
            If (Trim(txtPurpose5.Text) <> "") Then
                ApplCal.Purpose5 = txtPurpose5.Text
            Else
                ApplCal.Purpose5 = txtPur5.Text
            End If
            If (Trim(txtPurpose6.Text) <> "") Then
                ApplCal.Purpose6 = txtPurpose6.Text
            Else
                ApplCal.Purpose6 = txtPur6.Text
            End If
            If (Trim(txtPurpose7.Text) <> "") Then
                ApplCal.Purpose7 = txtPurpose7.Text
            Else
                ApplCal.Purpose7 = txtPur7.Text
            End If
            ApplCal.Holiday = lblHoliday.Visible
            ApplCal.Vacation = lblVacation.Visible
            ApplCal.CalendarCategory1 = CurrentCategory1
            ApplCal.CalendarCategory2 = CurrentCategory2
            ApplCal.CalendarCategory3 = CurrentCategory3
            ApplCal.CalendarCategory4 = CurrentCategory4
            ApplCal.CalendarCategory5 = CurrentCategory5
            ApplCal.CalendarCategory6 = CurrentCategory6
            ApplCal.CalendarCategory7 = CurrentCategory7
            ApplCal.CalendarCategory8 = CurrentCategory8
            ApplCal.CalendarComment = Trim(txtDrNote.Text)
            Set ApplCal.lstStartTime = lstCurTime
            Call ApplCal.ApplPostCalendar
        End If
    Else
        ApplCal.ResourceId = ResourceId
        ApplCal.CalendarDate = ADate
        ApplCal.Purpose1 = txtPurpose1.Text
        ApplCal.Purpose2 = txtPurpose2.Text
        ApplCal.Purpose3 = txtPurpose3.Text
        ApplCal.Purpose4 = txtPurpose4.Text
        ApplCal.Purpose5 = txtPurpose5.Text
        ApplCal.Purpose6 = txtPurpose6.Text
        ApplCal.Purpose7 = txtPurpose7.Text
        ApplCal.Holiday = lblHoliday.Visible
        ApplCal.Vacation = lblVacation.Visible
        ApplCal.DisplayColor = Trim(txtColor.Text)
        ApplCal.CalendarComment = Trim(txtDrNote.Text)
        If (Trim(txtLocation1.Text) <> "") Then
            ApplCal.Location1 = val(Trim(Mid(txtLocation1.Text, 57, Len(txtLocation1.Text) - 56)))
            If (Left(txtLocation1.Text, 6) = "Office") And (ApplCal.Location1 < 1000) Then
                ApplCal.Location1 = 0
            End If
        ElseIf (Trim(txtLoc1.Text) <> "") Then
            If (lstStartTime.SelCount > 0) Then
                ApplCal.Location1 = val(Trim(Mid(txtLoc1.Text, 57, Len(txtLoc1.Text) - 56)))
            Else
                ApplCal.Location1 = -1
            End If
        Else
            ApplCal.Location1 = -1
        End If
        If (Trim(txtLocation2.Text) <> "") Then
            ApplCal.Location2 = val(Trim(Mid(txtLocation2.Text, 57, Len(txtLocation2.Text) - 56)))
            If (Left(txtLocation2.Text, 6) = "Office") And (ApplCal.Location2 < 1000) Then
                ApplCal.Location2 = 0
            End If
        ElseIf (Trim(txtLoc2.Text) <> "") Then
            If (lstStartTime.SelCount > 2) Then
                ApplCal.Location2 = val(Trim(Mid(txtLoc2.Text, 57, Len(txtLoc2.Text) - 56)))
            Else
                ApplCal.Location2 = -1
            End If
        Else
            ApplCal.Location2 = -1
        End If
        If (Trim(txtLocation3.Text) <> "") Then
            ApplCal.Location3 = val(Trim(Mid(txtLocation3.Text, 57, Len(txtLocation3.Text) - 56)))
            If (Left(txtLocation3.Text, 6) = "Office") And (ApplCal.Location3 < 1000) Then
                ApplCal.Location3 = 0
            End If
        ElseIf (Trim(txtLoc3.Text) <> "") Then
            If (lstStartTime.SelCount > 4) Then
                ApplCal.Location3 = val(Trim(Mid(txtLoc3.Text, 57, Len(txtLoc3.Text) - 56)))
            Else
                ApplCal.Location3 = -1
            End If
        Else
            ApplCal.Location3 = -1
        End If
        If (Trim(txtLocation4.Text) <> "") Then
            ApplCal.Location4 = val(Trim(Mid(txtLocation4.Text, 57, Len(txtLocation4.Text) - 56)))
            If (Left(txtLocation4.Text, 6) = "Office") And (ApplCal.Location4 < 1000) Then
                ApplCal.Location4 = 0
            End If
        ElseIf (Trim(txtLoc4.Text) <> "") Then
            If (lstStartTime.SelCount > 6) Then
                ApplCal.Location4 = val(Trim(Mid(txtLoc4.Text, 57, Len(txtLoc4.Text) - 56)))
            Else
                ApplCal.Location4 = -1
            End If
        Else
            ApplCal.Location4 = -1
        End If
        If (Trim(txtLocation5.Text) <> "") Then
            ApplCal.Location5 = val(Trim(Mid(txtLocation5.Text, 57, Len(txtLocation5.Text) - 56)))
            If (Left(txtLocation5.Text, 6) = "Office") And (ApplCal.Location5 < 1000) Then
                ApplCal.Location5 = 0
            End If
        ElseIf (Trim(txtLoc5.Text) <> "") Then
            If (lstStartTime.SelCount > 8) Then
                ApplCal.Location5 = val(Trim(Mid(txtLoc5.Text, 57, Len(txtLoc5.Text) - 56)))
            Else
                ApplCal.Location5 = -1
            End If
        Else
            ApplCal.Location5 = -1
        End If
        If (Trim(txtLocation6.Text) <> "") Then
            ApplCal.Location6 = val(Trim(Mid(txtLocation6.Text, 57, Len(txtLocation6.Text) - 56)))
            If (Left(txtLocation6.Text, 6) = "Office") And (ApplCal.Location6 < 1000) Then
                ApplCal.Location6 = 0
            End If
        ElseIf (Trim(txtLoc6.Text) <> "") Then
            If (lstStartTime.SelCount > 10) Then
                ApplCal.Location6 = val(Trim(Mid(txtLoc6.Text, 57, Len(txtLoc6.Text) - 56)))
            Else
                ApplCal.Location6 = -1
            End If
        Else
            ApplCal.Location6 = -1
        End If
        If (Trim(txtLocation7.Text) <> "") Then
            ApplCal.Location7 = val(Trim(Mid(txtLocation7.Text, 57, Len(txtLocation7.Text) - 56)))
            If (Left(txtLocation7.Text, 6) = "Office") And (ApplCal.Location7 < 1000) Then
                ApplCal.Location7 = 0
            End If
        ElseIf (Trim(txtLoc7.Text) <> "") Then
            If (lstStartTime.SelCount > 12) Then
                ApplCal.Location7 = val(Trim(Mid(txtLoc7.Text, 57, Len(txtLoc7.Text) - 56)))
            Else
                ApplCal.Location7 = -1
            End If
        Else
            ApplCal.Location7 = -1
        End If
        ApplCal.CalendarCategory1 = CurrentCategory1
        ApplCal.CalendarCategory2 = CurrentCategory2
        ApplCal.CalendarCategory3 = CurrentCategory3
        ApplCal.CalendarCategory4 = CurrentCategory4
        ApplCal.CalendarCategory5 = CurrentCategory5
        ApplCal.CalendarCategory6 = CurrentCategory6
        ApplCal.CalendarCategory7 = CurrentCategory7
        ApplCal.CalendarCategory8 = CurrentCategory8
        Set ApplCal.lstStartTime = lstStartTime
        Call ApplCal.ApplPostCalendar
    End If
End If
If (ExitFast) Then
    Unload frmCalendar
End If
End Sub

Public Function LoadCalendarDisplay() As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim IAdtId As Long, OAdtId As Long
Dim GDate As String
Dim TheName As String, Temp As String
Dim Temp1 As String, Temp2 As String
Dim InTime As String, OutTime As String
Dim TempDate As String, DisplayText As String
Dim PrcAudit As Audit
Dim ApplTbl As ApplicationTables
Dim ApplCal As ApplicationCalendar
Dim ApplTemp As ApplicationTemplates
CalendarId = 0
LoadCalendarDisplay = True
DontWorry = False
AllowByPass = False
ExitFast = True
If (ATemplate = "S") Then
    LoadCalendarDisplay = LoadSuperDisplay
    Exit Function
End If
CurrentCategory1 = ""
CurrentCategory2 = ""
CurrentCategory3 = ""
CurrentCategory4 = ""
CurrentCategory5 = ""
CurrentCategory6 = ""
CurrentCategory7 = ""
CurrentCategory8 = ""
ForwardDeleteOn = False
CurrentAction = ""
If (BreakDown < 5) Then
    BreakDown = val(CheckConfigCollection("DURATIONBREAKDOWN", "0"))
    If (BreakDown < 5) Then
        BreakDown = 5
    End If
End If
lblResource.Caption = "Calendar for " + Trim(ResourceName)
Call LoadTime(lstStartTime, "", "", BreakDown)
For i = 0 To lstStartTime.ListCount - 1
    lstStartTime.Selected(i) = False
Next i
If (Trim(TemplateType) = "") Then
    TemplateType = "T"
End If
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
Call ApplTbl.ApplLoadPracticeList(TemplateType, True)
Set ApplCal = New ApplicationCalendar
Set ApplCal.lstStartTime = lstPractice
Call ApplCal.SetUpTemplates(TemplateType)
lstPractice.Visible = True
TempDate = SelectedDate
Call SetMinMaxDates(Temp1, Temp2)
SSMonth1.MinDate = Temp1
SSMonth1.MaxDate = Temp2
SSMonth1.x.SelectedDays.RemoveAll
SelectedDate = TempDate
If (Trim(SelectedDate) = "") Then
    Call FormatTodaysDate(SelectedDate, False)
End If
lblVacation.Visible = False
txtColor.Text = ""
txtLocation1.Text = ""
txtLocation2.Text = ""
txtLocation3.Text = ""
txtLocation4.Text = ""
txtLocation5.Text = ""
txtLocation6.Text = ""
txtLocation7.Text = ""
txtLoc1.Text = ""
txtLoc2.Text = ""
txtLoc3.Text = ""
txtLoc4.Text = ""
txtLoc5.Text = ""
txtLoc6.Text = ""
txtLoc7.Text = ""
txtPurpose1.Text = ""
txtPurpose2.Text = ""
txtPurpose3.Text = ""
txtPurpose4.Text = ""
txtPurpose5.Text = ""
txtPurpose6.Text = ""
txtPurpose7.Text = ""
txtPur1.Text = ""
txtPur2.Text = ""
txtPur3.Text = ""
txtPur4.Text = ""
txtPur5.Text = ""
txtPur6.Text = ""
txtPur7.Text = ""
txtDrNote.Text = ""
ApplCal.BreakDown = BreakDown
ApplCal.CalendarDate = SelectedDate
ApplCal.ResourceId = ResourceId
ApplCal.Location1 = -1
Set ApplCal.lstStartTime = lstCurTime
If (ApplCal.LoadCalendarDate) Then
    CalendarId = ApplCal.CalendarId
    lblVacation.Visible = ApplCal.Vacation
    lblHoliday.Visible = ApplCal.Holiday
    txtPur1.Text = ApplCal.Purpose1
    txtPur2.Text = ApplCal.Purpose2
    txtPur3.Text = ApplCal.Purpose3
    txtPur4.Text = ApplCal.Purpose4
    txtPur5.Text = ApplCal.Purpose5
    txtPur6.Text = ApplCal.Purpose6
    txtPur7.Text = ApplCal.Purpose7
    txtColor.Text = Trim(ApplCal.DisplayColor)
    txtDrNote.Text = Trim(ApplCal.CalendarComment)
    If (Trim(txtColor.Text) <> "") Then
        lblSample.BackColor = val(Trim(txtColor.Text))
    End If
    Set ApplTemp = New ApplicationTemplates
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location1, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location1))
        txtLoc1.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 0) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc1.Text = DisplayText
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location2, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location2))
        txtLoc2.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 2) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc2.Text = DisplayText
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location3, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location3))
        txtLoc3.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 4) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc3.Text = DisplayText
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location4, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location4))
        txtLoc4.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 6) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc4.Text = DisplayText
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location5, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location5))
        txtLoc5.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 8) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc5.Text = DisplayText
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location6, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location6))
        txtLoc6.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 10) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc6.Text = DisplayText
    End If
    DisplayText = Space(56)
    Call ApplTemp.ApplGetResourceName(0, ApplCal.Location7, TheName)
    If (Trim(TheName) <> "") Then
        Mid(DisplayText, 1, Len(Trim(TheName))) = Trim(TheName)
        DisplayText = DisplayText + Trim(Str(ApplCal.Location7))
        txtLoc7.Text = DisplayText
    ElseIf (lstCurTime.SelCount > 12) Then
        Mid(DisplayText, 1, 6) = "Office"
        DisplayText = DisplayText + Trim(Str(0))
        txtLoc7.Text = DisplayText
    End If
    CurrentCategory1 = ApplCal.CalendarCategory1
    CurrentCategory2 = ApplCal.CalendarCategory2
    CurrentCategory3 = ApplCal.CalendarCategory3
    CurrentCategory4 = ApplCal.CalendarCategory4
    CurrentCategory5 = ApplCal.CalendarCategory5
    CurrentCategory6 = ApplCal.CalendarCategory6
    CurrentCategory7 = ApplCal.CalendarCategory7
    CurrentCategory8 = ApplCal.CalendarCategory8
    Set ApplTemp = Nothing
    Set PrcAudit = New Audit
    Call PrcAudit.GetResourceHours(ResourceId, SelectedDate, InTime, OutTime, IAdtId, OAdtId, 1)
    Set PrcAudit = Nothing
End If
Set ApplCal = Nothing
SSMonth1.ShowCentury = False
SSMonth1.x.SelectedDays.Add SelectedDate
DontWorry = True
cmdCat.Visible = CheckConfigCollection("NEWCALENDAR") = "T"
If (lblVacation.Visible) Then
    cmdVac.Text = "UnSet Vacation Day"
End If
GblNote = ""
txtGblNote.Text = ""
GblDbDate = Mid(SelectedDate, 7, 4) + Mid(SelectedDate, 1, 2) + Mid(SelectedDate, 4, 2)
GblNoteId = frmNotes.GetGlobalNote(GblDbDate, GblNote)
txtGblNote.Text = GblNote
txtGblNote.Tag = Trim(Str(GblNoteId))
Exit Function
UIError_Label:
    LoadCalendarDisplay = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function LoadSuperDisplay() As Boolean
Dim i As Integer
Dim TheName As String
Dim Temp1 As String, Temp2 As String
Dim InTime As String, OutTime As String
Dim TempDate As String, DisplayText As String
Dim ApplTbl As ApplicationTables
LoadSuperDisplay = True
DontWorry = False
ExitFast = True
CurrentCategory1 = ""
CurrentCategory2 = ""
CurrentCategory3 = ""
CurrentCategory4 = ""
CurrentCategory5 = ""
CurrentCategory6 = ""
CurrentCategory7 = ""
CurrentCategory8 = ""
ForwardDeleteOn = False
CurrentAction = ""
lblResource.Caption = "Apply Super Templates"
lstStartTime.Visible = False
lstCurTime.Visible = False
TemplateType = "S"
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
Call ApplTbl.ApplLoadPracticeList(TemplateType, True)
Set ApplTbl = Nothing
lstPractice.Visible = True
TempDate = SelectedDate
Call SetMinMaxDates(Temp1, Temp2)
SSMonth1.MinDate = Temp1
SSMonth1.MaxDate = Temp2
SSMonth1.x.SelectedDays.RemoveAll
SelectedDate = TempDate
If (Trim(SelectedDate) = "") Then
    Call FormatTodaysDate(SelectedDate, False)
End If
lblSTime.Visible = False
lblCTime.Visible = False
lblCLoc.Visible = False
Label1.Visible = False
Label4.Visible = False
Label5.Visible = False
lblDay.Visible = False
lblAvail.Visible = False
lblColor.Visible = False
txtColor.Visible = False
lblSample.Visible = False
cmdWeekly.Visible = False
cmdVac.Visible = False
cmdCat.Visible = False
cmdForward.Visible = False
lblHoliday.Visible = False
lblVacation.Visible = False
txtLocation1.Visible = False
txtLocation2.Visible = False
txtLocation3.Visible = False
txtLocation4.Visible = False
txtLocation5.Visible = False
txtLocation6.Visible = False
txtLocation7.Visible = False
txtLoc1.Visible = False
txtLoc2.Visible = False
txtLoc3.Visible = False
txtLoc4.Visible = False
txtLoc5.Visible = False
txtLoc6.Visible = False
txtLoc7.Visible = False
txtPurpose1.Visible = False
txtPurpose2.Visible = False
txtPurpose3.Visible = False
txtPurpose4.Visible = False
txtPurpose5.Visible = False
txtPurpose6.Visible = False
txtPurpose7.Visible = False
txtPur1.Visible = False
txtPur2.Visible = False
txtPur3.Visible = False
txtPur4.Visible = False
txtPur5.Visible = False
txtPur6.Visible = False
txtPur7.Visible = False
SSMonth1.ShowCentury = False
SSMonth1.x.SelectedDays.Add SelectedDate
DontWorry = True
End Function

Private Sub cmdVac_Click()
Dim ApplSch As ApplicationScheduler
If (cmdVac.Text = "UnSet Vacation Day") Then
    frmEventMsgs.Header = "Un-Set Vacation ?"
Else
    frmEventMsgs.Header = "Set Vacation ?"
End If
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplSch = New ApplicationScheduler
    If (cmdVac.Text = "UnSet Vacation Day") Then
        Call ApplSch.UnsetResourceVacation(ResourceId, SetDate)
        lblVacation.Visible = False
    Else
        Call ApplSch.SetResourceVacation(ResourceId, SetDate)
        lblVacation.Visible = True
    End If
    Set ApplSch = Nothing
    DoEvents
    Unload frmCalendar
End If
End Sub

Private Sub cmdWeekly_Click()
Dim ApplTbl As ApplicationTables
If (TemplateType = "T") Or (InStrPS(lstPractice.List(0), "Daily") > 0) Then
    TemplateType = "W"
Else
    TemplateType = "T"
End If
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
Call ApplTbl.ApplLoadPracticeList(TemplateType, True)
Set ApplTbl = Nothing
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmCalendar.BorderStyle = 1
    frmCalendar.ClipControls = True
    frmCalendar.Caption = Mid(frmCalendar.Name, 4, Len(frmCalendar.Name) - 3)
    frmCalendar.AutoRedraw = True
    frmCalendar.Refresh
End If
End Sub

Private Sub lblHoliday_Click()
Dim ApplSch As ApplicationScheduler
frmEventMsgs.Header = "Unset Holiday ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplSch = New ApplicationScheduler
    Call ApplSch.UnsetResourceHoliday(ResourceId, SetDate)
    Set ApplSch = Nothing
    lblHoliday.Visible = False
    DoEvents
    AllowByPass = True
End If
End Sub

Private Sub lblVacation_Click()
Dim ApplSch As ApplicationScheduler
frmEventMsgs.Header = "Unset Vacation ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    Set ApplSch = New ApplicationScheduler
    Call ApplSch.UnsetResourceVacation(ResourceId, SetDate)
    Set ApplSch = Nothing
    lblVacation.Visible = False
    DoEvents
    AllowByPass = True
End If
End Sub

Private Sub lstPractice_Click()
Dim i As Integer, f As Integer
Dim IAdd As Integer
Dim UTemp As String
Dim AName As String, ZTemp As String
Dim ADate As String, BDate As String
Dim RetPrc As PracticeName
Dim RetPrc1 As PracticeName
Dim ApplTemp As ApplicationTemplates
Dim ApplCal As ApplicationCalendar
AName = Space(56)
Set ApplCal = New ApplicationCalendar
If (lstPractice.ListIndex > 0) Then
    PracticeId = val(Trim(Mid(lstPractice.List(lstPractice.ListIndex), 70, 5)))
Else
    PracticeId = 0
    If (frmTemplateNew.LoadTemplateDisplay(TemplateType)) Then
        frmTemplateNew.Show 1
        If (frmTemplateNew.PracticeId > 0) Then
            PracticeId = frmTemplateNew.PracticeId
        End If
    End If
End If
If (PracticeId > 0) Then
    If (TemplateType = "T") Then
        For i = 0 To lstStartTime.ListCount - 1
            lstStartTime.Selected(i) = False
        Next i
        txtLocation1.Text = ""
        txtLocation2.Text = ""
        txtLocation3.Text = ""
        txtLocation4.Text = ""
        txtLocation5.Text = ""
        txtLocation6.Text = ""
        txtLocation7.Text = ""
        txtPurpose1.Text = ""
        txtPurpose2.Text = ""
        txtPurpose3.Text = ""
        txtPurpose4.Text = ""
        txtPurpose5.Text = ""
        txtPurpose6.Text = ""
        txtPurpose7.Text = ""
        Set ApplCal.lstStartTime = lstStartTime
        Call ApplCal.LoadTemplate(PracticeId)
        txtColor.Text = ApplCal.DisplayColor
        Set ApplTemp = New ApplicationTemplates
        If (ApplCal.TemplateLocation >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.TemplateLocation, AName)
            If (Len(AName) < 56) Then
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation1.Text = AName + Trim(Str(ApplCal.TemplateLocation))
        End If
        If (ApplCal.Location1 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location1, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation1.Text = AName + Trim(Str(ApplCal.Location1))
        End If
        If (ApplCal.Location2 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location2, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation2.Text = AName + Trim(Str(ApplCal.Location2))
        End If
        If (ApplCal.Location3 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location3, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation3.Text = AName + Trim(Str(ApplCal.Location3))
        End If
        If (ApplCal.Location4 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location4, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation4.Text = AName + Trim(Str(ApplCal.Location4))
        End If
        If (ApplCal.Location5 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location5, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation5.Text = AName + Trim(Str(ApplCal.Location5))
        End If
        If (ApplCal.Location6 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location6, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation6.Text = AName + Trim(Str(ApplCal.Location6))
        End If
        If (ApplCal.Location7 >= 0) Then
            Call ApplTemp.ApplGetResourceName(0, ApplCal.Location7, AName)
            If (Len(AName) < 56) Then
                If (Trim(AName) = "") Then
                    AName = "OFFICE"
                End If
                AName = AName + Space(56 - Len(AName))
            End If
            txtLocation7.Text = AName + Trim(Str(ApplCal.Location7))
        End If
        Set ApplTemp = Nothing
        Set ApplCal.lstStartTime = lstPractice
        CurrentCategory1 = ApplCal.CalendarCategory1
        CurrentCategory2 = ApplCal.CalendarCategory2
        CurrentCategory3 = ApplCal.CalendarCategory3
        CurrentCategory4 = ApplCal.CalendarCategory4
        CurrentCategory5 = ApplCal.CalendarCategory5
        CurrentCategory6 = ApplCal.CalendarCategory6
        CurrentCategory7 = ApplCal.CalendarCategory7
        CurrentCategory8 = ApplCal.CalendarCategory8
        Call ApplCal.SetUpTemplates(TemplateType)
    ElseIf (TemplateType = "W") Then
        ADate = SSMonth1.Date
        Call FormatTodaysDate(ADate, False)
        Call GetWeekStart(ADate, BDate)
        Set RetPrc = New PracticeName
        RetPrc.PracticeId = PracticeId
        If (RetPrc.RetrievePracticeName) Then
            ZTemp = RetPrc.PracticeStartTime1
            GoSub PostWeekly
            ZTemp = RetPrc.PracticeEndTime1
            GoSub PostWeekly
            ZTemp = RetPrc.PracticeStartTime2
            GoSub PostWeekly
            ZTemp = RetPrc.PracticeEndTime2
            GoSub PostWeekly
            ZTemp = RetPrc.PracticeStartTime3
            GoSub PostWeekly
            ZTemp = RetPrc.PracticeEndTime3
            GoSub PostWeekly
            ZTemp = RetPrc.PracticeStartTime4
            GoSub PostWeekly
        End If
        Set RetPrc = Nothing
        Call SSMonth1_SelChange(SSMonth1.Date, "", True, False)
    ElseIf (TemplateType = "S") Then
        If (ApplySuperTemplate(PracticeId)) Then
            frmEventMsgs.Header = "Post of Super Template"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Completed"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            lblDay.Visible = False
        End If
    End If
End If
Set ApplCal = Nothing
Exit Sub
PostWeekly:
    lstTemp.Clear
    If (Trim(ZTemp) <> "") Then
        IAdd = Asc(Left(ZTemp, 1)) - 65
        If (IAdd > 0) Then
            Call AdjustDate(BDate, IAdd, ADate)
        Else
            ADate = BDate
        End If
        Set RetPrc1 = New PracticeName
        RetPrc1.PracticeId = val(Trim(Mid(ZTemp, 2, Len(ZTemp) - 1)))
        If (RetPrc1.RetrievePracticeName) Then
            ApplCal.ResourceId = ResourceId
            ApplCal.CalendarDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
            ApplCal.Purpose1 = ""
            ApplCal.Purpose2 = ""
            ApplCal.Purpose3 = ""
            ApplCal.Purpose4 = ""
            ApplCal.Purpose5 = ""
            ApplCal.Purpose6 = ""
            ApplCal.Purpose7 = ""
            ApplCal.Holiday = False
            ApplCal.Vacation = False
            ApplCal.DisplayColor = ""
            If (Trim(Mid(RetPrc1.PracticeCity, 1, 4)) <> "") Then
                ApplCal.Location1 = val(Trim(Mid(RetPrc1.PracticeCity, 1, 4)))
            Else
                ApplCal.Location1 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 5, 4)) <> "") Then
                ApplCal.Location2 = val(Trim(Mid(RetPrc1.PracticeCity, 5, 4)))
            Else
                ApplCal.Location2 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 9, 4)) <> "") Then
                ApplCal.Location3 = val(Trim(Mid(RetPrc1.PracticeCity, 9, 4)))
            Else
                ApplCal.Location3 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 13, 4)) <> "") Then
                ApplCal.Location4 = val(Trim(Mid(RetPrc1.PracticeCity, 13, 4)))
            Else
                ApplCal.Location4 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 17, 4)) <> "") Then
                ApplCal.Location5 = val(Trim(Mid(RetPrc1.PracticeCity, 17, 4)))
            Else
                ApplCal.Location5 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 21, 4)) <> "") Then
                ApplCal.Location6 = val(Trim(Mid(RetPrc1.PracticeCity, 21, 4)))
            Else
                ApplCal.Location6 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 25, 4)) <> "") Then
                ApplCal.Location7 = val(Trim(Mid(RetPrc1.PracticeCity, 25, 4)))
            Else
                ApplCal.Location7 = -1
            End If
            If (ApplCal.Location1 < 0) Then
                If (Trim(RetPrc1.PracticeEmail) <> "") Then
                    ApplCal.Location1 = val(Trim(RetPrc1.PracticeEmail))
                End If
            End If
            ApplCal.CalendarCategory1 = RetPrc1.PracticeCat1
            ApplCal.CalendarCategory2 = RetPrc1.PracticeCat2
            ApplCal.CalendarCategory3 = RetPrc1.PracticeCat3
            ApplCal.CalendarCategory4 = RetPrc1.PracticeCat4
            ApplCal.CalendarCategory5 = RetPrc1.PracticeCat5
            ApplCal.CalendarCategory6 = RetPrc1.PracticeCat6
            ApplCal.CalendarCategory7 = RetPrc1.PracticeCat7
            ApplCal.CalendarCategory8 = RetPrc1.PracticeCat8
            Call LoadTime(lstTemp, "12:00 AM", "11:59 PM", 15)
            If (Trim(RetPrc1.PracticeStartTime1) <> "") Then
                UTemp = RetPrc1.PracticeStartTime1
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime1) <> "") Then
                UTemp = RetPrc1.PracticeEndTime1
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime2) <> "") Then
                UTemp = RetPrc1.PracticeStartTime2
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime2) <> "") Then
                UTemp = RetPrc1.PracticeEndTime2
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime3) <> "") Then
                UTemp = RetPrc1.PracticeStartTime3
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime3) <> "") Then
                UTemp = RetPrc1.PracticeEndTime3
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime4) <> "") Then
                UTemp = RetPrc1.PracticeStartTime4
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime4) <> "") Then
                UTemp = RetPrc1.PracticeEndTime4
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime5) <> "") Then
                UTemp = RetPrc1.PracticeStartTime5
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime5) <> "") Then
                UTemp = RetPrc1.PracticeEndTime5
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime6) <> "") Then
                UTemp = RetPrc1.PracticeStartTime6
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime6) <> "") Then
                UTemp = RetPrc1.PracticeEndTime6
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime7) <> "") Then
                UTemp = RetPrc1.PracticeStartTime7
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime7) <> "") Then
                UTemp = RetPrc1.PracticeEndTime7
                GoSub SetTArray
            End If
            Set ApplCal.lstStartTime = lstTemp
            Call ApplCal.ApplPostCalendar
        End If
        Set RetPrc1 = Nothing
    End If
    Return
SetTArray:
    If (Trim(UTemp) <> "") Then
        For f = 1 To lstTemp.ListCount - 1
            If (Left(lstTemp.List(f), 8) = UTemp) Then
                lstTemp.Selected(f) = True
                Exit For
            End If
        Next f
    End If
    Return
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
Dim TmpDate As String
Dim OrgDate As String
Dim TheDate As String
Dim ApplCal As ApplicationCalendar
OrgDate = OldSelDate
TheDate = SelDate
SetDate = SelDate
Call FormatTodaysDate(TheDate, True)
Call FormatTodaysDate(SetDate, False)
lblDay.Caption = TheDate
If (Trim(SetDate) <> "") And (Trim(OrgDate) <> "") Then
    If (lstStartTime.SelCount > 0) Or (AllowByPass) Then
        frmEventMsgs.Header = "Apply changes ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            ExitFast = False
            Call FormatTodaysDate(OrgDate, False)
            TmpDate = SetDate
            SetDate = OrgDate
            Call cmdApply_Click
            SetDate = TmpDate
            ExitFast = True
        End If
    End If
End If
SelectedDate = SetDate
If (DontWorry) Then
    If (ATemplate <> "S") Then
        Call LoadCalendarDisplay
    End If
End If
End Sub

Private Sub txtColor_Click()
CommonDialog1.CancelError = True
On Error GoTo UI_ErrorHandler
CommonDialog1.Flags = &H1&
CommonDialog1.ShowColor
txtColor.Text = Trim(Str(CommonDialog1.Color))
If (Trim(txtColor.Text) <> "") Then
    lblSample.BackColor = val(Trim(txtColor.Text))
End If
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub txtColor_KeyPress(KeyAscii As Integer)
Call txtColor_Click
KeyAscii = 0
End Sub

Private Sub txtLocation1_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation1.Text = Temp
End If
End Sub

Private Sub txtLocation1_KeyPress(KeyAscii As Integer)
Call txtLocation1_Click
KeyAscii = 0
End Sub

Private Sub txtLocation2_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation2.Text = Temp
End If
End Sub

Private Sub txtLocation2_KeyPress(KeyAscii As Integer)
Call txtLocation2_Click
KeyAscii = 0
End Sub

Private Sub txtLocation3_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation3.Text = Temp
End If
End Sub

Private Sub txtLocation3_KeyPress(KeyAscii As Integer)
Call txtLocation3_Click
KeyAscii = 0
End Sub

Private Sub txtLocation4_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation4.Text = Temp
End If
End Sub

Private Sub txtLocation4_KeyPress(KeyAscii As Integer)
Call txtLocation4_Click
KeyAscii = 0
End Sub

Private Sub txtLocation5_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation5.Text = Temp
End If
End Sub

Private Sub txtLocation5_KeyPress(KeyAscii As Integer)
Call txtLocation5_Click
KeyAscii = 0
End Sub

Private Sub txtLocation6_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation6.Text = Temp
End If
End Sub

Private Sub txtLocation6_KeyPress(KeyAscii As Integer)
Call txtLocation6_Click
KeyAscii = 0
End Sub

Private Sub txtLocation7_Click()
Dim Temp As String
Call SetLocation(Temp)
If (Trim(Temp) <> "") Then
    txtLocation7.Text = Temp
End If
End Sub

Private Sub txtLocation7_KeyPress(KeyAscii As Integer)
Call txtLocation7_Click
KeyAscii = 0
End Sub

Private Sub SetLocation(ATemp As String)
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    ATemp = UCase(frmSelectDialogue.Selection)
End If
End Sub

Public Function IsTimeActive(ATime As String) As Boolean
Dim i As Integer
Dim ITime As Long
Dim STime As Long
Dim ETime As Long
IsTimeActive = False
If (Trim(ATime) <> "") Then
    ITime = ConvertTimeToMinutes(ATime)
    If (lstStartTime.SelCount > 0) Then
        STime = -1
        ETime = -1
        For i = 1 To lstStartTime.ListCount - 1
            If (lstStartTime.Selected(i)) And (STime < 0) Then
                STime = ConvertTimeToMinutes(lstStartTime.List(i))
            ElseIf (lstStartTime.Selected(i)) And (ETime < 0) Then
                ETime = ConvertTimeToMinutes(lstStartTime.List(i))
                If (ITime >= STime) And (ITime <= ETime) Then
                    IsTimeActive = True
                End If
                STime = -1
                ETime = -1
            End If
        Next i
    Else
        STime = -1
        ETime = -1
        For i = 1 To lstCurTime.ListCount - 1
            If (lstCurTime.Selected(i)) And (STime < 0) Then
                STime = ConvertTimeToMinutes(lstCurTime.List(i))
            ElseIf (lstCurTime.Selected(i)) And (ETime < 0) Then
                ETime = ConvertTimeToMinutes(lstCurTime.List(i))
                If (ITime >= STime) And (ITime <= ETime) Then
                    IsTimeActive = True
                End If
                STime = -1
                ETime = -1
            End If
        Next i
    End If
End If
End Function

Private Function ApplySuperTemplate(PrcId As Long) As Boolean
Dim i As Integer, f As Integer
Dim IAdd As Integer
Dim RscId As Long, WPrcId As Long
Dim UTemp As String
Dim AName As String, ZTemp As String
Dim ADate As String, BDate As String
Dim RetPrc As PracticeName
Dim RetPrc1 As PracticeName
Dim RetPrc2 As PracticeName
Dim ApplCal As ApplicationCalendar
Dim ApplTemp As ApplicationTemplates
ApplySuperTemplate = False
If (PrcId < 1) Then
    If (frmTemplateNew.LoadTemplateDisplay("S")) Then
        frmTemplateNew.Show 1
        If (frmTemplateNew.PracticeId > 0) Then
            PrcId = frmTemplateNew.PracticeId
        End If
    End If
End If
If (PrcId > 0) Then
    frmEventMsgs.Header = "Apply Template ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Apply"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        ApplySuperTemplate = True
        Set RetPrc2 = New PracticeName
        RetPrc2.PracticeId = PrcId
        If (RetPrc2.RetrievePracticeName) Then
            If (Trim(RetPrc2.PracticeCat1) <> "") Then
                If (Trim(Mid(RetPrc2.PracticeCat1, 1, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 1, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime1))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 5, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 5, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime1))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 9, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 9, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime2))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 13, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 13, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime2))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 17, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 17, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime3))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 21, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 21, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime3))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 25, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 25, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime4))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 29, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 29, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime4))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 33, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 33, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime5))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 37, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 37, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime5))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 41, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 41, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime6))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 45, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 45, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime6))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 39, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 49, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeStartTime7))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
                If (Trim(Mid(RetPrc2.PracticeCat1, 53, 4)) <> "") Then
                    ADate = SSMonth1.Date
                    Call FormatTodaysDate(ADate, False)
                    Call GetWeekStart(ADate, BDate)
                    RscId = val(Trim(Mid(RetPrc2.PracticeCat1, 53, 4)))
                    WPrcId = val(Trim(RetPrc2.PracticeEndTime7))
                    If (RscId > 0) And (WPrcId > 0) Then
                        GoSub GetWeekly
                    End If
                End If
            End If
        End If
        Set RetPrc2 = Nothing
    End If
End If
Exit Function
GetWeekly:
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetResourceName(0, RscId, AName)
    lblDay.Caption = AName
    lblDay.Visible = True
    DoEvents
    Set ApplCal = New ApplicationCalendar
    Set RetPrc = New PracticeName
    RetPrc.PracticeId = WPrcId
    If (RetPrc.RetrievePracticeName) Then
        ZTemp = RetPrc.PracticeStartTime1
        GoSub PostWeekly
        ZTemp = RetPrc.PracticeEndTime1
        GoSub PostWeekly
        ZTemp = RetPrc.PracticeStartTime2
        GoSub PostWeekly
        ZTemp = RetPrc.PracticeEndTime2
        GoSub PostWeekly
        ZTemp = RetPrc.PracticeStartTime3
        GoSub PostWeekly
        ZTemp = RetPrc.PracticeEndTime3
        GoSub PostWeekly
        ZTemp = RetPrc.PracticeStartTime4
        GoSub PostWeekly
    End If
    Set RetPrc = Nothing
    Set ApplCal = Nothing
    Set ApplTemp = Nothing
    Return
PostWeekly:
    lstTemp.Clear
    If (Trim(ZTemp) <> "") Then
        IAdd = Asc(Left(ZTemp, 1)) - 65
        If (IAdd > 0) Then
            Call AdjustDate(BDate, IAdd, ADate)
        Else
            ADate = BDate
        End If
        Set RetPrc1 = New PracticeName
        RetPrc1.PracticeId = val(Trim(Mid(ZTemp, 2, Len(ZTemp) - 1)))
        If (RetPrc1.RetrievePracticeName) Then
            ApplCal.ResourceId = RscId
            ApplCal.CalendarDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
            ApplCal.Purpose1 = ""
            ApplCal.Purpose2 = ""
            ApplCal.Purpose3 = ""
            ApplCal.Purpose4 = ""
            ApplCal.Purpose5 = ""
            ApplCal.Purpose6 = ""
            ApplCal.Purpose7 = ""
            ApplCal.Holiday = False
            ApplCal.Vacation = False
            ApplCal.DisplayColor = ""
            If (Trim(Mid(RetPrc1.PracticeCity, 1, 4)) <> "") Then
                ApplCal.Location1 = val(Trim(Mid(RetPrc1.PracticeCity, 1, 4)))
            Else
                ApplCal.Location1 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 5, 4)) <> "") Then
                ApplCal.Location2 = val(Trim(Mid(RetPrc1.PracticeCity, 5, 4)))
            Else
                ApplCal.Location2 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 9, 4)) <> "") Then
                ApplCal.Location3 = val(Trim(Mid(RetPrc1.PracticeCity, 9, 4)))
            Else
                ApplCal.Location3 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 13, 4)) <> "") Then
                ApplCal.Location4 = val(Trim(Mid(RetPrc1.PracticeCity, 13, 4)))
            Else
                ApplCal.Location4 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 17, 4)) <> "") Then
                ApplCal.Location5 = val(Trim(Mid(RetPrc1.PracticeCity, 17, 4)))
            Else
                ApplCal.Location5 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 21, 4)) <> "") Then
                ApplCal.Location6 = val(Trim(Mid(RetPrc1.PracticeCity, 21, 4)))
            Else
                ApplCal.Location6 = -1
            End If
            If (Trim(Mid(RetPrc1.PracticeCity, 25, 4)) <> "") Then
                ApplCal.Location7 = val(Trim(Mid(RetPrc1.PracticeCity, 25, 4)))
            Else
                ApplCal.Location7 = -1
            End If
            If (ApplCal.Location1 < 0) Then
                If (Trim(RetPrc1.PracticeEmail) <> "") Then
                    ApplCal.Location1 = val(Trim(RetPrc1.PracticeEmail))
                End If
            End If
            ApplCal.CalendarCategory1 = RetPrc1.PracticeCat1
            ApplCal.CalendarCategory2 = RetPrc1.PracticeCat2
            ApplCal.CalendarCategory3 = RetPrc1.PracticeCat3
            ApplCal.CalendarCategory4 = RetPrc1.PracticeCat4
            ApplCal.CalendarCategory5 = RetPrc1.PracticeCat5
            ApplCal.CalendarCategory6 = RetPrc1.PracticeCat6
            ApplCal.CalendarCategory7 = RetPrc1.PracticeCat7
            ApplCal.CalendarCategory8 = RetPrc1.PracticeCat8
            Call LoadTime(lstTemp, "12:00 AM", "11:59 PM", 15)
            If (Trim(RetPrc1.PracticeStartTime1) <> "") Then
                UTemp = RetPrc1.PracticeStartTime1
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime1) <> "") Then
                UTemp = RetPrc1.PracticeEndTime1
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime2) <> "") Then
                UTemp = RetPrc1.PracticeStartTime2
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime2) <> "") Then
                UTemp = RetPrc1.PracticeEndTime2
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime3) <> "") Then
                UTemp = RetPrc1.PracticeStartTime3
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime3) <> "") Then
                UTemp = RetPrc1.PracticeEndTime3
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime4) <> "") Then
                UTemp = RetPrc1.PracticeStartTime4
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime4) <> "") Then
                UTemp = RetPrc1.PracticeEndTime4
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime5) <> "") Then
                UTemp = RetPrc1.PracticeStartTime5
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime5) <> "") Then
                UTemp = RetPrc1.PracticeEndTime5
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime6) <> "") Then
                UTemp = RetPrc1.PracticeStartTime6
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime6) <> "") Then
                UTemp = RetPrc1.PracticeEndTime6
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeStartTime7) <> "") Then
                UTemp = RetPrc1.PracticeStartTime7
                GoSub SetTArray
            End If
            If (Trim(RetPrc1.PracticeEndTime7) <> "") Then
                UTemp = RetPrc1.PracticeEndTime7
                GoSub SetTArray
            End If
            Set ApplCal.lstStartTime = lstTemp
            Call ApplCal.ApplPostCalendar
        End If
        Set RetPrc1 = Nothing
    End If
    Return
SetTArray:
    If (Trim(UTemp) <> "") Then
        For f = 1 To lstTemp.ListCount - 1
            If (Left(lstTemp.List(f), 8) = UTemp) Then
                lstTemp.Selected(f) = True
                Exit For
            End If
        Next f
    End If
    Return
End Function
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
