VERSION 5.00
Begin VB.Form frmSelectDialogue 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Select"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10185
   ClipControls    =   0   'False
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "SelectDialogue.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   10185
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdInfo 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Pharm. details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   1
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   8160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.ListBox lstDet 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2220
      Index           =   1
      ItemData        =   "SelectDialogue.frx":305A
      Left            =   4920
      List            =   "SelectDialogue.frx":3061
      TabIndex        =   22
      Top             =   6600
      Visible         =   0   'False
      Width           =   3360
   End
   Begin VB.ListBox lstDet 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5460
      Index           =   0
      ItemData        =   "SelectDialogue.frx":306D
      Left            =   4920
      List            =   "SelectDialogue.frx":3074
      TabIndex        =   21
      Top             =   720
      Visible         =   0   'False
      Width           =   3360
   End
   Begin VB.CommandButton cmdInfo 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Pharm. details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   0
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton cmdPharm 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Done"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   2
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   5520
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton cmdPharm 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Remove"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   1
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   7440
      Width           =   1335
   End
   Begin VB.CommandButton cmdPharm 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Set Primary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   0
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   6720
      Width           =   1335
   End
   Begin VB.ListBox lstPharm 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2220
      ItemData        =   "SelectDialogue.frx":3080
      Left            =   240
      List            =   "SelectDialogue.frx":3087
      TabIndex        =   15
      Top             =   6600
      Width           =   8295
   End
   Begin VB.Frame frAddress 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   690
      Left            =   2760
      TabIndex        =   10
      Top             =   720
      Visible         =   0   'False
      Width           =   5775
      Begin VB.ComboBox cmbState 
         Height          =   315
         ItemData        =   "SelectDialogue.frx":3095
         Left            =   5160
         List            =   "SelectDialogue.frx":314D
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox txt 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   0
         MaxLength       =   64
         TabIndex        =   1
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox txt 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   3120
         MaxLength       =   64
         TabIndex        =   4
         Top             =   360
         Width           =   1935
      End
      Begin VB.TextBox txt 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   2160
         MaxLength       =   5
         TabIndex        =   2
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   0
         TabIndex        =   14
         Top             =   120
         Width           =   525
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "City"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   3120
         TabIndex        =   13
         Top             =   120
         Width           =   360
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "State"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   5160
         TabIndex        =   12
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "ZIP"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   2160
         TabIndex        =   11
         Top             =   120
         Width           =   270
      End
   End
   Begin VB.TextBox txtFilter 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4080
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   4455
   End
   Begin VB.ListBox lstSelection 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5460
      ItemData        =   "SelectDialogue.frx":323F
      Left            =   240
      List            =   "SelectDialogue.frx":3241
      TabIndex        =   8
      Top             =   720
      Width           =   8295
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   8640
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   4800
      Width           =   1335
   End
   Begin VB.CommandButton cmdSelect 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Select"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label lbl 
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient's pharmacies"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   6360
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Label lblInsurer 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   2760
      TabIndex        =   9
      Top             =   360
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   240
      TabIndex        =   7
      Top             =   360
      Width           =   615
   End
End
Attribute VB_Name = "frmSelectDialogue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public myPass As String
Public SelectionResult As Boolean
Public Selection As String
Public InsurerSelected As String
Public PatientId As Long
Private InsurerOn As Boolean
Private DisplayLabel As String

Private Sub cmbState_KeyPress(KeyAscii As Integer)
Call txt_KeyPress(3, KeyAscii)
End Sub

Private Sub cmdCancel_Click()
If (Not InsurerOn) Then
    If (InStrPS(Label1.Caption, "PLAN") <> 0) Then
        InsurerSelected = ""
        InsurerOn = False
        Call BuildSelectionDialogue("PLAN")
    ElseIf (InStrPS(Label1.Caption, "INSURER") = 0) Or (InsurerSelected = "") Then
        InsurerOn = False
        InsurerSelected = ""
        Selection = ""
        SelectionResult = False
        Unload frmSelectDialogue
    ElseIf (InsurerSelected <> "") Then
        InsurerSelected = ""
        InsurerOn = False
        Call BuildSelectionDialogue("PLAN")
    End If
Else
    If (InsurerSelected <> "") Then
        InsurerSelected = ""
        InsurerOn = False
        Call BuildSelectionDialogue("PLAN")
    Else
        InsurerOn = False
        InsurerSelected = ""
        Selection = ""
        SelectionResult = False
        Unload frmSelectDialogue
    End If
End If
End Sub


Private Sub cmdSelect_Click()
If (Not InsurerOn) Then
    If (lstSelection.ListIndex >= 0) Then
        If (lstSelection.ListIndex = 0) And (Trim(lstSelection.List(0)) = "Select Insurer") Then
            Call cmdCancel_Click
            Exit Sub
        End If
        InsurerOn = False
        Selection = lstSelection.List(lstSelection.ListIndex)
        SelectionResult = True
        Unload frmSelectDialogue
    Else
        Exit Sub
    End If
Else
    InsurerSelected = ""
    If (lstSelection.ListIndex > 0) Or ((lstSelection.ListIndex = 0) And (Left(lstSelection.List(0), 6) <> "Create")) Then
        txtFilter.Text = ""
        txtFilter.Visible = False
        lblInsurer.Visible = False
        InsurerSelected = Trim(Left(lstSelection.List(lstSelection.ListIndex), 56))
    ElseIf (lstSelection.ListIndex = 0) Then
        Selection = lstSelection.List(lstSelection.ListIndex)
        SelectionResult = True
        Unload frmSelectDialogue
    Else
        Exit Sub
    End If
    InsurerOn = False
End If
End Sub

Public Sub BuildSelectionDialogue(DialogueType As String)
Dim k As Integer
Dim q As Integer
Dim Total As Integer
Dim ViewOnly As Boolean
Dim ReverseOn As Boolean
Dim RetrieveCodes As PracticeCodes

On Error GoTo ErrorHandler

Me.Height = 6525
lbl.Visible = False
frAddress.Visible = False
ViewOnly = False
InsurerOn = False
If (Trim(InsurerSelected) <> "") Then
    InsurerOn = True
End If
ReverseOn = False
Selection = ""
SelectionResult = False
DisplayLabel = DialogueType
txtFilter.Text = ""
txtFilter.Visible = False
lblInsurer.Visible = False
lstSelection.Clear
If (myPass <> "") Then
    txtFilter.Text = myPass
    myPass = ""
Else
    txtFilter.Text = ""
End If
frmSelectDialogue.Label1.Caption = "Select " + DialogueType
lstSelection.FontSize = 12
If (DialogueType = "PCP") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Last Name"
    txtFilter.Visible = True
    frmSelectDialogue.Label1.Caption = "Select Doctor"
ElseIf (DialogueType = "PARTY") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Last Name"
    txtFilter.Visible = True
    frmSelectDialogue.Label1.Caption = "Select Party"
ElseIf (DialogueType = "VENDOR") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Vendor Name"
    txtFilter.Visible = True
    frmSelectDialogue.Label1.Caption = "Select Doctor"
    Call LoadPCP("Vendor")
ElseIf (DialogueType = "CONTACTS") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Contact"
    txtFilter.Visible = True
ElseIf (DialogueType = "RESOURCEWITHAFF") Then
    frmSelectDialogue.Label1.Caption = "Select Doctor"
    LoadResAff val(Trim(InsurerSelected))
    InsurerSelected = ""
    InsurerOn = False
ElseIf (DialogueType = "QUEUETO") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Last Name"
    txtFilter.Visible = True
ElseIf (DialogueType = "HOSPITALS") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Hospital"
    txtFilter.Visible = True
ElseIf (DialogueType = "LABS") Then
    lblInsurer.Visible = True
    lblInsurer.Caption = "Lab Name"
    txtFilter.Visible = True
ElseIf (DialogueType = "PLAN") Then
    If (InsurerSelected = "") Then
        InsurerOn = True
        InsurerSelected = ""
        lblInsurer.Visible = True
        lblInsurer.Caption = "Insurer Name"
        txtFilter.Visible = True
        frmSelectDialogue.Label1.Caption = "Select INSURER"
    Else
        InsurerOn = False
    End If
ElseIf (DialogueType = "INSURERONLY") Then
    InsurerOn = False
    InsurerSelected = ""
    lblInsurer.Visible = True
    lblInsurer.Caption = "Insurer Name"
    txtFilter.Visible = True
    frmSelectDialogue.Label1.Caption = "Select INSURER"
ElseIf (DialogueType = "SERVICE") Then
    Call LoadServices("ServiceWithAmt")
ElseIf (DialogueType = "SERVICEWITHAMT") Then
    Call LoadServices("ServiceWithAmt")
ElseIf (DialogueType = "DIAGNOSIS") Then
    Call LoadServices("Diagnosis")
ElseIf (DialogueType = "AnyDoctor") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "BillingDoctor") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "ScheduledDoctor") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "AppointmentType") Then
    LoadApptType
ElseIf (DialogueType = "ScheduledRoom") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "ScheduledStaff") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "ScheduledStaffing") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "ScheduledLocation") Then
    Call LoadDoctor(DialogueType)
ElseIf (DialogueType = "ScheduledPractice") Then
    Call LoadPractice
ElseIf (DialogueType = "REPORTS") Then
    Call LoadReports
ElseIf (DialogueType = "ADJUSTMENTSONLY") Then
    Call LoadAdjustments
Else
    If (DialogueType = "PRIVATE") Then
        frmSelectDialogue.Label1.Caption = "Select " + DialogueType + " Notes"
        lstSelection.FontSize = 18
    ElseIf (DialogueType = "CHART") Then
        frmSelectDialogue.Label1.Caption = "Select " + DialogueType + " Notes"
        lstSelection.FontSize = 18
    ElseIf (DialogueType = "ServiceMods") Then
        ReverseOn = True
    ElseIf (DialogueType = "TOS") Then
        frmSelectDialogue.Label1.Caption = "Select " + DialogueType + " Table"
        lstSelection.AddItem "Create TOS Table"
    ElseIf (DialogueType = "POS") Then
        frmSelectDialogue.Label1.Caption = "Select " + DialogueType + " Table"
        lstSelection.AddItem "Create POS Table"
    ElseIf (DialogueType = "CLINICALTEMPLATES") Then
        frmSelectDialogue.Label1.Caption = "Select " + DialogueType + " Table"
        lstSelection.AddItem "Create Clinical Template"
    ElseIf (DialogueType = "SurgicalRequests") Then
        frmSelectDialogue.Label1.Caption = "Select " + DialogueType + " Table"
        lstSelection.AddItem "Create Surgical Request"
    ElseIf (DialogueType = "LanguageA") Then
        lstSelection.AddItem "Any"
        DialogueType = "Language"
    End If
    q = InStrPS(UCase(Trim(DialogueType)), "VIEWONLY")
    If (q > 0) Then
        DialogueType = Left(DialogueType, q - 1)
        ViewOnly = True
    End If
    Set RetrieveCodes = New PracticeCodes
    RetrieveCodes.ReferenceType = UCase(Trim(DialogueType))
    lstSelection.ListIndex = -1
    If (ReverseOn) Then
        Total = RetrieveCodes.FindCodeReverse
    Else
        Total = RetrieveCodes.FindCode
    End If
    If (Total > 0) Then
        k = 1
        While (RetrieveCodes.SelectCode(k))
            If (ViewOnly) Then
                If (InStrPS(RetrieveCodes.ReferenceCode, "- P") = 0) Then
                    lstSelection.AddItem RetrieveCodes.ReferenceCode
                End If
            Else
                If DialogueType = "CLINICALTEMPLATES" Then
                    If RetrieveCodes.ReferenceAlternateCode = "" Then
                        lstSelection.AddItem "  " & Format$(Trim$(Str$(RetrieveCodes.Rank)), "@@@") & " - " & RetrieveCodes.ReferenceCode
                    Else
                        lstSelection.AddItem Trim$(RetrieveCodes.ReferenceAlternateCode) & " " & Format$(Trim$(Str$(RetrieveCodes.Rank)), "@@@") & " - " & RetrieveCodes.ReferenceCode
                    End If
                Else
                    lstSelection.AddItem RetrieveCodes.ReferenceCode
                End If
            End If
            k = k + 1
        Wend
    End If
    Set RetrieveCodes = Nothing
End If
If (txtFilter.Visible) Then
    txtFilter.SetFocus
End If
If (ViewOnly) Then
    cmdSelect.Visible = False
End If
Exit Sub
ErrorHandler:
    Set RetrieveCodes = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Private Function LoadAdjustments() As Boolean
Dim k As Integer
Dim u As Integer
Dim Total As Integer
Dim DisplayText As String
Dim RetrieveCodes As PracticeCodes
Set RetrieveCodes = New PracticeCodes
RetrieveCodes.ReferenceType = "PAYMENTTYPE"
lstSelection.ListIndex = -1
Total = RetrieveCodes.FindCode
If (Total > 0) Then
    k = 1
    While (RetrieveCodes.SelectCode(k))
        If (InStrPS(RetrieveCodes.ReferenceCode, "- P") = 0) And _
           (InStrPS(RetrieveCodes.ReferenceCode, "- D") = 0) And _
           (InStrPS(RetrieveCodes.ReferenceCode, "- @") = 0) Then
            DisplayText = Space(50)
            u = InStrPS(RetrieveCodes.ReferenceCode, "-")
            If (u > 0) Then
                If (u < 40) Then
                    Mid(DisplayText, 1, u - 1) = Left(RetrieveCodes.ReferenceCode, u - 1)
                    Mid(DisplayText, 40, 1) = Mid(RetrieveCodes.ReferenceCode, u + 2, 1)
                Else
                    Mid(DisplayText, 1, 40) = Left(RetrieveCodes.ReferenceCode, 39) + Mid(RetrieveCodes.ReferenceCode, u + 2, 1)
                End If
                lstSelection.AddItem DisplayText
            End If
        End If
        k = k + 1
    Wend
End If
Set RetrieveCodes = Nothing
End Function

Public Sub RetrieveSelectionDialogue(DialogueType As String)
On Error GoTo ErrorHandler
Dim RetrieveCodes As PracticeCodes
InsurerOn = False
SelectionResult = False
Set RetrieveCodes = New PracticeCodes
RetrieveCodes.ReferenceType = UCase(Trim(DialogueType))
RetrieveCodes.ReferenceCode = Selection
If (RetrieveCodes.FindCode > 0) Then
    If (RetrieveCodes.SelectCode(1)) Then
        Selection = RetrieveCodes.ReferenceCode
        SelectionResult = True
    End If
End If
Set RetrieveCodes = Nothing
Exit Sub
ErrorHandler:
    Set RetrieveCodes = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub LoadApptType()
Dim k As Integer
Dim DisplayText As String
Dim TheDocs As SchedulerAppointmentType
Set TheDocs = New SchedulerAppointmentType
TheDocs.AppointmentType = Chr(1)
If (TheDocs.FindAppointmentType > 0) Then
    k = 1
    While (TheDocs.SelectAppointmentType(k))
        DisplayText = Space(56)
        Mid(DisplayText, 1, Len(TheDocs.AppointmentType)) = TheDocs.AppointmentType
        DisplayText = DisplayText + Trim(Str(TheDocs.AppointmentTypeId))
        lstSelection.AddItem DisplayText
        k = k + 1
    Wend
End If
Set TheDocs = Nothing
End Sub

Private Sub LoadDoctor(DisplayType As String)
Dim k As Integer
Dim Tip As Boolean
Dim DisplayText As String
Dim TheDocs As SchedulerResource
Dim RetPrc As PracticeName
Set TheDocs = New SchedulerResource
Tip = False
If (DisplayType = "ScheduledDoctor") Then
    TheDocs.ResourceType = "DQ"
    TheDocs.ResourceServiceCode = ""
ElseIf (DisplayType = "AnyDoctor") Then
    TheDocs.ResourceType = "DBQ"
    TheDocs.ResourceServiceCode = ""
ElseIf (DisplayType = "ScheduledStaff") Then
    TheDocs.ResourceType = "S"
    TheDocs.ResourceServiceCode = ""
ElseIf (DisplayType = "ScheduledStaffing") Then
    TheDocs.ResourceType = "s"
    TheDocs.ResourceServiceCode = ""
ElseIf (DisplayType = "ScheduledRoom") Then
    TheDocs.ResourceType = "R"
    TheDocs.ResourceServiceCode = ""
ElseIf (DisplayType = "ScheduledLocation") Then
    TheDocs.ResourceType = "R"
    TheDocs.ResourceServiceCode = Chr(1)
    Tip = True
End If
If (Tip) Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        k = 1
        While (RetPrc.SelectPractice(k))
            DisplayText = Space(56)
            Mid(DisplayText, 1, 6) = "Office"
            If (Trim(RetPrc.PracticeLocationReference) <> "") Then
                Mid(DisplayText, 7, Len(Trim(RetPrc.PracticeLocationReference)) + 1) = "-" + Trim(RetPrc.PracticeLocationReference)
                DisplayText = DisplayText + Trim(Str(1000 + RetPrc.PracticeId))
            Else
                DisplayText = DisplayText + Trim(Str(0))
            End If
            lstSelection.AddItem DisplayText
            k = k + 1
        Wend
    End If
    Set RetPrc = Nothing
End If
TheDocs.ResourceName = Chr(1)
If (TheDocs.FindResource > 0) Then
    k = 1
    While (TheDocs.SelectResource(k))
        DisplayText = Space(56)
        If (DisplayType = "AnyDoctor") Then
            Mid(DisplayText, 1, Len(Trim(TheDocs.ResourceName))) = Trim(TheDocs.ResourceName)
            Mid(DisplayText, 17, Len(Trim(TheDocs.ResourceDescription))) = Trim(TheDocs.ResourceDescription)
        ElseIf (DisplayType = "ScheduledDoctor") Then
            Mid(DisplayText, 1, Len(Trim(TheDocs.ResourceName))) = Trim(TheDocs.ResourceName)
            Mid(DisplayText, 17, Len(Trim(TheDocs.ResourceDescription))) = Trim(TheDocs.ResourceDescription)
        Else
            Mid(DisplayText, 1, Len(Trim(TheDocs.ResourceDescription))) = Trim(TheDocs.ResourceDescription)
        End If
        If (Trim(DisplayText) <> "") Then
            If (Not Tip) Then
                DisplayText = DisplayText + Trim(Str(TheDocs.ResourceId))
                lstSelection.AddItem DisplayText
            ElseIf (TheDocs.ResourceServiceCode = "02") Then
                DisplayText = DisplayText + Trim(Str(TheDocs.ResourceId))
                lstSelection.AddItem DisplayText
            End If
        End If
        k = k + 1
    Wend
End If
Set TheDocs = Nothing
End Sub

Private Sub LoadResAff(AffIns As Long)
Dim q As Integer
Dim k As Integer
Dim TheInsName As String
Dim DisplayText As String
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim TheAffs As PracticeAffiliations
Dim TheDocs As SchedulerResource
Set TheAffs = New PracticeAffiliations
Set TheDocs = New SchedulerResource
Set RetPatientFinancialService = New PatientFinancialService
TheInsName = ""
RetPatientFinancialService.InsurerId = AffIns
Set RS = RetPatientFinancialService.RetrieveInsurer
If (Not RS Is Nothing And RS.RecordCount > 0) Then
    TheInsName = Trim(RS("Name"))
End If
TheDocs.ResourceType = "D"
TheDocs.ResourceServiceCode = ""
TheDocs.ResourceName = Chr(1)
If (TheDocs.FindResource > 0) Then
    k = 1
    While (TheDocs.SelectResource(k))
        DisplayText = Space(56)
        Mid(DisplayText, 1, Len(TheDocs.ResourceDescription)) = TheDocs.ResourceDescription
        If (AffIns > 0) Then
            TheAffs.AffiliationResourceType = "R"
            TheAffs.AffiliationResourceId = TheDocs.ResourceId
            TheAffs.AffiliationPlanId = 0
            TheAffs.AffiliationPin = ""
            If (TheAffs.FindAffiliation > 0) Then
                q = 1
                Do Until Not (TheAffs.SelectAffiliation(q))
                    Set RetPatientFinancialService = New PatientFinancialService
                    RetPatientFinancialService.InsurerId = TheAffs.AffiliationPlanId
                    Set RS = RetPatientFinancialService.RetrieveInsurer
                    If (Not RS Is Nothing And RS.RecordCount > 0) Then
                        If (TheInsName = Trim(RS("Name"))) Then
                            Mid(DisplayText, 30, Len(Trim(TheAffs.AffiliationPin))) = Trim(TheAffs.AffiliationPin)
                            Exit Do
                        End If
                    End If
                    q = q + 1
                Loop
            End If
        End If
        DisplayText = DisplayText + Trim(Str(TheDocs.ResourceId))
        lstSelection.AddItem DisplayText
        k = k + 1
    Wend
End If
Set RetPatientFinancialService = Nothing
Set TheAffs = Nothing
Set TheDocs = Nothing
End Sub

Private Sub LoadPractice()
Dim k As Integer
Dim DisplayText As String
Dim TheDocs As PracticeName
Set TheDocs = New PracticeName
TheDocs.PracticeType = "P"
TheDocs.PracticeName = Chr(1)
If (TheDocs.FindPractice > 0) Then
    k = 1
    While (TheDocs.SelectPractice(k))
        DisplayText = Space(56)
        Mid(DisplayText, 1, Len(TheDocs.PracticeName)) = TheDocs.PracticeName
        DisplayText = DisplayText + Trim(Str(TheDocs.PracticeId))
        lstSelection.AddItem DisplayText
        k = k + 1
    Wend
End If
Set TheDocs = Nothing
End Sub

Private Sub LoadReports()
Dim k As Integer
Dim DisplayText As String
Dim TheDocs As PracticeReports
Set TheDocs = New PracticeReports
lstSelection.AddItem "Create Report"
TheDocs.ReportName = Chr(1)
If (TheDocs.FindReport > 0) Then
    k = 1
    While (TheDocs.SelectReport(k))
        DisplayText = Space(56)
        Mid(DisplayText, 1, Len(TheDocs.ReportName)) = TheDocs.ReportName
        DisplayText = DisplayText + "1234"
        lstSelection.AddItem DisplayText
        k = k + 1
    Wend
End If
Set TheDocs = Nothing
End Sub

Private Sub LoadPCP(DisplayType As String)
Dim k As Integer
Dim TotalDocs As Integer
Dim DisplayText As String
Dim TheDocs As PracticeVendors
Set TheDocs = New PracticeVendors
TotalDocs = 0
lstSelection.Clear
If (UCase(DisplayType) = "PCP") Then
    TheDocs.VendorType = "D"
    TheDocs.VendorLastName = ""
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorLastName = Trim(txtFilter.Text)
    End If
    TheDocs.VendorFirstName = ""
    lstSelection.AddItem "Create Doctor"
    TotalDocs = TheDocs.FindVendorbyLastName
ElseIf (UCase(DisplayType) = "QUEUETO") Then
    TheDocs.VendorType = "D"
    TheDocs.VendorLastName = ""
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorLastName = Trim(txtFilter.Text)
    End If
    TheDocs.VendorFirstName = ""
    If (UCase(DisplayType) = "QUEUETO") Then
        lstSelection.AddItem "Create Queue To Doctor"
    Else
        lstSelection.AddItem "Create Referred From Doctor"
    End If
    TotalDocs = TheDocs.FindVendorbyLastName
ElseIf (UCase(DisplayType) = "VENDOR") Then
    TheDocs.VendorType = "V"
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorName = Trim(txtFilter.Text)
    End If
    lstSelection.AddItem "Create Vendor"
    TotalDocs = TheDocs.FindVendor
ElseIf (UCase(DisplayType) = "HOSPITALS") Then
    TheDocs.VendorType = "H"
    TheDocs.VendorName = ""
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorName = Trim(txtFilter.Text)
    End If
    TheDocs.VendorLastName = ""
    TheDocs.VendorFirstName = ""
    lstSelection.AddItem "Create Hospital"
    TotalDocs = TheDocs.FindVendor
ElseIf (UCase(DisplayType) = "LABS") Then
    TheDocs.VendorType = "L"
    TheDocs.VendorName = ""
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorName = Trim(txtFilter.Text)
    End If
    TheDocs.VendorLastName = ""
    TheDocs.VendorFirstName = ""
    lstSelection.AddItem "Create Lab"
    TotalDocs = TheDocs.FindVendor
ElseIf (UCase(DisplayType) = "PARTY") Then
    TheDocs.VendorLastName = ""
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorLastName = Trim(txtFilter.Text)
    End If
    TheDocs.VendorFirstName = ""
    TheDocs.VendorType = "D"
    lstSelection.AddItem "Create Party"
    TotalDocs = TheDocs.FindVendorbyLastName
ElseIf (UCase(DisplayType) = "COMPANY") Then
    TheDocs.VendorType = "~"
    TheDocs.VendorName = Chr(1)
    TheDocs.VendorLastName = Chr(1)
    If (Trim(txtFilter.Text) <> "") Then
        TheDocs.VendorName = Trim(txtFilter.Text)
        TheDocs.VendorLastName = Trim(txtFilter.Text)
    End If
    lstSelection.AddItem "Create Contacts"
    TotalDocs = TheDocs.FindVendor
Else
    TheDocs.VendorName = Chr(1)
    TheDocs.VendorType = ""
    lstSelection.AddItem "Create " + DisplayType
    TotalDocs = TheDocs.FindVendor
End If
If (TotalDocs > 0) Then
    k = 1
    While (TheDocs.SelectVendor(k))
        DisplayText = Space(56)
        If (Trim(TheDocs.VendorLastName) = "") Then
            Mid(DisplayText, 1, 18) = Left(TheDocs.VendorName, 18)
            Mid(DisplayText, 21, 19) = Left(TheDocs.VendorAddress, 19)
            Mid(DisplayText, 41, 14) = Left(TheDocs.VendorCity, 14)
        Else
            Mid(DisplayText, 1, 13) = Left(TheDocs.VendorLastName, 13)
            Mid(DisplayText, 15, 5) = Left(TheDocs.VendorFirstName, 5)
            Mid(DisplayText, 21, 19) = Left(TheDocs.VendorAddress, 19)
            Mid(DisplayText, 41, 14) = Left(TheDocs.VendorCity, 14)
        End If
        DisplayText = DisplayText + Trim(Str(TheDocs.VendorId))
        If (Trim(txtFilter.Text) <> "") Then
            If (Trim(UCase(txtFilter.Text)) = Trim(UCase(Left(TheDocs.VendorLastName, Len(Trim(txtFilter.Text)))))) Then
                lstSelection.AddItem DisplayText
            Else
                If (Trim(txtFilter.Text) < Trim(Left(TheDocs.VendorLastName, Len(Trim(txtFilter.Text))))) Then
                    Set TheDocs = Nothing
                    Exit Sub
                End If
            End If
        Else
            lstSelection.AddItem DisplayText
        End If
        k = k + 1
    Wend
End If
Set TheDocs = Nothing
End Sub
Private Sub LoadServices(DisplayType As String)
Dim k As Integer
Dim Tot As Integer
Dim Temp As String
Dim DisplayText As String
Dim TheDocs As Service
Set TheDocs = New Service
TheDocs.Service = Chr(1)
If (DisplayType = "Service") Or (DisplayType = "ServiceWithAmt") Then
    TheDocs.ServiceType = ""
    Tot = TheDocs.FindService
Else
    TheDocs.ServiceType = "D"
    Tot = TheDocs.FindDiagnosis
End If
lstSelection.AddItem "Create " + DisplayType
If (Tot > 0) Then
    k = 1
    While (TheDocs.SelectService(k))
        DisplayText = Space(56)
        Mid(DisplayText, 1, Len(TheDocs.Service)) = TheDocs.Service
        If (UCase(DisplayType) = "SERVICEWITHAMT") Then
            Call DisplayDollarAmount(Trim(Str(TheDocs.ServiceFee)), Temp)
            Mid(DisplayText, 8, 8) = Trim(Temp)
        End If
        If (Len(Trim(TheDocs.ServiceDescription)) > 38) Then
            Mid(DisplayText, 17, 38) = Left(Trim(TheDocs.ServiceDescription), 38)
        Else
            Mid(DisplayText, 17, Len(Trim(TheDocs.ServiceDescription))) = Trim(TheDocs.ServiceDescription)
        End If
        lstSelection.AddItem DisplayText
        k = k + 1
    Wend
End If
Set TheDocs = Nothing
End Sub


Private Sub Form_Load()
frAddress.Top = 0
End Sub

Private Sub lstSelection_DblClick()
On Error GoTo UI_ErrorHandler
Call cmdSelect_Click
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub


Private Sub txt_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case KeyAscii
Case 13, 10
    If Not txt(1).Text = "" Then
        txt(1).Text = Format(txt(1).Text, "00000")
    End If
Case vbKeyBack
Case Else
    If Index = 1 Then
        If Not IsNumeric(Chr(KeyAscii)) Then
            KeyAscii = 0
        End If
    End If
End Select

End Sub

Private Sub txtFilter_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Or (KeyAscii = Asc(vbTab)) Then
    txtFilter.Text = UCase(txtFilter.Text)
    If (lblInsurer.Caption = "Vendor Name") Or (lblInsurer.Caption = "Last Name") Then
        Call LoadPCP(DisplayLabel)
    ElseIf (lblInsurer.Caption = "Hospital") Then
        Call LoadPCP(DisplayLabel)
    ElseIf (lblInsurer.Caption = "Lab Name") Then
        Call LoadPCP(DisplayLabel)
    ElseIf (lblInsurer.Caption = "Contact") Then
        Call LoadPCP("COMPANY")
    End If
End If
End Sub
Public Function FrmClose()
 Call cmdCancel_Click
 Unload Me
End Function
