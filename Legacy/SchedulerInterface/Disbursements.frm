VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmDisbursements 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "View Disbursements"
   ClientHeight    =   8550
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11940
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8550
   ScaleWidth      =   11940
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstDisbursements 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6810
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   11685
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10800
      TabIndex        =   0
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disbursements.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   240
      TabIndex        =   1
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Disbursements.frx":01DF
   End
   Begin VB.Label Label2 
      BackColor       =   &H0077742D&
      Caption         =   "Disbursements "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   0
      Width           =   2925
   End
End
Attribute VB_Name = "frmDisbursements"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdDone_Click()
Unload frmDisbursements
End Sub

Private Sub cmdHome_Click()
Unload frmDisbursements
End Sub
Public Sub LoadDisbursements(ByVal PaymentAmount As String, ByVal PaymentCheck As String, ByVal EOBDate As String, ByVal PayerType As String)
Dim DisplayItem As String
Dim ADate As String
lstDisbursements.Clear
Dim fEOBDate As String
Dim i As Integer
If IsDate(EOBDate) Then
    fEOBDate = Format(EOBDate, "yyyymmdd")
Else
    fEOBDate = EOBDate
End If
Dim ApplTemp As New ApplicationTemplates
Dim RS As Recordset
Set RS = ApplTemp.GetDisburmentDetails(PaymentAmount, PaymentCheck, fEOBDate, PayerType)
If RS Is Nothing Then
 Exit Sub
End If
While Not RS.EOF
    DisplayItem = Space(100)
    Mid(DisplayItem, 1, Len(RS("PaymentId"))) = RS("PatientId")
    Mid(DisplayItem, 10, Len(RS("LastName"))) = RS("LastName")
    Mid(DisplayItem, 25, Len(RS("FirstName"))) = RS("FirstName")
    Mid(DisplayItem, 40, Len(RS("PaymentDate"))) = RS("PaymentDate")
    If PayerType = "Insurer" Then
        Mid(DisplayItem, 50, Len(RS("InsurerName"))) = RS("InsurerName")
    Else
        Mid(DisplayItem, 50, Len(RS("InsurerName"))) = ""
    End If
    Mid(DisplayItem, 60, Len(RS("PaymentType"))) = RS("PaymentType")
    Mid(DisplayItem, 70, Len(RS("PaymentAmount"))) = RS("PaymentAmount")
    lstDisbursements.AddItem DisplayItem
    RS.MoveNext
Wend
For i = 0 To lstDisbursements.ListCount - 1
    If PayerType = "Insurer" Then
        frmLineItem.lstDisb.AddItem lstDisbursements.List(i)
    Else
        frmPLineItem5.lstDisb.AddItem lstDisbursements.List(i)
    End If
Next
End Sub

