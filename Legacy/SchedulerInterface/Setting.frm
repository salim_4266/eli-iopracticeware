VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmSetting 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8790
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   5325
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8790
   ScaleWidth      =   5325
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fr1 
      BackColor       =   &H00FFFF80&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   6615
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Width           =   4935
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsOrphaned  "
         Height          =   255
         Index           =   26
         Left            =   720
         TabIndex        =   31
         Tag             =   "BillingTransactionOrphaned"
         Top             =   6240
         Width           =   3855
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Billing"
         Height          =   255
         Index           =   16
         Left            =   480
         TabIndex        =   28
         Tag             =   "Billing"
         Top             =   3840
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "ClaimTotal"
         Height          =   255
         Index           =   5
         Left            =   720
         TabIndex        =   27
         Tag             =   "ClaimTotal"
         Top             =   1200
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "NonInsuranceAdjustments"
         Height          =   255
         Index           =   14
         Left            =   720
         TabIndex        =   26
         Tag             =   "NonInsuranceAdjustments"
         Top             =   3360
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "InfoTransactions"
         Height          =   255
         Index           =   15
         Left            =   720
         TabIndex        =   25
         Tag             =   "InfoTransactions"
         Top             =   3600
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "PatientPayments"
         Height          =   255
         Index           =   12
         Left            =   720
         TabIndex        =   24
         Tag             =   "PatientPayments"
         Top             =   2880
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "InsuranceAdjustments"
         Height          =   255
         Index           =   13
         Left            =   720
         TabIndex        =   23
         Tag             =   "InsuranceAdjustments"
         Top             =   3120
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "InsurancePayments"
         Height          =   255
         Index           =   11
         Left            =   720
         TabIndex        =   22
         Tag             =   "InsurancePayments"
         Top             =   2640
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsMonthlyStatement"
         Height          =   255
         Index           =   25
         Left            =   720
         TabIndex        =   21
         Tag             =   "BillingTransactionsMonthlyStatement"
         Top             =   6000
         Width           =   3855
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsPatientStatementSent"
         Height          =   255
         Index           =   23
         Left            =   720
         TabIndex        =   20
         Tag             =   "BillingTransactionsPatientStatementSent"
         Top             =   5520
         Width           =   3495
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsPatientStatementClosed"
         Height          =   255
         Index           =   24
         Left            =   720
         TabIndex        =   19
         Tag             =   "BillingTransactionsPatientStatementClosed"
         Top             =   5760
         Width           =   3495
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsAppealClosed"
         Height          =   255
         Index           =   21
         Left            =   720
         TabIndex        =   18
         Tag             =   "BillingTransactionsAppealClosed"
         Top             =   5040
         Width           =   3495
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsPatientStatementQueued"
         Height          =   255
         Index           =   22
         Left            =   720
         TabIndex        =   17
         Tag             =   "BillingTransactionsPatientStatementQueued"
         Top             =   5280
         Width           =   3495
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsAppeal"
         Height          =   255
         Index           =   20
         Left            =   720
         TabIndex        =   16
         Tag             =   "BillingTransactionsAppeal"
         Top             =   4800
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsClaimsSent"
         Height          =   255
         Index           =   18
         Left            =   720
         TabIndex        =   15
         Tag             =   "BillingTransactionsClaimsSent"
         Top             =   4320
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Payments and Adjustments"
         Height          =   255
         Index           =   10
         Left            =   480
         TabIndex        =   14
         Tag             =   "Payments"
         Top             =   2400
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsClaimsClosed"
         Height          =   255
         Index           =   19
         Left            =   720
         TabIndex        =   13
         Tag             =   "BillingTransactionsClaimsClosed"
         Top             =   4560
         Width           =   3375
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "BillingTransactionsClaimsQueued"
         Height          =   255
         Index           =   17
         Left            =   720
         TabIndex        =   12
         Tag             =   "BillingTransactionsClaimsQueued"
         Top             =   4080
         Width           =   3255
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Transactions"
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   11
         Top             =   2160
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Code"
         Height          =   255
         Index           =   8
         Left            =   720
         TabIndex        =   10
         Tag             =   "Code"
         Top             =   1920
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "ServiceHeader"
         Height          =   255
         Index           =   7
         Left            =   720
         TabIndex        =   9
         Tag             =   "ServiceHeader"
         Top             =   1680
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Services"
         Height          =   255
         Index           =   6
         Left            =   480
         TabIndex        =   8
         Tag             =   "Services"
         Top             =   1440
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "InvoiceInformation"
         Height          =   255
         Index           =   4
         Left            =   720
         TabIndex        =   7
         Tag             =   "InvoiceInformation"
         Top             =   960
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "ClaimHeader"
         Height          =   255
         Index           =   3
         Left            =   720
         TabIndex        =   6
         Tag             =   "ClaimHeader"
         Top             =   720
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Invoice"
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   5
         Tag             =   "Invoice"
         Top             =   480
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "Charges"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   2535
      End
      Begin VB.CheckBox chk 
         BackColor       =   &H00FFFF80&
         Caption         =   "All"
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   3
         Top             =   0
         Width           =   2535
      End
      Begin VB.Line Line2 
         Index           =   25
         X1              =   600
         X2              =   720
         Y1              =   6360
         Y2              =   6360
      End
      Begin VB.Line Line1 
         Index           =   6
         X1              =   600
         X2              =   600
         Y1              =   3960
         Y2              =   6360
      End
      Begin VB.Line Line2 
         Index           =   24
         X1              =   600
         X2              =   840
         Y1              =   6120
         Y2              =   6120
      End
      Begin VB.Line Line2 
         Index           =   23
         X1              =   600
         X2              =   840
         Y1              =   5880
         Y2              =   5880
      End
      Begin VB.Line Line2 
         Index           =   22
         X1              =   600
         X2              =   840
         Y1              =   5640
         Y2              =   5640
      End
      Begin VB.Line Line2 
         Index           =   21
         X1              =   600
         X2              =   840
         Y1              =   5400
         Y2              =   5400
      End
      Begin VB.Line Line2 
         Index           =   20
         X1              =   600
         X2              =   840
         Y1              =   5160
         Y2              =   5160
      End
      Begin VB.Line Line2 
         Index           =   19
         X1              =   600
         X2              =   840
         Y1              =   4920
         Y2              =   4920
      End
      Begin VB.Line Line2 
         Index           =   18
         X1              =   600
         X2              =   840
         Y1              =   4680
         Y2              =   4680
      End
      Begin VB.Line Line2 
         Index           =   17
         X1              =   600
         X2              =   840
         Y1              =   4440
         Y2              =   4440
      End
      Begin VB.Line Line2 
         Index           =   16
         X1              =   600
         X2              =   840
         Y1              =   4200
         Y2              =   4200
      End
      Begin VB.Line Line2 
         Index           =   15
         X1              =   360
         X2              =   600
         Y1              =   3960
         Y2              =   3960
      End
      Begin VB.Line Line2 
         Index           =   14
         X1              =   600
         X2              =   840
         Y1              =   3720
         Y2              =   3720
      End
      Begin VB.Line Line2 
         Index           =   13
         X1              =   600
         X2              =   840
         Y1              =   3480
         Y2              =   3480
      End
      Begin VB.Line Line2 
         Index           =   12
         X1              =   600
         X2              =   840
         Y1              =   3240
         Y2              =   3240
      End
      Begin VB.Line Line2 
         Index           =   11
         X1              =   600
         X2              =   840
         Y1              =   3000
         Y2              =   3000
      End
      Begin VB.Line Line1 
         Index           =   3
         X1              =   360
         X2              =   360
         Y1              =   2280
         Y2              =   3960
      End
      Begin VB.Line Line2 
         Index           =   10
         X1              =   600
         X2              =   840
         Y1              =   2760
         Y2              =   2760
      End
      Begin VB.Line Line2 
         Index           =   9
         X1              =   600
         X2              =   840
         Y1              =   2520
         Y2              =   2520
      End
      Begin VB.Line Line2 
         Index           =   8
         X1              =   360
         X2              =   600
         Y1              =   2520
         Y2              =   2520
      End
      Begin VB.Line Line2 
         Index           =   7
         X1              =   120
         X2              =   360
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line Line1 
         Index           =   5
         X1              =   600
         X2              =   600
         Y1              =   2640
         Y2              =   3720
      End
      Begin VB.Line Line2 
         Index           =   6
         X1              =   600
         X2              =   840
         Y1              =   1800
         Y2              =   1800
      End
      Begin VB.Line Line2 
         Index           =   5
         X1              =   600
         X2              =   840
         Y1              =   2040
         Y2              =   2040
      End
      Begin VB.Line Line1 
         Index           =   4
         X1              =   600
         X2              =   600
         Y1              =   1560
         Y2              =   2040
      End
      Begin VB.Line Line2 
         Index           =   4
         X1              =   360
         X2              =   600
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Line Line2 
         Index           =   3
         X1              =   600
         X2              =   840
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Line Line2 
         Index           =   2
         X1              =   600
         X2              =   840
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Line Line2 
         Index           =   1
         X1              =   360
         X2              =   600
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         Index           =   0
         X1              =   120
         X2              =   360
         Y1              =   360
         Y2              =   360
      End
      Begin VB.Line Line1 
         Index           =   2
         X1              =   600
         X2              =   600
         Y1              =   600
         Y2              =   1080
      End
      Begin VB.Line Line1 
         Index           =   1
         X1              =   360
         X2              =   360
         Y1              =   480
         Y2              =   1560
      End
      Begin VB.Line Line1 
         Index           =   0
         X1              =   120
         X2              =   120
         Y1              =   120
         Y2              =   2280
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Index           =   0
      Left            =   4080
      TabIndex        =   0
      Top             =   7560
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Setting.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   7560
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Setting.frx":01DF
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFF80&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   6735
      Left            =   120
      TabIndex        =   29
      Top             =   720
      Width           =   5055
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Select Items to Display"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   30
      Top             =   240
      Width           =   4095
   End
End
Attribute VB_Name = "frmSetting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim bySys As Boolean

Private Sub chk_Click(Index As Integer)
If bySys Then Exit Sub
bySys = True

Dim c As Integer
'set children
For c = Index + 1 To chk.UBound
    If Not chk(c).Left > chk(Index).Left Then Exit For
    chk(c).Value = chk(Index).Value
Next

If Index = 0 Then GoTo ExitSub
'set all parents
Dim cnt As Integer
Dim chkParent As CheckBox, chkChild As CheckBox
Set chkChild = chk(Index)

Do
    'set parent
    Set chkParent = GetParent(chkChild)
    cnt = -1
    For c = chkParent.Index + 1 To chk.UBound
        If chk(c).Left > chkParent.Left Then
            If chk(c).Left = chkChild.Left Then
                If Not cnt = chk(c).Value Then
                    If cnt < 0 Then
                        cnt = chk(c).Value
                    Else
                        cnt = 2
                        Exit For
                    End If
                End If
            End If
        Else
            Exit For
        End If
    Next
    If cnt > 2 Then cnt = 2
    chkParent.Value = cnt
    
    'get next parent
    If chkParent.Left = 0 Then
        Exit Do
    Else
        Set chkChild = chkParent
    End If
Loop

ExitSub:
bySys = False
End Sub

Private Function GetParent(Child As CheckBox) As CheckBox

Dim c As Integer
For c = Child.Index To 0 Step -1
    If chk(c).Left < Child.Left Then
        Set GetParent = chk(c)
        Exit For
    End If
Next

End Function

Private Sub cmdDone_Click(Index As Integer)

If Index = 0 Then
    setSvrGrid
    With frmPaymentsTotal
        .setView
        .setRowsHeight
    End With
End If

Unload Me
End Sub


Public Sub setSvrGrid()
Dim i As Integer

For i = 0 To chk.UBound
    Select Case chk(i).Tag
    Case "ClaimHeader"
        SrvGrid.ClaimHeader = chk(i).Value
    Case "InvoiceInformation"
        SrvGrid.InvoiceInformation = chk(i).Value
    Case "ServiceHeader"
        SrvGrid.ServiceHeader = chk(i).Value
    Case "Code"
        SrvGrid.Code = chk(i).Value
    Case "BillingTransactionsClaimsClosed"
        SrvGrid.BillingTransactionsClaimsClosed = chk(i).Value
    Case "BillingTransactionsClaimsQueued"
        SrvGrid.BillingTransactionsClaimsQueued = chk(i).Value
    Case "BillingTransactionsClaimsSent"
        SrvGrid.BillingTransactionsClaimsSent = chk(i).Value
    Case "BillingTransactionsAppeal"
        SrvGrid.BillingTransactionsAppeal = chk(i).Value
    Case "BillingTransactionsAppealClosed"
        SrvGrid.BillingTransactionsAppealClosed = chk(i).Value
    Case "BillingTransactionsPatientStatementQueued"
        SrvGrid.BillingTransactionsPatientStatementQueued = chk(i).Value
    Case "BillingTransactionsPatientStatementSent"
        SrvGrid.BillingTransactionsPatientStatementSent = chk(i).Value
    Case "BillingTransactionsPatientStatementClosed"
        SrvGrid.BillingTransactionsPatientStatementClosed = chk(i).Value
    Case "BillingTransactionsMonthlyStatement"
        SrvGrid.BillingTransactionsMonthlyStatement = chk(i).Value
    Case "BillingTransactionOrphaned"
        SrvGrid.BillingTransactionOrphaned = chk(i).Value
    Case "InsurancePayments"
        SrvGrid.InsurancePayments = chk(i).Value
    Case "PatientPayments"
        SrvGrid.PatientPayments = chk(i).Value
    Case "InsuranceAdjustments"
        SrvGrid.InsuranceAdjustments = chk(i).Value
    Case "NonInsuranceAdjustments"
        SrvGrid.NonInsuranceAdjustments = chk(i).Value
    Case "InfoTransactions"
        SrvGrid.InfoTransactions = chk(i).Value
    Case "ClaimTotal"
        SrvGrid.ClaimTotal = chk(i).Value
        
    Case "Invoice"
        SrvGrid.Invoice = chk(i).Value
    Case "Services"
        SrvGrid.Services = chk(i).Value
    Case "Payments"
        SrvGrid.Payments = chk(i).Value
    Case "Billing"
        SrvGrid.Billing = chk(i).Value

    End Select
Next


End Sub


Private Sub Form_Load()
Dim i As Integer

For i = 0 To chk.UBound
    Select Case chk(i).Tag
    Case "ClaimHeader"
        chk(i).Value = -CInt(SrvGrid.ClaimHeader)
    Case "InvoiceInformation"
        chk(i).Value = -CInt(SrvGrid.InvoiceInformation)
    Case "ServiceHeader"
        chk(i).Value = -CInt(SrvGrid.ServiceHeader)
    Case "Code"
        chk(i).Value = -CInt(SrvGrid.Code)
    Case "BillingTransactionsClaimsClosed"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsClaimsClosed)
    Case "BillingTransactionsClaimsQueued"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsClaimsQueued)
    Case "BillingTransactionsClaimsSent"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsClaimsSent)
    Case "BillingTransactionsAppeal"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsAppeal)
    Case "BillingTransactionsAppealClosed"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsAppealClosed)
    Case "BillingTransactionsPatientStatementQueued"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsPatientStatementQueued)
    Case "BillingTransactionsPatientStatementSent"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsPatientStatementSent)
    Case "BillingTransactionsPatientStatementClosed"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsPatientStatementClosed)
    Case "BillingTransactionsMonthlyStatement"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionsMonthlyStatement)
    Case "BillingTransactionOrphaned"
        chk(i).Value = -CInt(SrvGrid.BillingTransactionOrphaned)
    Case "InsurancePayments"
        chk(i).Value = -CInt(SrvGrid.InsurancePayments)
    Case "PatientPayments"
        chk(i).Value = -CInt(SrvGrid.PatientPayments)
    Case "InsuranceAdjustments"
        chk(i).Value = -CInt(SrvGrid.InsuranceAdjustments)
    Case "NonInsuranceAdjustments"
        chk(i).Value = -CInt(SrvGrid.NonInsuranceAdjustments)
    Case "InfoTransactions"
        chk(i).Value = -CInt(SrvGrid.InfoTransactions)
    Case "ClaimTotal"
        chk(i).Value = -CInt(SrvGrid.ClaimTotal)
    End Select
Next

End Sub
Public Sub FrmClose()
Unload Me
End Sub
