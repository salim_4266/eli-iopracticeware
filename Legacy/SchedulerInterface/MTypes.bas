Attribute VB_Name = "MTypes"
Option Explicit

Public Type ClaimInfo
    ProcessCurrentCheck As Boolean
    FromFile As Boolean
    PaymentCheck As String
    PaymentAmount As String
    PaymentDate As String
    PaymentEOBDate As String
    PatientId As String
    InsurerId As String
    ClaimStatus As String   '< from file only
    PayerId As String       '< from file only
    NAIC As String          '< from file only
    PaymentMethod As String
    UserId As String
    ClaimDate As String
End Type

Public Type ServiceInfo
    InValidService As Boolean
    ItemId As String
    Service As String
    Invoice As String
    ReceivableId As String
    ServiceDate As String
    Modifier As String
    Comments As String
    ComOn As Boolean
    Amount(5) As Single
    PaymentType(5) As String
    Rsn(5) As String
    FrwBal As Single
    Nx As String
    Ac As String
    GroupCode(5) As Long
End Type

Public Function ClmToColl(Clm As ClaimInfo) As Collection
Dim coll As New Collection

coll.Add Clm.ProcessCurrentCheck, "ProcessCurrentCheck"
coll.Add Clm.FromFile, "FromFile"
coll.Add Clm.PaymentCheck, "PaymentCheck"
coll.Add Clm.PaymentAmount, "PaymentAmount"
coll.Add Clm.PaymentDate, "PaymentDate"
coll.Add Clm.PaymentEOBDate, "PaymentEOBDate"
coll.Add Clm.PatientId, "PatientId"
coll.Add Clm.InsurerId, "InsurerId"
coll.Add Clm.ClaimStatus, "ClaimStatus"
coll.Add Clm.PayerId, "PayerId"
coll.Add Clm.NAIC, "NAIC"
coll.Add Clm.PaymentMethod, "PaymentMethod"
coll.Add Clm.UserId, "UserId"

Set ClmToColl = coll
End Function

Public Function SrvToColl(Srv As ServiceInfo) As Collection
Dim coll As New Collection

coll.Add Srv.InValidService, "InValidService"
coll.Add Srv.ItemId, "ItemId"
coll.Add Srv.Service, "Service"
coll.Add Srv.Invoice, "Invoice"
coll.Add Srv.ReceivableId, "ReceivableId"
coll.Add Srv.ServiceDate, "ServiceDate"
coll.Add Srv.Modifier, "Modifier"
coll.Add Srv.Comments, "Comments"
coll.Add Srv.ComOn, "ComOn"
coll.Add Srv.Amount, "Amount"
coll.Add Srv.PaymentType, "PaymentType"
coll.Add Srv.Rsn, "Rsn"
coll.Add Srv.FrwBal, "FrwBal"
coll.Add Srv.Nx, "Nx"
coll.Add Srv.Ac, "Ac"

Set SrvToColl = coll
End Function

Public Function LoadClm(coll As Collection) As ClaimInfo
Dim Clm As ClaimInfo

Clm.ProcessCurrentCheck = coll("ProcessCurrentCheck")
Clm.FromFile = coll("FromFile")
Clm.PaymentCheck = coll("PaymentCheck")
Clm.PaymentAmount = coll("PaymentAmount")
Clm.PaymentDate = coll("PaymentDate")
Clm.PaymentEOBDate = coll("PaymentEOBDate")
Clm.PatientId = coll("PatientId")
Clm.InsurerId = coll("InsurerId")
Clm.ClaimStatus = coll("ClaimStatus")
Clm.PayerId = coll("PayerId")
Clm.NAIC = coll("NAIC")
Clm.PaymentMethod = coll("PaymentMethod")
Clm.UserId = coll("UserId")

LoadClm = Clm
End Function

Public Function LoadSrv(coll As Collection) As ServiceInfo
Dim Srv As ServiceInfo
Dim a As Integer

Srv.InValidService = coll("InValidService")
Srv.ItemId = coll("ItemId")
Srv.Service = coll("Service")
Srv.Invoice = coll("Invoice")
Srv.ReceivableId = coll("ReceivableId")
Srv.ServiceDate = coll("ServiceDate")
Srv.Modifier = coll("Modifier")
Srv.Comments = coll("Comments")
Srv.ComOn = coll("ComOn")
For a = 0 To 5
    Srv.Amount(a) = coll("Amount")(a)
    Srv.PaymentType(a) = coll("PaymentType")(a)
    Srv.Rsn(a) = coll("Rsn")(a)
Next
Srv.FrwBal = coll("FrwBal")
Srv.Nx = coll("Nx")
Srv.Ac = coll("Ac")

LoadSrv = Srv
End Function



Public Function GetInsLst(PatId As Long, ADate As String, Optional Copay As Boolean) As Collection
Dim lst As New Collection

Dim i As Integer, p As Integer
Dim IKey As Integer
Dim InsInfo(4) As String
Dim IType(1) As String
Dim DisplayText As String
Dim Temp1 As String, Temp2 As String

Dim RetPat As New Patient
Dim RetPatientFinancialService As PatientFinancialService
Dim RSPatIns As ADODB.Recordset
Dim ApplTemp As New ApplicationTemplates
Dim RS As New ADODB.Recordset
Dim RSIns As New ADODB.Recordset


Dim r As Long
RS.ActiveConnection = Nothing
RS.CursorLocation = adUseClient
RS.LockType = adLockBatchOptimistic
RS.Fields.Append "c0", adChar, 1
RS.Fields.Append "c1", adChar, 2
RS.Fields.Append "c2", adChar, 1
RS.Fields.Append "c3", adChar, 8
RS.Fields.Append "c4", adChar, 9
RS.Fields.Append "c5", adChar, 3
RS.Fields.Append "c6", adChar, 75
RS.Fields.Append "c7", adChar, 10
RS.Fields.Append "c8", adChar, 10
RS.Open

Set RetPatientFinancialService = New PatientFinancialService
RetPatientFinancialService.InsuredPatientId = PatId
Set RSPatIns = RetPatientFinancialService.GetPatientInsurerInfo
If (Not RSPatIns Is Nothing And RSPatIns.RecordCount > 0) Then
    While (Not RSPatIns.EOF)
        Call ApplTemp.ApplGetPlanName(CLng(RSPatIns("InsurerId")), _
        InsInfo(0), InsInfo(1), InsInfo(2), InsInfo(3), InsInfo(4), False)

        If Not ADate = "" Then
            If (Not ADate < RSPatIns("StartDateTime")) _
            And (Not ADate > RSPatIns("EndDateTime") Or _
                 RSPatIns("EndDateTime") = "") Then
            Else
                GoTo endloop
            End If
        End If

        RS.AddNew
        '-------------
        RS.Fields("c0") = p
        '-------------
        If (Trim(ConvertInsuranceTypeToLegacy(RSPatIns("InsuranceTypeId"))) = "") Then
            Temp1 = "M"
        Else
            Temp1 = ConvertInsuranceTypeToLegacy(RSPatIns("InsuranceTypeId"))
        End If

        Select Case Temp1
        Case "M"
            Temp1 = "1" & Temp1
        Case "V"
            Temp1 = "2" & Temp1
        Case "A"
            Temp1 = "3" & Temp1
        Case "W"
            Temp1 = "4" & Temp1
        End Select
        RS.Fields("c1") = Temp1
        '-------------
        If (RSPatIns("InsuredPatientId") = RetPat.PolicyPatientId) Then
            RS.Fields("c2") = IType(0)
        Else
            RS.Fields("c2") = IType(1)
        End If
        '-------------
        RS.Fields("c3") = RSPatIns("StartDateTime")
        '-------------
        Temp1 = RSPatIns("EndDateTime")
        If Temp1 = "" Then
            Temp1 = "3"
        Else
            If Temp1 < Format(Now, "yyyymmdd") Then
                Temp1 = "1" & Temp1
            Else
                Temp1 = "2" & Temp1
            End If
        End If
        RS.Fields("c4") = Temp1
        '-------------
        RS.Fields("c5") = RSPatIns("OrdinalId")
        '-------------
        RS.Fields("c6") = Trim(InsInfo(1))
        '-------------
        RS.Fields("c7") = ConvertInsuranceTypeToLegacy(RSPatIns("InsuranceTypeId"))
        '-------------
        If (RSPatIns("Copay") < 0.1) Then
        RS.Fields("c8") = "0.00"
        Else
         RS.Fields("c8") = RSPatIns("Copay")
        End If

        RS.Update
endloop:
        RSPatIns.MoveNext
    Wend
End If

RS.Sort = "c1, c0, c4 desc, c5, c6"
Do Until RS.EOF
    Temp1 = Mid(RS.Fields("c3"), 5, 2) + "/" + _
            Mid(RS.Fields("c3"), 7, 2) + "/" + _
            Mid(RS.Fields("c3"), 1, 4)

    Temp2 = Trim(Mid(RS.Fields("c4"), 2, 8))
    If Not Temp2 = "" Then
        Temp2 = " - " & _
                Mid(Temp2, 5, 2) + "/" + _
                Mid(Temp2, 7, 2) + "/" + _
                Mid(Temp2, 1, 4)
    End If

    DisplayText = Space(150)
    Mid(DisplayText, 1, 107) = RS.Fields("c6") & " " & _
                            Mid(RS.Fields("c1"), 2, 1) & " " & _
                            RS.Fields("c2") & " " & _
                            Temp1 & Temp2
    Mid(DisplayText, 107, 10) = RS.Fields("c7")

    If Copay Then
        DisplayText = DisplayText & RS.Fields("c8")
    End If

    lst.Add DisplayText, CStr(IKey)
    IKey = IKey + 1
    RS.MoveNext
Loop

Set RS = Nothing
Set RetPat = Nothing
Set ApplTemp = Nothing

Set GetInsLst = lst

End Function

Public Function getTranStatus(AppId As Long, ItemId As Long) As String
Dim RS1 As ADODB.Recordset
Dim SQL As String

SQL = _
    "SELECT transactionstatus FROM PatientReceivables pr " & _
    "join PracticeTransactionJournal ptj on  pr.receivableid = ptj.transactiontypeid " & _
    "join ServiceTransactions st on ptj.transactionid  = st.transactionid " & _
    "where pr.appointmentid = " & AppId & _
    " and ptj.TransactionType  = 'R' " & _
    " and st.serviceid = " & ItemId & _
    " order by transactionstatus"
Set RS1 = GetRS(SQL)

If RS1.RecordCount > 0 Then
    getTranStatus = RS1("transactionstatus")
End If

End Function

Public Function BilledParty_5(AppId As Long, SrvId As Long, Optional InsuredId As String) As String

Dim RS As ADODB.Recordset
Dim ApplList As New ApplicationAIList
Set RS = ApplList.getBilledParty(AppId, SrvId)
Set ApplList = Nothing

If RS.EOF Then
    InsuredId = 0
    Exit Function
End If

If RS("Transactionref") = "P" Then
    BilledParty_5 = "P"
    InsuredId = 0
Else
    BilledParty_5 = RS("InsurerID")
    If InsuredId = "?" Then
        InsuredId = RS("InsuredId")
    End If
End If
End Function

Public Function ItemExist(col As Collection, itm As String) As Boolean
Dim t
On Error GoTo Error
t = col(itm)

Error:
Select Case Err.Number
Case 0
    ItemExist = True
Case 5
    ItemExist = False
    Exit Function
Case Else
    Err.Raise Err.Number, , Err.Description
End Select

End Function

