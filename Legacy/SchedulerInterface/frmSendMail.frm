VERSION 5.00
Object = "{B92844A0-9A8B-11D0-9586-0000E8C0DC7F}#1.0#0"; "CSMSG32.OCX"
Object = "{C5FBB860-7B42-11CF-A8A0-444553540000}#1.0#0"; "CSDNS32.OCX"
Object = "{F070A868-7D7E-11CF-A8A0-444553540000}#1.0#0"; "CSMTP32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmSendMail 
   Caption         =   "Send Mail"
   ClientHeight    =   5595
   ClientLeft      =   45
   ClientTop       =   270
   ClientWidth     =   5640
   LinkTopic       =   "Form1"
   ScaleHeight     =   5595
   ScaleWidth      =   5640
   StartUpPosition =   3  'Windows Default
   Begin MailMessageCtrl.MailMessage MailMessage1 
      Left            =   3480
      Top             =   5160
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   16
      FileName        =   ""
      Localize        =   0   'False
   End
   Begin DnsClientCtrl.DnsClient DnsClient1 
      Left            =   3840
      Top             =   5160
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      HostProtocol    =   6
      LocalDomain     =   ""
      RemotePort      =   53
      RemoteService   =   "domain"
      Retry           =   4
      Timeout         =   4
      RecordName      =   ""
      RecordType      =   0
   End
   Begin SmtpClientCtrl.SmtpClient SmtpClient1 
      Left            =   4200
      Top             =   5160
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      Address         =   ""
      AutoResolve     =   -1  'True
      Blocking        =   -1  'True
      Domain          =   ""
      HostAddress     =   ""
      HostName        =   ""
      RemotePort      =   0
      RemoteService   =   ""
      Timeout         =   60
      UserName        =   ""
   End
   Begin RichTextLib.RichTextBox txtbxMsg 
      Height          =   2532
      Left            =   120
      TabIndex        =   5
      Top             =   2280
      Width           =   5412
      _ExtentX        =   9551
      _ExtentY        =   4471
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"frmSendMail.frx":0000
   End
   Begin VB.Frame pnlInformation 
      Height          =   2040
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   5412
      Begin VB.CommandButton cmdbtnAttach 
         Caption         =   "Attach"
         Enabled         =   0   'False
         Height          =   330
         Left            =   120
         TabIndex        =   3
         Top             =   1416
         Width           =   700
      End
      Begin VB.ComboBox cmbxAttachments 
         Height          =   288
         Left            =   888
         TabIndex        =   4
         Top             =   1440
         Width           =   4092
      End
      Begin VB.TextBox txtbxSubject 
         Height          =   288
         Left            =   888
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   1080
         Width           =   4092
      End
      Begin VB.TextBox txtbxFrom 
         Height          =   288
         Left            =   888
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   720
         Width           =   4092
      End
      Begin VB.TextBox txtbxTo 
         Enabled         =   0   'False
         Height          =   288
         Left            =   888
         Locked          =   -1  'True
         TabIndex        =   0
         Top             =   360
         Width           =   4092
      End
      Begin VB.Label lblSubject 
         AutoSize        =   -1  'True
         Caption         =   "Subject:"
         Height          =   192
         Left            =   180
         TabIndex        =   10
         Top             =   1128
         Width           =   576
      End
      Begin VB.Label lblFrom 
         AutoSize        =   -1  'True
         Caption         =   "From:"
         Height          =   192
         Left            =   264
         TabIndex        =   9
         Top             =   768
         Width           =   408
      End
      Begin VB.Label lblTo 
         AutoSize        =   -1  'True
         Caption         =   "To:"
         Height          =   192
         Left            =   348
         TabIndex        =   8
         Top             =   408
         Width           =   240
      End
   End
   Begin VB.CommandButton cmdbtnSend 
      Caption         =   "Send"
      Height          =   372
      Left            =   2400
      TabIndex        =   6
      Top             =   5040
      Width           =   972
   End
End
Attribute VB_Name = "frmSendMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private SmtpHost As String
Private SmtpPort As Long
Private DNSPort As Long

Private Sub cmdbtnSend_Click()
Dim bFinished As Boolean
Dim iRetExport As Long
Dim iRetConnect As Long
Dim iRetAddress As Long
Dim iRetSend As Long
Dim liRemaining As Long
Dim iWriteLen As Integer
Dim iRetWrite As Integer
Dim i As Integer
Dim strBuffer As String
Dim iPos As Integer
Dim iPosat As Integer
Dim strDomain As String
Dim iRetQuery As Long
Dim strData As String
Dim iRetAttach As Long
Dim strFileName As String
     
'Check recipient address to verify it is valid (or at least has enough characters to look valid)
cmdbtnSend.Enabled = False
If (Trim(txtbxTo.Text) <> "") Then
    txtbxTo.Text = Trim(txtbxTo.Text)
    iPosat = InStrPS(txtbxTo.Text, "@")
    If iPosat > 1 Then
        strDomain = Right(txtbxTo.Text, Len(txtbxTo.Text) - iPosat)
        iPos = InStrPS(strDomain, ".")
        If (iPos < 1) Then
            MsgBox "An invalid email address is being used."
            Exit Sub
        End If
    Else
        MsgBox "You must enter a valid email address in the form user@domain.com."
        Exit Sub
    End If
Else
    MsgBox "An invalid email address is being used."
    Exit Sub
End If
SmtpClient1.Recipient = txtbxTo.Text
MailMessage1.To = SmtpClient1.Recipient
    
'Check to make sure the user hasn't forgotten some vital piece of information
If Len(Trim(txtbxFrom.Text)) > 0 Then
    SmtpClient1.Address = Trim(txtbxFrom.Text)
    MailMessage1.From = SmtpClient1.Address
Else
    MsgBox "You must enter an originating address to send email."
    Exit Sub
End If
MailMessage1.Date = SmtpClient1.DateStamp
MailMessage1.Subject = Trim(txtbxSubject.Text)
MailMessage1.Text = txtbxMsg.Text
    
'Attach all the files
If cmbxAttachments.ListCount > 0 Then
    For i = 0 To cmbxAttachments.ListCount - 1
        If Dir(Trim(cmbxAttachments.List(i))) <> "" Then
            iRetAttach = MailMessage1.AttachFile(cmbxAttachments.List(i))
            If iRetAttach <> 0 Then
                MsgBox "Error " & iRetAttach & " attaching document:  " & cmbxAttachments.List(i)
            End If
        End If
    Next i
End If
    
'Write the message in toto to the clipboard using ExportFile
Clipboard.Clear
iRetExport = MailMessage1.ExportFile("")
strBuffer = ""
strBuffer = Clipboard.GetText
liRemaining = Len(strBuffer)
Clipboard.Clear
If liRemaining <= 2048 Then
    iWriteLen = liRemaining
Else
    iWriteLen = 2048
End If
bFinished = False
If SmtpClient1.Connected Then
    SmtpClient1.Disconnect
End If
    
'Begin the mail transaction by connecting to the Server
iRetConnect = SmtpClient1.Connect
If iRetConnect <> 0 Then
    MsgBox "Error " & iRetConnect & " connecting to SMTP server. Exiting..."
    Exit Sub
End If
    
'Identify yourself to the server
iRetAddress = SmtpClient1.AddressMail
If iRetAddress <> 0 Then
    MsgBox "Error " & iRetAddress & " addressing email. Exiting..."
    Exit Sub
End If
    
'Write the email to the server
While Not bFinished
    iRetWrite = SmtpClient1.Write(strBuffer, iWriteLen)
    If iRetWrite = -1 Then
        MsgBox "Error " & SmtpClient1.SocketError & " writing to server. Exiting..."
        Exit Sub
    End If
    If Len(strBuffer) = 0 Then
        bFinished = True
    Else
        liRemaining = liRemaining - iRetWrite
        If liRemaining <= 2048 Then
            iWriteLen = liRemaining
        Else
            iWriteLen = 2048
        End If
        strBuffer = Right(strBuffer, liRemaining)
    End If
Wend
    
'End the mail transaction and clean up
iRetSend = SmtpClient1.SendMail
If iRetSend <> 0 Then
    MsgBox "Error " & iRetSend & " sending email. Exiting..."
    Exit Sub
End If
SmtpClient1.Disconnect
txtbxTo.Text = ""
txtbxFrom.Text = ""
txtbxSubject.Text = ""
cmbxAttachments.Clear
End Sub

Private Sub Form_Load()
Call SetupSend
End Sub

Private Function IsThere(TheName As String) As Boolean
Dim i As Integer
IsThere = False
If cmbxAttachments.ListCount > 0 Then
    For i = 0 To cmbxAttachments.ListCount - 1
        If Trim(TheName) = Trim(cmbxAttachments.List(i)) Then
            IsThere = True
            Exit For
        End If
    Next i
End If
End Function

Private Sub SetupSend()
Dim Rec As String
FM.OpenFile LocalPinPointDirectory & "MailInterface\Send.txt", FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
If (Not EOF(101)) Then
    Line Input #101, Rec
    If (Trim(Rec) <> "") Then
        DNSPort = Val(Trim(Rec))
    End If
    Line Input #101, Rec
    If (Trim(Rec) <> "") Then
        SmtpPort = Val(Trim(Rec))
    End If
    Line Input #101, Rec
    If (Trim(Rec) <> "") Then
        SmtpHost = Trim(Rec)
    End If
    Line Input #101, Rec
    If (Trim(Rec) <> "") Then
        txtbxTo.Text = Trim(Rec)
    End If
    Line Input #101, Rec
    If (Trim(Rec) <> "") Then
        txtbxFrom.Text = Trim(Rec)
    End If
    Line Input #101, Rec
    If (Trim(Rec) <> "") Then
        txtbxSubject.Text = "Incoming Attachment"
        txtbxMsg.Text = "Incoming Attachement from " + Trim(Rec)
    End If
    Do Until (EOF(101))
        Line Input #101, Rec
        If (Trim(Rec) <> "") Then
            If Not (IsThere(Rec)) Then
                cmbxAttachments.Text = Rec
                cmbxAttachments.AddItem Rec
            End If
        End If
    Loop
End If
FM.CloseFile CLng(101)
SmtpClient1.HostName = SmtpHost
SmtpClient1.RemotePort = SmtpPort
DnsClient1.RemotePort = DNSPort
DnsClient1.NameServer(0) = ""
SmtpClient1.AutoResolve = False
SmtpClient1.Blocking = True
End Sub
