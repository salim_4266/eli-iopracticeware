VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPCNew 
   BackColor       =   &H009B9626&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H009B9626&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox AQuantity 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2640
      MaxLength       =   7
      TabIndex        =   1
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox SKU 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2640
      TabIndex        =   0
      Top             =   600
      Width           =   8535
   End
   Begin VB.TextBox Manufacturer 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2640
      TabIndex        =   5
      Top             =   2400
      Width           =   8535
   End
   Begin VB.TextBox Price 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   5280
      MaxLength       =   7
      TabIndex        =   2
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox Model 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2640
      TabIndex        =   6
      Top             =   3000
      Width           =   8535
   End
   Begin VB.TextBox AColor 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2640
      TabIndex        =   7
      Top             =   3600
      Width           =   8535
   End
   Begin VB.TextBox AType 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   2640
      TabIndex        =   4
      Top             =   1800
      Width           =   8535
   End
   Begin VB.TextBox WPrice 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   9840
      MaxLength       =   7
      TabIndex        =   3
      Top             =   1200
      Width           =   1335
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9480
      TabIndex        =   8
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCNew.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   9
      Top             =   7920
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCNew.frx":01DF
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Quantity"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   18
      Top             =   1200
      Width           =   1605
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Number"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   17
      Top             =   600
      Width           =   1455
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Enter a New Item into Inventory"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   450
      Left            =   2280
      TabIndex        =   16
      Top             =   120
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Manufacturer"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   15
      Top             =   2400
      Width           =   2355
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Price"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4200
      TabIndex        =   14
      Top             =   1200
      Width           =   1005
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Model"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   13
      Top             =   3000
      Width           =   1275
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Color"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   12
      Top             =   3600
      Width           =   1440
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   11
      Top             =   1800
      Width           =   2040
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      BackColor       =   &H009B9626&
      Caption         =   "Wholesale Price"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6960
      TabIndex        =   10
      Top             =   1200
      Width           =   2940
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmPCNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SKUNew As String
Public OrderType As String
Public InventoryId As Long
Private PCColor As String
Private PCSize As String
Private PCManufacturer As String
Private PCModel As String
Private PCTint As String
Private PCType As String
Private PCCoating As String
Private PCPDD As String
Private PCPDN As String
Private PCCEGHeight As String
Private PCQuantity As String
Private PCPrice As String
Private PCWPrice As String

Private Sub cmdHome_Click()
Unload frmPCNew
End Sub

Private Sub cmdDone_Click()
Dim RetrievePCInventory As PCInventory
If (SKU.Text <> "") Then
    If (Validate) Then
        If (InventoryId > 0) Then
            Set RetrievePCInventory = New PCInventory
            RetrievePCInventory.InventoryId = InventoryId
            If (RetrievePCInventory.RetrievePCInventory) Then
                RetrievePCInventory.SKU = SKU.Text
                RetrievePCInventory.Type_ = PCType
                RetrievePCInventory.Manufacturer = Trim(Manufacturer.Text)
                RetrievePCInventory.Model = Trim(Model.Text)
                RetrievePCInventory.PCColor = Trim(AColor.Text)
                RetrievePCInventory.Qty = val(PCQuantity)
                RetrievePCInventory.Cost = val(PCPrice)
                RetrievePCInventory.WCost = val(PCWPrice)
                RetrievePCInventory.PutPCInventory
                SKUNew = SKU.Text
            End If
            Set RetrievePCInventory = Nothing
        Else
            Set RetrievePCInventory = New PCInventory
            RetrievePCInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "'"
            RetrievePCInventory.FindPCInventory
            If (RetrievePCInventory.PCInventoryTotal < 1) Then
                RetrievePCInventory.SKU = SKU.Text
                RetrievePCInventory.Type_ = PCType
                RetrievePCInventory.Manufacturer = Manufacturer.Text
                RetrievePCInventory.Model = Model.Text
                RetrievePCInventory.PCColor = AColor.Text
                RetrievePCInventory.Qty = val(PCQuantity)
                RetrievePCInventory.Cost = val(PCPrice)
                RetrievePCInventory.WCost = val(PCWPrice)
                RetrievePCInventory.PutPCInventory
                SKUNew = SKU.Text
            End If
            Set RetrievePCInventory = Nothing
        End If
        Unload frmPCNew
    End If
End If
End Sub

Private Function Validate() As Boolean
Validate = True
If (SKU.Text = "") Or (PCType = "") Then
    Validate = False
    frmEventMsgs.Header = "Number and/or Description must be entered."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Function

Private Sub AQuantity_Validate(Cancel As Boolean)
Call AQuantity_KeyPress(13)
End Sub

Private Sub AQuantity_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(AQuantity.Text)) And (Trim(AQuantity.Text) <> "") Then
        frmEventMsgs.Header = "Invalid Quantity."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        AQuantity.Text = ""
        AQuantity.SetFocus
    Else
        PCQuantity = AQuantity.Text
    End If
End If
End Sub

Private Sub Form_Load()
SKUNew = ""
If (InventoryId > 0) Then
    Call LoadInventory(InventoryId)
Else
    SKU.Text = SKU
End If
End Sub

Private Sub Price_Validate(Cancel As Boolean)
Call Price_KeyPress(13)
End Sub

Private Sub Price_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(Price.Text)) Then
        frmEventMsgs.Header = "Invalid Price."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Price.Text = ""
        Price.SetFocus
    Else
        PCPrice = Price.Text
    End If
End If
End Sub

Private Sub WPrice_Validate(Cancel As Boolean)
Call WPrice_KeyPress(13)
End Sub

Private Sub WPrice_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(WPrice.Text)) Then
        frmEventMsgs.Header = "Invalid Price."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        WPrice.Text = ""
        WPrice.SetFocus
    Else
        PCWPrice = WPrice.Text
    End If
End If
End Sub

Private Sub SKU_KeyPress(KeyAscii As Integer)
Dim RetrievePCInventory As PCInventory
If (KeyAscii = 13) Then
    Set RetrievePCInventory = New PCInventory
    RetrievePCInventory.Criteria = "SKU = '" + Trim(SKU.Text) + "'"
    RetrievePCInventory.FindPCInventory
    If (RetrievePCInventory.PCInventoryTotal > 0) Then
        Call RetrievePCInventory.SelectPCInventory(1)
        InventoryId = RetrievePCInventory.InventoryId
        Set RetrievePCInventory = Nothing
        Call LoadInventory(InventoryId)
    Else
        Set RetrievePCInventory = Nothing
        AQuantity.SetFocus
    End If
End If
End Sub

Private Sub AType_Validate(Cancel As Boolean)
Call AType_KeyPress(13)
End Sub

Private Sub AType_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    PCType = AType.Text
End If
End Sub

Private Function getFieldValue(RefName As String, RefValue As String) As Boolean
getFieldValue = False
RefValue = ""
If (Trim(RefName) <> "") Then
    frmSelectDialogue.InsurerSelected = ""
    Call frmSelectDialogue.BuildSelectionDialogue(RefName)
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        RefValue = UCase(frmSelectDialogue.Selection)
        getFieldValue = True
    End If
End If
End Function

Private Function LoadInventory(InvId As Long) As Boolean
Dim RetrievePCInventory As PCInventory
InventoryId = InvId
If (InvId > 0) Then
    Set RetrievePCInventory = New PCInventory
    RetrievePCInventory.InventoryId = InventoryId
    If (RetrievePCInventory.RetrievePCInventory) Then
        SKU.Text = RetrievePCInventory.SKU
        PCType = RetrievePCInventory.Type_
        PCManufacturer = RetrievePCInventory.Manufacturer
        PCModel = RetrievePCInventory.Model
        PCColor = RetrievePCInventory.PCColor
        PCQuantity = Trim(Str(RetrievePCInventory.Qty))
        PCPrice = Trim(Str(RetrievePCInventory.Cost))
        PCWPrice = Trim(Str(RetrievePCInventory.WCost))
        
        SKU.Text = RetrievePCInventory.SKU
        AType.Text = RetrievePCInventory.Type_
        Manufacturer.Text = RetrievePCInventory.Manufacturer
        Model.Text = RetrievePCInventory.Model
        AColor.Text = RetrievePCInventory.PCColor
        AQuantity.Text = Trim(Str(RetrievePCInventory.Qty))
        Price.Text = Trim(Str(RetrievePCInventory.Cost))
        WPrice.Text = Trim(Str(RetrievePCInventory.WCost))
    End If
    Set RetrievePCInventory = Nothing
ElseIf (Trim(SKU) <> "") Then
    Set RetrievePCInventory = New PCInventory
    RetrievePCInventory.SKU = SKU
    If (RetrievePCInventory.FindPCInventory > 0) Then
        Call RetrievePCInventory.SelectPCInventory(1)
        SKU.Text = RetrievePCInventory.SKU
        PCType = RetrievePCInventory.Type_
        PCManufacturer = RetrievePCInventory.Manufacturer
        PCModel = RetrievePCInventory.Model
        PCColor = RetrievePCInventory.PCColor
        PCQuantity = Trim(Str(RetrievePCInventory.Qty))
        PCPrice = Trim(Str(RetrievePCInventory.Cost))
        PCWPrice = Trim(Str(RetrievePCInventory.WCost))
        SKU.Text = RetrievePCInventory.SKU
        AType.Text = RetrievePCInventory.Type_
        Manufacturer.Text = RetrievePCInventory.Manufacturer
        Model.Text = RetrievePCInventory.Model
        AColor.Text = RetrievePCInventory.PCColor
        AQuantity.Text = Trim(Str(RetrievePCInventory.Qty))
        Price.Text = Trim(Str(RetrievePCInventory.Cost))
        WPrice.Text = Trim(Str(RetrievePCInventory.WCost))
    Else
        AType.Text = ""
        Manufacturer.Text = ""
        Model.Text = ""
        AColor.Text = ""
        AQuantity.Text = ""
        Price.Text = ""
        WPrice.Text = ""
    End If
    Set RetrievePCInventory = Nothing
Else
    AType.Text = ""
    Manufacturer.Text = ""
    Model.Text = ""
    AColor.Text = ""
    AQuantity.Text = ""
    Price.Text = ""
    WPrice.Text = ""
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
