VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmCollectPayment 
   BackColor       =   &H006C6928&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Collect Payment"
   ClientHeight    =   4890
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5955
   ForeColor       =   &H006C6928&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4890
   ScaleWidth      =   5955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtPayRef 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      MaxLength       =   20
      TabIndex        =   2
      Top             =   3000
      Width           =   3975
   End
   Begin VB.ComboBox lstMethod 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   2520
      Width           =   2295
   End
   Begin VB.TextBox txtAmt 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      MaxLength       =   10
      TabIndex        =   0
      Top             =   2040
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   4440
      TabIndex        =   3
      Top             =   3600
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CollectPayment.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   240
      TabIndex        =   4
      Top             =   3600
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CollectPayment.frx":01DF
   End
   Begin VB.Label lblBalance 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Balance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   11
      Top             =   1200
      Width           =   705
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   10
      Top             =   120
      Width           =   1185
   End
   Begin VB.Label lblCopay 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Copay"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   9
      Top             =   840
      Width           =   555
   End
   Begin VB.Label lblPayRef 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Reference"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   8
      Top             =   3000
      Width           =   870
   End
   Begin VB.Label lblMethod 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Method"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   7
      Top             =   2520
      Width           =   675
   End
   Begin VB.Label lblAmt 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   6
      Top             =   2040
      Width           =   675
   End
   Begin VB.Label lblInsurer 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   5
      Top             =   480
      Width           =   585
   End
End
Attribute VB_Name = "frmCollectPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CopayAmount As Single
Public CopayMethod As String
Public CopayRef As String
Public QuitOn As Boolean
Private HomeOn As Boolean

Private Sub cmdDone_Click()
Dim i As Integer
Dim Temp As String
CopayAmount = val(Trim(txtAmt.Text))
If (CopayAmount > 0) Then
    If (lstMethod.ListIndex >= 0) Then
        CopayMethod = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
    End If
    If (Trim(txtPayRef.Text) = "") And (CopayMethod <> "C") Then
        If CheckConfigCollection("REQUIRECHECK") = "T" Then
            frmEventMsgs.Header = "Need Payment Reference"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtPayRef.Text = ""
            txtPayRef.SetFocus
            Exit Sub
        End If
    End If
    CopayRef = Trim(txtPayRef.Text)
End If
HomeOn = False
Unload frmCollectPayment
End Sub

Private Sub cmdQuit_Click()
QuitOn = True
If (HomeOn) Then
    CopayAmount = 0
    CopayMethod = ""
    CopayRef = ""
End If
Unload frmCollectPayment
End Sub

Public Function LoadCollect(ApptId As Long, CPay As Single, CMeth As String, CRef As String) As Boolean
Dim i As Integer
Dim PlnId As Long, PatId As Long
Dim Balance As Single, PBal As Single, IBal As Single
Dim BalText As String, Temp As String, Copay As String
Dim c1 As String
Dim TheCopay As String, ADate As String
Dim PatName As String, InsType As String
Dim Ar1 As String, Ar2 As String, Ar3 As String
Dim Ar4 As Long, Ar5 As Boolean
Dim p1 As Long, S1 As Long
Dim d1 As String
Dim P1Rel As String, S1Rel As String
Dim P1Bill As Boolean, S1Bill As Boolean
Dim ApplList As ApplicationAIList
LoadCollect = False
HomeOn = True
If (ApptId > 0) Then
    CopayAmount = CPay
    CopayMethod = CMeth
    CopayRef = CRef
    If (CopayAmount <> 0) Then
        HomeOn = False
    End If
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetAppointmentInfo(ApptId, PatId, Ar1, Ar2, Ar3, InsType, Ar4, Ar5, d1)
    Call ApplList.ApplGetCodes("PAYABLETYPE", True, lstMethod)
    p1 = 0
    P1Rel = ""
    P1Bill = False
    S1 = 0
    S1Rel = ""
    S1Bill = False
    Call ApplList.ApplGetPatientPolicyInfo(PatId, PatName, p1, P1Rel, P1Bill, S1, S1Rel, S1Bill)
    Label2.Caption = Trim(PatName)
    Copay = ""
    TheCopay = ""
    If (Trim(p1) > 0) Then
        TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatId, p1, Temp, InsType)))
        If (TheCopay = "-1") Then
            TheCopay = ""
        End If
    End If
    If (Trim(TheCopay) = "") Then
        If (Trim(S1) > 0) Then
            TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatId, S1, Temp, InsType)))
            If (TheCopay = "-1") Then
                TheCopay = "0"
            End If
        Else
            TheCopay = Trim(Str(ApplList.ApplGetCopayAmount(PatId, 0, Temp, InsType)))
            If (TheCopay = "-1") Then
                TheCopay = "0"
            End If
        End If
    End If
    lblInsurer.Caption = "Insurer: " + Temp
    If (val(TheCopay) > 0) Then
        Call DisplayDollarAmount(TheCopay, Copay)
        lblCopay.Caption = "Copay: $" + Copay
    Else
        lblCopay.Caption = "Copay: None Required"
    End If
    'PBal = ApplList.ApplGetBalancebyPatient(PatId)
    Call ApplList.ApplGetAllBalancebyPatient(PatId, PBal, 0, True)
    Call DisplayDollarAmount(Trim(Str(PBal)), BalText)
    lblBalance.Caption = "Patient Balance: $ " + BalText
    txtAmt.Text = Trim(Str(CPay))
    txtPayRef.Text = Trim(CRef)
    lstMethod.ListIndex = -1
    For i = 1 To lstMethod.ListCount - 1
        If (Mid(lstMethod.List(i), 40, 1) = CMeth) Then
            lstMethod.ListIndex = i
            Exit For
        End If
    Next i
    LoadCollect = True
End If
End Function

Private Sub Form_Activate()
QuitOn = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If (HomeOn) Then
    CopayAmount = 0
    CopayMethod = ""
    CopayRef = ""
End If
Unload frmCollectPayment
End Sub

Private Sub txtAmt_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 10) And (KeyAscii <> 13) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
Else
    KeyAscii = 0
End If
End Sub
Public Sub FrmClose()
Call cmdQuit_Click
Unload Me
End Sub
