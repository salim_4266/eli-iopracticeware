VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmHome 
   BackColor       =   &H00785211&
   BorderStyle     =   0  'None
   Caption         =   "Admin"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   FillColor       =   &H00785211&
   ForeColor       =   &H00785211&
   Icon            =   "Home.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleMode       =   0  'User
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdMain15 
      Height          =   975
      Left            =   9000
      TabIndex        =   7
      Top             =   2520
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0442
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain29 
      Height          =   975
      Left            =   9000
      TabIndex        =   29
      Top             =   4680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0639
   End
   Begin VB.Timer Timer2 
      Interval        =   10000
      Left            =   120
      Top             =   3240
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   15
      Left            =   7560
      TabIndex        =   46
      Top             =   4680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0821
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   14
      Left            =   5880
      TabIndex        =   45
      Top             =   4680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0A06
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   13
      Left            =   4200
      TabIndex        =   44
      Top             =   4680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0BEB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   12
      Left            =   2520
      TabIndex        =   43
      Top             =   4680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0DD0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   11
      Left            =   7560
      TabIndex        =   42
      Top             =   3600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":0FB5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   10
      Left            =   5880
      TabIndex        =   41
      Top             =   3600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":119A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   9
      Left            =   4200
      TabIndex        =   40
      Top             =   3600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":137F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   8
      Left            =   2520
      TabIndex        =   39
      Top             =   3600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":1564
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   7
      Left            =   7560
      TabIndex        =   38
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":1749
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   6
      Left            =   5880
      TabIndex        =   37
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":192E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   5
      Left            =   4200
      TabIndex        =   36
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":1B13
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   4
      Left            =   2520
      TabIndex        =   35
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":1CF8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   3
      Left            =   7560
      TabIndex        =   34
      Top             =   1440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":1EDD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   2
      Left            =   5880
      TabIndex        =   33
      Top             =   1440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":20C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   1
      Left            =   4200
      TabIndex        =   32
      Top             =   1440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":22A7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAccess 
      Height          =   975
      Index           =   0
      Left            =   2520
      TabIndex        =   31
      Top             =   1440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":248C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRxRequest 
      Height          =   975
      Left            =   7320
      TabIndex        =   30
      Top             =   4680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":2671
   End
   Begin VB.Timer Timer1 
      Interval        =   6000
      Left            =   120
      Top             =   2400
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain19 
      Height          =   975
      Left            =   9000
      TabIndex        =   15
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":285E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdINet 
      Height          =   975
      Left            =   7320
      TabIndex        =   19
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":2A4B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain6 
      Height          =   975
      Left            =   3360
      TabIndex        =   13
      Top             =   2520
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":2C38
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain17 
      Height          =   975
      Left            =   5640
      TabIndex        =   12
      Top             =   6240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":2E26
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain1 
      Height          =   975
      Left            =   1440
      TabIndex        =   9
      Top             =   2520
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":300C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   975
      Left            =   1440
      TabIndex        =   2
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15386752
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":31FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain16 
      Height          =   975
      Left            =   5640
      TabIndex        =   1
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":33DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain3 
      Height          =   975
      Left            =   1440
      TabIndex        =   0
      Top             =   1440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":35C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain2 
      Height          =   975
      Left            =   1440
      TabIndex        =   4
      Top             =   3600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":37AF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain9 
      Height          =   975
      Left            =   3360
      TabIndex        =   8
      Top             =   1440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":399D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain7 
      Height          =   975
      Left            =   3360
      TabIndex        =   10
      Top             =   3600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":3B8B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain8 
      Height          =   975
      Left            =   3360
      TabIndex        =   11
      Top             =   4680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":3D76
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain10 
      Height          =   975
      Left            =   1440
      TabIndex        =   3
      Top             =   4680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":3F61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClockIn 
      Height          =   975
      Left            =   3360
      TabIndex        =   20
      Top             =   6240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":4148
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLogin 
      Height          =   975
      Left            =   1440
      TabIndex        =   21
      Top             =   6240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11370015
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":4339
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClockOut 
      Height          =   975
      Left            =   3360
      TabIndex        =   23
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":451D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain28 
      Height          =   975
      Left            =   9000
      TabIndex        =   25
      Top             =   6240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":470C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain24 
      Height          =   975
      Left            =   7320
      TabIndex        =   27
      Top             =   6240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":48F9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain22 
      Height          =   975
      Left            =   9000
      TabIndex        =   22
      Top             =   1440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":4AE0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain13 
      Height          =   975
      Left            =   5640
      TabIndex        =   16
      Top             =   2520
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":4CD2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain5 
      Height          =   975
      Left            =   7320
      TabIndex        =   6
      Top             =   1440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":4EC1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain12 
      Height          =   975
      Left            =   7320
      TabIndex        =   17
      Top             =   3600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":50B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain4 
      Height          =   975
      Left            =   5640
      TabIndex        =   5
      Top             =   1440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":52A2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain11 
      Height          =   975
      Left            =   5640
      TabIndex        =   14
      Top             =   3600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":5493
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain26 
      Height          =   975
      Left            =   7320
      TabIndex        =   26
      Top             =   2520
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":5686
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRxStatus 
      Height          =   975
      Left            =   5640
      TabIndex        =   47
      Top             =   4680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":5875
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMain30 
      Height          =   975
      Left            =   9000
      TabIndex        =   48
      Top             =   2520
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":5A5D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDirectMsgng 
      Height          =   975
      Left            =   9000
      TabIndex        =   49
      Top             =   3600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14327845
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Home.frx":5C46
   End
   Begin VB.Image Image1 
      Height          =   1155
      Left            =   1440
      Top             =   120
      Width           =   2835
   End
   Begin VB.Label lblLoc 
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Date/Time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   5640
      TabIndex        =   28
      Top             =   8400
      Width           =   1080
   End
   Begin VB.Label lblWho 
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Date/Time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   7320
      TabIndex        =   24
      Top             =   8400
      Visible         =   0   'False
      Width           =   1080
   End
   Begin VB.Label lblNow 
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Date/Time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   1440
      TabIndex        =   18
      Top             =   8400
      Width           =   1080
   End
End
Attribute VB_Name = "frmHome"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Implements KeyboardHook
Public shortcut As Boolean
Public SCInd As Integer
Private ShowOldHomeScreenOnNextActivate As Boolean
Private PreviousPatientId As String
Private ClockedStatus As String
Private AccessAds() As String
Private IsExitInProgress As Boolean

Private Type LASTINPUTINFO
cbSize As Long
dwTime As Long
End Type
Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Declare Function GetLastInputInfo Lib "user32" (plii As Any) As Long
Private AutoLogOff As Boolean

Dim manager
Dim HomeScreenComWrapper As comWrapper

Private Sub cmdAccess_Click(Index As Integer)
    If Trim$(cmdAccess(Index).Tag) <> "" Then
        TriggerINet Trim$(cmdAccess(Index).Tag)
    End If
End Sub

Private Sub cmdDirectMsgng_Click()
Dim oeRx As New CeRx
    If Login() Then
        oeRx.strUserId = UserLogin.iId
        oeRx.nLocID = dbPracticeId
        oeRx.ClinicalMessagingSummary
    End If
End Sub

Private Sub cmdMain1_Click()
Dim DoAgain As Boolean
Dim TheDate As String
If Login() Then
    If UserLogin.HasPermission(epCheckIn) Then
        Call FormatTodaysDate(TheDate, False)
        DoAgain = True
        While (DoAgain)
            frmCheckInPatient.CurrentAction = ""
            frmCheckInPatient.AppointmentDate = TheDate
            If (frmCheckInPatient.AppointmentLoadList) Then
                frmCheckInPatient.Show 1
                If (Left(frmCheckInPatient.CurrentAction, 4) = "Home") Then
                    DoAgain = False
                End If
            Else
                frmEventMsgs.Header = "All Patients Checked-In"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                DoAgain = False
            End If
        Wend
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain2_Click()
Dim DoAgain As Boolean
Dim TheDate As String
If Login() Then
    If UserLogin.HasPermission(epCheckOut) Then
        Call FormatTodaysDate(TheDate, False)
        DoAgain = True
        While (DoAgain)
            frmCheckoutPatient.ActivityDate = TheDate
            If (frmCheckoutPatient.ActivityLoadList) Then
                frmCheckoutPatient.CurrentAction = ""
                frmCheckoutPatient.Show 1
                If (Left(frmCheckoutPatient.CurrentAction, 4) = "Home") Then
                    DoAgain = False
                ElseIf (frmCheckoutPatient.ActivityId > 0) Then
                    frmDisposition.CurrentAction = ""
                    frmDisposition.ReferenceDate = frmCheckoutPatient.ActivityDate
                    frmDisposition.ActivityId = frmCheckoutPatient.ActivityId
                    frmDisposition.PatientId = frmCheckoutPatient.PatientId
                    frmDisposition.AppointmentId = frmCheckoutPatient.AppointmentId
                    frmDisposition.ReferralId = frmCheckoutPatient.ReferralId
                    If (frmDisposition.LoadDispositionDisplay(True)) Then
                        frmDisposition.Show 1
                        If (frmDisposition.CurrentAction = "Home") Then
                            DoAgain = False
                        End If
                    End If
                Else
                    frmEventMsgs.Header = "Already Checked-Out"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    DoAgain = False
                End If
            Else
                frmEventMsgs.Header = "All Appointments Checked-Out"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                DoAgain = False
            End If
        Wend
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdClockIn_Click()
    frmEventMsgs.Header = "You have not clocked in."
    If (Clocking("I")) Then
        frmEventMsgs.Header = "You have clocked in."
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End Sub


Private Sub cmdLogin_Click()
    If (cmdLogin.Text <> "Log Off" And Login()) Then
        If (Trim(UserLogin.sNameLong) <> "") Then
            cmdLogin.Text = "Log Off"
        End If
    Else
        cmdLogin.Text = "Login"
        
        Call cmdExit_Click
    End If
End Sub

Private Sub cmdMain22_Click()
Dim PatId As Long
Dim AName As String
Dim DoAgain As Boolean
Dim ReturnArguments As Variant
If Login() Then
        DoAgain = True
        While (DoAgain)
            frmHome.Enabled = False
            PatId = 0
            Set ReturnArguments = DisplayPatientSearchScreen
            If Not (ReturnArguments Is Nothing) Then
                PatId = ReturnArguments.PatientId
                AName = Trim(ReturnArguments.PatientLastName)
            End If
            If (PatId > 0) Then
                frmNewInsured.PatientId = 0
                If (frmNewInsured.PatientLoadDisplay(PatId, True, AName)) Then
                    frmNewInsured.Show 1
                    DoAgain = False
                End If
            ElseIf (PatId = -1) Then
                PatId = 0
                frmNewInsured.PatientId = 0
                If (frmNewInsured.PatientLoadDisplay(PatId, True, AName)) Then
                    frmNewInsured.Show 1
                    DoAgain = False
                End If
            Else
                DoAgain = False
            End If
            frmHome.Enabled = True
        Wend
End If
End Sub

Private Sub cmdClockOut_Click()
    frmEventMsgs.Header = "You have not clocked out."
    If (Clocking("O")) Then
        frmEventMsgs.Header = "You have clocked out."
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End Sub

Private Sub cmdMain24_Click()
Dim PermOn As Boolean
Dim Ref As String
Dim Rec As Long
Dim DoAgain As Boolean
If Login() Then
    If UserLogin.HasPermission(epContacts) Then
        DoAgain = True
        While (DoAgain)
            frmHome.Enabled = False
            Call frmSelectDialogue.BuildSelectionDialogue("CONTACTS")
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                If (UCase(frmSelectDialogue.Selection) = "CREATE CONTACTS") Then
                    frmVendors.TheType = "~"
                    frmVendors.DeleteOn = False
                    If (frmVendors.VendorLoadDisplay(0, PermOn)) Then
                        frmVendors.Show 1
                    End If
                Else
                    frmVendors.TheType = ""
                    frmVendors.DeleteOn = False
                    frmVendors.DeleteOn = CheckConfigCollection("REMOVEACTIVE") = "T"
                    Rec = val(Mid(frmSelectDialogue.Selection, 57, Len(frmSelectDialogue.Selection) - 56))
                    If (frmVendors.VendorLoadDisplay(Rec, PermOn)) Then
                        frmVendors.Show 1
                    End If
                End If
            Else
                DoAgain = False
            End If
            frmHome.Enabled = True
        Wend
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain26_Click()
Dim NewForm As Boolean
NewForm = True
If Login() Then
        If NewForm Then
            'TO DO Add New .Net Patient Payment Screen Logic Here.
        Else
            'frmPLineItem.BatchOn = True
            'frmPLineItem.PatientId = 0
            'frmPLineItem.Show 1
        End If
End If
End Sub

Private Sub cmdMain28_Click()
If Login() Then
    If UserLogin.HasPermission(epPracticeNotes) Then
        frmHome.Enabled = False
        frmNotes.NoteId = 0
        frmNotes.SystemReference = ""
        frmNotes.PatientId = -1
        frmNotes.AppointmentId = -1
        frmNotes.CurrentAction = ""
        frmNotes.SetTo = "T"
        frmNotes.MaintainOn = True
        If (frmNotes.LoadNotes) Then
            frmNotes.Show 1
        End If
        frmHome.Enabled = True
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain29_Click()
If Login() Then
    frmEventMsgs.Header = "Spectacles or CL ?"
    frmEventMsgs.AcceptText = "Specs"
    frmEventMsgs.RejectText = "CL"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Others"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        frmCLAdd.OrderType = "G"
        frmCLAdd.Show 1
    ElseIf (frmEventMsgs.Result = 2) Then
        frmCLAdd.OrderType = "C"
        frmCLAdd.Show 1
    End If
End If
End Sub

Private Sub cmdMain30_Click()
    Set manager = BatchBuilderComWrapper.Instance
    Call manager.ShowBatchBuilder
End Sub

Private Sub cmdRxRequest_Click()
If Login() Then
    Dim oeRx As New CeRx
    oeRx.strProviderID = UserLogin.iId
    oeRx.nLocID = dbPracticeId
    oeRx.RenewalRequests
End If
End Sub

Private Sub cmdMain5_Click()
Dim PatId As Long
Dim DoAgain As Boolean
Dim ReturnArguments As Variant
If Login() Then
    If UserLogin.HasPermission(epPatientClinicalData) Then
        DoAgain = True
        While (DoAgain)
            PatId = 0
            Set ReturnArguments = DisplayPatientSearchScreen
            If Not (ReturnArguments Is Nothing) Then PatId = ReturnArguments.PatientId
            
            If (PatId > 0) Then
                frmReviewAppts.StartDate = ""
                If (frmReviewAppts.LoadApptsList(PatId, 0, False)) Then
                    frmReviewAppts.Show 1
                Else
                    frmEventMsgs.Header = "No Appointments Listed"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    DoAgain = False
                End If
            Else
                DoAgain = False
            End If
        Wend
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain15_Click()
If Login() Then
    If UserLogin.HasPermission(epClinicalExchange) Then
        frmPayable.BookingOn = False
        frmPayable.Show 1
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain10_Click()
If Login() Then
    Dim CalendarComWrapper As New comWrapper
    Call CalendarComWrapper.Create(MultiCalendarViewManagerType, emptyArgs)
    Call CalendarComWrapper.InvokeMethod("ShowMultiCalendar", emptyArgs)
End If
End Sub

Private Sub cmdExit_Click()
    IsExitInProgress = True

    Call InsertLog("frmHome.frm", "Admin Interface Closed", LoginCatg.AppExit, "", "", 0, 0)
    
    ' Ensure all active forms are closed
    Call CloseAllActiveForms
    
    Call CloseAllForms
    
    ' Log out
    If UserLogin.IsLoggedIn Then
        UserLogin.LogoffUser
    End If

    ' Cleanup & close active forms
    Call dbPinpointClose
    
    ' Close application
    Unload Me
    
    ' Perform log out via restart of the app
    ' Note: we have found bugs with app functionality after logging out with forms/windows open
     If AutoLogOff Then
        'Starting New Instance
        Call StartNewInstanceOfThisApp
        'Kill the CurrentProcess,Statement "END" is throwing an exception when an Event Call is made from .Net to Vb6
        Call KillCurrentProcess
    Else: End
    End If
End Sub

Public Sub CloseAllForms()
Dim iLoop As Integer
Dim iHighestForm As Integer
If Not Screen.ActiveForm Is Nothing Then
    iHighestForm = Screen.ActiveForm.Count
    iLoop = iHighestForm
    On Error Resume Next
    Do Until iLoop = 0
        Call Forms(iLoop).FrmClose
        iLoop = iLoop - 1
    Loop
End If
End Sub
 
Private Sub cmdMain4_Click()
Dim PatId As Long
Dim Temp As String
Dim DoAgain As Boolean
Dim ReturnArguments As Variant
If Login() Then
    DoAgain = True
    While (DoAgain)
        PatId = 0
        Set ReturnArguments = DisplayPatientSearchScreen
        If Not (ReturnArguments Is Nothing) Then PatId = ReturnArguments.PatientId
        
        If (PatId > 0) Then
            Dim PatientDemographics As New PatientDemographics
            PatientDemographics.PatientId = PatId
            Call PatientDemographics.DisplayPatientFinancialScreen
        Else
            DoAgain = False
        End If
    Wend
End If
End Sub

Private Sub cmdINet_Click()
Dim iIndex As Integer
Dim sTag As String
Dim sText As String
    If cmdINet.Text <> "Home" Then
        cmdMain1.Visible = False
        cmdMain2.Visible = False
        cmdMain3.Visible = False
        cmdMain4.Visible = False
        cmdMain5.Visible = False
        cmdMain6.Visible = False
        cmdMain7.Visible = False
        cmdMain8.Visible = False
        cmdMain9.Visible = False
        cmdMain10.Visible = False
        cmdMain11.Visible = False
        cmdMain12.Visible = False
        cmdMain13.Visible = False
        cmdMain15.Visible = False
        cmdMain16.Visible = False
        cmdMain17.Visible = False
        cmdMain19.Visible = False
        cmdClockIn.Visible = False
        cmdLogin.Visible = False
        cmdMain22.Visible = False
        cmdClockOut.Visible = False
        cmdMain24.Visible = False
        cmdMain26.Visible = False
        cmdMain28.Visible = False
        cmdMain29.Visible = False
        cmdRxRequest.Visible = False
        cmdRxStatus.Visible = False
        cmdDirectMsgng.Visible = False
        cmdExit.Visible = False
        SetAccessButtonsInvisible
        DisplayAccessAds
        cmdINet.Text = "Home"
    Else
        cmdMain1.Visible = True
        cmdMain2.Visible = True
        cmdMain3.Visible = True
        cmdMain4.Visible = True
        cmdMain5.Visible = True
        cmdMain6.Visible = True
        cmdMain7.Visible = True
        cmdMain8.Visible = True
        cmdMain9.Visible = True
        cmdMain10.Visible = True
        cmdMain11.Visible = True
        cmdMain12.Visible = True
        cmdMain13.Visible = True
        cmdMain15.Visible = True
        cmdMain16.Visible = True
        cmdMain17.Visible = True
        cmdMain19.Visible = True
        cmdClockIn.Visible = True
        cmdLogin.Visible = True
        cmdMain22.Visible = True
        cmdClockOut.Visible = True
        cmdMain24.Visible = True
        cmdMain26.Visible = True
        cmdMain28.Visible = True
        cmdMain29.Visible = True
        cmdRxRequest.Visible = True
        cmdRxStatus.Visible = True
        cmdDirectMsgng.Visible = True
        cmdExit.Visible = True
        cmdINet.Text = "Favorite Links"
        SetAccessButtonsInvisible
    End If
End Sub

Private Sub cmdMain9_Click()
Dim DoAgain As Boolean
Dim TheDate As String
If Login() Then
    If UserLogin.HasPermission(epPatientLocator) Then
        Call FormatTodaysDate(TheDate, False)
        frmPatientLocator.CurrentAction = ""
        frmPatientLocator.LocationId = 0
        frmPatientLocator.AppointmentDate = TheDate
        If (frmPatientLocator.AppointmentLoadList) Then
            frmPatientLocator.Show 1
        Else
            frmEventMsgs.Header = "No Patients"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    Else
        frmEventMsgs.InfoMessage "Not Permissioned"
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain19_Click()
Dim PatId As Long
Dim DoAgain As Boolean
Dim ReturnArguments As Variant
If Login() Then
        DoAgain = True
        If UserLogin.HasPermission(epPatientNotes) Then
            While (DoAgain)
                PatId = 0
                Set ReturnArguments = DisplayPatientSearchScreen
                If Not (ReturnArguments Is Nothing) Then
                    PatId = ReturnArguments.PatientId
                End If
                If (PatId > 0) Then
                    frmNotes.NoteId = 0
                    frmNotes.SystemReference = ""
                    frmNotes.PatientId = PatId
                    frmNotes.AppointmentId = 0
                    frmNotes.CurrentAction = ""
                    frmNotes.SetTo = "C"
                    frmNotes.MaintainOn = True
                    If (frmNotes.LoadNotes) Then
                        frmNotes.Show 1
                        If (frmNotes.CurrentAction = "Home") Then
                            DoAgain = False
                        End If
                    End If
                Else
                    DoAgain = False
                End If
            Wend
        Else
            frmEventMsgs.InfoMessage "Not Permissioned"
            frmEventMsgs.Show 1
        End If
    End If
End Sub

Private Sub cmdMain13_Click()
Dim PatientId As Long
Dim DoAgain As Boolean
If Login() Then
        'TO DO Add New Insurer Payments Screen Here
'        frmLineItem.BatchOn = True
'        frmLineItem.PatientId = 0
'        frmLineItem.InsurerId = 0
'        frmLineItem.Show 1
End If
End Sub

Private Sub cmdMain11_Click()
If Login() Then
    If UserLogin.HasPermission(epReview) Then
        frmReview.SendOn = False
        frmReview.PatientId = 0
        frmReview.Show 1
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain3_Click()
Dim PatId As Long
Dim DoAgain As Boolean
Dim ReturnArguments As Variant
If Login() Then
    DoAgain = True
    While (DoAgain)
        frmHome.Enabled = False
        PatId = 0
        Set ReturnArguments = DisplayPatientSearchScreen
        If Not (ReturnArguments Is Nothing) Then PatId = ReturnArguments.PatientId
        
        Dim PatientDemographics As New PatientDemographics
        If (PatId > 0) Then
            PatientDemographics.PatientId = PatId
            Call PatientDemographics.DisplayPatientInfoScreen
        Else
            DoAgain = False
        End If
        frmHome.Enabled = True
    Wend
End If
End Sub

Private Sub cmdMain16_Click()
If Login() Then
    frmSchedulerSetup.Show 1
End If
End Sub

Private Sub cmdMain12_Click()
If Login() Then
    If UserLogin.HasPermission(epSubmit) Then
        frmReview.SendOn = True
        frmReview.PatientId = 0
        frmReview.Show 1
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain8_Click()
If Login() Then
    If UserLogin.HasPermission(epExamRoom) Then
        If (frmExamMonitor.LoadExamRoom(True, "E")) Then
            frmExamMonitor.Show 1
        Else
            frmEventMsgs.Header = "No One Waiting"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain6_Click()
If Login() Then
    If UserLogin.HasPermission(epWaitingForQuestionnaire) Then
        If (frmExamMonitor.LoadExamRoom(True, "W")) Then
            frmExamMonitor.Show 1
        Else
            frmEventMsgs.Header = "No One Waiting"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMain7_Click()
If Login() Then
    If UserLogin.HasPermission(epWaitingForQuestionnaire) Then
        If (frmExamMonitor.LoadExamRoom(True, "M")) Then
            frmExamMonitor.Show 1
        Else
            frmEventMsgs.Header = "No One Waiting"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    Else
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdRxStatus_Click()
Dim oeRx As New CeRx
    If Login() Then
        oeRx.strUserId = UserLogin.iId
        oeRx.nLocID = dbPracticeId
        oeRx.StatusSummary
    End If
End Sub

Private Sub Form_Activate()
    If ShowOldHomeScreenOnNextActivate Then
        ShowOldHomeScreenOnNextActivate = False
    Else
        ' This allows automatically returning to .NET home after completing some activity in old home screen
        Me.DisplayNewHomeScreen
    End If
End Sub

Private Sub Form_Load()
' Begin showing splash screen immediately
frmSplash.Whom = "A"
frmSplash.Show
DoEvents

AutoLogOff = False
'shortcut = True
SCInd = 9

Dim ApplTbl As ApplicationTables
PreviousPatientId = 0
Call EstablishDirectory

Set UserLogin = New CUserLogin
ClockedStatus = "Clocked Out"
lblWho.Visible = False
      
'AutoLogin
If Not (UserLogin.IsLoggedIn) Then
    If (Login) Then
        DoEvents
        
        'Add handler
        Set Keyboardhandler.KeyboardHook = Me
        Keyboardhandler.HookKeyboard
        
        Call InitializeComWrapper
        LoadConfigCollection
        UserLogin.SetTaskMonitor
        SetGlobalDefaults
        Call Timer1_Timer
        AccessAds() = LoadAccessAds
        
        cmdMain29.Visible = CheckConfigCollection("INVENTORYON") = "T"
        Call InsertLog("frmHome.frm", "Opened Admin Interface", LoginCatg.AppOpen, "", "", 0, 0)
        InitSetting
                
        If shortcut Then
            Unload Me
        '============================================
        Select Case SCInd
        Case 1
            If (frmPendingClaims.LoadPendingClaims(True, 0, "", True)) Then
                frmPendingClaims.Show 1
            End If
        

        
        '============================================
        Case 6
        
            If frmPhysicianSpi.LoadInfo Then
                frmPhysicianSpi.Show
            End If
        
        '============================================
        Case 7
            
            frmDisposition.CurrentAction = ""
            frmDisposition.ReferenceDate = "04/21/2010"
            frmDisposition.ActivityId = 702
            frmDisposition.PatientId = 2923
            frmDisposition.AppointmentId = 4540
            frmDisposition.ReferralId = 0
            If (frmDisposition.LoadDispositionDisplay(True)) Then
                frmDisposition.Show
            End If
        
        '============================================
        Case 8
            frmDisposition.Show
            
        '============================================
            
        '============================================
            
        End Select
        End If
        Timer2.Enabled = True
        Unload frmSplash
        
        ' Open .NET Home
            Me.DisplayNewHomeScreen
    End If
End If
End Sub

Public Function DisplayNewHomeScreen()
    ' Show new home screen
    Me.Hide
    
    Dim HomeCloseAction As Integer
    Set HomeScreenComWrapper = New comWrapper
    Call HomeScreenComWrapper.Create(HomeScreenViewManagerType, emptyArgs)
    ' Use splash screen to ensure IO icon will always be present in taskbar when switching between .NET and VB6
    frmSplash.ShowWithIcon
    HomeCloseAction = HomeScreenComWrapper.Instance.ShowHome
    ' Hide splash screen
    frmSplash.HideWithIcon
    
    ShowOldHomeScreenOnNextActivate = True
    
    ' Handle close action if we are not existing app already
    If (Not IsExitInProgress) Then
        Select Case HomeCloseAction
            Case 1 'Showing old home
                Me.Show
            Case Else 'Exit application
                cmdExit.Value = True
        End Select
    End If
End Function

Public Function Login() As Boolean
    If LenB(UserLogin.sNameLong) Then
        Login = True
    Else
        Login = UserLogin.DisplayLoginScreen
        If Not Login And Not UserLogin.IsLoggedIn Then End
        
        Dim ApplTbl As New ApplicationTables
        Call ApplTbl.ApplGetPractice(dbPracticeId, Replace(dbSoftwareLocation, "Office-", ""))
        Set ApplTbl = Nothing
        If (dbPracticeId > 0) Then
            dbPracticeId = dbPracticeId + 1000
        End If
    
        lblLoc.Caption = dbSoftwareLocation
        
        If LenB(UserLogin.sNameLong) Then
            Login = True
            If UserLogin.HasPermission(epPowerUser) Then
                frmHome.BorderStyle = 1
                frmHome.ClipControls = True
                frmHome.Caption = Mid$(frmHome.Name, 4, Len(frmHome.Name) - 3)
                frmHome.AutoRedraw = True
                frmHome.Refresh
            End If
        End If
    End If
    If (Login) Then
        cmdLogin.Text = "Log Off"
    End If
End Function
Private Function Clocking(IType As String) As Boolean
Dim myAudit As Audit
Dim ClockUser As CUserLogin
    Set ClockUser = New CUserLogin
    Set FSignIn.FormLogin = ClockUser
    FSignIn.Show 1
    If LenB(ClockUser.sNameLong) Then
        Set myAudit = New Audit
        If IType <> "" Then
            myAudit.PostClocking ClockUser.iId, IType
        End If
        myAudit.GetClockStatus ClockUser.iId, ClockedStatus
        Set myAudit = Nothing
        lblWho.Caption = ClockUser.sNameLong & " " & ClockedStatus
        lblWho.Visible = True
        Clocking = True
    End If
    Set ClockUser = Nothing
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim comWrapper As New comWrapper
    comWrapper.Uninitialize
    Keyboardhandler.UnhookKeyboard
End Sub

Private Sub Timer1_Timer()
Dim TheDate As String
Dim TheTime As String
TheDate = ""
Call FormatTodaysDate(TheDate, False)
TheTime = ""
Call FormatTimeNow(TheTime)
lblNow.Caption = TheDate + " " + TheTime + Space(6) + "V" + AdministratorVersionId
End Sub

Public Function GetPreviousPatientId() As Long
GetPreviousPatientId = PreviousPatientId
End Function

Public Function SetPreviousPatientId(PatId As Long) As Boolean
SetPreviousPatientId = True
If (PatId > 0) Then
    PreviousPatientId = PatId
End If
End Function

Private Function SetAccessButtonsInvisible() As Boolean
    Dim iIndex As Long
    For iIndex = 0 To 15
        cmdAccess(iIndex).Visible = False
    Next iIndex
End Function

Private Function DisplayAccessAds() As Boolean
Dim iIndex As Long
Dim iPos As Long
Dim sTag As String
Dim sText As String
    For iIndex = 0 To 15
        If Trim$(AccessAds(iIndex)) <> "" Then
            sTag = ""
            sText = Trim$(Mid$(AccessAds(iIndex), 5, Len(AccessAds(iIndex)) - 4))
            iPos = InStrPS(sText, ":")
            If iPos > 0 Then
                sTag = Trim$(Mid$(sText, iPos + 1, Len(sText) - iPos))
                sText = Trim$(Left$(sText, iPos - 1))
            End If
            cmdAccess(iIndex).Tag = sTag
            cmdAccess(iIndex).Text = sText
            cmdAccess(iIndex).Visible = True
        End If
    Next iIndex
End Function

Private Sub Timer2_Timer()
If (UserLogin.IsLoggedIn) Then
    Dim lii As LASTINPUTINFO
    lii.cbSize = Len(lii)
    Dim LogoffTime As String
    LogoffTime = CheckConfigCollection("AUTOLOGOFF")
    If LogoffTime <> "" Then
        Call GetLastInputInfo(lii)
        If (FormatNumber((GetTickCount() - lii.dwTime) / 1000, 3)) >= LogoffTime * 60 Then
            ' Log out
            AutoLogOff = True
            Call cmdLogin_Click
        End If
    End If
End If
End Sub
Private Function KeyboardHook_InvokeHomeScreen() As Boolean
 If HomeScreenComWrapper Is Nothing Then Exit Function
 If HomeScreenComWrapper.Instance Is Nothing Then Exit Function
 
 Call HomeScreenComWrapper.Instance.CloseHome
 Set HomeScreenComWrapper = Nothing
End Function
