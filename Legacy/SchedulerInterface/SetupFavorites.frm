VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSetupFavorites 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtDrugRec 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      MaxLength       =   128
      TabIndex        =   30
      Top             =   6840
      Visible         =   0   'False
      Width           =   10815
   End
   Begin VB.Frame frProp 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   3855
      Left            =   120
      TabIndex        =   21
      Top             =   1440
      Visible         =   0   'False
      Width           =   2295
      Begin fpBtnAtlLibCtl.fpBtn cmdOcular 
         Height          =   615
         Index           =   1
         Left            =   0
         TabIndex        =   22
         Top             =   120
         Width           =   2295
         _Version        =   131072
         _ExtentX        =   4048
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "SetupFavorites.frx":0000
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdFamily 
         Height          =   615
         Index           =   2
         Left            =   0
         TabIndex        =   23
         Top             =   3000
         Width           =   2295
         _Version        =   131072
         _ExtentX        =   4048
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "SetupFavorites.frx":01E1
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdFamily 
         Height          =   615
         Index           =   1
         Left            =   0
         TabIndex        =   24
         Top             =   1920
         Width           =   2295
         _Version        =   131072
         _ExtentX        =   4048
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "SetupFavorites.frx":03BC
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOcular 
         Height          =   615
         Index           =   0
         Left            =   0
         TabIndex        =   28
         Top             =   720
         Width           =   2295
         _Version        =   131072
         _ExtentX        =   4048
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "SetupFavorites.frx":0597
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Category 2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Index           =   2
         Left            =   0
         TabIndex        =   26
         Top             =   2640
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Category 1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Index           =   3
         Left            =   0
         TabIndex        =   25
         Top             =   1560
         Width           =   1155
      End
   End
   Begin MSFlexGridLib.MSFlexGrid flxAvailable 
      Height          =   5415
      Left            =   2520
      TabIndex        =   13
      Top             =   960
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   9551
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   7104808
      ForeColor       =   16777215
      BackColorBkg    =   7104808
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLines       =   0
      GridLinesFixed  =   1
      ScrollBars      =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid flxSelected 
      Height          =   5415
      Left            =   7320
      TabIndex        =   14
      Top             =   960
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   9551
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   7104808
      ForeColor       =   16777215
      BackColorBkg    =   7104808
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLines       =   0
      GridLinesFixed  =   1
      ScrollBars      =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.ListBox lstSystem 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   3570
      ItemData        =   "SetupFavorites.frx":077C
      Left            =   120
      List            =   "SetupFavorites.frx":077E
      TabIndex        =   0
      Top             =   960
      Width           =   2295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10200
      TabIndex        =   2
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupFavorites.frx":0780
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   8
      Top             =   7200
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupFavorites.frx":095F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   990
      Left            =   6240
      TabIndex        =   9
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupFavorites.frx":0B3E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   4440
      TabIndex        =   10
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupFavorites.frx":0D1D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCopy 
      Height          =   990
      Left            =   8040
      TabIndex        =   15
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupFavorites.frx":0EFE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   990
      Left            =   2280
      TabIndex        =   16
      Top             =   7200
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupFavorites.frx":10DD
   End
   Begin VB.Frame frAll 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   4335
      Left            =   120
      TabIndex        =   17
      Top             =   960
      Visible         =   0   'False
      Width           =   11775
      Begin MSFlexGridLib.MSFlexGrid lstFamily 
         Height          =   5415
         Left            =   2400
         TabIndex        =   20
         Top             =   0
         Visible         =   0   'False
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   9551
         _Version        =   393216
         Rows            =   1
         Cols            =   3
         FixedCols       =   0
         BackColor       =   7104808
         ForeColor       =   16777215
         BackColorBkg    =   7104808
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         GridLines       =   0
         GridLinesFixed  =   1
         ScrollBars      =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
      End
      Begin MSFlexGridLib.MSFlexGrid lstAll 
         Height          =   5415
         Index           =   0
         Left            =   2400
         TabIndex        =   18
         Top             =   0
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   9551
         _Version        =   393216
         Rows            =   1
         Cols            =   0
         FixedCols       =   0
         BackColor       =   7104808
         ForeColor       =   16777215
         BackColorBkg    =   7104808
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         GridLines       =   0
         GridLinesFixed  =   1
         ScrollBars      =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
      End
      Begin MSFlexGridLib.MSFlexGrid lstAll 
         Height          =   5415
         Index           =   1
         Left            =   7200
         TabIndex        =   29
         Top             =   0
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   9551
         _Version        =   393216
         Cols            =   0
         FixedCols       =   0
         BackColor       =   7104808
         ForeColor       =   16777215
         BackColorSel    =   7104808
         ForeColorSel    =   16777215
         BackColorBkg    =   7104808
         GridColor       =   7104808
         GridColorFixed  =   7104808
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         Enabled         =   -1  'True
         FocusRect       =   0
         GridLines       =   0
         GridLinesFixed  =   1
         ScrollBars      =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
      End
   End
   Begin VB.TextBox txtRank 
      Height          =   375
      Left            =   1080
      MaxLength       =   2
      TabIndex        =   1
      Top             =   5760
      Width           =   735
   End
   Begin VB.TextBox txtDosage 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      MaxLength       =   128
      TabIndex        =   11
      Top             =   6480
      Width           =   10815
   End
   Begin VB.Label lblSel 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "lblSel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Visible         =   0   'False
      Width           =   570
   End
   Begin VB.Label lblAvailable 
      BackColor       =   &H00808000&
      Caption         =   "Diagnosis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2520
      TabIndex        =   5
      Top             =   600
      Width           =   4620
   End
   Begin VB.Label lblDosage 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Dosage"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   12
      Top             =   6600
      Width           =   870
   End
   Begin VB.Label lblRank 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Rank"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   360
      TabIndex        =   7
      Top             =   5880
      Width           =   570
   End
   Begin VB.Label lblSelected 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Selected"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7320
      TabIndex        =   6
      Top             =   600
      Width           =   990
   End
   Begin VB.Label lblSystem 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   795
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Setup Diagnosis Favorites"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   360
      TabIndex        =   3
      Top             =   240
      Width           =   11235
   End
   Begin VB.Label lblAll 
      BackColor       =   &H00808000&
      Caption         =   "All Meds"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2520
      TabIndex        =   19
      Top             =   600
      Width           =   4620
   End
End
Attribute VB_Name = "frmSetupFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private TheType As String
Private TheDiag As String
' Each msFlexGrid needs the next two for sort assistance.  Need IsRowChosen due to the FlexGrid's
' poor handling of rows 0 and 1.
Private iSortAvailable As Integer
Private iColumnAvailable As Integer
Private fIsAvailableRowChosen As Integer
Private iSortSelected As Integer
Private iColumnSelected As Integer
Private fIsSelectedRowChosen As Integer
Private fCopyMode As Boolean
Private sCopyFrom As String

Dim ShowAll As Boolean
Dim OcuSw As Boolean
Dim famSw As Integer

Dim SelectedLst As MSFlexGrid

Const ButtonSelectionColor As Long = 14745312
Const TextSelectionColor  As Long = 0
Const BaseBackColor  As Long = &H9B9626
Const BaseTextColor  As Long = &HFFFFFF


Private Sub cmdAll_Click()

ShowAll = Not ShowAll
frAll.Visible = ShowAll
flxAvailable.Visible = Not ShowAll
lblAvailable.Visible = Not ShowAll
flxSelected.Visible = Not ShowAll
lblSelected.Visible = Not ShowAll

resetAll
If ShowAll Then
    cmdAll.Text = "Practice Meds"
    LoadAll
Else
    cmdAll.Text = "All Meds"
    Call flxAvailable_RowColChange
    Call flxAvailable_MouseUp(0, 0, 0, 0)
End If
End Sub

Private Sub LoadAll()

frmAlphaPad.NumPad_Field = "All Meds"
frmAlphaPad.NumPad_Result = ""
frmAlphaPad.NumPad_Quit = False
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If frmAlphaPad.NumPad_Quit Then
    cmdAll_Click
    Exit Sub
End If


Dim SearchString As String
SearchString = Trim(frmAlphaPad.NumPad_Result)

Dim RS As Recordset
Dim DispString As String

With lstAll(1)
    If .Cols = 0 Then
        .Cols = 2
        .ColWidth(0) = 900
        .ColWidth(1) = .Width - .ColWidth(0)
        .TextMatrix(0, 0) = "Form"
        .TextMatrix(0, 1) = "Strength"
        .ColAlignment(0) = flexAlignLeftCenter
        .ColAlignment(1) = flexAlignLeftCenter
    End If
    .Rows = 1
End With
With lstAll(0)
    .Visible = False
    If .Cols = 0 Then
        .Cols = 2
        .ColWidth(0) = 0
        .ColWidth(1) = .Width
        .TextMatrix(0, 1) = "Name"
        .ColAlignment(1) = flexAlignLeftCenter
    End If
    .Rows = 1
    
    Dim cDrug As New cDrug
    cDrug.DisplayName = SearchString
    Set RS = cDrug.GetDrugListAll
    Do Until RS.EOF
        DispString = vbTab & RS("displayname")
        .AddItem DispString
        RS.MoveNext
    Loop
    Set cDrug = Nothing
    .Visible = True
    .SetFocus
    If .Rows > 1 Then
        .Row = 1
        .col = 1
    End If
End With
End Sub

Private Sub cmdApply_Click()
    Unload frmSetupFavorites
End Sub

Private Sub cmdBack_Click()
    Unload frmSetupFavorites
End Sub

Private Sub cmdCopy_Click()
    fCopyMode = True
    sCopyFrom = "<" + Trim$(Mid$(lstSystem.List(lstSystem.ListIndex), 75, 6))
    flxAvailable.Enabled = False
    flxSelected.Enabled = False
    lblTitle.Caption = "Please select the employee on the left to whom you'd like to copy these favorites."
    DoEvents
End Sub

Private Sub cmdDelete_Click()

If cmdAll.Visible Then
    Select Case SelectedLst.Name
    Case "flxAvailable"
        frmEventMsgs.Header = "You will permanently delete this drug from table"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If frmEventMsgs.Result = 2 Then
            GoSub RemoveSelection
        End If
    Case "flxSelected"
        GoSub DeleteSelection
    End Select
Else
    GoSub DeleteSelection
End If
Exit Sub
    
RemoveSelection:
    Dim clsDrug As New cDrug
    With clsDrug
        .DrugId = flxAvailable.TextMatrix(flxAvailable.Row, 2)
        If .SelectDrug(0) Then
            .KillDrug
        End If
    End With
    Set clsDrug = Nothing
    
    LoadFavorites "R"
Return
'------------------------------------
DeleteSelection:
    Dim RetDiag As PracticeFavorites
    If (fIsSelectedRowChosen > 0) Then
        Set RetDiag = New PracticeFavorites
        RetDiag.FavoritesId = flxSelected.TextMatrix(flxSelected.Row, 4)
        If (RetDiag.RetrieveFavorites) Then
            Call RetDiag.DeleteFavorites
        End If
        Select Case TheType
            Case "D"
                Call LoadFavoritesList(Left$(lstSystem.List(lstSystem.ListIndex), 1))
            Case "d"
                Call LoadFavoritesList("!")
            Case "R"
                Call LoadFavoritesList("-")
            Case "F"
                Call LoadFavoritesList("*")
            Case "T"
                Call LoadFavoritesList("<" + Trim$(Mid$(lstSystem.List(lstSystem.ListIndex), 75, 6)))
            Case "E"
                Call LoadFavoritesList("?")
            Case "L"
                Call LoadFavoritesList("{")
        End Select
        flxSelected.Row = 0
        fIsSelectedRowChosen = 0
        flxAvailable.Row = 0
        fIsAvailableRowChosen = 0
        txtRank.Text = ""
        txtDosage.Text = ""
        txtDrugRec.Text = ""
        TheDiag = ""
        Set RetDiag = Nothing
    Else
        frmEventMsgs.Header = "Select a favorite to remove"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Return
End Sub

Private Sub cmdFamily_Click(Index As Integer)
If flxAvailable.Row = 0 Then Exit Sub



famSw = Index
cmdFamily(Index).BackColor = ButtonSelectionColor
cmdFamily(Index).ForeColor = TextSelectionColor

Dim i As Integer
With lstFamily
    If .Rows = 1 Then
        Dim RetDrug As New DiagnosisMasterDrugs
        Dim RS As Recordset
        Set RS = RetDrug.FindDrugFamilies
        Set RetDrug = Nothing
        .Rows = 3
        .TextMatrix(1, 1) = " Close List"
        .TextMatrix(2, 1) = " None"
        i = 3
        Do Until RS.EOF
            .Rows = i + 1
            .TextMatrix(i, 1) = RS("code")
            .TextMatrix(i, 2) = RS("id")
            RS.MoveNext
            i = i + 1
        Loop
        .RowSel = 1
    End If
    If Not ShowAll Then
        flxAvailable.Enabled = False
        flxAvailable.Left = 7320
        lblAvailable.Left = 7320
    End If
    lblAll.Caption = "Categories"
    frAll.Visible = True
    .Visible = True
    .SetFocus
End With
End Sub

Private Sub cmdOcular_Click(Index As Integer)
If flxAvailable.Row = 0 Then Exit Sub


SetOcular Index
If Not ShowAll Then
    flxAvailable.TextMatrix(flxAvailable.Row, 3) = Index
    UpdateDrug
End If

End Sub

Private Sub SetOcular(Index As Integer)
OcuSw = (Index = 1)

Dim i As Integer
For i = 0 To 1
    If i = Index Then
        cmdOcular(i).BackColor = ButtonSelectionColor
        cmdOcular(i).ForeColor = TextSelectionColor
    Else
        cmdOcular(i).BackColor = BaseBackColor
        cmdOcular(i).ForeColor = BaseTextColor
    End If
Next
End Sub


Private Sub cmdPost_Click()
Dim tmp As String

If cmdAll.Visible Then
    GoSub IfExisted
    Select Case SelectedLst.Name
    Case "flxAvailable", "flxSelected"
        GoSub PostSelection
    Case "lstAll"
        Dim mem As String
        Dim i As Integer
        mem = lstAll(0).TextMatrix(lstAll(0).Row, 1)
        GoSub PostAll
        LoadFavorites ("R")
        For i = 1 To flxAvailable.Rows - 1
            If flxAvailable.TextMatrix(i, 1) = mem Then
                flxAvailable.Row = i
                flxAvailable.TopRow = i
                flxAvailable.col = 0
                flxAvailable.ColSel = flxAvailable.Cols - 1
                Exit For
            End If
        Next
        cmdAll_Click
    End Select
Else
    GoSub PostSelection
End If
Exit Sub

'----------------------------------------------------------------------
IfExisted:
Dim FGC As MSFlexGrid
Dim Test As String
If ShowAll Then
    Test = lstAll(0).TextMatrix(lstAll(0).Row, 1)
    Set FGC = flxAvailable
Else
    Test = flxAvailable.TextMatrix(flxAvailable.Row, 1)
    Set FGC = flxSelected
End If
Dim r As Integer
For r = 1 To FGC.Rows - 1
    If FGC.TextMatrix(r, 1) = Test Then
        frmEventMsgs.Header = "Already Set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        TheDiag = ""
        flxAvailable.Row = 0
        fIsAvailableRowChosen = 0
        Exit Sub
    End If
Next
Return
'----------------------------------------------------------------------
PostAll:
Dim clsDrug As New cDrug
With clsDrug
    .DrugId = 0
    .DisplayName = lstAll(0).TextMatrix(lstAll(0).Row, 1)
    .Focus = OcuSw
    .Family1 = cmdFamily(1).Text
    .Family2 = cmdFamily(2).Text
    .PutDrug
End With
Set clsDrug = Nothing
Return
'----------------------------------------------------------------------
PostSelection:
Dim RetDiag As PracticeFavorites
Set RetDiag = New PracticeFavorites
If (Trim$(TheDiag) <> "") Then
    If (Trim$(txtRank.Text) <> "") Then
        If (fIsSelectedRowChosen > 0) Then
            RetDiag.FavoritesId = flxSelected.TextMatrix(flxSelected.Row, 4)
        Else
            RetDiag.FavoritesId = 0
        End If
        If (RetDiag.RetrieveFavorites) Then
            RetDiag.FavoritesDiagnosis = Trim$(TheDiag)
            Select Case TheType
                Case "D"
                    RetDiag.FavoritesSystem = Left$(lstSystem.List(lstSystem.ListIndex), 1)
                Case "d"
                    RetDiag.FavoritesSystem = "!"
                    RetDiag.FavoritesDiagnosis = Left$(lstSystem.List(lstSystem.ListIndex), 1) & "-" & Trim$(TheDiag)
                Case "R"
                    RetDiag.FavoritesSystem = "-"
                Case "F"
                    RetDiag.FavoritesSystem = "*"
                Case "T"
                    RetDiag.FavoritesSystem = "<" + Trim$(Mid$(lstSystem.List(lstSystem.ListIndex), 75, 5))
                Case "E"
                    RetDiag.FavoritesSystem = "?"
                Case "L"
                    RetDiag.FavoritesSystem = "{"
            End Select
            RetDiag.FavoritesRank = val(Trim$(txtRank.Text))
            If TheType = "R" Then
                tmp = Trim$(txtDrugRec.Text)
                tmp = Mid(tmp, InStrPS(1, tmp, "-2/") + 3)
                RetDiag.FavoritesAlternate = tmp
            Else
                RetDiag.FavoritesAlternate = Trim$(txtDosage.Text)
            End If
            Call RetDiag.ApplyFavorites
            Select Case TheType
                Case "D"
                    Call LoadFavoritesList(Left$(lstSystem.List(lstSystem.ListIndex), 1))
                Case "d"
                    Call LoadFavoritesList("!")
                Case "R"
                    Call LoadFavoritesList("-")
                Case "F"
                    Call LoadFavoritesList("*")
                Case "T"
                    Call LoadFavoritesList("<" + Trim$(Mid$(lstSystem.List(lstSystem.ListIndex), 75, 6)))
                Case "E"
                    Call LoadFavoritesList("?")
                Case "L"
                    Call LoadFavoritesList("{")
            End Select
            If Not cmdAll.Visible Then
                flxSelected.Row = 0
                fIsSelectedRowChosen = 0
                flxAvailable.Row = 0
                fIsAvailableRowChosen = 0
            End If
            txtRank.Text = ""
            txtDosage.Text = ""
            txtDrugRec.Text = ""
            TheDiag = ""
        End If
    Else
        frmEventMsgs.Header = "Please enter a rank."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRank.SetFocus
    End If
Else
    frmEventMsgs.Header = "Please select an available item from the left."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set RetDiag = Nothing
Return
End Sub

Public Function LoadFavorites(IType As String) As Boolean
    Dim RetDrug As DiagnosisMasterDrugs
    Dim RetProc As DiagnosisMasterProcedures
    Dim RetDr As SchedulerResource
    Dim RetCLInv As CLInventory
    Dim DisplayText As String
    Dim i As Integer
    Dim g As Integer
    Dim TheId As Long
    Dim PostIt As Boolean
    TheType = IType
    lstSystem.Clear
    Call ResetAvailable
    Call ResetSelected
    lblDosage.Visible = False
    txtDosage.Visible = False
    cmdCopy.Visible = False
    txtDosage.Text = ""
    txtDrugRec.Text = ""
    lblAvailable.Caption = "Available Favorites"
    lblSelected.Caption = "Current Favorites"
    fCopyMode = False
    Select Case IType
        Case "D", "d" ' D is Diagnosis option and d is Super Diagnosis option
            lblTitle.Caption = "Setup Diagnosis Favorites"
            With lstSystem
                .AddItem "A - External"
                .AddItem "B - Pupils"
                .AddItem "C - EOM"
                .AddItem "D - VF"
                .AddItem "E - Lids Lacrimal"
                .AddItem "F - Conj/Sclera"
                .AddItem "G - Cornea"
                .AddItem "H - AC"
                .AddItem "I - Iris"
                .AddItem "J - Lens"
                .AddItem "K - Vitreous"
                .AddItem "L - ON/Glauc"
                .AddItem "M - BV"
                .AddItem "N - Macula"
                .AddItem "O - Retina Choriod"
                .AddItem "P - Periph Retina"
                .AddItem "Q - Systemic"
                .AddItem "R - Visual"
            End With
            If (TheType = "d") Then
                lblTitle.Caption = "Setup Global Diagnosis Favorites"
                Call LoadFavoritesList("!")
            End If
        Case "R" ' Drugs option
        
            lblAvailable.Caption = "Practice Meds"
            lblSelected.Caption = "Favorites Meds"
            
            lblDosage.Visible = True
            txtDosage.Visible = True
            lblTitle.Caption = "Setup Drug Favorites"
            lblSystem.Visible = False
            lstSystem.Visible = False
'            Set RetDrug = New DiagnosisMasterDrugs
'            RetDrug.DrugName = Chr(1)
'            RetDrug.DrugRank = 0
'            RetDrug.DrugGenericName = ""
'            RetDrug.DrugDiagnosisAlias1 = ""
'            RetDrug.DrugDiagnosisAlias2 = ""
'            RetDrug.DrugDiagnosisAlias3 = ""
            Dim clsDrug As New cDrug
            Dim RS As Recordset
            Set RS = clsDrug.GetDrugList
            With flxAvailable
'                If (RetDrug.FindDrugbyName > 0) Then
'                    i = 1
'                    While (RetDrug.SelectDrug(i))
'                        .Rows = i + 1
'                        .TextMatrix(i, 1) = Trim$(RetDrug.DrugName)
'                        .TextMatrix(i, 2) = RetDrug.DrugId
'                        i = i + 1
'                    Wend
'                End If
                .Rows = 1
                Do Until RS.EOF
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 1) = RS("DisplayName")
                    .TextMatrix(.Rows - 1, 2) = RS("DrugId")
                    If RS("Focus") Then
                        .TextMatrix(.Rows - 1, 3) = "1"
                    Else
                        .TextMatrix(.Rows - 1, 3) = "0"
                    End If
                    .TextMatrix(.Rows - 1, 4) = fixNull(RS("Family1"))
                    .TextMatrix(.Rows - 1, 5) = fixNull(RS("Family2"))
                    RS.MoveNext
                Loop
            End With
'            Set RetDrug = Nothing
            Set clsDrug = Nothing
            Call LoadFavoritesList("-")
        Case "F" ' Drug Families option
            lblTitle.Caption = "Setup Drug Family Favorites"
            lblSystem.Visible = False
            lstSystem.Visible = False
            Set RetDrug = New DiagnosisMasterDrugs
'            RetDrug.DrugName = ""
'            RetDrug.DrugRank = 0
'            RetDrug.DrugGenericName = ""
'            RetDrug.DrugDiagnosisAlias1 = ""
'            RetDrug.DrugDiagnosisAlias2 = ""
'            RetDrug.DrugDiagnosisAlias3 = ""
            With flxAvailable
                Set RS = RetDrug.FindDrugFamilies
                Set RetDrug = Nothing
                i = 1
                Do Until RS.EOF
                    .Rows = i + 1
                    .TextMatrix(i, 1) = RS("code")
                    .TextMatrix(i, 2) = RS("id")
                    RS.MoveNext
                    i = i + 1
                Loop
'''                If (RetDrug.FindDrugAlias > 0) Then
'''                    i = 1
'''                    While (RetDrug.SelectDrugAlias(i))
'''                        .Rows = i + 1
'''                        .TextMatrix(i, 1) = Trim$(RetDrug.DrugDiagnosisAlias1)
'''                        .TextMatrix(i, 2) = GetDrugId(RetDrug.DrugDiagnosisAlias1)
'''                        i = i + 1
'''                    Wend
'''                End If
            End With
            Set RetDrug = Nothing
            Call LoadFavoritesList("*")
        Case "T" ' Type T is the Test Order option
            lblTitle.Caption = "Setup Test Order Favorites"
            lblSystem.Visible = False
            cmdCopy.Visible = True
            DisplayText = Space(75)
            Mid$(DisplayText, 1, 8) = "Practice"
            DisplayText = DisplayText + Trim$(Str$(0))
            lstSystem.AddItem DisplayText
            lstSystem.Visible = True
            Set RetDr = New SchedulerResource
            If (RetDr.FindResourcebyDoctorTech > 0) Then
                i = 1
                While (RetDr.SelectResource(i))
                    DisplayText = Space(75)
                    Mid$(DisplayText, 1, Len(Trim$(RetDr.ResourceDescription))) = Trim$(RetDr.ResourceDescription)
                    TheId = RetDr.ResourceId
                    DisplayText = DisplayText + Trim$(Str$(TheId))
                    lstSystem.AddItem DisplayText
                    i = i + 1
                Wend
            End If
            Set RetDr = Nothing
        Case "E" ' E&M option
            lblDosage.Visible = False
            txtDosage.Visible = False
            lblSystem.Visible = False
            lstSystem.Visible = False
            lblTitle.UseMnemonic = False
            lblTitle.Caption = "Setup E&M Ranking"
            With flxAvailable
                Set RetProc = New DiagnosisMasterProcedures
                RetProc.ProcedureCPT = Chr(1)
                If (RetProc.FindProcedurebyEM > 0) Then
                    i = 1
                    While (RetProc.SelectProcedure(i))
                        .Rows = i + 1
                        .TextMatrix(i, 0) = Trim$(RetProc.ProcedureCPT)
                        .TextMatrix(i, 1) = Trim$(RetProc.ProcedureName)
                        i = i + 1
                    Wend
                End If
                Set RetProc = Nothing
            End With
            Call LoadFavoritesList("?")
        Case "L" 'Contact Lens option
	    Dim idx As Long
            lblDosage.Visible = False
            txtDosage.Visible = False
            lblSystem.Visible = False
            lstSystem.Visible = False
            lblTitle.Caption = "Setup CL Favorites"
            With flxAvailable
                Set RetCLInv = New CLInventory
                RetCLInv.Criteria = Chr(1)
                If (RetCLInv.FindCLSeries > 0) Then
                    idx = 1
                    g = 1
                    While (RetCLInv.SelectCLSeries(idx))
                        .Rows = g + 1
                        If (.Rows > idx) Then
                        .TextMatrix(idx, 0) = Trim$(RetCLInv.Series)
                        .TextMatrix(idx, 2) = RetCLInv.InventoryId
                        PostIt = True
                        End If
                        ' TEST THIS not sure this loop is quite right
                        For g = 1 To .Rows - 2
                        If (.Rows > idx) Then
                            If (Trim$(.TextMatrix(g, 0)) = Trim$(.TextMatrix(idx, 0))) Then
                                PostIt = False
                                Exit For
                            End If
                        End If
                        Next g
                        g = .Rows - 1
                        If (PostIt) Then
                            g = g + 1
                        End If
                        idx = idx + 1
                    Wend
                End If
            End With
            Set RetCLInv = Nothing
            Call LoadFavoritesList("{")
    End Select
    Select Case IType
    Case "R", "F"
    Case Else
            Call SortGridAvailable(False)
    End Select
    LoadFavorites = True
End Function

Private Sub flxAvailable_GotFocus()
If cmdAll.Visible And Not ShowAll Then
    Set SelectedLst = flxAvailable
    setPostRemove
    Call flxAvailable_MouseUp(0, 0, 0, 0)
End If
End Sub

Private Sub flxAvailable_RowColChange()
If cmdAll.Visible Then
    txtRank.Text = ""
    txtDosage.Text = ""
    txtDrugRec.Text = ""
End If
End Sub

Private Sub flxSelected_GotFocus()
If cmdAll.Visible Then
    Set SelectedLst = flxSelected
    setPostRemove
End If
End Sub

Private Sub Form_Activate()
On Error Resume Next
If cmdAll.Visible Then
    SelectedLst.SetFocus
End If
End Sub

Private Sub Form_Load()
    If UserLogin.HasPermission(epPowerUser) Then
        frmSetupFavorites.BorderStyle = 1
        frmSetupFavorites.ClipControls = True
        frmSetupFavorites.Caption = Mid$(frmSetupFavorites.Name, 4, Len(frmSetupFavorites.Name) - 3)
        frmSetupFavorites.AutoRedraw = True
        frmSetupFavorites.Refresh
    End If
    iSortAvailable = 1
    iColumnAvailable = 0
    iSortSelected = 1
    iColumnSelected = 0
    
    ShowAll = False
    frAll.Height = 6015
    lstFamily.ColWidth(0) = 0
    lstFamily.ColWidth(1) = lstFamily.Width
    lstFamily.TextMatrix(0, 1) = "Drugs Families"
    lstFamily.ColAlignment(1) = flexAlignLeftCenter
    Set SelectedLst = flxAvailable
If cmdAll.Visible Then
    setPostRemove
End If
End Sub



Private Sub lstAll_Click(Index As Integer)
If Index = 0 Then
    If lstAll(0).Rows = 1 Then Exit Sub
    Dim clsDrug As New cDrug
    Dim RS As Recordset
    clsDrug.DisplayName = lstAll(0).TextMatrix(lstAll(0).Row, 1)
    Set RS = clsDrug.GetDrugStrAll("")
    Set clsDrug = Nothing
    With lstAll(1)
        .Rows = 1
        Do Until RS.EOF
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = RS("MED_DOSAGE")
            .TextMatrix(.Rows - 1, 1) = RS("MED_STRENGTH_UOM")
            RS.MoveNext
        Loop
    End With
End If
End Sub

Private Sub lstAll_GotFocus(Index As Integer)
If Index = 0 Then
    If cmdAll.Visible Then
        Set SelectedLst = lstAll(Index)
        setPostRemove
        lstAll_Click Index
    End If
Else
    lstAll(0).SetFocus
End If
End Sub

Private Sub lstFamily_DblClick()
lblAll.Caption = "All Meds"
Dim tmp As String
Select Case lstFamily.Row
Case 1
    tmp = cmdFamily(famSw).Text
    GoTo CloseList
Case 2
Case Else
    tmp = lstFamily.TextMatrix(lstFamily.Row, 1)
End Select
cmdFamily(famSw).Text = tmp

CloseList:
If ShowAll Then
    lstAll(0).SetFocus
Else
    flxAvailable.TextMatrix(flxAvailable.Row, famSw + 3) = tmp
    UpdateDrug
    flxAvailable.Left = 2520
    lblAvailable.Left = 2520
    frAll.Visible = False
    flxAvailable.Enabled = True
    flxAvailable.SetFocus
End If

cmdFamily(1).BackColor = BaseBackColor
cmdFamily(1).ForeColor = BaseTextColor
cmdFamily(2).BackColor = BaseBackColor
cmdFamily(2).ForeColor = BaseTextColor
famSw = 0
lstFamily.Visible = False

End Sub

Private Sub lstFamily_GotFocus()
If cmdAll.Visible Then
    Set SelectedLst = lstFamily
    setPostRemove
End If
End Sub

Private Sub lstSystem_Click()
    If (lstSystem.ListIndex >= 0) Then
        If (fCopyMode) Then
            With frmEventMsgs
                .Header = "Overwrite existing favorites for " & Trim$(Left$(lstSystem.List(lstSystem.ListIndex), 75))
                .AcceptText = ""
                .RejectText = "Yes"
                .CancelText = "No"
                .Other0Text = ""
                .Other1Text = ""
                .Other2Text = ""
                .Other3Text = ""
                .Other4Text = ""
                .Show 1
            End With
            If (frmEventMsgs.Result = 2) Then
                If (CopyFavoriteTests(sCopyFrom, "<" + Trim$(Mid$(lstSystem.List(lstSystem.ListIndex), 75)))) Then
                    With frmEventMsgs
                        .Header = "Successfully copied favorites."
                        .AcceptText = ""
                        .RejectText = "OK"
                        .CancelText = ""
                        .Other0Text = ""
                        .Other1Text = ""
                        .Other2Text = ""
                        .Other3Text = ""
                        .Other4Text = ""
                        .Show 1
                    End With
                End If
            End If
            fCopyMode = False
            sCopyFrom = ""
            flxAvailable.Enabled = True
            flxSelected.Enabled = True
            lblTitle.Caption = "Setup Test Order Favorites"
        Else
            If (TheType = "D") Then
                Call LoadDiagsList(Left$(lstSystem.List(lstSystem.ListIndex), 1))
                Call LoadFavoritesList(Left$(lstSystem.List(lstSystem.ListIndex), 1))
            ElseIf (TheType = "d") Then
                Call LoadDiagsList(Left$(lstSystem.List(lstSystem.ListIndex), 1))
            ElseIf (TheType = "T") Then
                Call LoadTestList
                Call LoadFavoritesList("<" + Trim$(Mid$(lstSystem.List(lstSystem.ListIndex), 75)))
            End If
        End If
    End If
End Sub

Private Sub LoadDiagsList(TheSys As String)
    Dim i As Integer
    Dim g As Integer
    Dim RetDiag As DiagnosisMasterPrimary
    Call ResetAvailable
    Set RetDiag = New DiagnosisMasterPrimary
    RetDiag.PrimarySystem = TheSys
    RetDiag.PrimaryDiagnosis = ""
    RetDiag.PrimaryNextLevelDiagnosis = ""
    RetDiag.PrimaryRank = 0
    RetDiag.PrimaryLevel = 0
    RetDiag.PrimaryBilling = False
    If (RetDiag.FindPrimary > 0) Then
        i = 1
        g = 1
        While (RetDiag.SelectPrimary(i))
            If Not (IsPrimaryThere(Trim$(RetDiag.PrimaryNextLevelDiagnosis))) Then
                With flxAvailable
                    .Rows = g + 1
                    .TextMatrix(g, 0) = Trim$(RetDiag.PrimaryNextLevelDiagnosis)
                    If (Trim$(RetDiag.PrimaryLingo) <> "") Then
                        .TextMatrix(g, 1) = Trim$(RetDiag.PrimaryLingo)
                    Else
                        .TextMatrix(g, 1) = Trim$(RetDiag.PrimaryName)
                    End If
                End With
                g = g + 1
            End If
            i = i + 1
        Wend
    End If
    Set RetDiag = Nothing
End Sub

Private Sub LoadTestList()
    Dim i As Integer
    Dim RetTest As DynamicClass
    Call ResetAvailable
    Set RetTest = New DynamicClass
    RetTest.QuestionParty = "T"
    RetTest.QuestionSet = ""
    RetTest.QuestionOrder = ""
    With flxAvailable
        If (RetTest.FindClassForms > 0) Then
            i = 1
            While (RetTest.SelectClassForm(i))
                .Rows = i + 1
                .TextMatrix(i, 0) = Trim$(RetTest.QuestionOrder)
                .TextMatrix(i, 1) = Trim$(RetTest.Question)
                .TextMatrix(i, 2) = RetTest.ClassId
                i = i + 1
            Wend
        End If
    End With
End Sub

Private Sub LoadFavoritesList(TheSys As String)
    Dim RS As Recordset
    Dim i As Integer
    Dim RetTest As DynamicClass
    Dim RetDiag As PracticeFavorites
    Dim RetDrug As DiagnosisMasterDrugs
    Dim sCDrugRx As CDrugRx
    Dim RetCLInv As CLInventory
    Set RetTest = New DynamicClass
    Set RetDiag = New PracticeFavorites
    Set RetDrug = New DiagnosisMasterDrugs
    Set RetCLInv = New CLInventory
    Dim tmp As String
    Call ResetSelected
    TheDiag = ""
    RetDiag.FavoritesSystem = TheSys
    RetDiag.FavoritesDiagnosis = ""
    If (RetDiag.FindFavorites > 0) Then
        i = 1
        With flxSelected
            While (RetDiag.SelectFavorites(i))
                .Rows = i + 1
                .TextMatrix(i, 0) = RetDiag.FavoritesRank
                .TextMatrix(i, 4) = RetDiag.FavoritesId
                Select Case TheType
                    Case "D", "d"
                        .TextMatrix(i, 1) = Trim$(RetDiag.FavoritesDiagnosis)
                        .TextMatrix(i, 2) = Trim$(GetDiagName(Trim$(RetDiag.FavoritesDiagnosis)))
                    Case "R"
                        RetDrug.DrugId = val(Trim$(RetDiag.FavoritesDiagnosis))
                        Set RS = RetDrug.RetrieveDrug_6
                        If Not RS.EOF Then
                            .TextMatrix(i, 1) = RS("DisplayName")
                            .TextMatrix(i, 3) = RetDrug.DrugId
                            
                            Set sCDrugRx = New CDrugRx
                            sCDrugRx.LoadRxFromDB RS("DisplayName") & "-2/" & Trim$(RetDiag.FavoritesAlternate)
                            tmp = sCDrugRx.GetDrugRxFormat("list")
                            .TextMatrix(i, 2) = Trim(Mid(tmp, Len(sCDrugRx.Name) + 1))
                            .TextMatrix(i, 5) = sCDrugRx.GetDrugRxFormat("record2")
                        End If
                    Case "F"
                        RetDrug.DrugId = val(Trim$(RetDiag.FavoritesDiagnosis))
                        If (RetDrug.RetrieveDrug) Then
                            .TextMatrix(i, 1) = Trim$(RetDrug.DrugDiagnosisAlias1)
                            .TextMatrix(i, 3) = RetDrug.DrugId
                        End If
                    Case "T"
                        RetTest.ClassId = val(Trim$(RetDiag.FavoritesDiagnosis))
                        If (RetTest.RetrieveClass) Then
                            .TextMatrix(i, 1) = Trim$(RetTest.QuestionOrder)
                            .TextMatrix(i, 2) = Trim$(RetTest.Question)
                            .TextMatrix(i, 3) = RetTest.ClassId
                        End If
                    Case "E"
                        .TextMatrix(i, 1) = Trim$(RetDiag.FavoritesDiagnosis)
                        .TextMatrix(i, 2) = GetDiagName(Trim$(RetDiag.FavoritesDiagnosis))
                    Case "L"
                        RetCLInv.InventoryId = val(Trim$(RetDiag.FavoritesDiagnosis))
                        If (RetCLInv.RetrieveCLInventory) Then
                            .TextMatrix(i, 1) = Trim$(RetCLInv.Series)
                            .TextMatrix(i, 3) = Trim$(RetDiag.FavoritesDiagnosis)
                        End If
                End Select
                i = i + 1
            Wend
        End With
    End If
    Call SortGridSelected(False)
    Set RetTest = Nothing
    Set RetDiag = Nothing
    Set RetDrug = Nothing
    Set RetCLInv = Nothing
End Sub

Private Function IsPrimaryThere(ADiag As String) As Boolean
    Dim i As Integer
    IsPrimaryThere = False
    For i = 1 To flxAvailable.Rows - 1
        If (ADiag = Trim$(flxAvailable.TextMatrix(i, 0))) Then
            IsPrimaryThere = True
            Exit For
        End If
    Next i
End Function

Private Function GetDrugId(TheAlias As String) As Long
    Dim RetD As DiagnosisMasterDrugs
    GetDrugId = 0
    Set RetD = New DiagnosisMasterDrugs
    RetD.DrugDiagnosisAlias1 = TheAlias
    If (RetD.FindDrugbyName > 0) Then
        If (RetD.SelectDrug(1)) Then
            GetDrugId = RetD.DrugId
        End If
    End If
    Set RetD = Nothing
End Function

Private Function GetDiagName(Diag As String) As String
    Dim i As Integer
    GetDiagName = ""
    For i = 1 To flxAvailable.Rows - 1
        If (Trim$(flxAvailable.TextMatrix(i, 0)) = Trim$(Diag)) Then
            GetDiagName = Trim$(flxAvailable.TextMatrix(i, 1))
            Exit For
        End If
    Next i
End Function

Private Function CopyFavoriteTests(FromResource As String, ToResource As String) As Boolean
    Dim PracFav As PracticeFavorites
    Dim PracFavPost As PracticeFavorites
    Dim i As Integer
    CopyFavoriteTests = False
    Set PracFav = New PracticeFavorites
    PracFav.FavoritesSystem = ToResource
    If (PracFav.FindFavorites > 0) Then
        PracFav.DeleteAllFavorites
    End If
    Set PracFav = Nothing
    Set PracFav = New PracticeFavorites
    PracFav.FavoritesSystem = FromResource
    If (PracFav.FindFavorites > 0) Then
        i = 1
        While (PracFav.SelectFavorites(i))
            Set PracFavPost = New PracticeFavorites
            With PracFavPost
                .FavoritesId = 0
                If (.RetrieveFavorites) Then
                    .FavoritesSystem = ToResource
                    .FavoritesDiagnosis = PracFav.FavoritesDiagnosis
                    .FavoritesRank = PracFav.FavoritesRank
                    .FavoritesAlternate = PracFav.FavoritesAlternate
                    Call .ApplyFavorites
                End If
            End With
            Set PracFavPost = Nothing
            i = i + 1
        Wend
    End If
    Set PracFav = Nothing
    CopyFavoriteTests = True
End Function

Private Sub txtDosage_Click()
If Not SelectedLst.Name = "flxAvailable" Then Exit Sub

    Dim DId As Long
    Dim AText As String
    DId = 0
    AText = ""
    If (fIsSelectedRowChosen > 0) Then
       DId = val(flxSelected.TextMatrix(flxSelected.Row, 3))
    ElseIf (fIsAvailableRowChosen > 0) Then
       DId = val(flxAvailable.TextMatrix(flxAvailable.Row, 2))
    End If
    
    Dim sCDrugRx As New CDrugRx
    Set sCDrugRx = SetRxFavoriteDetails(DId)
    
    If sCDrugRx.Name = "" Then
        txtDosage.Text = ""
        txtDrugRec.Text = ""
    Else
        txtDosage.Text = sCDrugRx.GetDrugRxFormat("list")
        txtDrugRec.Text = sCDrugRx.GetDrugRxFormat("record2")
    End If
    'If (DId > 0) Then
    '    txtDosage.Text = SetRxFavoriteDetails(DId)
    'End If
    Set sCDrugRx = Nothing
End Sub

Private Sub txtDosage_KeyPress(KeyAscii As Integer)
'Call txtDosage_Click
KeyAscii = 0
End Sub

Private Function SetRxFavoriteDetails(DrugId As Long) As CDrugRx

Dim RetDrug As DiagnosisMasterDrugs

    If (DrugId > 0) Then
        Set RetDrug = New DiagnosisMasterDrugs
        RetDrug.DrugId = DrugId
        If (RetDrug.RetrieveDrug) Then
            frmRxDetails.AppointmentId = 0
            Set frmRxDetails.moDrugRx = New CDrugRx
            frmRxDetails.moDrugRx.Id = DrugId
            
            If Not txtDosage.Text = "" Then
                Call frmRxDetails.moDrugRx.LoadRxFromDB(txtDrugRec.Text)
            End If
            
            If (frmRxDetails.LoadRxDetails(False, False, False)) Then
                Set frmRxDetails.DIList = Nothing
                frmRxDetails.Show 1
            End If
        End If
        Set RetDrug = Nothing
        Set SetRxFavoriteDetails = frmRxDetails.moDrugRx
    End If
End Function

Private Function ResetAvailable() As Boolean
    ResetAvailable = False
    With flxAvailable
        .Clear
        .Rows = 2
        .Cols = 3
        Select Case TheType
            Case "D", "d"
                .FormatString = ("<Diagnosis|<Diagnosis Language|NOT USED")
                .ColWidth(0) = 1000
                .ColWidth(1) = 10000
            Case "R"
                .Cols = 6
                .FormatString = ("|<Drug Name|DrugId")
                .ColWidth(0) = 1
                .ColWidth(1) = 10000
            Case "F"
                .FormatString = ("|<Drug Family|DrugId")
                .ColWidth(0) = 1
                .ColWidth(1) = 10000
            Case "T"
                .FormatString = ("<Test|<Test Language|NOT USED")
                .ColWidth(0) = 700
                .ColWidth(1) = 10000
            Case "E"
                .FormatString = ("<CPT|<CPT Language|NOT USED")
                .ColWidth(0) = 700
                .ColWidth(1) = 10000
            Case "L"
                .FormatString = ("<CL Series|NOT USED|InventoryId")
                .ColWidth(0) = 10000
                .ColWidth(1) = 1000
        End Select
        .ColWidth(2) = 1000
    End With
    fIsAvailableRowChosen = 0
    ResetAvailable = True
End Function

Private Function ResetSelected() As Boolean
    ResetSelected = False
    With flxSelected
        .Clear
        .Rows = 1
        .Cols = 6
        Select Case TheType
            Case "D", "d"
                .FormatString = ("<Rank|<Diagnosis|<Diagnosis Language|NOT USED|FavoritesId")
                .ColWidth(0) = 300
                .ColWidth(1) = 1000
                .ColWidth(2) = 10000
            Case "R"
                .FormatString = ("<Rank|<Drug|<Prescription|DrugId|FavoritesId")
                .ColWidth(0) = 300
                .ColWidth(1) = 1800
                .ColWidth(2) = 10000
            Case "F"
                .FormatString = ("<Rank|<Drug Family|NOT USED|DrugId|FavoritesId")
                .ColWidth(0) = 300
                .ColWidth(1) = 10000
                .ColWidth(2) = 1000
            Case "T"
                .FormatString = ("<Rank|<Test|<Test Language|NOT USED|FavoritesId")
                .ColWidth(0) = 300
                .ColWidth(1) = 300
                .ColWidth(2) = 10000
            Case "E"
                .FormatString = ("<Rank|<CPT|<CPT Language|NOT USED|FavoritesId")
                .ColWidth(0) = 300
                .ColWidth(1) = 700
                .ColWidth(2) = 10000
            Case "L"
                .FormatString = ("<Rank|<CL Series|NOT USED|InventoryId|FavoritesId")
                .ColWidth(0) = 300
                .ColWidth(1) = 10000
                .ColWidth(2) = 1000
        End Select
        .ColWidth(3) = 1000
        .ColWidth(4) = 1000
    End With
    fIsSelectedRowChosen = 0
    ResetSelected = True
End Function

Private Sub flxAvailable_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
   ' MSFlexGrid has the strange feature of not being able to recognize
   ' when the heading of 1st fixed column is clicked on, it calls it Row 1,
   ' the Row below this also returns as Row 1. This bit of code below singles
   ' out the heading row which is required in this app for sorting the data.
    Dim i As Integer
    Dim Found As Boolean
    If (y < flxAvailable.RowHeight(flxAvailable.Row)) And Not cmdAll.Visible Then
        Dim WidthTotal As Long
        WidthTotal = 0
        For i = 0 To 2
            WidthTotal = WidthTotal + flxAvailable.ColWidth(i)
            If x < WidthTotal Then
                flxAvailable.col = i
                Exit For
            End If
        Next i
        Call SortGridAvailable
        flxAvailable.Row = 0
        fIsAvailableRowChosen = 0
    ElseIf (y > flxAvailable.Rows * flxAvailable.RowHeight(flxAvailable.Row)) Then
        'flxAvailable.Row = 0
        'fIsAvailableRowChosen = 0
    ElseIf (fIsAvailableRowChosen = flxAvailable.Row) And Not cmdAll.Visible Then
        flxAvailable.Row = 0
        fIsAvailableRowChosen = 0
    Else
        fIsAvailableRowChosen = flxAvailable.Row
        fIsSelectedRowChosen = 0
        Select Case TheType
            Case "D", "d", "E"
                TheDiag = Trim$(flxAvailable.TextMatrix(flxAvailable.Row, 0))
            Case "R", "F", "T", "L"
                TheDiag = Trim$(flxAvailable.TextMatrix(flxAvailable.Row, 2))
        End Select
        If cmdAll.Visible Then
            If flxAvailable.Row = 0 Then
                If flxAvailable.Rows > 1 Then flxAvailable.Row = 1
            End If
            If IsNumeric(flxAvailable.TextMatrix(flxAvailable.Row, 3)) Then
                Call SetOcular(flxAvailable.TextMatrix(flxAvailable.Row, 3))
            Else
                Call SetOcular(0)
            End If
            cmdFamily(1).Text = flxAvailable.TextMatrix(flxAvailable.Row, 4)
            cmdFamily(2).Text = flxAvailable.TextMatrix(flxAvailable.Row, 5)
        Else
            Found = False
            If (TheType = "d") Then
                TheDiag = Left$(lstSystem.List(lstSystem.ListIndex), 1) & "-" & TheDiag
            End If
            For i = 1 To flxSelected.Rows - 1
                Select Case TheType
                    Case "D", "d", "E"
                        If (Trim$(flxSelected.TextMatrix(i, 1)) = TheDiag) Then
                            Found = True
                            Exit For
                        End If
                    Case "R", "F", "T", "L"
                        If (Trim$(flxSelected.TextMatrix(i, 3)) = TheDiag) Then
                            Found = True
                            Exit For
                        End If
                End Select
            Next i
            If (TheType = "d") Then
                TheDiag = Mid$(TheDiag, 3)
            End If
            If (Found) Then
                frmEventMsgs.Header = "Already Set"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                TheDiag = ""
                flxAvailable.Row = 0
                fIsAvailableRowChosen = 0
            End If
        End If
    End If
End Sub

Private Sub SortGridAvailable(Optional fToggleColumn As Boolean = True)
    If fToggleColumn Then
        If flxAvailable.col = iColumnAvailable Then
           ' Same column so toggle sort
           If iSortAvailable = 2 Then
              iSortAvailable = iSortAvailable - 1
           Else
              iSortAvailable = iSortAvailable + 1
           End If
        Else
           iSortAvailable = 1 ' Sort ascending
        End If
    End If
    iColumnAvailable = flxAvailable.col
    flxAvailable.Sort = iSortAvailable
End Sub

Private Sub flxSelected_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    ' MSFlexGrid has the strange feature of not being able to recognize
    ' when the heading of 1st fixed column is clicked on, it calls it Row 1,
    ' the Row below this also returns as Row 1. This bit of code below singles
    ' out the heading row which is required in this app for sorting the data.
    Dim TheTemp As Long
    Dim RxTemp As String
    If (y < flxSelected.RowHeight(flxSelected.Row)) Then
        Dim WidthTotal As Long
        Dim i As Integer
        WidthTotal = 0
        For i = 0 To 2
            WidthTotal = WidthTotal + flxSelected.ColWidth(i)
            If x < WidthTotal Then
                flxSelected.col = i
                Exit For
            End If
        Next i
        Call SortGridSelected
        flxSelected.Row = 0
        fIsSelectedRowChosen = 0
    ElseIf (y > flxSelected.Rows * flxSelected.RowHeight(flxSelected.Row)) Then
        flxSelected.Row = 0
        fIsSelectedRowChosen = 0
    ElseIf (fIsSelectedRowChosen = flxSelected.Row) Then
        flxSelected.Row = 0
        fIsSelectedRowChosen = 0
        txtDosage.Text = ""
        txtDrugRec.Text = ""
    Else
        fIsAvailableRowChosen = 0
        fIsSelectedRowChosen = flxSelected.Row
        txtRank.Text = flxSelected.TextMatrix(flxSelected.Row, 0)
        Select Case TheType
            Case "D", "d"
                TheDiag = Trim$(flxSelected.TextMatrix(flxSelected.Row, 1))
            Case "R"
                TheDiag = Trim$(flxSelected.TextMatrix(flxSelected.Row, 3))
                TheTemp = val(flxSelected.TextMatrix(flxSelected.Row, 4))
                txtDosage.Text = flxSelected.TextMatrix(flxSelected.Row, 2)
                txtDrugRec.Text = flxSelected.TextMatrix(flxSelected.Row, 5)
            Case "F"
                TheDiag = Trim$(flxSelected.TextMatrix(flxSelected.Row, 4))
            Case "T"
                TheDiag = Trim$(flxSelected.TextMatrix(flxSelected.Row, 3))
        End Select
    End If
End Sub

Private Sub SortGridSelected(Optional fToggleColumn As Boolean = True)
    If fToggleColumn Then
        If flxSelected.col = iColumnSelected Then
           ' Same column so toggle sort
           If iSortSelected = 2 Then
              iSortSelected = iSortSelected - 1
           Else
              iSortSelected = iSortSelected + 1
           End If
        Else
           iSortSelected = 1 ' Sort ascending
        End If
    End If
    iColumnSelected = flxSelected.col
    flxSelected.Sort = iSortSelected
End Sub


Private Sub resetAll()
Call SetOcular(0)
cmdFamily(1).Text = ""
cmdFamily(2).Text = ""
famSw = 0
End Sub

Private Sub setPostRemove()
If cmdAll.Visible Then

    lblAll.FontBold = False
    lblAll.FontUnderline = False
    lblAvailable.FontBold = False
    lblAvailable.FontUnderline = False
    lblSelected.FontBold = False
    lblSelected.FontUnderline = False
    
    lblSel.Caption = SelectedLst.Name
    Select Case SelectedLst.Name
    Case "lstFamily"
        lblAll.FontBold = True
        lblAll.FontUnderline = True
        cmdPost.Enabled = True
        cmdDelete.Enabled = True
    Case "flxAvailable"
        lblAvailable.FontBold = True
        lblAvailable.FontUnderline = True
        cmdPost.Enabled = (SelectedLst.Left < 3000)
        cmdDelete.Enabled = True
    Case "flxSelected"
        If lblAvailable.Left < 3000 Then
            lblSelected.FontBold = True
            lblSelected.FontUnderline = True
        Else
            lblAvailable.FontBold = True
            lblAvailable.FontUnderline = True
        End If
        cmdPost.Enabled = (SelectedLst.Left < 3000)
        cmdDelete.Enabled = Not cmdPost.Enabled
    Case "lstAll"
        lblAll.FontBold = True
        lblAll.FontUnderline = True
    End Select
    'Case "flxAvailable"
    'Case "flxSelected"
    'Case "lstAll"
End If
End Sub

Private Sub UpdateDrug()
Dim clsDrug As New cDrug
With flxAvailable
    clsDrug.DrugId = .TextMatrix(.Row, 2)
    clsDrug.DisplayName = .TextMatrix(.Row, 1)
    clsDrug.Focus = (.TextMatrix(.Row, 3) = "1")
    clsDrug.Family1 = .TextMatrix(.Row, 4)
    clsDrug.Family2 = .TextMatrix(.Row, 5)
    clsDrug.PutDrug
End With
Set clsDrug = Nothing
End Sub
Public Sub FrmClose()
Call cmdApply_Click
Unload Me
End Sub

