VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmReview 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdPatientList 
      Height          =   855
      Left            =   4200
      TabIndex        =   27
      Top             =   5160
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLFulFill 
      Height          =   855
      Left            =   8040
      TabIndex        =   25
      Top             =   6120
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":01F2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemit 
      Height          =   855
      Left            =   8040
      TabIndex        =   14
      Top             =   4200
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":03D2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPreCert 
      Height          =   855
      Left            =   4200
      TabIndex        =   3
      Top             =   2280
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":05B8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClaims 
      Height          =   855
      Left            =   360
      TabIndex        =   5
      Top             =   360
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":07A5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRecall 
      Height          =   855
      Left            =   4200
      TabIndex        =   7
      Top             =   360
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":0987
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   10080
      TabIndex        =   0
      Top             =   8040
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":0B69
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRefer 
      Height          =   855
      Left            =   360
      TabIndex        =   1
      Top             =   1320
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":0D48
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdConsult 
      Height          =   855
      Left            =   360
      TabIndex        =   2
      Top             =   2280
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":0F33
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFacAdm 
      Height          =   855
      Left            =   360
      TabIndex        =   6
      Top             =   4200
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":1122
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExternal 
      Height          =   855
      Left            =   360
      TabIndex        =   8
      Top             =   7080
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":1306
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdConfirms 
      Height          =   855
      Left            =   4200
      TabIndex        =   9
      Top             =   1320
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":14F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMisc 
      Height          =   855
      Left            =   360
      TabIndex        =   11
      Top             =   3240
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":16DB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClaimSent 
      Height          =   855
      Left            =   8040
      TabIndex        =   12
      Top             =   1320
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":18CB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdUCharts 
      Height          =   855
      Left            =   360
      TabIndex        =   13
      Top             =   6120
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":1AB1
   End
   Begin VB.TextBox txtClosingDate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      MaxLength       =   10
      TabIndex        =   10
      Top             =   1680
      Visible         =   0   'False
      Width           =   1695
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClaimAck 
      Height          =   855
      Left            =   8040
      TabIndex        =   15
      Top             =   2280
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":1C9D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClaimWrkUp 
      Height          =   855
      Left            =   8040
      TabIndex        =   16
      Top             =   360
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":1E82
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   855
      Left            =   360
      TabIndex        =   17
      Top             =   8040
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":206C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdColl 
      Height          =   855
      Left            =   8040
      TabIndex        =   19
      Top             =   5160
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":224B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLOrders 
      Height          =   855
      Left            =   4200
      TabIndex        =   20
      Top             =   6120
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":2438
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPCOrders 
      Height          =   855
      Left            =   4200
      TabIndex        =   21
      Top             =   7080
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":2624
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPCFulFill 
      Height          =   855
      Left            =   8040
      TabIndex        =   22
      Top             =   7080
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":2812
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTranscrib 
      Height          =   855
      Left            =   4200
      TabIndex        =   23
      Top             =   4200
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":29F9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSummaryReports 
      Height          =   855
      Left            =   8040
      TabIndex        =   24
      Top             =   3240
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":2BDE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPtnChartReq 
      Height          =   855
      Left            =   360
      TabIndex        =   26
      Top             =   5160
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":2DC8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClinicalDataFiles 
      Height          =   855
      Left            =   4200
      TabIndex        =   28
      Top             =   3240
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Review.frx":2FB9
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Loading, Please Wait"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   330
      Left            =   4560
      TabIndex        =   18
      Top             =   3480
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Review What ?"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   5220
      TabIndex        =   4
      Top             =   0
      Width           =   1935
   End
End
Attribute VB_Name = "frmReview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public SendOn As Boolean

Private Sub cmdClaimAck_Click()
If (frmReviewClaimAcks.LoadAcks) Then
    frmReviewClaimAcks.Show 1
Else
    frmEventMsgs.Header = "Acknowledgements"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "No Ack Files Found"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdClaimSent_Click()
If (frmReviewDocs.LoadDocumentsList(SendOn, "^", "", True)) Then
    frmReviewDocs.Show 1
End If
End Sub

Public Sub ShowClaimFollowUp(ActionType As Integer)
    If (ActionType = 1) Then
        If (frmPendingClaims.LoadPendingClaims(True, 0, "@", True)) Then
            frmPendingClaims.Show 1
        Else
            frmEventMsgs.Header = "No Items"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (ActionType = 2) Then
        If (frmPendingClaims.LoadPendingClaims(True, 0, "D", True)) Then
            frmPendingClaims.Show 1
        Else
            frmEventMsgs.Header = "No Items"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (ActionType = 3) Then
        If (frmReviewDocs.LoadDocumentsList(False, "Q", "", True)) Then
            frmReviewDocs.Show 1
        Else
            frmEventMsgs.Header = "No Items"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (ActionType = 5) Then
        If (frmReviewDocs.LoadDocumentsList(False, "[", "", True)) Then
            frmReviewDocs.Show 1
        Else
            frmEventMsgs.Header = "No Items"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (ActionType = 6) Then
        If (frmReviewDocs.LoadDocumentsList(False, "]", "", True)) Then
            frmReviewDocs.Show 1
        Else
            frmEventMsgs.Header = "No Items"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End Sub

Private Sub cmdClaimWrkUp_Click()
    If (SendOn) Then
        Exit Sub
    End If

    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Appeals"
    frmEventMsgs.RejectText = "Denials"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Chart Requests"
    frmEventMsgs.Other1Text = "Appeal Letters"
    frmEventMsgs.Other2Text = "Denial Letters"
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1

    Call ShowClaimFollowUp(frmEventMsgs.Result)
End Sub

Private Sub cmdCLFulFill_Click()
frmPCList.CLFlag = True
frmPCList.cmdOrder.Visible = False
frmPCList.Show 1
End Sub

Private Sub cmdClinicalDataFiles_Click()
    Dim ClinicalDatasFilesComWrapper As New ComWrapper
    Call ClinicalDatasFilesComWrapper.Create(ClinicalDataFilesViewManagerType, emptyArgs)
    Call ClinicalDatasFilesComWrapper.InvokeMethod("ShowClinicalDataFilesGenerator", emptyArgs)
End Sub

Private Sub cmdColl_Click()
Dim DoAgain As Boolean
Dim TestOn As Boolean
TestOn = True
If (TestOn) Then
    frmEventMsgs.Header = "Collection Action."
    frmEventMsgs.AcceptText = "Process"
    frmEventMsgs.RejectText = "SetUp"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (frmCollections.LoadCollections(True, False)) Then
            frmCollections.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        frmCollectionSetup.Show 1
    End If
Else
    DoAgain = True
    While (DoAgain)
        If (frmReviewDocs.LoadDocumentsList(False, "W", "", True)) Then
            frmReviewDocs.Show 1
            DoAgain = False
        Else
            frmEventMsgs.Header = "No Patient Balances over 90 days."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoAgain = False
        End If
   Wend
End If
End Sub

Private Sub cmdHome_Click()
Unload frmReview
End Sub

Private Sub cmdCLOrders_Click()
frmCLOrders.OrderType = "C"
frmCLOrders.Show 1
End Sub

Private Sub cmdPatientList_Click()
Dim frm As New ClinicalReports.frmMain
Dim Helper As New ClinicalReports.DbHelper
Helper.DbObject = IdbConnection
frm.MDBFilePath = LocalPinPointDirectory
frm.OLEDbConnectionString = dbConnectionAccess
frm.UserId = UserLogin.iId
frm.ShowDialog
frm.Close
frm.Dispose
End Sub

Private Sub cmdPCFulFill_Click()
frmPCList.CLFlag = False
frmPCList.Show 1
End Sub

Private Sub cmdPCOrders_Click()
frmCLOrders.OrderType = "G"
frmCLOrders.Show 1
End Sub

Private Sub cmdPtnChartReq_Click()
frmPatientChartReq.FirstTime = True
frmPatientChartReq.Show 1
Unload frmPatientChartReq
End Sub

Private Sub cmdRemit_Click()
'To Do Add Display Fuinctionality for New .Net Remit Screen

'If (frmRemittance.LoadRemittance()) Then
'    frmRemittance.Show 1
'Else
'    frmEventMsgs.Header = "Remittances"
'    frmEventMsgs.AcceptText = ""
'    frmEventMsgs.RejectText = "No Remit Files Found"
'    frmEventMsgs.CancelText = ""
'    frmEventMsgs.Other0Text = ""
'    frmEventMsgs.Other1Text = ""
'    frmEventMsgs.Other2Text = ""
'    frmEventMsgs.Other3Text = ""
'    frmEventMsgs.Other4Text = ""
'    frmEventMsgs.Show 1
'End If
End Sub

Private Sub cmdDone_Click()
Unload frmReview
End Sub

Private Sub cmdClaims_Click()
Dim Temp As String
Dim PartyType As String
Dim DType As String
Dim DoAgain As Boolean
DoAgain = True
If (SendOn) Then
    PartyType = ""
    frmEventMsgs.Header = "Party ?"
    frmEventMsgs.AcceptText = "Patient Statements"
    frmEventMsgs.RejectText = "Electronic Claims"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Paper Claims"
    frmEventMsgs.Other1Text = "Monthly Statements"
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        PartyType = "S"
    ElseIf (frmEventMsgs.Result = 2) Then
        Call ShowSubmitTransactionScreen(1)
        Exit Sub
    ElseIf (frmEventMsgs.Result = 3) Then
        Call ShowSubmitTransactionScreen(2)
        Exit Sub
    ElseIf (frmEventMsgs.Result = 5) Then
        PartyType = "s"
    Else
        Exit Sub
    End If
    While (DoAgain)
        If (frmReviewDocs.LoadDocumentsList(SendOn, "R", PartyType, True)) Then
            frmReviewDocs.Show 1
            DoAgain = False
        Else
            frmEventMsgs.Header = "No Claims"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoAgain = False
        End If
    Wend
Else
    While (DoAgain)
        If (frmPendingClaims.LoadPendingClaims(True, 0, "", True)) Then
            frmPendingClaims.Show 1
            DoAgain = False
        End If
    Wend
End If
End Sub

Private Sub cmdMisc_Click()
Dim DoAgain As Boolean
DoAgain = True
While (DoAgain)
    If Not (SendOn) Then
        If (frmFilterLetters.LoadCriteria(False)) Then
            frmFilterLetters.Show 1
            If (frmFilterLetters.FilterCriteria) Then
                If (frmPendingLetters.LoadPendingLetters) Then
                    frmPendingLetters.Show 1
                    DoAgain = True
                Else
                    frmEventMsgs.Header = "No Misc. Letters for Criteria Selected"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    DoAgain = False
                End If
            Else
                DoAgain = False
            End If
        Else
            DoAgain = False
        End If
    Else
        If (frmReviewDocs.LoadDocumentsList(SendOn, "M", "", True)) Then
            frmReviewDocs.Show 1
            DoAgain = False
        Else
            frmEventMsgs.Header = "No Miscellaneous Letters"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoAgain = False
        End If
    End If
Wend
End Sub

Private Sub cmdSummaryReports_Click()
frmDenials.Show 1
End Sub

Private Sub cmdTranscrib_Click()
Dim DoAgain As Boolean
DoAgain = True
While (DoAgain)
    If (frmReviewDocs.LoadDocumentsList(False, "T", "", True)) Then
        frmReviewDocs.Show 1
        DoAgain = False
    Else
        frmEventMsgs.Header = "No Transcriptions"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoAgain = False
    End If
Wend
End Sub

Private Sub cmdUCharts_Click()
Dim DoAgain As Boolean
DoAgain = True
While (DoAgain)
    If (frmReviewDocs.LoadDocumentsList(SendOn, "C", "", True)) Then
        frmReviewDocs.Show 1
        DoAgain = False
    Else
        frmEventMsgs.Header = "No Incomplete Charts"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoAgain = False
    End If
Wend
End Sub

Private Sub cmdConsult_Click()
frmListLetters.TransStatus = "P"
If (SendOn) Then
    frmListLetters.TransStatus = "B"
End If
frmListLetters.TransType = "S"
 frmListLetters.Show 1
End Sub

Private Sub cmdExternal_Click()
Dim DoAgain As Boolean
DoAgain = True
If UserLogin.HasPermission(epFollowUpAndSurgery) Then
    While (DoAgain)
        If (frmReviewDocs.LoadDocumentsList(SendOn, "X", "", True)) Then
            frmReviewDocs.Show 1
            DoAgain = False
        Else
            frmEventMsgs.Header = "No External Requests"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoAgain = False
        End If
    Wend
Else
    frmEventMsgs.InfoMessage "Not Permissioned"
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdFacAdm_Click()
Dim DoAgain As Boolean
DoAgain = True
If UserLogin.HasPermission(epFollowUpAndSurgery) Then
    While (DoAgain)
        If (frmListFacAdm.LoadFacilityAdminList(True, False, False)) Then
            frmListFacAdm.Show 1
            DoAgain = False
        Else
            frmEventMsgs.Header = "No Facility Admissions"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoAgain = False
        End If
    Wend
Else
    frmEventMsgs.InfoMessage "Not Permissioned"
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdRefer_Click()
frmListLetters.TransStatus = "P"
If (SendOn) Then
    frmListLetters.TransStatus = "B"
End If
frmListLetters.TransType = "F"
frmListLetters.Show 1
End Sub

Private Sub cmdRecall_Click()
Dim IHow As String
Dim DoAgain As Boolean
frmEventMsgs.Header = "Recall Status"
frmEventMsgs.AcceptText = "Sent"
frmEventMsgs.RejectText = "Pend"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = "Closed"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result <> 4) Then
    If (frmEventMsgs.Result = 1) Then
        IHow = "S"
    ElseIf (frmEventMsgs.Result = 3) Then
        IHow = "Z"
    Else
        IHow = "P"
    End If
    DoAgain = True
    While (DoAgain)
        If (frmReviewDocs.LoadDocumentsList(SendOn, "L", IHow, True)) Then
            frmReviewDocs.lstLoc.Visible = False
            frmReviewDocs.lblLoc.Visible = False
            frmReviewDocs.Show 1
            DoAgain = False
        Else
            frmEventMsgs.Header = "No Recalls"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            DoAgain = False
        End If
    Wend
End If
End Sub

Private Sub cmdPreCert_Click()
Dim TheDate As String
Dim DoAgain As Boolean
DoAgain = True
Call FormatTodaysDate(TheDate, False)
While (DoAgain)
    If (frmReviewPrec.LoadApptsbyPreCert(True)) Then
        frmReviewPrec.Show 1
    End If
    DoAgain = False
Wend
End Sub

Private Sub cmdConfirms_Click()
Dim DoAgain As Boolean
DoAgain = True
While (DoAgain)
    If (frmReviewDocs.LoadDocumentsList(SendOn, "J", "", True)) Then
        frmReviewDocs.Show 1
        DoAgain = False
    Else
        frmEventMsgs.Header = "No Confirmations"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoAgain = False
    End If
Wend
End Sub

Private Sub Form_Load()
Dim Temp As String
If UserLogin.HasPermission(epPowerUser) Then
    frmReview.BorderStyle = 1
    frmReview.ClipControls = True
    frmReview.Caption = Mid(frmReview.Name, 4, Len(frmReview.Name) - 3)
    frmReview.AutoRedraw = True
    frmReview.Refresh
End If
If (SendOn) Then
    cmdRecall.Visible = False
    cmdColl.Visible = False
    cmdClaimAck.Visible = False
    cmdClaimWrkUp.Visible = False
    cmdRemit.Visible = False
    cmdUCharts.Visible = False
    cmdConfirms.Visible = False
    cmdPreCert.Visible = False
    cmdClaimSent.Visible = False
    cmdCLOrders.Visible = False
    cmdPCOrders.Visible = False
    cmdPCFulFill.Visible = False
    cmdCLFulFill.Visible = False
    cmdTranscrib.Visible = False
    cmdExternal.Visible = False
    cmdSummaryReports.Visible = False
    cmdMisc.Visible = False
    Label1.Caption = "Submit Transactions ?"
Else
    Label1.Caption = "Pending Transactions"
    If CheckConfigCollection("INVENTORYON") = "T" Then
        cmdCLOrders.Visible = True
        If CheckConfigCollection("GLASSON") = "T" Then
            cmdPCOrders.Visible = True
            cmdPCFulFill.Visible = True
            cmdCLFulFill.Visible = True
        Else
            cmdPCOrders.Visible = False
            cmdPCFulFill.Visible = False
            cmdCLFulFill.Visible = False
        End If
    End If
    cmdPtnChartReq.Visible = True
    cmdPatientList.Visible = True
    cmdMisc.Visible = True
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
Private Sub ShowSubmitTransactionScreen(ByVal ClaimType As Long)
On Error GoTo lShowSubmitTransactionScreen

    Dim arguments() As Variant
    Dim SubmitTransactionComWrapper As New ComWrapper
    Call SubmitTransactionComWrapper.Create(SubmitTransactionsLoadArgumentsType, emptyArgs)
    Dim loadArguments As Object
    Set loadArguments = SubmitTransactionComWrapper.Instance
        
    Call SubmitTransactionComWrapper.InvokeSet("DefaultClaimType", ClaimType)
    
    Call SubmitTransactionComWrapper.Create(SubmitTransactionsViewManagerType, emptyArgs)
    
    arguments = Array(loadArguments)
    Call SubmitTransactionComWrapper.InvokeMethod("ShowSubmitTransactions", arguments)
    
    Exit Sub
lShowSubmitTransactionScreen:
    LogError "frmReview", "ShowSubmitTransactionScreen", Err, Err.Description
End Sub
