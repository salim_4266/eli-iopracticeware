VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPayments 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtAll 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   1080
      MaxLength       =   7
      TabIndex        =   70
      Top             =   2400
      Width           =   1455
   End
   Begin VB.ListBox lstFilter 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "Payments.frx":0000
      Left            =   8040
      List            =   "Payments.frx":0010
      Sorted          =   -1  'True
      TabIndex        =   69
      Top             =   2280
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.TextBox txtReason 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   65
      Top             =   2880
      Width           =   1455
   End
   Begin VB.TextBox txtChkAmt 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   10200
      MaxLength       =   10
      TabIndex        =   8
      Top             =   1080
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.ComboBox lstSrv 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   600
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1920
      Width           =   2055
   End
   Begin VB.TextBox txtDOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      MaxLength       =   10
      TabIndex        =   45
      Top             =   4920
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.ComboBox lstIns 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "Payments.frx":0039
      Left            =   2520
      List            =   "Payments.frx":003B
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   4440
      Visible         =   0   'False
      Width           =   6615
   End
   Begin VB.TextBox txtBillDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   10
      Top             =   4920
      Visible         =   0   'False
      Width           =   6615
   End
   Begin VB.ComboBox lstSetIns 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "Payments.frx":003D
      Left            =   2520
      List            =   "Payments.frx":003F
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   4920
      Visible         =   0   'False
      Width           =   6615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   2520
      TabIndex        =   23
      Top             =   6000
      Visible         =   0   'False
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":0041
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFDone 
      Height          =   990
      Left            =   8160
      TabIndex        =   13
      Top             =   6000
      Visible         =   0   'False
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":0220
   End
   Begin VB.Frame fraAllocate 
      BackColor       =   &H00FF0000&
      BorderStyle     =   0  'None
      Height          =   3255
      Left            =   2400
      TabIndex        =   24
      Top             =   3960
      Visible         =   0   'False
      Width           =   6855
      Begin VB.TextBox txtPatAmt 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4440
         MaxLength       =   10
         TabIndex        =   26
         Top             =   1440
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox txtInsAmt 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4440
         MaxLength       =   10
         TabIndex        =   25
         Top             =   960
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label lblAllocate 
         Alignment       =   2  'Center
         BackColor       =   &H00FF0000&
         Caption         =   "Allocate To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   1800
         TabIndex        =   29
         Top             =   120
         Visible         =   0   'False
         Width           =   3135
      End
      Begin VB.Label lblPatAmt 
         BackColor       =   &H00FF0000&
         Caption         =   "Patient"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   3360
         TabIndex        =   28
         Top             =   1440
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblInsAmt 
         BackColor       =   &H00FF0000&
         Caption         =   "Insurer"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   3360
         TabIndex        =   27
         Top             =   960
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.TextBox txtRefId 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   5160
      MaxLength       =   64
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   2880
      Width           =   4455
   End
   Begin VB.TextBox txtAmount 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   4440
      MaxLength       =   7
      TabIndex        =   3
      Top             =   1440
      Width           =   1575
   End
   Begin VB.TextBox txtDate 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   7080
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1440
      Width           =   1455
   End
   Begin VB.TextBox txtVoidDate 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7080
      MaxLength       =   10
      TabIndex        =   14
      Top             =   1920
      Width           =   1455
   End
   Begin VB.ComboBox lstPayType 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   600
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1440
      Width           =   2895
   End
   Begin VB.ComboBox lstMethod 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   9480
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1440
      Width           =   2415
   End
   Begin VB.ComboBox lstRef 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "Payments.frx":03FF
      Left            =   600
      List            =   "Payments.frx":0418
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   960
      Width           =   7095
   End
   Begin VB.ListBox lstPrint 
      Height          =   840
      ItemData        =   "Payments.frx":04C2
      Left            =   165
      List            =   "Payments.frx":04C4
      Sorted          =   -1  'True
      TabIndex        =   12
      Top             =   4440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox txtCheck 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   9480
      MaxLength       =   20
      TabIndex        =   6
      Top             =   1920
      Width           =   2415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10800
      TabIndex        =   15
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":04C6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   16
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":06A5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRebill 
      Height          =   990
      Left            =   3360
      TabIndex        =   17
      Top             =   7800
      Visible         =   0   'False
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":0884
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEdit 
      Height          =   990
      Left            =   6600
      TabIndex        =   18
      Top             =   7800
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":0A6A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAllocate 
      Height          =   990
      Left            =   7560
      TabIndex        =   19
      Top             =   7800
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":0C49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFinRes 
      Height          =   990
      Left            =   9600
      TabIndex        =   20
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":0E2C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   990
      Left            =   8640
      TabIndex        =   21
      Top             =   7800
      Visible         =   0   'False
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":1017
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   1320
      TabIndex        =   22
      Top             =   7800
      Width           =   1020
      _Version        =   131072
      _ExtentX        =   1799
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":11F7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdIns 
      Height          =   990
      Left            =   4440
      TabIndex        =   31
      Top             =   7800
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":13D7
   End
   Begin VB.ListBox lstPayment 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4380
      ItemData        =   "Payments.frx":15BF
      Left            =   120
      List            =   "Payments.frx":15C1
      TabIndex        =   30
      Top             =   3240
      Width           =   11175
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDos 
      Height          =   990
      Left            =   2400
      TabIndex        =   44
      Top             =   7800
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":15C3
   End
   Begin VB.ListBox lstDetails 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4380
      ItemData        =   "Payments.frx":17A6
      Left            =   120
      List            =   "Payments.frx":17A8
      TabIndex        =   50
      Top             =   3240
      Visible         =   0   'False
      Width           =   11175
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1215
      Left            =   11400
      TabIndex        =   58
      Top             =   3240
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":17AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   1230
      Left            =   11400
      TabIndex        =   59
      Top             =   6360
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   2170
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":1989
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   1215
      Left            =   11400
      TabIndex        =   60
      Top             =   4800
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":1B68
   End
   Begin VB.CheckBox chkInclude 
      BackColor       =   &H0077742D&
      Caption         =   "Include Comment on Statement"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4200
      TabIndex        =   54
      Top             =   2520
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ComboBox lst90 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      ItemData        =   "Payments.frx":1D48
      Left            =   3480
      List            =   "Payments.frx":1D6D
      Style           =   2  'Dropdown List
      TabIndex        =   63
      Top             =   1920
      Visible         =   0   'False
      Width           =   2655
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMClaim 
      Height          =   990
      Left            =   5520
      TabIndex        =   64
      Top             =   7800
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payments.frx":1E54
   End
   Begin VB.ListBox lstTrans 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4380
      ItemData        =   "Payments.frx":203B
      Left            =   120
      List            =   "Payments.frx":203D
      TabIndex        =   68
      Top             =   3240
      Visible         =   0   'False
      Width           =   11175
   End
   Begin VB.Label lblAll 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Allowable"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   71
      Top             =   2400
      Width           =   855
   End
   Begin VB.Label lblAlert 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      Caption         =   "Alert ON"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10200
      TabIndex        =   67
      Top             =   840
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblReason 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Reason"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   9720
      TabIndex        =   66
      Top             =   2880
      Width           =   705
   End
   Begin VB.Label lblChkAmt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Check Amt"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   9120
      TabIndex        =   61
      Top             =   1080
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblRefCert 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Referring Physician"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   7800
      TabIndex        =   57
      Top             =   1080
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.Label lblIParty 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Primary:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   120
      TabIndex        =   56
      Top             =   2880
      Visible         =   0   'False
      Width           =   4050
   End
   Begin VB.Label lblSrv 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CPT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   55
      Top             =   1920
      Width           =   405
   End
   Begin VB.Label lblUnaTotal 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8040
      TabIndex        =   53
      Top             =   120
      Width           =   1935
   End
   Begin VB.Label lblInsTotal 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6120
      TabIndex        =   52
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label lblPatTotal 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4200
      TabIndex        =   51
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label lblTotal 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10080
      TabIndex        =   49
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label lblTPlan 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Third:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   8040
      TabIndex        =   48
      Top             =   600
      Width           =   3855
   End
   Begin VB.Label lblPPlan 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Primary:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   45
      TabIndex        =   47
      Top             =   600
      Width           =   4095
   End
   Begin VB.Label lblSPlan 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Second:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   4200
      TabIndex        =   46
      Top             =   600
      Width           =   3795
   End
   Begin VB.Label lblInvoice 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8280
      TabIndex        =   43
      Top             =   2880
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.Label lblPayRef 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Comment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4200
      TabIndex        =   42
      Top             =   2880
      Width           =   885
   End
   Begin VB.Label lblMethod 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Method"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   8760
      TabIndex        =   41
      Top             =   1440
      Width           =   675
   End
   Begin VB.Label lblPayDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pay Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6120
      TabIndex        =   40
      Top             =   1440
      Width           =   840
   End
   Begin VB.Label lblPayAmt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   3600
      TabIndex        =   39
      Top             =   1440
      Width           =   735
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Post a Payment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   45
      TabIndex        =   38
      Top             =   120
      Width           =   4095
   End
   Begin VB.Label lblPayer 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   60
      TabIndex        =   37
      Top             =   960
      Width           =   495
   End
   Begin VB.Label lblTrans 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   36
      Top             =   1440
      Width           =   465
   End
   Begin VB.Label lblVoidDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Void Dt"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   6195
      TabIndex        =   35
      Top             =   1920
      Width           =   795
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblCheck 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Check #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   8640
      TabIndex        =   33
      Top             =   1920
      Width           =   765
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   2640
      TabIndex        =   32
      Top             =   2400
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.Label lblRefer 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1215
      Left            =   7800
      TabIndex        =   34
      Top             =   1440
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.Label lbl90 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Over 90 Day Reason (Medicaid)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4560
      TabIndex        =   62
      Top             =   1080
      Visible         =   0   'False
      Width           =   2805
   End
End
Attribute VB_Name = "frmPayments"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public ReceivableId As Long
Public CurrentAction As String
Public InvAllocatedBalance As Single
Public AllocBalance As Single
Public UnallocBalance As Single
Public PatientBalance As Single
Public InsurerBalance As Single
Public Whom As Boolean
Public DisplayHow As String
Public AccessType As String

Public fromNew As Boolean

Private ResourceType As Integer
Private DontTrigger As Boolean
Private FirstTimeIn As Boolean
Private FilterType As String
Private DefaultPayDate As String
Private UserId As Long
Private TotalFound As Integer
Private TriggerDone As Boolean
Private TriggerDetails As Boolean
Private RequireCheck As Boolean
Private InvDate As String
Private InsType As String
Private MaxInvoices As Integer
Private CurrentIndex As Integer
Private MidPointIndex As Integer
Private PreviousIndex As Integer
Private PaymentId As Long
Private Invoicedate As String
Private PlanAsPayer As Long
Private PatientAsPayer As Long
Private PayerType As String
Private PaymentType As String
Private TheCheckId As Long
Private PlanId As Long
Private PlanName As String
Private PatientName As String
Private InsuredName As String
Private PolicyHolderid As Long
Private PolicyHolderIdRel As String
Private PolicyHolderIdBill As Boolean
Private SecondPolicyHolderId As Long
Private SecondPolicyHolderIdRel As String
Private SecondPolicyHolderIdBill As Boolean

Public Function LoadPayments(TrigOn As Boolean, InvDt As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim u As Integer
Dim RcvId As Long
Dim OpticalMedicalOn As Boolean
Dim TotalBalance As Single
Dim TheDate As String
Dim Temp As String
Dim LocalDate As ManagedDate
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim l1 As String, l2 As String, l3 As String
Set ApplList = New ApplicationAIList
Set ApplTemp = New ApplicationTemplates
LoadPayments = True
ResourceType = 1
If CheckConfigCollection("OPTICALMEDICALON") = "T" Then
    OpticalMedicalOn = True
    If (UserLogin.sType = "Q") Or (UserLogin.sType = "U") Or (UserLogin.sType = "Y") Then
        ResourceType = 2
    End If
End If
If (CurrentAction <> "") Then
    ResourceType = 0
End If
InsType = ""
UserId = UserLogin.iId
cmdIns.Visible = True
cmdFinRes.Visible = Whom
cmdEdit.Visible = True
cmdIns.Visible = True
If (TrigOn) Then
    DisplayHow = ""
    CurrentIndex = 0
    MidPointIndex = 0
    PreviousIndex = 0
End If
If (DisplayHow <> "") Then
    lblPrint.Caption = "Loading ... "
    lblPrint.Visible = True
    DoEvents
End If
InvDate = InvDt
TheDate = ""
If (Trim(InvDate) <> "") Then
    Set LocalDate = New ManagedDate
    LocalDate.ExposedDate = InvDate
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        TheDate = LocalDate.ExposedDate
    End If
    Set LocalDate = Nothing
End If
PolicyHolderid = 0
PolicyHolderIdRel = ""
PolicyHolderIdBill = False
SecondPolicyHolderId = 0
SecondPolicyHolderIdRel = ""
SecondPolicyHolderIdBill = False
Call ApplTemp.ApplGetAllPayers(PatientId, PatientName, PolicyHolderid, PolicyHolderIdBill, PolicyHolderIdRel, SecondPolicyHolderId, SecondPolicyHolderIdBill, SecondPolicyHolderIdRel, InsType, "", lstRef, l1, l2, l3, True, True, False, True)
Label1.Caption = Trim(PatientName)
If (DisplayHow = "") Then
    cmdAll.Text = "A L L"
Else
    cmdAll.Text = "S U M"
End If
lblPPlan.Caption = "Primary:" + l1
lblSPlan.Caption = "Second:" + l2
lblTPlan.Caption = "Third:" + l3
lstMethod.Clear
lstMethod.ListIndex = -1
Call ApplList.ApplGetCodes("PayableType", True, lstMethod)
lstPayType.Clear
lstPayType.ListIndex = -1
Call ApplList.ApplGetCodes("PaymentType", True, lstPayType)
Set ApplList = Nothing
RequireCheck = Not (CheckConfigCollection("REQUIRECHECK") = "F")
PlanId = 0
lblPayer.Visible = True
lstRef.Visible = True
RcvId = ReceivableId
Set ApplTemp.lstBox = lstPayment
PreviousIndex = MidPointIndex
MidPointIndex = CurrentIndex
If (DisplayHow = "") Then
    Call ApplTemp.ApplLoadInvoiceSummary(PatientId, TheDate, TotalBalance, PatientBalance, InsurerBalance, AllocBalance, UnallocBalance, DisplayHow, CurrentIndex, TotalFound, OpticalMedicalOn, ResourceType)
Else
    DisplayHow = "A"
    cmdAll.Text = "S U M"
    Call ApplTemp.ApplLoadInvoiceSummary(PatientId, TheDate, TotalBalance, PatientBalance, InsurerBalance, AllocBalance, UnallocBalance, DisplayHow, CurrentIndex, TotalFound, OpticalMedicalOn, ResourceType)
    CurrentIndex = 0
    MidPointIndex = 0
    PreviousIndex = 0
End If
Set ApplTemp = Nothing
ReceivableId = RcvId
If (ReceivableId > 0) Then
    For u = 0 To lstPayment.ListCount - 1
        If (Mid(lstPayment.List(u), 75, 1) = "R") Then
            If (ReceivableId = val(Mid(lstPayment.List(u), 76, Len(lstPayment.List(u)) - 75))) Then
                lstPayment.ListIndex = u
                Exit For
            End If
        End If
    Next u
End If
LoadPayments = True
Call DisplayDollarAmount(Trim(Str(PatientBalance)), Temp)
lblPatTotal.Caption = "Patient:" + Trim(Temp)
Call DisplayDollarAmount(Trim(Str(InsurerBalance)), Temp)
lblInsTotal.Caption = "Insurer:" + Trim(Temp)
Call DisplayDollarAmount(Trim(Str(UnallocBalance)), Temp)
lblUnaTotal.Caption = "Unalloc:" + Trim(Temp)
Call DisplayDollarAmount(Trim(Str(TotalBalance)), Temp)
lblTotal.Caption = "Balance:" + Trim(Temp)
LoadPayments = True
lblIParty.Visible = False
lblPrint.Visible = False
lblPayer.Visible = False
lstRef.Visible = False
lblPayAmt.Visible = False
txtAmount.Visible = False
txtAmount.Text = ""
lblPayRef.Visible = False
txtRefId.Visible = False
txtRefId.Text = ""
lblTrans.Visible = False
lstPayType.Visible = False
lstPayType.ListIndex = -1
lblPayDate.Visible = False
lblReason.Visible = False
txtReason.Visible = False
txtReason.Text = ""
txtDate.Visible = False
txtDate.Text = ""
lblMethod.Visible = False
lstMethod.Visible = False
lstMethod.ListIndex = -1
lblVoidDate.Visible = False
txtVoidDate.Visible = False
txtVoidDate.Text = ""
lblCheck.Visible = False
txtCheck.Visible = False
'txtCheck.Text = ""
lblChkAmt.Visible = False
txtChkAmt.Visible = False
lblSrv.Visible = False
lstSrv.Visible = False
lstSrv.ListIndex = -1
lblAll.Visible = False
txtAll.Visible = False
txtAll.Text = ""
chkInclude.Visible = False
cmdDone.Enabled = True
cmdHome.Enabled = True
cmdFinRes.Enabled = True
cmdRebill.Enabled = True
cmdEdit.Enabled = True
cmdAllocate.Enabled = True
cmdPrint.Enabled = True
cmdNotes.Enabled = True
cmdMClaim.Enabled = True
cmdIns.Enabled = True
cmdDos.Enabled = True
cmdAll.Visible = True
cmdPrev.Visible = False
cmdNext.Visible = False
If (TotalFound > 2) And (DisplayHow <> "A") Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
End If
lstPayment.Visible = True
lstPayment.Enabled = True
lstTrans.Visible = False
TriggerDetails = lstDetails.Visible
lstDetails.Visible = False
If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
    lblAlert.Visible = True
End If
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub cmdAll_Click()
If (cmdAll.Text = "A L L") Then
    lstPayment.ListIndex = -1
    DisplayHow = "A"
Else
    DisplayHow = ""
End If
Call ResetOuterList(MidPointIndex, False)
End Sub

Private Sub cmdIns_Click()
Dim PatId As Long
Dim InsId As Long
Dim RcvId As Long
Dim myk As Long
Dim b1 As Boolean, b2 As Boolean
Dim r1 As String, r2 As String
Dim TheDate As String, n1 As String
Dim l1 As String, l2 As String, l3 As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
If (ReceivableId < 1) Then
    frmEventMsgs.Header = "Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    cmdHome.SetFocus
ElseIf (lstTrans.Visible) Then
    Exit Sub
Else
    frmEventMsgs.Header = "Set Claim Details"
    frmEventMsgs.AcceptText = ""
    If (AccessType = "") Then
        frmEventMsgs.AcceptText = "Set Insurer"
    End If
    frmEventMsgs.RejectText = "Set Bill Dr"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Set Over 90"
    frmEventMsgs.Other1Text = "Sent Items"
    frmEventMsgs.Other2Text = ""
    If Not (lstDetails.Visible) Then
        frmEventMsgs.Other2Text = "Set As Primary"
    End If
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        lstSetIns.Clear
        Set ApplTemp = New ApplicationTemplates
        TheDate = ApplTemp.ApplGetReceivableDate(ReceivableId, PatId, InsId)
        If (Trim(TheDate) <> "") Then
            Call ApplTemp.ApplGetAllPayers(PatId, n1, InsId, b1, r1, SecondPolicyHolderId, b2, r2, InsType, TheDate, lstSetIns, l1, l2, l3, True, True, True, True)
            If (lstSetIns.ListCount < 1) Or (InsId <> PolicyHolderid) Then
                If (PolicyHolderid <> PatientId) Then
                    Call ApplTemp.ApplGetAllPayers(PatId, n1, PolicyHolderid, b1, r1, SecondPolicyHolderId, b2, r2, InsType, TheDate, lstSetIns, l1, l2, l3, True, False, True, True)
                Else
                    Call ApplTemp.ApplGetAllPayers(PatientId, n1, PolicyHolderid, b1, r1, SecondPolicyHolderId, b2, r2, InsType, TheDate, lstSetIns, l1, l2, l3, True, False, True, True)
                End If
                If (lstSetIns.ListCount < 1) Then
                    myk = SecondPolicyHolderId
                    Call ApplTemp.ApplGetAllPayers(PatientId, n1, myk, b2, r2, 0, b1, r2, InsType, TheDate, lstSetIns, l1, l2, l3, True, False, True, True)
                End If
            End If
        End If
        Set ApplTemp = Nothing
        If (lstSetIns.ListCount > 0) Then
            lstPayment.Enabled = False
            cmdDone.Enabled = False
            cmdHome.Enabled = False
            cmdFinRes.Enabled = False
            cmdRebill.Enabled = False
            cmdEdit.Enabled = False
            cmdAllocate.Enabled = False
            cmdPrint.Enabled = False
            cmdNotes.Enabled = False
            cmdMClaim.Enabled = False
            cmdIns.Enabled = False
            cmdDos.Enabled = False
            
            fraAllocate.Visible = True
            lblAllocate.Caption = "Set Insurer"
            lblAllocate.Visible = True
            lblInsAmt.Visible = False
            txtInsAmt.Visible = False
            txtInsAmt.Text = ""
            lblPatAmt.Visible = False
            txtPatAmt.Visible = False
            txtPatAmt.Text = ""
            lstIns.Visible = False
            txtBillDr.Visible = False
            txtBillDr.Text = ""
            txtDOS.Visible = False
            lstSetIns.Visible = True
            cmdFDone.Visible = True
            cmdQuit.Visible = True
            lstSetIns.SetFocus
            fraAllocate.ZOrder 0
            lblAllocate.ZOrder 0
            cmdFDone.ZOrder 0
            cmdQuit.ZOrder 0
            lstSetIns.ZOrder 0
        Else
            frmEventMsgs.Header = "No Insurers for Service Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        lstPayment.Enabled = False
        cmdDone.Enabled = False
        cmdHome.Enabled = False
        cmdFinRes.Enabled = False
        cmdRebill.Enabled = False
        cmdEdit.Enabled = False
        cmdAllocate.Enabled = False
        cmdPrint.Enabled = False
        cmdNotes.Enabled = False
        cmdMClaim.Enabled = False
        cmdIns.Enabled = False
        cmdDos.Enabled = False

        fraAllocate.Visible = True
        lblAllocate.Caption = "Set Billing Doctor"
        lblAllocate.Visible = True
        lblInsAmt.Visible = False
        txtInsAmt.Visible = False
        txtInsAmt.Text = ""
        lblPatAmt.Visible = False
        txtPatAmt.Visible = False
        txtPatAmt.Text = ""
        lstIns.Visible = False
        txtBillDr.Visible = True
        txtBillDr.Text = ""
        cmdFDone.Visible = True
        cmdQuit.Visible = True
        txtBillDr.SetFocus
        fraAllocate.ZOrder 0
        lblAllocate.ZOrder 0
        cmdFDone.ZOrder 0
        cmdQuit.ZOrder 0
        txtBillDr.ZOrder 0
    ElseIf (frmEventMsgs.Result = 3) Then
        Set ApplList = New ApplicationAIList
        lst90.ListIndex = ApplList.ApplGet90Day(ReceivableId)
        Set ApplList = Nothing
        lst90.Visible = True
        lbl90.Visible = True
    ElseIf (frmEventMsgs.Result = 5) Then
        lstTrans.Clear
        If (lstPayment.ListIndex >= 0) Then
            RcvId = val(Mid(lstPayment.List(lstPayment.ListIndex), 76, Len(lstPayment.List(lstPayment.ListIndex)) - 75))
            If (RcvId < 1) Then
                RcvId = ReceivableId
            End If
        Else
            RcvId = ReceivableId
        End If
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplLoadLastActivity(RcvId, PatientId, FilterType, lstTrans, lstPrint)
        Set ApplTemp = Nothing
        lstFilter.Visible = True
        lstTrans.Visible = True
        lstTrans.Enabled = True
        lstDetails.Visible = False
        lstPayment.Visible = False
        
        lblIParty.Visible = False
        lblPayer.Visible = False
        lstRef.Visible = False
        lblPayAmt.Visible = False
        txtAmount.Visible = False
        txtAmount.Text = ""
        lblReason.Visible = False
        txtReason.Visible = False
        txtReason.Text = ""
        lblPayRef.Visible = False
        txtRefId.Visible = False
        txtRefId.Text = ""
        lblTrans.Visible = False
        lstPayType.Visible = False
        lstPayType.ListIndex = -1
        lblPayDate.Visible = False
        txtDate.Visible = False
        txtDate.Text = ""
        lblMethod.Visible = False
        lstMethod.Visible = False
        lstMethod.ListIndex = -1
        lblVoidDate.Visible = False
        txtVoidDate.Visible = False
        txtVoidDate.Text = ""
        lblCheck.Visible = False
        txtCheck.Visible = False
        txtCheck.Text = ""
        lblChkAmt.Visible = False
        txtChkAmt.Visible = False
        txtChkAmt.Text = ""
        chkInclude.Visible = False
        lblSrv.Visible = False
        lstSrv.Visible = False
        lstSrv.ListIndex = -1
        lblAll.Visible = False
        txtAll.Visible = False
        txtAll.Text = ""
    ElseIf (frmEventMsgs.Result = 6) Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplSetReceivablePrimary(ReceivableId)
        Set ApplTemp = Nothing
    End If
End If
End Sub

Private Sub cmdMClaim_Click()
Dim i As Integer
Dim InvId As String
If (lstTrans.Visible) Then
    Exit Sub
End If
If Not UserLogin.HasPermission(epManualClaimEntry) Then
    frmEventMsgs.Header = "User is not permissioned."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
Dim PatientDemographics As New PatientDemographics
PatientDemographics.PatientId = PatientId
Call PatientDemographics.DisplayInvoiceEditAndBillScreen(ReceivableId)
InvId = ""
CurrentIndex = 0
PreviousIndex = 0
Call ResetOuterList(MidPointIndex, False)
If (cmdAll.Text = "A L L") Then
    Call cmdAll_Click
End If
If (Trim(InvId) <> "") Then
    For i = 0 To lstPayment.ListCount - 1
        If (Mid(lstPayment.List(i), 14, 11) = InvId) Then
            lstPayment.ListIndex = i
            Call lstPayment_Click
            Exit For
        End If
    Next i
End If
End Sub

Private Sub cmdNext_Click()
If (CurrentIndex >= TotalFound) Then
    CurrentIndex = 0
    MidPointIndex = 0
    PreviousIndex = 0
End If
Call LoadPayments(False, InvDate)
End Sub

Private Sub cmdPrev_Click()
CurrentIndex = PreviousIndex
MidPointIndex = PreviousIndex
PreviousIndex = 0
If (CurrentIndex < 1) Then
    CurrentIndex = 0
    MidPointIndex = 0
    PreviousIndex = 0
ElseIf (CurrentIndex >= TotalFound) Then
    CurrentIndex = 0
    MidPointIndex = 0
    PreviousIndex = 0
End If
Call LoadPayments(False, InvDate)
End Sub

Private Sub cmdQuit_Click()
fraAllocate.Visible = False
lblAllocate.Visible = False
lblInsAmt.Visible = False
txtInsAmt.Visible = False
lblPatAmt.Visible = False
txtPatAmt.Visible = False
cmdFDone.Visible = False
cmdQuit.Visible = False
lstIns.Visible = False
txtBillDr.Visible = False
txtBillDr.Text = ""
txtDOS.Visible = False
lstSetIns.Visible = False

lstPayment.Enabled = True
lstDetails.Enabled = True
lstTrans.Enabled = True
lstRef.Enabled = True
lstMethod.Enabled = True
lstPayType.Enabled = True
lstSrv.Enabled = True
txtAll.Enabled = True
txtReason.Enabled = True
txtCheck.Enabled = True
txtRefId.Enabled = True
txtAmount.Enabled = True
txtDate.Enabled = True
txtVoidDate.Enabled = True
chkInclude.Enabled = True
cmdDone.Enabled = True
cmdHome.Enabled = True
cmdFinRes.Enabled = True
cmdMClaim.Enabled = True
cmdIns.Enabled = True
cmdRebill.Enabled = True
cmdPrint.Enabled = True
cmdNotes.Enabled = True
cmdEdit.Enabled = True
cmdAllocate.Enabled = True
cmdDos.Enabled = True
End Sub

Private Sub cmdFDone_Click()
Dim RcvId As Long
Dim ATmpId As Long, BTmpId As Long
Dim Bal As Single
Dim PBal As Single, IBal As Single
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
If (txtBillDr.Visible) Then
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplSetBillToDoctor(ReceivableId, val(Trim(Mid(txtBillDr.Text, 57, 5))))
    Set ApplTemp = Nothing
    Call ResetOuterList(MidPointIndex, False)
ElseIf (lstSetIns.Visible) Then
    If (lstSetIns.ListIndex > -1) Then
        If (lblAllocate.Caption = "Set Insurer") Then
            Set ApplTemp = New ApplicationTemplates
            If (ApplTemp.ApplVerifySetInsurer(ReceivableId, val(Trim(Mid(lstSetIns.List(lstSetIns.ListIndex), 77, Len(lstSetIns.List(lstSetIns.ListIndex)) - 76))), Mid(lstSetIns.List(lstSetIns.ListIndex), 51, 1))) Then
                Call ApplTemp.ApplSetBillInsurer(ReceivableId, Mid(lstSetIns.List(lstSetIns.ListIndex), 53, 1), val(Trim(Mid(lstSetIns.List(lstSetIns.ListIndex), 77, Len(lstSetIns.List(lstSetIns.ListIndex)) - 76))), Mid(lstSetIns.List(lstSetIns.ListIndex), 51, 1))
                Set ApplTemp = Nothing
                Call ResetOuterList(MidPointIndex, False)
            Else
                frmEventMsgs.Header = "Insurer is already in use."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Set ApplTemp = Nothing
            End If
        Else
            If (lstSetIns.ListIndex >= 0) Then
                Set ApplList = New ApplicationAIList
                Bal = ApplList.ApplGetBalancebyReceivable(ReceivableId, PBal, IBal)
                Set ApplList = Nothing
                Set ApplTemp = New ApplicationTemplates
                If (ApplTemp.ApplIsReceivablePlanSet(ReceivableId, ATmpId)) Then
                    If (Len(lstSetIns.List(lstSetIns.ListIndex)) > 76) Then
                        ATmpId = val(Trim(Mid(lstSetIns.List(lstSetIns.ListIndex), 77, Len(lstSetIns.List(lstSetIns.ListIndex)) - 76)))
                        Call ApplTemp.ApplGetInsured(PatientId, ATmpId, BTmpId)
                        If (BTmpId > 0) Then
                            If (ReceivableId > 0) Then
                                RcvId = ApplTemp.ApplBuildReceivable(ReceivableId, ATmpId, BTmpId, Trim(Str(Bal)), "")
                            Else
                                RcvId = ApplTemp.ApplBuildReceivable(ReceivableId, ATmpId, BTmpId, Trim(Str(Bal)), "I")
                            End If
                            If (RcvId > 0) Then
                                Call BillHow(RcvId, "I", Trim(Str(Bal)), 0)
                            End If
                        End If
                    End If
                Else
                    frmEventMsgs.Header = "Insurer is not set, Set Insurer"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    Set ApplTemp = Nothing
                    Call cmdQuit_Click
                    Exit Sub
                End If
                Set ApplTemp = Nothing
                Call ResetOuterList(MidPointIndex, False)
            End If
        End If
    Else
        Set ApplTemp = Nothing
        Call ResetOuterList(MidPointIndex, False)
    End If
ElseIf (txtDOS.Visible) Then
    If (Trim(txtDOS.Text) <> "") Then
        CurrentIndex = 0
        PreviousIndex = 0
        Call LoadPayments(False, Trim(txtDOS.Text))
    End If
Else
    Set ApplList = New ApplicationAIList
    Bal = ApplList.ApplGetBalancebyReceivable(ReceivableId, PBal, IBal)
    Set ApplList = Nothing
    If (val(Trim(txtInsAmt.Text)) + val(Trim(txtPatAmt.Text)) <= Bal) Then
        If (Trim(txtInsAmt.Text) <> "") Then
            Set ApplTemp = New ApplicationTemplates
            If Not (ApplTemp.ApplIsReceivablePlanSet(ReceivableId, ATmpId)) Then
                frmEventMsgs.Header = "Insurer is not set, Set Insurer"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Set ApplTemp = Nothing
                Call cmdQuit_Click
                Exit Sub
            Else
                If (lstIns.ListIndex >= 0) Then
                    If (ReceivableId > 0) Then
                        RcvId = ApplTemp.ApplBuildReceivable(ReceivableId, val(Trim(Mid(lstIns.List(lstIns.ListIndex), 57, Len(lstIns.List(lstIns.ListIndex))) - 56)), val(Mid(lstIns.List(lstIns.ListIndex), 41, 16)), txtInsAmt.Text, "")
                    Else
                        RcvId = ApplTemp.ApplBuildReceivable(ReceivableId, val(Trim(Mid(lstIns.List(lstIns.ListIndex), 57, Len(lstIns.List(lstIns.ListIndex))) - 56)), val(Trim(Mid(lstIns.List(lstIns.ListIndex), 41, 16))), txtInsAmt.Text, "I")
                    End If
                    If (RcvId > 0) Then
                        Call BillHow(RcvId, "I", Trim(txtInsAmt.Text), 0)
                    End If
                ElseIf (ATmpId > 0) Then
                    Call BillHow(ReceivableId, "I", Trim(txtInsAmt.Text), 0)
                End If
            End If
            Set ApplTemp = Nothing
        End If
        If (Trim(txtPatAmt.Text) <> "") Then
            Call BillHow(ReceivableId, "P", Trim(txtPatAmt.Text), 0)
        End If
        Call ResetOuterList(MidPointIndex, False)
    Else
        frmEventMsgs.Header = "Allocated Amounts Exceed Balance"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPatAmt.SetFocus
        Exit Sub
    End If
End If
fraAllocate.Visible = False
lblAllocate.Visible = False
lblInsAmt.Visible = False
txtInsAmt.Visible = False
lblPatAmt.Visible = False
txtPatAmt.Visible = False
cmdFDone.Visible = False
cmdQuit.Visible = False
lstIns.Visible = False
txtBillDr.Visible = False
txtDOS.Visible = False
lstSetIns.Visible = False

lstPayment.Enabled = True
lstDetails.Enabled = True
lstTrans.Enabled = True
lstRef.Enabled = True
lstMethod.Enabled = True
lstPayType.Enabled = True
lstSrv.Enabled = True
txtAll.Enabled = True
txtReason.Enabled = True
txtCheck.Enabled = True
txtRefId.Enabled = True
txtAmount.Enabled = True
txtDate.Enabled = True
txtVoidDate.Enabled = True
chkInclude.Enabled = True
cmdDone.Enabled = True
cmdHome.Enabled = True
cmdFinRes.Enabled = True
cmdMClaim.Enabled = True
cmdIns.Enabled = True
cmdRebill.Enabled = True
cmdEdit.Enabled = True
cmdPrint.Enabled = True
cmdNotes.Enabled = True
cmdAllocate.Enabled = True
cmdDos.Enabled = True
End Sub

Private Sub cmdAllocate_Click()
Dim z As Integer
Dim PBal As Single, IBal As Single
Dim ATmpId As Long
Dim PatId As Long, InsId As Long
Dim IPlan As Long, ApptId As Long
Dim b1 As Boolean, b2 As Boolean
Dim TheDate As String, n1 As String
Dim r1 As String, r2 As String
Dim InvId As String, IDate As String
Dim l1 As String, l2 As String, l3 As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
If (lstTrans.Visible) Then
    Exit Sub
End If
If (ReceivableId < 1) Then
    frmEventMsgs.Header = "Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    cmdHome.SetFocus
Else
    Set ApplTemp = New ApplicationTemplates
    If (ApplTemp.ApplIsReceivablePlanSet(ReceivableId, ATmpId)) Then
        Call ApplTemp.ApplGetInvoice(ReceivableId, InvId, IDate, IPlan, ApptId)
' restricted to bypass the results of the test except where noted
        z = ApplTemp.ApplIsInvoiceVerified(InvId)
        If (z = -1) Then
            frmEventMsgs.Header = "Primary Insurer needs to be correctly set. Set Insurer."
            z = 0
        ElseIf (z = -2) Then
            frmEventMsgs.Header = "Secondary Insurer needs to be correctly set. Set Insurer."
            z = 0
        ElseIf (z = -3) Then
            frmEventMsgs.Header = "Third Insurer needs to be correctly set. Set Insurer."
            z = 0
        ElseIf (z = -99) Then
            frmEventMsgs.Header = "Patient has no Financial records."
        End If
        If (z <> 0) Then
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Set ApplTemp = Nothing
            Exit Sub
        End If
    Else
        Set ApplTemp = Nothing
        frmEventMsgs.Header = "Insurer is not set, Set Insurer"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
    If Not (ApplTemp.ApplIsReceivableVerified(ReceivableId, "I")) Then
        frmEventMsgs.Header = "Incorrect Insurer, Rebill and choose new payer."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTemp = Nothing
        Exit Sub
    End If
    Set ApplList = New ApplicationAIList
    If (ApplList.ApplGetBalancebyReceivable(ReceivableId, PBal, IBal) > 0) Then
        lstIns.Clear
        Set ApplTemp = New ApplicationTemplates
        TheDate = ApplTemp.ApplGetReceivableDate(ReceivableId, PatId, InsId)
        Call ApplTemp.ApplGetAllPayers(PatId, n1, InsId, b1, r1, 0, b2, r2, InsType, TheDate, lstIns, l1, l2, l3, False, False, False, True)
        If (lstIns.ListCount < 1) Then
            Call ApplTemp.ApplGetAllPayers(PatId, n1, InsId, b1, r1, SecondPolicyHolderId, b2, r2, InsType, TheDate, lstIns, l1, l2, l3, False, False, False, True)
        End If
        Set ApplTemp = Nothing
        lstPayment.Enabled = False
        lstDetails.Enabled = False
        lstTrans.Enabled = True
        lstRef.Enabled = False
        lstMethod.Enabled = False
        lstPayType.Enabled = False
        lstSrv.Enabled = False
        lblAll.Enabled = False
        txtReason.Enabled = False
        txtCheck.Enabled = False
        txtRefId.Enabled = False
        txtAmount.Enabled = False
        txtDate.Enabled = False
        txtVoidDate.Enabled = False
        chkInclude.Enabled = False
        cmdDone.Enabled = False
        cmdHome.Enabled = False
        cmdFinRes.Enabled = False
        cmdMClaim.Enabled = False
        cmdIns.Enabled = False
        cmdRebill.Enabled = False
        cmdPrint.Enabled = False
        cmdNotes.Enabled = False
        cmdEdit.Enabled = False
        cmdAllocate.Enabled = False
        cmdDos.Enabled = False
        fraAllocate.Visible = True
        lblAllocate.Caption = "Allocate"
        lblAllocate.Visible = True
        lblInsAmt.Visible = True
        txtInsAmt.Visible = True
        txtInsAmt.Text = ""
        lblPatAmt.Visible = True
        txtPatAmt.Visible = True
        txtPatAmt.Text = ""
        cmdFDone.Visible = True
        cmdQuit.Visible = True
        If (lstIns.ListCount > 0) Then
            lstIns.Visible = True
        Else
            lstIns.Visible = False
        End If
        lstSetIns.Visible = False
        txtBillDr.Visible = False
        txtDOS.Visible = False
        txtInsAmt.SetFocus
        fraAllocate.ZOrder 0
        lblAllocate.ZOrder 0
        cmdFDone.ZOrder 0
        cmdQuit.ZOrder 0
        txtPatAmt.ZOrder 0
        txtInsAmt.ZOrder 0
        lblInsAmt.ZOrder 0
        txtInsAmt.ZOrder 0
        lstIns.ZOrder 0
    Else
        frmEventMsgs.Header = "No Balance To Allocate"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
    Set ApplList = Nothing
End If
End Sub

Private Sub cmdRebill_Click()
Exit Sub



Dim p As Integer
Dim Bal As Single
Dim InsId As Long
Dim RcvId1 As Long, SrvItem As Long
Dim ATmpId As Long, PatId As Long, MyInsId As Long
Dim b1 As Boolean, b2 As Boolean
Dim r1 As String, r2 As String, n1 As String
Dim l1 As String, l2 As String, l3 As String
Dim Temp As String, TheDate As String
Dim LineBatchOn As Boolean
Dim ApplTemp As ApplicationTemplates
If (ReceivableId < 1) Then
    frmEventMsgs.Header = "Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    cmdHome.SetFocus
ElseIf (lstTrans.Visible) Then
    Exit Sub
Else
    Bal = 0
    SrvItem = 0
    If (lstDetails.Visible) Then
        If (lstDetails.ListIndex >= 0) Then
            If (Mid(lstDetails.List(lstDetails.ListIndex), 75, 1) = "S") Then
                SrvItem = val(Trim(Mid(lstDetails.List(lstDetails.ListIndex), 76, Len(lstDetails.List(lstDetails.ListIndex)) - 75)))
                frmEventMsgs.Header = "Send Service Only ?"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Service"
                frmEventMsgs.CancelText = "All"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 4) Then
                    SrvItem = 0
                Else
                    Bal = 0
                    For p = lstDetails.ListIndex To lstDetails.ListCount - 1
                        If (Left(lstDetails.List(p), 7) = "Balance") Then
                            Bal = val(Trim(Mid(lstDetails.List(p), 58, 10)))
                            Exit For
                        End If
                    Next p
                End If
            End If
        End If
    End If
    frmEventMsgs.Header = "Bill Whom ?"
    frmEventMsgs.AcceptText = "Patient"
    frmEventMsgs.RejectText = "Payer"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Next Payer"
    frmEventMsgs.Other1Text = "Choose Payer"
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (BillHow(ReceivableId, "P", Trim(Str(Bal)), SrvItem)) Then
            Call ResetOuterList(MidPointIndex, False)
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (BillHow(ReceivableId, "I", Trim(Str(Bal)), SrvItem)) Then
            Call ResetOuterList(MidPointIndex, False)
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Set ApplTemp = New ApplicationTemplates
        If (ApplTemp.ApplIsReceivablePlanSet(ReceivableId, ATmpId)) Then
            RcvId1 = ApplTemp.ApplBillSecondary(ReceivableId, ATmpId, 0, False, True)
            If (RcvId1 > 0) Then
                ReceivableId = RcvId1
                If (BillHow(ReceivableId, "I", Trim(Str(Bal)), SrvItem)) Then
                    Call ResetOuterList(MidPointIndex, False)
                End If
            ElseIf (RcvId1 = -1) Then
                frmEventMsgs.Header = "No Balance to Forward"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            ElseIf (RcvId1 = -2) Then
                If (Left(lstPayment.List(lstPayment.ListIndex + 1), 1) = "+") Or _
                   (Left(lstPayment.List(lstPayment.ListIndex + 1), 1) = "-") Then
                    ReceivableId = val(Trim(Mid(lstPayment.List(lstPayment.ListIndex + 1), 76, Len(lstPayment.List(lstPayment.ListIndex + 1)) - 75)))
                    If (BillHow(ReceivableId, "I", Trim(Str(Bal)), SrvItem)) Then
                        CurrentIndex = CurrentIndex - 2
                        If (CurrentIndex <= 1) Then
                            CurrentIndex = 0
                            PreviousIndex = 0
                        End If
                        Call LoadPayments(True, InvDate)
                    End If
                Else
                    frmEventMsgs.Header = "No Other Payers"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
            End If
        Else
            frmEventMsgs.Header = "Insurer is not set, Set Insurer"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
        Set ApplTemp = Nothing
    ElseIf (frmEventMsgs.Result = 5) Then
        lstSetIns.Clear
        Set ApplTemp = New ApplicationTemplates
        TheDate = ApplTemp.ApplGetReceivableDate(ReceivableId, PatId, InsId)
        If (Trim(TheDate) <> "") Then
            Call ApplTemp.ApplGetAllPayers(PatId, n1, _
                                           InsId, b1, _
                                           r1, SecondPolicyHolderId, _
                                           b2, r2, _
                                           InsType, TheDate, _
                                           lstSetIns, l1, _
                                           l2, l3, _
                                           True, True, _
                                           True, True)
            If (lstSetIns.ListCount < 1) Then
                If (PolicyHolderid <> PatientId) Then
                    Call ApplTemp.ApplGetAllPayers(PolicyHolderid, n1, PolicyHolderid, b1, r1, SecondPolicyHolderId, b2, r2, "", TheDate, lstSetIns, l1, l2, l3, True, False, True, True)
                Else
                    Call ApplTemp.ApplGetAllPayers(PatientId, n1, PolicyHolderid, b1, r1, SecondPolicyHolderId, b2, r2, "", TheDate, lstSetIns, l1, l2, l3, True, False, True, True)
                End If
                If (lstSetIns.ListCount < 1) Then
                    Call ApplTemp.ApplGetAllPayers(PatientId, n1, SecondPolicyHolderId, b2, r2, 0, b1, r1, "", TheDate, lstSetIns, l1, l2, l3, True, False, True, True)
                End If
            End If
        End If
        Set ApplTemp = Nothing
        If (lstSetIns.ListCount > 0) Then
            lstPayment.Enabled = False
            cmdDone.Enabled = False
            cmdHome.Enabled = False
            cmdFinRes.Enabled = False
            cmdRebill.Enabled = False
            cmdEdit.Enabled = False
            cmdAllocate.Enabled = False
            cmdPrint.Enabled = False
            cmdNotes.Enabled = False
            cmdMClaim.Enabled = False
            cmdIns.Enabled = False
            cmdDos.Enabled = False
            
            fraAllocate.Visible = True
            lblAllocate.Caption = "Choose Payer"
            lblAllocate.Visible = True
            lblInsAmt.Visible = False
            txtInsAmt.Visible = False
            txtInsAmt.Text = ""
            lblPatAmt.Visible = False
            txtPatAmt.Visible = False
            txtPatAmt.Text = ""
            lstIns.Visible = False
            txtBillDr.Visible = False
            txtBillDr.Text = ""
            txtDOS.Visible = False
            lstSetIns.Visible = True
            cmdFDone.Visible = True
            cmdQuit.Visible = True
            lstSetIns.SetFocus
            fraAllocate.ZOrder 0
            lblAllocate.ZOrder 0
            cmdFDone.ZOrder 0
            cmdQuit.ZOrder 0
            lstSetIns.ZOrder 0
        Else
            frmEventMsgs.Header = "No Insurers for Service Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End If
End Sub

Private Sub cmdDOS_Click()
If (lstTrans.Visible) Then
    Exit Sub
End If
If (lstDetails.Visible) Then
    Call cmdDone_Click
    lstPayment.ListIndex = -1
    ReceivableId = 0
End If
lstPayment.Enabled = False
lstTrans.Enabled = False
cmdDone.Enabled = False
cmdHome.Enabled = False
cmdFinRes.Enabled = False
cmdRebill.Enabled = False
cmdEdit.Enabled = False
cmdAllocate.Enabled = False
cmdPrint.Enabled = False
cmdNotes.Enabled = False
cmdMClaim.Enabled = False
cmdIns.Enabled = False
cmdDos.Enabled = False
    
fraAllocate.Visible = True
lblAllocate.Caption = "Find DOS"
lblAllocate.Visible = True
lblInsAmt.Visible = False
txtInsAmt.Visible = False
txtInsAmt.Text = ""
lblPatAmt.Visible = False
txtPatAmt.Visible = False
txtPatAmt.Text = ""
lstIns.Visible = False
txtBillDr.Visible = False
txtBillDr.Text = ""
txtDOS.Visible = True
txtDOS.Text = ""
txtDOS.SetFocus
cmdFDone.Visible = True
cmdQuit.Visible = True
fraAllocate.ZOrder 0
lblAllocate.ZOrder 0
cmdFDone.ZOrder 0
cmdQuit.ZOrder 0
txtDOS.ZOrder 0
End Sub

Private Sub cmdNotes_Click()
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim IPlan As Long
Dim RcvId As Long, ApptId As Long
Dim MyEye As String
Dim Temp As String
Dim ADate As String, AName As String
Dim InvId As String, SrvId As String
InvId = ""
SrvId = ""
frmNotes.NoteId = 0
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = 0
frmNotes.CurrentAction = ""
frmNotes.SetTo = "B"
frmNotes.MaintainOn = UserLogin.HasPermission(epBillingNotes)
If (lstPayment.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    RcvId = val(Trim(Mid(lstPayment.List(lstPayment.ListIndex), 76, Len(lstPayment.List(lstPayment.ListIndex)) - 75)))
    ApptId = ApplList.ApplGetAppt(RcvId)
    Set ApplList = Nothing
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetInvoice(RcvId, InvId, ADate, IPlan, ApptId)
    Set ApplTemp = Nothing
    frmNotes.EyeContext = ""
    frmNotes.AppointmentId = ApptId
    frmNotes.SystemReference = "R" + InvId
    If (lstDetails.ListIndex >= 0) And (lstDetails.Visible) Then
        If (Mid(lstDetails.List(lstDetails.ListIndex), 75, 1) = "S") Then
            If (Mid(lstDetails.List(lstDetails.ListIndex), 14, 4) = "Srv:") Then
                SrvId = Trim(Mid(lstDetails.List(lstDetails.ListIndex), 18, 10))
                MyEye = Trim(Mid(lstDetails.List(lstDetails.ListIndex), 28, 8))
                If (InStrPS(MyEye, "RT") > 0) Then
                    MyEye = "OD"
                ElseIf (InStrPS(MyEye, "LT") > 0) Then
                    MyEye = "OS"
                ElseIf (InStrPS(MyEye, "50") > 0) Then
                    MyEye = "OU"
                End If
                frmNotes.EyeContext = MyEye
                frmNotes.SystemReference = frmNotes.SystemReference + "C" + SrvId
            End If
        End If
    End If
End If
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (InvId <> "") Then
        Set ApplList = New ApplicationAIList
        If (ApplList.ApplIsBillingNotesOn(PatientId, "R" + Trim(InvId))) Then
            If (lstPayment.ListIndex >= 0) Then
                Temp = lstPayment.List(lstPayment.ListIndex)
                Mid(Temp, 60, 3) = "-N-"
                lstPayment.List(lstPayment.ListIndex) = Temp
            End If
        End If
        If (lstDetails.Visible) Then
            If (ApplList.ApplIsBillingNotesOn(PatientId, "R" + Trim(InvId) + "C" + Trim(SrvId))) Then
                If (lstDetails.ListIndex >= 0) Then
                    Temp = lstDetails.List(lstDetails.ListIndex)
                    Mid(Temp, 60, 3) = "-N-"
                    lstDetails.List(lstDetails.ListIndex) = Temp
                End If
            End If
        End If
    End If
    Set ApplList = Nothing
    If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
        lblAlert.Visible = True
    End If
End If
End Sub

Private Sub cmdPrint_Click()
Dim Ub92On As Boolean
Dim OptOn As Boolean
Dim p1 As Long
Dim PlnId As Long, ApptId As Long
Dim ITemp As Integer
Dim IType As Integer, TType As Integer
Dim InvId As String, InvDt As String
Dim RptA As String
Dim SSId As String
Dim Temp As String
Dim MyType As String
Dim DateToday As String
Dim PatName As String
Dim RptName As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim ParamArgs() As Variant
Dim TelerikReporting As Reporting

p1 = 0
If (ReceivableId > 0) Then
    OptOn = CheckConfigCollection("OPTICALMEDICALON") = "T"
    Ub92On = CheckConfigCollection("UB92ON") = "T"
    PatName = Trim(Label1.Caption)
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetPatientSS(PatientId, SSId)
    Set ApplTemp = Nothing
    frmEventMsgs.Header = "Purpose ?"
    frmEventMsgs.AcceptText = "Claim"
    frmEventMsgs.RejectText = "Proof"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Patient Statement"
    frmEventMsgs.Other1Text = "Patient Receipt"
    frmEventMsgs.Other2Text = "Patient Aggregate"
    frmEventMsgs.Other3Text = "Account History"
    frmEventMsgs.Other4Text = "Acct Hst Details"
    If (lstTrans.Visible) Then
        frmEventMsgs.Header = "Print Proof ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Proof"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
    End If
    frmEventMsgs.Show 1
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    If (frmEventMsgs.Result = 1) Then
        MyType = "I"
        If (Ub92On) Then
            frmEventMsgs.Header = ""
            If (frmEventMsgs.Result = 1) Then
                frmEventMsgs.Header = "How ?"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "UB92"
                frmEventMsgs.CancelText = "HCFA"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 2) Then
                    MyType = "U"
                End If
            End If
        End If
        ITemp = IsPrintTransactional
        frmEventMsgs.Header = ""
        If (ITemp = 1) Then
            p1 = ApplTemp.BatchTransaction(ReceivableId, "I", "", "P", False, 0, False, False, False)
            If (p1 > 0) Then
                Call ApplList.ApplSetTransactionStatus(p1, "S")
                lblPrint.Caption = "Printing"
                lblPrint.Visible = True
                DoEvents
                Set ApplTemp.lstBox = lstPrint
                If (ApplTemp.PrintTransaction(ReceivableId, PatientId, MyType, True, True, "", False, 0, "S", "", ResourceType)) Then
                    frmEventMsgs.Header = "Claim Done"
                Else
                    frmEventMsgs.Header = "Claim Not Done"
                End If
                lblPrint.Visible = False
            Else
                frmEventMsgs.Header = ""
            End If
        ElseIf (ITemp = 2) Then
            lblPrint.Caption = "Printing"
            lblPrint.Visible = True
            Set ApplTemp.lstBox = lstPrint
            If (ApplTemp.PrintTransaction(ReceivableId, PatientId, MyType, True, True, "", False, 0, "S", "", ResourceType)) Then
                frmEventMsgs.Header = "Claim Done"
            Else
                frmEventMsgs.Header = "Claim Not Done"
            End If
            lblPrint.Visible = False
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (lstPayment.ListIndex >= 0) Then
            SSId = SSId + " Invoice:" + (Mid(lstPayment.List(lstPayment.ListIndex), 14, 12))
        End If
        If (lstDetails.ListCount > 0) Then
            Set ApplTemp.lstBox = lstPayment
            Call ApplTemp.PrintProof(ReceivableId, PatName, SSId)
            Set ApplTemp.lstBox = lstDetails
            Call ApplTemp.PrintProof(ReceivableId, PatName, SSId)
            If (lstTrans.ListCount > 0) Then
                Set ApplTemp.lstBox = lstTrans
                Call ApplTemp.PrintProof(ReceivableId, PatName, SSId)
            End If
            frmEventMsgs.Header = ""
        Else
            frmEventMsgs.Header = "Trigger claim details"
        End If
    ElseIf (frmEventMsgs.Result = 3) Then 'Patient Statement
        ITemp = IsPrintTransactional
        frmEventMsgs.Header = ""
        
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppt(ReceivableId)
        Set ApplList = Nothing
        
        ReDim ParamArgs(1) As Variant
            
        ParamArgs(0) = Array("PatientId", Str(PatientId))
        ParamArgs(1) = Array("EncounterId", Str(ApptId))
        Set TelerikReporting = New Reporting
        'Invoke Patient Statement telerik Report.
        Call TelerikReporting.ViewReport("Patient Statement", ParamArgs)
        Set TelerikReporting = Nothing
        lblPrint.Visible = False
        DoEvents
    ElseIf (frmEventMsgs.Result = 5) Then
        frmEventMsgs.Header = ""
        lblPrint.Caption = "Printing"
        lblPrint.Visible = True
        DoEvents
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppt(ReceivableId)
        Set ApplList = Nothing
        
        ReDim ParamArgs(0) As Variant
        ParamArgs(0) = Array("EncounterId", Str(ApptId))
        Set TelerikReporting = New Reporting
        'Invoke PatientReceipt telerik Report.
        Call TelerikReporting.ViewReport("Patient Receipt", ParamArgs)
        Set TelerikReporting = Nothing
        
        lblPrint.Visible = False
    ElseIf (frmEventMsgs.Result = 6) Then ' Aggregate Statements
        ITemp = IsPrintTransactional
        frmEventMsgs.Header = ""
        lblPrint.Caption = "Printing"
        lblPrint.Visible = True
        DoEvents
            
        ReDim ParamArgs(0) As Variant
            
        ParamArgs(0) = Array("PatientId", Str(PatientId))
        'Invoke Patient Statement telerik Report.
        Call TelerikReporting.ViewReport("Patient Statement", ParamArgs)
        Set TelerikReporting = Nothing
        lblPrint.Visible = False
        DoEvents
    ElseIf (frmEventMsgs.Result = 8 Or frmEventMsgs.Result = 7) Then
        frmEventMsgs.Header = ""
        lblPrint.Caption = "Printing"
        lblPrint.Visible = True
        DoEvents
        
        'Patient Account History
        Set TelerikReporting = New Reporting
        ReDim ParamArgs(2) As Variant
        
        ParamArgs(0) = Array("PatientId", Str(PatientId))
        ParamArgs(1) = Array("StartDate", DateAdd("yyyy", -1, Now))
        ParamArgs(2) = Array("EndDate", Now)
        'Invoke Account History Details telerik Report.
        Call TelerikReporting.ViewReport("Patient Account History", ParamArgs)
        Set TelerikReporting = Nothing
        
        lblPrint.Visible = False
    Else
        frmEventMsgs.Header = ""
    End If
    Set ApplTemp = Nothing
    Set ApplList = Nothing
Else
    frmEventMsgs.Header = "Receivable must be selected"
End If
If (Trim(frmEventMsgs.Header) <> "") Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
If (p1 > 0) Then
    IType = lstPayment.ListIndex
    TType = TotalFound
    Call ResetOuterList(MidPointIndex, False)
    If (IType <= lstPayment.ListCount) And (TType = TotalFound) Then
        lstPayment.ListIndex = IType
        TriggerDetails = True
        Call lstPayment_Click
        TriggerDetails = False
    End If
End If
End Sub

Private Sub cmdEdit_Click()
Dim PatId As Long
Dim InsId As Long
Dim TheDate As String
Dim ApplTemp As ApplicationTemplates
If Not (lstTrans.Visible) Then
    If (ReceivableId > 0) Then
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = PatientId
        Call PatientDemographics.DisplayInvoiceEditAndBillScreen(ReceivableId)
        Call ResetOuterList(MidPointIndex, False)
    Else
        frmEventMsgs.Header = "Select a Receivable"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdFinRes_Click()
Dim RcvId As Long
Dim ApptId As Long
Dim ApplList As ApplicationAIList
If (lstTrans.Visible) Then
    Exit Sub
End If
If (PatientId > 0) Then
    frmEventMsgs.Header = "Patient Details"
    frmEventMsgs.AcceptText = "Demographics"
    frmEventMsgs.RejectText = "Insurance"
    If (Trim(AccessType) <> "") Then
        frmEventMsgs.RejectText = ""
    End If
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Clinical"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = PatientId
        Call PatientDemographics.DisplayPatientInfoScreen
        Set PatientDemographics = Nothing
    ElseIf (frmEventMsgs.Result = 2) Then
        Call PatientInsurerData
    ElseIf (frmEventMsgs.Result = 3) Then
        If (lstPayment.ListIndex >= 0) Then
            If (Mid(lstPayment.List(lstPayment.ListIndex), 75, 1) = "R") Then
                RcvId = val(Trim(Str(Mid(lstPayment.List(lstPayment.ListIndex), 76, Len(lstPayment.List(lstPayment.ListIndex)) - 75))))
                If (RcvId > 0) Then
                    Set ApplList = New ApplicationAIList
                    ApptId = ApplList.ApplGetAppt(RcvId)
                    If (ApptId > 0) Then
                        If (frmReviewAppts.LoadApptsList(PatientId, ApptId, True)) Then
                            frmReviewAppts.Show 1
                        End If
                    End If
                    Set ApplList = Nothing
                End If
            End If
        Else
            frmEventMsgs.Header = "Select a Receivable"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End If
End Sub

Private Sub Form_Activate()
FilterType = "A"
TheCheckId = 0
If (lblAlert.Visible) Then
    If (FirstTimeIn) Then
        Call lblAlert_Click
        FirstTimeIn = False
    End If
End If
cmdFinRes.Visible = Whom

If fromNew Then
    cmdAll.Text = "A L L"
    Call cmdAll_Click
    fromNew = False
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPayments.BorderStyle = 1
    frmPayments.ClipControls = True
    frmPayments.Caption = Mid(frmPayments.Name, 4, Len(frmPayments.Name) - 3)
    frmPayments.AutoRedraw = True
    frmPayments.Refresh
End If
FirstTimeIn = True
TriggerDone = False
TriggerDetails = False
DefaultPayDate = ""
            lstDetails.Visible = True
            lstDetails.ZOrder 0




End Sub

Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatFinancial, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatFinancial)) Then
        lblAlert.Visible = True
    Else
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub lblRefer_Click()
Dim PermOn As Boolean
Dim i As Integer
Dim j As Integer
Dim DrId As Long
Dim ApplList As ApplicationAIList
DrId = 0
i = InStrPS(lblRefer.Caption, "[")
If (i > 0) Then
    j = InStrPS(lblRefer.Caption, "]")
    If (j > 0) And (j > i) Then
        DrId = val(Trim(Mid(lblRefer.Caption, i + 1, (j - 1) - i)))
    End If
    If (DrId > 0) Then
        If (frmVendors.VendorLoadDisplay(DrId, PermOn)) Then
            frmVendors.Show 1
            Set ApplList = New ApplicationAIList
            Call ApplList.ApplGetReferral(ReceivableId, lblRefer, True)
            Call ApplList.ApplGetPreCert(ReceivableId, lblRefer, True)
            Set ApplList = Nothing
        End If
    End If
End If
End Sub

Private Sub lst90_Click()
Dim Item As Integer
Dim ApplList As ApplicationAIList
If (lst90.ListIndex >= 0) Then
    Item = lst90.ListIndex
    If (lst90.List(lst90.ListIndex) = "No Reason") Then
        Item = -1
    End If
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplSet90Day(ReceivableId, Item + 1)
    Set ApplList = Nothing
    lst90.Visible = False
    lbl90.Visible = False
End If
End Sub

Private Sub lstDetails_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (X < 1000) Then
    If (Mid(lstDetails.List(lstDetails.ListIndex), 1, 1) = "-") Then
        Call cmdDone_Click
    End If
End If
End Sub

Private Sub lstDetails_Click()
Dim u As Integer
Dim AFee As Single
Dim AUse As Boolean, IncCom As Boolean
Dim ApptId As Long, ItemId As Long, ARcvId As Long
Dim CAmt As String
Dim p1 As Long, r1 As String, b1 As Boolean
Dim p2 As Long, r2 As String, b2 As Boolean
Dim l1 As String, l2 As String, l3 As String
Dim pid1 As Long
Dim pd1 As String, RAmt As String, ed1 As String
Dim ADate As String
Dim DName As String, TheSrv As String, ARsn As String
Dim TheClm As String, InvDate As String, TDate As String
Dim RefId As String, RefType As String, RefChk As String
Dim TheAmt As String, TheInv As String, ClmDate As String
Dim InsNeic As String, InsName As String, AEType As String
Dim LocalDate As ManagedDate
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Set LocalDate = New ManagedDate
If (Trim(DefaultPayDate) <> "") Then
    TDate = DefaultPayDate
End If
Call FormatTodaysDate(TDate, False)
If (lstDetails.ListIndex >= 0) Then
' Init all practical payment fields
    txtDate.Text = ""
    txtAmount.Text = ""
    txtVoidDate.Text = ""
    txtRefId.Text = ""
    txtReason.Text = ""
    lstRef.ListIndex = -1
    lstPayType.ListIndex = -1
    lstSrv.ListIndex = -1
    txtAll.Text = ""
    chkInclude.Value = 0
    
    txtDate.Locked = False
    txtAmount.Locked = False
    txtCheck.Locked = False
    txtReason.Locked = False
    txtRefId.Locked = False
    lstPayType.Locked = False
    lstRef.Locked = False
    lstSrv.Locked = False
    txtAll.Locked = False
    lstMethod.Locked = False
    chkInclude.Enabled = True
    
    Set ApplTemp = New ApplicationTemplates
    Set ApplList = New ApplicationAIList
    If (Mid(lstDetails.List(lstDetails.ListIndex), 75, 1) = "P") Then
        TheInv = ""
        PaymentType = ""
        PaymentId = val(Mid(lstDetails.List(lstDetails.ListIndex), 76, Len(lstDetails.List(lstDetails.ListIndex)) - 75))
        ReceivableId = ApplTemp.ApplGetReceivablebyPayment(PaymentId, PlanId, PayerType, PaymentType, TheAmt, ClmDate, RefId, RefType, RefChk, TheSrv, ItemId, IncCom, ARsn, AFee, TheCheckId)
        lstDetails.Tag = TheSrv
        ARcvId = ApplTemp.ApplGetReceivableId(PaymentId)
        txtCheck.ToolTipText = ""
        If (PayerType = "I") Then
            Call ApplTemp.ApplGetPlanName(PlanId, InsName, TheInv, DName, InsNeic, AEType, AUse)
            If (RefType = "K") Then
                If (ApplTemp.ApplFindTheCheck(RefChk, InsName, PlanId, "I", CAmt, pd1, pid1, RAmt, ed1)) Then
                    txtCheck.ToolTipText = CAmt
                Else
                    txtCheck.ToolTipText = TheAmt
                End If
            End If
        ElseIf (PayerType = "P") Then
            Call ApplTemp.ApplGetPatientName(PatientId, TheInv, p1, r1, b1, p2, r2, b2, False)
            If (RefType = "K") Then
                If (ApplTemp.ApplFindTheCheck(RefChk, TheInv, PlanId, "P", CAmt, pd1, pid1, RAmt, ed1)) Then
                    txtCheck.ToolTipText = CAmt
                Else
                    txtCheck.ToolTipText = TheAmt
                End If
            End If
        End If
        lstRef.ListIndex = -1
        For u = 0 To lstRef.ListCount - 1
            If (Mid(lstRef.List(u), 76, 1) = PayerType) Then
                If (PlanId = val(Trim(Mid(lstRef.List(u), 77, Len(lstRef.List(u)) - 76)))) Then
                    lstRef.ListIndex = u
                    Exit For
                ElseIf (PatientId = val(Trim(Mid(lstRef.List(u), 77, Len(lstRef.List(u)) - 76)))) Then
                    lstRef.ListIndex = u
                    Exit For
                End If
            End If
        Next u
        Call DisplayDollarAmount(Trim(Str(TheAmt)), TheInv)
        txtAmount.Text = Trim(TheInv)

        Call ApplTemp.ApplGetInvoice(ARcvId, TheInv, InvDate, PlanId, ApptId)
        lblInvoice.Caption = TheInv
        txtRefId.Text = RefId
        txtRefId.Tag = ""
        If (Left(RefId, 1) = "~") Then
            txtRefId.Tag = "~"
            Mid(RefId, 1, 1) = " "
            txtRefId.Text = Trim(RefId)
        End If
        txtReason.Text = ARsn
        txtCheck.Text = RefChk
        lstMethod.ListIndex = -1
        For u = 0 To lstMethod.ListCount - 1
            If (Mid(lstMethod.List(u), 40, 1) = RefType) Then
                lstMethod.ListIndex = u
                Exit For
            End If
        Next u
        lstPayType.ListIndex = -1
        For u = 0 To lstPayType.ListCount - 1
            If (Mid(lstPayType.List(u), 40, 1) = PaymentType) Then
                lstPayType.ListIndex = u
                Exit For
            End If
        Next u
        chkInclude.Value = 0
        If (IncCom) Then
            chkInclude.Value = 1
        End If
        lstSrv.ListIndex = -1
        For u = 0 To lstSrv.ListCount - 1
            If (val(Trim(Mid(lstSrv.List(u), 76, Len(lstSrv.List(u)) - 75))) = ItemId) Then
                lstSrv.ListIndex = u
                Exit For
            End If
        Next u
        txtAll.Text = Trim(Str(AFee))
        InvDate = Mid(InvDate, 5, 2) + "/" + Mid(InvDate, 7, 2) + Mid(InvDate, 1, 4)
        Invoicedate = InvDate
        ClmDate = Left(lstDetails.List(lstDetails.ListIndex), 10)
        If (PaymentType <> "V") Then
            txtDate.Text = ClmDate
        Else
            txtVoidDate.Text = ClmDate
            txtDate.Text = ""
        End If
        DefaultPayDate = txtDate.Text
        If Not (ApplTemp.ApplIsClaimEditable(ReceivableId, ClmDate)) Then
            chkInclude.Enabled = False
            txtDate.Locked = True
            txtAmount.Locked = True
            txtCheck.Locked = True
            lstPayType.Locked = True
            lstRef.Locked = True
            lstMethod.Locked = True
            txtReason.Locked = True
            txtRefId.Locked = True
' Allow edit of service
            lstSrv.Locked = False
            txtAll.Locked = False
        End If
    ElseIf (Mid(lstDetails.List(lstDetails.ListIndex), 75, 1) = "S") Then
        PaymentId = 0
        ItemId = val(Mid(lstDetails.List(lstDetails.ListIndex), 76, Len(lstDetails.List(lstDetails.ListIndex)) - 75))
        Call ApplTemp.ApplGetServicebyId(ItemId, TheSrv, AFee)
        lstDetails.Tag = TheSrv
        lstSrv.ListIndex = -1
        For u = 0 To lstSrv.ListCount - 1
            If (val(Trim(Mid(lstSrv.List(u), 76, Len(lstDetails.List(lstDetails.ListIndex)) - 75))) = ItemId) Then
                lstSrv.ListIndex = u
                Exit For
            End If
        Next u
        txtAll.Text = ""
        txtAll.Locked = False
        If (AFee <> 0) Then
            txtAll.Text = Trim(Str(AFee))
        End If
        ADate = ""
        Call FormatTodaysDate(ADate, False)
        txtDate.Text = ADate
        txtDate.Locked = False
    Else ' Treat as a new payment
        PaymentId = 0
        ADate = ""
        Call FormatTodaysDate(ADate, False)
        txtDate.Text = ADate
        txtDate.Locked = False
    End If
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Sub

Private Sub lstFilter_Click()
Dim ApplTemp As ApplicationTemplates
If (lstFilter.ListIndex >= 0) Then
    FilterType = Left(lstFilter.List(lstFilter.ListIndex), 1)
    lstTrans.Clear
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplLoadLastActivity(ReceivableId, PatientId, FilterType, lstTrans, lstPrint)
    Set ApplTemp = Nothing
End If
End Sub

Private Sub lstTrans_Click()
Dim st As String
Dim PT As String
Dim RcvId As Long
Dim Temp As String
Dim Temp1 As String
Dim Test As Integer
Dim TType As Integer
Dim ApplList As ApplicationAIList
If (lstTrans.ListIndex = 0) Then
    lstFilter.Visible = False
    lstTrans.Visible = False
    lstDetails.Visible = True
    lstPayment.Visible = True
    Call lstPayment_MouseDown(1, 0, 60, 120)
ElseIf (lstTrans.ListIndex > 0) Then
    If (Mid(lstTrans.List(lstTrans.ListIndex), 75, 1) = "R") Then
        frmEventMsgs.Header = "Set Transaction Status"
        frmEventMsgs.AcceptText = "Sent"
        frmEventMsgs.RejectText = "Close"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Or (frmEventMsgs.Result = 1) Then
            PT = "I"
            If (InStrPS(lstTrans.List(lstTrans.ListIndex), "Stmt") > 0) Then
                PT = "P"
            End If
            st = "Z"
            If (InStrPS(lstTrans.List(lstTrans.ListIndex), "Sent") > 0) Then
                st = "S"
            ElseIf (InStrPS(lstTrans.List(lstTrans.ListIndex), "Que") > 0) Then
                st = "P"
            End If
            RcvId = val(Mid(lstTrans.List(lstTrans.ListIndex), 76, Len(lstTrans.List(lstTrans.ListIndex)) - 75))
            If (RcvId > 0) Then
                Set ApplList = New ApplicationAIList
                Temp = ""
                Temp1 = ""
                If (frmEventMsgs.Result = 1) Then
                    Temp = Mid(lstTrans.List(lstTrans.ListIndex), 7, 4) _
                         + Mid(lstTrans.List(lstTrans.ListIndex), 1, 2) _
                         + Mid(lstTrans.List(lstTrans.ListIndex), 4, 2)
                    Temp1 = Trim(Str(val(Trim(Mid(lstTrans.List(lstTrans.ListIndex), 63, 10)))))
                End If
                Call ApplList.ApplSetAllRcvTransStatus(RcvId, PT, st, Temp, Temp1, True)
                Set ApplList = Nothing
                Test = lstPayment.ListIndex
                TType = TotalFound
                Call ResetOuterList(MidPointIndex, False)
                If (Test <= lstPayment.ListCount) And (TType = TotalFound) Then
                    TriggerDetails = True
                    lstPayment.ListIndex = Test
                    Call lstPayment_Click
                    TriggerDetails = False
                End If
                lstFilter.Visible = lstTrans.Visible
            End If
        End If
    ElseIf (Mid(lstTrans.List(lstTrans.ListIndex), 75, 1) = "L") Or _
           (Mid(lstTrans.List(lstTrans.ListIndex), 75, 1) = "[") Or _
           (Mid(lstTrans.List(lstTrans.ListIndex), 75, 1) = "[") Then
        RcvId = 0
        st = ""
        frmEventMsgs.Header = "Set Letter Status"
        frmEventMsgs.AcceptText = "Sent"
        frmEventMsgs.RejectText = "Close"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Pending"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            st = "S"
            PT = "   Sent"
        ElseIf (frmEventMsgs.Result = 2) Then
            st = "Z"
            PT = " Closed"
        ElseIf (frmEventMsgs.Result = 3) Then
            st = "A"
            PT = "Pending"
        End If
        If (st <> "") Then
            RcvId = val(Mid(lstTrans.List(lstTrans.ListIndex), 76, Len(lstTrans.List(lstTrans.ListIndex)) - 75))
            If (RcvId > 0) Then
                Set ApplList = New ApplicationAIList
                Call ApplList.ApplSetTransactionStatus(RcvId, st)
                Set ApplList = Nothing
                Temp = lstTrans.List(lstTrans.ListIndex)
                Mid(Temp, 30, 7) = PT
                lstTrans.List(lstTrans.ListIndex) = Temp
            End If
        End If
    End If
End If
End Sub

Private Sub lstPayment_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If (x < 1000) Then
    If (Mid(lstPayment.List(lstPayment.ListIndex), 1, 1) = "+") Or (Mid(lstPayment.List(lstPayment.ListIndex), 1, 1) = "-") Then
        TriggerDetails = True
        Call lstPayment_Click
        TriggerDetails = False
    End If
End If
End Sub

Private Sub lstPayment_Click()
Dim z As Integer
Dim AUse As Boolean
Dim b1 As Boolean, b2 As Boolean
Dim r1 As String, r2 As String
Dim Temp As String, n1 As String
Dim Temp1 As String, Temp2 As String
Dim Temp3 As String, Temp4 As String
Dim ApptId As Long, ARcvId As Long
Dim TempDate As String, Temp1Date As String
Dim TheInv As String, InvDate As String, TDate As String
Dim l1 As String, l2 As String, l3 As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
TDate = ""
If (Trim(DefaultPayDate) <> "") Then
    TDate = DefaultPayDate
End If
If (lstPayment.ListIndex >= 0) Then
    Set ApplTemp = New ApplicationTemplates
    Set ApplList = New ApplicationAIList
    If (Mid(lstPayment.List(lstPayment.ListIndex), 75, 1) = "R") Then
        If (Mid(lstPayment.List(lstPayment.ListIndex), 76, 1) = "0") Then
            Exit Sub
        End If
        Call FormatTodaysDate(TDate, False)
        TheInv = ""
        TheCheckId = 0
        PlanId = 0
        PaymentId = 0
        PayerType = ""
        PaymentType = ""
        lstRef.ListIndex = -1
        ReceivableId = val(Mid(lstPayment.List(lstPayment.ListIndex), 76, Len(lstPayment.List(lstPayment.ListIndex)) - 75))
        Call ApplTemp.ApplGetInvoice(ReceivableId, TheInv, InvDate, PlanId, ApptId)
        Call ApplTemp.ApplGetInsuranceType(ApptId, InsType)
        Call ApplTemp.ApplGetAllPayers(PatientId, n1, PolicyHolderid, b1, r1, SecondPolicyHolderId, b2, r2, InsType, InvDate, lstRef, l1, l2, l3, True, True, False, True)
        Call ApplTemp.AddCurrentInvoicePayers(TheInv, lstRef)
        Call ApplTemp.ApplGetPlanName(PlanId, Temp, Temp1, Temp2, Temp3, Temp4, AUse)
        lblInvoice.Caption = TheInv
        txtAmount.Text = ""
        txtRefId.Text = ""
        txtReason.Text = ""
        If (Trim(txtChkAmt.Text) = "") Then
            txtDate.Text = DefaultPayDate
            txtVoidDate.Text = ""
            lstMethod.ListIndex = -1
            lstPayType.ListIndex = -1
        End If
        lstSrv.ListIndex = -1
        txtAll.Text = ""
        Call ApplList.ApplGetReferral(ReceivableId, lblRefer, True)
        Call ApplList.ApplGetPreCert(ReceivableId, lblRefer, True)
        lblRefer.Visible = True
        lblRefCert.Visible = True
        lst90.ListIndex = ApplList.ApplGet90Day(ReceivableId)
        lbl90.Visible = False
        lst90.Visible = False
        If (TriggerDetails) Then
            Set ApplTemp.lstBox = lstDetails
            Set ApplTemp.lstBoxC = lstSrv
            Call ApplTemp.ApplLoadPaymentsbyServices(TheInv, PatientId, ApptId)
            lblIParty.Visible = True
            lblIParty.Caption = Temp
            lstDetails.Visible = True
            lstDetails.ZOrder 0
            lstTrans.Visible = False
            lblPayer.Visible = True
            lstRef.Visible = True
            lblPayAmt.Visible = True
            lblCheck.Visible = True
            txtCheck.Visible = True
            lblChkAmt.Visible = True
            txtChkAmt.Visible = True
            txtAmount.Visible = True
            txtAmount.Text = ""
            lblPayDate.Visible = True
            txtDate.Visible = True
            txtDate.Text = TDate
            Temp1Date = CheckConfigCollection("EDITWINDOW")
            If (Trim(Temp1Date) <> "") Then
                Temp1Date = Mid(Temp1Date, 7, 4) + Mid(Temp1Date, 1, 2) + Mid(Temp1Date, 4, 2)
                TempDate = Mid(TDate, 7, 4) + Mid(TDate, 1, 2) + Mid(TDate, 4, 2)
                If (TempDate < Temp1Date) Then
                    Temp1Date = ""
                    Call FormatTodaysDate(Temp1Date, False)
                    txtDate.Text = Temp1Date
                End If
            End If
            lblPayRef.Visible = True
            txtRefId.Visible = True
            txtRefId.Text = ""
            lblReason.Visible = True
            txtReason.Visible = True
            txtReason.Text = ""
            lblSrv.Visible = True
            lstSrv.Visible = True
            lstSrv.ListIndex = -1
            lblAll.Visible = True
            txtAll.Visible = True
            txtAll.Text = ""
            lblTrans.Visible = True
            chkInclude.Visible = True
            chkInclude.Value = 0
            lstPayType.Visible = True
            lblMethod.Visible = True
            lstMethod.Visible = True
            If (Trim(txtChkAmt.Text) = "") Then
                lstPayType.ListIndex = -1
                lstMethod.ListIndex = -1
            End If
            cmdAll.Visible = False
            cmdPrev.Visible = False
            cmdNext.Visible = False
            If (Left(lstPayment.List(lstPayment.ListIndex), 1) = "+") Then
                Temp = lstPayment.List(lstPayment.ListIndex)
                Mid(Temp, 1, 1) = "-"
                lstPayment.List(lstPayment.ListIndex) = Temp
            Else
                For z = lstPayment.ListIndex - 1 To 0 Step -1
                    If (Left(lstPayment.List(z), 1) = "+") Then
                        Temp = lstPayment.List(z)
                        Mid(Temp, 1, 1) = "-"
                        lstPayment.List(z) = Temp
                        Exit For
                    End If
                Next z
            End If
            lstPayment.Enabled = False
            lblRefer.Visible = False
            lblRefCert.Visible = False
        End If
    Else
        lstPayment.ListIndex = -1
        TheCheckId = 0
        PlanId = 0
        PaymentId = 0
        PayerType = ""
        PaymentType = ""
        lblVoidDate.Visible = False
        txtVoidDate.Visible = False
        lblChkAmt.Visible = False
        txtChkAmt.Visible = False
        txtChkAmt.Text = ""
        txtCheck.Text = ""
        txtRefId.Text = ""
        txtAmount.Text = ""
        txtReason.Text = ""
        txtDate.Text = DefaultPayDate
        Temp1Date = CheckConfigCollection("EDITWINDOW")
        If (Trim(Temp1Date) <> "") Then
            Temp1Date = Mid(Temp1Date, 7, 4) + Mid(Temp1Date, 1, 2) + Mid(Temp1Date, 4, 2)
            TempDate = Mid(DefaultPayDate, 7, 4) + Mid(DefaultPayDate, 1, 2) + Mid(DefaultPayDate, 4, 2)
            If (TempDate < Temp1Date) Then
                Temp1Date = ""
                Call FormatTodaysDate(Temp1Date, False)
                txtDate.Text = Temp1Date
            End If
        End If
        chkInclude.Value = 0
        lblInvoice.Caption = ""
        lstMethod.ListIndex = -1
        lstPayType.ListIndex = -1
        lstSrv.ListIndex = -1
        txtAll.Text = ""
        lblRefer.Visible = True
        lblRefCert.Visible = True
        If (TotalFound > 2) And (DisplayHow <> "A") Then
            cmdPrev.Visible = True
            cmdNext.Visible = True
        End If
        cmdAll.Visible = True
    End If
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Sub

Private Sub lstPayType_Click()
Dim TheDate As String
Dim OtherType As String
If (lstPayType.ListIndex >= 0) Then
    If (lstPayment.ListIndex < 0) Then
        frmEventMsgs.Header = "Select Receivable"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        lstPayType.ListIndex = -1
        Exit Sub
    End If
    PaymentType = Mid(lstPayType.List(lstPayType.ListIndex), 40, 1)
    OtherType = Mid(lstPayType.List(lstPayType.ListIndex), 38, 1)
    lblVoidDate.Visible = False
    txtVoidDate.Visible = False
    lblPayAmt.Visible = True
    txtAmount.Visible = True
    If (PaymentType = "V") Then
        If (Mid(lstDetails.List(lstDetails.ListIndex), 75, 1) = "P") Then
            lblVoidDate.Visible = True
            txtVoidDate.Visible = True
            If (Trim(txtVoidDate.Text) = "") Then
                TheDate = ""
                Call FormatTodaysDate(TheDate, False)
                txtVoidDate.Text = TheDate
            End If
        Else
            frmEventMsgs.Header = "Voids require payment item selections"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            lstPayType.ListIndex = -1
        End If
    ElseIf (PaymentType = "D") Then
'        lblPayAmt.Visible = False
'        txtAmount.Visible = False
'        txtAmount.Text = "" (by Request)
    ElseIf (Trim(OtherType) = "") Then
'        lblPayAmt.Visible = False
'        txtAmount.Visible = False
        If (Trim(txtAmount.Text) = "") Then
            txtAmount.Text = "0"
        End If
    End If
End If
End Sub

Private Sub lstRef_Click()
If (lstRef.ListIndex >= 0) Then
    If (lstPayment.ListIndex < 0) Then
        frmEventMsgs.Header = "Must Select a Receivable"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        lstRef.ListIndex = -1
    Else
        PlanId = val(Trim(Mid(lstRef.List(lstRef.ListIndex), 77, Len(lstRef.List(lstRef.ListIndex)) - 76)))
        PayerType = Mid(lstRef.List(lstRef.ListIndex), 76, 1)
    End If
End If
End Sub

Private Sub txtChkAmt_KeyPress(KeyAscii As Integer)
If (lstPayment.ListIndex < 0) Then
    frmEventMsgs.Header = "Payment/Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtChkAmt.Text = ""
    Exit Sub
End If
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtAmount_KeyPress(KeyAscii As Integer)
If (lstPayment.ListIndex < 0) Then
    frmEventMsgs.Header = "Payment/Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtAmount.Text = ""
    Exit Sub
End If
If (KeyAscii <> 13) And (KeyAscii <> 10) Then
    If Not (IsCurrency(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Valid Currency Characters [-0123456789.]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtBillDr_Click()
Call txtBillDr_KeyPress(0)
End Sub

Private Sub txtBillDr_KeyPress(KeyAscii As Integer)
KeyAscii = 0
frmSelectDialogue.InsurerSelected = Trim(Str(PlanId))
Call frmSelectDialogue.BuildSelectionDialogue("RESOURCEWITHAFF")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtBillDr.Text = frmSelectDialogue.Selection
End If
End Sub

Private Sub txtDOS_Validate(Cancel As Boolean)
Call txtDOS_KeyPress(13)
End Sub

Private Sub txtDOS_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (OkDate(txtDOS.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDOS.SetFocus
        txtDOS.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtDOS, "D")
    End If
End If
End Sub

Private Sub txtDate_Validate(Cancel As Boolean)
Call txtDate_KeyPress(13)
End Sub

Private Sub txtDate_KeyPress(KeyAscii As Integer)
Dim ApplTemp As ApplicationTemplates
If (lstPayment.ListIndex < 0) Then
    frmEventMsgs.Header = "Payment/Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtDate.Text = ""
    Exit Sub
End If
If (KeyAscii = 13) Then
    If Not (OkDate(txtDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDate.Text = ""
        txtDate.SetFocus
    Else
        If (ReceivableId > 0) And (Trim(txtDate.Text) <> "") Then
            Call UpdateDisplay(txtDate, "D")
            Set ApplTemp = New ApplicationTemplates
            If (ApplTemp.ApplIsClaimEditable(ReceivableId, txtDate.Text)) Then
                DefaultPayDate = Trim(txtDate.Text)
            Else
                frmEventMsgs.Header = "Date cannot be older than the close date"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                KeyAscii = 0
                txtDate.Text = DefaultPayDate
                txtDate.SetFocus
            End If
            Set ApplTemp = Nothing
        Else
            Call UpdateDisplay(txtDate, "D")
            DefaultPayDate = Trim(txtDate.Text)
        End If
    End If
End If
End Sub

Private Sub txtReason_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PaymentReason")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If Not (txtReason.Locked) Then
        txtReason.Text = UCase(frmSelectDialogue.Selection)
    End If
End If
End Sub

Private Sub txtReason_KeyPress(KeyAscii As Integer)
Call txtReason_Click
End Sub

Private Sub txtVoidDate_Validate(Cancel As Boolean)
If Not (OkDate(txtVoidDate.Text)) Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtVoidDate.Text = ""
    txtVoidDate.SetFocus
Else
    Call UpdateDisplay(txtVoidDate, "D")
End If
End Sub

Private Sub txtVoidDate_KeyPress(KeyAscii As Integer)
If (lstPayment.ListIndex < 0) Then
    frmEventMsgs.Header = "Payment/Receivable must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
    txtVoidDate.Text = ""
    Exit Sub
End If
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtVoidDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtVoidDate.Text = ""
        txtVoidDate.SetFocus
    Else
        Call UpdateDisplay(txtVoidDate, "D")
    End If
End If
End Sub

Private Sub cmdHome_Click()
If (lstTrans.Visible) Then
    Call cmdDone_Click
Else
    CurrentAction = "Home"
    ReceivableId = 0
    Unload frmPayments
End If
End Sub

Private Sub cmdDone_Click()
Dim Amt As Single
Dim AFee As Single
Dim z As Integer, IType As Integer
Dim PlanId As Long, ItemId As Long
Dim TheCom As Boolean, TrigOn As Boolean
Dim Temp As String, ARsn As String
Dim TheSrv As String, TheRef As String
Dim PayType As String, PayMethod As String
Dim TempDate As String, Temp1Date As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
If (lstTrans.Visible) Then
    lstFilter.Visible = False
    lstTrans.Visible = False
    lstDetails.Visible = True
    lstPayment.Visible = True
    Call lstPayment_MouseDown(1, 0, 60, 120)
End If
If (lstDetails.Visible) Then
    If (PlanId < 1) And (lstRef.ListIndex >= 0) Then
        If lstRef.Visible Then
            If (PlanId < 1) And (lstRef.ListIndex >= 0) Then
                PlanId = val(Mid(lstRef.List(lstRef.ListIndex), 77, Len(lstRef.List(lstRef.ListIndex)) - 76))
            Else
                If ((lstPayType.ListIndex >= 0) And (lstPayType.Visible)) Or (Trim(txtAmount.Text) <> "") Then
                    lstRef.SetFocus
                    frmEventMsgs.Header = "Plan/Payer must be selected"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                Else
                    Unload frmPayments
                End If
                Exit Sub
            End If
        End If
        If (lstPayType.ListIndex < 0) Then
            If (lstPayType.Visible) Then
                frmEventMsgs.Header = "Transaction Type must be selected"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                lstPayType.SetFocus
                Exit Sub
            Else
                Exit Sub
            End If
        Else
            PayType = Mid(lstPayType.List(lstPayType.ListIndex), 40, 1)
        End If
        If (lstRef.ListIndex <> 0) And (Trim(txtAmount.Text) = "") And (PayType <> "D") Then
            lstPayment.ListIndex = -1
            Unload frmPayments
            Exit Sub
        End If
        If (lstMethod.ListIndex < 0) Then
            If (lstMethod.Visible) Then
                If (PayType = "P") Or (PayType = "R") Then
                    frmEventMsgs.Header = "Method must be selected"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    lstMethod.SetFocus
                    Exit Sub
                End If
                PayMethod = ""
            End If
        Else
            PayMethod = Mid(lstMethod.List(lstMethod.ListIndex), 40, 1)
        End If
        If (RequireCheck) Then
            If (PayType = "P") And (Trim(txtCheck.Text) = "") And (PayMethod = "K") Then
                If (txtCheck.Visible) Then
                    frmEventMsgs.Header = "Check must be entered"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtCheck.SetFocus
                End If
                Exit Sub
            End If
        End If
        If (txtAmount.Visible) Then
            If (Trim(txtAmount.Text) = "") Then
                frmEventMsgs.Header = "Payment Amount must be entered"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                txtAmount.SetFocus
                SendKeys "{Home}"
                Exit Sub
            End If
        End If
        If (Trim(txtDate.Text) = "") Then
            If (txtDate.Visible) Then
                frmEventMsgs.Header = "Payment Date must be entered"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                txtDate.SetFocus
                SendKeys "{Home}"
                Exit Sub
            Else
                Exit Sub
            End If
        End If
        If (txtVoidDate.Visible) Then
            If (Trim(txtVoidDate.Text) = "") Then
                If (txtVoidDate.Visible) Then
                    frmEventMsgs.Header = "Void Date must be entered"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtVoidDate.SetFocus
                    SendKeys "{Home}"
                    Exit Sub
                Else
                    Exit Sub
                End If
            End If
        End If
        If (lstRef.ListIndex = 0) And (PayType = "P") Then
            If (lstRef.Visible) Then
                frmEventMsgs.Header = "Payment cannot be used with Office"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                lstPayType.ListIndex = -1
                lstPayType.SetFocus
                SendKeys "{Home}"
                Exit Sub
            Else
                Exit Sub
            End If
        End If
        If (lstSrv.ListIndex < 0) Then
            frmEventMsgs.Header = "Service must be assigned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            lstSrv.ListIndex = -1
            lstSrv.SetFocus
            SendKeys "{Home}"
            Exit Sub
        End If
        ARsn = Trim(txtReason.Text)
        z = InStrPS(txtReason.Text, "-")
        If (z > 0) Then
            ARsn = Trim(Left(txtReason.Text, z - 1))
        End If
        Set ApplTemp = New ApplicationTemplates
        TheRef = ""
        If (Trim(txtRefId.Tag) = "~") Then
            TheRef = "~"
        End If
        TheCom = False
        If (chkInclude.Value = 1) Then
            TheCom = True
        End If
        TheSrv = Trim(lstDetails.Tag)
        If (lstSrv.ListIndex >= 0) Then
            TheSrv = Trim(Left(lstSrv.List(lstSrv.ListIndex), 10))
            ItemId = val(Trim(Mid(lstSrv.List(lstSrv.ListIndex), 76, Len(lstSrv.List(lstSrv.ListIndex)) - 75)))
        End If
        AFee = val(Trim(txtAll.Text))
        If (PaymentId <> 0) Then
            If Not (ApplTemp.ApplIsClaimEditable(ReceivableId, txtDate.Text)) Then
                frmEventMsgs.Result = 2
            Else
                frmEventMsgs.Header = "Caution: You are changing an existing transaction."
                frmEventMsgs.AcceptText = "New Payment"
                frmEventMsgs.RejectText = "Change Payment"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
            If (frmEventMsgs.Result = 1) Then
                PaymentId = 0
            ElseIf (frmEventMsgs.Result = 4) Then
                Exit Sub
            End If
        End If
        If (PaymentId = 0) Then
            Temp1Date = CheckConfigCollection("EDITWINDOW")
            If (Trim(Temp1Date) <> "") Then
                Temp1Date = Mid(Temp1Date, 7, 4) + Mid(Temp1Date, 1, 2) + Mid(Temp1Date, 4, 2)
                TempDate = Mid(txtDate.Text, 7, 4) + Mid(txtDate.Text, 1, 2) + Mid(txtDate.Text, 4, 2)
                If (TempDate < Temp1Date) Then
                    frmEventMsgs.Header = "Payment Date Cannot be older than Close Date."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtDate.Text = ""
                    txtDate.SetFocus
                    Exit Sub
                End If
            End If
        End If
        IType = ApplTemp.ApplPostPaymentActivity(ReceivableId, PatientId, PaymentId, _
                                                lblInvoice.Caption, PayerType, PlanId, _
                                                txtAmount.Text, txtDate.Text, PayType, _
                                                PayMethod, txtVoidDate.Text, txtCheck.Text, _
                                                TheRef + Trim(txtRefId.Text), _
                                                TheSrv, ItemId, TheCom, UserId, _
                                                True, ARsn, 0, "", "", AFee)
        If (IType = 0) Then
            lblChkAmt.Visible = False
            txtChkAmt.Visible = False
            If (PayType = "P") And (Trim(txtChkAmt.Text) <> "") Then
                Amt = val(Trim(txtChkAmt.Text)) - val(Trim(txtAmount.Text))
                If (Amt <= 0) Then
                    txtChkAmt.Text = ""
                Else
                    Call DisplayDollarAmount(Trim(Str(Amt)), Temp)
                    txtChkAmt.Text = Trim(Temp)
                End If
            End If
            CurrentAction = ""
            Call ResetOuterList(MidPointIndex, True)
        ElseIf (IType = -9) Then
            frmEventMsgs.Header = "Insurer is not set, Set Insurer"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        ElseIf (IType = -10) Then
            frmEventMsgs.Header = "Outstanding Claim not reconciled, Payment not applied."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
        Set ApplTemp = Nothing
    ElseIf (Trim(txtAmount.Text) <> "") Then
        lstRef.SetFocus
        frmEventMsgs.Header = "Plan/Payer must be selected"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        lblIParty.Visible = False
        lstDetails.Visible = False
        lstTrans.Visible = False
        lstPayment.Enabled = True
        lblPayer.Visible = False
        lstRef.Visible = False
        lblPayAmt.Visible = False
        txtAmount.Visible = False
        txtAmount.Text = ""
        lblReason.Visible = False
        txtReason.Visible = False
        txtReason.Text = ""
        lblPayRef.Visible = False
        txtRefId.Visible = False
        txtRefId.Text = ""
        lblTrans.Visible = False
        lstPayType.Visible = False
        lstPayType.ListIndex = -1
        lblPayDate.Visible = False
        txtDate.Visible = False
        txtDate.Text = ""
        lblMethod.Visible = False
        lstMethod.Visible = False
        lstMethod.ListIndex = -1
        lblVoidDate.Visible = False
        txtVoidDate.Visible = False
        txtVoidDate.Text = ""
        lblCheck.Visible = False
        txtCheck.Visible = False
        txtCheck.Text = ""
        lblChkAmt.Visible = False
        txtChkAmt.Visible = False
        txtChkAmt.Text = ""
        chkInclude.Visible = False
        lblSrv.Visible = False
        lstSrv.Visible = False
        lstSrv.ListIndex = -1
        lblAll.Visible = False
        txtAll.Visible = False
        txtAll.Text = ""
        If (lstPayment.ListIndex >= 0) Then
            Set ApplList = New ApplicationAIList
            Temp = lstPayment.List(lstPayment.ListIndex)
            If (ApplList.ApplIsBillingNotesOn(PatientId, "R" + Trim(Mid(Temp, 14, 11)))) Then
                Mid(Temp, 60, 3) = "-N-"
            ElseIf (ApplList.ApplIsBillingNotesOn(PatientId, "R" + Trim(Mid(Temp, 14, 11)) + "C~")) Then
                Mid(Temp, 60, 3) = "-C-"
            End If
            lstPayment.List(lstPayment.ListIndex) = Temp
            Set ApplList = Nothing
        End If
        For z = 0 To lstPayment.ListCount - 1
            If (Left(lstPayment.List(z), 1) = "-") Then
                Temp = lstPayment.List(z)
                Mid(Temp, 1, 1) = "+"
                lstPayment.List(z) = Temp
            End If
        Next z
        lblRefer.Visible = True
        lblRefCert.Visible = True
        If (TotalFound > 2) And (DisplayHow <> "A") Then
            cmdPrev.Visible = True
            cmdNext.Visible = True
        End If
        cmdAll.Visible = True
    End If
Else
    ReceivableId = 0
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ResetReceivableBalancesbyPatient(PatientId)
    Set ApplTemp = Nothing
    Unload frmPayments
End If
End Sub

Public Function BillHow(RecId As Long, Whom As String, TheAmt As String, SrvItem As Long) As Boolean
Dim z As Integer, ATmpId As Long
Dim p1 As Long, IPlan As Long, ApptId As Long
Dim NPIOn As Boolean
Dim NPIAll As Boolean
Dim AllowZero As Boolean
Dim InsThere As Boolean, ProcessHow As Boolean
Dim Temp As String, InvId As String, IDate As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
BillHow = True
ProcessHow = False
AllowZero = False
NPIOn = CheckConfigCollection("NPIONLY") = "T"
NPIAll = CheckConfigCollection("NPIALL") = "T"
frmEventMsgs.Header = ""
Set ApplTemp = New ApplicationTemplates
Set ApplList = New ApplicationAIList
' Get The Invoice
Call ApplTemp.ApplGetInvoice(RecId, InvId, IDate, IPlan, ApptId)
If (Whom = "I") Then
    AllowZero = CheckConfigCollection("RPTPATPAY") = "T"
    If (ApplTemp.ApplIsReceivablePlanSet(RecId, ATmpId)) Then
' restricted, bypass except where noted
        z = ApplTemp.ApplIsInvoiceVerified(InvId)
        If (z = -1) Then
            frmEventMsgs.Header = "Primary Insurer needs to be correctly set. Set Insurer."
            z = 0
            frmEventMsgs.Header = ""
        ElseIf (z = -2) Then
            frmEventMsgs.Header = "Secondary Insurer needs to be correctly set. Set Insurer."
            z = 0
            frmEventMsgs.Header = ""
        ElseIf (z = -3) Then
            frmEventMsgs.Header = "Third Insurer needs to be correctly set. Set Insurer."
            z = 0
            frmEventMsgs.Header = ""
        ElseIf (z = -99) Then
            frmEventMsgs.Header = "Patient has no Financial records."
        End If
        If (z = 0) Then
            If Not (ApplTemp.ApplIsReceivableVerified(RecId, "I")) Then
                frmEventMsgs.Header = "Incorrect Insurer, Rebill and choose new payer."
            End If
        End If
    Else
        frmEventMsgs.Header = "Insurer is not set, Set Insurer"
    End If
Else
    If Not (ApplTemp.ApplIsReceivableVerified(RecId, "P")) Then
        frmEventMsgs.Header = "Incorrect Insurer, Rebill and choose new payer"
    End If
End If
If (Whom = "I") Then
    If CheckConfigCollection("REQUIREBILLDR") = "T" Then
        If Not (ApplTemp.ApplIsDoctorBillable(RecId)) Then
            frmEventMsgs.Header = "Receivable requires a billable doctor"
        End If
    End If
    If CheckConfigCollection("REQUIREREFERRALTOBILL") = "T" Then
        If (ApplTemp.ApplIsReceivableReferralRequired(RecId)) Then
            frmEventMsgs.Header = "Receivable requires a referral to be billed"
        End If
    End If
    If CheckConfigCollection("REQUIREREFDRUPIN") = "T" Then
        If Not (NPIOn) And Not (NPIAll) Then
            If Not (ApplTemp.ApplIsRefDrUPinOn(RecId)) Then
                frmEventMsgs.Header = "Referring Doctor requires a UPIN"
            End If
            If Not (ApplTemp.ApplIsRefDrAffOn(RecId)) Then
                frmEventMsgs.Header = "Referring Doctor requires a PIN"
            End If
        End If
    End If
    If (NPIOn) Or (NPIAll) Then
        If Not (ApplTemp.ApplIsRefDrNPIOn(RecId)) Then
            frmEventMsgs.Header = "Referring Doctor requires an NPI"
        End If
    End If
    If Not (ApplTemp.ApplIsRefDrNameOn(RecId)) Then
        frmEventMsgs.Header = "Referring Doctor requires a First And Last Name"
    End If
    If (ApplTemp.ApplIsServiceReferralRequired(RecId)) Then
        frmEventMsgs.Header = "Consults with Diagnostic Tests Need Ref Dr"
    End If
    Temp = ApplTemp.ApplPatientLevelVerification(RecId)
    If (Trim(Temp) <> "") Then
        frmEventMsgs.Header = Temp
    End If
    Temp = ApplTemp.ApplDiagnosisLevelVerification(RecId)
    If (Trim(Temp) <> "") Then
        frmEventMsgs.Header = Temp
    End If
' Applies only to Medicaids
    If (ApplTemp.ApplIsInsurerMedicaidNJ(IPlan)) Then
        If Not (ApplTemp.ApplIsRefDrUniqueForServices(RecId)) Then
            Temp = "Referring Dr must be assigned and not the rendering physician"
            frmEventMsgs.Header = Temp
        ElseIf Not (ApplTemp.ApplIsRefDrAffPresent(RecId)) And (Not (NPIOn)) Then
            Temp = "Referring Dr must be assigned an affiliation number"
            frmEventMsgs.Header = Temp
        End If
    End If
' Verify the UPin
    If (NPIOn) Or (NPIAll) Then
        If Not (ApplTemp.ApplIsNPIValid(RecId)) Then
            Temp = "NPI is not valid"
            frmEventMsgs.Header = Temp
        End If
    Else
        If Not (ApplTemp.ApplIsUPinValid(RecId)) Then
            Temp = "Upin is not valid"
            frmEventMsgs.Header = Temp
        End If
    End If
End If
If (Trim(frmEventMsgs.Header) <> "") Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    BillHow = False
Else
    If (Whom = "I") Then
        ProcessHow = True
        If Not (ApplTemp.ApplIsInsurerPrimary(RecId)) Then
            If (ApplTemp.ApplIsInsurerPlanPaper(IPlan)) Then
                ProcessHow = False
            End If
        End If
    End If
    If (ProcessHow) Then
        If (ApplTemp.ApplIsElectronic(RecId)) Then
            If (ApplTemp.ApplIsElectronicValid(RecId)) Then
                p1 = ApplTemp.BatchTransaction(RecId, Whom, TheAmt, "B", True, SrvItem, False, False, AllowZero)
                If (p1 < 1) Then
                    BillHow = False
                End If
            Else
                BillHow = False
                frmEventMsgs.Header = "Insurer needs a valid NEIC Number"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
        Else
            p1 = ApplTemp.BatchTransaction(RecId, Whom, TheAmt, "P", True, SrvItem, False, False, AllowZero)
            If (p1 < 1) Then
                BillHow = False
            End If
        End If
    Else
        p1 = ApplTemp.BatchTransaction(RecId, Whom, TheAmt, "P", True, SrvItem, False, False, AllowZero)
        If (p1 < 1) Then
            BillHow = False
        End If
    End If
    If (BillHow) And (Whom = "I") Then
        Call ApplTemp.ApplProcessAutoCross(RecId)
    End If
    Call ApplList.ApplSetReceivableBalance(InvId, "", True)
End If
Set ApplList = Nothing
Set ApplTemp = Nothing
End Function

Private Sub PatientInsurerData()
Dim ApplList As ApplicationAIList
If (PatientId > 0) Then
    If (PolicyHolderid < 1) Then
        PolicyHolderid = PatientId
    End If
    frmPatientFinancial.ThePolicyHolder1 = PolicyHolderid
    frmPatientFinancial.ThePolicyHolder1Rel = PolicyHolderIdRel
    frmPatientFinancial.ThePolicyHolder1Bill = PolicyHolderIdBill
    frmPatientFinancial.ThePolicyHolder2 = SecondPolicyHolderId
    frmPatientFinancial.ThePolicyHolder2Rel = SecondPolicyHolderIdRel
    frmPatientFinancial.ThePolicyHolder2Bill = SecondPolicyHolderIdBill
    If (frmPatientFinancial.FinancialLoadDisplay(PatientId, PolicyHolderid, SecondPolicyHolderId, "M", True, True)) Then
        frmPatientFinancial.CurrentAction = ""
        frmPatientFinancial.Show 1
        If (InStrPS(frmPatientFinancial.CurrentAction, "Home") = 0) Then
            SecondPolicyHolderId = frmPatientFinancial.ThePolicyHolder2
            SecondPolicyHolderIdRel = frmPatientFinancial.ThePolicyHolder2Rel
            SecondPolicyHolderIdBill = frmPatientFinancial.ThePolicyHolder2Bill
            PolicyHolderid = frmPatientFinancial.ThePolicyHolder1
            PolicyHolderIdRel = frmPatientFinancial.ThePolicyHolder1Rel
            PolicyHolderIdBill = frmPatientFinancial.ThePolicyHolder1Bill
            CurrentIndex = CurrentIndex - 2
            If (CurrentIndex <= 1) Then
                CurrentIndex = 0
                PreviousIndex = 0
                Call ResetOuterList(MidPointIndex, False)
            End If
            If (frmPatientFinancial.ArchiveChanged) Then
                Call ResetOuterList(MidPointIndex, False)
            End If
        End If
    End If
End If
End Sub

Public Function IsPrintTransactional() As Integer
IsPrintTransactional = 0
frmEventMsgs.Header = "Post a Transaction ?"
frmEventMsgs.AcceptText = "Yes"
frmEventMsgs.RejectText = "No"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    IsPrintTransactional = 1
ElseIf (frmEventMsgs.Result = 2) Then
    IsPrintTransactional = 2
End If
End Function

Private Function ResetOuterList(Factor As Integer, PostOn As Boolean) As Boolean
Dim Temp1 As Integer
Dim Temp2 As Integer
ResetOuterList = True
CurrentIndex = Factor
If (CurrentIndex <= 1) Then
    CurrentIndex = 0
    MidPointIndex = 0
    PreviousIndex = 0
End If
Temp1 = lstPayment.ListIndex
Temp2 = lstDetails.ListIndex
Call LoadPayments(False, InvDate)
If (Temp1 >= 0) And (Temp1 <= lstPayment.ListCount - 1) Then
    If (PostOn) Or (Temp2 >= 0) Then
        lstDetails.ListIndex = -1
        If (IsReceivableStillInList(ReceivableId)) Then
            TriggerDetails = True
            lstPayment.ListIndex = Temp1
            Call lstPayment_Click
            TriggerDetails = False
        End If
    End If
End If
End Function

Private Function IsReceivableStillInList(RcvId As Long) As Boolean
Dim i As Integer
IsReceivableStillInList = False
For i = 0 To lstPayment.ListCount - 1
    If (Mid(lstPayment.List(i), 75, 1) = "R") Then
        If (RcvId = val(Trim(Mid(lstPayment.List(i), 76, Len(lstPayment.List(i)) - 75)))) Then
            IsReceivableStillInList = True
        End If
    End If
Next i
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
