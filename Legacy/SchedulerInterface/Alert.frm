VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmAlert 
   BackColor       =   &H00505050&
   Caption         =   "Alerts"
   ClientHeight    =   4920
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6690
   ClipControls    =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "Alert.frx":0000
   ScaleHeight     =   4920
   ScaleWidth      =   6690
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtAlert 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   240
      Width           =   6375
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   750
      Left            =   2640
      TabIndex        =   1
      Top             =   4080
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Alert.frx":36C9
   End
End
Attribute VB_Name = "frmAlert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private PatientId As Long
Private AlertType As Integer
Private CurrentIndex As Integer

Private Sub txtAlert_Click()
    On Error GoTo ltxtAlert_Click_Error

    If UserLogin.HasPermission(epSetAlerts) Then
        AlertMaintain
    End If

    Exit Sub

ltxtAlert_Click_Error:

    LogError "frmAlert", "txtAlert_Click", Err, Err.Description
End Sub

Private Sub cmdDone_Click()
    Unload frmAlert
End Sub

Public Function LoadAlerts(PatId As Long, AType As Integer, Init As Boolean, Optional ResId As Long) As Boolean
    'REFACTOR: Changed ResId to Optional so that it can be removed in the future.
Dim iIndex As Integer
Dim RetNote As New PatientNotes
Dim oApptDate As New CIODateTime
    On Error GoTo lLoadAlerts_Error

    If PatId > 0 And AType > 0 Then
        If Init Then
            CurrentIndex = 1
        End If
        PatientId = PatId
        AlertType = AType
        RetNote.NotesPatientId = PatId
        If RetNote.FindNotesbyPatientAlert > 0 Then
            iIndex = CurrentIndex
            Do Until Not (RetNote.SelectNotes(iIndex))
                If Mid$(RetNote.NotesAlertMask, AType, 1) <> "0" Then
                    oApptDate.SetDateFromIO RetNote.NotesDate
                    txtAlert.Text = txtAlert.Text _
                                  & "[" & Trim$(str(RetNote.NotesId)) & "]-" & oApptDate.GetDisplayDate(False) & " " _
                                  & Trim$(RetNote.NotesText1) & " " _
                                  & Trim$(RetNote.NotesText2) & " " _
                                  & Trim$(RetNote.NotesText3) & " " _
                                  & Trim$(RetNote.NotesText4) & vbCrLf
                    LoadAlerts = True
                End If
                iIndex = iIndex + 1
            Loop
            CurrentIndex = iIndex
        End If
    End If
    Set RetNote = Nothing
    Set oApptDate = Nothing

    Exit Function

lLoadAlerts_Error:

    LogError "frmAlert", "LoadAlerts", Err, Err.Description, , PatId
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Unload frmAlert
End Sub

Private Sub AlertMaintain()
Dim ANoteId As Long
Dim i As Integer
Dim k As Integer
    On Error GoTo lAlertMaintain_Error

    For k = txtAlert.SelStart To 1 Step -1
        If Mid$(txtAlert.Text, k, 1) = "[" Then
            i = k
            k = InStrPS(i, txtAlert.Text, "]")
            If k <= i Then
                k = i + 5
            End If
            ANoteId = Val(Mid$(txtAlert.Text, i + 1, (k - 1) - i))
            Exit For
        End If
    Next k
    If ANoteId > 0 Then
        frmEventMsgs.SetButtons "Clear Alert", "", "Yes", "No"
        frmEventMsgs.Show 1
        If frmEventMsgs.Result = 2 Then
            ClearAlert ANoteId
            txtAlert.Text = ""
            LoadAlerts PatientId, AlertType, True
        End If
    End If

    Exit Sub

lAlertMaintain_Error:

    LogError "frmAlert", "AlertMaintain", Err, Err.Description
End Sub

Private Function ClearAlert(iNoteId As Long) As Boolean
Dim sTemp As String
Dim RetNote As New PatientNotes
    On Error GoTo lClearAlert_Error

    If iNoteId > 0 And AlertType > 0 Then
        RetNote.NotesId = iNoteId
        If (RetNote.RetrieveNotes) Then
            sTemp = RetNote.NotesAlertMask
            Mid$(sTemp, AlertType, 1) = "0"
            RetNote.NotesAlertMask = sTemp
            ClearAlert = RetNote.ApplyNotes
        End If
    End If
    Set RetNote = Nothing

    Exit Function

lClearAlert_Error:

    LogError "frmAlert", "ClearAlert", Err, Err.Description
End Function
