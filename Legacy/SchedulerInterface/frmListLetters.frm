VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmListLetters 
   BackColor       =   &H0073702B&
   BorderStyle     =   0  'None
   Caption         =   "Document List"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstPrint 
      Height          =   645
      ItemData        =   "frmListLetters.frx":0000
      Left            =   3600
      List            =   "frmListLetters.frx":0007
      Sorted          =   -1  'True
      TabIndex        =   8
      Top             =   360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   6600
      MultiSelect     =   1  'Simple
      TabIndex        =   4
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   8280
      MultiSelect     =   1  'Simple
      TabIndex        =   2
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   9960
      MultiSelect     =   1  'Simple
      TabIndex        =   1
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstDocs 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6060
      Left            =   240
      TabIndex        =   0
      Top             =   1320
      Width           =   11295
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   7828525
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   9960
      TabIndex        =   11
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":0015
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   1110
      Left            =   8040
      TabIndex        =   12
      Top             =   7440
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":01F4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   870
      Left            =   1560
      TabIndex        =   13
      Top             =   7440
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":03E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   870
      Left            =   240
      TabIndex        =   14
      Top             =   7440
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":05C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   1095
      Left            =   3120
      TabIndex        =   15
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":07A1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPurgeRecalls 
      Height          =   1110
      Left            =   6360
      TabIndex        =   16
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":098E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQueue 
      Height          =   1110
      Left            =   4800
      TabIndex        =   17
      Top             =   7440
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":0B7E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLegend 
      Height          =   510
      Left            =   240
      TabIndex        =   18
      Top             =   8400
      Visible         =   0   'False
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   900
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmListLetters.frx":0D5E
   End
   Begin VB.Label lblSending 
      AutoSize        =   -1  'True
      BackColor       =   &H0000FFFF&
      Caption         =   "Sending Msg"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2520
      TabIndex        =   10
      Top             =   840
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2520
      TabIndex        =   9
      Top             =   720
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label lblPage 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0073702B&
      Caption         =   "Page Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9600
      TabIndex        =   7
      Top             =   8640
      Width           =   1935
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Review Consult Letters"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   240
      TabIndex        =   6
      Top             =   240
      Width           =   2925
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   405
   End
End
Attribute VB_Name = "frmListLetters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const recsPerPage As Integer = 25
Private PendTrans As PendingTrans
Public TransType As String

Private Sub Form_Load()
Set PendTrans = New PendingTrans
Call LoadStaffList
Call LoadLocList
Call LoadStatusList
'lstStatus.Selected(0) = True
PendTrans.CurrentPage = 1
Call LoadDocList
TransType = "S"
End Sub

Private Sub LoadStaffList()

Dim staffFilter As ListFilters

lstDr.Visible = True
lstDr.Clear
lstDr.AddItem ("All Doctors")
Set staffFilter = New ListFilters
staffFilter.ListType = "R"
Call staffFilter.PopList(lstDr)

End Sub

Private Sub LoadLocList()

Dim locFilter As ListFilters

lstLoc.Visible = True
lstLoc.Clear
lstLoc.AddItem ("All Locations")
Set locFilter = New ListFilters
locFilter.ListType = "L"
Call locFilter.PopList(lstLoc)

End Sub

Private Sub LoadStatusList()

Dim statusFilter As ListFilters

lstStatus.Visible = True
lstStatus.Clear
lstStatus.AddItem ("All Status")
Set statusFilter = New ListFilters
statusFilter.ListType = "S"
Call statusFilter.PopList(lstStatus)

End Sub

Private Sub LoadDocList()

Label1.Visible = True
lblDate.Visible = False
SSDateCombo1.Visible = False
lstStatus.Visible = True
lstLoc.Visible = True
lstDocs.Visible = True
cmdPrev.Visible = True
cmdNext.Visible = True
cmdLegend.Visible = True
cmdAll.Visible = False
cmdPurgeRecalls.Visible = True
cmdPatient.Visible = True
cmdDone.Visible = True
lblPage.Visible = True

Dim DrFilter As String, statusFilter As String
Dim locFilter As String
Dim i As Integer

DrFilter = ""
If (lstDr.Selected(0) Or lstDr.SelCount = 0) Then
    For i = 1 To lstDr.ListCount - 1
        DrFilter = DrFilter + Mid(lstDr.List(i), 21, Len(lstDr.List(i)) - 20) + " "
    Next i
Else
    For i = 1 To lstDr.ListCount - 1
        If (lstDr.Selected(i)) Then
            DrFilter = DrFilter + Mid(lstDr.List(i), 21, Len(lstDr.List(i)) - 20) + " "
        End If
    Next i
End If
DrFilter = "'" + Mid(DrFilter, 1, Len(DrFilter) - 1) + "'"

locFilter = ""
If (lstLoc.Selected(0) Or lstLoc.SelCount = 0) Then
    For i = 1 To lstLoc.ListCount - 1
        If (Mid(lstLoc.List(i), 21, 1) = "P") Then
            If (Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) = "1") Then
                locFilter = locFilter + "0 "
            Else
                locFilter = locFilter + Trim(Str(1000 + CInt(Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21))))) + " "
            End If
        ElseIf (Mid(lstLoc.List(i), 21, 1) = "R") Then
            locFilter = locFilter + Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) + " "
        End If
    Next i
Else
    For i = 1 To lstLoc.ListCount - 1
        If (lstLoc.Selected(i)) Then
            If (Mid(lstLoc.List(i), 21, 1) = "P") Then
                If (Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) = "1") Then
                    locFilter = locFilter + "0 "
                Else
                    locFilter = locFilter + Trim(Str(1000 + CInt(Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21))))) + " "
                End If
            ElseIf (Mid(lstLoc.List(i), 21, 1) = "R") Then
                locFilter = locFilter + Trim(Mid(lstLoc.List(i), 22, Len(lstLoc.List(i)) - 21)) + " "
            End If
        End If
    Next i
End If
locFilter = "'" + Mid(locFilter, 1, Len(locFilter) - 1) + "'"

statusFilter = ""
If (lstStatus.Selected(0) Or lstStatus.SelCount = 0) Then
    For i = 1 To lstStatus.ListCount - 1
        statusFilter = statusFilter + Mid(lstStatus.List(i), 1, 1) + ","
    Next i
Else
    For i = 1 To lstStatus.ListCount - 1
        If (lstStatus.Selected(i)) Then
            statusFilter = statusFilter + Mid(lstStatus.List(i), 1, 1) + ","
        End If
    Next i
End If
If (InStrPS(1, statusFilter, "N") > 0) Then
    statusFilter = "'" + statusFilter + "'"
Else
    statusFilter = "'" + Mid(statusFilter, 1, Len(statusFilter) - 1) + "'"
End If
PendTrans.PendType = "S"
Call PendTrans.RetrieveList(recsPerPage, statusFilter, DrFilter, locFilter)
lstDocs.Clear
While Not (PendTrans.EOF)
    lstDocs.AddItem (PendTrans.DisplayList)
    Call PendTrans.NextRec
Wend
lblPage.Caption = ("Page " + Str(PendTrans.CurrentPage) + " of " + Str(PendTrans.MaxPage))

End Sub

Private Sub lstDr_Click()

Call LoadDocList

End Sub

Private Sub lstLoc_Click()

Call LoadDocList

End Sub

Private Sub lstStatus_Click()

Call LoadDocList

End Sub

Private Sub cmdNext_Click()
If (PendTrans.LastPage) Then
    PendTrans.CurrentPage = 1
Else
    PendTrans.CurrentPage = PendTrans.CurrentPage + 1
End If
Call LoadDocList
End Sub

Private Sub cmdPrev_Click()
If (PendTrans.CurrentPage = 1) Then
    PendTrans.CurrentPage = PendTrans.MaxPage
Else
    PendTrans.CurrentPage = PendTrans.CurrentPage - 1
End If
Call LoadDocList
End Sub

Private Sub lstDocs_dblClick()

Dim TranId As String

If (lstDocs.ListIndex >= 0) Then
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Send"
    frmEventMsgs.RejectText = "Edit"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Delete"
    frmEventMsgs.Other1Text = "Status"
    frmEventMsgs.Other2Text = "Select Recipient"
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (SendLetter(lstDocs.ListIndex, TransType)) Then
        Call LoadDocList
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        Call MaintainLetter(lstDocs.ListIndex, TransType, "E", False)
    ElseIf (frmEventMsgs.Result = 3) Then
        Call MaintainLetter(lstDocs.ListIndex, TransType, "D", False)
    ElseIf (frmEventMsgs.Result = 5) Then
        Call MaintainStatus(lstDocs, TransType)
    ElseIf (frmEventMsgs.Result = 6) Then
        Call SelectParty(lstDocs.ListIndex, TransType)
    ElseIf (frmEventMsgs.Result = 7) Then
        'Call SelectSurgicalRequest(lstDocs.ListIndex, TransType)
    ElseIf (frmEventMsgs.Result = 8) Then
        'Call SelectSurgical(lstDocs.ListIndex, TransType)
        'Call LoadDocumentsList(SendOn, TransType, PartyType, Completed, False)
    End If
End If

End Sub

Private Sub cmdDone_Click()
Set PendTrans = Nothing
Unload frmListLetters
Set frmListLetters = Nothing
End Sub

Private Function MaintainLetter(Idx As Integer, IType As String, ActionType As String, AutoCreateOn As Boolean) As Boolean
Dim u As Integer
Dim PatId As Long, TransId As Long
Dim Temp As String, KTemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
If (Idx >= 0) Then
    Set ApplLetters = New Letters
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
'    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    TransId = Trim(Mid(lstDocs.List(Idx), 98, 10))
    If (ActionType = "E") Then
        If (IType <> "Y") And (IType <> "W") Then
            Temp = ApplLetters.ApplLettersFile(TransId)
            If (Trim(Temp) <> "") Then
                KTemp = IType
                If (KTemp = "[") Then
                    KTemp = "a"
                ElseIf (KTemp = "]") Then
                    KTemp = "d"
                End If
                If Not (FM.IsFileThere(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")) Then
                    lblPrint.Caption = "File Creation In Progress"
                    lblPrint.Visible = True
                    DoEvents
                    Set ApplTemp.lstBox = lstPrint
                    Call ApplTemp.PrintTransaction(TransId, 2, KTemp, True, True, Temp, False, 0, "S", "")
                End If
                lblPrint.Visible = False
                DoEvents
                If Not (AutoCreateOn) Then
                    Call TriggerWord(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")
                End If
            End If
        ElseIf (IType = "W") Then
            u = InStrPS(95, lstDocs.List(Idx), "/")
            If (u > 0) Then
                TransId = Val(Mid(lstDocs.List(Idx), 95, (u - 1) - 94))
                PatId = Val(Mid(lstDocs.List(Idx), u + 1, Len(lstDocs.List(Idx)) - u))
                KTemp = IType
                If (Trim(CollectionLetter) <> "") Then
                    If Not (FM.IsFileThere(DocumentDirectory + "Coll" + Trim(CollectionLetter) + "_" + Trim(Str(PatId)) + "-" + Trim(Str(TransId)) + ".doc")) Then
                        lblPrint.Caption = "File Creation In Progress"
                        lblPrint.Visible = True
                        DoEvents
                        Set ApplTemp.lstBox = lstPrint
                        Call ApplTemp.PrintTransaction(TransId, PatId, "W", True, True, "/Ltr/" + Trim(CollectionLetter), False, 0, "S", "")
                    End If
                    lblPrint.Visible = False
                    DoEvents
                    If Not (AutoCreateOn) Then
                        Call TriggerWord(DocumentDirectory + "Coll" + Trim(CollectionLetter) + "_" + Trim(Str(PatId)) + "-" + Trim(Str(TransId)) + ".doc")
                    End If
                End If
            End If
        End If
    ElseIf (ActionType = "D") Then
        If (IType <> "W") Then
            Temp = ApplLetters.ApplLettersFile(TransId)
            Call ApplTemp.ApplPurgeTransaction(TransId, True)
            'Call LoadDocumentsList(SendOn, TransType, PartyType, Completed, False)
            Call LoadDocList
        Else
            'u = InStrPS(95, lstDocs.List(Idx), "/")
            'If (u > 0) Then
                'TransId = Val(Mid(lstDocs.List(Idx), 95, (u - 1) - 94))
                'PatId = Val(Mid(lstDocs.List(Idx), u + 1, Len(lstDocs.List(Idx)) - u))
                'If (ViewCollectionLetter(TransId, PatId)) Then
                    'CollectionAction = "D"
                'End If
            'End If
        End If
    End If
    Set ApplLetters = Nothing
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function MaintainStatus(AList As ListBox, IType As String) As Boolean
Dim w As Integer
Dim TransId As Long
Dim Temp As String
Dim ATemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("LETTERSTATUS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Temp = Left(frmSelectDialogue.Selection, 1)
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    For w = 0 To AList.ListCount - 1
        If (AList.Selected(w)) Then
'            TransId = ApplList.ApplGetTransId(AList.List(w))
            TransId = Trim(Mid(AList.List(w), 98, 10))
            If (TransId > 0) Then
                Call ApplList.ApplSetLetterStatus(TransId, Temp)
                ATemp = AList.List(w)
                Mid(ATemp, 1, 1) = Temp
                AList.List(w) = ATemp
'            Call LoadDocumentsList(SendOn, TransType, PartyType, Completed, False)
            End If
        End If
    Next w
    For w = 0 To AList.ListCount - 1
        AList.Selected(w) = False
    Next w
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function SendLetter(Idx As Integer, IType As String) As Boolean
Dim q As Integer
Dim TransId As Long
Dim Temp As String
Dim FileName As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
SendLetter = False
If (Idx >= 0) Then
    Set ApplLetters = New Letters
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
'    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    TransId = Trim(Mid(lstDocs.List(Idx), 98, 10))
    FileName = DocumentDirectory + "Ltrs-" + TransType + Trim(Str(TransId)) + ".doc"
    If Not (FM.IsFileThere(FileName)) Then
        frmEventMsgs.Header = "File must exist before transmitting"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplLetters = Nothing
        Set ApplTemp = Nothing
        Set ApplList = Nothing
        Exit Function
    End If
    frmEventMsgs.Header = "Send Letter How ?"
    frmEventMsgs.AcceptText = "E-Mail"
    frmEventMsgs.RejectText = "Fax"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Print"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (ApplList.ApplIsEmailOkay(TransId)) Then
            Call ApplList.ApplSetTransactionStatus(TransId, "B")
            Call ApplList.ApplSetTransactionAction(TransId, "E")
            SendLetter = True
        Else
            frmEventMsgs.Header = "No e-mail address"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (ApplList.ApplIsFaxOkay(TransId)) Then
            Call ApplList.ApplSetTransactionStatus(TransId, "B")
            Call ApplList.ApplSetTransactionAction(TransId, "F")
            SendLetter = True
        Else
            frmEventMsgs.Header = "No Fax Number"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Call ApplList.ApplSetTransactionStatus(TransId, "B")
        Call ApplList.ApplSetTransactionAction(TransId, "P")
        SendLetter = True
    End If
    Set ApplLetters = Nothing
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function SelectParty(Idx As Integer, IType As String) As Boolean
Dim q As Integer
Dim GotIt As Boolean
Dim Temp As String
Dim Ref As String, TempRef As String
Dim ATemp As String, VndName As String
Dim FaxNumber As String, EMailAddress As String
Dim VndId As Long, VndIdOrg As Long, TransId As Long
Dim ApplList As ApplicationAIList
SelectParty = False
If (Idx >= 0) Then
    Set ApplList = New ApplicationAIList
'    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    TransId = Trim(Mid(lstDocs.List(Idx), 98, 10))
    GotIt = False
    frmSelectDialogue.InsurerSelected = ""
    If (IType = "Y") Then
        Call frmSelectDialogue.BuildSelectionDialogue("HOSPITALS")
        frmSelectDialogue.Show 1
        GotIt = frmSelectDialogue.SelectionResult
        If (GotIt) Then
            Temp = Trim(frmSelectDialogue.Selection)
        End If
    ElseIf (IType = "X") Then
        Call frmSelectDialogue.BuildSelectionDialogue("LABS")
        frmSelectDialogue.Show 1
        GotIt = frmSelectDialogue.SelectionResult
        If (GotIt) Then
            Temp = Trim(frmSelectDialogue.Selection)
        End If
    Else
        frmEventMsgs.Header = "Select Party"
        frmEventMsgs.AcceptText = "PCP"
        frmEventMsgs.RejectText = "Referring Doctor"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Select Doctor"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            GotIt = ApplList.ApplGetPartyFromTransaction(TransId, "P", Temp)
        ElseIf (frmEventMsgs.Result = 2) Then
            GotIt = ApplList.ApplGetPartyFromTransaction(TransId, "R", Temp)
        ElseIf (frmEventMsgs.Result = 3) Then
            Call frmSelectDialogue.BuildSelectionDialogue("PCP")
            frmSelectDialogue.Show 1
            GotIt = frmSelectDialogue.SelectionResult
            If (GotIt) Then
                Temp = Trim(frmSelectDialogue.Selection)
                q = InStrPS(Temp, " ")
                If (q > 0) Then
                    FaxNumber = Trim(Mid(Temp, q, 55 - (q - 1))) + " " + Left(Temp, q - 1)
                    Temp = FaxNumber + Space(55 - Len(FaxNumber)) + Mid(Temp, 56, Len(Temp) - 55)
                    FaxNumber = ""
                End If
            End If
        End If
    End If
    If (GotIt) Then
        VndId = Val(Mid(Temp, 57, Len(Temp) - 56))
        Call ApplList.ApplGetVendorTransmitInfo(VndId, EMailAddress, FaxNumber, VndName)
        If (ApplList.ApplGetTransactionReference(TransId, Ref, VndIdOrg)) Then
            Ref = ""
            Call ApplList.ApplSetTransactionReference(TransId, Ref, VndId)
            ATemp = lstDocs.List(Idx)
            If (Len(VndName) > 23) Then
                VndName = Left(VndName, 23)
            Else
                VndName = VndName + Space(23 - Len(VndName))
            End If
            Mid(ATemp, 66, 23) = Left(VndName, 23)
            lstDocs.List(Idx) = ATemp
        End If
        SelectParty = True
    End If
    Set ApplList = Nothing
End If
End Function

