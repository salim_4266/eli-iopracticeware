VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmEvalAccess 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   990
      Left            =   600
      TabIndex        =   0
      Top             =   600
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   990
      Left            =   585
      TabIndex        =   1
      Top             =   1680
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":01E1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   990
      Left            =   585
      TabIndex        =   2
      Top             =   2760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":03C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   990
      Left            =   585
      TabIndex        =   3
      Top             =   3840
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":05A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   990
      Left            =   585
      TabIndex        =   4
      Top             =   4920
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":0784
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   990
      Left            =   585
      TabIndex        =   5
      Top             =   6000
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":0965
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   6240
      TabIndex        =   6
      Top             =   7080
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":0B46
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   990
      Left            =   4560
      TabIndex        =   7
      Top             =   600
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":0D25
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   990
      Left            =   4560
      TabIndex        =   8
      Top             =   1680
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":0F06
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   990
      Left            =   4560
      TabIndex        =   9
      Top             =   2760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":10E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   990
      Left            =   4560
      TabIndex        =   10
      Top             =   3840
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":12C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   990
      Left            =   4560
      TabIndex        =   11
      Top             =   4920
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":14A9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   990
      Left            =   4560
      TabIndex        =   12
      Top             =   6000
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":168A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt13 
      Height          =   990
      Left            =   8415
      TabIndex        =   13
      Top             =   600
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":186B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt14 
      Height          =   990
      Left            =   8400
      TabIndex        =   14
      Top             =   1680
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":1A4C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt15 
      Height          =   990
      Left            =   8400
      TabIndex        =   15
      Top             =   2760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":1C2D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt16 
      Height          =   990
      Left            =   8400
      TabIndex        =   16
      Top             =   3840
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":1E0E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt17 
      Height          =   990
      Left            =   8400
      TabIndex        =   17
      Top             =   4920
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":1FEF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt18 
      Height          =   990
      Left            =   8400
      TabIndex        =   18
      Top             =   6000
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":21D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   600
      TabIndex        =   19
      Top             =   7080
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":23B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   8400
      TabIndex        =   20
      Top             =   7080
      Visible         =   0   'False
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":2590
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   4560
      TabIndex        =   21
      Top             =   7080
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EvalAccess.frx":276F
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Select An Appointment Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   22
      Top             =   120
      Width           =   3210
   End
End
Attribute VB_Name = "frmEvalAccess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Public ApptTypeName As String
Public ApptTypeId As Long
Public ApptPeriod As String
Public lstBox As ListBox

Private SetBackColor As Long
Private SetForeColor As Long
Private BaseBackColor As Long
Private BaseForeColor As Long
Private CurrentAdmin As Integer
Private AdminList(40) As String
Private DisplayType As String
Private CurrentIndex As Integer
Private TotalAppts As Long

Private Sub ClearAppt()
cmdAppt1.Text = ""
cmdAppt1.Visible = False
cmdAppt2.Text = ""
cmdAppt2.Visible = False
cmdAppt3.Text = ""
cmdAppt3.Visible = False
cmdAppt4.Text = ""
cmdAppt4.Visible = False
cmdAppt5.Text = ""
cmdAppt5.Visible = False
cmdAppt6.Text = ""
cmdAppt6.Visible = False
cmdAppt7.Text = ""
cmdAppt7.Visible = False
cmdAppt8.Text = ""
cmdAppt8.Visible = False
cmdAppt9.Text = ""
cmdAppt9.Visible = False
cmdAppt10.Text = ""
cmdAppt10.Visible = False
cmdAppt11.Text = ""
cmdAppt11.Visible = False
cmdAppt12.Text = ""
cmdAppt12.Visible = False
cmdAppt13.Text = ""
cmdAppt13.Visible = False
cmdAppt14.Text = ""
cmdAppt14.Visible = False
cmdAppt15.Text = ""
cmdAppt15.Visible = False
cmdAppt16.Text = ""
cmdAppt16.Visible = False
cmdAppt17.Text = ""
cmdAppt17.Visible = False
cmdAppt18.Text = ""
cmdAppt18.Visible = False
cmdMore.Text = "More"
cmdMore.Visible = False
End Sub

Private Sub cmdAppt1_Click()
Call SetItem(cmdAppt1)
End Sub

Private Sub cmdAppt2_Click()
Call SetItem(cmdAppt2)
End Sub

Private Sub cmdAppt3_Click()
Call SetItem(cmdAppt3)
End Sub

Private Sub cmdAppt4_Click()
Call SetItem(cmdAppt4)
End Sub

Private Sub cmdAppt5_Click()
Call SetItem(cmdAppt5)
End Sub

Private Sub cmdAppt6_Click()
Call SetItem(cmdAppt6)
End Sub

Private Sub cmdAppt7_Click()
Call SetItem(cmdAppt7)
End Sub

Private Sub cmdAppt8_Click()
Call SetItem(cmdAppt8)
End Sub

Private Sub cmdAppt9_Click()
Call SetItem(cmdAppt9)
End Sub

Private Sub cmdAppt10_Click()
Call SetItem(cmdAppt10)
End Sub

Private Sub cmdAppt11_Click()
Call SetItem(cmdAppt11)
End Sub

Private Sub cmdAppt12_Click()
Call SetItem(cmdAppt12)
End Sub

Private Sub cmdAppt13_Click()
Call SetItem(cmdAppt13)
End Sub

Private Sub cmdAppt14_Click()
Call SetItem(cmdAppt14)
End Sub

Private Sub cmdAppt15_Click()
Call SetItem(cmdAppt15)
End Sub

Private Sub cmdAppt16_Click()
Call SetItem(cmdAppt16)
End Sub

Private Sub cmdAppt17_Click()
Call SetItem(cmdAppt17)
End Sub

Private Sub cmdAppt18_Click()
Call SetItem(cmdAppt18)
End Sub

Private Sub SetItem(AButton As fpBtn)
Dim i As Integer
Dim j As Integer
If (DisplayType <> "Z") Then
    ApptTypeId = Val(Str(AButton.Tag))
    ApptTypeName = AButton.Text
    ApptPeriod = AButton.ToolTipText
    Unload frmEvalAccess
Else
    If (AButton.BackColor = BaseBackColor) Then
        AButton.BackColor = SetBackColor
        AButton.ForeColor = SetForeColor
        CurrentAdmin = CurrentAdmin + 1
        AdminList(CurrentAdmin) = AButton.Text
    Else
        AButton.BackColor = BaseBackColor
        AButton.ForeColor = BaseForeColor
        For i = 1 To CurrentAdmin
            If (AButton.Text = AdminList(i)) Then
                For j = i To CurrentAdmin - 1
                    AdminList(j) = AdminList(j + 1)
                Next j
                AdminList(CurrentAdmin) = ""
            End If
        Next i
        CurrentAdmin = CurrentAdmin - 1
        If (CurrentAdmin < 1) Then
            CurrentAdmin = 1
        End If
    End If
End If
End Sub

Private Sub cmdDelete_Click()
DeleteOn = True
ApptTypeId = -1
ApptTypeName = ""
Unload frmEvalAccess
End Sub

Private Sub cmdDone_Click()
Unload frmEvalAccess
End Sub

Private Sub cmdMore_Click()
If (CurrentIndex + 1 > TotalAppts) And (DisplayType = "A") Then
    CurrentIndex = 1
End If
Call LoadAppt(DisplayType)
End Sub

Public Function LoadAppt(TheType As String) As Boolean
Dim k As Integer
Dim i As Integer
Dim ButtonCnt As Integer
Dim DisplayText As String
Dim DontCare As Boolean
Dim GoodDr As Boolean
Dim RetCode As PracticeCodes
Dim RetrieveReferral As PracticeVendors
Dim RetrieveResource As SchedulerResource
Dim RetApptType As SchedulerAppointmentType
LoadAppt = False
ApptPeriod = ""
DisplayType = TheType
Call ClearAppt
DeleteOn = False
cmdDelete.Visible = False
cmdDone.Visible = False
If (TheType = "A") Then
    lblField.Caption = "Select an Appointment Type"
    Set RetApptType = New SchedulerAppointmentType
    RetApptType.AppointmentType = Chr(1)
    TotalAppts = RetApptType.FindAppointmentType
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetApptType.SelectAppointmentType(i)) And (ButtonCnt < 19)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = RetApptType.AppointmentType
                cmdAppt1.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt1.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = RetApptType.AppointmentType
                cmdAppt2.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt2.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = RetApptType.AppointmentType
                cmdAppt3.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt3.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = RetApptType.AppointmentType
                cmdAppt4.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt4.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = RetApptType.AppointmentType
                cmdAppt5.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt5.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = RetApptType.AppointmentType
                cmdAppt6.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt6.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = RetApptType.AppointmentType
                cmdAppt7.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt7.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = RetApptType.AppointmentType
                cmdAppt8.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt8.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = RetApptType.AppointmentType
                cmdAppt9.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt9.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = RetApptType.AppointmentType
                cmdAppt10.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt10.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = RetApptType.AppointmentType
                cmdAppt11.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt11.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = RetApptType.AppointmentType
                cmdAppt12.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt12.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = RetApptType.AppointmentType
                cmdAppt13.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt13.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = RetApptType.AppointmentType
                cmdAppt14.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt14.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = RetApptType.AppointmentType
                cmdAppt15.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt15.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = RetApptType.AppointmentType
                cmdAppt16.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt16.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = RetApptType.AppointmentType
                cmdAppt17.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt17.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = RetApptType.AppointmentType
                cmdAppt18.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt18.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetApptType = Nothing
    End If
ElseIf (TheType = "D") Or (TheType = "H") Then
    lblField.Caption = "Select a Doctor"
    If (TheType = "H") Then
        lblField.Caption = "Select a Hospital"
    ElseIf (TheType = "R") Then
        lblField.Caption = "Select a Referring Doctor"
    End If
    Set RetrieveReferral = New PracticeVendors
    RetrieveReferral.VendorName = ""
    RetrieveReferral.VendorLastName = Chr(1)
    RetrieveReferral.VendorType = TheType
    If (TheType <> "H") Then
        TotalAppts = RetrieveReferral.FindVendorbyLastNameT
    Else
        TotalAppts = RetrieveReferral.FindVendorT
    End If
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveReferral.SelectVendor(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetrieveReferral.VendorLastName) + " " + Trim(RetrieveReferral.VendorTitle) + " " + Trim(RetrieveReferral.VendorFirstName)
            If (TheType = "H") Then
                DisplayText = Trim(RetrieveReferral.VendorName)
            End If
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetrieveReferral = Nothing
    End If
ElseIf (TheType = "C") Then
    lblField.Caption = "Select the Type of Surgery"
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "SURGERYTYPE"
    TotalAppts = RetCode.FindCode
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetCode = Nothing
    End If
ElseIf (TheType = "S") Then
    lblField.Caption = "Select a Doctor"
    DontCare = False
    Set RetApptType = New SchedulerAppointmentType
    If (ApptTypeId > 0) Then
        RetApptType.AppointmentTypeId = ApptTypeId
        If (RetApptType.RetrieveSchedulerAppointmentType) Then
            If (RetApptType.AppointmentTypeResourceId1 = 0) And _
               (RetApptType.AppointmentTypeResourceId2 = 0) And _
               (RetApptType.AppointmentTypeResourceId3 = 0) And _
               (RetApptType.AppointmentTypeResourceId4 = 0) And _
               (RetApptType.AppointmentTypeResourceId5 = 0) And _
               (RetApptType.AppointmentTypeResourceId6 = 0) And _
               (RetApptType.AppointmentTypeResourceId7 = 0) Then
                DontCare = True
            End If
        End If
    End If
    Set RetrieveResource = New SchedulerResource
    RetrieveResource.ResourceName = Chr(1)
    RetrieveResource.ResourceType = TheType
    TotalAppts = RetrieveResource.FindResource
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveResource.SelectResource(i)) And (ButtonCnt < 19)
            GoodDr = False
            If (DontCare) Then
                GoodDr = True
            Else
                If (RetApptType.AppointmentTypeResourceId1 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId2 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId3 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId4 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId5 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId6 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId7 = RetrieveResource.ResourceId) Or _
                   (ApptTypeId < 1) Then
                    GoodDr = True
                End If
            End If
            If (GoodDr) Then
                If (ButtonCnt = 1) Then
                    cmdAppt1.Text = RetrieveResource.ResourceDescription
                    cmdAppt1.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt1.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt1.Visible = True
                ElseIf (ButtonCnt = 2) Then
                    cmdAppt2.Text = RetrieveResource.ResourceDescription
                    cmdAppt2.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt2.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt2.Visible = True
                ElseIf (ButtonCnt = 3) Then
                    cmdAppt3.Text = RetrieveResource.ResourceDescription
                    cmdAppt3.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt3.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt3.Visible = True
                ElseIf (ButtonCnt = 4) Then
                    cmdAppt4.Text = RetrieveResource.ResourceDescription
                    cmdAppt4.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt4.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt4.Visible = True
                ElseIf (ButtonCnt = 5) Then
                    cmdAppt5.Text = RetrieveResource.ResourceDescription
                    cmdAppt5.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt5.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt5.Visible = True
                ElseIf (ButtonCnt = 6) Then
                    cmdAppt6.Text = RetrieveResource.ResourceDescription
                    cmdAppt6.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt6.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt6.Visible = True
                ElseIf (ButtonCnt = 7) Then
                    cmdAppt7.Text = RetrieveResource.ResourceDescription
                    cmdAppt7.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt7.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt7.Visible = True
                ElseIf (ButtonCnt = 8) Then
                    cmdAppt8.Text = RetrieveResource.ResourceDescription
                    cmdAppt8.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt8.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt8.Visible = True
                ElseIf (ButtonCnt = 9) Then
                    cmdAppt9.Text = RetrieveResource.ResourceDescription
                    cmdAppt9.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt9.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt9.Visible = True
                ElseIf (ButtonCnt = 10) Then
                    cmdAppt10.Text = RetrieveResource.ResourceDescription
                    cmdAppt10.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt10.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt10.Visible = True
                ElseIf (ButtonCnt = 11) Then
                    cmdAppt11.Text = RetrieveResource.ResourceDescription
                    cmdAppt11.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt11.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt11.Visible = True
                ElseIf (ButtonCnt = 12) Then
                    cmdAppt12.Text = RetrieveResource.ResourceDescription
                    cmdAppt12.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt12.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt12.Visible = True
                ElseIf (ButtonCnt = 13) Then
                    cmdAppt13.Text = RetrieveResource.ResourceDescription
                    cmdAppt13.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt13.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt13.Visible = True
                ElseIf (ButtonCnt = 14) Then
                    cmdAppt14.Text = RetrieveResource.ResourceDescription
                    cmdAppt14.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt14.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt14.Visible = True
                ElseIf (ButtonCnt = 15) Then
                    cmdAppt15.Text = RetrieveResource.ResourceDescription
                    cmdAppt15.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt15.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt15.Visible = True
                ElseIf (ButtonCnt = 16) Then
                    cmdAppt16.Text = RetrieveResource.ResourceDescription
                    cmdAppt16.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt16.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt16.Visible = True
                ElseIf (ButtonCnt = 17) Then
                    cmdAppt17.Text = RetrieveResource.ResourceDescription
                    cmdAppt17.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt17.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt17.Visible = True
                ElseIf (ButtonCnt = 18) Then
                    cmdAppt18.Text = RetrieveResource.ResourceDescription
                    cmdAppt18.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt18.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt18.Visible = True
                End If
                LoadAppt = True
            End If
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetApptType = Nothing
        Set RetrieveResource = Nothing
    End If
ElseIf (TheType = "Z") Or (TheType = "h") Or (TheType = "&") Then
    Erase AdminList
    CurrentAdmin = 0
    cmdDone.Visible = True
    lblField.Caption = "Administrative CheckList"
    If (TheType = "h") Then
        lblField.Caption = "Facility Admission Forms"
    ElseIf (TheType = "&") Then
        lblField.Caption = "Surgical Requests"
    End If
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "PatientAdmin"
    If (TheType = "h") Then
        RetCode.ReferenceType = "FacilityAdmission"
    ElseIf (TheType = "&") Then
        RetCode.ReferenceType = "SurgicalRequests"
    End If
    TotalAppts = RetCode.FindCode
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetCode = Nothing
    End If
ElseIf (TheType = "!") Or (TheType = "'") Then
    cmdDone.Visible = False
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "CLINICALTEMPLATES"
    RetCode.ReferenceCode = ""
    If (TheType = "!") Then
        RetCode.ReferenceAlternateCode = "S"
        lblField.Caption = "Post Status Templates"
    Else
        RetCode.ReferenceAlternateCode = "F"
        lblField.Caption = "Favorite Findings Templates"
    End If
    TotalAppts = RetCode.FindAlternateCode
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetCode = Nothing
    End If
ElseIf (TheType = "~") And (lstBox.ListCount > 0) Then
    cmdDelete.Visible = True
    If (lstBox.Name = "lstOS") Then
        lblField.Caption = "Select an OS Finding"
    Else
        lblField.Caption = "Select an OD Finding"
    End If
    TotalAppts = lstBox.ListCount
    If (TotalAppts > 0) Then
        i = CurrentIndex - 1
        ButtonCnt = 1
        While (ButtonCnt < 19) And (Trim(lstBox.List(i)) <> "")
'            k = InStrPS(lstBox.List(i), ":")
            DisplayText = Trim(Left(lstBox.List(i), 40))
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = Trim(Str(i))
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = Trim(Str(i))
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = Trim(Str(i))
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = Trim(Str(i))
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = Trim(Str(i))
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = Trim(Str(i))
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = Trim(Str(i))
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = Trim(Str(i))
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = Trim(Str(i))
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = Trim(Str(i))
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = Trim(Str(i))
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = Trim(Str(i))
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = Trim(Str(i))
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = Trim(Str(i))
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = Trim(Str(i))
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = Trim(Str(i))
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = Trim(Str(i))
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = Trim(Str(i))
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
    End If
End If
If (CurrentIndex <= TotalAppts) Then
    cmdMore.Text = "More"
    cmdMore.Visible = True
Else
    cmdMore.Text = "Beginning of List"
    cmdMore.Visible = True
End If
If (TotalAppts < 19) Then
    cmdMore.Visible = False
End If
End Function

Private Sub cmdQuit_Click()
ApptTypeId = -1
ApptTypeName = ""
Unload frmApptType
End Sub

Private Sub Form_Load()
CurrentIndex = 1
BaseBackColor = cmdAppt1.BackColor
BaseForeColor = cmdAppt1.ForeColor
SetBackColor = 14745312
SetForeColor = 0
End Sub

Public Function GetAdminItem(Ref As Integer, Item As String) As Boolean
GetAdminItem = False
Item = ""
If (Ref > 0) And (Ref < 40) Then
    Item = Trim(AdminList(Ref))
    If (Item <> "") Then
        GetAdminItem = True
    End If
End If
End Function

