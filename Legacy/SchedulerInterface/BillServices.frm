VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmBillServices 
   BackColor       =   &H0077742D&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   8265
   ClientLeft      =   45
   ClientTop       =   150
   ClientWidth     =   7710
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8265
   ScaleWidth      =   7710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      BackColor       =   &H0077742D&
      BorderStyle     =   0  'None
      Height          =   270
      Left            =   6090
      ScaleHeight     =   270
      ScaleWidth      =   915
      TabIndex        =   15
      Top             =   2460
      Width           =   915
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   1560
      TabIndex        =   14
      Top             =   240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid lstSrvs2 
      Height          =   1455
      Left            =   0
      TabIndex        =   4
      Top             =   4200
      Visible         =   0   'False
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   2566
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedCols       =   0
      RowHeightMin    =   2
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      AllowUserResizing=   3
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ComboBox lstIns 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Index           =   1
      ItemData        =   "BillServices.frx":0000
      Left            =   0
      List            =   "BillServices.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   5640
      Visible         =   0   'False
      Width           =   6855
   End
   Begin MSFlexGridLib.MSFlexGrid lstSort 
      Height          =   255
      Left            =   1440
      TabIndex        =   12
      Top             =   5280
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      FixedRows       =   0
      FixedCols       =   0
      RowHeightMin    =   2
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   0
      AllowUserResizing=   3
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox pTxt 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   2520
      ScaleHeight     =   855
      ScaleWidth      =   1695
      TabIndex        =   7
      Top             =   1320
      Width           =   1695
      Begin VB.TextBox txt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         MaxLength       =   1
         TabIndex        =   8
         Text            =   "T"
         Top             =   15
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid lstSrvs 
      Height          =   255
      Left            =   4320
      TabIndex        =   10
      Top             =   720
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   5
      FixedRows       =   0
      FixedCols       =   0
      RowHeightMin    =   2
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   0
      AllowUserResizing=   3
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox pic 
      BackColor       =   &H0077742D&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   5415
      TabIndex        =   11
      Top             =   2385
      Width           =   5415
   End
   Begin VB.ComboBox lstIns 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Index           =   0
      ItemData        =   "BillServices.frx":0004
      Left            =   0
      List            =   "BillServices.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   4440
      Visible         =   0   'False
      Width           =   6855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   6120
      TabIndex        =   0
      Top             =   6960
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "BillServices.frx":0008
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   1
      Top             =   6960
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "BillServices.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReset 
      Height          =   990
      Left            =   3000
      TabIndex        =   5
      Top             =   6960
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "BillServices.frx":03C6
   End
   Begin MSFlexGridLib.MSFlexGrid lstSrvs1 
      Height          =   3855
      Left            =   120
      TabIndex        =   2
      Top             =   2760
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   6800
      _Version        =   393216
      Rows            =   1
      FixedCols       =   0
      RowHeightMin    =   2
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   0
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid lstIns1 
      Height          =   1800
      Left            =   120
      TabIndex        =   6
      Top             =   600
      Width           =   5865
      _ExtentX        =   10345
      _ExtentY        =   3175
      _Version        =   393216
      Rows            =   1
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   2
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblPat 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "frmBillServices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private ReceivableId As String
Private PatientId As Long
Private InvoiceId As String
Private Invoicedate As String
Private InsurerId As Long
Private PatPol1 As Long
Private PatPol2 As Long

Public HiddenMode As Boolean

Dim currentFGC As MSFlexGrid
Dim bySys As Boolean

Private Sub cmdDone_Click()
Dim r As Integer, c As Integer
Dim ErrMsg As String
Dim IsServiceChargeZero As Boolean

With lstSrvs1
    For r = 1 To .Rows - 1
        For c = 5 To 6
            If .TextMatrix(r, c) = "" Then
                frmEventMsgs.Header = "Please, fill up all boxes."
                GoTo ErrMsg
            End If
        Next
    Next
End With
'-------------------------------------------------------------------------------------
'check printable rules
Dim SortLine As String
With lstSort
    .Rows = 0
    .AddItem "" & vbTab & ""
    For r = 1 To lstSrvs1.Rows - 1
        If UCase(lstSrvs1.TextMatrix(r, 6)) = "W" Then
        'fail
'            If UCase(lstSrvs1.TextMatrix(r, 7)) = "P" Then
'                lstSrvs1.TextMatrix(r, 7) = ""
'                GoTo ErrMsg
'            End If
        'cleanup
            lstSrvs1.TextMatrix(r, 7) = ""
        Else
            .AddItem (lstSrvs1.TextMatrix(r, 5) & vbTab & lstSrvs1.TextMatrix(r, 7))
        End If
    Next
    
    .col = 0
    .Sort = flexSortGenericAscending
    For r = 1 To .Rows - 1
        If .TextMatrix(r, 0) = .TextMatrix(r - 1, 0) Then
            If .TextMatrix(r, 1) = .TextMatrix(r - 1, 1) Then
                GoTo nextR
            Else
                frmEventMsgs.Header = "For services billed to the same payer, you can print all or print none."
                GoTo ErrMsg
            End If
        End If
nextR:
    Next
End With

'To Check whether Billed Services has Charge zero
For r = 1 To lstSrvs1.Rows - 1
    If (lstSrvs1.TextMatrix(r, 6) = "B") And (val(Trim(lstSrvs1.TextMatrix(r, 3))) = 0) Then
        IsServiceChargeZero = True
        For c = 1 To lstSrvs1.Rows - 1
            If Trim(lstSrvs1.TextMatrix(r, 5)) = Trim(lstSrvs1.TextMatrix(c, 5)) _
                And Trim(lstSrvs1.TextMatrix(c, 5)) <> "Nxt" And lstSrvs1.TextMatrix(c, 6) = "B" _
                And (val(Trim(lstSrvs1.TextMatrix(c, 3))) > 0) Then
                IsServiceChargeZero = False
                Exit For
            End If
        Next
        If IsServiceChargeZero Then
            Exit For
        End If
    End If
Next

If IsServiceChargeZero Then
    frmEventMsgs.Header = "Service(s) has $0 charge"
    GoTo ErrMsg
End If
'-------------------------------------------------------------------------------------

GenerateSplitRow
'Call BatchOut_5(frmPaymentsTotal.lstSrvs2.TextMatrix(frmPaymentsTotal.lstSrvs1.Row, 3))

'-------------------------------------------------------------------------------------
'With lstSort
''print printable receivable
'    Dim ApplTemp As New ApplicationTemplates
'    Dim ApplList As New ApplicationAIList
'    Set ApplTemp.lstBox = frmPaymentsTotal.lstPrint
'    Dim AppId As String
'    Dim BilledParty As String
'    Dim RcvId As Long
'    Dim SQL As String
'    Dim RS As Recordset
'    For r = 1 To .Rows - 1
'        If Not .TextMatrix(r, 0) = .TextMatrix(r - 1, 0) Then
'            If .TextMatrix(r, 1) = "P" Then
'                AppId = frmPaymentsTotal.lstSrvs2.TextMatrix(frmPaymentsTotal.lstSrvs1.Row, 3)
'                If IsNumeric(.TextMatrix(r, 0)) Then
'                    BilledParty = Mid(lstIns(0).List(.TextMatrix(r, 0) - 1), 77)
'                    RcvId = getRcvId(BilledParty, AppId)
'                    If (ApplTemp.PrintTransaction(RcvId, PatientId, "I", True, False, "", False, 0, "S", "", 0)) Then
'                        SQL = _
'                            "update ptj set TransactionStatus ='S', " & _
'                            "TransactionAction = 'P', " & _
'                            "TransactionBatch = 'On-Demand' " & _
'                            "from PracticeTransactionJournal ptj " & _
'                            "Where TransactionType = 'R' " & _
'                            "and TransactionRef = 'I' " & _
'                            "and TransactionStatus <> 'Z' " & _
'                            "and TransactionTypeId = " & RcvId
'                        Set RS = GetRS(SQL)
'                    End If
'                Else
''                    SQL = _
''                        "select * from PatientReceivables pr " & _
''                        "inner join PracticeTransactionJournal ptj on ptj.TransactionTypeId = pr.ReceivableId " & _
''                        "where TransactionType = 'R' " & _
''                        "and TransactionStatus <> 'Z' " & _
''                        "and TransactionRef = 'P' " & _
''                        "and AppointmentId = " & AppId
''                    Set RS = GetRS(SQL)
''                    RcvId = RS("ReceivableId")
'                End If
'            End If
'        End If
'    Next
'End With
'-------------------------------------------------------------------------------------
'Set ApplList = Nothing
'Set ApplTemp = Nothing

'frmPaymentsTotal.BillServices = True
Unload frmBillServices

Exit Sub

'error ===============================================================================
ErrMsg:
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
End Sub

Private Sub cmdHome_Click()
Unload frmBillServices
End Sub


Private Sub cmdReset_Click()
Dim i As Integer
For i = 1 To lstSrvs1.Rows - 1
    lstSrvs1.TextMatrix(i, 5) = ""
    lstSrvs1.TextMatrix(i, 6) = ""
Next i
End Sub


Private Sub GenerateSplitRow()
Dim r As Integer
Dim c As Integer
Dim f As Integer
Dim fgc(2) As MSFlexGrid
Set fgc(1) = lstSrvs1
Set fgc(2) = lstSrvs2

Dim v() As String
Dim s As String

For r = 1 To fgc(1).Rows - 1
    If Not fgc(1).TextMatrix(r, 8) = "" Then
        For f = 1 To 2
            ReDim v(fgc(f).Cols - 1) As String
            
            'copy row
            For c = 0 To fgc(f).Cols - 1
                v(c) = fgc(f).TextMatrix(r, c)
            Next
            
            'correct cols
            Select Case f
            Case 1
                v(4) = v(8)
                v(5) = "P"
                v(7) = ""
                v(8) = ""
            Case 2
                v(0) = -v(0)
            End Select
        
            'paste row
            s = ""
            For c = 0 To fgc(f).Cols - 1
                s = s & v(c)
                If c < UBound(v) Then s = s & vbTab
            Next
            fgc(f).AddItem s
        Next
        
    End If
Next
End Sub

Private Sub Command1_Click()
GenerateSplitRow
End Sub

Private Sub Form_Activate()
editCell lstSrvs
End Sub

Private Sub Form_Load()
Dim cntl As Control
For Each cntl In Me
    On Error Resume Next
    cntl.TabStop = False
Next
pTxt.Height = lstSrvs.RowHeight(0) - 45
Set currentFGC = lstSrvs

End Sub

Private Sub lstIns1_Click()
Select Case lstIns1.col
Case 0 To 4
    editCell currentFGC
    txt.Text = lstIns1.TextMatrix(lstIns1.Row, 3)
Case 5
    If lstIns1.CellBackColor = vbWhite Then
        If lstIns1.Text = "N" Then
            lstIns1.Text = "Y"
        Else
            lstIns1.Text = "N"
        End If
    End If
End Select
End Sub

Private Sub lstSrvs_Click()

With lstSrvs
    Select Case .col
    Case 2, 4
        Dim v As String
        v = .Text
        Dim i As Integer
        If v = "" Then
            If .col = 2 Then
                v = "P"
            Else
                v = "B"
            End If
        Else
            v = ""
        End If
        .Text = v
        With lstSrvs1
            For i = 1 To .Rows - 1
                .TextMatrix(i, lstSrvs.col + 5) = v
                If lstSrvs.col = 2 Then
                    .TextMatrix(i, 4) = Format(lstSrvs2.TextMatrix(i, 4), "#####0.00")
                    .TextMatrix(i, 8) = ""
                End If
            Next
        End With
        
        .col = 0
        txt.SetFocus
    End Select
End With
editCell lstSrvs
End Sub

Private Sub lstSrvs_EnterCell()
With lstSrvs
    Select Case .col
    Case 2, 4
    Case Else
        editCell lstSrvs
    End Select
End With
End Sub

Private Sub lstSrvs_GotFocus()
lstSrvs_EnterCell
End Sub

Private Sub lstSrvs_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
lstSrvs_EnterCell
End Sub

Private Sub lstSrvs1_Click()
With lstSrvs1
    Select Case .col
    Case 7
        If .Text = "" Then
            .Text = "P"
        Else
            .Text = ""
        End If
        lstSrvs.TextMatrix(0, 2) = ""
        .TextMatrix(.Row, 4) = Format(lstSrvs2.TextMatrix(.Row, 4), "#####0.00")
        .TextMatrix(.Row, 8) = ""

        .col = 5
        txt.SetFocus
    Case 9
        If .Text = "" Then
            .Text = "B"
        Else
            .Text = ""
        End If
        lstSrvs.TextMatrix(0, 4) = ""
        
        .col = 5
        txt.SetFocus
    End Select
End With
editCell lstSrvs1

End Sub

Private Sub lstSrvs1_EnterCell()
With lstSrvs1
    If .col = 7 Then
    Else
        editCell lstSrvs1
    End If
End With
End Sub

Private Sub lstSrvs1_GotFocus()
lstSrvs1_EnterCell
End Sub

Private Sub lstSrvs1_KeyDown(KeyCode As Integer, Shift As Integer)
lstSrvs1_EnterCell
End Sub


Private Function ValidateAction(ATemp As String) As Boolean
ValidateAction = True
If (ATemp <> "") Then
    If (InStrPS("BPWA", UCase(ATemp)) = 0) Then
        ValidateAction = False
    End If
End If
End Function

Public Function ZeroAmtOK(fINS As String, fSRV As String) As Boolean
If Not IsNumeric(fINS) Then Exit Function


Dim stat(1) As Boolean
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Set RetPatientFinancialService = New PatientFinancialService
RetPatientFinancialService.InsurerId = fINS
Set RS = RetPatientFinancialService.RetrieveInsurer
If (Not RS Is Nothing And RS.RecordCount > 0) Then
    stat(0) = RS("InsurerServiceFilter")
End If
Set RetPatientFinancialService = Nothing

Dim RetSrv As New Service
RetSrv.Service = fSRV
If (RetSrv.RetrieveService) Then
    stat(1) = RetSrv.ServiceFilter
End If
Set RetSrv = Nothing

ZeroAmtOK = (stat(0) And stat(1))
End Function


Public Function getRcvId(InsId As String, AppId As String) As Long
If IsNumeric(InsId) Then
    
    Dim RetRcv As New PatientReceivables
    RetRcv.AppointmentId = val(AppId)
    RetRcv.InsurerId = InsId
    If (RetRcv.FindPatientReceivable > 0) Then
        Dim i As Integer
        i = 1
        While (RetRcv.SelectPatientReceivable(i))
            getRcvId = RetRcv.ReceivableId
            i = i + 1
        Wend
    End If
    Set RetRcv = Nothing
Else
    getRcvId = ReceivableId
End If
End Function

Private Function getAmt(srvs As Long) As String
Dim i As Integer
For i = 1 To lstSrvs1.Rows - 1
    If lstSrvs2.TextMatrix(i, 0) = srvs Then
        getAmt = lstSrvs1.TextMatrix(i, 4)
        Exit Function
    End If
Next
End Function

Private Function getSrvsRow(srvs As Long) As Integer
Dim i As Integer
For i = 1 To lstSrvs1.Rows - 1
    If lstSrvs2.TextMatrix(i, 0) = srvs Then
        getSrvsRow = i
        Exit Function
    End If
Next
End Function


'''''''''
'''''''''Private Sub BatchOut(PatId As Long)
'''''''''Dim pcnt As Integer
'''''''''Dim d As Integer, ARef As Integer
'''''''''Dim w As Integer, w1 As Integer
'''''''''Dim zz As Integer, p As Integer
'''''''''Dim z As Integer, ICol As Integer
'''''''''Dim i As Integer, j As Integer
'''''''''Dim SP As Integer, scnt As Integer
'''''''''
'''''''''Dim ComOn As Boolean, MedOn As Boolean
'''''''''Dim AppealOn As Boolean
'''''''''Dim XOver1 As Boolean
'''''''''Dim XOver2 As Boolean
'''''''''
'''''''''Dim k As Single
'''''''''Dim JAmt As String
'''''''''' represents amount for all 4 status APBU
'''''''''Dim myCreditAmt As Single
'''''''''Dim PAmt(12) As Single
'''''''''Dim PSrv(12) As Long
'''''''''Dim IAmt(12, 12) As Single
'''''''''Dim ISrv(12, 12, 12) As Long
'''''''''Dim srvcnt(12, 12) As Integer
'''''''''
'''''''''Dim LocalBal As Single
'''''''''Dim PBal As Single, IBal As Single
'''''''''
'''''''''Dim RcvId1 As Long
'''''''''Dim RcvIdUna As Long
'''''''''Dim PatInsId As String
'''''''''Dim PolHldId As Long
'''''''''Dim RcvIdBillParty As Long
'''''''''Dim TempInsId1 As Long
'''''''''Dim a1 As String, a2 As String
'''''''''Dim st As String
'''''''''Dim Srv As String, Amt As String
'''''''''Dim ItemId As Long, p1 As Long
'''''''''Dim myRef As String, Temp As String
'''''''''Dim RcvId As Long, InsId As String
'''''''''Dim CurInvId As String, Rsn As String
'''''''''Dim SendTo As String, InvId As String
'''''''''Dim InsrId As String, TrnId As Long
'''''''''Dim InsId1 As Long, AplNum As String
'''''''''Dim RecId As String, TempIns As Long
'''''''''
'''''''''Dim RetSrv As PatientReceivableService
'''''''''Dim ApplTemp As ApplicationTemplates
'''''''''Dim ApplList As ApplicationAIList
'''''''''
'''''''''myCreditAmt = 0
'''''''''If (Trim(PatId) <> "") Then
'''''''''    'If (dbArcOn) Or (dbArcBulkOn) Then
'''''''''    '    If (dbDivisionCurrent = "ARC") Then
'''''''''    '        Call frmHome.SetDivision(PatId, "")
'''''''''    '    End If
'''''''''    'End If
'''''''''    InsId = ""
'''''''''    InsrId = ""
'''''''''    p = 0
'''''''''    pcnt = 0
'''''''''    scnt = 0
'''''''''    Erase srvcnt
'''''''''    Erase ISrv
'''''''''    Erase IAmt
'''''''''    Erase PAmt
'''''''''    Erase PSrv
'''''''''    Set ApplList = New ApplicationAIList
'''''''''    Set ApplTemp = New ApplicationTemplates
'''''''''
''''''''''===================================================================================
'''''''''' process each line
'''''''''    For i = 1 To lstSrvs1.Rows - 1
'''''''''' this is the receivable used to apply any sitation where an Unbilled Balance remains
'''''''''        RcvIdUna = ReceivableId
'''''''''        Call ApplTemp.ApplGetPayerPartyRef(RcvIdUna, "M", ARef)
'''''''''        'If (lstSrvs1.TextMatrix(i, 6) = "A") Then < org
'''''''''        Select Case lstSrvs1.TextMatrix(i, 6)
'''''''''        Case "A"
'''''''''            GoSub SetAppeal
'''''''''            If (Trim(lstSrvs1.TextMatrix(i, 5)) = "P") Then
'''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) <> 0) Then
'''''''''                    PAmt(1) = PAmt(1) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                    pcnt = pcnt + 1
'''''''''                    PSrv(pcnt) = lstSrvs2.TextMatrix(i, 0)
'''''''''                End If
'''''''''            Else
'''''''''                SP = 0
'''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) > 0) Then
'''''''''                    IAmt(Val(lstSrvs1.TextMatrix(i, 5)), 1) = IAmt(Val(lstSrvs1.TextMatrix(i, 5)), 1) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                    SP = lstSrvs1.TextMatrix(i, 5)
'''''''''                    If (SP > 0) Then
'''''''''                        srvcnt(SP, 1) = srvcnt(SP, 1) + 1
'''''''''                        ISrv(SP, srvcnt(SP, 1), 1) = lstSrvs2.TextMatrix(i, 0)
'''''''''                    End If
'''''''''                Else
'''''''''                    myCreditAmt = myCreditAmt + Abs(Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                End If
'''''''''            End If
'''''''''
'''''''''        'ElseIf (lstSrvs1.TextMatrix(i, 6) = "P") Or (lstSrvs1.TextMatrix(i, 6) = "W") Then < org
'''''''''        Case "P", "W"
'''''''''            If (Trim(lstSrvs1.TextMatrix(i, 5)) = "P") Then
'''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) <> 0) Then
'''''''''                    PAmt(2) = PAmt(2) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                    pcnt = pcnt + 1
'''''''''                    PSrv(pcnt) = lstSrvs2.TextMatrix(i, 0)
'''''''''                End If
'''''''''            Else
'''''''''                SP = 0
'''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) > 0) Then
'''''''''                    IAmt(Val(lstSrvs1.TextMatrix(i, 5)), 2) = IAmt(Val(lstSrvs1.TextMatrix(i, 5)), 2) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                    SP = lstSrvs1.TextMatrix(i, 5)
'''''''''                    If (SP > 0) Then
'''''''''                        srvcnt(SP, 2) = srvcnt(SP, 2) + 1
'''''''''                        ISrv(SP, srvcnt(SP, 2), 2) = lstSrvs2.TextMatrix(i, 0)
'''''''''                    End If
'''''''''                Else
'''''''''                    myCreditAmt = myCreditAmt + Abs(Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                End If
'''''''''            End If
'''''''''        'ElseIf (lstSrvs1.TextMatrix(i, 6) = "B") Then < org
'''''''''        Case "B"
'''''''''            If (Trim(lstSrvs1.TextMatrix(i, 5)) = "P") Then
'''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) <> 0) Then
'''''''''                    PAmt(3) = PAmt(3) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                    pcnt = pcnt + 1
'''''''''                    PSrv(pcnt) = lstSrvs2.TextMatrix(i, 0)
'''''''''                End If
'''''''''            Else
'''''''''                SP = 0
'''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) > 0) Then
'''''''''                    IAmt(Val(lstSrvs1.TextMatrix(i, 5)), 3) = IAmt(Val(lstSrvs1.TextMatrix(i, 5)), 3) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                    SP = lstSrvs1.TextMatrix(i, 5)
'''''''''                    If (SP > 0) Then
'''''''''                        srvcnt(SP, 3) = srvcnt(SP, 3) + 1
'''''''''                        ISrv(SP, srvcnt(SP, 3), 3) = lstSrvs2.TextMatrix(i, 0)
'''''''''                    End If
'''''''''                Else
'''''''''                    myCreditAmt = myCreditAmt + Abs(Val(Trim(lstSrvs1.TextMatrix(i, 4))))
'''''''''                End If
'''''''''            End If
'''''''''        'Else    ' default is P (or W) but stay with the current insuring party for the service
'''''''''        Case Else
''''''''''            If (Trim(lstSrvs2.TextMatrix(i, 2)) <> "") Then
''''''''''                If (Trim(lstSrvs2.TextMatrix(i, 2)) = "P") Then
''''''''''                    w = ARef
''''''''''                Else
''''''''''                    w = Val(Trim(lstSrvs2.TextMatrix(i, 2)))
''''''''''                    If (w < 1) Then
''''''''''                        w = ARef
''''''''''                    End If
''''''''''                    If (w > 0) Then
''''''''''                        SrvCnt(w, 2) = SrvCnt(w, 2) + 1
''''''''''                        ISrv(w, SrvCnt(w, 2), 2) = lstSrvs2.TextMatrix(i, 0)
''''''''''                    End If
''''''''''                End If
''''''''''                If (Val(Trim(lstSrvs1.TextMatrix(i, 4))) > 0) Then
''''''''''                    IAmt(w, 2) = IAmt(w, 2) + (Val(Trim(lstSrvs1.TextMatrix(i, 4))))
''''''''''                Else
''''''''''                    myCreditAmt = myCreditAmt + Abs(Val(Trim(lstSrvs1.TextMatrix(i, 4))))
''''''''''                End If
''''''''''            End If
'''''''''        End Select
'''''''''    Next i
''''''''''===================================================================================
'''''''''    GoSub BatchIt
'''''''''    Set ApplList = Nothing
'''''''''    Set ApplTemp = Nothing
'''''''''End If
'''''''''Exit Sub
'''''''''BatchIt:
''''''''''
'''''''''' On a payer basis all services assigned to each party is posted
'''''''''' to the transaction journal link table. All services associated
'''''''''' with a claim are destined to be assigned to some paying party
'''''''''' regardless of whether the user assigned it or not. Those not
'''''''''' assigned will be places in a 'Wait' state. Each time this is
'''''''''' executed we start from a clean transactional state, so active
'''''''''' transactions are flushed (Z-ed) and the claim's transactional
'''''''''' assignments are balanced and re-established.
''''''''''
'''''''''    RcvId = lstSrvs2.TextMatrix(1, 1)
'''''''''    RcvId1 = lstSrvs2.TextMatrix(1, 1)
'''''''''' do patient statement
'''''''''' get rid of all Insurer Transactions for this invoice
'''''''''    Call ApplList.ApplSetAllInvTransStatus(RcvId1, "I", "Z", "", "", False)
'''''''''' get rid of all Patient Transactions for this invoice
'''''''''    Call ApplList.ApplSetAllInvTransStatus(RcvId1, "P", "Z", "", "", False)
'''''''''    JAmt = 0
'''''''''    For j = 1 To 4
'''''''''        If (PAmt(j) > 0) Then
'''''''''            JAmt = JAmt + PAmt(j)
'''''''''        End If
'''''''''    Next j
'''''''''    JAmt = Round(JAmt, 2)
'''''''''    If (JAmt > myCreditAmt) And (myCreditAmt > 0) Then
'''''''''        JAmt = JAmt - myCreditAmt
'''''''''        myCreditAmt = 0
'''''''''    End If
'''''''''    If (JAmt > 0) Then
'''''''''        RcvId1 = RcvId
'''''''''        If (Trim(RecId) <> "") Then
'''''''''            RcvId1 = RecId
'''''''''        End If
'''''''''        TrnId = ApplTemp.BatchHeaderTransaction(RcvId1, "P", Trim(Str(JAmt)), "P", False, "P", False)
'''''''''        For d = 1 To pcnt
'''''''''            Call ApplTemp.BatchSetPaymentTransaction(TrnId, PSrv(d), CCur(lstSrvs1.TextMatrix(d, 4)))
'''''''''        Next d
'''''''''    End If
'''''''''
'''''''''' do Insurer claims
'''''''''    XOver1 = False
'''''''''    If (ApplTemp.ApplIsInsurerPrimary(RcvId1)) Then   ' but only for the secondary
'''''''''        XOver1 = ApplTemp.ApplIsInsurerXOver(InsurerId)
'''''''''    End If
'''''''''
'''''''''    For p = 1 To 6
'''''''''' get the receivable of the bill party
'''''''''        RcvIdBillParty = 0
'''''''''        If (srvcnt(p, 1) > 0) Or (srvcnt(p, 2) > 0) Or (srvcnt(p, 3) > 0) Then
'''''''''            zz = p
'''''''''            TempIns = 0
'''''''''            a1 = ""
'''''''''            p1 = 0
'''''''''            TempInsId1 = InsurerId
'''''''''            Call ApplTemp.ApplGetNextPayerParty(PatientId, InvoiceId, Invoicedate, "M", False, _
'''''''''                                                TempInsId1, TempIns, a1, p1, zz)
'''''''''            Call ApplList.ApplGetRcvbyInsurer(TempIns, RcvId1, RcvIdBillParty)
'''''''''' if it does not exist, build it
'''''''''            If (RcvIdBillParty = 0) Then
'''''''''                RcvIdBillParty = ApplTemp.ApplBillSecondary(RcvId, InsId1, PolHldId, False, True)
'''''''''                If (RcvIdBillParty = -1) Or (RcvIdBillParty = -2) Or (RcvIdBillParty = 0) Then
'''''''''                    RcvIdBillParty = ApplTemp.ApplBillSecondary(RcvId, InsId1, PolHldId, True, True)
'''''''''                End If
'''''''''            End If
'''''''''            If (RcvIdBillParty <> 0) Then
'''''''''                XOver2 = False
'''''''''                If (zz = 2) Then   ' but only for the secondary
'''''''''                    XOver2 = ApplTemp.ApplIsInsurerXOver(TempIns)
'''''''''                End If
'''''''''                LocalBal = IAmt(p, 3) + IAmt(p, 2) + IAmt(p, 1)
'''''''''                If (LocalBal > myCreditAmt) And (myCreditAmt > 0) Then
'''''''''                    LocalBal = LocalBal - myCreditAmt
'''''''''                    myCreditAmt = 0
'''''''''                End If
'''''''''                If (LocalBal > 0) Then
'''''''''                    If (ApplTemp.ApplIsInsurerPlanPaper(TempIns)) Then
'''''''''                        If (XOver1) And (XOver2) Then
'''''''''                            p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "B", False, "S", False)
'''''''''                        Else
'''''''''                            If (p = 1) And (ApplTemp.ApplIsElectronic(RcvIdBillParty)) Then
'''''''''                                If (srvcnt(p, 3) > 0) Then
'''''''''                                    p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "B", False, "P", False)
'''''''''                                Else
'''''''''                                    p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "B", False, "S", False)
'''''''''                                End If
'''''''''                            Else
'''''''''                                If (srvcnt(p, 3) > 0) Then
'''''''''                                    p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "P", False, "P", False)
'''''''''                                Else
'''''''''                                    p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "P", False, "S", False)
'''''''''                                End If
'''''''''                            End If
'''''''''                        End If
'''''''''                    Else
'''''''''                        If (srvcnt(p, 3) > 0) Then
'''''''''                            p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "B", False, "P", False)
'''''''''                        Else
'''''''''                            p1 = ApplTemp.BatchHeaderTransaction(RcvIdBillParty, "I", Trim(Str(LocalBal)), "B", False, "S", False)
'''''''''                        End If
'''''''''                    End If
'''''''''                    If (p1 <> 0) Then
'''''''''' this is for payment services
'''''''''                        For d = 1 To srvcnt(p, 3)
'''''''''                            Call ApplTemp.BatchSetPaymentTransaction(p1, ISrv(p, d, 3), lstSrvs1.TextMatrix(d, 4))
'''''''''                        Next d
'''''''''' this is for pending services
'''''''''                        For d = 1 To srvcnt(p, 2)
'''''''''                            Call ApplTemp.BatchSetWaitTransaction(p1, ISrv(p, d, 2))
'''''''''                        Next d
'''''''''' this is for appeal services
'''''''''                        For d = 1 To srvcnt(p, 1)
'''''''''                            Call ApplTemp.BatchSetAppealTransaction(p1, ISrv(p, d, 1))
'''''''''                        Next d
'''''''''                    End If
'''''''''                End If
'''''''''            End If
'''''''''        End If
'''''''''    Next p
'''''''''
'''''''''' Final Review
'''''''''    Call ApplList.ApplSetReceivableBalance(InvoiceId, "", True)
'''''''''    Call ApplTemp.ApplReconcilePatientStatements(InvoiceId)
'''''''''    LocalBal = ApplList.ApplGetUnallocatedBalancebyInvoice(InvoiceId)
'''''''''    If (LocalBal > 0) And (Trim(RcvIdUna) <> "") Then
'''''''''        Call ApplList.ApplSetAllRcvTransStatus(RcvIdUna, "I", "Z", "", "", False)
'''''''''        LocalBal = ApplList.ApplGetUnallocatedBalancebyInvoice(InvoiceId)
'''''''''        p1 = ApplTemp.BatchHeaderTransaction(RcvIdUna, "I", Trim(Str(LocalBal)), "B", False, "P", False)
'''''''''        If (p1 <> 0) Then
'''''''''            Set RetSrv = New PatientReceivableService
'''''''''            RetSrv.Invoice = InvoiceId
'''''''''            If (RetSrv.FindPatientReceivableService > 0) Then
'''''''''                p = 1
'''''''''                While (RetSrv.SelectPatientReceivableService(p))
'''''''''                    Call ApplTemp.BatchSetPaymentTransaction(p1, RetSrv.ItemId, lstSrvs1.TextMatrix(p, 4))
'''''''''                    p = p + 1
'''''''''                Wend
'''''''''            End If
'''''''''            Set RetSrv = Nothing
'''''''''        End If
'''''''''    End If
'''''''''    Return
'''''''''SetAppeal:
'''''''''    If (Trim(lstSrvs1.TextMatrix(i, 1)) <> "") Then
'''''''''        InvId = InvoiceId
'''''''''        RcvId = Trim(lstSrvs2.TextMatrix(i, 1))
'''''''''        If (Trim(RcvId) <> "") Then
'''''''''            ItemId = Trim(lstSrvs2.TextMatrix(i, 0))
'''''''''            Srv = Trim(lstSrvs1.TextMatrix(i, 1))
'''''''''            Amt = lstSrvs1.TextMatrix(i, 4) ' Open Balance
'''''''''            Rsn = ""
'''''''''            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, _
'''''''''                                                InvId, "I", InsurerId, _
'''''''''                                                Amt, Invoicedate, "@", _
'''''''''                                                "K", "", "", _
'''''''''                                                "", Srv, ItemId, _
'''''''''                                                False, UserLogin.iId, False, _
'''''''''                                                "", 0, "", "", 0)
'''''''''        End If
'''''''''    End If
'''''''''    If (Trim(lstSrvs1.TextMatrix(i, 1)) <> "") Then
'''''''''        InvId = InvoiceId
'''''''''        RcvId = Trim(lstSrvs2.TextMatrix(i, 1))
'''''''''        If (Trim(RcvId) <> "") Then
'''''''''            ItemId = Trim(lstSrvs2.TextMatrix(i, 0))
'''''''''            Srv = Trim(lstSrvs1.TextMatrix(i, 1))
'''''''''            Amt = lstSrvs1.TextMatrix(i, 4) ' Open Balance
'''''''''            Rsn = ""
'''''''''            Call ApplTemp.ApplPostPaymentActivity(RcvId, PatientId, 0, _
'''''''''                                                InvId, "I", InsurerId, _
'''''''''                                                Amt, Invoicedate, "@", _
'''''''''                                                "K", "", "", _
'''''''''                                                "", Srv, ItemId, _
'''''''''                                                False, UserLogin.iId, False, _
'''''''''                                                "", 0, "", "", 0)
'''''''''        End If
'''''''''    End If
'''''''''
'''''''''    Return
'''''''''End Sub

Private Sub editCell(fgc As MSFlexGrid)
If bySys Then Exit Sub

If Not currentFGC.name = fgc.name Then Set currentFGC = fgc
Dim c As Integer, r As Integer
c = fgc.col
r = fgc.Row

With currentFGC
    If .name = "lstSrvs1" Then
        Select Case .col
        Case Is < 5
            .col = 5
        End Select
    End If
End With

pTxt.Top = fgc.Top + fgc.RowPos(r) + 15
pTxt.Left = fgc.Left + fgc.ColPos(c) + 15
pTxt.Width = fgc.ColWidth(c) - 30
bySys = True
If fgc.col = 8 Then
    txt.Alignment = 1
    txt.MaxLength = 0
Else
    txt.Alignment = 2
    txt.MaxLength = 1
End If
txt.Text = fgc.Text
txt.Width = pTxt.Width - 45
txt.SetFocus
txt.SelStart = 0
txt.SelLength = Len(txt.Text)

bySys = False
End Sub

Private Sub txt_KeyDown(KeyCode As Integer, Shift As Integer)
Dim nC As Integer, nR As Integer
        nR = currentFGC.Row
        nC = currentFGC.col

Select Case KeyCode
Case vbKeyPageDown, vbKeyPageUp
    If currentFGC.name = "lstSrvs" Then
        Set currentFGC = lstSrvs1
        lstSrvs1.Row = 1
        lstSrvs1.col = nC + 5
    Else
        Set currentFGC = lstSrvs
        lstSrvs.Row = 0
        lstSrvs.col = nC - 5
    End If
    editCell currentFGC
Case vbKeyTab
    With currentFGC
        Call txt_Validate(False)
        If Shift = 0 Then
            nC = .col + 1
            If .name = "lstSrvs" Then
                If nC = 2 Then nC = 0
            Else
                Select Case nC
                Case 7
                    nC = 8
                Case 9
                    nC = 5
                    nR = .Row + 1
                End Select
            End If
        Else
            nC = .col - 1
            If .name = "lstSrvs1" Then
                If nC < 5 Then nC = -1
                If nC = 7 Then nC = 6
            End If
            If nC = -1 Then
                If .name = "lstSrvs" Then
                    nC = 1
                Else
                    nC = 8
                    nR = .Row - 1
                End If
            End If
        End If
        
        If .name = "lstSrvs" Then
            nR = 0
        Else
            Select Case nR
            Case .Rows
                nR = 1
            Case 0
                nR = .Rows - 1
            End Select
        End If
        
        .col = nC
        .Row = nR
    End With
End Select

End Sub

Private Sub txt_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case vbKeyDelete, vbKeyBack
Case Else
    KeyAscii = validCode(KeyAscii)
End Select

End Sub

Private Function validCode(KCode As Integer) As Integer
Dim mC As Integer
If currentFGC.name = "lstSrvs" Then
    mC = currentFGC.col
Else
    mC = currentFGC.col - 5
End If

Dim c As Integer

If mC = 0 Then
    If IsNumeric(Chr(KCode)) Then
        Select Case Chr(KCode)
        Case 1 To lstIns1.Rows - 2
            c = KCode
        End Select
    Else
        If UCase(Chr(KCode)) = "P" Then c = Asc("P")
    End If
Else
    If currentFGC.col = 8 Then
        If Not IsNumeric(lstSrvs1.TextMatrix(lstSrvs1.Row, 5)) _
        Or lstSrvs1.TextMatrix(lstSrvs1.Row, 6) = "A" _
        Or lstSrvs1.TextMatrix(lstSrvs1.Row, 6) = "U" _
        Or lstSrvs1.TextMatrix(lstSrvs1.Row, 6) = "" _
        Or lstSrvs1.TextMatrix(lstSrvs1.Row, 7) = "P" _
        Then
            c = 0
        Else
            If IsNumeric(Chr(KCode)) Then
                If Chr(KCode) = "." Then
                    If InStrPS(1, txt.Text, ".") = 0 Then
                        c = KCode
                    End If
                Else
                    c = KCode
                End If
            End If
        End If
    Else
        Select Case UCase(Chr(KCode))
        Case "B", "A", "U", "W", "P"
            c = Asc(UCase(Chr(KCode)))
        End Select
        End If
End If

validCode = c
End Function
Private Sub txt_Validate(Cancel As Boolean)
Dim t As String
Dim n As Single

t = txt.Text
If currentFGC.name = "lstSrvs1" Then
    Select Case lstSrvs1.col
    Case 8
        n = val(t)
        If n = 0 Then
            t = ""
        Else
            t = Format(Round(n, 2), "#####0.00")
        End If
        lstSrvs1.TextMatrix(lstSrvs1.Row, 4) = _
        Format(Round(val(lstSrvs2.TextMatrix(lstSrvs1.Row, 4)) - n, 2), "#####0.00")
    Case 6
    End Select
End If
currentFGC.Text = t
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
