VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmSetupReports 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkOpt36 
      BackColor       =   &H0077742D&
      Caption         =   "Referral Doctor List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   67
      Top             =   5160
      Width           =   2655
   End
   Begin VB.CheckBox chkAdmin 
      BackColor       =   &H0077742D&
      Caption         =   "Administrative Access"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8400
      TabIndex        =   66
      Top             =   3480
      Width           =   2895
   End
   Begin VB.CheckBox chkOpt12 
      BackColor       =   &H0077742D&
      Caption         =   "Use Balance <"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   61
      Top             =   6120
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt13 
      BackColor       =   &H0077742D&
      Caption         =   "Use Balance >"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   60
      Top             =   6360
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt14 
      BackColor       =   &H0077742D&
      Caption         =   "Use Last Name <"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   59
      Top             =   6600
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt15 
      BackColor       =   &H0077742D&
      Caption         =   "Use Last Name >"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   58
      Top             =   6840
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt32 
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Time"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9120
      TabIndex        =   57
      Top             =   4440
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt33 
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9120
      TabIndex        =   56
      Top             =   4680
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt34 
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Type"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9120
      TabIndex        =   55
      Top             =   4920
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt35 
      BackColor       =   &H0077742D&
      Caption         =   "Appointment Notes"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9120
      TabIndex        =   54
      Top             =   5160
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt21 
      BackColor       =   &H0077742D&
      Caption         =   "Descending Order"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   53
      Top             =   4440
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt27 
      BackColor       =   &H0077742D&
      Caption         =   "Home Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   52
      Top             =   5880
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt28 
      BackColor       =   &H0077742D&
      Caption         =   "Work Phone"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   51
      Top             =   6120
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt30 
      BackColor       =   &H0077742D&
      Caption         =   "Date of Service"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   50
      Top             =   6600
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt31 
      BackColor       =   &H0077742D&
      Caption         =   "RVU"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   49
      Top             =   6840
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt29 
      BackColor       =   &H0077742D&
      Caption         =   "Primary Insurance"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   48
      Top             =   6360
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt22 
      BackColor       =   &H0077742D&
      Caption         =   "Referring Doctor"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   47
      Top             =   4680
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt23 
      BackColor       =   &H0077742D&
      Caption         =   "Outside Period"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   46
      Top             =   4920
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt25 
      BackColor       =   &H0077742D&
      Caption         =   "Pt Name &and Acct No"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   45
      Top             =   5400
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt26 
      BackColor       =   &H0077742D&
      Caption         =   "Patient Age"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   44
      Top             =   5640
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt24 
      BackColor       =   &H0077742D&
      Caption         =   "Operator"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   43
      Top             =   5160
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt1 
      BackColor       =   &H0077742D&
      Caption         =   "Fee Type List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   42
      Top             =   4440
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt7 
      BackColor       =   &H0077742D&
      Caption         =   "Procedure List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   41
      Top             =   6120
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt8 
      BackColor       =   &H0077742D&
      Caption         =   "Diagnosis List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   40
      Top             =   6360
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt10 
      BackColor       =   &H0077742D&
      Caption         =   "Operator List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   39
      Top             =   6840
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt11 
      BackColor       =   &H0077742D&
      Caption         =   "Aging Buckets"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   38
      Top             =   7080
      Width           =   2175
   End
   Begin VB.CheckBox chkOpt9 
      BackColor       =   &H0077742D&
      Caption         =   "Insurance List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   37
      Top             =   6600
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt2 
      BackColor       =   &H0077742D&
      Caption         =   "Balance Type List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   36
      Top             =   4680
      Width           =   2415
   End
   Begin VB.CheckBox chkOpt3 
      BackColor       =   &H0077742D&
      Caption         =   "Referral Type List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   35
      Top             =   4920
      Width           =   2655
   End
   Begin VB.CheckBox chkOpt5 
      BackColor       =   &H0077742D&
      Caption         =   "Payment Type List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   34
      Top             =   5640
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt6 
      BackColor       =   &H0077742D&
      Caption         =   "Procedure Type List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   33
      Top             =   5880
      Width           =   2775
   End
   Begin VB.CheckBox chkOpt4 
      BackColor       =   &H0077742D&
      Caption         =   "Adjustment Type List"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3360
      TabIndex        =   32
      Top             =   5400
      Width           =   2775
   End
   Begin VB.TextBox txtDrLcPtBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7920
      MaxLength       =   50
      TabIndex        =   10
      Top             =   3000
      Width           =   3375
   End
   Begin VB.TextBox txtLcPtBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7920
      MaxLength       =   50
      TabIndex        =   9
      Top             =   2520
      Width           =   3375
   End
   Begin VB.TextBox txtDrPtBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7920
      MaxLength       =   50
      TabIndex        =   8
      Top             =   2040
      Width           =   3375
   End
   Begin VB.TextBox txtDrLcBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2025
      MaxLength       =   50
      TabIndex        =   7
      Top             =   3480
      Width           =   3375
   End
   Begin VB.TextBox txtLcBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2025
      MaxLength       =   50
      TabIndex        =   6
      Top             =   3000
      Width           =   3375
   End
   Begin VB.TextBox txtDrBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2025
      MaxLength       =   50
      TabIndex        =   5
      Top             =   2520
      Width           =   3375
   End
   Begin VB.TextBox txtPtBrk 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2025
      MaxLength       =   50
      TabIndex        =   4
      Top             =   2040
      Width           =   3375
   End
   Begin VB.TextBox txtOrder 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   7920
      MaxLength       =   5
      TabIndex        =   3
      Top             =   1560
      Width           =   1335
   End
   Begin VB.TextBox txtDaily 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2025
      MaxLength       =   32
      TabIndex        =   2
      Top             =   1560
      Width           =   3375
   End
   Begin VB.CheckBox chkDocBrk 
      BackColor       =   &H0077742D&
      Caption         =   "Doctor Breakdown"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   4800
      Width           =   2775
   End
   Begin VB.CheckBox chkLocBrk 
      BackColor       =   &H0077742D&
      Caption         =   "Location Breakdown"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   5280
      Width           =   2775
   End
   Begin VB.CheckBox chkLocation 
      BackColor       =   &H0077742D&
      Caption         =   "Allow Locations"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   5040
      Width           =   2175
   End
   Begin VB.CheckBox chkDoctor 
      BackColor       =   &H0077742D&
      Caption         =   "Allow Doctors"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   4560
      Width           =   2175
   End
   Begin VB.CheckBox chkDate 
      BackColor       =   &H0077742D&
      Caption         =   "Allow Dates"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   4320
      Width           =   2175
   End
   Begin VB.CheckBox chkActivate 
      BackColor       =   &H0077742D&
      Caption         =   "Activate Report"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5520
      TabIndex        =   16
      Top             =   3480
      Width           =   2535
   End
   Begin VB.TextBox txtRpt 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2040
      MaxLength       =   50
      TabIndex        =   0
      Top             =   600
      Width           =   7215
   End
   Begin VB.TextBox txtDisplay 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2040
      MaxLength       =   32
      TabIndex        =   1
      Top             =   1080
      Width           =   7215
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   11
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupReports.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10200
      TabIndex        =   12
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupReports.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5040
      TabIndex        =   13
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetupReports.frx":03BE
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Entry Screen"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   65
      Top             =   3960
      Width           =   1410
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Textboxes"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   64
      Top             =   5760
      Width           =   1200
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Checkboxes"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   6480
      TabIndex        =   63
      Top             =   3960
      Width           =   1425
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Listboxes"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   3360
      TabIndex        =   62
      Top             =   3960
      Width           =   1125
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Report Setup"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      TabIndex        =   31
      Top             =   120
      Width           =   1770
   End
   Begin VB.Label lblDrLcPtBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doc/Loc/Pat Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5520
      TabIndex        =   30
      Top             =   3000
      Width           =   2370
   End
   Begin VB.Label lblLcPtBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Loc/Pat Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5520
      TabIndex        =   29
      Top             =   2520
      Width           =   1770
   End
   Begin VB.Label lblDrPtBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doc/Pat Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5520
      TabIndex        =   28
      Top             =   2040
      Width           =   1770
   End
   Begin VB.Label lblDrLcBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doc/Loc Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   27
      Top             =   3480
      Width           =   1770
   End
   Begin VB.Label lblLcBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   26
      Top             =   3000
      Width           =   1770
   End
   Begin VB.Label lblDrBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   25
      Top             =   2520
      Width           =   1770
   End
   Begin VB.Label lblPtBrk 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Patient Break"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   24
      Top             =   2040
      Width           =   1770
   End
   Begin VB.Label lblOrder 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Report Order"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5520
      TabIndex        =   23
      Top             =   1560
      Width           =   1650
   End
   Begin VB.Label lblDaily 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Daily"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   22
      Top             =   1560
      Width           =   1770
   End
   Begin VB.Label lblRptTmp 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "ReportTemplate"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   135
      TabIndex        =   15
      Top             =   1080
      Width           =   1800
   End
   Begin VB.Label lblRPtName 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Report Name"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   135
      TabIndex        =   14
      Top             =   600
      Width           =   1770
   End
End
Attribute VB_Name = "frmSetupReports"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Public ReportName As String
Private OrgRpt As String

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
If (Trim(ReportName) <> "") Then
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplDeleteReport(ReportName)) Then
        Unload frmSetupReports
    End If
    Set ApplTbl = Nothing
End If
End Sub

Private Sub cmdApply_Click()
Dim Temp As String
Dim TheOrder As Long
Dim RptTmp As String
Dim RptQry As String
Dim RptCrt As String
Dim RptAct As Boolean
Dim RptAdmin As Boolean
Dim RptDt As Boolean
Dim RptDr As Boolean
Dim RptLc As Boolean
Dim RptDaily As String
Dim RptCrDef As String
Dim RptDrBrk As Boolean
Dim RptLcBrk As Boolean
Dim RptPtBrk As String
Dim RptDrRpt As String, RptLcRpt As String, RptDrLcRpt As String
Dim RptDrPtRpt As String, RptLcPtRpt As String, RptDrLcPtRpt As String
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
Temp = txtRpt.Text
Call StripCharacters(Temp, "'")
txtRpt.Text = Temp
If (Trim(OrgRpt) <> Temp) And (Trim(OrgRpt) <> "") Then
    frmEventMsgs.Header = "Do you really want to replace this report ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result <> 2) Then
        Exit Sub
    End If
    Call ApplTbl.ApplDeleteReport(OrgRpt)
End If
If (ApplTbl.ApplGetReport(Temp, RptTmp, RptQry, RptCrt, RptAct, RptDt, _
                          RptDr, RptLc, RptDrBrk, RptLcBrk, RptPtBrk, _
                          RptDrRpt, RptLcRpt, RptDrLcRpt, RptDrPtRpt, _
                          RptLcPtRpt, RptDrLcPtRpt, RptDaily, RptCrDef, TheOrder, RptAdmin)) Then
    If (Trim(RptTmp) <> "") And (ReportName = "") Then
        If (Temp <> "") Then
            frmEventMsgs.Header = "Report Exists"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Set ApplTbl = Nothing
            Exit Sub
        End If
    End If
End If
RptAdmin = True
If (chkAdmin.Value = 0) Then
    RptAdmin = False
End If
RptAct = True
If (chkActivate.Value = 0) Then
    RptAct = False
End If
RptDt = True
If (chkDate.Value = 0) Then
    RptDt = False
End If
RptDr = True
If (chkDoctor.Value = 0) Then
    RptDr = False
End If
RptLc = True
If (chkLocation.Value = 0) Then
    RptLc = False
End If
RptLcBrk = True
If (chkLocBrk.Value = 0) Then
    RptLcBrk = False
End If
RptDrBrk = True
If (chkDocBrk.Value = 0) Then
    RptDrBrk = False
End If
RptDaily = txtDaily.Text
TheOrder = val(Trim(txtOrder.Text))
RptPtBrk = txtPtBrk.Text
RptDrRpt = txtDrBrk.Text
RptLcRpt = txtLcBrk.Text
RptDrLcRpt = txtDrLcBrk.Text
RptDrPtRpt = txtDrPtBrk.Text
RptLcPtRpt = txtLcPtBrk.Text
RptDrLcPtRpt = txtDrLcPtBrk.Text

RptCrDef = String(Len(RptCrDef), "0")
'Listboxes
If (chkOpt1.Value = 1) Then
    Mid(RptCrDef, 1, 1) = "1"
End If
If (chkOpt2.Value = 1) Then
    Mid(RptCrDef, 2, 1) = "1"
End If
If (chkOpt3.Value = 1) Then
    Mid(RptCrDef, 3, 1) = "1"
End If
If (chkOpt4.Value = 1) Then
    Mid(RptCrDef, 4, 1) = "1"
End If
If (chkOpt5.Value = 1) Then
    Mid(RptCrDef, 5, 1) = "1"
End If
If (chkOpt6.Value = 1) Then
    Mid(RptCrDef, 6, 1) = "1"
End If
If (chkOpt7.Value = 1) Then
    Mid(RptCrDef, 7, 1) = "1"
End If
If (chkOpt8.Value = 1) Then
    Mid(RptCrDef, 8, 1) = "1"
End If
If (chkOpt9.Value = 1) Then
    Mid(RptCrDef, 9, 1) = "1"
End If
If (chkOpt10.Value = 1) Then
    Mid(RptCrDef, 10, 1) = "1"
End If
If (chkOpt11.Value = 1) Then
    Mid(RptCrDef, 11, 1) = "1"
End If
' 12-15 Textboxes, 16-20 are open for textboxes
If (chkOpt12.Value = 1) Then
    Mid(RptCrDef, 12, 1) = "1"
End If
If (chkOpt13.Value = 1) Then
    Mid(RptCrDef, 13, 1) = "1"
End If
If (chkOpt14.Value = 1) Then
    Mid(RptCrDef, 14, 1) = "1"
End If
If (chkOpt15.Value = 1) Then
    Mid(RptCrDef, 15, 1) = "1"
End If
'Checkboxes
If (chkOpt21.Value = 1) Then
    Mid(RptCrDef, 21, 1) = "1"
End If
If (chkOpt22.Value = 1) Then
    Mid(RptCrDef, 22, 1) = "1"
End If
If (chkOpt23.Value = 1) Then
    Mid(RptCrDef, 23, 1) = "1"
End If
If (chkOpt24.Value = 1) Then
    Mid(RptCrDef, 24, 1) = "1"
End If
If (chkOpt25.Value = 1) Then
    Mid(RptCrDef, 25, 1) = "1"
End If
If (chkOpt26.Value = 1) Then
    Mid(RptCrDef, 26, 1) = "1"
End If
If (chkOpt27.Value = 1) Then
    Mid(RptCrDef, 27, 1) = "1"
End If
If (chkOpt28.Value = 1) Then
    Mid(RptCrDef, 28, 1) = "1"
End If
If (chkOpt29.Value = 1) Then
    Mid(RptCrDef, 29, 1) = "1"
End If
If (chkOpt30.Value = 1) Then
    Mid(RptCrDef, 30, 1) = "1"
End If
If (chkOpt31.Value = 1) Then
    Mid(RptCrDef, 31, 1) = "1"
End If
If (chkOpt32.Value = 1) Then
    Mid(RptCrDef, 32, 1) = "1"
End If
If (chkOpt33.Value = 1) Then
    Mid(RptCrDef, 33, 1) = "1"
End If
If (chkOpt34.Value = 1) Then
    Mid(RptCrDef, 34, 1) = "1"
End If
If (chkOpt35.Value = 1) Then
    Mid(RptCrDef, 35, 1) = "1"
End If
If (chkOpt36.Value = 1) Then
    Mid(RptCrDef, 36, 1) = "1"
End If
RptCrt = RptCrDef
If Not (ApplTbl.ApplPostReport(txtRpt.Text, txtDisplay.Text, "", RptCrt, _
                               RptAct, RptDt, RptDr, RptLc, RptDrBrk, RptLcBrk, RptPtBrk, _
                               RptDrRpt, RptLcRpt, RptDrLcRpt, RptDrPtRpt, RptLcPtRpt, _
                               RptDrLcPtRpt, RptDaily, RptCrDef, TheOrder, RptAdmin)) Then
    frmEventMsgs.Header = "Update Failed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set ApplTbl = Nothing
Unload frmSetupReports
End Sub

Private Sub cmdBack_Click()
Unload frmSetupReports
End Sub

Public Function LoadReportsDisplay(TheRpt As String) As Boolean
Dim u As Integer
Dim TheTmp As String
Dim TheQry As String
Dim TheTbl As String
Dim TheOrder As Long
Dim Active As Boolean
Dim RptAdmin As Boolean
Dim RptDt As Boolean
Dim RptDr As Boolean
Dim RptLc As Boolean
Dim RptDaily As String
Dim RptCrDef As String
Dim RptDrBrk As Boolean
Dim RptLcBrk As Boolean
Dim RptPtBrk As String
Dim RptDrRpt As String, RptLcRpt As String, RptDrLcRpt As String
Dim RptDrPtRpt As String, RptLcPtRpt As String, RptDrLcPtRpt As String
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
OrgRpt = ""
ReportName = TheRpt
If (Trim(TheRpt) <> "") Then
    txtRpt.Text = TheRpt
    If (ApplTbl.ApplGetReport(TheRpt, TheTmp, TheQry, TheTbl, Active, RptDt, _
                              RptDr, RptLc, RptDrBrk, RptLcBrk, RptPtBrk, _
                              RptDrRpt, RptLcRpt, RptDrLcRpt, RptDrPtRpt, _
                              RptLcPtRpt, RptDrLcPtRpt, RptDaily, RptCrDef, TheOrder, RptAdmin)) Then
        OrgRpt = TheRpt
        txtDisplay.Text = TheTmp
        chkAdmin.Value = 0
        If (RptAdmin) Then
            chkAdmin.Value = 1
        End If
        chkActivate.Value = 0
        If (Active) Then
            chkActivate.Value = 1
        End If
        chkDate.Value = 0
        If (RptDt) Then
            chkDate.Value = 1
        End If
        chkDoctor.Value = 0
        If (RptDr) Then
            chkDoctor.Value = 1
        End If
        chkLocation.Value = 0
        If (RptLc) Then
            chkLocation.Value = 1
        End If
        chkDocBrk.Value = 0
        If (RptDrBrk) Then
            chkDocBrk.Value = 1
        End If
        chkLocBrk.Value = 0
        If (RptLcBrk) Then
            chkLocBrk.Value = 1
        End If
        txtDaily.Text = RptDaily
        txtOrder.Text = Trim(Str(TheOrder))
        txtPtBrk.Text = RptPtBrk
        txtDrBrk.Text = RptDrRpt
        txtLcBrk.Text = RptLcRpt
        txtDrLcBrk.Text = RptDrLcRpt
        txtDrPtBrk.Text = RptDrPtRpt
        txtLcPtBrk.Text = RptLcPtRpt
        txtDrLcPtBrk.Text = RptDrLcRpt
        
        chkOpt1.Value = 0
        If (Mid(RptCrDef, 1, 1) = "1") Then
            chkOpt1.Value = 1
        End If
        chkOpt2.Value = 0
        If (Mid(RptCrDef, 2, 1) = "1") Then
            chkOpt2.Value = 1
        End If
        chkOpt3.Value = 0
        If (Mid(RptCrDef, 3, 1) = "1") Then
            chkOpt3.Value = 1
        End If
        chkOpt4.Value = 0
        If (Mid(RptCrDef, 4, 1) = "1") Then
            chkOpt4.Value = 1
        End If
        chkOpt5.Value = 0
        If (Mid(RptCrDef, 5, 1) = "1") Then
            chkOpt5.Value = 1
        End If
        chkOpt6.Value = 0
        If (Mid(RptCrDef, 6, 1) = "1") Then
            chkOpt6.Value = 1
        End If
        chkOpt7.Value = 0
        If (Mid(RptCrDef, 7, 1) = "1") Then
            chkOpt7.Value = 1
        End If
        chkOpt8.Value = 0
        If (Mid(RptCrDef, 8, 1) = "1") Then
            chkOpt8.Value = 1
        End If
        chkOpt9.Value = 0
        If (Mid(RptCrDef, 9, 1) = "1") Then
            chkOpt9.Value = 1
        End If
        chkOpt10.Value = 0
        If (Mid(RptCrDef, 10, 1) = "1") Then
            chkOpt10.Value = 1
        End If
        chkOpt11.Value = 0
        If (Mid(RptCrDef, 11, 1) = "1") Then
            chkOpt11.Value = 1
        End If
        chkOpt12.Value = 0
        If (Mid(RptCrDef, 12, 1) = "1") Then
            chkOpt12.Value = 1
        End If
        chkOpt13.Value = 0
        If (Mid(RptCrDef, 13, 1) = "1") Then
            chkOpt13.Value = 1
        End If
        chkOpt14.Value = 0
        If (Mid(RptCrDef, 14, 1) = "1") Then
            chkOpt14.Value = 1
        End If
        chkOpt15.Value = 0
        If (Mid(RptCrDef, 15, 1) = "1") Then
            chkOpt15.Value = 1
        End If
' 16-20 are open for textboxes
        chkOpt21.Value = 0
        If (Mid(RptCrDef, 21, 1) = "1") Then
            chkOpt21.Value = 1
        End If
        chkOpt22.Value = 0
        If (Mid(RptCrDef, 22, 1) = "1") Then
            chkOpt22.Value = 1
        End If
        chkOpt23.Value = 0
        If (Mid(RptCrDef, 23, 1) = "1") Then
            chkOpt23.Value = 1
        End If
        chkOpt24.Value = 0
        If (Mid(RptCrDef, 24, 1) = "1") Then
            chkOpt24.Value = 1
        End If
        chkOpt25.Value = 0
        If (Mid(RptCrDef, 25, 1) = "1") Then
            chkOpt25.Value = 1
        End If
        chkOpt26.Value = 0
        If (Mid(RptCrDef, 26, 1) = "1") Then
            chkOpt26.Value = 1
        End If
        chkOpt27.Value = 0
        If (Mid(RptCrDef, 27, 1) = "1") Then
            chkOpt27.Value = 1
        End If
        chkOpt28.Value = 0
        If (Mid(RptCrDef, 28, 1) = "1") Then
            chkOpt28.Value = 1
        End If
        chkOpt29.Value = 0
        If (Mid(RptCrDef, 29, 1) = "1") Then
            chkOpt29.Value = 1
        End If
        chkOpt30.Value = 0
        If (Mid(RptCrDef, 30, 1) = "1") Then
            chkOpt30.Value = 1
        End If
        chkOpt31.Value = 0
        If (Mid(RptCrDef, 31, 1) = "1") Then
            chkOpt31.Value = 1
        End If
        chkOpt32.Value = 0
        If (Mid(RptCrDef, 32, 1) = "1") Then
            chkOpt32.Value = 1
        End If
        chkOpt33.Value = 0
        If (Mid(RptCrDef, 33, 1) = "1") Then
            chkOpt33.Value = 1
        End If
        chkOpt34.Value = 0
        If (Mid(RptCrDef, 34, 1) = "1") Then
            chkOpt34.Value = 1
        End If
        chkOpt35.Value = 0
        If (Mid(RptCrDef, 35, 1) = "1") Then
            chkOpt35.Value = 1
        End If
        chkOpt36.Value = 0
        If (Mid(RptCrDef, 36, 1) = "1") Then
            chkOpt36.Value = 1
        End If
    End If
End If
Set ApplTbl = Nothing
cmdDelete.Visible = DeleteOn
LoadReportsDisplay = True
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmSetupReports.BorderStyle = 1
    frmSetupReports.ClipControls = True
    frmSetupReports.Caption = Mid(frmSetupReports.Name, 4, Len(frmSetupReports.Name) - 3)
    frmSetupReports.AutoRedraw = True
    frmSetupReports.Refresh
End If
End Sub

Private Sub txtOrder_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If Not (IsNumeric(Chr(KeyAscii))) Then
        frmEventMsgs.Header = "Not a valid number"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
    End If
End If
End Sub

Private Sub txtOrder_Validate(Cancel As Boolean)
Dim i As Integer
txtOrder.Text = Trim(txtOrder.Text)
For i = 1 To Len(txtOrder.Text)
    If Not (IsNumeric(Mid(txtOrder.Text, i, 1))) Then
        frmEventMsgs.Header = "Not a valid number"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtOrder.Text = ""
        txtOrder.SetFocus
        Exit For
    End If
Next i
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
