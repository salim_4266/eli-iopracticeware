VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPhysicianSpi 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Select"
   ClientHeight    =   6525
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10185
   ClipControls    =   0   'False
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6525
   ScaleWidth      =   10185
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lst 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Index           =   0
      Left            =   480
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1680
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.ListBox lst 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Index           =   1
      Left            =   2160
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   2160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.PictureBox pTxt 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   4440
      ScaleHeight     =   255
      ScaleWidth      =   1695
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2400
      Visible         =   0   'False
      Width           =   1695
      Begin VB.TextBox txt 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   45
         TabIndex        =   5
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   15
         Width           =   975
      End
   End
   Begin VB.ComboBox dd 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   6480
      Style           =   2  'Dropdown List
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1560
      Width           =   1935
   End
   Begin VB.Frame fr 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   4395
      Left            =   3960
      TabIndex        =   6
      Top             =   750
      Width           =   255
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   5400
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PhysicianSpi.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   975
      Left            =   8520
      TabIndex        =   1
      Top             =   5400
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PhysicianSpi.frx":01DF
   End
   Begin MSFlexGridLib.MSFlexGrid fgc 
      Height          =   4455
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   720
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   7858
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      GridColor       =   8388608
      AllowBigSelection=   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   2
      ScrollBars      =   2
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCode 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Doctor code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   10
      Top             =   240
      Width           =   1290
   End
   Begin VB.Label lblDocName 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Doctor name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4560
      TabIndex        =   9
      Top             =   240
      Width           =   1350
   End
End
Attribute VB_Name = "frmPhysicianSpi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mPhysicianId As Long
Dim bySys As Boolean

Private Sub cmdApply_Click()
Dim i As Integer

For i = 1 To fgc.Rows - 2
    If Not fgc.TextMatrix(i, 1) = "" Then
        If Not Len(fgc.TextMatrix(i, 1)) = 13 Then
            fgc.col = 1
            GoTo ErrMsg
        End If
    End If
    If Not fgc.TextMatrix(i, 1) = "" _
       And fgc.TextMatrix(i, 2) = "" Then
        fgc.col = 2
        GoTo ErrMsg
    End If
    If Not fgc.TextMatrix(i, 4) = "" _
       And fgc.TextMatrix(i, 5) = "" Then
        fgc.col = 5
        GoTo ErrMsg
    End If
Next

Dim Phisic As New CPhysicianSpi
Dim TST As String

Phisic.PhysicianId = mPhysicianId
If Phisic.KillPhysic Then
    For i = 1 To fgc.Rows - 2
        Phisic.Spi = fgc.TextMatrix(i, 1)
        
        TST = myTrim(fgc.TextMatrix(i, 2), 1)
        If Trim(TST) = "" Then TST = -1
        Phisic.LocationId = TST
        
        Phisic.Phone = fgc.TextMatrix(i, 4)
        
        TST = myTrim(fgc.TextMatrix(i, 5), 1)
        If Trim(TST) = "" Then TST = -1
        Phisic.PhoneType = TST
        
        Phisic.PutPhysic
    Next
End If
Set Phisic = Nothing
Unload Me

Exit Sub
ErrMsg:
fgc.Row = i

Select Case fgc.col
Case 1
    frmEventMsgs.Header = "Invalid Spi"
Case 2
    frmEventMsgs.Header = "Select Location"
Case 5
    frmEventMsgs.Header = "Select PhoneType"
End Select
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
End Sub

Private Sub cmdBack_Click()
Unload Me
End Sub

Public Function LoadInfo() As Boolean
Dim TST As String
Dim i As Integer
Dim RS As Recordset
Dim ApptList As New ApplicationAIList
Dim Phisic As New CPhysicianSpi
Dim RetCode As New PracticeCodes

    'REFACTOR: Much better to design this to pass in parameters than just copy from a form.
mPhysicianId = frmSchedulerResource.TheResourceId
lblDocName.Caption = frmSchedulerResource.Description.Text
lblCode.Caption = frmSchedulerResource.ResourceName.Text

Set ApptList.ApplList = lst(0)
Call ApptList.ApplLoadLocation(False, True)
Set ApptList = Nothing
lst(0).RemoveItem (0)

RetCode.ReferenceType = "PHONETYPE"
If (RetCode.FindCode > 0) Then
    i = 1
    Do Until Not RetCode.SelectCode(i)
        lst(1).AddItem RetCode.ReferenceCode & Space(40) & RetCode.ItemId
        i = i + 1
    Loop
End If
Set RetCode = Nothing

'format fgc
fgc.FormatString = "#|Spi|Location||Phone|PhoneType"
fgc.ColWidth(0) = 300
fgc.ColWidth(1) = 1560
fgc.ColWidth(2) = 1800
fgc.ColWidth(3) = 1500
fgc.ColWidth(4) = 2700
fgc.ColWidth(5) = 1800
fgc.ColAlignment(1) = flexAlignLeftCenter
fgc.ColAlignment(4) = flexAlignLeftCenter

'load fgc
Set RS = Phisic.GetPhysicList
Set Phisic = Nothing

fgc.Rows = 2
Do Until RS.EOF
    fgc.TextMatrix(fgc.Rows - 1, 0) = fgc.Rows - 1

    fgc.TextMatrix(fgc.Rows - 1, 1) = RS("spi")
    TST = RS("locationid")
    For i = 0 To lst(0).ListCount - 1
        If TST = myTrim(lst(0).List(i), 1) Then
            fgc.TextMatrix(fgc.Rows - 1, 2) = lst(0).List(i)
            Exit For
        End If
    Next
    
    fgc.TextMatrix(fgc.Rows - 1, 4) = RS("phone")
    TST = RS("phonetype")
    For i = 0 To lst(1).ListCount - 1
        If TST = myTrim(lst(1).List(i), 1) Then
            fgc.TextMatrix(fgc.Rows - 1, 5) = lst(1).List(i)
            Exit For
        End If
    Next
    
    fgc.Rows = fgc.Rows + 1
    RS.MoveNext
Loop


fr.Left = fgc.ColPos(3) + fgc.Left + 15
fr.Width = fgc.ColWidth(3) - 15

fgc.col = 1
fgc.Row = 1
editCell

LoadInfo = True
End Function

Private Sub dd_Click()
txt.Text = dd.Text
txt.SetFocus
End Sub

Private Sub fgc_EnterCell()
editCell
End Sub

Private Sub editCell()
Select Case fgc.col
Case 0
    fgc.col = 1
Case 2, 5
    If fgc.TextMatrix(fgc.Row, fgc.col - 1) = "" Then
        fgc.col = fgc.col - 1
    End If
End Select

Dim c As Integer, r As Integer
c = fgc.col
r = fgc.Row

pTxt.Top = fgc.Top + fgc.RowPos(r) + 15
pTxt.Left = fgc.Left + fgc.ColPos(c) + 15
pTxt.Width = fgc.ColWidth(c) - 30

bySys = True
Select Case c
Case 1
    txt.MaxLength = 13
Case 4
    txt.MaxLength = 25
Case Else
    txt.MaxLength = 0
End Select
txt.Text = fgc.Text
txt.Width = pTxt.Width - 45

Select Case c
Case 2, 5
    dd.Left = pTxt.Left
    dd.Width = pTxt.Width + 30
    dd.Top = pTxt.Top - 30
    dd.Visible = True
    loadDD c
    pTxt.Width = pTxt.Width - 240
Case Else
    dd.Visible = False
    txt.SelStart = 0
    txt.SelLength = Len(txt.Text)
End Select
bySys = False

pTxt.Visible = True
On Error Resume Next
txt.SetFocus

End Sub

Private Sub loadDD(c As Integer)
Dim l As Integer
If c = 5 Then l = 1

dd.Clear
Dim i As Integer
For i = 0 To lst(l).ListCount - 1
    dd.AddItem lst(l).List(i)
Next
End Sub

Private Sub fgc_GotFocus()
fgc_EnterCell
End Sub

Private Sub Form_Activate()
txt.SetFocus
End Sub

Private Sub Form_Load()
cmdBack.TabStop = False
cmdApply.TabStop = False
End Sub

Private Sub txt_KeyDown(KeyCode As Integer, Shift As Integer)

Dim ExitSub As Boolean
Select Case KeyCode
Case vbKeyTab, vbKeyReturn
    Call txt_Validate(ExitSub)
    If ExitSub Then Exit Sub
Case vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
    If dd.Visible Then KeyCode = 0
Case vbKeyPageDown, vbKeyPageUp
Case Else
    KeyCode = 0
End Select

Dim c As Integer, r As Integer
c = fgc.col
r = fgc.Row
Dim nextC As Integer, nextR As Integer
With fgc
    Select Case KeyCode
    Case vbKeyTab
        bySys = True
        Call NextCell(nextC, nextR, Shift)
        .Row = nextR
        .col = nextC
        bySys = False
        editCell
    Case vbKeyReturn
        txt.Text = .Text
    End Select
End With

End Sub

Private Sub NextCell(nC As Integer, nR As Integer, sShift As Integer)
Dim c As Integer, r As Integer

    With fgc
        If sShift = 0 Then
            If .col = .Cols - 1 Then
                nC = 1
                If .Row = .Rows - 1 Then
                    nR = 1
                Else
                    nR = .Row + 1
                End If
            Else
                nC = .col + 1
                nR = .Row
            End If
        Else
            If .col = 1 Then
                nC = .Cols - 1
                If .Row = 1 Then
                    nR = .Rows - 1
                Else
                    nR = .Row - 1
                End If
            Else
                nC = .col - 1
                nR = .Row
            End If
        End If
    End With
    
    Select Case nC
    Case 3
        If sShift Then
            nC = 2
        Else
            nC = 4
        End If
    End Select
    
    Select Case nC
    Case 2
        If fgc.TextMatrix(nR, nC - 1) = "" Then
            If sShift Then
                nC = 1
            Else
                nC = 4
            End If
        End If
    Case 5
        If fgc.TextMatrix(nR, nC - 1) = "" Then
            If sShift Then
                nC = 4
            Else
                nC = 1
                If nR = fgc.Rows - 1 Then
                    nR = 1
                Else
                    nR = nR + 1
                End If
            End If
        End If
    End Select
    
    
End Sub


Private Sub txt_KeyPress(KeyAscii As Integer)
If dd.Visible Then KeyAscii = 0
If fgc.col = 1 Then
    Select Case KeyAscii
    Case vbKeyLeft, vbKeyRight, vbKeyDelete, vbKeyBack, vbKeyReturn
    Case Else
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End Select
End If
End Sub

Private Sub txt_Validate(Cancel As Boolean)
fgc.Text = txt.Text

Select Case fgc.col
Case 1, 4
    If fgc.TextMatrix(fgc.Row, fgc.col) = "" Then
        fgc.TextMatrix(fgc.Row, fgc.col + 1) = ""
    End If
End Select

Dim TST As String
Dim i As Integer
For i = 1 To 4
    TST = TST & Trim(fgc.TextMatrix(fgc.Row, i))
Next

If fgc.TextMatrix(fgc.Row, 0) = "" Then
    If Not TST = "" Then
        fgc.TextMatrix(fgc.Row, 0) = fgc.Rows - 1
        fgc.Rows = fgc.Rows + 1
    End If
Else
    If TST = "" Then
        fgc.RemoveItem fgc.Row
        For i = 1 To fgc.Rows - 2
            fgc.TextMatrix(i, 0) = i
        Next
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
