VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSchedulerSetup 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9675
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12420
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9675
   ScaleWidth      =   12420
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdCancel 
      Height          =   990
      Left            =   240
      TabIndex        =   3
      Top             =   8280
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApptType 
      Height          =   990
      Left            =   240
      TabIndex        =   2
      Top             =   2880
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResources 
      Height          =   990
      Left            =   240
      TabIndex        =   1
      Top             =   1800
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":03CB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPractice 
      Height          =   990
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":05AF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsgCodes 
      Height          =   990
      Left            =   8880
      TabIndex        =   5
      Top             =   2880
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":0792
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlans 
      Height          =   990
      Left            =   3120
      TabIndex        =   6
      Top             =   1800
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":0972
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdVendors 
      Height          =   990
      Left            =   3120
      TabIndex        =   7
      Top             =   720
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":0B52
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdServices 
      Height          =   990
      Left            =   6000
      TabIndex        =   8
      Top             =   3960
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":0D34
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApptsCategories 
      Height          =   990
      Left            =   240
      TabIndex        =   9
      Top             =   3960
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":0F17
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdConfig 
      Height          =   990
      Left            =   8880
      TabIndex        =   10
      Top             =   5040
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":1108
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFavorites 
      Height          =   990
      Left            =   6000
      TabIndex        =   11
      Top             =   6120
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":12FA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFees 
      Height          =   990
      Left            =   8880
      TabIndex        =   12
      Top             =   1800
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":1513
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsgs 
      Height          =   990
      Left            =   3120
      TabIndex        =   13
      Top             =   6120
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":16FB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSrvCodes 
      Height          =   990
      Left            =   6000
      TabIndex        =   14
      Top             =   5040
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":190B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPOS 
      Height          =   990
      Left            =   6000
      TabIndex        =   15
      Top             =   2880
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":1AF3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTOS 
      Height          =   990
      Left            =   6000
      TabIndex        =   16
      Top             =   1800
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":1CE5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRefDr 
      Height          =   990
      Left            =   6000
      TabIndex        =   17
      Top             =   720
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":1ED6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReports 
      Height          =   990
      Left            =   8880
      TabIndex        =   18
      Top             =   720
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":20D5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOFavs 
      Height          =   990
      Left            =   3120
      TabIndex        =   19
      Top             =   3960
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":22B7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSurgical 
      Height          =   990
      Left            =   3120
      TabIndex        =   20
      Top             =   2880
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":24A4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMedLoc 
      Height          =   990
      Left            =   3120
      TabIndex        =   21
      Top             =   5040
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":2690
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   8880
      TabIndex        =   22
      Top             =   8280
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":2882
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSchTemplates 
      Height          =   990
      Left            =   240
      TabIndex        =   23
      Top             =   5040
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":2A61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTasks 
      Height          =   990
      Left            =   8880
      TabIndex        =   24
      Top             =   3960
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":2C4E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   990
      Left            =   8880
      TabIndex        =   25
      Top             =   6120
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":2E3C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCDS 
      Height          =   990
      Left            =   240
      TabIndex        =   26
      Top             =   6120
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":3038
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnBG 
      Height          =   990
      Left            =   240
      TabIndex        =   27
      Top             =   7200
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":3216
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnEP 
      Height          =   990
      Left            =   3120
      TabIndex        =   28
      Top             =   7200
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SchedulerSetup.frx":3400
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Setup"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   375
      Left            =   4710
      TabIndex        =   4
      Top             =   120
      Width           =   2595
   End
End
Attribute VB_Name = "frmSchedulerSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Ref As String
Dim manager

Private Sub cmdApptsCategories_Click()
On Error GoTo lcmdApptsCategories_Click_Error

    Dim ApptCategoriesComWrapper As New comWrapper
    Call ApptCategoriesComWrapper.Create(ConfigureCategoriesViewManagerType, emptyArgs)
    Call ApptCategoriesComWrapper.InvokeMethod("ShowConfigureCategories", emptyArgs)
    Exit Sub
    
lcmdApptsCategories_Click_Error:
    LogError "frmSchedulerSetup", "cmdApptsCategories_Click", Err, Err.Description
End Sub

Private Sub cmdApptType_Click()
    If Not UserLogin.HasPermission(epSetupAppointmentTypes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        If (frmApptType.LoadApptTypeList) Then
            frmApptType.Show 1
        End If
    End If
End Sub

Private Sub cmdCancel_Click()
Unload frmSchedulerSetup
End Sub

Private Sub cmdCDS_Click()
   Dim UserId As String
   UserId = UserLogin.iId
   Dim ClinicalMgr As New ClinicalIntegration.frmCDSConfig
   ClinicalMgr.DbObject = IdbConnection
   ClinicalMgr.ShowDialog
End Sub

Private Sub cmdConfig_Click()
    If Not UserLogin.HasPermission(epSetupConfiguration) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        If (frmSetupConfig.LoadConfigs) Then
            frmSetupConfig.Show 1
        End If
    End If
End Sub
Private Sub cmdDone_Click()
Unload frmSchedulerSetup
End Sub

Private Sub cmdFavorites_Click()
    If Not UserLogin.HasPermission(epSetupFavorites) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmEventMsgs.Header = "Which Favorite ?"
        frmEventMsgs.AcceptText = "Diagnosis"
        frmEventMsgs.RejectText = "Test Order"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "E M Ranks"
        frmEventMsgs.Other1Text = "Drugs"
        frmEventMsgs.Other2Text = "Drug Families"
        frmEventMsgs.Other3Text = "Super Diag Favs"
        frmEventMsgs.Other4Text = "Contact Lens"
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            If (frmSetupFavorites.LoadFavorites("D")) Then
                frmSetupFavorites.Show 1
            End If
        ElseIf (frmEventMsgs.Result = 2) Then
            If (frmSetupFavorites.LoadFavorites("T")) Then
                frmSetupFavorites.Show 1
            End If
        ElseIf (frmEventMsgs.Result = 3) Then
            If (frmSetupFavorites.LoadFavorites("E")) Then
                frmSetupFavorites.Show 1
            End If
        ElseIf (frmEventMsgs.Result = 5) Then
            If (frmSetupFavorites.LoadFavorites("R")) Then
                frmSetupFavorites.frProp.Visible = True
                frmSetupFavorites.cmdAll.Visible = True
                frmSetupFavorites.Show 1
            End If
        ElseIf (frmEventMsgs.Result = 6) Then
            If (frmSetupFavorites.LoadFavorites("F")) Then
                frmSetupFavorites.Show 1
            End If
        ElseIf (frmEventMsgs.Result = 7) Then
            If (frmSetupFavorites.LoadFavorites("d")) Then
                frmSetupFavorites.Show 1
            End If
        ElseIf (frmEventMsgs.Result = 8) Then
            If (frmSetupFavorites.LoadFavorites("L")) Then
                frmSetupFavorites.Show 1
            End If
        End If
    End If
End Sub

Private Sub cmdFees_Click()
Dim Keep As Boolean
    If Not UserLogin.HasPermission(epSetupFeeSchedules) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        Keep = True
        While (Keep)
            Call frmSelectDialogue.BuildSelectionDialogue("SERVICEWITHAMT")
            frmSelectDialogue.Show 1
            If (Trim(frmSelectDialogue.Selection) <> "") And (InStrPS(frmSelectDialogue.Selection, "Create") = 0) Then
                frmFeeSchedule.DeleteOn = False
                If (frmFeeSchedule.FeeLoadDisplay(Trim(Left(frmSelectDialogue.Selection, 7)), 0)) Then
                    frmFeeSchedule.Show 1
                Else
                    frmEventMsgs.Header = "No Fee Schedule for this Service"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    Keep = False
                End If
            Else
                Keep = False
            End If
        Wend
    End If
End Sub

Private Sub cmdMedLoc_Click()
    If Not UserLogin.HasPermission(epSetupResources) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        If (frmMedicaidLocations.LoadMedLocList(0)) Then
            frmMedicaidLocations.Show 1
        End If
    End If
End Sub

Private Sub cmdMsgCodes_Click()
    If Not UserLogin.HasPermission(epSetupNotesActionsTests) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmMessages.ServicesOn = False
        frmMessages.CodesOn = True
        frmMessages.DeleteOn = False
        frmMessages.Show 1
    End If
End Sub

Private Sub cmdMsgs_Click()
    If Not UserLogin.HasPermission(epSetupCodes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmMessages.ServicesOn = False
        frmMessages.CodesOn = False
        frmMessages.DeleteOn = False
        frmMessages.Show 1
    End If
End Sub

Private Sub cmdOFavs_Click()
Dim AName As String
Dim sRank As String
Dim sType As String
Dim Keep As Boolean
    On Error GoTo lcmdOFavs_Click_Error

    If Not UserLogin.HasPermission(epSetupOtherFavorites) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        Keep = True
        While (Keep)
            Call frmSelectDialogue.BuildSelectionDialogue("CLINICALTEMPLATES")
            frmSelectDialogue.Show 1
            AName = frmSelectDialogue.Selection
            If AName <> "" And InStrPS(AName, "Create") = 0 Then
                sType = Left$(frmSelectDialogue.Selection, 1)
                sRank = Mid$(frmSelectDialogue.Selection, 3, 3)
                AName = Trim$(Mid$(frmSelectDialogue.Selection, 8))
                If frmOtherFavorites.LoadOtherFavorites(AName, sRank, sType, False) Then
                    frmOtherFavorites.Show 1
                Else
                    Keep = False
                End If
            ElseIf Left$(frmSelectDialogue.Selection, 6) = "Create" Then
                If frmOtherFavorites.LoadOtherFavorites("", "000", "", True) Then
                    frmOtherFavorites.Show 1
                Else
                    Keep = False
                End If
            Else
                Keep = False
            End If
        Wend
    End If

    Exit Sub

lcmdOFavs_Click_Error:

    LogError "frmSchedulerSetup", "cmdOFavs_Click", Err, Err.Description
End Sub

Private Sub cmdPlans_Click()
Dim Keep As Boolean
Dim Rec As String
Dim ins As String
    If Not UserLogin.HasPermission(epSetupPlans) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        Call DisplayInsurancePlanScreen
    End If
End Sub

Private Sub cmdPractice_Click()
    If Not UserLogin.HasPermission(epSetupPractice) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        If (frmPractice.PracticeLoadDisplay) Then
            frmPractice.Show 1
        End If
    End If
End Sub

Private Sub cmdPrint_Click()
    Dim PrinterSetupComWrapper As New comWrapper
    Call PrinterSetupComWrapper.Create(PrinterSetupViewManagerType, emptyArgs)
    Set manager = PrinterSetupComWrapper.Instance
    Call manager.ShowPrinterOptions
End Sub

Private Sub cmdRefDr_Click()
Dim Keep As Boolean
Dim PermOn As Boolean
Dim AType As String
Dim TheType As String
Dim Rec As Long
    Keep = True
    If Not UserLogin.HasPermission(epSetupReferringDoctors) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmEventMsgs.Header = "Which Type?"
        frmEventMsgs.AcceptText = "Doctors"
        frmEventMsgs.RejectText = "Hospitals"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Labs"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
            
        Dim ExternalProvidersComWrapper As New comWrapper
         
        Call ExternalProvidersComWrapper.Create("IO.Practiceware.Presentation.Views.ExternalProviders.ExternalProvidersViewManager", emptyArgs)
                Dim arguments() As Variant
        If (frmEventMsgs.Result = 4) Then
            Exit Sub
        ElseIf (frmEventMsgs.Result = 1) Then
            ExternalProvidersComWrapper.InvokeMethod "ShowExternalProviders", emptyArgs
            Exit Sub
        ElseIf (frmEventMsgs.Result = 2) Then
            arguments = Array(3)
            ExternalProvidersComWrapper.InvokeMethod "ShowExternalProviders", arguments
            Exit Sub
        ElseIf (frmEventMsgs.Result = 3) Then
            arguments = Array(4)
            ExternalProvidersComWrapper.InvokeMethod "ShowExternalProviders", arguments
            Exit Sub
        End If
        While (Keep)
            Call frmSelectDialogue.BuildSelectionDialogue(TheType)
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                If (InStrPS(frmSelectDialogue.Selection, "Create") <> 0) Then
                    frmVendors.TheType = AType
                    frmVendors.DeleteOn = False
                    If (frmVendors.VendorLoadDisplay(0, PermOn)) Then
                        frmVendors.Show 1
                    End If
                Else
                    frmVendors.TheType = AType
                    frmVendors.DeleteOn = False
                    frmVendors.DeleteOn = CheckConfigCollection("REMOVEACTIVE") = "T"
                    Rec = val(Mid(frmSelectDialogue.Selection, 57, Len(frmSelectDialogue.Selection) - 56))
                    If (frmVendors.VendorLoadDisplay(Rec, PermOn)) Then
                        frmVendors.Show 1
                    End If
                End If
            Else
                Keep = False
            End If
        Wend
    End If
End Sub

Private Sub cmdResources_Click()
    If Not UserLogin.HasPermission(epSetupResources) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        Call frmResources.LoadResourceList
        frmResources.Show 1
    End If
End Sub

Private Sub cmdServices_Click()
Dim Keep As Boolean
Dim Temp As String
Dim IPlan As Long
Dim TheCode As String
    If Not UserLogin.HasPermission(epSetupServices) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        IPlan = 0
        Keep = True
        While (Keep)
            Temp = frmSelectDialogue.InsurerSelected
            frmSelectDialogue.InsurerSelected = ""
            Call frmSelectDialogue.BuildSelectionDialogue("SERVICE")
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                If (frmSelectDialogue.Selection = "Create Service") Then
                    frmService.DeleteOn = False
                    frmService.PreviousPlan = IPlan
                    If (frmService.ServiceLoadDisplay("-", "P")) Then
                        frmService.Show 1
                        IPlan = frmService.PreviousPlan
                    End If
                Else
                    frmService.PreviousPlan = IPlan
                    frmService.DeleteOn = CheckConfigCollection("REMOVEACTIVE") = "T"
                    TheCode = Trim(Left(frmSelectDialogue.Selection, 7))
                    If (Left(TheCode, 6) = "Create") Then
                        TheCode = ""
                    End If
                    If (frmService.ServiceLoadDisplay(TheCode, "P")) Then
                        frmService.Show 1
                        IPlan = frmService.PreviousPlan
                    End If
                End If
            Else
                Keep = False
            End If
            frmSelectDialogue.InsurerSelected = Temp
        Wend
    End If
End Sub

Private Sub cmdSrvCodes_Click()
    If Not UserLogin.HasPermission(epSetupServiceCodes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmMessages.ServicesOn = True
        frmMessages.CodesOn = False
        frmMessages.DeleteOn = False
        frmMessages.Show 1
    End If
End Sub

Private Sub cmdSchTemplates_Click()
On Error GoTo lcmdSchTemplates_Click_Error
    
    Dim ScheduleTemplateBuilderComWrapper As New comWrapper
    Call ScheduleTemplateBuilderComWrapper.Create(ScheduleTemplateBuilderViewManagerType, emptyArgs)
    Call ScheduleTemplateBuilderComWrapper.InvokeMethod("ShowScheduleTemplateBuilder", emptyArgs)
    Exit Sub

lcmdSchTemplates_Click_Error:
      LogError "frmSchedulerSetup", "lcmdSchTemplates_Click_Error", Err, Err.Description
End Sub

Private Sub cmdSurgical_Click()
Dim Keep As Boolean
    Keep = True
    If Not UserLogin.HasPermission(epSetupCodes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        While (Keep)
            Call frmSelectDialogue.BuildSelectionDialogue("SurgicalRequests")
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                If (Left(frmSelectDialogue.Selection, 6) = "Create") Then
                    frmSurgicalAI.AppointmentId = 0
                    frmSurgicalAI.PatientId = 0
                    frmSurgicalAI.ClinicalId = 0
                    frmSurgicalAI.TheTemplate = ""
                    frmSurgicalAI.Show 1
                ElseIf (Trim(frmSelectDialogue.Selection) <> "") Then
                    frmSurgicalAI.AppointmentId = 0
                    frmSurgicalAI.PatientId = 0
                    frmSurgicalAI.ClinicalId = 0
                    frmSurgicalAI.TheTemplate = Trim(Left(frmSelectDialogue.Selection, 55))
                    frmSurgicalAI.Show 1
                End If
            Else
                Keep = False
            End If
        Wend
    End If
End Sub

Private Sub cmdTasks_Click()
    Set manager = TaskComWrapper.Instance
    Call manager.AdministerTasks
End Sub

Private Sub cmdTOS_Click()
Dim Keep As Boolean
    Keep = True
    If Not UserLogin.HasPermission(epSetupTypeOfService) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        While (Keep)
            Call frmSelectDialogue.BuildSelectionDialogue("TOS")
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                If (Trim(frmSelectDialogue.Selection) = "Create TOS Table") Then
                    frmTosPos.DeleteOn = False
                    frmTosPos.TableName = ""
                    frmTosPos.TblType = "T"
                    If (frmTosPos.LoadDisplay) Then
                        frmTosPos.Show 1
                    End If
                ElseIf (Trim(frmSelectDialogue.Selection) <> "") Then
                    frmTosPos.DeleteOn = False
                    frmTosPos.TableName = Trim(Left(frmSelectDialogue.Selection, 55))
                    frmTosPos.TblType = "T"
                    If (frmTosPos.LoadDisplay) Then
                        frmTosPos.Show 1
                    End If
                End If
            Else
                Keep = False
            End If
        Wend
    End If
End Sub

Private Sub cmdPOS_Click()
Dim Keep As Boolean
    Keep = True
    If Not UserLogin.HasPermission(epSetupPlaceOfService) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        While (Keep)
            Call frmSelectDialogue.BuildSelectionDialogue("POS")
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                If (Trim(frmSelectDialogue.Selection) = "Create POS Table") Then
                    frmTosPos.DeleteOn = False
                    frmTosPos.TableName = ""
                    frmTosPos.TblType = "P"
                    If (frmTosPos.LoadDisplay) Then
                        frmTosPos.Show 1
                    End If
                ElseIf (Trim(frmSelectDialogue.Selection) <> "") Then
                    frmTosPos.DeleteOn = False
                    frmTosPos.TableName = Trim(Left(frmSelectDialogue.Selection, 55))
                    frmTosPos.TblType = "P"
                    If (frmTosPos.LoadDisplay) Then
                        frmTosPos.Show 1
                    End If
                End If
            Else
                Keep = False
            End If
        Wend
    End If
End Sub

Private Sub cmdVendors_Click()
Dim PermOn As Boolean
Dim Keep As Boolean
Dim Ref As String
Dim Rec As Long
    Keep = True
    If Not UserLogin.HasPermission(epSetupVendors) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        Dim ExternalProvidersComWrapper As New comWrapper
        Dim arguments() As Variant
        arguments = Array(2)
         
        Call ExternalProvidersComWrapper.Create("IO.Practiceware.Presentation.Views.ExternalProviders.ExternalProvidersViewManager", emptyArgs)
        ExternalProvidersComWrapper.InvokeMethod "ShowExternalProviders", arguments
    End If
End Sub

Private Sub cmdReports_Click()
Dim Keep As Boolean
Dim Rec As String
    Keep = True
    While (Keep)
        Call frmSelectDialogue.BuildSelectionDialogue("REPORTS")
        frmSelectDialogue.Show 1
        If (frmSelectDialogue.SelectionResult) Then
            If (frmSelectDialogue.Selection = "Create Report") Then
                frmSetupReports.ReportName = ""
                frmSetupReports.DeleteOn = False
                If (frmSetupReports.LoadReportsDisplay("")) Then
                    frmSetupReports.Show 1
                End If
            Else
                Rec = Trim(Left(frmSelectDialogue.Selection, 55))
                frmSetupReports.ReportName = ""
                frmSetupReports.DeleteOn = CheckConfigCollection("REMOVEACTIVE") = "T"
                If (frmSetupReports.LoadReportsDisplay(Rec)) Then
                    frmSetupReports.Show 1
                End If
            End If
        Else
            Keep = False
        End If
    Wend
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmSchedulerSetup.BorderStyle = 1
    frmSchedulerSetup.ClipControls = True
    frmSchedulerSetup.Caption = Mid(frmSchedulerSetup.Name, 4, Len(frmSchedulerSetup.Name) - 3)
    frmSchedulerSetup.AutoRedraw = True
    frmSchedulerSetup.Refresh
End If
 If UCase(UserLogin.sType) = "O" Then
        cmdCDS.Visible = True
        fpBtnEP.Visible = True
    Else
        cmdCDS.Visible = False
        fpBtnEP.Visible = False
    End If
    fpBtnBG.Visible = True
End Sub
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub

Private Sub fpBtnBG_Click()
Dim objPA As New PracticeActivity
Dim UserId As String
Dim access As Boolean
   UserId = UserLogin.iId
  access = objPA.SetPermission(UserId)
    If (access) Then
        MsgBox "Emergency Access activated. Please note all the actions will be recorded in the AuditLog!", 64, "Emergency Access Alert"
    UserLogin.AuthenticateUser (UserId)
    Else
        MsgBox "You do not have Emergency Access. kindly contact your administrator!", 48, "Emergency Access Alert"
 End If
End Sub

'Private Sub fpBtnCS_Click()
'Dim UserId As String
'   UserId = UserLogin.iId
'   Dim ClinicalMgr As New ClinicalIntegration.frmCheckSum
'  ClinicalMgr.DbObject = IdbConnection
'   ClinicalMgr.ShowDialog
'End Sub


Private Sub fpBtnEP_Click()
Dim UserId As String
   UserId = UserLogin.iId
   Dim ClinicalManager As New ClinicalIntegration.frmPermission
   ClinicalManager.UserId = UserId
   ClinicalManager.DbObject = IdbConnection
   ClinicalManager.ShowDialog
End Sub
