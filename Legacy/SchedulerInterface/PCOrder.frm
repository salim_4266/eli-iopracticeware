VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPCOrder 
   BackColor       =   &H0077742D&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Spectacle Orders"
   ClientHeight    =   8355
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10590
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8355
   ScaleWidth      =   10590
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox lstIns 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "PCOrder.frx":0000
      Left            =   4320
      List            =   "PCOrder.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   71
      Top             =   1560
      Width           =   6135
   End
   Begin VB.ComboBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7560
      Style           =   2  'Dropdown List
      TabIndex        =   69
      Top             =   1200
      Width           =   2895
   End
   Begin VB.ComboBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7560
      Style           =   2  'Dropdown List
      TabIndex        =   67
      Top             =   480
      Width           =   2895
   End
   Begin VB.TextBox txtB 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2160
      MaxLength       =   5
      TabIndex        =   23
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox txtTemple 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7800
      MaxLength       =   5
      TabIndex        =   28
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox txtEye 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5520
      MaxLength       =   5
      TabIndex        =   26
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox txtBridge 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6720
      MaxLength       =   5
      TabIndex        =   27
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox txtDBL 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4440
      MaxLength       =   5
      TabIndex        =   25
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox txtED 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3240
      MaxLength       =   5
      TabIndex        =   24
      Top             =   6720
      Width           =   615
   End
   Begin VB.ComboBox lstTypeOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "PCOrder.frx":0004
      Left            =   960
      List            =   "PCOrder.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   3360
      Width           =   2895
   End
   Begin VB.ComboBox lstBrandOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   4440
      Width           =   2895
   End
   Begin VB.TextBox txtA 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1200
      MaxLength       =   5
      TabIndex        =   22
      Top             =   6720
      Width           =   615
   End
   Begin VB.ListBox lstCoatingOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      ItemData        =   "PCOrder.frx":0008
      Left            =   4080
      List            =   "PCOrder.frx":000A
      MultiSelect     =   1  'Simple
      TabIndex        =   11
      Top             =   3360
      Width           =   3375
   End
   Begin VB.TextBox txtOSBC 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6720
      MaxLength       =   5
      TabIndex        =   17
      Top             =   1200
      Width           =   735
   End
   Begin VB.TextBox txtODBC 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6720
      MaxLength       =   5
      TabIndex        =   7
      Top             =   2640
      Width           =   735
   End
   Begin VB.TextBox txtOS 
      Height          =   285
      Left            =   120
      MaxLength       =   128
      TabIndex        =   12
      Top             =   1920
      Width           =   7335
   End
   Begin VB.TextBox txtOD 
      Height          =   285
      Left            =   120
      MaxLength       =   128
      TabIndex        =   0
      Top             =   480
      Width           =   7335
   End
   Begin VB.TextBox txtSKU 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2040
      TabIndex        =   19
      Top             =   5280
      Width           =   6375
   End
   Begin VB.TextBox txtHeightOD 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      MaxLength       =   5
      TabIndex        =   6
      Top             =   1200
      Width           =   735
   End
   Begin VB.TextBox txtPDNVOD 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2880
      MaxLength       =   5
      TabIndex        =   5
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtPDDVOD 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   960
      MaxLength       =   5
      TabIndex        =   4
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtHeightOS 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      MaxLength       =   5
      TabIndex        =   16
      Top             =   2640
      Width           =   735
   End
   Begin VB.TextBox txtPDNVOS 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2880
      MaxLength       =   5
      TabIndex        =   15
      Top             =   2640
      Width           =   855
   End
   Begin VB.TextBox txtPDDVOS 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   960
      MaxLength       =   5
      TabIndex        =   14
      Top             =   2640
      Width           =   855
   End
   Begin VB.TextBox txtNote 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   3000
      MaxLength       =   250
      MultiLine       =   -1  'True
      TabIndex        =   29
      Top             =   6000
      Width           =   5415
   End
   Begin VB.ComboBox lstSent 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "PCOrder.frx":000C
      Left            =   1200
      List            =   "PCOrder.frx":0016
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   6360
      Width           =   1095
   End
   Begin VB.ComboBox lstOwn 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "PCOrder.frx":0023
      Left            =   1200
      List            =   "PCOrder.frx":002D
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   6000
      Width           =   1095
   End
   Begin VB.TextBox txtNoteOU 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Left            =   7560
      MaxLength       =   250
      MultiLine       =   -1  'True
      TabIndex        =   18
      Top             =   3360
      Width           =   2775
   End
   Begin VB.ComboBox lstLensOS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   2280
      Width           =   6495
   End
   Begin VB.ComboBox lstCutOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "PCOrder.frx":003A
      Left            =   960
      List            =   "PCOrder.frx":0047
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   4800
      Width           =   855
   End
   Begin VB.ComboBox lstMaterialOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   3720
      Width           =   2895
   End
   Begin VB.ComboBox lstPhotoOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "PCOrder.frx":005D
      Left            =   960
      List            =   "PCOrder.frx":0067
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   4080
      Width           =   2895
   End
   Begin VB.ComboBox lstLensOD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   840
      Width           =   6495
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   31
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCOrder.frx":0074
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHold 
      Height          =   975
      Left            =   8520
      TabIndex        =   30
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCOrder.frx":0255
   End
   Begin VB.Label lblIns 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Bill"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   3960
      TabIndex        =   72
      Top             =   1560
      Width           =   270
   End
   Begin VB.Label lblLoc 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   7560
      TabIndex        =   70
      Top             =   960
      Width           =   735
   End
   Begin VB.Label lblDr 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   7560
      TabIndex        =   68
      Top             =   240
      Width           =   705
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Lens Specifications"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   66
      Top             =   3000
      Width           =   2490
   End
   Begin VB.Label Label17 
      BackColor       =   &H0077742D&
      Caption         =   "Tpl"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   7440
      TabIndex        =   65
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label16 
      BackColor       =   &H0077742D&
      Caption         =   "Bgd"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6240
      TabIndex        =   64
      Top             =   6720
      Width           =   375
   End
   Begin VB.Label Label15 
      BackColor       =   &H0077742D&
      Caption         =   "Eye"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   5160
      TabIndex        =   63
      Top             =   6720
      Width           =   495
   End
   Begin VB.Label Label14 
      BackColor       =   &H0077742D&
      Caption         =   "DBL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   3960
      TabIndex        =   62
      Top             =   6720
      Width           =   375
   End
   Begin VB.Label Label13 
      BackColor       =   &H0077742D&
      Caption         =   "ED"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   2880
      TabIndex        =   61
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label12 
      BackColor       =   &H0077742D&
      Caption         =   "B"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   1920
      TabIndex        =   60
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label11 
      BackColor       =   &H0077742D&
      Caption         =   "A"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   840
      TabIndex        =   59
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Brand"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   58
      Top             =   4440
      Width           =   510
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   57
      Top             =   3360
      Width           =   420
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "BC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6240
      TabIndex        =   56
      Top             =   2640
      Width           =   270
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "BC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6240
      TabIndex        =   55
      Top             =   1200
      Width           =   270
   End
   Begin VB.Label lblDesc 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   54
      Top             =   5640
      Width           =   8295
   End
   Begin VB.Label lblOSRx 
      BackColor       =   &H00808000&
      Caption         =   "OS Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   53
      Top             =   1920
      Width           =   7335
   End
   Begin VB.Label lblODRx 
      BackColor       =   &H00808000&
      Caption         =   "OD Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   52
      Top             =   480
      Width           =   7335
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Height"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4200
      TabIndex        =   51
      Top             =   1200
      Width           =   555
   End
   Begin VB.Label lblPDNVOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "PD(NV)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   1920
      TabIndex        =   50
      Top             =   1200
      Width           =   660
   End
   Begin VB.Label lblPDDVOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "PD(DV)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   49
      Top             =   1200
      Width           =   660
   End
   Begin VB.Label lblHeightOS 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Height"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4200
      TabIndex        =   48
      Top             =   2640
      Width           =   555
   End
   Begin VB.Label lblPDNVOS 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "PD(NV)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   1920
      TabIndex        =   47
      Top             =   2640
      Width           =   660
   End
   Begin VB.Label lblPDDVOS 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "PD(DV)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   46
      Top             =   2640
      Width           =   660
   End
   Begin VB.Label lblSent 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Sent Frame"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   45
      Top             =   6360
      Width           =   1020
   End
   Begin VB.Label lblNotes 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Notes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   2400
      TabIndex        =   44
      Top             =   6000
      Width           =   510
   End
   Begin VB.Label lblOwn 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Own Frame"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   43
      Top             =   6000
      Width           =   1005
   End
   Begin VB.Label lblMan 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   1320
      TabIndex        =   42
      Top             =   5280
      Width           =   675
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Frame"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   41
      Top             =   5160
      Width           =   825
   End
   Begin VB.Label lblNoteOS 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Notes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   7560
      TabIndex        =   40
      Top             =   3120
      Width           =   510
   End
   Begin VB.Label lblLensOS 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "V-Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   39
      Top             =   2280
      Width           =   645
   End
   Begin VB.Label lblCutOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Uncut"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   38
      Top             =   4800
      Width           =   510
   End
   Begin VB.Label lblMaterialOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Material"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   37
      Top             =   3720
      Width           =   690
   End
   Begin VB.Label lblCoatingOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Coating"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4080
      TabIndex        =   36
      Top             =   3120
      Width           =   660
   End
   Begin VB.Label lblPhotoOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Options"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   35
      Top             =   4080
      Width           =   675
   End
   Begin VB.Label lblLensOD 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "V-Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   34
      Top             =   840
      Width           =   645
   End
   Begin VB.Label lblEye 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "OD Lens"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   33
      Top             =   120
      Width           =   1140
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "OS Lens"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   32
      Top             =   1560
      Width           =   1125
   End
End
Attribute VB_Name = "frmPCOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public ClinicalId As Long
Public ODRx As String
Public OSRx As String
Public DemoOn As Boolean
Public OrderId As Long

Private Const MaxCoatsAllowed As Integer = 5
Private EditOn As Boolean
Private ReceivableId As Long
Private InventoryIdFrames As Long
Private InventoryLensOD As String
Private InventoryLensOS As String

Private Sub cmdHome_Click()
Unload frmPCOrder
End Sub

Private Sub cmdHold_Click()
If Not (DemoOn) Then
    If (Validate) Then
        If (EditOn) Then
            Call PostOrders(False, False)
            frmEventMsgs.Header = "Do you want to edit this claim ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
'''''                frmPayments.CurrentAction = ""
'''''                frmPayments.PatientId = PatientId
'''''                frmPayments.ReceivableId = ReceivableId
'''''                If (frmPayments.LoadPayments(False, "")) Then
'''''                    frmPayments.Show 1
'''''                End If
                    Dim PatientDemographics As New PatientDemographics
                    PatientDemographics.PatientId = PatientId
                    Call PatientDemographics.DisplayPatientFinancialScreen
                    Set PatientDemographics = Nothing
            End If
            'Call PostOrders(True, False)
            Unload frmPCOrder
        Else
            Call PostOrders(True, False)
            If (OrderId > 0) Then
                If (FillOrder(True, 10)) Then
                    Call PayOrder(OrderId)
                End If
                Call BillOrder(OrderId)
                Unload frmPCOrder
            End If
        End If
    End If
ElseIf (Not (EditOn)) Then
    If (Validate) Then
        If (AppointmentId < 1) And (ClinicalId < 1) Then
            If (SetupManual(txtOD.Text, txtOS.Text, AppointmentId, ClinicalId)) Then
                Call PostOrders(True, False)
                If (OrderId > 0) Then
                    If (FillOrder(True, 10)) Then
                        Call PayOrder(OrderId)
                        Call BillOrder(OrderId)
                        Unload frmPCOrder
                    End If
                End If
            End If
        Else
            If (SetupManual(lblODRx.Caption, lblOSRx.Caption, AppointmentId, ClinicalId)) Then
                Call PostOrders(True, False)
                If (OrderId > 0) Then
                    If (FillOrder(True, 10)) Then
                        Call PayOrder(OrderId)
                        Call BillOrder(OrderId)
                        Unload frmPCOrder
                    End If
                End If
            End If
        End If
    End If
End If
End Sub

Private Sub lblDesc_Click()
frmPCNew.OrderType = "G"
frmPCNew.InventoryId = 0
frmPCNew.Show 1
If (frmPCNew.SKUNew <> "") Then
    txtSKU.Text = frmPCNew.SKUNew
    Call txtSKU_KeyPress(13)
End If
End Sub

Private Sub txtSKU_KeyPress(KeyAscii As Integer)
Dim Temp As String
Dim ATemp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtSKU.Text)
    InventoryIdFrames = GetFrames(Temp, ATemp)
    If (InventoryIdFrames > 0) Then
        lblDesc.Caption = ATemp
    Else
        lblDesc.Caption = ""
        txtSKU.Text = ""
        If Not Temp = "" Then
            frmEventMsgs.Header = "Entry does not match inventory"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            
        End If
    End If
End If
End Sub

Private Sub txtSKU_Validate(Cancel As Boolean)
Call txtSKU_KeyPress(13)
End Sub

Private Sub Form_Load()
Dim i As Integer
Dim PatId As Long
Dim l1 As String, l2 As String, l3 As String
Dim PolicyHolderid As Long, SecondPolicyHolderId As Long
Dim b1 As Boolean, b2 As Boolean
Dim n1 As String, TheDate As String
Dim r1 As String, r2 As String
Dim DisplayDr As String
Dim RetPat As Patient
Dim RetRes As SchedulerResource
Dim RetPrc As PracticeName
Dim ApplTemp As ApplicationTemplates
EditOn = False
InventoryIdFrames = 0
lstDr.Clear
Set RetRes = New SchedulerResource
RetRes.ResourceName = Chr(1)
RetRes.ResourceType = "Q"
If (RetRes.FindResource > 0) Then
    i = 1
    While (RetRes.SelectResource(i))
        DisplayDr = Space(75)
        Mid(DisplayDr, 1, Len(RetRes.ResourceName)) = RetRes.ResourceName
        DisplayDr = DisplayDr + Trim(Str(RetRes.ResourceId))
        lstDr.AddItem DisplayDr
        i = i + 1
    Wend
End If

lstLoc.Clear
Set RetRes = Nothing
Set RetPrc = New PracticeName
RetPrc.PracticeType = "P"
RetPrc.PracticeName = Chr(1)
If (RetPrc.FindPractice > 0) Then
    i = 1
    While (RetPrc.SelectPractice(i))
        DisplayDr = Space(75)
        If (Trim(RetPrc.PracticeLocationReference) = "") Then
            Mid(DisplayDr, 1, 6) = "Office"
            DisplayDr = DisplayDr + Trim(Str(0))
        Else
            Mid(DisplayDr, 1, Len(RetPrc.PracticeLocationReference)) = RetPrc.PracticeLocationReference
            DisplayDr = DisplayDr + Trim(Str(RetPrc.PracticeId + 1000))
        End If
        lstLoc.AddItem DisplayDr
        i = i + 1
    Wend
End If
Set RetPrc = Nothing

Set RetPat = New Patient
RetPat.PatientId = PatientId
If (RetPat.RetrievePatient) Then
    PolicyHolderid = RetPat.PolicyPatientId
    SecondPolicyHolderId = RetPat.SecondPolicyPatientId
End If
Set RetPat = Nothing
lstIns.Clear
TheDate = ""
Call FormatTodaysDate(TheDate, False)
TheDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
lstIns.Clear
lstIns.AddItem "Patient"
TheDate = ""
Call FormatTodaysDate(TheDate, False)
TheDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
Set ApplTemp = New ApplicationTemplates
Call ApplTemp.ApplGetAllPayers(PatientId, n1, PolicyHolderid, b1, r1, SecondPolicyHolderId, b2, r2, "", TheDate, lstIns, l1, l2, l3, True, False, True, True)
Set ApplTemp = Nothing
For i = lstIns.ListCount - 1 To 0 Step -1
    lstIns.List(i + 1) = lstIns.List(i)
Next i
If (lstIns.ListCount < 1) Then
    lstIns.AddItem "Patient"
Else
    lstIns.List(0) = "Patient"
End If
If (lstIns.ListCount >= 0) Then
    lstIns.ListIndex = 0
End If
lstIns.AddItem "Do not bill"

Call LoadLensComponents
lblODRx.Caption = ODRx
lblOSRx.Caption = OSRx
lblODRx.Visible = True
lblOSRx.Visible = True
txtOD.Visible = False
txtOS.Visible = False
'----------
'lstDr.Visible = True
'lblDr.Visible = True
'lstLoc.Visible = True
'lblLoc.Visible = True

lstDr.Visible = False
lblDr.Visible = False
lstLoc.Visible = False
lblLoc.Visible = False
'----------
If (DemoOn) Then
    If (Trim(lblODRx.Caption) = "") And (Trim(lblOSRx.Caption) = "") Then
        lblODRx.Visible = False
        lblOSRx.Visible = False
        txtOD.Visible = True
        txtOD.Text = ""
        txtOS.Visible = True
        txtOS.Text = ""
    Else
        txtOD.Text = ""
        txtOS.Text = ""
    End If
    lstDr.Visible = True
    lblDr.Visible = True
    lstLoc.Visible = True
    lblLoc.Visible = True
ElseIf (OrderId > 0) Then
    Call LoadActiveOrder(OrderId)
    EditOn = True
End If
End Sub

Private Sub LoadLensComponents()
Dim i As Integer
Dim Temp As String
Dim RetCode As PracticeCodes
lstLensOD.Clear
lstLensOD.AddItem "None"
lstLensOS.Clear
lstLensOS.AddItem "None"
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "PCVCode"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Temp = Space(40)
        Mid(Temp, 1, Len(Trim(RetCode.ReferenceCode))) = Trim(RetCode.ReferenceCode)
        Temp = Temp + Trim(RetCode.ReferenceAlternateCode)
        lstLensOD.AddItem Temp
        lstLensOS.AddItem Temp
        i = i + 1
    Wend
End If
Set RetCode = Nothing

lstMaterialOD.Clear
lstMaterialOD.AddItem "None"
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "PCMaterial"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Temp = Space(40)
        Mid(Temp, 1, Len(Trim(RetCode.ReferenceCode))) = Trim(RetCode.ReferenceCode)
        Temp = Temp + Trim(RetCode.ReferenceAlternateCode)
        lstMaterialOD.AddItem Temp
        i = i + 1
    Wend
End If
Set RetCode = Nothing

lstCoatingOD.Clear
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "PCCoating"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Temp = Space(40)
        Mid(Temp, 1, Len(Trim(RetCode.ReferenceCode))) = Trim(RetCode.ReferenceCode)
        Temp = Temp + Trim(RetCode.ReferenceAlternateCode)
        lstCoatingOD.AddItem Temp
        i = i + 1
    Wend
End If
Set RetCode = Nothing

lstPhotoOD.Clear
lstPhotoOD.AddItem "None"
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "PCOptions"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Temp = Space(40)
        Mid(Temp, 1, Len(Trim(RetCode.ReferenceCode))) = Trim(RetCode.ReferenceCode)
        Temp = Temp + Trim(RetCode.ReferenceAlternateCode)
        lstPhotoOD.AddItem Temp
        i = i + 1
    Wend
End If
Set RetCode = Nothing

lstBrandOD.Clear
lstBrandOD.AddItem "None"
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "PCBrand"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Temp = Space(40)
        Mid(Temp, 1, Len(Trim(RetCode.ReferenceCode))) = Trim(RetCode.ReferenceCode)
        Temp = Temp + Trim(RetCode.ReferenceAlternateCode)
        lstBrandOD.AddItem Temp
        i = i + 1
    Wend
End If
Set RetCode = Nothing

lstTypeOD.Clear
lstTypeOD.AddItem "None"
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "PCType"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Temp = Space(40)
        Mid(Temp, 1, Len(Trim(RetCode.ReferenceCode))) = Trim(RetCode.ReferenceCode)
        Temp = Temp + Trim(RetCode.ReferenceAlternateCode)
        lstTypeOD.AddItem Temp
        i = i + 1
    Wend
End If
Set RetCode = Nothing

txtNoteOU.Text = ""
txtPDDVOD.Text = ""
txtPDNVOD.Text = ""
txtHeightOD.Text = ""
lstCutOD.Clear
lstCutOD.AddItem "Yes"
lstCutOD.AddItem "No"

' Frames
txtSKU.Text = ""
lstOwn.Clear
lstOwn.AddItem "Yes"
lstOwn.AddItem "No"
lstSent.Clear
lstSent.AddItem "Yes"
lstSent.AddItem "No"
End Sub

Private Function Validate() As Boolean
Validate = False
If lstDr.Visible Then
    If lstDr.Text = "" Then
        frmEventMsgs.Header = "Select glasses supplier"
        GoTo Invalid
    End If
    If lstLoc.Text = "" Then
        frmEventMsgs.Header = "Select location"
        GoTo Invalid
    End If
End If
Validate = True
Exit Function

Invalid:
If Not (Validate) Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Function

Private Function GetFrames(ASku As String, Desc As String) As Long
Dim Temp As String
Dim RetPC As PCInventory
GetFrames = 0
If (Trim(ASku) <> "") Then
    Set RetPC = New PCInventory
    RetPC.Criteria = "SKU = '" + ASku + "' "
    If (RetPC.FindPCInventory) Then
        Call DisplayDollarAmount(Trim(Str(RetPC.Cost)), Temp)
        If (Trim(RetPC.Type_) <> "") Then
            Desc = Trim(RetPC.Type_)
        End If
        If (Trim(RetPC.Model) <> "") Then
            Desc = Desc + ",  " + Trim(RetPC.Model)
        End If
        If (Trim(RetPC.PCColor) <> "") Then
            Desc = Desc + ",  " + Trim(RetPC.PCColor)
        End If
        If (Trim(Temp) <> "") Then
            Desc = Desc + ",  $" + Trim(Temp)
        End If
        GetFrames = RetPC.InventoryId
    End If
    Set RetPC = Nothing
End If
End Function

Private Function GetLensComponents(AEye As String) As String
Dim u As Integer
Dim w As Integer
Dim ADesc As String
Dim MyText As String
GetLensComponents = ""
MyText = ""
If (AEye = "OD") Or (AEye = "OS") Then
    If (AEye = "OD") Then
        If (Trim(lstLensOD.Text) <> "") And Trim$(lstLensOD.Text) <> "None" Then
            MyText = AEye + "L:" + Mid(lstLensOD.Text, 41, Len(lstLensOD.Text) - 40)
            ADesc = ADesc + MyText + "&"
        Else
            ADesc = ADesc + "&"
        End If
    ElseIf (AEye = "OS") Then
        If (Trim(lstLensOS.Text) <> "") And Trim$(lstLensOS.Text) <> "None" Then
            MyText = AEye + "L:" + Mid(lstLensOS.Text, 41, Len(lstLensOS.Text) - 40)
            ADesc = ADesc + MyText + "&"
        Else
            ADesc = ADesc + "&"
        End If
    End If
    If (Trim(lstCoatingOD.Text) <> "") Then
        w = 0
        MyText = ""
        For u = 0 To lstCoatingOD.ListCount - 1
            If (lstCoatingOD.Selected(u)) Then
                w = w + 1
                If (w < MaxCoatsAllowed) Then
                    MyText = MyText + Mid(lstCoatingOD.List(u), 41, Len(lstCoatingOD.List(u)) - 40) + ","
                End If
            End If
        Next u
        If (Trim(MyText) <> "") Then
            MyText = AEye + "C:" + MyText
            ADesc = ADesc + MyText + "&"
        Else
            ADesc = ADesc + "&"
        End If
    End If
    If (Trim(lstMaterialOD.Text) <> "") And (lstMaterialOD.ListIndex > 0) Then
        MyText = AEye + "M:" + Mid(lstMaterialOD.Text, 41, Len(lstMaterialOD.Text) - 40)
        ADesc = ADesc + MyText + "&"
    Else
        ADesc = ADesc + "&"
    End If
    If (Trim(lstPhotoOD.Text) <> "") And (lstPhotoOD.ListIndex > 0) Then
        MyText = AEye + "P:" + Mid(lstPhotoOD.Text, 41, Len(lstPhotoOD.Text) - 40)
        ADesc = ADesc + MyText + "&"
    Else
        ADesc = ADesc + "&"
    End If
    If (Trim(lstBrandOD.Text) <> "") And (lstBrandOD.ListIndex > 0) Then
        MyText = AEye + "B:" + Mid(lstBrandOD.Text, 41, Len(lstBrandOD.Text) - 40)
        ADesc = ADesc + MyText + "&"
    Else
        ADesc = ADesc + "&"
    End If
    If (Trim(lstTypeOD.Text) <> "") And (lstTypeOD.ListIndex > 0) Then
        MyText = AEye + "T:" + Mid(lstTypeOD.Text, 41, Len(lstTypeOD.Text) - 40)
        ADesc = ADesc + MyText + "&"
    Else
        ADesc = ADesc + "&"
    End If
End If
GetLensComponents = ADesc
End Function

Private Sub PostOrders(HoldOn As Boolean, FillOn As Boolean)
Dim u As Integer
Dim ClnId As Long
Dim Temp As String
Dim AEye As String
Dim ADate As String
Dim ANote As String
Dim ClnText As String
Dim InvCost As Single
Dim NoteId As Long
Dim InvId As Long
Dim CLOrder As CLOrders
Dim RetNote As PatientNotes
Dim RetCln As PatientClinical
Set CLOrder = New CLOrders
InvId = InventoryIdFrames
InvCost = 0
If (OrderId > 0) Then
' Set any Changes to the Prescription
    ClnId = 0
    If (Trim(txtOD.Tag) <> "") Then
        ClnId = val(Trim(txtOD.Tag))
    ElseIf (Trim(txtOS.Tag) <> "") Then
        ClnId = val(Trim(txtOS.Tag))
    End If
    If (ClnId > 0) Then
        ClnText = "DISPENSE SPECTACLE RX-9/-1/-2/" + Trim(txtOD.Text) + "-3/" + Trim(txtOS.Text) + "-4/-5/0-6/0-7/"
        Set RetCln = New PatientClinical
        RetCln.ClinicalId = ClnId
        If (RetCln.RetrievePatientClinical) Then
            RetCln.Findings = ClnText
            Call RetCln.ApplyPatientClinical
        End If
        Set RetCln = Nothing
    End If

' Set Order
    CLOrder.Criteria = "OrderID = " + CStr(OrderId)
    CLOrder.FindPCOrders
    CLOrder.Qty = 1
    CLOrder.OrderType = "G"
    CLOrder.AppointmentId = AppointmentId
    CLOrder.InventoryId = InventoryIdFrames
    CLOrder.ShipAddress1 = GetLensComponents("OD")
    CLOrder.ShipAddress2 = GetLensComponents("OS")
    CLOrder.ShipCity = "OD:" + Trim(txtPDDVOD.Text) + "&" + Trim(txtPDNVOD.Text) + "&" + Trim(txtHeightOD.Text) + "&" + Trim(txtODBC.Text) _
                     + "OS:" + Trim(txtPDDVOS.Text) + "&" + Trim(txtPDNVOS.Text) + "&" + Trim(txtHeightOS.Text) + "&" + Trim(txtOSBC.Text)
    Temp = "00"
    If (lstCutOD.ListIndex = 1) Then
        Mid(Temp, 1, 1) = "1"
    End If
    CLOrder.ShipState = Temp
    Temp = "00"
    If (lstOwn.ListIndex = 1) Then
        Mid(Temp, 1, 1) = "1"
    End If
    If (lstSent.ListIndex = 1) Then
        Mid(Temp, 2, 1) = "1"
    End If
    Temp = Temp + Trim(txtA.Text) + "&" _
         + Trim(txtB.Text) + "&" _
         + Trim(txtED.Text) + "&" _
         + Trim(txtDBL.Text) + "&" _
         + Trim(txtEye.Text) + "&" _
         + Trim(txtBridge.Text) + "&" _
         + Trim(txtTemple.Text) + "&"
    CLOrder.ShipZip = "&" + Temp
    If (FillOn) Then
        Temp = ""
        Call FormatTodaysDate(Temp, False)
        Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
        CLOrder.OrderComplete = Temp
    End If
    If (FillOn) Then
        CLOrder.Status = 1
    End If
    CLOrder.PutCLOrder
    Set RetNote = New PatientNotes
    RetNote.NotesSystem = "PCO" + Trim(Str(OrderId))
    If (RetNote.FindNotes > 0) Then
        u = 1
        While RetNote.SelectNotes(u)
            Call RetNote.DeleteNotes
            u = u + 1
        Wend
    End If
    Set RetNote = Nothing
    If (Trim(txtNoteOU.Text) <> "") Then
        AEye = "OU"
        NoteId = val(Trim(txtNoteOU.Tag))
        ANote = Trim(txtNoteOU.Text)
        GoSub PostNotes
    End If
    If (Trim(txtNote.Text) <> "") Then
        AEye = ""
        NoteId = val(Trim(txtNote.Tag))
        ANote = Trim(txtNote.Text)
        GoSub PostNotes
    End If
    ReceivableId = CLOrder.ReceivableId
' Update Claim --- dont do this anymore
'    Call UpdateClaim(CLOrder.ReceivableId, InvId, Trim(CLOrder.ShipAddress1), Trim(CLOrder.ShipAddress2))
Else
    CLOrder.Criteria = "OrderID = 0"
    CLOrder.FindPCOrders
    CLOrder.OrderType = "G"
    CLOrder.Eye = "OU"
    CLOrder.Qty = 1
    CLOrder.Cost = InvCost
    CLOrder.PatientId = PatientId
    CLOrder.AppointmentId = AppointmentId
    CLOrder.ShipTo = "O"
    CLOrder.InventoryId = InventoryIdFrames
    CLOrder.ShipAddress1 = GetLensComponents("OD")
    CLOrder.ShipAddress2 = GetLensComponents("OS")
    CLOrder.ShipCity = "OD:" + Trim(txtPDDVOD.Text) + "&" + Trim(txtPDNVOD.Text) + "&" + Trim(txtHeightOD.Text) + "&" + Trim(txtODBC.Text) _
                     + "OS:" + Trim(txtPDDVOS.Text) + "&" + Trim(txtPDNVOS.Text) + "&" + Trim(txtHeightOS.Text) + "&" + Trim(txtOSBC.Text)
    Temp = "00"
    If (lstCutOD.ListIndex = 0) Then
        Mid(Temp, 1, 1) = "1"
    End If
    CLOrder.ShipState = Temp
    Temp = "11"
    If (lstOwn.ListIndex = 0) Then
        Mid(Temp, 1, 1) = "0"
    End If
    If (lstSent.ListIndex = 0) Then
        Mid(Temp, 2, 1) = "0"
    End If
    Temp = Temp + Trim(txtA.Text) + "&" _
         + Trim(txtB.Text) + "&" _
         + Trim(txtED.Text) + "&" _
         + Trim(txtDBL.Text) + "&" _
         + Trim(txtEye.Text) + "&" _
         + Trim(txtBridge.Text) + "&" _
         + Trim(txtTemple.Text) + "&"
    CLOrder.ShipZip = "&" + Temp
    Temp = ""
    Call FormatTodaysDate(Temp, False)
    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
    CLOrder.OrderDate = Temp
    CLOrder.Status = 10
    CLOrder.OrderComplete = ""
    If (FillOn) Then
        CLOrder.Status = 1
        CLOrder.OrderComplete = Temp
    End If
    CLOrder.PutCLOrder
    OrderId = CLOrder.OrderId
    If (Trim(txtNoteOU.Text) <> "") Then
        AEye = "OU"
        NoteId = 0
        ANote = Trim(txtNoteOU.Text)
        GoSub PostNotes
    End If
    If (Trim(txtNote.Text) <> "") Then
        AEye = ""
        NoteId = 0
        ANote = Trim(txtNote.Text)
        GoSub PostNotes
    End If
End If
Set CLOrder = Nothing
Exit Sub
PostNotes:
    If (ANote <> "") And (OrderId > 0) Then
        ADate = ""
        Call FormatTodaysDate(ADate, False)
        ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        Set RetNote = New PatientNotes
        RetNote.NotesId = NoteId
        If (RetNote.RetrieveNotes) Then
            RetNote.NotesAudioOn = False
            RetNote.NotesAlertMask = ""
            RetNote.NotesCategory = ""
            RetNote.NotesClaimOn = False
            RetNote.NotesCommentOn = False
            RetNote.NotesEye = AEye
            RetNote.NotesHighlight = ""
            RetNote.NotesILPNRef = ""
            RetNote.NotesOffDate = ""
            RetNote.NotesPatientId = PatientId
            RetNote.NotesUser = Str$(UserLogin.iId)
            RetNote.NotesDate = ADate
            RetNote.NotesType = "C"
            RetNote.NotesText1 = Trim(ANote)
            RetNote.NotesText2 = ""
            RetNote.NotesText3 = ""
            RetNote.NotesText4 = ""
            RetNote.NotesAppointmentId = AppointmentId
            RetNote.NotesSystem = "PCO" + Trim$(Str$(OrderId))
            Call RetNote.ApplyNotes
        End If
        Set RetNote = Nothing
    End If
    Return
End Sub

Private Function PayOrder(OrderId As Long) As Boolean
Dim RcvId As Long
Dim RetrieveCLOrders As CLOrders
PayOrder = False
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
    RetrieveCLOrders.OrderBy = "OrderID"
    RetrieveCLOrders.FindPCOrders
    RcvId = RetrieveCLOrders.ReceivableId
    Set RetrieveCLOrders = Nothing
    If (RcvId > 0) Then
        PayOrder = True
        frmQuickPay.OrderType = "G"
        If (frmQuickPay.LoadPayments(RcvId)) Then
            frmQuickPay.Show 1
        End If
        frmQuickPay.OrderType = ""
    End If
End If
End Function

Private Function BillOrder(OrderId As Long) As Boolean
If lstIns.ListIndex = lstIns.ListCount - 1 Then
    Exit Function
End If

Dim RcvId As Long
Dim RetrieveCLOrders As CLOrders
BillOrder = False
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
    RetrieveCLOrders.OrderBy = "OrderID"
    RetrieveCLOrders.FindPCOrders
    RcvId = RetrieveCLOrders.ReceivableId
    Set RetrieveCLOrders = Nothing
    If (lstIns.ListIndex = 0) Then
        Call BillHow(RcvId, "P", "", 0)
    ElseIf (lstIns.ListIndex > 0) Then
        Call BillHow(RcvId, "I", "", 0)
    End If
End If
End Function

Private Function BillHow(RecId As Long, Whom As String, TheAmt As String, SrvItem As Long) As Boolean
Dim z As Integer, ATmpId As Long
Dim p1 As Long, IPlan As Long, ApptId As Long
Dim AllowZero As Boolean
Dim InsThere As Boolean, ProcessHow As Boolean
Dim Temp As String, InvId As String, IDate As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
BillHow = True
ProcessHow = False
AllowZero = False
frmEventMsgs.Header = ""
Set ApplTemp = New ApplicationTemplates
Set ApplList = New ApplicationAIList
' Get The Invoice
Call ApplTemp.ApplGetInvoice(RecId, InvId, IDate, IPlan, ApptId)
If (Whom = "I") Then
    AllowZero = CheckConfigCollection("RPTPATPAY") = "T"
    If (ApplTemp.ApplIsReceivablePlanSet(RecId, ATmpId)) Then
' restricted, bypass except where noted
        z = ApplTemp.ApplIsInvoiceVerified(InvId)
        If (z = -1) Then
            frmEventMsgs.Header = "Primary Insurer needs to be correctly set. Set Insurer."
            z = 0
            frmEventMsgs.Header = ""
        ElseIf (z = -2) Then
            frmEventMsgs.Header = "Secondary Insurer needs to be correctly set. Set Insurer."
            z = 0
            frmEventMsgs.Header = ""
        ElseIf (z = -3) Then
            frmEventMsgs.Header = "Third Insurer needs to be correctly set. Set Insurer."
            z = 0
            frmEventMsgs.Header = ""
        ElseIf (z = -99) Then
            frmEventMsgs.Header = "Patient has no Financial records."
        End If
        If (z = 0) Then
            If Not (ApplTemp.ApplIsReceivableVerified(RecId, "I")) Then
                frmEventMsgs.Header = "Incorrect Insurer, Rebill and choose new payer."
            End If
        End If
    Else
        frmEventMsgs.Header = "Insurer is not set, Set Insurer"
    End If
Else
'    If Not (ApplTemp.ApplIsReceivableVerified(RecId, "P")) Then
'        frmEventMsgs.Header = "Incorrect Insurer, Rebill and choose new payer."
'    End If
End If
If (Whom = "I") Then
    Temp = ApplTemp.ApplPatientLevelVerification(RecId)
    If (Trim(Temp) <> "") Then
        frmEventMsgs.Header = Temp
    End If
    Temp = ApplTemp.ApplDiagnosisLevelVerification(RecId)
    If (Trim(Temp) <> "") Then
        frmEventMsgs.Header = Temp
    End If
' Verify the UPin
    If Not (ApplTemp.ApplIsUPinValid(RecId)) Then
        Temp = "Upin is not valid"
        frmEventMsgs.Header = Temp
    End If
End If
If (Trim(frmEventMsgs.Header) <> "") Then
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    BillHow = False
Else
    If (Whom = "I") Then
        ProcessHow = True
        If Not (ApplTemp.ApplIsInsurerPrimary(RecId)) Then
            If (ApplTemp.ApplIsInsurerPlanPaper(IPlan)) Then
                ProcessHow = False
            End If
        End If
    End If
    If (ProcessHow) Then
        If (ApplTemp.ApplIsElectronic(RecId)) Then
            If (ApplTemp.ApplIsElectronicValid(RecId)) Then
                p1 = ApplTemp.BatchTransaction(RecId, Whom, TheAmt, "B", True, SrvItem, False, False, AllowZero)
                If (p1 < 1) Then
                    BillHow = False
                End If
            Else
                BillHow = False
                frmEventMsgs.Header = "Insurer needs a valid NEIC Number"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
        Else
            p1 = ApplTemp.BatchTransaction(RecId, Whom, TheAmt, "P", True, SrvItem, False, False, AllowZero)
            If (p1 < 1) Then
                BillHow = False
            End If
        End If
    Else
        p1 = ApplTemp.BatchTransaction(RecId, Whom, TheAmt, "P", True, SrvItem, False, False, AllowZero)
        If (p1 < 1) Then
            BillHow = False
        End If
    End If
    Call ApplList.ApplSetReceivableBalance(InvId, "", True)
End If
Set ApplList = Nothing
Set ApplTemp = Nothing
End Function

Private Function FillOrder(OrderOn As Boolean, AStat As Integer) As Boolean
Dim Action As Boolean
Dim PatId As Long
Dim RcvId As Long
Dim ApptId As Long
Dim Temp As String
Dim RetRcv As PatientReceivables
Dim RetrieveCLOrders As CLOrders
Dim RetrievePCInventory As PCInventory
FillOrder = False
Action = False
If (OrderId > 0) Then
    Set RetrieveCLOrders = New CLOrders
    RetrieveCLOrders.Criteria = "OrderID = " + Trim(Str(OrderId)) + " "
    RetrieveCLOrders.OrderBy = "OrderID"
    RetrieveCLOrders.FindPCOrders
    ApptId = RetrieveCLOrders.AppointmentId
    GoSub PostSrvs
    Set RetrieveCLOrders = Nothing
End If

If (Action) Then
    FillOrder = True
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        PatId = RetRcv.PatientId
    End If
    Set RetRcv = Nothing
    Dim PatientDemographics As New PatientDemographics
    PatientDemographics.PatientId = PatId
    Call PatientDemographics.DisplayInvoiceEditAndBillScreen(RcvId)
End If
Exit Function

PostSrvs:
RcvId = CreateManualClaim(RetrieveCLOrders.PatientId, RetrieveCLOrders.OrderDate, RetrieveCLOrders.AppointmentId, RetrieveCLOrders.InventoryId, RetrieveCLOrders.ShipAddress1, RetrieveCLOrders.ShipAddress2)
If (RcvId > 0) Then
'==========================
    Set RetrievePCInventory = New PCInventory
    RetrievePCInventory.Criteria = "InventoryID = " + Trim(Str(RetrieveCLOrders.InventoryId))
    RetrievePCInventory.FindPCInventory
    If (RetrievePCInventory.SelectPCInventory(1)) Then
        If (lstOwn.Text <> "Yes") Then
            RetrievePCInventory.Qty = RetrievePCInventory.Qty - RetrieveCLOrders.Qty
            RetrievePCInventory.PutPCInventory
        End If
    End If
    Set RetrievePCInventory = Nothing
'==========================
    RetrieveCLOrders.Status = AStat
    RetrieveCLOrders.ReceivableId = RcvId
    If Not (OrderOn) Then
        RetrieveCLOrders.Status = 5
        Temp = ""
        Call FormatTodaysDate(Temp, False)
        Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
        RetrieveCLOrders.OrderComplete = Temp
    End If
    RetrieveCLOrders.PutCLOrder
    Action = True
End If
Return
End Function

Private Function CreateManualClaim(PatId As Long, ApptDate As String, ApptId As Long, InvId As Long, ODOrderDetails As String, OSOrderDetails As String) As Long
Dim ADate As String
Dim RcvId1 As Long
CreateManualClaim = 0
If (PatId > 0) And (Trim(ApptDate) <> "") Then
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
    If (SetupReceivableAndService(ApptId, RcvId1, ADate, InvId, ODOrderDetails, OSOrderDetails)) Then
        CreateManualClaim = RcvId1
    End If
End If
End Function

Private Function SetupReceivableAndService(ApptId As Long, RcvId1 As Long, AptDate As String, InvId As Long, ODDetails As String, OSDetails As String) As Boolean
Dim MyCnt As Integer
Dim i As Integer, j As Integer
Dim u As Integer, p As Integer
Dim LcId As Long, DrId As Long
Dim Copay As Single, ACost As Single
Dim ApptIdNew As Long
Dim InsurerId As Long, InsuredId As Long
Dim myFilter As String
Dim mySrv As String, AEye As String
Dim ADesc As String, ASrv As String
Dim InsType As String, InvoiceId As String
Dim ApplClaim As ApplicationClaims
Dim ApplSch As ApplicationScheduler
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim RetrieveClinical As PatientClinical
Dim RetrieveService As PatientReceivableService
Dim RetRcv As PatientReceivables
Dim RetAppt As SchedulerAppointment
Dim RetPC As PCInventory
SetupReceivableAndService = False
InsType = ""
InvoiceId = ""
Copay = 0
RcvId1 = 0
ApptIdNew = 0
InsurerId = 0
InsuredId = 0
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        DrId = RetAppt.AppointmentResourceId1
        If (DrId = 0) Then
            DrId = GetOpticalDoctor
        End If
        LcId = RetAppt.AppointmentResourceId2
        If (LcId = 0) Then
            LcId = GetOpticalPractice
        End If
        Set ApplClaim = New ApplicationClaims
        If (lstIns.ListIndex > 0) Then
            InsType = Mid(lstIns.List(lstIns.ListIndex), 53, 1)
        ElseIf (lstIns.ListIndex = 0) Then
            InsType = "V"
        Else
            InsType = RetAppt.AppointmentInsType
        End If
        Set ApplList = New ApplicationAIList
        Call ApplList.ApplGetInsurance(RetAppt.AppointmentPatientId, RetAppt.AppointmentDate, InsuredId, InsurerId, Copay, InsType, False)
        If (InsurerId < 1) Then
            InsType = "V"
            Call ApplList.ApplGetInsurance(RetAppt.AppointmentPatientId, RetAppt.AppointmentDate, InsuredId, InsurerId, Copay, InsType, False)
            If (InsurerId < 1) Then
                InsType = "M"
                Call ApplList.ApplGetInsurance(RetAppt.AppointmentPatientId, RetAppt.AppointmentDate, InsuredId, InsurerId, Copay, InsType, False)
                If (InsurerId < 1) Then
                    InsType = "A"
                    Call ApplList.ApplGetInsurance(RetAppt.AppointmentPatientId, RetAppt.AppointmentDate, InsuredId, InsurerId, Copay, InsType, False)
                    If (InsurerId < 1) Then
                        InsType = "W"
                        Call ApplList.ApplGetInsurance(RetAppt.AppointmentPatientId, RetAppt.AppointmentDate, InsuredId, InsurerId, Copay, InsType, False)
                    End If
                End If
            End If
        End If
        Set ApplList = Nothing
        ApptIdNew = ApplClaim.BuildAppointment(RetAppt.AppointmentPatientId, DrId, LcId, RetAppt.AppointmentTypeId, AptDate, InsType, RetAppt.AppointmentReferralId, RetAppt.AppointmentPreCertId, True, True)
        If (ApptIdNew > 0) Then
            Set RetAppt = Nothing
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = ApptIdNew
            Call RetAppt.RetrieveSchedulerAppointment
            Call ApplClaim.BuildReceivable(RcvId1, RetAppt.AppointmentPatientId, InsuredId, InsurerId, 0, RetAppt.AppointmentResourceId1, ApptIdNew, RetAppt.AppointmentDate, "", "", "", "", "", "", "", "", "", "", InvoiceId, LcId, "I", False)
            If (RcvId1 > 0) Then
                Set RetrieveClinical = New PatientClinical
                RetrieveClinical.ClinicalId = 0
                Call RetrieveClinical.RetrievePatientClinical
                RetrieveClinical.ClinicalType = "K"
                RetrieveClinical.AppointmentId = ApptIdNew
                RetrieveClinical.PatientId = RetAppt.AppointmentPatientId
                RetrieveClinical.EyeContext = ""
                RetrieveClinical.ImageDescriptor = ""
                RetrieveClinical.ImageInstructions = ""
                RetrieveClinical.Findings = "367.9"
                RetrieveClinical.Symptom = "1"
                Call RetrieveClinical.ApplyPatientClinical
                Set RetrieveClinical = Nothing
                GoSub SetSrv
                Call ApplClaim.SetReceivableCosts(RcvId1, RetAppt.AppointmentReferralId, RetAppt.AppointmentResourceId1, False, LcId, "")
            End If
        End If
        Set ApplClaim = Nothing
        SetupReceivableAndService = True
    End If
    Set RetAppt = Nothing
End If
Exit Function
SetSrv:
    If (InvId > 0) Then
' this determines if we charge for the frames
        If (lstOwn.Text <> "Yes") Then
            Set RetPC = New PCInventory
            RetPC.InventoryId = InvId
            If (RetPC.RetrievePCInventory) Then
                Set RetrieveService = New PatientReceivableService
                RetrieveService.ItemId = 0
                Call RetrieveService.RetrievePatientReceivableService
                RetrieveService.Invoice = InvoiceId
                RetrieveService.Service = "V2020"
                RetrieveService.ServiceDate = RetAppt.AppointmentDate
                RetrieveService.ServiceQuantity = 1
                RetrieveService.ServiceCharge = 0
                RetrieveService.ServiceFeeCharge = 0
                RetrieveService.ServiceCharge = RetPC.Cost
                RetrieveService.ServiceFeeCharge = RetPC.Cost
                RetrieveService.ServiceTOS = ""
                RetrieveService.ServicePOS = ""
                RetrieveService.ServiceModifier = ""
                RetrieveService.ServiceLinkedDiag = ""
                RetrieveService.ServiceOrderDoc = False
                RetrieveService.ServiceConsultOn = False
                Call RetrieveService.ApplyPatientReceivableService
                Set RetrieveService = Nothing
            End If
            Set RetPC = Nothing
        End If
    End If
    MyCnt = 0
    If (Trim(ODDetails) <> "") Then
        i = InStrPS(ODDetails, ":")
        While (i > 0)
            j = InStrPS(i + 1, ODDetails, "&")
            If (j > i) Then
                MyCnt = MyCnt + 1
                myFilter = Mid(ODDetails, i - 1, 1)
                If (myFilter <> "T") Then
                    ASrv = Trim(Mid(ODDetails, i + 1, (j - 1) - i))
                    If (ASrv <> "") Then
                        p = 1
                        u = InStrPS(ASrv, ",")
                        While (u > 0)
                            mySrv = Mid(ASrv, p, (u - 1) - (p - 1))
                            If (Trim(mySrv) <> "") Then
                                If (MyCnt > 1) Then
                                    AEye = "RTLT"
                                Else
                                    AEye = "RT"
                                End If
                                GoSub PostSrv
                            End If
                            p = u + 1
                            u = InStrPS(p, ASrv, ",")
                        Wend
                        If (p < Len(ASrv)) Then
                            mySrv = Mid(ASrv, p, Len(ASrv) - (p - 1))
                            If (Trim(mySrv) <> "") Then
                                If (MyCnt > 1) Then
                                    AEye = "RTLT"
                                Else
                                    AEye = "RT"
                                End If
                                GoSub PostSrv
                            End If
                        End If
                    End If
                End If
            End If
            j = j + 1
            i = InStrPS(j, ODDetails, ":")
        Wend
    End If

' Just do the first one
    MyCnt = 0
    If (Trim(OSDetails) <> "") Then
        i = InStrPS(OSDetails, ":")
        While (i > 0) And (MyCnt < 1)
            j = InStrPS(i + 1, OSDetails, "&")
            If (j > i) Then
                MyCnt = MyCnt + 1
                myFilter = Mid(OSDetails, i - 1, 1)
                If (myFilter <> "T") Then
                    ASrv = Trim(Mid(OSDetails, i + 1, (j - 1) - i))
                    If (ASrv <> "") Then
                        p = 1
                        u = InStrPS(ASrv, ",")
                        While (u > 0)
                            mySrv = Mid(ASrv, p, (u - 1) - (p - 1))
                            If (Trim(mySrv) <> "") Then
                                If (MyCnt > 1) Then
                                    AEye = "RTLT"
                                Else
                                    AEye = "LT"
                                End If
                                GoSub PostSrv
                            End If
                            p = u + 1
                            u = InStrPS(p, ASrv, ",")
                        Wend
                        If (p < Len(ASrv)) Then
                            mySrv = Mid(ASrv, p, Len(ASrv) - (p - 1))
                            If (Trim(mySrv) <> "") Then
                                If (MyCnt > 1) Then
                                    AEye = "RTLT"
                                Else
                                    AEye = "LT"
                                End If
                                GoSub PostSrv
                            End If
                        End If
                    End If
                End If
            End If
            j = j + 1
            i = InStrPS(j, OSDetails, ":")
        Wend
    End If
    Return
PostSrv:
    If (mySrv <> "") Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplGetService(mySrv, ADesc, ACost)
        Set ApplTemp = Nothing
        Set RetrieveService = New PatientReceivableService
        RetrieveService.ItemId = 0
        Call RetrieveService.RetrievePatientReceivableService
        RetrieveService.Invoice = InvoiceId
        RetrieveService.Service = mySrv
        RetrieveService.ServiceDate = RetAppt.AppointmentDate
        RetrieveService.ServiceQuantity = 1
        If (AEye = "RTLT") Then
            RetrieveService.ServiceQuantity = 2
        End If
        RetrieveService.ServiceCharge = ACost
        RetrieveService.ServiceFeeCharge = ACost
        RetrieveService.ServiceTOS = ""
        RetrieveService.ServicePOS = ""
        RetrieveService.ServiceModifier = AEye
        RetrieveService.ServiceLinkedDiag = ""
        RetrieveService.ServiceOrderDoc = False
        RetrieveService.ServiceConsultOn = False
        Call RetrieveService.ApplyPatientReceivableService
        Set RetrieveService = Nothing
    End If
    Return
End Function

Private Function SetupManual(AODRx As String, AOSRx As String, ApptId As Long, ClnId As Long) As Boolean
Dim ATypeId As Long
Dim DrId As Long
Dim LcId As Long
Dim ADate As String
Dim ClnText As String
Dim InsType As String
Dim ApplClm As ApplicationClaims
Dim ApplList As ApplicationAIList
Dim RetCln As PatientClinical
' this appointment records the prescription as if it was a clinical appointment in the optical shop
ApptId = 0
SetupManual = False
If (PatientId > 0) Then
    If (lstDr.ListIndex >= 0) And (lstDr.Visible) Then
        DrId = val(Trim(Mid(lstDr.List(lstDr.ListIndex), 76, Len(lstDr.List(lstDr.ListIndex)) - 75)))
    Else
        DrId = GetOpticalDoctor
    End If
    LcId = GetOpticalPractice
    Call FormatTodaysDate(ADate, False)
    ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
    Set ApplClm = New ApplicationClaims
    ATypeId = ApplClm.GetManualApptType
    InsType = "V"
    If (lstIns.ListIndex > 0) Then
        InsType = Mid(lstIns.List(lstIns.ListIndex), 53, 1)
    End If
    ApptId = ApplClm.BuildAppointment(PatientId, DrId, LcId, ATypeId, ADate, InsType, 0, 0, True, True)
    Set ApplClm = Nothing
    If (ApptId > 0) Then
        ClnText = "DISPENSE SPECTACLE RX-9/-1/-2/" + AODRx + "-3/" + AOSRx + "-4/-5/0-6/0-7/"
        Set RetCln = New PatientClinical
        RetCln.ClinicalId = 0
        If (RetCln.RetrievePatientClinical) Then
            RetCln.AppointmentId = ApptId
            RetCln.PatientId = PatientId
            RetCln.ClinicalType = "A"
            RetCln.ClinicalDraw = ""
            RetCln.ClinicalFollowup = ""
            RetCln.ClinicalHighlights = ""
            RetCln.ClinicalPermanent = ""
            RetCln.ClinicalStatus = "A"
            RetCln.ClinicalSurgery = ""
            RetCln.EyeContext = "OU"
            RetCln.ImageDescriptor = ""
            RetCln.ImageInstructions = ""
            RetCln.Symptom = "1"
            RetCln.Findings = ClnText
            Call RetCln.ApplyPatientClinical
            ClnId = RetCln.ClinicalId
            SetupManual = True
        End If
        Set RetCln = Nothing
        Set RetCln = New PatientClinical
        RetCln.ClinicalId = 0
        If (RetCln.RetrievePatientClinical) Then
            RetCln.AppointmentId = ApptId
            RetCln.PatientId = PatientId
            RetCln.ClinicalType = "K"
            RetCln.ClinicalDraw = ""
            RetCln.ClinicalFollowup = ""
            RetCln.ClinicalHighlights = ""
            RetCln.ClinicalPermanent = ""
            RetCln.ClinicalStatus = "A"
            RetCln.ClinicalSurgery = ""
            RetCln.EyeContext = "OU"
            RetCln.ImageDescriptor = ""
            RetCln.ImageInstructions = ""
            RetCln.Symptom = "2"
            RetCln.Findings = "0001"
            Call RetCln.ApplyPatientClinical
            ClnId = RetCln.ClinicalId
            SetupManual = True
        End If
        Set RetCln = Nothing
    End If
End If
End Function

Private Function GetOpticalPractice() As Long
Dim i As Integer
Dim RetPrc As PracticeName
GetOpticalPractice = 0
If (lstLoc.ListIndex < 0) Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        i = 1
        Do Until Not (RetPrc.SelectPractice(i))
            If (RetPrc.PracticeLocationReference = "OPT") Then
                GetOpticalPractice = RetPrc.PracticeId + 1000
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetPrc = Nothing
Else
    GetOpticalPractice = val(Trim(Mid(lstLoc.List(lstLoc.ListIndex), 76, Len(lstLoc.List(lstLoc.ListIndex)) - 75)))
End If
End Function

Private Function GetOpticalDoctor() As Long
Dim i As Integer
Dim RetRes As SchedulerResource
Set RetRes = New SchedulerResource
GetOpticalDoctor = 0
RetRes.ResourceType = "Q"
RetRes.ResourceName = Chr(1)
If (RetRes.FindResource > 0) Then
    i = 1
    Do Until Not (RetRes.SelectResource(i))
        If (RetRes.ResourceType = "Q") Then
            GetOpticalDoctor = RetRes.ResourceId
            Exit Do
        End If
        i = i + 1
    Loop
End If
Set RetRes = Nothing
End Function

Public Function LoadActiveOrder(OrdId As Long) As Boolean
Dim ANoteIdOU As Long
Dim ClnId As Long
Dim z As Integer, v As Integer
Dim i As Integer, j As Integer
Dim u As Integer, w As Integer
Dim ODRx As String, OSRx As String
Dim ATemp As String, MyTemp As String
Dim ANoteFR As String, ANoteIdFR As Long
Dim ANoteOU As String, MySKU As String
Dim ATempOD As String, ATempOS As String
Dim OwnPC As String, SntPC As String
Dim CutOD As String, CutOS As String
Dim PDDOD As String, PDNOD As String, HgtOD As String, BCOD As String
Dim PDDOS As String, PDNOS As String, HgtOS As String, BCOS As String
Dim ATemp1OD As String, ATemp2OD As String, ATemp3OD As String, ATemp4OD As String, ATemp5OD As String, ATemp6OD As String
Dim ATemp1OS As String, ATemp2OS As String, ATemp3OS As String, ATemp4OS As String, ATemp5OS As String, ATemp6OS As String
Dim MyType As String, MyMan As String, MyModel As String, MyColor As String, FrameSize As String
Dim RetPC As CLOrders
Dim RetCln As PatientClinical
Dim RetRcv As PatientReceivables
Dim RetrievePCInventory As PCInventory
LoadActiveOrder = False
ClnId = 0
txtOD.Locked = False
txtOS.Locked = False
If (OrdId > 0) Then
    Set RetPC = New CLOrders
    RetPC.OrderId = OrdId
    If (RetPC.RetrieveCLOrders) Then
        PatientId = RetPC.PatientId
        AppointmentId = RetPC.AppointmentId
        If (RetPC.AppointmentId > 0) Then
            Set RetCln = New PatientClinical
            RetCln.ClinicalType = "A"
            RetCln.AppointmentId = RetPC.AppointmentId
            If (RetCln.FindPatientClinical > 0) Then
                i = 1
                While (RetCln.SelectPatientClinical(i))
                    If (Left(RetCln.Findings, 13) = "DISPENSE SPEC") Then
                        ClnId = RetCln.ClinicalId
                        u = InStrPS(RetCln.Findings, "-2/")
                        If (u > 0) Then
                            w = InStrPS(RetCln.Findings, "-3/")
                            If (w > 0) Then
                                ODRx = Mid(RetCln.Findings, u + 3, (w - 1) - (u + 2))
                            End If
                        End If
                        u = InStrPS(RetCln.Findings, "-3/")
                        If (u > 0) Then
                            w = InStrPS(RetCln.Findings, "-4/")
                            If (w > 0) Then
                                OSRx = Mid(RetCln.Findings, u + 3, (w - 1) - (u + 2))
                            End If
                        End If
                    End If
                    i = i + 1
                Wend
            End If
            Set RetCln = Nothing
        End If
' get the remaining elements to be loaded
        Set RetrievePCInventory = New PCInventory
        RetrievePCInventory.Criteria = "InventoryID = " + CStr(RetPC.InventoryId)
        RetrievePCInventory.FindPCInventory
        'If (RetrievePCInventory.SelectPCInventory(1)) Then
            MyType = RetrievePCInventory.Type_
            MySKU = RetrievePCInventory.SKU
            MyMan = RetrievePCInventory.Manufacturer
            MyModel = RetrievePCInventory.Model
            MyColor = RetrievePCInventory.PCColor
            OwnPC = "0"
            SntPC = "0"
            FrameSize = RetPC.ShipZip
            If (Left(RetPC.ShipZip, 1) = "&") Then
                OwnPC = Mid(RetPC.ShipZip, 2, 1)
                SntPC = Mid(RetPC.ShipZip, 3, 1)
                FrameSize = Mid(RetPC.ShipZip, 4, Len(RetPC.ShipZip) - 3)
            End If
' Load OD Stuff
            ATemp1OD = ""
            ATemp2OD = ""
            ATemp3OD = ""
            ATemp4OD = ""
            ATemp5OD = ""
            ATemp6OD = ""
            Call GetPCComponentParts(Trim(RetPC.ShipAddress1), "OD", ATemp1OD, ATemp2OD, ATemp3OD, ATemp4OD, ATemp5OD, ATemp6OD)
            ATempOD = ""
            ATempOS = ""
            i = InStrPS(RetPC.ShipCity, "OS:")
            If (i > 0) Then
                ATempOD = Trim(Left(RetPC.ShipCity, i - 1))
                Call ReplaceCharacters(ATempOD, "OD:", "~")
                Call StripCharacters(ATempOD, "~")
                ATempOS = Trim(Mid(RetPC.ShipCity, i, Len(RetPC.ShipCity) - (i - 1)))
                Call ReplaceCharacters(ATempOS, "OS:", "~")
                Call StripCharacters(ATempOS, "~")
            End If
            If (ATempOD <> "") Then
                ATempOD = ATempOD + " "
                j = 1
                i = InStrPS(ATempOD, "&")
                If (i = 0) Then
                    i = Len(ATempOD) + 1
                End If
                If (i <= Len(ATempOD) + 1) And (i > j) Then
                    PDDOD = Mid(ATempOD, j, (i - 1) - (j - 1))
                End If
                        
                j = i + 1
                i = InStrPS(j, ATempOD, "&")
                If (i = 0) Then
                    i = Len(ATempOD) + 1
                End If
                If (i <= Len(ATempOD) + 1) And (i > j) Then
                    PDNOD = Mid(ATempOD, j, (i - 1) - (j - 1))
                End If
                
                j = i + 1
                i = InStrPS(j, ATempOD, "&")
                If (i = 0) Then
                    i = Len(ATempOD) + 1
                End If
                If (i <= Len(ATempOD) + 1) And (i > j) Then
                    HgtOD = Mid(ATempOD, j, (i - 1) - (j - 1))
                End If
                
                j = i + 1
                i = InStrPS(j, ATempOD, "&")
                If (i = 0) Then
                    i = Len(ATempOD) + 1
                End If
                If (i <= Len(ATempOD) + 1) And (i > j) Then
                    BCOD = Mid(ATempOD, j, (i - 1) - (j - 1))
                End If
            End If
' Load OS Stuff
            ATemp1OS = ""
            ATemp2OS = ""
            ATemp3OS = ""
            ATemp4OS = ""
            ATemp5OS = ""
            ATemp6OS = ""
            Call GetPCComponentParts(Trim(RetPC.ShipAddress2), "OS", ATemp1OS, ATemp2OS, ATemp3OS, ATemp4OS, ATemp5OS, ATemp6OS)
            If (ATempOS <> "") Then
                ATempOS = ATempOS + " "
                j = 1
                i = InStrPS(ATempOS, "&")
                If (i = 0) Then
                    i = Len(ATempOS) + 1
                End If
                If (i <= Len(ATempOS) + 1) And (i > j) Then
                    PDDOS = Mid(ATempOS, j, (i - 1) - (j - 1))
                End If
                
                j = i + 1
                i = InStrPS(j, ATempOS, "&")
                If (i = 0) Then
                    i = Len(ATempOS) + 1
                End If
                If (i <= Len(ATempOS) + 1) And (i > j) Then
                    PDNOS = Mid(ATempOS, j, (i - 1) - (j - 1))
                End If
                
                j = i + 1
                i = InStrPS(j, ATempOS, "&")
                If (i = 0) Then
                    i = Len(ATempOS) + 1
                End If
                If (i <= Len(ATempOS) + 1) And (i > j) Then
                    HgtOS = Mid(ATempOS, j, (i - 1) - (j - 1))
                End If
                
                j = i + 1
                i = InStrPS(j, ATempOS, "&")
                If (i = 0) Then
                    i = Len(ATempOS) + 1
                End If
                If (i <= Len(ATempOS) + 1) And (i > j) Then
                    BCOS = Mid(ATempOS, j, (i - 1) - (j - 1))
                End If
            End If
            CutOD = Mid(RetPC.ShipState, 1, 1)
            CutOS = Mid(RetPC.ShipState, 2, 1)
        'End If
' Load Notes
        Set RetrievePCInventory = Nothing
        Call GetCLNote(OrdId, RetPC.OrderType, ANoteOU, ANoteIdOU, ANoteFR, ANoteIdFR)
    End If
    Set RetPC = Nothing
' Apply content to form
    txtOD.Text = ODRx
    txtOD.Visible = True
    txtOD.Tag = Trim(Str(ClnId))
    lblODRx.Visible = False
    txtOS.Text = OSRx
    txtOS.Visible = True
    txtOS.Tag = Trim(Str(ClnId))
    lblOSRx.Visible = False
    txtPDDVOD.Text = PDDOD
    txtPDDVOS.Text = PDDOS
    txtPDNVOD.Text = PDNOD
    txtPDNVOS.Text = PDNOS
    txtHeightOD.Text = HgtOD
    txtHeightOS.Text = HgtOS
    txtODBC.Text = BCOD
    txtOSBC.Text = BCOS
    
    If (ATemp1OD <> "") Then
        For z = 1 To lstLensOD.ListCount - 1
            If UCase(Trim(Mid(lstLensOD.List(z), 41, Len(lstLensOD.List(z)) - 40))) = UCase(ATemp1OD) Then
                lstLensOD.ListIndex = z
                Exit For
            End If
        Next z
    Else
        lstLensOD.ListIndex = -1
    End If
    
    For i = 0 To lstCoatingOD.ListCount - 1
        lstCoatingOD.Selected(i) = False
    Next i
    i = 1
    u = InStrPS(i, ATemp2OD, ",")
    If (u = 0) Then
        For z = 0 To lstCoatingOD.ListCount - 1
            If (Trim(Mid(lstCoatingOD.List(z), 41, Len(lstCoatingOD.List(z)) - 40)) = ATemp2OD) Then
                lstCoatingOD.Selected(z) = True
            End If
        Next z
        u = InStrPS(i, ATemp2OD, ",")
    Else
        While (u > 0)
            MyTemp = Mid(ATemp2OD, i, (u - 1) - (i - 1))
            For z = 0 To lstCoatingOD.ListCount - 1
                If (Trim(Mid(lstCoatingOD.List(z), 41, Len(lstCoatingOD.List(z)) - 40)) = MyTemp) Then
                    lstCoatingOD.Selected(z) = True
                End If
            Next z
            i = u + 1
            u = InStrPS(i, ATemp2OD, ",")
        Wend
    End If
    
    If (ATemp3OD <> "") Then
        For z = 1 To lstMaterialOD.ListCount - 1
            If UCase(Trim(Mid(lstMaterialOD.List(z), 41, Len(lstMaterialOD.List(z)) - 40))) = UCase(ATemp3OD) Then
                lstMaterialOD.ListIndex = z
                Exit For
            End If
        Next z
    Else
        lstMaterialOD.ListIndex = -1
    End If
    If (ATemp4OD <> "") Then
        For z = 1 To lstPhotoOD.ListCount - 1
            If UCase(Trim(Mid(lstPhotoOD.List(z), 41, Len(lstPhotoOD.List(z)) - 40))) = UCase(ATemp4OD) Then
                lstPhotoOD.ListIndex = z
                Exit For
            End If
        Next z
    Else
        lstPhotoOD.ListIndex = -1
    End If
    If (ATemp5OD <> "") Then
        For z = 1 To lstBrandOD.ListCount - 1
            If UCase(Trim(Mid(lstBrandOD.List(z), 41, Len(lstBrandOD.List(z)) - 40))) = UCase(ATemp5OD) Then
                lstBrandOD.ListIndex = z
                Exit For
            End If
        Next z
    Else
        lstBrandOD.ListIndex = -1
    End If
    If (ATemp6OD <> "") Then
        For z = 1 To lstTypeOD.ListCount - 1
            If UCase(Trim(Mid(lstTypeOD.List(z), 41, Len(lstTypeOD.List(z)) - 40))) = UCase(ATemp6OD) Then
                lstTypeOD.ListIndex = z
                Exit For
            End If
        Next z
    Else
        lstTypeOD.ListIndex = -1
    End If
    
    If (ATemp1OS <> "") Then
        For z = 1 To lstLensOS.ListCount - 1
            If UCase(Trim(Mid(lstLensOS.List(z), 41, Len(lstLensOS.List(z)) - 40))) = UCase(ATemp1OS) Then
                lstLensOS.ListIndex = z
                Exit For
            End If
        Next z
    Else
        lstLensOS.ListIndex = -1
    End If
    
    txtSKU.Text = MySKU
    Call txtSKU_KeyPress(13)
    
    txtA.Text = ""
    txtB.Text = ""
    txtED.Text = ""
    txtDBL.Text = ""
    txtEye.Text = ""
    txtBridge.Text = ""
    txtTemple.Text = ""
    v = InStrPS(1, FrameSize, "&")
    If (v > 0) Then
        txtA.Text = Left(FrameSize, v - 1)
        z = v + 1
        v = InStrPS(z, FrameSize, "&")
        If (v > 0) Then
            txtB.Text = Mid(FrameSize, z, (v - 1) - (z - 1))
            z = v + 1
            v = InStrPS(z, FrameSize, "&")
            If (v > 0) Then
                txtED.Text = Mid(FrameSize, z, (v - 1) - (z - 1))
                z = v + 1
                v = InStrPS(z, FrameSize, "&")
                If (v > 0) Then
                    txtDBL.Text = Mid(FrameSize, z, (v - 1) - (z - 1))
                    z = v + 1
                    v = InStrPS(z, FrameSize, "&")
                    If (v > 0) Then
                        txtEye.Text = Mid(FrameSize, z, (v - 1) - (z - 1))
                        z = v + 1
                        v = InStrPS(z, FrameSize, "&")
                        If (v > 0) Then
                            txtBridge.Text = Mid(FrameSize, z, (v - 1) - (z - 1))
                            z = v + 1
                            v = InStrPS(z, FrameSize, "&")
                            If (v > 0) Then
                                txtTemple.Text = Mid(FrameSize, z, (v - 1) - (z - 1))
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
        
    lstCutOD.ListIndex = 0
    If (CutOD <> "0") Then
        lstCutOD.ListIndex = 1
    End If
    lstOwn.ListIndex = 0
    If (OwnPC <> "0") Then
        lstOwn.ListIndex = 1
    End If
    lstSent.ListIndex = 0
    If (SntPC <> "0") Then
        lstSent.ListIndex = 1
    End If
' for the moment put notes in OD
    txtNoteOU.Text = ""
    txtNoteOU.Tag = ""
    If (Trim(ANoteOU) <> "") Then
        txtNoteOU.Text = ANoteOU
        txtNoteOU.Tag = Trim(Str(ANoteIdOU))
    End If
    txtNote.Text = ""
    txtNote.Tag = ""
    If (Trim(ANoteFR) <> "") Then
        txtNote.Text = ANoteFR
        txtNote.Tag = Trim(Str(ANoteIdFR))
    End If
    LoadActiveOrder = True
    If (OrdId > 0) Then
        cmdHold.Text = "Apply Change"
        EditOn = True
        OrderId = OrdId
    Else
        cmdHold.Text = "Manual Order"
    End If
End If
End Function

Private Sub GetPCComponentParts(ATemp As String, AEye As String, ATemp1 As String, ATemp2 As String, ATemp3 As String, ATemp4 As String, ATemp5 As String, ATemp6 As String)
Dim i As Integer
Dim j As Integer
ATemp1 = ""
ATemp2 = ""
ATemp3 = ""
ATemp4 = ""
ATemp5 = ""
ATemp6 = ""
ATemp = UCase(ATemp)
If (Trim(ATemp) <> "") Then
    i = InStrPS(ATemp, AEye + "L:")
    If (i > 0) Then
        j = InStrPS(ATemp, "&")
        If (j > 0) Then
            ATemp1 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
        End If
    End If
    i = InStrPS(ATemp, AEye + "C:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp2 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
        End If
    End If
    i = InStrPS(ATemp, AEye + "M:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp3 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
        End If
    End If
    i = InStrPS(ATemp, AEye + "P:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp4 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
        End If
    End If
    i = InStrPS(ATemp, AEye + "B:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp5 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
        End If
    End If
    i = InStrPS(ATemp, AEye + "T:")
    If (i > 0) Then
        j = InStrPS(i, ATemp, "&")
        If (j > 0) Then
            ATemp6 = Mid(ATemp, i + 4, (j - 1) - (i + 3))
        End If
    End If
End If
End Sub

Private Function GetCLNote(OrderId As Long, OType As String, ATextOU As String, ANoteIdOU As Long, ATextFR As String, ANoteIdFR As Long) As Boolean
Dim i As Integer
Dim RetOrder As CLOrders
Dim RetNote As PatientNotes
GetCLNote = False
i = 1
ATextOU = ""
ANoteIdOU = 0
ATextFR = ""
ANoteIdFR = 0
If (OrderId > 0) Then
    Set RetOrder = New CLOrders
    RetOrder.OrderId = OrderId
    If (RetOrder.RetrieveCLOrders) Then
        Set RetNote = New PatientNotes
        RetNote.NotesAppointmentId = RetOrder.AppointmentId
        RetNote.NotesSystem = "PCO" + Trim(Str(OrderId))
        If (RetNote.FindNotes > 0) Then
            i = 1
            While (RetNote.SelectNotes(i))
                If (RetNote.NotesEye = "OU") Then
                    ATextOU = ATextOU + Trim(RetNote.NotesText1) + " "
                    ANoteIdOU = RetNote.NotesId
                Else
                    ATextFR = ATextFR + Trim(RetNote.NotesText1) + " "
                    ANoteIdFR = RetNote.NotesId
                End If
                i = i + 1
            Wend
            GetCLNote = True
        End If
        Set RetNote = Nothing
    End If
    Set RetOrder = Nothing
End If
End Function

Private Function UpdateClaim(RcvId As Long, InvId As Long, ODDetails As String, OSDetails As String) As Boolean
Dim MyCnt As Integer
Dim i As Integer, j As Integer
Dim u As Integer, p As Integer
Dim SrvId(100) As Long
Dim LcId As Long, DrId As Long
Dim Copay As Single, ACost As Single
Dim InsurerId As Long, InsuredId As Long
Dim mySrv As String, AEye As String
Dim ADesc As String, ASrv As String
Dim InsType As String, InvoiceId As String
Dim RetPC As PCInventory
Dim RetRcv As PatientReceivables
Dim ApplList As ApplicationAIList
Dim ApplClaim As ApplicationClaims
Dim ApplSch As ApplicationScheduler
Dim ApplTemp As ApplicationTemplates
Dim RetPay As PatientReceivablePayment
Dim RetSrv As PatientReceivableService
UpdateClaim = False
If (RcvId > 0) Then
    Set RetRcv = New PatientReceivables
    RetRcv.ReceivableId = RcvId
    If (RetRcv.RetrievePatientReceivable) Then
        Set ApplClaim = New ApplicationClaims
        Call ApplClaim.UpdateReceivable(RcvId, RetRcv.ReceivableInvoiceDate, RetRcv.ReceivableAccidentType, RetRcv.ReceivableAccidentState, RetRcv.ReceivableHospitalAdmission, RetRcv.ReceivableHospitalDismissal, RetRcv.ReceivableDisability, RetRcv.ReceivableRsnType, RetRcv.ReceivableRsnDate, RetRcv.ReceivableConsDate, RetRcv.ReceivablePrevCond, RetRcv.ReceivableExternalRefInfo, RetRcv.ReceivableBillOffice)
        Call ApplClaim.UpdateOtherReceivableDates(RcvId, RetRcv.ReceivableInvoiceDate)
' Purge Current Services of the Optical Bill
        Set RetSrv = New PatientReceivableService
        RetSrv.Invoice = RetRcv.ReceivableInvoice
        If (RetSrv.FindPatientReceivableService > 0) Then
            j = 0
            i = 1
            While (RetSrv.SelectPatientReceivableService(i))
                If (RetSrv.ServiceStatus <> "X") Then
                    j = j + 1
                    SrvId(j) = RetSrv.ItemId
                    RetSrv.ServiceStatus = "X"
                    Call RetSrv.ApplyPatientReceivableService
                End If
                i = i + 1
            Wend
        End If
        Set RetSrv = Nothing
        GoSub SetSrv
        GoSub MovePayments
        Call ApplClaim.SetReceivableCosts(RcvId, RetRcv.ReceivableRefDr, RetRcv.ReceivableBillToDr, False, RetRcv.ReceivableBillOffice, "")
        Set ApplClaim = Nothing
        UpdateClaim = True
    End If
    Set RetRcv = Nothing
End If
Exit Function
MovePayments:
    For p = 1 To j
        If (SrvId(p) > 0) Then
            Set RetSrv = New PatientReceivableService
            RetSrv.ItemId = SrvId(p)
            If (RetSrv.RetrievePatientReceivableService) Then
                ASrv = RetSrv.Service
            End If
            Set RetSrv = Nothing
            If (Trim(ASrv) <> "") Then
                Set RetPay = New PatientReceivablePayment
                RetPay.ReceivableId = RetRcv.ReceivableId
                RetPay.PaymentService = ""
                RetPay.PaymentServiceItem = SrvId(p)
                RetPay.PaymentPayerType = ""
                RetPay.PaymentPayerId = 0
                RetPay.PaymentCheck = ""
                RetPay.PaymentType = ""
                If (RetPay.FindPatientReceivablePayments > 0) Then
                    i = 1
                    While (RetPay.SelectPatientReceivablePayments(i))
                        Set RetSrv = New PatientReceivableService
                        RetSrv.Invoice = RetRcv.ReceivableInvoice
                        RetSrv.Service = ASrv
                        RetSrv.ServiceStatus = ""
                        If (RetSrv.FindPatientReceivableService > 0) Then
                            u = 1
                            Do Until Not (RetSrv.SelectPatientReceivableService(u))
                                If (RetSrv.ServiceStatus <> "X") Then
                                    RetPay.PaymentServiceItem = RetSrv.ItemId
                                    Call RetPay.ApplyPatientReceivablePayments
                                    Exit Do
                                End If
                                u = u + 1
                            Loop
                        End If
                        Set RetSrv = Nothing
                        i = i + 1
                    Wend
                End If
                Set RetPay = Nothing
            End If
        End If
    Next p
    Return
SetSrv:
    If (InvId > 0) Then
        Set RetPC = New PCInventory
        RetPC.InventoryId = InvId
        If (RetPC.RetrievePCInventory) Then
            Set RetSrv = New PatientReceivableService
            RetSrv.ItemId = 0
            Call RetSrv.RetrievePatientReceivableService
            RetSrv.Invoice = RetRcv.ReceivableInvoice
            RetSrv.Service = "V2020"
            RetSrv.ServiceDate = RetRcv.ReceivableInvoiceDate
            RetSrv.ServiceQuantity = 1
' this determines if we charge for the frames
            RetSrv.ServiceCharge = 0
            RetSrv.ServiceFeeCharge = 0
            If (lstOwn.Text <> "Yes") Then
                RetSrv.ServiceCharge = RetPC.Cost
                RetSrv.ServiceFeeCharge = RetPC.Cost
            End If
            RetSrv.ServiceTOS = ""
            RetSrv.ServicePOS = ""
            RetSrv.ServiceModifier = ""
            RetSrv.ServiceLinkedDiag = ""
            RetSrv.ServiceOrderDoc = False
            RetSrv.ServiceConsultOn = False
            Call RetSrv.ApplyPatientReceivableService
            Set RetSrv = Nothing
        End If
        Set RetPC = Nothing
    End If
    If (Trim(ODDetails) <> "") Then
        i = InStrPS(ODDetails, ":")
        While (i > 0)
            MyCnt = MyCnt + 1
            j = InStrPS(i + 1, ODDetails, "&")
            If (j > i) Then
                ASrv = Trim(Mid(ODDetails, i + 1, (j - 1) - i))
                If (ASrv <> "") Then
                    p = 1
                    u = InStrPS(ASrv, ",")
                    While (u > 0)
                        mySrv = Mid(ASrv, p, (u - 1) - (p - 1))
                        If (Trim(mySrv) <> "") Then
                            If (MyCnt > 1) Then
                                AEye = "RTLT"
                            Else
                                AEye = "RT"
                            End If
                            GoSub PostSrv
                        End If
                        p = u + 1
                        u = InStrPS(p, ASrv, ",")
                    Wend
                    If (p < Len(ASrv)) Then
                        mySrv = Mid(ASrv, p, Len(ASrv) - (p - 1))
                        If (Trim(mySrv) <> "") Then
                            If (MyCnt > 1) Then
                                AEye = "RTLT"
                            Else
                                AEye = "RT"
                            End If
                            GoSub PostSrv
                        End If
                    End If
                End If
            End If
            j = j + 1
            i = InStrPS(j, ODDetails, ":")
        Wend
    End If
' just the first one
    MyCnt = 0
    If (Trim(OSDetails) <> "") Then
        i = InStrPS(OSDetails, ":")
        While (i > 0) And (MyCnt < 1)
            j = InStrPS(i + 1, OSDetails, "&")
            If (j > i) Then
                ASrv = Trim(Mid(OSDetails, i + 1, (j - 1) - i))
                If (ASrv <> "") Then
                    p = 1
                    u = InStrPS(ASrv, ",")
                    While (u > 0)
                        mySrv = Mid(ASrv, p, (u - 1) - (p - 1))
                        If (Trim(mySrv) <> "") Then
                            If (MyCnt > 1) Then
                                AEye = "RTLT"
                            Else
                                AEye = "LT"
                            End If
                            GoSub PostSrv
                        End If
                        p = u + 1
                        u = InStrPS(p, ASrv, ",")
                    Wend
                    If (p < Len(ASrv)) Then
                        mySrv = Mid(ASrv, p, Len(ASrv) - (p - 1))
                        If (Trim(mySrv) <> "") Then
                            If (MyCnt > 1) Then
                                AEye = "RTLT"
                            Else
                                AEye = "LT"
                            End If
                            GoSub PostSrv
                        End If
                    End If
                End If
            End If
            j = j + 1
            i = InStrPS(j, OSDetails, ":")
        Wend
    End If
    Return
PostSrv:
    If (mySrv <> "") Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplGetService(mySrv, ADesc, ACost)
        Set ApplTemp = Nothing
        Set RetSrv = New PatientReceivableService
        RetSrv.ItemId = 0
        Call RetSrv.RetrievePatientReceivableService
        RetSrv.Invoice = RetRcv.ReceivableInvoice
        RetSrv.Service = mySrv
        RetSrv.ServiceDate = RetRcv.ReceivableInvoiceDate
        RetSrv.ServiceQuantity = 1
        If (AEye = "RTLT") Then
            RetSrv.ServiceQuantity = 2
        End If
        RetSrv.ServiceCharge = ACost
        RetSrv.ServiceFeeCharge = ACost
        RetSrv.ServiceTOS = ""
        RetSrv.ServicePOS = ""
        RetSrv.ServiceModifier = AEye
        RetSrv.ServiceLinkedDiag = ""
        RetSrv.ServiceOrderDoc = False
        RetSrv.ServiceConsultOn = False
        Call RetSrv.ApplyPatientReceivableService
        Set RetSrv = Nothing
    End If
    Return
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
