VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmInsurer 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   FillColor       =   &H00FFFFFF&
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtSJ 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10920
      MaxLength       =   2
      TabIndex        =   14
      Top             =   3120
      Width           =   855
   End
   Begin VB.CheckBox chkSrvF 
      BackColor       =   &H0077742D&
      Caption         =   "Allow 0 Amt"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3960
      TabIndex        =   73
      Top             =   3600
      Width           =   2175
   End
   Begin VB.TextBox txtCPTClass 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   71
      Top             =   4080
      Width           =   2655
   End
   Begin VB.CheckBox chkOCode 
      BackColor       =   &H0077742D&
      Caption         =   "Opth Codes On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3960
      TabIndex        =   70
      Top             =   3360
      Width           =   1935
   End
   Begin VB.TextBox txtBBL 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   68
      Top             =   4560
      Width           =   2655
   End
   Begin VB.ListBox lst50 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      ItemData        =   "Insurer.frx":0000
      Left            =   3960
      List            =   "Insurer.frx":000D
      TabIndex        =   7
      Top             =   2520
      Width           =   2175
   End
   Begin VB.TextBox txtPlanFormat 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   32
      TabIndex        =   29
      Top             =   7440
      Width           =   3200
   End
   Begin VB.TextBox txtComment 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   64
      TabIndex        =   28
      Top             =   7440
      Width           =   3195
   End
   Begin VB.TextBox txtMdId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   16
      TabIndex        =   15
      Top             =   3600
      Width           =   2655
   End
   Begin VB.CheckBox chkPrec 
      BackColor       =   &H0077742D&
      Caption         =   "PreCert Required"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   62
      Top             =   3360
      Width           =   2295
   End
   Begin VB.CheckBox chkDPrim 
      BackColor       =   &H0077742D&
      Caption         =   "Plan can be used by a  Dependent"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   58
      Top             =   4080
      Width           =   3975
   End
   Begin VB.TextBox txtPOS 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   25
      Top             =   6480
      Width           =   3200
   End
   Begin VB.TextBox txtPType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      TabIndex        =   59
      Top             =   2640
      Width           =   1575
   End
   Begin VB.CheckBox chkPrint 
      BackColor       =   &H0077742D&
      Caption         =   "Paper Claim if not primary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   57
      Top             =   3840
      Width           =   2775
   End
   Begin VB.CheckBox chkXOver 
      BackColor       =   &H0077742D&
      Caption         =   "Auto-CrossOver"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   56
      Top             =   3600
      Width           =   2295
   End
   Begin VB.TextBox TFormat 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      TabIndex        =   26
      Top             =   6960
      Width           =   3200
   End
   Begin VB.TextBox txtRef 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1920
      TabIndex        =   6
      Top             =   2640
      Width           =   1935
   End
   Begin VB.TextBox txtAdj 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   27
      Top             =   6960
      Width           =   3200
   End
   Begin VB.TextBox txtTOS 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   23
      Top             =   6000
      Width           =   3200
   End
   Begin VB.TextBox txtNEIC 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   16
      TabIndex        =   13
      Top             =   3120
      Width           =   2655
   End
   Begin VB.TextBox txtClaimPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   21
      Top             =   5520
      Width           =   1935
   End
   Begin VB.TextBox txtProvPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8550
      MaxLength       =   16
      TabIndex        =   19
      Top             =   5040
      Width           =   1935
   End
   Begin VB.TextBox txtEligPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   16
      TabIndex        =   20
      Top             =   5520
      Width           =   1935
   End
   Begin VB.TextBox txtPrecPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   16
      TabIndex        =   18
      Top             =   5040
      Width           =   1935
   End
   Begin VB.TextBox EFormat 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      TabIndex        =   24
      Top             =   6480
      Width           =   3200
   End
   Begin VB.TextBox ENumber 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   20
      TabIndex        =   22
      Top             =   6000
      Width           =   3200
   End
   Begin VB.CheckBox chkReferral 
      BackColor       =   &H0077742D&
      Caption         =   "Referral Required"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   3120
      Width           =   2415
   End
   Begin VB.TextBox PlanType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   8
      TabIndex        =   12
      Top             =   2160
      Width           =   1875
   End
   Begin VB.TextBox GroupId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   10
      Top             =   1200
      Width           =   3855
   End
   Begin VB.TextBox PlanName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   8
      Top             =   240
      Width           =   3855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   30
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Insurer.frx":0049
   End
   Begin VB.TextBox State 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1920
      MaxLength       =   2
      TabIndex        =   3
      Top             =   1680
      Width           =   615
   End
   Begin VB.TextBox InsurerName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1913
      MaxLength       =   32
      TabIndex        =   0
      Top             =   240
      Width           =   4215
   End
   Begin VB.TextBox PlanId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   11
      Top             =   1680
      Width           =   3855
   End
   Begin VB.TextBox City 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1920
      MaxLength       =   30
      TabIndex        =   2
      Top             =   1200
      Width           =   4215
   End
   Begin VB.TextBox Zip 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4560
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1680
      Width           =   1575
   End
   Begin VB.TextBox Phone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1920
      MaxLength       =   16
      TabIndex        =   5
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox GroupName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   9
      Top             =   720
      Width           =   3855
   End
   Begin VB.TextBox Address 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1913
      MaxLength       =   50
      TabIndex        =   1
      Top             =   720
      Width           =   4215
   End
   Begin VB.TextBox Copay 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2640
      MaxLength       =   12
      TabIndex        =   17
      Top             =   4560
      Width           =   1935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   240
      TabIndex        =   31
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Insurer.frx":0228
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5160
      TabIndex        =   42
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Insurer.frx":0407
   End
   Begin VB.Label Label32 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State Jurisdiction"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   630
      Left            =   10440
      TabIndex        =   74
      Top             =   2520
      Width           =   1485
   End
   Begin VB.Label Label31 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CPT Class"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   72
      Top             =   4080
      Width           =   2325
   End
   Begin VB.Label Label30 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Business Class"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   69
      Top             =   4560
      Width           =   2325
   End
   Begin VB.Label lblInsId 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Id:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   10080
      TabIndex        =   67
      Top             =   2160
      Width           =   1815
   End
   Begin VB.Label Label29 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Modifier 50"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3960
      TabIndex        =   66
      Top             =   2160
      Width           =   2175
   End
   Begin VB.Label Label28 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan Format"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   65
      Top             =   7440
      Width           =   2325
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Comment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   64
      Top             =   7440
      Width           =   2325
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Medigap Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6330
      TabIndex        =   63
      Top             =   3600
      Width           =   1605
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "POS Table"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   61
      Top             =   6480
      Width           =   2325
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pay Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   60
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Transmission Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   55
      Top             =   6960
      Width           =   2325
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Reference"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   54
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   3600
      TabIndex        =   53
      Top             =   1680
      Width           =   840
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   52
      Top             =   1680
      Width           =   1560
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Default Adjustment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   51
      Top             =   6960
      Width           =   2325
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "TOS Table"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   50
      Top             =   6000
      Width           =   2325
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "NEIC #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   49
      Top             =   3120
      Width           =   1575
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Claim Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   48
      Top             =   5520
      Width           =   2325
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Provider Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6120
      TabIndex        =   47
      Top             =   5040
      Width           =   2325
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Eligibility Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   46
      Top             =   5520
      Width           =   2325
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "PreCert Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   45
      Top             =   5040
      Width           =   2325
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Submission Format"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   44
      Top             =   6480
      Width           =   2325
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Submission Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   43
      Top             =   6000
      Width           =   2325
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Group Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   41
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "GroupName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   40
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   39
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   38
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   37
      Top             =   240
      Width           =   1560
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   6360
      TabIndex        =   36
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   35
      Top             =   1200
      Width           =   1560
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   34
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   33
      Top             =   720
      Width           =   1560
   End
   Begin VB.Label Label21 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Copay"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   240
      TabIndex        =   32
      Top             =   4560
      Width           =   2325
   End
End
Attribute VB_Name = "frmInsurer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DeleteOn As Boolean
Public ReadOnly As Boolean
Public TheInsurerId As Long
Public CurrentAction As String
Private ClaimMap As String
Private OriginalBClass As String
Private OriginalInsurerName As String
Private OriginalNEIC As String
Private OriginalRefCode As String
Private OriginalTFormat As String
Private OriginalPOS As String
Private OriginalTOS As String
Private OriginalAdj As String
Private OriginalPType As String
Private OriginalXOver As Boolean

Private Sub chkSrvF_Click()
If chkSrvF.Value = 0 Then
    frmEventMsgs.Header = "You are disabling PQRI.  This will result in lower revenues to the practice.  Are you sure?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If frmEventMsgs.Result = 4 Then
         chkSrvF.Value = 1
    End If
End If
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
If Not (ApplTbl.ApplIsPlanInUse(TheInsurerId)) Then
    If (ApplTbl.ApplDeletePlan(TheInsurerId)) Then
        TheInsurerId = 0
        Unload frmInsurer
    End If
Else
    frmEventMsgs.Header = "Insurer Plan is in use. Delete Denied."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set ApplTbl = Nothing
End Sub

Private Sub cmdDone_Click()
Dim ApplTbl As ApplicationTables
Dim ACpt As String
Dim Temp As String, BusClass As String
Dim TheAction As String, AClm As String
Dim Prec As Boolean, AOCode As Boolean
Dim Req As Boolean, Xc As Boolean
Dim Pc As Boolean, Dp As Boolean, ASrvF As Boolean
If (Trim(InsurerName.Text) = "") Then
    frmEventMsgs.Header = "Select Insurer Name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    InsurerName.Text = ""
    InsurerName.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
' test for medicare and/or medicaid ... must have a US State juristiction
If (Left(txtRef.Text, 1) = "C") Or (Left(txtRef.Text, 1) = "M") Or (Left(txtRef.Text, 1) = "N") Or (Left(txtRef.Text, 1) = "[") Then
    If (Trim(txtSJ.Text) = "") Then
        frmEventMsgs.Header = "Insurer must have a US State Jurisdiction"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSJ.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
End If
Temp = InsurerName.Text
Call StripCharacters(Temp, "'")
InsurerName.Text = Temp
Temp = PlanName.Text
Call StripCharacters(PlanName.Text, "'")
PlanName.Text = Temp
Temp = GroupName.Text
Call StripCharacters(GroupName.Text, "'")
GroupName.Text = Temp
Temp = GroupId.Text
Call StripCharacters(GroupId.Text, "'")
GroupId.Text = Temp
Set ApplTbl = New ApplicationTables
If (TheInsurerId < 1) Then
    If (ApplTbl.ApplIsInsurer(Trim(InsurerName.Text), Trim(Address.Text), Trim(City.Text), Trim(State.Text), Trim(Zip.Text), Trim(PlanName.Text), Trim(PlanId.Text), Left(PlanType.Text, 3), Trim(GroupName.Text), Trim(GroupId.Text), 0)) Then
        frmEventMsgs.Header = "Insurer already exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        InsurerName.SetFocus
        Set ApplTbl = Nothing
        Exit Sub
    End If
Else
    If (ApplTbl.ApplIsInsurer(Trim(InsurerName.Text), Trim(Address.Text), Trim(City.Text), Trim(State.Text), Trim(Zip.Text), Trim(PlanName.Text), Trim(PlanId.Text), Left(PlanType.Text, 3), Trim(GroupName.Text), Trim(GroupId.Text), 1)) Then
        frmEventMsgs.Header = "Duplicate Plan Information not allowed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Continue"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result <> 2) Then
            InsurerName.SetFocus
            Set ApplTbl = Nothing
            Exit Sub
        End If
    End If
End If
Req = False
If (chkReferral.Value = 1) Then
    Req = True
End If
Xc = False
If (chkXOver.Value = 1) Then
    Xc = True
End If
Pc = False
If (chkPrint.Value = 1) Then
    Pc = True
End If
Dp = False
If (chkDPrim.Value = 1) Then
    Dp = True
End If
Prec = False
If (chkPrec.Value = 1) Then
    Prec = True
End If
If (lst50.ListIndex < 0) Then
    Mid(ClaimMap, 1, 1) = " "
Else
    Mid(ClaimMap, 1, 1) = Trim(Str(lst50.ListIndex + 1))
End If
BusClass = Trim(txtBBL.Text)
AOCode = False
If (chkOCode.Value = 1) Then
    AOCode = True
End If
ACpt = Trim(txtCPTClass.Text)
ASrvF = False
If (chkSrvF.Value = 1) Then
    ASrvF = True
End If
If (ApplTbl.ApplPostPlan _
        (TheInsurerId, Trim(InsurerName.Text), Trim(PlanName.Text), _
         Trim(PlanId.Text), UCase(PlanType.Text), Trim(GroupName.Text), _
         Trim(GroupId.Text), Trim(Address.Text), Trim(City.Text), _
         Trim(State.Text), Trim(Zip.Text), Trim(Phone.Text), _
         UCase(Left(txtRef.Text, 1)), UCase(Left(txtPType.Text, 2)), _
         Req, Xc, Copay.Text, _
         Trim(txtTOS.Text), Trim(txtPos.Text), Trim(ENumber.Text), _
         UCase(Left(EFormat.Text, 1)), UCase(Left(TFormat.Text, 1)), _
         Trim(txtPrecPhone.Text), Trim(txtProvPhone.Text), Trim(txtEligPhone.Text), _
         Trim(txtClaimPhone.Text), Trim(txtNEIC.Text), Pc, Trim(txtAdj.Text), Dp, Prec, _
         Trim(txtMdId.Text), Trim(txtComment.Text), Trim(txtPlanFormat.Text), _
         ClaimMap, BusClass, AOCode, ACpt, ASrvF, Trim(txtSJ.Text))) Then
    If UserLogin.HasPermission(epAllowAggregateInsurerChanges) Then
        If ((Trim(txtNEIC.Text) <> OriginalNEIC) Or _
            (Trim(InsurerName.Text) <> OriginalInsurerName) Or _
            (Trim(txtTOS.Text) <> OriginalTOS) Or _
            (Trim(txtPos.Text) <> OriginalPOS) Or _
            (Trim(txtAdj.Text) <> OriginalAdj) Or _
            (Trim(txtPType.Text) <> OriginalPType) Or _
            (Trim(txtBBL.Text) <> OriginalBClass) Or _
            (Trim(txtRef.Text) <> OriginalRefCode) Or _
            (Xc <> OriginalXOver) Or _
            (Trim(Left(TFormat.Text, 1)) <> OriginalTFormat)) And _
            (TheInsurerId > 0) Then
            frmEventMsgs.Header = "Distribute Across all Plans of this Insurer ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                Call ApplTbl.ApplUpdateInsurerAcrossPolicys(Trim(InsurerName.Text), OriginalInsurerName, txtNEIC.Text, OriginalNEIC, Left(TFormat.Text, 1), OriginalTFormat, txtTOS.Text, OriginalTOS, txtPos.Text, OriginalPOS, Xc, OriginalXOver, Trim(txtAdj.Text), OriginalAdj, Trim(txtPType.Text), OriginalPType, Trim(txtRef.Text), OriginalRefCode, Trim(txtBBL.Text), OriginalBClass)
            End If
        End If
    End If
End If
Set ApplTbl = Nothing
Unload frmInsurer
End Sub

Private Sub cmdHome_Click()
CurrentAction = "HomeNoChanges"
Unload frmInsurer
End Sub

Public Function InsurerLoadDisplay(InsurerId As Long, InsName As String) As Boolean
Dim Temp As String, ACpt As String
Dim ACom As String, APFrm As String, AClm As String
Dim PName As String, PID As String, PType As String
Dim GName As String, GId As String, Addr As String, ABus As String
Dim Cty As String, st As String, Zp As String, Phn As String, MdId As String
Dim rf As String, PT As String, Cp As String, OOP As String, Ts As String
Dim Ps As String, En As String, Ef As String, Tf As String, ClPhn As String
Dim PcPhn As String, PrPhn As String, ElPhn As String, Nn As String, Adj As String
Dim Rq As Boolean, Xo As Boolean, Pc As Boolean, Dp As Boolean, Prec As Boolean
Dim Ao As Boolean, ASrvF As Boolean, AStaJ As String, ADme As String
Dim ApplTbl As ApplicationTables
Dim RetIns As Insurer
TheInsurerId = 0
InsurerLoadDisplay = True
cmdDelete.Visible = DeleteOn
cmdDone.Visible = Not ReadOnly
Set ApplTbl = New ApplicationTables
If InsurerId = 0 Then
    InsurerId = -1
    InsName = ""
End If
If (ApplTbl.ApplLoadInsurer(InsurerId, InsName, PName, PID, PType, GName, GId, Addr, Cty, st, Zp, Phn, rf, PT, Cp, Ts, Ps, En, Ef, Tf, PcPhn, PrPhn, ElPhn, ClPhn, Nn, Rq, Xo, Pc, Adj, Dp, Prec, MdId, ACom, APFrm, AClm, ABus, Ao, ACpt, ASrvF, AStaJ)) Then
    TheInsurerId = InsurerId
    InsurerName.Text = InsName
    OriginalInsurerName = InsName
    If (InsurerId > 0) Then
        PlanName.Text = PName
        PlanId.Text = PID
        PlanType.Text = PType
        GroupName.Text = GName
        GroupId.Text = GId
        Address.Text = Addr
        City.Text = Cty
        State.Text = st
        Zip.Text = Zp
        Phone.Text = Phn
    End If
    txtPlanFormat.Text = APFrm
    txtComment.Text = ACom
    txtRef.Text = rf
    OriginalRefCode = rf
    txtPType.Text = PT
    OriginalPType = PT
    Copay.Text = Cp
    txtTOS.Text = Ts
    OriginalTOS = Ts
    txtPos.Text = Ps
    OriginalPOS = Ps
    ENumber.Text = En
    EFormat.Text = Ef
    TFormat.Text = Tf
    txtSJ.Text = AStaJ
    OriginalTFormat = Tf
    txtPrecPhone.Text = PcPhn
    txtProvPhone.Text = PrPhn
    txtEligPhone.Text = ElPhn
    txtClaimPhone.Text = ClPhn
    txtMdId.Text = MdId
    txtNEIC.Text = Nn
    OriginalNEIC = Nn
    txtAdj.Text = Adj
    OriginalAdj = Adj
    If (ASrvF) Then
        chkSrvF.Value = 1
    Else
        chkSrvF.Value = 0
    End If
    If (Ao) Then
        chkOCode.Value = 1
    Else
        chkOCode.Value = 0
    End If
    If (Rq) Then
        chkReferral.Value = 1
    Else
        chkReferral.Value = 0
    End If
    If (Prec) Then
        chkPrec.Value = 1
    Else
        chkPrec.Value = 0
    End If
    If (Xo) Then
        chkXOver.Value = 1
    Else
        chkXOver.Value = 0
    End If
    If (Pc) Then
        chkPrint.Value = 1
    Else
        chkPrint.Value = 0
    End If
    If (Dp) Then
        chkDPrim.Value = 1
    Else
        chkDPrim.Value = 0
    End If
    OriginalXOver = Xo
    lst50.ListIndex = -1
    If (Len(Trim(AClm)) > 0) Then
        lst50.ListIndex = val(Mid(AClm, 1, 1)) - 1
    End If
    ClaimMap = AClm
    If (Trim(AClm) = "") Then
        ClaimMap = Space(32)
    End If
    txtBBL.Text = ABus
    OriginalBClass = ABus
    txtCPTClass.Text = ACpt
End If
InsurerName.Locked = False
lblInsId.Caption = "Id: " + Trim(Str(InsurerId))
Set ApplTbl = Nothing
End Function

Private Sub Copay_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub ENumber_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(ENumber, "P")
End If
End Sub

Private Sub ENumber_Validate(Cancel As Boolean)
Call UpdateDisplay(ENumber, "P")
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmInsurer.BorderStyle = 1
    frmInsurer.ClipControls = True
    frmInsurer.Caption = Mid(frmInsurer.Name, 4, Len(frmInsurer.Name) - 3)
    frmInsurer.AutoRedraw = True
    frmInsurer.Refresh
End If
End Sub

Private Sub Phone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Phone, "P")
End If
End Sub

Private Sub Phone_Validate(Cancel As Boolean)
Call UpdateDisplay(Phone, "P")
End Sub

Private Sub txtAdj_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PAYMENTTYPE")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtAdj.Text = Mid(frmSelectDialogue.Selection, Len(frmSelectDialogue.Selection), 1)
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub txtAdj_KeyPress(KeyAscii As Integer)
Call txtAdj_Click
KeyAscii = 0
End Sub

Private Sub txtBBL_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("BUSINESSCLASS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtBBL.Text = Trim(frmSelectDialogue.Selection)
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub txtBBL_KeyPress(KeyAscii As Integer)
Call txtBBL_Click
KeyAscii = 0
End Sub

Private Sub txtCPTClass_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("CPTCLASS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtCPTClass.Text = Trim(frmSelectDialogue.Selection)
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub txtCPTClass_KeyPress(KeyAscii As Integer)
Call txtCPTClass_Click
KeyAscii = 0
End Sub

Private Sub txtClaimPhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(txtClaimPhone, "P")
End If
End Sub

Private Sub txtClaimPhone_Validate(Cancel As Boolean)
Call UpdateDisplay(txtClaimPhone, "P")
End Sub

Private Sub txtPlanFormat_KeyPress(KeyAscii As Integer)
Dim ATemp As String
ATemp = UCase(Chr(KeyAscii))
If (InStrPS("ANEO", UCase(ATemp)) = 0) Then
    frmEventMsgs.Header = "Must be [A]lpha, [N]umeric, [E]ither, [O]ptional."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
Else
    KeyAscii = Asc(ATemp)
End If
End Sub

Private Sub txtProvPhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(txtProvPhone, "P")
End If
End Sub

Private Sub txtProvPhone_Validate(Cancel As Boolean)
Call UpdateDisplay(txtProvPhone, "P")
End Sub

Private Sub txtPrecPhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(txtPrecPhone, "P")
End If
End Sub

Private Sub txtPrecPhone_Validate(Cancel As Boolean)
Call UpdateDisplay(txtPrecPhone, "P")
End Sub

Private Sub txtEligPhone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(txtEligPhone, "P")
End If
End Sub

Private Sub txtEligPhone_Validate(Cancel As Boolean)
Call UpdateDisplay(txtEligPhone, "P")
End Sub

Private Sub txtPType_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("InsurerPType")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtPType.Text = Left(frmSelectDialogue.Selection, 2)
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub txtPType_KeyPress(KeyAscii As Integer)
Call txtPType_Click
KeyAscii = 0
End Sub

Private Sub txtRef_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("InsurerRef")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtRef.Text = Left(frmSelectDialogue.Selection, 1)
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub txtRef_KeyPress(KeyAscii As Integer)
Call txtRef_Click
KeyAscii = 0
End Sub

Private Sub PlanType_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PlanType")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    PlanType.Text = frmSelectDialogue.Selection
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub PlanType_KeyPress(KeyAscii As Integer)
Call PlanType_Click
KeyAscii = 0
End Sub

Private Sub EFormat_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("SubFormat")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    EFormat.Text = frmSelectDialogue.Selection
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub EFormat_KeyPress(KeyAscii As Integer)
Call EFormat_Click
KeyAscii = 0
End Sub

Private Sub txtSJ_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("USSTATES")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (Left(frmSelectDialogue.Selection, 6) = "Create") Then
        txtSJ.Text = ""
    Else
        txtSJ.Text = Left(frmSelectDialogue.Selection, 2)
    End If
End If
End Sub

Private Sub txtSJ_KeyPress(KeyAscii As Integer)
Call txtSJ_Click
KeyAscii = 0
End Sub

Private Sub txtSJ_Validate(Cancel As Boolean)
If (txtSJ.Text = "") Then
    Call txtSJ_Click
End If
End Sub

Private Sub Zip_KeyPress(KeyAscii As Integer)
Dim ACity As String
Dim AState As String
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Zip, "Z")
    If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
        Call GetCityStatebyZip(Zip.Text, ACity, AState)
        If (Trim(ACity) <> "") Then
            City.Text = ACity
        End If
        If (Trim(AState) <> "") Then
            State.Text = AState
        End If
    End If
End If
End Sub

Private Sub Zip_Validate(Cancel As Boolean)
Dim ACity As String
Dim AState As String
Call UpdateDisplay(Zip, "Z")
If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
    Call GetCityStatebyZip(Zip.Text, ACity, AState)
    If (Trim(ACity) <> "") Then
        City.Text = ACity
    End If
    If (Trim(AState) <> "") Then
        State.Text = AState
    End If
End If
End Sub

Private Sub txtTOS_DblClick()
txtTOS.Text = ""
End Sub

Private Sub txtTOS_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("TOS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection = "Create TOS Table") Then
        frmTosPos.DeleteOn = False
        frmTosPos.TableName = ""
        frmTosPos.TblType = "T"
        If (frmTosPos.LoadDisplay) Then
            frmTosPos.Show 1
        End If
        txtTOS.Text = ""
    Else
        txtTOS.Text = frmSelectDialogue.Selection
    End If
End If
frmSelectDialogue.InsurerSelected = Temp
End Sub

Private Sub txtTOS_KeyPress(KeyAscii As Integer)
Call txtTOS_Click
KeyAscii = 0
End Sub

Private Sub txtPOS_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("POS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection = "Create POS Table") Then
        frmTosPos.DeleteOn = False
        frmTosPos.TableName = ""
        frmTosPos.TblType = "P"
        If (frmTosPos.LoadDisplay) Then
            frmTosPos.Show 1
        End If
        txtPos.Text = ""
    Else
        txtPos.Text = frmSelectDialogue.Selection
    End If
End If
frmSelectDialogue.InsurerSelected = ""
End Sub

Private Sub txtPOS_KeyPress(KeyAscii As Integer)
Call txtPOS_Click
KeyAscii = 0
End Sub

Private Sub TFormat_Click()
Dim Temp As String
Temp = frmSelectDialogue.InsurerSelected
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("TransmitType")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    TFormat.Text = frmSelectDialogue.Selection
End If
frmSelectDialogue.InsurerSelected = ""
End Sub

Private Sub TFormat_KeyPress(KeyAscii As Integer)
Call TFormat_Click
KeyAscii = 0
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
