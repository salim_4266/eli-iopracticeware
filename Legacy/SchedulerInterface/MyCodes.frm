VERSION 5.00
Begin VB.Form frmMyCodes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select A Code"
   ClientHeight    =   6315
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6090
   ForeColor       =   &H8000000F&
   Icon            =   "MyCodes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCode 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   1455
   End
   Begin VB.ListBox lstCodes 
      Height          =   4740
      ItemData        =   "MyCodes.frx":000C
      Left            =   240
      List            =   "MyCodes.frx":000E
      TabIndex        =   1
      Top             =   840
      Width           =   5535
   End
   Begin VB.CommandButton cmdDone 
      BackColor       =   &H0080FFFF&
      Caption         =   "Done"
      Height          =   495
      Left            =   4200
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5760
      Width           =   1575
   End
   Begin VB.Label lblEnter 
      AutoSize        =   -1  'True
      Caption         =   "Enter Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "frmMyCodes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheType As String
Public TheCode As String

Private Sub cmdDone_Click()
TheCode = ""
If (lstCodes.ListIndex >= 0) Then
    TheCode = Trim(Mid(lstCodes.List(lstCodes.ListIndex), 76, Len(lstCodes.List(lstCodes.ListIndex)) - 75))
End If
Unload frmMyCodes
End Sub

Public Function LoadMyCodes() As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetCode As PracticeCodes
LoadMyCodes = False
txtCode.Text = ""
If (TheType <> "") Then
    lstCodes.Clear
    Set RetCode = New PracticeCodes
    frmMyCodes.Caption = "Select Diagnosis"
    RetCode.ReferenceType = "MYDIAGNOSIS"
    If (TheType = "P") Then
        frmMyCodes.Caption = "Select Procedure"
        RetCode.ReferenceType = "MYPROCEDURES"
    End If
    If (RetCode.FindCodePartial > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            DisplayText = Space(75)
            Mid(DisplayText, 1, 10) = Left(RetCode.ReferenceCode, 10)
            DisplayText = DisplayText + Trim(RetCode.ReferenceAlternateCode)
            lstCodes.AddItem DisplayText
            i = i + 1
        Wend
    End If
    Set RetCode = Nothing
    If (lstCodes.ListCount > 0) Then
        LoadMyCodes = True
    End If
End If
End Function

Private Sub lstCodes_Click()
If (lstCodes.ListIndex >= 0) Then
    If (Trim(txtCode.Text) = "") Then
        txtCode.Text = Trim(Left(lstCodes.List(lstCodes.ListIndex), 10))
    End If
End If
End Sub

Private Sub txtCode_KeyPress(KeyAscii As Integer)
Dim i As Integer
If (KeyAscii = 13) Then
    For i = 0 To lstCodes.ListCount - 1
        If (UCase(Trim(txtCode.Text)) = Trim(Left(lstCodes.List(i), 10))) Then
            lstCodes.ListIndex = i
            Exit For
        End If
    Next i
    If (lstCodes.ListIndex < 0) Then
        frmEventMsgs.Header = "Code Not Found"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtCode.Text = ""
        txtCode.SetFocus
    End If
End If
End Sub
Private Sub txtCode_Validate(Cancel As Boolean)
If (Trim(txtCode.Text) <> "") Then
    Call txtCode_KeyPress(13)
End If
End Sub
Public Sub FrmClose()
Unload Me
End Sub
