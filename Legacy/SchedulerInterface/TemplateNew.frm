VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "SSCALB32.OCX"
Begin VB.Form frmTemplateNew 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstRes 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1950
      ItemData        =   "TemplateNew.frx":0000
      Left            =   2640
      List            =   "TemplateNew.frx":0019
      TabIndex        =   20
      Top             =   4080
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.ListBox lstCat 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6150
      ItemData        =   "TemplateNew.frx":005D
      Left            =   3840
      List            =   "TemplateNew.frx":005F
      TabIndex        =   19
      Top             =   480
      Visible         =   0   'False
      Width           =   4695
   End
   Begin VB.ListBox lstSuper 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "TemplateNew.frx":0061
      Left            =   120
      List            =   "TemplateNew.frx":0063
      Sorted          =   -1  'True
      TabIndex        =   18
      Top             =   480
      Width           =   3720
   End
   Begin VB.ListBox lstWeek 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "TemplateNew.frx":0065
      Left            =   3960
      List            =   "TemplateNew.frx":0067
      Sorted          =   -1  'True
      TabIndex        =   17
      Top             =   4080
      Width           =   5280
   End
   Begin VB.ListBox lstSelected 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1950
      ItemData        =   "TemplateNew.frx":0069
      Left            =   120
      List            =   "TemplateNew.frx":006B
      TabIndex        =   15
      Top             =   4080
      Visible         =   0   'False
      Width           =   2475
   End
   Begin VB.ListBox lstDaily 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "TemplateNew.frx":006D
      Left            =   3960
      List            =   "TemplateNew.frx":006F
      Sorted          =   -1  'True
      TabIndex        =   14
      Top             =   4080
      Width           =   5280
   End
   Begin VB.ListBox lstWeekly 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "TemplateNew.frx":0071
      Left            =   120
      List            =   "TemplateNew.frx":0073
      Sorted          =   -1  'True
      TabIndex        =   12
      Top             =   480
      Width           =   3720
   End
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   6105
      Left            =   8640
      TabIndex        =   11
      Top             =   480
      Width           =   3360
      _Version        =   65537
      _ExtentX        =   5927
      _ExtentY        =   9340
      _StockProps     =   79
      Caption         =   "SSDay1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty CaptionFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TimeInterval    =   2
      AllowEdit       =   0   'False
      AllowAdd        =   0   'False
      AllowDelete     =   0   'False
   End
   Begin VB.ListBox lstDay 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1950
      ItemData        =   "TemplateNew.frx":0075
      Left            =   2640
      List            =   "TemplateNew.frx":008E
      TabIndex        =   9
      Top             =   4080
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.ListBox lstLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1950
      ItemData        =   "TemplateNew.frx":00D2
      Left            =   120
      List            =   "TemplateNew.frx":00D4
      TabIndex        =   7
      Top             =   4080
      Visible         =   0   'False
      Width           =   2475
   End
   Begin VB.ListBox lstPractice 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1920
      ItemData        =   "TemplateNew.frx":00D6
      Left            =   120
      List            =   "TemplateNew.frx":00D8
      Sorted          =   -1  'True
      TabIndex        =   4
      Top             =   480
      Width           =   3720
   End
   Begin VB.TextBox PracticeName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      MaxLength       =   64
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   3735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   120
      TabIndex        =   0
      Top             =   6840
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TemplateNew.frx":00DA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   9960
      TabIndex        =   1
      Top             =   6720
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TemplateNew.frx":02B9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5280
      TabIndex        =   2
      Top             =   6840
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TemplateNew.frx":0498
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWeekly 
      Height          =   495
      Left            =   1440
      TabIndex        =   13
      Top             =   2400
      Width           =   2355
      _Version        =   131072
      _ExtentX        =   4154
      _ExtentY        =   873
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TemplateNew.frx":0679
   End
   Begin VB.Label lblWarning 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Always Set Morning Hours First"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   8040
      TabIndex        =   22
      Top             =   120
      Width           =   3285
   End
   Begin VB.Label lblRes 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resource"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   2760
      TabIndex        =   21
      Top             =   3720
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblSelected 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Selected"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   16
      Top             =   3720
      Visible         =   0   'False
      Width           =   960
   End
   Begin VB.Label lblDay 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Day of Week"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   2400
      TabIndex        =   10
      Top             =   3720
      Visible         =   0   'False
      Width           =   1380
   End
   Begin VB.Label lblLoc 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   8
      Top             =   3720
      Visible         =   0   'False
      Width           =   930
   End
   Begin VB.Label lblTemplate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1005
   End
   Begin VB.Label lblName 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   5
      Top             =   2880
      Visible         =   0   'False
      Width           =   660
   End
End
Attribute VB_Name = "frmTemplateNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PracticeId As Long
Public TemplateType As String
Private TriggerOn As Boolean
Private AStart As String
Private AEnd As String
Private TimeArray(96) As String

Private Sub SetControls(DisplayType As Boolean)
Dim i As Integer
Dim Temp As String
lblTemplate.Visible = Not DisplayType
If (TemplateType = "T") Then
    lstPractice.Visible = Not DisplayType
    lstWeekly.Visible = False
    lstSuper.Visible = False
    lblDay.Visible = False
    lstDay.Visible = False
    lstDaily.Visible = False
    lstWeek.Visible = False
    lblSelected.Visible = False
    lstSelected.Visible = False
    lblLoc.Visible = False
    lstLoc.Visible = False
    lstCat.Visible = DisplayType
    lblRes.Visible = False
    lstRes.Visible = False
    lblWarning.Visible = DisplayType
    SSDay1.Visible = DisplayType
    For i = 1 To 96
        TimeArray(i) = Space(32)
    Next i
ElseIf (TemplateType = "W") Then
    lstPractice.Visible = False
    lstWeekly.Visible = Not DisplayType
    lstSuper.Visible = False
    lblDay.Visible = DisplayType
    lstDay.Visible = DisplayType
    lstDaily.Visible = DisplayType
    lstWeek.Visible = False
    lblSelected.Visible = DisplayType
    lstSelected.Visible = DisplayType
    SSDay1.Visible = False
    lblLoc.Visible = False
    lstLoc.Visible = False
    lblRes.Visible = False
    lstRes.Visible = False
    lblWarning.Visible = False
ElseIf (TemplateType = "S") Then
    lstPractice.Visible = False
    lstWeekly.Visible = False
    lstSuper.Visible = Not DisplayType
    lblDay.Visible = False
    lstDay.Visible = False
    lstDaily.Visible = False
    lstWeek.Visible = DisplayType
    lblSelected.Visible = DisplayType
    lstSelected.Visible = DisplayType
    lblRes.Visible = DisplayType
    lstRes.Visible = DisplayType
    SSDay1.Visible = False
    lblLoc.Visible = False
    lstLoc.Visible = False
    lblWarning.Visible = False
End If
cmdWeekly.Visible = Not DisplayType
cmdApply.Visible = DisplayType
lblName.Visible = DisplayType
PracticeName.Visible = DisplayType
cmdDelete.Visible = False
If (DisplayType) Then
    If (CheckConfigCollection("REMOVEACTIVE") = "T") And (PracticeId > 0) Then
        cmdDelete.Visible = True
    End If
End If
End Sub

Private Sub LoadTemplate(PracticeId As Long)
Dim ALoc As Long
Dim oDate As Boolean
Dim i As Integer, l As Integer
Dim k As Integer, z As Integer
Dim TheLoc As String, QTemp As String
Dim ATemp As String, Temp As String
Dim VacDay As String, Yr As String, PCity As String
Dim PCat1 As String, PCat2 As String
Dim PCat3 As String, PCat4 As String
Dim PCat5 As String, PCat6 As String
Dim PCat7 As String, PCat8 As String
Dim PName As String, PColor As String, PVac As String
Dim Ps1 As String, Ps2 As String, Ps3 As String, Ps4 As String, Ps5 As String, Ps6 As String, Ps7 As String
Dim Pe1 As String, Pe2 As String, Pe3 As String, Pe4 As String, Pe5 As String, Pe6 As String, Pe7 As String
Dim ApplTbl As ApplicationTables
Dim ApplSch As ApplicationScheduler
If (PracticeId >= 0) Then
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplGetTemplate(PracticeId, PName, PCity, PColor, PVac, oDate, Ps1, Ps2, Ps3, Ps4, Ps5, Ps6, Ps7, Pe1, Pe2, Pe3, Pe4, Pe5, Pe6, Pe7, ALoc, PCat1, PCat2, PCat3, PCat4, PCat5, PCat6, PCat7, PCat8)) Then
        PracticeName.Text = PName
        Call SetControls(True)
        If (TemplateType = "T") Then
            SSDay1.Caption = PracticeName.Text
            Set ApplSch = New ApplicationScheduler
            Set ApplSch.lstStartTime = lstLoc
            Call ApplSch.LoadLocList
            Set ApplSch = Nothing
            lstLoc.Visible = False
            SSDay1.x.Tasks.RemoveAll
            If (Trim(Ps1) <> "") And (Trim(Pe1) <> "") Then
                z = 0
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps1, Pe1, TheLoc
            End If
            If (Trim(Ps2) <> "") And (Trim(Pe2) <> "") Then
                z = 1
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps2, Pe2, TheLoc
            End If
            If (Trim(Ps3) <> "") And (Trim(Pe3) <> "") Then
                z = 2
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps3, Pe3, TheLoc
            End If
            If (Trim(Ps4) <> "") And (Trim(Pe4) <> "") Then
                z = 3
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps4, Pe4, TheLoc
            End If
            If (Trim(Ps5) <> "") And (Trim(Pe5) <> "") Then
                z = 4
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps5, Pe5, TheLoc
            End If
            If (Trim(Ps6) <> "") And (Trim(Pe7) <> "") Then
                z = 5
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps6, Pe6, TheLoc
            End If
            If (Trim(Ps7) <> "") And (Trim(Pe7) <> "") Then
                z = 6
                GoSub SetLoc
                SSDay1.x.Tasks.Add Ps7, Pe7, TheLoc
            End If
            GoSub SetCat
        ElseIf (TemplateType = "W") Then
            If (Trim(Ps1) <> "") Then
                Temp = ""
                ATemp = Ps1
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe1) <> "") Then
                Temp = ""
                ATemp = Pe1
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps2) <> "") Then
                Temp = ""
                ATemp = Ps2
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe2) <> "") Then
                Temp = ""
                ATemp = Pe2
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps3) <> "") Then
                Temp = ""
                ATemp = Ps3
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe3) <> "") Then
                Temp = ""
                ATemp = Pe3
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps4) <> "") Then
                Temp = ""
                ATemp = Ps4
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe4) <> "") Then
                Temp = ""
                ATemp = Pe4
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps5) <> "") Then
                Temp = ""
                ATemp = Ps5
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe5) <> "") Then
                Temp = ""
                ATemp = Pe5
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps6) <> "") Then
                Temp = ""
                ATemp = Ps6
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe6) <> "") Then
                Temp = ""
                ATemp = Pe6
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps7) <> "") Then
                Temp = ""
                ATemp = Ps7
                QTemp = ""
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe7) <> "") Then
                Temp = ""
                ATemp = Pe7
                GoSub GetDay
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
        ElseIf (TemplateType = "S") Then
            If (Trim(Ps1) <> "") Then
                Temp = ""
                ATemp = Ps1
                QTemp = Mid(PCat1, 1, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe1) <> "") Then
                Temp = ""
                ATemp = Pe1
                QTemp = Mid(PCat1, 5, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps2) <> "") Then
                Temp = ""
                ATemp = Ps2
                QTemp = Mid(PCat1, 9, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe2) <> "") Then
                Temp = ""
                ATemp = Pe2
                QTemp = Mid(PCat1, 13, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps3) <> "") Then
                Temp = ""
                ATemp = Ps3
                QTemp = Mid(PCat1, 17, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe3) <> "") Then
                Temp = ""
                ATemp = Pe3
                QTemp = Mid(PCat1, 21, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps4) <> "") Then
                Temp = ""
                ATemp = Ps4
                QTemp = Mid(PCat1, 25, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe4) <> "") Then
                Temp = ""
                ATemp = Pe4
                QTemp = Mid(PCat1, 29, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps5) <> "") Then
                Temp = ""
                ATemp = Ps5
                QTemp = Mid(PCat1, 33, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe5) <> "") Then
                Temp = ""
                ATemp = Pe5
                QTemp = Mid(PCat1, 37, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps6) <> "") Then
                Temp = ""
                ATemp = Ps6
                QTemp = Mid(PCat1, 41, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe6) <> "") Then
                Temp = ""
                ATemp = Pe6
                QTemp = Mid(PCat1, 45, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Ps7) <> "") Then
                Temp = ""
                ATemp = Ps7
                QTemp = Mid(PCat1, 49, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
            If (Trim(Pe7) <> "") Then
                Temp = ""
                ATemp = Pe7
                QTemp = Mid(PCat1, 53, 4)
                GoSub GetTmp
                If (Temp <> "") Then
                    lstSelected.AddItem Temp
                End If
            End If
        End If
    End If
    PracticeName.SetFocus
    SendKeys "{Home}"
    Set ApplTbl = Nothing
End If
Exit Sub
SetCat:
    If (Trim(PCat1) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat1, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat1, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 1, 2) = Mid(PCat1, z + 1, 1) + "S"
                End If
                If (Trim(Mid(PCat1, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 3, 2) = Mid(PCat1, z + 2, 1) + "I"
                End If
                If (Trim(Mid(PCat1, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 5, 2) = Mid(PCat1, z + 3, 1) + "L"
                End If
                If (Trim(Mid(PCat1, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 7, 2) = Mid(PCat1, z + 4, 1) + "E"
                End If
            End If
        Next z
    End If
    If (Trim(PCat3) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat3, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat3, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 9, 2) = Mid(PCat3, z + 1, 1) + "W"
                End If
                If (Trim(Mid(PCat3, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 11, 2) = Mid(PCat3, z + 2, 1) + "X"
                End If
                If (Trim(Mid(PCat3, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 13, 2) = Mid(PCat3, z + 3, 1) + "Y"
                End If
                If (Trim(Mid(PCat3, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 15, 2) = Mid(PCat3, z + 4, 1) + "Z"
                End If
            End If
        Next z
    End If
    If (Trim(PCat5) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat5, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat5, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 17, 2) = Mid(PCat5, z + 1, 1) + "A"
                End If
                If (Trim(Mid(PCat5, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 19, 2) = Mid(PCat5, z + 2, 1) + "B"
                End If
                If (Trim(Mid(PCat5, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 21, 2) = Mid(PCat5, z + 3, 1) + "C"
                End If
                If (Trim(Mid(PCat5, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 23, 2) = Mid(PCat5, z + 4, 1) + "D"
                End If
            End If
        Next z
    End If
    If (Trim(PCat7) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat7, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat7, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 25, 2) = Mid(PCat7, z + 1, 1) + "M"
                End If
                If (Trim(Mid(PCat7, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 27, 2) = Mid(PCat7, z + 2, 1) + "N"
                End If
                If (Trim(Mid(PCat7, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 29, 2) = Mid(PCat7, z + 3, 1) + "O"
                End If
                If (Trim(Mid(PCat7, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1), 31, 2) = Mid(PCat7, z + 4, 1) + "P"
                End If
            End If
        Next z
    End If
    
    If (Trim(PCat2) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat2, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat2, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 1, 2) = Mid(PCat2, z + 1, 1) + "S"
                End If
                If (Trim(Mid(PCat2, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 3, 2) = Mid(PCat2, z + 2, 1) + "I"
                End If
                If (Trim(Mid(PCat2, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 5, 2) = Mid(PCat2, z + 3, 1) + "L"
                End If
                If (Trim(Mid(PCat2, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 7, 2) = Mid(PCat2, z + 4, 1) + "E"
                End If
            End If
        Next z
    End If
    If (Trim(PCat4) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat4, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat4, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 9, 2) = Mid(PCat4, z + 1, 1) + "W"
                End If
                If (Trim(Mid(PCat4, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 11, 2) = Mid(PCat4, z + 2, 1) + "X"
                End If
                If (Trim(Mid(PCat4, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 13, 2) = Mid(PCat4, z + 3, 1) + "Y"
                End If
                If (Trim(Mid(PCat4, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 15, 2) = Mid(PCat4, z + 4, 1) + "Z"
                End If
            End If
        Next z
    End If
    If (Trim(PCat6) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat6, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat6, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 17, 2) = Mid(PCat6, z + 1, 1) + "A"
                End If
                If (Trim(Mid(PCat6, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 19, 2) = Mid(PCat6, z + 2, 1) + "B"
                End If
                If (Trim(Mid(PCat6, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 21, 2) = Mid(PCat6, z + 3, 1) + "C"
                End If
                If (Trim(Mid(PCat6, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 23, 2) = Mid(PCat6, z + 4, 1) + "D"
                End If
            End If
        Next z
    End If
    If (Trim(PCat8) <> "") Then
        For z = 0 To 191 Step 4
            If (Trim(Mid(PCat8, z + 1, 4)) <> "") Then
                If (Trim(Mid(PCat8, z + 1, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 25, 2) = Mid(PCat8, z + 1, 1) + "M"
                End If
                If (Trim(Mid(PCat8, z + 2, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 27, 2) = Mid(PCat8, z + 2, 1) + "N"
                End If
                If (Trim(Mid(PCat8, z + 3, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 29, 2) = Mid(PCat8, z + 3, 1) + "O"
                End If
                If (Trim(Mid(PCat8, z + 4, 1)) <> "") Then
                    Mid(TimeArray((z / 4) + 1 + 48), 31, 2) = Mid(PCat8, z + 4, 1) + "P"
                End If
            End If
        Next z
    End If
    For z = 1 To 96
        If (Trim(TimeArray(z)) <> "") Then
            lstCat.List(z + 1) = Left(lstCat.List(z + 1), 16) + " " + TimeArray(z)
        End If
    Next z
    Return
SetLoc:
    TheLoc = ""
    ALoc = val(Trim(Mid(PCity, (z * 4) + 1, 4)))
    For z = 0 To lstLoc.ListCount - 1
        If (ALoc = val(Trim(Mid(lstLoc.List(z), 56, 5)))) Then
            TheLoc = "(" + Trim(Left(lstLoc.List(z), 20)) + " [" + Trim(Str(ALoc)) + "])"
            Exit For
        End If
    Next z
    Return
GetTmp:
    If (Temp <> "") Or (TemplateType = "S") Then
        If (TemplateType = "S") Then
            Temp = Space(75)
            For k = 0 To lstRes.ListCount - 1
                If (val(Trim(QTemp)) = val(Trim(Mid(lstRes.List(k), 57, 5)))) Then
                    Mid(Temp, 18, 10) = Trim(Left(lstRes.List(k), 10))
                    Temp = Temp + QTemp + "/"
                    Exit For
                End If
            Next k
            For k = 0 To lstWeek.ListCount - 1
                If (val(Trim(ATemp)) = val(Trim(Mid(lstWeek.List(k), 70, 5)))) Then
                    Mid(Temp, 1, 16) = Trim(Left(lstWeek.List(k), 16))
                    Temp = Temp + ATemp
                    Exit For
                End If
            Next k
        ElseIf (TemplateType = "W") Then
            For k = 0 To lstDaily.ListCount - 1
                If (val(Trim(Mid(ATemp, 2, Len(ATemp) - 1))) = val(Trim(Mid(lstDaily.List(k), 70, 5)))) Then
                    Temp = Temp + Trim(Left(lstDaily.List(k), 32))
                    Temp = Temp + Space(75 - Len(Temp)) + ATemp
                    Exit For
                End If
            Next k
        End If
    End If
    Return
GetDay:
    k = Asc(Left(ATemp, 1)) - 65
    If (k < 7) Then
        Temp = Temp + lstDay.List(k) + " : "
    End If
    Return
End Sub

Public Function LoadTemplateDisplay(TType As String) As Boolean
Dim i As Integer
Dim ApplTbl As ApplicationTables
LoadTemplateDisplay = True
TemplateType = TType
AStart = ""
AEnd = ""
lstCat.Clear
Call LoadTime(lstCat, "12:00 AM", "11:59 PM", 15)
TriggerOn = True
lstSelected.Clear
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPractice
Call ApplTbl.ApplLoadPracticeList("T", True)
Set ApplTbl = Nothing
lstDaily.Clear
For i = 1 To lstPractice.ListCount - 1
    lstDaily.AddItem lstPractice.List(i)
Next i
lstDaily.ListIndex = -1
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstWeekly
Call ApplTbl.ApplLoadPracticeList("W", True)
Set ApplTbl = Nothing
lstWeek.Clear
For i = 1 To lstWeekly.ListCount - 1
    lstWeek.AddItem lstWeekly.List(i)
Next i
lstWeek.ListIndex = -1
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstSuper
Call ApplTbl.ApplLoadPracticeList("S", True)
Set ApplTbl = Nothing
lstRes.Clear
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstRes
Call ApplTbl.ApplLoadResource("D", False, False, False, True, False)
Set ApplTbl = Nothing
If (TemplateType = "T") Then
    lstPractice.Visible = True
    lstWeekly.Visible = False
    lstSuper.Visible = False
    lblTemplate.Caption = "Daily Template"
    lblName.Caption = "Daily Template Name"
    lblRes.Visible = False
    lstRes.Visible = False
    cmdWeekly.Visible = True
ElseIf (TemplateType = "W") Then
    lstPractice.Visible = False
    lstWeekly.Visible = True
    lstSuper.Visible = False
    lblTemplate.Caption = "Weekly Template"
    lblName.Caption = "Weekly Template Name"
    lblRes.Visible = False
    lstRes.Visible = False
    cmdWeekly.Visible = True
ElseIf (TemplateType = "S") Then
    lstPractice.Visible = False
    lstWeekly.Visible = False
    lstSuper.Visible = True
    lblTemplate.Caption = "Super Template"
    lblName.Caption = "Super Template Name"
    lblRes.Visible = False
    lstRes.Visible = False
    cmdWeekly.Visible = True
End If
Call SetControls(False)
End Function

Private Sub cmdBack_Click()
Unload frmTemplateNew
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
If (PracticeId > 0) Then
    Set ApplTbl = New ApplicationTables
    Call ApplTbl.ApplDeletePractice(PracticeId)
    Set ApplTbl = Nothing
    PracticeId = 0
    Unload frmTemplateNew
End If
End Sub

Private Sub cmdApply_Click()
Dim TheLoc As Long
Dim oDate As Boolean
Dim p As Integer, z As Integer
Dim k As Integer, i As Integer
Dim PVac As String, ZTemp As String
Dim PCat1 As String, PCat2 As String
Dim PCat3 As String, PCat4 As String
Dim PCat5 As String, PCat6 As String
Dim PCat7 As String, PCat8 As String
Dim dd As String, mm As String, ATemp As String
Dim VacDay As String, Temp As String, ALoc As String
Dim Ps1 As String, Ps2 As String, Ps3 As String, Ps4 As String, Ps5 As String, Ps6 As String, Ps7 As String
Dim Pe1 As String, Pe2 As String, Pe3 As String, Pe4 As String, Pe5 As String, Pe6 As String, Pe7 As String
Dim ApplTbl As ApplicationTables
If (Trim(PracticeName.Text) = "") Then
    If ((lstSelected.Visible) And (lstSelected.ListCount > 0)) Or ((SSDay1.Visible) And (SSDay1.Tasks.Count > 0)) Then
        frmEventMsgs.Header = "Please select a name"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        PracticeName.SetFocus
        SendKeys "{Home}"
    Else
        Unload frmTemplateNew
    End If
    Exit Sub
End If
Set ApplTbl = New ApplicationTables
If (PracticeId < 1) Then
    If (ApplTbl.ApplIsTemplate(PracticeName.Text)) Then
        frmEventMsgs.Header = "Template Already Exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplTbl = Nothing
        PracticeName.SetFocus
        SendKeys "{Home}+{End}"
        Exit Sub
    End If
End If
If (TemplateType = "T") Then
    ALoc = ""
    TheLoc = 0
    For i = 0 To SSDay1.Tasks.Count - 1
        If (i = 0) Then
            GoSub GetLoc
            Ps1 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps1) = 7) Then
                Ps1 = "0" + Ps1
            End If
            Pe1 = SSDay1.Tasks(i).EndTime
            If (Len(Pe1) = 7) Then
                Pe1 = "0" + Pe1
            End If
        ElseIf (i = 1) Then
            GoSub GetLoc
            Ps2 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps2) = 7) Then
                Ps2 = "0" + Ps2
            End If
            Pe2 = SSDay1.Tasks(i).EndTime
            If (Len(Pe2) = 7) Then
                Pe2 = "0" + Pe2
            End If
        ElseIf (i = 2) Then
            GoSub GetLoc
            Ps3 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps3) = 7) Then
                Ps3 = "0" + Ps3
            End If
            Pe3 = SSDay1.Tasks(i).EndTime
            If (Len(Pe3) = 7) Then
                Pe3 = "0" + Pe3
            End If
        ElseIf (i = 3) Then
            GoSub GetLoc
            Ps4 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps4) = 7) Then
                Ps4 = "0" + Ps4
            End If
            Pe4 = SSDay1.Tasks(i).EndTime
            If (Len(Pe4) = 7) Then
                Pe4 = "0" + Pe4
            End If
        ElseIf (i = 4) Then
            GoSub GetLoc
            Ps5 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps5) = 7) Then
                Ps5 = "0" + Ps5
            End If
            Pe5 = SSDay1.Tasks(i).EndTime
            If (Len(Pe5) = 7) Then
                Pe5 = "0" + Pe5
            End If
        ElseIf (i = 5) Then
            GoSub GetLoc
            Ps6 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps5) = 7) Then
                Ps6 = "0" + Ps6
            End If
            Pe6 = SSDay1.Tasks(i).EndTime
            If (Len(Pe6) = 7) Then
                Pe6 = "0" + Pe6
            End If
        ElseIf (i = 6) Then
            GoSub GetLoc
            Ps7 = SSDay1.Tasks(i).BeginTime
            If (Len(Ps7) = 7) Then
                Ps7 = "0" + Ps7
            End If
            Pe7 = SSDay1.Tasks(i).EndTime
            If (Len(Pe7) = 7) Then
                Pe7 = "0" + Pe7
            End If
        End If
    Next i
    PCat1 = ""
    PCat3 = ""
    PCat5 = ""
    PCat7 = ""
    For i = 1 To 48
        PCat1 = PCat1 + Mid(TimeArray(i), 1, 1) + Mid(TimeArray(i), 3, 1) + Mid(TimeArray(i), 5, 1) + Mid(TimeArray(i), 7, 1)
        PCat3 = PCat3 + Mid(TimeArray(i), 9, 1) + Mid(TimeArray(i), 11, 1) + Mid(TimeArray(i), 13, 1) + Mid(TimeArray(i), 15, 1)
        PCat5 = PCat5 + Mid(TimeArray(i), 17, 1) + Mid(TimeArray(i), 19, 1) + Mid(TimeArray(i), 21, 1) + Mid(TimeArray(i), 23, 1)
        PCat7 = PCat7 + Mid(TimeArray(i), 25, 1) + Mid(TimeArray(i), 27, 1) + Mid(TimeArray(i), 29, 1) + Mid(TimeArray(i), 31, 1)
    Next i
    PCat2 = ""
    PCat4 = ""
    PCat6 = ""
    PCat8 = ""
    For i = 49 To 96
        PCat2 = PCat2 + Mid(TimeArray(i), 1, 1) + Mid(TimeArray(i), 3, 1) + Mid(TimeArray(i), 5, 1) + Mid(TimeArray(i), 7, 1)
        PCat4 = PCat4 + Mid(TimeArray(i), 9, 1) + Mid(TimeArray(i), 11, 1) + Mid(TimeArray(i), 13, 1) + Mid(TimeArray(i), 15, 1)
        PCat6 = PCat6 + Mid(TimeArray(i), 17, 1) + Mid(TimeArray(i), 19, 1) + Mid(TimeArray(i), 21, 1) + Mid(TimeArray(i), 23, 1)
        PCat8 = PCat8 + Mid(TimeArray(i), 25, 1) + Mid(TimeArray(i), 27, 1) + Mid(TimeArray(i), 29, 1) + Mid(TimeArray(i), 31, 1)
    Next i
    Call ApplTbl.ApplPostTemplate(PracticeId, PracticeName.Text, ALoc, "", "", False, Ps1, Ps2, Ps3, Ps4, Ps5, Ps6, Ps7, Pe1, Pe2, Pe3, Pe4, Pe5, Pe6, Pe7, TheLoc, PCat1, PCat2, PCat3, PCat4, PCat5, PCat6, PCat7, PCat8)
ElseIf (TemplateType = "W") Then
    k = 0
    For i = 0 To lstSelected.ListCount - 1
        Temp = Mid(lstSelected.List(i), 76, Len(lstSelected.List(i)) - 75)
        k = k + 1
        If (k = 1) Then
            Ps1 = Temp
        ElseIf (k = 2) Then
            Pe1 = Temp
        ElseIf (k = 3) Then
            Ps2 = Temp
        ElseIf (k = 4) Then
            Pe2 = Temp
        ElseIf (k = 5) Then
            Ps3 = Temp
        ElseIf (k = 6) Then
            Pe3 = Temp
        ElseIf (k = 7) Then
            Ps4 = Temp
        ElseIf (k = 8) Then
            Pe4 = Temp
        ElseIf (k = 9) Then
            Ps5 = Temp
        ElseIf (k = 10) Then
            Pe5 = Temp
        ElseIf (k = 11) Then
            Ps6 = Temp
        ElseIf (k = 12) Then
            Pe6 = Temp
        ElseIf (k = 13) Then
            Ps7 = Temp
        ElseIf (k = 14) Then
            Pe7 = Temp
        End If
    Next i
    Call ApplTbl.ApplPostTemplate(PracticeId, PracticeName.Text, "", "", "-", False, Ps1, Ps2, Ps3, Ps4, Ps5, Ps6, Ps7, Pe1, Pe2, Pe3, Pe4, Pe5, Pe6, Pe7, 1, "", "", "", "", "", "", "", "")
ElseIf (TemplateType = "S") Then
    k = 0
    ALoc = ""
    For i = 0 To lstSelected.ListCount - 1
        ZTemp = Mid(lstSelected.List(i), 76, Len(lstSelected.List(i)) - 75)
        p = InStrPS(76, lstSelected.List(i), "/")
        If (p > 0) Then
            Temp = Trim(Mid(lstSelected.List(i), p + 1, Len(lstSelected.List(i)) - p))
            ZTemp = Trim(Mid(lstSelected.List(i), 76, (p - 1) - 75))
            ZTemp = ZTemp + Space(4 - Len(ZTemp))
            ALoc = ALoc + ZTemp
            k = k + 1
            If (k = 1) Then
                Ps1 = Temp
            ElseIf (k = 2) Then
                Pe1 = Temp
            ElseIf (k = 3) Then
                Ps2 = Temp
            ElseIf (k = 4) Then
                Pe2 = Temp
            ElseIf (k = 5) Then
                Ps3 = Temp
            ElseIf (k = 6) Then
                Pe3 = Temp
            ElseIf (k = 7) Then
                Ps4 = Temp
            ElseIf (k = 8) Then
                Pe4 = Temp
            ElseIf (k = 9) Then
                Ps5 = Temp
            ElseIf (k = 10) Then
                Pe5 = Temp
            ElseIf (k = 11) Then
                Ps6 = Temp
            ElseIf (k = 12) Then
                Pe6 = Temp
            ElseIf (k = 13) Then
                Ps7 = Temp
            ElseIf (k = 14) Then
                Pe7 = Temp
            End If
        End If
    Next i
    Call ApplTbl.ApplPostTemplate(PracticeId, PracticeName.Text, "", "", "/", False, Ps1, Ps2, Ps3, Ps4, Ps5, Ps6, Ps7, Pe1, Pe2, Pe3, Pe4, Pe5, Pe6, Pe7, 2, ALoc, "", "", "", "", "", "", "")
End If
Set ApplTbl = Nothing
Call LoadTemplateDisplay(TemplateType)
Exit Sub
GetLoc:
    If (i >= 0) Then
        z = InStrPS(SSDay1.Tasks(i).Text, "[")
        If (z > 0) Then
            p = InStrPS(SSDay1.Tasks(i).Text, "]")
            If (p > 0) Then
                ATemp = Trim(Mid(SSDay1.Tasks(i).Text, z + 1, (p - 1) - z))
                ATemp = ATemp + Space(4 - Len(ATemp))
                ALoc = ALoc + ATemp
            End If
        Else
            ALoc = ALoc + "0   "
        End If
    End If
    Return
End Sub

Private Sub cmdWeekly_Click()
frmEventMsgs.Header = "Template Type"
frmEventMsgs.AcceptText = "Daily"
frmEventMsgs.RejectText = "Weekly"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = "Super"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    TemplateType = "T"
    Call LoadTemplateDisplay(TemplateType)
ElseIf (frmEventMsgs.Result = 2) Then
    TemplateType = "W"
    Call LoadTemplateDisplay(TemplateType)
ElseIf (frmEventMsgs.Result = 3) Then
    TemplateType = "S"
    Call LoadTemplateDisplay(TemplateType)
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmTemplateNew.BorderStyle = 1
    frmTemplateNew.ClipControls = True
    frmTemplateNew.Caption = Mid(frmTemplateNew.Name, 4, Len(frmTemplateNew.Name) - 3)
    frmTemplateNew.AutoRedraw = True
    frmTemplateNew.Refresh
End If
End Sub

Private Sub lstCat_Click()
Dim ATime As String
Dim Temp As String
Dim TimeSlot As Integer
If (lstCat.ListIndex > 0) Then
    ATime = Left(lstCat.List(lstCat.ListIndex), 8)
    TimeSlot = SSDay1.IndexFromTime(ATime)
    If (TriggerOn) And (IsInsideTaskWindow(ATime)) Then
        frmNumericPad.NumPad_ACategoryOn = True
        frmNumericPad.NumPad_Field = "Category " + ATime + " " + TimeArray(TimeSlot)
        frmNumericPad.NumPad_Min = 1
        frmNumericPad.NumPad_Max = 9
        frmNumericPad.Show 1
        If Not (frmNumericPad.NumPad_Quit) Then
            If (frmNumericPad.NumPad_Result <> "") And Not (frmNumericPad.NumPad_ChoiceOn9) Then
                Temp = Trim(frmNumericPad.NumPad_Result)
                If (frmNumericPad.NumPad_ChoiceOn1) Then
                    Temp = Temp + "S"
                    Mid(TimeArray(TimeSlot), 1, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn2) Then
                    Temp = Temp + "I"
                    Mid(TimeArray(TimeSlot), 3, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn3) Then
                    Temp = Temp + "L"
                    Mid(TimeArray(TimeSlot), 5, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn4) Then
                    Temp = Temp + "E"
                    Mid(TimeArray(TimeSlot), 7, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn5) Then
                    Temp = Temp + "W"
                    Mid(TimeArray(TimeSlot), 9, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn6) Then
                    Temp = Temp + "X"
                    Mid(TimeArray(TimeSlot), 11, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn7) Then
                    Temp = Temp + "Y"
                    Mid(TimeArray(TimeSlot), 13, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn8) Then
                    Temp = Temp + "Z"
                    Mid(TimeArray(TimeSlot), 15, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn10) Then
                    Temp = Temp + "A"
                    Mid(TimeArray(TimeSlot), 17, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn11) Then
                    Temp = Temp + "B"
                    Mid(TimeArray(TimeSlot), 19, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn12) Then
                    Temp = Temp + "C"
                    Mid(TimeArray(TimeSlot), 21, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn13) Then
                    Temp = Temp + "D"
                    Mid(TimeArray(TimeSlot), 23, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn14) Then
                    Temp = Temp + "M"
                    Mid(TimeArray(TimeSlot), 25, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn15) Then
                    Temp = Temp + "N"
                    Mid(TimeArray(TimeSlot), 27, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn16) Then
                    Temp = Temp + "O"
                    Mid(TimeArray(TimeSlot), 29, 2) = Left(Temp, 2)
                ElseIf (frmNumericPad.NumPad_ChoiceOn17) Then
                    Temp = Temp + "P"
                    Mid(TimeArray(TimeSlot), 31, 2) = Left(Temp, 2)
                Else
                    Temp = ""
                End If
                If (Trim(Temp) <> "") Then
                    lstCat.List(TimeSlot + 1) = Left(lstCat.List(TimeSlot + 1), 8) + " " + TimeArray(TimeSlot)
                End If
            ElseIf (frmNumericPad.NumPad_ChoiceOn9) Then
                lstCat.List(lstCat.ListIndex) = Left(lstCat.List(lstCat.ListIndex), 8)
                TimeArray(lstCat.ListIndex - 1) = Space(32)
            End If
        End If
    End If
End If
End Sub

Private Sub lstDaily_Click()
Dim DisplayText As String
If (lstDaily.ListIndex >= 0) And (lstSelected.ListCount < 14) Then
    If (lstDay.ListIndex >= 0) Then
        DisplayText = Trim(lstDay.List(lstDay.ListIndex)) + " : " + Trim(Left(lstDaily.List(lstDaily.ListIndex), 32))
        DisplayText = DisplayText + Space(75 - Len(DisplayText))
        DisplayText = DisplayText + Chr(lstDay.ListIndex + 65) + Trim(Mid(lstDaily.List(lstDaily.ListIndex), 70, 5))
        lstSelected.AddItem DisplayText
        lstDay.ListIndex = -1
        lstDaily.ListIndex = -1
    End If
ElseIf (lstSelected.ListCount >= 14) Then
    frmEventMsgs.Header = "Only 14 Daily Templates Allowed."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub lstDay_Click()
Dim DisplayText As String
If (lstDay.ListIndex >= 0) And (lstSelected.ListCount < 14) Then
    If (lstDaily.ListIndex >= 0) Then
        DisplayText = Trim(lstDay.List(lstDay.ListIndex)) + " : " + Trim(Left(lstDaily.List(lstDaily.ListIndex), 32))
        DisplayText = DisplayText + Space(75 - Len(DisplayText))
        DisplayText = DisplayText + Chr(lstDay.ListIndex + 65) + Trim(Mid(lstDaily.List(lstDaily.ListIndex), 70, 5))
        lstSelected.AddItem DisplayText
        lstDay.ListIndex = -1
        lstDaily.ListIndex = -1
    End If
ElseIf (lstSelected.ListCount >= 14) Then
    frmEventMsgs.Header = "Only 14 Daily Templates Allowed."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub lstLoc_Click()
Dim i As Integer
Dim p As Integer
Dim Temp As String
Dim ATemp As String
If (lstLoc.ListIndex >= 0) Then
    Temp = "(" + Trim(Left(lstLoc.List(lstLoc.ListIndex), 20)) + " [" + Trim(Mid(lstLoc.List(lstLoc.ListIndex), 57, 5)) + "])"
    If (SSDay1.TaskSelected >= 0) Then
        ATemp = SSDay1.Tasks(SSDay1.TaskSelected).Text
        p = InStrPS(ATemp, "]")
        If (p > 0) Then
            ATemp = Temp + Mid(ATemp, p + 1, Len(ATemp) - p)
        Else
            ATemp = Temp + " " + ATemp
        End If
        SSDay1.Tasks(SSDay1.TaskSelected).Text = ATemp
    End If
    lblLoc.Visible = False
    lstLoc.Visible = False
End If
End Sub

Private Sub lstWeek_Click()
Dim DisplayText As String
If (lstWeek.ListIndex >= 0) And (lstSelected.ListCount < 14) Then
    If (lstRes.ListIndex >= 0) Then
        DisplayText = Space(75)
        Mid(DisplayText, 1, 16) = Trim(Left(lstWeek.List(lstWeek.ListIndex), 16))
        Mid(DisplayText, 18, 10) = Trim(Left(lstRes.List(lstRes.ListIndex), 10))
        DisplayText = DisplayText + Trim(Mid(lstRes.List(lstRes.ListIndex), 57, 5)) + "/" + Trim(Mid(lstWeek.List(lstWeek.ListIndex), 70, 5))
        lstSelected.AddItem DisplayText
        lstRes.ListIndex = -1
    Else
        frmEventMsgs.Header = "Select Resource"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
ElseIf (lstSelected.ListCount >= 14) Then
    frmEventMsgs.Header = "Only 14 Daily Templates Allowed."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub lstPractice_Click()
If (lstPractice.ListIndex > 0) Then
    PracticeId = val(Trim(Mid(lstPractice.List(lstPractice.ListIndex), 70, 5)))
    Call LoadTemplate(PracticeId)
ElseIf (lstPractice.ListIndex = 0) Then
    PracticeId = 0
    Call LoadTemplate(PracticeId)
End If
End Sub

Private Sub lstSelected_DblClick()
If (lstSelected.ListIndex >= 0) Then
    lstSelected.RemoveItem lstSelected.ListIndex
End If
End Sub

Private Sub lstSuper_Click()
If (lstSuper.ListIndex > 0) Then
    PracticeId = val(Trim(Mid(lstSuper.List(lstSuper.ListIndex), 70, 5)))
    Call LoadTemplate(PracticeId)
ElseIf (lstSuper.ListIndex = 0) Then
    PracticeId = 0
    Call LoadTemplate(PracticeId)
End If
End Sub

Private Sub lstWeekly_Click()
If (lstWeekly.ListIndex > 0) Then
    PracticeId = val(Trim(Mid(lstWeekly.List(lstWeekly.ListIndex), 70, 5)))
    Call LoadTemplate(PracticeId)
ElseIf (lstWeekly.ListIndex = 0) Then
    PracticeId = 0
    Call LoadTemplate(PracticeId)
End If
End Sub

Private Sub PracticeName_Validate(Cancel As Boolean)
If (Trim(PracticeName.Text) <> "") Then
    If (Trim(SSDay1.Caption) = "") Then
        SSDay1.Caption = PracticeName.Text
    End If
End If
End Sub

Private Sub SSDay1_DblClick()
Dim i As Integer
Dim TimeSlot1 As Integer
Dim TimeSlot2 As Integer
Dim Temp As String
Dim ATemp As String
Temp = ""
ATemp = ""
If ((PracticeId = 0) Or (SSDay1.TaskSelected >= 0)) Then
    frmEventMsgs.Header = "Set Details"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = ""
    If (SSDay1.TaskSelected >= 0) Then
        frmEventMsgs.AcceptText = "Location"
        frmEventMsgs.RejectText = "Delete"
    End If
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    If (PracticeId = 0) Then
        frmEventMsgs.Other2Text = "Add Start Time"
        frmEventMsgs.Other3Text = "Add End Time"
    End If
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        lstLoc.ListIndex = -1
        lstLoc.Visible = True
        lblLoc.Visible = True
    ElseIf (frmEventMsgs.Result = 2) Then
        TimeSlot1 = SSDay1.IndexFromTime(SSDay1.x.Tasks(SSDay1.TaskSelected).BeginTime)
        TimeSlot2 = SSDay1.IndexFromTime(SSDay1.x.Tasks(SSDay1.TaskSelected).EndTime)
        For i = TimeSlot1 To TimeSlot2
            TimeArray(i) = ""
            lstCat.List(i + 1) = Left(lstCat.List(i + 1), 9) + Space(12)
        Next i
        SSDay1.x.Tasks.Remove SSDay1.TaskSelected
    ElseIf (frmEventMsgs.Result = 6) Then
        AStart = SSDay1.TimeFromIndex(SSDay1.TimeSlotIndex)
        If (Trim(AEnd) <> "") Then
            SSDay1.x.Tasks.Add AStart, AEnd, ""
            AStart = ""
            AEnd = ""
        End If
    ElseIf (frmEventMsgs.Result = 7) Then
        AEnd = SSDay1.TimeFromIndex(SSDay1.TimeSlotIndex)
        If (Trim(AStart) <> "") And (ConvertTimeToMinutes(AStart) < ConvertTimeToMinutes(AEnd)) Then
            SSDay1.x.Tasks.Add AStart, AEnd, ""
            AStart = ""
            AEnd = ""
        Else
            AEnd = ""
            frmEventMsgs.Header = "Start Date must precede End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
ElseIf ((PracticeId > 0) And (SSDay1.TaskSelected < 0)) Then
    frmEventMsgs.Header = "Set Details"
    frmEventMsgs.AcceptText = "Add Start Time"
    frmEventMsgs.RejectText = "Add End Time"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        AStart = SSDay1.TimeFromIndex(SSDay1.TimeSlotIndex)
        If (Trim(AEnd) <> "") Then
            SSDay1.x.Tasks.Add AStart, AEnd, ""
            AStart = ""
            AEnd = ""
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        AEnd = SSDay1.TimeFromIndex(SSDay1.TimeSlotIndex)
        If (Trim(AStart) <> "") And (ConvertTimeToMinutes(AStart) < ConvertTimeToMinutes(AEnd)) Then
            SSDay1.x.Tasks.Add AStart, AEnd, ""
            AStart = ""
            AEnd = ""
        Else
            AEnd = ""
            frmEventMsgs.Header = "Start Date must precede End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End If
End Sub

Private Sub SSDay1_TimeBtnClick(Time As String)
Dim TimeSlot As Integer
Dim Temp As String
If (Trim(Time) <> "") And (IsInsideTaskWindow(Time)) Then
    TriggerOn = False
    TimeSlot = SSDay1.IndexFromTime(Time)
    lstCat.ListIndex = TimeSlot + 1
    TriggerOn = True
    frmNumericPad.NumPad_ACategoryOn = True
    frmNumericPad.NumPad_Field = "Category " + Time + " " + TimeArray(TimeSlot)
    frmNumericPad.NumPad_Min = 1
    frmNumericPad.NumPad_Max = 9
    frmNumericPad.Show 1
    If Not (frmNumericPad.NumPad_Quit) Then
        If (frmNumericPad.NumPad_Result <> "") And Not (frmNumericPad.NumPad_ChoiceOn5) Then
            Temp = Trim(frmNumericPad.NumPad_Result)
            If (frmNumericPad.NumPad_ChoiceOn1) Then
                Temp = Temp + "S"
                Mid(TimeArray(TimeSlot), 1, 2) = Left(Temp, 2)
            ElseIf (frmNumericPad.NumPad_ChoiceOn2) Then
                Temp = Temp + "I"
                Mid(TimeArray(TimeSlot), 3, 2) = Left(Temp, 2)
            ElseIf (frmNumericPad.NumPad_ChoiceOn3) Then
                Temp = Temp + "L"
                Mid(TimeArray(TimeSlot), 5, 2) = Left(Temp, 2)
            ElseIf (frmNumericPad.NumPad_ChoiceOn4) Then
                Temp = Temp + "E"
                Mid(TimeArray(TimeSlot), 7, 2) = Left(Temp, 2)
            Else
                Temp = ""
            End If
            If (Trim(Temp) <> "") Then
                lstCat.List(TimeSlot + 1) = Left(lstCat.List(TimeSlot + 1), 8) + " " + TimeArray(TimeSlot)
            End If
        ElseIf (frmNumericPad.NumPad_ChoiceOn5) Then
            lstCat.List(TimeSlot + 1) = Left(lstCat.List(TimeSlot + 1), 8)
            TimeArray(TimeSlot) = Space(8)
        End If
    End If
End If
End Sub

Private Function IsInsideTaskWindow(ATime As String) As Boolean
Dim i As Integer
Dim TheTime As Long
Dim st As Long, et As Long
IsInsideTaskWindow = False
If (Trim(ATime) <> "") Then
    TheTime = ConvertTimeToMinutes(ATime)
    For i = 0 To SSDay1.Tasks.Count - 1
        st = ConvertTimeToMinutes(SSDay1.Tasks(i).BeginTime)
        et = ConvertTimeToMinutes(SSDay1.Tasks(i).EndTime)
        If (TheTime >= st) And (TheTime < et) Then
            IsInsideTaskWindow = True
        End If
    Next i
End If
End Function
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
