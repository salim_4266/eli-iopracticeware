VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPayable 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtPaidDate 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7440
      MaxLength       =   10
      TabIndex        =   22
      Top             =   3360
      Width           =   1935
   End
   Begin VB.TextBox txtPaidAmt 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7440
      MaxLength       =   10
      TabIndex        =   21
      Top             =   2880
      Width           =   1935
   End
   Begin VB.ListBox lstPayerType 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   570
      ItemData        =   "Payable.frx":0000
      Left            =   3360
      List            =   "Payable.frx":000A
      TabIndex        =   19
      Top             =   720
      Width           =   1935
   End
   Begin VB.TextBox txtGL 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7440
      MaxLength       =   20
      TabIndex        =   7
      Top             =   3840
      Width           =   4215
   End
   Begin VB.ListBox lstMethod 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1335
      ItemData        =   "Payable.frx":001F
      Left            =   9480
      List            =   "Payable.frx":0021
      TabIndex        =   6
      Top             =   2400
      Width           =   2175
   End
   Begin VB.TextBox txtPayCode 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3360
      MaxLength       =   16
      TabIndex        =   3
      Top             =   2400
      Width           =   3975
   End
   Begin VB.TextBox txtDesc 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3360
      MaxLength       =   64
      TabIndex        =   2
      Top             =   1920
      Width           =   8295
   End
   Begin VB.TextBox txtVendor 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3360
      MaxLength       =   32
      TabIndex        =   0
      Top             =   1440
      Width           =   3975
   End
   Begin VB.ListBox lstPayables 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   3120
      ItemData        =   "Payable.frx":0023
      Left            =   3360
      List            =   "Payable.frx":0025
      TabIndex        =   1
      Top             =   4320
      Width           =   8295
   End
   Begin VB.TextBox txtAmount 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3360
      MaxLength       =   10
      TabIndex        =   4
      Top             =   2880
      Width           =   1935
   End
   Begin VB.TextBox txtDate 
      BackColor       =   &H00A39B6D&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3360
      MaxLength       =   10
      TabIndex        =   5
      Top             =   3360
      Width           =   1935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9840
      TabIndex        =   8
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payable.frx":0027
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   240
      TabIndex        =   9
      Top             =   7560
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Payable.frx":0206
   End
   Begin VB.Label lblPaidAmt 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Paid Amt"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5520
      TabIndex        =   24
      Top             =   2880
      Width           =   1815
   End
   Begin VB.Label lblPaidDate 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Paid Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5520
      TabIndex        =   23
      Top             =   3360
      Width           =   1815
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Payer Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   20
      Top             =   720
      Width           =   2865
   End
   Begin VB.Label lblGL 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Payment Account"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5160
      TabIndex        =   18
      Top             =   3840
      Width           =   2145
   End
   Begin VB.Label lblMethod 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Payment Method"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   7440
      TabIndex        =   17
      Top             =   2400
      Width           =   1905
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Payable Invoice"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   16
      Top             =   2400
      Width           =   2895
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   15
      Top             =   1920
      Width           =   2895
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Vendor Payables"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   14
      Top             =   4320
      Width           =   2895
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Payable Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   13
      Top             =   3360
      Width           =   2895
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Vendors"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   1440
      Width           =   2895
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Payable Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   240
      TabIndex        =   11
      Top             =   2880
      Width           =   2895
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      Caption         =   "Book a Payable"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3360
      TabIndex        =   10
      Top             =   240
      Width           =   2895
   End
End
Attribute VB_Name = "frmPayable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public BookingOn As Boolean
Public CurrentAction As String

Private ExistingPayment As Boolean
Private PayableId As Long
Private VendorId As Long
Private VendorName As String
Private PayerType As String
Private LocalDate As ManagedDate
Private RetrievePayable As PracticePayables
Private RetrieveVendor As PracticeVendors

Private Function LoadPayables() As Boolean
Dim i As Integer
Dim TheAmt As String
Dim DisplayText As String
LoadPayables = False
Set LocalDate = New ManagedDate
Set RetrievePayable = New PracticePayables
lstPayables.Clear
If (BookingOn) Then
    lstPayables.List(0) = "Create Payable"
End If
RetrievePayable.PayableVendorId = VendorId
RetrievePayable.PayablePayerType = PayerType
If (RetrievePayable.FindPayablesbyVendor > 0) Then
    i = 1
    While (RetrievePayable.SelectPayables(i))
        DisplayText = Space(75)
        If (RetrievePayable.PayableType = "B") Then
            LocalDate.ExposedDate = Trim(RetrievePayable.PayableDate)
            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                Mid(DisplayText, 1, 10) = LocalDate.ExposedDate
            End If
            Call DisplayDollarAmount(Trim(Str(RetrievePayable.PayableAmount)), TheAmt)
        Else
            LocalDate.ExposedDate = Trim(RetrievePayable.PayablePaidDate)
            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                Mid(DisplayText, 1, 10) = LocalDate.ExposedDate
            End If
            Call DisplayDollarAmount(Trim(Str(RetrievePayable.PayablePaidAmount)), TheAmt)
        End If
        Mid(DisplayText, 12, 1) = RetrievePayable.PayableType
        Mid(DisplayText, 14, 10) = Trim(TheAmt)
        Mid(DisplayText, 26, 16) = RetrievePayable.PayableInvoice
        Mid(DisplayText, 43, 30) = RetrievePayable.PayableDescription
        Mid(DisplayText, 75, 1) = RetrievePayable.PayablePayerType
        DisplayText = DisplayText + Trim(Str(RetrievePayable.PayableId))
        lstPayables.AddItem DisplayText
        i = i + 1
    Wend
End If
Set LocalDate = Nothing
Set RetrievePayable = Nothing
If (PayerType = "P") Then
    Call AddRefunds(VendorId)
End If
Call SetupAccess
lstPayables.SetFocus
End Function

Private Function AddRefunds(PatId) As Boolean
Dim i As Integer
Dim TheAmt As String
Dim TheClm As String
Dim DisplayText As String
Dim RetrievePayment As PatientReceivablePayment
AddRefunds = False
Set LocalDate = New ManagedDate
Set RetrievePayment = New PatientReceivablePayment
If (PatId < 1) Then
    Exit Function
End If
RetrievePayment.PaymentPayerId = PatId
RetrievePayment.PaymentPayerType = "P"
If (RetrievePayment.FindPatientReceivablePayments > 0) Then
    i = 1
    While (RetrievePayment.SelectPatientReceivablePayments(i))
        If (RetrievePayment.PaymentType = "R") Then
            DisplayText = Space(75)
            LocalDate.ExposedDate = Trim(RetrievePayment.PaymentDate)
            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                Mid(DisplayText, 1, 10) = LocalDate.ExposedDate
            End If
            Call DisplayDollarAmount(Trim(Str(RetrievePayment.PaymentAmount)), TheAmt)
            Mid(DisplayText, 12, 1) = "B"
            Mid(DisplayText, 14, 10) = Trim(TheAmt)
            Mid(DisplayText, 74, 1) = "R"
            Mid(DisplayText, 75, 1) = "P"
            DisplayText = DisplayText + Trim(Str(RetrievePayment.PaymentId))
            lstPayables.AddItem DisplayText
        End If
        i = i + 1
    Wend
End If
Set LocalDate = Nothing
Set RetrievePayment = Nothing
End Function

Private Function getVendor(VendorId As Long, VendorName As String) As Boolean
Dim TheDocs As PracticeVendors
getVendor = False
VendorName = ""
Set TheDocs = New PracticeVendors
TheDocs.VendorId = VendorId
If (TheDocs.RetrieveVendor) Then
    VendorName = Trim(TheDocs.VendorName)
    getVendor = True
End If
Set TheDocs = Nothing
End Function

Private Function GetPatient(PatientId As Long, PatientName As String) As Boolean
Dim ThePatient As Patient
GetPatient = False
PatientName = ""
Set ThePatient = New Patient
ThePatient.PatientId = PatientId
If (ThePatient.RetrievePatient) Then
    PatientName = Trim(ThePatient.FirstName) + " " _
                + Trim(ThePatient.MiddleInitial) + " " _
                + Trim(ThePatient.LastName) + " " _
                + Trim(ThePatient.NameRef)
    GetPatient = True
End If
Set ThePatient = Nothing
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPayable.BorderStyle = 1
    frmPayable.ClipControls = True
    frmPayable.Caption = Mid(frmPayable.Name, 4, Len(frmPayable.Name) - 3)
    frmPayable.AutoRedraw = True
    frmPayable.Refresh
End If
If (BookingOn) Then
    Label1.Caption = "Book a Payable"
    lblMethod.Visible = False
    lstMethod.Visible = False
    lblGL.Visible = False
    txtGL.Visible = False
    lblPaidDate.Visible = False
    txtPaidDate.Visible = False
    lblPaidAmt.Visible = False
    txtPaidAmt.Visible = False
Else
    Label1.Caption = "Disbursements"
    Call LoadCodes("PayableType")
    lblMethod.Visible = True
    lstMethod.Visible = True
    lblGL.Visible = True
    txtGL.Visible = True
    lblPaidDate.Visible = True
    txtPaidDate.Visible = True
    lblPaidAmt.Visible = True
    txtPaidAmt.Visible = True
End If
End Sub

Private Sub LoadCodes(CodeType)
Dim k As Integer
Dim RetrieveCode As PracticeCodes
Set RetrieveCode = New PracticeCodes
lstMethod.Clear
lstMethod.ListIndex = -1
RetrieveCode.ReferenceType = Trim(CodeType)
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        lstMethod.List(k - 1) = Trim(RetrieveCode.ReferenceCode)
        k = k + 1
    Loop
End If
Set RetrieveCode = Nothing
End Sub

Private Sub lstPayables_Click()
Dim u As Integer
Dim Temp As Integer
Dim TheAmt As String
Dim TheClm As String
Dim RetrievePayment As PatientReceivablePayment
If (VendorId < 1) Then
    frmEventMsgs.Header = "Select a Vendor"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    Temp = -1
    If (BookingOn) Then
        Temp = 0
    End If
    If (lstPayables.ListIndex > Temp) Then
        If (BookingOn) Then
            ExistingPayment = False
            If (Mid(lstPayables.List(lstPayables.ListIndex), 12, 1) <> "B") Then
                frmEventMsgs.Header = "Select a Booked Payable Only"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                lstPayables.ListIndex = -1
                Exit Sub
            End If
        Else
            If (Mid(lstPayables.List(lstPayables.ListIndex), 12, 1) = "B") Then
                ExistingPayment = False
            Else
                ExistingPayment = True
            End If
        End If
        If (Mid(lstPayables.List(lstPayables.ListIndex), 74, 1) = "R") Then
            PayableId = val(Mid(lstPayables.List(lstPayables.ListIndex), 76, 5))
            Set LocalDate = New ManagedDate
            Set RetrievePayment = New PatientReceivablePayment
            RetrievePayment.PaymentId = PayableId
            If (RetrievePayment.RetrievePatientReceivablePayments) Then
                PayerType = "P"
                If (PayerType = "P") Then
                    lstPayerType.ListIndex = 1
                Else
                    lstPayerType.ListIndex = 0
                End If
                txtDesc.Text = RetrievePayment.PaymentRefId
                txtPayCode.Text = ""
                Call DisplayDollarAmount(Trim(Str(RetrievePayment.PaymentAmount)), TheAmt)
                txtAmount.Text = Trim(TheAmt)
                LocalDate.ExposedDate = Trim(RetrievePayment.PaymentDate)
                If (LocalDate.ConvertManagedDateToDisplayDate) Then
                    txtDate.Text = LocalDate.ExposedDate
                Else
                    txtDate.Text = ""
                End If
                lstMethod.ListIndex = -1
                txtPaidAmt.Text = ""
                txtPaidDate.Text = ""
                txtGL.Text = ""
                If Not (BookingOn) And Not (ExistingPayment) Then
                    txtPaidAmt.SetFocus
                    SendKeys "{Home}"
                End If
            End If
            Set LocalDate = Nothing
            Set RetrievePayment = Nothing
            Call SetupAccess
        Else
            PayableId = val(Mid(lstPayables.List(lstPayables.ListIndex), 76, 5))
            Set LocalDate = New ManagedDate
            Set RetrievePayable = New PracticePayables
            RetrievePayable.PayableId = PayableId
            If (RetrievePayable.RetrievePracticePayable) Then
                PayerType = RetrievePayable.PayablePayerType
                If (PayerType = "P") Then
                    lstPayerType.ListIndex = 1
                Else
                    lstPayerType.ListIndex = 0
                End If
                txtDesc.Text = RetrievePayable.PayableDescription
                txtPayCode.Text = RetrievePayable.PayableInvoice
                Call DisplayDollarAmount(Trim(Str(RetrievePayable.PayableAmount)), TheAmt)
                txtAmount.Text = Trim(TheAmt)
                LocalDate.ExposedDate = Trim(RetrievePayable.PayableDate)
                If (LocalDate.ConvertManagedDateToDisplayDate) Then
                    txtDate.Text = LocalDate.ExposedDate
                Else
                    txtDate.Text = ""
                End If
                lstMethod.ListIndex = -1
                For u = 0 To lstMethod.ListCount - 1
                    If (Left(lstMethod.List(u), 1) = RetrievePayable.PayableType) Then
                        lstMethod.ListIndex = u
                        Exit For
                    End If
                Next u
                Call DisplayDollarAmount(Trim(Str(RetrievePayable.PayablePaidAmount)), TheAmt)
                txtPaidAmt.Text = Trim(TheAmt)
                LocalDate.ExposedDate = Trim(RetrievePayable.PayablePaidDate)
                If (LocalDate.ConvertManagedDateToDisplayDate) Then
                    txtPaidDate.Text = LocalDate.ExposedDate
                Else
                    txtPaidDate.Text = ""
                End If
                txtGL.Text = RetrievePayable.APAccount
                If Not (BookingOn) And Not (ExistingPayment) Then
                    lstMethod.ListIndex = -1
                    txtPaidAmt.Text = ""
                    txtPaidDate.Text = ""
                    txtGL.Text = ""
                    txtPaidAmt.SetFocus
                    SendKeys "{Home}"
                End If
            End If
            Set LocalDate = Nothing
            Set RetrievePayable = Nothing
            Call SetupAccess
        End If
    Else
        If (BookingOn) Then
            lstMethod.ListIndex = -1
            txtDesc.Text = ""
            txtPayCode.Text = ""
            txtAmount.Text = ""
            txtDate.Text = ""
            txtPaidAmt.Text = ""
            txtPaidDate.Text = ""
            txtGL.Text = ""
            txtDesc.SetFocus
            SendKeys "{Home}"
        End If
        Call SetupAccess
    End If
End If
End Sub

Private Sub lstPayerType_Click()
If (lstPayerType.ListIndex = 1) Then
    PayerType = "P"
ElseIf (lstPayerType.ListIndex = 0) Then
    PayerType = "V"
End If
End Sub

Private Sub txtAmount_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters $0123456789."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
    txtAmount.SetFocus
    txtAmount.Text = ""
    SendKeys "{Home}"
End If
End Sub

Private Sub txtDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtDate.SetFocus
        txtDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtDate, "D")
    End If
End If
End Sub

Private Sub txtPaidAmt_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters $0123456789."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
    txtPaidAmt.SetFocus
    txtPaidAmt.Text = ""
    SendKeys "{Home}"
End If
End Sub

Private Sub txtPaidDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtPaidDate.Text)) Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPaidDate.SetFocus
        txtPaidDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPaidDate, "D")
    End If
End If
End Sub

Private Sub txtVendor_Click()
Dim PermOn As Boolean
Dim DisplayText As String
If (PayerType = "V") Then
    Call frmSelectDialogue.BuildSelectionDialogue("VENDOR")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        If (frmSelectDialogue.Selection = "Create Vendor") Then
            frmVendors.TheType = "D"
            frmVendors.DeleteOn = False
            If (frmVendors.VendorLoadDisplay(0, PermOn)) Then
                frmVendors.Show 1
                If (frmVendors.TheVendorId > 0) Then
                    If (getVendor(frmVendors.TheVendorId, DisplayText)) Then
                        txtVendor.Text = DisplayText
                        VendorId = frmVendors.TheVendorId
                        Call LoadPayables
                    End If
                End If
            End If
        Else
            txtVendor.Text = Trim(Left(frmSelectDialogue.Selection, 56))
            VendorId = val(Mid(frmSelectDialogue.Selection, 57, 5))
            VendorName = Trim(txtVendor.Text)
            Call LoadPayables
        End If
    End If
ElseIf (PayerType = "P") Then
        Dim ReturnArguments As Variant
        Set ReturnArguments = DisplayPatientSearchScreen
        If Not (ReturnArguments Is Nothing) Then
            VendorId = ReturnArguments.PatientId
        End If
        If (GetPatient(VendorId, VendorName)) Then
            txtVendor.Text = VendorName
            Call LoadPayables
        End If
    End If
End Sub

Private Sub txtVendor_KeyPress(KeyAscii As Integer)
Call txtVendor_Click
KeyAscii = 0
End Sub

Private Sub cmdHome_Click()
CurrentAction = "Home"
Unload frmPayable
End Sub

Private Sub cmdDone_Click()
If (VendorId < 1) Then
    frmEventMsgs.Header = "Vendor must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtVendor.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (lstPayables.ListIndex < 0) Then
    frmEventMsgs.Header = "Select a payable"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    lstPayables.SetFocus
    Exit Sub
End If
If (Trim(txtDesc.Text) = "") Then
    frmEventMsgs.Header = "Description must be entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtDesc.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtAmount.Text) = "") Then
    frmEventMsgs.Header = "Amount must be entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtAmount.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtPayCode.Text) = "") Then
    frmEventMsgs.Header = "Invoice must be entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPayCode.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtDate.Text) = "") Then
    frmEventMsgs.Header = "Date must be entered"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtDate.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Mid(lstPayables.List(lstPayables.ListIndex), 74, 1) = "R") And (BookingOn) Then
    frmEventMsgs.Header = "Changed tp Refunds are performed in Post a Payment"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If Not (BookingOn) Then
    If (Trim(txtPaidAmt.Text) = "") Then
        frmEventMsgs.Header = "Paid Amount must be entered"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPaidAmt.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
    If (Trim(txtPaidDate.Text) = "") Then
        frmEventMsgs.Header = "Paid Date must be entered"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPaidDate.SetFocus
        SendKeys "{Home}"
        Exit Sub
    End If
End If
Set LocalDate = New ManagedDate
Set RetrievePayable = New PracticePayables
If (BookingOn) Then
    RetrievePayable.PayableId = PayableId
Else
    If Not (ExistingPayment) Then
        RetrievePayable.PayableId = 0
    Else
        RetrievePayable.PayableId = PayableId
    End If
End If
If (RetrievePayable.RetrievePracticePayable) Then
    RetrievePayable.PayableDescription = Trim(txtDesc.Text)
    RetrievePayable.PayablePayerType = PayerType
    RetrievePayable.PayableAmount = val(Trim(txtAmount.Text))
    RetrievePayable.PayableInvoice = Trim(txtPayCode.Text)
    RetrievePayable.PayableVendorId = VendorId
    RetrievePayable.PayableDate = ""
    LocalDate.ExposedDate = Trim(txtDate.Text)
    If (LocalDate.ConvertDisplayDateToManagedDate) Then
        RetrievePayable.PayableDate = LocalDate.ExposedDate
    End If
    If (BookingOn) Then
        RetrievePayable.PayablePaidAmount = 0
        RetrievePayable.PayablePaidDate = ""
        RetrievePayable.PayableType = "B"
        RetrievePayable.APAccount = ""
    Else
        RetrievePayable.PayablePaidAmount = val(Trim(txtPaidAmt.Text))
        RetrievePayable.PayablePaidDate = ""
        LocalDate.ExposedDate = Trim(txtPaidDate.Text)
        If (LocalDate.ConvertDisplayDateToManagedDate) Then
            RetrievePayable.PayablePaidDate = LocalDate.ExposedDate
        End If
        If (lstMethod.ListIndex <> -1) Then
            RetrievePayable.PayableType = Left(lstMethod.List(lstMethod.ListIndex), 1)
        Else
            RetrievePayable.PayableType = "B"
        End If
        RetrievePayable.APAccount = Trim(txtGL.Text)
    End If
    If Not (RetrievePayable.ApplyPracticePayable) Then
        frmEventMsgs.Header = "Update Failed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
Set LocalDate = Nothing
Set RetrievePayable = Nothing
Unload frmPayable
End Sub

Private Sub SetupAccess()
If (BookingOn) Then
    txtDesc.Locked = False
    txtAmount.Locked = False
    txtDate.Locked = False
    txtPayCode.Locked = False
    txtVendor.Locked = False
    txtPaidAmt.Locked = True
    txtPaidDate.Locked = True
    txtGL.Locked = True
Else
    txtDesc.Locked = True
    txtAmount.Locked = True
    txtDate.Locked = True
    If (Trim(txtPayCode.Text) = "") Then
        txtPayCode.Locked = False
    Else
        txtPayCode.Locked = True
    End If
    txtVendor.Locked = True
    txtPaidAmt.Locked = False
    txtPaidDate.Locked = False
    txtGL.Locked = False
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
