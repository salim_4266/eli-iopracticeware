VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPendingLetters 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstLtrs 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3900
      ItemData        =   "PendingLetters.frx":0000
      Left            =   2760
      List            =   "PendingLetters.frx":0002
      TabIndex        =   8
      Top             =   1920
      Visible         =   0   'False
      Width           =   4815
   End
   Begin VB.ListBox lstPrint 
      Height          =   450
      Left            =   6600
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQueue 
      Height          =   1110
      Left            =   6120
      TabIndex        =   0
      Top             =   7320
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingLetters.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   9840
      TabIndex        =   1
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingLetters.frx":01EC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   1110
      Left            =   7680
      TabIndex        =   3
      Top             =   7320
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingLetters.frx":03CB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiffLtr 
      Height          =   1110
      Left            =   4560
      TabIndex        =   7
      Top             =   7320
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingLetters.frx":05BA
   End
   Begin VB.ListBox lstDocs 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6540
      ItemData        =   "PendingLetters.frx":07A7
      Left            =   120
      List            =   "PendingLetters.frx":07A9
      MultiSelect     =   1  'Simple
      TabIndex        =   2
      Top             =   720
      Width           =   11295
   End
   Begin VB.Label lblLtr 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   240
      TabIndex        =   9
      Top             =   7440
      Visible         =   0   'False
      Width           =   1050
   End
   Begin VB.Label lblLoading 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   4560
      TabIndex        =   5
      Top             =   240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pending Letters to Queue"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   3555
   End
End
Attribute VB_Name = "frmPendingLetters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private PatId As Long
Private ApptId As Long
Private ImprOnly As String
Private LatestTests As String
Private LastName As String
Private FirstName As String
Private PatSal As String
Private ResourceName As String
Private ApptStatus As String
Private ApptType As String
Private ApptDate As String
Private BirthDate As String
Private PCPDr As String
Private RefDr As String
Private RefDrP As String
Private InsName As String
Private TheLetter As String
Private TheReceiver As String
Private ThePatient As String
Private ResourceLocId As Long
Private PlacementPerson As Long

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset

Private Sub cmdDiffLtr_Click()
lstLtrs.Visible = True
End Sub

Private Sub cmdDone_Click()
Unload frmPendingLetters
End Sub

Private Sub cmdPatient_Click()
Dim j As Integer
Dim PatId As Long
Dim ApplList As ApplicationAIList
If (lstDocs.ListIndex >= 0) Then
    j = InStrPS(75, lstDocs.List(lstDocs.ListIndex), "/")
    If (j > 0) Then
        PatId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), j + 1, Len(lstDocs.List(lstDocs.ListIndex)) - j)))
        If (PatId > 0) Then
            Dim PatientDemoGraphics As New PatientDemoGraphics
            PatientDemoGraphics.PatientId = PatId
            Call PatientDemoGraphics.DisplayPatientInfoScreen
        End If
    End If
Else
    frmEventMsgs.Header = "Select Patient"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdQueue_Click()
Dim i As Integer, j As Integer
Dim TheId As Long, PatId As Long, ApptId As Long
Dim QueOn As Boolean, AllOn As Boolean, PrintOn As Boolean
Dim FirstOne As Boolean, LastOne As Boolean, CurrentOn As Boolean
Dim TheName As String, ARcv As String
Dim PrintFile As String
Dim AFile As String, BFile As String
AllOn = False
QueOn = False
PrintOn = False
If (lstDocs.ListCount < 1) Then
    frmEventMsgs.Header = "Nothing to Submit"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (lstDocs.SelCount > 0) Then
    AllOn = False
    frmEventMsgs.Header = "Action for Selected Items ?"
'    frmEventMsgs.AcceptText = "Queue"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Print"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        PrintOn = True
        QueOn = False
    ElseIf (frmEventMsgs.Result = 1) Then
        PrintOn = False
        QueOn = True
    Else
        Exit Sub
    End If
ElseIf (lstDocs.SelCount <= 0) Then
    AllOn = True
    frmEventMsgs.Header = "Action for All Items ?"
'    frmEventMsgs.AcceptText = "Queue"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Print"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        PrintOn = True
        QueOn = False
    ElseIf (frmEventMsgs.Result = 1) Then
        PrintOn = False
        QueOn = True
    Else
        Exit Sub
    End If
    For i = 0 To lstDocs.ListCount - 1
        lstDocs.Selected(i) = True
    Next i
    lstDocs.ListIndex = 0
End If
' test 1 or 2
PrintFile = DocumentDirectory + "Ltrs-M00000.txt"
If (FM.IsFileThere(PrintFile)) Then
    PrintFile = DocumentDirectory + "Ltrs-M00001.txt"
    If (FM.IsFileThere(PrintFile)) Then
        PrintFile = DocumentDirectory + "Ltrs-M00002.txt"
        If (FM.IsFileThere(PrintFile)) Then
            PrintFile = DocumentDirectory + "Ltrs-M00003.txt"
            If (FM.IsFileThere(PrintFile)) Then
                PrintFile = DocumentDirectory + "Ltrs-M00004.txt"
            End If
        End If
    End If
End If
FirstOne = True
LastOne = False
CurrentOn = False
Dim count1 As Integer
Dim count2 As Integer
count1 = 0
count2 = 0
For i = 0 To lstDocs.ListCount - 1
If (lstDocs.Selected(i)) Then
count1 = count1 + 1
End If

Next


For i = 0 To lstDocs.ListCount - 1


    If (lstDocs.Selected(i)) Then
    
    count2 = count2 + 1
    If count2 = count1 Then
    LastOne = True
    End If

        j = InStrPS(75, lstDocs.List(i), "/")
        If (j > 0) Then
            ApptId = val(Trim(Mid(lstDocs.List(i), 76, (j - 1) - 75)))
            PatId = val(Trim(Mid(lstDocs.List(i), j + 1, Len(lstDocs.List(i)) - j)))
            If (PlacementPerson < 1) Then
                PlacementPerson = PatId
            End If
            If (TheReceiver = "PCP") Then
                ARcv = GetName(PatId, ApptId, "D")
            ElseIf (TheReceiver = "Ref Doc") Then
                ARcv = GetName(PatId, ApptId, "R")
            ElseIf (TheReceiver = "Other Ref Doc") Then
                ARcv = GetName(PatId, ApptId, "O")
            Else
                ARcv = GetName(PatId, ApptId, "P")
            End If
            TheId = PostToJournal(PatId, ApptId, TheLetter, ARcv, ImprOnly, LatestTests)
            If (TheId > 0) Then
                Call MaintainLetter(TheId, ARcv, FirstOne, LastOne, False, PrintFile)
                FirstOne = False
            End If
            QueOn = True
            lstDocs.Selected(i) = False
            DoEvents
        End If
    End If
Next i
If (QueOn) Then
    If (PrintOn) Then
        Call MaintainLetter(-1, "", False, False, True, PrintFile)
        AFile = "Printed"
    Else
        AFile = "Queued"
    End If
    lblLoading.Visible = False
    lblLoading.Caption = "Loading"
    DoEvents
    TheId = PostToJournal(-1, -1, TheLetter, "Batch", "", "")
    If (TheId > 0) And Not (PrintOn) Then
        AFile = DocumentDirectory + "Ltrs-M00000.txt"
        BFile = DocumentDirectory + "Ltrs-M" + Trim(Str(TheId)) + ".txt"
        If (FM.IsFileThere(BFile)) Then
            FM.Kill BFile
        End If
        FM.Name AFile, BFile
        AFile = DocumentDirectory + "Ltrs-M00000.doc"
        BFile = DocumentDirectory + "Ltrs-M" + Trim(Str(TheId)) + ".doc"
        If (FM.IsFileThere(BFile)) Then
            FM.Kill BFile
        End If
        FM.Name AFile, BFile
    End If
    frmEventMsgs.Header = "Items " + BFile
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (AllOn) Then
        Call cmdDone_Click
    End If
End If
PlacementPerson = 0
End Sub

Public Function LoadPendingLetters() As Boolean
Dim BullshitOn As Boolean
Dim i As Long, j As Long
Dim QueryId As Integer, StartYear As Integer
Dim Temp As String, Temp1 As String, Temp2 As String, Temp3 As String
Dim Ref As String, DatesQuery As String
Dim TheName As String, DisplayText As String
Dim ApplLtrs As ApplicationTables
On Error GoTo Db_ErrorHandler
LoadPendingLetters = False
lblLoading.Visible = True
Ref = ""
lblLtr.Visible = False
lstLtrs.Visible = False
Set ApplLtrs = New ApplicationTables
Set ApplLtrs.lstBox = lstLtrs
Call ApplLtrs.ApplLoadCodes("MISCELLANEOUSLETTERS", True, True, True)
lstLtrs.List(0) = "Close Miscellaneous Letters"
Set ApplLtrs = Nothing
DatesQuery = ""
QueryId = 0
PlacementPerson = 0
BullshitOn = False
TheLetter = frmFilterLetters.GetFilterCriteria("Letter")
i = InStrPS(TheLetter, "   ")
If (i > 0) Then
    TheLetter = Trim(Left(TheLetter, i - 1))
End If
i = InStrPS(TheLetter, " ")
If (i > 0) Then
    TheLetter = Trim(Mid(TheLetter, i + 1, Len(TheLetter) - i))
End If
TheReceiver = frmFilterLetters.GetFilterCriteria("Recipient")
ImprOnly = frmFilterLetters.GetFilterCriteria("IMPRONLY")
LatestTests = frmFilterLetters.GetFilterCriteria("LATESTTESTS")
Temp = frmFilterLetters.GetFilterCriteria("ADate")
If (Trim(Temp) <> "") Then
    QueryId = 1
    i = InStrPS(Temp, "&")
    If (i > 0) Then
        Temp1 = Trim(Left(Temp, i - 1))
        Temp2 = Trim(Mid(Temp, i + 1, Len(Temp) - 1))
        Temp1 = Mid(Temp1, 7, 4) + Mid(Temp1, 1, 2) + Mid(Temp1, 4, 2)
        Temp2 = Mid(Temp2, 7, 4) + Mid(Temp2, 1, 2) + Mid(Temp2, 4, 2)
        DatesQuery = DatesQuery + Ref + "(Appointments.AppDate >= '" + Temp1 + "') And (Appointments.AppDate <= '" + Temp2 + "') "
    Else
        Temp1 = Trim(Temp)
        Temp1 = Mid(Temp1, 7, 4) + Mid(Temp1, 1, 2) + Mid(Temp1, 4, 2)
        DatesQuery = DatesQuery + Ref + "(Appointments.AppDate >= '" + Temp1 + "') "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("BDate")
If (Trim(Temp) <> "") Then
    If (QueryId = 0) Then
        QueryId = 4
    Else
        QueryId = 3
    End If
    i = InStrPS(Temp, "&")
    If (i > 0) Then
        Temp1 = Trim(Left(Temp, i - 1))
        j = InStrPS(i + 1, Temp, "&")
        If (j > 0) Then
            Temp2 = Trim(Mid(Temp, i + 1, j - i))
            Temp3 = Trim(Mid(Temp, j + 1, 1))
        Else
            Temp3 = Trim(Mid(Temp, i + 1, Len(Temp) - i))
        End If
        If (Temp3 = "+") Then
            BullshitOn = True
'            Temp1 = "YYYY" + Mid(Temp1, 1, 2) + Mid(Temp1, 4, 2)
'            Temp2 = "YYYY" + Mid(Temp2, 1, 2) + Mid(Temp2, 4, 2)
            Temp1 = Mid(Temp1, 1, 2) + Mid(Temp1, 4, 2)
            Temp2 = Mid(Temp2, 1, 2) + Mid(Temp2, 4, 2)
            DatesQuery = DatesQuery + Ref + "(SUBSTRING(PatientDemographics.BirthDate,5,4) >= '" + Temp1 + "' And SUBSTRING(PatientDemographics.BirthDate,5,4) <= '" + Temp2 + "') "
        Else
            Temp1 = Mid(Temp1, 7, 4) + Mid(Temp1, 1, 2) + Mid(Temp1, 4, 2)
            Temp2 = Mid(Temp2, 7, 4) + Mid(Temp2, 1, 2) + Mid(Temp2, 4, 2)
            DatesQuery = DatesQuery + Ref + "(PatientDemographics.BirthDate >= '" + Temp1 + "' And PatientDemographics.BirthDate <= '" + Temp2 + "') "
        End If
        Ref = "And "
    Else
        Temp1 = Trim(Temp)
        If (Len(Temp1) >= 10) Then
            Temp1 = Mid(Temp1, 7, 4) + Mid(Temp1, 1, 2) + Mid(Temp1, 4, 2)
            DatesQuery = DatesQuery + Ref + "(PatientDemographics.BirthDate >= '" + Temp1 + "') "
            Ref = "And "
        End If
    End If
End If
Temp = frmFilterLetters.GetFilterCriteria("AStatus")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp = "All") Then
        DatesQuery = DatesQuery + Ref + "(Appointments.ScheduleStatus <> '') "
    Else
        DatesQuery = DatesQuery + Ref + "("
        For i = 1 To Len(Temp) Step 2
            If (i = 1) Then
                DatesQuery = DatesQuery + "(Appointments.ScheduleStatus = '" + Mid(Temp, i, 1) + "') "
            ElseIf (i = Len(Temp) - 1) Then
                DatesQuery = DatesQuery + "Or (Appointments.ScheduleStatus = '" + Mid(Temp, i, 1) + "') "
            Else
                DatesQuery = DatesQuery + "Or (Appointments.ScheduleStatus = '" + Mid(Temp, i, 1) + "') "
            End If
        Next i
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("Language")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        DatesQuery = DatesQuery + Ref + "(PatientDemographics.Language = '" + UCase(Trim(Temp)) + "') "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("AType")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp = "All") Then
        DatesQuery = DatesQuery + Ref + "(AppointmentType.AppointmentType <> '') "
    Else
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(AppointmentType.AppointmentType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (AppointmentType.AppointmentType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(AppointmentType.AppointmentType = '" + Trim(Temp) + "')"
            Else
                DatesQuery = DatesQuery + "Or (AppointmentType.AppointmentType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("Contact")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp = "All") Then
        DatesQuery = DatesQuery + Ref + "(PracticeVendors.VendorType <> '') "
    Else
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PracticeVendors.VendorType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors.VendorType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PracticeVendors.VendorType = '" + Trim(Temp) + "')"
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors.VendorType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("Insurer")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp = "All") Then
        DatesQuery = DatesQuery + Ref + "(i.Name <> '') "
    Else
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(i.Name = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (i.Name = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(i.Name = '" + Trim(Temp) + "')"
            Else
                DatesQuery = DatesQuery + "Or (i.Name = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("PCP")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp = "All") Then
        DatesQuery = DatesQuery + Ref + "(PracticeVendors.VendorLastName <> '') "
    Else
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PracticeVendors.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PracticeVendors.VendorLastName = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("RefDr")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp = "All") Then
        DatesQuery = DatesQuery + Ref + "(PracticeVendors_1.VendorLastName <> '') "
    Else
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PracticeVendors_1.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors_1.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PracticeVendors_1.VendorLastName = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors_1.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If

' From Connections with Paper Referral
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PracticeVendors_2.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors_2.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PracticeVendors_1.VendorLastName = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PracticeVendors_1.VendorLastName = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("Docs")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(Appointments.ResourceId1 = " + Mid(Temp, j, (i - 1) - (j - 1)) + ") "
            Else
                DatesQuery = DatesQuery + "Or (Appointments.ResourceId1 = " + Mid(Temp, j, (i - 1) - (j - 1)) + ") "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(Appointments.ResourceId1 = " + Trim(Temp) + ")"
            Else
                DatesQuery = DatesQuery + "Or (Appointments.ResourceId1 = " + Mid(Temp, j, (i - 1) - (j - 1)) + ") "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("Locs")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(Appointments.ResourceId2 = " + Mid(Temp, j, (i - 1) - (j - 1)) + ") "
            Else
                DatesQuery = DatesQuery + "Or (Appointments.ResourceId2 = " + Mid(Temp, j, (i - 1) - (j - 1)) + ") "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(Appointments.ResourceId2 = " + Trim(Temp) + ") "
            Else
                DatesQuery = DatesQuery + "Or (Appointments.ResourceId2 = " + Mid(Temp, j, (i - 1) - (j - 1)) + ") "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("Zip")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PatientDemographics.Zip = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientDemographics.Zip = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PatientDemographics.Zip = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientDemographics.Zip = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("RefCat")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PatientDemographics.ReferralCategory = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientDemographics.ReferralCategory = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PatientDemographics.ReferralCategory = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientDemographics.ReferralCategory = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("CPT")
If (Trim(Temp) <> "") Then
    QueryId = 2
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PatientReceivableServices.Service = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientReceivableServices.Service = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PatientReceivableServices.Service = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientReceivableServices.Service = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("ICD")
If (Trim(Temp) <> "") Then
    QueryId = 2
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PatientClinical.FindingDetail = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientClinical.FindingDetail = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PatientClinical.FindingDetail = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientClinical.FindingDetail = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If
Temp = frmFilterLetters.GetFilterCriteria("PlanType")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If

Temp = frmFilterLetters.GetFilterCriteria("PATIENTTYPE")
If (Trim(Temp) <> "") Then
    QueryId = 1
    If (Temp <> "All") Then
        i = InStrPS(Temp, "&")
        j = 1
        DatesQuery = DatesQuery + Ref + "("
        While (i > 0)
            If (j = 1) Then
                DatesQuery = DatesQuery + "(PatientDemographics.PatType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientDemographics.PatType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
            j = i + 1
            i = InStrPS(j, Temp, "&")
        Wend
        If (i > j) Or (j = 1) Then
            If (i = 0) Then
                DatesQuery = DatesQuery + "(PatientDemographics.PatType = '" + Trim(Temp) + "') "
            Else
                DatesQuery = DatesQuery + "Or (PatientDemographics.PatType = '" + Mid(Temp, j, (i - 1) - (j - 1)) + "') "
            End If
        End If
        DatesQuery = DatesQuery + ") "
    End If
    Ref = "And "
End If

ThePatient = frmFilterLetters.GetFilterCriteria("Patient")
If (Trim(ThePatient) <> "") Then
    DatesQuery = DatesQuery + Ref + "(PatientDemographics.PatientId = " + Trim(ThePatient) + ") "
    Ref = "And "
End If
DatesQuery = DatesQuery + Ref + "(PatientDemographics.Status = 'A') "
Call ReplaceCharacters(DatesQuery, ") )", "))")
If (QueryId = 2) Then
    MyPracticeRepositoryCmd.CommandText _
        = "SELECT DISTINCT PatientDemographics.PatientId, PatientDemographics.LastName, PatientDemographics.FirstName, " _
        + "Appointments.AppointmentId, Appointments.AppDate, Appointments.ScheduleStatus, Resources.ResourceName, " _
        + "PracticeVendors.VendorName, PracticeVendors_1.VendorName as 'Vendor1', PracticeVendors_2.VendorName as 'Vendor2', " _
        + "i.Name As InsurerName, PracticeVendors.VendorType, " _
        + "PatientDemographics.Zip, PatientDemographics.BirthDate, " _
        + "AppointmentType.AppointmentType, PatientDemographics.ReferralCatagory , PatientClinical.FindingDetail, " _
        + "PatientReceivableServices.Service, PatientDemographics.Salutation " _
        + "FROM (((((((((((Appointments " _
        + "INNER JOIN PatientDemographics ON Appointments.PatientId = PatientDemographics.PatientId) " _
        + "INNER JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId) " _
        + "LEFT JOIN PatientReferral ON Appointments.ReferralId = PatientReferral.ReferralId) " _
        + "LEFT JOIN PatientClinical ON Appointments.AppointmentId = PatientClinical.AppointmentId) " _
        + "LEFT JOIN model.PatientInsurances pi ON PatientDemographics.PolicyPatientId = pi.InsuredPatientId) " _
        + "LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId " _
        + "LEFT JOIN PracticeVendors ON PatientDemographics.PrimaryCarePhysician = PracticeVendors.VendorId) " _
        + "LEFT JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId) " _
        + "LEFT JOIN model.Insurers i ON ip.InsurerId= i.Id) " _
        + "LEFT JOIN model.InsurerPlanTypes pt ON pt.Id = i.InsurerPlanTypeId " _
        + "LEFT JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId) " _
        + "LEFT JOIN PatientReceivableServices ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) " _
        + "LEFT JOIN PracticeVendors AS PracticeVendors_1 ON PatientDemographics.ReferringPhysician = PracticeVendors_1.VendorId) " _
        + "LEFT JOIN PracticeVendors AS PracticeVendors_2 ON PatientReferral.ReferredFromId = PracticeVendors_2.VendorId " _
        + "WHERE " + DatesQuery _
        + "ORDER BY PatientDemographics.LastName ASC, PatientDemographics.FirstName ASC "
ElseIf (QueryId = 4) Then
    MyPracticeRepositoryCmd.CommandText _
        = "SELECT DISTINCT PatientDemographics.PatientId, PatientDemographics.LastName, PatientDemographics.FirstName, " _
        + "Appointments.AppointmentId, Appointments.AppDate, Appointments.ScheduleStatus, Resources.ResourceName, " _
        + "PracticeVendors.VendorName, PracticeVendors_1.VendorName AS 'Vendor1', PracticeVendors_2.VendorName AS 'Vendor2', " _
        + " i.Name As InsurerName, PracticeVendors.VendorType, PatientDemographics.Zip, PatientDemographics.BirthDate, " _
        + "AppointmentType.AppointmentType, PatientDemographics.ReferralCatagory, PatientDemographics.Salutation " _
        + "FROM (((((((((PatientDemographics " _
        + "LEFT JOIN Appointments ON PatientDemographics.PatientId = Appointments.PatientId) " _
        + "LEFT JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId) " _
        + "LEFT JOIN PatientReferral ON Appointments.ReferralId = PatientReferral.ReferralId) " _
        + "LEFT JOIN PatientClinical ON Appointments.AppointmentId = PatientClinical.AppointmentId) " _
        + "LEFT JOIN model.PatientInsurances pi ON PatientDemographics.PolicyPatientId = pi.InsuredPatientId) " _
        + "LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId " _
        + "LEFT JOIN PracticeVendors ON PatientDemographics.PrimaryCarePhysician = PracticeVendors.VendorId) " _
        + "LEFT JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId) " _
        + "LEFT JOIN model.Insurers i ON ip.InsurerId= i.Id) " _
        + "LEFT JOIN model.InsurerPlanTypes pt ON pt.Id = i.InsurerPlanTypeId " _
        + "LEFT JOIN PracticeVendors AS PracticeVendors_1 ON PatientDemographics.ReferringPhysician = PracticeVendors_1.VendorId) " _
        + "LEFT JOIN PracticeVendors AS PracticeVendors_2 ON PatientReferral.ReferredFromId = PracticeVendors_2.VendorId " _
        + "WHERE " + DatesQuery _
        + "ORDER BY PatientDemographics.LastName ASC, PatientDemographics.FirstName ASC "
Else
    MyPracticeRepositoryCmd.CommandText _
        = "SELECT DISTINCT PatientDemographics.PatientId, PatientDemographics.LastName, PatientDemographics.FirstName, " _
        + "Appointments.AppointmentId, Appointments.AppDate, Appointments.ScheduleStatus, Resources.ResourceName, " _
        + "PracticeVendors.VendorName, PracticeVendors_1.VendorName as 'Vendor1', PracticeVendors_2.VendorName as 'Vendor2', " _
        + "i.Name AS InsurerName, PracticeVendors.VendorType, " _
        + "PatientDemographics.Zip, PatientDemographics.BirthDate, " _
        + "AppointmentType.AppointmentType, PatientDemographics.ReferralCatagory , " _
        + "PatientDemographics.Salutation " _
        + "FROM (((((((((PatientDemographics " _
        + "INNER JOIN Appointments ON Appointments.PatientId = PatientDemographics.PatientId) " _
        + "LEFT JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId) " _
        + "LEFT JOIN PatientReferral ON Appointments.ReferralId = PatientReferral.ReferralId) " _
        + "LEFT JOIN PatientClinical ON Appointments.AppointmentId = PatientClinical.AppointmentId) " _
        + "LEFT JOIN model.PatientInsurances pi ON PatientDemographics.PolicyPatientId = pi.InsuredPatientId) " _
        + "LEFT JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId " _
        + "LEFT JOIN PracticeVendors ON PatientDemographics.PrimaryCarePhysician = PracticeVendors.VendorId) " _
        + "LEFT JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId) " _
        + "LEFT JOIN model.Insurers i ON ip.InsurerId= i.Id) " _
        + "LEFT JOIN model.InsurerPlanTypes pt ON pt.Id = i.InsurerPlanTypeId " _
        + "LEFT JOIN PracticeVendors AS PracticeVendors_1 ON PatientDemographics.ReferringPhysician = PracticeVendors_1.VendorId) " _
        + "LEFT JOIN PracticeVendors AS PracticeVendors_2 ON PatientReferral.ReferredFromId = PracticeVendors_2.VendorId " _
        + "WHERE " + DatesQuery _
        + "ORDER BY PatientDemographics.LastName ASC, PatientDemographics.FirstName ASC, Appointments.AppointmentId DESC "
End If
If (BullshitOn) Then
    lstDocs.Clear
'    StartYear = Year(Date)
'    Temp = MyPracticeRepositoryCmd.CommandText
'    For j = StartYear To 1900 Step -1
'        Temp1 = Temp
'        Call ReplaceCharacters(Temp1, "YYYY", Trim(Str(j)))
'        MyPracticeRepositoryCmd.CommandText = Temp1
'        GoSub DoLoad
'    Next j
    GoSub DoLoad
Else
    GoSub DoLoad
End If
lblLoading.Visible = False
If (lstDocs.ListCount > 0) Then
    LoadPendingLetters = True
End If
Exit Function
DoLoad:
    ApplListTotal = 0
    Set ApplListTbl = Nothing
    Set ApplListTbl = CreateAdoRecordset
    Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (ApplListTbl.EOF) Then
        ApplListTbl.MoveLast
        ApplListTotal = ApplListTbl.RecordCount
    End If
    If (ApplListTotal > 0) Then
        If Not (BullshitOn) Then
            lstDocs.Clear
            If (ApplListTotal > 750) Then
                ApplListTotal = 750
            End If
        End If
        ApplListTbl.MoveFirst
        For i = 1 To ApplListTotal
            Call LoadResultList(QueryId)
            If (Trim(PatSal) = "") Then
                TheName = Trim(FirstName) + " " + Trim(LastName)
            Else
                TheName = Trim(PatSal) + " " + Trim(FirstName) + " " + Trim(LastName)
            End If
            DisplayText = Space(75)
            If (Trim(ApptDate) = "") Then
                Mid(DisplayText, 1, 10) = "No Appts  "
            Else
                Mid(DisplayText, 1, 10) = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
            End If
            Mid(DisplayText, 12, 15) = Trim(Left(ApptType, 15))
            Mid(DisplayText, 28, Len(TheName)) = Trim(TheName)
            Mid(DisplayText, 55, 10) = ResourceName
            Mid(DisplayText, 66, 10) = Trim(Left(PCPDr, 10))
            Mid(DisplayText, 75, 1) = ApptStatus
            DisplayText = DisplayText + Trim(Str(ApptId)) + "/" + Trim(Str(PatId))
            If (QueryId = 3) Or (QueryId = 4) Then
                If (lstDocs.ListCount < 1) Then
                    lstDocs.AddItem DisplayText
                ElseIf (Mid(DisplayText, 28, 26) <> Mid(lstDocs.List(lstDocs.ListCount - 1), 28, 26)) Then
                    lstDocs.AddItem DisplayText
                End If
            ElseIf (DisplayText <> lstDocs.List(lstDocs.ListCount - 1)) Then
                lstDocs.AddItem DisplayText
            End If
            If (i <> ApplListTotal) Then
                ApplListTbl.MoveNext
            End If
        Next i
    End If
    Return
Db_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub LoadResultList(IType As Integer)
If (ApplListTbl("AppointmentId") <> "") Then
    ApptId = ApplListTbl("AppointmentId")
Else
    ApptId = 0
End If
If (ApplListTbl("AppDate") <> "") Then
    ApptDate = ApplListTbl("AppDate")
Else
    ApptDate = ""
End If
If (ApplListTbl("AppointmentType") <> "") Then
    ApptType = ApplListTbl("AppointmentType")
Else
    ApptType = ""
End If
If (ApplListTbl("ResourceName") <> "") Then
    ResourceName = ApplListTbl("ResourceName")
Else
    ResourceName = ""
End If
If (ApplListTbl("ScheduleStatus") <> "") Then
    ApptStatus = ApplListTbl("ScheduleStatus")
Else
    ApptStatus = ""
End If
If (ApplListTbl("PatientId") <> "") Then
    PatId = ApplListTbl("PatientId")
Else
    PatId = 0
End If
If (ApplListTbl("Salutation") <> "") Then
    PatSal = ApplListTbl("Salutation")
Else
    PatSal = ""
End If
If (ApplListTbl("LastName") <> "") Then
    LastName = ApplListTbl("LastName")
Else
    LastName = ""
End If
If (ApplListTbl("FirstName") <> "") Then
    FirstName = ApplListTbl("FirstName")
Else
    FirstName = ""
End If
If (ApplListTbl("BirthDate") <> "") Then
    BirthDate = ApplListTbl("BirthDate")
Else
    BirthDate = ""
End If
If (ApplListTbl("VendorName") <> "") Then
    PCPDr = ApplListTbl("VendorName")
Else
    PCPDr = ""
End If
If (ApplListTbl("Vendor1") <> "") Then
    RefDr = ApplListTbl("Vendor1")
Else
    RefDr = ""
End If
If (ApplListTbl("Vendor2") <> "") Then
    RefDrP = ApplListTbl("Vendor2")
Else
    RefDrP = ""
End If
If (ApplListTbl("InsurerName") <> "") Then
    InsName = ApplListTbl("InsurerName")
Else
    InsName = ""
End If
End Sub

Private Function PostToJournal(PatId As Long, ApptId As Long, ALtr As String, ARcv As String, MyImprOn As String, MyLTestsOn As String) As Long
Dim ADate As String
Dim TheDate As String
Dim RetTrans As PracticeTransactionJournal
PostToJournal = 0
If (PatId > 0) And (Trim(ALtr) <> "") And (Trim(ARcv) <> "") Then
    TheDate = ""
    Call FormatTodaysDate(TheDate, False)
    ADate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
    Set RetTrans = New PracticeTransactionJournal
    RetTrans.TransactionJournalId = 0
    If (RetTrans.RetrieveTransactionJournal) Then
        RetTrans.TransactionJournalStatus = "B"
        RetTrans.TransactionJournalTypeId = ApptId
        RetTrans.TransactionJournalAction = "P"
        If (ApptId < 1) Then
            RetTrans.TransactionJournalAction = "-"
            RetTrans.TransactionJournalTypeId = PatId
        End If
        If (Trim(MyImprOn) = "") Then
            MyImprOn = "F"
        End If
        If (Trim(MyLTestsOn) = "") Then
            MyLTestsOn = "F"
        End If
        RetTrans.TransactionJournalBatch = MyImprOn + MyLTestsOn
        RetTrans.TransactionJournalDate = ADate
        RetTrans.TransactionJournalServiceItem = 0
        RetTrans.TransactionJournalType = "M"
        RetTrans.TransactionJournalTime = -1
        RetTrans.TransactionJournalRemark = "/Ltr/" + Trim(ALtr)
        RetTrans.TransactionJournalReference = ARcv
        Call RetTrans.ApplyTransactionJournal
        PostToJournal = RetTrans.TransactionJournalId
    End If
    Set RetTrans = Nothing
ElseIf (PatId = -1) And (ApptId = -1) And (Trim(ALtr) <> "") And (Trim(ARcv) <> "") Then
    TheDate = ""
    Call FormatTodaysDate(TheDate, False)
    ADate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
    Set RetTrans = New PracticeTransactionJournal
    RetTrans.TransactionJournalId = 0
    If (RetTrans.RetrieveTransactionJournal) Then
        RetTrans.TransactionJournalStatus = "B"
        RetTrans.TransactionJournalTypeId = -1
        RetTrans.TransactionJournalAction = "P"
        If (ApptId < 1) Then
            RetTrans.TransactionJournalAction = "-"
            RetTrans.TransactionJournalTypeId = PlacementPerson
        End If
        RetTrans.TransactionJournalBatch = MyImprOn
        RetTrans.TransactionJournalDate = ADate
        RetTrans.TransactionJournalServiceItem = 0
        RetTrans.TransactionJournalType = "M"
        RetTrans.TransactionJournalTime = -1
        RetTrans.TransactionJournalRemark = "/Ltr/" + Trim(ALtr)
        RetTrans.TransactionJournalReference = ARcv
        Call RetTrans.ApplyTransactionJournal
        PostToJournal = RetTrans.TransactionJournalId
    End If
    Set RetTrans = Nothing
End If
End Function

Private Function GetName(PatId As Long, ApptId As Long, IType As String) As String
Dim RetPat As Patient
Dim RetVnd As PracticeVendors
Dim RetRef As PatientReferral
Dim RetAppt As SchedulerAppointment
GetName = ""
If (PatId > 0) Then
    If (IType = "P") Then
        Set RetPat = New Patient
        RetPat.PatientId = PatId
        If (RetPat.RetrievePatient) Then
            GetName = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
        End If
        Set RetPat = Nothing
    ElseIf (IType = "R") Then
        Set RetPat = New Patient
        RetPat.PatientId = PatId
        If (RetPat.RetrievePatient) Then
            If (RetPat.ReferralDr > 0) Then
                Set RetVnd = New PracticeVendors
                RetVnd.VendorId = RetPat.ReferralDr
                If (RetVnd.RetrieveVendor) Then
                    GetName = Trim(RetVnd.VendorLastName) + ", " + Trim(RetVnd.VendorTitle) + ", " + Trim(RetVnd.VendorFirstName)
                End If
                Set RetVnd = Nothing
            End If
        End If
        Set RetPat = Nothing
    ElseIf (IType = "O") Then
        If (ApptId > 0) Then
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = ApptId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                If (RetAppt.AppointmentReferralId > 0) Then
                    Set RetRef = New PatientReferral
                    RetRef.ReferralId = RetAppt.AppointmentReferralId
                    If (RetRef.RetrievePatientReferral) Then
                        Set RetVnd = New PracticeVendors
                        RetVnd.VendorId = RetRef.ReferralFromId
                        If (RetVnd.RetrieveVendor) Then
                            GetName = Trim(RetVnd.VendorLastName) + ", " + Trim(RetVnd.VendorTitle) + ", " + Trim(RetVnd.VendorFirstName)
                        End If
                        Set RetVnd = Nothing
                    End If
                    Set RetRef = Nothing
                End If
            End If
            Set RetAppt = Nothing
        End If
    ElseIf (IType = "D") Then
        Set RetPat = New Patient
        RetPat.PatientId = PatId
        If (RetPat.RetrievePatient) Then
            If (RetPat.PrimaryCarePhysician > 0) Then
                Set RetVnd = New PracticeVendors
                RetVnd.VendorId = RetPat.PrimaryCarePhysician
                If (RetVnd.RetrieveVendor) Then
                    GetName = Trim(RetVnd.VendorLastName) + ", " + Trim(RetVnd.VendorTitle) + ", " + Trim(RetVnd.VendorFirstName)
                End If
                Set RetVnd = Nothing
            End If
        End If
        Set RetPat = Nothing
    End If
End If
End Function

Private Function MaintainLetter(TransId As Long, TheName As String, AFirst As Boolean, ALast As Boolean, PrintOn As Boolean, AFile As String) As Boolean
Dim Temp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplLetters As Letters
If (TransId > 0) Then
    Set ApplLetters = New Letters
    Temp = ApplLetters.ApplLettersFile(TransId)
    Set ApplLetters = Nothing
    If (Trim(Temp) <> "") Then
        lblLoading.Caption = "Letter Creation In Progress for " + TheName
        lblLoading.Visible = True
        DoEvents
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBox = lstPrint
        Call ApplTemp.PrintTransaction(TransId, 2, "m", AFirst, ALast, Temp, False, 0, "S", AFile, 0)
        Call ApplTemp.ApplPurgeTransaction(TransId, True, False)
        Set ApplTemp = Nothing
        lblLoading.Visible = False
        lblLoading.Caption = "Loading"
        DoEvents
    End If
ElseIf (Trim(TheName) = "") And (TransId = -1) Then
    lblLoading.Caption = "Letter Printing In Progress"
    lblLoading.Visible = True
    DoEvents
    Set ApplTemp = New ApplicationTemplates
    Set ApplTemp.lstBox = lstPrint
    Call ApplTemp.PrintTransaction(-1, 1, "z", False, True, "/Ltr/" + TheLetter, False, 0, "S", AFile, 0)
    Set ApplTemp = Nothing
End If
End Function

Private Sub lstLtrs_Click()
Dim i As Integer
If (lstLtrs.ListIndex > 0) Then
    TheLetter = Trim(lstLtrs.List(lstLtrs.ListIndex))
    i = InStrPS(TheLetter, "   ")
    If (i > 0) Then
        TheLetter = Trim(Left(TheLetter, i - 1))
    End If
    i = InStrPS(TheLetter, " ")
    If (i > 0) Then
        TheLetter = Trim(Mid(TheLetter, i + 1, Len(TheLetter) - i))
    End If
    lstLtrs.Visible = False
    lblLtr.Caption = "Letter Changed to " + Trim(TheLetter)
    lblLtr.Visible = True
ElseIf (lstLtrs.ListIndex = 0) Then
    lstLtrs.Visible = False
End If
End Sub
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub
