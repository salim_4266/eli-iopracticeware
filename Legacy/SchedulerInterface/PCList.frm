VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPCList 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "PC List"
   ClientHeight    =   9000
   ClientLeft      =   120
   ClientTop       =   690
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Moveable        =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtPat 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9000
      TabIndex        =   4
      Top             =   360
      Width           =   2775
   End
   Begin VB.ListBox OrdersList 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6750
      ItemData        =   "PCList.frx":0000
      Left            =   120
      List            =   "PCList.frx":0002
      TabIndex        =   1
      Top             =   720
      Width           =   11655
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   2
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCList.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   3
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCList.frx":01E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOrder 
      Height          =   990
      Left            =   5160
      TabIndex        =   6
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PCList.frx":03C2
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9000
      TabIndex        =   5
      Top             =   120
      Width           =   675
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Spectacle Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   1800
   End
End
Attribute VB_Name = "frmPCList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CLFlag As Boolean

Private ManualPatId As Long

Private Sub cmdDone_Click()
Unload frmPCList
End Sub

Private Sub cmdHome_Click()
Unload frmPCList
End Sub

Private Sub cmdOrder_Click()
If (ManualPatId > 0) Then
    If CLFlag Then
        Call CLManualOrders(ManualPatId)
    Else
        Call PCManualOrders(ManualPatId)
    End If
Else
    frmEventMsgs.Header = "Please select a patient."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub Form_Load()
If CLFlag Then
    Label1.Caption = "CL Rx"
Else
    Label1.Caption = "Spectacle Rx"
End If

ManualPatId = 0
Call LoadOrderList
End Sub

Private Sub OrdersList_Click()
Dim i As Integer
Dim ClnId As Long
Dim Idx As Integer
Dim ATemp1 As String
Dim ATemp2 As String
Dim lbl As String
If (OrdersList.ListIndex >= 0) Then
    Idx = OrdersList.ListIndex
    ClnId = OrdersList.ItemData(Idx)
    If (ClnId > 0) Then
        frmEventMsgs.Header = "Action ?"
        frmEventMsgs.AcceptText = "Clinical"
        frmEventMsgs.RejectText = "Patient Demo"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Financial"
        frmEventMsgs.Other1Text = "Order"
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            Call DisplayClinical(ClnId)
        ElseIf (frmEventMsgs.Result = 2) Then
            Call DisplayPatient(ClnId)
        ElseIf (frmEventMsgs.Result = 3) Then
            Call DisplayFinancial(ClnId)
        ElseIf (frmEventMsgs.Result = 5) Then
            ATemp1 = ""
            ATemp2 = ""
            If (Mid(OrdersList.List(Idx), 44, 2) = "OD") Then
                ATemp1 = Mid(OrdersList.List(Idx), 48, Len(OrdersList.List(Idx)) - 47)
                ATemp2 = Mid(OrdersList.List(Idx + 1), 48, Len(OrdersList.List(Idx + 1)) - 47)
                lbl = Mid(OrdersList.List(Idx + 1), 22, 11)
            ElseIf (Mid(OrdersList.List(Idx), 44, 2) = "OS") Then
                ATemp1 = Mid(OrdersList.List(Idx - 1), 48, Len(OrdersList.List(Idx - 1)) - 47)
                ATemp2 = Mid(OrdersList.List(Idx), 48, Len(OrdersList.List(Idx)) - 47)
                lbl = Mid(OrdersList.List(Idx), 22, 11)
            End If
            If CLFlag Then
                Call CLOrders(ClnId, ATemp1, ATemp2, lbl)
            Else
                Call PCOrders(ClnId, ATemp1, ATemp2)
            End If
        End If
    End If
End If
End Sub

Private Sub DisplayClinical(ClnId As Long)
Dim RetCln As PatientClinical
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        frmReviewAppts.StartDate = ""
        DoEvents
        If (frmReviewAppts.LoadApptsList(RetCln.PatientId, RetCln.AppointmentId, True)) Then
            frmReviewAppts.Show 1
        End If
    End If
    Set RetCln = Nothing
End If
End Sub

Private Sub DisplayPatient(ClnId As Long)
Dim RetCln As PatientClinical
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        Dim PatientDemographics As New PatientDemographics
        PatientDemographics.PatientId = RetCln.PatientId
        If Not PatientDemographics.DisplayPatientInfoScreen Then Exit Sub
    End If
    Set RetCln = Nothing
End If
End Sub

Private Sub DisplayFinancial(ClnId As Long)
Dim RetCln As PatientClinical
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
'''''        frmPayments.PatientId = RetCln.PatientId
'''''        frmPayments.ReceivableId = 0
'''''        If (frmPayments.LoadPayments(False, "")) Then
'''''            frmPayments.Show 1
'''''        Else
'''''            frmEventMsgs.Header = "No Active Financials"
'''''            frmEventMsgs.AcceptText = ""
'''''            frmEventMsgs.RejectText = "Ok"
'''''            frmEventMsgs.CancelText = ""
'''''            frmEventMsgs.Other0Text = ""
'''''            frmEventMsgs.Other1Text = ""
'''''            frmEventMsgs.Other2Text = ""
'''''            frmEventMsgs.Other3Text = ""
'''''            frmEventMsgs.Other4Text = ""
'''''            frmEventMsgs.Show 1
'''''        End If
             Dim PatientDemographics As New PatientDemographics
             PatientDemographics.PatientId = RetCln.PatientId
             Call PatientDemographics.DisplayPatientFinancialScreen
             Set PatientDemographics = Nothing
    End If
    Set RetCln = Nothing
End If
End Sub
Private Sub CLManualOrders(PatId As Long)
Exit Sub


If (ManualPatId > 0) Then
'''''    frmCLOrder.PatientId = 0 'PatientId
'''''    frmCLOrder.AppointmentId = 0 'AppointmentId
    frmCLOrder.OrderIdOD = 0
    frmCLOrder.OrderIdOS = 0
    frmCLOrder.InventoryIdOD = 0
    frmCLOrder.InventoryIdOS = 0
'''''    frmCLOrder.ClnId = ClnId
'''''    frmCLOrder.Show 1

    
    
    
    
    
    'frmCLOrder.DemoOn = True
    'frmCLOrder.OrderId = 0
    'frmCLOrder.ClinicalId = 0
    frmCLOrder.AppointmentId = 0
    frmCLOrder.PatientId = ManualPatId
    'frmCLOrder.ODRx = ""
    'frmCLOrder.OSRx = ""
    frmCLOrder.Show 1
    'frmCLOrder.DemoOn = False
    Call LoadOrderList
End If
End Sub

Private Sub PCManualOrders(PatId As Long)
If (ManualPatId > 0) Then
    frmPCOrder.DemoOn = True
    frmPCOrder.OrderId = 0
    frmPCOrder.ClinicalId = 0
    frmPCOrder.AppointmentId = 0
    frmPCOrder.PatientId = ManualPatId
    frmPCOrder.ODRx = ""
    frmPCOrder.OSRx = ""
    frmPCOrder.Show 1
    frmPCOrder.DemoOn = False
    Call LoadOrderList
End If
End Sub

Private Sub CLOrders(ClnId As Long, ODRx As String, OSRx As String, Optional lbl As String)

Dim ProceedOn As Boolean
ProceedOn = CheckConfigCollection("INVENTORYON") = "T"
If (ProceedOn) Then
    Dim InventId As String
    Dim Delim As Integer

    InventId = getInvId(ClnId)
    If InventId = "0&0" Then
        frmEventMsgs.Header = "No CL selected. Please re-prescribe."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        frmCLOrder.PatientId = 0 'PatientId
        frmCLOrder.AppointmentId = 0 'AppointmentId
        frmCLOrder.OrderIdOD = 0
        frmCLOrder.OrderIdOS = 0
        
        Delim = InStrPS(1, InventId, "&")
        frmCLOrder.InventoryIdOD = Mid(InventId, 1, Delim - 1)
        frmCLOrder.InventoryIdOS = Mid(InventId, Delim + 1)
        
        frmCLOrder.ClnId = ClnId
        frmCLOrder.lbl = lbl
        frmCLOrder.Show 1
    End If
End If
End Sub

Private Function getInvId(ClnId As Long) As String
getInvId = ""

Dim SQL As String
SQL = "Select * FROM PatientClinical WHERE clinicalid = " & ClnId
Dim RS As Recordset
Set RS = GetRS(SQL)
If RS.RecordCount = 0 Then Exit Function

Dim FDet As String
FDet = RS("FindingDetail")
Dim Delim As Long

Delim = InStrPS(1, FDet, "-5/")
FDet = Mid(FDet, Delim + 3)
Delim = InStrPS(1, FDet, "-6/")
getInvId = Trim(Mid(FDet, 1, Delim - 1))

FDet = Mid(FDet, Delim + 3)
Delim = InStrPS(1, FDet, "-7/")
getInvId = getInvId & "&" & Trim(Mid(FDet, 1, Delim - 1))
End Function


Private Sub PCOrders(ClnId As Long, ODRx As String, OSRx As String)
Dim PatId As Long
Dim ApptId As Long
Dim RetCln As PatientClinical
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        PatId = RetCln.PatientId
        ApptId = RetCln.AppointmentId
    End If
    Set RetCln = Nothing
    frmPCOrder.DemoOn = True
    frmPCOrder.OrderId = 0
    frmPCOrder.ClinicalId = ClnId
    frmPCOrder.PatientId = PatId
    frmPCOrder.AppointmentId = ApptId
    frmPCOrder.ODRx = ODRx
    frmPCOrder.OSRx = OSRx
    frmPCOrder.Show 1
End If
End Sub

Private Sub txtPat_KeyPress(KeyAscii As Integer)
KeyAscii = 0
Call txtPat_Click
End Sub

Private Sub txtPat_Click()
Dim RetPat As Patient
Dim ReturnArguments As Variant

Set ReturnArguments = DisplayPatientSearchScreen(False)
If Not (ReturnArguments Is Nothing) Then txtPat.Tag = ReturnArguments.PatientId
If (val(Trim(txtPat.Tag)) > 0) Then
    ManualPatId = val(Trim(txtPat.Tag))
    Set RetPat = New Patient
    RetPat.PatientId = val(Trim(txtPat.Tag))
    If (RetPat.RetrievePatient) Then
        txtPat.Text = Trim(RetPat.LastName) + ", " + Trim(RetPat.FirstName)
    End If
    Set RetPat = Nothing
    Call LoadOrderList
End If
End Sub

Private Function LoadOrderList() As Boolean
Dim ADate As String
Dim ApplList As ApplicationTemplates
ADate = ""
Call FormatTodaysDate(ADate, False)
ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
If (Trim(txtPat.Tag) <> "") Then
    ADate = ""
End If
OrdersList.Clear
Set ApplList = New ApplicationTemplates
Set ApplList.lstBoxA = OrdersList
If CLFlag Then
    Call ApplList.LoadCLRx(ADate, val(Trim(txtPat.Tag)))
Else
    Call ApplList.LoadSpectacleRx(ADate, val(Trim(txtPat.Tag)))
End If
Set ApplList = Nothing
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
