VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCCDNLab 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "frmCCDNLab"
   ClientHeight    =   7755
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10425
   ClipControls    =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   7755
   ScaleWidth      =   10425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdCCDImport 
      Height          =   1095
      Left            =   860
      TabIndex        =   0
      Top             =   600
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CCDNLab.frx":0000
      Begin fpBtnAtlLibCtl.fpBtn cmdMain14 
         Height          =   975
         Left            =   2400
         TabIndex        =   1
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1720
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "CCDNLab.frx":01F6
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1095
      Left            =   840
      TabIndex        =   2
      Top             =   6480
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   7828525
      Redraw          =   -1  'True
      ButtonDesigner  =   "CCDNLab.frx":03DF
      Begin fpBtnAtlLibCtl.fpBtn fpBtn2 
         Height          =   975
         Left            =   2400
         TabIndex        =   3
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1720
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "CCDNLab.frx":05C2
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCCDExport 
      Height          =   1095
      Left            =   840
      TabIndex        =   4
      Top             =   2400
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CCDNLab.frx":07AB
      Begin fpBtnAtlLibCtl.fpBtn fpBtn3 
         Height          =   975
         Left            =   2400
         TabIndex        =   5
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1720
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11098385
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "CCDNLab.frx":09A1
      End
   End
End
Attribute VB_Name = "frmCCDNLab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim PatId As Long
Dim PatientName As String
Dim frm As CCDImport.frmLaikaReport
Dim CCDGen As New CCDExport.CCDGenerator
Private Sub cmdCCDExport_Click()
Dim DoAgain As Boolean
If Login() Then
    DoAgain = True
    While (DoAgain)
        frmCCDNLab.Enabled = False
        frmPatientSchedulerSearch.AddOn = True
        frmPatientSchedulerSearch.Header = "Schedule A Patient"
        frmPatientSchedulerSearch.Show 1
        PatId = frmPatientSchedulerSearch.PatientId
        PatientName = frmPatientSchedulerSearch.FirstName
        Unload frmPatientSchedulerSearch
        If (PatId > 0) Then
            Call ExportCCDXML
            DoAgain = False
        ElseIf (PatId = -1) Then
            PatId = 0
            DoAgain = False
            On Error Resume Next
        Else
            DoAgain = False
        End If
        frmCCDNLab.Enabled = True
    Wend
End If
End Sub

Private Sub cmdCCDImport_Click()
Dim DoAgain As Boolean
If Login() Then
    DoAgain = True
    While (DoAgain)
        frmCCDNLab.Enabled = False
        frmPatientSchedulerSearch.AddOn = True
        frmPatientSchedulerSearch.Header = "Schedule A Patient"
        frmPatientSchedulerSearch.Show 1
        PatId = frmPatientSchedulerSearch.PatientId
        PatientName = frmPatientSchedulerSearch.FirstName
        Unload frmPatientSchedulerSearch
        If (PatId > 0) Then
            Call ImportCCDXML
            DoAgain = False
        ElseIf (PatId = -1) Then
            PatId = 0
            Call ImportCCDXML
            DoAgain = False
            On Error Resume Next
        Else
            DoAgain = False
        End If
        frmCCDNLab.Enabled = True
    Wend
    End If
End Sub
Private Function Login() As Boolean
    If LenB(UserLogin.sNameLong) Then
        Login = True
    Else
        Set FSignIn.FormLogin = UserLogin
        FSignIn.Show 1
        If LenB(UserLogin.sNameLong) Then
            Login = True
            If UserLogin.HasPermission(epPowerUser) Then
                frmHome.BorderStyle = 1
                frmHome.ClipControls = True
                frmHome.Caption = Mid$(frmHome.Name, 4, Len(frmHome.Name) - 3)
                frmHome.AutoRedraw = True
                frmHome.Refresh
            End If
        End If
    End If
End Function
Public Sub ExportCCDXML()
CCDGen.ConnectionString = ServerConnectionString
CCDGen.PatientId = PatId
CCDGen.PatientName = PatientName
CCDGen.MDBFilePath = LocalPinPointDirectory
CCDGen.CCDSavePath = PinPointDirectory
CCDGen.GenerateCCDXML CCDGen.PatientId, CCDGen.PatientName
End Sub
Public Sub ImportCCDXML()
Set frm = New frmLaikaReport
frm.ConnectionString = ServerConnectionString
frm.PatientId = PatId
frm.UserId = UserLogin.iId
frm.FilePath = PinPointDirectory + "ClinicalExchange"
frm.FileSavePath = PinPointDirectory + "MyScan"
frm.Show
End Sub
Private Sub cmdHome_Click()
Unload frmCCDNLab
End Sub
Public Sub FrmClose()
If Not frm Is Nothing Then
frm.Close
End If
Call cmdHome_Click
Unload Me
End Sub
