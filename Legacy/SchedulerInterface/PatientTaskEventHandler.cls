VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientTaskEventHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler

Private m_PatientId As Long
Private m_PatientScreenToOpen As String
Dim pat As New Patient
Dim answer As Integer
Dim appointment As Long
Dim question As String
Dim referalid As Long
Dim frx As Form, fCount As Integer
Dim exam As New DI_ExamClinical

Property Get PatientId() As Long
      PatientId = m_PatientId
End Property
Property Get PatientScreenToOpen() As String
      PatientScreenToOpen = m_PatientScreenToOpen
End Property

Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)
Dim PatRunsUsed As Long
fCount = 0
appointment = 0
question = ""
m_PatientId = 0
referalid = 0
Dim task As Variant
task = e.task
m_PatientId = e.task.PatientId
m_PatientScreenToOpen = e.ScreenToOpen
appointment = exam.CurrentAppointment(PatientId)
question = exam.CurrentAppointmentQuestion(PatientId)
referalid = exam.CurrentAppointmentReferal(PatientId)
Dim PatientDemographics As PatientDemographics

If PatientScreenToOpen = "Chart" Then
    For Each frx In Forms()
        If frx.Name = frmReviewAppts.Name Then
           fCount = 1
        End If
    Next
    
    If fCount > 0 Then
        answer = MsgBox("The PatientChart is already open Do you want to save changes before closing?", vbInformation + vbYesNo, "Add Confirm")
        If answer = vbYes Then
            Unload frmReviewAppts
        Else
            Unload frmReviewAppts
        End If
    End If
    
    PatRunsUsed = pat.GetPatientAccess(m_PatientId)
    If PatRunsUsed = -1 Then
        frmEventMsgs.InfoMessage "Access Blocked"
        frmEventMsgs.Show 1
        Exit Sub
    End If
        
    If (frmReviewAppts.LoadApptsList(m_PatientId, 0, False)) Then
        frmReviewAppts.Show 1, frmHome
    Else
        frmEventMsgs.InfoMessage "No Appointments"
        frmEventMsgs.Show 1
        frmReviewAppts.FrmClose
    End If
     
    Exit Sub
End If

If PatientScreenToOpen = "Financials" Then
    Set PatientDemographics = New PatientDemographics
    PatientDemographics.PatientId = PatientId
    Call PatientDemographics.DisplayPatientFinancialScreen
    Set PatientDemographics = Nothing
Exit Sub
End If

If PatientScreenToOpen = "Info" Then
    Set PatientDemographics = New PatientDemographics
    PatientDemographics.PatientId = PatientId
    Call PatientDemographics.DisplayPatientInfoScreen
    Set PatientDemographics = Nothing
  Exit Sub
End If

End Sub

