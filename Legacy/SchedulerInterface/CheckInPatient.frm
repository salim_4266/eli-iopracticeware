VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmCheckInPatient 
   BackColor       =   &H00785211&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00785211&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   360
      TabIndex        =   0
      Top             =   960
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   9724948
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "CheckInPatient.frx":0000
      Left            =   9480
      List            =   "CheckInPatient.frx":0002
      TabIndex        =   12
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "CheckInPatient.frx":0004
      Left            =   7440
      List            =   "CheckInPatient.frx":0006
      TabIndex        =   8
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstPayment 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   4800
      Sorted          =   -1  'True
      TabIndex        =   7
      Top             =   840
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H00946414&
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5700
      ItemData        =   "CheckInPatient.frx":0008
      Left            =   360
      List            =   "CheckInPatient.frx":000A
      TabIndex        =   2
      Top             =   1440
      Width           =   11055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   360
      TabIndex        =   3
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   0   'False
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   1
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CheckInPatient.frx":000C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9600
      TabIndex        =   4
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   0   'False
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   1
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CheckInPatient.frx":01EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrePrint 
      Height          =   990
      Left            =   3360
      TabIndex        =   6
      Top             =   7200
      Visible         =   0   'False
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   0   'False
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   1
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CheckInPatient.frx":03CA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   990
      Left            =   6600
      TabIndex        =   11
      Top             =   7200
      Visible         =   0   'False
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   0   'False
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   1
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CheckInPatient.frx":05B7
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9480
      TabIndex        =   13
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   5640
      TabIndex        =   10
      Top             =   960
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   7440
      TabIndex        =   9
      Top             =   120
      Width           =   570
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00785211&
      Caption         =   "Appointment Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   5
      Top             =   720
      Width           =   1560
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00785211&
      Caption         =   "Check In Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   360
      Left            =   4680
      TabIndex        =   1
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "frmCheckInPatient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public AppointmentDate As String
Private ResourceId As Long
Private LocationId As Long
Private PatientId As Long
Private CPayAmount As Single
Private CPayMethod As String
Private CPayRef As String

Private Sub cmdDone_Click()
Dim ApptId As Long
Dim Temp As String
Dim ADate As String
Dim ApplList As ApplicationAIList
Dim CheckInFlag As Boolean
If (lstAppts.ListIndex >= 0) Then
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Check In Print"
    frmEventMsgs.RejectText = "Check In Only"
    frmEventMsgs.CancelText = "Quit"
    frmEventMsgs.Other0Text = "Check In Copay"
    frmEventMsgs.Other1Text = "Check In Copay Print"
    frmEventMsgs.Other2Text = "Cancel"
    frmEventMsgs.Other3Text = "Patient  Info"
    frmEventMsgs.Other4Text = "Print    Only"
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Or (frmEventMsgs.Result = 2) Then
        ADate = AppointmentDate
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
        If ApplList.CheckAppointmentLocation(ApptId) Then
            If (ApplList.ApplIsIntraDayAppointmentOn(ApptId, ADate)) Then
                CheckInFlag = False
                frmEventMsgs.Header = "Patient has another active appointment which must be checked out first."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            ElseIf Not (ApplList.ApplCheckPatientDemo(lstAppts.List(lstAppts.ListIndex))) Then
                CheckInFlag = False
                frmEventMsgs.Header = "Patient birthdate, gender, language, race and ethnicity must be completed prior to check-in."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            ElseIf Not (ApplList.ApplCheckInPatient(lstAppts.List(lstAppts.ListIndex), AppointmentDate, CPayAmount, CPayMethod, CPayRef, UserLogin.iId)) Then
                CheckInFlag = False
                frmEventMsgs.Header = "Cannot Check In (Check Patient, PolicyHolder and Insurer Information or Patient Checked-In already.)"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            Else
                CheckInFlag = True
                CPayAmount = 0
                CPayMethod = ""
                CPayRef = ""
                If (frmEventMsgs.Result = 1) Then
                    Call cmdPrePrint_Click
                End If
            End If
            Set ApplList = Nothing
            If CheckInFlag Then
                Call AppointmentLoadList
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Or (frmEventMsgs.Result = 5) Then
        ADate = AppointmentDate
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
        If (ApptId > 0) Then
            If ApplList.CheckAppointmentLocation(ApptId) Then
                If (ApplList.ApplIsIntraDayAppointmentOn(ApptId, ADate)) Then
                    CheckInFlag = False
                    frmEventMsgs.Header = "Patient has a currently checked in, active appointment."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                ElseIf Not (ApplList.ApplCheckPatientDemo(lstAppts.List(lstAppts.ListIndex))) Then
                    CheckInFlag = False
                    frmEventMsgs.Header = "Patient birthdate, gender, language, race and ethnicity must be completed prior to check-in."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                ElseIf (frmCollectPayment.LoadCollect(ApptId, 0, "", "")) Then
                    CheckInFlag = False
                    frmCollectPayment.Show 1
                    If Not (frmCollectPayment.QuitOn) Then
                        CPayAmount = frmCollectPayment.CopayAmount
                        CPayMethod = frmCollectPayment.CopayMethod
                        CPayRef = frmCollectPayment.CopayRef
                        If Not (ApplList.ApplCheckInPatient(lstAppts.List(lstAppts.ListIndex), AppointmentDate, CPayAmount, CPayMethod, CPayRef, UserLogin.iId)) Then
                            frmEventMsgs.Header = "Cannot Check In (Check Patient, PolicyHolder and Insurer Information or Patient Checked-In already.)"
                            frmEventMsgs.AcceptText = ""
                            frmEventMsgs.RejectText = "Ok"
                            frmEventMsgs.CancelText = ""
                            frmEventMsgs.Other0Text = ""
                            frmEventMsgs.Other1Text = ""
                            frmEventMsgs.Other2Text = ""
                            frmEventMsgs.Other3Text = ""
                            frmEventMsgs.Other4Text = ""
                            frmEventMsgs.Show 1
                        Else
                            CPayAmount = 0
                            CPayMethod = ""
                            CPayRef = ""
                            CheckInFlag = True
                            If (frmEventMsgs.Result = 5) Then
                                Call cmdPrePrint_Click
                            End If
                        End If
                    End If
                End If
            End If
        End If
        Set ApplList = Nothing
        If CheckInFlag Then
            Call AppointmentLoadList
        End If
    ElseIf (frmEventMsgs.Result = 4) Then
        lstAppts.ListIndex = -1
    ElseIf (frmEventMsgs.Result = 6) Then
        Set ApplList = New ApplicationAIList
        ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
        Dim sValid As New Validations
        Dim PassValid As Boolean
        PassValid = sValid.AppCancel(ApptId)
        Set sValid = Nothing
        If PassValid Then
            Set ApplList = New ApplicationAIList
            ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
                    
            Dim arguments() As Variant
            Dim ReturnArguments As Variant
            Dim CancelAppointmentComWrapper As New comWrapper
            Call CancelAppointmentComWrapper.Create(CancelAppointmentLoadArgumentsType, emptyArgs)
            
            Dim wrapper As New comWrapper
            Call wrapper.Create("System.Collections.Generic.List`1[[System.Int32]]", emptyArgs)
            
            Dim AddItemArgs() As Variant
            AddItemArgs = Array(ApptId)
            Call wrapper.InvokeMethod("Add", AddItemArgs)

            Dim loadArguments As Object
            Set loadArguments = CancelAppointmentComWrapper.Instance
            loadArguments.ListOfAppointmentId = wrapper.Instance

            arguments = Array(loadArguments, True, Nothing)

            Call CancelAppointmentComWrapper.Create(CancelAppointmentViewManagerType, emptyArgs)
            Set ReturnArguments = CancelAppointmentComWrapper.InvokeMethod("ShowCancelAppointment", arguments)
        
            Call AppointmentLoadList
        End If
    ElseIf (frmEventMsgs.Result = 7) Then
        Call cmdPatient_Click
    ElseIf (frmEventMsgs.Result = 8) Then
        Call cmdPrePrint_Click
    End If
Else
    CurrentAction = "Home"
    Unload frmCheckInPatient
End If
End Sub

Private Sub cmdHome_Click()
CurrentAction = "Home"
Unload frmCheckInPatient
End Sub

Private Sub cmdPrePrint_Click()
Dim ApptId As Long
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
    On Error GoTo lcmdPrePrint_Click_Error

If (lstAppts.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
    PatientId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
    Set ApplList = Nothing
    lblPrint.Caption = "Printing In Progress"
    lblPrint.Visible = True
    Set ApplTemp = New ApplicationTemplates
    Set ApplTemp.lstBox = lstPayment
    Call ApplTemp.PrintTransaction(ApptId, PatientId, "C", True, True, "", False, 0, "S", "", 0)
    Set ApplTemp = Nothing
    lblPrint.Visible = False
    lstAppts.Clear
    DoEvents
    If Not (AppointmentLoadList) Then
        Call cmdHome_Click
    End If
Else
    frmEventMsgs.Header = "Make a list selection"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If

    Exit Sub

lcmdPrePrint_Click_Error:
    LogError "frmCheckInPatient", "cmdPrePrint_Click", Err, Err.Description, , PatientId, ApptId
End Sub

Private Sub cmdPatient_Click()
Dim ApptId As Long
Dim AStatus As String
Dim ApplList As ApplicationAIList
If (lstAppts.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    PatientId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
    ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
    Set ApplList = Nothing
    Dim PatientDemoGraphics As New PatientDemoGraphics
    PatientDemoGraphics.PatientId = PatientId
    If Not PatientDemoGraphics.DisplayPatientInfoScreen Then Exit Sub
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplGetAppointmentStatus(ApptId, AStatus)
    Set ApplList = Nothing
    If (InStrPS("PRA", AStatus) = 0) Then
        lstAppts.RemoveItem lstAppts.ListIndex
    End If
Else
    frmEventMsgs.Header = "Make a list selection"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub Form_Load()
CPayAmount = 0
CPayMethod = ""
CPayRef = ""
If UserLogin.HasPermission(epPowerUser) Then
    frmCheckInPatient.BorderStyle = 1
    frmCheckInPatient.ClipControls = True
    frmCheckInPatient.Caption = Mid(frmCheckInPatient.Name, 4, Len(frmCheckInPatient.Name) - 3)
    frmCheckInPatient.AutoRedraw = True
    frmCheckInPatient.Refresh
End If
End Sub

Private Sub lstAppts_DblClick()
If (lstAppts.ListIndex >= 0) Then
    Call cmdDone_Click
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        LocationId = -1
    Else
        Set ApplList = New ApplicationAIList
        LocationId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
    Call AppointmentLoadList
    If (frmPatientLocator.Visible) Then
        If (lstAppts.ListCount < 1) Then
            frmEventMsgs.Header = "Nothing Outstanding"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            LocationId = dbPracticeId
            DoEvents
            Call AppointmentLoadList
            lstLoc.ListIndex = -1
        End If
    End If
End If
End Sub

Private Sub SSDateCombo1_Change()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_Click()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_CloseUp()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") Then
        AppointmentDate = SSDateCombo1.DateValue
        Call FormatTodaysDate(AppointmentDate, False)
        If (AppointmentDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = AppointmentDate
        End If
    Else
        If (AppointmentDate = "") Then
            SSDateCombo1.Text = ""
        ElseIf (SSDateCombo1.DateValue = "") Then
            SSDateCombo1.Text = AppointmentDate
        Else
            AppointmentDate = SSDateCombo1.DateValue
            AppointmentDate = Mid(AppointmentDate, 1, 2) + Mid(AppointmentDate, 4, 2) + Mid(AppointmentDate, 9, 2)
        End If
        ATemp = AppointmentDate
        If (Len(AppointmentDate) > 10) Then
            ATemp = Mid(AppointmentDate, 11, Len(AppointmentDate) - 10)
        ElseIf (AppointmentDate = "") Then
            Call FormatTodaysDate(AppointmentDate, False)
            ATemp = Mid(AppointmentDate, 1, 2) + Mid(AppointmentDate, 4, 2) + Mid(AppointmentDate, 9, 2)
        End If
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        AppointmentDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(AppointmentDate, False)
        If (AppointmentDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = AppointmentDate
        End If
    End If
    Call AppointmentLoadList
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        AppointmentDate = AppointmentDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(AppointmentDate) > 0) Then
        AppointmentDate = Left(AppointmentDate, Len(AppointmentDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(AppointmentDate) < 1) Then
        AppointmentDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo1_Validate(Cancel As Boolean)
Dim Temp As String
Temp = SSDateCombo1.Date
Call FormatTodaysDate(Temp, False)
If (AppointmentDate <> Temp) Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As ApplicationAIList
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = 0
    Else
        Set ApplList = New ApplicationAIList
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
    Call AppointmentLoadList
    If (lstAppts.ListCount < 1) Then
        frmEventMsgs.Header = "Nothing Outstanding"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        ResourceId = 0
        DoEvents
        Call AppointmentLoadList
        lstDr.ListIndex = -1
    End If
End If
End Sub

Public Function AppointmentLoadList() As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim LocId As Long
Dim RefOn As Boolean
Dim Temp As String
Dim TheDate As String
Dim LocalDate As ManagedDate
Dim ApptList As ApplicationAIList
AppointmentLoadList = False
Set LocalDate = New ManagedDate
If (lstDr.ListCount < 1) Then
    Set ApptList = New ApplicationAIList
    Set ApptList.ApplList = lstDr
    Call ApptList.ApplLoadStaff(False)
    Set ApptList = Nothing
    ResourceId = 0
End If
If (lstLoc.ListCount < 1) Then
    Set ApptList = New ApplicationAIList
    Set ApptList.ApplList = lstLoc
    Call ApptList.ApplLoadLocation(False, True)
    Set ApptList = Nothing
    LocationId = dbPracticeId
End If
If CheckConfigCollection("MULTIOFFICE") = "T" Then
    Label1.Visible = True
    lstLoc.Visible = True
Else
    Label1.Visible = False
    lstLoc.Visible = False
    LocationId = -1
End If
RefOn = CheckConfigCollection("REQUIREREFDOC") = "T"
lstAppts.Clear
Set ApptList = New ApplicationAIList
TheDate = ""
If (Trim(AppointmentDate) <> "") Then
    TheDate = AppointmentDate
End If
Call FormatTodaysDate(TheDate, False)
LocalDate.ExposedDate = TheDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    ApptList.ApplDate = LocalDate.ExposedDate
End If
Set ApptList.ApplList = lstAppts
ApptList.ApplResourceId = ResourceId
ApptList.ApplLocId = LocationId
Call ApptList.ApplRetrieveCheckInList(RefOn)
AppointmentLoadList = True
Set LocalDate = Nothing
Set ApptList = Nothing
If (LocationId > -1) And Not (frmCheckInPatient.Visible) Then
    For i = 0 To lstLoc.ListCount - 1
        If (LocationId = val(Trim(Mid(lstLoc.List(i), 56, 5)))) Then
            LocId = i
            Exit For
        End If
    Next i
    If (LocId > 0) Then
        lstLoc.ListIndex = LocId
    End If
End If
Exit Function
UIError_Label:
    Set LocalDate = Nothing
    Set ApptList = Nothing
    Resume LeaveFast
LeaveFast:
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
