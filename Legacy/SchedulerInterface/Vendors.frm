VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmVendors 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtNPI 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   10
      TabIndex        =   50
      Top             =   4320
      Width           =   3375
   End
   Begin VB.CheckBox chkXRef 
      BackColor       =   &H0077742D&
      Caption         =   "Exclude Referring Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8280
      TabIndex        =   49
      Top             =   5760
      Width           =   3015
   End
   Begin VB.TextBox txtTaxo 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   16
      TabIndex        =   17
      Top             =   3840
      Width           =   3375
   End
   Begin VB.TextBox TaxId 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   32
      TabIndex        =   48
      Top             =   3360
      Width           =   3375
   End
   Begin VB.TextBox txtFirm 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   16
      Top             =   3840
      Width           =   4695
   End
   Begin VB.TextBox txtSal 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   25
      TabIndex        =   14
      Top             =   2880
      Width           =   3015
   End
   Begin VB.TextBox txtTitle 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   8
      TabIndex        =   3
      Top             =   960
      Width           =   1935
   End
   Begin VB.TextBox txtMI 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   1
      TabIndex        =   6
      Top             =   1440
      Width           =   495
   End
   Begin VB.TextBox txtFName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7080
      MaxLength       =   16
      TabIndex        =   5
      Top             =   1440
      Width           =   2055
   End
   Begin VB.TextBox txtLName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   20
      TabIndex        =   4
      Top             =   1440
      Width           =   3135
   End
   Begin VB.TextBox txtUpin 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   6
      TabIndex        =   18
      Top             =   4320
      Width           =   4695
   End
   Begin VB.ListBox lstAff 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1260
      ItemData        =   "Vendors.frx":0000
      Left            =   2280
      List            =   "Vendors.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   21
      Top             =   5760
      Width           =   4575
   End
   Begin VB.TextBox txtOtherOffice2 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   80
      TabIndex        =   20
      Top             =   5280
      Width           =   8055
   End
   Begin VB.TextBox txtOtherOffice1 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   80
      TabIndex        =   19
      Top             =   4800
      Width           =   8055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   990
      Left            =   240
      TabIndex        =   24
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Vendors.frx":0004
   End
   Begin VB.TextBox Fax 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   16
      TabIndex        =   13
      Top             =   2880
      Width           =   1695
   End
   Begin VB.TextBox VType 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   1
      TabIndex        =   0
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox VName 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   2
      Top             =   960
      Width           =   4935
   End
   Begin VB.TextBox State 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6720
      MaxLength       =   2
      TabIndex        =   10
      Top             =   2400
      Width           =   615
   End
   Begin VB.TextBox Zip 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   10
      TabIndex        =   11
      Top             =   2400
      Width           =   1695
   End
   Begin VB.TextBox Address 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   50
      TabIndex        =   7
      Top             =   1920
      Width           =   4935
   End
   Begin VB.TextBox City 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   30
      TabIndex        =   9
      Top             =   2400
      Width           =   3135
   End
   Begin VB.TextBox Phone 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   16
      TabIndex        =   12
      Top             =   2880
      Width           =   2415
   End
   Begin VB.TextBox Suite 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   10
      TabIndex        =   8
      Top             =   1920
      Width           =   1695
   End
   Begin VB.TextBox Email 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   15
      Top             =   3360
      Width           =   3495
   End
   Begin VB.TextBox Specialty 
      Appearance      =   0  'Flat
      BackColor       =   &H0093A22D&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4800
      MaxLength       =   64
      TabIndex        =   1
      Top             =   480
      Width           =   6975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10080
      TabIndex        =   22
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Vendors.frx":01E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5160
      TabIndex        =   23
      Top             =   7200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Vendors.frx":03C2
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "NPI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Index           =   2
      Left            =   7080
      TabIndex        =   51
      Top             =   4320
      Width           =   1260
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Taxonomy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Index           =   1
      Left            =   7080
      TabIndex        =   47
      Top             =   3840
      Width           =   1260
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Firm Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   46
      Top             =   3840
      Width           =   1575
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Salutation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7440
      TabIndex        =   45
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7800
      TabIndex        =   44
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "MI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      TabIndex        =   43
      Top             =   1440
      Width           =   495
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      TabIndex        =   42
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   720
      TabIndex        =   41
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "UPIN"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   915
      TabIndex        =   40
      Top             =   4320
      Width           =   1140
   End
   Begin VB.Label lblAffiliations 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Affiliations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   600
      TabIndex        =   39
      Top             =   5760
      Width           =   1455
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Other Office"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   38
      Top             =   5280
      Width           =   1815
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Other Office"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      TabIndex        =   37
      Top             =   4800
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4800
      TabIndex        =   36
      Top             =   2880
      Width           =   735
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   960
      TabIndex        =   35
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Display Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   34
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Tax Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Index           =   0
      Left            =   7200
      TabIndex        =   33
      Top             =   3360
      Width           =   1140
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      TabIndex        =   32
      Top             =   2400
      Width           =   1020
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   960
      TabIndex        =   31
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8100
      TabIndex        =   30
      Top             =   2400
      Width           =   555
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   960
      TabIndex        =   29
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   960
      TabIndex        =   28
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Suite"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7680
      TabIndex        =   27
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label Label28 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "E-mail"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   930
      TabIndex        =   26
      Top             =   3360
      Width           =   1125
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialty"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3360
      TabIndex        =   25
      Top             =   480
      Width           =   1290
   End
End
Attribute VB_Name = "frmVendors"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheType As String
Public DeleteOn As Boolean
Public TheVendorId As Long
Public VendorType As String
Private RealDone As Boolean

Private Sub cmdApply_Click()
Dim XRef As Boolean
Dim TheAction As String
Dim Temp As String
Dim ApplTbl As ApplicationTables

If (Trim(VType.Text) = "") Then
    frmEventMsgs.Header = "Please Select A Vendor Type"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    VType.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(txtLName.Text) = "") And Trim(VType.Text) = "D" Then
    frmEventMsgs.Header = "Please Select A Vendor Last Name"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtLName.SetFocus
    SendKeys "{Home}"
    Exit Sub
End If
If (Trim(VName.Text) = "") Then
    If (Trim(txtFName.Text) <> "") Then
        Temp = Trim(txtFName.Text) + " " + Trim(txtLName.Text)
    Else
        Temp = Trim(txtLName.Text)
    End If
Else
    Temp = Trim(VName.Text)
End If
Set ApplTbl = New ApplicationTables
XRef = False
If (chkXRef.Value = 1) Then
    XRef = True
End If
If (ApplTbl.ApplPostVendor(TheVendorId, VType.Text, Temp, txtLName.Text, txtFName.Text, _
                            txtMI.Text, txtTitle.Text, Specialty.Text, Address.Text, Suite.Text, City.Text, _
                            State.Text, Zip.Text, Phone.Text, TaxId.Text, txtUpin.Text, Email.Text, _
                            Fax.Text, txtOtherOffice1.Text, txtOtherOffice2.Text, txtSal.Text, txtFirm.Text, _
                            txtTaxo.Text, XRef, txtNPI.Text)) Then
Else
    frmEventMsgs.Header = "Update not performed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set ApplTbl = Nothing
If (RealDone) Then
    Unload frmVendors
End If
End Sub

Private Sub cmdBack_Click()
Unload frmVendors
End Sub

Private Sub cmdDelete_Click()
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
If Not (ApplTbl.ApplIsVendorInUse(TheVendorId)) Then
    If (ApplTbl.ApplDeleteVendor(TheVendorId)) Then
        TheVendorId = 0
        Unload frmVendors
    End If
Else
    frmEventMsgs.Header = "Vendor is in use. Delete Denied."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set ApplTbl = Nothing
End Sub

Public Function VendorLoadDisplay(VendorId As Long, PermOn As Boolean) As Boolean
Dim ApplTbl As ApplicationTables
Dim ATaxo As String, NPI As String
Dim AName As String, ALName As String, AFName As String
Dim AMI As String, ATitle As String, AVType As String
Dim AAddr As String, ASuite As String, ACity As String
Dim AState As String, AZip As String, APhone As String
Dim AFax As String, ATaxId As String, AUpin As String
Dim AEmail As String, ASpec As String, AOther1 As String
Dim AOther2 As String, ASal As String, ADisplay As String
Dim XRef As Boolean
RealDone = True
TheVendorId = VendorId
VendorLoadDisplay = True
cmdDelete.Visible = DeleteOn
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstAff
Call ApplTbl.ApplLoadVendor(VendorId, AName, ALName, AFName, AMI, _
                            ATitle, AVType, AAddr, ASuite, ACity, _
                            AState, AZip, APhone, AFax, ATaxId, AUpin, _
                            AEmail, ASpec, AOther1, AOther2, ASal, _
                            ADisplay, ATaxo, XRef, NPI)
VName.Text = AName
VType.Text = AVType
If (Trim(AVType) = "") Then
    VType.Text = TheType
End If
txtLName.Text = ALName
txtFName.Text = AFName
txtMI.Text = AMI
txtTitle.Text = ATitle
Address.Text = AAddr
Suite.Text = ASuite
City.Text = ACity
State.Text = AState
Zip.Text = AZip
Phone.Text = APhone
Fax.Text = AFax
TaxId.Text = ATaxId
txtUpin.Text = AUpin
txtFirm.Text = ADisplay
Email.Text = AEmail
Specialty.Text = ASpec
txtOtherOffice1.Text = AOther1
txtOtherOffice2.Text = AOther2
txtSal.Text = ASal
txtTaxo.Text = ATaxo
txtNPI.Text = NPI
chkXRef.Value = 0
If (XRef) Then
    chkXRef.Value = 1
End If
VType.Locked = True
If (VType.Text <> "D") And (TheType <> "V") Then
    lstAff.Visible = False
    lblAffiliations.Visible = False
    Label10.Visible = False
    txtTitle.Visible = False
    Label6.Visible = False
    txtUpin.Visible = False
    Label17.Visible = False
    Specialty.Visible = False
End If
Set ApplTbl = Nothing
If (VType.Text = "V") Then
    PermOn = UserLogin.HasPermission(epSetupVendors)
Else
    PermOn = UserLogin.HasPermission(epSetupReferringDoctors)
End If
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmVendors.BorderStyle = 1
    frmVendors.ClipControls = True
    frmVendors.Caption = Mid(frmVendors.Name, 4, Len(frmVendors.Name) - 3)
    frmVendors.AutoRedraw = True
    frmVendors.Refresh
End If
End Sub

Private Sub Specialty_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("Specialty")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Specialty.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub Specialty_KeyPress(KeyAscii As Integer)
Call Specialty_Click
End Sub

Private Sub Fax_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Fax, "P")
End If
End Sub

Private Sub Fax_Validate(Cancel As Boolean)
Call UpdateDisplay(Fax, "P")
End Sub

Private Sub Phone_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Phone, "P")
End If
End Sub

Private Sub Phone_Validate(Cancel As Boolean)
Call UpdateDisplay(Phone, "P")
End Sub

Private Sub lstAff_Click()
Dim ApplTbl As ApplicationTables
If (lstAff.ListIndex = 0) Then
    If (TheVendorId < 1) Then
        RealDone = False
        Call cmdApply_Click
        RealDone = True
    End If
    frmAffiliations.AffiliationId = 0
    frmAffiliations.ResourceId = TheVendorId
    frmAffiliations.ResourceType = "V"
    frmAffiliations.OverrideOn = False
    frmAffiliations.RemoveOn = False
    If (frmAffiliations.LoadAffiliationDisplay) Then
        frmAffiliations.Show 1
        If (frmAffiliations.AffiliationId <> 0) Then
            Set ApplTbl = New ApplicationTables
            Set ApplTbl.lstBox = lstAff
            Call ApplTbl.ApplLoadAffiliations(TheVendorId, "V")
            Set ApplTbl = Nothing
        End If
    End If
ElseIf (lstAff.ListIndex > 0) Then
    frmAffiliations.AffiliationId = val(Trim(Mid(lstAff.List(lstAff.ListIndex), 76, 5)))
    frmAffiliations.ResourceId = TheVendorId
    frmAffiliations.ResourceType = "V"
    frmAffiliations.RemoveOn = DeleteOn
    If (frmAffiliations.LoadAffiliationDisplay) Then
        frmAffiliations.Show 1
        If (frmAffiliations.AffiliationId <> 0) Then
            Set ApplTbl = New ApplicationTables
            Set ApplTbl.lstBox = lstAff
            Call ApplTbl.ApplLoadAffiliations(TheVendorId, "V")
            Set ApplTbl = Nothing
        End If
    End If
End If
End Sub

Private Sub txtNPI_KeyPress(KeyAscii As Integer)
Dim i As Integer
If (KeyAscii = 13) Then
    If (Len(txtNPI.Text) <> 10) And (Len(txtNPI.Text) <> 0) Then
        frmEventMsgs.Header = "Invalid NPI. Must be all numeric and 10 characters in length."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtNPI.Text = ""
        txtNPI.SetFocus
    Else
        For i = 1 To Len(txtNPI.Text)
            If Not (IsNumeric(Mid(txtNPI.Text, i, 1))) Then
                frmEventMsgs.Header = "Invalid NPI. Must be all numeric and 10 characters in length."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                txtNPI.Text = ""
                txtNPI.SetFocus
                Exit For
            End If
        Next i
    End If
End If
End Sub

Private Sub txtNPI_Validate(Cancel As Boolean)
Call txtNPI_KeyPress(13)
End Sub

Private Sub txtUpin_KeyPress(KeyAscii As Integer)
Dim ApplTemp As ApplicationTemplates
If (KeyAscii = 13) Then
    If (Len(txtUpin.Text) <> 6) And (Len(txtUpin.Text) <> 0) Then
        frmEventMsgs.Header = "Invalid Upin"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtUpin.Text = ""
        txtUpin.SetFocus
    ElseIf (Trim(txtUpin.Text) <> "") Then
        Set ApplTemp = New ApplicationTemplates
        If Not (ApplTemp.ApplIsUPinFormatValid(txtUpin.Text)) Then
            frmEventMsgs.Header = "Invalid Upin"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtUpin.Text = ""
            txtUpin.SetFocus
        End If
        Set ApplTemp = Nothing
    End If
End If
End Sub

Private Sub txtUpin_Validate(Cancel As Boolean)
Call txtUpin_KeyPress(13)
End Sub

Private Sub Zip_KeyPress(KeyAscii As Integer)
Dim ACity As String
Dim AState As String
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Call UpdateDisplay(Zip, "Z")
    If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
        Call GetCityStatebyZip(Zip.Text, ACity, AState)
        If (Trim(ACity) <> "") Then
            City.Text = ACity
        End If
        If (Trim(AState) <> "") Then
            State.Text = AState
        End If
    End If
End If
End Sub

Private Sub Zip_Validate(Cancel As Boolean)
Dim ACity As String
Dim AState As String
Call UpdateDisplay(Zip, "Z")
If CheckConfigCollection("ZIPLOOKUPON") = "T" Then
    Call GetCityStatebyZip(Zip.Text, ACity, AState)
    If (Trim(ACity) <> "") Then
        City.Text = ACity
    End If
    If (Trim(AState) <> "") Then
        State.Text = AState
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdBack_Click
Unload Me
End Sub
