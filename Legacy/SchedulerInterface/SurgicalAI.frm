VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{4BD5A3A1-7FFE-11D4-A13A-004005FA6275}#1.0#0"; "ImagXpr6.dll"
Begin VB.Form frmSurgicalAI 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstAllTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5580
      ItemData        =   "SurgicalAI.frx":0000
      Left            =   1920
      List            =   "SurgicalAI.frx":0002
      TabIndex        =   62
      Top             =   1320
      Visible         =   0   'False
      Width           =   7335
   End
   Begin VB.ComboBox lstIOLb 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3120
      Sorted          =   -1  'True
      TabIndex        =   57
      Top             =   3840
      Width           =   1575
   End
   Begin VB.ComboBox lstIOL 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3120
      Sorted          =   -1  'True
      TabIndex        =   56
      Top             =   3240
      Width           =   1575
   End
   Begin VB.TextBox txtIOLP 
      Height          =   375
      Left            =   4800
      TabIndex        =   5
      Top             =   3240
      Width           =   1215
   End
   Begin VB.TextBox txtArc 
      Height          =   375
      Left            =   5400
      MaxLength       =   3
      TabIndex        =   4
      Top             =   5400
      Width           =   615
   End
   Begin VB.TextBox txtAxis 
      Height          =   375
      Left            =   5400
      MaxLength       =   3
      TabIndex        =   3
      Top             =   4920
      Width           =   615
   End
   Begin VB.TextBox txtInc 
      Height          =   375
      Left            =   5400
      MaxLength       =   3
      TabIndex        =   2
      Top             =   4440
      Width           =   615
   End
   Begin VB.TextBox txtIOLbP 
      Height          =   375
      Left            =   4800
      TabIndex        =   6
      Top             =   3840
      Width           =   1215
   End
   Begin VB.TextBox txtPO 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6120
      MaxLength       =   128
      TabIndex        =   40
      Top             =   4200
      Width           =   2775
   End
   Begin VB.TextBox txtNotes 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   3840
      TabIndex        =   1
      Top             =   6000
      Width           =   7935
   End
   Begin VB.ListBox lstSurgReq 
      Height          =   645
      Left            =   4680
      TabIndex        =   32
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstMedClr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":0004
      Left            =   9000
      List            =   "SurgicalAI.frx":0006
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   28
      Top             =   3240
      Width           =   2775
   End
   Begin VB.ListBox lstORMedSup 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":0008
      Left            =   9000
      List            =   "SurgicalAI.frx":000A
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   30
      Top             =   4080
      Width           =   2775
   End
   Begin VB.ListBox lstTrans 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":000C
      Left            =   9000
      List            =   "SurgicalAI.frx":000E
      Sorted          =   -1  'True
      TabIndex        =   26
      Top             =   4920
      Width           =   2775
   End
   Begin VB.ListBox lstAType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":0010
      Left            =   6120
      List            =   "SurgicalAI.frx":0012
      Sorted          =   -1  'True
      TabIndex        =   24
      Top             =   3360
      Width           =   2775
   End
   Begin VB.ListBox lstResource2 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":0014
      Left            =   6120
      List            =   "SurgicalAI.frx":0016
      Sorted          =   -1  'True
      TabIndex        =   21
      Top             =   2520
      Width           =   2775
   End
   Begin VB.ListBox lstResource1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":0018
      Left            =   6120
      List            =   "SurgicalAI.frx":001A
      Sorted          =   -1  'True
      TabIndex        =   19
      Top             =   1680
      Width           =   2775
   End
   Begin VB.ListBox lstPreMeds 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "SurgicalAI.frx":001C
      Left            =   9000
      List            =   "SurgicalAI.frx":001E
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   17
      Top             =   840
      Width           =   2775
   End
   Begin VB.TextBox txtTemplate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      MaxLength       =   64
      TabIndex        =   10
      Top             =   600
      Width           =   4335
   End
   Begin VB.ListBox lstORIns 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      ItemData        =   "SurgicalAI.frx":0020
      Left            =   9000
      List            =   "SurgicalAI.frx":0022
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   9
      Top             =   2160
      Width           =   2775
   End
   Begin VB.ListBox lstResource0 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "SurgicalAI.frx":0024
      Left            =   6120
      List            =   "SurgicalAI.frx":0026
      Sorted          =   -1  'True
      TabIndex        =   8
      Top             =   840
      Width           =   2775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10560
      TabIndex        =   11
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":0028
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   0
      Top             =   7920
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":0207
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDocs 
      Height          =   990
      Left            =   7320
      TabIndex        =   12
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":03E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReview 
      Height          =   990
      Left            =   9000
      TabIndex        =   33
      Top             =   7920
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":05CA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSchedule 
      Height          =   990
      Left            =   2880
      TabIndex        =   35
      Top             =   7920
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":07B4
   End
   Begin IMAGXPR6LibCtl.ImagXpress FxImage1 
      Height          =   2895
      Left            =   120
      TabIndex        =   48
      Top             =   4920
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   5106
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   1145469816
      ErrInfo         =   -1347712044
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      AutoSize        =   4
      BorderType      =   2
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      Begin VB.Label lblDrawDate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         Height          =   255
         Left            =   0
         TabIndex        =   49
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   5880
      TabIndex        =   58
      Top             =   7920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":099F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextPic 
      Height          =   990
      Left            =   1440
      TabIndex        =   59
      Top             =   7920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":0B7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTemp 
      Height          =   990
      Left            =   4440
      TabIndex        =   60
      Top             =   7920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":0D64
   End
   Begin VB.ComboBox lstCT 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3120
      Sorted          =   -1  'True
      TabIndex        =   63
      Top             =   4440
      Width           =   1575
   End
   Begin VB.TextBox txtAim 
      Height          =   375
      Left            =   3840
      MaxLength       =   12
      TabIndex        =   7
      Top             =   5400
      Width           =   1095
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemove 
      Height          =   990
      Left            =   4920
      TabIndex        =   66
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgicalAI.frx":0F50
   End
   Begin VB.Label lblAim 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Objective"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3840
      TabIndex        =   65
      Top             =   5160
      Width           =   795
   End
   Begin VB.Label lblCT 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Case Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3120
      TabIndex        =   64
      Top             =   4200
      Width           =   930
   End
   Begin VB.Label lblLoc2 
      BackColor       =   &H00404000&
      Caption         =   "Loc:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6120
      TabIndex        =   61
      Top             =   5160
      Width           =   2805
   End
   Begin VB.Label lblIOL 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "IOL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3120
      TabIndex        =   55
      Top             =   3000
      Width           =   300
   End
   Begin VB.Label lblMRX 
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   945
      Left            =   120
      TabIndex        =   54
      Top             =   3000
      Width           =   2925
   End
   Begin VB.Label lblIOLM 
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   945
      Left            =   120
      TabIndex        =   53
      Top             =   1920
      Width           =   2925
   End
   Begin VB.Label lblAScan 
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   945
      Left            =   120
      TabIndex        =   52
      Top             =   840
      Width           =   2925
   End
   Begin VB.Label lblKero 
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   945
      Left            =   3120
      TabIndex        =   51
      Top             =   1920
      Width           =   2925
   End
   Begin VB.Label lblAKero 
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   945
      Left            =   3120
      TabIndex        =   50
      Top             =   840
      Width           =   2925
   End
   Begin VB.Label lblPower1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Power"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4800
      TabIndex        =   47
      Top             =   3000
      Width           =   540
   End
   Begin VB.Label lblInc 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Incisions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   5160
      TabIndex        =   46
      Top             =   4200
      Width           =   765
   End
   Begin VB.Label lblAxis 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Axis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4920
      TabIndex        =   45
      Top             =   4920
      Width           =   390
   End
   Begin VB.Label lblArc 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Arc"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   5040
      TabIndex        =   44
      Top             =   5400
      Width           =   300
   End
   Begin VB.Label lblLRI 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "LRI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4800
      TabIndex        =   43
      Top             =   4200
      Width           =   285
   End
   Begin VB.Label lblPower2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Power"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4800
      TabIndex        =   42
      Top             =   3600
      Width           =   540
   End
   Begin VB.Label lblPO 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient Orders"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6120
      TabIndex        =   41
      Top             =   3960
      Width           =   1260
   End
   Begin VB.Label lblTime 
      BackColor       =   &H00404000&
      Caption         =   "PostOp:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6120
      TabIndex        =   39
      Top             =   4800
      Width           =   2805
   End
   Begin VB.Label lblDate 
      BackColor       =   &H00404000&
      Caption         =   "Surgery:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   120
      TabIndex        =   38
      Top             =   4080
      Width           =   2925
   End
   Begin VB.Label lblPrec 
      BackColor       =   &H00404000&
      Caption         =   "PreCert:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   8040
      TabIndex        =   37
      Top             =   240
      Width           =   3765
   End
   Begin VB.Label lblLoc1 
      BackColor       =   &H00404000&
      Caption         =   "Loc:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   120
      TabIndex        =   36
      Top             =   4440
      Width           =   2925
   End
   Begin VB.Label lblNotes 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Notes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3840
      TabIndex        =   34
      Top             =   5760
      Width           =   510
   End
   Begin VB.Label lblORMedSup 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "OR Meds/Supplies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9000
      TabIndex        =   31
      Top             =   3840
      Width           =   1635
   End
   Begin VB.Label lblMedClr 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Medical Clearance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9000
      TabIndex        =   29
      Top             =   3000
      Width           =   1605
   End
   Begin VB.Label lblTrans 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Transportation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9000
      TabIndex        =   27
      Top             =   4680
      Width           =   1230
   End
   Begin VB.Label lblAType 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Anesthesia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6120
      TabIndex        =   25
      Top             =   3120
      Width           =   975
   End
   Begin VB.Label lblIOLb 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "IOL Backup"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3120
      TabIndex        =   23
      Top             =   3600
      Width           =   1020
   End
   Begin VB.Label lblResource2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Surgical Assistant"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6120
      TabIndex        =   22
      Top             =   2280
      Width           =   1590
   End
   Begin VB.Label lblResource1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Assisting Surgeon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6120
      TabIndex        =   20
      Top             =   1440
      Width           =   1590
   End
   Begin VB.Label lblPreMeds 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Pre-Op Meds"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9000
      TabIndex        =   18
      Top             =   600
      Width           =   1155
   End
   Begin VB.Label lblTemplate 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Surgical Request"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   16
      Top             =   360
      Width           =   1485
   End
   Begin VB.Label lblORIns 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "OR Instruments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9000
      TabIndex        =   15
      Top             =   1920
      Width           =   1365
   End
   Begin VB.Label lblResource0 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Supervising Surgeon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6120
      TabIndex        =   14
      Top             =   600
      Width           =   1770
   End
   Begin VB.Label lblHeader 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Setup Surgical Requests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   2160
   End
End
Attribute VB_Name = "frmSurgicalAI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ClinicalId As Long
Public PatientId As Long
Public AppointmentId As Long
Public TheTemplate As String
Public TheEye As String
Public TheLoc As String
Public TheDate As String
Public TheProcs As String
Public DocName As String
Public OrderDate As String

Private ReplaceOn As Boolean
Private AssignedSurgeryDate As String
Private AssignedPostOpDate As String
Private SchApptId As Long
Private DrawFiles(20) As String
Private DrawFilesCln(20) As Long
Private CurrentTestId As Integer
Private CurrentTestNum(5) As Integer
Private OtherTestsToInclude(5, 4) As Long
Private CurrentImageIdx As Integer

Private Sub cmdDone_Click()
If (AppointmentId < 1) And (PatientId < 1) Then
    Call SetupCodes
Else
    Call PostSurgical
End If
TheTemplate = ""
Unload frmSurgicalAI
End Sub

Private Sub cmdHome_Click()
If (Trim(lblDate.Tag) <> "") Or (Trim(lblTime.Tag) <> "") Then
    Call PostDatesOnly
End If
TheTemplate = ""
Unload frmSurgicalAI
End Sub

Private Sub cmdDocs_Click()
If (PatientId > 0) Then
    frmScan.PatientId = PatientId
    frmScan.AppointmentId = 0
    If (frmScan.LoadScan) Then
        frmScan.Show 1
    End If
End If
End Sub

Private Sub cmdNextPic_Click()
Call LoadNextImage(CurrentImageIdx)
End Sub

Private Sub cmdNotes_Click()
If (PatientId > 0) Then
    frmNotes.PatientId = PatientId
    frmNotes.NoteId = 0
    frmNotes.AppointmentId = 0
    frmNotes.SystemReference = ""
    frmNotes.CurrentAction = ""
    frmNotes.SetTo = "S"
    frmNotes.MaintainOn = True
    If (frmNotes.LoadNotes) Then
        frmNotes.Show 1
    End If
End If
End Sub

Private Sub cmdRemove_Click()
Dim ApplTbl As ApplicationTables
If (Trim(txtTemplate.Text) <> "") Then
    frmEventMsgs.Header = "Are you sure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        Set ApplTbl = New ApplicationTables
        If (ApplTbl.ApplIsCode("SurgicalRequests", Trim(txtTemplate.Text))) Then
            Call ApplTbl.ApplDeleteCode("SurgicalRequests", Trim(txtTemplate.Text), True)
        End If
        Set ApplTbl = Nothing
        Unload frmSurgicalAI
    End If
End If
End Sub

Private Sub cmdReview_Click()
If (frmReviewAppts.LoadApptsList(PatientId, AppointmentId, True)) Then
    frmReviewAppts.Show 1
End If
End Sub

Private Sub cmdSchedule_Click()

On Error GoTo lcmdSchedule_Click

Dim myApptId As Long
Dim SurgOn As Boolean
Dim ARef As String, ARem As String, AEye As String
Dim AResourceName As String
Dim RetAppt  As SchedulerAppointment
Dim RetApptType As SchedulerAppointmentType
Dim Temp As String, ACom As String, ARv As String
Dim TDate As String, NDate As String
Dim u As Integer, k As Integer, Ref As Integer
Dim NumDays As Integer
Dim LocalDate As ManagedDate
Dim RetRes As SchedulerResource
Dim RetCode As PracticeCodes
Dim ReturnArguments As Variant
Dim AppointmentIds
Dim ApptId As Variant
Dim RetrieveAppointment As SchedulerAppointment
Dim Filter As String

frmEventMsgs.Header = "Specify Date"
frmEventMsgs.AcceptText = "Surgery"
frmEventMsgs.RejectText = "PostOp"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    SurgOn = True
    myApptId = val(Trim(lblDate.Tag))
ElseIf (frmEventMsgs.Result = 2) Then
    SurgOn = False
    myApptId = val(Trim(lblTime.Tag))
Else
    Exit Sub
End If

ACom = ""
ARv = ""
Call FormatTodaysDate(TDate, False)
ARef = UCase(Trim(TheDate))
u = InStrPS(ARef, "<")
If (u > 0) Then
k = InStrPS(u + 1, ARef, "<")
If (k > 0) Then
    ARv = Mid(ARef, k + 1, (Len(ARef) - 1) - k)
    ARv = Mid(ARv, 2, Len(ARv) - 1)
    ACom = Mid(ARef, u + 1, (k - 1) - u)
Else
    ARv = ""
    ACom = Mid(ARef, u + 1, (Len(ARef) - 1) - u)
    If (Left(ACom, 1) = "^") Then
        ARv = Mid(ACom, 2, Len(ACom) - 1)
        ACom = ""
    End If
End If
ARef = Left(ARef, u - 1)
End If
Call ReplaceCharacters(ARef, "OD ", " ")
Call ReplaceCharacters(ARef, "OS ", " ")
Call ReplaceCharacters(ARef, "OU ", " ")
ARef = Trim(ARef)
u = InStrPS(ARef, " ")
If (u = 0) Then
Ref = 1
Else
If (u < 5) Then
    Ref = val(Trim(Left(ARef, u)))
End If
End If
If (InStrPS(ARef, "STAT") > 0) Then
NumDays = 0
ElseIf (InStrPS(ARef, "WEEK") > 0) Then
NumDays = 7
If (Ref > 52) Then
    Ref = 52
End If
ElseIf (InStrPS(ARef, "DAY") > 0) Then
NumDays = 1
ElseIf (InStrPS(ARef, "MONTH") > 0) Then
NumDays = 30
If (Ref > 24) Then
    Ref = 24
End If
ElseIf (InStrPS(ARef, "YEAR") > 0) Then
NumDays = 365
If (Ref > 20) Then
    Ref = 20
End If
Else
NumDays = 1
End If
NumDays = NumDays * Ref
Call AdjustDate(TDate, NumDays, NDate)
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = NDate
    
Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentId = AppointmentId
If (RetAppt.RetrieveSchedulerAppointment) Then
    Dim arguments() As Variant
    Dim AppointmentSearchWrapper As New comWrapper
    Call AppointmentSearchWrapper.Create(AppointmentSearchLoadArgumentsType, emptyArgs)
    
    Dim loadArguments As Object
    Set loadArguments = AppointmentSearchWrapper.Instance
    
    loadArguments.PatientId = PatientId
    
    'Get AppointmentTypeId
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "SURGERYTYPE"
    RetCode.ReferenceCode = TheProcs
    If RetCode.FindCode > 0 Then
        Set RetApptType = New SchedulerAppointmentType
        If RetCode.SelectCode(1) Then
            RetApptType.AppointmentType = RetCode.ReferenceAlternateCode
            If RetApptType.FindAppointmentType > 0 Then
                If RetApptType.SelectAppointmentType(1) Then
                   'Do nothing.  Previously was setting the appointment type, but this is no longer desired.
                   'Maintaining the above logic just in case...
                End If
            End If
        End If
    End If
        
    loadArguments.OrderAppointmentDate = CDate(OrderDate)
    
    'Get Location
    Temp = Mid(lblLoc1.Caption, 5)
    If InStrPS(1, Temp, "(") > 0 Then
        Temp = Mid(Temp, 1, InStrPS(1, Temp, "(") - 1)
        loadArguments.LocationId = GetServiceLocationId(Trim(Temp), False)
    ElseIf Temp = "" Then
        Temp = "OFFICE"
        loadArguments.LocationId = GetServiceLocationId(Trim(Temp), True)
    Else
        Temp = "OFFICE-" + Temp
        loadArguments.LocationId = GetServiceLocationId(Trim(Temp), True)
    End If
    
    If LocalDate.ConvertDisplayDateToManagedDate Then
            Temp = Mid(LocalDate.ExposedDate, 5, 2) + "/" + Mid(LocalDate.ExposedDate, 7, 2) + "/" + Left(LocalDate.ExposedDate, 4)
            loadArguments.StartDate = CDate(Temp)
    End If
    
    Set RetRes = New SchedulerResource
    RetRes.ResourceName = Trim(DocName)
    If (RetRes.FindResource > 0) Then
        If (RetRes.SelectResource(1)) Then
            loadArguments.ResourceId = RetRes.ResourceId
        End If
    End If
    loadArguments.ReasonForVisit = RetAppt.AppointmentVisitReason
    loadArguments.Comment = RetAppt.AppointmentComments
    arguments = Array(loadArguments)
    
    Call AppointmentSearchWrapper.Create(AppointmentSearchViewManagerType, emptyArgs)
    Set ReturnArguments = AppointmentSearchWrapper.InvokeMethod("ShowAppointmentSearch", arguments)
    If Not (SurgOn) Then
        AppointmentIds = ReturnArguments.AppointmentIds
        For Each ApptId In AppointmentIds
            Filter = Filter + CStr(ApptId) & ","
        Next
        If Filter <> "" Then
            If (Mid(Filter, Len(Filter), 1) = ",") Then
                Filter = Left(Filter, Len(Filter) - 1)
                myApptId = GetSurgeryAppointment(Filter)
            End If
        End If
    End If
    If myApptId > 0 Then
        Call GetAppointmentDetails(myApptId, SurgOn)
        Call VerifySurgeryAppointment(myApptId, SurgOn)
    End If
End If
Set RetCode = Nothing
Set RetRes = Nothing
Set RetAppt = Nothing
Set RetApptType = Nothing

Exit Sub

lcmdSchedule_Click:
    LogError "frmSurgicalAI", "cmdSchedule_Click", Err, Err.Description
End Sub

Public Function LoadSurgical(InitOn As Boolean, IName As String, iLoc As String, IDate As String, IProcs As String, AEye As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim PatName As String
Dim TheText As String
Dim TheOrder As String
Dim DisplayText As String
Dim ApplTest As ApplicationApptDetails
Dim ApplTbl As ApplicationTables
LoadSurgical = False
TheEye = AEye
TheLoc = iLoc
TheDate = IDate
TheProcs = IProcs
CurrentImageIdx = 1
cmdReview.Visible = False
cmdSchedule.Visible = False
lblPrec.Visible = False
lblDate.Visible = False
lblTime.Visible = False
lblLoc1.Visible = False
lblLoc2.Visible = False
lblNotes.Visible = False
txtNotes.Visible = False
lblPO.Visible = False
txtPO.Visible = False
lblAim.Visible = False
txtAim.Visible = False
lblHeader.Caption = "Setup Surgical Requests"
lblTemplate.Visible = False
txtTemplate.Visible = False
cmdRemove.Visible = False
lblCT.Visible = False
lstCT.Visible = False
If (InitOn) Then
    lblDate.Tag = ""
    lblTime.Tag = ""
End If
If (AppointmentId > 0) And (PatientId > 0) Then
    Set ApplTbl = New ApplicationTables
    Call ApplTbl.ApplGetPatientName(PatientId, PatName)
    Set ApplTbl = Nothing
    If (AEye <> "") Then
        lblHeader.Caption = "Setup " + Trim(IProcs) + " in the " + AEye + " for " + Trim(PatName)
    Else
        lblHeader.Caption = "Setup " + Trim(IProcs) + " for " + Trim(PatName)
    End If
    lblTemplate.Visible = False
    txtTemplate.Visible = False
    cmdRemove.Visible = False
    cmdReview.Visible = True
    cmdSchedule.Visible = True
    lblLoc1.Visible = True
    lblLoc1.Caption = "Loc: " + iLoc
    lblLoc2.Visible = True
    lblLoc2.Caption = "Loc: "
    lblDate.Visible = True
    lblDate.Caption = "Surgery: " + IDate
    lblTime.Visible = True
    lblPrec.Visible = True
    lblPO.Visible = True
    txtPO.Visible = True
    lblAim.Visible = True
    txtAim.Visible = True
    lblCT.Visible = True
    lstCT.Visible = True
    lblNotes.Visible = True
    txtNotes.Visible = True
End If
TheTemplate = IName
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstResource0
Call ApplTbl.ApplLoadCodes("SurgicalResources", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstResource1
Call ApplTbl.ApplLoadCodes("SurgicalResources", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstResource2
Call ApplTbl.ApplLoadCodes("SurgicalResources", False, True, True)
Set ApplTbl = Nothing

Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBoxA = lstIOL
Call ApplTbl.ApplLoadCodes("SurgicalIOL", False, True, False)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBoxA = lstIOLb
Call ApplTbl.ApplLoadCodes("SurgicalIOL", False, True, False)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBoxA = lstCT
Call ApplTbl.ApplLoadCodes("SurgeryCaseType", False, True, False)
Set ApplTbl = Nothing

Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstPreMeds
Call ApplTbl.ApplLoadCodes("SurgicalPreOpMeds", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstMedClr
Call ApplTbl.ApplLoadCodes("SurgicalMedicalClearance", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstORIns
Call ApplTbl.ApplLoadCodes("SurgicalInstrumentation", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstORMedSup
Call ApplTbl.ApplLoadCodes("SurgicalMedsAndSupplies", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstTrans
Call ApplTbl.ApplLoadCodes("SurgicalTransportation", False, True, True)
Set ApplTbl = Nothing
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstAType
Call ApplTbl.ApplLoadCodes("SurgicalAnesthesiaType", False, True, True)
Set ApplTbl = Nothing

' Load Tests
'32A AScan A SCAN BIOMETRY
'12A AKero, AUTOKERATOMETRY
'91B IOLM, IOL MASTER
'11A Kero, KERATOMETRY
'13A MRX, MANIFEST REFRACTION
If (AppointmentId > 0) And (PatientId > 0) Then
    Set ApplTest = New ApplicationApptDetails
    Call ApplTest.BuildTestResults(PatientId, AppointmentId, 1, False, False, False)
    i = 1
    While (ApplTest.AccessTestResults(i, TheText, False, False))
        Call ApplTest.AccessTestOrder(i, TheOrder)
        If (TheOrder = "32A") Then
            lblAScan.Caption = TheText
        ElseIf (TheOrder = "12A") Then
            lblAKero.Caption = TheText
        ElseIf (TheOrder = "11A") Then
            lblKero.Caption = TheText
        ElseIf (TheOrder = "13A") Then
            lblMRX.Caption = TheText
        ElseIf (TheOrder = "91B") Then
            lblIOLM.Caption = TheText
        End If
        i = i + 1
    Wend
    Set ApplTest = Nothing
' Load Draw
    Call SetDrawing(AppointmentId)
End If

If (Trim(IName) <> "") And (AppointmentId < 1) And (PatientId < 1) Then
    lstResource0.ListIndex = -1
    lstResource1.ListIndex = -1
    lstResource2.ListIndex = -1
    lstPreMeds.ListIndex = -1
    lstORIns.ListIndex = -1
    lstTrans.ListIndex = -1
    lstORMedSup.ListIndex = -1
    lstMedClr.ListIndex = -1
    lstCT.ListIndex = -1
    lstIOL.ListIndex = -1
    lstIOLb.ListIndex = -1
    txtIOLP.Text = ""
    txtIOLbP.Text = ""
    txtInc.Text = ""
    txtAxis.Text = ""
    txtArc.Text = ""
    LoadSurgical = LoadList(IName, ReplaceOn)
    lblAScan.Visible = False
    lblAKero.Visible = False
    lblKero.Visible = False
    lblMRX.Visible = False
    lblIOLM.Visible = False
    lblCT.Visible = True
    lstCT.Visible = True
    FxImage1.Visible = False
    cmdDocs.Visible = False
    cmdNotes.Visible = False
    cmdTemp.Visible = False
    cmdSchedule.Visible = False
    cmdNextPic.Visible = False
    cmdReview.Visible = False
    lblTemplate.Caption = "Surgical Template"
    lblTemplate.Visible = True
    txtTemplate.Text = IName
    txtTemplate.Locked = True
    txtTemplate.Visible = True
    cmdRemove.Visible = True
ElseIf ((AppointmentId > 0) And (PatientId > 0)) Then
    LoadSurgical = True
    If (ClinicalId > 0) Then
        Call SetupSurgical(ClinicalId)
        If (Trim(IName) <> "") Then
            LoadSurgical = LoadList(IName, ReplaceOn)
        End If
    End If
    lblAType.Visible = True
    lstAType.Visible = True
    lblResource0.Visible = True
    lstResource0.Visible = True
    lblResource1.Visible = True
    lstResource1.Visible = True
    lblResource2.Visible = True
    lstResource2.Visible = True
    lblPreMeds.Visible = True
    lstPreMeds.Visible = True
    lblORIns.Visible = True
    lstORIns.Visible = True
    lblTrans.Visible = True
    lstTrans.Visible = True
    lblORMedSup.Visible = True
    lstORMedSup.Visible = True
    lblMedClr.Visible = True
    lstMedClr.Visible = True
    lblCT.Visible = True
    lstCT.Visible = True
    lblIOL.Visible = True
    lstIOL.Visible = True
    lblIOLb.Visible = True
    lstIOLb.Visible = True
    lblPower1.Visible = True
    txtIOLP.Visible = True
    lblPower2.Visible = True
    txtIOLbP.Visible = True
    lblLRI.Visible = True
    lblInc.Visible = True
    txtInc.Visible = True
    lblAxis.Visible = True
    txtAxis.Visible = True
    lblArc.Visible = True
    txtArc.Visible = True
    txtTemplate.Text = IName
    txtTemplate.Locked = True
Else
    lblAType.Visible = False
    lstAType.Visible = False
    lblResource0.Visible = False
    lstResource0.Visible = False
    lblResource1.Visible = False
    lstResource1.Visible = False
    lblResource2.Visible = False
    lstResource2.Visible = False
    lblPreMeds.Visible = False
    lstPreMeds.Visible = False
    lblORIns.Visible = False
    lstORIns.Visible = False
    lblTrans.Visible = False
    lstTrans.Visible = False
    lblORMedSup.Visible = False
    lstORMedSup.Visible = False
    lblMedClr.Visible = False
    lstMedClr.Visible = False
    lblCT.Visible = False
    lstCT.Visible = False
    lblIOL.Visible = False
    lstIOL.Visible = False
    lblIOLb.Visible = False
    lstIOLb.Visible = False
    lblPower1.Visible = False
    txtIOLP.Visible = False
    lblPower2.Visible = False
    txtIOLbP.Visible = False
    lblLRI.Visible = False
    lblInc.Visible = False
    txtInc.Visible = False
    lblAxis.Visible = False
    txtAxis.Visible = False
    lblArc.Visible = False
    txtArc.Visible = False
    lblAim.Visible = False
    txtAim.Visible = False
    lblAScan.Visible = False
    lblAKero.Visible = False
    lblKero.Visible = False
    lblMRX.Visible = False
    lblIOLM.Visible = False
    lblTemplate.Caption = "Surgical Template (Hit <Enter> to Create)"
    lblTemplate.Visible = True
    txtTemplate.Text = ""
    txtTemplate.Locked = False
    txtTemplate.Visible = True
    cmdRemove.Visible = True
    FxImage1.Visible = False
    cmdDocs.Visible = False
    cmdNotes.Visible = False
    cmdTemp.Visible = False
    cmdSchedule.Visible = False
    cmdNextPic.Visible = False
    cmdReview.Visible = False
    LoadSurgical = True
End If
End Function

Private Sub cmdTemp_Click()
Dim TheReq As String
Call frmSelectDialogue.BuildSelectionDialogue("SurgicalRequests")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    TheReq = Trim(Left(frmSelectDialogue.Selection, 50))
    If (Trim(TheReq) <> "") Then
        ReplaceOn = False
        Call PostSurgical
        Call LoadSurgical(False, TheReq, TheLoc, TheDate, TheProcs, TheEye)
    End If
End If
End Sub

Private Sub Form_Load()
ReplaceOn = True
Erase CurrentTestNum
Erase OtherTestsToInclude
If UserLogin.HasPermission(epPowerUser) Then
    frmSurgicalAI.BorderStyle = 1
    frmSurgicalAI.ClipControls = True
    frmSurgicalAI.Caption = Mid(frmSurgicalAI.Name, 4, Len(frmSurgicalAI.Name) - 3)
    frmSurgicalAI.AutoRedraw = True
    frmSurgicalAI.Refresh
End If
Call LoadSurgical(False, TheTemplate, TheLoc, TheDate, TheProcs, TheEye)
End Sub

Private Sub FxImage1_Click()
Dim ClnId As Long
ClnId = val(Trim(FxImage1.Tag))
If (ClnId > 0) Then
    If (frmViewDraw.LoadDraw(ClnId, "")) Then
        frmViewDraw.Show 1
    End If
End If
End Sub

Private Sub lblAKero_Click()
CurrentTestId = 1
Call SetupHistoricalTests("12A")
End Sub

Private Sub lblAScan_Click()
CurrentTestId = 2
Call SetupHistoricalTests("32A")
End Sub

Private Sub lblIOLM_Click()
CurrentTestId = 3
Call SetupHistoricalTests("91B")
End Sub

Private Sub lblKero_Click()
CurrentTestId = 4
Call SetupHistoricalTests("11A")
End Sub

Private Sub lblMRX_Click()
CurrentTestId = 5
Call SetupHistoricalTests("13A")
End Sub

Private Sub lstAllTests_Click()
Dim i As Integer
If (lstAllTests.ListIndex > 0) Then
    If (CurrentTestNum(CurrentTestId) + 1 > 4) Then
        frmEventMsgs.Header = "You may retain up to 4 past tests ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Clear All Past Test"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            For i = 1 To 4
                OtherTestsToInclude(CurrentTestId, CurrentTestNum(CurrentTestId)) = 0
            Next i
            CurrentTestNum(CurrentTestId) = 0
        End If
    Else
        For i = 1 To 4
            If (OtherTestsToInclude(CurrentTestId, i) = lstAllTests.ItemData(lstAllTests.ListIndex)) Then
                Exit Sub
            End If
        Next i
        CurrentTestNum(CurrentTestId) = CurrentTestNum(CurrentTestId) + 1
        OtherTestsToInclude(CurrentTestId, CurrentTestNum(CurrentTestId)) = lstAllTests.ItemData(lstAllTests.ListIndex)
    End If
ElseIf (lstAllTests.ListIndex = 0) Then
    lstAllTests.Clear
    lstAllTests.Visible = False
End If
End Sub

Private Sub lstCT_Click()
If (lstCT.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub lstIOL_Click()
If (lstIOL.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub lstIOLb_Click()
If (lstIOLb.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub txtTemplate_KeyPress(KeyAscii As Integer)
Dim Temp As String
Dim ApplTbl As ApplicationTables
If (KeyAscii = 13) Or (KeyAscii = 10) Then
    Temp = "SurgicalRequests"
    Set ApplTbl = New ApplicationTables
    If (ApplTbl.ApplIsCode(Temp, Trim(txtTemplate.Text))) Then
        frmEventMsgs.Header = "Surgical Request Already exists"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtTemplate.Text = ""
        txtTemplate.SetFocus
    Else
        Set ApplTbl = New ApplicationTables
        Call ApplTbl.ApplPostCode("SurgicalRequests", Trim(txtTemplate.Text), "", "", True, "", "", "", "", "", "", "", 0)
        Set ApplTbl = Nothing
        Call LoadSurgical(False, Trim(txtTemplate.Text), "", "", "", "")
    End If
End If
End Sub

Private Function SetupCodes() As Boolean
Dim i As Integer
Dim j As Integer
Dim NId As Long
Dim Ref As String
Dim Ref1 As String
Dim Ref2 As String
Dim ApplTbl As ApplicationTables
Set ApplTbl = New ApplicationTables
' Purge existing items
For i = 0 To lstSurgReq.ListCount - 1
    Ref1 = Trim(Left(lstSurgReq.List(i), 64))
    Call ApplTbl.ApplDeleteCode(Trim(TheTemplate), Ref1, False)
Next i
' Post new Stuff
Ref = UCase(Trim(txtTemplate.Text))
If (lstResource0.ListIndex >= 0) Then
    Ref1 = Left(lstResource0.List(lstResource0.ListIndex), 62) + "1S"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (lstResource1.ListIndex >= 0) Then
    Ref1 = Left(lstResource1.List(lstResource1.ListIndex), 62) + "2S"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (lstResource2.ListIndex >= 0) Then
    Ref1 = Left(lstResource2.List(lstResource2.ListIndex), 62) + "3S"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (lstCT.ListIndex >= 0) Then
    Ref1 = Left(lstCT.List(lstCT.ListIndex), 62) + "CT"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
For j = 0 To lstPreMeds.ListCount - 1
    If (lstPreMeds.Selected(j)) Then
        Ref1 = Left(lstPreMeds.List(j), 62) + "PM"
        Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
    End If
Next j
For j = 0 To lstMedClr.ListCount - 1
    If (lstMedClr.Selected(j)) Then
        Ref1 = Left(lstMedClr.List(j), 62) + "MC"
        Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
    End If
Next j
If (lstIOL.ListIndex >= 0) Then
    Ref1 = Left(lstIOL.List(lstIOL.ListIndex), 62) + "IO"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (Trim(txtIOLP.Text) <> "") Then
    Ref1 = Trim(txtIOLP.Text)
    Ref1 = Ref1 + Space(62 - Len(Ref1)) + "P1"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (lstIOLb.ListIndex >= 0) Then
    Ref1 = Left(lstIOLb.List(lstIOLb.ListIndex), 62) + "IB"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (Trim(txtIOLbP.Text) <> "") Then
    Ref1 = Trim(txtIOLbP.Text)
    Ref1 = Ref1 + Space(62 - Len(Ref1)) + "P2"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
For j = 0 To lstORIns.ListCount - 1
    If (lstORIns.Selected(j)) Then
        Ref1 = Left(lstORIns.List(j), 62) + "OI"
        Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
    End If
Next j
For j = 0 To lstORMedSup.ListCount - 1
    If (lstORMedSup.Selected(j)) Then
        Ref1 = Left(lstORMedSup.List(j), 62) + "OM"
        Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
    End If
Next j
For j = 0 To lstTrans.ListCount - 1
    If (lstTrans.Selected(j)) Then
        Ref1 = Left(lstTrans.List(j), 62) + "TR"
        Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
    End If
Next j
For j = 0 To lstAType.ListCount - 1
    If (lstAType.Selected(j)) Then
        Ref1 = Left(lstAType.List(j), 62) + "AT"
        Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
    End If
Next j
If (Trim(txtInc.Text) <> "") Then
    Ref1 = Trim(txtInc.Text)
    Ref1 = Ref1 + Space(62 - Len(Ref1)) + "IN"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (Trim(txtAxis.Text) <> "") Then
    Ref1 = Trim(txtAxis.Text)
    Ref1 = Ref1 + Space(62 - Len(Ref1)) + "AX"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
If (Trim(txtArc.Text) <> "") Then
    Ref1 = Trim(txtArc.Text)
    Ref1 = Ref1 + Space(62 - Len(Ref1)) + "AC"
    Call ApplTbl.ApplPostCode(Ref, Ref1, "", "", True, "", "", "", "", "", "", "", 0)
End If
Set ApplTbl = Nothing
'Call LoadSurgical(UCase(Trim(txtTemplate.Text)), "", "", "", "")
End Function

Private Function LoadList(TheName As String, MyReplace As Boolean) As Boolean
Dim i As Integer, j As Integer
Dim DisplayText As String
Dim Temp As String, BoxId As String
Dim ByPassP As Boolean
Dim ByPassU As Boolean
Dim ByPassT As Boolean
Dim ByPassA As Boolean
Dim ByPassO As Boolean
Dim ByPassM As Boolean
Dim ApplTbl As ApplicationTables
LoadList = False
ByPassP = False
ByPassU = False
ByPassT = False
ByPassA = False
ByPassO = False
ByPassM = False
Set ApplTbl = New ApplicationTables
lstSurgReq.Clear
Set ApplTbl.lstBox = lstSurgReq
LoadList = ApplTbl.ApplLoadCodes(Trim(TheName), False, True, True)
For i = 0 To lstSurgReq.ListCount - 1
    BoxId = Mid(lstSurgReq.List(i), 63, 2)
    If (BoxId = "1S") Then
        If (lstResource0.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstResource0.ListCount - 1
                If (Trim(Left(lstResource0.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstResource0.ListIndex = j
                    lstResource0.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "2S") Then
        If (lstResource1.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstResource1.ListCount - 1
                If (Trim(Left(lstResource1.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstResource1.ListIndex = j
                    lstResource1.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "3S") Then
        If (lstResource2.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstResource2.ListCount - 1
                If (Trim(Left(lstResource2.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstResource2.ListIndex = j
                    lstResource2.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "PM") Then
        If (lstPreMeds.SelCount < 1) Or (MyReplace) Or (ByPassP) Then
            ByPassP = True
            For j = 0 To lstPreMeds.ListCount - 1
                If (Trim(Left(lstPreMeds.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstPreMeds.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "MC") Then
        If (lstMedClr.SelCount < 1) Or (MyReplace) Or (ByPassM) Then
            ByPassM = True
            For j = 0 To lstMedClr.ListCount - 1
                If (Trim(Left(lstMedClr.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstMedClr.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "IO") Then
        If (lstIOL.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstIOL.ListCount - 1
                If (Trim(Left(lstIOL.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstIOL.ListIndex = j
                End If
            Next j
        End If
    ElseIf (BoxId = "IB") Then
        If (lstIOLb.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstIOLb.ListCount - 1
                If (Trim(Left(lstIOLb.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstIOLb.ListIndex = j
                End If
            Next j
        End If
    ElseIf (BoxId = "OI") Then
        If (lstORIns.SelCount < 1) Or (MyReplace) Or (ByPassO) Then
            ByPassO = True
            For j = 0 To lstORIns.ListCount - 1
                If (Trim(Left(lstORIns.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstORIns.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "OM") Then
        If (lstORMedSup.SelCount < 1) Or (MyReplace) Or (ByPassU) Then
            ByPassU = True
            For j = 0 To lstORMedSup.ListCount - 1
                If (Trim(Left(lstORMedSup.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstORMedSup.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "TR") Then
        If (lstTrans.SelCount < 1) Or (MyReplace) Or (ByPassT) Then
            ByPassT = True
            For j = 0 To lstTrans.ListCount - 1
                If (Trim(Left(lstTrans.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstTrans.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "CT") Then
        If (lstCT.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstCT.ListCount - 1
                If (Trim(Left(lstCT.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstCT.ListIndex = j
                End If
            Next j
        End If
    ElseIf (BoxId = "AT") Then
        If (lstAType.SelCount < 1) Or (MyReplace) Or (ByPassA) Then
            ByPassA = True
            For j = 0 To lstAType.ListCount - 1
                If (Trim(Left(lstAType.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstAType.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "P1") Then
        If (Trim(txtIOLP.Text) = "") Or (MyReplace) Then
            txtIOLP.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "P2") Then
        If (Trim(txtIOLbP.Text) = "") Or (MyReplace) Then
            txtIOLbP.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "IN") Then
        If (Trim(txtInc.Text) = "") Or (MyReplace) Then
            txtInc.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "AX") Then
        If (Trim(txtAxis.Text) = "") Or (MyReplace) Then
            txtAxis.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "AC") Then
        If (Trim(txtArc.Text) = "") Or (MyReplace) Then
            txtArc.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    End If
Next i
Set ApplTbl = Nothing
LoadList = True
End Function

Private Function GetDoctor() As String
Dim i As Integer
Dim j As Integer
GetDoctor = ""
For i = 0 To lstResource0.ListCount - 1
    If (lstResource0.Selected(i)) Then
        j = InStrPS(lstResource0.List(i), ",")
        If (j > 0) Then
            GetDoctor = UCase(Trim(Left(lstResource0.List(i), j - 1)))
        End If
        Exit For
    End If
Next i
End Function

Private Function GetExpectedDate(TheRange As String) As String
Dim i As Integer
Dim DayAhead As Integer
Dim ADate As String
Dim NDate As String
Dim RetAppt As SchedulerAppointment
ADate = ""
NDate = ""
GetExpectedDate = ""
If (AppointmentId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = AppointmentId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
    End If
    Set RetAppt = Nothing
    If (ADate <> "") Then
        DayAhead = 0
        i = InStrPS(UCase(TheRange), "DAY")
        If (i > 0) Then
            DayAhead = val(Trim(Left(TheRange, i - 1)))
        End If
        i = InStrPS(UCase(TheRange), "WEEK")
        If (i > 0) Then
            DayAhead = val(Trim(Left(TheRange, i - 1))) * 7
        End If
        i = InStrPS(UCase(TheRange), "MONTH")
        If (i > 0) Then
            DayAhead = val(Trim(Left(TheRange, i - 1))) * 31
        End If
        i = InStrPS(UCase(TheRange), "YEAR")
        If (i > 0) Then
            DayAhead = val(Trim(Left(TheRange, i - 1))) * 365
        End If
        If (DayAhead > 0) Then
            Call AdjustDate(ADate, DayAhead, NDate)
            If (Trim(NDate) <> "") Then
                NDate = Mid(NDate, 7, 4) + Mid(NDate, 1, 2) + Mid(NDate, 4, 2)
            End If
            GetExpectedDate = NDate
        Else
            GetExpectedDate = ""
        End If
    End If
End If
End Function

Private Function GetAppointmentDetails(TheId As Long, AWhich As Boolean) As Boolean
Dim RscId As Long
Dim PrecId As Long
Dim ADate As String
Dim ATime As String
Dim RetAppt As SchedulerAppointment
Dim RetRes As SchedulerResource
Dim RetPrc As PracticeName
Dim RetPre As PatientPreCerts
GetAppointmentDetails = False
If (TheId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = TheId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
        Call ConvertMinutesToTime(RetAppt.AppointmentTime, ATime)
        RscId = RetAppt.AppointmentResourceId2
        PrecId = RetAppt.AppointmentPreCertId
    End If
    Set RetAppt = Nothing
    If (AWhich) Then
        lblDate.Caption = "Surgery: " + ADate + " " + ATime
        lblDate.Tag = Trim(Str(TheId))
    Else
        lblTime.Caption = "PostOp: " + ADate + " " + ATime
        lblTime.Tag = Trim(Str(TheId))
    End If
    If (RscId > 0) And (RscId < 1000) Then
        Set RetRes = New SchedulerResource
        RetRes.ResourceId = RscId
        If (RetRes.RetrieveSchedulerResource) Then
            If (RetRes.ResourceType = "R") Then
                If (AWhich) Then
                    lblLoc1.Caption = "Loc:" + RetRes.ResourceDescription
                Else
                    lblLoc2.Caption = "Loc:" + RetRes.ResourceDescription
                End If
            End If
        End If
        Set RetRes = Nothing
    ElseIf (RscId > 1000) Then
        Set RetPrc = New PracticeName
        RetPrc.PracticeId = RscId - 1000
        If (RetPrc.RetrievePracticeName) Then
            If (AWhich) Then
                lblLoc1.Caption = "Loc:" + RetPrc.PracticeLocationReference
            Else
                lblLoc2.Caption = "Loc:" + RetPrc.PracticeLocationReference
            End If
        End If
        Set RetPrc = Nothing
    Else
        If (AWhich) Then
            lblLoc1.Caption = "Loc:Office"
        Else
            lblLoc2.Caption = "Loc:Office"
        End If
    End If
    If (PrecId > 0) Then
        Set RetPre = New PatientPreCerts
        RetPre.PreCertsId = PrecId
        If (RetPre.RetrievePatientPreCerts) Then
            lblPrec.Caption = "Prec: " + Trim(RetPre.PreCerts)
        End If
        Set RetPre = Nothing
    End If
End If
End Function

Private Function SetDrawing(ApptId As Long) As Boolean
Dim m As Integer
Dim i As Integer, u As Integer
Dim Temp As String, ADate As String
Dim ADiag As String, ADraw As String
Dim MyFile As String, TheFile As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
On Error GoTo UI_ErrorHandler
SetDrawing = False
Erase DrawFiles
Erase DrawFilesCln
FxImage1.Tag = ""
Set FxImage1.Picture = Nothing
lblDrawDate.Visible = False
lblDrawDate.Caption = ""
If (ApptId > 0) Then
    u = 0
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = RetAppt.AppointmentDate
        ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Mid(ADate, 1, 4)
    End If
    Set RetAppt = Nothing
    Set RetCln = New PatientClinical
    RetCln.AppointmentId = ApptId
    RetCln.ClinicalType = "Q"
    RetCln.ClinicalStatus = "A"
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        Do Until Not (RetCln.SelectPatientClinical(i))
            ADiag = Trim(RetCln.Findings)
            ADraw = Trim(RetCln.ClinicalDraw)
            If (ADraw <> "") Then
                Temp = ADraw
                If (Left(Temp, 2) = "R-") Then
                    MyFile = MyImageDir + Mid(Temp, 3, Len(Temp) - 2)
                Else
                    MyFile = MyImageDir + "MyDraw-A" + Trim(Str(ApptId)) + "*" + Temp + ".jpg"
                End If
                TheFile = Dir(MyFile)
                If (Trim(TheFile) <> "") Then
                    MyFile = MyImageDir + TheFile
                Else
                    MyFile = MyImageDir + "MyDraw-A" + Trim(Str(ApptId)) + "*" + Temp + ".bmp"
                    TheFile = Dir(MyFile)
                    If (Trim(TheFile) <> "") Then
                        MyFile = MyImageDir + TheFile
                    Else
                        MyFile = ""
                    End If
                End If
                If (Trim(MyFile) <> "") Then
                    For m = 1 To u
                        If (DrawFiles(m) = MyFile) Then
                            m = -1
                            Exit For
                        End If
                    Next m
                    If (m <> -1) Then
                        u = u + 1
                        DrawFiles(u) = MyFile
                        DrawFilesCln(u) = RetCln.ClinicalId
                    End If
                End If
            End If
            i = i + 1
        Loop
        If (Trim(DrawFiles(1)) <> "") Then
            CurrentImageIdx = 1
            MyFile = DrawFiles(CurrentImageIdx)
            If (InStrPS(MyFile, ".bmp") = 0) Then
                FxImage1.FileName = FM.GetPathForDirectIO(MyFile)
            Else
                FxImage1.Picture = FM.LoadPicture(MyFile)
            End If
            FxImage1.Tag = Trim(Str(DrawFilesCln(CurrentImageIdx)))
            lblDrawDate.Caption = ADate
            lblDrawDate.Visible = True
        End If
    End If
    Set RetCln = Nothing
End If
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub LoadNextImage(MyIdx As Integer)
Dim MyFile As String
CurrentImageIdx = CurrentImageIdx + 1
If (Trim(DrawFiles(CurrentImageIdx)) = "") Then
    CurrentImageIdx = 1
End If
If (Trim(DrawFiles(CurrentImageIdx)) <> "") Then
    MyFile = DrawFiles(CurrentImageIdx)
    If (InStrPS(MyFile, ".bmp") = 0) Then
        FxImage1.FileName = FM.GetPathForDirectIO(MyFile)
    Else
        FxImage1.Picture = FM.LoadPicture(MyFile)
    End If
    FxImage1.Tag = Trim(Str(DrawFilesCln(CurrentImageIdx)))
End If
End Sub

Private Function PostSurgical() As Boolean
Dim i As Integer
Dim z As Integer
Dim NoteId As Long
Dim RType As String
Dim RValue As String
Dim RetNote As PatientNotes
Dim RetSCln As PatientClinicalSurgeryPlan
PostSurgical = True
GoSub CleanOldRecords
If (lstResource0.ListIndex >= 0) Then
    RType = "S"
    RValue = Trim(Left(lstResource0.List(lstResource0.ListIndex), 62))
    GoSub PostRecord
End If
If (lstResource1.ListIndex >= 0) Then
    RType = "A"
    RValue = Trim(Left(lstResource1.List(lstResource1.ListIndex), 62))
    GoSub PostRecord
End If
If (lstResource2.ListIndex >= 0) Then
    RType = "O"
    RValue = Trim(Left(lstResource2.List(lstResource2.ListIndex), 62))
    GoSub PostRecord
End If
If (lstIOL.ListIndex >= 0) And (lstIOL.Text <> "") Then
    RType = "C"
    RValue = Trim(Left(lstIOL.List(lstIOL.ListIndex), 62))
    RValue = RValue + "Power:" + Trim(txtIOLP.Text)
    GoSub PostRecord
End If
If (lstIOLb.ListIndex >= 0) And (lstIOLb.Text <> "") Then
    RType = "B"
    RValue = Trim(Left(lstIOLb.List(lstIOLb.ListIndex), 62))
    RValue = RValue + "Power:" + Trim(txtIOLbP.Text)
    GoSub PostRecord
End If
If (lstCT.ListIndex >= 0) Then
    RType = "Q"
    RValue = Trim(Left(lstCT.List(lstCT.ListIndex), 62))
    GoSub PostRecord
End If
If (lstAType.ListIndex >= 0) Then
    RType = "N"
    RValue = Trim(Left(lstAType.List(lstAType.ListIndex), 62))
    GoSub PostRecord
End If
If (lstTrans.ListIndex >= 0) Then
    RType = "T"
    RValue = Trim(Left(lstTrans.List(lstTrans.ListIndex), 62))
    GoSub PostRecord
End If
For i = 0 To lstPreMeds.ListCount - 1
    If (lstPreMeds.Selected(i)) Then
        RType = "M"
        RValue = Trim(Left(lstPreMeds.List(i), 62))
        GoSub PostRecord
    End If
Next i
For i = 0 To lstORIns.ListCount - 1
    If (lstORIns.Selected(i)) Then
        RType = "I"
        RValue = Trim(Left(lstORIns.List(i), 62))
        GoSub PostRecord
    End If
Next i
For i = 0 To lstMedClr.ListCount - 1
    If (lstMedClr.Selected(i)) Then
        RType = "Y"
        RValue = Trim(Left(lstMedClr.List(i), 62))
        GoSub PostRecord
    End If
Next i
For i = 0 To lstORMedSup.ListCount - 1
    If (lstORMedSup.Selected(i)) Then
        RType = "Z"
        RValue = Trim(Left(lstORMedSup.List(i), 62))
        GoSub PostRecord
    End If
Next i
If (Trim(txtPO.Text) <> "") Then
    RType = "P"
    RValue = Trim(txtPO.Text)
    GoSub PostRecord
End If
If (Trim(txtAim.Text) <> "") Then
    RType = "E"
    RValue = Trim(txtAim.Text)
    GoSub PostRecord
End If
RValue = ""
If (Trim(txtInc.Text) <> "") Then
    RValue = "Inc:" + Trim(txtInc.Text) + " "
End If
If (Trim(txtAxis.Text) <> "") Then
    RValue = RValue + "Axis:" + Trim(txtAxis.Text) + " "
End If
If (Trim(txtArc.Text) <> "") Then
    RValue = RValue + "Arc:" + Trim(txtArc.Text)
End If
If (Trim(RValue) <> "") Then
    RType = "D"
    GoSub PostRecord
End If
If (Trim(lblDate.Tag) <> "") Then
    RType = "F"
    RValue = Trim(lblDate.Tag)
    GoSub PostRecord
End If
If (Trim(lblTime.Tag) <> "") Then
    RType = "G"
    RValue = Trim(lblTime.Tag)
    GoSub PostRecord
End If
' Post older Tests that are to be included as part of the SR
For i = 1 To 5
    For z = 1 To 4
        If (OtherTestsToInclude(i, z) > 0) Then
            RType = Trim(Str(i))
            RValue = Trim(Str(OtherTestsToInclude(i, z)))
            GoSub PostRecord
        End If
    Next z
Next i
If (Trim(txtNotes.Text) <> "") Then
    If (Trim(txtNotes.Tag) <> "") Then
        If (val(Trim(txtNotes.Tag)) > 0) Then
            Set RetNote = New PatientNotes
            RetNote.NotesId = val(Trim(txtNotes.Tag))
            If (RetNote.RetrieveNotes) Then
                Call RetNote.DeleteNotes
            End If
            Set RetNote = Nothing
        End If
    End If
    Set RetNote = New PatientNotes
    RetNote.NotesId = 0
    If (RetNote.RetrieveNotes) Then
        RetNote.NotesAppointmentId = AppointmentId
        RetNote.NotesPatientId = PatientId
        RetNote.NotesCategory = ""
        RetNote.NotesAudioOn = False
        RetNote.NotesCommentOn = False
        RetNote.NotesClaimOn = False
        RetNote.NotesDate = ""
        RetNote.NotesEye = TheEye
        RetNote.NotesHighlight = ""
        RetNote.NotesILPNRef = ""
        RetNote.NotesSystem = ""
        RetNote.NotesOffDate = ""
        RetNote.NotesUser = UserLogin.iId
        RetNote.NotesType = "S"
        RetNote.NotesText1 = Left(txtNotes.Text, 250)
        If (Len(txtNotes.Text) > 250) Then
            RetNote.NotesText2 = Mid(txtNotes.Text, 251, 250)
            If (Len(txtNotes.Text) > 511) Then
                RetNote.NotesText3 = Mid(txtNotes.Text, 511, 250)
                If (Len(txtNotes.Text) > 761) Then
                    RetNote.NotesText4 = Mid(txtNotes.Text, 761, 250)
                End If
            End If
        End If
        Call RetNote.ApplyNotes
    End If
    Set RetNote = Nothing
End If
Exit Function
CleanOldRecords:
    If (ClinicalId > 0) Then
        Set RetSCln = New PatientClinicalSurgeryPlan
        RetSCln.SurgeryClnId = ClinicalId
        RetSCln.SurgeryRefType = ""
        If (RetSCln.FindPatientClinicalPlan > 0) Then
            z = 1
            While (RetSCln.SelectPatientClinicalPlan(z))
                If Not RetSCln.SurgeryRefType = "F" Then
                    Call RetSCln.KillPatientClinicalPlan
                End If
                z = z + 1
            Wend
        End If
        Set RetSCln = Nothing
    End If
    Return
PostRecord:
    If (Trim(RType) <> "") And (Trim(RValue) <> "") Then
        Set RetSCln = New PatientClinicalSurgeryPlan
        RetSCln.SurgeryId = 0
        If (RetSCln.RetrievePatientClinicalPlan) Then
            RetSCln.SurgeryClnId = ClinicalId
            RetSCln.SurgeryRefType = RType
            RetSCln.SurgeryValue = RValue
            RetSCln.SurgeryStatus = "A"
            Call RetSCln.ApplyPatientClinicalPlan
        End If
        Set RetSCln = Nothing
    End If
    Return
End Function

Private Function PostDatesOnly() As Boolean
Dim RType As String
Dim RValue As String
Dim RetSCln As PatientClinicalSurgeryPlan
PostDatesOnly = True
If (Trim(lblDate.Tag) <> "") Then
    RType = "F"
    RValue = Trim(lblDate.Tag)
    GoSub PostRecord
End If
If (Trim(lblTime.Tag) <> "") Then
    RType = "G"
    RValue = Trim(lblTime.Tag)
    GoSub PostRecord
End If
Exit Function
PostRecord:
    If (Trim(RType) <> "") And (Trim(RValue) <> "") Then
        Set RetSCln = New PatientClinicalSurgeryPlan
        RetSCln.SurgeryClnId = ClinicalId
        RetSCln.SurgeryRefType = RType
        RetSCln.SurgeryStatus = "A"
        If (RetSCln.FindPatientClinicalPlan > 0) Then
            If (RetSCln.SelectPatientClinicalPlan(1)) Then
                RetSCln.SurgeryValue = RValue
                Call RetSCln.ApplyPatientClinicalPlan
            End If
        End If
        Set RetSCln = Nothing
    End If
    Return
End Function

Private Function SetupSurgical(ClnId As Long) As Boolean
Dim i As Integer, z As Integer
Dim p As Integer, q As Integer
Dim RValue As String
Dim RetNote As PatientNotes
Dim RetSCln As PatientClinicalSurgeryPlan
SetupSurgical = True
If (ClinicalId > 0) Then
    Set RetSCln = New PatientClinicalSurgeryPlan
    RetSCln.SurgeryClnId = ClinicalId
    RetSCln.SurgeryRefType = ""
    RetSCln.SurgeryStatus = "A"
    If (RetSCln.FindPatientClinicalPlan > 0) Then
        z = 1
        While (RetSCln.SelectPatientClinicalPlan(z))
            If (RetSCln.SurgeryRefType = "S") Then
                For i = 0 To lstResource0.ListCount - 1
                    If (UCase(Trim(Left(lstResource0.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
'                        lstResource0.ListIndex = i
                        lstResource0.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "A") Then
                For i = 0 To lstResource1.ListCount - 1
                    If (UCase(Trim(Left(lstResource1.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
'                        lstResource1.ListIndex = i
                        lstResource1.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "O") Then
                For i = 0 To lstResource2.ListCount - 1
                    If (UCase(Trim(Left(lstResource2.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
'                        lstResource2.ListIndex = i
                        lstResource2.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "Q") Then
                For i = 0 To lstCT.ListCount - 1
                    If (UCase(Trim(Left(lstCT.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstCT.ListIndex = i
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "C") Then
                p = InStrPS(RetSCln.SurgeryValue, "POWER:")
                If (p > 0) Then
                    RValue = Trim(Left(RetSCln.SurgeryValue, p - 1))
                    For i = 0 To lstIOL.ListCount - 1
                        If (UCase(Trim(Left(lstIOL.List(i), 62))) = Trim(RValue)) Then
                            lstIOL.ListIndex = i
                            Exit For
                        End If
                    Next i
                    RValue = Trim(Mid(RetSCln.SurgeryValue, p + 6, Len(RetSCln.SurgeryValue) - (p + 5)))
                    txtIOLP.Text = RValue
                Else
                    For i = 0 To lstIOL.ListCount - 1
                        If (UCase(Trim(Left(lstIOL.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                            lstIOL.ListIndex = i
                            Exit For
                        End If
                    Next i
                    txtIOLP.Text = ""
                End If
            ElseIf (RetSCln.SurgeryRefType = "B") Then
                p = InStrPS(RetSCln.SurgeryValue, "POWER:")
                If (p > 0) Then
                    RValue = Trim(Left(RetSCln.SurgeryValue, p - 1))
                    For i = 0 To lstIOLb.ListCount - 1
                        If (UCase(Trim(Left(lstIOLb.List(i), 62))) = Trim(RValue)) Then
                            lstIOLb.ListIndex = i
                            Exit For
                        End If
                    Next i
                    RValue = Trim(Mid(RetSCln.SurgeryValue, p + 6, Len(RetSCln.SurgeryValue) - (p + 5)))
                    txtIOLbP.Text = RValue
                Else
                    For i = 0 To lstIOLb.ListCount - 1
                        If (UCase(Trim(Left(lstIOLb.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                            lstIOLb.ListIndex = i
                            Exit For
                        End If
                    Next i
                    txtIOLbP.Text = ""
                End If
            ElseIf (RetSCln.SurgeryRefType = "N") Then
                For i = 0 To lstAType.ListCount - 1
                    If (UCase(Trim(Left(lstAType.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstAType.ListIndex = i
                        lstAType.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "T") Then
                For i = 0 To lstTrans.ListCount - 1
                    If (UCase(Trim(Left(lstTrans.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstTrans.ListIndex = i
                        lstTrans.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "M") Then
                For i = 0 To lstPreMeds.ListCount - 1
                    If (UCase(Trim(Left(lstPreMeds.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstPreMeds.ListIndex = i
                        lstPreMeds.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "I") Then
                For i = 0 To lstORIns.ListCount - 1
                    If (UCase(Trim(Left(lstORIns.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstORIns.ListIndex = i
                        lstORIns.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "Y") Then
                For i = 0 To lstMedClr.ListCount - 1
                    If (UCase(Trim(Left(lstMedClr.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstMedClr.ListIndex = i
                        lstMedClr.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "Z") Then
                For i = 0 To lstORMedSup.ListCount - 1
                    If (UCase(Trim(Left(lstORMedSup.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstORMedSup.ListIndex = i
                        lstORMedSup.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "P") Then
                txtPO.Text = Trim(RetSCln.SurgeryValue)
            ElseIf (RetSCln.SurgeryRefType = "F") Then
                Call GetAppointmentDetails(val(Trim(RetSCln.SurgeryValue)), True)
            ElseIf (RetSCln.SurgeryRefType = "G") Then
                Call GetAppointmentDetails(val(Trim(RetSCln.SurgeryValue)), False)
            ElseIf (RetSCln.SurgeryRefType = "E") Then
                txtAim.Text = Trim(RetSCln.SurgeryValue)
            ElseIf (RetSCln.SurgeryRefType = "D") Then
                p = InStrPS(RetSCln.SurgeryValue, "AXIS:")
                If (p > 0) Then
                    q = InStrPS(p, RetSCln.SurgeryValue, "ARC:")
                    If (q = 0) Then
                        q = Len(RetSCln.SurgeryValue)
                    End If
                    txtAxis.Text = Trim(Mid(RetSCln.SurgeryValue, p + 5, (q - 1) - (p + 4)))
                End If
                p = InStrPS(RetSCln.SurgeryValue, "INC:")
                If (p > 0) Then
                    q = InStrPS(p, RetSCln.SurgeryValue, "AXIS:")
                    If (q = 0) Then
                        q = InStrPS(p, RetSCln.SurgeryValue, "ARC:")
                        If (q = 0) Then
                            q = Len(RetSCln.SurgeryValue)
                        End If
                    End If
                    txtInc.Text = Trim(Mid(RetSCln.SurgeryValue, p + 4, (q - 1) - (p + 3)))
                End If
                p = InStrPS(RetSCln.SurgeryValue, "ARC:")
                If (p > 0) Then
                    q = Len(RetSCln.SurgeryValue) + 1
                    txtArc.Text = Trim(Mid(RetSCln.SurgeryValue, p + 4, (q - 1) - (p + 3)))
                End If
            ElseIf (RetSCln.SurgeryRefType = "1") Then
                p = 1
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "2") Then
                p = 2
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "3") Then
                p = 3
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "4") Then
                p = 4
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "5") Then
                p = 5
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = val(Trim(RetSCln.SurgeryValue))
                End If
            End If
            z = z + 1
        Wend
    End If
    Set RetSCln = Nothing
' Get The Surgery Notes
    txtNotes.Text = ""
    txtNotes.Tag = ""
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatientId
    RetNote.NotesAppointmentId = AppointmentId
    RetNote.NotesType = "S"
    If (RetNote.FindNotes > 0) Then
        If (RetNote.SelectNotes(1)) Then
            txtNotes.Text = Trim(txtNotes.Text) _
                          + " " + Trim(RetNote.NotesText1) _
                          + " " + Trim(RetNote.NotesText2) _
                          + " " + Trim(RetNote.NotesText3) _
                          + " " + Trim(RetNote.NotesText4)
            txtNotes.Tag = Trim(Str(RetNote.NotesId))
        End If
    End If
    Set RetNote = Nothing
End If
End Function

Private Function SetupHistoricalTests(TheOrder As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim ADate As String
Dim PDate As String
Dim Temp As String
Dim TestResult As String
Dim RetAppt As SchedulerAppointment
Dim RetDet As ApplicationApptDetails
SetupHistoricalTests = False
lstAllTests.Clear
lstAllTests.Tag = TheOrder
lstAllTests.AddItem "Close Test List"
CurrentTestNum(CurrentTestId) = 0
For i = 1 To 4
    OtherTestsToInclude(CurrentTestId, i) = 0
Next i
Set RetDet = New ApplicationApptDetails
Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentPatientId = PatientId
RetAppt.AppointmentStatus = "D"
If (RetAppt.FindAppointment > 0) Then
    i = 1
    While (RetAppt.SelectAppointment(i))
        PDate = RetAppt.AppointmentDate
        ADate = Mid(PDate, 5, 2) + "/" + Mid(PDate, 7, 2) + "/" + Left(PDate, 4)
        TestResult = RetDet.BuildSpecificTestResults(PatientId, RetAppt.AppointmentId, TheOrder, False)
        If (Trim(TestResult) <> "") Then
            j = InStrPS(TestResult, vbCrLf)
            Temp = ADate + " " + Left(TestResult, j - 1)
            lstAllTests.AddItem Temp
            lstAllTests.ItemData(lstAllTests.NewIndex) = RetAppt.AppointmentId
            TestResult = Mid(TestResult, j + 2, Len(TestResult) - (j + 1))
            j = InStrPS(TestResult, vbCrLf)
            While (j > 0)
                Temp = Space(11) + Trim(Left(TestResult, j - 1))
                lstAllTests.AddItem Temp
                lstAllTests.ItemData(lstAllTests.NewIndex) = RetAppt.AppointmentId
                TestResult = Mid(TestResult, j + 2, Len(TestResult) - (j + 1))
                j = InStrPS(TestResult, vbCrLf)
            Wend
            Temp = Space(11) + Trim(TestResult)
            lstAllTests.AddItem Temp
            lstAllTests.ItemData(lstAllTests.NewIndex) = RetAppt.AppointmentId
        End If
        i = i + 1
    Wend
    If (lstAllTests.ListCount > 0) Then
        lstAllTests.Visible = True
    End If
End If
Set RetDet = Nothing
Set RetAppt = Nothing
SetupHistoricalTests = True
End Function

Private Function VerifySurgeryAppointment(ApptId As Long, AWhich As Boolean) As Boolean
Dim z As Integer
Dim RetSCln As PatientClinicalSurgeryPlan
Dim RetAppt As SchedulerAppointment
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        If (InStrPS("PRA", RetAppt.AppointmentStatus) = 0) Then
            If (AWhich) Then
                lblDate.Caption = "Surgery: "
                lblDate.Tag = ""
            Else
                lblTime.Caption = "PostOp: "
                lblTime.Tag = ""
            End If
' kill the Surgery or PostOp appt
            Set RetSCln = New PatientClinicalSurgeryPlan
            RetSCln.SurgeryClnId = ClinicalId
            RetSCln.SurgeryValue = Trim(Str(ApptId))
            RetSCln.SurgeryRefType = "F"
            If (RetSCln.FindPatientClinicalPlan > 0) Then
                z = 1
                While (RetSCln.SelectPatientClinicalPlan(z))
                    Call RetSCln.KillPatientClinicalPlan
                    z = z + 1
                Wend
            End If
            Set RetSCln = Nothing
        End If
    End If
    Set RetAppt = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

Private Function GetServiceLocationId(ByVal LocationName As String, ByVal IsShortName)
GetServiceLocationId = -1
Dim SQL As String
Dim RS As Recordset
If IsShortName Then
    SQL = "Select TOP 1 Id From Model.ServiceLocations Where ShortName= '" & LocationName & "' "
Else
    SQL = "Select TOP 1 Id From Model.ServiceLocations Where Name= '" & LocationName & "' "
End If
Set RS = GetRS(SQL)
If RS.RecordCount > 0 Then
   GetServiceLocationId = RS("Id")
End If
End Function
Private Function GetLocName(LocId As Long, LocName As String) As Boolean
Dim RetLoc As SchedulerResource
Dim RetPrc As PracticeName
GetLocName = False
If (LocId > 0) And (LocId < 1000) Then
    Set RetLoc = New SchedulerResource
    RetLoc.ResourceId = LocId
    If (RetLoc.RetrieveSchedulerResource) Then
        GetLocName = True
        LocName = Trim(RetLoc.ResourceName)
    End If
    Set RetLoc = Nothing
ElseIf (LocId > 1000) Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeId = LocId - 1000
    If (RetPrc.RetrievePracticeName) Then
        GetLocName = True
        LocName = "OFFICE-" + Trim(RetPrc.PracticeLocationReference)
    End If
    Set RetPrc = Nothing
Else
    GetLocName = True
    LocName = "OFFICE"
End If
End Function
Private Function GetSurgeryAppointment(ByVal Filter As String)
Dim sqlStr As String
Dim RS As Recordset

GetSurgeryAppointment = 0

sqlStr = " SELECT TOP 1 ap.Id as Id FROM model.Appointments ap " & _
        " INNER JOIN model.AppointmentTypes apt ON apt.id=ap.AppointmentTypeId " & _
        " INNER JOIN model.Encounters e ON e.id=ap.EncounterId " & _
        " WHERE apt.Name LIKE 'POST%' AND e.EncounterStatusId NOT BETWEEN 8 AND 14 AND ap.Id IN(" & Filter & ") AND e.PatientId=" & PatientId

Set RS = GetRS(sqlStr)

If (RS.RecordCount > 0) Then
     GetSurgeryAppointment = RS("Id")
End If

End Function
