VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmReviewAppts 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstZoom 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5010
      ItemData        =   "ReviewAppts.frx":0000
      Left            =   1560
      List            =   "ReviewAppts.frx":0002
      TabIndex        =   26
      Top             =   2040
      Visible         =   0   'False
      Width           =   9015
   End
   Begin VB.ListBox lstMeds 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1110
      ItemData        =   "ReviewAppts.frx":0004
      Left            =   3720
      List            =   "ReviewAppts.frx":000B
      TabIndex        =   23
      Top             =   3480
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.ListBox lstPrint 
      Height          =   840
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   21
      Top             =   120
      Visible         =   0   'False
      Width           =   735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   855
      Left            =   5400
      TabIndex        =   1
      Top             =   8280
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":0018
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   10560
      TabIndex        =   2
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":01FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   855
      Left            =   9360
      TabIndex        =   8
      Top             =   7680
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":03DD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChart 
      Height          =   855
      Left            =   4440
      TabIndex        =   11
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":05BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   855
      Left            =   5760
      TabIndex        =   12
      Top             =   7680
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":07A2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   855
      Left            =   360
      TabIndex        =   13
      Top             =   7680
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":0985
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   855
      Left            =   3120
      TabIndex        =   24
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":0B64
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   855
      Left            =   1800
      TabIndex        =   25
      Top             =   7680
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":0D43
   End
   Begin VB.ListBox lstImpr 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1980
      ItemData        =   "ReviewAppts.frx":0F22
      Left            =   6720
      List            =   "ReviewAppts.frx":0F29
      TabIndex        =   20
      Top             =   3480
      Visible         =   0   'False
      Width           =   5175
   End
   Begin VB.ListBox lstOD 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1020
      ItemData        =   "ReviewAppts.frx":0F36
      Left            =   360
      List            =   "ReviewAppts.frx":0F3D
      TabIndex        =   4
      Top             =   5640
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.ListBox lstTests 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2370
      ItemData        =   "ReviewAppts.frx":0F48
      Left            =   6720
      List            =   "ReviewAppts.frx":0F4F
      TabIndex        =   5
      Top             =   1080
      Visible         =   0   'False
      Width           =   5175
   End
   Begin VB.ListBox lstOS 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1020
      ItemData        =   "ReviewAppts.frx":0F5D
      Left            =   360
      List            =   "ReviewAppts.frx":0F64
      TabIndex        =   9
      Top             =   6600
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.ListBox lstHPI 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      IntegralHeight  =   0   'False
      ItemData        =   "ReviewAppts.frx":0F6F
      Left            =   360
      List            =   "ReviewAppts.frx":0F76
      TabIndex        =   14
      Top             =   1080
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.ListBox lstALG 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1110
      ItemData        =   "ReviewAppts.frx":0F82
      Left            =   3720
      List            =   "ReviewAppts.frx":0F89
      TabIndex        =   15
      Top             =   4560
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.ListBox lstMHX 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1110
      ItemData        =   "ReviewAppts.frx":0F95
      Left            =   360
      List            =   "ReviewAppts.frx":0F9C
      TabIndex        =   16
      Top             =   3480
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ListBox lstFMX 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1110
      ItemData        =   "ReviewAppts.frx":0FA8
      Left            =   360
      List            =   "ReviewAppts.frx":0FAF
      TabIndex        =   17
      Top             =   4560
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ListBox lstOHX 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1125
      IntegralHeight  =   0   'False
      ItemData        =   "ReviewAppts.frx":0FBB
      Left            =   360
      List            =   "ReviewAppts.frx":0FC2
      TabIndex        =   18
      Top             =   2400
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.ListBox lstCC 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   765
      IntegralHeight  =   0   'False
      ItemData        =   "ReviewAppts.frx":0FCE
      Left            =   360
      List            =   "ReviewAppts.frx":0FD5
      TabIndex        =   19
      Top             =   1680
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.ListBox lstEval 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2220
      ItemData        =   "ReviewAppts.frx":0FE0
      Left            =   6720
      List            =   "ReviewAppts.frx":0FE7
      TabIndex        =   10
      Top             =   5400
      Visible         =   0   'False
      Width           =   5175
   End
   Begin VB.ListBox lstAppts 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6540
      ItemData        =   "ReviewAppts.frx":0FF4
      Left            =   360
      List            =   "ReviewAppts.frx":0FF6
      TabIndex        =   0
      Top             =   1080
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOrder 
      Height          =   855
      Left            =   6960
      TabIndex        =   28
      Top             =   7680
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":0FF8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdERx 
      Height          =   855
      Left            =   8160
      TabIndex        =   29
      Top             =   7680
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewAppts.frx":11DC
   End
   Begin VB.Label lblAlert 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      Caption         =   "Alert ON"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9720
      TabIndex        =   27
      Top             =   720
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label lblPrint 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Printing In Progress"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9720
      TabIndex        =   22
      Top             =   480
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Loading Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   9720
      TabIndex        =   7
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "ReferralId/ReferFromDoctor/ReferToDoctor/ReferWhy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Top             =   600
      Width           =   9375
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Review Appointments for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   120
      Width           =   9375
   End
End
Attribute VB_Name = "frmReviewAppts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SetApptOn As Boolean
Public SetApptId As Long
Public StartDate As String
Private FirstTimeIn As Boolean
Private DirectAccess As Boolean
Private DirectAccessApptId As Long
Private ItemSelected As Integer
Private TriggerItem As String
Private PatientId As Long
Private AppointmentId As Long
Private OrgCaption As String
Private ApplDet As ApplicationApptDetails

Private Sub cmdDone_Click()
If (lstAppts.Visible) Or (DirectAccess) Then
    If DirectAccess Then
        Call InsertLog("FrmReviewAppts.frm", "Closed Patient Chart", LoginCatg.EncounterClose, "", "", 0, AppointmentId)
    End If
    Set ApplDet = Nothing
    Unload frmReviewAppts
Else
    Label2.Visible = False
    Label3.Visible = False
    cmdNotes.Visible = False
    cmdPrint.Visible = False
    cmdChart.Visible = False
    cmdRx.Visible = False
    cmdOrder.Visible = False
    cmdERx.Visible = False
    cmdNext.Visible = False
    cmdPrev.Visible = False
    lstImpr.Visible = False
    lstOD.Visible = False
    lstOS.Visible = False
    lstCC.Visible = False
    lstALG.Visible = False
    lstMeds.Visible = False
    lstOHX.Visible = False
    lstHPI.Visible = False
    lstFMX.Visible = False
    lstMHX.Visible = False
    lstTests.Visible = False
    lstEval.Visible = False
    lstAppts.Visible = True
    Label1.Caption = OrgCaption
    Call InsertLog("FrmReviewAppts.frm", "Closed Patient Chart", LoginCatg.EncounterClose, "", "", 0, AppointmentId)
End If
End Sub

Private Sub cmdERx_Click()
Dim clsERX As New CeRx
    If UserLogin.HasPermission(EPermissions.epeRx) Then
        With clsERX
            .strPatientID = PatientId
            .strAppointmentId = AppointmentId
            .ComposeeRx
        End With
    Else
        frmEventMsgs.InfoMessage "Not permissioned for eRx"
        frmEventMsgs.Show 1
    End If
    Set clsERX = Nothing
End Sub

Private Sub cmdHome_Click()
If lstAppts.Visible = False Then
    Call InsertLog("FrmReviewAppts.frm", "Closed Patient Chart", LoginCatg.EncounterClose, "", "", 0, AppointmentId)
End If
Unload frmReviewAppts
End Sub

Private Sub cmdNext_Click()
If (DirectAccess) Then
    If (lstAppts.ListIndex < 1) Then
        If (lstAppts.ListIndex = 0) Then
            lstAppts.ListIndex = lstAppts.ListCount - 1
        Else
            lstAppts.ListIndex = 0
        End If
    ElseIf (lstAppts.ListIndex = 0) Then
        lstAppts.ListIndex = -1
        Call LoadApptDisplay(DirectAccessApptId, PatientId, 0, "")
    Else
        lstAppts.ListIndex = lstAppts.ListIndex - 1
    End If
ElseIf (lstAppts.ListIndex < 1) Then
        If (lstAppts.ListIndex = 0) Then
            lstAppts.ListIndex = lstAppts.ListCount - 1
        Else
            lstAppts.ListIndex = 0
        End If
ElseIf (lstAppts.ListIndex <= lstAppts.ListCount - 1) Then
    lstAppts.ListIndex = lstAppts.ListIndex - 1
End If
End Sub

Private Sub cmdPrev_Click()
If (DirectAccess) Then
    If (lstAppts.ListIndex < 1) Then
        If (lstAppts.ListIndex = 0) Then
            lstAppts.ListIndex = lstAppts.ListCount - 1
        Else
            lstAppts.ListIndex = 0
        End If
    ElseIf (lstAppts.ListIndex >= lstAppts.ListCount - 1) Then
        lstAppts.ListIndex = 0
'        Call LoadApptDisplay(DirectAccessApptId, PatientId, 0, "")
    Else
        lstAppts.ListIndex = lstAppts.ListIndex + 1
    End If
ElseIf (lstAppts.ListIndex < 1) Then
    If (lstAppts.ListIndex = 0) Then
        lstAppts.ListIndex = lstAppts.ListCount - 1
    Else
        lstAppts.ListIndex = 0
    End If
ElseIf (lstAppts.ListIndex >= lstAppts.ListCount - 1) Then
    lstAppts.ListIndex = 0
Else
    lstAppts.ListIndex = lstAppts.ListIndex + 1
End If
End Sub

Private Sub cmdOrder_Click()
Dim u As Integer
Dim MyIdx As Integer
If (lstEval.ListIndex >= 0) Then
    If CheckConfigCollection("INVENTORYON") = "T" Then
        If (UCase(Left(lstEval.List(lstEval.ListIndex), 13)) = "DISPENSE SPEC") Then
            frmPCOrder.OrderId = 0
            frmPCOrder.PatientId = PatientId
            frmPCOrder.AppointmentId = AppointmentId
            frmPCOrder.ClinicalId = 0
            frmPCOrder.Show 1
        ElseIf (UCase(Left(lstEval.List(lstEval.ListIndex), 11)) = "DISPENSE CL") Then
            MyIdx = 0
            For u = lstEval.ListIndex + 1 To lstEval.ListCount - 1
                If (Left(lstEval.ListIndex, 5) <> "     ") Then
                    MyIdx = u - 2
                    Exit For
                End If
            Next u
            'If (MyIdx = 0) Then
            '    MyIdx = lstEval.ListCount - 3
            'Else
                MyIdx = MyIdx + 7
            'End If
            If ((Trim(lstEval.List(MyIdx)) <> "-") And (Trim(lstEval.List(MyIdx)) <> "")) Or _
               ((Trim(lstEval.List(MyIdx + 1)) <> "-") And (Trim(lstEval.List(MyIdx)) <> "")) Then
                If (val(Trim(lstEval.ItemData(MyIdx))) > 0) Or (val(Trim(lstEval.ItemData(MyIdx + 1))) > 0) Then
                    frmCLOrder.PatientId = PatientId
                    frmCLOrder.AppointmentId = AppointmentId
                    frmCLOrder.OrderIdOD = 0
                    frmCLOrder.OrderIdOS = 0
                    frmCLOrder.InventoryIdOD = Trim(lstEval.ItemData(MyIdx))
                    frmCLOrder.InventoryIdOS = Trim(lstEval.ItemData(MyIdx + 1))
                    frmCLOrder.ReorderOn = True
                    frmCLOrder.ClnId = myTrim(lstEval.Text, 1)
                    frmCLOrder.Show 1
                    frmCLOrder.ReorderOn = False
                Else
                    frmEventMsgs.Header = "No CL selected. Please re-prescribe."
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
            End If
        End If
    Else
        frmEventMsgs.Header = "Select a CL/Spectacle RX Item"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
lstEval.ListIndex = -1
End Sub

Private Sub cmdNotes_Click()
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.CurrentAction = ""
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Sub cmdPrint_Click()
lblPrint.Visible = True
Label3.Caption = "Printing Clinical"
Label3.Visible = True
DoEvents
Set ApplDet.lstCC = lstCC
Set ApplDet.lstALG = lstALG
Set ApplDet.lstMeds = lstMeds
Set ApplDet.lstOHX = lstOHX
Set ApplDet.lstFMX = lstFMX
Set ApplDet.lstMHX = lstMHX
Set ApplDet.lstHPI = lstHPI
'Set ApplDet.lstBox = lstPrint
Set ApplDet.lstPrint = lstPrint
Set ApplDet.lstOD = lstOD
Set ApplDet.lstOS = lstOS
Set ApplDet.lstEval = lstEval
Set ApplDet.lstImpr = lstImpr
Set ApplDet.lstTests = lstTests
Call ApplDet.PrintDIAppointment(AppointmentId)
Label3.Caption = "Load Details"
Label3.Visible = False
lblPrint.Visible = False
DoEvents
End Sub

Private Sub cmdChart_Click()
Dim DrId As Long
Dim DrName As String
Dim ApplTemp As ApplicationTemplates
If (AppointmentId > 0) Then
    Call frmSelectDialogue.BuildSelectionDialogue("QUEUETO")
    frmSelectDialogue.Show 1
    If (Trim(frmSelectDialogue.Selection) <> "") Then
        DrId = val(Mid(frmSelectDialogue.Selection, 56, 5))
        DrName = Trim(Left(frmSelectDialogue.Selection, 55))
        If (DrId > 0) Then
            Set ApplTemp = New ApplicationTemplates
            Call ApplTemp.ApplQueueChart(AppointmentId, DrId, DrName)
            Set ApplTemp = Nothing
            frmEventMsgs.Header = "Chart Queued"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
End If
End Sub

Private Sub cmdRx_Click()
Dim ApplTemp As ApplicationTemplates
If (lstEval.ListIndex >= 0) Then
    If (UCase(Left(lstEval.List(lstEval.ListIndex), 2)) = "RX") Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBoxA = lstEval
        Set ApplTemp.lstBox = lstPrint
        lblPrint.Visible = True
        Call ApplTemp.ApplApptDrugPrint(lstEval.ListIndex, PatientId, AppointmentId)
        Call InsertLog("frmReviewAppts.frm", "Printed  Medication for Patient " & PatientId & " ", LoginCatg.ExamElementsInsert, "", "", 0, AppointmentId)
        lblPrint.Visible = False
        Set ApplTemp = Nothing
    ElseIf (UCase(Left(lstEval.List(lstEval.ListIndex), 8)) = "DISPENSE") Then
        Set ApplTemp = New ApplicationTemplates
        Set ApplTemp.lstBoxA = lstEval
        Set ApplTemp.lstBox = lstPrint
        lblPrint.Visible = True
        
        
        If (InStrPS(UCase(lstEval.List(lstEval.ListIndex)), "SPECTACLE") <> 0) Then
            Call ApplTemp.ApplApptEyeWearPrint(lstEval.ListIndex, PatientId, AppointmentId, False, myTrim(lstEval.List(lstEval.ListIndex), 1))
        Else
            Call ApplTemp.ApplApptEyeWearPrint(lstEval.ListIndex, PatientId, AppointmentId, True, myTrim(lstEval.List(lstEval.ListIndex), 1))
        End If
        lblPrint.Visible = False
        Set ApplTemp = Nothing
    Else
        frmEventMsgs.Header = "Select an RX Item"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    frmEventMsgs.Header = "Select an RX Item"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
lstEval.ListIndex = -1
End Sub

Private Sub Form_Activate()
If (lblAlert.Visible) Then
    If (FirstTimeIn) Then
        Call lblAlert_Click
        FirstTimeIn = False
    End If
End If
End Sub

Private Sub Form_Load()
FirstTimeIn = True
If UserLogin.HasPermission(epPowerUser) Then
    frmReviewAppts.BorderStyle = 1
    frmReviewAppts.ClipControls = True
    frmReviewAppts.Caption = Mid(frmReviewAppts.Name, 4, Len(frmReviewAppts.Name) - 3)
    frmReviewAppts.AutoRedraw = True
    frmReviewAppts.Refresh
End If
End Sub

Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatClinical, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatClinical)) Then
        lblAlert.Visible = True
    End If
End If
End Sub

Private Sub lstAppts_Click()
Dim PatId As Long
Dim ApptId As Long
Dim ReferId As Long
Dim AQuestion As String
Dim ApplList As ApplicationAIList
If (lstAppts.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    If (ApplList.ApplGetStatus(lstAppts.List(lstAppts.ListIndex)) = "D") Then
        ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
        PatId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
        ReferId = ApplList.ApplGetReferralId(lstAppts.List(lstAppts.ListIndex))
        AQuestion = ApplList.ApplGetQuestion(lstAppts.List(lstAppts.ListIndex))
        Set ApplList = Nothing
        If (SetApptOn) Then
            SetApptId = ApptId
            Call cmdDone_Click
        Else
            Label3.Visible = True
            DoEvents
            Call LoadApptDisplay(ApptId, PatId, ReferId, AQuestion)
            Label3.Visible = False
            Call InsertLog("FrmReviewAppts.frm", "Opened Patient Chart  " & CStr(PatId) & "", LoginCatg.EncounterOpen, "", "", 0, AppointmentId)
        End If
    Else
        frmEventMsgs.Header = "Appointment did not occur"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        lstAppts.ListIndex = -1
        Set ApplList = Nothing
    End If
End If
End Sub

Public Function LoadApptsList(PatId As Long, ApptId As Long, DAccess As Boolean) As Boolean
Dim ASex As String
Dim AName As String
Dim APcp As String
Dim ABirthDate As String
Dim TheDate As String
Dim LocalDate As ManagedDate
Dim ApplList As ApplicationAIList
LoadApptsList = False
Set ApplList = New ApplicationAIList
Set LocalDate = New ManagedDate
DirectAccess = DAccess
lblAlert.Visible = False
cmdPrint.Visible = False
cmdChart.Visible = False
cmdRx.Visible = False
cmdOrder.Visible = False
cmdERx.Visible = False
cmdNext.Visible = False
cmdPrev.Visible = False
Label1.Caption = "Review Appointments for "
lstAppts.Clear
TriggerItem = ""
ItemSelected = 0
AppointmentId = ApptId
PatientId = PatId
If (PatId > 0) Then
    Call ApplList.ApplGetPatientInfo(PatId, AName, ASex, APcp, ABirthDate)
    Label1.Caption = "Review Appointments for " + Trim(AName)
    Label2.Visible = False
    Label3.Visible = False
    OrgCaption = Label1.Caption
    TheDate = ""
    If (Trim(StartDate) <> "") Then
        LocalDate.ExposedDate = StartDate
        If (LocalDate.ConvertDisplayDateToManagedDate) Then
            TheDate = LocalDate.ExposedDate
        End If
    Else
        Call FormatTodaysDate(TheDate, False)
        TheDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
    End If
    ApplList.ApplDate = TheDate
    ApplList.ApplPatId = PatId
    ApplList.ApplLocId = -1
    ApplList.ApplResourceId = -1
    ApplList.ApplTransactionStatus = "D"
    Set ApplList.ApplList = lstAppts
    ApplList.ApplStartAt = 0
    ApplList.ApplReturnCount = 0
    Call ApplList.ApplRetrieveReviewApptsList(False)
    If CheckConfigCollection("INVENTORYON") = "T" Then
        Set ApplList = Nothing
        Set ApplList = New ApplicationAIList
        Set ApplList.ApplList = lstAppts
        Call ApplList.ApplSetPCCLOn
        Set ApplList = Nothing
        Set ApplList = New ApplicationAIList
    End If
    If (DAccess) Then
        LoadApptsList = LoadApptDisplay(ApptId, PatId, 0, "")
        DirectAccessApptId = ApptId
        lstAppts.ListIndex = -1
    ElseIf (lstAppts.ListCount > 0) Then
        LoadApptsList = True
    End If
    If (IsAlertPresent(PatId, EAlertType.eatClinical)) Then
        lblAlert.Visible = True
    End If
End If
Set LocalDate = Nothing
Set ApplList = Nothing
End Function

Public Function LoadApptDisplay(ApptId As Long, PatId As Long, ReferId As Long, question As String) As Boolean
Dim ASex As String
Dim AName As String
Dim APcp As String
Dim ApptDate As String
Dim ABirthDate As String
Dim ApplList As ApplicationAIList
Dim RetApt As SchedulerAppointment
LoadApptDisplay = False
lstZoom.Clear
lstZoom.Visible = False
lblAlert.Visible = False
cmdNotes.Visible = True
'cmdPrint.Visible = True
Set ApplList = New ApplicationAIList
Call ApplList.ApplGetPatientInfo(PatId, AName, ASex, APcp, ABirthDate)
If (DirectAccess) Then
    Set RetApt = New SchedulerAppointment
    RetApt.AppointmentId = ApptId
    If (RetApt.RetrieveSchedulerAppointment) Then
        ApptDate = Mid(RetApt.AppointmentDate, 5, 2) + "/" + Mid(RetApt.AppointmentDate, 7, 2) + "/" + Left(RetApt.AppointmentDate, 4)
    End If
    Set RetApt = Nothing
Else
    ApptDate = Trim(Left(lstAppts.List(lstAppts.ListIndex), 75))
End If
Label1.Caption = AName + " " + ApptDate
Label2.Caption = ASex + " " + ABirthDate + " " + APcp + " "
Call ApplList.ApplGetReferral(ApptId, Label2, False)
Call ApplList.ApplGetPreCert(ApptId, Label2, False)
Set ApplList = Nothing
Set ApplDet = Nothing
Set ApplDet = New ApplicationApptDetails
AppointmentId = ApptId
If (ApptId > 0) Then
    Label3.Caption = "Load Details"
    Set ApplDet = New ApplicationApptDetails
    Set ApplDet.lstCC = lstCC
    Set ApplDet.lstALG = lstALG
    Set ApplDet.lstMeds = lstMeds
    Set ApplDet.lstOHX = lstOHX
    Set ApplDet.lstFMX = lstFMX
    Set ApplDet.lstMHX = lstMHX
    Set ApplDet.lstHPI = lstHPI
    Set ApplDet.lstBox = lstPrint
    Set ApplDet.lstOD = lstOD
    Set ApplDet.lstOS = lstOS
    Set ApplDet.lstEval = lstEval
    Set ApplDet.lstImpr = lstImpr
    Set ApplDet.lstTests = lstTests
    LoadApptDisplay = ApplDet.LoadAppointmentDetails(ApptId, PatId, 0)
    If (FM.IsFileThere(SchedulerInterfaceDirectory + Trim(Str(PatId)) + "TestFile.txt")) Then
        FM.Kill SchedulerInterfaceDirectory + Trim(Str(PatId)) + "TestFile.txt"
    End If
End If
If (LoadApptDisplay) Then
    Label2.Visible = True
    lstImpr.Visible = True
    lstOD.Visible = True
    lstOS.Visible = True
    lstTests.Visible = True
    lstEval.Visible = True
    lstHPI.Visible = True
    lstMHX.Visible = True
    lstFMX.Visible = True
    lstOHX.Visible = True
    lstCC.Visible = True
    lstALG.Visible = True
    lstMeds.Visible = True
    lstAppts.Visible = False
    cmdNotes.Visible = True
    cmdChart.Visible = True
    cmdRx.Visible = True
    cmdOrder.Visible = True
    cmdERx.Visible = True
    cmdNext.Visible = True
    cmdPrev.Visible = True
    If (IsAlertPresent(PatId, EAlertType.eatClinical)) Then
        lblAlert.Visible = True
    End If
End If
End Function

Private Function MagnifyList(AList As ListBox) As Boolean
Dim i As Integer
lstZoom.Clear
For i = 0 To AList.ListCount - 1
    lstZoom.AddItem Left(AList.List(i), 75)
Next i
lstZoom.Visible = True
End Function

Private Sub lstCC_Click()
Call MagnifyList(lstCC)
End Sub

Private Sub lstEval_Click()
If (lstEval.ListIndex >= 0) Then
    frmEventMsgs.Header = "Magnify ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        Call MagnifyList(lstEval)
    End If
End If
End Sub

Private Sub lstHPI_Click()
Call MagnifyList(lstHPI)
End Sub

Private Sub lstImpr_Click()
Call MagnifyList(lstImpr)
End Sub

Private Sub lstMeds_Click()
Call MagnifyList(lstMeds)
End Sub

Private Sub lstOHX_Click()
Call MagnifyList(lstOHX)
End Sub

Private Sub lstFMX_Click()
Call MagnifyList(lstFMX)
End Sub

Private Sub lstMHX_Click()
Call MagnifyList(lstMHX)
End Sub

Private Sub lstALG_Click()
Call MagnifyList(lstALG)
End Sub

Private Sub lstTests_Click()
Call MagnifyList(lstTests)
End Sub

Private Sub lstZoom_Click()
lstZoom.Visible = False
lstZoom.Clear
End Sub

Private Sub lstZoom_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
lstZoom.Visible = False
lstZoom.Clear
End Sub
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function
