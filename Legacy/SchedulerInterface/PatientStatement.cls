VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPatientStatement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public PatId                       As Long
Public ReceivableId                As Long
Public TransactionId               As Long
Public Invoice                     As String
Public Aggregate                   As Boolean
Public ver2008 As Boolean
Public EncounterId                 As Long


Private sStatementReportFile       As String
Private Const GeneralReportFile    As String = "statement.rpt"
Private Const GeneralReportFile08    As String = "statement081.rpt"
Private Const OpticalReportFile    As String = "statementOptical.rpt"
Private Const MedicalReportfile    As String = "statementMedical.rpt"

Public Sub setReportFile(rpt As Long)
Select Case rpt
Case 1
    sStatementReportFile = "statementMedical.rpt"
Case 2
    sStatementReportFile = "statementOptical.rpt"
End Select
End Sub

Private Sub Class_Initialize()
    PatId = 0
    TransactionId = 0
    ReceivableId = 0
    EncounterId = 0
    Invoice = vbNullString
    Aggregate = False
    sStatementReportFile = GeneralReportFile
End Sub

Private Function GetInvoiceFromReceivable() As Boolean
Dim Receivable As PatientReceivables

    On Error GoTo lGetInvoiceFromReceivable_Error
    If ReceivableId = 0 Then
        GetReceivableFromEncounter
    End If
    If ReceivableId > 0 Then
        Set Receivable = New PatientReceivables
        Receivable.ReceivableId = ReceivableId
        If Receivable.RetrievePatientReceivable Then
            Invoice = Receivable.ReceivableInvoice
        End If
        Set Receivable = Nothing
    Else
        Err.Raise vbError, , "ReceivableId not initialized."
    End If
Exit Function

lGetInvoiceFromReceivable_Error:
    LogError "CPatientStatement", "GetInvoiceFromReceivable", Err, Err.Description
End Function

Private Function GetReceivableFromEncounter() As Boolean
Dim RetPatientReceivable As PatientReceivables

If EncounterId > 0 Then
    Set RetPatientReceivable = New PatientReceivables
    RetPatientReceivable.AppointmentId = EncounterId
    RetPatientReceivable.PatientId = PatId
    If RetPatientReceivable.FindPatientReceivable Then
        If (RetPatientReceivable.SelectPatientReceivable(1)) Then
            ReceivableId = RetPatientReceivable.ReceivableId
        End If
    End If
    Else
    Err.Raise vbError, , "EncounterId not initialized."
    End If
End Function

Public Function HasDoctor(iDoctorId As Long) As Boolean

    HasDoctor = IsDocLocMatch(iDoctorId, True)
End Function

Public Function HasLocation(iLocationId As Long) As Boolean

    HasLocation = IsDocLocMatch(iLocationId, False)
End Function

Private Function IsDocLocMatch(ByVal iResourceId As Long, _
                               ByVal fCheckDoc As Boolean) As Boolean
Dim Receivable  As PatientReceivables

Dim Transaction As PracticeTransactionJournal
Dim Appointment As SchedulerAppointment

    On Error GoTo lIsDocLocMatch_Error
    IsDocLocMatch = True
    If TransactionId > 0 Then
        Set Transaction = New PracticeTransactionJournal
        Transaction.TransactionJournalId = TransactionId
        If Transaction.RetrieveTransactionJournal Then
            Set Receivable = New PatientReceivables
            Receivable.ReceivableId = Transaction.TransactionJournalTypeId
            If Receivable.RetrievePatientReceivable Then
                Appointment = New SchedulerAppointment
                Appointment.AppointmentId = Receivable.AppointmentId
                If Appointment.RetrieveSchedulerAppointment Then
                    If fCheckDoc Then
                        If Appointment.AppointmentResourceId1 <> iResourceId Then
                            IsDocLocMatch = False
                        End If
                    Else
                        If Appointment.AppointmentResourceId2 <> iResourceId Then
                            IsDocLocMatch = False
                        End If
                    End If
                End If
                Set Appointment = Nothing
            End If
            Set Receivable = Nothing
        End If
        Set Transaction = Nothing
    Else
        Err.Raise vbError, , "TransactionId not initialized."
    End If
Exit Function

lIsDocLocMatch_Error:
    LogError "CPatientStatement", "IsDocLocMatch", Err, Err.Description, "TransactionId " & Str$(TransactionId)
End Function

Public Sub LoginOptMed()
If userlogin.IsLoginOptical Then
    sStatementReportFile = OpticalReportFile
Else
    sStatementReportFile = MedicalReportfile
End If
End Sub

Public Function IsMedical() As Boolean

    If Not IsOptical Then
        sStatementReportFile = MedicalReportfile
        IsMedical = True
    End If
End Function


Public Function IsOptical() As Boolean
Dim Receivable As PatientReceivables

Dim Doctor     As SchedulerResource

    On Error GoTo lIsOptical_Error
    If ReceivableId = 0 Then
        GetReceivableFromEncounter
    End If
    If ReceivableId > 0 Then
        Set Receivable = New PatientReceivables
        Receivable.ReceivableId = ReceivableId
        If Receivable.RetrievePatientReceivable Then
            Set Doctor = New SchedulerResource
            Doctor.ResourceId = Receivable.ReceivableBillToDr
            If Doctor.RetrieveSchedulerResource Then
                If Doctor.ResourceType = "Q" Or Doctor.ResourceType = "U" Then
                    sStatementReportFile = OpticalReportFile
                    IsOptical = True
                End If
            End If
            Set Doctor = Nothing
        End If
        Set Receivable = Nothing
    Else
        Err.Raise vbError, , "ReceivableId not initialized."
    End If
Exit Function

lIsOptical_Error:
    LogError "CPatientStatement", "IsOptical", Err, Err.Description
End Function

Public Function IsOverStatementLimit() As Boolean
Dim Temp              As String
Dim LowerLimit        As Single
Dim TransactionAmount As Single
Dim Transaction       As PracticeTransactionJournal

    On Error GoTo lIsOverStatementLimit_Error
    IsOverStatementLimit = True
    LowerLimit = Round(Val(CheckConfigCollection("STATEMENTLOWERLIMIT", "0")), 2)
    If TransactionId > 0 Then
        Set Transaction = New PracticeTransactionJournal
        Transaction.TransactionJournalId = TransactionId
        If Transaction.RetrieveTransactionJournal Then
            TransactionAmount = Round(Val(Trim$(Mid$(Transaction.TransactionJournalRemark, 6, 20))), 2)
            If LowerLimit > TransactionAmount Then
                If TransactionAmount <> 0 Then
                    IsOverStatementLimit = False
                End If
            End If
        End If
        Set Transaction = Nothing
    End If
Exit Function

lIsOverStatementLimit_Error:
    LogError "CPatientStatement", "IsOverStatementLimit", Err, Err.Description, "TransactionId " & Str$(TransactionId)
End Function


Public Function IsPastStatementRange() As Boolean

Dim sStatementRange As String
Dim sLatestDate     As String

Dim i               As Integer
Dim Transaction     As PracticeTransactionJournal
    On Error GoTo lIsPastStatementRange_Error
    sStatementRange = CheckConfigCollection("STATEMENTRANGE", "0")
    If PatId > 0 Then
        sLatestDate = "20000101"
        Set Transaction = New PracticeTransactionJournal
        Transaction.TransactionJournalRcvrId = PatId
        Transaction.TransactionJournalType = "R"
        Transaction.TransactionJournalReference = "G"
        Transaction.TransactionJournalStatus = vbNullString
        If Transaction.FindTransactionJournal > 0 Then
            i = 1
            With Transaction
                Do While .SelectTransactionJournal(i)
                    If .TransactionJournalDate > sLatestDate Then
                        sLatestDate = .TransactionJournalDate
                    End If
                    i = i + 1
                Loop
            End With
        End If
        Set Transaction = Nothing
        IsPastStatementRange = Not (DateDiff("d", DateValue(Mid$(sLatestDate, 5, 2) & "/" & Mid$(sLatestDate, 7, 2) & "/" & Mid$(sLatestDate, 1, 4)), Now) < Val(sStatementRange))
    Else
        Err.Raise vbError, , "PatientId not initialized."
    End If
Exit Function

lIsPastStatementRange_Error:
    LogError "CPatientStatement", "IsPastStatementRange", Err, Err.Description, sLatestDate
End Function


Private Function PostAggregateTransaction() As Boolean

Dim Transaction       As PracticeTransactionJournal
Dim AppTemplate       As ApplicationTemplates
Dim SDate             As String
Dim STime             As String
Dim LocalDate         As ManagedDate
Dim ITime             As Long
Dim iOpenReceivableId As Long

    FormatTodaysDate SDate, False
    FormatTimeNow STime
    Set LocalDate = New ManagedDate
    LocalDate.ExposedDate = SDate
    If LocalDate.ConvertDisplayDateToManagedDate Then
        SDate = LocalDate.ExposedDate
    End If
    Set LocalDate = Nothing
    ITime = ConvertTimeToMinutes(STime)
    Set AppTemplate = New ApplicationTemplates
    iOpenReceivableId = AppTemplate.ApplGetOpenReceivableId(PatId)
    Set AppTemplate = Nothing
    Set Transaction = New PracticeTransactionJournal
    With Transaction
        .TransactionJournalId = 0
        .RetrieveTransactionJournal
        .TransactionJournalType = "R"
        .TransactionJournalTypeId = iOpenReceivableId
        .TransactionJournalAction = "P"
        .TransactionJournalStatus = "Z"
        .TransactionJournalDate = SDate
        .TransactionJournalTime = ITime
        .TransactionJournalReference = "G"
        .TransactionJournalRemark = vbNullString
        .TransactionJournalServiceItem = 0
        .TransactionJournalAssign = vbNullString
        .TransactionJournalRcvrId = PatId
        .ApplyTransactionJournal
    End With
    Set Transaction = Nothing
End Function

Public Function PostTransactions() As Boolean
    If Aggregate Then
        PostAggregateTransaction
    Else
        SetClaimToBilled
        SetStatementToSent
    End If
End Function

'Adding new functionality to update statements when Set Patient Balance is Pressed.Only for Patient Statement
Public Function SetPatientBalance() As Boolean
    UpdateStatement
    SetClaimToBilled
End Function

Private Function SetClaimToBilled() As Boolean

Dim AIList    As ApplicationAIList
Dim DateToday As String

    On Error GoTo lSetClaimToBilled_Error
    If LenB(Invoice) = 0 Then
        GetInvoiceFromReceivable
    End If
    If LenB(Invoice) > 0 Then
        FormatTodaysDate DateToday, False
        DateToday = Mid$(DateToday, 7, 4) + Mid$(DateToday, 1, 2) + Mid$(DateToday, 4, 2)
        Set AIList = New ApplicationAIList
        AIList.ApplSetClaimStatus Invoice, "B", DateToday
        Set AIList = Nothing
    Else
        Err.Raise vbError, , "Invoice not initialized."
    End If
Exit Function

lSetClaimToBilled_Error:
    LogError "CPatientStatement", "SetClaimToBilled", Err, Err.Description
End Function

Public Function SetStatementToSent() As Boolean
On Error GoTo lSetStatementToSent_Error
SetStatementToSent = False
Dim StrSql As String
Dim RS As Recordset

If EncounterId > 0 Then
    StrSql = " UPDATE model.BillingServiceTransactions SET BillingServiceTransactionStatusId = 2 " & _
             " WHERE Id IN( " & _
             " SELECT bst.Id FROM model.BillingServiceTransactions bst " & _
             " INNER JOIN model.InvoiceReceivables ir ON bst.InvoiceReceivableId = ir.Id " & _
             " INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id " & _
             " Where ir.PatientInsuranceId Is Null " & _
             " AND bst.BillingServiceTransactionStatusId = 1 " & _
             " AND i.EncounterId =" & EncounterId & " ) "
    Set RS = GetRS(StrSql)
    SetStatementToSent = True
    End If
Exit Function
lSetStatementToSent_Error:
    LogError "CPatientStatement", "SetStatementToSent", Err, Err.Description
End Function

Public Function UpdateStatement() As Boolean
On Error GoTo lUpdateStatement_Error
UpdateStatement = False
Dim StrSql As String
Dim RS As Recordset

If EncounterId > 0 Then
    StrSql = " UPDATE model.BillingServiceTransactions SET " & _
             " BillingServiceTransactionStatusId = CASE WHEN BillingServiceTransactionStatusId IN (1,3) THEN 2 WHEN BillingServiceTransactionStatusId = 2 THEN 6  ELSE BillingServiceTransactionStatusId END " & _
             " WHERE Id IN( " & _
             " SELECT bst.Id FROM model.BillingServiceTransactions bst " & _
             " INNER JOIN model.InvoiceReceivables ir ON bst.InvoiceReceivableId = ir.Id " & _
             " INNER JOIN model.Invoices i ON ir.InvoiceId = i.Id " & _
             " Where ir.PatientInsuranceId Is Null " & _
             " AND i.EncounterId =" & EncounterId & " ) "
    Set RS = GetRS(StrSql)
    UpdateStatement = True
End If
Exit Function
lUpdateStatement_Error:
    LogError "CPatientStatement", "UpdateStatement", Err, Err.Description
End Function
