VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmMedicaidLocations 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   240
      MaxLength       =   62
      TabIndex        =   2
      Top             =   6000
      Width           =   3900
   End
   Begin VB.ListBox lstDocs 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4350
      ItemData        =   "MedicaidLocations.frx":0000
      Left            =   240
      List            =   "MedicaidLocations.frx":0002
      TabIndex        =   0
      Top             =   1080
      Width           =   3900
   End
   Begin VB.TextBox txtMedCode 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   4440
      MaxLength       =   62
      TabIndex        =   3
      Top             =   6000
      Width           =   900
   End
   Begin VB.ListBox lstLocs 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4350
      ItemData        =   "MedicaidLocations.frx":0004
      Left            =   4440
      List            =   "MedicaidLocations.frx":0006
      TabIndex        =   1
      Top             =   1080
      Width           =   7260
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   240
      TabIndex        =   4
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MedicaidLocations.frx":0008
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   9840
      TabIndex        =   5
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MedicaidLocations.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   7680
      TabIndex        =   6
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MedicaidLocations.frx":03C6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   990
      Left            =   2400
      TabIndex        =   12
      Top             =   7680
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MedicaidLocations.frx":05A7
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   240
      TabIndex        =   11
      Top             =   5640
      Width           =   930
   End
   Begin VB.Label lblDoctor 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Medicaid Location Codes for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   240
      TabIndex        =   10
      Top             =   240
      Width           =   3705
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctors"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   240
      TabIndex        =   9
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Medicaid Location Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   4440
      TabIndex        =   8
      Top             =   5640
      Width           =   2610
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Locations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   4440
      TabIndex        =   7
      Top             =   720
      Width           =   1050
   End
End
Attribute VB_Name = "frmMedicaidLocations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private DoctorId As Long

Private Sub cmdApply_Click()
Unload frmMedicaidLocations
End Sub

Private Sub cmdPost_Click()
Dim ALocCode As String
Dim AMed As Long, ADoc As Long, ALocId As Long
Dim LocId As Long
Dim LocCode As String
Dim ApplTbl As ApplicationTables
If (lstDocs.ListIndex < 0) Then
    frmEventMsgs.Header = "Must select a doctor"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (lstLocs.ListIndex < 0) Then
    frmEventMsgs.Header = "Must select a location"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (Trim(txtMedCode.Text) = "") Then
    frmEventMsgs.Header = "Must enter a Medicaid Location Code"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
Set ApplTbl = New ApplicationTables
LocCode = Trim(txtMedCode.Text)
If (lstLocs.ListIndex = 0) Then
    LocId = val(Mid(txtLoc.Text, 57, 5))
    If (LocId < 1) Then
        LocId = -1
    End If
    Call ApplTbl.ApplPostMedicaidLocation(DoctorId, LocId, LocCode)
Else
    AMed = val(Trim(Mid(lstLocs.List(lstLocs.ListIndex), 57, 5)))
    Call ApplTbl.ApplGetMedicaidLocation(AMed, ADoc, ALocId, ALocCode)
    Call ApplTbl.ApplDeleteMedicaidLocation(AMed)
    Call ApplTbl.ApplPostMedicaidLocation(ADoc, ALocId, LocCode)
End If
Set ApplTbl = Nothing
Label1.Visible = False
txtLoc.Text = ""
txtLoc.Visible = False
txtLoc.Locked = False
Label2.Visible = False
txtMedCode.Visible = False
txtMedCode.Text = ""
Call LoadMedLocList(DoctorId)
End Sub

Private Sub cmdDelete_Click()
Dim LocId As Long
Dim ApplTbl As ApplicationTables
If (lstDocs.ListIndex < 0) Then
    frmEventMsgs.Header = "Must select a doctor"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (lstLocs.ListIndex < 1) Then
    frmEventMsgs.Header = "Must select a location"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
Set ApplTbl = New ApplicationTables
LocId = val(Trim(Mid(lstLocs.List(lstLocs.ListIndex), 57, 5)))
Call ApplTbl.ApplDeleteMedicaidLocation(LocId)
Set ApplTbl = Nothing
Label1.Visible = False
txtLoc.Text = ""
txtLoc.Visible = False
txtLoc.Locked = False
Label2.Visible = False
txtMedCode.Visible = False
txtMedCode.Text = ""
Call LoadMedLocList(DoctorId)
End Sub

Private Sub cmdExit_Click()
Unload frmMedicaidLocations
End Sub

Public Function LoadMedLocList(ADoc As Long) As Boolean
Dim i As Integer
Dim AName As String
Dim ApplTbl As ApplicationTables
Dim ApplTemp As ApplicationTemplates
LoadMedLocList = False
DoctorId = ADoc
Set ApplTbl = New ApplicationTables
Set ApplTbl.lstBox = lstDocs
Call ApplTbl.ApplLoadResource("D", False, False, False, False, False)
Label1.Visible = False
txtLoc.Visible = False
Label2.Visible = False
txtMedCode.Visible = False
If (ADoc < 1) Then
    LoadMedLocList = True
    lblDoctor.Caption = "Select Doctor"
    lstLocs.Clear
    lstLocs.AddItem "Add Medicaid Location Code"
Else
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetResourceName(0, DoctorId, AName)
    lblDoctor.Caption = "Medicaid Location Codes for " + Trim(AName)
    Set ApplTemp = Nothing
    LoadMedLocList = True
    For i = 0 To lstDocs.ListCount - 1
        If (DoctorId = val(Trim(Mid(lstDocs.List(i), 57, 5)))) Then
            lstDocs.ListIndex = i
            Exit For
        End If
    Next i
End If
Set ApplTbl = Nothing
End Function

Private Sub lstDocs_Click()
Dim i As Integer
Dim AName As String
Dim ApplTbl As ApplicationTables
Dim ApplTemp As ApplicationTemplates
If (lstDocs.ListIndex >= 0) Then
    DoctorId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 57, 5)))
    Set ApplTemp = New ApplicationTemplates
    Call ApplTemp.ApplGetResourceName(0, DoctorId, AName)
    lblDoctor.Caption = "Medicaid Location Codes for " + Trim(AName)
    Set ApplTemp = Nothing
    Set ApplTbl = New ApplicationTables
    Set ApplTbl.lstBox = lstLocs
    Call ApplTbl.ApplLoadMedicaidLocations(DoctorId)
    Set ApplTbl = Nothing
    txtLoc.Locked = False
End If
End Sub

Private Sub lstLocs_Click()
If (lstLocs.ListIndex = 0) Then
    Label2.Visible = True
    txtMedCode.Text = Trim(Mid(lstLocs.List(lstLocs.ListIndex), 40, 2))
    txtMedCode.Visible = True
    Label1.Visible = True
    txtLoc.Visible = True
    txtLoc.Text = ""
    txtLoc.Locked = False
ElseIf (lstLocs.ListIndex > 0) Then
    Label2.Visible = True
    txtMedCode.Text = Trim(Mid(lstLocs.List(lstLocs.ListIndex), 40, 2))
    txtMedCode.Visible = True
    Label1.Visible = True
    txtLoc.Visible = True
    txtLoc.Text = Trim(Left(lstLocs.List(lstLocs.ListIndex), 30))
    txtLoc.Locked = True
End If
End Sub

Private Sub txtLoc_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("ScheduledLocation")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtLoc.Text = UCase(frmSelectDialogue.Selection)
End If
End Sub

Private Sub txtLoc_KeyPress(KeyAscii As Integer)
Call txtLoc_Click
End Sub

Private Sub txtMedCode_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If (IsNumeric(Trim(txtMedCode.Text))) Then
        If (val(Trim(txtMedCode.Text)) < 1) Or (val(Trim(txtMedCode.Text)) > 20) Then
            frmEventMsgs.Header = "Must be [1,20] only."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtMedCode.Text = ""
            txtMedCode.SetFocus
        End If
    Else
        frmEventMsgs.Header = "Must be [1,20] only."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtMedCode.Text = ""
        txtMedCode.SetFocus
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
