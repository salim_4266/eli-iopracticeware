VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmListFacAdm 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "ListFacAdm.frx":0000
      Left            =   3600
      List            =   "ListFacAdm.frx":0002
      TabIndex        =   13
      Top             =   120
      Width           =   2655
   End
   Begin VB.ListBox lstForms 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5415
      Left            =   2520
      MultiSelect     =   1  'Simple
      TabIndex        =   12
      Top             =   1680
      Visible         =   0   'False
      Width           =   6135
   End
   Begin VB.ListBox lstPrint 
      Height          =   450
      ItemData        =   "ListFacAdm.frx":0004
      Left            =   120
      List            =   "ListFacAdm.frx":0006
      Sorted          =   -1  'True
      TabIndex        =   11
      Top             =   8040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid lstDocs 
      Height          =   5655
      Left            =   120
      TabIndex        =   2
      Top             =   1680
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   9975
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "ListFacAdm.frx":0008
      Left            =   6360
      List            =   "ListFacAdm.frx":000A
      TabIndex        =   6
      Top             =   120
      Width           =   2055
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "ListFacAdm.frx":000C
      Left            =   8520
      List            =   "ListFacAdm.frx":000E
      TabIndex        =   4
      Top             =   120
      Width           =   1575
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "ListFacAdm.frx":0010
      Left            =   10200
      List            =   "ListFacAdm.frx":0012
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1095
      Left            =   5520
      TabIndex        =   0
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListFacAdm.frx":0014
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   10320
      TabIndex        =   1
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListFacAdm.frx":0200
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   720
      TabIndex        =   5
      Top             =   720
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   375
      Left            =   720
      TabIndex        =   9
      Top             =   1200
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAny 
      Height          =   1095
      Left            =   3840
      TabIndex        =   14
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListFacAdm.frx":03DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAged 
      Height          =   1095
      Left            =   2160
      TabIndex        =   16
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ListFacAdm.frx":05CB
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   15
      Top             =   720
      Width           =   450
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   120
      TabIndex        =   10
      Top             =   1200
      Width           =   210
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Surgeries"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   1230
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   7680
      Visible         =   0   'False
      Width           =   780
   End
End
Attribute VB_Name = "frmListFacAdm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private DoAll As Boolean
Private AnyOn As Boolean
Private ZAccessOn As Boolean
Private ResourceId As Long
Private ResourceLocId As Long
Private TheTest As String
Private TheStatus As String
Private CurrentRow As Integer
Private m_Direction As String
Private m_SortColumnName As String
Private m_SortColumn As Integer
Private m_SortOrder As SortSettings

Private Sub cmdAged_Click()
AnyOn = True
ZAccessOn = True
Call LoadFacilityAdminList(True, AnyOn, ZAccessOn)
End Sub

Private Sub cmdAny_Click()
AnyOn = True
ZAccessOn = False
Call LoadFacilityAdminList(True, AnyOn, ZAccessOn)
End Sub

Private Sub cmdDisplay_Click()
AnyOn = False
Call LoadFacilityAdminList(False, AnyOn, False)
End Sub

Private Sub cmdDone_Click()
Unload frmListFacAdm
End Sub

Public Function LoadFacilityAdminList(InitOn As Boolean, AnyOn As Boolean, ZOn As Boolean) As Boolean
Dim p As Integer
Dim i As Integer
Dim m As Integer
Dim Temp As String
Dim ADate As String
Dim CleanOn As Boolean
Dim MyType As Integer
Dim DateNow As String
Dim ApplList As ApplicationAIList
'm_SortColumn = 0
LoadFacilityAdminList = False
ADate = SSDateCombo1.DateValue
Call FormatTodaysDate(ADate, False)
ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
DateNow = SSDateCombo2.DateValue
Call FormatTodaysDate(DateNow, False)
DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
MyType = 0
If (InitOn) Or (AnyOn) Then
    If (AnyOn) Then
        lstLoc.ListIndex = 0
        lstDr.ListIndex = 0
        lstStatus.ListIndex = 0
        lstTests.ListIndex = 0
        ResourceId = -1
        ResourceLocId = -1
        TheStatus = "0"
        ADate = ""
        DateNow = ""
        MyType = 1
        If (ZOn) Then
            MyType = 2
        End If
    Else
        TheTest = ""
        TheStatus = ""
        m_Direction = "A"
    End If
    If Not (AnyOn) Then
        Set ApplList = New ApplicationAIList
        Set ApplList.ApplList = lstDr
        Call ApplList.ApplLoadStaff(False)
        Set ApplList = Nothing
        ResourceId = -1
        Set ApplList = New ApplicationAIList
        Set ApplList.ApplList = lstLoc
        Call ApplList.ApplLoadLocation(False, False)
        Set ApplList = Nothing
        ResourceLocId = -1
        Set ApplList = New ApplicationAIList
        Call ApplList.ApplGetCodesbyList("SURGERYSTATUS", False, True, lstStatus)
        lstStatus.List(0) = "All Status"
        Call ApplList.ApplGetCodesbyList("SURGERYTYPE", False, True, lstTests)
        lstTests.List(0) = "All Surgeries"
        Set ApplList = Nothing
    End If
End If
Set ApplList = New ApplicationAIList
ApplList.ApplStartDate = ADate
ApplList.ApplEndDate = DateNow
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplConfirmStat = TheStatus
ApplList.ApplOtherFilter = TheTest
Set ApplList.ApplGrid = lstDocs
Call ApplList.ApplRetrieveFacilityAdminsList(MyType)
Set ApplList = Nothing
' Clear any empty cells
m = lstDocs.Rows - 1
For i = 1 To m
    CleanOn = True
    For p = 1 To 10
        If (lstDocs.TextMatrix(i, p) <> "") Then
            CleanOn = False
            Exit For
        End If
    Next p
    If (CleanOn) Then
        If (i > 1) Then
            lstDocs.RemoveItem i
            If (i <> m) Then
                i = i - 1
                m = m - 1
            End If
        End If
    End If
Next i
LoadFacilityAdminList = True
If (AnyOn) Then
    Call SortByColumn(m_SortColumn, False)
Else
    Call SortByColumn(m_SortColumn, True)
End If
End Function

Private Sub Form_Load()
m_SortColumn = 0
m_Direction = "A"
DoAll = False
ZAccessOn = False
If UserLogin.HasPermission(epPowerUser) Then
    frmListFacAdm.BorderStyle = 1
    frmListFacAdm.ClipControls = True
    frmListFacAdm.Caption = Mid(frmListFacAdm.Name, 4, Len(frmListFacAdm.Name) - 3)
    frmListFacAdm.AutoRedraw = True
    frmListFacAdm.Refresh
End If
End Sub

Private Sub SortByColumn(ByVal sort_column As Integer, PassiveOn As Boolean)
Dim i As Integer
Dim Temp As String
If (sort_column > 0) Then
    lstDocs.Visible = False
    lstDocs.Refresh
    If (InStrPS(lstDocs.TextMatrix(0, sort_column), "Date") > 0) Then
        For i = 1 To lstDocs.Rows - 1
            Temp = lstDocs.TextMatrix(i, sort_column)
            If (Trim(Temp) <> "") And (InStrPS(Temp, "/") > 0) Then
                Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
            End If
            lstDocs.TextMatrix(i, sort_column) = Temp
        Next i
    End If
' Sort using the clicked column.
    lstDocs.col = sort_column
    lstDocs.ColSel = sort_column
    lstDocs.Row = 0
    lstDocs.RowSel = 0
' If this is a new sort column, sort ascending.
' Otherwise switch which sort order we use.
    If m_SortColumn <> sort_column Then
        m_SortOrder = flexSortGenericAscending
        m_Direction = "A"
    ElseIf m_SortOrder = flexSortGenericAscending Then
        If Not (PassiveOn) Then
            m_SortOrder = flexSortGenericDescending
            m_Direction = "D"
        End If
    Else
        If Not (PassiveOn) Then
            m_SortOrder = flexSortGenericAscending
            m_Direction = "A"
        End If
    End If
    lstDocs.Sort = m_SortOrder

' Restore the previous sort column's name.
    If (m_SortColumn <> sort_column) And (m_SortColumn > 0) Then
        If (m_SortColumnName <> "") Then
            lstDocs.TextMatrix(0, m_SortColumn) = m_SortColumnName
        End If
        m_SortColumnName = lstDocs.TextMatrix(0, sort_column)
    ElseIf (Trim(m_SortColumnName) = "") Then
        m_SortColumnName = lstDocs.TextMatrix(0, sort_column)
    End If

' Display the new sort column's name.
    m_SortColumn = sort_column
    If m_SortOrder = flexSortGenericAscending Then
        lstDocs.TextMatrix(0, m_SortColumn) = ">" & m_SortColumnName
    Else
        lstDocs.TextMatrix(0, m_SortColumn) = "<" & m_SortColumnName
    End If
    If (InStrPS(m_SortColumnName, "Date") > 0) Then
        For i = 1 To lstDocs.Rows - 1
            Temp = lstDocs.TextMatrix(i, sort_column)
            If (Trim(Temp) <> "") And (IsNumeric(Temp)) Then
                Temp = Mid(Temp, 5, 2) + "/" + Mid(Temp, 7, 2) + "/" + Left(Temp, 4)
                lstDocs.TextMatrix(i, sort_column) = Temp
            End If
        Next i
    End If
    lstDocs.Visible = True
End If
End Sub

Private Sub lstDocs_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    ' If this is not row 0, do nothing.
    If lstDocs.MouseRow <> 0 Then
        Exit Sub
    End If
    ' Sort by the clicked column.
    Call SortByColumn(lstDocs.MouseCol, False)
End Sub

Private Sub lstDocs_Click()
Dim i As Integer
Dim PatId As Long
CurrentRow = lstDocs.RowSel
i = lstDocs.ColSel
If (CurrentRow > 0) Then
    If (i = 0) Then
        If (Trim(lstDocs.TextMatrix(CurrentRow, 0)) = "") Then
            lstDocs.TextMatrix(CurrentRow, 0) = "Y"
        Else
            lstDocs.TextMatrix(CurrentRow, 0) = ""
        End If
    Else
        If (i = 3) Then
            PatId = val(lstDocs.TextMatrix(CurrentRow, 3))
            If (PatId > 0) Then
                Dim PatientDemographics As New PatientDemographics
                PatientDemographics.PatientId = PatId
                If Not PatientDemographics.DisplayPatientInfoScreen Then Exit Sub
            End If
        Else
            lstDocs.TextMatrix(CurrentRow, 0) = "Y"
        End If
    End If
End If
End Sub

Private Sub lstDocs_dblClick()
Dim i As Integer
Dim cnt As Integer
Dim PatId As Long
Dim p1 As Long
Dim p2 As Long
Dim ApptId As Long
Dim Temp As String
Dim RetPat As Patient
CurrentRow = lstDocs.RowSel
If (CurrentRow > 0) Then
    PatId = val(lstDocs.TextMatrix(CurrentRow, 3))
    ApptId = val(lstDocs.TextMatrix(CurrentRow, 9))
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Generate Forms"
    frmEventMsgs.RejectText = "Set Status"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Edit Surgical Request"
    frmEventMsgs.Other1Text = "Financial Resp"
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        cnt = 0
        For i = 1 To lstDocs.Rows - 1
            If (lstDocs.TextMatrix(i, 0) = "Y") Then
                cnt = cnt + 1
            End If
        Next i
        If (cnt > 1) Then
            frmEventMsgs.Header = "Create Facility Forms ?"
            frmEventMsgs.AcceptText = "Last Selected"
            frmEventMsgs.RejectText = "All Selected"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                DoAll = False
                Call LoadFacilityForms
            ElseIf (frmEventMsgs.Result = 2) Then
                DoAll = True
                Call LoadFacilityForms
            End If
        Else
            DoAll = False
            Call LoadFacilityForms
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        cnt = 0
        For i = 1 To lstDocs.Rows - 1
            If (lstDocs.TextMatrix(i, 0) = "Y") Then
                cnt = cnt + 1
            End If
        Next i
        If (cnt > 1) Then
            Temp = ""
            frmEventMsgs.Header = "Set Status"
            frmEventMsgs.AcceptText = "Last Selected"
            frmEventMsgs.RejectText = "All Selected"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                Call MaintainStatus(CurrentRow, Temp)
                Call LoadFacilityAdminList(False, AnyOn, False)
            ElseIf (frmEventMsgs.Result = 2) Then
                For i = 1 To lstDocs.Rows - 1
                    If (lstDocs.TextMatrix(i, 0) = "Y") Then
                        Call MaintainStatus(i, Temp)
                        lstDocs.TextMatrix(i, 0) = " "
                    End If
                Next i
                Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
            End If
        Else
            Call MaintainStatus(CurrentRow, Temp)
            Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Call SelectSurgical(CurrentRow)
        Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
    ElseIf (frmEventMsgs.Result = 5) Then
        If (PatId > 0) Then
            Call DisplayInsuranceScreen(PatId)
        End If
    End If
End If
End Sub

Private Sub lstForms_Click()
If (lstForms.ListIndex = 1) Then
    Call BuildFacilityForms(DoAll)
    lstForms.Visible = False
ElseIf (lstForms.ListIndex = 0) Then
    lstForms.Visible = False
End If
End Sub

Private Sub lstTests_Click()
If (lstTests.ListIndex >= 0) Then
    If (TheTest = Trim(lstTests.List(lstTests.ListIndex))) Or (lstTests.ListIndex = 0) Then
        TheTest = ""
    Else
        TheTest = Trim(lstTests.List(lstTests.ListIndex))
    End If
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        ResourceLocId = -1
    Else
        Set ApplList = New ApplicationAIList
        ResourceLocId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As ApplicationAIList
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = -1
    Else
        Set ApplList = New ApplicationAIList
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    If (TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)) Or (lstStatus.ListIndex = 0) Then
        TheStatus = ""
    Else
        TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)
    End If
End If
End Sub

Private Function MaintainStatus(Idx As Integer, IType As String) As Boolean
Dim ClnId As Long
Dim Temp As String
Dim RetCln As PatientClinical
MaintainStatus = False
If (IType = "") Then
    frmSelectDialogue.InsurerSelected = ""
    Call frmSelectDialogue.BuildSelectionDialogue("SURGERYSTATUS")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        IType = Left(frmSelectDialogue.Selection, 1)
    End If
End If
If (Trim(IType) <> "") Then
    ClnId = val(lstDocs.TextMatrix(Idx, 11))
    If (ClnId > 0) Then
        Set RetCln = New PatientClinical
        RetCln.ClinicalId = ClnId
        If (RetCln.RetrievePatientClinical) Then
            RetCln.ClinicalSurgery = IType
            MaintainStatus = RetCln.ApplyPatientClinical
        End If
        Set RetCln = Nothing
    End If
End If
End Function

Private Function SelectSurgical(Idx As Integer) As Boolean
Dim i As Integer, j As Integer
Dim VndIdOrg As Long
Dim PatId As Long, ApptId As Long, TransId As Long
Dim ARef As String, ARem As String, AEye As String
Dim ALoc As String, ADate As String, AName As String
SelectSurgical = False
If (Idx > 0) Then
    PatId = val(lstDocs.TextMatrix(Idx, 3))
    ApptId = val(lstDocs.TextMatrix(Idx, 9))
    If Not (AnyOn) Then
        ApptId = val(lstDocs.TextMatrix(Idx, 10))
    End If
    TransId = val(lstDocs.TextMatrix(Idx, 11))
    ARem = Trim(lstDocs.TextMatrix(Idx, 5))
    AEye = Mid(ARem, Len(ARem) - 2, 2)
    If (AEye <> "OD") And (AEye <> "OS") And (AEye <> "OU") Then
        AEye = ""
    End If
    ARem = Trim(Left(ARem, Len(ARem) - 4))
    ADate = Trim(lstDocs.TextMatrix(Idx, 6))
    ALoc = Trim(lstDocs.TextMatrix(Idx, 8))
    frmSurgicalAI.ClinicalId = TransId
    frmSurgicalAI.PatientId = PatId
    frmSurgicalAI.AppointmentId = ApptId
    frmSurgicalAI.TheProcs = ARem
    frmSurgicalAI.TheLoc = ALoc
    frmSurgicalAI.TheDate = ADate
    frmSurgicalAI.TheEye = AEye
    frmSurgicalAI.DocName = Trim(lstDocs.TextMatrix(Idx, 7))
    frmSurgicalAI.OrderDate = Trim(lstDocs.TextMatrix(Idx, 2))
    frmSurgicalAI.Show 1
    SelectSurgical = True
    DoEvents
    DoEvents
End If
End Function
Private Function LoadFacilityForms() As Boolean
Dim ApplTemp As ApplicationTemplates
LoadFacilityForms = False
lstForms.Visible = False
lstForms.Clear
Set ApplTemp = New ApplicationTemplates
Set ApplTemp.lstBoxA = lstForms
Call ApplTemp.LoadFacilityAdmissionList(True)
Set ApplTemp = Nothing
If (lstForms.ListCount > 2) Then
    lstForms.Visible = True
    LoadFacilityForms = True
End If
End Function

Private Function BuildFacilityForms(AItem As Boolean) As Boolean
Dim u As Integer
Dim i As Integer
Dim cnt As Integer
Dim MyCnt As Integer
Dim Temp As String
Dim TransId As Long
Dim AFile As String
Dim ApplTemp As ApplicationTemplates
If (AItem) Then
    For u = 1 To lstDocs.Rows - 1
        If (lstDocs.TextMatrix(u, 0) = "Y") Then
            cnt = cnt + 1
        End If
    Next u
    For i = 2 To lstForms.ListCount - 1
        If (lstForms.Selected(i)) Then
            Temp = Trim(Mid(lstForms.List(i), 51, 50))
            lblPrint.Caption = "Creation In Progress for " + Trim(Left(lstForms.List(i), 50))
            lblPrint.Visible = True
            DoEvents
            MyCnt = 0
            For u = 1 To lstDocs.Rows - 1
                If (lstDocs.TextMatrix(u, 0) = "Y") Then
                    MyCnt = MyCnt + 1
                    TransId = val(lstDocs.TextMatrix(u, 11))
                    If (TransId > 0) Then
                        Set ApplTemp = New ApplicationTemplates
                        Set ApplTemp.lstBox = lstPrint
                        If (MyCnt = 1) Then
                            If (MyCnt = cnt) Then
                                Call ApplTemp.PrintTransaction(TransId, 2, "Y", True, True, Temp, False, 0, "S", "", 0)
                            Else
                                Call ApplTemp.PrintTransaction(TransId, 2, "Y", True, False, Temp, False, 0, "S", "", 0)
                            End If
                        ElseIf (MyCnt = cnt) Then
                            Call ApplTemp.PrintTransaction(TransId, 2, "Y", False, True, Temp, False, 0, "S", "", 0)
                        Else
                            Call ApplTemp.PrintTransaction(TransId, 2, "Y", False, False, Temp, False, 0, "S", "", 0)
                        End If
                        Set ApplTemp = Nothing
                    End If
                End If
            Next u
            lblPrint.Visible = False
            DoEvents
        End If
    Next i
Else
    If (CurrentRow > 0) Then
        TransId = val(lstDocs.TextMatrix(CurrentRow, 11))
        If (TransId > 0) Then
            For i = 2 To lstForms.ListCount - 1
                If (lstForms.Selected(i)) Then
                    Temp = Trim(Mid(lstForms.List(i), 51, 50))
                    lblPrint.Caption = "Creation In Progress for " + Trim(Left(lstForms.List(i), 50))
                    lblPrint.Visible = True
                    DoEvents
                    Set ApplTemp = New ApplicationTemplates
                    Set ApplTemp.lstBox = lstPrint
                    AFile = "S"
                    Call ApplTemp.PrintTransaction(TransId, 2, "Y", True, True, Temp, False, 0, "S", AFile, 0)
                    Set ApplTemp = Nothing
                    AFile = Left(AFile, Len(AFile) - 3) + "doc"
                    Call TriggerWord(AFile)
                End If
                lblPrint.Visible = False
                DoEvents
            Next i
        End If
    End If
End If
End Function
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub

