VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{C5FBB860-7B42-11CF-A8A0-444553540000}#1.0#0"; "CSDNS32.OCX"
Object = "{B92844A0-9A8B-11D0-9586-0000E8C0DC7F}#1.0#0"; "CSMSG32.OCX"
Object = "{F070A868-7D7E-11CF-A8A0-444553540000}#1.0#0"; "CSMTP32.OCX"
Begin VB.Form frmReviewDocs 
   BackColor       =   &H0073702B&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin SmtpClientCtrl.SmtpClient SmtpClient1 
      Left            =   7920
      Top             =   600
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      Address         =   ""
      AutoResolve     =   -1  'True
      Blocking        =   -1  'True
      Domain          =   ""
      HostAddress     =   ""
      HostName        =   ""
      RemotePort      =   0
      RemoteService   =   ""
      Timeout         =   60
      UserName        =   ""
   End
   Begin MailMessageCtrl.MailMessage MailMessage1 
      Left            =   7440
      Top             =   600
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   16
      FileName        =   ""
      Localize        =   0   'False
   End
   Begin DnsClientCtrl.DnsClient DnsClient1 
      Left            =   6960
      Top             =   600
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      HostProtocol    =   6
      LocalDomain     =   ""
      RemotePort      =   53
      RemoteService   =   "domain"
      Retry           =   4
      Timeout         =   4
      RecordName      =   ""
      RecordType      =   0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1095
      Left            =   4800
      TabIndex        =   28
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":0000
   End
   Begin VB.ListBox lstFiles 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      ItemData        =   "ReviewDocs.frx":01E2
      Left            =   3360
      List            =   "ReviewDocs.frx":01E4
      TabIndex        =   27
      Top             =   2280
      Visible         =   0   'False
      Width           =   4215
   End
   Begin VB.ListBox lstTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "ReviewDocs.frx":01E6
      Left            =   4680
      List            =   "ReviewDocs.frx":01E8
      Sorted          =   -1  'True
      TabIndex        =   25
      Top             =   360
      Width           =   1815
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "ReviewDocs.frx":01EA
      Left            =   9960
      List            =   "ReviewDocs.frx":01EC
      TabIndex        =   19
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstPrint 
      Height          =   645
      ItemData        =   "ReviewDocs.frx":01EE
      Left            =   120
      List            =   "ReviewDocs.frx":01F5
      Sorted          =   -1  'True
      TabIndex        =   10
      Top             =   1320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Timer Timer1 
      Left            =   8400
      Top             =   600
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "ReviewDocs.frx":0203
      Left            =   8280
      List            =   "ReviewDocs.frx":0205
      TabIndex        =   0
      Top             =   360
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   9960
      TabIndex        =   3
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":0207
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   1110
      Left            =   8160
      TabIndex        =   5
      Top             =   7440
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":03E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   870
      Left            =   1560
      TabIndex        =   12
      Top             =   7440
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":05CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   855
      Left            =   240
      TabIndex        =   13
      Top             =   7440
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":07AC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAll 
      Height          =   1095
      Left            =   3120
      TabIndex        =   16
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":098B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPurge 
      Height          =   1110
      Left            =   3120
      TabIndex        =   15
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":0B78
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLetters 
      Height          =   1110
      Left            =   6480
      TabIndex        =   9
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":0D67
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQueue 
      Height          =   1110
      Left            =   4800
      TabIndex        =   17
      Top             =   7440
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":0F49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSend 
      Height          =   1110
      Left            =   4920
      TabIndex        =   2
      Top             =   7440
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":1129
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLegend 
      Height          =   510
      Left            =   240
      TabIndex        =   18
      Top             =   8400
      Visible         =   0   'False
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   900
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewDocs.frx":130C
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   240
      TabIndex        =   21
      Top             =   840
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BackColor       =   7828525
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "ReviewDocs.frx":14F4
      Left            =   6600
      List            =   "ReviewDocs.frx":14F6
      TabIndex        =   22
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstAllEntries 
      Height          =   645
      ItemData        =   "ReviewDocs.frx":14F8
      Left            =   120
      List            =   "ReviewDocs.frx":14FA
      MultiSelect     =   1  'Simple
      TabIndex        =   24
      Top             =   1320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.ListBox lstDocs 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6060
      ItemData        =   "ReviewDocs.frx":14FC
      Left            =   360
      List            =   "ReviewDocs.frx":14FE
      MultiSelect     =   1  'Simple
      TabIndex        =   1
      Top             =   1320
      Width           =   11175
   End
   Begin VB.ListBox lstType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "ReviewDocs.frx":1500
      Left            =   4440
      List            =   "ReviewDocs.frx":1510
      TabIndex        =   14
      Top             =   360
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblTests 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Tests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4680
      TabIndex        =   26
      Top             =   120
      Width           =   480
   End
   Begin VB.Label lblStatus 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6600
      TabIndex        =   23
      Top             =   120
      Width           =   570
   End
   Begin VB.Label lblLoc 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9960
      TabIndex        =   20
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1560
      TabIndex        =   11
      Top             =   600
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label lblSending 
      AutoSize        =   -1  'True
      BackColor       =   &H0000FFFF&
      Caption         =   "Sending Msg"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2280
      TabIndex        =   8
      Top             =   960
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   7
      Top             =   600
      Width           =   405
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8280
      TabIndex        =   6
      Top             =   120
      Width           =   570
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Review Documents for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   240
      TabIndex        =   4
      Top             =   240
      Width           =   2880
   End
End
Attribute VB_Name = "frmReviewDocs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CollectionAction As String
Private CollectionLetter As String
Private TransTypeId As Long
Private TransType As String
Private DisplayType As String
Private SendOn As Boolean
Private TriggerOn As Boolean
Private PatientId As Long
Private ResourceId As Long
Private ResourceLocId As Long
Private ActivityDate As String
Private DateNow As String
Private TheName As String
Private PartyType As String
Private TheTest As String
Private TheStatus As String
Private PageCnt As Integer
Private TotalPageCnt As Integer
Private CurrentIndex As Integer
Private MidCurrentIndex As Integer
Private PrevCurrentIndex As Integer
Private TotalFound As Long
Private Const MaxCount As Integer = 25

Private Sub cmdAll_Click()
Dim i As Integer
Dim k As Integer
Dim DoIt As Boolean
If (lstDocs.ListCount > 0) Then
    frmEventMsgs.Header = "Do you want to create all the letters ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        DoIt = True
        If (TransType = "W") Then
            DoIt = SelectCollectionLetter(CollectionLetter)
        End If
        If (DoIt) Then
            For i = 0 To lstDocs.ListCount - 1
                Call MaintainLetter(i, TransType, "E", True)
                For k = 0 To 100
                    DoEvents
                Next k
            Next i
        End If
    End If
End If
End Sub

Private Sub cmdDisplay_Click()
CurrentIndex = 1
Call LoadClinicalList(False)
End Sub

Private Sub cmdDone_Click()
Dim ApplTemp As ApplicationTemplates
If (InStrPS(Label1.Caption, "Review Appointments") <> 0) Then
    Call LoadDocumentsList(SendOn, TransType, PartyType, False)
Else
    Unload frmReviewDocs
End If
End Sub

Private Sub cmdLegend_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("LETTERSTATUS")
frmSelectDialogue.Show 1
End Sub

Private Sub cmdLetters_Click()
Dim PatId As Long
Dim TotalFound As Integer
Dim ApplLetters As Letters
Dim ReturnArguments As Variant

Set ReturnArguments = DisplayPatientSearchScreen
If Not (ReturnArguments Is Nothing) Then
    PatId = ReturnArguments.PatientId
End If
If (PatId > 0) Then
    lstDocs.Clear
    Label1.Caption = "Review Appointments "
    cmdLetters.Visible = False
    PatientId = PatId
    Set ApplLetters = New Letters
    Set ApplLetters.ApplList = lstDocs
    ApplLetters.ApplDate = ""
    ApplLetters.ApplPatId = PatientId
    TotalFound = ApplLetters.ApplRetrieveLettersList
    Set ApplLetters = Nothing
    If (TotalFound < 1) Then
        frmEventMsgs.Header = "No Appointments"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Call cmdDone_Click
    End If
End If
End Sub

Private Sub cmdPatient_Click()
Dim PatId As Long
Dim AccessType As Boolean
Dim ApplList As ApplicationAIList
If (lstDocs.ListIndex >= 0) Then
    AccessType = False
    Set ApplList = New ApplicationAIList
    If (TransType = "L") Then
        PatId = ApplList.ApplGetTransPatientId(lstDocs.List(lstDocs.ListIndex))
    ElseIf (TransType = "J") Or (TransType = "W") Then
        PatId = ApplList.ApplGetConfPatientId(lstDocs.List(lstDocs.ListIndex))
    ElseIf (TransType = "T") Or (TransType = "X") Then
        PatId = ApplList.ApplGetTranscribePatientId(lstDocs.List(lstDocs.ListIndex))
    Else
        PatId = ApplList.ApplGetTransPatientId(lstDocs.List(lstDocs.ListIndex))
    End If
    Set ApplList = Nothing
    If (PatId > 0) Then
        Select Case TransType
        Case "L", "J", "X"
            AccessType = True
        End Select

        Dim PatientDemoGraphics As New PatientDemoGraphics
        PatientDemoGraphics.PatientId = PatId
        If Not PatientDemoGraphics.DisplayPatientInfoScreen Then Exit Sub
    End If
Else
    If (lstDocs.ListCount > 0) Then
        frmEventMsgs.Header = "Select Patient"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdNext_Click()
If (TransType = "J") Then
    PrevCurrentIndex = MidCurrentIndex
    MidCurrentIndex = CurrentIndex
    Call LoadApptList(0, False)
ElseIf (TransType = "X") Then
'    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = CurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    Call LoadClinicalList(False)
ElseIf (TransType = "T") Then
'    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = CurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    Call LoadTranscribeList(False)
ElseIf (TransType = "W") Then
'    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = CurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    Call LoadCollectionList(False)
Else
    PageCnt = PageCnt + 1
    If (PageCnt > TotalPageCnt) Then
        PageCnt = 0
    End If
    CurrentIndex = (PageCnt * MaxCount) + 1
    If (CurrentIndex > TotalFound) Then
        CurrentIndex = 1
    End If
    Call LoadDocList(False, DisplayType)
    If (lstDocs.ListCount < 1) Then
        Call LoadDocList(True, DisplayType)
    End If
End If
End Sub

Private Sub cmdPrev_Click()
If (TransType = "J") Then
    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = CurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    Call LoadApptList(0, False)
ElseIf (TransType = "X") Then
    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = PrevCurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    CurrentIndex = PrevCurrentIndex
    Call LoadClinicalList(False)
ElseIf (TransType = "T") Then
    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = CurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    Call LoadTranscribeList(False)
ElseIf (TransType = "W") Then
    CurrentIndex = PrevCurrentIndex
    PrevCurrentIndex = CurrentIndex - MaxCount
    If (PrevCurrentIndex < 1) Then
        PrevCurrentIndex = 1
    End If
    Call LoadCollectionList(False)
Else
    PageCnt = PageCnt - 1
    If (PageCnt < 0) Then
        PageCnt = TotalPageCnt
    End If
    CurrentIndex = (PageCnt * MaxCount) + 1
    If (CurrentIndex > TotalFound) Or (CurrentIndex < 1) Then
        CurrentIndex = 1
    End If
    Call LoadDocList(False, DisplayType)
End If
End Sub

Private Sub cmdPurge_Click()
Dim i As Integer
Dim k As Integer
Dim IRcv As Long
Dim AllOn As Boolean
Dim iTempId As Long
Dim ApplTemp As ApplicationTemplates
Dim Statement As CPatientStatement
AllOn = False
If (lstDocs.ListCount < 1) Then
    frmEventMsgs.Header = "No Items Present"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (TransType = "R") Then
    If (lstDocs.SelCount > 0) Then
        frmEventMsgs.Header = "Submit All Selected Items ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result <> 2) Then
            Exit Sub
        End If
    ElseIf (lstDocs.SelCount <= 0) Then
        frmEventMsgs.Header = "Submit All Items ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result <> 2) Then
            Exit Sub
        End If
        AllOn = True
        For i = 0 To lstAllEntries.ListCount - 1
            lstAllEntries.Selected(i) = True
        Next i
        For i = 0 To lstDocs.ListCount - 1
            lstDocs.Selected(i) = True
        Next i
    End If
End If
If (lstDocs.SelCount > 0) Then
    frmEventMsgs.Header = "Purge Selected Items"
    If (TransType = "R") And (lstType.ListIndex = 0) Then
        frmEventMsgs.Header = "Set Patient Balances, Do Not Print Statements."
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result <> 4) Then
        lblPrint.Caption = "Purging Selected Items"
        If (TransType = "R") And (lstType.ListIndex = 0) Then
            lblPrint.Caption = "Setting Patient Balances"
        End If
        lblPrint.Visible = True
        DoEvents
        Set ApplTemp = New ApplicationTemplates
        If (AllOn) Then
' used for setting patient statements only
            Set ApplTemp.lstBoxD = lstAllEntries
            Call ApplTemp.ApplPrintFilesSetOnly(SSDateCombo1.DateValue, True, 0, 0)
        Else
            For i = 0 To lstDocs.ListCount - 1
                If (lstDocs.Selected(i)) Then
                    If (TransType = "R") And (lstType.ListIndex = 0) Then
                        Set Statement = New CPatientStatement
                                                
                        'PatientId
                        iTempId = 0
                        k = InStrPS(95, lstAllEntries.List(i), "-")
                        If k > 0 Then
                            iTempId = val(Trim(Mid(lstAllEntries.List(i), k + 1, Len(lstAllEntries.List(i)) - k)))
                        End If
                        PatientId = iTempId
                        Statement.PatId = iTempId
                        
                        'EncounterId
                        iTempId = 0
                        k = InStrPS(95, lstAllEntries.List(i), "/")
                        If k > 0 Then
                            iTempId = val(Trim(Mid(lstAllEntries.List(i), k + 1, Len(lstAllEntries.List(i)) - k)))
                        End If
                        Statement.EncounterId = iTempId
                        Statement.SetPatientBalance
                    Else
                        k = InStrPS(95, lstDocs.List(i), "/")
                        If (k > 0) Then
                            IRcv = val(Trim(Mid(lstDocs.List(i), 95, (k - 1) - 94)))
                        End If
                        Call ApplTemp.ApplPurgeTransaction(IRcv, False, True)
                    End If
                End If
            Next i
        End If
        Set ApplTemp = Nothing
        lblPrint.Caption = "Print"
        lblPrint.Visible = False
        Call LoadDocumentsList(SendOn, TransType, PartyType, False)
        DoEvents
    End If
Else
    frmEventMsgs.Header = "Nothing To Purge"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdQueue_Click()
Dim PatId As Long, p1 As Integer
Dim i As Integer, ErrNum As Integer
Dim ExportOn As Boolean
Dim AllOn As Boolean, QueOn As Boolean
Dim LblOn As Boolean, EnvOn As Boolean
Dim FirstOn As Boolean, LastOn As Boolean
Dim TheLang As String, FileName As String, NewFileName As String
Dim TempFile As String, TxFileName As String
Dim ApplTemp As ApplicationTemplates
Dim EnvelopeFile As String, LabelFile As String
Dim EnvFileName As String, LblFileName As String
Dim FileContent As String
AllOn = False
QueOn = False
LblOn = False
EnvOn = False
ExportOn = False
EnvelopeFile = TemplateDirectory + "RecallTEnv.doc"
LabelFile = TemplateDirectory + "RecallTLbl.doc"
If (lstDocs.ListCount < 1) Then
    frmEventMsgs.Header = "Nothing to Submit"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (lstDocs.SelCount > 0) Then
    AllOn = False
    frmEventMsgs.Header = "Action for Selected Items ?"
    frmEventMsgs.AcceptText = ""
    If (TransType <> "L") Then
        frmEventMsgs.AcceptText = "Queue"
    Else
        frmEventMsgs.AcceptText = "Export"
    End If
    frmEventMsgs.RejectText = "Print"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (TransType = "L") Then
            ExportOn = True
        Else
            QueOn = True
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        QueOn = False
    Else
        Exit Sub
    End If
ElseIf (lstDocs.SelCount <= 0) Then
    AllOn = True
    frmEventMsgs.Header = "Action for All Items ?"
    frmEventMsgs.AcceptText = ""
    If (TransType <> "L") Then
        frmEventMsgs.AcceptText = "Queue"
    Else
        frmEventMsgs.AcceptText = "Export"
    End If
    frmEventMsgs.RejectText = "Print"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (TransType = "L") Then
            ExportOn = True
        Else
            QueOn = True
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        QueOn = False
    Else
        Exit Sub
    End If
    For i = 0 To lstDocs.ListCount - 1
        lstDocs.Selected(i) = True
    Next i
    lstDocs.ListIndex = 0
    For i = 0 To lstAllEntries.ListCount - 1
        lstAllEntries.Selected(i) = True
    Next i
    DoEvents
End If
If (TransType = "L") Then
    For i = 0 To lstDocs.ListCount - 1
        If (lstDocs.Selected(i)) Then
            p1 = InStrPS(95, lstDocs.List(i), "/")
            If (p1 > 0) Then
                p1 = Len(lstDocs.List(i))
            End If
            PatId = GetPatientId(val(Trim(Mid(lstDocs.List(i), 95, p1 - 94))))
            Exit For
        End If
    Next i
    If Not (ExportOn) Then
        frmSelectDialogue.InsurerSelected = ""
        Call frmSelectDialogue.BuildSelectionDialogue("LanguageA")
        frmSelectDialogue.Show 1
        If (frmSelectDialogue.SelectionResult) Then
            TheLang = Trim(UCase(frmSelectDialogue.Selection))
        Else
            Exit Sub
        End If
        If (TheLang = "") Then
            TheLang = "ENGLISH"
        End If
        If (FM.IsFileThere(EnvelopeFile)) Then
            frmEventMsgs.Header = "Do you want to create Envelopes ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                EnvOn = True
            End If
        End If
        If (FM.IsFileThere(LabelFile)) Then
            frmEventMsgs.Header = "Do you want to create Labels ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                LblOn = True
            End If
        End If
    Else
        TheLang = "ENGLISH"
    End If
    If (ExportOn) Then
        lblPrint.Caption = "Exporting In Progress"
    Else
        lblPrint.Caption = "Assembling In Progress"
    End If
    lblPrint.Visible = True
    DoEvents
    Set ApplTemp = New ApplicationTemplates
    Set ApplTemp.lstBox = lstPrint
    Set ApplTemp.lstBoxA = lstDocs
    If (ExportOn) Then
        Set ApplTemp.lstBoxD = lstAllEntries
        ErrNum = ApplTemp.ApplDoExportRecallFile(AllOn)
    Else
        If (TransType = "L") Then
            Set ApplTemp.lstBoxD = lstAllEntries
        End If
        ErrNum = ApplTemp.ApplTransmitFiles(SSDateCombo1.DateValue, AllOn, TransType, "", "", DisplayType, FileName, TempFile, TxFileName, TheLang, QueOn, ResourceLocId, ResourceId)
        
        Dim printrecall As Boolean
        
        If Not Trim(FileName) = "" Then
            Dim astrSplitItems() As String
            Dim intx As Integer
            astrSplitItems = Split(FileName, "@")
            NewFileName = astrSplitItems(UBound(astrSplitItems))
            FileContent = ""
            For intx = 0 To UBound(astrSplitItems) - 1
                FileContent = FileContent & "@" & astrSplitItems(intx)
                printrecall = True
            Next
        End If
        
        If Trim(FileContent) = "" Then
            ErrNum = -104
        End If
    End If
    
    If (ErrNum = 0) Then
        If (QueOn) Then
            lblPrint.Caption = "Queuing In Progress"
            lblPrint.Visible = True
            DoEvents
            If (LblOn) Then
                LblFileName = NewFileName
                Call ReplaceCharacters(LblFileName, "Recall", "RecallLbl")
                If (FM.IsFileThere(LblFileName)) Then
                    FM.Kill LblFileName
                End If
                If (FM.IsFileThere(NewFileName)) Then
                    FM.FileCopy NewFileName, LblFileName
                Else
                    LblOn = False
                End If
            End If
            If (EnvOn) Then
               EnvFileName = NewFileName
                Call ReplaceCharacters(EnvFileName, "Recall", "RecallEnv")
                If (FM.IsFileThere(EnvFileName)) Then
                    FM.Kill EnvFileName
                End If
                If (FM.IsFileThere(NewFileName)) Then
                    FM.FileCopy NewFileName, EnvFileName
                Else
                    LblOn = False
                End If
            End If
            Call SetTemplateLanguage(TheLang, TempFile)
            Call PostToJournal(PatId, TheLang, "Recall")
            If (LblOn) Then
                Call PostToJournal(PatId, "", "RecallTLbl")
            End If
            If (EnvOn) Then
                Call PostToJournal(PatId, "", "RecallTEnv")
            End If
        ElseIf Not (ExportOn) Then
            lblPrint.Caption = "Printing In Progress"
            lblPrint.Visible = True
            DoEvents
            If (LblOn) Then
                LblFileName = NewFileName
                Call ReplaceCharacters(LblFileName, "Recall", "RecallLbl")
                If (FM.IsFileThere(LblFileName)) Then
                    FM.Kill LblFileName
                End If
                If (FM.IsFileThere(NewFileName)) Then
                    FM.FileCopy NewFileName, LblFileName
                End If
            End If
            If (EnvOn) Then
                EnvFileName = NewFileName
                Call ReplaceCharacters(EnvFileName, "Recall", "RecallEnv")
                If (FM.IsFileThere(EnvFileName)) Then
                    FM.Kill EnvFileName
                End If
                 If (FM.IsFileThere(NewFileName)) Then
                    FM.FileCopy NewFileName, EnvFileName
                 End If
            End If
            Call SetTemplateLanguage(TheLang, TempFile)
            
            If printrecall = True And Trim(FileContent) <> "" Then
                Call PrintTemplate(FileContent & "@" & NewFileName, TempFile, True, False, 1, "", "", 0, False)
            End If
           
            If (LblOn) Then
                lblPrint.Caption = "Printing Labels"
                DoEvents
                LblFileName = FileContent & "@" & LblFileName
                If printrecall = True And Trim(FileContent) <> "" Then
                    Call PrintTemplate(LblFileName, LabelFile, True, False, 1, "", "", 0, False)
                End If
            End If
            If (EnvOn) Then
                lblPrint.Caption = "Printing Envelopes"
                DoEvents
                EnvFileName = FileContent & "@" & EnvFileName
                If printrecall = True And Trim(FileContent) <> "" Then
                    Call PrintTemplate(EnvFileName, EnvelopeFile, True, False, 1, "", "", 0, False)
                End If
            End If
        End If
    End If
    lblPrint.Visible = False
    Set ApplTemp = Nothing
End If
frmEventMsgs.Header = ""
If (ErrNum = -104) Then
    frmEventMsgs.Header = "Nothing to transmit"
ElseIf ErrNum = -105 Then
    frmEventMsgs.Header = "Export file already exists for today.  You can't export recalls twice in one day."
ElseIf (ErrNum <> 0) Then
    frmEventMsgs.Header = "General Connectivity Error"
Else
    If (QueOn) Then
        frmEventMsgs.Header = "Recalls queued"
    ElseIf (ExportOn) Then
        frmEventMsgs.Header = "Recalls exported"
    Else
        frmEventMsgs.Header = "Recalls printed"
    End If
End If
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
Call LoadDocumentsList(SendOn, TransType, PartyType, False)
End Sub

Private Sub cmdSend_Click()
cmdSend.Enabled = False
DoEvents


Dim UserTypeOn As Integer
Dim i As Integer
Dim ErrNum As Integer
Dim ExportOn As Boolean
Dim IgnoreDays As Boolean
Dim SetOnly As Boolean
Dim AllOn As Boolean
Dim MyExt As String
Dim TheLang As String
Dim Temp As String
Dim TheType As String
Dim TxName As String
Dim TheFacility As String
Dim FileName As String
Dim TempFile As String
Dim TxFileName As String
Dim Ax As String
Dim Bx As String
Dim Cx As String
Dim Dx As String
Dim Ex As String
Dim Fx As String
Dim ApplTemp As ApplicationTemplates
Dim TempInt As Integer
Dim TempId As Long
Dim iListTotal As Long
Dim Statement As CPatientStatement
TheLang = ""
AllOn = False
SetOnly = False
ExportOn = False
IgnoreDays = False
If (lstDocs.ListCount < 1) Then
    frmEventMsgs.Header = "No Items Present"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    GoTo ExitSub
End If
If (lstDocs.SelCount > 0) Then
    frmEventMsgs.Header = "Submit All Selected Items ?"
    For i = 0 To lstDocs.ListCount - 1
        If lstDocs.Selected(i) Then
            lstAllEntries.Selected(i + PageCnt * MaxCount) = True
        End If
    Next i
Else
    frmEventMsgs.Header = "Submit All Items ?"
    AllOn = True
    For i = 0 To lstAllEntries.ListCount - 1
        lstAllEntries.Selected(i) = True
    Next i
End If
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Submit"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
If TransType = "R" Then
    If lstType.ListIndex = 1 Then
        frmEventMsgs.AcceptText = "Export"
        frmEventMsgs.Other0Text = "Export Any"
    End If
    If lstType.ListIndex <> 0 Then
        frmEventMsgs.RejectText = "Print"
    End If
End If
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 4) Then
    If frmEventMsgs.Header = "Submit All Items ?" Then
        For i = 0 To lstAllEntries.ListCount - 1
            lstAllEntries.Selected(i) = False
        Next i
    End If
    GoTo ExitSub
ElseIf (frmEventMsgs.Result = 1) Then
    ExportOn = True
ElseIf (frmEventMsgs.Result = 3) Then
    ExportOn = True
    IgnoreDays = True
End If

If AllOn Then
    For i = 0 To lstDocs.ListCount - 1
        lstDocs.Selected(i) = True
    Next i
End If

TxName = ""
TheType = ""
TheFacility = ""
If (TransType = "R") Then
    UserTypeOn = 0
    If CheckConfigCollection("OPTICALMEDICALON") = "T" Then
        UserTypeOn = 1 ' Medical
        If (UserLogin.sType = "Q") Or (UserLogin.sType = "U") Or (UserLogin.sType = "Y") Then
            UserTypeOn = 2 ' Optical
        End If
    End If
End If
ErrNum = 0
If (TransType = "R") Then
    If (TheType = "P") Then
        ErrNum = SendViaEmail("", "P", "")
    End If
ElseIf (TransType <> "R") And (TransType <> "L") Then
    If (lstType.ListIndex = 0) Then
        ErrNum = 0
    ElseIf (lstType.ListIndex = 1) Then
        ErrNum = SendViaFax("", "P", "")
    End If
End If

If TransType = "R" And Not ExportOn Then
    If lstType.List(lstType.ListIndex) = "Monthly Statements" Then
        If ProcessStatementList(True, True, IgnoreDays) Then
            'It worked
        End If
    ElseIf lstType.List(lstType.ListIndex) = "Statements" Then
        If ProcessStatementList(False, True, False) Then
            'It worked
        End If
    End If
End If

If (ErrNum = 0) Then
    DoEvents
    Set ApplTemp = New ApplicationTemplates
    Set ApplTemp.lstBox = lstPrint
    Set ApplTemp.lstBoxA = lstDocs
    
    If (TransType = "L") Then
        lblPrint.Caption = "Printing In Progress"
        lblPrint.Visible = True
        DoEvents
        ErrNum = ApplTemp.ApplTransmitRecalls(SSDateCombo1.DateValue, AllOn, "L")
        lblPrint.Visible = False
    ElseIf (TransType = "R") Then
        If (lstType.ListIndex = 0) Then 'Patient Statement handled above
            ' NO OP
        ElseIf (lstType.ListIndex = 1) Then ' Monthly Statements moved up (except for export)
            If ExportOn Then
                Set ApplTemp.lstBoxA = lstAllEntries
                Set ApplTemp.lstBoxD = lstAllEntries
                ErrNum = ApplTemp.ApplPrintFiles(SSDateCombo1.DateValue, AllOn, TransType, "m", "", "", FileName, TempFile, TxFileName, ResourceLocId, ResourceId, ExportOn, IgnoreDays, UserTypeOn, lblPrint)
            Else
                ' NO OP
            End If
        Else
            ErrNum = -104
        End If
    Else
        If (lstType.ListIndex = 0) Then
            ErrNum = ApplTemp.ApplTransmitLetters(AllOn, TransType, "E")
        ElseIf (lstType.ListIndex = 1) Then
            ErrNum = ApplTemp.ApplTransmitLetters(AllOn, TransType, "F")
        ElseIf (lstType.ListIndex = 2) Then
            ErrNum = ApplTemp.ApplPrintLetters(AllOn, TransType, "P")
        Else
            ErrNum = -104
        End If
    End If
    Set ApplTemp = Nothing
End If
If (ErrNum = -1) Then
    frmEventMsgs.Header = "Incorrect Configuration Parameters"
ElseIf (ErrNum = -2) Then
    frmEventMsgs.Header = "Bad To Address"
ElseIf (ErrNum = -3) Then
    frmEventMsgs.Header = "Cannot attach the file"
ElseIf (ErrNum = -4) Then
    frmEventMsgs.Header = "Could not connect to SMTP Server"
ElseIf (ErrNum = -5) Then
    frmEventMsgs.Header = "No From Address"
ElseIf (ErrNum = -6) Then
    frmEventMsgs.Header = "Could not write message to SMTP Server"
ElseIf (ErrNum = -7) Then
    frmEventMsgs.Header = "Transmission Failed"
ElseIf (ErrNum = -100) Then
    frmEventMsgs.Header = "Access not available"
ElseIf (ErrNum = -102) Then
    frmEventMsgs.Header = "Feature Coming Soon"
ElseIf (ErrNum = -103) Then
    frmEventMsgs.Header = "No Fax available"
ElseIf (ErrNum = -104) Then
    frmEventMsgs.Header = "Nothing to transmit"
ElseIf (ErrNum <> 0 And ErrNum <> -999) Then
    frmEventMsgs.Header = "General Connectivity Error"
Else
    If (ErrNum <> -999) Then
        If (TransType = "R") Then
            frmEventMsgs.Header = FileName + " Processed"
        ElseIf (TransType = "L") Then
            frmEventMsgs.Header = "Recalls printed"
        Else
            frmEventMsgs.Header = "Done"
        End If
    End If
End If
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If ((TransType = "J") Or (TransType = "C")) And Not (SendOn) Then
    Call LoadApptList(0, True)
Else
    Call LoadDocList(True, PartyType)
End If

ExitSub:
cmdSend.Enabled = True
End Sub

Public Function LoadDocumentsList(IsSendOn As Boolean, TheType As String, PType As String, InitOn As Boolean) As Boolean
Dim ADate As String
Dim DocType As String
Dim ApplList As ApplicationAIList
LoadDocumentsList = False
ActivityDate = ""
PageCnt = 0
TheStatus = ""
CollectionAction = ""
CollectionLetter = ""
SendOn = IsSendOn
TransType = TheType
TransTypeId = 0
DisplayType = PType
Call FormatTodaysDate(ADate, False)
PartyType = PType
lblStatus.Visible = False
lstStatus.Visible = False
Label2.Visible = True
lstDr.Visible = True
lblTests.Visible = False
lstTests.Visible = False
cmdSend.Visible = SendOn
cmdPatient.Visible = Not SendOn
cmdQueue.Visible = False
cmdAll.Visible = False
cmdLetters.Visible = False
cmdLetters.Text = "Letters"
cmdPurge.Visible = False
cmdLegend.Visible = False
cmdDisplay.Visible = False
If (InStrPS("JR^LCYXT", TransType) = 0) And (Not (SendOn)) Then
    cmdAll.Visible = True
End If
lblDate.Visible = False
SSDateCombo1.Visible = False
If (TransType = "L") Or (TransType = "J") Then
    If Not (SendOn) Then
        If (TransType = "L") Then
            cmdQueue.Visible = True
        End If
        lblDate.Visible = True
        SSDateCombo1.Visible = True
    End If
Else
    If (InStrPS("R^CXT", TransType) = 0) And Not (SendOn) Then
        cmdLetters.Visible = True
        cmdLegend.Visible = True
    End If
End If
DocType = ""
lstType.Clear
lstType.Visible = False
lstType.ListIndex = -1
' Actions depend on the naming of these lstTypes. Don't change without altering business logic choices.
If (InStrPS("R^L", TransType) = 0) Then
    lstType.AddItem "E-Mail"
    lstType.AddItem "Fax"
    lstType.AddItem "Print"
Else
    lstType.AddItem "Statements"
    lstType.AddItem "Monthly Statements"
End If
If (TransType = "F") Then
    DocType = "Referral Letters"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
    lblStatus.Visible = True
    lstStatus.Visible = True
ElseIf (TransType = "S") Then
    DocType = "Consult Letters"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
    lblStatus.Visible = True
    lstStatus.Visible = True
ElseIf (TransType = "Y") Then
    DocType = "Facility Admission"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
    lblStatus.Visible = True
    lstStatus.Visible = True
ElseIf (TransType = "M") Then
    DocType = "Misc Letters"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
ElseIf (TransType = "X") Then
    DocType = "Clinical Follow Up"
    lstType.Visible = SendOn
    lblStatus.Visible = True
    lstStatus.Visible = True
    lblTests.Visible = True
    lstTests.Visible = True
    cmdDisplay.Visible = True
    cmdPrev.Visible = False
    cmdNext.Visible = False
ElseIf (TransType = "R") Then
    DocType = "Claims"
    lstType.Visible = True
ElseIf (TransType = "^") Then
    DocType = "Claims Sent"
    lstType.Visible = True
ElseIf (TransType = "Q") Then
    DocType = "Chart Requests"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
ElseIf (TransType = "[") Then
    DocType = "Appeal Letter"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
ElseIf (TransType = "]") Then
    DocType = "Denial Letter"
    cmdLetters.Text = DocType
    lstType.Visible = SendOn
ElseIf (TransType = "T") Then
    DocType = "Transcriptions"
    lstType.Visible = False
ElseIf (TransType = "W") Then
    DocType = "Collection Letters"
    lstType.Visible = False
    cmdLegend.Visible = False
    cmdLetters.Visible = False
ElseIf (TransType = "L") Then
    DocType = "Recalls"
    lblStatus.Visible = True
    lstStatus.Visible = True
ElseIf (TransType = "J") Then
    DocType = "Confirmations"
    lblStatus.Visible = True
    lstStatus.Visible = True
ElseIf (TransType = "C") Then
    DocType = "Incomplete Charts"
Else
    Exit Function
End If
If (InitOn) Then
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstDr
    Call ApplList.ApplLoadStaff(False)
    Set ApplList = Nothing
    ResourceId = -1
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstLoc
    Call ApplList.ApplLoadLocation(False, True)
    Set ApplList = Nothing
    ResourceLocId = dbPracticeId
    If (TransType = "L") Or (TransType = "R") Or (TransType = "S") Or (TransType = "W") Or (TransType = "T") Then
        ResourceLocId = -1
    End If
    Set ApplList = New ApplicationAIList
    If (TransType = "X") Then
        Call ApplList.ApplGetCodesbyList("CLINICALFOLLOWUPSTATUS", False, True, lstStatus)
        Call ApplList.ApplGetCodesbyTest(lstTests)
    ElseIf (TransType = "S") Or (TransType = "F") Or (TransType = "Y") Then
        Call ApplList.ApplGetCodesbyList("LETTERSTATUS", False, True, lstStatus)
    Else
        Call ApplList.ApplGetCodesbyList("CONFIRMSTATUS", False, True, lstStatus)
    End If
    Set ApplList = Nothing
End If
frmEventMsgs.Header = ""
If (TransType = "J") And Not (SendOn) Then
    Call LoadApptList(0, True)
ElseIf (TransType = "C") And Not (SendOn) Then
    Call LoadApptList(0, True)
ElseIf (TransType = "X") And Not (SendOn) Then
'    Call LoadClinicalList(True)
ElseIf (TransType = "T") And Not (SendOn) Then
    Call LoadTranscribeList(True)
ElseIf (TransType = "W") And Not (SendOn) Then
    Call LoadCollectionList(True)
Else
    Call LoadDocList(True, PType)
End If
LoadDocumentsList = True
If (TransType = "R") Then
    TriggerOn = True
    If (lstType.ListIndex < 0) Then
        CurrentIndex = 1
        If (PartyType = "S") Then
            lstType.ListIndex = 0
        ElseIf (PartyType = "s") Then
            lstType.ListIndex = 1
        Else
            lstType.ListIndex = 0
        End If
    End If
ElseIf (InStrPS("R^LCXTW", TransType) = 0) Then
'    lstType.ListIndex = 0
    CurrentIndex = 1
End If
If (TransType = "^") Then
    lstType.Visible = False
End If
Label1.Caption = "Review " + DocType
If (SendOn) Then
    Label1.Caption = "Submit " + DocType
    If (TransType <> "L") Then
        cmdPurge.Text = "Purge Selected Items"
        cmdPurge.Visible = True
        If (TransType = "R") Then
            If (lstType.ListIndex = 0) Then
                cmdPurge.Text = "Set Patient Balance"
            Else
                cmdPurge.Visible = False
            End If
        End If
    End If
ElseIf (TransType = "T") Then
    lstLoc.ListIndex = 0
ElseIf (TransType = "L") Then
    lblStatus.Visible = False
    lstStatus.Visible = False
End If
End Function

Private Sub LoadDocList(Init As Boolean, IType As String)
Dim z As Integer
Dim Temp As String
Dim RvwWdw As String
Dim ApplList As ApplicationAIList
cmdPrev.Visible = False
cmdNext.Visible = False
If (Init) Then
    CurrentIndex = 1
    PageCnt = 0
End If
If CheckConfigCollection("MULTIOFFICE") = "" Then
    Temp = "F"
    ResourceLocId = -1
End If
lstDocs.Clear
DateNow = SSDateCombo1.DateValue
Call FormatTodaysDate(DateNow, False)
Set ApplList = New ApplicationAIList
Set ApplList.ApplList = lstDocs
ApplList.ApplTransactionType = TransType
ApplList.ApplDate = DateNow
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplSendOn = False
ApplList.ApplPatientType = False
ApplList.ApplTransactionRef = ""
ApplList.ApplStartAt = CurrentIndex
ApplList.ApplReturnCount = 25
ApplList.ApplTransactionStatus = "P"
ApplList.ApplTransactionAction = "P"
If (TransType = "L") Then
    ApplList.ApplTransactionAction = "-"
    ApplList.ApplTransactionStatus = DisplayType
ElseIf (TransType = "J") Then
    ApplList.ApplTransactionAction = "A"
    ApplList.ApplTransactionStatus = DisplayType
ElseIf (TransType = "R") Then
    If (IType = "S") Or (IType = "s") Then
        ApplList.ApplTransactionRef = "P"
        ApplList.ApplTransactionAction = "P"
        ApplList.ApplTransactionStatus = "P"
        If (IType = "s") Then
            ApplList.ApplPatientType = True
            ApplList.ApplSendOn = True
            ApplList.ApplDate = ""
            ApplList.ApplTransactionAction = "P"
            ApplList.ApplTransactionStatus = "PS"
        End If
    ElseIf (IType = "P") Then
        ApplList.ApplTransactionRef = "I"
        ApplList.ApplTransactionAction = "P"
        ApplList.ApplTransactionStatus = "PB"
    Else
        ApplList.ApplTransactionRef = "I"
        ApplList.ApplTransactionAction = "B"
        ApplList.ApplTransactionStatus = "PB"
    End If
ElseIf (TransType = "^") Then
    ApplList.ApplTransactionType = "R"
    ApplList.ApplTransactionStatus = "S"
    ApplList.ApplTransactionAction = ""
Else
    ApplList.ApplTransactionStatus = "B"
    ApplList.ApplTransactionAction = ""
    If (lstType.ListIndex = 0) Then
        ApplList.ApplTransactionAction = "F"
    ElseIf (lstType.ListIndex = 1) Then
        ApplList.ApplTransactionAction = "P-"
    End If
    If Not SendOn Then
        ApplList.ApplTransactionStatus = "P"
        ApplList.ApplTransactionAction = ""
    End If
End If
If (TransType = "S") Or (TransType = "F") Or (TransType = "Y") Then
    ApplList.ApplConfirmStat = TheStatus
End If

ApplList.ApplResName = ""
If (TransType = "L") Then
    If (lstDr.ListIndex > 0) Then
        ApplList.ApplResName = Trim(Left(lstDr.List(lstDr.ListIndex), 16))
    End If
End If

If (InStrPS("R^L", TransType) = 0) Then
    TotalFound = ApplList.ApplRetrieveTransLtrsList(False)
    CurrentIndex = ApplList.ApplReturnCount
Else
    ApplList.ApplStartAt = 1
    ApplList.ApplReturnCount = 0
    lstAllEntries.Clear
    Set ApplList.ApplList = lstAllEntries
    If (IType = "s") Then
        TotalFound = ApplList.ApplRetrieveTransApptsList(False, "m")
    ElseIf (IType = "S") Then
        TotalFound = ApplList.ApplRetrieveTransApptsList(False, "S")
    Else
        Dim RcvrId As Long
        Select Case Label1.Caption
        Case "Review Recalls"
            If lstDr.ListIndex > 0 Then
                RcvrId = val(myTrim(lstDr.List(lstDr.ListIndex), 1))
            End If
        End Select
        TotalFound = ApplList.ApplRetrieveTransApptsList(False, "", RcvrId)
    End If
    lstDocs.Clear
    For z = CurrentIndex To CurrentIndex + 24
        If (z <= lstAllEntries.ListCount) Then
            lstDocs.AddItem lstAllEntries.List(z - 1)
        End If
    Next z
    CurrentIndex = CurrentIndex + 24
End If
TotalPageCnt = Int(TotalFound / MaxCount)
If (TotalFound > 25) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
End If
Set ApplList = Nothing
lstDocs.ListIndex = -1
If (SendOn) Then
    If (TransType <> "L") Then
        cmdPurge.Text = "Purge Selected Items"
        cmdPurge.Visible = True
        If (TransType = "R") Then
            If (lstType.ListIndex = 0) Then
                cmdPurge.Text = "Set Patient Balance"
            Else
                cmdPurge.Visible = False
            End If
        End If
    End If
End If
End Sub
Private Sub LoadApptList(PatId As Long, Init As Boolean)
Dim DateNow As String
Dim ApplList As ApplicationAIList
If (Init) Then
    CurrentIndex = 1
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
End If
DateNow = ""
Call FormatTodaysDate(DateNow, False)
DateNow = SSDateCombo1.DateValue
Call FormatTodaysDate(DateNow, False)
cmdNext.Visible = False
cmdPrev.Visible = False
lstDocs.Clear
Set ApplList = New ApplicationAIList
ApplList.ApplDate = DateNow
ApplList.ApplConfirmStat = ""
If Not (SendOn) Then
    ApplList.ApplConfirmStat = TheStatus
End If
ApplList.ApplTransactionStatus = ""
If (TransType = "C") Then
    ApplList.ApplTransactionStatus = "C"
End If
ApplList.ApplPatId = PatId
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplStartAt = CurrentIndex
MidCurrentIndex = CurrentIndex
ApplList.ApplReturnCount = 0
Set ApplList.ApplList = lstDocs
TotalFound = ApplList.ApplRetrieveReviewConfList
TotalPageCnt = Int(TotalFound / MaxCount)
CurrentIndex = ApplList.ApplReturnCount
cmdPrev.Visible = False
cmdNext.Visible = False
Set ApplList = Nothing
lstDocs.ListIndex = -1
End Sub

Private Sub LoadClinicalList(Init As Boolean)
Dim DateNow As String
Dim ApplList As ApplicationAIList
If (Init) Then
    CurrentIndex = 1
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
End If
DateNow = ""
Call FormatTodaysDate(DateNow, False)
DateNow = SSDateCombo1.DateValue
Call FormatTodaysDate(DateNow, False)
cmdNext.Visible = False
cmdPrev.Visible = False
lstDocs.Clear
Set ApplList = New ApplicationAIList
ApplList.ApplDate = DateNow
ApplList.ApplConfirmStatus = TheStatus
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplTransactionRef = TheTest
ApplList.ApplStartAt = CurrentIndex
MidCurrentIndex = CurrentIndex
ApplList.ApplReturnCount = 25
Set ApplList.ApplList = lstDocs
TotalFound = ApplList.ApplRetrieveReviewClinicalList
TotalPageCnt = Int(TotalFound / MaxCount)
CurrentIndex = ApplList.ApplReturnCount
If (TotalFound > 25) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
Else
    cmdPrev.Visible = False
    cmdNext.Visible = False
End If
Set ApplList = Nothing
lstDocs.ListIndex = -1
End Sub

Private Sub LoadTranscribeList(Init As Boolean)
Dim DateNow As String
Dim ApplList As ApplicationAIList
If (Init) Then
    CurrentIndex = 1
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
End If
DateNow = ""
Call FormatTodaysDate(DateNow, False)
DateNow = SSDateCombo1.DateValue
Call FormatTodaysDate(DateNow, False)
cmdNext.Visible = False
cmdPrev.Visible = False
lstDocs.Clear
Set ApplList = New ApplicationAIList
ApplList.ApplDate = DateNow
ApplList.ApplConfirmStatus = TheStatus
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplTransactionRef = TheTest
ApplList.ApplStartAt = CurrentIndex
MidCurrentIndex = CurrentIndex
ApplList.ApplReturnCount = 25
Set ApplList.ApplList = lstDocs
TotalFound = ApplList.ApplRetrieveReviewTranscribeList
TotalPageCnt = Int(TotalFound / MaxCount)
CurrentIndex = ApplList.ApplReturnCount
If (TotalFound > 25) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
Else
    cmdPrev.Visible = False
    cmdNext.Visible = False
End If
Set ApplList = Nothing
lstDocs.ListIndex = -1
End Sub

Private Sub LoadCollectionList(Init As Boolean)
Dim ADate As String
Dim DateNow As String
Dim ApplList As ApplicationAIList
If (Init) Then
    CurrentIndex = 1
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
End If
DateNow = ""
Call FormatTodaysDate(DateNow, False)
DateNow = SSDateCombo1.DateValue
Call FormatTodaysDate(DateNow, False)
Call AdjustDate(DateNow, -91, ADate)
DateNow = ADate
cmdNext.Visible = False
cmdPrev.Visible = False
lstDocs.Clear
Set ApplList = New ApplicationAIList
ApplList.ApplDate = DateNow
ApplList.ApplConfirmStatus = TheStatus
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplTransactionRef = TheTest
ApplList.ApplStartAt = CurrentIndex
MidCurrentIndex = CurrentIndex
ApplList.ApplReturnCount = 25
Set ApplList.ApplList = lstDocs
TotalFound = ApplList.ApplRetrieveReviewCollectionList
TotalPageCnt = Int(TotalFound / MaxCount)
CurrentIndex = ApplList.ApplReturnCount
If (TotalFound > 25) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
Else
    cmdPrev.Visible = False
    cmdNext.Visible = False
End If
Set ApplList = Nothing
lstDocs.ListIndex = -1
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmReviewDocs.BorderStyle = 1
    frmReviewDocs.ClipControls = True
    frmReviewDocs.Caption = Mid(frmReviewDocs.Name, 4, Len(frmReviewDocs.Name) - 3)
    frmReviewDocs.AutoRedraw = True
    frmReviewDocs.Refresh
End If
End Sub

Private Sub lstDocs_dblClick()
Dim w As Integer
Dim q As Integer, u As Integer
Dim Proceed As Boolean
Dim NoteId As Long
Dim ApptId As Long, TransId As Long
Dim PatId As Long, ClnId As Long
Dim TheRef As String, TheAction As String, Temp As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim ApplSch As ApplicationScheduler
Dim ApplLetters As Letters
If (lstDocs.ListIndex >= 0) Then
    If (lstDocs.Selected(lstDocs.ListIndex)) Then
        If (TransType = "X") Then
            frmEventMsgs.Header = "Action ?"
            frmEventMsgs.AcceptText = "Go To Clinical Data"
            frmEventMsgs.RejectText = "Change Status"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "/")
                If (u > 0) Then
                    ApptId = val(Mid(lstDocs.List(lstDocs.ListIndex), 95, (u - 1) - 94))
                    q = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
                    If (q > 0) Then
                        PatId = val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, (q - 1) - u))
                    End If
                End If
                If (ApptId > 0) And (PatId > 0) Then
                    frmReviewAppts.StartDate = ""
                    If (frmReviewAppts.LoadApptsList(PatId, ApptId, True)) Then
                        frmReviewAppts.Show 1
                    End If
                End If
            ElseIf (frmEventMsgs.Result = 2) Then
                Call frmSelectDialogue.BuildSelectionDialogue("ClinicalFollowUpStatus")
                frmSelectDialogue.Show 1
                If (frmSelectDialogue.SelectionResult) Then
                    If (Trim(frmSelectDialogue.Selection) <> "") Then
                        Temp = Left(frmSelectDialogue.Selection, 1)
                        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
                        If (u > 0) Then
                            ClnId = val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
                            If (ClnId > 0) Then
                                Set ApplList = New ApplicationAIList
                                Call ApplList.ApplSetFollowUpStatus(ClnId, Temp)
                                Set ApplList = Nothing
                                Call LoadClinicalList(False)
                            End If
                        End If
                    End If
                End If
            End If
        ElseIf (TransType = "T") Then
            u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "/")
            If (u > 0) Then
                ApptId = val(Mid(lstDocs.List(lstDocs.ListIndex), 95, (u - 1) - 94))
                q = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
                If (q > 0) Then
                    PatId = val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, (q - 1) - u))
                End If
                If (ApptId > 0) And (PatId > 0) Then
                    u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
                    If (u > 0) Then
                        NoteId = val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
                        Temp = Trim(Mid(lstDocs.List(lstDocs.ListIndex), 12, 26))
                        If (NoteId > 0) Then
                            frmTranscribe.NoteId = NoteId
                            frmTranscribe.PatientId = PatId
                            frmTranscribe.AppointmentId = ApptId
                            frmTranscribe.PatName = Temp
                            frmTranscribe.Show 1
                            Call LoadTranscribeList(False)
                        End If
                    End If
                End If
            End If
        ElseIf (TransType = "W") Then
            ApptId = val(Mid(lstDocs.List(lstDocs.ListIndex), 95, Len(lstDocs.List(lstDocs.ListIndex)) - 94))
            If (ApptId > 0) Then
                frmEventMsgs.Header = "Action ?"
                frmEventMsgs.AcceptText = "Edit"
                frmEventMsgs.RejectText = "Delete"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = "List"
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 1) Then
                    If (SelectCollectionLetter(CollectionLetter)) Then
                        Call MaintainLetter(lstDocs.ListIndex, "W", "E", False)
                    End If
                ElseIf (frmEventMsgs.Result = 2) Then
                    Call MaintainLetter(lstDocs.ListIndex, "W", "D", False)
                ElseIf (frmEventMsgs.Result = 3) Then
                    u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "/")
                    If (u > 0) Then
                        CollectionAction = "V"
                        ApptId = val(Mid(lstDocs.List(lstDocs.ListIndex), 95, (u - 1) - 94))
                        PatId = val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
                        Call ViewCollectionLetter(ApptId, PatId)
                    End If
                End If
            End If
        ElseIf (TransType <> "C") And (TransType <> "^") And (lstDocs.ListIndex >= 0) Then
            If (InStrPS(Label1.Caption, "Review Appointments") <> 0) Then
                If (TransType = "M") Then
                    TheRef = "MiscellaneousLetters"
                ElseIf (TransType = "S") Then
                    TheRef = "ConsultationLetters"
                ElseIf (TransType = "F") Then
                    TheRef = "ReferralLetters"
                ElseIf (TransType = "X") Then
                    TheRef = "ExternalLetters"
                ElseIf (TransType = "Y") Then
                    If (DoFacilityAdmin(lstDocs.ListIndex)) Then
                        Call LoadDocumentsList(SendOn, TransType, PartyType, False)
                    End If
                    Exit Sub
                Else
                    Exit Sub
                End If
                Call frmSelectDialogue.BuildSelectionDialogue(TheRef)
                frmSelectDialogue.Show 1
                If (Trim(frmSelectDialogue.Selection) <> "") Then
                    Set ApplLetters = New Letters
                    ApptId = ApplLetters.ApplLettersApptId(lstDocs.List(lstDocs.ListIndex))
                    PatId = ApplLetters.ApplLettersPatientId(lstDocs.List(lstDocs.ListIndex))
                    Proceed = ApplLetters.ApplPostLetter(PatId, ApptId, Trim(frmSelectDialogue.Selection), "", 0, TransType)
                    Set ApplLetters = Nothing
                    If (Proceed) Then
                        Call LoadDocumentsList(SendOn, TransType, PartyType, False)
                    Else
                        frmEventMsgs.Header = "Posting Letter Not Done"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                    End If
                End If
            ElseIf (TransType <> "L") And (TransType <> "J") Then
                If Not (SendOn) Then
                    frmEventMsgs.Header = "Action ?"
                    frmEventMsgs.AcceptText = "Send"
                    frmEventMsgs.RejectText = "Edit"
                    frmEventMsgs.CancelText = "Cancel"
                    frmEventMsgs.Other0Text = "Delete"
                    frmEventMsgs.Other1Text = "Status"
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    If (InStrPS("SFQX", TransType) <> 0) Then
                        frmEventMsgs.Other2Text = "Select Recipient"
                    ElseIf (TransType = "Y") Then
                        frmEventMsgs.Other4Text = "Edit Surgical Request"
                    End If
                    If (TransType <> "J") Then
                        frmEventMsgs.Show 1
                    Else
                        frmEventMsgs.Result = 5
                    End If
                    If (frmEventMsgs.Result = 1) Then
                        If (SendLetter(lstDocs.ListIndex, TransType)) Then
                            lstDocs.RemoveItem lstDocs.ListIndex
                        End If
                    ElseIf (frmEventMsgs.Result = 2) Then
                        Call MaintainLetter(lstDocs.ListIndex, TransType, "E", False)
                    ElseIf (frmEventMsgs.Result = 3) Then
                        Call MaintainLetter(lstDocs.ListIndex, TransType, "D", False)
                    ElseIf (frmEventMsgs.Result = 5) Then
                        Call MaintainStatus(lstDocs, TransType)
                    ElseIf (frmEventMsgs.Result = 6) Then
                        Call SelectParty(lstDocs.ListIndex, TransType)
                    ElseIf (frmEventMsgs.Result = 7) Then
                        Call SelectSurgicalRequest(lstDocs.ListIndex, TransType)
                    ElseIf (frmEventMsgs.Result = 8) Then
                        Call SelectSurgical(lstDocs.ListIndex, TransType)
                        Call LoadDocumentsList(SendOn, TransType, PartyType, False)
                    End If
                Else
                    If (InStrPS("SFQYX", TransType) <> 0) Then
                        If (SendLetter(lstDocs.ListIndex, TransType)) Then
                            lstDocs.RemoveItem lstDocs.ListIndex
                        End If
                    End If
                End If
            Else
                If (TransType = "J") Then
                    Call frmSelectDialogue.BuildSelectionDialogue("CONFIRMSTATUS")
                    frmSelectDialogue.Show 1
                    If (Trim(frmSelectDialogue.Selection) <> "") Then
                        q = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "/")
                        If (q > 95) Then
                            ApptId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 96, (q - 1) - 95)))
                            If (ApptId > 0) Then
                                Set ApplSch = New ApplicationScheduler
                                Call ApplSch.ApplUpdateConfirmStatus(ApptId, Left(frmSelectDialogue.Selection, 1), UserLogin.iId)
                                Set ApplSch = Nothing
                                Temp = lstDocs.List(lstDocs.ListIndex)
                                Mid(Temp, 61, 1) = Left(frmSelectDialogue.Selection, 1)
                                lstDocs.List(lstDocs.ListIndex) = Temp
                            End If
                        End If
                    End If
                End If
            End If
        ElseIf (TransType = "C") And (lstDocs.ListIndex >= 0) Then
            If (InStrPS(Label1.Caption, "Review Incomplete Charts") <> 0) Then
                ApptId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 95, 5)))
                PatId = val(Trim(Mid(lstDocs.List(lstDocs.ListIndex), 101, 5)))
                If (frmReviewAppts.LoadApptsList(PatId, ApptId, True)) Then
                    frmReviewAppts.Show 1
                    frmEventMsgs.Header = "Is Chart Complete ?"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Yes"
                    frmEventMsgs.CancelText = "No"
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    If (frmEventMsgs.Result = 2) Then
                        Set ApplList = New ApplicationAIList
                        Call ApplList.ApplSetAppointmentStatus(ApptId, "D")
                        Set ApplList = Nothing
                        lstDocs.RemoveItem lstDocs.ListIndex
                    End If
                End If
            End If
        ElseIf (TransType = "^") Then
            frmEventMsgs.Header = "Resend ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                Set ApplList = New ApplicationAIList
                TransId = ApplList.ApplGetTransId(lstDocs.List(lstDocs.ListIndex))
                If (TransId > 0) Then
                    Call ApplList.ApplSetTransactionStatus(TransId, "P")
                    lstDocs.RemoveItem lstDocs.ListIndex
                End If
                lstDocs.ListIndex = -1
                Set ApplList = Nothing
            End If
        End If
    End If
End If
End Sub

Private Sub lstFiles_Click()
If (lstFiles.ListIndex = 0) Then
    lstFiles.Clear
    lstFiles.Visible = False
ElseIf (lstFiles.ListIndex > 0) Then
    CollectionLetter = Trim(lstFiles.List(lstFiles.ListIndex))
    If (CollectionAction = "D") Then
        If (Trim(CollectionLetter) <> "") Then
            If (FM.IsFileThere(DocumentDirectory + CollectionLetter)) Then
                FM.Kill DocumentDirectory + Trim(CollectionLetter)
            End If
        End If
        lstFiles.Clear
        lstFiles.Visible = False
    ElseIf (CollectionAction = "V") Then
        If (Trim(CollectionLetter) <> "") Then
            If (FM.IsFileThere(DocumentDirectory + CollectionLetter)) Then
                Call TriggerWord(DocumentDirectory + Trim(CollectionLetter))
            End If
        End If
        lstFiles.Clear
        lstFiles.Visible = False
    End If
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        ResourceLocId = -1
    Else
        Set ApplList = New ApplicationAIList
        ResourceLocId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
    If (TransType = "J") Or (TransType = "C") Then
        Call LoadApptList(0, True)
    ElseIf (TransType = "X") Then
'        Call LoadClinicalList(False)
    ElseIf (TransType = "T") Then
        Call LoadTranscribeList(False)
    Else
        Call LoadDocList(True, DisplayType)
    End If
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As ApplicationAIList
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = -1
    Else
        Set ApplList = New ApplicationAIList
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
    If (TransType = "J") Or (TransType = "C") Then
        Call LoadApptList(0, True)
    ElseIf (TransType = "X") Then
'        Call LoadClinicalList(False)
    ElseIf (TransType = "T") Then
        Call LoadTranscribeList(False)
    Else
        Call LoadDocList(True, DisplayType)
    End If
End If
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    If (TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)) Or (lstStatus.ListIndex = 0) Then
        TheStatus = ""
    Else
        TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)
    End If
    If (TransType = "J") Then
        Call LoadApptList(0, True)
    ElseIf (TransType = "S") Or (TransType = "F") Or (TransType = "Y") Then
        CurrentIndex = 1
        Call LoadDocList(True, TheStatus)
    ElseIf (TransType = "X") Then
'        CurrentIndex = 1
'        Call LoadClinicalList(True)
    End If
End If
End Sub

Private Sub lstTests_Click()
If (lstTests.ListIndex >= 0) Then
    If (TheTest = Trim(lstTests.List(lstTests.ListIndex))) Or (lstTests.ListIndex = 0) Then
        TheTest = ""
    Else
        TheTest = Trim(lstTests.List(lstTests.ListIndex))
    End If
    If (TransType = "X") Then
'        CurrentIndex = 1
'        Call LoadClinicalList(True)
    End If
End If
End Sub

Private Sub lstType_Click()
If (lstType.ListIndex >= 0) Then
    If (lstType.ListIndex = 0) Then
        DisplayType = "S"
    ElseIf (lstType.ListIndex = 1) Then
        DisplayType = "s"
    End If
    If Not (TriggerOn) Then
        PageCnt = 0
        Call LoadDocList(True, DisplayType)
        PartyType = DisplayType
    End If
    TriggerOn = False
End If
End Sub

Private Sub SSDateCombo1_Change()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_Click()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_CloseUp()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") Then
        ActivityDate = SSDateCombo1.DateValue
        Call FormatTodaysDate(ActivityDate, False)
        If (ActivityDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = ActivityDate
        End If
    Else
        ATemp = ActivityDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        ActivityDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(ActivityDate, False)
        If (ActivityDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = ActivityDate
        End If
    End If
    If (ActivityDate <> "") Then
        If ((TransType = "J") Or (TransType = "C")) And Not (SendOn) Then
            Call LoadApptList(0, True)
        Else
            Call LoadDocList(True, DisplayType)
        End If
    End If
ElseIf (KeyAscii = 27) Then
    lblSending.Caption = "Printing Content"
    lblSending.Visible = True
    Call PrintContent(lstDocs, Label1.Caption)
    lblSending.Visible = False
    lblSending.Caption = "Sending Msg"
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        ActivityDate = ActivityDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(ActivityDate) > 0) Then
        ActivityDate = Left(ActivityDate, Len(ActivityDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(ActivityDate) < 1) Then
        ActivityDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo1_Validate(Cancel As Boolean)
'Call SSDateCombo1_KeyPress(13)
End Sub

Private Sub Timer1_Timer()
lblSending.Visible = True
lblSending.Caption = lblSending.Caption + "."
End Sub

Private Function MaintainLetter(Idx As Integer, IType As String, ActionType As String, AutoCreateOn As Boolean) As Boolean
Dim u As Integer
Dim PatId As Long, TransId As Long
Dim Temp As String, KTemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
If (Idx >= 0) Then
    Set ApplLetters = New Letters
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    If (ActionType = "E") Then
        If (IType <> "Y") And (IType <> "W") Then
            Temp = ApplLetters.ApplLettersFile(TransId)
            If (Trim(Temp) <> "") Then
                KTemp = IType
                If (KTemp = "[") Then
                    KTemp = "a"
                ElseIf (KTemp = "]") Then
                    KTemp = "d"
                End If
                If Not (FM.IsFileThere(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")) Then
                    lblPrint.Caption = "File Creation In Progress"
                    lblPrint.Visible = True
                    DoEvents
                    Set ApplTemp.lstBox = lstPrint
                    Call ApplTemp.PrintTransaction(TransId, 2, KTemp, True, True, Temp, False, 0, "S", "", 0)
                End If
                lblPrint.Visible = False
                DoEvents
                If Not (AutoCreateOn) Then
                    Call TriggerWord(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")
                End If
            End If
        ElseIf (IType = "W") Then
            u = InStrPS(95, lstDocs.List(Idx), "/")
            If (u > 0) Then
                TransId = val(Mid(lstDocs.List(Idx), 95, (u - 1) - 94))
                PatId = val(Mid(lstDocs.List(Idx), u + 1, Len(lstDocs.List(Idx)) - u))
                KTemp = IType
                If (Trim(CollectionLetter) <> "") Then
                    If Not (FM.IsFileThere(DocumentDirectory + "Coll" + Trim(CollectionLetter) + "_" + Trim(Str(PatId)) + "-" + Trim(Str(TransId)) + ".doc")) Then
                        lblPrint.Caption = "File Creation In Progress"
                        lblPrint.Visible = True
                        DoEvents
                        Set ApplTemp.lstBox = lstPrint
                        Call ApplTemp.PrintTransaction(TransId, PatId, "W", True, True, "/Ltr/" + Trim(CollectionLetter), False, 0, "S", "", 0)
                    End If
                    lblPrint.Visible = False
                    DoEvents
                    If Not (AutoCreateOn) Then
                        Call TriggerWord(DocumentDirectory + "Coll" + Trim(CollectionLetter) + "_" + Trim(Str(PatId)) + "-" + Trim(Str(TransId)) + ".doc")
                    End If
                End If
            End If
        End If
    ElseIf (ActionType = "D") Then
        If (IType <> "W") Then
            Temp = ApplLetters.ApplLettersFile(TransId)
            Call ApplTemp.ApplPurgeTransaction(TransId, True, False)
            Call LoadDocumentsList(SendOn, TransType, PartyType, False)
        Else
            u = InStrPS(95, lstDocs.List(Idx), "/")
            If (u > 0) Then
                TransId = val(Mid(lstDocs.List(Idx), 95, (u - 1) - 94))
                PatId = val(Mid(lstDocs.List(Idx), u + 1, Len(lstDocs.List(Idx)) - u))
                If (ViewCollectionLetter(TransId, PatId)) Then
                    CollectionAction = "D"
                End If
            End If
        End If
    End If
    Set ApplLetters = Nothing
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function MaintainStatus(AList As ListBox, IType As String) As Boolean
Dim w As Integer
Dim TransId As Long
Dim Temp As String
Dim ATemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
frmSelectDialogue.InsurerSelected = ""
If (IType = "J") Then
    Call frmSelectDialogue.BuildSelectionDialogue("CONFIRMSTATUS")
Else
    Call frmSelectDialogue.BuildSelectionDialogue("LETTERSTATUS")
End If
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    Temp = Left(frmSelectDialogue.Selection, 1)
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    For w = 0 To AList.ListCount - 1
        If (AList.Selected(w)) Then
            TransId = ApplList.ApplGetTransId(AList.List(w))
            If (TransId > 0) Then
                Call ApplList.ApplSetLetterStatus(TransId, Temp)
                ATemp = AList.List(w)
                Mid(ATemp, 1, 1) = Temp
                AList.List(w) = ATemp
'            Call LoadDocumentsList(SendOn, TransType, PartyType, Completed, False)
            End If
        End If
    Next w
    For w = 0 To AList.ListCount - 1
        AList.Selected(w) = False
    Next w
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function SendLetter(Idx As Integer, IType As String) As Boolean
Dim q As Integer
Dim TransId As Long
Dim Temp As String
Dim FileName As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
SendLetter = False
If (Idx >= 0) Then
    Set ApplLetters = New Letters
    Set ApplList = New ApplicationAIList
    Set ApplTemp = New ApplicationTemplates
    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    FileName = DocumentDirectory + "Ltrs-" + TransType + Trim(Str(TransId)) + ".doc"
    If Not (FM.IsFileThere(FileName)) Then
        frmEventMsgs.Header = "File must exist before transmitting"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Set ApplLetters = Nothing
        Set ApplTemp = Nothing
        Set ApplList = Nothing
        Exit Function
    End If
    frmEventMsgs.Header = "Send Letter How ?"
    frmEventMsgs.AcceptText = "E-Mail"
    frmEventMsgs.RejectText = "Fax"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Print"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        If (ApplList.ApplIsEmailOkay(TransId)) Then
            Call ApplList.ApplSetTransactionStatus(TransId, "B")
            Call ApplList.ApplSetTransactionAction(TransId, "E")
            SendLetter = True
        Else
            frmEventMsgs.Header = "No e-mail address"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (ApplList.ApplIsFaxOkay(TransId)) Then
            Call ApplList.ApplSetTransactionStatus(TransId, "B")
            Call ApplList.ApplSetTransactionAction(TransId, "F")
            SendLetter = True
        Else
            frmEventMsgs.Header = "No Fax Number"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Call ApplList.ApplSetTransactionStatus(TransId, "B")
        Call ApplList.ApplSetTransactionAction(TransId, "P")
        SendLetter = True
    End If
    Set ApplLetters = Nothing
    Set ApplTemp = Nothing
    Set ApplList = Nothing
End If
End Function

Private Function SelectCollectionLetter(ATemp As String) As Boolean
SelectCollectionLetter = False
ATemp = ""
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("COLLECTIONLETTERS")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    SelectCollectionLetter = frmSelectDialogue.SelectionResult
    ATemp = Trim(frmSelectDialogue.Selection)
End If
End Function

Private Function SelectParty(Idx As Integer, IType As String) As Boolean
Dim q As Integer
Dim GotIt As Boolean
Dim Temp As String
Dim Ref As String, TempRef As String
Dim ATemp As String, VndName As String
Dim FaxNumber As String, EMailAddress As String
Dim VndId As Long, VndIdOrg As Long, TransId As Long
Dim ApplList As ApplicationAIList
SelectParty = False
If (Idx >= 0) Then
    Set ApplList = New ApplicationAIList
    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    GotIt = False
    frmSelectDialogue.InsurerSelected = ""
    If (IType = "Y") Then
        Call frmSelectDialogue.BuildSelectionDialogue("HOSPITALS")
        frmSelectDialogue.Show 1
        GotIt = frmSelectDialogue.SelectionResult
        If (GotIt) Then
            Temp = Trim(frmSelectDialogue.Selection)
        End If
    ElseIf (IType = "X") Then
        Call frmSelectDialogue.BuildSelectionDialogue("LABS")
        frmSelectDialogue.Show 1
        GotIt = frmSelectDialogue.SelectionResult
        If (GotIt) Then
            Temp = Trim(frmSelectDialogue.Selection)
        End If
    Else
        frmEventMsgs.Header = "Select Party"
        frmEventMsgs.AcceptText = "PCP"
        frmEventMsgs.RejectText = "Referring Doctor"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Select Doctor"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            GotIt = ApplList.ApplGetPartyFromTransaction(TransId, "P", Temp)
        ElseIf (frmEventMsgs.Result = 2) Then
            GotIt = ApplList.ApplGetPartyFromTransaction(TransId, "R", Temp)
        ElseIf (frmEventMsgs.Result = 3) Then
            Call frmSelectDialogue.BuildSelectionDialogue("PCP")
            frmSelectDialogue.Show 1
            GotIt = frmSelectDialogue.SelectionResult
            If (GotIt) Then
                Temp = Trim(frmSelectDialogue.Selection)
                q = InStrPS(Temp, " ")
                If (q > 0) Then
                    FaxNumber = Trim(Mid(Temp, q, 55 - (q - 1))) + " " + Left(Temp, q - 1)
                    Temp = FaxNumber + Space(55 - Len(FaxNumber)) + Mid(Temp, 56, Len(Temp) - 55)
                    FaxNumber = ""
                End If
            End If
        End If
    End If
    If (GotIt) Then
        VndId = val(Mid(Temp, 57, Len(Temp) - 56))
        Call ApplList.ApplGetVendorTransmitInfo(VndId, EMailAddress, FaxNumber, VndName)
        If (ApplList.ApplGetTransactionReference(TransId, Ref, VndIdOrg)) Then
            Ref = ""
            Call ApplList.ApplSetTransactionReference(TransId, Ref, VndId)
            ATemp = lstDocs.List(Idx)
            If (Len(VndName) > 23) Then
                VndName = Left(VndName, 23)
            Else
                VndName = VndName + Space(23 - Len(VndName))
            End If
            Mid(ATemp, 66, 23) = Left(VndName, 23)
            lstDocs.List(Idx) = ATemp
        End If
        SelectParty = True
    End If
    Set ApplList = Nothing
End If
End Function

Public Function SendViaEmail(TheFile As String, IHow As String, ToEmailAddress As String) As Integer
Dim bFinished As Boolean
Dim iRetExport As Long
Dim iRetConnect As Long
Dim iRetAddress As Long
Dim iRetSend As Long
Dim liRemaining As Long
Dim iWriteLen As Integer
Dim iRetWrite As Integer
Dim i As Integer
Dim strBuffer As String
Dim iPos As Integer
Dim iPosat As Integer
Dim strDomain As String
Dim iRetQuery As Long
Dim strData As String
Dim iRetAttach As Long
Dim strFileName As String

Dim SubmitId As String
Dim DNSPort As String
Dim SmtpPort As String
Dim SmtpAddress As String
Dim FromEmailAddress As String

SendViaEmail = -1
DNSPort = ""
SmtpPort = CheckConfigCollection("SMTPPORT")
SmtpAddress = CheckConfigCollection("SMTPADDRESS")
FromEmailAddress = CheckConfigCollection("EUSER")
If (IHow = "P") Then
    SubmitId = CheckConfigCollection("SUBMITID")
    ToEmailAddress = CheckConfigCollection("TOEUSER")
End If
If (Trim(SmtpAddress) <> "") And (Trim(FromEmailAddress) <> "") And (Trim(ToEmailAddress) <> "") Then
    i = InStrPS(SmtpPort, "/")
    If (i <> 0) Then
        DNSPort = Mid(SmtpPort, i + 1, Len(SmtpPort) - i)
        SmtpPort = Left(SmtpPort, i - 1)
    End If
    SmtpClient1.HostName = SmtpAddress
    SmtpClient1.RemotePort = val(Trim(SmtpPort))
    DnsClient1.RemotePort = val(Trim(DNSPort))
    DnsClient1.NameServer(0) = ""
    SmtpClient1.AutoResolve = False
    SmtpClient1.Blocking = True

'Check recipient address to verify it is valid (or at least has enough characters to look valid)
    ToEmailAddress = Trim(ToEmailAddress)
    iPosat = InStrPS(ToEmailAddress, "@")
    If iPosat > 1 Then
        strDomain = Right(ToEmailAddress, Len(ToEmailAddress) - iPosat)
        iPos = InStrPS(strDomain, ".")
        If (iPos < 1) Then
            SendViaEmail = -2
            Exit Function
        End If
    Else
        SendViaEmail = -2
        Exit Function
    End If
Else
    SendViaEmail = -2
    Exit Function
End If
SmtpClient1.Recipient = ToEmailAddress
MailMessage1.To = SmtpClient1.Recipient
    
SmtpClient1.Address = Trim(FromEmailAddress)
MailMessage1.From = SmtpClient1.Address
MailMessage1.Date = SmtpClient1.DateStamp
If (Trim(TheFile) <> "") Then
    MailMessage1.Subject = "Incoming Attachment"
    MailMessage1.Text = "Incoming Attachment from " + SubmitId + " at " + Str(Time()) + " on " + Str(Date)
Else
    MailMessage1.Subject = "Preparing "
    MailMessage1.Text = "Preparing from " + SubmitId + " at " + Str(Time()) + " on " + Str(Date)
End If
'Attach file
If (Trim(TheFile) <> "") Then
    If Dir(Trim(TheFile)) <> "" Then
        iRetAttach = MailMessage1.AttachFile(TheFile)
        If iRetAttach <> 0 Then
            SendViaEmail = -3
            Exit Function
        End If
    End If
End If
   
'Write the message into the clipboard using ExportFile
Clipboard.Clear
iRetExport = MailMessage1.ExportFile("")
strBuffer = ""
strBuffer = Clipboard.getText
liRemaining = Len(strBuffer)
Clipboard.Clear
If liRemaining <= 2048 Then
    iWriteLen = liRemaining
Else
    iWriteLen = 2048
End If
bFinished = False
If SmtpClient1.Connected Then
    SmtpClient1.Disconnect
End If

'Begin the mail transaction by connecting to the Server
iRetConnect = SmtpClient1.Connect
If iRetConnect <> 0 Then
    SendViaEmail = -4
    Exit Function
End If
   
'Identify yourself to the server
iRetAddress = SmtpClient1.AddressMail
If iRetAddress <> 0 Then
    SendViaEmail = -5
    Exit Function
End If

'Write the email to the server
Timer1.Enabled = True
If (Trim(TheFile) = "") Then
    lblSending.Caption = "Verify Connection"
Else
    lblSending.Caption = "Sending "
End If
Call Timer1_Timer
While Not bFinished
    iRetWrite = SmtpClient1.Write(strBuffer, iWriteLen)
    If iRetWrite = -1 Then
        SendViaEmail = -6
        Exit Function
    End If
    If Len(strBuffer) = 0 Then
        bFinished = True
    Else
        liRemaining = liRemaining - iRetWrite
        If liRemaining <= 2048 Then
            iWriteLen = liRemaining
        Else
            iWriteLen = 2048
        End If
        strBuffer = Right(strBuffer, liRemaining)
    End If
Wend

'End the mail transaction and clean up
iRetSend = SmtpClient1.SendMail
If iRetSend <> 0 Then
    SendViaEmail = -7
    Exit Function
End If
SmtpClient1.Disconnect
SendViaEmail = 0
Timer1.Enabled = False
lblSending.Visible = False
Exit Function
UI_ErrorHandler:
    SendViaEmail = -10
    Timer1.Enabled = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function DoFacilityAdmin(TheId As Integer) As Boolean
Dim KeepGoing As Boolean
Dim ApptId As Long
Dim PatId As Long
Dim TheFac As String
Dim TheReq As String
Dim TheSurg As String
Dim TheHosp As String
Dim TheDate As String
Dim ApplLetters As Letters
DoFacilityAdmin = False
TheSurg = ""
TheHosp = ""
KeepGoing = True
While (KeepGoing)
    Call frmSelectDialogue.BuildSelectionDialogue("SurgeryType")
    frmSelectDialogue.Show 1
    If (Trim(frmSelectDialogue.Selection) <> "") Then
        If (Trim(TheSurg) = "") Then
            TheSurg = Trim(Left(frmSelectDialogue.Selection, 50))
        Else
            TheSurg = TheSurg + ", " + Trim(Left(frmSelectDialogue.Selection, 50))
        End If
    End If
    frmEventMsgs.Header = "Another Surgical Procedure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 4) Then
        KeepGoing = False
    End If
Wend
Call frmSelectDialogue.BuildSelectionDialogue("HOSPITALS")
frmSelectDialogue.Show 1
If (Trim(frmSelectDialogue.Selection) <> "") Then
    TheHosp = Trim(Left(frmSelectDialogue.Selection, 50))
End If
Call frmSelectDialogue.BuildSelectionDialogue("SurgicalRequests")
frmSelectDialogue.Show 1
If (Trim(frmSelectDialogue.Selection) <> "") Then
    TheReq = Trim(Left(frmSelectDialogue.Selection, 50))
End If
' Schedule Surgury Date
Set ApplLetters = New Letters
ApptId = ApplLetters.ApplLettersApptId(lstDocs.List(TheId))
PatId = ApplLetters.ApplLettersPatientId(lstDocs.List(TheId))
TheFac = "SCHEDULE SURGERY-3/" + TheSurg + "-F/-1/" + TheDate + "-7/" + TheHosp + "-5/" + TheReq + "-6/"
DoFacilityAdmin = ApplLetters.ApplPostLetter(PatId, ApptId, TheFac, 0, "", TransType)
Set ApplLetters = Nothing
End Function

Private Function SelectSurgical(Idx As Integer, IType As String) As Boolean
Dim i As Integer, j As Integer
Dim VndIdOrg As Long
Dim PatId As Long, ApptId As Long, TransId As Long
Dim ARef As String, ARem As String, AEye As String
Dim ALoc As String, ADate As String, AName As String
Dim ApplList As ApplicationAIList
Dim ApplLetters As Letters
SelectSurgical = False
If (Idx >= 0) And (IType = "Y") Then
    Set ApplList = New ApplicationAIList
    Set ApplLetters = New Letters
    PatId = ApplList.ApplGetTransPatientId(lstDocs.List(Idx))
    ApptId = ApplList.ApplGetTransApptId(lstDocs.List(Idx))
    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    Call ApplList.ApplGetTransactionReference(TransId, ARef, VndIdOrg)
    Call ApplList.ApplGetTransactionRemark(TransId, ARem)
    Call ReplaceCharacters(ARem, "/Ltr/", " ")
    ARem = Trim(ARem)
    AEye = ""
    i = InStrPS(ARem, "(")
    If (i > 0) Then
        AEye = Mid(ARem, i + 1, 2)
        ARem = Trim(Left(ARem, i - 1))
    End If
    i = InStrPS(ARef, "/")
    If (i > 0) Then
        ALoc = Left(ARef, i - 1)
        j = i + 1
        i = InStrPS(j, ARef, "/")
        If (i > 0) Then
            ADate = Trim(Mid(ARef, j, (i - 1) - (j - 1)))
            AName = Trim(Mid(ARef, i + 1, Len(ARef) - i))
            If (Trim(AName) <> "") Then
                Do Until (Mid(AName, Len(AName), 1) <> "/") Or (Len(AName) < 2)
                    AName = Trim(AName)
                    Call ReplaceCharacters(AName, "//", "~")
                    Call StripCharacters(AName, "~")
                    If (Trim(AName) = "") Then
                        Exit Do
                    End If
                Loop
            Else
                AName = ""
            End If
        Else
            ADate = Trim(Mid(ARef, j, Len(ARef) - (j - 1)))
            AName = ""
        End If
    Else
        ALoc = Trim(ARef)
        ADate = ""
        AName = ""
    End If
    frmSurgicalAI.ClinicalId = TransId
    frmSurgicalAI.PatientId = PatId
    frmSurgicalAI.AppointmentId = ApptId
    If (frmSurgicalAI.LoadSurgical(True, AName, ALoc, ADate, ARem, AEye)) Then
        frmSurgicalAI.Show 1
        SelectSurgical = True
    End If
    Set ApplList = Nothing
    Set ApplLetters = Nothing
End If
End Function

Private Function SelectSurgicalRequest(Idx As Integer, IType As String) As Boolean
Dim TransId As Long, VndIdOrg As Long
Dim ARef As String, TheReq As String
Dim ApplList As ApplicationAIList
SelectSurgicalRequest = False
If (Idx >= 0) And (IType = "Y") Then
    Call frmSelectDialogue.BuildSelectionDialogue("SurgicalRequests")
    frmSelectDialogue.Show 1
    If (Trim(frmSelectDialogue.Selection) <> "") Then
        TheReq = Trim(Left(frmSelectDialogue.Selection, 50))
    End If
    Set ApplList = New ApplicationAIList
    TransId = ApplList.ApplGetTransId(lstDocs.List(Idx))
    Call ApplList.ApplGetTransactionReference(TransId, ARef, VndIdOrg)
    ARef = Trim(ARef) + TheReq + "/"
    Call ApplList.ApplSetTransactionReference(TransId, ARef, VndIdOrg)
    Set ApplList = Nothing
    SelectSurgicalRequest = True
End If
End Function

Private Function PostToJournal(PatId As Long, ALang As String, ATemp As String) As Boolean
Dim NewId As Long
Dim ADate As String
Dim TheDate As String
Dim FileName As String
Dim AFile As String
Dim RetTrans As PracticeTransactionJournal
PostToJournal = False
If (PatId > 0) Then
    TheDate = ""
    Call FormatTodaysDate(TheDate, False)
    ADate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
    Set RetTrans = New PracticeTransactionJournal
    RetTrans.TransactionJournalId = 0
    If (RetTrans.RetrieveTransactionJournal) Then
        RetTrans.TransactionJournalStatus = "S"
        RetTrans.TransactionJournalTypeId = PatId
        RetTrans.TransactionJournalAction = "-"
        RetTrans.TransactionJournalBatch = ""
        RetTrans.TransactionJournalDate = ADate
        RetTrans.TransactionJournalServiceItem = 0
        RetTrans.TransactionJournalType = "L"
        RetTrans.TransactionJournalTime = -1
        RetTrans.TransactionJournalRemark = ALang
        RetTrans.TransactionJournalReference = ""
        PostToJournal = RetTrans.ApplyTransactionJournal
        NewId = RetTrans.TransactionJournalId
        AFile = SubmissionsDirectory + "Recall-L00000.xls"
        If (FM.IsFileThere(AFile)) Then
            FileName = SubmissionsDirectory + ATemp + "-L" + Trim(Str(NewId)) + ".txt"
            If (FM.IsFileThere(FileName)) Then
                FM.Kill FileName
            End If
            FM.Name AFile, FileName
        End If
    End If
    Set RetTrans = Nothing
End If
End Function

Private Function GetPatientId(TransId As Long) As Long
Dim RetTrans As PracticeTransactionJournal
GetPatientId = 0
If (TransId > 0) Then
    Set RetTrans = New PracticeTransactionJournal
    RetTrans.TransactionJournalId = TransId
    If (RetTrans.RetrieveTransactionJournal) Then
        GetPatientId = RetTrans.TransactionJournalTypeId
    End If
    Set RetTrans = Nothing
End If
End Function

Private Function ViewCollectionLetter(AId As Long, PatId As Long) As Boolean
Dim ATemp As String
Dim TheFile As String
ViewCollectionLetter = False
If (AId > 0) Then
    lstFiles.Visible = False
    lstFiles.Clear
    lstFiles.AddItem "Close Collections List"
    TheFile = DocumentDirectory + "Coll*_" + Trim(PatId) + "-" + Trim(Str(AId)) + ".doc"
    ATemp = Dir(TheFile)
    While (ATemp <> "")
        lstFiles.AddItem ATemp
        ATemp = Dir
    Wend
    If (lstFiles.ListCount > 1) Then
        lstFiles.Visible = True
        ViewCollectionLetter = True
    Else
        lstFiles.Clear
    End If
End If
End Function

Private Function ProcessStatementList(fIsAggregate As Boolean, fIsPrint As Boolean, fIgnoreDays As Boolean) As Boolean
Dim i As Long
Dim iTempInt As Integer
Dim iTempId As Long
Dim iListTotal As Long
Dim Statement As CPatientStatement
Dim iUserTypeOn As Long
Dim TelerikReporting As New Reporting
Dim PrintReport As Boolean
Dim ParamArgs() As Variant
Dim PatientId As Long
Dim BillingOrganizationId As Long

    iUserTypeOn = 0
    If CheckConfigCollection("OPTICALMEDICALON") = "T" Then
        iUserTypeOn = 1 ' Medical
        If (UserLogin.sType = "Q") Or (UserLogin.sType = "U") Or (UserLogin.sType = "Y") Then
            iUserTypeOn = 2 ' Optical
        End If
    End If

    iListTotal = lstAllEntries.ListCount
    For i = 0 To lstAllEntries.ListCount - 1
        If lstAllEntries.Selected(i) Then
            Set Statement = New CPatientStatement
            Statement.Aggregate = fIsAggregate
            
            'PatientId
            iTempId = 0
            iTempInt = InStrPS(95, lstAllEntries.List(i), "-")
            If iTempInt > 0 Then
                iTempId = val(Trim(Mid(lstAllEntries.List(i), iTempInt + 1, Len(lstAllEntries.List(i)) - iTempInt)))
            End If
            PatientId = iTempId
            Statement.PatId = iTempId
            
            'EncounterId
            iTempId = 0
            iTempInt = InStrPS(95, lstAllEntries.List(i), "/")
            If iTempInt > 0 Then
                iTempId = val(Trim(Mid(lstAllEntries.List(i), iTempInt + 1, Len(lstAllEntries.List(i)) - iTempInt)))
            End If
            Statement.EncounterId = iTempId
            
            'TransactionId.We Will still need this for Monthly Statements
            iTempId = 0
            iTempInt = InStrPS(95, lstAllEntries.List(i), "/")
            If iTempInt > 0 Then
                iTempId = val(Trim(Mid(lstAllEntries.List(i), 95, iTempInt - 94)))
            End If
            
            'Get location to pass it as BillingOrganizationId to the report.
            BillingOrganizationId = -1
            If (lstLoc.ListIndex >= 0) Then
                If (lstLoc.ListIndex = 1) Then
                    'To get back Main Office locationId.We make/Get 'Zero' while loading the location details
                    Dim sqlStr As String
                    Dim RS As Recordset
                    sqlStr = " SELECT TOP 1 Id from model.BillingOrganizations WHERE IsMain=1 "
                    Set RS = GetRS(sqlStr)
                    If RS.RecordCount > 0 Then BillingOrganizationId = RS("Id")
                Else
                    Dim ApplList As ApplicationAIList
                    Set ApplList = New ApplicationAIList
                    BillingOrganizationId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
                    If BillingOrganizationId > 1000 Then
                        BillingOrganizationId = BillingOrganizationId - 1000
                    End If
                    Set ApplList = Nothing
                End If
            End If

            Statement.TransactionId = iTempId
            If fIsAggregate Then
                If Not fIgnoreDays Then
                    If Not Statement.IsPastStatementRange Then
                        lstAllEntries.Selected(i) = False
                    End If
                End If
                If Not Statement.IsOverStatementLimit Then
                    lstAllEntries.Selected(i) = False
                End If
            End If
            
            If iUserTypeOn <> 0 Then
                If (IsMedical(iTempId) And iUserTypeOn = 1) _
                Or (IsOptical(iTempId) And iUserTypeOn = 2) Then
                Else
                    lstAllEntries.Selected(i) = False
                    
                    'msgbox
                    
                End If
            End If
            
            If lstAllEntries.Selected(i) Then
                lblPrint.Caption = "Processing Item " & Trim$(Str$(i)) & " of " + Trim$(Str$(iListTotal))
                lblPrint.Visible = True
                DoEvents
                
                ReDim ParamArgs(0) As Variant
                ParamArgs(0) = Array("PatientId", Str(PatientId))
                
                If BillingOrganizationId > 0 Then
                    ReDim Preserve ParamArgs(1) As Variant
                    ParamArgs(1) = Array("BillingOrganizationId", Str(BillingOrganizationId))
                End If
                
                'Invoke Patient Statement telerik Report.
                PrintReport = TelerikReporting.PrintReport("Patient Statement", ParamArgs)
                If PrintReport Then Statement.PostTransactions
            End If
            Set Statement = Nothing
        End If
    Next i
    lblPrint.Caption = ""
    lblPrint.Visible = False
    DoEvents
End Function


Private Function IsMedical(TranId As Long) As Boolean
IsMedical = isMedicalOptical(TranId, 0)
End Function
Private Function IsOptical(TranId As Long) As Boolean
IsOptical = isMedicalOptical(TranId, 1)
End Function

Private Function isMedicalOptical(TranId As Long, sw As Integer) As Boolean
Dim p As String
If sw = 0 Then p = "not "

Dim SQL As String
SQL = _
    " select * from PatientReceivables pr " & _
    " inner join Resources re on re.Resourceid = pr.BilltoDr " & _
    " inner join PracticeTransactionJournal ptj on pr.ReceivableId = ptj.TransactionTypeId " & _
    " where ptj.TransactionType = 'R' " & _
    " and ptj.TransactionRef = 'P' " & _
    " and ptj.TransactionStatus <> 'Z' " & _
    " and OpenBalance > 0 " & _
    " and ResourceType " & p & "in ('U', 'Q') " & _
    " and PatientId in " & _
    " (select PatientId " & _
    " from PracticeTransactionJournal ptj " & _
    " inner join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId " & _
    " where TransactionId = " & TranId & ") "
Dim RS As Recordset
Set RS = GetRS(SQL)

isMedicalOptical = (RS.RecordCount > 0)
End Function
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub
