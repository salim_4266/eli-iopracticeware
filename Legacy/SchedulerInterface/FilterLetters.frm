VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmFilterLetters 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstPATIENTTYPE 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "FilterLetters.frx":0000
      Left            =   7320
      List            =   "FilterLetters.frx":0007
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   47
      Top             =   4320
      Width           =   1695
   End
   Begin VB.CheckBox chkLTests 
      BackColor       =   &H0077742D&
      Caption         =   "Include Latest Tests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   46
      Top             =   8520
      Width           =   2775
   End
   Begin VB.CheckBox chkImpr 
      BackColor       =   &H0077742D&
      Caption         =   "Previous Impressions Only"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   45
      Top             =   8280
      Width           =   2775
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "FilterLetters.frx":001B
      Left            =   9240
      List            =   "FilterLetters.frx":001D
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   11
      Top             =   2760
      Width           =   2655
   End
   Begin VB.TextBox txtPatient 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6240
      TabIndex        =   39
      Top             =   7800
      Width           =   3615
   End
   Begin VB.ListBox lstLanguage 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "FilterLetters.frx":001F
      Left            =   2520
      List            =   "FilterLetters.frx":0021
      Sorted          =   -1  'True
      TabIndex        =   41
      Top             =   7800
      Width           =   1575
   End
   Begin VB.ListBox lstWho 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "FilterLetters.frx":0023
      Left            =   4200
      List            =   "FilterLetters.frx":0033
      TabIndex        =   38
      Top             =   7800
      Width           =   1815
   End
   Begin VB.CheckBox chkYear 
      BackColor       =   &H0077742D&
      Caption         =   "Exclude Year"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3480
      TabIndex        =   8
      Top             =   3975
      Width           =   1935
   End
   Begin VB.ListBox lstZip 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "FilterLetters.frx":005D
      Left            =   360
      List            =   "FilterLetters.frx":005F
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   12
      Top             =   4320
      Width           =   2895
   End
   Begin VB.ListBox lstCType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "FilterLetters.frx":0061
      Left            =   5640
      List            =   "FilterLetters.frx":0063
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   9
      Top             =   2760
      Width           =   1575
   End
   Begin VB.ListBox lstIns 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":0065
      Left            =   9240
      List            =   "FilterLetters.frx":0067
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   960
      Width           =   2655
   End
   Begin VB.ListBox lstAStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":0069
      Left            =   6600
      List            =   "FilterLetters.frx":006B
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   960
      Width           =   2415
   End
   Begin VB.ListBox lstAType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":006D
      Left            =   3480
      List            =   "FilterLetters.frx":006F
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   960
      Width           =   2895
   End
   Begin VB.ListBox lstLetter 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":0071
      Left            =   360
      List            =   "FilterLetters.frx":0073
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   960
      Width           =   2895
   End
   Begin VB.ListBox lstPlanType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "FilterLetters.frx":0075
      Left            =   7320
      List            =   "FilterLetters.frx":0077
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   10
      Top             =   2760
      Width           =   1695
   End
   Begin VB.ListBox lstRefCat 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      ItemData        =   "FilterLetters.frx":0079
      Left            =   9240
      List            =   "FilterLetters.frx":007B
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   13
      Top             =   4320
      Width           =   2295
   End
   Begin VB.ListBox lstDrs 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":007D
      Left            =   8280
      List            =   "FilterLetters.frx":007F
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   16
      Top             =   5880
      Width           =   3615
   End
   Begin VB.ListBox lstRefDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":0081
      Left            =   4320
      List            =   "FilterLetters.frx":0083
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   15
      Top             =   5880
      Width           =   3855
   End
   Begin VB.ListBox lstPCP 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "FilterLetters.frx":0085
      Left            =   360
      List            =   "FilterLetters.frx":0087
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   14
      Top             =   5880
      Width           =   3735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   360
      TabIndex        =   19
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FilterLetters.frx":0089
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10080
      TabIndex        =   17
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FilterLetters.frx":0268
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   360
      TabIndex        =   4
      Top             =   3000
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2050/12/31"
      AllowNullDate   =   -1  'True
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   3600
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2050/12/31"
      AllowNullDate   =   -1  'True
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo3 
      Height          =   375
      Left            =   3480
      TabIndex        =   6
      Top             =   3000
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2050/12/31"
      AllowNullDate   =   -1  'True
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo4 
      Height          =   375
      Left            =   3480
      TabIndex        =   7
      Top             =   3600
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2050/12/31"
      AllowNullDate   =   -1  'True
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   7320
      TabIndex        =   48
      Top             =   4080
      Width           =   1200
   End
   Begin VB.Label Label22 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Locations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9240
      TabIndex        =   44
      Top             =   2520
      Width           =   915
   End
   Begin VB.Label Label21 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6240
      TabIndex        =   43
      Top             =   7560
      Width           =   1290
   End
   Begin VB.Label Label20 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Language"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   2520
      TabIndex        =   42
      Top             =   7560
      Width           =   960
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Receiver"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4200
      TabIndex        =   40
      Top             =   7560
      Width           =   840
   End
   Begin VB.Label lblLoading 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4800
      TabIndex        =   37
      Top             =   240
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3480
      TabIndex        =   36
      Top             =   3360
      Width           =   825
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3480
      TabIndex        =   35
      Top             =   2760
      Width           =   915
   End
   Begin VB.Label lblSDate 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   34
      Top             =   2760
      Width           =   915
   End
   Begin VB.Label lblEDate 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   33
      Top             =   3360
      Width           =   825
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Referral Category"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9240
      TabIndex        =   32
      Top             =   4080
      Width           =   1680
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Doctors"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8280
      TabIndex        =   31
      Top             =   5640
      Width           =   705
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Referring Doc"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4320
      TabIndex        =   30
      Top             =   5640
      Width           =   1305
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "PCP"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   29
      Top             =   5640
      Width           =   405
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Plan Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   7320
      TabIndex        =   28
      Top             =   2520
      Width           =   960
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   9240
      TabIndex        =   27
      Top             =   720
      Width           =   660
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Type of Contact"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   5640
      TabIndex        =   26
      Top             =   2520
      Width           =   1485
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Zip Code"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   25
      Top             =   4080
      Width           =   840
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "DOB"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3480
      TabIndex        =   24
      Top             =   2520
      Width           =   420
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appt Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6600
      TabIndex        =   23
      Top             =   720
      Width           =   1080
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appt Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3480
      TabIndex        =   22
      Top             =   720
      Width           =   960
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Letter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   21
      Top             =   720
      Width           =   555
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Appt Date Range"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   360
      TabIndex        =   20
      Top             =   2520
      Width           =   1605
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Select Letter Criteria"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   360
      Left            =   360
      TabIndex        =   18
      Top             =   240
      Width           =   3015
   End
End
Attribute VB_Name = "frmFilterLetters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public FilterCriteria As Boolean
Public QuitOn As Boolean
Private StartDate As String
Private EndDate As String
Private DOBStartDate As String
Private DOBEndDate As String
Private CriteriaLatestTests As String
Private CriteriaImprOnly As String
Private CriteriaLetter As String
Private CriteriaAType As String
Private CriteriaAStatus As String
Private CriteriaBDate As String
Private CriteriaADate As String
Private CriteriaPCP As String
Private CriteriaRefDr As String
Private CriteriaDocs As String
Private CriteriaLocs As String
Private CriteriaZip As String
Private CriteriaRefCat As String
Private CriteriaContact As String
Private CriteriaPlanType As String
Private CriteriaPATIENTTYPE As String
Private CriteriaInsurer As String
Private CriteriaCPT As String
Private CriteriaICD As String
Private CriteriaPatient As String
Private CriteriaRecipient As String
Private CriteriaLanguage As String

Private Sub cmdDone_Click()
FilterCriteria = IsCriteriaOn
If (FilterCriteria) Then
    If (lstLetter.ListIndex < 0) Then
        frmEventMsgs.Header = "No Letter Selected"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        Call SetupExposedCriteria
        lblLoading.Visible = True
        DoEvents
        Unload frmFilterLetters
    End If
Else
    QuitOn = True
    Unload frmFilterLetters
End If
End Sub

Private Sub cmdHome_Click()
FilterCriteria = False
QuitOn = True
Unload frmFilterLetters
End Sub

Private Sub LoadList(AListbox As ListBox)
Dim i As Integer
Dim k As Integer
Dim w As Integer
Dim DoIt As Boolean
Dim AllString As String
Dim RetPrc As PracticeName
Dim RetRsc As SchedulerResource
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim RetProc As Service
Dim RetDiag As DiagnosisMasterPrimary
Dim DisplayText As String
Dim ApplTbl As ApplicationTables
AListbox.Clear
DisplayText = ""
AllString = " All Items"
Set ApplTbl = New ApplicationTables
If (AListbox.Name = "lstPlanType") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadCodes("PLANTYPE", False, True, True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstPATIENTTYPE") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadCodes("PATIENTTYPE", False, True, True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstLanguage") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadCodes("LANGUAGE", False, True, True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstIns") Then
    Set RetPatientFinancialService = New PatientFinancialService
    RetPatientFinancialService.InsurerName = Chr(1)
    Set RS = RetPatientFinancialService.GetInsurerInfoByName(True)
    If (Not RS Is Nothing And RS.RecordCount > 0) Then
        While (Not RS.EOF)
            AListbox.AddItem Trim(RS("Name"))
            RS.MoveNext
        Wend
    End If
    Set RetPatientFinancialService = Nothing
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstAStatus") Then
    AListbox.AddItem "A - Arrived"
    AListbox.AddItem "P - Pending"
    AListbox.AddItem "R - Rescheduled"
    AListbox.AddItem "N - No Show"
    AListbox.AddItem "Q - Special"
    AListbox.AddItem "S - Same Day Cancel"
    AListbox.AddItem "Y - Patient Cancel"
    AListbox.AddItem "O - Office Cancel"
    AListbox.AddItem "F - Left"
    AListbox.AddItem "D - Discharged"
    AListbox.AddItem "X - Cancel"
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstDrs") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadResource("D", False, False, False, True, False)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstLoc") Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        k = 1
        While (RetPrc.SelectPractice(k))
            DisplayText = Space(56)
            Mid(DisplayText, 1, 6) = "Office"
            If (Trim(RetPrc.PracticeLocationReference) <> "") Then
                Mid(DisplayText, 7, Len(Trim(RetPrc.PracticeLocationReference)) + 1) = "-" + Trim(RetPrc.PracticeLocationReference)
                DisplayText = DisplayText + Trim(Str(1000 + RetPrc.PracticeId))
            Else
                DisplayText = DisplayText + Trim(Str(0))
            End If
            AListbox.AddItem DisplayText
            k = k + 1
        Wend
    End If
    Set RetPrc = Nothing
    Set RetRsc = New SchedulerResource
    RetRsc.ResourceName = Chr(1)
    If (RetRsc.FindResource > 0) Then
        k = 1
        While (RetRsc.SelectResource(k))
            DisplayText = Space(56)
            Mid(DisplayText, 1, Len(Trim(RetRsc.ResourceDescription))) = Trim(RetRsc.ResourceDescription)
            If (RetRsc.ResourceServiceCode = "02") Then
                DisplayText = DisplayText + Trim(Str(RetRsc.ResourceId))
                AListbox.AddItem DisplayText
            End If
            k = k + 1
        Wend
    End If
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstPCP") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadVendorList(False, "D", True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstRefDr") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadVendorList(False, "D", True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstAType") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadApptType(False)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstRefCat") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadCodes("REFERBY", False, True, True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstCType") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadCodes("VENDORTYPE", False, True, True)
    AListbox.AddItem AllString
ElseIf (AListbox.Name = "lstLetter") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadCodes("MISCELLANEOUSLETTERS", False, True, True)
ElseIf (AListbox.Name = "lstZip") Then
    Set ApplTbl.lstBox = AListbox
    Call ApplTbl.ApplLoadZipCodeList
    AListbox.AddItem AllString
End If
Set ApplTbl = Nothing
End Sub

Public Function LoadCriteria(ZipOn As Boolean) As Boolean
Dim i As Integer
LoadCriteria = True
lblLoading.Visible = False
FilterCriteria = False
StartDate = ""
EndDate = ""
DOBStartDate = ""
DOBEndDate = ""
CriteriaLatestTests = ""
CriteriaImprOnly = ""
CriteriaLetter = ""
CriteriaAType = ""
CriteriaAStatus = ""
CriteriaBDate = ""
CriteriaADate = ""
CriteriaPCP = ""
CriteriaRefDr = ""
CriteriaDocs = ""
CriteriaZip = ""
CriteriaRefCat = ""
CriteriaContact = ""
CriteriaPlanType = ""
CriteriaPATIENTTYPE = ""
CriteriaInsurer = ""
CriteriaCPT = ""
CriteriaICD = ""
CriteriaLocs = ""
CriteriaPatient = ""
CriteriaRecipient = ""
CriteriaLanguage = ""
chkYear.Value = 0
chkImpr.Value = 0
Call LoadList(lstLetter)
Call LoadList(lstAType)
Call LoadList(lstAStatus)
Call LoadList(lstCType)
Call LoadList(lstRefCat)
Call LoadList(lstPlanType)
Call LoadList(lstDrs)
Call LoadList(lstLoc)
Call LoadList(lstLanguage)
Call LoadList(lstPATIENTTYPE)

If (ZipOn) Then
    Call LoadList(lstIns)
    Call LoadList(lstZip)
    Call LoadList(lstPCP)
    lstRefDr.Clear
    For i = 0 To lstPCP.ListCount - 1
        lstRefDr.AddItem lstPCP.List(i)
    Next i
Else
    lstZip.Clear
    lstZip.AddItem "Show Zip Codes"
    lstPCP.Clear
    lstPCP.AddItem "Show PCPs"
    lstRefDr.Clear
    lstRefDr.AddItem "Show Referring Docs"
    lstIns.Clear
    lstIns.AddItem "Show Insurers"
End If
lstWho.ListIndex = 0
End Function

Private Function SetupExposedCriteria() As Boolean
Dim i As Integer
Dim j As Integer
Dim TheText As String
TheText = ""
If (lstLetter.ListIndex >= 0) Then
    CriteriaLetter = Trim(lstLetter.List(lstLetter.ListIndex))
End If

TheText = ""
If (lstAType.SelCount > 0) Then
    If (lstAType.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstAType.ListCount - 1
            If (lstAType.Selected(i)) Then
                j = InStrPS(lstAType.List(i), "  ")
                If (j > 0) Then
                    TheText = TheText + Trim(Left(lstAType.List(i), j - 1))
                Else
                    TheText = TheText + Trim(lstAType.List(i))
                End If
                'If (i <> lstAType.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaAType = TheText
    
TheText = ""
If (lstAStatus.SelCount > 0) Then
    If (lstAStatus.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstAStatus.ListCount - 1
            If (lstAStatus.Selected(i)) Then
                TheText = TheText + Trim(Left(lstAStatus.List(i), 1))
                'If (i <> lstAStatus.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaAStatus = TheText
    
TheText = ""
If (lstCType.SelCount > 0) Then
    If (lstCType.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstCType.ListCount - 1
            If (lstCType.Selected(i)) Then
                TheText = TheText + Trim(Left(lstCType.List(i), 1))
                'If (i <> lstCType.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaContact = TheText

TheText = ""
If (lstPlanType.SelCount > 0) Then
    If (lstPlanType.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstPlanType.ListCount - 1
            If (lstPlanType.Selected(i)) Then
                j = InStrPS(lstPlanType.List(i), "  ")
                If (j > 0) Then
                    TheText = TheText + Trim(Left(lstPlanType.List(i), j - 1))
                Else
                    TheText = TheText + Trim(lstPlanType.List(i))
                End If
                'If (i <> lstPlanType.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaPlanType = TheText

TheText = ""
If (lstPATIENTTYPE.SelCount > 0) Then
    If (lstPATIENTTYPE.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstPATIENTTYPE.ListCount - 1
            If (lstPATIENTTYPE.Selected(i)) Then
                j = InStrPS(lstPATIENTTYPE.List(i), "  ")
                If (j > 0) Then
                    TheText = TheText + Trim(Left(lstPATIENTTYPE.List(i), j - 1))
                Else
                    TheText = TheText + Trim(lstPATIENTTYPE.List(i))
                End If
                TheText = Mid(TheText, 1, InStrPS(1, TheText, " - ") - 1)
                'If (i <> lstPATIENTTYPE.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaPATIENTTYPE = TheText

TheText = ""
If (lstIns.SelCount > 0) Then
    If (lstIns.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstIns.ListCount - 1
            If (lstIns.Selected(i)) Then
                TheText = TheText + Trim(lstIns.List(i))
                'If (i <> lstIns.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaInsurer = TheText

TheText = ""
If (lstZip.SelCount > 0) Then
    If (lstZip.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstZip.ListCount - 1
            If (lstZip.Selected(i)) Then
                TheText = TheText + Trim(lstZip.List(i))
                'If (i <> lstZip.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaZip = TheText

TheText = ""
CriteriaCPT = TheText

TheText = ""
CriteriaICD = TheText

TheText = ""
If (lstRefCat.SelCount > 0) Then
    If (lstRefCat.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstRefCat.ListCount - 1
            If (lstRefCat.Selected(i)) Then
                j = InStrPS(lstRefCat.List(i), "  ")
                If (j > 0) Then
                    TheText = TheText + Trim(Left(lstRefCat.List(i), j - 1))
                Else
                    TheText = TheText + Trim(lstRefCat.List(i))
                End If
                'If (i <> lstRefCat.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaRefCat = TheText

TheText = ""
If (lstPCP.SelCount > 0) Then
    If (lstPCP.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstPCP.ListCount - 1
            If (lstPCP.Selected(i)) Then
                j = InStrPS(lstPCP.List(i), "  ")
                If (j > 0) Then
                    TheText = TheText + Trim(Left(lstPCP.List(i), j - 1))
                Else
                    TheText = TheText + Trim(lstPCP.List(i))
                End If
                'If (i <> lstPCP.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaPCP = TheText

TheText = ""
If (lstRefDr.SelCount > 0) Then
    If (lstRefDr.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstRefDr.ListCount - 1
            If (lstRefDr.Selected(i)) Then
                j = InStrPS(lstRefDr.List(i), "  ")
                If (j > 0) Then
                    TheText = TheText + Trim(Left(lstRefDr.List(i), j - 1))
                Else
                    TheText = TheText + Trim(lstRefDr.List(i))
                End If
                'If (i <> lstRefDr.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaRefDr = TheText

TheText = ""
If (lstDrs.SelCount > 0) Then
    If (lstDrs.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstDrs.ListCount - 1
            If (lstDrs.Selected(i)) Then
                j = InStrPS(lstDrs.List(i), " ")
                TheText = TheText + Trim(Mid(lstDrs.List(i), 57, Len(lstDrs.List(i)) - 56))
                'If (i <> lstDrs.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaDocs = TheText

TheText = ""
If (lstLoc.SelCount > 0) Then
    If (lstLoc.Selected(0)) Then
        TheText = "All"
    Else
        For i = 0 To lstLoc.ListCount - 1
            If (lstLoc.Selected(i)) Then
                TheText = TheText + Trim(Mid(lstLoc.List(i), 57, Len(lstLoc.List(i)) - 56))
                'If (i <> lstLoc.ListCount - 1) Then
                    TheText = TheText + "&"
                'End If
            End If
        Next i
    End If
End If
CriteriaLocs = TheText

TheText = ""
If (Trim(StartDate) <> "") Then
    Call FormatTodaysDate(StartDate, False)
    TheText = Trim(StartDate)
End If
TheText = TheText + "&"
If (Trim(EndDate) <> "") Then
    Call FormatTodaysDate(EndDate, False)
    TheText = TheText + Trim(EndDate)
End If
If (TheText = "&") Then
    TheText = ""
End If
CriteriaADate = TheText

TheText = ""
If (Trim(DOBStartDate) <> "") Then
    Call FormatTodaysDate(DOBStartDate, False)
    TheText = Trim(DOBStartDate)
End If
TheText = TheText + "&"
If (Trim(DOBEndDate) <> "") Then
    Call FormatTodaysDate(DOBEndDate, False)
    TheText = TheText + Trim(DOBEndDate)
End If
TheText = TheText + "&"
If (TheText = "&&") Then
    TheText = ""
End If
If (Trim(TheText) <> "") Then
    If (chkYear.Value = 1) Then
        TheText = TheText + "+"
    Else
        TheText = TheText + "-"
    End If
End If
CriteriaBDate = TheText
If (lstWho.ListIndex >= 0) Then
    CriteriaRecipient = lstWho.List(lstWho.ListIndex)
End If
If (lstLanguage.ListIndex >= 0) Then
    CriteriaLanguage = Trim(Left(lstLanguage.List(lstLanguage.ListIndex), 32))
End If
If (Trim(txtPatient.Text) <> "") Then
    CriteriaPatient = Trim(Mid(txtPatient.Text, 57, Len(txtPatient.Text) - 56))
End If
CriteriaImprOnly = ""
If (chkImpr.Value = 1) Then
    CriteriaImprOnly = "T"
End If
CriteriaLatestTests = ""
If (chkLTests.Value = 1) Then
    CriteriaLatestTests = "T"
End If
End Function

Public Function GetFilterCriteria(CriteriaName) As String
Dim TheText As String
TheText = ""
If (UCase(Trim(CriteriaName)) = "LETTER") Then
    TheText = CriteriaLetter
ElseIf (UCase(Trim(CriteriaName)) = "ATYPE") Then
    TheText = CriteriaAType
ElseIf (UCase(Trim(CriteriaName)) = "ASTATUS") Then
    TheText = CriteriaAStatus
ElseIf (UCase(Trim(CriteriaName)) = "CONTACT") Then
    TheText = CriteriaContact
ElseIf (UCase(Trim(CriteriaName)) = "PLANTYPE") Then
    TheText = CriteriaPlanType
ElseIf (UCase(Trim(CriteriaName)) = "PATIENTTYPE") Then
    TheText = CriteriaPATIENTTYPE
ElseIf (UCase(Trim(CriteriaName)) = "INSURER") Then
    TheText = CriteriaInsurer
ElseIf (UCase(Trim(CriteriaName)) = "ZIP") Then
    TheText = CriteriaZip
ElseIf (UCase(Trim(CriteriaName)) = "CPT") Then
    TheText = CriteriaCPT
ElseIf (UCase(Trim(CriteriaName)) = "ICD") Then
    TheText = CriteriaICD
ElseIf (UCase(Trim(CriteriaName)) = "REFCAT") Then
    TheText = CriteriaRefCat
ElseIf (UCase(Trim(CriteriaName)) = "PCP") Then
    TheText = CriteriaPCP
ElseIf (UCase(Trim(CriteriaName)) = "REFDR") Then
    TheText = CriteriaRefDr
ElseIf (UCase(Trim(CriteriaName)) = "DOCS") Then
    TheText = CriteriaDocs
ElseIf (UCase(Trim(CriteriaName)) = "ADATE") Then
    TheText = CriteriaADate
ElseIf (UCase(Trim(CriteriaName)) = "BDATE") Then
    TheText = CriteriaBDate
ElseIf (UCase(Trim(CriteriaName)) = "RECIPIENT") Then
    TheText = CriteriaRecipient
ElseIf (UCase(Trim(CriteriaName)) = "LANGUAGE") Then
    TheText = CriteriaLanguage
ElseIf (UCase(Trim(CriteriaName)) = "PATIENT") Then
    TheText = CriteriaPatient
ElseIf (UCase(Trim(CriteriaName)) = "LOCS") Then
    TheText = CriteriaLocs
ElseIf (UCase(Trim(CriteriaName)) = "IMPRONLY") Then
    TheText = CriteriaImprOnly
ElseIf (UCase(Trim(CriteriaName)) = "LATESTTESTS") Then
    TheText = CriteriaLatestTests
End If
GetFilterCriteria = Trim(TheText)
End Function

Private Function IsCriteriaOn() As Boolean
Dim i As Integer
If (lstAType.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstAStatus.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstCType.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstPlanType.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstIns.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstZip.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstRefCat.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstPCP.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstRefDr.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstDrs.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstLoc.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstLanguage.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
If (StartDate <> "") Or (EndDate <> "") Then
    IsCriteriaOn = True
    Exit Function
End If
If (DOBStartDate <> "") Or (DOBEndDate <> "") Then
    IsCriteriaOn = True
    Exit Function
End If
If (Trim(txtPatient.Text) <> "") Then
    IsCriteriaOn = True
    Exit Function
End If
If (chkImpr.Value = 1) Then
    IsCriteriaOn = True
    Exit Function
End If
If (lstPATIENTTYPE.SelCount > 0) Then
    IsCriteriaOn = True
    Exit Function
End If
End Function

Private Sub lstIns_Click()
If (lstIns.ListIndex = 0) Then
    If (lstIns.List(0) = "Show Insurers") Then
        Call LoadList(lstIns)
    End If
End If
End Sub

Private Sub lstPCP_Click()
If (lstPCP.ListIndex = 0) Then
    If (lstPCP.List(0) = "Show PCPs") Then
        Call LoadList(lstPCP)
    End If
End If
End Sub

Private Sub lstRefDr_Click()
If (lstRefDr.ListIndex = 0) Then
    If (lstRefDr.List(0) = "Show Referring Docs") Then
        Call LoadList(lstRefDr)
    End If
End If
End Sub

Private Sub lstZip_Click()
If (lstZip.ListIndex = 0) Then
    If (lstZip.List(0) = "Show Zip Codes") Then
        Call LoadList(lstZip)
    End If
End If
End Sub

Private Sub SSDateCombo1_Change()
StartDate = SSDateCombo1.DateValue
End Sub

Private Sub SSDateCombo1_Click()
StartDate = SSDateCombo1.DateValue
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call SSDateCombo1_Change
End If
End Sub

Private Sub SSDateCombo2_Change()
EndDate = SSDateCombo2.DateValue
End Sub

Private Sub SSDateCombo2_Click()
EndDate = SSDateCombo2.DateValue
End Sub

Private Sub SSDateCombo2_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call SSDateCombo2_Change
End If
End Sub

Private Sub SSDateCombo3_Change()
DOBStartDate = SSDateCombo3.DateValue
End Sub

Private Sub SSDateCombo3_Click()
DOBStartDate = SSDateCombo3.DateValue
End Sub

Private Sub SSDateCombo3_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call SSDateCombo3_Change
End If
End Sub

Private Sub SSDateCombo4_Change()
DOBEndDate = SSDateCombo4.DateValue
End Sub

Private Sub SSDateCombo4_Click()
DOBEndDate = SSDateCombo4.DateValue
End Sub

Private Sub SSDateCombo4_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call SSDateCombo4_Change
End If
End Sub

Private Sub txtPatient_Click()
Dim PatId As Long
Dim PatName As String
Dim RetPat As Patient
Dim ReturnArguments As Variant

Set ReturnArguments = DisplayPatientSearchScreen(False)
If Not (ReturnArguments Is Nothing) Then PatId = ReturnArguments.PatientId
If (PatId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        PatName = Trim(RetPat.LastName) + " " + Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial)
        PatName = PatName + Space(56 - Len(PatName)) + Trim(Str(PatId))
        txtPatient.Text = PatName
    End If
    Set RetPat = Nothing
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
