Attribute VB_Name = "Setting"
Option Explicit

Public Type ServicesGrid
    ClaimHeader As Boolean
    InvoiceInformation As Boolean
    ServiceHeader As Boolean
    Code As Boolean
    BillingTransactionsClaimsClosed As Boolean
    BillingTransactionsClaimsQueued As Boolean
    BillingTransactionsClaimsSent As Boolean
    BillingTransactionsAppeal As Boolean
    BillingTransactionsAppealClosed As Boolean
    BillingTransactionsPatientStatementQueued As Boolean
    BillingTransactionsPatientStatementSent As Boolean
    BillingTransactionsPatientStatementClosed As Boolean
    BillingTransactionsMonthlyStatement As Boolean
    BillingTransactionOrphaned As Boolean
    InsurancePayments As Boolean
    PatientPayments As Boolean
    InsuranceAdjustments As Boolean
    NonInsuranceAdjustments As Boolean
    InfoTransactions As Boolean
    ClaimTotal As Boolean
    
    Invoice As Integer
    Services As Integer
    Payments As Integer
    Billing As Integer
End Type
Public SrvGrid As ServicesGrid
Public NumDispVisits As Integer
Public AllVisits() As String

Public Sub InitSetting()
NumDispVisits = 3
With SrvGrid
    .ClaimHeader = True
    .InvoiceInformation = True
    .ServiceHeader = True
    .ClaimTotal = True
    .ServiceHeader = True
    .Code = True
    .InsurancePayments = True
    .PatientPayments = True
    .InsuranceAdjustments = True
    .NonInsuranceAdjustments = True
    .InfoTransactions = True
    .BillingTransactionsClaimsQueued = False
    .BillingTransactionsClaimsSent = False
    .BillingTransactionsClaimsClosed = False
    .BillingTransactionsAppeal = False
    .BillingTransactionsAppealClosed = False
    .BillingTransactionsPatientStatementQueued = False
    .BillingTransactionsPatientStatementSent = False
    .BillingTransactionsPatientStatementClosed = False
    .BillingTransactionsMonthlyStatement = False
    .BillingTransactionOrphaned = False
    
    .Invoice = 1
    .Services = 1
    .Payments = 1
    .Billing = 0
    
End With

ApptCategoryColors(0) = "A"
ApptCategoryColors(1) = "B"
ApptCategoryColors(2) = "C"
ApptCategoryColors(3) = "D"
ApptCategoryColors(4) = "E"
ApptCategoryColors(5) = "I"
ApptCategoryColors(6) = "L"
ApptCategoryColors(7) = "M"
ApptCategoryColors(8) = "N"
ApptCategoryColors(9) = "O"
ApptCategoryColors(10) = "P"
ApptCategoryColors(11) = "S"
ApptCategoryColors(12) = "W"
ApptCategoryColors(13) = "X"
ApptCategoryColors(14) = "Y"
ApptCategoryColors(15) = "Z"


End Sub
