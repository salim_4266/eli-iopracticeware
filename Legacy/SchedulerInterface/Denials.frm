VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmDenials 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Denial Reconciliation"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog msCDlg 
      Left            =   7800
      Top             =   8280
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox txtDenials 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7320
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Text            =   "Denials.frx":0000
      Top             =   240
      Width           =   11535
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10200
      TabIndex        =   1
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Denials.frx":0028
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatFin 
      Height          =   975
      Left            =   8400
      TabIndex        =   2
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Denials.frx":0207
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOpen 
      Height          =   975
      Left            =   2040
      TabIndex        =   3
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Denials.frx":03F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdParse 
      Height          =   975
      Left            =   240
      TabIndex        =   4
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Denials.frx":05DD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemoveDenials 
      Height          =   975
      Left            =   3840
      TabIndex        =   5
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Denials.frx":07CF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrintDenials 
      Height          =   975
      Left            =   5640
      TabIndex        =   6
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Denials.frx":09C1
   End
End
Attribute VB_Name = "frmDenials"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
If (Dir(PinPointDirectory & "SummaryReports", vbDirectory) = "") Then
    FM.MkDir (PinPointDirectory & "SummaryReports")
End If
If (Dir(PinPointDirectory & "SummaryReports\Archived", vbDirectory) = "") Then
    FM.MkDir (PinPointDirectory & "SummaryReports\Archived")
End If
Call DisplayTextFile(txtDenials, PinPointDirectory & "SummaryReports\ParsedDenials.txt")
End Sub

Private Sub cmdOpen_Click()
Call ParseSumForDenials(OpenSingleFile(msCDlg, PinPointDirectory & "SummaryReports\Archived\"))
Call DisplayTextFile(txtDenials, PinPointDirectory & "SummaryReports\ParsedDenials.txt")
End Sub

Private Sub cmdPatFin_Click()
Dim PatId As Long
Dim DoAgain As Boolean
DoAgain = True
Dim ReturnArguments As Variant
While (DoAgain)
    PatId = 0
    Set ReturnArguments = DisplayPatientSearchScreen
    If Not (ReturnArguments Is Nothing) Then
        PatId = ReturnArguments.PatientId
    End If
    If (PatId > 0) Then
''''''        frmPayments.AccessType = ""
''''''        frmPayments.Whom = True
''''''        frmPayments.CurrentAction = ""
''''''        frmPayments.PatientId = PatId
''''''        frmPayments.ReceivableId = 0
''''''        Call frmPayments.LoadPayments(True, "")
''''''        frmPayments.Show 1
''''''        If (frmPayments.CurrentAction = "Home") Then
''''''            DoAgain = False
''''''        End If
            Dim PatientDemographics As New PatientDemographics
            PatientDemographics.PatientId = PatId
            Call PatientDemographics.DisplayPatientFinancialScreen
            Set PatientDemographics = Nothing
            DoAgain = False
    Else
        DoAgain = False
    End If
Wend
End Sub

Private Sub cmdRemoveDenials_Click()
If (FM.IsFileThere(PinPointDirectory & "SummaryReports\ParsedDenials.txt")) Then
    FM.Kill (PinPointDirectory & "SummaryReports\ParsedDenials.txt")
End If
txtDenials.Text = "No Denial Worksheet currently in use."
End Sub

Private Sub cmdPrintDenials_Click()
    If (FM.IsFileThere(PinPointDirectory & "SummaryReports\ParsedDenials.txt")) Then
        Call PrintDocument("Denials", PinPointDirectory & "SummaryReports\ParsedDenials.txt", "")
    End If
End Sub

Private Sub cmdDone_Click()
Unload frmDenials
End Sub

Private Sub cmdParse_Click()
Dim ParseFile As String
If (FM.IsFileThere(PinPointDirectory & "SummaryReports\ParsedDenials.txt")) Then
    If (MsgBox("Delete current Denial Worksheet and create another?", vbYesNo) = vbNo) Then
        Exit Sub
    Else
        FM.Kill (PinPointDirectory & "SummaryReports\ParsedDenials.txt")
    End If
End If
ParseFile = Dir(PinPointDirectory & "SummaryReports\")
If (ParseFile = "") Then
    MsgBox ("No summary report available for processing.")
ElseIf (ParseSumForDenials(PinPointDirectory & "SummaryReports\" & ParseFile)) Then
    Call DisplayTextFile(txtDenials, PinPointDirectory & "SummaryReports\ParsedDenials.txt")
    Call FileCopy(PinPointDirectory & "SummaryReports\" & ParseFile, PinPointDirectory & "SummaryReports\Archived\" & ParseFile)
    If (FM.IsFileThere(PinPointDirectory & "SummaryReports\Archived\" & ParseFile)) Then
        FM.Kill (PinPointDirectory & "SummaryReports\" & ParseFile)
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub


