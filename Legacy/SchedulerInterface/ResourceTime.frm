VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmResourceTime 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstOTime 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      ItemData        =   "ResourceTime.frx":0000
      Left            =   9120
      List            =   "ResourceTime.frx":0002
      TabIndex        =   12
      Top             =   4680
      Width           =   2175
   End
   Begin VB.ListBox lstITime 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      ItemData        =   "ResourceTime.frx":0004
      Left            =   5760
      List            =   "ResourceTime.frx":0006
      TabIndex        =   11
      Top             =   4680
      Width           =   2175
   End
   Begin VB.ListBox lstHours 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2700
      ItemData        =   "ResourceTime.frx":0008
      Left            =   4560
      List            =   "ResourceTime.frx":000A
      TabIndex        =   6
      Top             =   1440
      Width           =   6735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   9480
      TabIndex        =   0
      Top             =   7800
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ResourceTime.frx":000C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   600
      TabIndex        =   1
      Top             =   7800
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ResourceTime.frx":01EB
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2655
      Left            =   600
      TabIndex        =   2
      Top             =   1440
      Width           =   3615
      _Version        =   65537
      _ExtentX        =   6376
      _ExtentY        =   4683
      _StockProps     =   76
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "2000/1/1"
      MaxDate         =   "2100/12/31"
      BevelColorFace  =   14285823
      BevelColorShadow=   14285823
      BevelColorHighlight=   8388608
      BevelColorFrame =   8388608
      BackColorSelected=   8388608
      ForeColorSelected=   8454143
      BevelWidth      =   1
      CaptionBevelWidth=   1
      CaptionBevelType=   0
      DayCaptionAlignment=   7
      CaptionAlignmentYear=   3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPost 
      Height          =   990
      Left            =   5040
      TabIndex        =   3
      Top             =   7800
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ResourceTime.frx":03CA
   End
   Begin VB.Label lblOTime 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "New Time Out"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   9120
      TabIndex        =   10
      Top             =   4320
      Visible         =   0   'False
      Width           =   1605
   End
   Begin VB.Label lblITime 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "New Time In"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   5760
      TabIndex        =   9
      Top             =   4320
      Visible         =   0   'False
      Width           =   1425
   End
   Begin VB.Label lblDate 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "For Date: MM/DD/YYYY"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   600
      TabIndex        =   8
      Top             =   4200
      Visible         =   0   'False
      Width           =   2640
   End
   Begin VB.Label lblHours 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Time In: HH:MM XX Time Out: HH:MM XX"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   600
      TabIndex        =   7
      Top             =   4560
      Visible         =   0   'False
      Width           =   4665
   End
   Begin VB.Label lblWeek 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Week Beginning: MM/DD/YYY"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4560
      TabIndex        =   5
      Top             =   1080
      Width           =   3390
   End
   Begin VB.Label lblResource 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Edit Time Clock for Resource"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   4440
      TabIndex        =   4
      Top             =   240
      Width           =   3300
   End
End
Attribute VB_Name = "frmResourceTime"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private ResourceId As Long
Private StartDate As String
Private EmpHoursTotal As Long
Private ModuleName As String
Private EmpHoursTbl As ADODB.Recordset


Private Sub cmdExit_Click()
Unload frmResourceTime
End Sub

Private Sub cmdHome_Click()
Unload frmResourceTime
End Sub

Private Sub cmdPost_Click()
Dim i As Integer
Dim ADate As String
Dim ITime As String, OTime As String
Dim DisplayText As String
Dim PrcAudit As PracticeAudit
Dim ICurId As Long, OCurId As Long
Dim UpdateQuery As String
Dim SelectQuery As String
Dim EmpHoursTotal As Integer
Dim Id As String
EmpHoursTotal = 0

If (lstHours.ListIndex >= 0) Then
    ADate = Left(lstHours.List(lstHours.ListIndex), 10)
    'Change this line to use the variable/TextBox/etc that holds the 12H time
          
    DisplayText = Mid(lstHours.List(lstHours.ListIndex), 60, Len(lstHours.List(lstHours.ListIndex)) - 59)
    i = InStrPS(DisplayText, "/")
    If (i > 0) Then
        Set PrcAudit = New PracticeAudit
        Id = Mid(DisplayText, i + 1, Len(DisplayText))
        If (lstITime.ListIndex > 0) Then
            ITime = Trim(lstITime.List(lstITime.ListIndex))
        End If
        If (lstOTime.ListIndex > 0) Then
            OTime = Trim(lstOTime.List(lstOTime.ListIndex))
        End If
    Dim dtToConvert, dtToConvert1 As Date
        Dim hourminute() As String
        Dim hourminute1() As String
         Dim converteddate, converteddate1 As String
         If (Trim(ITime) <> "") Then
         dtToConvert = CDate(ITime)
         converteddate = Format$(dtToConvert, "hh:mm")
         hourminute() = Split(converteddate, ":")
         End If
         If (Trim(OTime) <> "") Then
         dtToConvert1 = CDate(OTime)
         converteddate1 = Format$(dtToConvert1, "hh:mm")
         hourminute1() = Split(converteddate1, ":")
         End If
    SelectQuery = "select * from Model.EmployeeHours where  cast(userid as varchar(10))='" + CStr(ResourceId) + "' and"
    SelectQuery = SelectQuery & " convert(varchar(10), startdatetime,101)  ='" + ADate + "' AND Id = " & Id & ""
   
   Set EmpHoursTbl = Nothing
Set EmpHoursTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = SelectQuery
Call EmpHoursTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (EmpHoursTbl.EOF) Then
    EmpHoursTotal = 0
Else
    EmpHoursTotal = EmpHoursTbl.RecordCount
End If


If (Trim(ITime) <> "" And Trim(OTime) <> "") Then
      
        Dim ITimeMin As String, OTimeMin As String
        ITimeMin = ConvertTimeToMinutes(ITime)
        OTimeMin = ConvertTimeToMinutes(OTime)
        Dim DifferenceIntoTime As Integer
        DifferenceIntoTime = OTimeMin - ITimeMin
        If (DifferenceIntoTime >= 0) Then
        
        Else
    frmEventMsgs.Header = "Please select Correct Clock IN/Clock Out Time."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
      Exit Sub
   
    End If
Else
       frmEventMsgs.Header = "Please select Clock IN/Clock Out Time."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
     
End If

If (EmpHoursTotal > 0) Then
UpdateQuery = "update  Model.EmployeeHours set startdatetime= DATEADD(Hour," + hourminute(0) + ", DATEADD(Minute," + hourminute(1) + ",convert(varchar(10), startdatetime,101))) ,Enddatetime = DATEADD(Hour, " + hourminute1(0) + ", DATEADD(Minute, " + hourminute1(1) + ", convert(varchar(10), Enddatetime,101)))  "
                UpdateQuery = UpdateQuery & " Where cast(Model.EmployeeHours.UserId as varchar(10)) = " + CStr(ResourceId) + " "
                UpdateQuery = UpdateQuery & " and  convert(varchar(10),Model.EmployeeHours.startdatetime,101)= '" + ADate + "'"
                UpdateQuery = UpdateQuery & " and  Id = '" + Id + "'"

                '   MsgBox UpdateQuery
                Set EmpHoursTbl = Nothing
                Set EmpHoursTbl = CreateAdoRecordset
                MyPracticeRepositoryCmd.CommandType = adCmdText
                MyPracticeRepositoryCmd.CommandText = UpdateQuery
                Call EmpHoursTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
'      UpdateQuery = "update  Model.EmployeeHours set startdatetime=startdate,Enddatetime=enddate "
'        UpdateQuery = UpdateQuery & " From (select e1.*, DATEADD(Hour," + hourminute(0) + ", DATEADD(Minute," + hourminute(1) + ",convert(varchar(10), e1.startdatetime,101)) ) as startdate , DATEADD(Hour, " + hourminute1(0) + ", DATEADD(Minute, " + hourminute1(1) + ", convert(VarChar(10), e1.EndDateTime, 101))) As EndDate  "
'
'        UpdateQuery = UpdateQuery & " from Model.EmployeeHours e1 inner join"
'        UpdateQuery = UpdateQuery & " ( select Max(ID) ID, UserId from Model.EmployeeHours where"
'
'        UpdateQuery = UpdateQuery & " cast(userid as varchar(10))='" + CStr(ResourceId) + "' and  convert(varchar(10), startdatetime,101)= '" + ADate + "' group by userid) e2"
'        UpdateQuery = UpdateQuery & " on e1.ID=e2.ID and e1.UserId=e2.UserId ) A inner join"
'        UpdateQuery = UpdateQuery & " Model.EmployeeHours on A.Id= Model.EmployeeHours.ID"
'        UpdateQuery = UpdateQuery & " Where cast(Model.EmployeeHours.UserId as varchar(10)) = " + CStr(ResourceId) + " "
'        UpdateQuery = UpdateQuery & " and  convert(varchar(10),Model.EmployeeHours.startdatetime,101)= '" + ADate + "'"
'    '   MsgBox UpdateQuery
'         Set EmpHoursTbl = Nothing
'        Set EmpHoursTbl = CreateAdoRecordset
'        MyPracticeRepositoryCmd.CommandType = adCmdText
'        MyPracticeRepositoryCmd.CommandText = UpdateQuery
'        Call EmpHoursTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
       
  Else
  
  UpdateQuery = ""
  UpdateQuery = " INSERT INTO [model].[EmployeeHours] ([StartDateTime],[EndDateTime],[UserId],[Comment])"
  UpdateQuery = UpdateQuery & " select  DATEADD(Hour," + hourminute(0) + ", DATEADD(Minute," + hourminute(1) + ",convert(varchar(10), '" + ADate + "',101)) ) as startdate, "
  UpdateQuery = UpdateQuery & " DATEADD(Hour, " + hourminute1(0) + ", DATEADD(Minute, " + hourminute1(1) + ", convert(VarChar(10), '" + ADate + "', 101))) As EndDate "
  UpdateQuery = UpdateQuery & " , '" + CStr(ResourceId) + "','' "
 
       '' MsgBox UpdateQuery
         Set EmpHoursTbl = Nothing
        Set EmpHoursTbl = CreateAdoRecordset
        MyPracticeRepositoryCmd.CommandType = adCmdText
        MyPracticeRepositoryCmd.CommandText = UpdateQuery
        Call EmpHoursTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)

  
  End If
       
        Set PrcAudit = Nothing
        Call SetupTimes(ResourceId, StartDate)
    End If
End If
End Sub

Public Function LoadTimeSheets(RscId As Long) As Boolean
Dim TheDate As String
Dim RetRes As SchedulerResource
LoadTimeSheets = False
If (RscId > 0) Then
    ResourceId = RscId
    Set RetRes = New SchedulerResource
    RetRes.ResourceId = RscId
    If (RetRes.RetrieveSchedulerResource) Then
        lblResource.Caption = "Edit Time Clock for " + Trim(RetRes.ResourceDescription)
        TheDate = SSMonth1.DefaultDate
        Call FormatTodaysDate(TheDate, False)
        Call GetWeekStart(TheDate, StartDate)
        Call SetupTimes(RscId, StartDate)
    End If
    Set RetRes = Nothing
    LoadTimeSheets = True
End If
End Function

Private Function SetupTimes(RscId As Long, ADate As String) As Boolean
Dim p As Integer, k As Integer
Dim IRecId As Long, ORecId As Long
Dim ATime As Single, ATotal As Single
Dim ITime As Long, OTime As Long
Dim InTime As String, OutTime As String
Dim TheTotal As String
Dim NewDate As String, DisplayText As String
Dim PrcAudit As Audit
SetupTimes = False
lstHours.Clear
If (Trim(ADate) <> "") And (RscId > 0) Then
    SetupTimes = True
    ATotal = 0
    lblWeek.Caption = "Week Beginning: " + Trim(ADate)
    lblWeek.Visible = True
    lblDate.Visible = False
    lblHours.Visible = False
    lblITime.Visible = False
    lstITime.Visible = False
    lblOTime.Visible = False
    lstOTime.Visible = False
    cmdPost.Visible = False
    Set PrcAudit = New Audit
For p = 0 To 6
    Call AdjustDate(ADate, p, NewDate)
    k = 1
    DisplayText = Space(60)
    Call GetEmpHoursList(RscId, NewDate, "I")
    If Not EmpHoursTbl.RecordCount = 0 Then
        EmpHoursTbl.MoveFirst
    While Not EmpHoursTbl.EOF
        DisplayText = Space(60)
        If (EmpHoursTbl("Intime") <> "") Then
            InTime = EmpHoursTbl("Intime")
        Else
            InTime = "Not Set "
        End If
    If (EmpHoursTbl("Outtime") <> "") Then
        OutTime = EmpHoursTbl("Outtime")
    Else
        OutTime = "Not Set "
    End If
    If (Trim(InTime) <> "") Or (Trim(OutTime) <> "") Then
        ATime = EmpHoursTbl("Total")
        TheTotal = EmpHoursTbl("Total")
        Mid(DisplayText, 1, 10) = NewDate
        Mid(DisplayText, 12, 8) = InTime
        Mid(DisplayText, 22, 8) = OutTime
        If (val(Trim(TheTotal)) > 0) Then
        Mid(DisplayText, 32, 5) = TheTotal
        ATotal = ATotal + ATime
    ElseIf (val(Trim(TheTotal)) = 0) Then
        Mid(DisplayText, 32, 5) = "0.00"
    End If
        DisplayText = DisplayText + "/" + CStr(EmpHoursTbl("Id"))
        lstHours.AddItem DisplayText
    ElseIf (k = 1) Then
        Mid(DisplayText, 1, 10) = NewDate
        Mid(DisplayText, 12, 8) = "Not Set "
        Mid(DisplayText, 22, 8) = "Not Set "
        Mid(DisplayText, 32, 5) = "0.00"
        DisplayText = DisplayText + "/" + Trim(Str(ORecId))
        lstHours.AddItem DisplayText
    End If
    If (Trim(InTime) <> "") Or (Trim(OutTime) <> "") Then
        k = k + 1
    Else
        k = 0
    End If
    
        EmpHoursTbl.MoveNext
    Wend
    Else
        Mid(DisplayText, 1, 10) = NewDate
        Mid(DisplayText, 12, 8) = "Not Set "
        Mid(DisplayText, 22, 8) = "Not Set "
        Mid(DisplayText, 32, 5) = "0.00"
        DisplayText = DisplayText + Trim(Str(IRecId)) + "/" + Trim(Str(ORecId))
        lstHours.AddItem DisplayText
    End If
Next p
DisplayText = Space(60)
Mid(DisplayText, 1, 12) = "Total Hours:"
Mid(DisplayText, 15, 5) = Trim(Str(ATotal))
lstHours.AddItem DisplayText
Set PrcAudit = Nothing
End If
End Function



Private Function GetEmpHoursList(ResourceId As Long, TheDate As String, AuditType As String) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
EmpHoursTotal = 0
Ref = ""
GetEmpHoursList = -1
If (Trim(ResourceId) = "") Then
    Exit Function
End If

OrderString = "Select * , convert(char(8), startdatetime, 108) [Intime], convert(char(8), Enddatetime, 108) [Outtime]"
'OrderString = OrderString + ", DATEDIFF(hour, convert(char(8), startdatetime, 108), convert(char(8), Enddatetime, 108) ) diffTime"
 OrderString = OrderString + " ,ISNULL(CAST(CAST(DATEDIFF(minute,StartDateTime,EndDateTime)AS DECIMAL(6,2))/60 AS DECIMAL(6,2)), 0) AS Total"
OrderString = OrderString + " FROM Model.EmployeeHours WHERE "

If (Trim(TheDate) <> "") Then
    OrderString = OrderString + Ref + " convert(varchar(10), startdatetime,101)  = '" + UCase(Trim(TheDate)) + "' "
    Ref = "And "
End If
'If (AuditTime > 0) Then
'    OrderString = OrderString + Ref + "Time >= " + Trim(Str(AuditTime)) + " "
'    Ref = "And "
'End If
If (Trim(ResourceId) <> "") Then
    OrderString = OrderString + Ref + "UserId =" + Str(ResourceId) + " "
    Ref = "And "
End If
'If (Trim(AuditTableId) > 0) Then
'    OrderString = OrderString + Ref + "TableId = " + Trim(Str(AuditTableId)) + " "
'    Ref = "And "
'End If
'If (Trim(AuditRecordId) > 0) Then
'    OrderString = OrderString + Ref + "RecordId = " + Trim(Str(AuditRecordId)) + " "
'    Ref = "And "
'End If
'If (Trim(AuditType) <> "") Then
'    OrderString = OrderString + Ref + "Action = '" + Trim(AuditType) + "' "
'    Ref = "And "
'End If
OrderString = OrderString + "ORDER BY StartDateTime ASC"
Set EmpHoursTbl = Nothing
Set EmpHoursTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call EmpHoursTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (EmpHoursTbl.EOF) Then
    GetEmpHoursList = -1
   ' Call InitAudit
Else

    'EmpHoursTbl.MoveLast
    EmpHoursTotal = EmpHoursTbl.RecordCount
    GetEmpHoursList = EmpHoursTotal
    

End If
Exit Function
DbErrorHandler:
    ModuleName = "GetEmpHoursList"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Sub lstHours_Click()
Dim i As Integer
Dim ADate As String
Dim ITime As String
Dim OTime As String
Dim DisplayText As String
If (lstHours.ListIndex >= 0) Then
    Call LoadTime(lstITime, "", "", 5)
    Call LoadTime(lstOTime, "", "", 5)
    DisplayText = lstHours.List(lstHours.ListIndex)
    ADate = Mid(DisplayText, 1, 10)
    ITime = Mid(DisplayText, 12, 8)
    OTime = Mid(DisplayText, 22, 8)
    lblDate.Caption = "For Date " + Trim(ADate)
    lblDate.Visible = True
    lblHours.Caption = "Time IN: " + Trim(ITime) + " Time OUT: " + Trim(OTime)
    lblHours.Visible = True
    lblITime.Visible = True
    ITime = Left(ITime, 5) + " " + Mid(ITime, 6, 2)
    lstITime.ListIndex = -1
    For i = 0 To lstITime.ListCount - 1
        If (Trim(ITime) = lstITime.List(i)) Then
            lstITime.ListIndex = i
            Exit For
        End If
    Next i
    lstITime.Visible = True
    lblOTime.Visible = True
    OTime = Left(OTime, 5) + " " + Mid(OTime, 6, 2)
    lstOTime.ListIndex = -1
    For i = 0 To lstOTime.ListCount - 1
        If (Trim(OTime) = lstOTime.List(i)) Then
            lstOTime.ListIndex = i
            Exit For
        End If
    Next i
    lstOTime.Visible = True
    cmdPost.Visible = True
End If
End Sub

Private Sub SSMonth1_Click()
Dim ADate As String
ADate = SSMonth1.DateValue
Call FormatTodaysDate(ADate, False)
Call GetWeekStart(ADate, StartDate)
Call SetupTimes(ResourceId, StartDate)
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub

