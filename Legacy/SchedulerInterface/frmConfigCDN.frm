VERSION 5.00
Begin VB.Form frmConfigCDN 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Role Configuration"
   ClientHeight    =   7065
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8850
   ForeColor       =   &H0080FFFF&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7065
   ScaleWidth      =   8850
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdQuit 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Quit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   960
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   5400
      Width           =   1335
   End
   Begin VB.CommandButton cmdDone 
      BackColor       =   &H009B9626&
      Caption         =   "Done"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5760
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   5280
      Width           =   1335
   End
   Begin VB.CheckBox chkConfig5 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   2520
      TabIndex        =   23
      Top             =   4200
      Width           =   495
   End
   Begin VB.CheckBox chkDs5 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   4440
      TabIndex        =   22
      Top             =   4200
      Width           =   495
   End
   Begin VB.CheckBox chkFb5 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   6000
      TabIndex        =   21
      Top             =   4200
      Width           =   495
   End
   Begin VB.CheckBox chkFb4 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   6000
      TabIndex        =   19
      Top             =   3600
      Width           =   495
   End
   Begin VB.CheckBox chkFb3 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   6000
      TabIndex        =   18
      Top             =   3000
      Width           =   495
   End
   Begin VB.CheckBox chkFb2 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   6000
      TabIndex        =   17
      Top             =   2400
      Width           =   495
   End
   Begin VB.CheckBox chkFb1 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   6000
      TabIndex        =   16
      Top             =   1800
      Width           =   495
   End
   Begin VB.CheckBox chkDs4 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   4440
      TabIndex        =   15
      Top             =   3600
      Width           =   495
   End
   Begin VB.CheckBox chkDs3 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   4440
      TabIndex        =   14
      Top             =   3000
      Width           =   495
   End
   Begin VB.CheckBox chkDs2 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   4440
      TabIndex        =   13
      Top             =   2400
      Width           =   495
   End
   Begin VB.CheckBox chkDs1 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   4440
      TabIndex        =   12
      Top             =   1800
      Width           =   495
   End
   Begin VB.CheckBox chkConfig4 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   2520
      TabIndex        =   11
      Top             =   3600
      Width           =   495
   End
   Begin VB.CheckBox chkConfig3 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   2520
      TabIndex        =   10
      Top             =   3000
      Width           =   495
   End
   Begin VB.CheckBox chkConfig2 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   2520
      TabIndex        =   9
      Top             =   2400
      Width           =   495
   End
   Begin VB.CheckBox chkConfig1 
      BackColor       =   &H00E0E0E0&
      Height          =   495
      Left            =   2520
      TabIndex        =   8
      Top             =   1800
      Width           =   495
   End
   Begin VB.Label Label10 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   495
      Left            =   -600
      TabIndex        =   24
      Top             =   120
      Width           =   2415
   End
   Begin VB.Label Label9 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Technician"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404000&
      Height          =   375
      Left            =   960
      TabIndex        =   20
      Top             =   4320
      Width           =   1215
   End
   Begin VB.Label Label8 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Billing"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404000&
      Height          =   375
      Left            =   960
      TabIndex        =   7
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label Label7 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404000&
      Height          =   375
      Left            =   960
      TabIndex        =   6
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Label Label6 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Front Desk"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404000&
      Height          =   375
      Left            =   960
      TabIndex        =   5
      Top             =   3720
      Width           =   1215
   End
   Begin VB.Label Label5 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Admin"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404000&
      Height          =   375
      Left            =   960
      TabIndex        =   4
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label4 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Enable Intervention"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   615
      Left            =   4320
      TabIndex        =   3
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label3 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Enable Reference Resources"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   615
      Left            =   5880
      TabIndex        =   2
      Top             =   1080
      Width           =   2415
   End
   Begin VB.Label Label2 
      BackColor       =   &H00E0E0E0&
      Caption         =   "CDS Configuration"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   615
      Left            =   2520
      TabIndex        =   1
      Top             =   1080
      Width           =   1695
   End
   Begin VB.Label Label1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Roles"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   960
      TabIndex        =   0
      Top             =   1080
      Width           =   1215
   End
End
Attribute VB_Name = "frmConfigCDN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub chkDs1_Click()
If chkDs1.Value = 0 Then
    chkFb1.Value = 0
    chkFb1.Enabled = False
Else
    chkFb1.Enabled = True
End If
End Sub

Private Sub chkDs2_Click()
If chkDs2.Value = 0 Then
    chkFb2.Value = 0
    chkFb2.Enabled = False
Else
    chkFb2.Enabled = True
End If
End Sub

Private Sub chkDs3_Click()
If chkDs3.Value = 0 Then
    chkFb3.Value = 0
    chkFb3.Enabled = False
Else
    chkFb3.Enabled = True
End If
End Sub

Private Sub chkDs4_Click()
If chkDs4.Value = 0 Then
    chkFb4.Value = 0
    chkFb4.Enabled = False
Else
    chkFb4.Enabled = True
End If
End Sub

Private Sub chkDs5_Click()
If chkDs5.Value = 0 Then
    chkFb5.Value = 0
    chkFb5.Enabled = False

Else
    chkFb5.Enabled = True
    End If
End Sub

Private Sub cmdDone_Click()
Dim Index As Integer
For Index = 1 To 5
    Dim objUserCDS As New UserCDS
    Select Case Index
            Case 1
                 objUserCDS.ResourceType = "O"
                 objUserCDS.CDSConfiguration = chkConfig1.Value
                 objUserCDS.Alerts = chkDs1.Value
                 objUserCDS.Info = chkFb1.Value
            Case 2
                objUserCDS.ResourceType = "S"
                objUserCDS.CDSConfiguration = chkConfig2.Value
                 objUserCDS.Alerts = chkDs2.Value
                 objUserCDS.Info = chkFb2.Value
            Case 3
                objUserCDS.ResourceType = "D"
                objUserCDS.CDSConfiguration = chkConfig3.Value
                 objUserCDS.Alerts = chkDs3.Value
                 objUserCDS.Info = chkFb3.Value
            Case 4
                objUserCDS.ResourceType = "F"
                objUserCDS.CDSConfiguration = chkConfig4.Value
                 objUserCDS.Alerts = chkDs4.Value
                 objUserCDS.Info = chkFb4.Value
            Case 5
                objUserCDS.ResourceType = "T"
                objUserCDS.CDSConfiguration = chkConfig5.Value
                 objUserCDS.Alerts = chkDs5.Value
                 objUserCDS.Info = chkFb5.Value
    End Select
    objUserCDS.UpdateCDS objUserCDS
Next
Me.Hide
End Sub

Private Sub cmdPractice_Click()

End Sub

Private Sub cmdQuit_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
Dim objUserCDS As New UserCDS
Dim RCount As Integer
Dim Index As Integer
RCount = objUserCDS.GetCds
For Index = 1 To RCount
    objUserCDS.SelectCDS (Index)
    Select Case objUserCDS.ResourceType
            Case "O"
                chkConfig1.Value = objUserCDS.CDSConfiguration
                chkDs1.Value = objUserCDS.Alerts
                If chkDs1.Value = 0 Then
                    chkFb1.Enabled = False
                    chkFb1.Value = 0
                Else
                    chkFb1.Value = objUserCDS.Info
                End If
            Case "S"
                chkConfig2.Value = objUserCDS.CDSConfiguration
                chkDs2.Value = objUserCDS.Alerts
                If chkDs2.Value = 0 Then
                    chkFb2.Enabled = False
                    chkFb2.Value = 0
                Else
                    chkFb2.Value = objUserCDS.Info
                End If
                 
            Case "D"
                chkConfig3.Value = objUserCDS.CDSConfiguration
                chkDs3.Value = objUserCDS.Alerts
                If chkDs3.Value = 0 Then
                    chkFb3.Enabled = False
                    chkFb3.Value = 0
                Else
                    chkFb3.Value = objUserCDS.Info
                End If
            Case "F"
                chkConfig4.Value = objUserCDS.CDSConfiguration
                chkDs4.Value = objUserCDS.Alerts
                If chkDs4.Value = 0 Then
                    chkFb4.Enabled = False
                    chkFb4.Value = 0
                Else
                    chkFb4.Value = objUserCDS.Info
                End If
            Case "T"
                chkConfig5.Value = objUserCDS.CDSConfiguration
                chkDs5.Value = objUserCDS.Alerts
                If chkDs5.Value = 0 Then
                    chkFb5.Enabled = False
                    chkFb5.Value = 0
                Else
                    chkFb5.Value = objUserCDS.Info
                End If
    End Select
Next
End Sub

Private Sub fpBtn1_Click()

End Sub

