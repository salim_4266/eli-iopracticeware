VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmPatientFinancial 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtOPolId 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5280
      MaxLength       =   20
      TabIndex        =   93
      Top             =   4440
      Visible         =   0   'False
      Width           =   3615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   2760
      TabIndex        =   63
      Top             =   4920
      Visible         =   0   'False
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFDone 
      Height          =   990
      Left            =   7920
      TabIndex        =   62
      Top             =   4920
      Visible         =   0   'False
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":01DF
   End
   Begin VB.Frame fraAllocate 
      BackColor       =   &H00FF0000&
      BorderStyle     =   0  'None
      Height          =   3375
      Left            =   2400
      TabIndex        =   87
      Top             =   2640
      Visible         =   0   'False
      Width           =   6855
      Begin VB.TextBox txtOEDate 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2880
         MaxLength       =   10
         TabIndex        =   61
         Top             =   1320
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.TextBox txtOSDate 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2880
         MaxLength       =   10
         TabIndex        =   60
         Top             =   840
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lblOPolId 
         BackColor       =   &H00FF0000&
         Caption         =   "Policy #"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   1560
         TabIndex        =   94
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label lblAllocate 
         Alignment       =   2  'Center
         BackColor       =   &H00FF0000&
         Caption         =   "Change Past Policy Dates"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   1800
         TabIndex        =   90
         Top             =   240
         Visible         =   0   'False
         Width           =   3135
      End
      Begin VB.Label lblOEDate 
         BackColor       =   &H00FF0000&
         Caption         =   "End Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   1560
         TabIndex        =   89
         Top             =   1320
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblOSDate 
         BackColor       =   &H00FF0000&
         Caption         =   "Start Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   1560
         TabIndex        =   88
         Top             =   840
         Visible         =   0   'False
         Width           =   1215
      End
   End
   Begin VB.ComboBox lstInsType 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      ItemData        =   "PatientFinancial.frx":03BE
      Left            =   2760
      List            =   "PatientFinancial.frx":03CE
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   2160
      Width           =   1695
   End
   Begin VB.TextBox txtFirstGroup2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   16
      Top             =   3000
      Width           =   1215
   End
   Begin VB.TextBox txtFirstGroup3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   23
      Top             =   3480
      Width           =   1215
   End
   Begin VB.TextBox txtFirstGroup1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   8
      Top             =   2520
      Width           =   1215
   End
   Begin VB.TextBox txtSecondGroup2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   42
      Top             =   6840
      Width           =   1215
   End
   Begin VB.TextBox txtSecondGroup3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   50
      Top             =   7320
      Width           =   1215
   End
   Begin VB.TextBox txtSecondGroup1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8040
      MaxLength       =   32
      TabIndex        =   34
      Top             =   6360
      Width           =   1215
   End
   Begin VB.CheckBox chkBill2 
      BackColor       =   &H0077742D&
      Caption         =   "Bill Policyholder"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      TabIndex        =   29
      Top             =   4320
      Width           =   1815
   End
   Begin VB.CheckBox chkBill1 
      BackColor       =   &H0077742D&
      Caption         =   "Bill Policyholder"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6240
      TabIndex        =   2
      Top             =   720
      Width           =   1815
   End
   Begin VB.TextBox txtCopayS3 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      MaxLength       =   10
      TabIndex        =   48
      Top             =   7320
      Width           =   615
   End
   Begin VB.TextBox txtCopayS2 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      MaxLength       =   10
      TabIndex        =   40
      Top             =   6840
      Width           =   615
   End
   Begin VB.TextBox txtCopayS1 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      MaxLength       =   10
      TabIndex        =   32
      Top             =   6360
      Width           =   615
   End
   Begin VB.TextBox txtCopayP3 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      MaxLength       =   10
      TabIndex        =   21
      Top             =   3480
      Width           =   615
   End
   Begin VB.TextBox txtCopayP2 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      MaxLength       =   10
      TabIndex        =   14
      Top             =   3000
      Width           =   615
   End
   Begin VB.TextBox txtCopayP1 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      MaxLength       =   10
      TabIndex        =   6
      Top             =   2520
      Width           =   615
   End
   Begin VB.TextBox txtSecond3 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   46
      Top             =   7320
      Width           =   375
   End
   Begin VB.TextBox txtSecond2 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   38
      Top             =   6840
      Width           =   375
   End
   Begin VB.TextBox txtSecond1 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   30
      Top             =   6360
      Width           =   375
   End
   Begin VB.TextBox txtPrimary3 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   20
      Top             =   3480
      Width           =   375
   End
   Begin VB.TextBox txtPrimary2 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   12
      Top             =   3000
      Width           =   375
   End
   Begin VB.TextBox txtPrimary1 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MaxLength       =   1
      TabIndex        =   4
      Top             =   2520
      Width           =   375
   End
   Begin VB.TextBox txtRel1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   6960
      TabIndex        =   1
      Top             =   1080
      Width           =   555
   End
   Begin VB.CheckBox chkDelete1 
      BackColor       =   &H00A95911&
      Caption         =   "Check1"
      Height          =   255
      Left            =   11520
      TabIndex        =   11
      Top             =   2520
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CheckBox chkDelete2 
      BackColor       =   &H00A95911&
      Caption         =   "Check1"
      Height          =   255
      Left            =   11520
      TabIndex        =   19
      Top             =   3000
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CheckBox chkDelete3 
      BackColor       =   &H00A95911&
      Caption         =   "Check1"
      Height          =   255
      Left            =   11520
      TabIndex        =   26
      Top             =   3480
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CheckBox chkDelete4 
      BackColor       =   &H00A95911&
      Caption         =   "Check1"
      Height          =   255
      Left            =   11520
      TabIndex        =   37
      Top             =   6360
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CheckBox chkDelete5 
      BackColor       =   &H00A95911&
      Caption         =   "Check1"
      Height          =   255
      Left            =   11520
      TabIndex        =   45
      Top             =   6840
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CheckBox chkDelete6 
      BackColor       =   &H00A95911&
      Caption         =   "Check1"
      Height          =   255
      Left            =   11520
      TabIndex        =   53
      Top             =   7320
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtSecondEDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   44
      Top             =   6840
      Width           =   975
   End
   Begin VB.TextBox txtSecondEDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   36
      Top             =   6360
      Width           =   975
   End
   Begin VB.TextBox txtSecondEDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   52
      Top             =   7320
      Width           =   975
   End
   Begin VB.TextBox txtPrimaryEDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   18
      Top             =   3000
      Width           =   975
   End
   Begin VB.TextBox txtPrimaryEDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   10
      Top             =   2520
      Width           =   975
   End
   Begin VB.TextBox txtPrimaryEDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   10440
      MaxLength       =   10
      TabIndex        =   25
      Top             =   3480
      Width           =   975
   End
   Begin VB.TextBox txtSecondSDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      MaxLength       =   10
      TabIndex        =   43
      Top             =   6840
      Width           =   975
   End
   Begin VB.TextBox txtSecondSDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      MaxLength       =   10
      TabIndex        =   35
      Top             =   6360
      Width           =   975
   End
   Begin VB.TextBox txtSecondSDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      MaxLength       =   10
      TabIndex        =   51
      Top             =   7320
      Width           =   975
   End
   Begin VB.TextBox txtPrimarySDate2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      MaxLength       =   10
      TabIndex        =   17
      Top             =   3000
      Width           =   975
   End
   Begin VB.TextBox txtPrimarySDate1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      MaxLength       =   10
      TabIndex        =   9
      Top             =   2520
      Width           =   975
   End
   Begin VB.TextBox txtPrimarySDate3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9360
      MaxLength       =   10
      TabIndex        =   24
      Top             =   3480
      Width           =   975
   End
   Begin VB.TextBox txtSecondMember3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   49
      Top             =   7320
      Width           =   2535
   End
   Begin VB.TextBox txtPrimaryMember3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   22
      Top             =   3480
      Width           =   2535
   End
   Begin VB.ListBox lstOldPolicy1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   540
      ItemData        =   "PatientFinancial.frx":0403
      Left            =   120
      List            =   "PatientFinancial.frx":0405
      TabIndex        =   74
      Top             =   1560
      Width           =   9705
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   58
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":0407
   End
   Begin VB.TextBox txtSecondMember2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   41
      Top             =   6840
      Width           =   2535
   End
   Begin VB.TextBox txtSecondMember1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   33
      Top             =   6360
      Width           =   2535
   End
   Begin VB.TextBox FirstPolicyHolder 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      MaxLength       =   35
      TabIndex        =   0
      Top             =   1080
      Width           =   5325
   End
   Begin VB.TextBox txtPrimaryMember1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   7
      Top             =   2520
      Width           =   2535
   End
   Begin VB.TextBox txtPrimaryMember2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5400
      MaxLength       =   32
      TabIndex        =   15
      Top             =   3000
      Width           =   2535
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   59
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":05E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemove1 
      Height          =   630
      Left            =   10440
      TabIndex        =   54
      Top             =   1440
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1111
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":07C5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemove2 
      Height          =   630
      Left            =   10440
      TabIndex        =   55
      Top             =   5040
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1111
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":09A6
   End
   Begin VB.ListBox lstOldPolicy2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   540
      ItemData        =   "PatientFinancial.frx":0B87
      Left            =   120
      List            =   "PatientFinancial.frx":0B89
      TabIndex        =   73
      Top             =   5160
      Width           =   9705
   End
   Begin VB.TextBox SecondPolicyHolder 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      MaxLength       =   35
      TabIndex        =   27
      Top             =   4680
      Width           =   5295
   End
   Begin VB.TextBox txtRel2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   7080
      TabIndex        =   28
      Top             =   4680
      Width           =   615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDependent 
      Height          =   990
      Left            =   7200
      TabIndex        =   57
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":0B8B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   4920
      TabIndex        =   56
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":0D7B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDemo1 
      Height          =   630
      Left            =   10440
      TabIndex        =   95
      Top             =   720
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1111
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":0F5B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDemo2 
      Height          =   630
      Left            =   10440
      TabIndex        =   96
      Top             =   4320
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1111
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":113D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdShift 
      Height          =   990
      Left            =   2520
      TabIndex        =   99
      Top             =   7920
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientFinancial.frx":131F
   End
   Begin VB.Line Line3 
      BorderColor     =   &H000000C0&
      BorderWidth     =   3
      X1              =   120
      X2              =   11880
      Y1              =   7800
      Y2              =   7800
   End
   Begin VB.Line Line2 
      BorderColor     =   &H000000C0&
      BorderWidth     =   3
      X1              =   120
      X2              =   11880
      Y1              =   600
      Y2              =   600
   End
   Begin VB.Label lblHldId 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Policy Holder ID:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8040
      TabIndex        =   98
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label lblPatId 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Patient ID:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   7200
      TabIndex        =   97
      Top             =   120
      Width           =   915
   End
   Begin VB.Label Label18 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Group #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8040
      TabIndex        =   92
      Top             =   2160
      Width           =   1065
   End
   Begin VB.Label Label17 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Group #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8040
      TabIndex        =   91
      Top             =   6000
      Width           =   1185
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Relationship"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   5715
      TabIndex        =   86
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Relationship"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   5850
      TabIndex        =   85
      Top             =   4680
      Width           =   1095
   End
   Begin VB.Label Label12 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Copay"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      TabIndex        =   84
      Top             =   6000
      Width           =   705
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Copay"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4680
      TabIndex        =   83
      Top             =   2160
      Width           =   705
   End
   Begin VB.Label lblDelete2 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   11400
      TabIndex        =   82
      Top             =   6000
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.Label lblDelete1 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   11400
      TabIndex        =   81
      Top             =   2160
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10440
      TabIndex        =   80
      Top             =   6000
      Width           =   945
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "End Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10440
      TabIndex        =   79
      Top             =   2160
      Width           =   945
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   9360
      TabIndex        =   78
      Top             =   6000
      Width           =   1065
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Start Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   9360
      TabIndex        =   77
      Top             =   2160
      Width           =   1065
   End
   Begin VB.Label lblSecondInsurer3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   47
      Top             =   7320
      Width           =   4095
   End
   Begin VB.Label lblPrimaryInsurer3 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   67
      Top             =   3480
      Width           =   4095
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Past Policies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8520
      TabIndex        =   76
      Top             =   1200
      Width           =   1155
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Past Policies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8595
      TabIndex        =   75
      Top             =   4800
      Width           =   1185
   End
   Begin VB.Label lblSecondInsurer2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   39
      Top             =   6840
      Width           =   4095
   End
   Begin VB.Label lblSecondInsurer1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   31
      Top             =   6360
      Width           =   4095
   End
   Begin VB.Label lblPrimaryInsurer1 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   5
      Top             =   2520
      Width           =   4095
   End
   Begin VB.Label lblPrimaryInsurer2 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   480
      TabIndex        =   13
      Top             =   3000
      Width           =   4095
   End
   Begin VB.Label lblPatientName 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   3120
      TabIndex        =   72
      Top             =   120
      Width           =   4005
   End
   Begin VB.Label Label19 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Policy #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5400
      TabIndex        =   71
      Top             =   6000
      Width           =   1305
   End
   Begin VB.Label Label16 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   480
      TabIndex        =   70
      Top             =   6000
      Width           =   2385
   End
   Begin VB.Label Label15 
      Appearance      =   0  'Flat
      BackColor       =   &H000000C0&
      Caption         =   " Another Policy Holder for the patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   120
      TabIndex        =   69
      Top             =   4320
      Width           =   5265
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   " Financial Responsibility for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   68
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H000000C0&
      Caption         =   " Person holding the patient's primary policy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   120
      TabIndex        =   66
      Top             =   720
      Width           =   5325
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   480
      TabIndex        =   65
      Top             =   2160
      Width           =   2385
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Policy #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   5400
      TabIndex        =   64
      Top             =   2160
      Width           =   1305
   End
   Begin VB.Line Line1 
      BorderColor     =   &H000000C0&
      BorderWidth     =   3
      X1              =   120
      X2              =   11880
      Y1              =   4080
      Y2              =   4080
   End
End
Attribute VB_Name = "frmPatientFinancial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public PatientId As Long
Public ThePolicyHolder1 As Long
Public ThePolicyHolder1Rel As String
Public ThePolicyHolder1Bill As Boolean
Public ThePolicyHolder2 As Long
Public ThePolicyHolder2Rel As String
Public ThePolicyHolder2Bill As Boolean
Public ArchiveChanged As Boolean

Private AllowOtherPartyAccess1 As Boolean
Private AllowOtherPartyAccess2 As Boolean
Private FormLoadOn As Boolean
Private ValidatePolicyNumbers As Boolean
Private FirstTimeIn As Boolean
Private AllowAccessOn As Boolean
Private PostButRemain As Boolean
Private PostButRemainFailed As Boolean
Private InsuranceType As String
Private PrimaryFirstCurrent As Long
Private PrimarySecondCurrent As Long
Private PrimaryThirdCurrent As Long
Private SecondFirstCurrent As Long
Private SecondSecondCurrent As Long
Private SecondThirdCurrent As Long

Private PolicyHolder_1 As Long
Private OriginalPolicyHolder_1 As Long
Private FirstInsurerId1 As Long
Private OriginalPrimaryInsurerId1 As Long
Private OriginalPrimaryMember1 As String
Private OriginalPrimaryGroup1 As String
Private OriginalPrimary1 As String
Private OriginalPrimarySDate1 As String
Private OriginalPrimaryEDate1 As String
Private OriginalPrimaryCopay1 As String
Private FirstInsurerId2 As Long
Private OriginalPrimaryInsurerId2 As Long
Private OriginalPrimaryMember2 As String
Private OriginalPrimaryGroup2 As String
Private OriginalPrimary2 As String
Private OriginalPrimarySDate2 As String
Private OriginalPrimaryEDate2 As String
Private OriginalPrimaryCopay2 As String
Private FirstInsurerId3 As Long
Private OriginalPrimaryInsurerId3 As Long
Private OriginalPrimaryMember3 As String
Private OriginalPrimaryGroup3 As String
Private OriginalPrimary3 As String
Private OriginalPrimarySDate3 As String
Private OriginalPrimaryEDate3 As String
Private OriginalPrimaryCopay3 As String

Private PolicyHolder_2 As Long
Private OriginalPolicyHolder_2 As Long
Private SecondInsurerId1 As Long
Private OriginalSecondInsurerId1 As Long
Private OriginalSecondMember1 As String
Private OriginalSecondGroup1 As String
Private OriginalSecond1 As String
Private OriginalSecondSDate1 As String
Private OriginalSecondEDate1 As String
Private OriginalSecondCopay1 As String
Private SecondInsurerId2 As Long
Private OriginalSecondInsurerId2 As Long
Private OriginalSecondMember2 As String
Private OriginalSecondGroup2 As String
Private OriginalSecond2 As String
Private OriginalSecondSDate2 As String
Private OriginalSecondEDate2 As String
Private OriginalSecondCopay2 As String
Private SecondInsurerId3 As Long
Private OriginalSecondInsurerId3 As Long
Private OriginalSecondMember3 As String
Private OriginalSecondGroup3 As String
Private OriginalSecond3 As String
Private OriginalSecondSDate3 As String
Private OriginalSecondEDate3 As String
Private OriginalSecondCopay3 As String

Private RetrieveInsurer As Insurer
Private RetrievePatientFinancial As PatientFinance
Private RetrieveAnotherPatient As Patient
Private selectedindicator As String



Private Function verifyChanged() As Boolean
verifyChanged = False
If (PolicyHolder_1 <> OriginalPolicyHolder_1) Then
    verifyChanged = True
End If
If (FirstInsurerId1 <> OriginalPrimaryInsurerId1) Then
    verifyChanged = True
End If
If (UCase(Trim(txtPrimaryMember1.Text)) <> OriginalPrimaryMember1) Then
    verifyChanged = True
End If
If (UCase(Trim(txtFirstGroup1.Text)) <> OriginalPrimaryGroup1) Then
    verifyChanged = True
End If
If (Trim(txtPrimarySDate1.Text) <> OriginalPrimarySDate1) Then
    verifyChanged = True
End If
If (Trim(txtPrimaryEDate1.Text) <> OriginalPrimaryEDate1) Then
    verifyChanged = True
End If
If (Trim(txtCopayP1.Text) <> OriginalPrimaryCopay1) Then
    verifyChanged = True
End If
If (txtPrimary1.Text <> OriginalPrimary1) Then
    verifyChanged = True
End If
If (FirstInsurerId2 <> OriginalPrimaryInsurerId2) Then
    verifyChanged = True
End If
If (UCase(Trim(txtPrimaryMember2.Text)) <> OriginalPrimaryMember2) Then
    verifyChanged = True
End If
If (UCase(Trim(txtFirstGroup2.Text)) <> OriginalPrimaryGroup2) Then
    verifyChanged = True
End If
If (Trim(txtPrimarySDate2.Text) <> OriginalPrimarySDate2) Then
    verifyChanged = True
End If
If (Trim(txtPrimaryEDate2.Text) <> OriginalPrimaryEDate2) Then
    verifyChanged = True
End If
If (Trim(txtCopayP2.Text) <> OriginalPrimaryCopay2) Then
    verifyChanged = True
End If
If (txtPrimary2.Text <> OriginalPrimary2) Then
    verifyChanged = True
End If
If (FirstInsurerId3 <> OriginalPrimaryInsurerId3) Then
    verifyChanged = True
End If
If (UCase(Trim(txtPrimaryMember3.Text)) <> OriginalPrimaryMember3) Then
    verifyChanged = True
End If
If (UCase(Trim(txtFirstGroup3.Text)) <> OriginalPrimaryGroup3) Then
    verifyChanged = True
End If
If (Trim(txtPrimarySDate3.Text) <> OriginalPrimarySDate3) Then
    verifyChanged = True
End If
If (Trim(txtPrimaryEDate3.Text) <> OriginalPrimaryEDate3) Then
    verifyChanged = True
End If
If (Trim(txtCopayP3.Text) <> OriginalPrimaryCopay3) Then
    verifyChanged = True
End If
If (txtPrimary3.Text <> OriginalPrimary3) Then
    verifyChanged = True
End If

If (PolicyHolder_2 <> OriginalPolicyHolder_2) Then
    verifyChanged = True
End If
If (SecondInsurerId1 <> OriginalSecondInsurerId1) Then
    verifyChanged = True
End If
If (UCase(Trim(txtSecondMember1.Text)) <> OriginalSecondMember1) Then
    verifyChanged = True
End If
If (UCase(Trim(txtSecondGroup1.Text)) <> OriginalSecondGroup1) Then
    verifyChanged = True
End If
If (Trim(txtSecondSDate1.Text) <> OriginalSecondSDate1) Then
    verifyChanged = True
End If
If (Trim(txtSecondEDate1.Text) <> OriginalSecondEDate1) Then
    verifyChanged = True
End If
If (Trim(txtCopayS1.Text) <> OriginalSecondCopay1) Then
    verifyChanged = True
End If
If (txtSecond1.Text <> OriginalSecond1) Then
    verifyChanged = True
End If
If (SecondInsurerId2 <> OriginalSecondInsurerId2) Then
    verifyChanged = True
End If
If (UCase(Trim(txtSecondMember2.Text)) <> OriginalSecondMember2) Then
    verifyChanged = True
End If
If (UCase(Trim(txtSecondGroup2.Text)) <> OriginalPrimaryGroup2) Then
    verifyChanged = True
End If
If (Trim(txtSecondSDate2.Text) <> OriginalSecondSDate2) Then
    verifyChanged = True
End If
If (Trim(txtSecondEDate2.Text) <> OriginalSecondEDate2) Then
    verifyChanged = True
End If
If (Trim(txtCopayS2.Text) <> OriginalSecondCopay2) Then
    verifyChanged = True
End If
If (txtSecond2.Text <> OriginalSecond2) Then
    verifyChanged = True
End If
If (SecondInsurerId3 <> OriginalSecondInsurerId3) Then
    verifyChanged = True
End If
If (UCase(Trim(txtSecondMember3.Text)) <> OriginalSecondMember3) Then
    verifyChanged = True
End If
If (UCase(Trim(txtSecondGroup3.Text)) <> OriginalPrimaryGroup3) Then
    verifyChanged = True
End If
If (Trim(txtSecondSDate3.Text) <> OriginalSecondSDate3) Then
    verifyChanged = True
End If
If (Trim(txtSecondEDate3.Text) <> OriginalSecondEDate3) Then
    verifyChanged = True
End If
If (Trim(txtCopayS3.Text) <> OriginalSecondCopay3) Then
    verifyChanged = True
End If
If (txtSecond3.Text <> OriginalSecond3) Then
    verifyChanged = True
End If
End Function

Private Sub chkBill1_Click()
If (chkBill1.Value = 1) Then
    ThePolicyHolder1Bill = True
Else
    ThePolicyHolder1Bill = False
End If
End Sub

Private Sub chkBill2_Click()
If (chkBill2.Value = 1) Then
    ThePolicyHolder2Bill = True
Else
    ThePolicyHolder2Bill = False
End If
End Sub

Public Function FalseEnddate() As Boolean
FalseEnddate = False

Dim SDate As String
Dim SDate2 As String
Dim SDate3 As String


SDate = txtPrimarySDate1.Text
SDate2 = txtPrimarySDate2.Text
SDate3 = txtPrimarySDate3.Text

If Not SDate = "" Then
If Not ArchiveFinancialStartdate(PrimaryFirstCurrent, SDate, IIf(OriginalPrimary1 = "", "1", OriginalPrimary1)) Then
 FalseEnddate = True
 Exit Function
End If
End If

If Not SDate2 = "" Then
If Not ArchiveFinancialStartdate(PrimarySecondCurrent, SDate2, IIf(OriginalPrimary2 = "", "2", OriginalPrimary2)) Then
 FalseEnddate = True
  Exit Function
End If
End If

If Not SDate3 = "" Then
If Not ArchiveFinancialStartdate(PrimaryThirdCurrent, SDate3, IIf(OriginalPrimary3 = "", "3", OriginalPrimary3)) Then
 FalseEnddate = True
  Exit Function
End If
End If

End Function

Private Sub chkDelete1_Click()
Dim SDate As String
If (chkDelete1.Value = 1) Then
    If (txtPrimaryEDate1.Text <> "") Then
        If (OriginalPrimaryInsurerId1 < 1) Then
        If PrimaryFirstCurrent > 0 Then
          OriginalPrimaryInsurerId1 = FirstInsurerId1
        End If
        End If
        If (OriginalPrimaryInsurerId1 > 0) Then
       SDate = txtPrimarySDate1.Text
        OriginalPrimaryEDate1 = txtPrimaryEDate1.Text
            If ArchiveFinancial(PrimaryFirstCurrent, SDate, OriginalPrimaryEDate1, OriginalPrimary1) Then
                OriginalPrimary1 = txtPrimary1.Text
                txtPrimary1.Text = ""
                lblPrimaryInsurer1.Caption = ""
                txtCopayP1.Text = ""
                txtPrimaryMember1.Text = ""
                txtFirstGroup1.Text = ""
                SDate = txtPrimarySDate1.Text
                txtPrimarySDate1.Text = ""
                OriginalPrimaryEDate1 = txtPrimaryEDate1.Text
                txtPrimaryEDate1.Text = ""
                FirstInsurerId1 = 0
                chkDelete1.Visible = False
                OriginalPrimaryInsurerId1 = 0
                OriginalPrimaryCopay1 = ""
                OriginalPrimaryMember1 = ""
                OriginalPrimaryGroup1 = ""
                OriginalPrimarySDate1 = ""
                OriginalPrimaryEDate1 = ""
                OriginalPrimary1 = ""
                Call SetPolicy(lstOldPolicy1, PolicyHolder_1)
                PrimaryFirstCurrent = 0
            End If
        End If
    Else
        frmEventMsgs.Header = "End Date must be set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        chkDelete1.Visible = True
    End If
End If
chkDelete1.Value = 0
End Sub

Private Sub chkDelete2_Click()
Dim SDate As String
If (chkDelete2.Value = 1) Then
    If (txtPrimaryEDate2.Text <> "") Then
        If (OriginalPrimaryInsurerId2 < 1) Then
        If PrimarySecondCurrent > 0 Then
         OriginalPrimaryInsurerId2 = FirstInsurerId2
        End If
           
        End If
         SDate = txtPrimarySDate2.Text
        OriginalPrimaryEDate2 = txtPrimaryEDate2.Text
        If (OriginalPrimaryInsurerId2 > 0) Then
           If ArchiveFinancial(PrimarySecondCurrent, SDate, OriginalPrimaryEDate2, OriginalPrimary2) = True Then
                OriginalPrimary2 = txtPrimary2.Text
                txtPrimary2.Text = ""
                lblPrimaryInsurer2.Caption = ""
                txtCopayP2.Text = ""
                txtPrimaryMember2.Text = ""
                txtFirstGroup2.Text = ""
                SDate = txtPrimarySDate2.Text
                txtPrimarySDate2.Text = ""
                OriginalPrimaryEDate2 = txtPrimaryEDate2.Text
                txtPrimaryEDate2.Text = ""
                FirstInsurerId2 = 0
                chkDelete2.Visible = False
                OriginalPrimaryInsurerId2 = 0
                OriginalPrimaryCopay2 = ""
                OriginalPrimaryMember2 = ""
                OriginalPrimaryGroup2 = ""
                OriginalPrimarySDate2 = ""
                OriginalPrimaryEDate2 = ""
                OriginalPrimary2 = ""
                Call SetPolicy(lstOldPolicy1, PolicyHolder_1)
                PrimarySecondCurrent = 0
            End If
        End If
    Else
        frmEventMsgs.Header = "End Date must be set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        chkDelete2.Visible = True
    End If
End If
chkDelete2.Value = 0
End Sub

Private Sub chkDelete3_Click()
Dim SDate As String
If (chkDelete3.Value = 1) Then
    If (txtPrimaryEDate3.Text <> "") Then
        If (OriginalPrimaryInsurerId3 < 1) Then
        If PrimaryThirdCurrent > 0 Then
         OriginalPrimaryInsurerId3 = FirstInsurerId3
        End If
        End If
         SDate = txtPrimarySDate3.Text
        OriginalPrimaryEDate3 = txtPrimaryEDate3.Text
        If (OriginalPrimaryInsurerId3 > 0) Then
            If ArchiveFinancial(PrimaryThirdCurrent, SDate, OriginalPrimaryEDate3, OriginalPrimary3) Then
                OriginalPrimary3 = txtPrimary3.Text
                txtPrimary3.Text = ""
                lblPrimaryInsurer3.Caption = ""
                txtCopayP3.Text = ""
                txtPrimaryMember3.Text = ""
                txtFirstGroup3.Text = ""
                SDate = txtPrimarySDate3.Text
                txtPrimarySDate3.Text = ""
                OriginalPrimaryEDate3 = txtPrimaryEDate3.Text
                txtPrimaryEDate3.Text = ""
                FirstInsurerId3 = 0
                chkDelete3.Visible = False
                OriginalPrimaryInsurerId3 = 0
                OriginalPrimaryCopay3 = ""
                OriginalPrimaryMember3 = ""
                OriginalPrimaryGroup3 = ""
                OriginalPrimarySDate3 = ""
                OriginalPrimaryEDate3 = ""
                OriginalPrimary3 = ""
                Call SetPolicy(lstOldPolicy1, PolicyHolder_1)
                PrimaryThirdCurrent = 0
            End If
        End If
    Else
        frmEventMsgs.Header = "End Date must be set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        chkDelete3.Visible = True
    End If
End If
chkDelete3.Value = 0
End Sub

Private Sub chkDelete4_Click()
Dim SDate As String
If (chkDelete4.Value = 1) Then
    If (txtSecondEDate1.Text <> "") Then
        If (OriginalSecondInsurerId1 < 1) Then
            OriginalSecondInsurerId1 = SecondInsurerId1
        End If
        OriginalSecond1 = txtSecond1.Text
        txtSecond1.Text = ""
        lblSecondInsurer1.Caption = ""
        txtCopayS1.Text = ""
        txtSecondMember1.Text = ""
        txtSecondGroup1.Text = ""
        SDate = txtSecondSDate1.Text
        txtSecondSDate1.Text = ""
        OriginalSecondEDate1 = txtSecondEDate1.Text
        txtSecondEDate1.Text = ""
        SecondInsurerId1 = 0
        chkDelete4.Visible = False
        If (OriginalSecondInsurerId1 > 0) Then
            Call ArchiveFinancial(SecondFirstCurrent, SDate, OriginalSecondEDate1, OriginalSecond1)
            OriginalSecondInsurerId1 = 0
            OriginalSecondCopay1 = ""
            OriginalSecondMember1 = ""
            OriginalSecondGroup1 = ""
            OriginalSecondSDate1 = ""
            OriginalSecondEDate1 = ""
            OriginalSecond1 = ""
            Call SetPolicy(lstOldPolicy2, PolicyHolder_2)
        End If
    Else
        frmEventMsgs.Header = "End Date must be set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        chkDelete4.Visible = True
    End If
End If
chkDelete4.Value = 0
End Sub

Private Sub chkDelete5_Click()
Dim SDate As String
If (chkDelete5.Value = 1) Then
    If (txtSecondEDate2.Text <> "") Then
        If (OriginalSecondInsurerId2 < 1) Then
            OriginalSecondInsurerId2 = SecondInsurerId2
        End If
        OriginalSecond2 = txtSecond1.Text
        txtSecond2.Text = ""
        lblSecondInsurer2.Caption = ""
        txtCopayS2.Text = ""
        txtSecondMember2.Text = ""
        txtSecondGroup2.Text = ""
        SDate = txtSecondSDate2.Text
        txtSecondSDate2.Text = ""
        OriginalSecondEDate2 = txtSecondEDate2.Text
        txtSecondEDate2.Text = ""
        SecondInsurerId2 = 0
        chkDelete5.Visible = False
        If (OriginalSecondInsurerId2 > 0) Then
            Call ArchiveFinancial(SecondSecondCurrent, SDate, OriginalSecondEDate2, OriginalSecond2)
            OriginalSecondInsurerId2 = 0
            OriginalSecondCopay2 = ""
            OriginalSecondMember2 = ""
            OriginalSecondGroup2 = ""
            OriginalSecondSDate2 = ""
            OriginalSecondEDate2 = ""
            OriginalSecond2 = ""
            Call SetPolicy(lstOldPolicy2, PolicyHolder_2)
        End If
    Else
        frmEventMsgs.Header = "End Date must be set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        chkDelete5.Visible = True
    End If
End If
chkDelete5.Value = 0
End Sub

Private Sub chkDelete6_Click()
Dim SDate As String
If (chkDelete6.Value = 1) Then
    If (txtSecondEDate3.Text <> "") Then
        If (OriginalSecondInsurerId3 < 1) Then
            OriginalSecondInsurerId3 = SecondInsurerId3
        End If
        OriginalSecond3 = txtSecond1.Text
        txtSecond3.Text = ""
        lblSecondInsurer3.Caption = ""
        txtCopayS3.Text = ""
        txtSecondMember3.Text = ""
        txtSecondGroup3.Text = ""
        SDate = txtSecondSDate3.Text
        txtSecondSDate3.Text = ""
        OriginalSecondEDate3 = txtSecondEDate3.Text
        txtSecondEDate3.Text = ""
        SecondInsurerId3 = 0
        chkDelete6.Visible = False
        If (OriginalSecondInsurerId3 > 0) Then
            Call ArchiveFinancial(SecondThirdCurrent, SDate, OriginalSecondEDate3, OriginalSecond3)
            OriginalSecondInsurerId3 = 0
            OriginalSecondCopay3 = ""
            OriginalSecondMember3 = ""
            OriginalSecondGroup3 = ""
            OriginalSecondSDate3 = ""
            OriginalSecondEDate3 = ""
            OriginalSecond3 = ""
            Call SetPolicy(lstOldPolicy2, PolicyHolder_2)
        End If
    Else
        frmEventMsgs.Header = "End Date must be set"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        chkDelete6.Visible = True
    End If
End If
chkDelete6.Value = 0
End Sub

Private Sub cmdDemo1_Click()
If (PolicyHolder_1 > 0) Then
    frmNewInsured.PatientId = PatientId
    If (frmNewInsured.PatientLoadDisplay(PolicyHolder_1, False, "")) Then
        frmNewInsured.Show 1
    End If
End If
End Sub

Private Sub cmdDemo2_Click()
If (PolicyHolder_2 > 0) Then
    frmNewInsured.PatientId = PatientId
    If (frmNewInsured.PatientLoadDisplay(PolicyHolder_2, False, "")) Then
        frmNewInsured.Show 1
    End If
End If
End Sub

Private Sub cmdDependent_Click()
If (PatientId > 0) And ((PolicyHolder_1 > 0) Or (PolicyHolder_2 > 0)) Then
    If (frmPatientDependents.FinancialLoadDisplay(PatientId, PolicyHolder_1, PolicyHolder_2, InsuranceType)) Then
        frmPatientDependents.Show 1
    End If
End If
End Sub

Private Sub cmdNotes_Click()
frmNotes.PatientId = PatientId
frmNotes.NoteId = 0
frmNotes.AppointmentId = 0
frmNotes.SystemReference = ""
frmNotes.CurrentAction = ""
frmNotes.SetTo = "B"
frmNotes.MaintainOn = True
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Sub cmdQuit_Click()
fraAllocate.Visible = False
lblAllocate.Visible = False
lblOPolId.Visible = False
txtOPolId.Visible = False
lblOSDate.Visible = False
txtOSDate.Visible = False
txtOSDate.Text = ""
lblOEDate.Visible = False
txtOEDate.Visible = False
txtOEDate.Text = ""
cmdQuit.Visible = False
cmdFDone.Visible = False
    
lstInsType.Enabled = True
txtPrimary1.Enabled = True
txtPrimary2.Enabled = True
txtPrimary3.Enabled = True
lblPrimaryInsurer1.Enabled = True
lblPrimaryInsurer2.Enabled = True
lblPrimaryInsurer3.Enabled = True
txtCopayP1.Enabled = True
txtCopayP2.Enabled = True
txtCopayP3.Enabled = True
txtPrimaryMember1.Enabled = True
txtPrimaryMember2.Enabled = True
txtPrimaryMember3.Enabled = True
txtFirstGroup1.Enabled = True
txtFirstGroup2.Enabled = True
txtFirstGroup3.Enabled = True
txtPrimarySDate1.Enabled = True
txtPrimarySDate2.Enabled = True
txtPrimarySDate3.Enabled = True
txtPrimaryEDate1.Enabled = True
txtPrimaryEDate2.Enabled = True
txtPrimaryEDate3.Enabled = True
chkDelete1.Enabled = True
chkDelete2.Enabled = True
chkDelete3.Enabled = True
chkBill1.Enabled = True
cmdRemove1.Enabled = True
txtRel1.Enabled = True
FirstPolicyHolder.Enabled = True
lstOldPolicy1.Enabled = True

txtSecond1.Enabled = True
txtSecond2.Enabled = True
txtSecond3.Enabled = True
lblSecondInsurer1.Enabled = True
lblSecondInsurer2.Enabled = True
lblSecondInsurer3.Enabled = True
txtCopayS1.Enabled = True
txtCopayS2.Enabled = True
txtCopayS3.Enabled = True
txtSecondMember1.Enabled = True
txtSecondMember2.Enabled = True
txtSecondMember3.Enabled = True
txtSecondGroup1.Enabled = True
txtSecondGroup2.Enabled = True
txtSecondGroup3.Enabled = True
txtSecondSDate1.Enabled = True
txtSecondSDate2.Enabled = True
txtSecondSDate3.Enabled = True
txtSecondEDate1.Enabled = True
txtSecondEDate2.Enabled = True
txtSecondEDate3.Enabled = True
chkDelete1.Enabled = True
chkDelete4.Enabled = True
chkDelete6.Enabled = True
chkBill2.Enabled = True
cmdRemove2.Enabled = True
txtRel2.Enabled = True
SecondPolicyHolder.Enabled = True
lstOldPolicy2.Enabled = True

cmdDone.Enabled = True
cmdHome.Enabled = True
End Sub

Private Sub cmdFDone_Click()
Dim SDate As String
Dim EDate As String
Dim LocalDate As ManagedDate
Dim FinRec As PatientFinance
Dim ApplTemp As ApplicationTemplates
If (Trim(txtOSDate.Text) = "") Then
    frmEventMsgs.Header = "Start Date must be present"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOSDate.SetFocus
    Exit Sub
End If
If (Trim(txtOEDate.Text) = "") Then
    frmEventMsgs.Header = "End Date must be present"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOEDate.SetFocus
    Exit Sub
End If

If Not ArchivePostedFinancialStartdateandEnddate(val(Trim(txtOSDate.Tag)), Trim(txtOSDate.Text), Trim(txtOEDate.Text), selectedindicator) Then

Exit Sub
End If

SDate = ""
EDate = ""
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = Trim(txtOSDate.Text)
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    SDate = LocalDate.ExposedDate
End If
LocalDate.ExposedDate = Trim(txtOEDate.Text)
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    EDate = LocalDate.ExposedDate
End If
Set FinRec = New PatientFinance
FinRec.FinancialId = val(Trim(txtOSDate.Tag))

FinRec.FinancialStatus = ""
If (FinRec.RetrievePatientFinance) Then
    FinRec.PrimaryStartDate = SDate
    FinRec.PrimaryEndDate = EDate
    FinRec.PrimaryPerson = Trim(txtOPolId.Text)
    Call FinRec.ApplyPatientFinance
    Set ApplTemp = New ApplicationTemplates
    If (ApplTemp.ApplCloseArchivedTransactions(FinRec.FinancialId)) Then
        ArchiveChanged = True
    End If
    Set ApplTemp = Nothing
End If
Set FinRec = Nothing
Call SetPolicy(lstOldPolicy1, PolicyHolder_1)
Call SetPolicy(lstOldPolicy2, PolicyHolder_2)
fraAllocate.Visible = False
lblAllocate.Visible = False
lblOPolId.Visible = False
txtOPolId.Visible = False
lblOSDate.Visible = False
txtOSDate.Visible = False
txtOSDate.Text = ""
lblOEDate.Visible = False
txtOEDate.Visible = False
txtOEDate.Text = ""
cmdQuit.Visible = False
cmdFDone.Visible = False
    
lstInsType.Enabled = True
txtPrimary1.Enabled = True
txtPrimary2.Enabled = True
txtPrimary3.Enabled = True
lblPrimaryInsurer1.Enabled = True
lblPrimaryInsurer2.Enabled = True
lblPrimaryInsurer3.Enabled = True
txtCopayP1.Enabled = True
txtCopayP2.Enabled = True
txtCopayP3.Enabled = True
txtPrimaryMember1.Enabled = True
txtPrimaryMember2.Enabled = True
txtPrimaryMember3.Enabled = True
txtFirstGroup1.Enabled = True
txtFirstGroup2.Enabled = True
txtFirstGroup3.Enabled = True
txtPrimarySDate1.Enabled = True
txtPrimarySDate2.Enabled = True
txtPrimarySDate3.Enabled = True
txtPrimaryEDate1.Enabled = True
txtPrimaryEDate2.Enabled = True
txtPrimaryEDate3.Enabled = True
chkDelete1.Enabled = True
chkDelete2.Enabled = True
chkDelete3.Enabled = True
chkBill1.Enabled = True
cmdRemove1.Enabled = True
txtRel1.Enabled = True
FirstPolicyHolder.Enabled = True
lstOldPolicy1.Enabled = True

txtSecond1.Enabled = True
txtSecond2.Enabled = True
txtSecond3.Enabled = True
lblSecondInsurer1.Enabled = True
lblSecondInsurer2.Enabled = True
lblSecondInsurer3.Enabled = True
txtCopayS1.Enabled = True
txtCopayS2.Enabled = True
txtCopayS3.Enabled = True
txtSecondMember1.Enabled = True
txtSecondMember2.Enabled = True
txtSecondMember3.Enabled = True
txtSecondGroup1.Enabled = True
txtSecondGroup2.Enabled = True
txtSecondGroup3.Enabled = True
txtSecondSDate1.Enabled = True
txtSecondSDate2.Enabled = True
txtSecondSDate3.Enabled = True
txtSecondEDate1.Enabled = True
txtSecondEDate2.Enabled = True
txtSecondEDate3.Enabled = True
chkDelete1.Enabled = True
chkDelete4.Enabled = True
chkDelete6.Enabled = True
chkBill2.Enabled = True
cmdRemove2.Enabled = True
txtRel2.Enabled = True
SecondPolicyHolder.Enabled = True
lstOldPolicy2.Enabled = True

cmdDone.Enabled = True
cmdHome.Enabled = True
End Sub

Private Sub cmdShift_Click()
Dim ATemp As Long
Dim ATemp1 As Boolean
Dim ATemp2 As String
Dim RetPat As Patient
Dim ApplTemp As ApplicationTemplates
If (ThePolicyHolder1 > 0) And (ThePolicyHolder2 > 0) And (PatientId > 0) Then
    PostButRemain = True
    Call cmdDone_Click
    PostButRemain = False
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        ATemp = RetPat.PolicyPatientId
        ATemp1 = RetPat.BillParty
        ATemp2 = RetPat.Relationship
        RetPat.PolicyPatientId = RetPat.SecondPolicyPatientId
        RetPat.Relationship = RetPat.SecondRelationship
        RetPat.BillParty = RetPat.SecondBillParty
        RetPat.SecondPolicyPatientId = ATemp
        RetPat.SecondBillParty = ATemp1
        RetPat.SecondRelationship = ATemp2
        Call RetPat.ApplyPatient
        Set RetPat = Nothing
        ThePolicyHolder1 = ThePolicyHolder2
        ThePolicyHolder2 = ATemp
        ThePolicyHolder1Bill = ThePolicyHolder2Bill
        ThePolicyHolder2Bill = ATemp1
        ThePolicyHolder1Rel = ThePolicyHolder2Rel
        ThePolicyHolder2Rel = ATemp2
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplResetInsurerDesignation(PatientId)
        Set ApplTemp = Nothing
        Call FinancialLoadDisplay(PatientId, ThePolicyHolder1, ThePolicyHolder2, InsuranceType, True, AllowAccessOn)
    Else
        Set RetPat = Nothing
    End If
End If
End Sub

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmPatientFinancial.BorderStyle = 1
    frmPatientFinancial.ClipControls = True
    frmPatientFinancial.Caption = Mid(frmPatientFinancial.Name, 4, Len(frmPatientFinancial.Name) - 3)
    frmPatientFinancial.AutoRedraw = True
    frmPatientFinancial.Refresh
End If
FirstTimeIn = True
PostButRemain = False
PostButRemainFailed = False
End Sub

Private Sub lstInsType_Click()
Dim InsType As String
If (lstInsType.ListIndex >= 0) Then
    InsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    If (InsType <> InsuranceType) Then
        PostButRemain = True
        Call cmdDone_Click
        PostButRemain = False
        Call FinancialLoadDisplay(PatientId, ThePolicyHolder1, ThePolicyHolder2, InsType, True, AllowAccessOn)
    End If
End If
End Sub

Private Sub lstOldPolicy1_Click()
If (lstOldPolicy1.ListIndex >= 0) Then
   selectedindicator = ""
    selectedindicator = Trim(Mid(lstOldPolicy1.List(lstOldPolicy1.ListIndex), 100, 5))
    fraAllocate.Visible = True
    lblAllocate.Visible = True
    lblOPolId.Visible = True
    txtOPolId.Visible = True
    txtOPolId.Text = Trim(Mid(lstOldPolicy1.List(lstOldPolicy1.ListIndex), 72, 20))
    lblOSDate.Visible = True
    txtOSDate.Visible = True
    txtOSDate.Text = Trim(Mid(lstOldPolicy1.List(lstOldPolicy1.ListIndex), 50, 10))
    txtOSDate.Tag = Mid(lstOldPolicy1.List(lstOldPolicy1.ListIndex), 121) ', Len(lstOldPolicy1.List(lstOldPolicy1.ListIndex)) - 6)
    lblOEDate.Visible = True
    txtOEDate.Visible = True
    txtOEDate.Text = Trim(Mid(lstOldPolicy1.List(lstOldPolicy1.ListIndex), 62, 10))
    cmdQuit.Visible = True
    cmdFDone.Visible = True
    
    lstInsType.Enabled = False
    txtPrimary1.Enabled = False
    txtPrimary2.Enabled = False
    txtPrimary3.Enabled = False
    lblPrimaryInsurer1.Enabled = False
    lblPrimaryInsurer2.Enabled = False
    lblPrimaryInsurer3.Enabled = False
    txtCopayP1.Enabled = False
    txtCopayP2.Enabled = False
    txtCopayP3.Enabled = False
    txtPrimaryMember1.Enabled = False
    txtPrimaryMember2.Enabled = False
    txtPrimaryMember3.Enabled = False
    txtFirstGroup1.Enabled = False
    txtFirstGroup2.Enabled = False
    txtFirstGroup3.Enabled = False
    txtPrimarySDate1.Enabled = False
    txtPrimarySDate2.Enabled = False
    txtPrimarySDate3.Enabled = False
    txtPrimaryEDate1.Enabled = False
    txtPrimaryEDate2.Enabled = False
    txtPrimaryEDate3.Enabled = False
    chkDelete1.Enabled = False
    chkDelete2.Enabled = False
    chkDelete3.Enabled = False
    chkBill1.Enabled = False
    cmdRemove1.Enabled = False
    txtRel1.Enabled = False
    FirstPolicyHolder.Enabled = False
    lstOldPolicy1.Enabled = False

    txtSecond1.Enabled = False
    txtSecond2.Enabled = False
    txtSecond3.Enabled = False
    lblSecondInsurer1.Enabled = False
    lblSecondInsurer2.Enabled = False
    lblSecondInsurer3.Enabled = False
    txtCopayS1.Enabled = False
    txtCopayS2.Enabled = False
    txtCopayS3.Enabled = False
    txtSecondMember1.Enabled = False
    txtSecondMember2.Enabled = False
    txtSecondMember3.Enabled = False
    txtSecondGroup1.Enabled = False
    txtSecondGroup2.Enabled = False
    txtSecondGroup3.Enabled = False
    txtSecondSDate1.Enabled = False
    txtSecondSDate2.Enabled = False
    txtSecondSDate3.Enabled = False
    txtSecondEDate1.Enabled = False
    txtSecondEDate2.Enabled = False
    txtSecondEDate3.Enabled = False
    chkDelete1.Enabled = False
    chkDelete4.Enabled = False
    chkDelete6.Enabled = False
    chkBill2.Enabled = False
    cmdRemove2.Enabled = False
    txtRel2.Enabled = False
    SecondPolicyHolder.Enabled = False
    lstOldPolicy2.Enabled = False
    cmdDone.Enabled = False
    cmdHome.Enabled = False
End If
End Sub

Private Sub lstOldPolicy2_Click()
If (lstOldPolicy2.ListIndex >= 0) Then
    fraAllocate.Visible = True
    lblAllocate.Visible = True
    lblOPolId.Visible = True
    txtOPolId.Visible = True
    txtOPolId.Text = Trim(Mid(lstOldPolicy2.List(lstOldPolicy2.ListIndex), 72, 20))
    lblOSDate.Visible = True
    txtOSDate.Visible = True
    txtOSDate.Text = Trim(Mid(lstOldPolicy2.List(lstOldPolicy2.ListIndex), 50, 10))
    txtOSDate.Tag = Mid(lstOldPolicy2.List(lstOldPolicy2.ListIndex), 96, Len(lstOldPolicy2.List(lstOldPolicy2.ListIndex)) - 95)
    lblOEDate.Visible = True
    txtOEDate.Visible = True
    txtOEDate.Text = Trim(Mid(lstOldPolicy2.List(lstOldPolicy2.ListIndex), 62, 10))
    cmdQuit.Visible = True
    cmdFDone.Visible = True
    
    lstInsType.Enabled = False
    txtPrimary1.Enabled = False
    txtPrimary2.Enabled = False
    txtPrimary3.Enabled = False
    lblPrimaryInsurer1.Enabled = False
    lblPrimaryInsurer2.Enabled = False
    lblPrimaryInsurer3.Enabled = False
    txtCopayP1.Enabled = False
    txtCopayP2.Enabled = False
    txtCopayP3.Enabled = False
    txtPrimaryMember1.Enabled = False
    txtPrimaryMember2.Enabled = False
    txtPrimaryMember3.Enabled = False
    txtFirstGroup1.Enabled = False
    txtFirstGroup2.Enabled = False
    txtFirstGroup3.Enabled = False
    txtPrimarySDate1.Enabled = False
    txtPrimarySDate2.Enabled = False
    txtPrimarySDate3.Enabled = False
    txtPrimaryEDate1.Enabled = False
    txtPrimaryEDate2.Enabled = False
    txtPrimaryEDate3.Enabled = False
    chkDelete1.Enabled = False
    chkDelete2.Enabled = False
    chkDelete3.Enabled = False
    chkBill1.Enabled = False
    cmdRemove1.Enabled = False
    txtRel1.Enabled = False
    FirstPolicyHolder.Enabled = False
    lstOldPolicy1.Enabled = False

    txtSecond1.Enabled = False
    txtSecond2.Enabled = False
    txtSecond3.Enabled = False
    lblSecondInsurer1.Enabled = False
    lblSecondInsurer2.Enabled = False
    lblSecondInsurer3.Enabled = False
    txtCopayS1.Enabled = False
    txtCopayS2.Enabled = False
    txtCopayS3.Enabled = False
    txtSecondMember1.Enabled = False
    txtSecondMember2.Enabled = False
    txtSecondMember3.Enabled = False
    txtSecondGroup1.Enabled = False
    txtSecondGroup2.Enabled = False
    txtSecondGroup3.Enabled = False
    txtSecondSDate1.Enabled = False
    txtSecondSDate2.Enabled = False
    txtSecondSDate3.Enabled = False
    txtSecondEDate1.Enabled = False
    txtSecondEDate2.Enabled = False
    txtSecondEDate3.Enabled = False
    chkDelete1.Enabled = False
    chkDelete4.Enabled = False
    chkDelete6.Enabled = False
    chkBill2.Enabled = False
    cmdRemove2.Enabled = False
    txtRel2.Enabled = False
    SecondPolicyHolder.Enabled = False
    lstOldPolicy2.Enabled = False
    cmdDone.Enabled = False
    cmdHome.Enabled = False
End If
End Sub

Private Sub txtCopayP1_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtCopayP2_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtCopayP3_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtCopayS1_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtCopayS2_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtCopayS3_KeyPress(KeyAscii As Integer)
If Not (IsCurrency(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Valid Currency Characters [$0123456789.]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    KeyAscii = 0
End If
End Sub

Private Sub txtPrimary1_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 8) Then
    If ((KeyAscii < 49) Or (KeyAscii > 51)) And (KeyAscii <> 32) Then
        frmEventMsgs.Header = "Must be 1, 2 or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimary1.Text = "1"
    End If
End If
End Sub

Private Sub txtPrimary2_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 8) Then
    If ((KeyAscii < 49) Or (KeyAscii > 51)) And (KeyAscii <> 32) Then
        frmEventMsgs.Header = "Must be 1, 2 or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimary2.Text = "2"
    End If
End If
End Sub

Private Sub txtPrimary3_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 8) Then
    If ((KeyAscii < 49) Or (KeyAscii > 51)) And (KeyAscii <> 32) Then
        frmEventMsgs.Header = "Must be 1, 2 or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimary3.Text = "3"
    End If
End If
End Sub

Private Sub txtPrimaryMember1_KeyPress(KeyAscii As Integer)
If (Trim(lblPrimaryInsurer1.Caption) = "") Then
    frmEventMsgs.Header = "Must Enter Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimaryMember1.Text = ""
    KeyAscii = 0
End If
End Sub

Private Sub txtPrimaryMember1_Validate(Cancel As Boolean)
Dim i As Integer
Dim ATemp As String
If Not (FormLoadOn) And (Trim(txtPrimaryMember1.Text) <> "") Then
    If (Len(txtPrimaryMember1.Text) < 3) Then
        frmEventMsgs.Header = "Policy number is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryMember1.Text = ""
        txtPrimaryMember1.SetFocus
        Exit Sub
    End If
    If (Trim(lblPrimaryInsurer1.Tag) <> "") Then
        i = InStrPS(lblPrimaryInsurer1.Tag, "O")
        If (i < 0) Then
            i = Len(lblPrimaryInsurer1.Tag)
        End If
        If (Len(txtPrimaryMember1.Text) < i - 1) Then
            frmEventMsgs.Header = "Policy number must be at least " & Trim(Str(i - 1)) & " characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtPrimaryMember1.Text = ""
            txtPrimaryMember1.SetFocus
            Exit Sub
        End If
        For i = 1 To Len(txtPrimaryMember1.Text)
            ATemp = UCase(Mid(txtPrimaryMember1.Text, i, 1))
            If (Mid(lblPrimaryInsurer1.Tag, i, 1) = "A") Then
                If Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer1.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember1.Text = ""
                    txtPrimaryMember1.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblPrimaryInsurer1.Tag, i, 1) = "N") Then
                If Not (IsNumeric(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer1.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember1.Text = ""
                    txtPrimaryMember1.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblPrimaryInsurer1.Tag, i, 1) = "E") Then
                If Not (IsNumeric(ATemp)) And Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer1.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember1.Text = ""
                    txtPrimaryMember1.SetFocus
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Sub txtPrimaryMember2_KeyPress(KeyAscii As Integer)
If (Trim(lblPrimaryInsurer2.Caption) = "") Then
    frmEventMsgs.Header = "Must Enter Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimaryMember2.Text = ""
    KeyAscii = 0
End If
End Sub

Private Sub txtPrimaryMember2_Validate(Cancel As Boolean)
Dim i As Integer
Dim ATemp As String
If Not (FormLoadOn) And (Trim(txtPrimaryMember2.Text) <> "") Then
    If (Len(txtPrimaryMember2.Text) < 3) Then
        frmEventMsgs.Header = "Policy number is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryMember2.Text = ""
        txtPrimaryMember2.SetFocus
        Exit Sub
    End If
    If (Trim(lblPrimaryInsurer2.Tag) <> "") Then
        i = InStrPS(lblPrimaryInsurer2.Tag, "O")
        If (i < 0) Then
            i = Len(lblPrimaryInsurer2.Tag)
        End If
        If (Len(txtPrimaryMember2.Text) < i - 1) Then
            frmEventMsgs.Header = "Policy number must be at least " & Trim(Str(i - 1)) & " characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtPrimaryMember2.Text = ""
            txtPrimaryMember2.SetFocus
            Exit Sub
        End If
        For i = 1 To Len(txtPrimaryMember2.Text)
            ATemp = UCase(Mid(txtPrimaryMember2.Text, i, 1))
            If (Mid(lblPrimaryInsurer2.Tag, i, 1) = "A") Then
                If Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer2.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember2.Text = ""
                    txtPrimaryMember2.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblPrimaryInsurer2.Tag, i, 1) = "N") Then
                If Not (IsNumeric(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer2.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember2.Text = ""
                    txtPrimaryMember2.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblPrimaryInsurer2.Tag, i, 1) = "E") Then
                If Not (IsNumeric(ATemp)) And Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer2.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember2.Text = ""
                    txtPrimaryMember2.SetFocus
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Sub txtPrimaryMember3_KeyPress(KeyAscii As Integer)
If (Trim(lblPrimaryInsurer3.Caption) = "") Then
    frmEventMsgs.Header = "Must Enter Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimaryMember3.Text = ""
    KeyAscii = 0
End If
End Sub

Private Sub txtPrimaryMember3_Validate(Cancel As Boolean)
Dim i As Integer
Dim ATemp As String
If Not (FormLoadOn) And (Trim(txtPrimaryMember3.Text) <> "") Then
    If (Len(txtPrimaryMember3.Text) < 3) Then
        frmEventMsgs.Header = "Policy number is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryMember3.Text = ""
        txtPrimaryMember3.SetFocus
        Exit Sub
    End If
    If (Trim(lblPrimaryInsurer3.Tag) <> "") Then
        i = InStrPS(lblPrimaryInsurer3.Tag, "O")
        If (i < 0) Then
            i = Len(lblPrimaryInsurer3.Tag)
        End If
        If (Len(txtPrimaryMember3.Text) < i - 1) Then
            frmEventMsgs.Header = "Policy number must be at least " & Trim(Str(i - 1)) & " characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtPrimaryMember3.Text = ""
            txtPrimaryMember3.SetFocus
            Exit Sub
        End If
        For i = 1 To Len(txtPrimaryMember3.Text)
            ATemp = UCase(Mid(txtPrimaryMember3.Text, i, 1))
            If (Mid(lblPrimaryInsurer3.Tag, i, 1) = "A") Then
                If Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer3.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember3.Text = ""
                    txtPrimaryMember3.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblPrimaryInsurer3.Tag, i, 1) = "N") Then
                If Not (IsNumeric(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer3.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember3.Text = ""
                    txtPrimaryMember3.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblPrimaryInsurer3.Tag, i, 1) = "E") Then
                If Not (IsNumeric(ATemp)) And Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblPrimaryInsurer3.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtPrimaryMember3.Text = ""
                    txtPrimaryMember3.SetFocus
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Sub txtSecond1_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 8) Then
    If ((KeyAscii < 49) Or (KeyAscii > 51)) And (KeyAscii <> 32) Then
        frmEventMsgs.Header = "Must be 1, 2 or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecond1.Text = "1"
    End If
End If
End Sub

Private Sub txtSecond2_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 8) Then
    If ((KeyAscii < 49) Or (KeyAscii > 51)) And (KeyAscii <> 32) Then
        frmEventMsgs.Header = "Must be 1, 2 or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecond2.Text = "2"
    End If
End If
End Sub

Private Sub txtSecond3_KeyPress(KeyAscii As Integer)
If (KeyAscii <> 8) Then
    If ((KeyAscii < 49) Or (KeyAscii > 51)) And (KeyAscii <> 32) Then
        frmEventMsgs.Header = "Must be 1, 2 or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecond3.Text = "3"
    End If
End If
End Sub

Private Sub txtSecondMember1_KeyPress(KeyAscii As Integer)
Dim i As Integer
If (Trim(lblSecondInsurer1.Caption) = "") Then
    frmEventMsgs.Header = "Must Enter Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondMember1.Text = ""
    KeyAscii = 0
End If
End Sub

Private Sub txtSecondMember1_Validate(Cancel As Boolean)
Dim i As Integer
Dim ATemp As String
If Not (FormLoadOn) And (Trim(txtSecondMember1.Text) <> "") Then
    If (Len(txtSecondMember1.Text) < 3) Then
        frmEventMsgs.Header = "Policy number is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondMember1.Text = ""
        txtSecondMember1.SetFocus
        Exit Sub
    End If
    If (Trim(lblSecondInsurer1.Tag) <> "") Then
        i = InStrPS(lblSecondInsurer1.Tag, "O")
        If (i < 0) Then
            i = Len(lblSecondInsurer1.Tag)
        End If
        If (Len(txtSecondMember1.Text) < i - 1) Then
            frmEventMsgs.Header = "Policy number must be at least " & Trim(Str(i - 1)) & " characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtSecondMember1.Text = ""
            txtSecondMember1.SetFocus
            Exit Sub
        End If
        For i = 1 To Len(txtSecondMember1.Text)
            ATemp = UCase(Mid(txtSecondMember1.Text, i, 1))
            If (Mid(lblSecondInsurer1.Tag, i, 1) = "A") Then
                If Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer1.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember1.Text = ""
                    txtSecondMember1.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblSecondInsurer1.Tag, i, 1) = "N") Then
                If Not (IsNumeric(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer1.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember1.Text = ""
                    txtSecondMember1.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblSecondInsurer1.Tag, i, 1) = "E") Then
                If Not (IsNumeric(ATemp)) And Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer1.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember1.Text = ""
                    txtSecondMember1.SetFocus
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Sub txtSecondMember2_KeyPress(KeyAscii As Integer)
If (Trim(lblSecondInsurer2.Caption) = "") Then
    frmEventMsgs.Header = "Must Enter Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondMember2.Text = ""
    KeyAscii = 0
End If
End Sub

Private Sub txtSecondMember2_Validate(Cancel As Boolean)
Dim i As Integer
Dim ATemp As String
If Not (FormLoadOn) And (Trim(txtSecondMember2.Text) <> "") Then
    If (Len(txtSecondMember2.Text) < 3) Then
        frmEventMsgs.Header = "Policy number is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondMember2.Text = ""
        txtSecondMember2.SetFocus
        Exit Sub
    End If
    If (Trim(lblSecondInsurer2.Tag) <> "") Then
        i = InStrPS(lblSecondInsurer2.Tag, "O")
        If (i < 0) Then
            i = Len(lblSecondInsurer2.Tag)
        End If
        If (Len(txtSecondMember2.Text) < i - 1) Then
            frmEventMsgs.Header = "Policy number must be at least " & Trim(Str(i - 1)) & " characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtSecondMember2.Text = ""
            txtSecondMember2.SetFocus
            Exit Sub
        End If
        For i = 1 To Len(txtSecondMember2.Text)
            ATemp = UCase(Mid(txtSecondMember2.Text, i, 1))
            If (Mid(lblSecondInsurer2.Tag, i, 1) = "A") Then
                If Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer2.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember2.Text = ""
                    txtSecondMember2.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblSecondInsurer2.Tag, i, 1) = "N") Then
                If Not (IsNumeric(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer2.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember2.Text = ""
                    txtSecondMember2.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblSecondInsurer2.Tag, i, 1) = "E") Then
                If Not (IsNumeric(ATemp)) And Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer2.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember2.Text = ""
                    txtSecondMember2.SetFocus
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Sub txtSecondMember3_KeyPress(KeyAscii As Integer)
If (Trim(lblSecondInsurer3.Caption) = "") Then
    frmEventMsgs.Header = "Must Enter Insurer"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondMember3.Text = ""
    KeyAscii = 0
End If
End Sub

Private Sub txtSecondMember3_Validate(Cancel As Boolean)
Dim i As Integer
Dim ATemp As String
If Not (FormLoadOn) And (Trim(txtSecondMember3.Text) <> "") Then
    If (Len(txtSecondMember3.Text) < 3) Then
        frmEventMsgs.Header = "Policy number is too short."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondMember3.Text = ""
        txtSecondMember3.SetFocus
        Exit Sub
    End If
    If (Trim(lblSecondInsurer3.Tag) <> "") Then
        i = InStrPS(lblSecondInsurer3.Tag, "O")
        If (i < 0) Then
            i = Len(lblSecondInsurer3.Tag)
        End If
        If (Len(txtSecondMember3.Text) < i - 1) Then
            frmEventMsgs.Header = "Policy number must be at least " & Trim(Str(i - 1)) & " characters in length."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            txtSecondMember3.Text = ""
            txtSecondMember3.SetFocus
            Exit Sub
        End If
        For i = 1 To Len(txtSecondMember3.Text)
            ATemp = UCase(Mid(txtSecondMember3.Text, i, 1))
            If (Mid(lblSecondInsurer3.Tag, i, 1) = "A") Then
                If Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer3.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember3.Text = ""
                    txtSecondMember3.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblSecondInsurer3.Tag, i, 1) = "N") Then
                If Not (IsNumeric(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer3.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember3.Text = ""
                    txtSecondMember3.SetFocus
                    Exit For
                End If
            ElseIf (Mid(lblSecondInsurer3.Tag, i, 1) = "E") Then
                If Not (IsNumeric(ATemp)) And Not (IsAlpha(ATemp)) Then
                    frmEventMsgs.Header = "Policy number is not correctly formatted. Plan Format must be " & Trim(lblSecondInsurer3.Tag)
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "OK"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    txtSecondMember3.Text = ""
                    txtSecondMember3.SetFocus
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Function VerifyPrimarys(ErrType As Integer) As Boolean
Dim Temp As String
Dim PolicyOn As Boolean
VerifyPrimarys = False
If (PolicyHolder_1 <> PatientId) And (PolicyHolder_1 > 0) Then
    VerifyPrimarys = True
    Exit Function
End If
PolicyOn = CheckConfigCollection("POLICYREQUIRED") = "T"
If (txtPrimary1.Text = "1") And ((txtPrimary2.Text = "1") Or (txtPrimary3.Text = "1")) Then
    ErrType = 1
    Exit Function
End If
If ((txtPrimary1.Text < "1") Or (txtPrimary1.Text > "3")) And (Trim(txtPrimary1.Text) <> "") Then
    ErrType = 21
    Exit Function
End If
If (PolicyOn) Then
    If (Trim(lblPrimaryInsurer1.Caption) <> "") And (Trim(txtPrimaryMember1.Text) = "") Then
        ErrType = 2
        Exit Function
    End If
End If
If (txtPrimary2.Text = "1") And ((txtPrimary3.Text = "1") Or (txtPrimary1.Text = "1")) Then
    ErrType = 3
    Exit Function
End If
If ((txtPrimary2.Text < "1") Or (txtPrimary2.Text > "3")) And (Trim(txtPrimary2.Text) <> "") Then
    ErrType = 22
    Exit Function
End If
If (PolicyOn) Then
    If (Trim(lblPrimaryInsurer2.Caption) <> "") And (Trim(txtPrimaryMember2.Text) = "") Then
        ErrType = 4
        Exit Function
    End If
End If
If (txtPrimary3.Text = "1") And ((txtPrimary1.Text = "1") Or (txtPrimary2.Text = "1")) Then
    ErrType = 5
    Exit Function
End If
If ((txtPrimary3.Text < "1") Or (txtPrimary3.Text > "3")) And (Trim(txtPrimary3.Text) <> "") Then
    ErrType = 23
    Exit Function
End If
If (PolicyOn) Then
    If (Trim(lblPrimaryInsurer3.Caption) <> "") And (Trim(txtPrimaryMember3.Text) = "") Then
        ErrType = 6
        Exit Function
    End If
End If
If (txtPrimary2.Text = txtPrimary3.Text) And (Trim(txtPrimary2.Text) <> "") And (Trim(txtPrimary3.Text) <> "") Then
    ErrType = 7
    Exit Function
End If
If (txtPrimary1.Text = txtPrimary3.Text) And (Trim(txtPrimary1.Text) <> "") And (Trim(txtPrimary3.Text) <> "") Then
    ErrType = 8
    Exit Function
End If
If (txtPrimary1.Text = txtPrimary2.Text) And (Trim(txtPrimary1.Text) <> "") And (Trim(txtPrimary2.Text) <> "") Then
    ErrType = 9
    Exit Function
End If
If (Trim(txtPrimaryMember1.Text) <> "") Then
    If (Trim(lblPrimaryInsurer1.Tag) <> "") Then
        If (val(Trim(lblPrimaryInsurer1.ToolTipText)) > Len(Trim(txtPrimaryMember1.Text))) Then
            ErrType = 10
            Exit Function
        End If
    End If
End If
If (Trim(txtPrimaryMember2.Text) <> "") Then
    If (Trim(lblPrimaryInsurer2.Tag) <> "") Then
        If (val(Trim(lblPrimaryInsurer2.ToolTipText)) > Len(Trim(txtPrimaryMember2.Text))) Then
            ErrType = 11
            Exit Function
        End If
    End If
End If
If (Trim(txtPrimaryMember3.Text) <> "") Then
    If (Trim(lblPrimaryInsurer3.Tag) <> "") Then
        If (val(Trim(lblPrimaryInsurer3.ToolTipText)) > Len(Trim(txtPrimaryMember3.Text))) Then
            ErrType = 12
            Exit Function
        End If
    End If
End If
VerifyPrimarys = True
End Function

Private Function VerifySecondarys(ErrType As Integer) As Boolean
Dim Temp As String
Dim PolicyOn As Boolean
ErrType = 0
VerifySecondarys = False
If (PolicyHolder_2 <> PatientId) And (PolicyHolder_2 > 0) Then
    VerifySecondarys = True
    Exit Function
End If
PolicyOn = CheckConfigCollection("POLICYREQUIRED") = "T"
If (txtSecond1.Text = "1") And ((txtSecond2.Text = "1") Or (txtSecond3.Text = "1")) Then
    ErrType = 1
    Exit Function
End If
If ((txtSecond1.Text < "1") Or (txtSecond1.Text > "3")) And (Trim(txtSecond1.Text) <> "") Then
    ErrType = 21
    Exit Function
End If
If (PolicyOn) Then
    If (Trim(lblSecondInsurer1.Caption) <> "") And (Trim(txtSecondMember1.Text) = "") Then
        ErrType = 2
        Exit Function
    End If
End If
If (txtSecond2.Text = "1") And ((txtSecond3.Text = "1") Or (txtSecond1.Text = "1")) Then
    ErrType = 3
    Exit Function
End If
If ((txtSecond2.Text < "1") Or (txtSecond2.Text > "3")) And (Trim(txtSecond2.Text) <> "") Then
    ErrType = 22
    Exit Function
End If
If (PolicyOn) Then
    If (Trim(lblSecondInsurer2.Caption) <> "") And (Trim(txtSecondMember2.Text) = "") Then
        ErrType = 4
        Exit Function
    End If
End If
If (txtSecond3.Text = "1") And ((txtSecond1.Text = "1") Or (txtSecond2.Text = "1")) Then
    ErrType = 5
    Exit Function
End If
If ((txtSecond3.Text < "1") Or (txtSecond3.Text > "3")) And (Trim(txtSecond3.Text) <> "") Then
    ErrType = 23
    Exit Function
End If
If (PolicyOn) Then
    If (Trim(lblSecondInsurer3.Caption) <> "") And (Trim(txtSecondMember3.Text) = "") Then
        ErrType = 6
        Exit Function
    End If
End If
If (txtSecond2.Text = txtSecond3.Text) And (Trim(txtSecond2.Text) <> "") And (Trim(txtSecond3.Text) <> "") Then
    ErrType = 7
    Exit Function
End If
If (txtSecond1.Text = txtSecond3.Text) And (Trim(txtSecond1.Text) <> "") And (Trim(txtSecond3.Text) <> "") Then
    ErrType = 8
    Exit Function
End If
If (txtSecond1.Text = txtSecond2.Text) And (Trim(txtSecond1.Text) <> "") And (Trim(txtSecond2.Text) <> "") Then
    ErrType = 9
    Exit Function
End If
If (Trim(txtSecondMember1.Text) <> "") Then
    If (Trim(lblSecondInsurer1.Tag) <> "") Then
        If (val(Trim(lblSecondInsurer1.ToolTipText)) > Len(Trim(txtSecondMember1.Text))) Then
            ErrType = 10
            Exit Function
        End If
    End If
End If
If (Trim(txtSecondMember2.Text) <> "") Then
    If (Trim(lblSecondInsurer2.Tag) <> "") Then
        If (val(Trim(lblSecondInsurer2.ToolTipText)) > Len(Trim(txtSecondMember2.Text))) Then
            ErrType = 11
            Exit Function
        End If
    End If
End If
If (Trim(txtSecondMember3.Text) <> "") Then
    If (Trim(lblSecondInsurer3.Tag) <> "") Then
        If (val(Trim(lblSecondInsurer3.ToolTipText)) > Len(Trim(txtSecondMember3.Text))) Then
            ErrType = 12
            Exit Function
        End If
    End If
End If
VerifySecondarys = True
End Function

Private Function ResetToControl(AType As String, ErrType As Integer) As Boolean
If (AType = "P") Then
    If (ErrType = 1) Then
    ElseIf (ErrType = 2) Then
        txtPrimaryMember1.SetFocus
    ElseIf (ErrType = 3) Then
    ElseIf (ErrType = 4) Then
        txtPrimaryMember2.SetFocus
    ElseIf (ErrType = 5) Then
    ElseIf (ErrType = 6) Then
        txtPrimaryMember3.SetFocus
    ElseIf (ErrType = 7) Then
    ElseIf (ErrType = 8) Then
    ElseIf (ErrType = 9) Then
    ElseIf (ErrType = 10) Then
        txtPrimaryMember1.SetFocus
    ElseIf (ErrType = 11) Then
        txtPrimaryMember2.SetFocus
    ElseIf (ErrType = 12) Then
        txtPrimaryMember3.SetFocus
    End If
ElseIf (AType = "S") Then
    If (ErrType = 1) Then
    ElseIf (ErrType = 2) Then
        txtSecondMember1.SetFocus
    ElseIf (ErrType = 3) Then
    ElseIf (ErrType = 4) Then
        txtSecondMember2.SetFocus
    ElseIf (ErrType = 5) Then
    ElseIf (ErrType = 6) Then
        txtSecondMember3.SetFocus
    ElseIf (ErrType = 7) Then
    ElseIf (ErrType = 8) Then
    ElseIf (ErrType = 9) Then
    ElseIf (ErrType = 10) Then
        txtSecondMember1.SetFocus
    ElseIf (ErrType = 11) Then
        txtSecondMember2.SetFocus
    ElseIf (ErrType = 12) Then
        txtSecondMember3.SetFocus
    End If
End If
End Function

Private Sub cmdDone_Click()
'
If FalseEnddate() Then
Exit Sub
End If

Dim ErrType As Integer
Dim ZTemp As String
Dim PTemp As String
Dim ChangesApplied As Boolean
Dim LocalDate As ManagedDate
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
Dim FinId As Long

ChangesApplied = True
If Not (VerifyPrimarys(ErrType)) Then
    frmEventMsgs.Header = "Enter Policy ID Number"
    If (ErrType = 21) Or (ErrType = 22) Or (ErrType = 23) Then
        frmEventMsgs.Header = "Insurer Indicator must be 1,2,3."
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Call ResetToControl("P", ErrType)
    If (PostButRemain) Then
        PostButRemainFailed = True
    End If
    Exit Sub
End If
If (Trim(FirstPolicyHolder.Text) <> "") And (Trim(txtRel1.Text) = "") Then
    If Not (txtRel1.Visible) Then
        txtRel1.Text = "Y"
    Else
        frmEventMsgs.Header = "Enter Relationship"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
'If (Trim(FirstPolicyHolder.Text) <> Trim(lblPatientName.Caption)) And (Trim(txtRel1.Text) = "Y") Then
If (Trim(lblHldId.Caption) <> Trim(lblPatId.Caption)) And (Trim(txtRel1.Text) = "Y") Then
    frmEventMsgs.Header = "Relationship cannot be Self."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (PostButRemain) Then
        PostButRemainFailed = True
    End If
    txtRel1.SetFocus
    Exit Sub
End If
If (Trim(lblPrimaryInsurer1.Caption) <> "") Then
    If (Trim(txtPrimary1.Text) = "") Then
        frmEventMsgs.Header = "Indicator must be set as 1, 2, or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimary1.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
    If (Trim(txtPrimarySDate1.Text) = "") Then
        frmEventMsgs.Header = "Start Date must be present"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate1.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If (Trim(lblPrimaryInsurer2.Caption) <> "") Then
    If (Trim(txtPrimary2.Text) = "") Then
        frmEventMsgs.Header = "Indicator must be set as 1, 2, or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimary2.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
    If (Trim(txtPrimarySDate2.Text) = "") Then
        frmEventMsgs.Header = "Start Date must be present"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate2.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If (Trim(lblPrimaryInsurer3.Caption) <> "") Then
    If (Trim(txtPrimary3.Text) = "") Then
        frmEventMsgs.Header = "Indicator must be set as 1, 2, or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimary3.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
    If (Trim(txtPrimarySDate3.Text) = "") Then
        frmEventMsgs.Header = "Start Date must be present"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate3.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If Not (VerifySecondarys(ErrType)) Then
    frmEventMsgs.Header = "Enter Policy ID Number"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Call ResetToControl("S", ErrType)
    If (PostButRemain) Then
        PostButRemainFailed = True
    End If
    Exit Sub
End If
If (Trim(SecondPolicyHolder.Text) <> "") And (Trim(txtRel2.Text) = "") Then
    If Not (txtRel2.Visible) Then
        txtRel2.Text = "Y"
    Else
        frmEventMsgs.Header = "Enter Relationship"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If (Trim(SecondPolicyHolder.Text) <> Trim(lblPatientName.Caption)) And (Trim(txtRel2.Text) = "Y") Then
    frmEventMsgs.Header = "Relationship cannot be Self."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (PostButRemain) Then
        PostButRemainFailed = True
    End If
    txtRel2.SetFocus
    Exit Sub
End If
If (Trim(lblSecondInsurer1.Caption) <> "") Then
    If (Trim(txtSecond1.Text) = "") Then
        frmEventMsgs.Header = "Indicator must be set as 1, 2, or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecond1.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
    If (Trim(txtSecondSDate1.Text) = "") Then
        frmEventMsgs.Header = "Start Date must be present"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondSDate1.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If (Trim(lblSecondInsurer2.Caption) <> "") Then
    If (Trim(txtSecond2.Text) = "") Then
        frmEventMsgs.Header = "Indicator must be set as 1, 2, or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecond2.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
    If (Trim(txtSecondSDate2.Text) = "") Then
        frmEventMsgs.Header = "Start Date must be present"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondSDate2.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If (Trim(lblSecondInsurer3.Caption) <> "") Then
    If (Trim(txtSecond3.Text) = "") Then
        frmEventMsgs.Header = "Indicator must be set as 1, 2, or 3"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecond3.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
    If (Trim(txtSecondSDate3.Text) = "") Then
        frmEventMsgs.Header = "Start Date must be present"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondSDate3.SetFocus
        If (PostButRemain) Then
            PostButRemainFailed = True
        End If
        Exit Sub
    End If
End If
If (PatientId <> PolicyHolder_1) And (PolicyHolder_1 <> 0) Then
    If (txtRel1.Text = "Y") Then
        frmEventMsgs.Header = "Relationship cannot be Y"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRel1.Text = ""
        txtRel1.SetFocus
        Exit Sub
    End If
ElseIf (PolicyHolder_1 <> 0) Then
    If (txtRel1.Text <> "Y") Then
        frmEventMsgs.Header = "Relationship must be Y"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRel1.Text = ""
        txtRel1.SetFocus
        Exit Sub
    End If
End If
If (PatientId <> PolicyHolder_2) And (PolicyHolder_2 <> 0) Then
    If (txtRel2.Text = "Y") Then
        frmEventMsgs.Header = "Relationship cannot be Y"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRel2.Text = ""
        txtRel2.SetFocus
        Exit Sub
    End If
ElseIf (PolicyHolder_2 <> 0) Then
    If (txtRel2.Text <> "Y") Then
        frmEventMsgs.Header = "Relationship must be Y"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRel2.Text = ""
        txtRel2.SetFocus
        Exit Sub
    End If
End If
Set LocalDate = New ManagedDate
If (verifyChanged) Then
    ArchiveChanged = True
    Set ApplTemp = New ApplicationTemplates
    If (PolicyHolder_1 <> 0) Then
        If (PrimaryFirstCurrent > 0) And (FirstInsurerId1 <> OriginalPrimaryInsurerId1) Then
            If (FirstInsurerId1 > 0) Then
                Set RetrievePatientFinancial = Nothing
                Set RetrievePatientFinancial = New PatientFinance
                RetrievePatientFinancial.FinancialId = 0
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    RetrievePatientFinancial.PatientId = PolicyHolder_1
                    RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId1
                    ZTemp = UCase(Trim(txtPrimaryMember1.Text))
                    Call StripMultiCharacters(ZTemp, PTemp, True)
                    RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                    RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup1.Text))
                    RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary1.Text)
                    RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP1.Text))
                    RetrievePatientFinancial.PrimaryInsType = InsuranceType
                    LocalDate.ExposedDate = txtPrimarySDate1.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.PrimaryEndDate = ""
                    LocalDate.ExposedDate = txtPrimaryEDate1.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.FinancialStatus = "C"
                    If (RetrievePatientFinancial.ApplyPatientFinance) Then
                    Else
                        frmEventMsgs.Header = "Financial Update Not Applied"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ChangesApplied = False
                    End If
                End If
            End If
        ElseIf (PrimaryFirstCurrent > 0) And (FirstInsurerId1 = OriginalPrimaryInsurerId1) And (FirstInsurerId1 <> 0) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = PrimaryFirstCurrent
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_1
                RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId1
                ZTemp = UCase(Trim(txtPrimaryMember1.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup1.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary1.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP1.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtPrimarySDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtPrimaryEDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        ElseIf (PrimaryFirstCurrent = 0) And (FirstInsurerId1 <> OriginalPrimaryInsurerId1) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = 0
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_1
                RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId1
                ZTemp = UCase(Trim(txtPrimaryMember1.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup1.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary1.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP1.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtPrimarySDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtPrimaryEDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        End If
        If (PrimarySecondCurrent > 0) And (FirstInsurerId2 <> OriginalPrimaryInsurerId2) Then
            If (FirstInsurerId2 > 0) Then
                Set RetrievePatientFinancial = Nothing
                Set RetrievePatientFinancial = New PatientFinance
                RetrievePatientFinancial.FinancialId = 0
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    RetrievePatientFinancial.PatientId = PolicyHolder_1
                    RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId2
                    ZTemp = UCase(Trim(txtPrimaryMember2.Text))
                    Call StripMultiCharacters(ZTemp, PTemp, True)
                    RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                    RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup2.Text))
                    RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary2.Text)
                    RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP2.Text))
                    RetrievePatientFinancial.PrimaryInsType = InsuranceType
                    LocalDate.ExposedDate = txtPrimarySDate2.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.PrimaryEndDate = ""
                    LocalDate.ExposedDate = txtPrimaryEDate2.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.FinancialStatus = "C"
                    If (RetrievePatientFinancial.ApplyPatientFinance) Then
                    Else
                        frmEventMsgs.Header = "Financial Update Not Applied"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ChangesApplied = False
                    End If
                End If
            End If
        ElseIf (PrimarySecondCurrent > 0) And (FirstInsurerId2 = OriginalPrimaryInsurerId2) And (FirstInsurerId2 <> 0) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = PrimarySecondCurrent
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_1
                RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId2
                ZTemp = UCase(Trim(txtPrimaryMember2.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup2.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary2.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP2.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtPrimarySDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtPrimaryEDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        ElseIf (PrimarySecondCurrent = 0) And (FirstInsurerId2 <> OriginalPrimaryInsurerId2) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = 0
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_1
                RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId2
                ZTemp = UCase(Trim(txtPrimaryMember2.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup2.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary2.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP2.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtPrimarySDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtPrimaryEDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        End If
        If (PrimaryThirdCurrent > 0) And (FirstInsurerId3 <> OriginalPrimaryInsurerId3) Then
            If (FirstInsurerId3 > 0) Then
                Set RetrievePatientFinancial = Nothing
                Set RetrievePatientFinancial = New PatientFinance
                RetrievePatientFinancial.FinancialId = 0
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    RetrievePatientFinancial.PatientId = PolicyHolder_1
                    RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId3
                    ZTemp = UCase(Trim(txtPrimaryMember3.Text))
                    Call StripMultiCharacters(ZTemp, PTemp, True)
                    RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                    RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup3.Text))
                    RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary3.Text)
                    RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP3.Text))
                    RetrievePatientFinancial.PrimaryInsType = InsuranceType
                    LocalDate.ExposedDate = txtPrimarySDate3.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.PrimaryEndDate = ""
                    LocalDate.ExposedDate = txtPrimaryEDate3.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.FinancialStatus = "C"
                    If (RetrievePatientFinancial.ApplyPatientFinance) Then
                    Else
                        frmEventMsgs.Header = "Financial Update Not Applied"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ChangesApplied = False
                    End If
                End If
            End If
        ElseIf (PrimaryThirdCurrent > 0) And (FirstInsurerId3 = OriginalPrimaryInsurerId3) And (FirstInsurerId3 <> 0) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = PrimaryThirdCurrent
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_1
                RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId3
                ZTemp = UCase(Trim(txtPrimaryMember3.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup3.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary3.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP3.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtPrimarySDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtPrimaryEDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        ElseIf (PrimaryThirdCurrent = 0) And (FirstInsurerId3 <> OriginalPrimaryInsurerId3) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = 0
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_1
                RetrievePatientFinancial.PrimaryInsurerId = FirstInsurerId3
                ZTemp = UCase(Trim(txtPrimaryMember3.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtFirstGroup3.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtPrimary3.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayP3.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtPrimarySDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtPrimaryEDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        End If
        Set ApplList = New ApplicationAIList
        Set ApplTemp = New ApplicationTemplates
        Call ApplList.ApplSetPatientPolicyInfo(PatientId, ThePolicyHolder1, ThePolicyHolder1Rel, ThePolicyHolder1Bill, ThePolicyHolder2, ThePolicyHolder2Rel, ThePolicyHolder2Bill)
        'Update all Patient Receivables which has InsurerId=0 with Policy PatientId's insurance details
        Call ApplTemp.UpdateReceivable(Nothing, PatientId, False)
        Set ApplTemp = Nothing
        Set ApplList = Nothing
    End If
    
    If (PolicyHolder_2 <> 0) Then
        If (SecondFirstCurrent > 0) And (SecondInsurerId1 <> OriginalSecondInsurerId1) Then
            If (SecondInsurerId1 > 0) Then
                Set RetrievePatientFinancial = Nothing
                Set RetrievePatientFinancial = New PatientFinance
                RetrievePatientFinancial.FinancialId = 0
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    RetrievePatientFinancial.PatientId = PolicyHolder_2
                    RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId1
                    ZTemp = UCase(Trim(txtSecondMember1.Text))
                    Call StripMultiCharacters(ZTemp, PTemp, True)
                    RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                    RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup1.Text))
                    RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond1.Text)
                    RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS1.Text))
                    RetrievePatientFinancial.PrimaryInsType = InsuranceType
                    LocalDate.ExposedDate = txtSecondSDate1.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.PrimaryEndDate = ""
                    LocalDate.ExposedDate = txtSecondEDate1.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.FinancialStatus = "C"
                    If (RetrievePatientFinancial.ApplyPatientFinance) Then
                    Else
                        frmEventMsgs.Header = "Financial Update Not Applied"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ChangesApplied = False
                    End If
                End If
            End If
        ElseIf (SecondFirstCurrent > 0) And (SecondInsurerId1 = OriginalSecondInsurerId1) And (SecondInsurerId1 <> 0) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = SecondFirstCurrent
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_2
                RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId1
                ZTemp = UCase(Trim(txtSecondMember1.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup1.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond1.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS1.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtSecondSDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtSecondEDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        ElseIf (SecondFirstCurrent = 0) And (SecondInsurerId1 <> OriginalSecondInsurerId1) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = 0
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_2
                RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId1
                ZTemp = UCase(Trim(txtSecondMember1.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup1.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond1.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS1.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtSecondSDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtSecondEDate1.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        End If
        If (SecondSecondCurrent > 0) And (SecondInsurerId2 <> OriginalSecondInsurerId2) Then
            If (SecondInsurerId2 > 0) Then
                Set RetrievePatientFinancial = Nothing
                Set RetrievePatientFinancial = New PatientFinance
                RetrievePatientFinancial.FinancialId = 0
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    RetrievePatientFinancial.PatientId = PolicyHolder_2
                    RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId2
                    ZTemp = UCase(Trim(txtSecondMember2.Text))
                    Call StripMultiCharacters(ZTemp, PTemp, True)
                    RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                    RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup2.Text))
                    RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond2.Text)
                    RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS2.Text))
                    RetrievePatientFinancial.PrimaryInsType = InsuranceType
                    LocalDate.ExposedDate = txtSecondSDate2.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.PrimaryEndDate = ""
                    LocalDate.ExposedDate = txtSecondEDate2.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.FinancialStatus = "C"
                    If (RetrievePatientFinancial.ApplyPatientFinance) Then
                    Else
                        frmEventMsgs.Header = "Financial Update Not Applied"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ChangesApplied = False
                    End If
                End If
            End If
        ElseIf (SecondSecondCurrent > 0) And (SecondInsurerId2 = OriginalSecondInsurerId2) And (SecondInsurerId2 <> 0) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = SecondSecondCurrent
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_2
                RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId2
                ZTemp = UCase(Trim(txtSecondMember2.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup2.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond2.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS2.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtSecondSDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtSecondEDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        ElseIf (SecondSecondCurrent = 0) And (SecondInsurerId2 <> OriginalSecondInsurerId2) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = 0
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_2
                RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId2
                ZTemp = UCase(Trim(txtSecondMember2.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup2.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond2.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS2.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtSecondSDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtSecondEDate2.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        End If
        If (SecondThirdCurrent > 0) And (SecondInsurerId3 <> OriginalSecondInsurerId3) Then
            If (SecondInsurerId3 > 0) Then
                Set RetrievePatientFinancial = Nothing
                Set RetrievePatientFinancial = New PatientFinance
                RetrievePatientFinancial.FinancialId = 0
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    RetrievePatientFinancial.PatientId = PolicyHolder_2
                    RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId3
                    ZTemp = UCase(Trim(txtSecondMember3.Text))
                    Call StripMultiCharacters(ZTemp, PTemp, True)
                    RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                    RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup3.Text))
                    RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond3.Text)
                    RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS3.Text))
                    RetrievePatientFinancial.PrimaryInsType = InsuranceType
                    LocalDate.ExposedDate = txtSecondSDate3.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.PrimaryEndDate = ""
                    LocalDate.ExposedDate = txtSecondEDate3.Text
                    If (LocalDate.ConvertDisplayDateToManagedDate) Then
                        RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                    End If
                    RetrievePatientFinancial.FinancialStatus = "C"
                    If (RetrievePatientFinancial.ApplyPatientFinance) Then
                    Else
                        frmEventMsgs.Header = "Financial Update Not Applied"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ChangesApplied = False
                    End If
                End If
            End If
        ElseIf (SecondThirdCurrent > 0) And (SecondInsurerId3 = OriginalSecondInsurerId3) And (SecondInsurerId3 <> 0) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = SecondThirdCurrent
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_2
                RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId3
                ZTemp = UCase(Trim(txtSecondMember3.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond3.Text)
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup3.Text))
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS3.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtSecondSDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtSecondEDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        ElseIf (SecondThirdCurrent = 0) And (SecondInsurerId3 <> OriginalSecondInsurerId3) Then
            Set RetrievePatientFinancial = Nothing
            Set RetrievePatientFinancial = New PatientFinance
            RetrievePatientFinancial.FinancialId = 0
            RetrievePatientFinancial.FinancialStatus = ""
            If (RetrievePatientFinancial.RetrievePatientFinance) Then
                RetrievePatientFinancial.PatientId = PolicyHolder_2
                RetrievePatientFinancial.PrimaryInsurerId = SecondInsurerId3
                ZTemp = UCase(Trim(txtSecondMember3.Text))
                Call StripMultiCharacters(ZTemp, PTemp, True)
                RetrievePatientFinancial.PrimaryPerson = UCase(Trim(PTemp))
                RetrievePatientFinancial.PrimaryGroup = UCase(Trim(txtSecondGroup3.Text))
                RetrievePatientFinancial.PrimaryPIndicator = val(txtSecond3.Text)
                RetrievePatientFinancial.FinancialCopay = val(Trim(txtCopayS3.Text))
                RetrievePatientFinancial.PrimaryInsType = InsuranceType
                LocalDate.ExposedDate = txtSecondSDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryStartDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.PrimaryEndDate = ""
                LocalDate.ExposedDate = txtSecondEDate3.Text
                If (LocalDate.ConvertDisplayDateToManagedDate) Then
                    RetrievePatientFinancial.PrimaryEndDate = LocalDate.ExposedDate
                End If
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.ApplyPatientFinance) Then
                Else
                    frmEventMsgs.Header = "Financial Update Not Applied"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    ChangesApplied = False
                End If
            End If
        End If
    End If
    Set ApplTemp = Nothing
    If (PolicyHolder_1 <> PatientId) And (PolicyHolder_1 > 0) Then
        Call PostDependentInfo(PatientId, PolicyHolder_1, _
                               FirstInsurerId1, UCase(Trim(txtPrimaryMember1.Text)), _
                               FirstInsurerId2, UCase(Trim(txtPrimaryMember2.Text)), _
                               FirstInsurerId3, UCase(Trim(txtPrimaryMember3.Text)))
    End If
    If (PolicyHolder_2 <> PatientId) And (PolicyHolder_2 > 0) Then
        Call PostDependentInfo(PatientId, PolicyHolder_2, _
                               SecondInsurerId1, UCase(Trim(txtSecondMember1.Text)), _
                               SecondInsurerId2, UCase(Trim(txtSecondMember2.Text)), _
                               SecondInsurerId2, UCase(Trim(txtSecondMember3.Text)))
    End If
    If (ChangesApplied) Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplResetInsurerDesignation(PatientId)
        Set ApplTemp = Nothing
    End If
Else
    Set ApplList = New ApplicationAIList
    Call ApplList.ApplSetPatientPolicyInfo(PatientId, ThePolicyHolder1, ThePolicyHolder1Rel, ThePolicyHolder1Bill, ThePolicyHolder2, ThePolicyHolder2Rel, ThePolicyHolder2Bill)
    Set ApplList = Nothing
End If
Set LocalDate = Nothing
Set RetrievePatientFinancial = Nothing
If Not (PostButRemain) Then
    If Not (VerifyEndDates(PatientId)) Then
        frmEventMsgs.Header = "Policy Coverage Conflict, Archived End Dates cannot overlap with the same policy level."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Continue"
        frmEventMsgs.CancelText = "Remain to Fix"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 4) Then
            Exit Sub
        End If
    End If
    Unload frmPatientFinancial
End If
PostButRemain = False
End Sub

Private Sub cmdHome_Click()
If (CurrentAction <> "HomeNoChanges") Then
    CurrentAction = "HomeNoChanges"
End If
Unload frmPatientFinancial
End Sub

Public Function FinancialLoadDisplay(PatId As Long, FirstPolicyHolderId As Long, SecondPolicyHolderId As Long, InsType As String, Initial As Boolean, AccessRef As Boolean) As Boolean
On Error GoTo UIError_Label
Dim i As Integer
Dim Copay As String
Dim FirstFinancialId As Long
Dim SecondFinancialId As Long
Dim RetrievePatient As Patient
Dim LocalDate As ManagedDate
FormLoadOn = True
FinancialLoadDisplay = True
Set LocalDate = New ManagedDate
Set RetrievePatient = New Patient
ArchiveChanged = False
AllowOtherPartyAccess1 = False
AllowOtherPartyAccess2 = False
ValidatePolicyNumbers = CheckConfigCollection("POLICYREQUIRED") = "T"
PatientId = PatId
RetrievePatient.PatientId = PatId
If (RetrievePatient.RetrievePatient) Then
    lblPatientName.Caption = RetrievePatient.FirstName + " " _
                           + RetrievePatient.MiddleInitial + " " _
                           + RetrievePatient.LastName + " " _
                           + RetrievePatient.NameRef
End If
Set RetrievePatient = Nothing
Label13.Visible = AccessRef
txtRel1.Visible = AccessRef
Label14.Visible = AccessRef
txtRel2.Visible = AccessRef
cmdDependent.Visible = False
cmdShift.Visible = False
cmdRemove1.Visible = AccessRef
cmdRemove2.Visible = AccessRef
Label2.Visible = AccessRef
Label15.Visible = AccessRef
FirstPolicyHolder.Visible = AccessRef
SecondPolicyHolder.Visible = AccessRef
AllowAccessOn = AccessRef
FirstPolicyHolder.Text = ""
SecondPolicyHolder.Text = ""
txtPrimary1.Text = ""
txtRel2.Text = ""
txtPrimary2.Text = ""
txtPrimary3.Text = ""
lblPrimaryInsurer1.Caption = ""
lblPrimaryInsurer2.Caption = ""
lblPrimaryInsurer3.Caption = ""
txtCopayP1.Text = ""
txtCopayP2.Text = ""
txtCopayP3.Text = ""
txtPrimaryMember1.Text = ""
txtPrimaryMember2.Text = ""
txtPrimaryMember3.Text = ""
txtFirstGroup1.Text = ""
txtFirstGroup2.Text = ""
txtFirstGroup3.Text = ""
txtPrimarySDate1.Text = ""
txtPrimarySDate2.Text = ""
txtPrimarySDate3.Text = ""
txtPrimaryEDate1.Text = ""
txtPrimaryEDate2.Text = ""
txtPrimaryEDate3.Text = ""

txtSecond1.Text = ""
txtSecond2.Text = ""
txtSecond3.Text = ""
lblSecondInsurer1.Caption = ""
lblSecondInsurer2.Caption = ""
lblSecondInsurer3.Caption = ""
txtCopayS1.Text = ""
txtCopayS2.Text = ""
txtCopayS3.Text = ""
txtSecondMember1.Text = ""
txtSecondMember2.Text = ""
txtSecondMember3.Text = ""
txtSecondGroup1.Text = ""
txtSecondGroup2.Text = ""
txtSecondGroup3.Text = ""
txtSecondSDate1.Text = ""
txtSecondSDate2.Text = ""
txtSecondSDate3.Text = ""
txtSecondEDate1.Text = ""
txtSecondEDate2.Text = ""
txtSecondEDate3.Text = ""

If (Trim(InsType) = "") Then
    InsType = "M"
End If
InsuranceType = InsType
lstInsType.ListIndex = -1
For i = 0 To lstInsType.ListCount - 1
    If (Left(lstInsType.List(i), 1) = InsType) Then
        lstInsType.ListIndex = i
        Exit For
    End If
Next i
' Get financial records
' Policy Holder 1 Insurer Stuff
lblDelete1.Visible = True
PolicyHolder_1 = 0
FirstInsurerId1 = 0
FirstInsurerId2 = 0
FirstInsurerId3 = 0
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
If (FirstPolicyHolderId <> 0) Then
    Call GetActiveFinancial(FirstPolicyHolderId, PrimaryFirstCurrent, PrimarySecondCurrent, PrimaryThirdCurrent)
    Call SetPolicy(lstOldPolicy1, FirstPolicyHolderId)
    ThePolicyHolder1 = FirstPolicyHolderId
    If (FirstPolicyHolderId > 0) Then
        RetrieveAnotherPatient.PatientId = FirstPolicyHolderId
        If (RetrieveAnotherPatient.RetrievePatient) Then
            cmdRemove1.Visible = AccessRef
            PolicyHolder_1 = FirstPolicyHolderId
            If (Initial) Then
                OriginalPolicyHolder_1 = PolicyHolder_1
            End If
            FirstPolicyHolder.Text = RetrieveAnotherPatient.FirstName + " " _
                                    + RetrieveAnotherPatient.MiddleInitial + " " _
                                    + RetrieveAnotherPatient.LastName + " " _
                                    + RetrieveAnotherPatient.NameRef
            If ThePolicyHolder1Rel = "" And PatId = FirstPolicyHolderId Then
                ThePolicyHolder1Rel = "Y"
            End If
            txtRel1.Text = ThePolicyHolder1Rel
            If (ThePolicyHolder1Bill) Then
                chkBill1.Value = 1
            Else
                chkBill1.Value = 0
            End If
            If (PrimaryFirstCurrent > 0) Then
                chkDelete1.Visible = True
                RetrievePatientFinancial.PatientId = FirstPolicyHolderId
                RetrievePatientFinancial.FinancialId = PrimaryFirstCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId1 = RetrievePatientFinancial.PrimaryInsurerId
                            If (Initial) Then
                                OriginalPrimaryInsurerId1 = FirstInsurerId1
                            End If
                            lblPrimaryInsurer1.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            lblPrimaryInsurer1.Tag = ""
                            lblPrimaryInsurer1.ToolTipText = Trim(FormatMinLength(RetrieveInsurer.InsurerPlanFormat))
                            If (ValidatePolicyNumbers) Then
                                lblPrimaryInsurer1.Tag = Trim(RetrieveInsurer.InsurerPlanFormat)
                            End If
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP1.Text = Trim(Copay)
                        End If
                        OriginalPrimaryCopay1 = Trim(Copay)
                        txtPrimaryMember1.Text = RetrievePatientFinancial.PrimaryPerson
                        txtFirstGroup1.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimarySDate1.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimaryEDate1.Text = LocalDate.ExposedDate
                        End If
                        txtPrimary1.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        If (Initial) Then
                            OriginalPrimaryMember1 = txtPrimaryMember1.Text
                            OriginalPrimaryGroup1 = txtFirstGroup1.Text
                            OriginalPrimarySDate1 = txtPrimarySDate1.Text
                            OriginalPrimaryEDate1 = txtPrimaryEDate1.Text
                            OriginalPrimary1 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        End If
                    End If
                End If
            Else
                OriginalPrimaryInsurerId1 = 0
                OriginalPrimaryMember1 = ""
                OriginalPrimaryGroup1 = ""
                OriginalPrimarySDate1 = ""
                OriginalPrimaryEDate1 = ""
                OriginalPrimaryCopay1 = ""
                OriginalPrimary1 = ""
            End If
            If (PrimarySecondCurrent > 0) Then
                chkDelete2.Visible = True
                RetrievePatientFinancial.PatientId = FirstPolicyHolderId
                RetrievePatientFinancial.FinancialId = PrimarySecondCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId2 = RetrievePatientFinancial.PrimaryInsurerId
                            If (Initial) Then
                                OriginalPrimaryInsurerId2 = FirstInsurerId2
                            End If
                            lblPrimaryInsurer2.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            lblPrimaryInsurer2.Tag = ""
                            lblPrimaryInsurer2.ToolTipText = Trim(FormatMinLength(RetrieveInsurer.InsurerPlanFormat))
                            If (ValidatePolicyNumbers) Then
                                lblPrimaryInsurer2.Tag = Trim(RetrieveInsurer.InsurerPlanFormat)
                            End If
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP2.Text = Trim(Copay)
                            OriginalPrimaryCopay2 = Trim(Copay)
                        End If
                        txtPrimaryMember2.Text = RetrievePatientFinancial.PrimaryPerson
                        txtFirstGroup2.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimarySDate2.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimaryEDate2.Text = LocalDate.ExposedDate
                        End If
                        txtPrimary2.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        If (Initial) Then
                            OriginalPrimaryMember2 = txtPrimaryMember2.Text
                            OriginalPrimaryGroup2 = txtFirstGroup2.Text
                            OriginalPrimarySDate2 = txtPrimarySDate2.Text
                            OriginalPrimaryEDate2 = txtPrimaryEDate2.Text
                            OriginalPrimary2 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        End If
                    End If
                End If
            Else
                OriginalPrimaryInsurerId2 = 0
                OriginalPrimaryMember2 = ""
                OriginalPrimaryGroup2 = ""
                OriginalPrimarySDate2 = ""
                OriginalPrimaryEDate2 = ""
                OriginalPrimaryCopay2 = ""
                OriginalPrimary2 = ""
            End If
            If (PrimaryThirdCurrent > 0) Then
                chkDelete3.Visible = True
                RetrievePatientFinancial.PatientId = FirstPolicyHolderId
                RetrievePatientFinancial.FinancialId = PrimaryThirdCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId3 = RetrievePatientFinancial.PrimaryInsurerId
                            If (Initial) Then
                                OriginalPrimaryInsurerId3 = FirstInsurerId3
                            End If
                            lblPrimaryInsurer3.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            lblPrimaryInsurer3.Tag = ""
                            lblPrimaryInsurer3.ToolTipText = Trim(FormatMinLength(RetrieveInsurer.InsurerPlanFormat))
                            If (ValidatePolicyNumbers) Then
                                lblPrimaryInsurer3.Tag = Trim(RetrieveInsurer.InsurerPlanFormat)
                            End If
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP3.Text = Trim(Copay)
                            OriginalPrimaryCopay3 = Trim(Copay)
                        End If
                        txtPrimaryMember3.Text = RetrievePatientFinancial.PrimaryPerson
                        txtFirstGroup3.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimarySDate3.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtPrimaryEDate3.Text = LocalDate.ExposedDate
                        End If
                        txtPrimary3.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        If (Initial) Then
                            OriginalPrimaryMember3 = txtPrimaryMember3.Text
                            OriginalPrimaryGroup3 = txtFirstGroup3.Text
                            OriginalPrimarySDate3 = txtPrimarySDate3.Text
                            OriginalPrimaryEDate3 = txtPrimaryEDate3.Text
                            OriginalPrimary3 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        End If
                    End If
                End If
            Else
                OriginalPrimaryInsurerId3 = 0
                OriginalPrimaryMember3 = ""
                OriginalPrimaryGroup3 = ""
                OriginalPrimarySDate3 = ""
                OriginalPrimaryEDate3 = ""
                OriginalPrimaryCopay3 = ""
                OriginalPrimary3 = ""
            End If
        End If
    Else
        OriginalPrimaryInsurerId1 = 0
        OriginalPrimaryMember1 = ""
        OriginalPrimaryGroup1 = ""
        OriginalPrimarySDate1 = ""
        OriginalPrimaryEDate1 = ""
        OriginalPrimaryCopay1 = ""
        OriginalPrimary1 = ""
        OriginalPrimaryInsurerId2 = 0
        OriginalPrimaryMember2 = ""
        OriginalPrimaryGroup2 = ""
        OriginalPrimarySDate2 = ""
        OriginalPrimaryEDate2 = ""
        OriginalPrimaryCopay2 = ""
        OriginalPrimary2 = ""
        OriginalPrimaryInsurerId3 = 0
        OriginalPrimaryMember3 = ""
        OriginalPrimaryGroup3 = ""
        OriginalPrimarySDate3 = ""
        OriginalPrimaryEDate3 = ""
        OriginalPrimaryCopay3 = ""
        OriginalPrimary3 = ""
    End If
Else
    OriginalPrimaryInsurerId1 = 0
    OriginalPrimaryMember1 = ""
    OriginalPrimaryGroup1 = ""
    OriginalPrimarySDate1 = ""
    OriginalPrimaryEDate1 = ""
    OriginalPrimaryCopay1 = ""
    OriginalPrimary1 = ""
    OriginalPrimaryInsurerId2 = 0
    OriginalPrimaryMember2 = ""
    OriginalPrimaryGroup2 = ""
    OriginalPrimarySDate2 = ""
    OriginalPrimaryEDate2 = ""
    OriginalPrimaryCopay2 = ""
    OriginalPrimary2 = ""
    OriginalPrimaryInsurerId3 = 0
    OriginalPrimaryMember3 = ""
    OriginalPrimaryGroup3 = ""
    OriginalPrimarySDate3 = ""
    OriginalPrimaryEDate3 = ""
    OriginalPrimaryCopay3 = ""
    OriginalPrimary3 = ""
End If

' Policy Holder 2 Insurer Stuff
lblDelete2.Visible = True
PolicyHolder_2 = 0
SecondInsurerId1 = 0
SecondInsurerId2 = 0
SecondInsurerId3 = 0
If (SecondPolicyHolderId <> 0) Then
    Call GetActiveFinancial(SecondPolicyHolderId, SecondFirstCurrent, SecondSecondCurrent, SecondThirdCurrent)
    Call SetPolicy(lstOldPolicy2, SecondPolicyHolderId)
    ThePolicyHolder2 = SecondPolicyHolderId
    If (SecondPolicyHolderId > 0) Then
        RetrieveAnotherPatient.PatientId = SecondPolicyHolderId
        If (RetrieveAnotherPatient.RetrievePatient) Then
            cmdRemove2.Visible = AccessRef
            PolicyHolder_2 = SecondPolicyHolderId
            If (Initial) Then
                OriginalPolicyHolder_2 = PolicyHolder_2
            End If
            SecondPolicyHolder.Text = RetrieveAnotherPatient.FirstName + " " _
                                    + RetrieveAnotherPatient.MiddleInitial + " " _
                                    + RetrieveAnotherPatient.LastName + " " _
                                    + RetrieveAnotherPatient.NameRef
            txtRel2.Text = ThePolicyHolder2Rel
            If (ThePolicyHolder2Bill) Then
                chkBill2.Value = 1
            Else
                chkBill2.Value = 0
            End If
            If (SecondFirstCurrent <> 0) Then
                chkDelete4.Visible = True
                RetrievePatientFinancial.PatientId = SecondPolicyHolderId
                RetrievePatientFinancial.FinancialId = SecondFirstCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId1 = RetrievePatientFinancial.PrimaryInsurerId
                            If (Initial) Then
                                OriginalSecondInsurerId1 = SecondInsurerId1
                            End If
                            lblSecondInsurer1.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            lblSecondInsurer1.Tag = ""
                            lblSecondInsurer1.ToolTipText = Trim(FormatMinLength(RetrieveInsurer.InsurerPlanFormat))
                            If (ValidatePolicyNumbers) Then
                                lblSecondInsurer1.Tag = Trim(RetrieveInsurer.InsurerPlanFormat)
                            End If
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS1.Text = Trim(Copay)
                            OriginalSecondCopay1 = Trim(Copay)
                        End If
                        txtSecondMember1.Text = RetrievePatientFinancial.PrimaryPerson
                        txtSecondGroup1.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondSDate1.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondEDate1.Text = LocalDate.ExposedDate
                        End If
                        txtSecond1.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        If (Initial) Then
                            OriginalSecondMember1 = txtSecondMember1.Text
                            OriginalSecondGroup1 = txtSecondGroup1.Text
                            OriginalSecondSDate1 = txtSecondSDate1.Text
                            OriginalSecondEDate1 = txtSecondEDate1.Text
                            OriginalSecond1 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        End If
                    End If
                End If
            Else
                OriginalSecondInsurerId1 = 0
                OriginalSecondMember1 = ""
                OriginalSecondGroup1 = ""
                OriginalSecondSDate1 = ""
                OriginalSecondEDate1 = ""
                OriginalSecondCopay1 = ""
                OriginalSecond1 = ""
            End If
            If (SecondSecondCurrent <> 0) Then
                chkDelete5.Visible = True
                RetrievePatientFinancial.PatientId = SecondPolicyHolderId
                RetrievePatientFinancial.FinancialId = SecondSecondCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId2 = RetrievePatientFinancial.PrimaryInsurerId
                            If (Initial) Then
                                OriginalSecondInsurerId2 = SecondInsurerId2
                            End If
                            lblSecondInsurer2.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            lblSecondInsurer2.Tag = ""
                            lblSecondInsurer2.ToolTipText = Trim(FormatMinLength(RetrieveInsurer.InsurerPlanFormat))
                            If (ValidatePolicyNumbers) Then
                                lblSecondInsurer2.Tag = Trim(RetrieveInsurer.InsurerPlanFormat)
                            End If
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS2.Text = Trim(Copay)
                            OriginalSecondCopay2 = Trim(Copay)
                        End If
                        txtSecondGroup2.Text = RetrievePatientFinancial.PrimaryGroup
                        txtSecondMember2.Text = RetrievePatientFinancial.PrimaryPerson
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondSDate2.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondEDate2.Text = LocalDate.ExposedDate
                        End If
                        txtSecond2.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        If (Initial) Then
                            OriginalSecondMember2 = txtSecondMember2.Text
                            OriginalSecondGroup2 = txtSecondGroup2.Text
                            OriginalSecondSDate2 = txtSecondSDate2.Text
                            OriginalSecondEDate2 = txtSecondEDate2.Text
                            OriginalSecond2 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        End If
                    End If
                End If
            Else
                OriginalSecondInsurerId2 = 0
                OriginalSecondMember2 = ""
                OriginalSecondGroup2 = ""
                OriginalSecondSDate2 = ""
                OriginalSecondEDate2 = ""
                OriginalSecondCopay2 = Copay
                OriginalSecond2 = ""
            End If
            If (SecondThirdCurrent <> 0) Then
                chkDelete6.Visible = True
                RetrievePatientFinancial.PatientId = SecondPolicyHolderId
                RetrievePatientFinancial.FinancialId = SecondThirdCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.PrimaryInsType = InsType
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId3 = RetrievePatientFinancial.PrimaryInsurerId
                            If (Initial) Then
                                OriginalSecondInsurerId3 = SecondInsurerId3
                            End If
                            lblSecondInsurer3.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            lblSecondInsurer3.Tag = ""
                            lblSecondInsurer3.ToolTipText = Trim(FormatMinLength(RetrieveInsurer.InsurerPlanFormat))
                            If (ValidatePolicyNumbers) Then
                                lblSecondInsurer3.Tag = Trim(RetrieveInsurer.InsurerPlanFormat)
                            End If
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS3.Text = Trim(Copay)
                            OriginalSecondCopay3 = Trim(Copay)
                        End If
                        txtSecondMember3.Text = RetrievePatientFinancial.PrimaryPerson
                        txtSecondGroup3.Text = RetrievePatientFinancial.PrimaryGroup
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondSDate3.Text = LocalDate.ExposedDate
                        End If
                        LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                        If (LocalDate.ConvertManagedDateToDisplayDate) Then
                            txtSecondEDate3.Text = LocalDate.ExposedDate
                        End If
                        txtSecond3.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        If (Initial) Then
                            OriginalSecondMember3 = txtSecondMember3.Text
                            OriginalSecondGroup3 = txtSecondGroup3.Text
                            OriginalSecondSDate3 = txtSecondSDate3.Text
                            OriginalSecondEDate3 = txtSecondEDate3.Text
                            OriginalSecond3 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        End If
                    End If
                End If
            Else
                OriginalSecondInsurerId3 = 0
                OriginalSecondMember3 = ""
                OriginalSecondGroup3 = ""
                OriginalSecondSDate3 = ""
                OriginalSecondEDate3 = ""
                OriginalSecondCopay3 = Copay
                OriginalSecond3 = ""
            End If
        End If
    Else
        OriginalSecondInsurerId1 = 0
        OriginalSecondMember1 = ""
        OriginalSecondGroup1 = ""
        OriginalSecondSDate1 = ""
        OriginalSecondEDate1 = ""
        OriginalSecondCopay1 = ""
        OriginalSecond1 = ""
        OriginalSecondInsurerId2 = 0
        OriginalSecondMember2 = ""
        OriginalSecondGroup2 = ""
        OriginalSecondSDate2 = ""
        OriginalSecondEDate2 = ""
        OriginalSecondCopay2 = ""
        OriginalSecond2 = ""
        OriginalSecondInsurerId3 = 0
        OriginalSecondMember3 = ""
        OriginalSecondGroup3 = ""
        OriginalSecondSDate3 = ""
        OriginalSecondEDate3 = ""
        OriginalSecondCopay3 = ""
        OriginalSecond3 = ""
    End If
Else
    OriginalSecondInsurerId1 = 0
    OriginalSecondMember1 = ""
    OriginalSecondSDate1 = ""
    OriginalSecondEDate1 = ""
    OriginalSecondCopay1 = ""
    OriginalSecond1 = ""
    OriginalSecondInsurerId2 = 0
    OriginalSecondMember2 = ""
    OriginalSecondSDate2 = ""
    OriginalSecondEDate2 = ""
    OriginalSecondCopay2 = ""
    OriginalSecond2 = ""
    OriginalSecondInsurerId3 = 0
    OriginalSecondMember3 = ""
    OriginalSecondSDate3 = ""
    OriginalSecondEDate3 = ""
    OriginalSecondCopay3 = ""
    OriginalSecond3 = ""
End If
If (FirstPolicyHolderId <> PatId) And (FirstPolicyHolderId > 0) Then
    txtPrimary1.Locked = True
    txtPrimary2.Locked = True
    txtPrimary3.Locked = True
    txtCopayP1.Locked = True
    txtCopayP2.Locked = True
    txtCopayP3.Locked = True
    txtPrimaryMember1.Locked = True
    txtPrimaryMember2.Locked = True
    txtPrimaryMember3.Locked = True
    txtFirstGroup1.Locked = True
    txtFirstGroup2.Locked = True
    txtFirstGroup3.Locked = True
    txtPrimarySDate1.Locked = True
    txtPrimarySDate2.Locked = True
    txtPrimarySDate3.Locked = True
    txtPrimaryEDate1.Locked = True
    txtPrimaryEDate2.Locked = True
    txtPrimaryEDate3.Locked = True
    chkDelete1.Enabled = False
    chkDelete2.Enabled = False
    chkDelete3.Enabled = False
Else
    txtPrimary1.Locked = False
    txtPrimary2.Locked = False
    txtPrimary3.Locked = False
    txtCopayP1.Locked = False
    txtCopayP2.Locked = False
    txtCopayP3.Locked = False
    txtPrimaryMember1.Locked = False
    txtPrimaryMember2.Locked = False
    txtPrimaryMember3.Locked = False
    txtFirstGroup1.Locked = False
    txtFirstGroup2.Locked = False
    txtFirstGroup3.Locked = False
    txtPrimarySDate1.Locked = False
    txtPrimarySDate2.Locked = False
    txtPrimarySDate3.Locked = False
    txtPrimaryEDate1.Locked = False
    txtPrimaryEDate2.Locked = False
    txtPrimaryEDate3.Locked = False
    chkDelete1.Enabled = True
    chkDelete2.Enabled = True
    chkDelete3.Enabled = True
End If
If (SecondPolicyHolderId <> PatId) And (SecondPolicyHolderId > 0) Then
    txtSecond1.Locked = True
    txtSecond2.Locked = True
    txtSecond3.Locked = True
    txtCopayS1.Locked = True
    txtCopayS2.Locked = True
    txtCopayS3.Locked = True
    txtSecondMember1.Locked = True
    txtSecondMember2.Locked = True
    txtSecondMember3.Locked = True
    txtSecondGroup1.Locked = True
    txtSecondGroup2.Locked = True
    txtSecondGroup3.Locked = True
    txtSecondSDate1.Locked = True
    txtSecondSDate2.Locked = True
    txtSecondSDate3.Locked = True
    txtSecondEDate1.Locked = True
    txtSecondEDate2.Locked = True
    txtSecondEDate3.Locked = True
    chkDelete4.Enabled = False
    chkDelete5.Enabled = False
    chkDelete6.Enabled = False
Else
    txtSecond1.Locked = False
    txtSecond2.Locked = False
    txtSecond3.Locked = False
    txtCopayS1.Locked = False
    txtCopayS2.Locked = False
    txtCopayS3.Locked = False
    txtSecondMember1.Locked = False
    txtSecondMember2.Locked = False
    txtSecondMember3.Locked = False
    txtSecondGroup1.Locked = False
    txtSecondGroup2.Locked = False
    txtSecondGroup3.Locked = False
    txtSecondSDate1.Locked = False
    txtSecondSDate2.Locked = False
    txtSecondSDate3.Locked = False
    txtSecondEDate1.Locked = False
    txtSecondEDate2.Locked = False
    txtSecondEDate3.Locked = False
    chkDelete4.Enabled = True
    chkDelete5.Enabled = True
    chkDelete6.Enabled = True
End If
Set LocalDate = Nothing
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
fraAllocate.Visible = False
lblAllocate.Visible = False
lblOSDate.Visible = False
txtOSDate.Visible = False
txtOSDate.Text = ""
lblOEDate.Visible = False
txtOEDate.Visible = False
txtOEDate.Text = ""
cmdQuit.Visible = False
cmdFDone.Visible = False
If ((Trim(txtRel1.Text) <> "") And (Trim(txtRel1.Text) <> "Y")) Or _
   ((Trim(txtRel2.Text) <> "") And (Trim(txtRel2.Text) <> "Y")) Then
    cmdDependent.Visible = True
End If
cmdDemo1.Visible = False
If (PolicyHolder_1 <> PatientId) And (PolicyHolder_1 > 0) Then
    cmdDemo1.Visible = True
End If
cmdDemo2.Visible = False
If (PolicyHolder_2 <> PatientId) And (PolicyHolder_2 > 0) Then
    cmdDemo2.Visible = True
End If
If (PolicyHolder_2 > 0) And (PolicyHolder_1 > 0) Then
    cmdShift.Visible = True
End If
lblPatId.Caption = "ID:" + Trim(Str(PatientId))
lblHldId.Caption = "ID:" + Trim(Str(PolicyHolder_1))
FormLoadOn = False
Exit Function
UIError_Label:
    FormLoadOn = False
    FinancialLoadDisplay = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function SetPolicy(AListbox As ListBox, PolicyHolderid As Long) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim InsRec As Insurer
Dim FinRec As PatientFinance
Dim LocalDate As ManagedDate
Set LocalDate = New ManagedDate
Set InsRec = New Insurer
Set FinRec = New PatientFinance
SetPolicy = True
AListbox.Clear
If (PolicyHolderid > 0) Then
    FinRec.PatientId = PolicyHolderid
    FinRec.FinancialId = 0
    FinRec.PrimaryInsurerId = 0
    FinRec.PrimaryPIndicator = 0
    FinRec.PrimaryStartDate = ""
    FinRec.PrimaryEndDate = ""
    FinRec.PrimaryInsType = ""
    If (lstInsType.ListIndex >= 0) Then
        FinRec.PrimaryInsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    End If
    FinRec.FinancialStatus = "X"
    If (FinRec.FindPatientFinancial > 0) Then
        i = 1
        While (FinRec.SelectPatientFinancial(i))
            If (FinRec.PrimaryInsurerId > 0) Then
                InsRec.InsurerId = FinRec.PrimaryInsurerId
                If (InsRec.RetrieveInsurer) Then
                    DisplayText = Space(120)
                    Mid(DisplayText, 1, Len(Trim(InsRec.InsurerName))) = Trim(InsRec.InsurerName)
                    Mid(DisplayText, 21, Len(Trim(InsRec.InsurerAddress))) = Trim(InsRec.InsurerAddress)
                    Mid(DisplayText, 36, Len(Trim(InsRec.InsurerCity))) = Trim(InsRec.InsurerCity)
                    LocalDate.ExposedDate = FinRec.PrimaryStartDate
                    If (LocalDate.ConvertManagedDateToDisplayDate) Then
                        Mid(DisplayText, 50, 10) = LocalDate.ExposedDate + " "
                    End If
                    LocalDate.ExposedDate = FinRec.PrimaryEndDate
                    If (LocalDate.ConvertManagedDateToDisplayDate) Then
                        Mid(DisplayText, 62, 10) = LocalDate.ExposedDate
                    End If
                    Mid(DisplayText, 72, 20) = Trim(FinRec.PrimaryPerson)
                    Mid(DisplayText, 92, 5) = Trim(FinRec.PrimaryInsType)
                    Mid(DisplayText, 100, 5) = Trim(FinRec.PrimaryPIndicator)
                    Mid(DisplayText, 105, 5) = Trim(FinRec.PrimaryInsType)
                    Mid(DisplayText, 110, 5) = Trim(FinRec.FinancialStatus)
                    DisplayText = DisplayText + Trim(Str(FinRec.FinancialId))
                   
                    AListbox.AddItem DisplayText
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set LocalDate = Nothing
Set InsRec = Nothing
Set FinRec = Nothing
End Function

Private Function GetActiveFinancial(PolicyHolderid As Long, FirstCurrent As Long, SecondCurrent As Long, ThirdCurrent As Long) As Boolean
Dim i As Integer
Dim FinRec As PatientFinance
Set FinRec = New PatientFinance
GetActiveFinancial = False
FirstCurrent = 0
SecondCurrent = 0
ThirdCurrent = 0
If (PolicyHolderid > 0) Then
    FinRec.PatientId = PolicyHolderid
    FinRec.PrimaryInsurerId = 0
    FinRec.FinancialId = 0
    FinRec.PrimaryStartDate = ""
    FinRec.PrimaryEndDate = ""
    FinRec.PrimaryInsType = ""
    If (lstInsType.ListIndex >= 0) Then
        FinRec.PrimaryInsType = Left(lstInsType.List(lstInsType.ListIndex), 1)
    End If
    FinRec.FinancialStatus = "C"
    If (FinRec.FindPatientFinancial > 0) Then
        i = 1
        While (FinRec.SelectPatientFinancial(i))
            If (FinRec.PrimaryPIndicator = 1) Then
                FirstCurrent = FinRec.FinancialId
            ElseIf (FinRec.PrimaryPIndicator = 2) Then
                SecondCurrent = FinRec.FinancialId
            ElseIf (FinRec.PrimaryPIndicator = 3) Then
                ThirdCurrent = FinRec.FinancialId
            End If
            i = i + 1
        Wend
        GetActiveFinancial = True
    End If
End If
Set FinRec = Nothing
End Function

Private Sub cmdRemove1_Click()
Dim p1 As Long, p2 As Long
Dim RetPat As Patient
If (lblPatientName.Caption = FirstPolicyHolder.Text) And (lblPatId.Caption = lblHldId.Caption) Then
    frmEventMsgs.Header = "Cannot remove Self"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
ElseIf (Trim(FirstPolicyHolder.Text) = "") And (ThePolicyHolder1 < 1) Then
    Exit Sub
End If
PostButRemain = True
PostButRemainFailed = False
Call cmdDone_Click
If Not (PostButRemainFailed) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        If (RetPat.SecondPolicyPatientId > 0) Then
            RetPat.PolicyPatientId = RetPat.SecondPolicyPatientId
            RetPat.BillParty = RetPat.SecondBillParty
            RetPat.Relationship = RetPat.SecondRelationship
            RetPat.SecondPolicyPatientId = 0
            RetPat.SecondBillParty = False
            RetPat.SecondRelationship = ""
        Else
            RetPat.PolicyPatientId = PatientId
            RetPat.BillParty = False
            RetPat.Relationship = "Y"
            RetPat.SecondPolicyPatientId = 0
            RetPat.SecondBillParty = False
            RetPat.SecondRelationship = ""
        End If
        If (RetPat.ApplyPatient) Then
            ThePolicyHolder1 = RetPat.PolicyPatientId
            ThePolicyHolder1Rel = RetPat.Relationship
            ThePolicyHolder1Bill = RetPat.BillParty
            ThePolicyHolder2 = RetPat.SecondPolicyPatientId
            ThePolicyHolder2Rel = RetPat.SecondRelationship
            ThePolicyHolder2Bill = RetPat.SecondBillParty
        End If
        lstOldPolicy1.Clear
        Call FinancialLoadDisplay(PatientId, ThePolicyHolder1, ThePolicyHolder2, "M", True, AllowAccessOn)
    End If
    Set RetPat = Nothing
End If
PostButRemain = False
End Sub

Private Sub cmdRemove2_Click()
Dim RetPat As Patient
If (lblPatientName.Caption = SecondPolicyHolder.Text) Then
    frmEventMsgs.Header = "Cannot remove Self"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
ElseIf (Trim(SecondPolicyHolder.Text) = "") And (ThePolicyHolder2 < 1) Then
    Exit Sub
End If
PostButRemain = True
PostButRemainFailed = False
Call cmdDone_Click
If Not (PostButRemainFailed) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        RetPat.SecondPolicyPatientId = 0
        RetPat.SecondBillParty = False
        RetPat.SecondRelationship = ""
        If (RetPat.ApplyPatient) Then
            ThePolicyHolder2 = RetPat.SecondPolicyPatientId
            ThePolicyHolder2Rel = RetPat.SecondRelationship
            ThePolicyHolder2Bill = RetPat.SecondBillParty
        End If
        lstOldPolicy2.Clear
        Call FinancialLoadDisplay(PatientId, ThePolicyHolder1, ThePolicyHolder2, "M", True, AllowAccessOn)
    End If
    Set RetPat = Nothing
End If
PostButRemain = False
End Sub

Private Sub FirstPolicyHolder_Click()
Dim p1 As Long
Dim PatId As Long
p1 = 0
PatId = 0
If (Trim(FirstPolicyHolder.Text) = Trim(lblPatientName.Caption)) Then
    If (Trim(lblPrimaryInsurer1.Caption) <> "") Or _
       (Trim(lblPrimaryInsurer2.Caption) <> "") Or _
       (Trim(lblPrimaryInsurer3.Caption) <> "") Then
        frmEventMsgs.Header = "Archive Policies First"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If
frmPatientSchedulerSearch.AddOn = False
frmPatientSchedulerSearch.Header = "Select Policy Holder"
frmPatientSchedulerSearch.Show 1
PatId = frmPatientSchedulerSearch.PatientId
Unload frmPatientSchedulerSearch
If (PatId < 1) Then
    If (PatId = -1) Then
        frmNewInsured.PatientId = PatientId
        If (frmNewInsured.PatientLoadDisplay(PatientId, False, Trim(frmPatientSchedulerSearch.LastName), True)) Then
            frmNewInsured.Show 1
            If (frmNewInsured.PolicyPatientId > 0) Then
                AllowOtherPartyAccess1 = True
                txtPrimary1.Text = ""
                txtPrimary2.Text = ""
                txtPrimary3.Text = ""
                lblPrimaryInsurer1.Caption = ""
                lblPrimaryInsurer2.Caption = ""
                lblPrimaryInsurer3.Caption = ""
                txtCopayP1.Text = ""
                txtCopayP2.Text = ""
                txtCopayP3.Text = ""
                txtPrimaryMember1.Text = ""
                txtPrimaryMember2.Text = ""
                txtPrimaryMember3.Text = ""
                txtFirstGroup1.Text = ""
                txtFirstGroup2.Text = ""
                txtFirstGroup3.Text = ""
                txtPrimarySDate1.Text = ""
                txtPrimarySDate2.Text = ""
                txtPrimarySDate3.Text = ""
                txtPrimaryEDate1.Text = ""
                txtPrimaryEDate2.Text = ""
                txtPrimaryEDate3.Text = ""
                p1 = frmNewInsured.PolicyPatientId
                Call LoadFirstPolicyHolder(p1)
                txtPrimary1.Locked = False
                txtPrimary2.Locked = False
                txtPrimary3.Locked = False
                txtCopayP1.Locked = False
                txtCopayP2.Locked = False
                txtCopayP3.Locked = False
                txtPrimaryMember1.Locked = False
                txtPrimaryMember2.Locked = False
                txtPrimaryMember3.Locked = False
                txtFirstGroup1.Locked = False
                txtFirstGroup2.Locked = False
                txtFirstGroup3.Locked = False
                txtPrimarySDate1.Locked = False
                txtPrimarySDate2.Locked = False
                txtPrimarySDate3.Locked = False
            End If
        End If
    End If
ElseIf Not (IsPolicyHolderActive(PatId)) Then
    frmEventMsgs.Header = "Policy Holder must have active policies"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
ElseIf (PatId <> PolicyHolder_2) Then
    p1 = PatId
    Call LoadFirstPolicyHolder(p1)
End If
lblPatId.Caption = "ID:" + Trim(Str(PatientId))
lblHldId.Caption = "ID:" + Trim(Str(PolicyHolder_1))
cmdDemo1.Visible = False
If (PolicyHolder_1 <> PatientId) And (PolicyHolder_1 > 0) Then
    cmdDemo1.Visible = True
End If
End Sub

Private Sub FirstPolicyHolder_KeyPress(KeyAscii As Integer)
Call FirstPolicyHolder_Click
KeyAscii = 0
End Sub

Private Sub SecondPolicyHolder_Click()
Dim p1 As Long
Dim PatId As Long
Dim AName As String
p1 = 0
PatId = 0
If (Trim(FirstPolicyHolder.Text) = "") Or (Trim(lblPrimaryInsurer1.Caption) = "") Then
    frmEventMsgs.Header = "Must have a primary policy holder; with a policy"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If (Trim(SecondPolicyHolder.Text) = Trim(lblPatientName.Caption)) Then
    If (Trim(lblSecondInsurer1.Caption) <> "") Or _
       (Trim(lblSecondInsurer2.Caption) <> "") Or _
       (Trim(lblSecondInsurer3.Caption) <> "") Then
        frmEventMsgs.Header = "Archive Policies First"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If
frmPatientSchedulerSearch.AddOn = False
frmPatientSchedulerSearch.Header = "Select Policy Holder"
frmPatientSchedulerSearch.Show 1
PatId = frmPatientSchedulerSearch.PatientId
AName = Trim(frmPatientSchedulerSearch.LastName)
Unload frmPatientSchedulerSearch
If (PatId < 1) Then
    If (PatId = -1) Then
        frmNewInsured.PatientId = PatientId
        If (frmNewInsured.PatientLoadDisplay(0, False, AName)) Then
            frmNewInsured.Show 1
            If (frmNewInsured.PolicyPatientId > 0) Then
                AllowOtherPartyAccess2 = True
                txtSecond1.Text = ""
                txtSecond2.Text = ""
                txtSecond3.Text = ""
                lblSecondInsurer1.Caption = ""
                lblSecondInsurer2.Caption = ""
                lblSecondInsurer3.Caption = ""
                txtCopayS1.Text = ""
                txtCopayS2.Text = ""
                txtCopayS3.Text = ""
                txtSecondMember1.Text = ""
                txtSecondMember2.Text = ""
                txtSecondMember3.Text = ""
                txtSecondGroup1.Text = ""
                txtSecondGroup2.Text = ""
                txtSecondGroup3.Text = ""
                txtSecondSDate1.Text = ""
                txtSecondSDate2.Text = ""
                txtSecondSDate3.Text = ""
                txtSecondEDate1.Text = ""
                txtSecondEDate2.Text = ""
                txtSecondEDate3.Text = ""
                p1 = frmNewInsured.PolicyPatientId
                Call LoadSecondPolicyHolder(p1)
                txtSecond1.Locked = False
                txtSecond2.Locked = False
                txtSecond3.Locked = False
                txtCopayS1.Locked = False
                txtCopayS2.Locked = False
                txtCopayS3.Locked = False
                txtSecondMember1.Locked = False
                txtSecondMember2.Locked = False
                txtSecondMember3.Locked = False
                txtSecondGroup1.Locked = False
                txtSecondGroup2.Locked = False
                txtSecondGroup3.Locked = False
                txtSecondSDate1.Locked = False
                txtSecondSDate2.Locked = False
                txtSecondSDate3.Locked = False
            End If
        End If
    End If
ElseIf Not (IsPolicyHolderActive(PatId)) Then
    If (PatId <> PatientId) And (PatId <> PolicyHolder_1) Then
        frmEventMsgs.Header = "Policy Holder must have active policies"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    Else
        p1 = PatId
        Call LoadSecondPolicyHolder(p1)
    End If
ElseIf (PatId <> PolicyHolder_1) Then
    p1 = PatId
    Call LoadSecondPolicyHolder(p1)
Else
    frmEventMsgs.Header = "Cannot link same policy holder to this account."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
cmdDemo2.Visible = False
If (PolicyHolder_2 <> PatientId) And (PolicyHolder_2 > 0) Then
    cmdDemo2.Visible = True
    cmdShift.Visible = True
End If
End Sub

Private Sub SecondPolicyHolder_KeyPress(KeyAscii As Integer)
Call SecondPolicyHolder_Click
KeyAscii = 0
End Sub

Private Sub EditInsurer(Rec As Long, EditSet As Integer)
Dim OneOn As Boolean
Dim TwoOn As Boolean
Dim ThreeOn As Boolean
Dim Copay As String
Dim InsurerFind As Insurer
OneOn = False
TwoOn = False
ThreeOn = False
Set InsurerFind = New Insurer
InsurerFind.InsurerId = Rec
If (InsurerFind.RetrieveInsurer) Then
    If (EditSet = 1) And (FirstInsurerId1 <> Rec) Then
        FirstInsurerId1 = Rec
        OriginalPrimaryEDate1 = txtPrimaryEDate1.Text
        lblPrimaryInsurer1.Caption = Trim(InsurerFind.InsurerName) + " " _
                                   + Trim(InsurerFind.InsurerPlanName) + " " _
                                   + Trim(InsurerFind.InsurerGroupName)
        lblPrimaryInsurer1.Tag = ""
        lblPrimaryInsurer1.ToolTipText = Trim(FormatMinLength(InsurerFind.InsurerPlanFormat))
        If (ValidatePolicyNumbers) Then
            lblPrimaryInsurer1.Tag = Trim(InsurerFind.InsurerPlanFormat)
        End If
        Call DisplayDollarAmount(Trim(Str(InsurerFind.Copay)), Copay)
        txtCopayP1.Text = Trim(Copay)
        txtPrimaryMember1.Text = ""
        txtFirstGroup1.Text = ""
        txtPrimarySDate1.Text = ""
        txtPrimaryEDate1.Text = ""
        chkDelete1.Visible = True
        chkDelete1.Value = 0
        If (Trim(txtPrimary1.Text) = "") Then
            If (txtPrimary2.Text = "2") Or (txtPrimary3.Text = "2") Then
                TwoOn = True
            End If
            If (txtPrimary2.Text = "3") Or (txtPrimary3.Text = "3") Then
                ThreeOn = True
            End If
            If Not (OneOn) Then
                txtPrimary1.Text = "1"
            ElseIf Not (TwoOn) Then
                txtPrimary1.Text = "2"
            ElseIf Not (ThreeOn) Then
                txtPrimary1.Text = "3"
            Else
                txtPrimary1.Text = ""
            End If
        End If
    ElseIf (EditSet = 2) And (FirstInsurerId2 <> Rec) Then
        FirstInsurerId2 = Rec
        OriginalPrimaryEDate2 = txtPrimaryEDate2.Text
        lblPrimaryInsurer2.Caption = Trim(InsurerFind.InsurerName) + " " _
                                   + Trim(InsurerFind.InsurerPlanName) + " " _
                                   + Trim(InsurerFind.InsurerGroupName)
        lblPrimaryInsurer2.Tag = ""
        lblPrimaryInsurer2.ToolTipText = Trim(FormatMinLength(InsurerFind.InsurerPlanFormat))
        If (ValidatePolicyNumbers) Then
            lblPrimaryInsurer2.Tag = Trim(InsurerFind.InsurerPlanFormat)
        End If
        Call DisplayDollarAmount(Trim(Str(InsurerFind.Copay)), Copay)
        txtCopayP2.Text = Trim(Copay)
        txtPrimaryMember2.Text = ""
        txtFirstGroup2.Text = ""
        txtPrimarySDate2.Text = ""
        txtPrimaryEDate2.Text = ""
        chkDelete2.Visible = True
        chkDelete2.Value = 0
        If (Trim(txtPrimary2.Text) = "") Then
            If (txtPrimary1.Text = "1") Or (txtPrimary3.Text = "1") Then
                OneOn = True
            End If
            If (txtPrimary1.Text = "3") Or (txtPrimary3.Text = "3") Then
                ThreeOn = True
            End If
            If Not (OneOn) Then
                txtPrimary2.Text = "1"
            ElseIf Not (TwoOn) Then
                txtPrimary2.Text = "2"
            ElseIf Not (ThreeOn) Then
                txtPrimary2.Text = "3"
            Else
                txtPrimary2.Text = ""
            End If
        End If
    ElseIf (EditSet = 3) And (FirstInsurerId3 <> Rec) Then
        FirstInsurerId3 = Rec
        OriginalPrimaryEDate3 = txtPrimaryEDate3.Text
        lblPrimaryInsurer3.Caption = Trim(InsurerFind.InsurerName) + " " _
                                   + Trim(InsurerFind.InsurerPlanName) + " " _
                                   + Trim(InsurerFind.InsurerGroupName)
        lblPrimaryInsurer3.Tag = ""
        lblPrimaryInsurer3.ToolTipText = Trim(FormatMinLength(InsurerFind.InsurerPlanFormat))
        If (ValidatePolicyNumbers) Then
            lblPrimaryInsurer3.Tag = Trim(InsurerFind.InsurerPlanFormat)
        End If
        Call DisplayDollarAmount(Trim(Str(InsurerFind.Copay)), Copay)
        txtCopayP3.Text = Trim(Copay)
        txtPrimaryMember3.Text = ""
        txtFirstGroup3.Text = ""
        txtPrimarySDate3.Text = ""
        txtPrimaryEDate3.Text = ""
        chkDelete3.Visible = True
        chkDelete3.Value = 0
        If (Trim(txtPrimary3.Text) = "") Then
            If (txtPrimary2.Text = "2") Or (txtPrimary1.Text = "2") Then
                TwoOn = True
            End If
            If (txtPrimary2.Text = "1") Or (txtPrimary1.Text = "1") Then
                OneOn = True
            End If
            If Not (OneOn) Then
                txtPrimary3.Text = "1"
            ElseIf Not (TwoOn) Then
                txtPrimary3.Text = "2"
            ElseIf Not (ThreeOn) Then
                txtPrimary3.Text = "3"
            Else
                txtPrimary3.Text = ""
            End If
        End If
    ElseIf (EditSet = 4) And (SecondInsurerId1 <> Rec) Then
        SecondInsurerId1 = Rec
        OriginalSecondEDate1 = txtSecondEDate1.Text
        lblSecondInsurer1.Caption = Trim(InsurerFind.InsurerName) + " " _
                                  + Trim(InsurerFind.InsurerPlanName) + " " _
                                  + Trim(InsurerFind.InsurerGroupName)
        lblSecondInsurer1.Tag = ""
        lblSecondInsurer1.ToolTipText = Trim(FormatMinLength(InsurerFind.InsurerPlanFormat))
        If (ValidatePolicyNumbers) Then
            lblSecondInsurer1.Tag = Trim(InsurerFind.InsurerPlanFormat)
        End If
        Call DisplayDollarAmount(Trim(Str(InsurerFind.Copay)), Copay)
        txtCopayS1.Text = Trim(Copay)
        txtSecondMember1.Text = ""
        txtSecondGroup1.Text = ""
        txtSecondSDate1.Text = ""
        txtSecondEDate1.Text = ""
        chkDelete4.Visible = True
        chkDelete4.Value = 0
        If (Trim(txtSecond1.Text) = "") Then
            If (txtSecond2.Text = "2") Or (txtSecond3.Text = "2") Then
                TwoOn = True
            End If
            If (txtSecond2.Text = "3") Or (txtSecond3.Text = "3") Then
                ThreeOn = True
            End If
            If Not (OneOn) Then
                txtSecond1.Text = "1"
            ElseIf Not (TwoOn) Then
                txtSecond1.Text = "2"
            ElseIf Not (ThreeOn) Then
                txtSecond1.Text = "3"
            Else
                txtSecond1.Text = ""
            End If
        End If
    ElseIf (EditSet = 5) And (SecondInsurerId2 <> Rec) Then
        SecondInsurerId2 = Rec
        OriginalSecondEDate2 = txtSecondEDate2.Text
        lblSecondInsurer2.Caption = Trim(InsurerFind.InsurerName) + " " _
                                  + Trim(InsurerFind.InsurerPlanName) + " " _
                                  + Trim(InsurerFind.InsurerGroupName)
        lblSecondInsurer2.Tag = ""
        lblSecondInsurer2.ToolTipText = Trim(FormatMinLength(InsurerFind.InsurerPlanFormat))
        If (ValidatePolicyNumbers) Then
            lblSecondInsurer2.Tag = Trim(InsurerFind.InsurerPlanFormat)
        End If
        Call DisplayDollarAmount(Trim(Str(InsurerFind.Copay)), Copay)
        txtCopayS2.Text = Trim(Copay)
        txtSecondMember2.Text = ""
        txtSecondGroup2.Text = ""
        txtSecondSDate2.Text = ""
        txtSecondEDate2.Text = ""
        chkDelete5.Visible = True
        chkDelete5.Value = 0
        If (Trim(txtSecond2.Text) = "") Then
            If (txtSecond1.Text = "1") Or (txtSecond3.Text = "1") Then
                OneOn = True
            End If
            If (txtSecond1.Text = "3") Or (txtSecond3.Text = "3") Then
                ThreeOn = True
            End If
            If Not (OneOn) Then
                txtSecond2.Text = "1"
            ElseIf Not (TwoOn) Then
                txtSecond2.Text = "2"
            ElseIf Not (ThreeOn) Then
                txtSecond2.Text = "3"
            Else
                txtSecond2.Text = ""
            End If
        End If
    ElseIf (EditSet = 6) And (SecondInsurerId3 <> Rec) Then
        SecondInsurerId3 = Rec
        OriginalSecondEDate3 = txtSecondEDate3.Text
        lblSecondInsurer3.Caption = Trim(InsurerFind.InsurerName) + " " _
                                  + Trim(InsurerFind.InsurerPlanName) + " " _
                                  + Trim(InsurerFind.InsurerGroupName)
        lblSecondInsurer3.Tag = ""
        lblSecondInsurer3.ToolTipText = Trim(FormatMinLength(InsurerFind.InsurerPlanFormat))
        If (ValidatePolicyNumbers) Then
            lblSecondInsurer3.Tag = Trim(InsurerFind.InsurerPlanFormat)
        End If
        Call DisplayDollarAmount(Trim(Str(InsurerFind.Copay)), Copay)
        txtCopayS3.Text = Trim(Copay)
        txtSecondMember3.Text = ""
        txtSecondGroup3.Text = ""
        txtSecondSDate3.Text = ""
        txtSecondEDate3.Text = ""
        chkDelete6.Visible = True
        chkDelete6.Value = 0
        If (Trim(txtSecond1.Text) = "") Then
            If (txtSecond2.Text = "2") Or (txtSecond1.Text = "2") Then
                TwoOn = True
            End If
            If (txtSecond2.Text = "1") Or (txtSecond1.Text = "1") Then
                OneOn = True
            End If
            If Not (OneOn) Then
                txtSecond3.Text = "1"
            ElseIf Not (TwoOn) Then
                txtSecond3.Text = "2"
            ElseIf Not (ThreeOn) Then
                txtSecond3.Text = "3"
            Else
                txtSecond3.Text = ""
            End If
        End If
    End If
End If
EditSet = 0
Set InsurerFind = Nothing
End Sub

Private Sub lblPrimaryInsurer1_Click()
Dim ins As Long
If Not (txtPrimary1.Locked) Or (AllowOtherPartyAccess1) Then
    If (Trim(FirstPolicyHolder.Text) = "") Then
        frmEventMsgs.Header = "Select a Policy Holder"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        FirstPolicyHolder.SetFocus
    ElseIf (Trim(txtPrimaryEDate1.Text) = "") And (Trim(lblPrimaryInsurer1.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId1)
    ElseIf (Trim(lblPrimaryInsurer1.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId1)
    Else
        If (Trim(lblPrimaryInsurer1.Caption) = "") Then
            ins = SetPlans(False)
        Else
            ins = SetPlans(True)
        End If
        If (ins > 0) Then
            Call EditInsurer(ins, 1)
        End If
    End If
Else
    If (Trim(txtPrimaryEDate1.Text) = "") And (Trim(lblPrimaryInsurer1.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId1)
    End If
End If
txtCopayP1.SetFocus
End Sub

Private Sub lblPrimaryInsurer2_Click()
Dim ins As Long
If Not (txtPrimary2.Locked) Or (AllowOtherPartyAccess1) Then
    If (Trim(FirstPolicyHolder.Text) = "") Then
        frmEventMsgs.Header = "Select a Policy Holder"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        FirstPolicyHolder.SetFocus
    ElseIf (Trim(txtPrimaryEDate2.Text) = "") And (Trim(lblPrimaryInsurer2.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId2)
    ElseIf (Trim(lblPrimaryInsurer2.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId2)
    Else
        If (Trim(lblPrimaryInsurer2.Caption) = "") Then
            ins = SetPlans(False)
        Else
            ins = SetPlans(True)
        End If
        If (ins > 0) Then
            Call EditInsurer(ins, 2)
        End If
    End If
Else
    If (Trim(txtPrimaryEDate2.Text) = "") And (Trim(lblPrimaryInsurer2.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId2)
    End If
End If
txtCopayP2.SetFocus
End Sub

Private Sub lblPrimaryInsurer3_Click()
Dim ins As Long
If Not (txtPrimary3.Locked) Or (AllowOtherPartyAccess1) Then
    If (Trim(FirstPolicyHolder.Text) = "") Then
        frmEventMsgs.Header = "Select a Policy Holder"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        FirstPolicyHolder.SetFocus
    ElseIf (Trim(txtPrimaryEDate3.Text) = "") And (Trim(lblPrimaryInsurer3.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId3)
    ElseIf (Trim(lblPrimaryInsurer3.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId3)
    Else
        If (Trim(lblPrimaryInsurer3.Caption) = "") Then
            ins = SetPlans(False)
        Else
            ins = SetPlans(True)
        End If
        If (ins > 0) Then
            Call EditInsurer(ins, 3)
        End If
    End If
Else
    If (Trim(txtPrimaryEDate3.Text) = "") And (Trim(lblPrimaryInsurer3.Caption) <> "") Then
        Call DisplayPlan(FirstInsurerId3)
    End If
End If
txtCopayP3.SetFocus
End Sub

Private Sub lblSecondInsurer1_Click()
Dim ins As Long
If Not (txtSecond1.Locked) Or (AllowOtherPartyAccess2) Then
    If (Trim(SecondPolicyHolder.Text) = "") Then
        frmEventMsgs.Header = "Select a Policy Holder"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SecondPolicyHolder.SetFocus
    ElseIf (Trim(txtSecondEDate1.Text) = "") And (Trim(lblSecondInsurer1.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId1)
    ElseIf (Trim(lblSecondInsurer1.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId1)
    Else
        If (Trim(lblSecondInsurer1.Caption) = "") Then
            ins = SetPlans(False)
        Else
            ins = SetPlans(True)
        End If
        If (ins > 0) Then
            Call EditInsurer(ins, 4)
        End If
    End If
Else
    If (Trim(txtSecondEDate1.Text) = "") And (Trim(lblSecondInsurer1.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId1)
    End If
End If
txtCopayS1.SetFocus
End Sub

Private Sub lblSecondInsurer2_Click()
Dim ins As Long
If Not (txtSecond2.Locked) Or (AllowOtherPartyAccess2) Then
    If (Trim(SecondPolicyHolder.Text) = "") Then
        frmEventMsgs.Header = "Select a Policy Holder"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SecondPolicyHolder.SetFocus
    ElseIf (Trim(txtSecondEDate2.Text) = "") And (Trim(lblSecondInsurer2.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId2)
    ElseIf (Trim(lblSecondInsurer2.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId2)
    Else
        If (Trim(lblSecondInsurer2.Caption) = "") Then
            ins = SetPlans(False)
        Else
            ins = SetPlans(True)
        End If
        If (ins > 0) Then
            Call EditInsurer(ins, 5)
        End If
    End If
Else
    If (Trim(txtSecondEDate2.Text) = "") And (Trim(lblSecondInsurer2.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId2)
    End If
End If
txtCopayS2.SetFocus
End Sub

Private Sub lblSecondInsurer3_Click()
Dim ins As Long
If Not (txtSecond3.Locked) Or (AllowOtherPartyAccess2) Then
    If (Trim(SecondPolicyHolder.Text) = "") Then
        frmEventMsgs.Header = "Select a Policy Holder"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        SecondPolicyHolder.SetFocus
    ElseIf (Trim(txtSecondEDate3.Text) = "") And (Trim(lblSecondInsurer3.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId3)
    ElseIf (Trim(lblSecondInsurer3.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId3)
    Else
        If (Trim(lblSecondInsurer3.Caption) = "") Then
            ins = SetPlans(False)
        Else
            ins = SetPlans(True)
        End If
        If (ins > 0) Then
            Call EditInsurer(ins, 6)
        End If
    End If
Else
    If (Trim(txtSecondEDate3.Text) = "") And (Trim(lblSecondInsurer3.Caption) <> "") Then
        Call DisplayPlan(SecondInsurerId3)
    End If
End If
txtCopayS3.SetFocus
End Sub

Private Function ArchiveFinancial(CurrentId As Long, TheStartDate As String, TheEnddate As String, TheIndicator As String) As Boolean
Dim ADate As ManagedDate
Dim FinRec As PatientFinance
Dim ApplTemp As ApplicationTemplates
Set ADate = New ManagedDate
Set FinRec = New PatientFinance
ArchiveFinancial = False
If (CurrentId > 0) Then
    FinRec.FinancialId = CurrentId
    FinRec.FinancialStatus = ""
    If (FinRec.RetrievePatientFinance) Then
        ADate.ExposedDate = TheStartDate
        If (ADate.ConvertDisplayDateToManagedDate) Then
            FinRec.PrimaryStartDate = ADate.ExposedDate
        End If
        ADate.ExposedDate = TheEnddate
        If (ADate.ConvertDisplayDateToManagedDate) Then
            FinRec.PrimaryEndDate = ADate.ExposedDate
        End If
        If (TheIndicator > "0") And (TheIndicator < "4") Then
            FinRec.PrimaryPIndicator = val(TheIndicator)
        End If
        Dim StartDate As String
        Dim EndDate As String
        Dim PrimaryInsType As String
        Dim PrimaryIndicator As String
        Dim InsType As String
        Dim FinancialStatus As String
        Dim i As Integer
        If lstOldPolicy1.ListCount > 0 Then
        For i = 0 To lstOldPolicy1.ListCount - 1
        PrimaryInsType = Trim(Mid(lstOldPolicy1.List(i), 92, 5))
        PrimaryIndicator = Trim(Mid(lstOldPolicy1.List(i), 100, 5))
        InsType = Trim(Mid(lstOldPolicy1.List(i), 105, 5))
        FinancialStatus = Trim(Mid(lstOldPolicy1.List(i), 110, 5))
        EndDate = Trim(Mid(lstOldPolicy1.List(i), 62, 10))
        StartDate = Trim(Mid(lstOldPolicy1.List(i), 50, 10))
        If FinancialStatus = "X" And FinRec.FinancialStatus = "C" Then
            If PrimaryIndicator = FinRec.PrimaryPIndicator Then
                If PrimaryInsType = FinRec.PrimaryInsType Then
                     If CompareDate(TheStartDate, StartDate) >= 0 And CompareDate(TheStartDate, EndDate) <= 0 Then
                        frmEventMsgs.Header = "Archived policy cannot overlap with current policy"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ArchiveFinancial = False
                        Exit Function
                    ElseIf CompareDate(TheEnddate, StartDate) >= 0 And CompareDate(TheEnddate, EndDate) <= 0 Then
                        frmEventMsgs.Header = "Archived policy cannot overlap with current policy"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ArchiveFinancial = False
                        Exit Function
                    ElseIf CompareDate(TheEnddate, EndDate) >= 0 And (CompareDate(TheStartDate, EndDate) <= 0) Then
                        frmEventMsgs.Header = "Archived policy cannot overlap with current policy"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ArchiveFinancial = False
                        Exit Function
                    End If
                End If
            End If
        End If
        Next i
        End If
        FinRec.FinancialStatus = "X"
        Call FinRec.ApplyPatientFinance
        Set ApplTemp = New ApplicationTemplates
        If (ApplTemp.ApplCloseArchivedTransactions(FinRec.FinancialId)) Then
            ArchiveFinancial = True
        End If
        Set ApplTemp = Nothing
    End If
End If
Set ADate = Nothing
Set FinRec = Nothing
End Function
Private Function ArchivePostedFinancialStartdateandEnddate(FinancialId As Long, TheStartDate As String, TheEnddate As String, TheIndicator As String) As Boolean
ArchivePostedFinancialStartdateandEnddate = True
        Dim StartDate As String
        Dim EndDate As String
        Dim PrimaryIndicator As String
      
        Dim i As Integer
        If lstOldPolicy1.ListCount > 0 Then
        For i = 0 To lstOldPolicy1.ListCount - 1
        
        If Trim(val(Mid(lstOldPolicy1.List(i), 121))) <> FinancialId Then
            PrimaryIndicator = Trim(Mid(lstOldPolicy1.List(i), 100, 5))
            StartDate = Trim(Mid(lstOldPolicy1.List(i), 50, 10))
            EndDate = Trim(Mid(lstOldPolicy1.List(i), 62, 10))
           If PrimaryIndicator = TheIndicator Then
                If CompareDate(TheStartDate, EndDate) <= 0 And CompareDate(TheStartDate, StartDate) >= 0 Then
                        frmEventMsgs.Header = "Archived policy cannot overlap with current policy"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ArchivePostedFinancialStartdateandEnddate = False
                          Exit Function
                ElseIf CompareDate(TheEnddate, EndDate) <= 0 And CompareDate(TheEnddate, StartDate) >= 0 Then
                        frmEventMsgs.Header = "Archived policy cannot overlap with current policy"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ArchivePostedFinancialStartdateandEnddate = False
                        Exit Function
                   
                       End If
            End If
         
        
        End If

          
      Next i
        
        End If
       


End Function


Private Function ArchiveFinancialStartdate(CurrentId As Long, TheStartDate As String, TheIndicator As String) As Boolean

ArchiveFinancialStartdate = True

        Dim StartDate As String
        Dim EndDate As String
        Dim PrimaryIndicator As String
       
        Dim i As Integer
        If lstOldPolicy1.ListCount > 0 Then
        For i = 0 To lstOldPolicy1.ListCount - 1
       PrimaryIndicator = Trim(Mid(lstOldPolicy1.List(i), 100, 5))
       StartDate = Trim(Mid(lstOldPolicy1.List(i), 50, 10))
       EndDate = Trim(Mid(lstOldPolicy1.List(i), 62, 10))
           If PrimaryIndicator = TheIndicator Then
                If CompareDate(TheStartDate, EndDate) <= 0 And CompareDate(TheStartDate, StartDate) >= 0 Then
                        frmEventMsgs.Header = "Archived policy cannot overlap with current policy"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                        ArchiveFinancialStartdate = False
                        Exit Function
                End If
            End If
      Next i
    End If
End Function

Private Sub DisplayPlan(Rec As Long)
If (Rec > 0) Then
    frmInsurer.ReadOnly = True
    frmInsurer.DeleteOn = False
    If (frmInsurer.InsurerLoadDisplay(Rec, "")) Then
        frmInsurer.Show 1
    End If
End If
End Sub

Private Function SetPlans(ReadOn As Boolean) As Long
SetPlans = 0
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("PLAN")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (frmSelectDialogue.Selection = "Create Insurer") Then
        frmInsurer.DeleteOn = False
        frmInsurer.ReadOnly = ReadOn
        If (frmInsurer.InsurerLoadDisplay(0, "")) Then
            frmInsurer.Show 1
            SetPlans = frmInsurer.TheInsurerId
        End If
    ElseIf (frmSelectDialogue.Selection = "Create Plan") Then
        frmInsurer.ReadOnly = ReadOn
        frmInsurer.DeleteOn = False
        If (frmInsurer.InsurerLoadDisplay(0, frmSelectDialogue.InsurerSelected)) Then
            frmInsurer.Show 1
            If (frmInsurer.TheInsurerId > 0) Then
                SetPlans = frmInsurer.TheInsurerId
            ElseIf (Len(Trim(frmSelectDialogue.Selection)) > 56) Then
                SetPlans = val(Mid(frmSelectDialogue.Selection, 57, Len(frmSelectDialogue.Selection) - 56))
            End If
        End If
    Else
        If (Len(frmSelectDialogue.Selection) > 56) Then
            SetPlans = val(Mid(frmSelectDialogue.Selection, 57, Len(frmSelectDialogue.Selection) - 56))
        End If
    End If
End If
End Function

Private Sub txtOSDate_Validate(Cancel As Boolean)
If txtOSDate.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOSDate.SetFocus
    txtOSDate.Text = ""
    SendKeys "{Home}"
    Exit Sub
    End If
    
If Not (OkDate(txtOSDate.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOSDate.SetFocus
    txtOSDate.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtOSDate, "D")
    If (CompareDate(txtOEDate.Text, txtOSDate.Text) < 0) And (Trim(txtOEDate.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtOSDate.SetFocus
        txtOSDate.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtOSDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtOSDate.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtOSDate.SetFocus
        txtOSDate.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
        
    If Not (OkDate(txtOSDate.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtOSDate.SetFocus
        txtOSDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtOSDate, "D")
        If (CompareDate(txtOEDate.Text, txtOSDate.Text) < 0) And (Trim(txtOEDate.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtOSDate.SetFocus
            txtOSDate.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtOEDate_Validate(Cancel As Boolean)
If txtOEDate.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOEDate.SetFocus
    txtOEDate.Text = ""
    SendKeys "{Home}"
    Exit Sub
    End If
    
If Not (OkDate(txtOEDate.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOEDate.SetFocus
    txtOEDate.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtOEDate, "D")
    If (CompareDate(txtOEDate.Text, txtOSDate.Text) < 0) And (Trim(txtOEDate.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtOEDate.SetFocus
        txtOEDate.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtOEDate_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtOEDate.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtOEDate.SetFocus
        txtOEDate.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
        
    If Not (OkDate(txtOEDate.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtOEDate.SetFocus
        txtOEDate.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtOEDate, "D")
        If (CompareDate(txtOEDate.Text, txtOSDate.Text) < 0) And (Trim(txtOEDate.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtOEDate.SetFocus
            txtOEDate.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtPrimarySDate1_Validate(Cancel As Boolean)
If lblPrimaryInsurer1.Caption = "" Then Exit Sub
If txtPrimarySDate1.Text = "" Then

    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimarySDate1.SetFocus
    txtPrimarySDate1.Text = ""
    SendKeys "{Home}"
        
    Exit Sub


End If



Dim NowDate As String
Dim EndDate As String
If Not (OkDate(txtPrimarySDate1.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimarySDate1.SetFocus
    txtPrimarySDate1.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtPrimarySDate1, "D")
    Call FormatTodaysDate(NowDate, False)
    If CompareDate(txtPrimarySDate1.Text, NowDate) > 0 Then
        frmEventMsgs.Header = "Do you want to enter a future date?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate1.SetFocus
        SendKeys "{Home}"
    End If
    If (CompareDate(txtPrimaryEDate1.Text, txtPrimarySDate1.Text) < 0) And (Trim(txtPrimaryEDate1.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate1.SetFocus
        txtPrimarySDate1.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtPrimarySDate1_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then


    If txtPrimarySDate1.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimarySDate1.SetFocus
        txtPrimarySDate1.Text = ""
        SendKeys "{Home}"
        Exit Sub
    End If

    If Not (OkDate(txtPrimarySDate1.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimarySDate1.SetFocus
        txtPrimarySDate1.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPrimarySDate1, "D")
        If (CompareDate(txtPrimaryEDate1.Text, txtPrimarySDate1.Text) < 0) And (Trim(txtPrimaryEDate1.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtPrimarySDate1.SetFocus
            txtPrimarySDate1.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtPrimarySDate2_Validate(Cancel As Boolean)
If lblPrimaryInsurer2.Caption = "" Then Exit Sub
If txtPrimarySDate2.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimarySDate2.SetFocus
    txtPrimarySDate2.Text = ""
    SendKeys "{Home}"
Exit Sub
End If

Dim EndDate As String
Dim NowDate As String
If Not (OkDate(txtPrimarySDate2.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimarySDate2.SetFocus
    txtPrimarySDate2.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtPrimarySDate2, "D")
    Call FormatTodaysDate(NowDate, False)
    If CompareDate(txtPrimarySDate2.Text, NowDate) > 0 Then
        frmEventMsgs.Header = "Do you want to enter a future date?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate2.SetFocus
        SendKeys "{Home}"
    End If
    If (CompareDate(txtPrimaryEDate2.Text, txtPrimarySDate2.Text) < 0) And (Trim(txtPrimaryEDate2.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate2.SetFocus
        txtPrimarySDate2.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtPrimarySDate2_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
   If txtPrimarySDate2.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimarySDate2.SetFocus
        txtPrimarySDate2.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtPrimarySDate2.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimarySDate2.SetFocus
        txtPrimarySDate2.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPrimarySDate2, "D")
        If (CompareDate(txtPrimaryEDate2.Text, txtPrimarySDate2.Text) < 0) And (Trim(txtPrimaryEDate2.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtPrimarySDate2.SetFocus
            txtPrimarySDate2.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtPrimarySDate3_Validate(Cancel As Boolean)
If lblPrimaryInsurer3.Caption = "" Then Exit Sub
If txtPrimarySDate3.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimarySDate3.SetFocus
    txtPrimarySDate3.Text = ""
    SendKeys "{Home}"
Exit Sub
End If

Dim EndDate As String
Dim NowDate As String
If Not (OkDate(txtPrimarySDate3.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimarySDate3.SetFocus
    txtPrimarySDate3.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtPrimarySDate3, "D")
    Call FormatTodaysDate(NowDate, False)
    If CompareDate(txtPrimarySDate3.Text, NowDate) > 0 Then
        frmEventMsgs.Header = "Do you want to enter a future date?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate3.SetFocus
        SendKeys "{Home}"
    End If
    If (CompareDate(txtPrimaryEDate3.Text, txtPrimarySDate3.Text) < 0) And (Trim(txtPrimaryEDate3.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimarySDate3.SetFocus
        txtPrimarySDate3.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtPrimarySDate3_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtPrimarySDate3.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimarySDate3.SetFocus
        txtPrimarySDate3.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtPrimarySDate3.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimarySDate3.SetFocus
        txtPrimarySDate3.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPrimarySDate3, "D")
        If (CompareDate(txtPrimaryEDate3.Text, txtPrimarySDate3.Text) < 0) And (Trim(txtPrimaryEDate3.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtPrimarySDate3.SetFocus
            txtPrimarySDate3.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtPrimaryEDate1_Validate(Cancel As Boolean)
If txtPrimaryEDate1.Text = "" Then
    Exit Sub
End If

Dim NowDate As String
If Not (OkDate(txtPrimaryEDate1.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimaryEDate1.SetFocus
    txtPrimaryEDate1.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtPrimaryEDate1, "D")
    Call FormatTodaysDate(NowDate, False)
    If CompareDate(txtPrimaryEDate1.Text, NowDate) > 0 Then
        frmEventMsgs.Header = "Do you want to enter a future date?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryEDate1.SetFocus
        SendKeys "{Home}"
    End If
    If (CompareDate(txtPrimaryEDate1.Text, txtPrimarySDate1.Text) < 0) And (Trim(txtPrimaryEDate1.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryEDate1.SetFocus
        txtPrimaryEDate1.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtPrimaryEDate1_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtPrimaryEDate1.Text = "" Then
frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimaryEDate1.SetFocus
        txtPrimaryEDate1.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtPrimaryEDate1.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimaryEDate1.SetFocus
        txtPrimaryEDate1.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPrimaryEDate1, "D")
        If (CompareDate(txtPrimaryEDate1.Text, txtPrimarySDate1.Text) < 0) And (Trim(txtPrimaryEDate1.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtPrimaryEDate1.SetFocus
            txtPrimaryEDate1.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtPrimaryEDate2_Validate(Cancel As Boolean)
If txtPrimaryEDate2.Text = "" Then
    Exit Sub
End If
Dim NowDate As String
If Not (OkDate(txtPrimaryEDate2.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimaryEDate2.SetFocus
    txtPrimaryEDate2.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtPrimaryEDate2, "D")
    Call FormatTodaysDate(NowDate, False)
    If CompareDate(txtPrimaryEDate2.Text, NowDate) > 0 Then
        frmEventMsgs.Header = "Do you want to enter a future date?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryEDate2.SetFocus
        SendKeys "{Home}"
    End If
    If (CompareDate(txtPrimaryEDate2.Text, txtPrimarySDate2.Text) < 0) And (Trim(txtPrimaryEDate2.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryEDate2.SetFocus
        txtPrimaryEDate2.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtPrimaryEDate2_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtPrimaryEDate2.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimaryEDate2.SetFocus
        txtPrimaryEDate2.Text = ""
        SendKeys "{Home}"
Exit Sub
End If
    If Not (OkDate(txtPrimaryEDate2.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimaryEDate2.SetFocus
        txtPrimaryEDate2.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPrimaryEDate2, "D")
        If (CompareDate(txtPrimaryEDate2.Text, txtPrimarySDate2.Text) < 0) And (Trim(txtPrimaryEDate2.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtPrimaryEDate2.SetFocus
            txtPrimaryEDate2.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtPrimaryEDate3_Validate(Cancel As Boolean)
If txtPrimaryEDate3.Text = "" Then Exit Sub

Dim NowDate As String
If Not (OkDate(txtPrimaryEDate3.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtPrimaryEDate3.SetFocus
    txtPrimaryEDate3.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtPrimaryEDate3, "D")
    Call FormatTodaysDate(NowDate, False)
    If CompareDate(txtPrimaryEDate3.Text, NowDate) > 0 Then
        frmEventMsgs.Header = "Do you want to enter a future date?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryEDate3.SetFocus
        SendKeys "{Home}"
    End If
    If (CompareDate(txtPrimaryEDate3.Text, txtPrimarySDate3.Text) < 0) And (Trim(txtPrimaryEDate3.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtPrimaryEDate3.SetFocus
        txtPrimaryEDate3.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtPrimaryEDate3_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtPrimaryEDate3.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimaryEDate3.SetFocus
        txtPrimaryEDate3.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtPrimaryEDate3.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtPrimaryEDate3.SetFocus
        txtPrimaryEDate3.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtPrimaryEDate3, "D")
        If (CompareDate(txtPrimaryEDate3.Text, txtPrimarySDate3.Text) < 0) And (Trim(txtPrimaryEDate3.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtPrimaryEDate3.SetFocus
            txtPrimaryEDate3.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtRel1_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("Relationship")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (UCase(Left(frmSelectDialogue.Selection, 1)) <> "Y") And (PatientId = PolicyHolder_1) Then
        frmEventMsgs.Header = "Must be Yourself Only"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRel1.Text = "Y"
    Else
        txtRel1.Text = UCase(Left(frmSelectDialogue.Selection, 1))
        ThePolicyHolder1Rel = Trim(txtRel1.Text)
        If (Trim(txtRel1.Text) <> "Y") And Trim((txtRel1.Text) <> "") Then
            cmdDependent.Visible = True
        ElseIf (Trim(txtRel2.Text) <> "Y") And Trim((txtRel2.Text) <> "") Then
            cmdDependent.Visible = True
        End If
    End If
End If
End Sub

Private Sub txtRel1_KeyPress(KeyAscii As Integer)
Call txtRel1_Click
KeyAscii = 0
End Sub

Private Sub txtRel2_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("Relationship")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    If (UCase(Left(frmSelectDialogue.Selection, 1)) <> "Y") And (PatientId = PolicyHolder_2) Then
        frmEventMsgs.Header = "Must be Yourself Only"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtRel2.Text = "Y"
    Else
        txtRel2.Text = UCase(Left(frmSelectDialogue.Selection, 1))
        ThePolicyHolder2Rel = Trim(txtRel2.Text)
        If (Trim(txtRel2.Text) <> "Y") And Trim((txtRel2.Text) <> "") Then
            cmdDependent.Visible = True
        ElseIf (Trim(txtRel1.Text) <> "Y") And (Trim(txtRel1.Text) <> "") Then
            cmdDependent.Visible = True
        End If
    End If
End If
End Sub

Private Sub txtRel2_KeyPress(KeyAscii As Integer)
Call txtRel2_Click
KeyAscii = 0
End Sub

Private Sub txtSecondSDate1_Validate(Cancel As Boolean)
If lblSecondInsurer1.Caption = "" Then Exit Sub
If txtSecondSDate1.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondSDate1.SetFocus
    txtSecondSDate1.Text = ""
    SendKeys "{Home}"
    Exit Sub
    End If
    
If Not (OkDate(txtSecondSDate1.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondSDate1.SetFocus
    txtSecondSDate1.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtSecondSDate1, "D")
    If (CompareDate(txtSecondEDate1.Text, txtSecondSDate1.Text) < 0) And (Trim(txtSecondEDate1.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondSDate1.SetFocus
        txtSecondSDate1.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtSecondSDate1_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtSecondSDate1.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondSDate1.SetFocus
        txtSecondSDate1.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtSecondSDate1.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondSDate1.SetFocus
        txtSecondSDate1.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSecondSDate1, "D")
        If (CompareDate(txtSecondEDate1.Text, txtSecondSDate1.Text) < 0) And (Trim(txtSecondEDate1.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtSecondSDate1.SetFocus
            txtSecondSDate1.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtSecondSDate2_Validate(Cancel As Boolean)
If lblSecondInsurer2.Caption = "" Then Exit Sub
If txtSecondSDate2.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondSDate2.SetFocus
    txtSecondSDate2.Text = ""
    SendKeys "{Home}"
    Exit Sub
    End If
If Not (OkDate(txtSecondSDate2.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondSDate2.SetFocus
    txtSecondSDate2.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtSecondSDate2, "D")
    If (CompareDate(txtSecondEDate2.Text, txtSecondSDate2.Text) < 0) And (Trim(txtSecondEDate2.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondSDate2.SetFocus
        txtSecondSDate2.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtSecondSDate2_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtSecondSDate2.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondSDate2.SetFocus
        txtSecondSDate2.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
        
    If Not (OkDate(txtSecondSDate2.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondSDate2.SetFocus
        txtSecondSDate2.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSecondSDate2, "D")
        If (CompareDate(txtSecondEDate2.Text, txtSecondSDate2.Text) < 0) And (Trim(txtSecondEDate2.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtSecondSDate2.SetFocus
            txtSecondSDate2.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtSecondSDate3_Validate(Cancel As Boolean)
If lblSecondInsurer3.Caption = "" Then Exit Sub
If txtSecondSDate3.Text = "" Then
    frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondSDate3.SetFocus
    txtSecondSDate3.Text = ""
    SendKeys "{Home}"
    Exit Sub
    End If
If Not (OkDate(txtSecondSDate3.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondSDate3.SetFocus
    txtSecondSDate3.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtSecondSDate3, "D")
    If (CompareDate(txtSecondEDate3.Text, txtSecondSDate3.Text) < 0) And (Trim(txtSecondEDate3.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondSDate3.SetFocus
        txtSecondSDate3.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtSecondSDate3_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtSecondSDate3.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondSDate3.SetFocus
        txtSecondSDate3.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtSecondSDate3.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondSDate3.SetFocus
        txtSecondSDate3.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSecondSDate3, "D")
        If (CompareDate(txtSecondEDate3.Text, txtSecondSDate3.Text) < 0) And (Trim(txtSecondEDate3.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtSecondSDate3.SetFocus
            txtSecondSDate3.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtSecondEDate1_Validate(Cancel As Boolean)
If txtSecondEDate1.Text = "" Then
    Exit Sub
End If
If Not (OkDate(txtSecondEDate1.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondEDate1.SetFocus
    txtSecondEDate1.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtSecondEDate1, "D")
    If (CompareDate(txtSecondEDate1.Text, txtSecondSDate1.Text) < 0) And (Trim(txtSecondEDate1.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondEDate1.SetFocus
        txtSecondEDate1.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtSecondEDate1_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtSecondEDate1.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondEDate1.SetFocus
        txtSecondEDate1.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
    If Not (OkDate(txtSecondEDate1.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondEDate1.SetFocus
        txtSecondEDate1.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSecondEDate1, "D")
        If (CompareDate(txtSecondEDate1.Text, txtSecondSDate1.Text) < 0) And (Trim(txtSecondEDate1.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtSecondEDate1.SetFocus
            txtSecondEDate1.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtSecondEDate2_Validate(Cancel As Boolean)
If txtSecondEDate2.Text = "" Then
    Exit Sub
End If

If Not (OkDate(txtSecondEDate2.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondEDate2.SetFocus
    txtSecondEDate2.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtSecondEDate2, "D")
    If (CompareDate(txtSecondEDate2.Text, txtSecondSDate2.Text) < 0) And (Trim(txtSecondEDate2.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondEDate2.SetFocus
        txtSecondEDate2.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtSecondEDate2_KeyPress(KeyAscii As Integer)
If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
If txtSecondEDate2.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondEDate2.SetFocus
        txtSecondEDate2.Text = ""
        SendKeys "{Home}"
        Exit Sub
        End If
        
    If Not (OkDate(txtSecondEDate2.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondEDate2.SetFocus
        txtSecondEDate2.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSecondEDate2, "D")
        If (CompareDate(txtSecondEDate2.Text, txtSecondSDate2.Text) < 0) And (Trim(txtSecondEDate2.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtSecondEDate2.SetFocus
            txtSecondEDate2.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Sub txtSecondEDate3_Validate(Cancel As Boolean)
If txtSecondEDate3.Text = "" Then
    Exit Sub
End If
    
If Not (OkDate(txtSecondEDate3.Text)) Then
    frmEventMsgs.Header = "Invalid Date"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtSecondEDate3.SetFocus
    txtSecondEDate3.Text = ""
    SendKeys "{Home}"
Else
    Call UpdateDisplay(txtSecondEDate3, "D")
    If (CompareDate(txtSecondEDate3.Text, txtSecondSDate3.Text) < 0) And (Trim(txtSecondEDate3.Text) <> "") Then
        frmEventMsgs.Header = "Start Date precedes End Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtSecondEDate3.SetFocus
        txtSecondEDate3.Text = ""
        SendKeys "{Home}"
    End If
End If
End Sub

Private Sub txtSecondEDate3_KeyPress(KeyAscii As Integer)
If txtSecondEDate3.Text = "" Then
        frmEventMsgs.Header = "Dates are Entered MM/DD/YYYY"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondEDate3.SetFocus
        txtSecondEDate3.Text = ""
        SendKeys "{Home}"
Exit Sub
End If

If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
    If Not (OkDate(txtSecondEDate3.Text)) Then
        frmEventMsgs.Header = "Invalid Date"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        KeyAscii = 0
        txtSecondEDate3.SetFocus
        txtSecondEDate3.Text = ""
        SendKeys "{Home}"
    Else
        Call UpdateDisplay(txtSecondEDate3, "D")
        If (CompareDate(txtSecondEDate3.Text, txtSecondSDate3.Text) < 0) And (Trim(txtSecondEDate3.Text) <> "") Then
            frmEventMsgs.Header = "Start Date precedes End Date"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            KeyAscii = 0
            txtSecondEDate3.SetFocus
            txtSecondEDate3.Text = ""
            SendKeys "{Home}"
        End If
    End If
End If
End Sub

Private Function LoadFirstPolicyHolder(PolicyHolderid As Long) As Boolean
Dim Copay As String
Dim LocalDate As ManagedDate
LoadFirstPolicyHolder = True
Set LocalDate = New ManagedDate
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
lblDelete1.Visible = True
If (PolicyHolderid > 0) Then
    FirstInsurerId1 = 0
    FirstInsurerId2 = 0
    FirstInsurerId3 = 0
    Call GetActiveFinancial(PolicyHolderid, PrimaryFirstCurrent, PrimarySecondCurrent, PrimaryThirdCurrent)
    Call SetPolicy(lstOldPolicy1, PolicyHolderid)
    ThePolicyHolder1 = PolicyHolderid
    If (PolicyHolderid > 0) Then
        RetrieveAnotherPatient.PatientId = PolicyHolderid
        If (RetrieveAnotherPatient.RetrievePatient) Then
            cmdRemove1.Visible = True
            PolicyHolder_1 = PolicyHolderid
            OriginalPolicyHolder_1 = 0
            FirstPolicyHolder.Text = RetrieveAnotherPatient.FirstName + " " _
                                    + RetrieveAnotherPatient.MiddleInitial + " " _
                                    + RetrieveAnotherPatient.LastName + " " _
                                    + RetrieveAnotherPatient.NameRef
            txtRel1.Text = ThePolicyHolder1Rel
            If (ThePolicyHolder1Bill) Then
                chkBill1.Value = 1
            Else
                chkBill1.Value = 0
            End If
            If (PrimaryFirstCurrent > 0) Then
                chkDelete1.Visible = True
                RetrievePatientFinancial.PatientId = PolicyHolderid
                RetrievePatientFinancial.FinancialId = PrimaryFirstCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.FinancialStatus = "C"
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId1 = RetrievePatientFinancial.PrimaryInsurerId
                            OriginalPrimaryInsurerId1 = FirstInsurerId1
                            lblPrimaryInsurer1.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP1.Text = Trim(Copay)
                            OriginalPrimaryCopay1 = Trim(Copay)
                            txtPrimaryMember1.Text = RetrievePatientFinancial.PrimaryPerson
                            txtFirstGroup1.Text = RetrievePatientFinancial.PrimaryGroup
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtPrimarySDate1.Text = LocalDate.ExposedDate
                            End If
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtPrimaryEDate1.Text = LocalDate.ExposedDate
                            End If
                            txtPrimary1.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                            OriginalPrimaryMember1 = txtPrimaryMember1.Text
                            OriginalPrimaryGroup1 = txtFirstGroup1.Text
                            OriginalPrimarySDate1 = txtPrimarySDate1.Text
                            OriginalPrimaryEDate1 = txtPrimaryEDate1.Text
                            OriginalPrimary1 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        Else
                            FirstInsurerId1 = 0
                            OriginalPrimaryInsurerId1 = 0
                            OriginalPrimaryCopay1 = ""
                            OriginalPrimaryMember1 = ""
                            OriginalPrimaryGroup1 = ""
                            OriginalPrimarySDate1 = ""
                            OriginalPrimaryEDate1 = ""
                            OriginalPrimary1 = ""
                        End If
                    End If
                Else
                    OriginalPrimaryInsurerId1 = 0
                    OriginalPrimaryCopay1 = ""
                    OriginalPrimaryMember1 = ""
                    OriginalPrimaryGroup1 = ""
                    OriginalPrimarySDate1 = ""
                    OriginalPrimaryEDate1 = ""
                    OriginalPrimary1 = ""
                End If
            Else
                OriginalPrimaryInsurerId1 = 0
                OriginalPrimaryMember1 = ""
                OriginalPrimaryGroup1 = ""
                OriginalPrimarySDate1 = ""
                OriginalPrimaryEDate1 = ""
                OriginalPrimaryCopay1 = ""
                OriginalPrimary1 = ""
            End If
            If (PrimarySecondCurrent > 0) Then
                chkDelete2.Visible = True
                RetrievePatientFinancial.PatientId = PolicyHolderid
                RetrievePatientFinancial.FinancialId = PrimarySecondCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId2 = RetrievePatientFinancial.PrimaryInsurerId
                            OriginalPrimaryInsurerId2 = FirstInsurerId2
                            lblPrimaryInsurer2.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP2.Text = Trim(Copay)
                            OriginalPrimaryCopay2 = Trim(Copay)
                            txtPrimaryMember2.Text = RetrievePatientFinancial.PrimaryPerson
                            txtFirstGroup2.Text = RetrievePatientFinancial.PrimaryGroup
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtPrimarySDate2.Text = LocalDate.ExposedDate
                            End If
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtPrimaryEDate2.Text = LocalDate.ExposedDate
                            End If
                            txtPrimary2.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                            OriginalPrimaryMember2 = txtPrimaryMember2.Text
                            OriginalPrimaryGroup2 = txtFirstGroup2.Text
                            OriginalPrimarySDate2 = txtPrimarySDate2.Text
                            OriginalPrimaryEDate2 = txtPrimaryEDate2.Text
                            OriginalPrimary2 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        Else
                            FirstInsurerId2 = 0
                            OriginalPrimaryInsurerId2 = 0
                            OriginalPrimaryCopay2 = ""
                            OriginalPrimaryMember2 = ""
                            OriginalPrimaryGroup2 = ""
                            OriginalPrimarySDate2 = ""
                            OriginalPrimaryEDate2 = ""
                            OriginalPrimary2 = ""
                        End If
                    End If
                End If
            Else
                OriginalPrimaryInsurerId2 = 0
                OriginalPrimaryMember2 = ""
                OriginalPrimaryGroup2 = ""
                OriginalPrimarySDate2 = ""
                OriginalPrimaryEDate2 = ""
                OriginalPrimaryCopay2 = ""
                OriginalPrimary2 = ""
            End If
            If (PrimaryThirdCurrent > 0) Then
                chkDelete3.Visible = True
                RetrievePatientFinancial.PatientId = PolicyHolderid
                RetrievePatientFinancial.FinancialId = PrimaryThirdCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            FirstInsurerId3 = RetrievePatientFinancial.PrimaryInsurerId
                            OriginalPrimaryInsurerId3 = FirstInsurerId3
                            lblPrimaryInsurer3.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                       + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayP3.Text = Trim(Copay)
                            OriginalPrimaryCopay3 = Trim(Copay)
                            txtPrimaryMember3.Text = RetrievePatientFinancial.PrimaryPerson
                            txtFirstGroup3.Text = RetrievePatientFinancial.PrimaryGroup
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtPrimarySDate3.Text = LocalDate.ExposedDate
                            End If
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtPrimaryEDate3.Text = LocalDate.ExposedDate
                            End If
                            txtPrimary3.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                            OriginalPrimaryMember3 = txtPrimaryMember3.Text
                            OriginalPrimaryGroup3 = txtFirstGroup3.Text
                            OriginalPrimarySDate3 = txtPrimarySDate3.Text
                            OriginalPrimaryEDate3 = txtPrimaryEDate3.Text
                            OriginalPrimary3 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        Else
                            FirstInsurerId3 = 0
                            OriginalPrimaryInsurerId3 = 0
                            OriginalPrimaryCopay3 = ""
                            OriginalPrimaryMember3 = ""
                            OriginalPrimaryGroup3 = ""
                            OriginalPrimarySDate3 = ""
                            OriginalPrimaryEDate3 = ""
                            OriginalPrimary3 = ""
                        End If
                    End If
                End If
            Else
                OriginalPrimaryInsurerId3 = 0
                OriginalPrimaryMember3 = ""
                OriginalPrimaryGroup3 = ""
                OriginalPrimarySDate3 = ""
                OriginalPrimaryEDate3 = ""
                OriginalPrimaryCopay3 = ""
                OriginalPrimary3 = ""
            End If
        End If
    Else
        OriginalPrimaryInsurerId1 = 0
        OriginalPrimaryMember1 = ""
        OriginalPrimaryGroup1 = ""
        OriginalPrimarySDate1 = ""
        OriginalPrimaryEDate1 = ""
        OriginalPrimaryCopay1 = ""
        OriginalPrimary1 = ""
        OriginalPrimaryInsurerId2 = 0
        OriginalPrimaryMember2 = ""
        OriginalPrimaryGroup2 = ""
        OriginalPrimarySDate2 = ""
        OriginalPrimaryEDate2 = ""
        OriginalPrimaryCopay2 = ""
        OriginalPrimary2 = ""
        OriginalPrimaryInsurerId3 = 0
        OriginalPrimaryMember3 = ""
        OriginalPrimaryGroup3 = ""
        OriginalPrimarySDate3 = ""
        OriginalPrimaryEDate3 = ""
        OriginalPrimaryCopay3 = ""
        OriginalPrimary3 = ""
    End If
End If
Set LocalDate = Nothing
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
If (PolicyHolderid <> PatientId) And (PolicyHolderid > 0) Then
    txtPrimary1.Locked = True
    txtPrimary2.Locked = True
    txtPrimary3.Locked = True
    txtCopayP1.Locked = True
    txtCopayP2.Locked = True
    txtCopayP3.Locked = True
    txtPrimaryMember1.Locked = True
    txtPrimaryMember2.Locked = True
    txtPrimaryMember3.Locked = True
    txtFirstGroup1.Locked = True
    txtFirstGroup2.Locked = True
    txtFirstGroup3.Locked = True
    txtPrimarySDate1.Locked = True
    txtPrimarySDate2.Locked = True
    txtPrimarySDate3.Locked = True
    txtPrimaryEDate1.Locked = True
    txtPrimaryEDate2.Locked = True
    txtPrimaryEDate3.Locked = True
    chkDelete1.Enabled = False
    chkDelete2.Enabled = False
    chkDelete3.Enabled = False
Else
    txtPrimary1.Locked = False
    txtPrimary2.Locked = False
    txtPrimary3.Locked = False
    txtCopayP1.Locked = False
    txtCopayP2.Locked = False
    txtCopayP3.Locked = False
    txtPrimaryMember1.Locked = False
    txtPrimaryMember2.Locked = False
    txtPrimaryMember3.Locked = False
    txtFirstGroup1.Locked = False
    txtFirstGroup2.Locked = False
    txtFirstGroup3.Locked = False
    txtPrimarySDate1.Locked = False
    txtPrimarySDate2.Locked = False
    txtPrimarySDate3.Locked = False
    txtPrimaryEDate1.Locked = False
    txtPrimaryEDate2.Locked = False
    txtPrimaryEDate3.Locked = False
    chkDelete1.Enabled = True
    chkDelete2.Enabled = True
    chkDelete3.Enabled = True
End If
fraAllocate.Visible = False
lblAllocate.Visible = False
lblOSDate.Visible = False
txtOSDate.Visible = False
txtOSDate.Text = ""
lblOEDate.Visible = False
txtOEDate.Visible = False
txtOEDate.Text = ""
cmdQuit.Visible = False
cmdFDone.Visible = False
End Function

Private Function LoadSecondPolicyHolder(PolicyHolderid As Long) As Boolean
Dim Copay As String
Dim Initial As Boolean
Dim LocalDate As ManagedDate
LoadSecondPolicyHolder = True
Set LocalDate = New ManagedDate
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
lblDelete2.Visible = True
If (PolicyHolderid > 0) Then
    SecondInsurerId1 = 0
    SecondInsurerId2 = 0
    SecondInsurerId3 = 0
    Call GetActiveFinancial(PolicyHolderid, SecondFirstCurrent, SecondSecondCurrent, SecondThirdCurrent)
    Call SetPolicy(lstOldPolicy2, PolicyHolderid)
    ThePolicyHolder2 = PolicyHolderid
    If (PolicyHolderid > 0) Then
        RetrieveAnotherPatient.PatientId = PolicyHolderid
        If (RetrieveAnotherPatient.RetrievePatient) Then
            cmdRemove2.Visible = True
            PolicyHolder_2 = PolicyHolderid
            OriginalPolicyHolder_2 = PolicyHolder_2
            SecondPolicyHolder.Text = RetrieveAnotherPatient.FirstName + " " _
                                    + RetrieveAnotherPatient.MiddleInitial + " " _
                                    + RetrieveAnotherPatient.LastName + " " _
                                    + RetrieveAnotherPatient.NameRef
            txtRel2.Text = ThePolicyHolder2Rel
            If (ThePolicyHolder2Bill) Then
                chkBill2.Value = 1
            Else
                chkBill2.Value = 0
            End If
            If (SecondFirstCurrent <> 0) Then
                chkDelete4.Visible = True
                RetrievePatientFinancial.PatientId = PolicyHolderid
                RetrievePatientFinancial.FinancialId = SecondFirstCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId1 = RetrievePatientFinancial.PrimaryInsurerId
                            OriginalSecondInsurerId1 = SecondInsurerId1
                            lblSecondInsurer1.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS1.Text = Trim(Copay)
                            OriginalSecondCopay1 = Trim(Copay)
                            txtSecondMember1.Text = RetrievePatientFinancial.PrimaryPerson
                            txtSecondGroup1.Text = RetrievePatientFinancial.PrimaryGroup
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtSecondSDate1.Text = LocalDate.ExposedDate
                            End If
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtSecondEDate1.Text = LocalDate.ExposedDate
                            End If
                            txtSecond1.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                            OriginalSecondGroup1 = txtSecondGroup1.Text
                            OriginalSecondMember1 = txtSecondMember1.Text
                            OriginalSecondSDate1 = txtSecondSDate1.Text
                            OriginalSecondEDate1 = txtSecondEDate1.Text
                            OriginalSecond1 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        Else
                            SecondInsurerId1 = 0
                            OriginalSecondInsurerId1 = 0
                            OriginalSecondCopay1 = ""
                            OriginalSecondMember1 = ""
                            OriginalSecondGroup1 = ""
                            OriginalSecondSDate1 = ""
                            OriginalSecondEDate1 = ""
                            OriginalSecond1 = ""
                        End If
                    End If
                End If
            Else
                OriginalSecondInsurerId1 = 0
                OriginalSecondMember1 = ""
                OriginalSecondSDate1 = ""
                OriginalSecondEDate1 = ""
                OriginalSecondCopay1 = ""
                OriginalSecond1 = ""
            End If
            If (SecondSecondCurrent <> 0) Then
                chkDelete5.Visible = True
                RetrievePatientFinancial.PatientId = PolicyHolderid
                RetrievePatientFinancial.FinancialId = SecondSecondCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId2 = RetrievePatientFinancial.PrimaryInsurerId
                            OriginalSecondInsurerId2 = SecondInsurerId2
                            lblSecondInsurer2.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS2.Text = Trim(Copay)
                            OriginalSecondCopay2 = Trim(Copay)
                            txtSecondGroup2.Text = RetrievePatientFinancial.PrimaryGroup
                            txtSecondMember2.Text = RetrievePatientFinancial.PrimaryPerson
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtSecondSDate2.Text = LocalDate.ExposedDate
                            End If
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtSecondEDate2.Text = LocalDate.ExposedDate
                            End If
                            txtSecond2.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                            OriginalSecondGroup2 = txtSecondGroup2.Text
                            OriginalSecondMember2 = txtSecondMember2.Text
                            OriginalSecondSDate2 = txtSecondSDate2.Text
                            OriginalSecondEDate2 = txtSecondEDate2.Text
                            OriginalSecond2 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        Else
                            SecondInsurerId2 = 0
                            OriginalSecondInsurerId2 = 0
                            OriginalSecondCopay2 = ""
                            OriginalSecondMember2 = ""
                            OriginalSecondGroup2 = ""
                            OriginalSecondSDate2 = ""
                            OriginalSecondEDate2 = ""
                            OriginalSecond2 = ""
                        End If
                    End If
                End If
            Else
                OriginalSecondInsurerId2 = 0
                OriginalSecondMember2 = ""
                OriginalSecondGroup2 = ""
                OriginalSecondSDate2 = ""
                OriginalSecondEDate2 = ""
                OriginalSecondCopay2 = Copay
                OriginalSecond2 = ""
            End If
            If (SecondThirdCurrent <> 0) Then
                chkDelete6.Visible = True
                RetrievePatientFinancial.PatientId = PolicyHolderid
                RetrievePatientFinancial.FinancialId = SecondThirdCurrent
                RetrievePatientFinancial.PrimaryInsurerId = 0
                RetrievePatientFinancial.PrimaryPIndicator = 0
                RetrievePatientFinancial.PrimaryStartDate = ""
                RetrievePatientFinancial.PrimaryEndDate = ""
                RetrievePatientFinancial.FinancialStatus = ""
                If (RetrievePatientFinancial.RetrievePatientFinance) Then
                    If (RetrievePatientFinancial.PrimaryInsurerId > 0) Then
                        RetrieveInsurer.InsurerId = RetrievePatientFinancial.PrimaryInsurerId
                        If (RetrieveInsurer.RetrieveInsurer) Then
                            SecondInsurerId3 = RetrievePatientFinancial.PrimaryInsurerId
                            OriginalSecondInsurerId3 = SecondInsurerId3
                            lblSecondInsurer3.Caption = Trim(RetrieveInsurer.InsurerName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerPlanName) + " " _
                                                      + Trim(RetrieveInsurer.InsurerGroupName)
                            If (RetrievePatientFinancial.FinancialCopay > 0) Then
                                Call DisplayDollarAmount(Trim(Str(RetrievePatientFinancial.FinancialCopay)), Copay)
                            Else
                                Call DisplayDollarAmount(Trim(Str(RetrieveInsurer.Copay)), Copay)
                            End If
                            txtCopayS3.Text = Trim(Copay)
                            OriginalSecondCopay3 = Trim(Copay)
                            txtSecondMember3.Text = RetrievePatientFinancial.PrimaryPerson
                            txtSecondGroup3.Text = RetrievePatientFinancial.PrimaryGroup
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryStartDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtSecondSDate3.Text = LocalDate.ExposedDate
                            End If
                            LocalDate.ExposedDate = RetrievePatientFinancial.PrimaryEndDate
                            If (LocalDate.ConvertManagedDateToDisplayDate) Then
                                txtSecondEDate3.Text = LocalDate.ExposedDate
                            End If
                            txtSecond3.Text = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                            OriginalSecondMember3 = txtSecondMember3.Text
                            OriginalSecondGroup3 = txtSecondGroup3.Text
                            OriginalSecondSDate3 = txtSecondSDate3.Text
                            OriginalSecondEDate3 = txtSecondEDate3.Text
                            OriginalSecond3 = Trim(Str(RetrievePatientFinancial.PrimaryPIndicator))
                        Else
                            SecondInsurerId3 = 0
                            OriginalSecondInsurerId3 = 0
                            OriginalSecondCopay3 = ""
                            OriginalSecondMember3 = ""
                            OriginalSecondGroup3 = ""
                            OriginalSecondSDate3 = ""
                            OriginalSecondEDate3 = ""
                            OriginalSecond3 = ""
                        End If
                    End If
                End If
            Else
                OriginalSecondInsurerId3 = 0
                OriginalSecondMember3 = ""
                OriginalSecondGroup3 = ""
                OriginalSecondSDate3 = ""
                OriginalSecondEDate3 = ""
                OriginalSecondCopay3 = Copay
                OriginalSecond3 = ""
            End If
        End If
    Else
        OriginalSecondInsurerId1 = 0
        OriginalSecondMember1 = ""
        OriginalSecondGroup1 = ""
        OriginalSecondSDate1 = ""
        OriginalSecondEDate1 = ""
        OriginalSecondCopay1 = ""
        OriginalSecond1 = ""
        OriginalSecondInsurerId2 = 0
        OriginalSecondMember2 = ""
        OriginalSecondGroup2 = ""
        OriginalSecondSDate2 = ""
        OriginalSecondEDate2 = ""
        OriginalSecondCopay2 = ""
        OriginalSecond2 = ""
        OriginalSecondInsurerId3 = 0
        OriginalSecondMember3 = ""
        OriginalSecondGroup3 = ""
        OriginalSecondSDate3 = ""
        OriginalSecondEDate3 = ""
        OriginalSecondCopay3 = ""
        OriginalSecond3 = ""
    End If
End If
Set LocalDate = Nothing
Set RetrieveInsurer = New Insurer
Set RetrieveAnotherPatient = New Patient
Set RetrievePatientFinancial = New PatientFinance
If (PolicyHolderid <> PatientId) And (PolicyHolderid > 0) Then
    txtSecond1.Locked = True
    txtSecond2.Locked = True
    txtSecond3.Locked = True
    txtCopayS1.Locked = True
    txtCopayS2.Locked = True
    txtCopayS3.Locked = True
    txtSecondMember1.Locked = True
    txtSecondMember2.Locked = True
    txtSecondMember3.Locked = True
    txtSecondGroup1.Locked = True
    txtSecondGroup2.Locked = True
    txtSecondGroup3.Locked = True
    txtSecondSDate1.Locked = True
    txtSecondSDate2.Locked = True
    txtSecondSDate3.Locked = True
    txtSecondEDate1.Locked = True
    txtSecondEDate2.Locked = True
    txtSecondEDate3.Locked = True
    chkDelete4.Enabled = False
    chkDelete5.Enabled = False
    chkDelete6.Enabled = False
Else
    txtSecond1.Locked = False
    txtSecond2.Locked = False
    txtSecond3.Locked = False
    txtCopayS1.Locked = False
    txtCopayS2.Locked = False
    txtCopayS3.Locked = False
    txtSecondMember1.Locked = False
    txtSecondMember2.Locked = False
    txtSecondMember3.Locked = False
    txtSecondGroup1.Locked = False
    txtSecondGroup2.Locked = False
    txtSecondGroup3.Locked = False
    txtSecondSDate1.Locked = False
    txtSecondSDate2.Locked = False
    txtSecondSDate3.Locked = False
    txtSecondEDate1.Locked = False
    txtSecondEDate2.Locked = False
    txtSecondEDate3.Locked = False
    chkDelete4.Enabled = True
    chkDelete5.Enabled = True
    chkDelete6.Enabled = True
End If
fraAllocate.Visible = False
lblAllocate.Visible = False
lblOSDate.Visible = False
txtOSDate.Visible = False
txtOSDate.Text = ""
lblOEDate.Visible = False
txtOEDate.Visible = False
txtOEDate.Text = ""
cmdQuit.Visible = False
cmdFDone.Visible = False
End Function

Private Function IsPolicyHolderActive(PatId As Long) As Boolean
Dim RetFin As PatientFinance
Set RetFin = New PatientFinance
RetFin.PatientId = PatId
IsPolicyHolderActive = RetFin.RetrievePatientFinancialPrimary
Set RetFin = Nothing
End Function

Private Function PostDependentInfo(PatId As Long, PatPolId As Long, InsId1 As Long, PolId1 As String, InsId2 As Long, PolId2 As String, InsId3 As Long, PolId3 As String) As Boolean
Dim RetDep As PatientFinanceDependents
Dim RetDep1 As PatientFinanceDependents
PostDependentInfo = False
If (PatId > 0) And (PatPolId > 0) And (PatId <> PatPolId) Then
    If (InsId1 > 0) Then
        Set RetDep = New PatientFinanceDependents
        RetDep.DependentPatientId = PatId
        RetDep.DependentPolicyHolderId = PatPolId
        RetDep.DependentInsurerId = InsId1
        If (RetDep.FindPatientFinancialDependents < 1) Then
            Set RetDep1 = New PatientFinanceDependents
            RetDep1.DependentId = 0
            Call RetDep1.RetrievePatientFinancialDependents
            RetDep1.DependentPatientId = PatId
            RetDep1.DependentPolicyHolderId = PatPolId
            RetDep1.DependentInsurerId = InsId1
            RetDep1.DependentMember = PolId1
            Call RetDep1.ApplyPatientFinancialDependents
            Set RetDep1 = Nothing
        End If
        Set RetDep = Nothing
    End If
    If (InsId2 > 0) Then
        Set RetDep = New PatientFinanceDependents
        RetDep.DependentPatientId = PatId
        RetDep.DependentPolicyHolderId = PatPolId
        RetDep.DependentInsurerId = InsId2
        If (RetDep.FindPatientFinancialDependents < 1) Then
            Set RetDep1 = New PatientFinanceDependents
            RetDep1.DependentId = 0
            Call RetDep1.RetrievePatientFinancialDependents
            RetDep1.DependentPatientId = PatId
            RetDep1.DependentPolicyHolderId = PatPolId
            RetDep1.DependentInsurerId = InsId2
            RetDep1.DependentMember = PolId2
            Call RetDep1.ApplyPatientFinancialDependents
            Set RetDep1 = Nothing
        End If
        Set RetDep = Nothing
    End If
    If (InsId3 > 0) Then
        Set RetDep = New PatientFinanceDependents
        RetDep.DependentPatientId = PatId
        RetDep.DependentPolicyHolderId = PatPolId
        RetDep.DependentInsurerId = InsId3
        If (RetDep.FindPatientFinancialDependents < 1) Then
            Set RetDep1 = New PatientFinanceDependents
            RetDep1.DependentId = 0
            Call RetDep1.RetrievePatientFinancialDependents
            RetDep1.DependentPatientId = PatId
            RetDep1.DependentPolicyHolderId = PatPolId
            RetDep1.DependentInsurerId = InsId3
            RetDep1.DependentMember = PolId3
            Call RetDep1.ApplyPatientFinancialDependents
            Set RetDep1 = Nothing
        End If
        Set RetDep = Nothing
    End If
End If
End Function

Private Function FormatMinLength(AFormat As String) As Integer
Dim i As Integer
FormatMinLength = 0
AFormat = Trim(AFormat)
For i = 1 To Len(AFormat)
    If (Mid(AFormat, i, 1) <> "O") Then
        FormatMinLength = FormatMinLength + 1
    End If
Next i
End Function

Private Function VerifyEndDates(PatId As Long) As Boolean
Dim i As Integer
Dim j As Integer
Dim PInd As Integer
Dim EDate As String
Dim RetFin1 As PatientFinance
Dim RetFin2 As PatientFinance
VerifyEndDates = True
' the test is archived policies (end dates)
' of the same level that conflict.
If (PatId > 0) Then
    Set RetFin1 = New PatientFinance
    RetFin1.PatientId = PatId
    RetFin1.FinancialStatus = "X"
    If (RetFin1.FindPatientFinancial > 0) Then
        i = 1
        Do Until Not (RetFin1.SelectPatientFinancial(i))
            EDate = Trim(RetFin1.PrimaryEndDate)
            PInd = RetFin1.PrimaryPIndicator
            If (EDate <> "") Then
                Set RetFin2 = New PatientFinance
                RetFin2.PatientId = PatId
                RetFin2.FinancialStatus = "C"
                RetFin2.PrimaryPIndicator = PInd
                If (RetFin2.FindPatientFinancial > 0) Then
                    j = 1
                    Do Until Not (RetFin2.SelectPatientFinancial(j))
                        If (EDate >= RetFin2.PrimaryStartDate) And (RetFin1.FinancialStatus = "C") Then
                            VerifyEndDates = False
                            Exit Do
                        End If
                        j = j + 1
                    Loop
                End If
                Set RetFin2 = Nothing
            End If
            If Not (VerifyEndDates) Then
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetFin1 = Nothing
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub


