VERSION 5.00
Begin VB.Form frmExistingMachines 
   Caption         =   "Current Machines and Settings"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8595
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   8595
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Exit"
      Height          =   735
      Left            =   6000
      TabIndex        =   6
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton cmdChange 
      Caption         =   "Change Current Settings"
      Height          =   735
      Left            =   4560
      TabIndex        =   4
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete Machine"
      Height          =   735
      Left            =   3120
      TabIndex        =   3
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add New Machine"
      Height          =   735
      Left            =   1680
      TabIndex        =   2
      Top             =   2280
      Width           =   975
   End
   Begin VB.ListBox lboDevices 
      Height          =   1035
      Left            =   330
      TabIndex        =   0
      Top             =   1080
      Width           =   7935
   End
   Begin VB.Label lblColHeadings 
      Height          =   255
      Left            =   330
      TabIndex        =   5
      Top             =   840
      Width           =   7935
   End
   Begin VB.Label lblInfo 
      Caption         =   "The machines currently set up and their settings are:"
      Height          =   255
      Left            =   2370
      TabIndex        =   1
      Top             =   360
      Width           =   3855
   End
End
Attribute VB_Name = "frmExistingMachines"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private myPort        As String
Private mySettings    As String
Private myDevice      As String
Private deviceCount   As String


Public Sub CheckDevices()

'    Checks the registry for the number of connected devices then calls routine to populate
'    the list box with the devices and their settings

    If (mdlRegSettings.QueryHKLMValue("DeviceCount", deviceCount)) Then
        PopulateDeviceChoices Val(deviceCount)
    End If
End Sub


Private Sub cmdAdd_Click()

    strAction = "Add"
    cmdAdd.Tag = Val(deviceCount) + 1
    frmMachines.Show vbModal
End Sub


Private Sub cmdCancel_Click()

    Unload Me
End Sub


Private Sub cmdChange_Click()

Dim i             As Long
Dim j             As Long
Dim k             As Long
Dim strSettings   As String
Dim strDeviceNo   As String
Dim aTabs(0 To 6) As Long
Dim dev           As Device
    i = Me.lboDevices.ListIndex
    If i = -1 Then
        MsgBox prompt:="Please select a device", buttons:=vbOKOnly + vbInformation, title:="No device selected"
    Else
        strSettings = Me.lboDevices.List(i)
        Erase aTabs
        aTabs(0) = InStr(1, strSettings, vbTab, vbTextCompare)
        For k = 1 To 6
            aTabs(k) = InStr(aTabs(k - 1) + 1, strSettings, vbTab, vbTextCompare)
        Next k
        j = InStr(1, strSettings, " ", vbTextCompare)
        strDeviceNo = Trim$(Mid$(strSettings, j - 1, aTabs(0) - j - 1))
        With frmMachines
            .cboMachine.Text = Trim$(Mid$(strSettings, aTabs(0) + 1, aTabs(1) - aTabs(0) - 1))
            For Each dev In connectedDevices
                If dev.DeviceName = .cboMachine.Text Then
                    .lblType = dev.DeviceType
                    .lblMaker = dev.Brand
                    Exit For
                End If
            Next dev
            .cboMachine.Locked = True
            .cboBaud.Text = Trim$(Mid$(strSettings, aTabs(2) + 1, aTabs(3) - aTabs(2) - 1))
            .cboParity.Text = Trim$(Mid$(strSettings, aTabs(3) + 1, aTabs(4) - aTabs(3) - 1))
            .cboData.Text = Trim$(Mid$(strSettings, aTabs(4) + 1, aTabs(5) - aTabs(4) - 1))
            .cboStop.Text = Trim$(Mid$(strSettings, aTabs(5) + 1, aTabs(6) - aTabs(5) - 1))
            .cboPort.Text = Trim$(Mid$(strSettings, aTabs(6) + 1))
            .lblMaker.Visible = True
            .lblType.Visible = True
        End With
        strAction = "Change"
        cmdChange.Tag = strDeviceNo
        frmMachines.Show vbModal
        If Not connectedDevice Is Nothing Then
            With connectedDevice
                strSettings = Pad(.DeviceID, 15) & vbTab & Pad(.DeviceName, 15) & vbTab & Pad(.DeviceType, 20) & vbTab & Pad(.Baud, 10) & vbTab & Pad(Left$(.Parity, 1), 5) & vbTab & Pad(.DataBits, 5) & vbTab & Pad(.StopBits, 5) & vbTab & .Port
            End With
        End If
        Me.lboDevices.List(i) = strSettings
    End If
End Sub


Private Sub cmdDelete_Click()

Dim i           As Long
Dim idx         As Long
Dim strSettings As String
Dim strDeviceNo As String
Dim strDevices  As String
    i = Me.lboDevices.ListIndex
    If i = -1 Then
        MsgBox prompt:="Please select a device", buttons:=vbOKOnly + vbInformation, title:="No device selected"
    Else
        strSettings = Me.lboDevices.List(i)
        strDeviceNo = Mid$(Trim$(Left$(strSettings, InStr(1, strSettings, vbTab, vbTextCompare) - 1)), 11)
        With lboDevices
            .RemoveItem i
            connectedDevices.DeleteDevice Val(strDeviceNo)
            For idx = i To .ListCount - 1
                .List(idx) = Pad(connectedDevices(idx + 1).DeviceID, 15) & Mid$(.List(idx), 16)
            Next idx
        End With
        strDevices = CStr(connectedDevices.Count)
        SetKeyValue RegPath, "DeviceCount", strDevices, REG_SZ
        For idx = i + 1 To strDevices
            With connectedDevices(idx)
                SetKeyValue RegPath, .DeviceID, .DeviceName, REG_SZ
                SetKeyValue RegPath, "ComSettings" & CStr(idx), .Baud & "," & Left$(.Parity, 1) & "," & .DataBits & "," & .StopBits, REG_SZ
                SetKeyValue RegPath, "ComPort" & CStr(idx), .Port, REG_SZ
            End With
        Next idx
        DeleteKeyValue RegPath, "DeviceName" & CStr(strDevices + 1)
        DeleteKeyValue RegPath, "ComSettings" & CStr(strDevices + 1)
        DeleteKeyValue RegPath, "ComPort" & CStr(strDevices + 1)
        Call InsertLog("FrmExistingMachines.frm", "Deleted Machine Config", LoginCatg.MachineConfig, "", "", 0, 0)
    End If
End Sub


Private Sub Form_Load()

    Set connectedDevices = New Devices
    Set connectedDevice = New Device
    CheckDevices
    Me.lblColHeadings.Caption = " Device" & Space$(20) & "Model" & Space$(22) & "Type" & Space$(24) & "Baud" & Space$(5) & "Parity" & Space$(4) & "DataBits" & Space$(3) & "StopBits" & Space$(6) & "Port"
End Sub


Private Sub Form_Unload(Cancel As Integer)

    RemoveReferences
    Unload frmMachines
End Sub


Private Sub PopulateDeviceChoices(ByVal deviceCount As Long)

Dim strCom            As String
Dim strDeviceSettings As String
Dim strBaud           As String
Dim strParity         As String
Dim strDBits          As String
Dim strSBits          As String
Dim strDevices        As String
Dim strDeviceName     As String
Dim deviceCounter     As Long
Dim i                 As Long
Dim j                 As Long
Dim k                 As Long
Dim dev               As Device
    On Error Resume Next
'    Query registry for devices & their settings.  Parse COM settings to extract baud rate,
'    parity, data bits & stop bits.  Add each device in registry to connectedDevices collection.
    lboDevices.Clear
    Load frmMachines
    For deviceCounter = 0 To Val(deviceCount) - 1
        strDevices = Trim$(Str$(deviceCounter + 1))
        If (mdlRegSettings.QueryHKLMValue("ComPort" & strDevices, myPort) And mdlRegSettings.QueryHKLMValue("ComSettings" & strDevices, mySettings) And mdlRegSettings.QueryHKLMValue("DeviceName" & strDevices, myDevice)) Then
            i = InStr(1, mySettings, ",", vbTextCompare)
            j = InStr(i + 1, mySettings, ",", vbTextCompare)
            k = InStr(j + 1, mySettings, ",", vbTextCompare)
            strBaud = Trim$(Left$(mySettings, i - 1))
            strParity = Trim$(Mid$(mySettings, i + 1, j - i - 1))
            strDBits = Trim$(Mid$(mySettings, j + 1, k - j - 1))
            strSBits = Trim$(Mid$(mySettings, k + 1))
            strDeviceName = "DeviceName" & CStr(connectedDevices.Count + 1)
            Set connectedDevice = connectedDevices.Add(strDeviceName, myDevice, strBaud, strParity, strDBits, strSBits, myPort)
            With connectedDevice
                strCom = .Baud & "," & Left$(.Parity, 1) & "," & .DataBits & "," & .StopBits
'                SetKeyValue RegPath, .DeviceID, .DeviceName, REG_SZ
'                SetKeyValue RegPath, "ComSettings" & CStr(connectedDevices.Count), strCom, REG_SZ
'                SetKeyValue RegPath, "ComPort" & CStr(connectedDevices.Count), .Port, REG_SZ
                For Each dev In registeredDevices
                    If dev.DeviceAlias = myDevice Then
                        .DeviceAlias = myDevice
                        .DeviceType = dev.DeviceType
                        .Brand = dev.Brand
                        Exit For
'                    Debug.Print connectedDevice.DeviceName, connectedDevice.DeviceType
                    End If
                Next dev
'                concatenate & space device name, settings, & port and display in listbox
                strDeviceSettings = Pad(.DeviceID, 15) & vbTab & Pad(.DeviceName, 15) & vbTab & Pad(.DeviceType, 20) & vbTab & Pad(.Baud, 10) & vbTab & Pad(.Parity, 5) & vbTab & Pad(.DataBits, 5) & vbTab & Pad(.StopBits, 5) & vbTab & .Port
            End With
            Me.lboDevices.AddItem strDeviceSettings
'            Debug.Print strDeviceSettings
        End If
    Next deviceCounter
    On Error GoTo 0
End Sub


