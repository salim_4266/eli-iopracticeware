VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Devices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private mcolDevices   As New Collection


Public Function Add(ByVal DeviceID As String, _
                    ByVal strDeviceName As String, _
                    ByVal Baud As String, _
                    ByVal Parity As String, _
                    ByVal DataBits As String, _
                    ByVal StopBits As String, _
                    ByVal strPort As String, _
                    Optional ByVal DeviceType As String, _
                    Optional ByVal DeviceAlias As String, _
                    Optional ByVal Brand As String)
                    
Dim deviceNew As New Device

    With deviceNew
        .DeviceID = DeviceID
        .DeviceName = strDeviceName
        .DeviceAlias = DeviceAlias
        .Baud = Baud
        .Parity = Parity
        .DataBits = DataBits
        .StopBits = StopBits
        .Port = strPort
        .DeviceType = DeviceType
        .Brand = Brand
    End With
    mcolDevices.Add deviceNew
    Set Add = deviceNew
End Function


Private Sub Class_Terminate()

    Set mcolDevices = Nothing
End Sub


Public Function Count() As Long

    Count = mcolDevices.Count
End Function


Public Sub DeleteDevice(ByVal varIndex As Variant)

Dim i As Long

    mcolDevices.Remove varIndex
    For i = varIndex To mcolDevices.Count
        mcolDevices(i).DeviceID = "DeviceName" & CStr(i)
    Next i
End Sub


Public Function item(ByVal varIndex As Variant) As Device
Attribute item.VB_UserMemId = 0

    Set item = mcolDevices.item(varIndex)
End Function


Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    Set NewEnum = mcolDevices.[_NewEnum]
End Function
