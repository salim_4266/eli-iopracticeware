VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmHome 
   BackColor       =   &H0077742D&
   Caption         =   "Admin Utilities"
   ClientHeight    =   8040
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   Icon            =   "HomeTools.frx":0000
   LinkTopic       =   "AdminUtilities"
   ScaleHeight     =   8040
   ScaleWidth      =   12000
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   6000
      Left            =   3840
      Top             =   360
   End
   Begin VB.ListBox lstPayment 
      Height          =   450
      Left            =   3120
      TabIndex        =   18
      Top             =   360
      Visible         =   0   'False
      Width           =   615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMed 
      Height          =   975
      Left            =   2040
      TabIndex        =   17
      Top             =   4440
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":0442
   End
   Begin VB.TextBox txtToDr 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   5
      Top             =   2760
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.TextBox txtFromDr 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   4
      Top             =   2280
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.TextBox txtClosingDate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      MaxLength       =   10
      TabIndex        =   3
      Top             =   3840
      Width           =   1695
   End
   Begin VB.TextBox txtFromIns 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   2
      Top             =   1200
      Visible         =   0   'False
      Width           =   6855
   End
   Begin VB.TextBox txtToIns 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   1
      Top             =   1680
      Visible         =   0   'False
      Width           =   6855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9840
      TabIndex        =   0
      Top             =   6600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":062F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMergeDr 
      Height          =   975
      Left            =   240
      TabIndex        =   6
      Top             =   2280
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":080E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMergePlans 
      Height          =   975
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":09F6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMachines 
      Height          =   975
      Left            =   240
      TabIndex        =   8
      Top             =   4440
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":0BDC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCloseDate 
      Height          =   975
      Left            =   240
      TabIndex        =   20
      Top             =   3360
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":0DC5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   975
      Left            =   240
      TabIndex        =   21
      Top             =   5520
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "HomeTools.frx":0FAE
   End
   Begin VB.Label lblNow 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Date/Time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   240
      TabIndex        =   19
      Top             =   7680
      Width           =   1080
   End
   Begin VB.Label lblFromDr 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor to Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      TabIndex        =   16
      Top             =   2280
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label lblToDr 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Doctor To Keep"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      TabIndex        =   15
      Top             =   2760
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Tools"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4560
      TabIndex        =   14
      Top             =   120
      Width           =   2775
   End
   Begin VB.Label lblClosingDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Closing Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      TabIndex        =   13
      Top             =   3840
      Width           =   1935
   End
   Begin VB.Label lblLastDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Closing Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      TabIndex        =   12
      Top             =   3360
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblTotal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Closing Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   5160
      TabIndex        =   11
      Top             =   600
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.Label lblToIns 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan to Keep"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      TabIndex        =   10
      Top             =   1680
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label lblFromIns 
      Appearance      =   0  'Flat
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Plan to Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2040
      TabIndex        =   9
      Top             =   1200
      Visible         =   0   'False
      Width           =   1935
   End
End
Attribute VB_Name = "frmHome"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private ToDoctor As Long
Private FromDoctor As Long
Private ToIns As Long
Private FromIns As Long

Sub ButtonHandler(ByVal strButton As String)
    Dim Tools As ToolRoutines
    Dim ComTemp As CommonTemplates
    Dim strHeaderStart As String
    Const strHeaderEnd As String = ":  Are you sure ?"
    
    If Not Authenticate Then Exit Sub

    Select Case strButton
        Case "cmdInsDes", "cmdMergeDr", "cmdMergePlans"
        Case "cmdMed"
            frmFeeUtility.Show vbModal
            Exit Sub
        Case "cmdMachines"
            frmExistingMachines.Show vbModal
            Exit Sub
        Case Else
            lblFromDr.Visible = False
            txtFromDr.Visible = False
            lblToDr.Visible = False
            txtToDr.Visible = False
            lblFromIns.Visible = False
            txtFromIns.Visible = False
            lblToIns.Visible = False
            txtToIns.Visible = False
    End Select
    
    With frmEventMsgs
        .AcceptText = ""
        .RejectText = "Yes"
        .CancelText = "No"
        .Other0Text = ""
        .Other1Text = ""
        .Other2Text = ""
        .Other3Text = ""
        .Other4Text = ""
    
        Select Case strButton
            Case "cmdCLLoad"
                strHeaderStart = "CL Inventory"
            Case "cmdCosts"
                strHeaderStart = "Reset Fees"
            Case "cmdDeny"
                strHeaderStart = "Close Denials"
            Case "cmdInsDes"
                strHeaderStart = "Set Rcv Ins Des"
            Case "cmdLBD"
                strHeaderStart = "Reset Bill Date"
            Case "cmdMergeDr"
                strHeaderStart = "Ref Dr Merge"
            Case "cmdMergePlans"
                strHeaderStart = "Plan Merge"
            Case "cmdPay"
                strHeaderStart = "Reset Pay Date"
            Case "cmdPlant"
                strHeaderStart = "Plant Dxs"
            Case "cmdReset"
                strHeaderStart = "Reset Balances"
            Case "cmdScanFix"
                strHeaderStart = "Scan Fix"
            Case "cmdVC"
                strHeaderStart = "Verify Check Out"
        End Select
        
        .Header = strHeaderStart & strHeaderEnd
        .Show 1
    End With
        
    If (frmEventMsgs.result = 2) Then
        Set Tools = New ToolRoutines
        Set ComTemp = New CommonTemplates
        lblTotal.Visible = True
        With Tools
            Call InsertLog("FrmHome.frm", strHeaderStart & " Tool Used", LoginCatg.Tools, "", "", 0, 0)
            Select Case strButton
                Case "cmdMergeDr"                               'Only call to Function"
                    .ApplMergeDoctors FromDoctor, ToDoctor, lblTotal
                    lblTotal.Visible = False
                    txtFromDr.Text = ""
                    txtToDr.Text = ""
                    Set ComTemp = Nothing
                    Exit Sub
                Case "cmdMergePlans"                            'Only call to Function
                    .ApplMergePlans FromIns, ToIns, lblTotal
                    lblTotal.Visible = False
                    txtFromIns.Text = ""
                    txtToIns.Text = ""
                    FromIns = 0
                    ToIns = 0
                    Set ComTemp = Nothing
                    Exit Sub
            End Select
        End With
        Set ComTemp = Nothing
    End If
    lblFromDr.Visible = True
    txtFromDr.Visible = True
    lblToDr.Visible = True
    txtToDr.Visible = True
    lblFromIns.Visible = True
    txtFromIns.Visible = True
    lblToIns.Visible = True
    txtToIns.Visible = True

End Sub

Private Sub cmdCLLoad_Click()
    ButtonHandler ActiveControl.name
End Sub

Private Sub cmdCloseDate_Click()
    Dim Comlist As CommonAIList
    If (Trim$(txtClosingDate.Text) <> "") Then
        Set Comlist = New CommonAIList
        Call Comlist.ApplSetClosingMonth(Trim$(txtClosingDate.Text))
        Set Comlist = Nothing
        LoadConfigCollection
    End If
End Sub

Private Sub cmdMore_Click()
   
End Sub


Private Sub cmdDone_Click()
    'Unload frmHome
    If UserLogin.IsLoggedIn Then
        UserLogin.LogoffUser
    End If
    Call dbPinpointClose
    End
End Sub

Private Sub cmdMachines_Click()
    ButtonHandler ActiveControl.name
End Sub

Private Sub cmdMed_Click()
    ButtonHandler ActiveControl.name
End Sub

Private Sub cmdMergeDr_Click()
    ButtonHandler ActiveControl.name
End Sub

Private Sub cmdMergePlans_Click()
    ButtonHandler ActiveControl.name
End Sub

Private Sub Form_Load()
Dim oDate As New CIODateTime
Dim ComTbl As CommonTables
    
frmSplash.Whom = "B"
frmSplash.Show
DoEvents

EstablishDirectory
dbPinpointOpen

Set UserLogin = New CUserLogin
If (Authenticate) Then
    GetSoftwareLocation
    Call InitializeComWrapper
    LoadConfigCollection
    UserLogin.SetTaskMonitor
    SetGlobalDefaults
    
    Timer1_Timer

    Set ComTbl = New CommonTables
    Set ComTbl.lstBox = lstPayment
    ComTbl.ApplLoadCodes "AccessAds", True, True, True
    ComTbl.ApplGetPractice dbPracticeId, dbSoftwareLocation
    Set ComTbl = Nothing
    If dbPracticeId > 0 Then
        dbPracticeId = dbPracticeId + 1000
    End If
    FromDoctor = 0
    ToDoctor = 0
    
    oDate.SetDateUnknown CheckConfigCollection("EDITWINDOW")
    txtClosingDate.Text = ""
    lblLastDate.Caption = "Last Date: " & oDate.GetDisplayDate(False)
    lblLastDate.Visible = True
    lblTotal.Visible = False
    lblToDr.Visible = True
    txtToDr.Visible = True
    txtToDr.Text = ""
    lblFromDr.Visible = True
    txtFromDr.Visible = True
    txtFromDr.Text = ""
    lblToIns.Visible = True
    txtToIns.Visible = True
    txtToIns.Text = ""
    lblFromIns.Visible = True
    txtFromIns.Visible = True
    txtFromIns.Text = ""
    cmdMergePlans.Visible = True
    cmdMergeDr.Visible = True
    cmdMed.Visible = True
    cmdMachines.Visible = True
        
    Unload frmSplash
    DoEvents
    If DoMonthEnd() Then
        End
    End If
End If
Set oDate = Nothing
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim comWrapper As New comWrapper
    comWrapper.Uninitialize
End Sub

Private Sub txtFromDr_Click()
    FromDoctor = HandleTxt("PCP", "Doctors", txtFromDr)
End Sub

Private Sub txtFromDr_KeyPress(KeyAscii As Integer)
    Call txtFromDr_Click
    KeyAscii = 0
End Sub

Sub NullKey(ByRef KeyAscii As Integer)
    If KeyAscii = 13 Or KeyAscii = 10 Then
        If Not IsNumeric(Chr(KeyAscii)) Then
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtToDr_Click()
    ToDoctor = HandleTxt("PCP", "Doctors", txtToDr)
End Sub

Private Sub txtToDr_KeyPress(KeyAscii As Integer)
    Call txtToDr_Click
    KeyAscii = 0
End Sub

Private Sub txtFromIns_Click()
    Dim TheText As String
    Dim ReturnArguments As Object
    Dim RetPatientFinancialService As PatientFinancialService
    Dim RS As Recordset
    Set ReturnArguments = DisplayInsurancePlanSearchScreen
    If Not ReturnArguments Is Nothing Then
        If (ReturnArguments.InsurerId > 0) Then
            Set RetPatientFinancialService = New PatientFinancialService
            RetPatientFinancialService.InsurerId = ReturnArguments.InsurerId
            Set RS = RetPatientFinancialService.RetrieveInsurer
            If Not RS Is Nothing And RS.RecordCount > 0 Then
                txtFromIns.Text = RS("Name")
                FromIns = ReturnArguments.InsurerId
            End If
            Set RetPatientFinancialService = Nothing
        End If
    End If
End Sub

Private Sub txtFromIns_KeyPress(KeyAscii As Integer)
    Call txtFromIns_Click
    KeyAscii = 0
End Sub

Private Sub txtToIns_Click()
    Dim TheText As String
    Dim ReturnArguments As Object
    Dim RetPatientFinancialService As PatientFinancialService
    Dim RS As Recordset
    Set ReturnArguments = DisplayInsurancePlanSearchScreen
    If Not ReturnArguments Is Nothing Then
        If (ReturnArguments.InsurerId > 0) Then
            Set RetPatientFinancialService = New PatientFinancialService
            RetPatientFinancialService.InsurerId = ReturnArguments.InsurerId
            Set RS = RetPatientFinancialService.RetrieveInsurer
            If Not RS Is Nothing And RS.RecordCount > 0 Then
                txtToIns.Text = RS("Name")
                ToIns = ReturnArguments.InsurerId
            End If
            Set RetPatientFinancialService = Nothing
        End If
    End If
End Sub

Function HandleTxt(ByVal strText1 As String, ByVal strText2 As String, ByRef txt As TextBox) As Long
    With frmSelectDialogue
        .InsurerSelected = ""
        Call .BuildSelectionDialogue(strText1)
        .Show 1
        If (.SelectionResult) Then
            If (.Selection <> "Create " & strText2) Then
                txt.Text = UCase$(.Selection)
                HandleTxt = Val(Trim$(Mid$(txt.Text, 57, Len(txt.Text) - 56)))
            End If
        End If
    End With
End Function

Private Sub txtToIns_KeyPress(KeyAscii As Integer)
    Call txtToIns_Click
    KeyAscii = 0
End Sub

Private Sub txtClosingDate_KeyPress(KeyAscii As Integer)
    If Not Authenticate Then Exit Sub
    If ((KeyAscii = 13) Or (Chr(KeyAscii) = vbTab)) Then
        If Trim$(txtClosingDate.Text) <> "" Then
            If Not (OkDate(txtClosingDate.Text)) Then
                With frmEventMsgs
                    .Header = "Dates are Entered MM/DD/YYYY"
                    .AcceptText = ""
                    .RejectText = "Ok"
                    .CancelText = ""
                    .Other0Text = ""
                    .Other1Text = ""
                    .Other2Text = ""
                    .Other3Text = ""
                    .Other4Text = ""
                    .Show 1
                End With
                txtClosingDate.Text = ""
                txtClosingDate.SetFocus
                SendKeys "{Home}"
                KeyAscii = 0
            Else
                Call UpdateDisplay(txtClosingDate, "D")
            End If
        End If
    End If
End Sub

Private Sub txtClosingDate_Validate(Cancel As Boolean)
    txtClosingDate_KeyPress 13
End Sub

Private Function Authenticate() As Boolean
    If LenB(UserLogin.sNameLong) Then
        Authenticate = True
    Else
        Call UserLogin.DisplayLoginScreen
        If Not Login And Not UserLogin.IsLoggedIn Then End
        If LenB(UserLogin.sNameLong) Then
            Authenticate = True
        End If
    End If
End Function

Private Sub Timer1_Timer()
Dim dtDisplay As CIODateTime
    Set dtDisplay = New CIODateTime
    dtDisplay.MyDateTime = Now
    lblNow.Caption = dtDisplay.GetDisplayDate(False) & " " & dtDisplay.GetDisplayTime & Space(6) & "V" & ToolsVersionId
    Set dtDisplay = Nothing
End Sub
