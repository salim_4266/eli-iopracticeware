VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Device"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private msDeviceID      As String
Private msDeviceName    As String
Private msDeviceAlias   As String
Private msDeviceType    As String
Private msBaud          As String
Private msParity        As String
Private msDataBits      As String
Private msStopBits      As String
Private msBrand         As String
Private msPort          As String


Public Property Get Baud() As String

    Baud = msBaud
End Property


Public Property Let Baud(ByVal newValue As String)

    msBaud = newValue
End Property


Public Property Get Brand() As String

    Brand = msBrand
End Property


Public Property Let Brand(ByVal newValue As String)

    msBrand = newValue
End Property


Public Property Get DataBits() As String

    DataBits = msDataBits
End Property


Public Property Let DataBits(ByVal newValue As String)

    msDataBits = newValue
End Property


Public Property Get DeviceAlias() As String

    DeviceAlias = msDeviceAlias
End Property


Public Property Let DeviceAlias(ByVal newValue As String)

    msDeviceAlias = newValue
End Property


Public Property Get DeviceID() As String

    DeviceID = msDeviceID
End Property


Public Property Let DeviceID(ByVal newValue As String)

    msDeviceID = newValue
End Property


Public Property Get DeviceName() As String

    DeviceName = msDeviceName
End Property


Public Property Let DeviceName(ByVal newValue As String)

    msDeviceName = newValue
End Property


Public Property Get DeviceType() As String

    DeviceType = msDeviceType
End Property


Public Property Let DeviceType(ByVal newValue As String)

    msDeviceType = newValue
End Property


Public Property Get Parity() As String

    Parity = msParity
End Property


Public Property Let Parity(ByVal newValue As String)

    msParity = newValue
End Property


Public Property Get Port() As String

    Port = msPort
End Property


Public Property Let Port(ByVal newValue As String)

    msPort = newValue
End Property


Public Property Get StopBits() As String

    StopBits = msStopBits
End Property


Public Property Let StopBits(ByVal newValue As String)

    msStopBits = newValue
End Property
