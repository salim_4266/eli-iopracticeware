VERSION 5.00
Begin VB.Form frmMachines 
   Caption         =   "Machine Setup"
   ClientHeight    =   4245
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   ScaleHeight     =   4245
   ScaleWidth      =   4710
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cboPort 
      Height          =   315
      ItemData        =   "frmMachines.frx":0000
      Left            =   2040
      List            =   "frmMachines.frx":001F
      TabIndex        =   14
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   615
      Left            =   2520
      TabIndex        =   13
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Write to Registry"
      Height          =   615
      Left            =   720
      TabIndex        =   12
      Top             =   3360
      Width           =   1455
   End
   Begin VB.ComboBox cboStop 
      Height          =   315
      ItemData        =   "frmMachines.frx":0059
      Left            =   2040
      List            =   "frmMachines.frx":0063
      TabIndex        =   8
      Top             =   2400
      Width           =   1215
   End
   Begin VB.ComboBox cboData 
      Height          =   315
      ItemData        =   "frmMachines.frx":006D
      Left            =   2040
      List            =   "frmMachines.frx":0077
      TabIndex        =   6
      Top             =   2040
      Width           =   1215
   End
   Begin VB.ComboBox cboParity 
      Height          =   315
      ItemData        =   "frmMachines.frx":0081
      Left            =   2040
      List            =   "frmMachines.frx":008E
      TabIndex        =   4
      Top             =   1680
      Width           =   1215
   End
   Begin VB.ComboBox cboMachine 
      Height          =   315
      Left            =   2040
      Sorted          =   -1  'True
      TabIndex        =   3
      Text            =   "Select a Machine"
      Top             =   720
      Width           =   2295
   End
   Begin VB.ComboBox cboBaud 
      Height          =   315
      ItemData        =   "frmMachines.frx":00A3
      Left            =   2040
      List            =   "frmMachines.frx":00B3
      TabIndex        =   2
      Top             =   1290
      Width           =   1215
   End
   Begin VB.Label lblPort 
      Caption         =   "Serial Port"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   2790
      Width           =   1455
   End
   Begin VB.Label lblType 
      Caption         =   "Label1"
      Height          =   255
      Left            =   2400
      TabIndex        =   11
      Top             =   240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblMaker 
      Height          =   255
      Left            =   360
      TabIndex        =   10
      Top             =   240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblStop 
      Caption         =   "Stop Bits"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   2430
      Width           =   1455
   End
   Begin VB.Label lblData 
      Caption         =   "Data Bits"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   2070
      Width           =   1455
   End
   Begin VB.Label lblParity 
      Caption         =   "Parity"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   1710
      Width           =   1455
   End
   Begin VB.Label lblBaud 
      Caption         =   "Baud rate"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label lblMachine 
      Caption         =   "Machine"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   780
      Width           =   1455
   End
End
Attribute VB_Name = "frmMachines"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Private RS               As ADODB.Recordset
Private strAlias         As String
Private Const WM_CLOSE   As Long = &H10
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hwnd As Long, _
                                                                        ByVal wMsg As Long, _
                                                                        ByVal wParam As Long, _
                                                                        ByVal lParam As Long) As Long


Private Sub cboMachine_Click()

Dim dev As Device

    If cboMachine.DataChanged Then
        For Each dev In registeredDevices
            If dev.DeviceName = cboMachine.Text Then
                strAlias = dev.DeviceAlias
                With Me
                    .lblMaker.Visible = True
                    .lblType.Visible = True
                    .lblMaker = dev.Brand
                    .lblType = dev.DeviceType
                    .cboBaud = dev.Baud
                    .cboParity = dev.Parity
                    .cboData = dev.DataBits
                    .cboStop = dev.StopBits
                End With
                Exit For
            End If
        Next dev
    End If
End Sub


Private Sub cmdCancel_Click()

    If Not connectedDevice Is Nothing Then
        Set connectedDevice = Nothing
    End If
' This code allows you to unload a form that is being loaded, otherwise an RTE is generated.
    PostMessage Me.hwnd, WM_CLOSE, 0, 0
End Sub


Private Sub cmdOk_Click()

Dim strDevice         As String
Dim strCom            As String
Dim strPort           As String
Dim strDevices        As String
Dim strDeviceName     As String
Dim strDeviceSettings As String
    Select Case strAction
    Case "Add"
'           NoOp
    Case "Change"
        strDevices = frmExistingMachines.cmdChange.Tag
    Case Else
        Stop
    End Select
    strPort = Right$(Me.cboPort.Text, 1)
    strDevice = Me.cboMachine
    strDeviceName = "DeviceName" & CStr(connectedDevices.Count + 1)
    Select Case strAction
    Case "Add"
        Set connectedDevice = connectedDevices.Add(strDeviceName, strDevice, cboBaud, cboParity, cboData, cboStop, strPort)
        strDevices = CStr(connectedDevices.Count)
        With connectedDevice
            .DeviceAlias = strAlias
            .DeviceType = Me.lblType
            .Brand = Me.lblMaker
            strCom = .Baud & "," & Left$(.Parity, 1) & "," & .DataBits & "," & .StopBits
            CreateNewKey RegPath, HKEY_LOCAL_MACHINE
            SetKeyValue RegPath, .DeviceID, .DeviceAlias, REG_SZ
            SetKeyValue RegPath, "ComSettings" & strDevices, strCom, REG_SZ
            SetKeyValue RegPath, "ComPort" & strDevices, .Port, REG_SZ
            SetKeyValue RegPath, "DeviceCount", strDevices, REG_SZ
            strDeviceSettings = Pad(.DeviceID, 15) & vbTab & Pad(.DeviceName, 15) & vbTab & Pad(.DeviceType, 20) & vbTab & Pad(.Baud, 10) & vbTab & Pad(Left$(.Parity, 1), 5) & vbTab & Pad(.DataBits, 5) & vbTab & Pad(.StopBits, 5) & vbTab & .Port
        End With
        frmExistingMachines.lboDevices.AddItem strDeviceSettings
        Call InsertLog("FrmMachine.frm", "Added New Machine Config", LoginCatg.MachineConfig, "", "", 0, 0)
    Case "Change"
        Set connectedDevice = connectedDevices(Val(frmExistingMachines.cmdChange.Tag))
        With connectedDevice
            .Baud = Me.cboBaud.Text
            .Parity = Me.cboParity.Text
            .DataBits = Me.cboData.Text
            .StopBits = Me.cboStop
            .Port = strPort
            strCom = .Baud & "," & Left$(Me.cboParity.Text, 1) & "," & Me.cboData.Text & "," & Me.cboStop.Text
            SetKeyValue RegPath, "ComSettings" & strDevices, strCom, REG_SZ
            SetKeyValue RegPath, "ComPort" & strDevices, strPort, REG_SZ
        End With
        cboMachine.Locked = False
    Case Else
        Stop
    End Select
    Me.Hide
End Sub


Private Sub Form_Load()

Dim strSQL    As String
Dim TheRec    As String
Dim FN1       As Long
    Set registeredDevices = New Devices
    Me.cboPort.ListIndex = 0
    
    strSQL = "SELECT Model, Make, Type, Baud, Parity, Databits, Stopbits, DeviceAlias FROM Devices"
    Set RS = GetRS(strSQL)
    On Error GoTo handler
    With RS
        .MoveFirst
        Do While Not .EOF
            Set registeredDevice = registeredDevices.Add("Dev" & registeredDevices.Count + 1, RS(0), KillNull(RS(3)), KillNull(RS(4)), KillNull(RS(5)), KillNull(RS(6)), "", KillNull(RS(2)), KillNull(RS(7)), KillNull(RS(1)))
            Me.cboMachine.AddItem RS(0)
            .MoveNext
        Loop
    End With
Exit Sub

handler:
    MsgBox prompt:="The Devices table does not exist in the database. Copy the table into your database from PracticeRepositoryModel", buttons:=vbOKOnly + vbInformation, title:="Missing Table"
    cmdCancel_Click
End Sub


Private Function KillNull(ByRef rsField As Variant) As Variant

    If Not IsNull(rsField) Then
        KillNull = rsField
    Else
        KillNull = vbNullString
    End If
End Function



