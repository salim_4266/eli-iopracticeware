VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmHomeAlt 
   BackColor       =   &H0077742D&
   Caption         =   "Administrative Utilities"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10275
   LinkTopic       =   "Form1"
   ScaleHeight     =   6945
   ScaleWidth      =   10275
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Exit"
      Height          =   495
      Left            =   5400
      TabIndex        =   28
      Top             =   6120
      Width           =   1335
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Go"
      Height          =   495
      Left            =   3600
      TabIndex        =   27
      Top             =   6120
      Width           =   1335
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5175
      Left            =   990
      TabIndex        =   0
      Top             =   600
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   9128
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BackColor       =   7828525
      TabCaption(0)   =   "Maintainence Utilities"
      TabPicture(0)   =   "frmHomeAlt.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Option1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Option1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Option1(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Option1(3)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Option1(4)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Option1(5)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Option1(6)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Option1(7)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Option1(8)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Option1(9)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Option1(10)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Option1(11)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Option1(12)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Option1(13)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Option1(14)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Option1(15)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Option1(16)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Option1(17)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Option1(18)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Option1(19)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Option1(20)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Option1(21)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Option1(22)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Option1(23)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).ControlCount=   24
      TabCaption(1)   =   "Set Up Utilities"
      TabPicture(1)   =   "frmHomeAlt.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Option1(25)"
      Tab(1).Control(1)=   "Option1(24)"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmHomeAlt.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.OptionButton Option1 
         Caption         =   "Set Up Devices"
         Height          =   255
         Index           =   25
         Left            =   -74640
         TabIndex        =   26
         Top             =   1200
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Import Clinical"
         Height          =   255
         Index           =   24
         Left            =   -74640
         TabIndex        =   25
         Top             =   720
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Merge Doctors"
         Height          =   255
         Index           =   23
         Left            =   5280
         TabIndex        =   24
         Top             =   3960
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Merge Plans"
         Height          =   255
         Index           =   22
         Left            =   5280
         TabIndex        =   23
         Top             =   3600
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Patient Referrals"
         Height          =   255
         Index           =   21
         Left            =   5280
         TabIndex        =   22
         Top             =   2280
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Appointment Referral"
         Height          =   255
         Index           =   20
         Left            =   5280
         TabIndex        =   21
         Top             =   1920
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Scan Fix"
         Height          =   255
         Index           =   19
         Left            =   5280
         TabIndex        =   20
         Top             =   1560
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "CL Inventory"
         Height          =   255
         Index           =   18
         Left            =   5280
         TabIndex        =   19
         Top             =   1200
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Quantifier"
         Height          =   255
         Index           =   17
         Left            =   3240
         TabIndex        =   18
         Top             =   4680
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Plant Rcv Dxs"
         Height          =   255
         Index           =   16
         Left            =   3240
         TabIndex        =   17
         Top             =   4320
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Bill Office"
         Height          =   255
         Index           =   15
         Left            =   3240
         TabIndex        =   16
         Top             =   3960
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Locations"
         Height          =   255
         Index           =   14
         Left            =   3240
         TabIndex        =   15
         Top             =   3600
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Verify Checkout"
         Height          =   255
         Index           =   13
         Left            =   3240
         TabIndex        =   14
         Top             =   2640
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Verify Clinical"
         Height          =   255
         Index           =   12
         Left            =   3240
         TabIndex        =   13
         Top             =   2280
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Impressions"
         Height          =   255
         Index           =   11
         Left            =   3240
         TabIndex        =   12
         Top             =   1920
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Rx"
         Height          =   255
         Index           =   10
         Left            =   3240
         TabIndex        =   11
         Top             =   1560
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Eyes"
         Height          =   255
         Index           =   9
         Left            =   3240
         TabIndex        =   10
         Top             =   1200
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Close Denials"
         Height          =   255
         Index           =   8
         Left            =   600
         TabIndex        =   9
         Top             =   4680
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Set Insurer Designation"
         Height          =   255
         Index           =   7
         Left            =   600
         TabIndex        =   8
         Top             =   4320
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Fee Charges"
         Height          =   255
         Index           =   6
         Left            =   600
         TabIndex        =   7
         Top             =   3960
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Medicare Schedules"
         Height          =   255
         Index           =   5
         Left            =   600
         TabIndex        =   6
         Top             =   3600
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Patient Dollars"
         Height          =   255
         Index           =   4
         Left            =   600
         TabIndex        =   5
         Top             =   2640
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Batch Checks"
         Height          =   255
         Index           =   3
         Left            =   600
         TabIndex        =   4
         Top             =   2280
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Paid Date"
         Height          =   255
         Index           =   2
         Left            =   600
         TabIndex        =   3
         Top             =   1920
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Balances"
         Height          =   255
         Index           =   1
         Left            =   600
         TabIndex        =   2
         Top             =   1560
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Reset Bill Date"
         Height          =   255
         Index           =   0
         Left            =   600
         TabIndex        =   1
         Top             =   1200
         Width           =   2775
      End
   End
End
Attribute VB_Name = "frmHomeAlt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

