Attribute VB_Name = "mdlRegSettings"
Option Explicit
Public Const REG_SZ                       As Long = 1
Private Const REG_DWORD                   As Long = 4
Public Const HKEY_LOCAL_MACHINE           As Long = &H80000002
Private Const ERROR_NONE                  As Long = 0
Private Const KEY_QUERY_VALUE             As Long = &H1
Private Const KEY_SET_VALUE               As Long = &H2
Private Const KEY_ALL_ACCESS              As Long = &H3F
Private Const REG_OPTION_NON_VOLATILE     As Long = 0
Public Const RegPath                      As String = "Software\IOPracticeware"
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, _
                                                                                    ByVal lpSubKey As String, _
                                                                                    ByVal Reserved As Long, _
                                                                                    ByVal lpClass As String, _
                                                                                    ByVal dwOptions As Long, _
                                                                                    ByVal samDesired As Long, _
                                                                                    ByVal lpSecurityAttributes As Long, _
                                                                                    phkResult As Long, _
                                                                                    lpdwDisposition As Long) As Long
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, _
                                                                                ByVal lpSubKey As String, _
                                                                                ByVal ulOptions As Long, _
                                                                                ByVal samDesired As Long, _
                                                                                phkResult As Long) As Long
Private Declare Function RegQueryValueExString Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                                                                            ByVal lpValueName As String, _
                                                                                            ByVal lpReserved As Long, _
                                                                                            lpType As Long, _
                                                                                            ByVal lpData As String, _
                                                                                            lpcbData As Long) As Long
Private Declare Function RegSetValueExString Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, _
                                                                                        ByVal lpValueName As String, _
                                                                                        ByVal Reserved As Long, _
                                                                                        ByVal dwType As Long, _
                                                                                        ByVal lpValue As String, _
                                                                                        ByVal cbData As Long) As Long
Private Declare Function RegQueryValueExLong Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                                                                          ByVal lpValueName As String, _
                                                                                          ByVal lpReserved As Long, _
                                                                                          lpType As Long, _
                                                                                          lpData As Long, _
                                                                                          lpcbData As Long) As Long
Private Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                                                                          ByVal lpValueName As String, _
                                                                                          ByVal lpReserved As Long, _
                                                                                          lpType As Long, _
                                                                                          ByVal lpData As Long, _
                                                                                          lpcbData As Long) As Long
Private Declare Function RegSetValueExLong Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, _
                                                                                      ByVal lpValueName As String, _
                                                                                      ByVal Reserved As Long, _
                                                                                      ByVal dwType As Long, _
                                                                                      lpValue As Long, _
                                                                                      ByVal cbData As Long) As Long
Private Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, _
                                                                                    ByVal lpValueName As String) As Long



Public Sub CreateNewKey(sNewKeyName As String, _
                        lPredefinedKey As Long)

Dim hNewKey As Long   'handle to the new key

Dim lRetVal As Long   'result of the RegCreateKeyEx function
    lRetVal = RegCreateKeyEx(lPredefinedKey, sNewKeyName, 0&, vbNullString, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 0&, hNewKey, lRetVal)
    RegCloseKey (hNewKey)
End Sub


Public Sub DeleteKeyValue(sKeyName As String, _
                          lpValueName As String)

Dim hKey As Long         'handle of open key

'open the specified key
    RegOpenKeyEx HKEY_LOCAL_MACHINE, sKeyName, 0, KEY_SET_VALUE, hKey
    RegDeleteValue hKey, lpValueName
    RegCloseKey (hKey)
End Sub


Public Function QueryHKLMValue(sValueName As String, _
                               sValue As String) As Boolean

    QueryHKLMValue = True
    QueryValue RegPath, sValueName, sValue
    If LenB(sValue) = 0 Then
        QueryHKLMValue = False
    End If
End Function


Public Sub QueryValue(sKeyName As String, _
                      sValueName As String, _
                      sValue As String)

Dim hKey As Long         'handle of opened key

    RegOpenKeyEx HKEY_LOCAL_MACHINE, sKeyName, 0, KEY_QUERY_VALUE, hKey
    QueryValueEx hKey, sValueName, sValue
    RegCloseKey (hKey)
End Sub


Private Function QueryValueEx(ByVal lhKey As Long, _
                              ByVal szValueName As String, _
                              vValue As Variant) As Long

Dim cch    As Long
Dim lrc    As Long
Dim lType  As Long
Dim lValue As Long
Dim sValue As String
    On Error GoTo QueryValueExError
' Determine the size and type of data to be read
    lrc = RegQueryValueExNULL(lhKey, szValueName, 0&, lType, 0&, cch)
    If lrc <> ERROR_NONE Then
        Error 5
    End If
    Select Case lType
' For strings
    Case REG_SZ:
        sValue = String$(cch, 0)
        lrc = RegQueryValueExString(lhKey, szValueName, 0&, lType, sValue, cch)
        If lrc = ERROR_NONE Then
            vValue = Left$(sValue, cch - 1)
        Else
            vValue = Empty
        End If
' For DWORDS
    Case REG_DWORD:
        lrc = RegQueryValueExLong(lhKey, szValueName, 0&, lType, lValue, cch)
        If lrc = ERROR_NONE Then
            vValue = lValue
        End If
    Case Else
'all other data types not supported
        lrc = -1
    End Select
QueryValueExExit:
    QueryValueEx = lrc
Exit Function

QueryValueExError:
    Resume QueryValueExExit
End Function


Public Sub SetKeyValue(sKeyName As String, _
                       sValueName As String, _
                       vValueSetting As Variant, _
                       lValueType As Long)

Dim hKey As Long         'handle of open key

'open the specified key
    RegOpenKeyEx HKEY_LOCAL_MACHINE, sKeyName, 0, KEY_SET_VALUE, hKey
    SetValueEx hKey, sValueName, lValueType, vValueSetting
    RegCloseKey (hKey)
End Sub


Public Function SetValueEx(ByVal hKey As Long, _
                           sValueName As String, _
                           lType As Long, _
                           vValue As Variant) As Long

Dim sValue As String

    Select Case lType
    Case REG_SZ
        sValue = vValue & vbNullChar
        SetValueEx = RegSetValueExString(hKey, sValueName, 0&, lType, sValue, Len(sValue))
    Case REG_DWORD
''lValue = vValue
        SetValueEx = RegSetValueExLong(hKey, sValueName, 0&, lType, vValue, 4)
    End Select
End Function
