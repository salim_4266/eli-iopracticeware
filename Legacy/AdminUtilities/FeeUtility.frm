VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmFeeUtility 
   BackColor       =   &H006C6928&
   BorderStyle     =   0  'None
   Caption         =   "Medicare Fee Schedules"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   -105
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   Icon            =   "FeeUtility.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstFees 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3900
      Left            =   8040
      TabIndex        =   4
      Top             =   2040
      Width           =   3375
   End
   Begin VB.ListBox lstType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5340
      ItemData        =   "FeeUtility.frx":0442
      Left            =   360
      List            =   "FeeUtility.frx":0444
      MultiSelect     =   1  'Simple
      TabIndex        =   2
      Top             =   2040
      Width           =   7215
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   975
      Left            =   9840
      TabIndex        =   6
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FeeUtility.frx":0446
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFees 
      Height          =   975
      Left            =   360
      TabIndex        =   7
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FeeUtility.frx":0625
   End
   Begin VB.Label lblSelectSchedules 
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Select Schedules"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   8040
      TabIndex        =   5
      Top             =   1680
      Width           =   1860
   End
   Begin VB.Label lblSelectPlans 
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Select Plans"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   360
      TabIndex        =   3
      Top             =   1680
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Message"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   5640
      TabIndex        =   1
      Top             =   1080
      Visible         =   0   'False
      Width           =   1170
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H006C6928&
      Caption         =   "Set Medicare Fee Schedules "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   4095
      TabIndex        =   0
      Top             =   120
      Width           =   4185
   End
End
Attribute VB_Name = "frmFeeUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Function ClosePrevFee(ByVal ASrv As String, _
                              ByVal TheIns As Long, _
                              ByVal TheDate As String, _
                              ByVal StartDate As String) As Boolean

Dim i      As Integer
Dim RetSrv As PracticeFeeSchedules
    ClosePrevFee = False
    If LenB(Trim$(ASrv)) Then
        Set RetSrv = New PracticeFeeSchedules
        RetSrv.SchService = ASrv
        RetSrv.SchPlanId = TheIns
        RetSrv.SchDoctorId = 0
        RetSrv.SchStartDate = vbNullString
        If RetSrv.FindServiceDirect > 0 Then
            i = 1
            Do Until Not (RetSrv.SelectService(i))
                If RetSrv.SchStartDate < StartDate Then
                    If LenB(Trim$(RetSrv.SchEndDate)) = 0 Then
                        RetSrv.SchEndDate = TheDate
                        RetSrv.ApplyService
                        Exit Do
                    End If
                End If
                i = i + 1
            Loop
        End If
        Set RetSrv = Nothing
    End If
End Function


Private Sub cmdExit_Click()

    Unload frmFeeUtility
End Sub


Private Sub cmdFees_Click()

    frmEventMsgs.SetButtons "Are you sure ?", "", "Yes", "No"
    frmEventMsgs.Show 1
    If frmEventMsgs.Result = 2 Then
        DoMedicareFees
        With frmEventMsgs
            .Header = "Finished Loading Fee Schedules"
            .AcceptText = vbNullString
            .RejectText = "Yes"
            .CancelText = "No"
            .Other0Text = vbNullString
            .Other1Text = vbNullString
            .Other2Text = vbNullString
            .Other3Text = vbNullString
            .Other4Text = vbNullString
            .Show 1
        End With
    End If
End Sub


Private Function DoMedicareDMEFees() As Boolean

Dim i             As Long
Dim q             As Long
Dim j             As Long
Dim InsId         As Long
Dim SetLoc        As String
Dim Rec           As String
Dim TheFile       As String
Dim TheLoc        As String
Dim TheFee1       As Single
Dim TheFeeNY      As Single
Dim TheFeeNJ      As Single
Dim TheFeePA      As Single
Dim TheCode       As String
Dim TheMod        As String
Dim SrvOn         As Boolean
Dim RetSrv        As Service
Dim RetFee        As PracticeFeeSchedules
Dim FirstOfYear   As CIODateTime
Dim EndOfLastYear As CIODateTime
Dim FN1           As Long
    DoMedicareDMEFees = True
    If (lstType.SelCount > 0) And (lstFees.ListIndex >= 0) Then
        TheFile = FeeDirectory + Trim$(lstFees.List(lstFees.ListIndex))
        TheFile = Trim$(Left$(TheFile, Len(TheFile) - 4))
        If FM.IsFileThere(TheFile) Then
            SetLoc = Trim$(Mid$(lstFees.List(lstFees.ListIndex), Len(lstFees.List(lstFees.ListIndex)) - 4, 4))
            SetLoc = Left$(SetLoc, 3)
            SetLoc = Mid$(SetLoc, 2, 2)
            GoSub DoSch
        End If
    Else
        MsgBox "Nothing selected"
    End If
Exit Function


DoSch:
    Set FirstOfYear = New CIODateTime
    Set EndOfLastYear = New CIODateTime
    FirstOfYear.MyDateTime = DateSerial(Year(Now), 1, 1)
    EndOfLastYear.MyDateTime = DateSerial(Year(Now) - 1, 12, 31)
    Set RetFee = New PracticeFeeSchedules
    FN1 = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FN1)
    Line Input #FN1, Rec
    Do While Not (EOF(FN1))
        Line Input #FN1, Rec
        TheCode = vbNullString
        TheMod = vbNullString
        TheFee1 = 0
        TheFeeNY = 0
        TheFeeNJ = 0
        TheFeePA = 0
        i = InStr(Rec, vbTab)
        If i > 0 Then
            TheCode = Trim$(Left$(Rec, i - 1))
            TheMod = vbNullString
            j = i + 1
            i = InStr(j, Rec, vbTab)
            If j > i Then
                If i > 0 Then
                    TheMod = Mid$(Rec, j, (i - 1) - (j - 1))
                End If
            End If
            j = i + 1
            i = InStr(j, Rec, vbTab)
            If i > 0 Then
                TheLoc = Trim$(Mid$(Rec, j, (i - 1) - (j - 1)))
                StripCharacters TheLoc, "$"
                If Not (IsNumeric(Left$(TheLoc, 1))) Then
                    TheLoc = Mid$(TheLoc, 2, Len(TheLoc) - 1)
                End If
                If Not (IsNumeric(Mid$(TheLoc, Len(TheLoc), 1))) Then
                    TheLoc = Left$(TheLoc, Len(TheLoc) - 1)
                End If
                TheFeeNY = Val(TheLoc)
                j = i + 1
                i = InStr(j, Rec, vbTab)
                If i > 0 Then
                    TheLoc = Trim$(Mid$(Rec, j, (i - 1) - (j - 1)))
                    StripCharacters TheLoc, "$"
                    If Not (IsNumeric(Left$(TheLoc, 1))) Then
                        TheLoc = Mid$(TheLoc, 2, Len(TheLoc) - 1)
                    End If
                    If Not (IsNumeric(Mid$(TheLoc, Len(TheLoc), 1))) Then
                        TheLoc = Left$(TheLoc, Len(TheLoc) - 1)
                    End If
                    TheFeeNJ = Val(TheLoc)
                    TheLoc = Trim$(Mid$(Rec, i + 1, Len(Rec) - i))
                    StripCharacters TheLoc, "$"
                    If Not (IsNumeric(Left$(TheLoc, 1))) Then
                        TheLoc = Mid$(TheLoc, 2, Len(TheLoc) - 1)
                    End If
                    If Not (IsNumeric(Mid$(TheLoc, Len(TheLoc), 1))) Then
                        TheLoc = Left$(TheLoc, Len(TheLoc) - 1)
                    End If
                    TheFeePA = Val(TheLoc)
                End If
            End If
            If SetLoc = "NY" Then
                TheFee1 = TheFeeNY
            ElseIf (SetLoc = "NJ") Then
                TheFee1 = TheFeeNJ
            ElseIf (SetLoc = "PA") Then
                TheFee1 = TheFeePA
            End If
        End If
        If TheFee1 > 0 Then
            If LenB(Trim$(TheCode)) Then
                GoSub GetSrv
                If SrvOn And (Trim$(TheMod) = "") And (Trim$(TheCode) <> "") And (Len(Trim$(TheCode)) > 3) And (Trim$(TheCode) <> "0") Then
                    With lstType
                        For q = 0 To .ListCount - 1
                            If .Selected(q) Then
                                InsId = Val(Trim$(Mid$(.List(q), 57, 10)))
                                If InsId > 0 Then
                                    RetFee.SchService = TheCode
                                    RetFee.SchPlanId = InsId
                                    RetFee.SchDoctorId = -1
                                    RetFee.SchStartDate = FirstOfYear.GetIODate
                                    RetFee.SchEndDate = vbNullString
                                    If RetFee.FindService < 1 Then
                                        RetFee.SchServiceId = 0
                                        RetFee.SchService = TheCode
                                        RetFee.SchPlanId = InsId
                                        RetFee.SchDoctorId = -1
                                        RetFee.SchAdjustmentAllowed = Round(TheFee1, 2)
                                        RetFee.SchFacFee = Round(TheFee1, 2)
                                        RetFee.SchAdjustmentRate = 80
                                        RetFee.SchEndDate = vbNullString
                                        RetFee.SchStartDate = FirstOfYear.GetIODate
                                        RetFee.SchFee = Round(GetServiceFee(TheCode), 2)
                                        RetFee.ApplyService
                                        ClosePrevFee TheCode, InsId, EndOfLastYear.GetIODate, FirstOfYear.GetIODate
                                    Else
                                        If RetFee.SelectService(1) Then
                                            If RetFee.SchStartDate < FirstOfYear.GetIODate Then
                                                RetFee.SchServiceId = 0
                                                If RetFee.RetrieveService Then
                                                    RetFee.SchServiceId = 0
                                                    RetFee.SchService = TheCode
                                                    RetFee.SchPlanId = InsId
                                                    RetFee.SchDoctorId = -1
                                                    RetFee.SchAdjustmentAllowed = Round(TheFee1, 2)
                                                    RetFee.SchFacFee = Round(TheFee1, 2)
                                                    RetFee.SchAdjustmentRate = 80
                                                    RetFee.SchEndDate = vbNullString
                                                    RetFee.SchStartDate = FirstOfYear.GetIODate
                                                    RetFee.SchFee = Round(GetServiceFee(TheCode), 2)
                                                    RetFee.ApplyService
                                                    ClosePrevFee TheCode, InsId, EndOfLastYear.GetIODate, FirstOfYear.GetIODate
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            lblTotal.Caption = "Service Code " & Trim$(TheCode) & " Added for InsId" & Str$(InsId)
                            lblTotal.Visible = True
                            DoEvents
                        Next q
                    End With
                Else
                    lblTotal.Caption = "Service Code " & Trim$(TheCode) & " Skipped"
                    lblTotal.Visible = True
                    DoEvents
                End If
            End If
        End If
    Loop
    FM.CloseFile CLng(FN1)
    lblTotal.Visible = False
    Set RetFee = Nothing
    Set EndOfLastYear = Nothing
    Set FirstOfYear = Nothing
    Return
GetSrv:
    SrvOn = False
    Set RetSrv = New Service
    RetSrv.ServiceType = "P"
    RetSrv.Service = Trim$(TheCode)
    If RetSrv.RetrieveService Then
        If Trim$(RetSrv.ServiceType) = "P" Then
            SrvOn = True
        End If
    End If
    Set RetSrv = Nothing
    Return
End Function


Private Function DoMedicareFees() As Boolean

Dim i         As Long
Dim q         As Long
Dim InsId     As Long
Dim Rec       As String
Dim SetLoc    As String
Dim TheFile   As String
Dim ClsDate   As String
Dim TheDate   As String
Dim TheLoc    As String
Dim TheFee1   As Single
Dim TheFee2   As Single
Dim TheCode   As String
Dim TheMod    As String
Dim IsAllowed As Boolean
Dim SrvOn     As Boolean
Dim RetSrv    As Service
Dim RetFee    As PracticeFeeSchedules
Dim FN1       As Long
    DoMedicareFees = True
    If (lstType.SelCount > 0) And (lstFees.ListIndex >= 0) Then
        If (InStr(lstFees.List(lstFees.ListIndex), "(") <> 0) Then
            DoMedicareDMEFees
            Exit Function
        End If
        TheFile = FeeDirectory + Trim$(lstFees.List(lstFees.ListIndex))
        If FM.IsFileThere(TheFile) Then
            If (MsgBox("Use Allowed as Standard Fee ?", vbYesNo) = vbYes) Then
                IsAllowed = True
            End If
            i = InStr(lstFees.List(lstFees.ListIndex), "-")
            If i > 0 Then
                SetLoc = Mid$(lstFees.List(lstFees.ListIndex), i + 1, 2)
            Else
                SetLoc = "01"
            End If
            GoSub DoSch
        End If
    Else
        MsgBox "Nothing selected"
    End If
Exit Function


DoSch:
    Set RetFee = New PracticeFeeSchedules
    FN1 = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FN1)
    Do While Not (EOF(FN1))
        Line Input #FN1, Rec
        ClsDate = Trim$(Str$(Val(Mid$(Rec, 2, 4)) - 1)) & "1231"
        TheDate = Mid$(Rec, 2, 4) & "0101"
        TheLoc = Mid$(Rec, 17, 2)
        TheCode = Trim$(Mid$(Rec, 22, 5))
        If (InStr(Rec, ",,") > 0) Then
            TheMod = vbNullString
            TheFee1 = Val(Trim$(Mid$(Rec, 31, 10)))
            TheFee2 = Val(Trim$(Mid$(Rec, 44, 10)))
        Else
            TheMod = Mid$(Rec, 30, 2)
            TheFee1 = Val(Trim$(Mid$(Rec, 35, 10)))
            TheFee2 = Val(Trim$(Mid$(Rec, 48, 10)))
        End If
        If TheCode <> "0" Then
            GoSub GetSrv
            If SrvOn And (Trim$(TheMod) = "") And (TheLoc = SetLoc) And (Trim$(TheCode) <> "") And (Len(Trim$(TheCode)) > 3) And (Trim$(TheCode) <> "0") Then
                For q = 0 To lstType.ListCount - 1
                    If lstType.Selected(q) Then
                        InsId = Val(Trim$(Mid$(lstType.List(q), 57, 10)))
                        If InsId > 0 Then
                            RetFee.SchService = TheCode
                            RetFee.SchPlanId = InsId
                            RetFee.SchDoctorId = -1
                            RetFee.SchStartDate = TheDate
                            RetFee.SchEndDate = vbNullString
                            If RetFee.FindService < 1 Then
                                RetFee.SchServiceId = 0
                                RetFee.SchService = TheCode
                                RetFee.SchPlanId = InsId
                                RetFee.SchDoctorId = -1
                                RetFee.SchLocId = -1
                                RetFee.SchAdjustmentAllowed = Round(TheFee1, 2)
                                RetFee.SchFacFee = Round(TheFee2, 2)
                                RetFee.SchAdjustmentRate = 80
                                RetFee.SchEndDate = vbNullString
                                RetFee.SchStartDate = TheDate
                                RetFee.SchFee = Round(GetServiceFee(TheCode), 2)
                                If IsAllowed Then
                                    RetFee.SchFee = Round(TheFee1, 2)
                                End If
                                RetFee.ApplyService
                                ClosePrevFee TheCode, InsId, ClsDate, TheDate
                            Else
                                If RetFee.SelectService(1) Then
                                    If RetFee.SchStartDate < TheDate Then
                                        RetFee.SchServiceId = 0
                                        If RetFee.RetrieveService Then
                                            RetFee.SchServiceId = 0
                                            RetFee.SchService = TheCode
                                            RetFee.SchPlanId = InsId
                                            RetFee.SchDoctorId = -1
                                            RetFee.SchLocId = -1
                                            RetFee.SchAdjustmentAllowed = Round(TheFee1, 2)
                                            RetFee.SchFacFee = Round(TheFee2, 2)
                                            RetFee.SchAdjustmentRate = 80
                                            RetFee.SchEndDate = vbNullString
                                            RetFee.SchStartDate = TheDate
                                            RetFee.SchFee = Round(GetServiceFee(TheCode), 2)
                                            If IsAllowed Then
                                                RetFee.SchFee = Round(TheFee1, 2)
                                            End If
                                            RetFee.ApplyService
                                            ClosePrevFee TheCode, InsId, ClsDate, TheDate
                                        End If
                                    ElseIf (RetFee.SchStartDate = TheDate) And (RetFee.SchLocId <> -1) Then 'NOT RETFEE.SCHSTARTDATE...
                                        RetFee.SchServiceId = 0
                                        If RetFee.RetrieveService Then
                                            RetFee.SchServiceId = 0
                                            RetFee.SchService = TheCode
                                            RetFee.SchPlanId = InsId
                                            RetFee.SchDoctorId = -1
                                            RetFee.SchLocId = -1
                                            RetFee.SchAdjustmentAllowed = Round(TheFee1, 2)
                                            RetFee.SchFacFee = Round(TheFee2, 2)
                                            RetFee.SchAdjustmentRate = 80
                                            RetFee.SchEndDate = vbNullString
                                            RetFee.SchStartDate = TheDate
                                            RetFee.SchFee = Round(GetServiceFee(TheCode), 2)
                                            If IsAllowed Then
                                                RetFee.SchFee = Round(TheFee1, 2)
                                            End If
                                            RetFee.ApplyService
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                    lblTotal.Caption = "Service Code " & Trim$(TheCode) & " Added for InsId" & Str$(InsId)
                    lblTotal.Visible = True
                    DoEvents
                Next q
            Else
                lblTotal.Caption = "Service Code " & Trim$(TheCode) & " Skipped"
                lblTotal.Visible = True
                DoEvents
            End If
        End If
    Loop
    Call InsertLog("FrmFeeUtility.frm", "Loaded Medicare FeeUtility", LoginCatg.MediCare, "", "", 0, 0)
    FM.CloseFile CLng(FN1)
    lblTotal.Visible = False
    Set RetFee = Nothing
    Return
GetSrv:
    SrvOn = False
    Set RetSrv = New Service
    RetSrv.ServiceType = "P"
    RetSrv.Service = Trim$(TheCode)
    If RetSrv.RetrieveService Then
        If Trim$(RetSrv.ServiceType) = "P" Then
            SrvOn = True
        End If
    End If
    Set RetSrv = Nothing
    Return
End Function

Private Sub Form_Load()

    LoadPlans
    LoadFees
End Sub


Private Function GetServiceFee(ByVal ASrv As String) As Single

Dim RetSrv As Service

    GetServiceFee = 0
    If LenB(Trim$(ASrv)) Then
        Set RetSrv = New Service
        RetSrv.ServiceType = "P"
        RetSrv.Service = Trim$(ASrv)
        If RetSrv.RetrieveService Then
            GetServiceFee = RetSrv.ServiceFee
        End If
        Set RetSrv = Nothing
    End If
End Function


Private Sub LoadFees()

Dim MyFile As String

    lstFees.Clear
    MyFile = Dir(FeeDirectory & "PF*.txt")
    Do While (Trim$(MyFile) <> "")
        lstFees.AddItem Trim$(MyFile)
        MyFile = Dir
    Loop
    MyFile = Dir(FeeDirectory & "DME*.txt")
    Do While (Trim$(MyFile) <> "")
        With lstFees
            .AddItem Trim$(MyFile) & " (NY)"
            .AddItem Trim$(MyFile) & " (NJ)"
            .AddItem Trim$(MyFile) & " (PA)"
        End With 'lstFees
        MyFile = Dir
    Loop
End Sub


Private Sub LoadPlans()

On Error GoTo lLoadPlans_Error
Dim k           As Integer
Dim Temp        As String
Dim DisplayText As String
Dim RS As Recordset
    lstType.Clear
    Dim StrSQL As String
    StrSQL = " Select ISNULL(InsurerName,'') AS InsurerName,ISNULL(InsurerPlanType,'') AS InsurerPlanType,ISNULL(InsurerReferenceCode,'') As InsurerReferenceCode,ISNULL(InsurerAddress,'') AS InsurerAddress,ISNULL(InsurerCity,'') As InsurerCity,ISNULL(InsurerState,'') AS InsurerState,ISNULL(InsurerId,'') AS InsurerId FROM PracticeInsurers WHERE InsurerName >= '' ORDER BY InsurerName ASC, InsurerAddress ASC, InsurerPlanId ASC, InsurerGroupId ASC "
    Set RS = GetRS(StrSQL)
    If (Not RS Is Nothing And RS.RecordCount > 0) Then
        Do While Not (RS.EOF)
            DisplayText = Space$(56)
            Temp = Trim$(RS("InsurerName"))
            If Len(Temp) > 15 Then
                Temp = Left$(Temp, 15)
            End If
            Mid$(DisplayText, 1, Len(Temp)) = Temp
            Temp = Trim$(Left$(RS("InsurerPlanType"), 3))
            Mid$(DisplayText, 17, Len(Temp)) = Temp
            Mid$(DisplayText, 21, 1) = RS("InsurerReferenceCode")
            Temp = Trim$(RS("InsurerAddress")) & " " & Trim$(RS("InsurerCity")) & " " & Trim$(RS("InsurerState"))
            If Len(Temp) > 34 Then
                Temp = Left$(Temp, 34)
            End If
            Mid$(DisplayText, 23, Len(Temp)) = Temp
            DisplayText = DisplayText + Trim$(Str$(RS("InsurerId")))
            lstType.AddItem DisplayText
            RS.MoveNext
        Loop
    End If
    Set RS = Nothing
lLoadPlans_Error:
    LogError "FeeUtility", "LoadPlans", Err, Err.Description
End Sub
