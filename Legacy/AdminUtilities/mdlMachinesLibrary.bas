Attribute VB_Name = "mdlMachineLibrary"
Option Explicit
Public strAction            As String
Public connectedDevices     As Devices
Public connectedDevice      As Device
Public registeredDevices    As Devices
Public registeredDevice     As Device


Public Function Pad(ByVal strValue As String, _
                    ByVal intLen As Long) As String

    Pad = strValue & Space$(intLen - Len(strValue))
End Function


Public Sub RemoveReferences()

    Set connectedDevice = Nothing
    Set connectedDevices = Nothing
    Set registeredDevice = Nothing
    Set registeredDevices = Nothing
End Sub



