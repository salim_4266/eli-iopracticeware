Attribute VB_Name = "ToolsLib"
Option Explicit
Public DMCn   As ADODB.Connection
Public DMCmd  As ADODB.Command

Public Sub dbDMClose()
    If DMCn.State > 0 Then
        DMCn.Close
    End If
End Sub

Public Sub dbDMOpen(ByVal ServerName As String, _
                    ByVal DBName As String)

    Set DMCn = New ADODB.Connection
    With DMCn
        .Provider = "Microsoft.Jet.OLEDB.4.0"
        .Properties("Data Source").Value = ServerName
'DMCn.Properties("Initial Catalog").Value = DBName
'DMCn.Properties("Integrated Security").Value = "SSPI"
        .Open
    End With
    Set DMCmd = CreateObject("ADODB.Command")
    DMCmd.ActiveConnection = DMCn
End Sub

Public Function DoMonthEnd() As Boolean
Dim Tool    As ToolRoutines
Dim comTool As CommonTemplates
    DoMonthEnd = False
    If (InStr(1, LCase$(Command$), "--monthend") <> 0) Then
        Set Tool = New ToolRoutines
        Tool.ResetReceivableBalances frmHome.Label1
        Set Tool = Nothing
        Set comTool = New CommonTemplates
        comTool.ResetInsurerDesignation "", False, frmHome.Label1
        Set comTool = Nothing
        Set comTool = New CommonTemplates
        comTool.ResetPatientBillDate "", False, frmHome.Label1
        Set comTool = Nothing
        DoMonthEnd = True
    End If
End Function


'Changes  were done for TFS item #2261.As Patient.cls needs this function and used in AI.
'We already have this Function in Library.bas file.As Library.bas doesn't exists in this project i am adding this method here.
Public Function GetAge(BirthDate As String) As Integer
Dim Offset As Integer
Dim LclDate1 As String
Dim LclDate2 As String
Dim BirthYear As Integer
Dim CurrentYear As Integer
Dim MyDate As Date
Offset = 0
LclDate1 = ""
MyDate = Date
If (Trim$(BirthDate) = "") Then
    GetAge = -1
    Exit Function
End If
Call FormatTodaysDate(LclDate1, False)
LclDate1 = Left(LclDate1, 2) + Mid(LclDate1, 4, 2)
LclDate2 = Mid(BirthDate, 5, 4)
CurrentYear = Year(MyDate)
BirthYear = Val(Left(BirthDate, 4))
If (InStrPS(BirthDate, "/") > 0) Then
    LclDate2 = Left(BirthDate, 2) + Mid(BirthDate, 4, 2)
    BirthYear = Val(Mid(BirthDate, 7, 4))
End If
If (LclDate2 > LclDate1) Then
    Offset = 1
End If
GetAge = (CurrentYear - BirthYear) - Offset
End Function
