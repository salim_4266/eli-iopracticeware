VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form FSignIn 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdOne 
      Height          =   990
      Left            =   5880
      TabIndex        =   1
      Top             =   1680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTwo 
      Height          =   990
      Left            =   7560
      TabIndex        =   2
      Top             =   1680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":01DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFour 
      Height          =   990
      Left            =   5880
      TabIndex        =   4
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":03B8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFive 
      Height          =   990
      Left            =   7560
      TabIndex        =   5
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":0594
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSeven 
      Height          =   990
      Left            =   5880
      TabIndex        =   7
      Top             =   3840
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":0770
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEight 
      Height          =   990
      Left            =   7560
      TabIndex        =   8
      Top             =   3840
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":094C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZero 
      Height          =   990
      Left            =   7560
      TabIndex        =   11
      Top             =   4920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":0B28
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdThree 
      Height          =   990
      Left            =   9240
      TabIndex        =   3
      Top             =   1680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":0D04
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSix 
      Height          =   990
      Left            =   9240
      TabIndex        =   6
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":0EE0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNine 
      Height          =   990
      Left            =   9240
      TabIndex        =   9
      Top             =   3840
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":10BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9240
      TabIndex        =   0
      Top             =   4920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":1298
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5880
      TabIndex        =   12
      Top             =   4920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   -1  'True
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":1477
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   1680
      TabIndex        =   13
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":1658
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLogout 
      Height          =   990
      Left            =   3960
      TabIndex        =   14
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SignIn.frx":1837
   End
   Begin VB.Label lblPID 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "****"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   720
      Left            =   2160
      TabIndex        =   15
      Top             =   3960
      Width           =   1500
   End
   Begin VB.Label lblInstructionsPID 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Please Enter Your Personal Id Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1215
      Left            =   360
      TabIndex        =   10
      Top             =   2640
      Width           =   5055
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "FSignIn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public FormLogin As CUserLogin

Private sCurrentValue As String
Private iMaxEntry As Integer
Private fPreventDoubleFire As Boolean

Private Sub cmdDelete_Click()
    If Len(sCurrentValue) > 0 Then
        lblPID.Caption = String$(Len(sCurrentValue) - 1, "*")
        sCurrentValue = Left$(sCurrentValue, Len(sCurrentValue) - 1)
    End If
End Sub

Private Sub cmdDone_Click()

    If (Len(sCurrentValue) = iMaxEntry) Then
        If FormLogin.AuthenticateUserPID(sCurrentValue) Then
            Set FormLogin = Nothing
            sCurrentValue = ""
            lblPID.Caption = ""
            Unload FSignIn
        Else
            cmdLogout.Visible = False
            sCurrentValue = ""
            lblPID.Caption = ""
        End If
    End If

End Sub

Private Sub cmdExit_Click()
    sCurrentValue = ""
    lblPID.Caption = ""
    Set FormLogin = Nothing
    Unload FSignIn
End Sub

Private Sub cmdLogout_Click()
FormLogin.LogoffUser
cmdLogout.Visible = False
'Set FormLogin = Nothing
'Unload FSignIn
End Sub

Private Sub cmdZero_Click()
    Call EnterPIDKey("0")
End Sub

Private Sub cmdOne_Click()
    Call EnterPIDKey("1")
End Sub

Private Sub cmdTwo_Click()
    Call EnterPIDKey("2")
End Sub

Private Sub cmdThree_Click()
    Call EnterPIDKey("3")
End Sub

Private Sub cmdFour_Click()
    Call EnterPIDKey("4")
End Sub

Private Sub cmdFive_Click()
    Call EnterPIDKey("5")
End Sub

Private Sub cmdSix_Click()
    Call EnterPIDKey("6")
End Sub

Private Sub cmdSeven_Click()
    Call EnterPIDKey("7")
End Sub

Private Sub cmdEight_Click()
    Call EnterPIDKey("8")
End Sub

Private Sub cmdNine_Click()
    Call EnterPIDKey("9")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If (47 < KeyAscii And KeyAscii < 58) Then ' Make sure the key pressed is a number 0-9
        If (fPreventDoubleFire) Then 'HACK: For some reason this event fires twice for each click.
            fPreventDoubleFire = False
        Else
            Call EnterPIDKey(Chr(KeyAscii))
            fPreventDoubleFire = True
        End If
    ElseIf (KeyAscii = 8) Then ' If Backspace is pressed delete an entry
        If (fPreventDoubleFire) Then
            fPreventDoubleFire = False
        Else
            Call cmdDelete_Click
            fPreventDoubleFire = True
        End If
    ElseIf (KeyAscii = 13) Then
        If (fPreventDoubleFire) Then
            fPreventDoubleFire = False
        Else
            Call cmdDone_Click
            fPreventDoubleFire = True
        End If
    End If
End Sub

Private Sub EnterPIDKey(PIDKey As String)
    If Len(sCurrentValue) < iMaxEntry Then
        sCurrentValue = sCurrentValue & PIDKey
        lblPID.Caption = String$(Len(sCurrentValue), "*")
    End If
End Sub

Private Sub Form_Load()
    On Error GoTo lForm_Load_Error

    lblPID.Caption = ""
    sCurrentValue = ""
    iMaxEntry = 4
    If (FormLogin.sNameLong <> "") Then
        Call cmdLogout_Click
    Else
        cmdLogout.Visible = False
    End If
    fPreventDoubleFire = False

    Exit Sub

lForm_Load_Error:

    LogError "FSignIn", "Form_Load", Err, Err.Description
End Sub

Private Sub Form_Initialize()
    Set FormLogin = New CUserLogin
End Sub
Public Function FrmClose()
 Call cmdExit_Click
 End Function

