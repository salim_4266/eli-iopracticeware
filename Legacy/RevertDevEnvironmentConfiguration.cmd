REM Checking whether IO's client is installed
if not exist "%LocalAppData%\IO Practiceware\IO.Practiceware.Interop.dll" goto EndScriptNotInstalled

REM Going to installation directory to unregister .NET and native COM registrations
cd "%LocalAppData%\IO Practiceware"

regsvr32.exe /u /s IOConnectedDevices.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u IO.Practiceware.Interop.dll

%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u LabReport.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u EPLib.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u ClinicalDecisions.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u GrowthCharts.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u Immunizations.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u Surveillance.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u EmergencyAccess.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u ClinicalReports.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe /u ClinicalIntegration.dll

REM Remove all Dlls from GAC
for %%I in (*.dll) do "%programfiles%\Microsoft SDKs\Windows\v7.1\Bin\NETFX 4.0 Tools\gacutil" -u %%~nI

reg delete HKCR\IO.Practiceware.Interop.ComWrapper /f > null
reg delete HKCR\IO.Practiceware.Interop.EventHandler /f > null
reg delete HKCR\IO.Practiceware.Interop.LogManager /f > null
reg delete HKCR\CLSID\{2AFDBB7E-80D8-4C88-BE9D-FE94D6BEB968} /f > null
reg delete HKCR\CLSID\{AC5B9762-C5D1-4104-99DD-8055777DBDDF} /f > null
reg delete HKCR\CLSID\{9D8C1A4F-D415-423C-BB5F-824ABFA4668A} /f > null
reg delete HKCR\TypeLib\{D6F6B5FE-B077-45BC-8DBE-3158FC011F32} /f > null
reg delete HKCR\Wow6432Node\IO.Practiceware.Interop.ComWrapper /f > null
reg delete HKCR\Wow6432Node\IO.Practiceware.Interop.EventHandler /f > null
reg delete HKCR\Wow6432Node\IO.Practiceware.Interop.LogManager /f > null
reg delete HKCR\Wow6432Node\CLSID\{2AFDBB7E-80D8-4C88-BE9D-FE94D6BEB968} /f > null
reg delete HKCR\Wow6432Node\CLSID\{AC5B9762-C5D1-4104-99DD-8055777DBDDF} /f > null
reg delete HKCR\Wow6432Node\CLSID\{9D8C1A4F-D415-423C-BB5F-824ABFA4668A} /f > null
reg delete HKCR\Wow6432Node\TypeLib\{D6F6B5FE-B077-45BC-8DBE-3158FC011F32} /f > null

reg delete HKCR\Interface\{0A9ABEB7-C95E-4377-B67A-2B96D2EA23A3} /f > null
reg delete HKCR\Interface\{0896D946-8A8B-4E7D-9D0D-BB29A52B5D08} /f > null
reg delete HKCR\Interface\{D61F6748-F0A3-49FC-8B39-CC1C8EEFD507} /f > null
reg delete HKCR\Interface\{2704B256-B8DB-3C62-B846-B02DD69B1AEC} /f > null
reg delete HKCR\Interface\{0A9ABEB7-C95E-4377-B67A-2B96D2EA23A3} /f > null
reg delete HKCR\Interface\{0896D946-8A8B-4E7D-9D0D-BB29A52B5D08} /f > null
reg delete HKCR\Interface\{D61F6748-F0A3-49FC-8B39-CC1C8EEFD507} /f > null
reg delete HKCR\Interface\{2704B256-B8DB-3C62-B846-B02DD69B1AEC} /f > null

echo Deleted Registrations

exit /b 0

:EndScriptNotInstalled
echo Please, install IO Practiceware Client Setup.exe first
pause
exit /b 0