Attribute VB_Name = "MServerLib"
Option Explicit
' Publicly accessible paths
Private sServerDir                      As String
' Publicly accessible databases
Private sPRProvider                     As String
Private sPRDataSource                   As String
Private sPRInitialCatalog               As String
Public sDFProvider                      As String
Public sDFDataSource                    As String
Private sDMProvider                     As String
Private sDMDataSource                   As String
' The way GblDevice is read is not compatible with newer IO versions
Private GblDevice                       As String
Private PinPointDirectory               As String
Private PinpointDatabaseOpen            As Boolean
Private mMyPracticeRepository           As AdoConnection
Private mMyPracticeRepositoryCmd        As ADODB.Command
Private tmTimer                         As Integer
Private dbConnection                    As String
Public MyDynamicForms As ADODB.Connection
Public MyDynamicFormsCmd As ADODB.Command
Public MyDiagnosisMaster As ADODB.Connection
Public MyDiagnosisMasterCmd As ADODB.Command
Private FileManager As VB6FileManager
Public LclDevice As String
Public LocalPinPointDirectory As String

Public emptyArgs() As Variant
Public Const dbConnectionAccess As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="

Public Property Get FM() As VB6FileManager
    If FileManager Is Nothing Then
        Set FileManager = New VB6FileManager
    End If
    Set FM = FileManager
End Property
Public Property Get CreateAdoConnection() As AdoConnection
Dim wrapper As New ComWrapper
    Set CreateAdoConnection = wrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
End Property

Public Property Get CreateAdoCommand() As ADODB.Command
Dim wrapper As New ComWrapper
    Set CreateAdoCommand = wrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
End Property
    
Public Property Get CreateAdoRecordset() As ADODB.Recordset
Dim wrapper As New ComWrapper
    Set CreateAdoRecordset = wrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance
End Property
Property Get MyPracticeRepository() As AdoConnection
    Set MyPracticeRepository = mMyPracticeRepository
End Property
Property Get MyPracticeRepositoryCmd() As AdoCommand
    Dim t As Integer
    If dbConnection <> "" Then
        Do While mMyPracticeRepository.State = 0
            mMyPracticeRepository.CursorLocation = adUseClient
            mMyPracticeRepository.Open dbConnection
            t = t + 1
            If t > 3 Then
                Err.Raise -5000, , "Cannot open closed SQL connection"
            End If
        Loop
        End If
    Set MyPracticeRepositoryCmd = mMyPracticeRepositoryCmd
End Property
Public Sub dbPinpointClose()

    MyPracticeRepository.Close
    MyDynamicForms.Close
    MyDiagnosisMaster.Close
    PinpointDatabaseOpen = False

End Sub

Public Sub dbPinpointOpen()

'Legacy support for continuous connections
    Dim sConnectString As String
    
    PinpointDatabaseOpen = False
    Set mMyPracticeRepository = CreateAdoConnection
    mMyPracticeRepository.IsAsyncExecuteEnabled = False
    Set mMyPracticeRepositoryCmd = CreateAdoCommand
    MyPracticeRepositoryCmd.ActiveConnection = MyPracticeRepository
    
    sConnectString = dbConnectionAccess & sDFDataSource & ";"
    Set MyDynamicForms = New ADODB.Connection
    MyDynamicForms.CursorLocation = adUseClient
    Call MyDynamicForms.Open(sConnectString)
    Set MyDynamicFormsCmd = New ADODB.Command
    MyDynamicFormsCmd.ActiveConnection = MyDynamicForms
    
    sConnectString = dbConnectionAccess & sDMDataSource & ";"
    Set MyDiagnosisMaster = New ADODB.Connection
    MyDiagnosisMaster.CursorLocation = adUseClient
    Call MyDiagnosisMaster.Open(sConnectString)
    Set MyDiagnosisMasterCmd = New ADODB.Command
    MyDiagnosisMasterCmd.ActiveConnection = MyDiagnosisMaster
    PinpointDatabaseOpen = True
Exit Sub

ldbPinpointOpen_Error:
    LogError "MServerLib", "dbPinpointOpen", Err, Err.Description

End Sub
Property Get ConnectionString() As String
    Dim wrapper As New ComWrapper
    ConnectionString = wrapper.InvokeStaticGet("IO.Practiceware.Configuration.ConfigurationManager", "PracticeRepositoryConnectionString")
End Property
Property Get GetRootDrive() As String
    On Error GoTo lGetRootDrive_Error
    Dim wrapper As New ComWrapper
    GetRootDrive = wrapper.InvokeStaticGet("IO.Practiceware.Configuration.ConfigurationManager", "ApplicationDataPath")
    If Not GetRootDrive = "" Then
        GetRootDrive = FM.GetParentFolder(GetRootDrive)
    End If

    Exit Property

lGetRootDrive_Error:
    LogError "CommonLib", "GetRootDrive", Err, Err.Description
End Property
Property Get GetServerDrive() As String
    On Error GoTo lGetServerDrive_Error

    Dim wrapper As New ComWrapper
    GetServerDrive = wrapper.InvokeStaticGet("IO.Practiceware.Configuration.ConfigurationManager", "ServerDataPath")
    If Not GetServerDrive = "" Then
        GetServerDrive = FM.GetParentFolder(GetServerDrive)
    End If
    
    Exit Property

lGetServerDrive_Error:
    LogError "CommonLib", "GetServerDrive", Err, Err.Description
End Property
Public Sub EstablishDirectory()

    On Error GoTo lEstablishDirectory_Error
    tmTimer = 5

    dbConnection = vbNullString
    LclDevice = GetRootDrive 'Local data path root
    If LclDevice = "" Then LclDevice = "C:"

    GblDevice = GetServerDrive 'Server DataPath
    If GblDevice = "" Then GblDevice = "G:"
    
    LocalPinPointDirectory = LclDevice + "\Pinpoint\"
    PinPointDirectory = GblDevice & "\Pinpoint\"

    ' Set db variables
    dbConnection = ConnectionString
    sDFDataSource = PinPointDirectory & "NewVersion\" & "DynamicForms.mdb"
    sDMDataSource = PinPointDirectory & "NewVersion\" & "DiagnosisMaster.mdb"

    Exit Sub
lEstablishDirectory_Error:
    LogError "MServerLib", "EstablishDirectory", Err.Number, Err.Description

End Sub

Public Function IsFileThere(sTheFile As String) As Boolean
Dim iMaxLen As Integer
Dim FilePath As String
    On Error GoTo lIsFileThere_Error

    IsFileThere = True
    iMaxLen = 5
    Dim wrapper As New ComWrapper
    
    FilePath = wrapper.Evaluate("ConfigurationManager.Configuration.FilePath")
    
    If sTheFile = FilePath Then
        iMaxLen = 1
    End If
    If FM.GetFileLength(sTheFile) < iMaxLen Then
        IsFileThere = False
        FM.Kill sTheFile
    End If

    Exit Function

lIsFileThere_Error:
    IsFileThere = False
End Function

Public Sub LogError(ByVal sModule As String, _
                    ByVal sMethod As String, _
                    ByVal iErr As Long, _
                    ByVal sDescription As String, _
                    Optional ByVal sExtraInfo As String = vbNullString)

Dim sErrorMessage As String
Dim sErrorLog     As String
Dim iFileNum      As Integer

    On Error Resume Next
    Err.Raise iErr
    sErrorLog = PinPointDirectory & "IOError.log"
    sErrorMessage = "Product: " & Trim$(App.ProductName) & vbTab & "Revision: " & Trim$(App.Revision) & vbTab & "Date: " & Now & vbTab & "Module: " & sModule & vbTab & "Method: " & sMethod & vbTab & "Number: " & Str$(Err.Number) & vbTab & "Description: " & sDescription & vbTab & "Details: " & sExtraInfo & vbTab
    iFileNum = FreeFile
    FM.OpenFile sErrorLog, FileOpenMode.Append, FileAccess.WriteShared, CLng(iFileNum)
    Print #iFileNum, sErrorMessage
    FM.CloseFile CLng(iFileNum)
    Err.Clear
    On Error GoTo 0

End Sub


Public Sub LogDBError(ByVal sModule As String, _
                    ByVal sMethod As String, _
                    ByVal iErr As Long, _
                    ByVal sDescription As String, _
                    Optional ByVal sExtraInfo As String = vbNullString)

Dim sErrorMessage As String
Dim sErrorLog     As String
Dim iFileNum      As Integer

    On Error Resume Next
    Err.Raise iErr
    sErrorLog = PinPointDirectory & "PracticeRepositoryError.log"
    sErrorMessage = "Product: " & Trim$(App.ProductName) & vbTab & "Revision: " & Trim$(App.Revision) & vbTab & "Date: " & Now & vbTab & "Module: " & sModule & vbTab & "Method: " & sMethod & vbTab & "Number: " & Str$(Err.Number) & vbTab & "Description: " & sDescription & vbTab & "Details: " & sExtraInfo & vbTab
    iFileNum = FreeFile
    FM.OpenFile sErrorLog, FileOpenMode.Append, FileAccess.WriteShared, CLng(iFileNum)
    Print #iFileNum, sErrorMessage
    FM.CloseFile CLng(iFileNum)
    Err.Clear
    On Error GoTo 0

End Sub
