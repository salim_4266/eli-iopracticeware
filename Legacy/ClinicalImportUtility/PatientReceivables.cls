VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientReceivables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Receivables Object Module
Option Explicit
Public PatientId                        As Long
Public InsurerId                        As Long
Public InsuredId                        As Long
Public AppointmentId                    As Long
Public ReceivableId                     As Long
Public ReceivableType                   As String
Public ReceivableInvoice                As String
Public ReceivableInvoiceDate            As String
Public ReceivableAmount                 As Single
Public ReceivableFeeAmount              As Single
Public ReceivableBalance                As Single
Public ReceivableUnAllocated            As Single
Public ReceivableBillToDr               As Long
Public ReceivableRefDr                  As Long
Public ReceivableAccidentType           As String
Public ReceivableAccidentState          As String
Public ReceivableHospitalAdmission      As String
Public ReceivableHospitalDismissal      As String
Public ReceivableDisability             As String
Public ReceivableRsnType                As String
Public ReceivableRsnDate                As String
Public ReceivableConsDate               As String
Public ReceivablePrevCond               As String
Public ReceivableExternalRefInfo        As String
Public ReceivableOver90                 As String
Public ReceivableWriteOff               As String
Public ReceivableBillOffice             As Long
Public ReceivablePatientBillDate        As String
Public ReceivableLastPayDate            As String
Public ReceivableInsurerDesignation     As String
Public ReceivableStatus                 As String
Private ModuleName                      As String
Private PatientReceivableTotal          As Long
Private PatientReceivableTbl            As ADODB.Recordset
Private PatientReceivableNew            As Boolean

Private Sub Class_Initialize()

    Set PatientReceivableTbl = CreateAdoRecordset
    InitPatientReceivable

End Sub

Private Sub Class_Terminate()

    InitPatientReceivable
    Set PatientReceivableTbl = Nothing

End Sub

Public Function FindPatientReceivable() As Long

    FindPatientReceivable = GetReceivables(0)

End Function

Private Function GetReceivables(ByVal IType As Integer) As Long

Dim OrderString As String
Dim Srt         As String
Dim Ref         As String

    On Error GoTo lGetReceivables_Error

    Srt = vbNullString
    Ref = vbNullString
    PatientReceivableTotal = 0
    GetReceivables = -1
    If PatientId < 1 Then
        If AppointmentId < 1 Then
            If LenB(Trim$(ReceivableInvoice)) = 0 Then
                Exit Function
            End If
        End If
    End If
    OrderString = "Select * FROM PatientReceivables WHERE "
    If PatientId > 0 Then
        OrderString = OrderString + Ref & "PatientId =" & Str$(PatientId) & " "
        Ref = "And "
        Srt = "ORDER BY InsurerDesignation DESC, InvoiceDate DESC, Invoice DESC "
        If IType = 1 Then
            Srt = "ORDER BY Invoice DESC, InvoiceDate DESC "
        End If
    End If
    If AppointmentId > 0 Then
        OrderString = OrderString + Ref & "AppointmentId =" & Str$(AppointmentId) & " "
        Ref = "And "
        Srt = "ORDER BY PatientId ASC, AppointmentId DESC, InsurerDesignation DESC "
    End If
    If LenB(Trim$(ReceivableInvoice)) Then
        If Len(ReceivableInvoice) = 1 Then
            OrderString = OrderString + Ref & "Invoice >= '" & Trim$(ReceivableInvoice) & "' "
            Srt = "ORDER BY PatientId ASC, Invoice DESC, InsurerDesignation DESC, ReceivableId ASC "
        Else 'NOT LEN(RECEIVABLEINVOICE)...
            OrderString = OrderString + Ref & "Invoice = '" & Trim$(ReceivableInvoice) & "' "
            Srt = "ORDER BY InsurerDesignation DESC, ReceivableId ASC, PatientId ASC, Invoice DESC"
        End If
        Ref = "And "
    End If
    If LenB(Trim$(ReceivableType)) Then
        OrderString = OrderString + Ref & "ReceivableType = '" & Trim$(ReceivableType) & "' "
        Ref = "And "
        Srt = "ORDER BY PatientId ASC, AppointmentId DESC, InsurerDesignation DESC "
    End If
    If LenB(Trim$(ReceivableInsurerDesignation)) Then
        OrderString = OrderString + Ref & "InsurerDesignation = '" & Trim$(ReceivableInsurerDesignation) & "' "
        Ref = "And "
        Srt = "ORDER BY PatientId ASC, AppointmentId DESC "
    End If
    If LenB(Trim$(ReceivableStatus)) Then
        OrderString = OrderString + Ref & "Status = '" & Trim$(ReceivableStatus) & "' "
        Ref = "And "
        Srt = "ORDER BY PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC, ReceivableId ASC "
    End If
    If LenB(Trim$(ReceivableInvoiceDate)) Then
        OrderString = OrderString + Ref & "InvoiceDate = '" & Trim$(ReceivableInvoiceDate) & "' "
        Ref = "And "
        Srt = "ORDER BY PatientId ASC, InvoiceDate DESC, InsurerDesignation DESC "
    End If
    If InsurerId > 0 Then
        OrderString = OrderString + Ref & "InsurerId =" & Str$(InsurerId) & " "
        Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC "
        Ref = "And "
    End If
    If InsuredId > 0 Then
        OrderString = OrderString + Ref & "InsuredId =" & Str$(InsuredId) & " "
        If InsuredId <> PatientId Then
            Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC "
        End If
        Ref = "And "
    End If
    If ReceivableBalance > 0 Then
        If InsurerId > 0 Then
            OrderString = OrderString + Ref & "OpenBalance <> 0 "
            Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC "
            Ref = "And "
        End If
    End If
    If IType = -9 Then
        OrderString = OrderString + Ref & "((OpenBalance > 0.01) Or (OpenBalance < -0.01)) "
        Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC "
        Ref = "And "
    End If
    OrderString = OrderString + Srt
    Set PatientReceivableTbl = Nothing
    Set PatientReceivableTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PatientReceivableTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PatientReceivableTbl.EOF Then
        GetReceivables = -1
    Else
        With PatientReceivableTbl
            .MoveLast
            PatientReceivableTotal = .RecordCount
            GetReceivables = .RecordCount
        End With
    End If

    Exit Function

lGetReceivables_Error:

    LogDBError "PatientReceivables", "GetReceivables", Err, Err.Description

End Function

Private Sub InitPatientReceivable()

    PatientReceivableNew = True
    PatientId = 0
    InsurerId = 0
    InsuredId = 0
    AppointmentId = 0
    ReceivableType = vbNullString
    ReceivableInvoice = vbNullString
    ReceivableInvoiceDate = vbNullString
    ReceivableAmount = 0
    ReceivableFeeAmount = 0
    ReceivableBalance = 0
    ReceivableUnAllocated = 0
    ReceivableBillToDr = 0
    ReceivableRefDr = 0
    ReceivableAccidentType = vbNullString
    ReceivableAccidentState = vbNullString
    ReceivableHospitalAdmission = vbNullString
    ReceivableHospitalDismissal = vbNullString
    ReceivableDisability = vbNullString
    ReceivableRsnType = vbNullString
    ReceivableRsnDate = vbNullString
    ReceivableConsDate = vbNullString
    ReceivablePrevCond = vbNullString
    ReceivableExternalRefInfo = vbNullString
    ReceivableOver90 = vbNullString
    ReceivableBillOffice = 0
    ReceivablePatientBillDate = vbNullString
    ReceivableLastPayDate = vbNullString
    ReceivableInsurerDesignation = vbNullString
    ReceivableStatus = "A"

End Sub

Private Sub LoadPatientReceivable()

    PatientReceivableNew = False
    If LenB(PatientReceivableTbl("ReceivableId")) Then
        ReceivableId = PatientReceivableTbl("ReceivableId")
    Else
        ReceivableId = 0
    End If
    If LenB(PatientReceivableTbl("PatientId")) Then
        PatientId = PatientReceivableTbl("PatientId")
    Else
        PatientId = 0
    End If
    If LenB(PatientReceivableTbl("AppointmentId")) Then
        AppointmentId = PatientReceivableTbl("AppointmentId")
    Else
        AppointmentId = 0
    End If
    If LenB(PatientReceivableTbl("InsurerId")) Then
        InsurerId = PatientReceivableTbl("InsurerId")
    Else
        InsurerId = 0
    End If
    If LenB(PatientReceivableTbl("InsuredId")) Then
        InsuredId = PatientReceivableTbl("InsuredId")
    Else
        InsuredId = 0
    End If
    If LenB(PatientReceivableTbl("ReceivableType")) Then
        ReceivableType = PatientReceivableTbl("ReceivableType")
    Else
        ReceivableType = vbNullString
    End If
    If LenB(PatientReceivableTbl("Invoice")) Then
        ReceivableInvoice = PatientReceivableTbl("Invoice")
    Else
        ReceivableInvoice = vbNullString
    End If
    If LenB(PatientReceivableTbl("InvoiceDate")) Then
        ReceivableInvoiceDate = PatientReceivableTbl("InvoiceDate")
    Else
        ReceivableInvoiceDate = vbNullString
    End If
    If LenB(PatientReceivableTbl("PatientBillDate")) Then
        ReceivablePatientBillDate = PatientReceivableTbl("PatientBillDate")
    Else
        ReceivablePatientBillDate = vbNullString
    End If
    If LenB(PatientReceivableTbl("Charge")) Then
        ReceivableAmount = PatientReceivableTbl("Charge")
    Else
        ReceivableAmount = 0
    End If
    If LenB(PatientReceivableTbl("ChargesByFee")) Then
        ReceivableFeeAmount = PatientReceivableTbl("ChargesByFee")
    Else
        ReceivableFeeAmount = 0
    End If
    If LenB(PatientReceivableTbl("OpenBalance")) Then
        ReceivableBalance = PatientReceivableTbl("OpenBalance")
    Else
        ReceivableBalance = 0
    End If
    If LenB(PatientReceivableTbl("UnallocatedBalance")) Then
        ReceivableUnAllocated = PatientReceivableTbl("UnallocatedBalance")
    Else
        ReceivableUnAllocated = 0
    End If
    If LenB(PatientReceivableTbl("BillToDr")) Then
        ReceivableBillToDr = PatientReceivableTbl("BillToDr")
    Else
        ReceivableBillToDr = 0
    End If
    If LenB(PatientReceivableTbl("ReferDr")) Then
        ReceivableRefDr = PatientReceivableTbl("ReferDr")
    Else
        ReceivableRefDr = 0
    End If
    If LenB(PatientReceivableTbl("AccType")) Then
        ReceivableAccidentType = PatientReceivableTbl("AccType")
    Else
        ReceivableAccidentType = vbNullString
    End If
    If LenB(PatientReceivableTbl("AccState")) Then
        ReceivableAccidentState = PatientReceivableTbl("AccState")
    Else
        ReceivableAccidentState = vbNullString
    End If
    If LenB(PatientReceivableTbl("HspAdmDate")) Then
        ReceivableHospitalAdmission = PatientReceivableTbl("HspAdmDate")
    Else
        ReceivableHospitalAdmission = vbNullString
    End If
    If LenB(PatientReceivableTbl("HspDisDate")) Then
        ReceivableHospitalDismissal = PatientReceivableTbl("HspDisDate")
    Else
        ReceivableHospitalDismissal = vbNullString
    End If
    If LenB(PatientReceivableTbl("Disability")) Then
        ReceivableDisability = PatientReceivableTbl("Disability")
    Else
        ReceivableDisability = vbNullString
    End If
    If LenB(PatientReceivableTbl("RsnType")) Then
        ReceivableRsnType = PatientReceivableTbl("RsnType")
    Else
        ReceivableRsnType = vbNullString
    End If
    If LenB(PatientReceivableTbl("RsnDate")) Then
        ReceivableRsnDate = PatientReceivableTbl("RsnDate")
    Else
        ReceivableRsnDate = vbNullString
    End If
    If LenB(PatientReceivableTbl("FirstConsDate")) Then
        ReceivableConsDate = PatientReceivableTbl("FirstConsDate")
    Else
        ReceivableConsDate = vbNullString
    End If
    If LenB(PatientReceivableTbl("PrevCond")) Then
        ReceivablePrevCond = PatientReceivableTbl("PrevCond")
    Else
        ReceivablePrevCond = vbNullString
    End If
    If LenB(PatientReceivableTbl("ExternalRefInfo")) Then
        ReceivableExternalRefInfo = PatientReceivableTbl("ExternalRefInfo")
    Else
        ReceivableExternalRefInfo = vbNullString
    End If
    If LenB(PatientReceivableTbl("Over90")) Then
        ReceivableOver90 = PatientReceivableTbl("Over90")
    Else
        ReceivableOver90 = vbNullString
    End If
    If LenB(PatientReceivableTbl("WriteOff")) Then
        ReceivableWriteOff = PatientReceivableTbl("WriteOff")
    Else
        ReceivableWriteOff = vbNullString
    End If
    If LenB(PatientReceivableTbl("BillingOffice")) Then
        ReceivableBillOffice = PatientReceivableTbl("BillingOffice")
    Else
        ReceivableBillOffice = 0
    End If
    If LenB(PatientReceivableTbl("LastPayDate")) Then
        ReceivableLastPayDate = PatientReceivableTbl("LastPayDate")
    Else
        ReceivableLastPayDate = vbNullString
    End If
    If LenB(PatientReceivableTbl("InsurerDesignation")) Then
        ReceivableInsurerDesignation = PatientReceivableTbl("InsurerDesignation")
    Else
        ReceivableInsurerDesignation = vbNullString
    End If
    If LenB(PatientReceivableTbl("Status")) Then
        ReceivableStatus = PatientReceivableTbl("Status")
    Else
        ReceivableStatus = vbNullString
    End If

End Sub

Public Function SelectPatientReceivable(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectPatientReceivable_Error

    SelectPatientReceivable = False
    If PatientReceivableTbl.EOF Or (ListItem < 1) Or (ListItem > PatientReceivableTotal) Then
        Exit Function
    End If
    PatientReceivableTbl.MoveFirst
    PatientReceivableTbl.Move ListItem - 1
    SelectPatientReceivable = True
    LoadPatientReceivable

    Exit Function

lSelectPatientReceivable_Error:

    LogDBError "PatientReceivables", "SelectPatientReceivable", Err, Err.Description

End Function

