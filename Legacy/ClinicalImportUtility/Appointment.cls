VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAppointment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Scheduler Appointment Object Module
Option Explicit
Public ResourceId                  As Long
Public AppointmentId               As Long
Public AppointmentTypeId           As Long
Public AppointmentPatientId        As Long
Public AppointmentReferralId       As Long
Public AppointmentDate             As String
Public AppointmentTime             As Long
Public AppointmentStatus           As String
Public AppointmentDuration         As Long
Public AppointmentTypeTech         As Long
Public AppointmentComments         As String
Public AppointmentResourceId1      As Long
Public AppointmentResourceId2      As Long
Public AppointmentResourceId3      As Long
Public AppointmentResourceId4      As Long
Public AppointmentResourceId5      As Long
Public AppointmentResourceId6      As Long
Public AppointmentResourceId7      As Long
Public AppointmentResourceId8      As Long
Public AppointmentPreCertId        As Long
Public AppointmentInsType          As String
Public AppointmentConfirmStatus    As String
Public AppointmentTypeCategory     As String
Public AppointmentSetDate          As String
Public AppointmentVisitReason      As String
Private SchedulerApptTotal         As Long
Private ModuleName                 As String
Private SchedulerAppointmentTbl    As ADODB.Recordset
Private SchedulerAppointmentNew    As Boolean

Private Sub Class_Initialize()

    Set SchedulerAppointmentTbl = CreateAdoRecordset
    Call InitAppointment
End Sub

Private Sub Class_Terminate()

    Call InitAppointment
    Set SchedulerAppointmentTbl = Nothing
End Sub

Private Sub InitAppointment()

    SchedulerAppointmentNew = True
    AppointmentTypeId = 0
    AppointmentPatientId = 0
    AppointmentReferralId = 0
    AppointmentDate = ""
    AppointmentTime = 0
    AppointmentDuration = 0
    AppointmentTypeTech = 0
    AppointmentComments = ""
    AppointmentResourceId1 = 0
    AppointmentResourceId2 = 0
    AppointmentResourceId3 = 0
    AppointmentResourceId4 = 0
    AppointmentResourceId5 = 0
    AppointmentResourceId6 = 0
    AppointmentResourceId7 = 0
    AppointmentResourceId8 = 0
    AppointmentStatus = "P"
    AppointmentInsType = "M"
    AppointmentConfirmStatus = ""
    AppointmentTypeCategory = ""
    AppointmentSetDate = ""
    AppointmentVisitReason = ""
End Sub

Private Sub LoadAppointment()

    SchedulerAppointmentNew = False
    If (SchedulerAppointmentTbl("AppointmentId") <> "") Then
        AppointmentId = SchedulerAppointmentTbl("AppointmentId")
    Else
        AppointmentId = 0
    End If
    If (SchedulerAppointmentTbl("PatientId") <> "") Then
        AppointmentPatientId = SchedulerAppointmentTbl("PatientId")
    Else
        AppointmentPatientId = 0
    End If
    If (SchedulerAppointmentTbl("ReferralId") <> "") Then
        AppointmentReferralId = SchedulerAppointmentTbl("ReferralId")
    Else
        AppointmentReferralId = 0
    End If
    If (SchedulerAppointmentTbl("PreCertId") <> "") Then
        AppointmentPreCertId = SchedulerAppointmentTbl("PreCertId")
    Else
        AppointmentPreCertId = 0
    End If
    If (SchedulerAppointmentTbl("AppTypeId") <> "") Then
        AppointmentTypeId = SchedulerAppointmentTbl("AppTypeId")
    Else
        AppointmentTypeId = 0
    End If
    If (SchedulerAppointmentTbl("AppDate") <> "") Then
        AppointmentDate = SchedulerAppointmentTbl("AppDate")
    Else
        AppointmentDate = ""
    End If
    If (SchedulerAppointmentTbl("AppTime") <> "") Then
        AppointmentTime = SchedulerAppointmentTbl("AppTime")
    Else
        AppointmentTime = 0
    End If
    If (SchedulerAppointmentTbl("Duration") <> "") Then
        AppointmentDuration = SchedulerAppointmentTbl("Duration")
    Else
        AppointmentDuration = 0
    End If
    If (SchedulerAppointmentTbl("TechApptTypeId") <> "") Then
        AppointmentTypeTech = SchedulerAppointmentTbl("TechApptTypeId")
    Else
        AppointmentTypeTech = 0
    End If
    If (SchedulerAppointmentTbl("ScheduleStatus") <> "") Then
        AppointmentStatus = SchedulerAppointmentTbl("ScheduleStatus")
    Else
        AppointmentStatus = ""
    End If
    If (SchedulerAppointmentTbl("ConfirmStatus") <> "") Then
        AppointmentConfirmStatus = SchedulerAppointmentTbl("ConfirmStatus")
    Else
        AppointmentConfirmStatus = ""
    End If
    If (SchedulerAppointmentTbl("Comments") <> "") Then
        AppointmentComments = SchedulerAppointmentTbl("Comments")
    Else
        AppointmentComments = ""
    End If
    If (SchedulerAppointmentTbl("ResourceId1") <> "") Then
        AppointmentResourceId1 = SchedulerAppointmentTbl("ResourceId1")
    Else
        AppointmentResourceId1 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId2") <> "") Then
        AppointmentResourceId2 = SchedulerAppointmentTbl("ResourceId2")
    Else
        AppointmentResourceId2 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId3") <> "") Then
        AppointmentResourceId3 = SchedulerAppointmentTbl("ResourceId3")
    Else
        AppointmentResourceId3 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId4") <> "") Then
        AppointmentResourceId4 = SchedulerAppointmentTbl("ResourceId4")
    Else
        AppointmentResourceId4 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId5") <> "") Then
        AppointmentResourceId5 = SchedulerAppointmentTbl("ResourceId5")
    Else
        AppointmentResourceId5 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId6") <> "") Then
        AppointmentResourceId6 = SchedulerAppointmentTbl("ResourceId6")
    Else
        AppointmentResourceId6 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId7") <> "") Then
        AppointmentResourceId7 = SchedulerAppointmentTbl("ResourceId7")
    Else
        AppointmentResourceId7 = 0
    End If
    If (SchedulerAppointmentTbl("ResourceId8") <> "") Then
        AppointmentResourceId8 = SchedulerAppointmentTbl("ResourceId8")
    Else
        AppointmentResourceId8 = 0
    End If
    If (SchedulerAppointmentTbl("ApptInsType") <> "") Then
        AppointmentInsType = SchedulerAppointmentTbl("ApptInsType")
    Else
        AppointmentInsType = "M"
    End If
    If (SchedulerAppointmentTbl("ApptTypeCat") <> "") Then
        AppointmentTypeCategory = SchedulerAppointmentTbl("ApptTypeCat")
    Else
        AppointmentTypeCategory = ""
    End If
    If (SchedulerAppointmentTbl("SetDate") <> "") Then
        AppointmentSetDate = SchedulerAppointmentTbl("SetDate")
    Else
        AppointmentSetDate = ""
    End If
    If (SchedulerAppointmentTbl("VisitReason") <> "") Then
        AppointmentVisitReason = SchedulerAppointmentTbl("VisitReason")
    Else
        AppointmentVisitReason = ""
    End If
End Sub

Private Function GetAppointment() As Long
Dim j           As Integer
Dim Ref         As String
Dim PRef        As String
Dim OrderString As String

    On Error GoTo lGetAppointment_Error

    SchedulerApptTotal = 0
    GetAppointment = -1
    OrderString = "Select * FROM Appointments WHERE PatientId =" & Str$(AppointmentPatientId) & " "
    If (AppointmentStatus <> "") Then
        If (Len(AppointmentStatus) = 1) Then
            OrderString = OrderString & "And ScheduleStatus = '" & UCase$(AppointmentStatus) & "' "
        Else
            Ref = " Or "
            PRef = "="
            If (Left$(AppointmentStatus, 1) = "-") Then
                PRef = "<>"
                Ref = " And "
                AppointmentStatus = Mid$(AppointmentStatus, 2, Len(AppointmentStatus) - 1)
            End If
            For j = 1 To Len(AppointmentStatus)
                If (j = 1) Then
                    OrderString = OrderString & "And ("
                    OrderString = OrderString & "ScheduleStatus" & PRef & "'" & UCase$(Mid$(AppointmentStatus, j, 1)) & " ' "
                ElseIf (j = Len(AppointmentStatus)) Then
                    OrderString = OrderString + Ref & "ScheduleStatus" & PRef & "'" & UCase$(Mid$(AppointmentStatus, j, 1)) & " ' "
                Else
                    OrderString = OrderString + Ref & "ScheduleStatus" & PRef & "'" & UCase$(Mid$(AppointmentStatus, j, 1)) & " ' "
                End If
            Next j
        End If
    End If
    OrderString = OrderString & " ORDER BY AppDate DESC, AppTime ASC "
    Set SchedulerAppointmentTbl = Nothing
    Set SchedulerAppointmentTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call SchedulerAppointmentTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (SchedulerAppointmentTbl.EOF) Then
        GetAppointment = -1
    Else
        SchedulerAppointmentTbl.MoveLast
        SchedulerApptTotal = SchedulerAppointmentTbl.RecordCount
        GetAppointment = SchedulerAppointmentTbl.RecordCount
    End If

    Exit Function

lGetAppointment_Error:

    LogDBError "CAppointment", "GetAppointment", Err, Err.Description
    
End Function

Private Function GetSchedulerAppointment() As Boolean
Dim OrderString As String

    On Error GoTo lGetSchedulerAppointment_Error

    GetSchedulerAppointment = True
    OrderString = "SELECT * FROM Appointments WHERE AppointmentId =" & Str$(AppointmentId) & " "
    Set SchedulerAppointmentTbl = Nothing
    Set SchedulerAppointmentTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call SchedulerAppointmentTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (SchedulerAppointmentTbl.EOF) Then
        Call InitAppointment
    Else
        Call LoadAppointment
    End If

    Exit Function

lGetSchedulerAppointment_Error:

    LogDBError "CAppointment", "GetSchedulerAppointment", Err, Err.Description

End Function

Public Function RetrieveSchedulerAppointment() As Boolean

    RetrieveSchedulerAppointment = GetSchedulerAppointment
End Function

Public Function SelectAppointment(ListItem As Long) As Boolean
    On Error GoTo lSelectAppointment_Error
    
    If Not SchedulerAppointmentTbl.EOF Then
        If ListItem > 0 Then
            If ListItem <= SchedulerApptTotal Then
                SchedulerAppointmentTbl.Move ListItem - SchedulerAppointmentTbl.AbsolutePosition
                LoadAppointment
                SelectAppointment = True
            End If
        End If
    End If
    Exit Function

lSelectAppointment_Error:

    LogDBError "CAppointment", "SelectAppointment", Err, Err.Description
End Function

Public Function FindAppointment() As Long

    FindAppointment = GetAppointment()
End Function
