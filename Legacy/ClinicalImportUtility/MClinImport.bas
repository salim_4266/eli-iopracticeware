Attribute VB_Name = "MClinImport"
Option Explicit
Private Const PROC_FILE        As String = "ProcList.txt"
Private sAppFolder             As String
Private ProcedureDictionary    As New Scripting.Dictionary
Private Type TInvoiceConvert '1-PatId, 2-ApptId, 3-Proc, 4-RMod, 5-LMod
    iPatId                         As Long
    iApptId                        As Long
    sProc                          As String
    fRMod                          As Boolean
    fLMod                          As Boolean
End Type

Private Function GetEyeControls(ByVal ClsId As Long, _
                                ByVal AQues As String, _
                                REyeId As Long, _
                                LEyeId As Long, _
                                ModId As Long, _
                                REyeName As String, _
                                LEyeName As String, _
                                ModName As String) As Boolean

Dim i       As Integer
Dim RetFrm  As DynamicForms
Dim RetCtrl As DynamicControls

    On Error GoTo lGetEyeControls_Error

    GetEyeControls = False
    REyeId = 0
    REyeName = vbNullString
    LEyeId = 0
    LEyeName = vbNullString
    ModId = 0
    ModName = vbNullString
    If ClsId > 0 Then
        If LenB(Trim$(AQues)) Then
            Set RetFrm = New DynamicForms
            RetFrm.Question = AQues
            If RetFrm.RetrieveForm Then
                If RetFrm.SelectForm(1) Then
                    Set RetCtrl = New DynamicControls
                    RetCtrl.FormId = RetFrm.FormId
                    If RetCtrl.FindControl > 0 Then
                        i = 1
                        With RetCtrl
                            Do Until Not (.SelectControl(i))
                                If .TabIndex = 2 Then
                                    If .ControlType <> "X" Then
                                        REyeId = .ControlId
                                        REyeName = .ControlName
                                        GetEyeControls = True
                                    End If
                                End If
                                If .TabIndex = 3 Then
                                    If .ControlType <> "X" Then
                                        LEyeId = .ControlId
                                        LEyeName = .ControlName
                                        GetEyeControls = True
                                    End If
                                End If
                                If .TabIndex = 99 Then
                                    ModId = .ControlId
                                    ModName = .ControlName
                                    GetEyeControls = True
                                End If
                                i = i + 1
                            Loop
                        End With 'RetCtrl
                    End If
                    Set RetCtrl = Nothing
                End If
            End If
            Set RetFrm = Nothing
        End If
    End If

    Exit Function

lGetEyeControls_Error:

    LogError "MClinImport", "GetEyeControls", Err, Err.Description

End Function

Private Function IsThisFRecord(ByVal ACpt As String, _
                               AQues As String) As Integer

Dim i      As Integer
Dim RetCls As DynamicClass

    On Error GoTo lIsThisFRecord_Error

    IsThisFRecord = 0
    AQues = vbNullString
    If LenB(Trim$(ACpt)) Then
        Set RetCls = New DynamicClass
        RetCls.QuestionParty = "T"
        If RetCls.FindClassForms > 0 Then
            i = 1
            Do Until Not (RetCls.SelectClassForm(i))
                If LenB(Trim$(RetCls.ControlName)) Then
                    If (InStr(1, RetCls.ControlName, ACpt) <> 0) Then
                        AQues = RetCls.Question
                        IsThisFRecord = RetCls.ClassId
                        Exit Do
                    End If
                End If
                i = i + 1
            Loop
        End If
        Set RetCls = Nothing
    End If

    Exit Function

lIsThisFRecord_Error:

    LogError "MClinImport", "IsThisFRecord", Err, Err.Description

End Function

Private Sub LogResultsCI(ByVal sMessage As String)

Dim iFile       As Integer
Dim sResultsLog As String
Dim FileNum As Integer

    On Error GoTo lLogResultsCI_Error
    iFile = FreeFile
    sResultsLog = sAppFolder & "ClinOutput\b2cresults" & Trim$(Str$(Format$(Now, "yyyymmDd"))) & ".txt"
    FileNum = FreeFile
    FM.OpenFile sResultsLog, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
    Print #FileNum, sMessage
    FM.CloseFile CLng(FileNum)

Exit Sub

lLogResultsCI_Error:
    LogError "MClinImport", "LogResultsCI", Err, Err.Description, sResultsLog

End Sub

Public Sub Main()

Dim PatId         As Long
Dim Proc          As String
Dim ApptId        As Long
Dim PrevAppt      As Long
Dim p             As Long
Dim d             As Long
Dim MaxProcs      As Long
Dim Mods          As String
Dim aInvoices(25) As TInvoiceConvert   '1-PatId, 2-ApptId, 3-Proc, 4-RMod, 5-LMod
Dim InvCnt        As Long
Dim InProc        As String
Dim fDoneInvoice  As Boolean
Dim RetPRS        As PatientReceivableService
Dim FileNum As Integer
    On Error GoTo lMain_Error
    EstablishDirectory
    sAppFolder = LocalPinPointDirectory & "\ClinImport\"
    If Not AutoLogin Then
        Err.Raise vbError, , "Failed to login into Software."
    End If
    If (Not (IsFileThere(sAppFolder & PROC_FILE))) Then
        Err.Raise vbError, , "Missing " & PROC_FILE & "  This file can be generated by using the AdminUtilities software."
    End If
    dbPinpointOpen
'Code Fixer Ignore Threat OK by dev 11/4/2010 11:59:38 AM
    FM.OpenFile sAppFolder & PROC_FILE, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(101)
    MaxProcs = 1
    Do While Not (EOF(101))
        Line Input #101, InProc
        If (InStr(Trim$(InProc), "::")) Then
            ProcedureDictionary.Add Split(Trim$(InProc), "::")(0), Array(Split(Trim$(InProc), "::")(1), Split(Trim$(InProc), "::")(2))
        Else 'NOT (INSTR(TRIM$(INPROC),...
            ProcedureDictionary.Add Trim$(InProc), Null
        End If
        MaxProcs = MaxProcs + 1
    Loop
    FM.CloseFile CLng(101)
    
'Code Fixer Ignore Threat OK by dev 11/4/2010 12:00:27 PM
    LogResultsCI "Start - " & Trim$(Format$(Now, "hh:nn:ss"))
    Set RetPRS = New PatientReceivableService
    If RetPRS.FindPatientReceivableServiceActive > 0 Then
        d = 1
        Do Until Not (RetPRS.SelectPatientReceivableServiceAggregate(d))
            InvCnt = 1
            fDoneInvoice = False
            PrevAppt = 0
            Erase aInvoices
            Do While Not fDoneInvoice
                If LenB(RetPRS.Invoice) Then
                    If RetPRS.SelectPatientReceivableServiceAggregate(d) Then
                        p = InStr(1, RetPRS.Invoice, "-")
                        If p > 0 Then
                            PatId = Trim$(Mid$(RetPRS.Invoice, 1, p - 1))
                            ApptId = Trim$(Mid$(RetPRS.Invoice, p + 1, Len(RetPRS.Invoice) - p))
                            If ApptId = PrevAppt Or PrevAppt = 0 Then
                                Proc = Trim$(RetPRS.Service)
                                If ProcedureDictionary.Exists(Proc) Then
                                    Mods = Trim$(RetPRS.ServiceModifier)
                                    If Trim$(RetPRS.ServiceStatus) <> "X" Then
                                        With aInvoices(InvCnt)
                                            .iPatId = PatId
                                            .iApptId = ApptId
                                            .sProc = Proc
                                        End With 'aInvoices(InvCnt)
                                        aInvoices(InvCnt).fRMod = (InStr(1, Mods, "RT"))
                                        aInvoices(InvCnt).fLMod = (InStr(1, Mods, "LT"))
                                        'Code Fixer Ignore Threat OK by dev 11/4/2010 12:00:56 PM
                                        LogResultsCI ("Inv " & Trim$(Str$(aInvoices(InvCnt).iPatId)) & "-" & Trim$(Str$(aInvoices(InvCnt).iApptId)) & " - svc " & Trim$(aInvoices(InvCnt).sProc) & " - mod " & Trim$(Mods))
                                        InvCnt = InvCnt + 1
                                    End If
                                End If
                            Else 'NOT (APPTID...
                                fDoneInvoice = True
                            End If
                        End If
                    End If
                End If
                If Not fDoneInvoice Then
                    If Not (RetPRS.SelectPatientReceivableServiceAggregate(d)) Then
                        fDoneInvoice = True
                    End If
                    PrevAppt = ApptId
                End If
                d = d + 1
            Loop
            If aInvoices(1).iPatId > 0 Then
                If Not IsClaimManual(aInvoices(1).iApptId) Then
                    PostArray aInvoices()
                Else
                    LogResultsCI "Inv " & Trim$(Str$(aInvoices(1).iPatId)) & "-" & Trim$(Str$(aInvoices(1).iApptId)) & " is a manual claim.  No posting done."
                End If
            End If
            d = d - 1
        Loop
    End If
    LogResultsCI "END - " & Trim$(Format$(Now, "hh:nn:ss"))
    dbPinpointClose

Exit Sub

lMain_Error:
    LogError "MClinImport", "Main", Err, Err.Description

End Sub

Public Sub PostArray(aInvoices() As TInvoiceConvert)

Dim iCount   As Long
Dim iRange   As Long
Dim Clinical As PatientClinical

    On Error GoTo lPostArray_Error
    'Check if invoice exists already
    Set Clinical = New PatientClinical
    Clinical.PatientId = aInvoices(1).iPatId
    Clinical.AppointmentId = aInvoices(1).iApptId
    Clinical.ClinicalType = "F"
    If Clinical.FindPatientClinical < 1 Then
        For iCount = 1 To UBound(aInvoices) - 1
            If (aInvoices(iCount).sProc = aInvoices(iCount + 1).sProc) Then
                If aInvoices(iCount).fRMod And Not aInvoices(iCount).fLMod Then
                    If Not aInvoices(iCount + 1).fRMod And aInvoices(iCount + 1).fLMod Then
                        aInvoices(iCount).fLMod = True
                        aInvoices(iCount + 1).sProc = ""
                    End If
                End If
                If Not aInvoices(iCount).fRMod And aInvoices(iCount).fLMod Then
                    If aInvoices(iCount + 1).fRMod And Not aInvoices(iCount + 1).fLMod Then
                        aInvoices(iCount).fRMod = False
                        aInvoices(iCount + 1).sProc = ""
                    End If
                End If
            End If
        Next iCount
        For iCount = 1 To UBound(aInvoices) - 1
            With aInvoices(iCount)
                If LenB(.sProc) > 0 Then
                    If PostExamElement(.iPatId, .iApptId, .sProc, .fRMod, .fLMod) Then
                        iRange = PostOpRange(.iApptId, .sProc)
                        If iRange > 0 Then
                            PostPRecord .iPatId, .iApptId, .sProc, iRange
                        End If
                    End If
                End If
            End With 'AINVOICES(ICOUNT)
        Next iCount
    Else 'NOT CLINICAL.FINDPATIENTCLINICAL...
        LogResultsCI "PatientId " & aInvoices(1).iPatId & " for AppointmentId " & aInvoices(1).iApptId & " has F records already."
    End If
    Set Clinical = Nothing

Exit Sub

lPostArray_Error:
    Set Clinical = Nothing
    LogError "MClinImport", "PostArray", Err, Err.Description, "ApptId: " & Str$(aInvoices(1).iApptId)

End Sub

Private Function PostExamElement(iPatId As Long, _
                            iApptId As Long, _
                            sProc As String, _
                            ByVal fModRight As Boolean, _
                            ByVal fModLeft As Boolean) As Boolean

Dim iClassId  As Long
Dim sAQues    As String
Dim iREyeId   As Long
Dim iLEyeId   As Long
Dim iModId    As Long
Dim sREyeName As String
Dim sLEyeName As String
Dim sModName  As String
Dim Clinical  As PatientClinical

    On Error GoTo lPostExamElement_Error
    If IsThisFRecord(sProc, sAQues) Then
        If IsNull(ProcedureDictionary(sProc)) Then
            iClassId = IsThisFRecord(sProc, sAQues)
        Else
            iClassId = CInt(ProcedureDictionary(sProc)(1))
            sAQues = ProcedureDictionary(sProc)(0)
        End If
        GetEyeControls iClassId, sAQues, iREyeId, iLEyeId, iModId, sREyeName, sLEyeName, sModName
        If iPatId > 0 Then
            Set Clinical = New PatientClinical
            Clinical.ClinicalId = 0
            If Clinical.RetrievePatientClinical Then
                Clinical.PatientId = iPatId
                Clinical.AppointmentId = iApptId
                Clinical.ClinicalType = "F"
                Clinical.EyeContext = vbNullString
                Clinical.Symptom = "/" & UCase$(sAQues)
                Clinical.Findings = "TIME=12:00 AM (IOP)"
                Clinical.ImageDescriptor = vbNullString
                Clinical.ImageInstructions = vbNullString
                Clinical.ClinicalDraw = vbNullString
                Clinical.ClinicalStatus = "A"
                Clinical.ClinicalHighlights = "T"
                Clinical.ApplyPatientClinical
            End If
            Set Clinical = Nothing
            Set Clinical = New PatientClinical
            Clinical.ClinicalId = 0
            If Clinical.RetrievePatientClinical Then
                Clinical.PatientId = iPatId
                Clinical.AppointmentId = iApptId
                Clinical.ClinicalType = "F"
                Clinical.EyeContext = vbNullString
                Clinical.Symptom = "/" & UCase$(sAQues)
                Clinical.Findings = "*" & sREyeName & "=T" & Trim$(Str$(iREyeId)) & " <*" & sREyeName & ">"
                Clinical.ImageDescriptor = vbNullString
                Clinical.ImageInstructions = vbNullString
                Clinical.ClinicalDraw = vbNullString
                Clinical.ClinicalStatus = "A"
                Clinical.ClinicalHighlights = "T"
                Clinical.ApplyPatientClinical
            End If
            Set Clinical = Nothing
            If fModRight Then
                PostEye iPatId, iApptId, sAQues, sModName, iModId
            End If
            Set Clinical = New PatientClinical
            Clinical.ClinicalId = 0
            If Clinical.RetrievePatientClinical Then
                Clinical.PatientId = iPatId
                Clinical.AppointmentId = iApptId
                Clinical.ClinicalType = "F"
                Clinical.EyeContext = vbNullString
                Clinical.Symptom = "/" & UCase$(sAQues)
                Clinical.Findings = "*" & sLEyeName & "=T" & Trim$(Str$(iLEyeId)) & " <*" & sLEyeName & ">"
                Clinical.ImageDescriptor = vbNullString
                Clinical.ImageInstructions = vbNullString
                Clinical.ClinicalDraw = vbNullString
                Clinical.ClinicalStatus = "A"
                Clinical.ClinicalHighlights = "T"
                Clinical.ApplyPatientClinical
            End If
            Set Clinical = Nothing
            If fModLeft Then
                PostEye iPatId, iApptId, sAQues, sModName, iModId
            End If
            LogResultsCI "Posting Pat " & Trim$(Str$(iPatId)) & " and Appt " & Trim$(Str$(iApptId)) & " and Svc " & Trim$(sProc)
            PostExamElement = True
        End If
    End If

Exit Function

lPostExamElement_Error:
    LogError "MClinImport", "PostExamElement", Err, Err.Description

End Function

Private Sub PostEye(ByVal iPatId As Long, _
                    ByVal iApptId As Long, _
                    ByVal sSymptom As String, _
                    ByVal sModName As String, _
                    ByVal iModId As Long)

Dim Clinical As PatientClinical

    On Error GoTo lPostEye_Error
    Set Clinical = New PatientClinical
    Clinical.ClinicalId = 0
    If Clinical.RetrievePatientClinical Then
        Clinical.PatientId = iPatId
        Clinical.AppointmentId = iApptId
        Clinical.ClinicalType = "F"
        Clinical.EyeContext = vbNullString
        Clinical.Symptom = "/" & UCase$(sSymptom)
        Clinical.Findings = "*" & sModName & "=T" & Trim$(Str$(iModId)) & " <Highlight>"
        Clinical.ImageDescriptor = vbNullString
        Clinical.ImageInstructions = vbNullString
        Clinical.ClinicalDraw = vbNullString
        Clinical.ClinicalStatus = "A"
        Clinical.ClinicalHighlights = "T"
        Clinical.ApplyPatientClinical
    End If
    Set Clinical = Nothing

Exit Sub

lPostEye_Error:
    LogError "MClinImport", "PostEye", Err, Err.Description

End Sub

Private Function PostOpRange(ByVal iApptId As Long, _
                             ByVal sCPT As String) As Long

Dim Receivable   As New PatientReceivables
Dim myInsurer    As New Insurer
Dim ModifierRule As New PracticeModifierRules

    On Error GoTo lPostOpRange_Error
    Receivable.AppointmentId = iApptId
    Receivable.ReceivableInsurerDesignation = "T"
    If Receivable.FindPatientReceivable Then
        If Receivable.SelectPatientReceivable(1) Then
            myInsurer.InsurerId = Receivable.InsurerId
            If myInsurer.RetrieveInsurer Then
                ModifierRule.ModifierClassId = myInsurer.InsurerBusinessClass
                ModifierRule.ModifierCPT = sCPT
                If ModifierRule.FindModifierRule Then
                    If ModifierRule.SelectModifierRule(1) Then
                        If Len(ModifierRule.ModifierRule5Criteria) > 1 Then
                            PostOpRange = Val(Right$(ModifierRule.ModifierRule5Criteria, Len(ModifierRule.ModifierRule5Criteria) - 1))
                        End If
                    End If
                End If
                Set ModifierRule = Nothing
            End If
            Set myInsurer = Nothing
        End If
    End If
    Set Receivable = Nothing

Exit Function

lPostOpRange_Error:
    LogError "MClinImport", "PostOpRange", Err, Err.Description, "Appt: " & Str$(iApptId) & " - CPT: " & sCPT

End Function

Private Sub PostPRecord(ByVal iPatId As Long, _
                        ByVal iApptId As Long, _
                        ByVal sCPT As String, _
                        ByVal iDayRange As Long)

Dim Clinical As PatientClinical

    On Error GoTo lPostPRecord_Error
    Set Clinical = New PatientClinical
    With Clinical
        .ClinicalId = 0
        If .RetrievePatientClinical Then
            .PatientId = iPatId
            .AppointmentId = iApptId
            .ClinicalType = "P"
            .EyeContext = vbNullString
            .Symptom = vbNullString
            'This should be an incrementing number based on # of P records for this appointment
            .Findings = "?:" & sCPT
            .ImageDescriptor = vbNullString
            .ImageInstructions = vbNullString
            .ClinicalStatus = "A"
            .ClinicalDraw = vbNullString
            .ClinicalHighlights = vbNullString
            .ClinicalFollowup = vbNullString
            .ClinicalPermanent = vbNullString
            .ClinicalPostOpPeriod = iDayRange
            .ApplyPatientClinical
        End If
    End With 'CLINICAL
    Set Clinical = Nothing
    LogResultsCI "Posting Pat " & Trim$(Str$(iPatId)) & " and Appt " & Trim$(Str$(iApptId)) & " and Svc " & Trim$(sCPT) & " - PRecord"

Exit Sub

lPostPRecord_Error:
    LogError "MClinImport", "PostPRecord", Err, Err.Description

End Sub

Public Function IsClaimManual(iAppointmentId As Long) As Boolean
Dim Appointment As New CAppointment
    On Error GoTo lIsClaimManual_Error

    Appointment.AppointmentId = iAppointmentId
    If Appointment.RetrieveSchedulerAppointment Then
        If Appointment.AppointmentComments = "ADD VIA BILLING" Then
            IsClaimManual = True
        ElseIf Appointment.AppointmentComments = "ADD VIA OPTICAL" Then
            IsClaimManual = True
        End If
    End If
    Set Appointment = Nothing

    Exit Function

lIsClaimManual_Error:

    LogError "MClinImport", "IsClaimManual", Err, Err.Description
    
End Function
Public Function AutoLogin() As Boolean
On Error GoTo lAutoLogin_Error
AutoLogin = False
Dim iLoc As Integer
Dim sLoginPID As String
Dim LoginId As Long
    iLoc = InStr(1, Command$, "--login ")
    If (iLoc > 0) Then
        sLoginPID = Trim$(Mid$(Command$, iLoc + 7, 5))
        If (Len(sLoginPID) = 4) Then
            LoginId = Login(sLoginPID)
            If LoginId > 0 Then AutoLogin = True
        End If
    End If
Exit Function
lAutoLogin_Error:
    AutoLogin = False
    LogError "MClinImport", "AutoLogin", Err, Err.Description
End Function
Private Function Login(sPID As String) As Long
    On Error GoTo lLogin_Error
    
    Login = -1
    Dim resourceArgs(0) As Variant
    resourceArgs(0) = sPID
    Dim UserContextComWrapper As New ComWrapper
    UserContextComWrapper.Create "IO.Practiceware.Application.UserContext", emptyArgs
    Login = UserContextComWrapper.InvokeMethod("Login", resourceArgs)
    
    Exit Function
lLogin_Error:
    LogError "MClinImport", "Login", Err, Err.Description
End Function

