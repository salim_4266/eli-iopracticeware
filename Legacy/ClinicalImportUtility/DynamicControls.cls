VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicControl Object Module
Option Explicit
Public Language                      As String
Public ControlId                     As Long
Public FormId                        As Long
Public ControlName                   As String
Public ControlType                   As String
Public BackColor                     As Long
Public ForeColor                     As Long
Public TabIndex                      As Long
Public ResponseLength                As Long
Public ControlTemplateName           As String
Public ControlFont                   As String
Public ControlTop                    As Long
Public ControlLeft                   As Long
Public ControlHeight                 As Long
Public ControlWidth                  As Long
Public ControlText                   As String
Public ControlAlternateText          As String
Public ControlAlternateTrigger       As String
Public ControlVisibleAfterTrigger    As Boolean
Public ConfirmLingo                  As String
Public ControlVisible                As Boolean
Public DrugQuestion                  As Boolean
Public DoctorLingo                   As String
Public IEChoiceName                  As String
Public DecimalDefault                As String
Public Exclusion                     As String
Public LetterLanguage                As String
Public LetterLanguage1               As String
Public ICDAlias1                     As String
Public ICDAlias2                     As String
Public ICDAlias3                     As String
Public ICDAlias4                     As String
Public IgnoreIt                      As Boolean
Public Ros                           As String
Public AIncrement                    As String
Private ModuleName                   As String
Private ClinicalControlsTbl          As ADODB.Recordset
Private DynamicControlTotal          As Long
Private ClinicalControlsNew          As Boolean

Private Sub Class_Initialize()

    Set ClinicalControlsTbl = New ADODB.Recordset
    ClinicalControlsTbl.CursorLocation = adUseClient
    InitControl

End Sub

Private Sub Class_Terminate()

    InitControl
    Set ClinicalControlsTbl = Nothing
    
End Sub

Private Function ConvertToLanguage(ByVal TheLang As String) As Boolean

Dim RetLang As DynamicLanguage

    ConvertToLanguage = True
    Set RetLang = New DynamicLanguage
    RetLang.ControlId = ControlId
    RetLang.Language = TheLang
    If RetLang.FindLanguageControl > 0 Then
        If RetLang.SelectLanguageControl(1) Then
            If LenB(Trim$(RetLang.ControlText)) Then
                ControlText = RetLang.ControlText
            End If
            If LenB(Trim$(RetLang.ConfirmLingo)) Then
                ConfirmLingo = RetLang.ConfirmLingo
            End If
        End If
    End If
    Set RetLang = Nothing

End Function

Public Function FindControl() As Long

    FindControl = GetAllControl()

End Function

Private Function GetAllControl() As Long

Dim OrderString As String

    On Error GoTo lGetAllControl_Error

    DynamicControlTotal = 0
    GetAllControl = -1
    If FormId > -10 Then
        If FormId <> 0 Then
            OrderString = "SELECT * FROM FormsControls WHERE FormId =" & Str$(FormId) & " "
            If LenB(Trim$(ControlName)) Then
                OrderString = OrderString & "And ControlName = '" & Trim$(ControlName) & "' "
            End If
            If LenB(Trim$(IEChoiceName)) Then
                OrderString = OrderString & "And InferenceEngineChoiceName = '" & Trim$(IEChoiceName) & "' "
            End If
            OrderString = OrderString & "ORDER BY FormId ASC, TabIndex ASC "
            Set ClinicalControlsTbl = Nothing
            Set ClinicalControlsTbl = New ADODB.Recordset
            MyDynamicFormsCmd.CommandText = OrderString
            Call ClinicalControlsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
            If ClinicalControlsTbl.EOF Then
                GetAllControl = -1
            Else 'CLINICALCONTROLSTBL.EOF = FALSE/0
                DynamicControlTotal = ClinicalControlsTbl.RecordCount
                GetAllControl = ClinicalControlsTbl.RecordCount
            End If
        End If
    End If

    Exit Function

lGetAllControl_Error:

    LogDBError "DynamicControls", "GetAllControl", Err, Err.Description

End Function

Private Sub InitControl()

    ClinicalControlsNew = False
    FormId = 0
    ControlName = vbNullString
    ControlType = vbNullString
    BackColor = 0
    ForeColor = 0
    TabIndex = 0
    ResponseLength = 0
    ControlVisible = True
    ControlTemplateName = vbNullString
    ControlFont = vbNullString
    ControlTop = 0
    ControlHeight = 0
    ControlLeft = 0
    ControlWidth = 0
    IEChoiceName = vbNullString
    ControlText = vbNullString
    DrugQuestion = False
    ControlAlternateText = vbNullString
    ControlAlternateTrigger = vbNullString
    ControlVisibleAfterTrigger = False
    ConfirmLingo = vbNullString
    DoctorLingo = vbNullString
    DecimalDefault = vbNullString
    ICDAlias1 = vbNullString
    ICDAlias2 = vbNullString
    ICDAlias3 = vbNullString
    ICDAlias4 = vbNullString
    Exclusion = vbNullString
    LetterLanguage = vbNullString
    LetterLanguage1 = vbNullString
    Ros = vbNullString
    AIncrement = vbNullString
    IgnoreIt = False

End Sub

Private Sub LoadControl()

    InitControl
    ControlId = ClinicalControlsTbl("ControlId")
    If LenB(ClinicalControlsTbl("FormId")) Then
        FormId = ClinicalControlsTbl("FormId")
    End If
    If LenB(ClinicalControlsTbl("ControlName")) Then
        ControlName = ClinicalControlsTbl("ControlName")
    End If
    If LenB(ClinicalControlsTbl("ControlType")) Then
        ControlType = ClinicalControlsTbl("ControlType")
    End If
    If LenB(ClinicalControlsTbl("BackColor")) Then
        BackColor = ClinicalControlsTbl("BackColor")
    End If
    If LenB(ClinicalControlsTbl("ForeColor")) Then
        ForeColor = ClinicalControlsTbl("ForeColor")
    End If
    If LenB(ClinicalControlsTbl("TabIndex")) Then
        TabIndex = ClinicalControlsTbl("TabIndex")
    End If
    If LenB(ClinicalControlsTbl("ResponseLength")) Then
        ResponseLength = ClinicalControlsTbl("ResponseLength")
    End If
    If LenB(ClinicalControlsTbl("ControlVisible")) Then
        ControlVisible = ClinicalControlsTbl("ControlVisible") = "T"
    End If
    If LenB(ClinicalControlsTbl("ControlTemplateName")) Then
        ControlTemplateName = ClinicalControlsTbl("ControlTemplateName")
    End If
    If LenB(ClinicalControlsTbl("ControlFont")) Then
        ControlFont = ClinicalControlsTbl("ControlFont")
    End If
    If LenB(ClinicalControlsTbl("ControlTop")) Then
        ControlTop = ClinicalControlsTbl("ControlTop")
    End If
    If LenB(ClinicalControlsTbl("ControlHeight")) Then
        ControlHeight = ClinicalControlsTbl("ControlHeight")
    End If
    If LenB(ClinicalControlsTbl("ControlLeft")) Then
        ControlLeft = ClinicalControlsTbl("ControlLeft")
    End If
    If LenB(ClinicalControlsTbl("ControlWidth")) Then
        ControlWidth = ClinicalControlsTbl("ControlWidth")
    End If
    If LenB(ClinicalControlsTbl("InferenceEngineChoiceName")) Then
        IEChoiceName = ClinicalControlsTbl("InferenceEngineChoiceName")
    End If
    If LenB(ClinicalControlsTbl("ControlText")) Then
        ControlText = ClinicalControlsTbl("ControlText")
    End If
    If LenB(ClinicalControlsTbl("DrugQuestion")) Then
        DrugQuestion = ClinicalControlsTbl("DrugQuestion") = "T"
    End If
    If LenB(ClinicalControlsTbl("ControlAlternateText")) Then
        ControlAlternateText = ClinicalControlsTbl("ControlAlternateText")
    End If
    If LenB(ClinicalControlsTbl("ControlAlternateTrigger")) Then
        ControlAlternateTrigger = ClinicalControlsTbl("ControlAlternateTrigger")
    End If
    If LenB(ClinicalControlsTbl("ControlVisibleAfterTrigger")) Then
        ControlVisibleAfterTrigger = ClinicalControlsTbl("ControlVisibleAfterTrigger") = "T"
    End If
    If LenB(ClinicalControlsTbl("ConfirmLingo")) Then
        ConfirmLingo = ClinicalControlsTbl("ConfirmLingo")
    End If
    If LenB(ClinicalControlsTbl("DoctorLingo")) Then
        DoctorLingo = ClinicalControlsTbl("DoctorLingo")
    End If
    If LenB(ClinicalControlsTbl("DecimalDefault")) Then
        DecimalDefault = ClinicalControlsTbl("DecimalDefault")
    End If
    If LenB(ClinicalControlsTbl("ICDAlias1")) Then
        ICDAlias1 = ClinicalControlsTbl("ICDAlias1")
    End If
    If LenB(ClinicalControlsTbl("ICDAlias2")) Then
        ICDAlias2 = ClinicalControlsTbl("ICDAlias2")
    End If
    If LenB(ClinicalControlsTbl("ICDAlias3")) Then
        ICDAlias3 = ClinicalControlsTbl("ICDAlias3")
    End If
    If LenB(ClinicalControlsTbl("ICDAlias4")) Then
        ICDAlias4 = ClinicalControlsTbl("ICDAlias4")
    End If
    If LenB(ClinicalControlsTbl("Exclusion")) Then
        Exclusion = ClinicalControlsTbl("Exclusion")
    End If
    If LenB(ClinicalControlsTbl("LetterTranslation")) Then
        LetterLanguage = ClinicalControlsTbl("LetterTranslation")
    End If
    If LenB(ClinicalControlsTbl("OtherLtrTrans")) Then
        LetterLanguage1 = ClinicalControlsTbl("OtherLtrTrans")
    End If
    If LenB(ClinicalControlsTbl("Ros")) Then
        Ros = ClinicalControlsTbl("Ros")
    End If
    If LenB(ClinicalControlsTbl("Increment")) Then
        AIncrement = ClinicalControlsTbl("Increment")
    End If
    If LenB(ClinicalControlsTbl("IgnoreIt")) Then
        IgnoreIt = ClinicalControlsTbl("IgnoreIt") = "T"
    End If
    If LenB(Trim$(Language)) Then
        ConvertToLanguage Language
    End If

End Sub

Public Function SelectControl(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectControl_Error

    SelectControl = False
    If Not ClinicalControlsTbl.EOF Then
        If ListItem > 0 Then
            If ListItem <= DynamicControlTotal Then
                ClinicalControlsTbl.Move (ListItem - ClinicalControlsTbl.AbsolutePosition)
                SelectControl = True
                LoadControl
            End If
        End If
    End If

    Exit Function

lSelectControl_Error:

    LogDBError "DynamicControls", "SelectControl", Err, Err.Description

End Function

