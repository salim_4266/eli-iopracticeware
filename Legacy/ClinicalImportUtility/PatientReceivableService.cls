VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientReceivableService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Receivable Service Object Module
Option Explicit
Public ItemId                             As Long
Public Invoice                            As String
Public Service                            As String
Public ServiceModifier                    As String
Public ServiceCharge                      As Single
Public ServiceFeeCharge                   As Single
Public ServiceQuantity                    As Single
Public ServiceDate                        As String
Public ServiceLinkedDiag                  As String
Public ServiceTOS                         As String
Public ServicePOS                         As String
Public ServiceOrder                       As Long
Public ServiceOrderDoc                    As Boolean
Public ServiceConsultOn                   As Boolean
Public ServiceStatus                      As String
Private ModuleName                        As String
Private PatientReceivableServiceTotal     As Long
Private PatientReceivableServiceTbl       As ADODB.Recordset
Private PatientReceivableServiceNew       As Boolean

Private Sub Class_Initialize()

    Set PatientReceivableServiceTbl = CreateAdoRecordset
    InitPatientReceivableService

End Sub

Private Sub Class_Terminate()

    InitPatientReceivableService
    Set PatientReceivableServiceTbl = Nothing

End Sub

Public Function FindPatientReceivableServiceActive() As Long

    FindPatientReceivableServiceActive = GetPatientReceivableServicesActive()

End Function

Private Function GetPatientReceivableServicesActive() As Long

Dim OrderString As String

    On Error GoTo lGetPatientReceivableServicesActive_Error

    GetPatientReceivableServicesActive = -1
    PatientReceivableServiceTotal = 0
    OrderString = "Select * FROM PatientReceivableServices WHERE Status = 'A' ORDER BY Invoice ASC, ItemOrder ASC "
    Set PatientReceivableServiceTbl = Nothing
    Set PatientReceivableServiceTbl = CreateAdoRecordset
    PatientReceivableServiceTbl.CursorLocation = adUseClient
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PatientReceivableServiceTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PatientReceivableServiceTbl.EOF Then
        GetPatientReceivableServicesActive = -1
    Else 'PATIENTRECEIVABLESERVICETBL.EOF = FALSE/0
        With PatientReceivableServiceTbl
            PatientReceivableServiceTotal = .RecordCount
            GetPatientReceivableServicesActive = .RecordCount
        End With 'PatientReceivableServiceTbl
    End If

    Exit Function

lGetPatientReceivableServicesActive_Error:

    LogDBError "PatientReceivableService", "GetPatientReceivableServicesActive", Err, Err.Description

End Function

Private Sub InitPatientReceivableService()

    PatientReceivableServiceNew = True
    Invoice = vbNullString
    Service = vbNullString
    ServiceModifier = vbNullString
    ServiceQuantity = 0
    ServiceCharge = 0
    ServiceTOS = vbNullString
    ServicePOS = vbNullString
    ServiceDate = vbNullString
    ServiceLinkedDiag = vbNullString
    ServiceStatus = "A"
    ServiceOrderDoc = False
    ServiceConsultOn = False
    ServiceFeeCharge = 0
    ServiceOrder = 0

End Sub

Private Sub LoadPatientReceivableService()

    InitPatientReceivableService
    PatientReceivableServiceNew = False
    If LenB(PatientReceivableServiceTbl("ItemId")) Then
        ItemId = PatientReceivableServiceTbl("ItemId")
    End If
    If LenB(PatientReceivableServiceTbl("Invoice")) Then
        Invoice = PatientReceivableServiceTbl("Invoice")
    End If
    If LenB(PatientReceivableServiceTbl("Service")) Then
        Service = PatientReceivableServiceTbl("Service")
    End If
    If LenB(PatientReceivableServiceTbl("Modifier")) Then
        ServiceModifier = PatientReceivableServiceTbl("Modifier")
    End If
    If LenB(PatientReceivableServiceTbl("Quantity")) Then
        ServiceQuantity = PatientReceivableServiceTbl("Quantity")
    End If
    If LenB(PatientReceivableServiceTbl("Charge")) Then
        ServiceCharge = PatientReceivableServiceTbl("Charge")
    End If
    If LenB(PatientReceivableServiceTbl("TypeOfService")) Then
        ServiceTOS = PatientReceivableServiceTbl("TypeOfService")
    End If
    If LenB(PatientReceivableServiceTbl("PlaceOfService")) Then
        ServicePOS = PatientReceivableServiceTbl("PlaceOfService")
    End If
    If LenB(PatientReceivableServiceTbl("ServiceDate")) Then
        ServiceDate = PatientReceivableServiceTbl("ServiceDate")
    End If
    If LenB(PatientReceivableServiceTbl("LinkedDiag")) Then
        ServiceLinkedDiag = PatientReceivableServiceTbl("LinkedDiag")
    End If
    If LenB(PatientReceivableServiceTbl("Status")) Then
        ServiceStatus = PatientReceivableServiceTbl("Status")
    End If
    If LenB(PatientReceivableServiceTbl("OrderDoc")) Then
        ServiceOrderDoc = PatientReceivableServiceTbl("OrderDoc") = "T"
    End If
    If LenB(PatientReceivableServiceTbl("ConsultOn")) Then
        ServiceConsultOn = PatientReceivableServiceTbl("ConsultOn") = "T"
    End If
    If LenB(PatientReceivableServiceTbl("FeeCharge")) Then
        ServiceFeeCharge = PatientReceivableServiceTbl("FeeCharge")
    End If
    If LenB(PatientReceivableServiceTbl("ItemOrder")) Then
        ServiceOrder = PatientReceivableServiceTbl("ItemOrder")
    End If

End Sub

Public Function SelectPatientReceivableServiceAggregate(ByVal ListItem As Long) As Boolean

    On Error GoTo lSelectPatientReceivableServiceAggregate_Error

    SelectPatientReceivableServiceAggregate = False
    If Not PatientReceivableServiceTbl.EOF Then
        If ListItem > 0 Then
            If ListItem <= PatientReceivableServiceTotal Then
                PatientReceivableServiceTbl.Move ListItem - PatientReceivableServiceTbl.AbsolutePosition
                SelectPatientReceivableServiceAggregate = True
                LoadPatientReceivableService
            End If
        End If
    End If

    Exit Function

lSelectPatientReceivableServiceAggregate_Error:

    LogDBError "PatientReceivableService", "SelectPatientReceivableServiceAggregate", Err, Err.Description

End Function

