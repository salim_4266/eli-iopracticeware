VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Insurer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Insurer Object Module
Option Explicit
Public InsurerId                   As Long
Public InsurerName                 As String
Public InsurerPlanId               As String
Public InsurerPlanName             As String
Public InsurerPlanType             As String
Public InsurerGroupId              As String
Public InsurerGroupName            As String
Public InsurerAddress              As String
Public InsurerCity                 As String
Public InsurerState                As String
Public InsurerZip                  As String
Public InsurerPhone                As String
Public InsurerRefCode              As String
Public InsurerPType                As String
Public InsurerReferralRequired     As Boolean
Public InsurerPreCertRequired      As Boolean
Public InsurerCrossOver            As Boolean
Public InsurerFeeSchedule          As String
Public InsurerTOS                  As String
Public InsurerPOS                  As String
Public InsurerNEICNumber           As String
Public InsurerENumber              As String
Public InsurerEFormat              As String
Public InsurerTFormat              As String
Public InsurerPcPhone              As String
Public InsurerElPhone              As String
Public InsurerClPhone              As String
Public InsurerPrPhone              As String
Public InsurerMedicareXOverId      As String
Public InsurerAdjustmentDefault    As String
Public InsurerComment              As String
Public InsurerPlanFormat           As String
Public InsurerClaimMap             As String
Public InsurerBusinessClass        As String
Public InsurerCPTClass             As String
Public InsurerOCodeOn              As Boolean
Public Availability                As String
Public Copay                       As Single
Public OutOfPocket                 As Single
Public InsurerServiceFilter        As Boolean
Public StateJurisdiction           As String
Private JustName                   As Integer
Private ModuleName                 As String
Private PracticeInsurerTbl         As ADODB.Recordset
Private PracticeInsurerNew         As Boolean

Private Sub Class_Initialize()

    Set PracticeInsurerTbl = CreateAdoRecordset
    InitInsurer

End Sub

Private Sub Class_Terminate()

    InitInsurer
    Set PracticeInsurerTbl = Nothing

End Sub

Private Function GetInsurer() As Boolean

Dim OrderString As String

    On Error GoTo lGetInsurer_Error

    JustName = False
    GetInsurer = True
    OrderString = "SELECT * FROM PracticeInsurers WHERE InsurerId =" & Str$(InsurerId)
    Set PracticeInsurerTbl = Nothing
    Set PracticeInsurerTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandText = OrderString
    PracticeInsurerTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PracticeInsurerTbl.EOF Then
        InitInsurer
    Else
        LoadInsurer
    End If

    Exit Function

lGetInsurer_Error:

    LogDBError "Insurer", "GetInsurer", Err, Err.Description

End Function

Private Function GetInsurerList(ByVal IType As Integer) As Long

Dim Ref         As String
Dim OrderString As String

    On Error GoTo lGetInsurerList_Error

    JustName = False
    GetInsurerList = -1
    If LenB(Trim$(InsurerName)) = 0 Then
        Exit Function
    End If
    Ref = ">="
    If IType = 1 Then
        Ref = "="
    End If
    OrderString = "Select * FROM PracticeInsurers WHERE InsurerName " & Ref & " '" & UCase$(Trim$(InsurerName)) & "' "
    If IType = 2 Then
        JustName = 1
        OrderString = "Select DISTINCT InsurerName FROM PracticeInsurers WHERE InsurerName " & Ref & "'" & UCase$(Trim$(InsurerName)) & "' "
    End If
    If IType = 3 Then
        JustName = 2
        OrderString = "Select DISTINCT InsurerName, InsurerId FROM PracticeInsurers WHERE InsurerName " & Ref & "'" & UCase$(Trim$(InsurerName)) & "' "
    End If
    If LenB(Trim$(InsurerGroupId)) Then
        OrderString = OrderString & "And InsurerGroupId = '" & UCase$(Trim$(InsurerGroupId)) & "' "
    End If
    If LenB(Trim$(InsurerGroupName)) Then
        OrderString = OrderString & "And InsurerGroupName = '" & UCase$(Trim$(InsurerGroupName)) & "' "
    End If
    If LenB(Trim$(InsurerPlanId)) Then
        OrderString = OrderString & "And InsurerPlanId = '" & UCase$(Trim$(InsurerPlanId)) & "' "
    End If
    If LenB(Trim$(InsurerPlanName)) Then
        OrderString = OrderString & "And InsurerPlanName = '" & UCase$(Trim$(InsurerPlanName)) & "' "
    End If
    If LenB(Trim$(InsurerPlanType)) Then
        OrderString = OrderString & "And InsurerPlanType = '" & UCase$(Trim$(InsurerPlanType)) & "' "
    End If
    If LenB(Trim$(InsurerAddress)) Then
        OrderString = OrderString & "And InsurerAddress = '" & UCase$(Trim$(InsurerAddress)) & "' "
    End If
    If LenB(Trim$(InsurerCity)) Then
        OrderString = OrderString & "And InsurerCity = '" & UCase$(Trim$(InsurerCity)) & "' "
    End If
    If LenB(Trim$(InsurerState)) Then
        OrderString = OrderString & "And InsurerState = '" & UCase$(Trim$(InsurerState)) & "' "
    End If
    If LenB(Trim$(InsurerZip)) Then
        OrderString = OrderString & "And InsurerZip = '" & UCase$(Trim$(InsurerZip)) & "' "
    End If
    If LenB(Trim$(InsurerPlanType)) Then
        OrderString = OrderString & "And InsurerPlanType = '" & UCase$(Trim$(InsurerPlanType)) & "' "
    End If
    If LenB(Trim$(InsurerRefCode)) Then
        OrderString = OrderString & "And InsurerReferenceCode = '" & UCase$(Trim$(InsurerRefCode)) & "' "
    End If
    If JustName Then
        OrderString = OrderString & "ORDER BY InsurerName ASC "
    Else
        OrderString = OrderString & "ORDER BY InsurerName ASC, InsurerAddress ASC, InsurerPlanId ASC, InsurerGroupId ASC "
    End If
    Set PracticeInsurerTbl = Nothing
    Set PracticeInsurerTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PracticeInsurerTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PracticeInsurerTbl.EOF Then
        GetInsurerList = -1
        InitInsurer
    Else
        If IType = 1 Then
            GetInsurerList = PracticeInsurerTbl.RecordCount
        Else
            If (Left$(InsurerName, 8) = "MEDICARE") Then
                GetInsurerList = PracticeInsurerTbl.RecordCount
            Else
                GetInsurerList = 1
            End If
        End If
    End If

    Exit Function

lGetInsurerList_Error:

    LogDBError "Insurer", "GetInsurerList", Err, Err.Description

End Function

Private Sub InitInsurer()

    PracticeInsurerNew = True
    InsurerName = vbNullString
    InsurerGroupId = vbNullString
    InsurerGroupName = vbNullString
    InsurerPlanId = vbNullString
    InsurerPlanName = vbNullString
    InsurerPlanType = vbNullString
    InsurerAddress = vbNullString
    InsurerCity = vbNullString
    InsurerState = vbNullString
    InsurerZip = vbNullString
    InsurerPhone = vbNullString
    InsurerRefCode = vbNullString
    InsurerPType = vbNullString
    InsurerReferralRequired = False
    InsurerPreCertRequired = False
    InsurerCrossOver = False
    InsurerFeeSchedule = vbNullString
    InsurerTOS = vbNullString
    InsurerPOS = vbNullString
    InsurerNEICNumber = vbNullString
    InsurerENumber = vbNullString
    InsurerEFormat = vbNullString
    InsurerTFormat = vbNullString
    InsurerPcPhone = vbNullString
    InsurerPrPhone = vbNullString
    InsurerElPhone = vbNullString
    InsurerClPhone = vbNullString
    InsurerMedicareXOverId = vbNullString
    InsurerAdjustmentDefault = vbNullString
    InsurerComment = vbNullString
    InsurerPlanFormat = vbNullString
    InsurerClaimMap = vbNullString
    InsurerBusinessClass = vbNullString
    InsurerCPTClass = vbNullString
    InsurerOCodeOn = False
    Copay = 0
    OutOfPocket = 0
    Availability = "A"
    InsurerServiceFilter = False
    StateJurisdiction = vbNullString
    JustName = 0

End Sub

Private Sub LoadInsurer()

    PracticeInsurerNew = False
    If LenB(PracticeInsurerTbl("InsurerName")) Then
        InsurerName = PracticeInsurerTbl("InsurerName")
    Else
        InsurerName = vbNullString
    End If
    If JustName = 1 Then
        Exit Sub
    End If
    If LenB(PracticeInsurerTbl("InsurerId")) Then
        InsurerId = PracticeInsurerTbl("InsurerId")
    Else
        InsurerId = 0
    End If
    If JustName = 2 Then
        Exit Sub
    End If
    If LenB(PracticeInsurerTbl("InsurerAddress")) Then
        InsurerAddress = PracticeInsurerTbl("InsurerAddress")
    Else
        InsurerAddress = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerCity")) Then
        InsurerCity = PracticeInsurerTbl("InsurerCity")
    Else
        InsurerCity = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerState")) Then
        InsurerState = PracticeInsurerTbl("InsurerState")
    Else
        InsurerState = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerZip")) Then
        InsurerZip = PracticeInsurerTbl("InsurerZip")
    Else
        InsurerZip = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPhone")) Then
        InsurerPhone = PracticeInsurerTbl("InsurerPhone")
    Else
        InsurerPhone = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerReferenceCode")) Then
        InsurerRefCode = PracticeInsurerTbl("InsurerReferenceCode")
    Else
        InsurerRefCode = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPayType")) Then
        InsurerPType = PracticeInsurerTbl("InsurerPayType")
    Else
        InsurerPType = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPlanId")) Then
        InsurerPlanId = PracticeInsurerTbl("InsurerPlanId")
    Else
        InsurerPlanId = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPlanName")) Then
        InsurerPlanName = PracticeInsurerTbl("InsurerPlanName")
    Else
        InsurerPlanName = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPlanType")) Then
        InsurerPlanType = PracticeInsurerTbl("InsurerPlanType")
    Else
        InsurerPlanType = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerGroupId")) Then
        InsurerGroupId = PracticeInsurerTbl("InsurerGroupId")
    Else
        InsurerGroupId = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerGroupName")) Then
        InsurerGroupName = PracticeInsurerTbl("InsurerGroupName")
    Else
        InsurerGroupName = vbNullString
    End If
    If LenB(PracticeInsurerTbl("ReferralRequired")) Then
        InsurerReferralRequired = Not (PracticeInsurerTbl("ReferralRequired") = "N")
    Else
        InsurerReferralRequired = False
    End If
    If LenB(PracticeInsurerTbl("PrecertRequired")) Then
        InsurerPreCertRequired = Not (PracticeInsurerTbl("PreCertRequired") = "N")
    Else
        InsurerPreCertRequired = False
    End If
    If LenB(PracticeInsurerTbl("InsurerCrossOver")) Then
        InsurerCrossOver = Not (PracticeInsurerTbl("InsurerCrossOver") = "N")
    Else
        InsurerCrossOver = False
    End If
    If LenB(PracticeInsurerTbl("OCode")) Then
        InsurerOCodeOn = Not (PracticeInsurerTbl("OCode") = "N")
    Else
        InsurerOCodeOn = False
    End If
    If LenB(PracticeInsurerTbl("Copay")) Then
        Copay = PracticeInsurerTbl("Copay")
    Else
        Copay = 0
    End If
    If LenB(PracticeInsurerTbl("OutOfPocket")) Then
        OutOfPocket = PracticeInsurerTbl("OutOfPocket")
    Else
        OutOfPocket = 0
    End If
    If LenB(PracticeInsurerTbl("InsurerFeeSchedule")) Then
        InsurerFeeSchedule = PracticeInsurerTbl("InsurerFeeSchedule")
    Else
        InsurerFeeSchedule = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerTOSTbl")) Then
        InsurerTOS = PracticeInsurerTbl("InsurerTOSTbl")
    Else
        InsurerTOS = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPOSTbl")) Then
        InsurerPOS = PracticeInsurerTbl("InsurerPOSTbl")
    Else
        InsurerPOS = vbNullString
    End If
    If LenB(PracticeInsurerTbl("MedicareCrossOverId")) Then
        InsurerMedicareXOverId = PracticeInsurerTbl("MedicareCrossOverId")
    Else
        InsurerMedicareXOverId = vbNullString
    End If
    If LenB(PracticeInsurerTbl("NEICNumber")) Then
        InsurerNEICNumber = PracticeInsurerTbl("NEICNumber")
    Else
        InsurerNEICNumber = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerENumber")) Then
        InsurerENumber = PracticeInsurerTbl("InsurerENumber")
    Else
        InsurerENumber = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerEFormat")) Then
        InsurerEFormat = PracticeInsurerTbl("InsurerEFormat")
    Else
        InsurerEFormat = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerTFormat")) Then
        InsurerTFormat = PracticeInsurerTbl("InsurerTFormat")
    Else
        InsurerTFormat = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPrecPhone")) Then
        InsurerPcPhone = PracticeInsurerTbl("InsurerPrecPhone")
    Else
        InsurerPcPhone = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerEligPhone")) Then
        InsurerElPhone = PracticeInsurerTbl("InsurerEligPhone")
    Else
        InsurerElPhone = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerClaimPhone")) Then
        InsurerClPhone = PracticeInsurerTbl("InsurerClaimPhone")
    Else
        InsurerClPhone = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerProvPhone")) Then
        InsurerPrPhone = PracticeInsurerTbl("InsurerProvPhone")
    Else
        InsurerPrPhone = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerAdjustmentDefault")) Then
        InsurerAdjustmentDefault = PracticeInsurerTbl("InsurerAdjustmentDefault")
    Else
        InsurerAdjustmentDefault = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerPlanFormat")) Then
        InsurerPlanFormat = PracticeInsurerTbl("InsurerPlanFormat")
    Else
        InsurerPlanFormat = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerClaimMap")) Then
        InsurerClaimMap = PracticeInsurerTbl("InsurerClaimMap")
    Else
        InsurerClaimMap = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerBusinessClass")) Then
        InsurerBusinessClass = PracticeInsurerTbl("InsurerBusinessClass")
    Else
        InsurerBusinessClass = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerCPTClass")) Then
        InsurerCPTClass = PracticeInsurerTbl("InsurerCPTClass")
    Else
        InsurerCPTClass = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerComment")) Then
        InsurerComment = PracticeInsurerTbl("InsurerComment")
    Else
        InsurerComment = vbNullString
    End If
    If LenB(PracticeInsurerTbl("Availability")) Then
        Availability = PracticeInsurerTbl("Availability")
    Else
        Availability = vbNullString
    End If
    If LenB(PracticeInsurerTbl("StateJurisdiction")) Then
        StateJurisdiction = PracticeInsurerTbl("StateJurisdiction")
    Else
        StateJurisdiction = vbNullString
    End If
    If LenB(PracticeInsurerTbl("InsurerServiceFilter")) Then
        InsurerServiceFilter = Not (PracticeInsurerTbl("InsurerServiceFilter") = "N")
    Else
        InsurerServiceFilter = False
    End If

End Sub

Public Function RetrieveInsurer() As Boolean

    RetrieveInsurer = GetInsurer

End Function

Public Function SelectInsurer(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectInsurer_Error

    SelectInsurer = False
    If PracticeInsurerTbl.EOF Or (ListItem < 1) Then
        Exit Function
    End If
    PracticeInsurerTbl.MoveFirst
    PracticeInsurerTbl.Move ListItem - 1
    SelectInsurer = True
    LoadInsurer

    Exit Function

lSelectInsurer_Error:

    LogDBError "Insurer", "SelectInsurer", Err, Err.Description

End Function

