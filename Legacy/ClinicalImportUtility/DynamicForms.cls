VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicForms"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicForms Object Module
Option Explicit
Public FormId                   As Long
Public FormName                 As String
Public FormInterface            As String
Public BackColor                As Long
Public ForeColor                As Long
Public WindowState              As Long
Public WindowHeight             As Long
Public WindowWidth              As Long
Public ExitButton               As Boolean
Public ApplyButton              As Boolean
Public DontKnowButton           As Boolean
Public Question                 As String
Public QuestionType             As String
Public NoToAllButton            As Boolean
Public HelpButton               As Boolean
Public EditType                 As Boolean
Public StaticLabel              As String
Private ModuleName              As String
Private ClinicalTotalForms      As Integer
Private ClinicalFormsTbl        As ADODB.Recordset
Private ClinicalFormsNew        As Boolean

Private Sub Class_Initialize()

    Set ClinicalFormsTbl = New ADODB.Recordset
    ClinicalFormsTbl.CursorLocation = adUseClient
    InitForm

End Sub

Private Sub Class_Terminate()

    InitForm
    Set ClinicalFormsTbl = Nothing
    
End Sub

Private Function GetForm() As Boolean

Dim Ref         As String
Dim OrderString As String

    On Error GoTo lGetForm_Error

    GetForm = True
    If LenB(Trim$(Question)) = 0 Then
        If FormId < 1 Then
            If LenB(Trim$(FormName)) = 0 Then
                InitForm
                Exit Function
            End If
        End If
    End If
    Ref = vbNullString
    OrderString = "SELECT * FROM Forms WHERE "
    If FormId > 0 Then
        OrderString = OrderString & "FormId =" & Str$(FormId) & " "
        Ref = "And "
    End If
    If LenB(Trim$(Question)) Then
        If Len(Question) = 1 Then
            OrderString = OrderString + Ref & "Question >= '" & Question & "' "
        Else 'NOT LEN(QUESTION)...
            OrderString = OrderString + Ref & "Question = '" & Question & "' "
        End If
    End If
    If LenB(Trim$(FormName)) Then
        OrderString = OrderString + Ref & "FormName = '" & FormName & "' "
    End If
    Set ClinicalFormsTbl = Nothing
    Set ClinicalFormsTbl = New ADODB.Recordset
    MyDynamicFormsCmd.CommandText = OrderString
    Call ClinicalFormsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
    If ClinicalFormsTbl.EOF Then
        InitForm
    Else 'CLINICALFORMSTBL.EOF = FALSE/0
        LoadForm
        ClinicalTotalForms = ClinicalFormsTbl.RecordCount
    End If
    

    Exit Function

lGetForm_Error:

    LogDBError "DynamicForms", "GetForm", Err, Err.Description

End Function

Private Sub InitForm()

    ClinicalFormsNew = False
    FormName = vbNullString
    FormInterface = vbNullString
    BackColor = 0
    ForeColor = 0
    WindowState = 0
    ExitButton = False
    ApplyButton = False
    DontKnowButton = False
    HelpButton = False
    NoToAllButton = False
    StaticLabel = vbNullString
    Question = vbNullString
    QuestionType = vbNullString
    WindowHeight = 0
    WindowWidth = 0
    EditType = False

End Sub

Private Function LoadForm() As Boolean

    InitForm
    FormId = ClinicalFormsTbl("FormId")
    If LenB(ClinicalFormsTbl("FormName")) Then
        FormName = ClinicalFormsTbl("FormName")
    End If
    If LenB(ClinicalFormsTbl("Forminterface")) Then
        FormInterface = ClinicalFormsTbl("FormInterface")
    End If
    If LenB(ClinicalFormsTbl("BackColor")) Then
        BackColor = ClinicalFormsTbl("BackColor")
    End If
    If LenB(ClinicalFormsTbl("ForeColor")) Then
        ForeColor = ClinicalFormsTbl("ForeColor")
    End If
    If LenB(ClinicalFormsTbl("WindowState")) Then
        WindowState = ClinicalFormsTbl("WindowState")
    End If
    If LenB(ClinicalFormsTbl("ExitButton")) Then
        ExitButton = ClinicalFormsTbl("ExitButton") = "T"
    End If
    If LenB(ClinicalFormsTbl("ApplyButton")) Then
        ApplyButton = ClinicalFormsTbl("ApplyButton") = "T"
    End If
    If LenB(ClinicalFormsTbl("DontKnowButton")) Then
        DontKnowButton = ClinicalFormsTbl("DontKnowButton") = "T"
    End If
    If LenB(ClinicalFormsTbl("HelpButton")) Then
        HelpButton = ClinicalFormsTbl("HelpButton") = "T"
    End If
    If LenB(ClinicalFormsTbl("NoToAllButton")) Then
        NoToAllButton = ClinicalFormsTbl("NoToAllButton") = "T"
    End If
    If LenB(ClinicalFormsTbl("StaticLabel")) Then
        StaticLabel = ClinicalFormsTbl("StaticLabel")
    End If
    If LenB(ClinicalFormsTbl("Question")) Then
        Question = ClinicalFormsTbl("Question")
    End If
    If LenB(ClinicalFormsTbl("QuestionType")) Then
        QuestionType = ClinicalFormsTbl("QuestionType")
    End If
    If LenB(ClinicalFormsTbl("FormHeight")) Then
        WindowHeight = ClinicalFormsTbl("FormHeight")
    End If
    If LenB(ClinicalFormsTbl("FormWidth")) Then
        WindowWidth = ClinicalFormsTbl("FormWidth")
    End If
    If LenB(ClinicalFormsTbl("EditStyle")) Then
        EditType = ClinicalFormsTbl("EditStyle") = "T"
    End If

End Function

Public Function RetrieveForm() As Boolean

    RetrieveForm = GetForm

End Function

Public Function SelectForm(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectForm_Error

    SelectForm = False
    If Not ClinicalFormsTbl.EOF Then
        If ListItem > 0 Then
            If ListItem <= ClinicalTotalForms Then
                ClinicalFormsTbl.Move (ListItem - ClinicalFormsTbl.AbsolutePosition)
                SelectForm = True
                LoadForm
            End If
        End If
    End If

    Exit Function

lSelectForm_Error:

    LogDBError "DynamicForms", "SelectForm", Err, Err.Description

End Function

