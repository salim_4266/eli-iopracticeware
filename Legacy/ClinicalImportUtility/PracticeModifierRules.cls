VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeModifierRules"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Modifierss Object Module
Option Explicit
Public ModifierId                 As Long
Public ModifierClassId            As String
Public ModifierCPT                As String
Public ModifierRule1              As String
Public ModifierRule1Criteria      As String
Public ModifierRule2              As String
Public ModifierRule2Criteria      As String
Public ModifierRule3              As String
Public ModifierRule3Criteria      As String
Public ModifierRule4              As String
Public ModifierRule4Criteria      As String
Public ModifierRule5              As String
Public ModifierRule5Criteria      As String
Public ModifierRule6              As String
Public ModifierRule6Criteria      As String
Public ModifierRule7              As String
Public ModifierRule7Criteria      As String
Public ModifierRule8              As String
Public ModifierRule8Criteria      As String
Public ModifierRule9              As String
Public ModifierRule9Criteria      As String
Public ModifierRule10             As String
Public ModifierRule10Criteria     As String
Public ModifierRule11             As String
Public ModifierRule11Criteria     As String
Public ModifierRule12             As String
Public ModifierRule12Criteria     As String
Public ModifierRule13             As String
Public ModifierRule13Criteria     As String
Public ModifierRule14             As String
Public ModifierRule14Criteria     As String
Public ModifierRule15             As String
Public ModifierRule15Criteria     As String
Public ModifierRuleStatus         As String
Private ModuleName                As String
Private PracticeModifiersTotal    As Long
Private PracticeModifiersTbl      As ADODB.Recordset
Private PracticeModifiersNew      As Boolean

Private Sub Class_Initialize()

    Set PracticeModifiersTbl = CreateAdoRecordset
    InitModifierRule

End Sub

Private Sub Class_Terminate()

    InitModifierRule
    Set PracticeModifiersTbl = Nothing

End Sub

Public Function FindModifierRule() As Long

    FindModifierRule = GetModifierRule

End Function

Private Function GetModifierRule() As Long

Dim Ref         As String
Dim OrderString As String

    On Error GoTo lGetModifierRule_Error

    PracticeModifiersTotal = 0
    GetModifierRule = -1
    OrderString = "Select * FROM ModifierRules WHERE "
    If LenB(Trim$(ModifierClassId)) Then
        OrderString = OrderString & "BusinessClassId = '" & UCase$(Trim$(ModifierClassId)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(ModifierCPT)) Then
        OrderString = OrderString + Ref & "CPT = '" & UCase$(Trim$(ModifierCPT)) & "' "
        Ref = "And "
    End If
    OrderString = OrderString & "ORDER BY BusinessClassId ASC, CPT ASC "
    Set PracticeModifiersTbl = Nothing
    Set PracticeModifiersTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PracticeModifiersTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PracticeModifiersTbl.EOF Then
        InitModifierRule
    Else 'PRACTICEMODIFIERSTBL.EOF = FALSE/0
        With PracticeModifiersTbl
            .MoveLast
            PracticeModifiersTotal = .RecordCount
            GetModifierRule = .RecordCount
        End With 'PracticeModifiersTbl
    End If

    Exit Function

lGetModifierRule_Error:

    LogDBError "PracticeModifierRules", "GetModifierRule", Err, Err.Description

End Function

Private Sub InitModifierRule()

    PracticeModifiersNew = True
    ModifierClassId = vbNullString
    ModifierCPT = vbNullString
    ModifierRule1 = vbNullString
    ModifierRule1Criteria = vbNullString
    ModifierRule2 = vbNullString
    ModifierRule2Criteria = vbNullString
    ModifierRule3 = vbNullString
    ModifierRule3Criteria = vbNullString
    ModifierRule4 = vbNullString
    ModifierRule4Criteria = vbNullString
    ModifierRule5 = vbNullString
    ModifierRule5Criteria = vbNullString
    ModifierRule6 = vbNullString
    ModifierRule6Criteria = vbNullString
    ModifierRule7 = vbNullString
    ModifierRule7Criteria = vbNullString
    ModifierRule8 = vbNullString
    ModifierRule8Criteria = vbNullString
    ModifierRule9 = vbNullString
    ModifierRule9Criteria = vbNullString
    ModifierRule10 = vbNullString
    ModifierRule10Criteria = vbNullString
    ModifierRule11 = vbNullString
    ModifierRule11Criteria = vbNullString
    ModifierRule12 = vbNullString
    ModifierRule12Criteria = vbNullString
    ModifierRule13 = vbNullString
    ModifierRule13Criteria = vbNullString
    ModifierRule14 = vbNullString
    ModifierRule14Criteria = vbNullString
    ModifierRule15 = vbNullString
    ModifierRule15Criteria = vbNullString
    ModifierRuleStatus = "A"

End Sub

Private Sub LoadModifierRule()

    PracticeModifiersNew = False
    ModifierId = PracticeModifiersTbl("RefId")
    If LenB(PracticeModifiersTbl("BusinessClassId")) Then
        ModifierClassId = PracticeModifiersTbl("BusinessClassId")
    Else
        ModifierClassId = vbNullString
    End If
    If LenB(PracticeModifiersTbl("CPT")) Then
        ModifierCPT = PracticeModifiersTbl("CPT")
    Else
        ModifierCPT = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule1")) Then
        ModifierRule1 = PracticeModifiersTbl("Rule1")
    Else
        ModifierRule1 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description1")) Then
        ModifierRule1Criteria = PracticeModifiersTbl("Description1")
    Else
        ModifierRule1Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule2")) Then
        ModifierRule2 = PracticeModifiersTbl("Rule2")
    Else
        ModifierRule2 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description2")) Then
        ModifierRule2Criteria = PracticeModifiersTbl("Description2")
    Else
        ModifierRule2Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule3")) Then
        ModifierRule3 = PracticeModifiersTbl("Rule3")
    Else
        ModifierRule3 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description3")) Then
        ModifierRule3Criteria = PracticeModifiersTbl("Description3")
    Else
        ModifierRule3Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule4")) Then
        ModifierRule4 = PracticeModifiersTbl("Rule4")
    Else
        ModifierRule4 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description4")) Then
        ModifierRule4Criteria = PracticeModifiersTbl("Description4")
    Else
        ModifierRule4Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule5")) Then
        ModifierRule5 = PracticeModifiersTbl("Rule5")
    Else
        ModifierRule5 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description5")) Then
        ModifierRule5Criteria = PracticeModifiersTbl("Description5")
    Else
        ModifierRule5Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule6")) Then
        ModifierRule6 = PracticeModifiersTbl("Rule6")
    Else
        ModifierRule6 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description6")) Then
        ModifierRule6Criteria = PracticeModifiersTbl("Description6")
    Else
        ModifierRule6Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule7")) Then
        ModifierRule7 = PracticeModifiersTbl("Rule7")
    Else
        ModifierRule7 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description7")) Then
        ModifierRule7Criteria = PracticeModifiersTbl("Description7")
    Else
        ModifierRule7Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule8")) Then
        ModifierRule8 = PracticeModifiersTbl("Rule8")
    Else
        ModifierRule8 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description8")) Then
        ModifierRule8Criteria = PracticeModifiersTbl("Description8")
    Else
        ModifierRule8Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule9")) Then
        ModifierRule9 = PracticeModifiersTbl("Rule9")
    Else
        ModifierRule9 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description9")) Then
        ModifierRule9Criteria = PracticeModifiersTbl("Description9")
    Else
        ModifierRule9Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule10")) Then
        ModifierRule10 = PracticeModifiersTbl("Rule10")
    Else
        ModifierRule10 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description10")) Then
        ModifierRule10Criteria = PracticeModifiersTbl("Description10")
    Else
        ModifierRule10Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule11")) Then
        ModifierRule11 = PracticeModifiersTbl("Rule11")
    Else
        ModifierRule11 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description11")) Then
        ModifierRule11Criteria = PracticeModifiersTbl("Description11")
    Else
        ModifierRule11Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule12")) Then
        ModifierRule12 = PracticeModifiersTbl("Rule12")
    Else
        ModifierRule12 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description12")) Then
        ModifierRule12Criteria = PracticeModifiersTbl("Description12")
    Else
        ModifierRule12Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule13")) Then
        ModifierRule13 = PracticeModifiersTbl("Rule13")
    Else
        ModifierRule13 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description13")) Then
        ModifierRule13Criteria = PracticeModifiersTbl("Description13")
    Else
        ModifierRule13Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule14")) Then
        ModifierRule14 = PracticeModifiersTbl("Rule14")
    Else
        ModifierRule14 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description14")) Then
        ModifierRule14Criteria = PracticeModifiersTbl("Description14")
    Else
        ModifierRule14Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Rule15")) Then
        ModifierRule15 = PracticeModifiersTbl("Rule15")
    Else
        ModifierRule15 = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Description15")) Then
        ModifierRule15Criteria = PracticeModifiersTbl("Description15")
    Else
        ModifierRule15Criteria = vbNullString
    End If
    If LenB(PracticeModifiersTbl("Status")) Then
        ModifierRuleStatus = PracticeModifiersTbl("Status")
    Else
        ModifierRuleStatus = 0
    End If

End Sub

Public Function SelectModifierRule(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectModifierRule_Error

    SelectModifierRule = False
    If PracticeModifiersTbl.EOF Or (ListItem < 1) Or (ListItem > PracticeModifiersTotal) Then
        Exit Function
    End If
    PracticeModifiersTbl.MoveFirst
    PracticeModifiersTbl.Move ListItem - 1
    SelectModifierRule = True
    LoadModifierRule

    Exit Function

lSelectModifierRule_Error:

    LogDBError "PracticeModifierRules", "SelectModifierRule", Err, Err.Description

End Function

