VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientClinical"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Clinical Object Module
Option Explicit
Public ClinicalId               As Long
Public PatientId                As Long
Public AppointmentId            As Long
Public ClinicalType             As String
Public EyeContext               As String
Public Symptom                  As String
Public Findings                 As String
Public ImageDescriptor          As String
Public ImageInstructions        As String
Public ClinicalDraw             As String
Public ClinicalStatus           As String
Public ClinicalHighlights       As String
Public ClinicalFollowup         As String
Public ClinicalPermanent        As String
Public ClinicalPostOpPeriod     As Long
Private ModuleName              As String
Private PatientClinicalTotal    As Long
Private PatientClinicalTbl      As ADODB.Recordset
Private PatientClinicalNew      As Boolean

Public Function ApplyPatientClinical() As Boolean

    ApplyPatientClinical = PutPatientClinical

End Function

Private Sub Class_Initialize()

    Set PatientClinicalTbl = CreateAdoRecordset
    InitPatientClinical

End Sub

Private Sub Class_Terminate()

    InitPatientClinical
    Set PatientClinicalTbl = Nothing

End Sub

Public Function FindPatientClinical() As Long

    FindPatientClinical = GetListPatientClinical(0)

End Function

Private Function GetListPatientClinical(ByVal IType As Integer) As Long

Dim w           As Integer
Dim Temp        As String
Dim Ref         As String
Dim OrderString As String

    On Error GoTo lGetListPatientClinical_Error

    Ref = vbNullString
    PatientClinicalTotal = 0
    GetListPatientClinical = -1
    If PatientId < 1 Then
        If AppointmentId < 1 Then
            If LenB(Trim$(ClinicalType)) = 0 Then
                Exit Function
            End If
        End If
    End If
    OrderString = "Select * FROM PatientClinical WHERE "
    If PatientId > 0 Then
        OrderString = OrderString + Ref & "PatientId =" & Str$(PatientId) & " "
        Ref = "And "
    End If
    If AppointmentId > 0 Then
        If IType = 6 Then
            OrderString = OrderString + Ref & "AppointmentId <=" & Str$(AppointmentId) & " "
        Else 'NOT ITYPE...
            OrderString = OrderString + Ref & "AppointmentId =" & Str$(AppointmentId) & " "
        End If
        Ref = "And "
    End If
    If LenB(Trim$(ClinicalType)) Then
        If Len(ClinicalType) = 1 Then
            OrderString = OrderString + Ref & "ClinicalType = '" & UCase$(Trim$(ClinicalType)) & "' "
            Ref = "And "
        Else 'NOT LEN(CLINICALTYPE)...
            Temp = vbNullString
            For w = 1 To Len(ClinicalType)
                If w = Len(ClinicalType) Then
                    Temp = Temp & "ClinicalType = '" & UCase$(Mid$(ClinicalType, w, 1)) & "') "
                ElseIf (w = 1) Then 'NOT W...
                    Temp = Temp & "(ClinicalType = '" & UCase$(Mid$(ClinicalType, w, 1)) & "' Or "
                Else 'NOT (W...
                    Temp = Temp & "ClinicalType = '" & UCase$(Mid$(ClinicalType, w, 1)) & "' Or "
                End If
            Next w
            OrderString = OrderString + Ref + Temp
            Ref = "And "
        End If
    End If
    If LenB(Trim$(ClinicalPermanent)) Then
        OrderString = OrderString + Ref & "PermanentCondition = '" & UCase$(Trim$(ClinicalPermanent)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(EyeContext)) Then
        OrderString = OrderString + Ref & "EyeContext = '" & UCase$(Trim$(EyeContext)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(Symptom)) Then
        OrderString = OrderString + Ref & "Symptom = '" & UCase$(Trim$(Symptom)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(Findings)) Then
        OrderString = OrderString + Ref & "FindingDetail = '" & UCase$(Trim$(Findings)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(ClinicalStatus)) Then
        If (Left$(ClinicalStatus, 1) = "-") Then
            OrderString = OrderString + Ref & "Status <> '" & Mid$(ClinicalStatus, 2, 1) & "' "
        ElseIf (ClinicalStatus = "]") Then 'NOT (LEFT$(CLINICALSTATUS,...
            OrderString = OrderString + Ref & "Status = ']' "
        Else 'NOT (CLINICALSTATUS...
            OrderString = OrderString + Ref & "Status <> 'D' "
        End If
    End If
    If IType = 9 Then
        If LenB(Trim$(ImageDescriptor)) Then
            OrderString = OrderString + Ref & "ImageDescriptor = '" & UCase$(Trim$(ImageDescriptor)) & "' "
            Ref = "And "
        End If
    Else 'NOT ITYPE...
        If LenB(Trim$(ImageDescriptor)) Then
            OrderString = OrderString + Ref & "ImageDescriptor = '" & UCase$(Trim$(ImageDescriptor)) & "' "
            Ref = "And "
        End If
    End If
    If LenB(Trim$(ClinicalDraw)) Then
        OrderString = OrderString + Ref & "DrawFileName = '" & UCase$(Trim$(ClinicalDraw)) & "' "
        Ref = "And "
        ClinicalDraw = vbNullString
    End If
    If IType = 0 Then
        OrderString = OrderString & "ORDER BY ClinicalId ASC "
    End If
    If IType = 1 Then
        OrderString = OrderString & "ORDER BY ClinicalId DESC "
    End If
    If IType = 6 Then
        OrderString = OrderString & "ORDER BY ClinicalId ASC "
    End If
    If IType = 7 Then
        OrderString = OrderString & "ORDER BY EyeContext ASC, Symptom ASC, ClinicalId ASC "
    End If
    If IType = 2 Then
        OrderString = OrderString & "ORDER BY AppointmentId DESC, ClinicalId ASC "
    End If
    Set PatientClinicalTbl = Nothing
    Set PatientClinicalTbl = CreateAdoRecordset
    PatientClinicalTbl.CursorLocation = adUseClient
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PatientClinicalTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PatientClinicalTbl.EOF Then
        GetListPatientClinical = -1
    Else 'PATIENTCLINICALTBL.EOF = FALSE/0
        PatientClinicalTotal = PatientClinicalTbl.RecordCount
        GetListPatientClinical = PatientClinicalTotal
    End If

    Exit Function

lGetListPatientClinical_Error:

    LogDBError "PatientClinical", "GetListPatientClinical", Err, Err.Description

End Function

Private Function GetPatientClinical() As Boolean

Dim OrderString As String

    On Error GoTo lGetPatientClinical_Error

    GetPatientClinical = True
    OrderString = "SELECT * FROM PatientClinical WHERE ClinicalId =" & Str$(ClinicalId)
    Set PatientClinicalTbl = Nothing
    Set PatientClinicalTbl = CreateAdoRecordset
    PatientClinicalTbl.CursorLocation = adUseClient
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PatientClinicalTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PatientClinicalTbl.EOF Then
        InitPatientClinical
    Else 'PATIENTCLINICALTBL.EOF = FALSE/0
        LoadPatientClinical
    End If

    Exit Function

lGetPatientClinical_Error:

    LogDBError "PatientClinical", "GetPatientClinical", Err, Err.Description

End Function

Private Sub InitPatientClinical()

    PatientClinicalNew = True
    ClinicalId = 0
    AppointmentId = 0
    PatientId = 0
    ClinicalType = vbNullString
    EyeContext = vbNullString
    Symptom = vbNullString
    Findings = vbNullString
    ImageDescriptor = vbNullString
    ImageInstructions = vbNullString
    ClinicalStatus = "A"
    ClinicalDraw = vbNullString
    ClinicalHighlights = vbNullString
    ClinicalFollowup = vbNullString
    ClinicalPermanent = vbNullString
    ClinicalPostOpPeriod = 0

End Sub

Private Sub LoadPatientClinical()

    InitPatientClinical
    PatientClinicalNew = False
    If LenB(PatientClinicalTbl("ClinicalId")) Then
        ClinicalId = PatientClinicalTbl("ClinicalId")
    End If
    If LenB(PatientClinicalTbl("AppointmentId")) Then
        AppointmentId = PatientClinicalTbl("AppointmentId")
    End If
    If LenB(PatientClinicalTbl("PatientId")) Then
        PatientId = PatientClinicalTbl("PatientId")
    End If
    If LenB(PatientClinicalTbl("ClinicalType")) Then
        ClinicalType = PatientClinicalTbl("ClinicalType")
    End If
    If LenB(PatientClinicalTbl("EyeContext")) Then
        EyeContext = PatientClinicalTbl("EyeContext")
    End If
    If LenB(PatientClinicalTbl("Symptom")) Then
        Symptom = PatientClinicalTbl("Symptom")
    End If
    If LenB(PatientClinicalTbl("FindingDetail")) Then
        Findings = PatientClinicalTbl("FindingDetail")
    End If
    If LenB(PatientClinicalTbl("ImageDescriptor")) Then
        ImageDescriptor = PatientClinicalTbl("ImageDescriptor")
    End If
    If LenB(PatientClinicalTbl("ImageInstructions")) Then
        ImageInstructions = PatientClinicalTbl("ImageInstructions")
    End If
    If LenB(PatientClinicalTbl("Status")) Then
        ClinicalStatus = PatientClinicalTbl("Status")
    End If
    If LenB(PatientClinicalTbl("DrawFileName")) Then
        ClinicalDraw = PatientClinicalTbl("DrawFileName")
    End If
    If LenB(PatientClinicalTbl("Highlights")) Then
        ClinicalHighlights = PatientClinicalTbl("Highlights")
    End If
    If LenB(PatientClinicalTbl("Followup")) Then
        ClinicalFollowup = PatientClinicalTbl("Followup")
    End If
    If LenB(PatientClinicalTbl("PermanentCondition")) Then
        ClinicalPermanent = PatientClinicalTbl("PermanentCondition")
    End If
    If LenB(PatientClinicalTbl("PostOpPeriod")) Then
        ClinicalPostOpPeriod = PatientClinicalTbl("PostOpPeriod")
    End If

End Sub

Private Function PutPatientClinical() As Boolean

    On Error GoTo lPutPatientClinical_Error

    PutPatientClinical = True
    If PatientClinicalNew Or (ClinicalId < 1) Then
        PatientClinicalTbl.AddNew
    End If
    PatientClinicalTbl("AppointmentId") = AppointmentId
    PatientClinicalTbl("PatientId") = PatientId
    PatientClinicalTbl("ClinicalType") = UCase$(Trim$(ClinicalType))
    PatientClinicalTbl("EyeContext") = UCase$(Trim$(EyeContext))
    PatientClinicalTbl("Symptom") = UCase$(Trim$(Symptom))
    PatientClinicalTbl("FindingDetail") = UCase$(Trim$(Findings))
    PatientClinicalTbl("ImageDescriptor") = UCase$(Trim$(ImageDescriptor))
    PatientClinicalTbl("ImageInstructions") = UCase$(Trim$(ImageInstructions))
    PatientClinicalTbl("DrawFileName") = Trim$(ClinicalDraw)
    PatientClinicalTbl("Highlights") = ClinicalHighlights
    PatientClinicalTbl("Followup") = ClinicalFollowup
    PatientClinicalTbl("PermanentCondition") = ClinicalPermanent
    PatientClinicalTbl("Status") = ClinicalStatus
    PatientClinicalTbl("PostOpPeriod") = ClinicalPostOpPeriod
    PatientClinicalTbl.Update
    If PatientClinicalNew Then
        PatientClinicalNew = False
    End If

    Exit Function

lPutPatientClinical_Error:

    LogDBError "PatientClinical", "PutPatientClinical", Err, Err.Description

End Function

Public Function RetrievePatientClinical() As Boolean

    RetrievePatientClinical = GetPatientClinical

End Function

