VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicLanguage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicLanguage Object Module
Option Explicit
Public LanguageId               As Long
Public Language                 As String
Public ControlId                As Long
Public ControlText              As String
Public ConfirmLingo             As String
Private ModuleName              As String
Private ClinicalLanguageTbl     As ADODB.Recordset
Private DynamicLanguageTotal    As Long
Private ClinicalLanguageNew     As Boolean

Private Sub Class_Initialize()

    Set ClinicalLanguageTbl = New ADODB.Recordset
    ClinicalLanguageTbl.CursorLocation = adUseClient
    InitLanguageControl

End Sub

Private Sub Class_Terminate()

    InitLanguageControl
     Set ClinicalLanguageTbl = Nothing
End Sub

Public Function FindLanguageControl() As Long

    FindLanguageControl = GetLanguageControlExclusive()

End Function

Private Function GetLanguageControlExclusive() As Long

Dim Ref         As String
Dim OrderString As String

    On Error GoTo lGetLanguageControlExclusive_Error

    DynamicLanguageTotal = 0
    Ref = vbNullString
    GetLanguageControlExclusive = -1
    If ControlId > 0 Then
        OrderString = "SELECT * FROM FormsLanguage WHERE "
        If ControlId > 0 Then
            OrderString = OrderString & "ControlId = " & Trim$(Str$(ControlId)) & " "
            Ref = "And "
        End If
        If LenB(Trim$(Language)) Then
            OrderString = OrderString + Ref & "Language = '" & Trim$(Language) & "' "
        Else 'LENB(TRIM$(LANGUAGE)) = FALSE/0
            OrderString = OrderString + Ref & "Language = 'English' "
        End If
        OrderString = OrderString & "ORDER BY Language ASC, ControlId ASC "
        Set ClinicalLanguageTbl = Nothing
        Set ClinicalLanguageTbl = New ADODB.Recordset
        MyDynamicFormsCmd.CommandText = OrderString
        Call ClinicalLanguageTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
        If Not ClinicalLanguageTbl.EOF Then
            DynamicLanguageTotal = ClinicalLanguageTbl.RecordCount
            GetLanguageControlExclusive = ClinicalLanguageTbl.RecordCount
        End If
    End If

    Exit Function

lGetLanguageControlExclusive_Error:

    LogDBError "DynamicLanguage", "GetLanguageControlExclusive", Err, Err.Description

End Function

Private Sub InitLanguageControl()

    ClinicalLanguageNew = False
    Language = vbNullString
    ControlId = 0
    ControlText = vbNullString
    ConfirmLingo = vbNullString

End Sub

Private Sub LoadLanguageControl()

    InitLanguageControl
    LanguageId = ClinicalLanguageTbl("LanguageId")
    If LenB(ClinicalLanguageTbl("Language")) Then
        Language = ClinicalLanguageTbl("Language")
    End If
    If LenB(ClinicalLanguageTbl("ControlId")) Then
        ControlId = ClinicalLanguageTbl("ControlId")
    End If
    If LenB(ClinicalLanguageTbl("ControlText")) Then
        ControlText = ClinicalLanguageTbl("ControlText")
    End If
    If LenB(ClinicalLanguageTbl("ConfirmLingo")) Then
        ConfirmLingo = ClinicalLanguageTbl("ConfirmLingo")
    End If

End Sub

Public Function SelectLanguageControl(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectLanguageControl_Error

    SelectLanguageControl = False
    If Not ClinicalLanguageTbl.EOF Then
        If ListItem > 0 Then
            If ListItem <= DynamicLanguageTotal Then
                ClinicalLanguageTbl.Move (ListItem - ClinicalLanguageTbl.AbsolutePosition)
                SelectLanguageControl = True
                LoadLanguageControl
            End If
        End If
    End If

    Exit Function

lSelectLanguageControl_Error:

    LogDBError "DynamicLanguage", "SelectLanguageControl", Err, Err.Description

End Function

