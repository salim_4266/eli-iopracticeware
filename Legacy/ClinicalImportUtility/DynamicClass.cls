VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicClass Object Module
Option Explicit
Public ClassId                  As Long
Public QuestionSet              As String
Public QuestionParty            As String
Public Question                 As String
Public QuestionOrder            As String
Public ControlName              As String
Public IEQuestion               As String
Public IEQuestionType           As String
Public QuestionFamily           As Integer
Public TimeOut                  As Integer
Public MergeId                  As Integer
Public Highlight                As String
Public FollowUp                 As String
Public QuestionIndicator        As String
Public DIPrintOrder             As Integer
Private ModuleName              As String
Private DynamicClassTotal       As Integer
Private ClassQuestionsTbl       As ADODB.Recordset
Private ClassQuestionsNew       As Boolean

Private Sub Class_Initialize()

    Set ClassQuestionsTbl = New ADODB.Recordset
    ClassQuestionsTbl.CursorLocation = adUseClient
    InitClass

End Sub

Private Sub Class_Terminate()

    InitClass
    Set ClassQuestionsTbl = Nothing
End Sub

Public Function FindClassForms() As Long

    FindClassForms = GetAllClassForms(0)

End Function

Private Function GetAllClassForms(ByVal IType As Integer) As Long

Dim OrderString As String

    On Error GoTo lGetAllClassForms_Error

    DynamicClassTotal = 0
    GetAllClassForms = -1
    If LenB(Trim$(QuestionParty)) Then
        OrderString = "SELECT * FROM Questions WHERE QuestionParty = '" & UCase$(Trim$(QuestionParty)) & "' "
        If LenB(Trim$(QuestionSet)) Then
            OrderString = OrderString & "And QuestionSet = '" & Trim$(QuestionSet) & "' "
        End If
        If LenB(Trim$(QuestionOrder)) Then
            OrderString = OrderString & "And QuestionOrder = '" & Trim$(QuestionOrder) & "' "
        End If
        If IType = 0 Then
            OrderString = OrderString & "ORDER BY QuestionOrder ASC "
        Else 'NOT ITYPE...
            OrderString = OrderString & "ORDER BY Question ASC "
        End If
        MyDynamicFormsCmd.CommandText = OrderString
        Set ClassQuestionsTbl = Nothing
        Set ClassQuestionsTbl = New ADODB.Recordset
        Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
        If ClassQuestionsTbl.EOF Then
            GetAllClassForms = -1
        Else 'CLASSQUESTIONSTBL.EOF = FALSE/0
            DynamicClassTotal = ClassQuestionsTbl.RecordCount
            GetAllClassForms = ClassQuestionsTbl.RecordCount
        End If
    End If

    Exit Function

lGetAllClassForms_Error:

    LogDBError "DynamicClass", "GetAllClassForms", Err, Err.Description

End Function

Private Sub InitClass()

    ClassQuestionsNew = False
    QuestionParty = vbNullString
    Question = vbNullString
    QuestionOrder = vbNullString
    QuestionSet = vbNullString
    ControlName = vbNullString
    IEQuestion = vbNullString
    IEQuestionType = vbNullString
    QuestionFamily = 0
    TimeOut = 0
    MergeId = 0
    Highlight = vbNullString
    QuestionIndicator = vbNullString
    FollowUp = vbNullString
    DIPrintOrder = 0

End Sub

Private Sub LoadClass()

    InitClass
    ClassId = ClassQuestionsTbl("ClassId")
    If LenB(ClassQuestionsTbl("QuestionParty")) Then
        QuestionParty = ClassQuestionsTbl("QuestionParty")
    End If
    If LenB(ClassQuestionsTbl("Question")) Then
        Question = ClassQuestionsTbl("Question")
    End If
    If LenB(ClassQuestionsTbl("QuestionOrder")) Then
        QuestionOrder = ClassQuestionsTbl("QuestionOrder")
    End If
    If LenB(ClassQuestionsTbl("QuestionSet")) Then
        QuestionSet = ClassQuestionsTbl("QuestionSet")
    End If
    If LenB(ClassQuestionsTbl("ControlName")) Then
        ControlName = ClassQuestionsTbl("ControlName")
    End If
    If LenB(ClassQuestionsTbl("InferenceEngineQuestion")) Then
        IEQuestion = ClassQuestionsTbl("InferenceEngineQuestion")
    End If
    If LenB(ClassQuestionsTbl("InferenceEngineQuestionType")) Then
        IEQuestionType = ClassQuestionsTbl("InferenceEngineQuestionType")
    End If
    If LenB(ClassQuestionsTbl("Family")) Then
        QuestionFamily = ClassQuestionsTbl("Family")
    End If
    If LenB(ClassQuestionsTbl("Timeout")) Then
        TimeOut = ClassQuestionsTbl("Timeout")
    End If
    If LenB(ClassQuestionsTbl("MergeId")) Then
        MergeId = ClassQuestionsTbl("MergeId")
    End If
    If LenB(ClassQuestionsTbl("Highlight")) Then
        Highlight = ClassQuestionsTbl("Highlight")
    End If
    If LenB(ClassQuestionsTbl("ProcOrTest")) Then
        QuestionIndicator = ClassQuestionsTbl("ProcOrTest")
    End If
    If LenB(ClassQuestionsTbl("Followup")) Then
        FollowUp = ClassQuestionsTbl("Followup")
    End If
    If LenB(ClassQuestionsTbl("PrintOrder")) Then
        DIPrintOrder = ClassQuestionsTbl("PrintOrder")
    End If

End Sub

Public Function SelectClassForm(ByVal ListItem As Integer) As Boolean

    On Error GoTo lSelectClassForm_Error

    If Not ClassQuestionsTbl.EOF Then
        If ListItem > 0 Then
            If ListItem <= DynamicClassTotal Then
                ClassQuestionsTbl.Move ListItem - ClassQuestionsTbl.AbsolutePosition
                LoadClass
                SelectClassForm = True
            End If
        End If
    End If

    Exit Function

lSelectClassForm_Error:

    LogDBError "DynamicClass", "SelectClassForm", Err, Err.Description

End Function

