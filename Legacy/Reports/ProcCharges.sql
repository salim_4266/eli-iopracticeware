

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec ProcCharges '20120931','20120901'

IF OBJECT_ID('[dbo].[ProcCharges]') IS NOT NULL
	DROP PROCEDURE [dbo].[ProcCharges]
GO

CREATE       Procedure [dbo].[ProcCharges]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

/**** Object:  StoredProcedure [dbo].[ProcCharges]    Script Date: 12/10/2012 2:51:48 PM ******/

SET NOCOUNT ON

select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.service, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.billingoffice, pr.billtodr, pr.invoice,
		prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype,
		re.resourcedescription
into #Table1
from patientreceivableservices prs
left outer join patientreceivables pr on pr.invoice = prs.invoice
left outer join patientreceivablepayments prp on prp.paymentserviceitem = prs.itemid
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join resources re on re.resourceid = pr.billtodr
where
prs.status = 'a'
and prs.servicedate between @enddate and @startdate
and pr.insurerdesignation = 't'


--Getting all itemids where insurerdesignation = F and no T receivable exists
select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.service, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.billingoffice, pr.billtodr, pr.invoice,
		prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype,
		re.resourcedescription
into #TableInsDesF
from patientreceivableservices prs
left outer join patientreceivables pr on pr.invoice = prs.invoice
left outer join patientreceivablepayments prp on prp.paymentserviceitem = prs.itemid
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join resources re on re.resourceid = pr.billtodr
where
prs.status = 'a'
and prs.servicedate between @enddate and @startdate
and pr.InsurerDesignation = 'F'
and prs.itemid not in (select distinct itemid from #table1)

--Inserting missing services and payments
Insert into #Table1
Select * from #TableInsDesF



CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, practicename + ' - ' + LocationReference as LocationDescription
from practicename 
where practicetype = 'P'
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = 'R'
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''
and BillingOffice = 'T'

--select * from #Locations
--drop table #Locations


select	distinct lastname, firstname, patientid,
		itemid, service, modifier, quantity, charge, servicedate,
		billingoffice, billtodr, invoice,
		resourcedescription,
		convert(nvarchar(250),0) as LocationDescription,
		convert(Decimal(38,2),0) as InsPaid,
		convert(Decimal(38,2),0) as InsAdj,
		convert(Decimal(38,2),0) as AdjDebit,
		convert(Decimal(38,2),0) as PatPaid
into #Table2
from #Table1
group by lastname, firstname, patientid, itemid, service, modifier, quantity, charge, billingoffice, billtodr, invoice, servicedate,
		resourcedescription


update #Table2 
set  #Table2.LocationDescription = #Locations.LocationDescription
from #Locations ,#Table2
where #Table2.billingoffice = #Locations.LocationId

--select * from #Table2

--drop table #Table1
--drop table #Table2

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, itemid
into #InsPaid 
from  #Table1 
where PaymentType = 'P' and PayerType = 'I'
Group by itemid

update #Table2 
set  #Table2.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Table2
where #Table2.itemid = #InsPaid.itemid

--Patient Payment
select sum(paymentamount) as PatPaid, itemid
into #PatPaid 
from  #Table1 
where PaymentType = 'P' and PayerType = 'P'
Group by itemid

update #Table2 
set  #Table2.PatPaid = #PatPaid.PatPaid
from #PatPaid ,#Table2
where #Table2.itemid = #PatPaid.itemid

--drop table #InsAdj

--Insurance Adjustments
select sum(paymentamount) as InsAdj,itemid
into #InsAdj 
from  #Table1 
where PaymentFinancialType = 'C' And PaymentType <> 'P'
Group by itemid

update #Table2 
set  #Table2.InsAdj = #InsAdj.InsAdj
from #InsAdj ,#Table2
where #Table2.itemid = #InsAdj.itemid



--drop table #AdjustmentDebit

---Adjustments for Debit
select sum(paymentamount) as AdjDebit,itemid
into #AdjustmentDebit 
from  #Table1
where PaymentFinancialType = 'D'
Group by itemid

update #Table2 
set  #Table2.AdjDebit = #AdjustmentDebit.AdjDebit
from #AdjustmentDebit ,#Table2
where #Table2.itemid = #AdjustmentDebit.itemid




--drop table #FinalTable


select  lastname, (firstname + ' - ' + convert(nvarchar,PatientId)) as firstname , patientid, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName,
		itemid, ps.codecategory, service, Modifier, ps.Description, quantity, sum(charge*quantity) as TotalCharge, #Table2.billingoffice, billtodr, invoice,
		servicedate, 
		resourcedescription, Locationdescription,
		InsPaid, InsAdj, AdjDebit, PatPaid, sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit) - PatPaid) as Balance,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip
into #FinalTable
from #Table2
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '' and pn.BillingOffice = 'T'
left outer join PracticeServices ps on ps.Code = #Table2.service
group by lastname, firstname, patientid, itemid, ps.CodeCategory, service, Description, modifier, quantity, #Table2.billingoffice, billtodr, invoice,
		servicedate, resourcedescription, locationdescription, InsPaid, InsAdj, AdjDebit, PatPaid,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip



select * from #FinalTable
order by ResourceDescription, LocationDescription, codecategory, service

Drop table #Table2

Drop table #Table1

SET NOCOUNT OFF



