Imports System.Data.SqlClient
<System.Runtime.InteropServices.ComVisible(False)> _
Public Class frmCMSScript
    Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New(ByVal iCMSRepId As Integer, ByVal strCMSRepName As String, ByVal strCMSQuery As String)
        MyBase.New()
        CMSRepId = iCMSRepId
        CMSRepName = strCMSRepName
        CMSQuery = strCMSQuery
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlReportDetails As System.Windows.Forms.Panel
    Friend WithEvents txtRepName As System.Windows.Forms.TextBox
    Friend WithEvents lblScript As System.Windows.Forms.Label
    Friend WithEvents btnSaveScript As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblScript = New System.Windows.Forms.Label
        Me.pnlReportDetails = New System.Windows.Forms.Panel
        Me.txtRepName = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnSaveScript = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlReportDetails.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblScript
        '
        Me.lblScript.BackColor = System.Drawing.Color.MediumAquamarine
        Me.lblScript.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblScript.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScript.ForeColor = System.Drawing.Color.DarkRed
        Me.lblScript.Location = New System.Drawing.Point(0, 0)
        Me.lblScript.Name = "lblScript"
        Me.lblScript.Size = New System.Drawing.Size(913, 24)
        Me.lblScript.TabIndex = 3
        Me.lblScript.Text = "CMS Reports"
        Me.lblScript.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlReportDetails
        '
        Me.pnlReportDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReportDetails.Controls.Add(Me.txtRepName)
        Me.pnlReportDetails.Controls.Add(Me.Panel1)
        Me.pnlReportDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlReportDetails.Location = New System.Drawing.Point(0, 24)
        Me.pnlReportDetails.Name = "pnlReportDetails"
        Me.pnlReportDetails.Size = New System.Drawing.Size(913, 515)
        Me.pnlReportDetails.TabIndex = 4
        '
        'txtRepName
        '
        Me.txtRepName.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtRepName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRepName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRepName.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRepName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtRepName.Location = New System.Drawing.Point(0, 0)
        Me.txtRepName.MaxLength = 0
        Me.txtRepName.Multiline = True
        Me.txtRepName.Name = "txtRepName"
        Me.txtRepName.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRepName.Size = New System.Drawing.Size(911, 481)
        Me.txtRepName.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.btnSaveScript)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 481)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(911, 32)
        Me.Panel1.TabIndex = 13
        '
        'btnSaveScript
        '
        Me.btnSaveScript.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSaveScript.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnSaveScript.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveScript.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSaveScript.Location = New System.Drawing.Point(686, 3)
        Me.btnSaveScript.Name = "btnSaveScript"
        Me.btnSaveScript.Size = New System.Drawing.Size(107, 27)
        Me.btnSaveScript.TabIndex = 3
        Me.btnSaveScript.Text = "Update Script"
        Me.btnSaveScript.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnClose.Location = New System.Drawing.Point(799, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(107, 27)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'frmCMSScript
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(913, 539)
        Me.Controls.Add(Me.pnlReportDetails)
        Me.Controls.Add(Me.lblScript)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCMSScript"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Script for "
        Me.TopMost = True
        Me.pnlReportDetails.ResumeLayout(False)
        Me.pnlReportDetails.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim CMSRepId As Integer
    Dim CMSRepName As String
    Dim CMSQuery As String
    Private UtilityProxy As New Utility
    Dim CONNECTION_STRING As String = UtilityProxy.ConnectionString
    Public Headername As String

    Private Sub frmCMSScript_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = Headername
        lblScript.Text = CMSRepName
        ShowCMSScript()
    End Sub
    Private Sub ShowCMSScript()
        Dim sqlConn As New SqlConnection(CONNECTION_STRING)
        Dim sqlComm As New SqlCommand("SP_HELPTEXT " & CMSQuery, sqlConn)
        Dim sqlRead As SqlDataReader
        Try
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            While sqlRead.Read
                txtRepName.Text &= sqlRead("Text")
            End While
            txtRepName.Text = txtRepName.Text.Replace("CREATE PROCEDURE", "ALTER PROCEDURE")
            txtRepName.Select(0, 0)
            txtRepName.Focus()
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub

    Private Sub btnSaveScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveScript.Click
        Dim sqlConn As New SqlConnection(CONNECTION_STRING)
        Dim sqlComm As New SqlCommand(txtRepName.Text, sqlConn)
        Try
            sqlConn.Open()
            sqlComm.ExecuteNonQuery()
            MsgBox("Script Updated successfullY", MsgBoxStyle.Information, "CMS Script Updation")
        Catch ex As Exception
            MsgBox(ex.Message & vbCrLf, MsgBoxStyle.Information, "CMS Script Updation")
        Finally
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
