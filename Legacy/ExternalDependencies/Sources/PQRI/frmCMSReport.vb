Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Text

Public Class frmCMSReport
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents cmbreportheader As System.Windows.Forms.ComboBox
    Friend WithEvents btnprint As System.Windows.Forms.Button
    Friend WithEvents lblCMSReport As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents lblcmsparameters As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnPQRIXml As System.Windows.Forms.Button
    Friend WithEvents cboSubMethod As System.Windows.Forms.ComboBox
    Friend WithEvents lblSubMethod As System.Windows.Forms.Label
    Friend WithEvents lblMsrGroup As System.Windows.Forms.Label
    Friend WithEvents cboGrpMeasure As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents pnlReportDetails As System.Windows.Forms.Panel
    Friend WithEvents btnCancelDetails As System.Windows.Forms.Button
    Friend WithEvents btnSaveDetails As System.Windows.Forms.Button
    Friend WithEvents txtRepCode As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDenominator As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNumerator As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSubHead As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtRepName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pnlcmsparams As System.Windows.Forms.Panel
    Friend WithEvents lsvCmsParams As CustomListView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnparamscancel As System.Windows.Forms.Button
    Friend WithEvents btnsavecmsparams As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents btnPrintImg As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtEndtime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtStartTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnProviders As System.Windows.Forms.PictureBox
    Friend WithEvents btnGenCSV As System.Windows.Forms.Button
    Friend WithEvents btnViewScript As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCMSReport))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnGenCSV = New System.Windows.Forms.Button
        Me.btnProviders = New System.Windows.Forms.PictureBox
        Me.dtEndtime = New System.Windows.Forms.DateTimePicker
        Me.dtStartTime = New System.Windows.Forms.DateTimePicker
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnPQRIXml = New System.Windows.Forms.Button
        Me.btnprint = New System.Windows.Forms.Button
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblMsrGroup = New System.Windows.Forms.Label
        Me.cboGrpMeasure = New System.Windows.Forms.ComboBox
        Me.lblSubMethod = New System.Windows.Forms.Label
        Me.cboSubMethod = New System.Windows.Forms.ComboBox
        Me.btnViewScript = New System.Windows.Forms.PictureBox
        Me.lblcmsparameters = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cmbreportheader = New System.Windows.Forms.ComboBox
        Me.lblCMSReport = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnPrintImg = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.pnlReportDetails = New System.Windows.Forms.Panel
        Me.btnCancelDetails = New System.Windows.Forms.Button
        Me.btnSaveDetails = New System.Windows.Forms.Button
        Me.txtRepCode = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtDenominator = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtNumerator = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtSubHead = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtRepName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnlcmsparams = New System.Windows.Forms.Panel
        Me.lsvCmsParams = New CustomListView
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.btnparamscancel = New System.Windows.Forms.Button
        Me.btnsavecmsparams = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.Panel1.SuspendLayout()
        CType(Me.btnProviders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnViewScript, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.pnlReportDetails.SuspendLayout()
        Me.pnlcmsparams.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1028, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "CMS Reports"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnGenCSV)
        Me.Panel1.Controls.Add(Me.btnProviders)
        Me.Panel1.Controls.Add(Me.dtEndtime)
        Me.Panel1.Controls.Add(Me.dtStartTime)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.btnPQRIXml)
        Me.Panel1.Controls.Add(Me.btnprint)
        Me.Panel1.Controls.Add(Me.lblStatus)
        Me.Panel1.Controls.Add(Me.lblMsrGroup)
        Me.Panel1.Controls.Add(Me.cboGrpMeasure)
        Me.Panel1.Controls.Add(Me.lblSubMethod)
        Me.Panel1.Controls.Add(Me.cboSubMethod)
        Me.Panel1.Controls.Add(Me.btnViewScript)
        Me.Panel1.Controls.Add(Me.lblcmsparameters)
        Me.Panel1.Controls.Add(Me.cmbreportheader)
        Me.Panel1.Controls.Add(Me.lblCMSReport)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1028, 126)
        Me.Panel1.TabIndex = 3
        '
        'btnGenCSV
        '
        Me.btnGenCSV.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnGenCSV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenCSV.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnGenCSV.Location = New System.Drawing.Point(837, 56)
        Me.btnGenCSV.Name = "btnGenCSV"
        Me.btnGenCSV.Size = New System.Drawing.Size(148, 29)
        Me.btnGenCSV.TabIndex = 19
        Me.btnGenCSV.Text = "Generate CSV"
        Me.btnGenCSV.UseVisualStyleBackColor = False
        '
        'btnProviders
        '
        Me.btnProviders.Image = CType(resources.GetObject("btnProviders.Image"), System.Drawing.Image)
        Me.btnProviders.Location = New System.Drawing.Point(780, 19)
        Me.btnProviders.Name = "btnProviders"
        Me.btnProviders.Size = New System.Drawing.Size(24, 24)
        Me.btnProviders.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnProviders.TabIndex = 18
        Me.btnProviders.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnProviders, "Select Doctors")
        '
        'dtEndtime
        '
        Me.dtEndtime.Location = New System.Drawing.Point(498, 58)
        Me.dtEndtime.Name = "dtEndtime"
        Me.dtEndtime.Size = New System.Drawing.Size(280, 20)
        Me.dtEndtime.TabIndex = 17
        Me.dtEndtime.Value = New Date(2010, 12, 31, 0, 0, 0, 0)
        '
        'dtStartTime
        '
        Me.dtStartTime.Location = New System.Drawing.Point(120, 56)
        Me.dtStartTime.Name = "dtStartTime"
        Me.dtStartTime.Size = New System.Drawing.Size(300, 20)
        Me.dtStartTime.TabIndex = 16
        Me.dtStartTime.Value = New Date(2010, 1, 1, 0, 0, 0, 0)
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label9.Location = New System.Drawing.Point(423, 50)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 36)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "End Date"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label8.Location = New System.Drawing.Point(24, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 36)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Start Date"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnPQRIXml
        '
        Me.btnPQRIXml.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPQRIXml.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnPQRIXml.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPQRIXml.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnPQRIXml.Location = New System.Drawing.Point(837, 93)
        Me.btnPQRIXml.Name = "btnPQRIXml"
        Me.btnPQRIXml.Size = New System.Drawing.Size(148, 29)
        Me.btnPQRIXml.TabIndex = 4
        Me.btnPQRIXml.Text = "Generate PQRS Xml"
        Me.btnPQRIXml.UseVisualStyleBackColor = False
        '
        'btnprint
        '
        Me.btnprint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnprint.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnprint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnprint.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnprint.Location = New System.Drawing.Point(837, 18)
        Me.btnprint.Name = "btnprint"
        Me.btnprint.Size = New System.Drawing.Size(148, 29)
        Me.btnprint.TabIndex = 3
        Me.btnprint.Text = "View Report"
        Me.btnprint.UseVisualStyleBackColor = False
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblStatus.Location = New System.Drawing.Point(803, 53)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(12, 16)
        Me.lblStatus.TabIndex = 13
        Me.lblStatus.Text = "."
        Me.lblStatus.Visible = False
        '
        'lblMsrGroup
        '
        Me.lblMsrGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsrGroup.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblMsrGroup.Location = New System.Drawing.Point(6, 83)
        Me.lblMsrGroup.Name = "lblMsrGroup"
        Me.lblMsrGroup.Size = New System.Drawing.Size(114, 36)
        Me.lblMsrGroup.TabIndex = 12
        Me.lblMsrGroup.Text = "Measure Group"
        Me.lblMsrGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMsrGroup.Visible = False
        '
        'cboGrpMeasure
        '
        Me.cboGrpMeasure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrpMeasure.Items.AddRange(New Object() {"Diabetes Mellitus", "Chronic Kidney Disease (CKD)", "Preventive Care", "Coronary Artery Bypass Graft (CABG)", "Rheumatoid Arthritis (RA)", "Perioperative Care", "Back Pain", "Hepatitis C", "Heart Failure (HF)", "Coronary Artery Disease (CAD)", "Ischemic ", "Vascular Disease (IVD)", "HIV/AIDS", "Community-Acquired Pneumonia (CAP)", "Asthma", "A - Diabetes Mellitis", "C - CKD", "D - Preventive Care", "E - Perioperative Care", "F - Rheumatoid Arthritis", "G - Back Pain", "H - CABG", "X - Not Applicable"})
        Me.cboGrpMeasure.Location = New System.Drawing.Point(122, 88)
        Me.cboGrpMeasure.Name = "cboGrpMeasure"
        Me.cboGrpMeasure.Size = New System.Drawing.Size(300, 21)
        Me.cboGrpMeasure.TabIndex = 11
        Me.cboGrpMeasure.Visible = False
        '
        'lblSubMethod
        '
        Me.lblSubMethod.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubMethod.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblSubMethod.Location = New System.Drawing.Point(428, 84)
        Me.lblSubMethod.Name = "lblSubMethod"
        Me.lblSubMethod.Size = New System.Drawing.Size(145, 36)
        Me.lblSubMethod.TabIndex = 10
        Me.lblSubMethod.Text = "Submission Method"
        Me.lblSubMethod.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSubMethod.Visible = False
        '
        'cboSubMethod
        '
        Me.cboSubMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubMethod.Items.AddRange(New Object() {"A - 12 months, 3 or more measures", "B - 6 months, 3 or more measures", "C - 12 months, 30 consecutive, measure group", "E - 12 months, measure group", "F - 6 months, measure group"})
        Me.cboSubMethod.Location = New System.Drawing.Point(568, 92)
        Me.cboSubMethod.Name = "cboSubMethod"
        Me.cboSubMethod.Size = New System.Drawing.Size(211, 21)
        Me.cboSubMethod.TabIndex = 9
        Me.cboSubMethod.Visible = False
        '
        'btnViewScript
        '
        Me.btnViewScript.Image = CType(resources.GetObject("btnViewScript.Image"), System.Drawing.Image)
        Me.btnViewScript.Location = New System.Drawing.Point(807, 90)
        Me.btnViewScript.Name = "btnViewScript"
        Me.btnViewScript.Size = New System.Drawing.Size(24, 24)
        Me.btnViewScript.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnViewScript.TabIndex = 8
        Me.btnViewScript.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnViewScript, "View Script")
        Me.btnViewScript.Visible = False
        '
        'lblcmsparameters
        '
        Me.lblcmsparameters.ImageIndex = 0
        Me.lblcmsparameters.ImageList = Me.ImageList1
        Me.lblcmsparameters.Location = New System.Drawing.Point(777, 90)
        Me.lblcmsparameters.Name = "lblcmsparameters"
        Me.lblcmsparameters.Size = New System.Drawing.Size(24, 24)
        Me.lblcmsparameters.TabIndex = 7
        Me.lblcmsparameters.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        '
        'cmbreportheader
        '
        Me.cmbreportheader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbreportheader.Location = New System.Drawing.Point(120, 19)
        Me.cmbreportheader.Name = "cmbreportheader"
        Me.cmbreportheader.Size = New System.Drawing.Size(656, 21)
        Me.cmbreportheader.TabIndex = 1
        '
        'lblCMSReport
        '
        Me.lblCMSReport.AutoSize = True
        Me.lblCMSReport.BackColor = System.Drawing.Color.Transparent
        Me.lblCMSReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCMSReport.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblCMSReport.Location = New System.Drawing.Point(20, 22)
        Me.lblCMSReport.Name = "lblCMSReport"
        Me.lblCMSReport.Size = New System.Drawing.Size(100, 16)
        Me.lblCMSReport.TabIndex = 0
        Me.lblCMSReport.Text = "Report Name"
        '
        'Panel2
        '
        Me.Panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.btnPrintImg)
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 484)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1028, 36)
        Me.Panel2.TabIndex = 4
        '
        'btnPrintImg
        '
        Me.btnPrintImg.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPrintImg.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnPrintImg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintImg.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnPrintImg.Location = New System.Drawing.Point(779, 3)
        Me.btnPrintImg.Name = "btnPrintImg"
        Me.btnPrintImg.Size = New System.Drawing.Size(101, 29)
        Me.btnPrintImg.TabIndex = 4
        Me.btnPrintImg.Text = "Print"
        Me.btnPrintImg.UseVisualStyleBackColor = False
        Me.btnPrintImg.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnCancel.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnCancel.Location = New System.Drawing.Point(914, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(101, 29)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.pnlReportDetails)
        Me.Panel3.Controls.Add(Me.pnlcmsparams)
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 150)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1028, 520)
        Me.Panel3.TabIndex = 5
        '
        'pnlReportDetails
        '
        Me.pnlReportDetails.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlReportDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReportDetails.Controls.Add(Me.btnCancelDetails)
        Me.pnlReportDetails.Controls.Add(Me.btnSaveDetails)
        Me.pnlReportDetails.Controls.Add(Me.txtRepCode)
        Me.pnlReportDetails.Controls.Add(Me.Label6)
        Me.pnlReportDetails.Controls.Add(Me.txtDenominator)
        Me.pnlReportDetails.Controls.Add(Me.Label5)
        Me.pnlReportDetails.Controls.Add(Me.txtNumerator)
        Me.pnlReportDetails.Controls.Add(Me.Label4)
        Me.pnlReportDetails.Controls.Add(Me.txtSubHead)
        Me.pnlReportDetails.Controls.Add(Me.Label3)
        Me.pnlReportDetails.Controls.Add(Me.txtRepName)
        Me.pnlReportDetails.Controls.Add(Me.Label2)
        Me.pnlReportDetails.Location = New System.Drawing.Point(121, 2)
        Me.pnlReportDetails.Name = "pnlReportDetails"
        Me.pnlReportDetails.Size = New System.Drawing.Size(450, 176)
        Me.pnlReportDetails.TabIndex = 6
        Me.pnlReportDetails.Visible = False
        '
        'btnCancelDetails
        '
        Me.btnCancelDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancelDetails.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnCancelDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelDetails.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnCancelDetails.Location = New System.Drawing.Point(328, 141)
        Me.btnCancelDetails.Name = "btnCancelDetails"
        Me.btnCancelDetails.Size = New System.Drawing.Size(102, 29)
        Me.btnCancelDetails.TabIndex = 3
        Me.btnCancelDetails.Text = "Cancel"
        Me.btnCancelDetails.UseVisualStyleBackColor = False
        '
        'btnSaveDetails
        '
        Me.btnSaveDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSaveDetails.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnSaveDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveDetails.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSaveDetails.Location = New System.Drawing.Point(226, 141)
        Me.btnSaveDetails.Name = "btnSaveDetails"
        Me.btnSaveDetails.Size = New System.Drawing.Size(102, 29)
        Me.btnSaveDetails.TabIndex = 3
        Me.btnSaveDetails.Text = "Save Details"
        Me.btnSaveDetails.UseVisualStyleBackColor = False
        '
        'txtRepCode
        '
        Me.txtRepCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRepCode.Location = New System.Drawing.Point(120, 135)
        Me.txtRepCode.MaxLength = 10
        Me.txtRepCode.Name = "txtRepCode"
        Me.txtRepCode.Size = New System.Drawing.Size(100, 20)
        Me.txtRepCode.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label6.Location = New System.Drawing.Point(16, 135)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Report Code"
        '
        'txtDenominator
        '
        Me.txtDenominator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDenominator.Location = New System.Drawing.Point(120, 108)
        Me.txtDenominator.MaxLength = 100
        Me.txtDenominator.Name = "txtDenominator"
        Me.txtDenominator.Size = New System.Drawing.Size(312, 20)
        Me.txtDenominator.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label5.Location = New System.Drawing.Point(15, 108)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Denominator"
        '
        'txtNumerator
        '
        Me.txtNumerator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumerator.Location = New System.Drawing.Point(120, 82)
        Me.txtNumerator.MaxLength = 100
        Me.txtNumerator.Name = "txtNumerator"
        Me.txtNumerator.Size = New System.Drawing.Size(312, 20)
        Me.txtNumerator.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label4.Location = New System.Drawing.Point(29, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Numerator"
        '
        'txtSubHead
        '
        Me.txtSubHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubHead.Location = New System.Drawing.Point(120, 56)
        Me.txtSubHead.MaxLength = 100
        Me.txtSubHead.Name = "txtSubHead"
        Me.txtSubHead.Size = New System.Drawing.Size(312, 20)
        Me.txtSubHead.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label3.Location = New System.Drawing.Point(14, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Sub Heading"
        '
        'txtRepName
        '
        Me.txtRepName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRepName.Location = New System.Drawing.Point(120, 8)
        Me.txtRepName.MaxLength = 400
        Me.txtRepName.Multiline = True
        Me.txtRepName.Name = "txtRepName"
        Me.txtRepName.Size = New System.Drawing.Size(312, 40)
        Me.txtRepName.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label2.Location = New System.Drawing.Point(12, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Report Name"
        '
        'pnlcmsparams
        '
        Me.pnlcmsparams.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlcmsparams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlcmsparams.Controls.Add(Me.lsvCmsParams)
        Me.pnlcmsparams.Controls.Add(Me.Panel4)
        Me.pnlcmsparams.Controls.Add(Me.btnparamscancel)
        Me.pnlcmsparams.Controls.Add(Me.btnsavecmsparams)
        Me.pnlcmsparams.Location = New System.Drawing.Point(581, 0)
        Me.pnlcmsparams.Name = "pnlcmsparams"
        Me.pnlcmsparams.Size = New System.Drawing.Size(336, 178)
        Me.pnlcmsparams.TabIndex = 7
        Me.pnlcmsparams.Visible = False
        '
        'lsvCmsParams
        '
        Me.lsvCmsParams.BackColor = System.Drawing.Color.MintCream
        Me.lsvCmsParams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lsvCmsParams.CustomForm = Nothing
        Me.lsvCmsParams.Dock = System.Windows.Forms.DockStyle.Top
        Me.lsvCmsParams.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvCmsParams.FullRowSelect = True
        Me.lsvCmsParams.GridLines = True
        Me.lsvCmsParams.Location = New System.Drawing.Point(0, 24)
        Me.lsvCmsParams.Name = "lsvCmsParams"
        Me.lsvCmsParams.ReadOnly = False
        Me.lsvCmsParams.RowMultiSelect = False
        Me.lsvCmsParams.Size = New System.Drawing.Size(334, 120)
        Me.lsvCmsParams.TabIndex = 294
        Me.lsvCmsParams.UseCompatibleStateImageBehavior = False
        Me.lsvCmsParams.View = System.Windows.Forms.View.Details
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(334, 24)
        Me.Panel4.TabIndex = 296
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(0, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(152, 24)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Report Parameters"
        '
        'btnparamscancel
        '
        Me.btnparamscancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnparamscancel.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnparamscancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnparamscancel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnparamscancel.Location = New System.Drawing.Point(242, 146)
        Me.btnparamscancel.Name = "btnparamscancel"
        Me.btnparamscancel.Size = New System.Drawing.Size(90, 27)
        Me.btnparamscancel.TabIndex = 3
        Me.btnparamscancel.Text = "Cancel"
        Me.btnparamscancel.UseVisualStyleBackColor = False
        '
        'btnsavecmsparams
        '
        Me.btnsavecmsparams.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnsavecmsparams.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnsavecmsparams.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsavecmsparams.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnsavecmsparams.Location = New System.Drawing.Point(151, 146)
        Me.btnsavecmsparams.Name = "btnsavecmsparams"
        Me.btnsavecmsparams.Size = New System.Drawing.Size(90, 27)
        Me.btnsavecmsparams.TabIndex = 3
        Me.btnsavecmsparams.Text = "Save"
        Me.btnsavecmsparams.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1028, 484)
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'PrintDocument1
        '
        '
        'frmCMSReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1028, 670)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCMSReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CMS Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.btnProviders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnViewScript, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.pnlReportDetails.ResumeLayout(False)
        Me.pnlReportDetails.PerformLayout()
        Me.pnlcmsparams.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variables"
    Dim first As Integer
    'Private Utility.ConnectionString As String = Utility.ConnectionString
    Dim sqlconn As New SqlConnection
    Dim sqlComm As New SqlCommand
    Dim sqlda As SqlDataAdapter
    Private ReportHeader As String
    Private InputQuery As String
    Private TotalCount As Integer
    Private PresentCount As Integer
    Private ActualCount As Integer
    Private PatientCount As Integer
    Private PrsentValueName As String
    Private denominator As String = ""
    Private CMSRepCode As String = ""
    Private Numerator As String = ""
    Private SPType As Boolean
    Private PracticeName, RegistryName, RegistryId As String
    Private PQRIXmlPath As String
    Private bReportGenerated As Boolean = False
    Private dtStartDate, dtEndDate As Date
    Public RepType As String = ""
    Dim strPorviderId As String = ""
    Dim strProviderNames As String = ""
    Private UtilityProxy As New Utility
    Dim objpqr As New FolderBrowserDialog
    Dim CsvFileName As String = ""
#End Region
    Private m_strCSVFilePath As String
    Public Property CSVFilePath() As String
        Get
            Return m_strCSVFilePath
        End Get
        Set(ByVal value As String)
            m_strCSVFilePath = value
        End Set
    End Property

#Region "Functions"
    Private Sub InitLsvAuditEventtypeView()
        Dim exCol As ExColumnHeader
        'Clear
        lsvCmsParams.Items.Clear()
        lsvCmsParams.Columns.Clear()
        lsvCmsParams.ClearAllEmbeddedControls()

        '0 CMSRepid
        exCol = New ExColumnHeader
        With exCol
            .Text = "CmsparamCode" '"Reportid"
            .Width = 0 '150
            .ColumnType = ColumnType.ColumnTypeEditBox

        End With
        lsvCmsParams.Columns.Add(exCol)
        '0 CMSparamCode
        exCol = New ExColumnHeader
        With exCol
            .Text = "Code" '"paramcode"
            .Width = 0 '150
            .ColumnType = ColumnType.ColumnTypeEditBox
        End With
        lsvCmsParams.Columns.Add(exCol)
        '1 CMSparamDesc
        exCol = New ExColumnHeader
        With exCol
            .Text = "Description" '"paramDesc"
            .Width = 170
            .ColumnType = ColumnType.ColumnTypeReadOnly
            .TextAlign = HorizontalAlignment.Right
        End With
        lsvCmsParams.Columns.Add(exCol)
        '2 CMSparamValue
        exCol = New ExColumnHeader
        With exCol
            .Text = "Value" '"paramvalue"
            .Width = 60
            .ColumnType = ColumnType.ColumnTypeEditBox
            .TextAlign = HorizontalAlignment.Center
        End With
        lsvCmsParams.Columns.Add(exCol)
        '2 CMSparamValue
        exCol = New ExColumnHeader
        With exCol
            .Text = "Units" '"paramvalue"
            .Width = 80
            .ColumnType = ColumnType.ColumnTypeReadOnly
            .TextAlign = HorizontalAlignment.Left
        End With
        lsvCmsParams.Columns.Add(exCol)
        lsvCmsParams.GridLines = False
        lsvCmsParams.FullRowSelect = True
    End Sub
    Private Sub GetPMSCorpValues()
        Dim strsQL As String = "Select TOP 1 Code AS RegistryName,Alternatecode As RegistryId,LetterTranslation as ReportName FROM PracticeCodeTable where ReferenceType='REGISTRY'"
        Dim sqlConn As New SqlConnection(UtilityProxy.ConnectionString)
        Dim sqlComm As New SqlCommand(strSQL, sqlConn)
        Dim sqlRead As SqlDataReader
        Try
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader
            If sqlRead.Read Then
                PracticeName = sqlRead("ReportName")
                RegistryName = sqlRead("RegistryName")
                RegistryId = sqlRead("RegistryId")
            Else
                PracticeName = "IOPractice"
                RegistryName = "RegistryA"
                RegistryId = "123456"
            End If
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub
    Private Sub LoadCmsreports()
        Dim strSql As String
        strSql = "select cmsrepid,cmsrepname,CMSQuery from cmsreports Where ReportType='" & RepType & "' And Active_YN=1 order by cmsrepid"
        Dim tblCmsreports As New DataTable("CmsReports")
        Try
            If Not cmbreportheader.DataSource Is Nothing Then cmbreportheader.DataSource = Nothing
            sqlda = New SqlDataAdapter(strSql, UtilityProxy.ConnectionString)
            sqlda.Fill(tblCmsreports)
            cmbreportheader.DataSource = tblCmsreports
            cmbreportheader.DisplayMember = "cmsrepname"
            cmbreportheader.ValueMember = "cmsrepid"
        Catch ex As Exception
            MessageBox.Show("Fetch ProcedureNames Data : Exception -" + ex.Message)
        Finally
            strSql = Nothing
            sqlda = Nothing
        End Try
    End Sub
    Private Sub getReportDetails(ByVal cmsid As Integer)
        Dim ds As New DataSet
        Try
            Dim strSql As String = "select cmsquery,cmssubheading,issp,Numerator,denominator,CMSRepCode,substring(CMSRepName, 1, 4) csvFileName from cmsreports where cmsrepid=" & cmsid & ""
            sqlconn = New SqlConnection(UtilityProxy.ConnectionString)
            sqlconn.Open()
            sqlda = New SqlDataAdapter(strSql, sqlconn)
            sqlda.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                ReportHeader = cmbreportheader.Text
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i).Item("cmssubheading") Is DBNull.Value Then
                        PrsentValueName = ds.Tables(0).Rows(i).Item("cmssubheading")
                    Else
                        PrsentValueName = ""
                    End If
                    If Not ds.Tables(0).Rows(i).Item("issp") Is DBNull.Value Then
                        SPType = ds.Tables(0).Rows(i).Item("issp")
                    Else
                        SPType = False
                    End If
                    If Not ds.Tables(0).Rows(i).Item("cmsquery") Is DBNull.Value Then
                        InputQuery = ds.Tables(0).Rows(i).Item("cmsquery")
                    Else
                        InputQuery = ""
                    End If
                    If Not ds.Tables(0).Rows(i).Item("Numerator") Is DBNull.Value Then
                        Numerator = ds.Tables(0).Rows(i).Item("Numerator")
                    Else
                        Numerator = ""
                    End If
                    If Not ds.Tables(0).Rows(i).Item("denominator") Is DBNull.Value Then
                        denominator = ds.Tables(0).Rows(i).Item("denominator")
                    Else
                        denominator = ""
                    End If
                    If Not ds.Tables(0).Rows(i).Item("CMSRepCode") Is DBNull.Value Then
                        CMSRepCode = ds.Tables(0).Rows(i).Item("CMSRepCode")
                    Else
                        CMSRepCode = ""
                    End If
                    If Not ds.Tables(0).Rows(i).Item("csvFileName") Is DBNull.Value Then
                        CsvFileName = ds.Tables(0).Rows(i).Item("csvFileName")
                    Else
                        CsvFileName = ""
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, RepType)
        End Try
    End Sub
    Private Function getReportVlaues(ByVal Query As String) As DataTable
        Dim dtCms As DataTable = New DataTable("Cmsreport")
        Try
            Dim sqlconn As New SqlConnection(UtilityProxy.ConnectionString)
            Dim sqlCmd As New SqlCommand(Query, sqlconn)
            Dim sqlAdpt As SqlDataAdapter
            If SPType Then
                sqlCmd.CommandType = CommandType.StoredProcedure
            Else
                sqlCmd.CommandType = CommandType.Text
            End If
            sqlconn.Open()
            sqlCmd.Parameters.Add("@StartDate", SqlDbType.NVarChar).Value = Format(dtStartTime.Value, "yyyyMMdd")
            sqlCmd.Parameters.Add("@EndDate", SqlDbType.NVarChar).Value = Format(dtEndtime.Value, "yyyyMMdd")
            sqlCmd.Parameters.Add("@ResourceIds", SqlDbType.NVarChar).Value = strPorviderId
            sqlCmd.CommandTimeout = 1000
            sqlAdpt = New SqlDataAdapter(sqlCmd)
            sqlAdpt.Fill(dtCms)
            If dtCms.Rows.Count > 0 Then
            End If
        Catch ex As SqlException
            If ex.Number = -2 Then
                MsgBox("Timeout Occured....", MsgBoxStyle.Information, RepType)
            Else
                MsgBox(ex.Message, MsgBoxStyle.Information, RepType)
            End If
        End Try
        lblStatus.Text = PatientCount.ToString & "/" & TotalCount.ToString & "/" & PresentCount.ToString
        Return dtCms
    End Function
#End Region

#Region "Events"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub frmCMSReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If RepType.ToUpper = "PQRI" Then
            Me.Text = "PQRS" & " Reports"
        Else
            Me.Text = "Meaningful Use Calculator"
        End If
        Label1.Text = Me.Text
        first = 0
        GetPMSCorpValues()
        LoadCmsreports()
        InitLsvAuditEventtypeView()
        first = 1
        If first = 1 Then
            getcmsparameterStatus(cmbreportheader.SelectedValue)
        End If
        cboSubMethod.SelectedIndex = 0
        cboGrpMeasure.SelectedIndex = 0
        GetCMSDetails()
        dtStartTime.Value = "1 / 1 /" & Now.Year
        dtEndtime.Value = "12/31/" & Now.Year
        If RepType <> "PQRI" Then
            btnPQRIXml.Hide()
            cboSubMethod.Hide()
            cboGrpMeasure.Hide()
            lblMsrGroup.Hide()
            lblSubMethod.Hide()
            lblStatus.Hide()
            dtStartTime.Visible = True
            dtEndtime.Visible = True
            Label8.Visible = True
            Label9.Visible = True
            btnGenCSV.Visible = False
        Else
            Label8.Visible = True
            Label9.Visible = True
            dtStartTime.Visible = True
            dtEndtime.Visible = True
        End If
    End Sub
    Private Sub btnprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprint.Click
        If cmbreportheader.Text = "" Then
            MsgBox("Please select Report", MsgBoxStyle.Information, RepType)
            Exit Sub
        End If
        If RepType = "PQRI" Then
            LoadPQRIDetails()
        End If
        If RepType = "CMS" Then
            LoadCMSDetails()
        End If
    End Sub

    Private Sub LoadPQRIDetails()
        Dim percentage As Integer
        Dim ds As New DataSet
        Try
            getReportDetails(cmbreportheader.SelectedValue)
            ds.Tables.Add(getReportVlaues(InputQuery))
            bReportGenerated = True
            If Not ds.Tables(0).Rows.Count > 0 Then
                btnPrintImg.Visible = False
                MsgBox("No Records Found.....!!!!!!", MsgBoxStyle.Information, RepType)
                Dim g As Graphics
                If Not PictureBox1.Image Is Nothing Then
                    g = Graphics.FromImage(PictureBox1.Image)
                    g.Clear(PictureBox1.BackColor)
                End If
                PictureBox1.Refresh()
                Exit Sub
            End If
            Try
                Dim yLast As Single
                Dim p() As Integer
                Dim myImage As Bitmap
                Dim vals() As String
                Dim g As Graphics
                Dim myBrushes() As Brush
                Dim ValsPercentage() As Double
                TotalCount = ds.Tables(0).Rows.Count
                PresentCount = 0
                Select Case cmbreportheader.SelectedIndex + 1
                    Case 1, 2, 3, 6, 7, 8, 9
                        For iNumCount As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            If ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "1" Then
                                PresentCount = PresentCount + 1
                            End If
                        Next
                        p = Array.CreateInstance(GetType(System.Int32), 3)
                        vals = Array.CreateInstance(GetType(System.String), 3)
                        myBrushes = Array.CreateInstance(GetType(Brush), 2)
                        ValsPercentage = Array.CreateInstance(GetType(Double), 3)
                        ActualCount = TotalCount - PresentCount
                        p(0) = ActualCount
                        p(1) = PresentCount
                        p(2) = TotalCount
                        vals(0) = "Not Qualified"
                        vals(1) = Numerator
                        vals(2) = denominator
                        ValsPercentage(0) = Math.Round((ActualCount / TotalCount) * 100, 2)
                        ValsPercentage(1) = Math.Round((PresentCount / TotalCount) * 100, 2)
                        ValsPercentage(2) = 0
                        myBrushes(0) = New SolidBrush(Color.Peru)
                        myBrushes(1) = New SolidBrush(Color.Green)
                        yLast = 25 + (2 * 50) + 350
                    Case 4
                        Dim iFundusCount As Integer = 0
                        Dim iFundusComCount As Integer = 0
                        For iNumCount As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            If ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "1" Then
                                iFundusCount = iFundusCount + 1
                            End If
                            If ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 2).ToString = "1" Then
                                iFundusComCount = iFundusComCount + 1
                            End If
                        Next
                        p = Array.CreateInstance(GetType(System.Int32), 4)
                        myBrushes = Array.CreateInstance(GetType(Brush), 3)
                        vals = Array.CreateInstance(GetType(System.String), 4)
                        ValsPercentage = Array.CreateInstance(GetType(Double), 4)
                        ActualCount = TotalCount - iFundusComCount - iFundusCount
                        p(0) = ActualCount
                        p(1) = iFundusCount
                        p(2) = iFundusComCount
                        p(3) = TotalCount
                        vals(0) = "Not Qualified"
                        vals(1) = "Dilated Exam"
                        vals(2) = "Dilated Exam with Communication"
                        vals(3) = denominator
                        myBrushes(0) = New SolidBrush(Color.Peru)
                        myBrushes(1) = New SolidBrush(Color.Gold)
                        myBrushes(2) = New SolidBrush(Color.Teal)
                        PresentCount = iFundusCount + iFundusComCount
                        ValsPercentage(0) = Math.Round((ActualCount / TotalCount) * 100, 2)
                        ValsPercentage(1) = Math.Round((iFundusCount / TotalCount) * 100, 2)
                        ValsPercentage(2) = Math.Round((iFundusComCount / TotalCount) * 100, 2)
                        ValsPercentage(3) = 0
                        yLast = 25 + (3 * 50) + 300
                    Case 5
                        Dim iDilationCount As Integer = 0
                        Dim iFundusCount As Integer = 0
                        Dim iImageCount As Integer = 0
                        Dim iNDilationCount As Integer = 0
                        Dim iNFundusCount As Integer = 0
                        Dim iNImageCount As Integer = 0
                        PresentCount = 0
                        For iNumCount As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            If ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "1" Then
                                iDilationCount = iDilationCount + 1
                            ElseIf ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "2" Then
                                iFundusCount = iFundusCount + 1
                            ElseIf ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "3" Then
                                iImageCount = iImageCount + 1
                            ElseIf ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "4" Then
                                iNDilationCount = iNDilationCount + 1
                            ElseIf ds.Tables(0).Rows(iNumCount)(ds.Tables(0).Columns.Count - 1).ToString = "5" Then
                                iNFundusCount = iNFundusCount + 1
                            End If
                        Next
                        p = Array.CreateInstance(GetType(System.Int32), 7)
                        myBrushes = Array.CreateInstance(GetType(Brush), 6)
                        vals = Array.CreateInstance(GetType(System.String), 7)
                        ValsPercentage = Array.CreateInstance(GetType(Double), 7)
                        ActualCount = TotalCount - iDilationCount - iFundusCount - iImageCount - iNDilationCount - iNFundusCount
                        p(0) = ActualCount
                        p(1) = iDilationCount
                        p(2) = iFundusCount
                        p(3) = iImageCount
                        p(4) = iNDilationCount
                        p(5) = iNFundusCount
                        p(6) = TotalCount
                        vals(0) = "Not Qualified"
                        vals(1) = "Dilated Exam"
                        vals(2) = "With Fundus Phots"
                        vals(3) = "With Additional Imaging"
                        vals(4) = "Low Risk"
                        vals(5) = "Not Dilated"
                        vals(6) = denominator
                        myBrushes(0) = New SolidBrush(Color.Peru)
                        myBrushes(1) = New SolidBrush(Color.Gold)
                        myBrushes(2) = New SolidBrush(Color.Teal)
                        myBrushes(3) = New SolidBrush(Color.Green)
                        myBrushes(4) = New SolidBrush(Color.DimGray)
                        myBrushes(5) = New SolidBrush(Color.DarkRed)
                        PresentCount = iDilationCount + iFundusCount + iImageCount + iNDilationCount + iNFundusCount
                        ValsPercentage(0) = Math.Round((ActualCount / TotalCount) * 100, 2)
                        ValsPercentage(1) = Math.Round((iDilationCount / TotalCount) * 100, 2)
                        ValsPercentage(2) = Math.Round((iFundusCount / TotalCount) * 100, 2)
                        ValsPercentage(3) = Math.Round((iImageCount / TotalCount) * 100, 2)
                        ValsPercentage(4) = Math.Round((iNDilationCount / TotalCount) * 100, 2)
                        ValsPercentage(5) = Math.Round((iNFundusCount / TotalCount) * 100, 2)
                        ValsPercentage(6) = 0
                        yLast = 25 + (6 * 50) + 150
                End Select
                btnPrintImg.Visible = True
                myImage = New Bitmap(PictureBox1.Width, PictureBox1.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb)
                ' Get the graphics context for the bitmap.
                g = Graphics.FromImage(myImage)
                Dim brush As New SolidBrush(Color.WhiteSmoke)
                g.FillRectangle(brush, 0, 0, Width, Height)
                brush.Dispose()

                Dim i As Integer
                Dim total As Integer
                Dim x As Integer = PictureBox1.Width / 2 - 180
                Dim y As Integer = PictureBox1.Height / 2 - 100
                If x < 0 Or y < 0 Then
                    x = 150
                    y = 150
                End If
                Dim angleSoFar As Double = 0.0

                '   Caculates the total
                For i = 0 To p.Length - 1
                    total += p(i)
                Next
                'Draws Heading
                If PrsentValueName.Length > 50 Then
                    g.DrawString(PrsentValueName, New Font("Verdana", 13, FontStyle.Bold), Brushes.Brown, 12, 8)
                Else
                    g.DrawString(PrsentValueName, New Font("Verdana", 13, FontStyle.Bold), Brushes.Brown, (PictureBox1.Width / 2) - (PrsentValueName.Length * 5), 8)
                End If

                '   Draws the pie chart
                For i = 0 To p.Length - 1
                    If i = p.Length - 1 Then
                        g.DrawString(vals(i) & "=" & p(i).ToString, New Font("Verdana", 8, FontStyle.Bold), Brushes.Brown, 150, yLast)
                        g.DrawString(strProviderNames, New Font("Verdana", 8, FontStyle.Bold), Brushes.Red, 150, yLast + 20)
                    Else
                        percentage = p(i) / TotalCount * 360

                        g.FillPie(myBrushes(i), x, y, 300, 300, CInt(angleSoFar), CInt(percentage))

                        angleSoFar += percentage

                        '   Draws the names
                        g.FillRectangle(myBrushes(i), 120, 28 + (i * 50), 25, 25)

                        g.DrawString(vals(i) & "=" & p(i).ToString & "(" & ValsPercentage(i).ToString & "%" & ")", New Font("Verdana", 8, FontStyle.Bold), Brushes.Brown, 150, 25 + (i * 50) + 10)
                    End If
                Next
                PictureBox1.Image = myImage

            Catch ex As Exception
                btnPrintImg.Visible = False
                Exit Sub
            End Try
        Catch ex As Exception
            btnPrintImg.Visible = False
            MsgBox("Report Cannot be Generated", MsgBoxStyle.Information, RepType)
        End Try
    End Sub
    Private Sub LoadCMSDetails()
        Dim percentage As Integer
        Dim ds As New DataSet
        Try
            getReportDetails(cmbreportheader.SelectedValue)
            ds.Tables.Add(getReportVlaues(InputQuery))
            bReportGenerated = True
            If Not ds.Tables(0).Rows.Count > 0 Then
                btnPrintImg.Visible = False
                MsgBox("No Records Found.....!!!!!!", MsgBoxStyle.Information, RepType)
                Dim g As Graphics
                If Not PictureBox1.Image Is Nothing Then
                    g = Graphics.FromImage(PictureBox1.Image)
                    g.Clear(PictureBox1.BackColor)
                End If
                PictureBox1.Refresh()
                Exit Sub
            End If
            Try
                TotalCount = CInt(ds.Tables(0).Rows(0)("Denominator"))
                PresentCount = CInt(ds.Tables(0).Rows(0)("Numerator"))
                Dim g As Graphics
                If TotalCount = 0 And PresentCount = 0 Then
                    btnPrintImg.Visible = False
                    MsgBox("No patients match criteria!!!!!!", MsgBoxStyle.Information, RepType)
                    If Not PictureBox1.Image Is Nothing Then
                        g = Graphics.FromImage(PictureBox1.Image)
                        g.Clear(PictureBox1.BackColor)
                    End If
                    PictureBox1.Refresh()
                    Exit Sub
                End If
                Dim p() As Integer
                Dim myImage As Bitmap
                Dim vals() As String
                Dim myBrushes() As Brush
                Dim valsPercentage() As Double
                'Dim s As String = ""
                p = Array.CreateInstance(GetType(System.Int32), 3)
                vals = Array.CreateInstance(GetType(System.String), 3)
                valsPercentage = Array.CreateInstance(GetType(System.Double), 3)
                myBrushes = Array.CreateInstance(GetType(Brush), 2)
                ActualCount = TotalCount - PresentCount
                p(0) = ActualCount
                p(1) = PresentCount
                p(2) = TotalCount
                vals(0) = "Not Qualified"
                vals(1) = Numerator
                vals(2) = denominator
                myBrushes(0) = New SolidBrush(Color.Peru)
                myBrushes(1) = New SolidBrush(Color.Green)
                btnPrintImg.Visible = True
                myImage = New Bitmap(PictureBox1.Width, PictureBox1.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb)
                ' Get the graphics context for the bitmap.
                g = Graphics.FromImage(myImage)
                Dim brush As New SolidBrush(Color.WhiteSmoke)
                g.FillRectangle(brush, 0, 0, Width, Height)
                brush.Dispose()
                Dim i As Integer
                Dim total As Integer
                Dim x As Integer = PictureBox1.Width / 2 - 180
                Dim y As Integer = PictureBox1.Height / 2 - 100
                If x < 0 Or y < 0 Then
                    x = 150
                    y = 150
                End If
                'Dim percentage As Double
                Dim angleSoFar As Double = 0.0

                '   Caculates the total
                For i = 0 To p.Length - 1
                    total += p(i)
                Next
                valsPercentage(0) = Math.Round((ActualCount / TotalCount) * 100, 2)
                valsPercentage(1) = Math.Round((PresentCount / TotalCount) * 100, 2)
                valsPercentage(2) = 100
                'Draws Heading 
                g.DrawString(PrsentValueName, New Font("Verdana", 13, FontStyle.Bold), Brushes.Brown, (PictureBox1.Width / 2) - (PrsentValueName.Length * 5), 12)
                'Draws the pie chart
                For i = 0 To p.Length - 1
                    If i = 2 Then
                        g.DrawString(vals(i) & "=" & p(i).ToString, New Font("Verdana", 8, FontStyle.Bold), Brushes.Brown, 150, 25 + (i * 50) + 350)
                        g.DrawString(strProviderNames, New Font("Verdana", 8, FontStyle.Bold), Brushes.Red, 150, 25 + (i * 50) + 370)
                    Else
                        percentage = p(i) / TotalCount * 360
                        g.FillPie(myBrushes(i), x, y, 300, 300, CInt(angleSoFar), CInt(percentage))
                        angleSoFar += percentage
                        '   Draws the names
                        g.FillRectangle(myBrushes(i), 100, 28 + (i * 50), 25, 25)
                        g.DrawString(vals(i) & "=" & p(i).ToString & "(" & valsPercentage(i).ToString & "%" & ")", New Font("Verdana", 8, FontStyle.Bold), Brushes.Brown, 150, 25 + (i * 50) + 10)
                    End If
                Next
                PictureBox1.Image = myImage
            Catch ex As Exception
                MsgBox(ex.Message)
                btnPrintImg.Visible = False
                Exit Sub
            End Try
        Catch ex As Exception
            btnPrintImg.Visible = False
            MsgBox("Report Cannot be Generated", MsgBoxStyle.Information, RepType)
        End Try
    End Sub
    Private Sub btnSaveDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveDetails.Click
        If txtRepName.Text.Trim = "" Then
            MsgBox("ReportName can't be NULL")
            txtRepName.Focus()
            Exit Sub
        End If
        If txtSubHead.Text.Trim = "" Then
            MsgBox("SubHeading can't be NULL")
            txtSubHead.Focus()
            Exit Sub
        End If
        If txtNumerator.Text.Trim = "" Then
            MsgBox("Numerator can't be NULL")
            txtNumerator.Focus()
            Exit Sub
        End If
        If txtDenominator.Text.Trim = "" Then
            MsgBox("Denominator can't be NULL")
            txtDenominator.Focus()
            Exit Sub
        End If
        If txtRepCode.Text.Trim = "" Then
            MsgBox("Report Code can't be NULL")
            txtRepCode.Focus()
            Exit Sub
        End If
        If MsgBox("Do you want to update the infomation", MsgBoxStyle.YesNo, "Save Changes") = MsgBoxResult.Yes Then
            Dim strSQL As String = ""
            strSQL &= "Update CMSReports Set CMSRepName=@CMSRepName, CMSSubHeading=@CMSSubHeading, Numerator=@Numerator, Denominator=@Denominator, CMSRepCode=@CMSRepCode "
            strSQL &= "Where CMSRepId=@CMSRepId"
            Dim sqlConn As New SqlConnection(UtilityProxy.ConnectionString)
            Dim sqlComm As New SqlCommand(strSQL, sqlConn)
            Try
                sqlConn.Open()
                sqlComm.Parameters.Add("@CMSRepName", SqlDbType.VarChar).Value = txtRepName.Text.Trim
                sqlComm.Parameters.Add("@CMSSubHeading", SqlDbType.VarChar).Value = txtSubHead.Text.Trim
                sqlComm.Parameters.Add("@Numerator", SqlDbType.VarChar).Value = txtNumerator.Text.Trim
                sqlComm.Parameters.Add("@Denominator", SqlDbType.VarChar).Value = txtDenominator.Text.Trim
                sqlComm.Parameters.Add("@CMSRepCode", SqlDbType.VarChar).Value = txtRepCode.Text.Trim
                sqlComm.Parameters.Add("@CMSRepId", SqlDbType.Int).Value = cmbreportheader.SelectedValue
                sqlComm.ExecuteNonQuery()
            Catch ex As Exception
            Finally
                sqlComm = Nothing
                If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
                sqlConn = Nothing
            End Try
            LoadCmsreports()
        End If
        pnlReportDetails.Hide()
    End Sub

    Private Sub lblCMSReport_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCMSReport.DoubleClick
        pnlReportDetails.BringToFront()
        pnlReportDetails.Show()
        txtRepName.Focus()
    End Sub
    Private Sub getcmsparameterStatus(ByVal CmsReportid As Integer)
        Dim ds As New DataSet
        Dim litem As New System.Windows.Forms.ListViewItem
        lsvCmsParams.Items.Clear()
        Try
            Dim strSql As String = "SELECT CMSREPID,ISNULL(CMSPARAMCODE,'') AS 'CMSPARAMCODE',ISNULL(CMSPARAMDESC,'') AS 'CMSPARAMDESC',ISNULL(CMSPARAMVALUE,'') AS 'CMSPARAMVALUE',ISNULL(CMSPARAMUNITS,'') AS 'CMSPARAMUNITS' FROM CMSREPORTS_PARAMS WHERE CMSREPID='" & CmsReportid & "'"
            sqlconn = New SqlConnection(UtilityProxy.ConnectionString)
            sqlconn.Open()
            sqlda = New SqlDataAdapter(strSql, sqlconn)
            sqlda.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    litem = New System.Windows.Forms.ListViewItem
                    If Not ds.Tables(0).Rows(i).Item("CMSREPID") Is DBNull.Value Then
                        litem.Text = ds.Tables(0).Rows(i).Item("CMSREPID")
                    Else
                        litem.Text = ""
                    End If
                    If Not ds.Tables(0).Rows(i).Item("CMSPARAMCODE") Is DBNull.Value Then
                        litem.SubItems.Add(ds.Tables(0).Rows(i).Item("CMSPARAMCODE"))
                    Else
                        litem.SubItems.Add("")
                    End If
                    If Not ds.Tables(0).Rows(i).Item("CMSPARAMDESC") Is DBNull.Value Then
                        litem.SubItems.Add(ds.Tables(0).Rows(i).Item("CMSPARAMDESC"))
                    Else
                        litem.SubItems.Add("")
                    End If

                    If Not ds.Tables(0).Rows(i).Item("CMSPARAMVALUE") Is DBNull.Value Then
                        litem.SubItems.Add(ds.Tables(0).Rows(i).Item("CMSPARAMVALUE"))
                    Else
                        litem.SubItems.Add("")
                    End If
                    If Not ds.Tables(0).Rows(i).Item("CMSPARAMUNITS") Is DBNull.Value Then
                        litem.SubItems.Add(ds.Tables(0).Rows(i).Item("CMSPARAMUNITS"))
                    Else
                        litem.SubItems.Add("")
                    End If
                    lsvCmsParams.Items.Add(litem)
                Next
                'lblcmsparameters.Visible = True
            Else
                lblcmsparameters.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, RepType)
        End Try
    End Sub
    Private Sub cmbreportheader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbreportheader.SelectedIndexChanged
        strPorviderId = String.Empty
        strProviderNames = String.Empty
        Utility.m_strProviderList.Clear()
        Utility.m_strProviderName.Clear()
        Dim g As Graphics
        If Not PictureBox1.Image Is Nothing Then
            g = Graphics.FromImage(PictureBox1.Image)
            g.Clear(PictureBox1.BackColor)
        End If
        PictureBox1.Refresh()
        If first = 1 Then
            pnlcmsparams.Visible = False
            getcmsparameterStatus(cmbreportheader.SelectedValue)
        End If
        GetCMSDetails()
        bReportGenerated = False
    End Sub
    Private Sub lblcmsparameters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblcmsparameters.Click
        pnlcmsparams.Visible = True
    End Sub

    Private Sub btnsavecmsparams_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsavecmsparams.Click
        Dim litem As New System.Windows.Forms.ListViewItem
        Dim strSQL As String = ""
        If lsvCmsParams.Items.Count = 0 Then Exit Sub
        For Each litem In lsvCmsParams.Items
            strSQL = "Update CMSREPORTS_PARAMS Set CMSparamdesc=@CMSParamDesc, CMSParamvalue=@CMSParamvalue "
            strSQL &= " Where CMSRepId='" & litem.SubItems(0).Text & "' and CMSPARAMCODE='" & litem.SubItems(1).Text & "'"
            Dim sqlConn As New SqlConnection(UtilityProxy.ConnectionString)
            Dim sqlComm As New SqlCommand(strSQL, sqlConn)
            Try
                sqlConn.Open()
                If Not litem.SubItems(2).Text = "" Then
                    sqlComm.Parameters.Add("@CMSParamDesc", SqlDbType.VarChar).Value = litem.SubItems(2).Text
                Else
                    sqlComm.Parameters.Add("@CMSParamDesc", SqlDbType.VarChar).Value = System.DBNull.Value
                End If

                If Not litem.SubItems(3).Text = "" Then
                    sqlComm.Parameters.Add("@CMSParamvalue", SqlDbType.VarChar).Value = litem.SubItems(3).Text
                Else
                    sqlComm.Parameters.Add("@CMSParamvalue", SqlDbType.VarChar).Value = System.DBNull.Value
                End If
                sqlComm.ExecuteNonQuery()

            Catch ex As Exception
            Finally
                sqlComm = Nothing
                If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
                sqlConn = Nothing
            End Try
        Next
        getcmsparameterStatus(cmbreportheader.SelectedValue)
        pnlcmsparams.Visible = False
    End Sub
    Private Sub btnparamscancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnparamscancel.Click
        pnlcmsparams.Visible = False
    End Sub

    Private Sub btnViewScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewScript.Click
        Try
            If cmbreportheader.SelectedValue Is Nothing Then Exit Sub
            Dim objCMSScript As New frmCMSScript(cmbreportheader.SelectedValue, cmbreportheader.Text, cmbreportheader.SelectedItem("CMSQuery"))
            objCMSScript.Headername = "Report creation tool for " & cmbreportheader.Text.Trim
            objCMSScript.TopMost = True
            objCMSScript.ShowDialog()
            objCMSScript.Dispose()
        Catch ex As Exception
            Me.Show()
        End Try
    End Sub

    Private Sub btnCancelDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelDetails.Click
        pnlReportDetails.Hide()
    End Sub

    Private Sub btnPQRIXml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPQRIXml.Click
        If cmbreportheader.Text = "" Then
            MsgBox("Please select Report", MsgBoxStyle.Information, RepType)
            Exit Sub
        End If
        If Not bReportGenerated Then
            getReportDetails(cmbreportheader.SelectedValue)
            getReportVlaues(InputQuery)
        End If

        Dim TextWriter As New StringWriter
        Dim writer As New XmlTextWriter(TextWriter)

        writer.WriteStartElement("submission")
        writer.WriteAttributeString("type", "PQRI-REGISTRY")
        writer.WriteAttributeString("option", "PAYMENT")
        writer.WriteAttributeString("version", "2.0")
        writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        writer.WriteAttributeString("xsi:noNamespaceSchemaLocation", "Registry_Payment.xsd")

        writer.WriteStartElement("file-audit-data")
        writer.WriteElementString("create-date", Now.ToShortDateString)
        writer.WriteElementString("create-time", Now.ToString("hh:mm"))
        writer.WriteElementString("create-by", PracticeName)
        writer.WriteElementString("version", "1.0")
        writer.WriteElementString("file-number", "1")
        writer.WriteElementString("number-of-files", "1")
        writer.WriteEndElement() 'gile-audit-data
        writer.WriteStartElement("registry")
        writer.WriteElementString("registry-name", RegistryName)
        writer.WriteElementString("registry-id", RegistryId)
        writer.WriteElementString("submission-method", cboSubMethod.Text.Substring(0, 1).Trim)
        writer.WriteEndElement() 'registry
        writer.WriteStartElement("measure-group")
        writer.WriteAttributeString("ID", cboGrpMeasure.Text.Substring(0, 1).Trim)
        writer.WriteStartElement("provider")
        writer.WriteElementString("npi", "")
        writer.WriteElementString("tin", "")
        writer.WriteElementString("waiver-signed", "Y")
        writer.WriteElementString("encounter-from-date", dtStartTime.Value.ToString)
        writer.WriteElementString("encounter-to-date", dtEndtime.Value.ToString)
        If cboGrpMeasure.Text.Substring(0, 1).Trim <> "X" Then
            writer.WriteStartElement("measure-group-stat")
            writer.WriteElementString("ffs-patient-count", PatientCount.ToString)
            writer.WriteElementString("group-reporting-rate-numerator", PresentCount.ToString)
            writer.WriteElementString("group-eligible-instances", TotalCount.ToString)
            writer.WriteElementString("group-reporting-rate", PresentCount * 100 / TotalCount)
            writer.WriteEndElement() 'measure-group-stat
        End If
        writer.WriteStartElement("pqri-measure")
        writer.WriteElementString("pqri-measure-number", txtRepCode.Text.Replace("PQRI", "").Trim)
        writer.WriteElementString("eligible-instances", TotalCount.ToString)
        writer.WriteElementString("meets-performance-instances", PresentCount.ToString)
        writer.WriteElementString("performance-exclusion-instances", (PatientCount - TotalCount).ToString)
        writer.WriteElementString("performance-not-met-instances", (TotalCount - PresentCount).ToString)
        writer.WriteElementString("reporting-rate", (PresentCount + PatientCount - TotalCount + TotalCount - PresentCount) * 100 / PatientCount)
        If TotalCount = 0 Then
            writer.WriteRaw("<performance-rate xsi:nil=""true""/>")
        Else
            writer.WriteElementString("performance-rate", PresentCount * 100 / TotalCount)
        End If
        writer.WriteEndElement() 'pqri-measure
        writer.WriteEndElement() 'provider
        writer.WriteEndElement() 'measure-group
        writer.WriteEndElement() 'submission End
        writer.Close()

        'save the xml info textfile
        PQRIXmlPath = ""
        If objpqr.ShowDialog = Windows.Forms.DialogResult.OK Then
            PQRIXmlPath = objpqr.SelectedPath
        End If

        If PQRIXmlPath = "" Then
            Exit Sub
        End If

        Dim strFileName As String = PQRIXmlPath & "\PQRI_" & Now.ToString("MMddyyyy_hhmmss") & ".xml"
        Dim fsWrite As New StreamWriter(strFileName)
        fsWrite.WriteLine("<?xml version=""1.0"" encoding=""UTF-8"" ?>" & TextWriter.ToString)
        fsWrite.Flush()
        fsWrite.Close()

        MsgBox(RepType & " Xml File is generated", MsgBoxStyle.Information, RepType & " Report")
    End Sub

    Private Sub cboSubMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubMethod.SelectedIndexChanged
        Try
            'new changes Made

            dtStartDate = CType(dtStartTime.Text, Date)
            Select Case cboSubMethod.SelectedIndex
                Case 1, 4
                    
            End Select
            dtEndDate = CType(dtEndtime.Text, Date)
            CalculatePatientCount()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetCMSDetails()
        Try
            getReportDetails(cmbreportheader.SelectedValue)
            txtRepName.Text = cmbreportheader.Text
            txtSubHead.Text = PrsentValueName
            txtNumerator.Text = Numerator
            txtDenominator.Text = denominator
            txtRepCode.Text = CMSRepCode
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CalculatePatientCount()
        Dim strsql = ""
        strsql = "SELECT COUNT(DISTINCT PAT.PATIENTID) TOTALPATIENTS "
        strsql &= " FROM PATIENTDEMOGRAPHICS PAT "
        strsql &= " JOIN Appointments App ON App.PATIENTID=PAT.PATIENTID AND App.AppDate BETWEEN Convert(Varchar(10),@StartDate,112) and Convert(Varchar(10),@EndDate,112)"
        strsql &= " JOIN PatientClinical PATCLN ON PATCLN.AppointmentId=App.AppointmentId "
        Dim sqlConn As New SqlConnection(UtilityProxy.ConnectionString)
        Dim sqlComm As New SqlCommand(strsql, sqlConn)
        Try
            sqlConn.Open()
            sqlComm.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = dtStartDate
            sqlComm.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = dtEndDate
            PatientCount = sqlComm.ExecuteScalar()
        Catch ex As Exception
            MsgBox(ex.Message & vbCrLf & ex.StackTrace)
            PatientCount = 100
        Finally
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub
#End Region

    Private Sub btnPrintImg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintImg.Click
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, RepType)
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Try
            If PictureBox1.Image Is Nothing Then
                MsgBox("No Document Found to Print....!!!", MsgBoxStyle.Information, RepType)
                Exit Sub
            End If
            Dim g As Graphics
            g = e.Graphics
            g.DrawImage(PictureBox1.Image, 0, 0)
            g.Dispose()
            e.HasMorePages = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, RepType)
        End Try
    End Sub

    Private Sub btnProviders_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProviders.Click
        Try
            Dim Count As Integer = 1
            Dim frm As New frmProviders(RepType)
            frm.ShowDialog()
            strPorviderId = String.Empty
            strProviderNames = String.Empty
            If Utility.m_strProviderList.Count > 0 Then
                strProviderNames = "Selected Doctor:  "
                For i As Integer = 0 To Utility.m_strProviderList.Count - 1
                    If strProviderNames.Length >= Count * 100 Then
                        strProviderNames &= vbCrLf
                        Count = Count + 1
                    End If
                    strPorviderId += Utility.m_strProviderList(i).ToString
                    strPorviderId += ","
                    strProviderNames += Utility.m_strProviderName(i).ToString
                    strProviderNames += ","
                Next
                strPorviderId = strPorviderId.TrimEnd(",").Trim
                strProviderNames = strProviderNames.TrimEnd(",").Trim
                btnprint_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, RepType)
        End Try
    End Sub

    Private Sub btnGenCSV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenCSV.Click
        Try
            If cmbreportheader.Text = "" Then
                MsgBox("Please select Report", MsgBoxStyle.Information, RepType)
                Exit Sub
            End If
            Dim Csv As New CSVConverter()
            Dim strLocation As String = ""
            If String.IsNullOrEmpty(m_strCSVFilePath.Trim) Then
                strLocation = System.Reflection.Assembly.GetExecutingAssembly().Location.ToString
            Else
                strLocation = m_strCSVFilePath.Trim
            End If
            If strLocation.Contains(".dll") Then
                strLocation = strLocation.Replace(".dll", "")
                strLocation = strLocation.Trim
            End If
            Dim strDirPath As String = strLocation & "\PQRS"
            If Not Directory.Exists(strDirPath) Then
                Directory.CreateDirectory(strDirPath)
            End If
            getReportDetails(cmbreportheader.SelectedValue)
            
            Dim dt As New DataTable
            dt = getReportVlaues(InputQuery)
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    Dim strFilePath As String = String.Empty
                    'Dim strFilePath As String = strDirPath & "\PQRS_" & Now.ToString("MMddyyyy_hhmmss") & ".CSV"
                    If CsvFileName = String.Empty Then
                        strFilePath = strDirPath & "\PQRS_" & Now.ToString("MMddyyyy_hhmmss") & ".CSV"
                    Else
                        strFilePath = strDirPath & "\PQRS_" & CsvFileName & ".CSV"
                    End If
                    File.Create(strFilePath).Close()
                    Using CsvWriter As New StreamWriter(strFilePath)
                        CsvWriter.Write(Csv.CsvFromDatatable(dt))
                        MsgBox("Created CSV File SuccessFully...!!!!!", MsgBoxStyle.Information, RepType)
                    End Using
                Else
                    MsgBox("No Records Found...!!!!!", MsgBoxStyle.Information, RepType)
                End If
            Else
                MsgBox("Failed To Get Data...!!!!!", MsgBoxStyle.Information, RepType)
            End If
        Catch ex As Exception
            MsgBox("Failed To Create CSV File...!!!!!", MsgBoxStyle.Information, RepType)
        End Try
    End Sub
End Class
