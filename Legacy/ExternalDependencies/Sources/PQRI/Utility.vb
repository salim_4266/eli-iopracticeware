﻿Public Class Utility

    Public Shared strConnectionString As String
    Public Shared m_strProviderList As New ArrayList
    Public Shared m_strProviderName As New ArrayList
    Public Property ConnectionString() As String
        Get
            Return strConnectionString
        End Get
        Set(ByVal value As String)
            strConnectionString = value
        End Set
    End Property

    Public Property ProviderList() As ArrayList
        Get
            Return m_strProviderList
        End Get
        Set(ByVal value As ArrayList)
            m_strProviderList = value
        End Set
    End Property

    Public Property ProviderName() As ArrayList
        Get
            Return m_strProviderName
        End Get
        Set(ByVal value As ArrayList)
            m_strProviderName = value
        End Set
    End Property
    


    Public Shared Function GetCMSDataSet(ByVal sndReportName As String, ByVal sndTotal As Integer, ByVal sndCount As Integer, ByVal sndSubTitle As String) As DataTable
        Dim tblData As New DataTable("CMS")
        Dim tblRow As DataRow
        tblData.Columns.Add("Title", System.Type.GetType("System.String"))
        tblData.Columns.Add("SubTitle", System.Type.GetType("System.String"))
        tblData.Columns.Add("Count", System.Type.GetType("System.Int32"))

        tblRow = tblData.NewRow
        tblRow.Item(0) = sndReportName
        tblRow.Item(1) = ""
        tblRow.Item(2) = sndTotal - sndCount
        tblData.Rows.Add(tblRow)

        tblRow = tblData.NewRow
        tblRow.Item(0) = sndReportName
        tblRow.Item(1) = sndSubTitle
        tblRow.Item(2) = sndCount
        tblData.Rows.Add(tblRow)

        Return tblData
    End Function
End Class
