﻿Imports System.Data.SqlClient
Imports System.Windows.Forms


Public Class frmProviders
    Dim UtilityProxy As New Utility
    Public m_Providerlist As New ArrayList
    Public m_ProviderName As New ArrayList
    Dim m_checked As Boolean = False
    Dim m_RepType As String
    Private Sub frmProviders_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_Providerlist.Clear()
            m_ProviderName.Clear()
            GetUsers()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, m_RepType)
        End Try
    End Sub
    Public Sub GetUsers()
        Dim sqlConn As SqlConnection
        Dim sqlComm As SqlCommand
        Dim sqlAdapt As SqlDataAdapter
        Dim tblUsers As New DataTable("Users")
        Dim lstitem As ListViewItem
        Try
            sqlConn = New SqlConnection(UtilityProxy.ConnectionString)
            sqlComm = New SqlCommand("Select ResourceId,ResourceName from Resources  where ResourceType IN ('D','Q') Order By ResourceId", sqlConn)
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter(sqlComm)
            sqlAdapt.Fill(tblUsers)
            lstProviders.Items.Clear()
            lstProviders.Columns.Add("ResourceName", Me.Width, Windows.Forms.HorizontalAlignment.Left)
            lstProviders.Columns.Add("ResourceId", 0, Windows.Forms.HorizontalAlignment.Left)
            lstProviders.Items.Insert(0, "All")
            For i As Integer = 0 To tblUsers.Rows.Count - 1
                lstitem = New ListViewItem
                lstitem.Text = tblUsers.Rows(i)("ResourceName")
                lstitem.SubItems.Add(tblUsers.Rows(i)("ResourceId"))
                'lstProviders.Items.Add(tblUsers.Rows(i)("ResourceName"))
                'lstProviders.Items(i).SubItems.Add(tblUsers.Rows(i)("ResourceId"))
                lstProviders.Items.Add(lstitem)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, m_RepType)
        Finally
            sqlAdapt = Nothing
            sqlComm = Nothing
            sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If m_Providerlist.Count > 0 Then
                Utility.m_strProviderList.Clear()
                Utility.m_strProviderName.Clear()
                Utility.m_strProviderList = m_Providerlist
                Utility.m_strProviderName = m_ProviderName
            End If
            Me.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, m_RepType)
        End Try
    End Sub

    Private Sub lstProviders_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lstProviders.ItemChecked
        Try
            If lstProviders.Items.Count > 1 Then
                If e.Item.Checked Then
                    If e.Item.Text.ToUpper = "All".ToUpper Then
                        m_Providerlist.Clear()
                        m_ProviderName.Clear()
                        m_checked = True
                        For i As Integer = 1 To lstProviders.Items.Count - 1
                            lstProviders.Items(i).Checked = True
                        Next
                        For i As Integer = 1 To lstProviders.Items.Count - 1
                            m_Providerlist.Add(lstProviders.Items(i).SubItems(1).Text)
                            m_ProviderName.Add(lstProviders.Items(i).Text)
                        Next

                        m_checked = False
                    Else
                        If Not m_checked Then
                            m_Providerlist.Add(e.Item.SubItems(1).Text)
                            m_ProviderName.Add(e.Item.Text)
                        End If
                    End If
                Else
                    If e.Item.Text.ToUpper = "All".ToUpper Then
                        For i As Integer = 1 To lstProviders.Items.Count - 1
                            m_checked = True
                            lstProviders.Items(i).Checked = False
                        Next
                        m_Providerlist.Clear()
                        m_ProviderName.Clear()
                        m_checked = False
                    Else
                        If Not m_checked Then
                            If m_Providerlist.Count > 0 Then
                                m_Providerlist.Remove(e.Item.SubItems(1).Text)
                            End If
                            If m_ProviderName.Count > 0 Then
                                m_ProviderName.Remove(e.Item.Text)
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
    End Sub

    Public Sub New(ByVal strReportType As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_RepType = strReportType
    End Sub
End Class