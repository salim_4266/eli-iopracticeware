﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Management
Imports System.Drawing.Printing


Public Class ClsMain
    Public DebugMode As Boolean

    Public Function PrintReport(ByVal SQLServ As String, ByVal DataBase As String, ByVal RptName As String, _
                                ByVal Pat As Long, ByVal Inv As String) As Boolean

        If DebugMode Then MsgBox("Function PrintReport. pos #1")

        Dim fResp As Boolean = False

        Dim mReport As New ReportDocument
        mReport.Load(RptName)

        Dim patientid As New CrystalDecisions.Shared.ParameterDiscreteValue
        Dim InvId As New CrystalDecisions.Shared.ParameterDiscreteValue
        Dim pvCollection As New CrystalDecisions.Shared.ParameterValues

        patientid.Value = Pat
        pvCollection.Add(patientid)
        mReport.DataDefinition.ParameterFields("patientid").ApplyCurrentValues(pvCollection)

        If Len(Inv) = 0 Then
            Inv = ""
        End If
        InvId.Value = Inv
        pvCollection.Add(InvId)
        mReport.DataDefinition.ParameterFields("InvId").ApplyCurrentValues(pvCollection)
        mReport.DataSourceConnections(0).SetConnection(SQLServ, DataBase, True)

        Try
            mReport.PrintToPrinter(1, False, 0, 0)
            fResp = True
        Catch ex As Exception
            Debug.Print(ex.InnerException.Message)
            GoTo ExitFunction
        End Try

ExitFunction:
        mReport = Nothing
        patientid = Nothing
        InvId = Nothing
        pvCollection = Nothing
        Return fResp
    End Function


    Public Function GetJobsNumber() As Integer
        Return GetPrintJobsCollection(DefaultPrinterName).Count
    End Function

    Public Function GetPrintersCollection() As Collection

        Dim printerNameCollection As New Collection
        Dim searchQuery As String = "SELECT * FROM Win32_Printer"
        Dim searchPrinters As New ManagementObjectSearcher(searchQuery)
        Dim printerCollection As ManagementObjectCollection = searchPrinters.Get()
        Dim printerName As String
        For Each printer As ManagementObject In printerCollection
            printerName = printer.Properties("Name").Value.ToString()
            printerNameCollection.Add(printerName)
        Next
        Return printerNameCollection

    End Function

    Public Function DefaultPrinterName() As String
        Dim oPS As New PrinterSettings

        Try
            DefaultPrinterName = oPS.PrinterName
        Catch ex As System.Exception
            DefaultPrinterName = ""
        Finally
            oPS = Nothing
        End Try
    End Function


    Public Function GetPrintJobsCollection(ByVal printerName As String) As Collection

        Dim printJobCollection As New Collection()
        Dim searchQuery As String = "SELECT * FROM Win32_PrintJob"

        Dim searchPrintJobs As New ManagementObjectSearcher(searchQuery)
        Dim prntJobCollection As ManagementObjectCollection = searchPrintJobs.Get()
        Dim jobName As String
        For Each prntJob As ManagementObject In prntJobCollection
            jobName = prntJob.Properties("Name").Value.ToString()
            If InStr(jobName, printerName) > 0 Then
                printJobCollection.Add(jobName)
            End If
        Next

        Return printJobCollection
    End Function



    Public Sub New()
        If DebugMode Then MsgBox("Sub New. pos #1")
    End Sub
End Class
