﻿Public Class Form1


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PrintRep()
        Me.Dispose()
    End Sub


    Private Sub PrintRep()
        Dim RptName As String = "h:\Pinpoint\Reports\statement081.rpt"
        Dim SQLServ As String = "SQLREP"
        Dim DataBase As String = "PracticeRepositoryElman0901"

        Dim PatId(155) As Long
        Dim InvId(155) As String
        loadList(PatId, InvId)
        'Dim PatId(0) As Long
        'Dim InvId(0) As String

        PatId(0) = 24
        InvId(0) = ""
        Dim I As Integer
        Dim tTime As Date = Now
        Dim rep As CrRepCls.ClsMain

        Do Until I > UBound(PatId)
            rep = New CrRepCls.ClsMain
            If rep.GetJobsNumber = 0 Then
                If rep.PrintReport(SQLServ, DataBase, RptName, PatId(I), InvId(I)) Then
                    I = I + 1
                    tTime = Now
                End If
            End If
            If DateDiff(DateInterval.Minute, tTime, Now) > 2 Then
                MsgBox("CHECK PRINTER")
                Exit Do
            End If

            rep = Nothing
        Loop
    End Sub


    Private Sub loadList(ByRef Pat() As Long, ByRef Inv() As String)
        For i As Int16 = 0 To UBound(Inv)
            Inv(i) = ""
        Next



        Pat(0) = 1
        Pat(1) = 24
        Pat(2) = 158
        Pat(3) = 277
        Pat(4) = 279
        Pat(5) = 299
        Pat(6) = 340
        Pat(7) = 572
        Pat(8) = 702
        Pat(9) = 960
        Pat(10) = 1084
        Pat(11) = 1121
        Pat(12) = 1279
        Pat(13) = 1285
        Pat(14) = 1329
        Pat(15) = 1409
        Pat(16) = 1431
        Pat(17) = 1460
        Pat(18) = 1464
        Pat(19) = 1506
        Pat(20) = 1553
        Pat(21) = 1687
        Pat(22) = 1876
        Pat(23) = 2127
        Pat(24) = 2161
        Pat(25) = 2207
        Pat(26) = 2257
        Pat(27) = 2265
        Pat(28) = 2299
        Pat(29) = 2380
        Pat(30) = 2381
        Pat(31) = 2616
        Pat(32) = 2663
        Pat(33) = 2730
        Pat(34) = 2812
        Pat(35) = 2819
        Pat(36) = 2888
        Pat(37) = 2957
        Pat(38) = 3237
        Pat(39) = 3347
        Pat(40) = 3408
        Pat(41) = 3416
        Pat(42) = 3442
        Pat(43) = 3450
        Pat(44) = 3623
        Pat(45) = 3679
        Pat(46) = 3740
        Pat(47) = 3862
        Pat(48) = 3929
        Pat(49) = 3974
        Pat(50) = 4008
        Pat(51) = 4070
        Pat(52) = 4189
        Pat(53) = 4294
        Pat(54) = 4342
        Pat(55) = 4350
        Pat(56) = 4363
        Pat(57) = 4515
        Pat(58) = 4522
        Pat(59) = 4558
        Pat(60) = 4582
        Pat(61) = 4703
        Pat(62) = 4706
        Pat(63) = 4709
        Pat(64) = 4713
        Pat(65) = 4807
        Pat(66) = 4857
        Pat(67) = 5301
        Pat(68) = 5389
        Pat(69) = 5506
        Pat(70) = 5509
        Pat(71) = 5798
        Pat(72) = 5820
        Pat(73) = 5855
        Pat(74) = 5942
        Pat(75) = 5954
        Pat(76) = 5989
        Pat(77) = 6057
        Pat(78) = 6207
        Pat(79) = 6311
        Pat(80) = 6409
        Pat(81) = 6414
        Pat(82) = 6463
        Pat(83) = 6567
        Pat(84) = 6581
        Pat(85) = 6623
        Pat(86) = 6908
        Pat(87) = 6910
        Pat(88) = 7038
        Pat(89) = 7058
        Pat(90) = 7088
        Pat(91) = 7148
        Pat(92) = 7247
        Pat(93) = 7250
        Pat(94) = 7312
        Pat(95) = 7319
        Pat(96) = 7391
        Pat(97) = 7459
        Pat(98) = 7574
        Pat(99) = 7643
        Pat(100) = 7959
        Pat(101) = 8023
        Pat(102) = 8216
        Pat(103) = 8306
        Pat(104) = 8662
        Pat(105) = 8668
        Pat(106) = 8690
        Pat(107) = 8697
        Pat(108) = 8831
        Pat(109) = 8880
        Pat(110) = 8888
        Pat(111) = 8912
        Pat(112) = 8960
        Pat(113) = 9098
        Pat(114) = 9118
        Pat(115) = 9199
        Pat(116) = 9212
        Pat(117) = 9227
        Pat(118) = 9346
        Pat(119) = 9354
        Pat(120) = 9526
        Pat(121) = 9584
        Pat(122) = 9630
        Pat(123) = 9681
        Pat(124) = 9802
        Pat(125) = 9856
        Pat(126) = 9900
        Pat(127) = 9939
        Pat(128) = 9983
        Pat(129) = 9986
        Pat(130) = 9991
        Pat(131) = 9996
        Pat(132) = 10000
        Pat(133) = 10002
        Pat(134) = 10008
        Pat(135) = 10016
        Pat(136) = 10022
        Pat(137) = 10033
        Pat(138) = 10037
        Pat(139) = 10038
        Pat(140) = 20003
        Pat(141) = 20011
        Pat(142) = 20024
        Pat(143) = 30010
        Pat(144) = 30032
        Pat(145) = 30033
        Pat(146) = 30043
        Pat(147) = 30091
        Pat(148) = 30110
        Pat(149) = 200013
        Pat(150) = 300024
        Pat(151) = 300026
        Pat(152) = 300030
        Pat(153) = 300033
        Pat(154) = 300043
        Pat(155) = 300067

    End Sub
End Class
