Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Xml
<System.Runtime.InteropServices.ComVisible(True)> _
Public Class frmInferenceEngine
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlDetails As System.Windows.Forms.Panel
    Friend WithEvents colOperation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colFieldValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents colAndOr As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cboFields As System.Windows.Forms.ComboBox
    Friend WithEvents cboOperation As System.Windows.Forms.ComboBox
    Friend WithEvents txtValue As System.Windows.Forms.TextBox
    Friend WithEvents chklstValues As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblCalculator As System.Windows.Forms.Label
    Friend WithEvents tbarOptions As System.Windows.Forms.ToolBar
    Friend WithEvents tbtnPatient As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbtnICD As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbtnCPT As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbtnMeds As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnAND As System.Windows.Forms.Button
    Friend WithEvents btnOR As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents colFieldDesp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colFieldArea As System.Windows.Forms.ColumnHeader
    Friend WithEvents colFieldName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblDeleteCondition As System.Windows.Forms.Label
    Friend WithEvents lblAddCondition As System.Windows.Forms.Label
    Friend WithEvents lblEditCondition As System.Windows.Forms.Label
    Friend WithEvents pnlPharase As System.Windows.Forms.Panel
    Friend WithEvents lsvCondition As System.Windows.Forms.ListView
    Friend WithEvents grpCalculator As System.Windows.Forms.GroupBox
    Friend WithEvents btnCalc5 As System.Windows.Forms.Button
    Friend WithEvents btnCalc6 As System.Windows.Forms.Button
    Friend WithEvents btnCalc7 As System.Windows.Forms.Button
    Friend WithEvents btnCalc8 As System.Windows.Forms.Button
    Friend WithEvents btnCalc9 As System.Windows.Forms.Button
    Friend WithEvents btnCalc0 As System.Windows.Forms.Button
    Friend WithEvents btnCalcBackSpace As System.Windows.Forms.Button
    Friend WithEvents btnCalcOK As System.Windows.Forms.Button
    Friend WithEvents btnCalc1 As System.Windows.Forms.Button
    Friend WithEvents btnCalc2 As System.Windows.Forms.Button
    Friend WithEvents btnCalc3 As System.Windows.Forms.Button
    Friend WithEvents btnCalc4 As System.Windows.Forms.Button
    Friend WithEvents txtCalcResult As System.Windows.Forms.TextBox
    Friend WithEvents lblClosePhrase As System.Windows.Forms.Label
    Friend WithEvents grpResult As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboEvents As System.Windows.Forms.ComboBox
    Friend WithEvents txtResult As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtRuleName As System.Windows.Forms.TextBox
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuAND As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOR As System.Windows.Forms.MenuItem
    Friend WithEvents colRuleId As System.Windows.Forms.ColumnHeader
    Friend WithEvents colRuleName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colRuleResult As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblEditRule As System.Windows.Forms.Label
    Friend WithEvents lblDeleteRule As System.Windows.Forms.Label
    Friend WithEvents lblAddRule As System.Windows.Forms.Label
    Friend WithEvents grpCondition As System.Windows.Forms.GroupBox
    Friend WithEvents rtfCondition As System.Windows.Forms.RichTextBox
    Friend WithEvents colRuleEventID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colRuleEventName As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEvaluate As System.Windows.Forms.Button
    Friend WithEvents pnlMainHeader As System.Windows.Forms.Panel
    Friend WithEvents pnlMainFooter As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboPriority As System.Windows.Forms.ComboBox
    Friend WithEvents colRulePriorityID As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkAlert As System.Windows.Forms.CheckBox
    Friend WithEvents colRuleAlert As System.Windows.Forms.ColumnHeader
    Friend WithEvents tbtnEncounter As System.Windows.Forms.ToolBarButton
    Friend WithEvents chkPatientWise As System.Windows.Forms.CheckBox
    Friend WithEvents colRulePatient As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPatientSearch As System.Windows.Forms.Label
    Friend WithEvents tbtnLabs As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblPatientName As System.Windows.Forms.Label
    Public WithEvents lsvRules As System.Windows.Forms.ListView
    Public WithEvents btnSelect As System.Windows.Forms.Button
    Friend WithEvents tbtnLabResult As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList3 As System.Windows.Forms.ImageList
    Friend WithEvents pnlSelect As System.Windows.Forms.Panel
    Friend WithEvents lstRisk As System.Windows.Forms.ListBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lstComplexity As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lstDiagnoses As System.Windows.Forms.ListBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInferenceEngine))
        Me.pnlDetails = New System.Windows.Forms.Panel
        Me.grpResult = New System.Windows.Forms.GroupBox
        Me.pnlSelect = New System.Windows.Forms.Panel
        Me.lstRisk = New System.Windows.Forms.ListBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lstComplexity = New System.Windows.Forms.ListBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.lstDiagnoses = New System.Windows.Forms.ListBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtResult = New System.Windows.Forms.TextBox
        Me.lblPatientName = New System.Windows.Forms.Label
        Me.lblPatientSearch = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.chkPatientWise = New System.Windows.Forms.CheckBox
        Me.chkAlert = New System.Windows.Forms.CheckBox
        Me.cboPriority = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtRuleName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboEvents = New System.Windows.Forms.ComboBox
        Me.lblEditCondition = New System.Windows.Forms.Label
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblDeleteCondition = New System.Windows.Forms.Label
        Me.lblAddCondition = New System.Windows.Forms.Label
        Me.lsvCondition = New System.Windows.Forms.ListView
        Me.colFieldArea = New System.Windows.Forms.ColumnHeader
        Me.colFieldName = New System.Windows.Forms.ColumnHeader
        Me.colFieldDesp = New System.Windows.Forms.ColumnHeader
        Me.colOperation = New System.Windows.Forms.ColumnHeader
        Me.colFieldValue = New System.Windows.Forms.ColumnHeader
        Me.colAndOr = New System.Windows.Forms.ColumnHeader
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.mnuAND = New System.Windows.Forms.MenuItem
        Me.mnuOR = New System.Windows.Forms.MenuItem
        Me.pnlPharase = New System.Windows.Forms.Panel
        Me.lblClosePhrase = New System.Windows.Forms.Label
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnOR = New System.Windows.Forms.Button
        Me.btnAND = New System.Windows.Forms.Button
        Me.lblCalculator = New System.Windows.Forms.Label
        Me.txtValue = New System.Windows.Forms.TextBox
        Me.cboOperation = New System.Windows.Forms.ComboBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.tbarOptions = New System.Windows.Forms.ToolBar
        Me.tbtnPatient = New System.Windows.Forms.ToolBarButton
        Me.tbtnICD = New System.Windows.Forms.ToolBarButton
        Me.tbtnCPT = New System.Windows.Forms.ToolBarButton
        Me.tbtnMeds = New System.Windows.Forms.ToolBarButton
        Me.tbtnEncounter = New System.Windows.Forms.ToolBarButton
        Me.tbtnLabs = New System.Windows.Forms.ToolBarButton
        Me.tbtnLabResult = New System.Windows.Forms.ToolBarButton
        Me.ImageList3 = New System.Windows.Forms.ImageList(Me.components)
        Me.cboFields = New System.Windows.Forms.ComboBox
        Me.chklstValues = New System.Windows.Forms.CheckedListBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblEditRule = New System.Windows.Forms.Label
        Me.lblDeleteRule = New System.Windows.Forms.Label
        Me.lblAddRule = New System.Windows.Forms.Label
        Me.grpCalculator = New System.Windows.Forms.GroupBox
        Me.btnCalc5 = New System.Windows.Forms.Button
        Me.btnCalc6 = New System.Windows.Forms.Button
        Me.btnCalc7 = New System.Windows.Forms.Button
        Me.btnCalc8 = New System.Windows.Forms.Button
        Me.btnCalc9 = New System.Windows.Forms.Button
        Me.btnCalc0 = New System.Windows.Forms.Button
        Me.btnCalcBackSpace = New System.Windows.Forms.Button
        Me.btnCalcOK = New System.Windows.Forms.Button
        Me.btnCalc1 = New System.Windows.Forms.Button
        Me.btnCalc2 = New System.Windows.Forms.Button
        Me.btnCalc3 = New System.Windows.Forms.Button
        Me.btnCalc4 = New System.Windows.Forms.Button
        Me.txtCalcResult = New System.Windows.Forms.TextBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.grpCondition = New System.Windows.Forms.GroupBox
        Me.rtfCondition = New System.Windows.Forms.RichTextBox
        Me.pnlMainFooter = New System.Windows.Forms.Panel
        Me.btnSelect = New System.Windows.Forms.Button
        Me.btnEvaluate = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.pnlMainHeader = New System.Windows.Forms.Panel
        Me.lsvRules = New System.Windows.Forms.ListView
        Me.colRuleId = New System.Windows.Forms.ColumnHeader
        Me.colRuleEventID = New System.Windows.Forms.ColumnHeader
        Me.colRuleResult = New System.Windows.Forms.ColumnHeader
        Me.colRulePriorityID = New System.Windows.Forms.ColumnHeader
        Me.colRuleName = New System.Windows.Forms.ColumnHeader
        Me.colRuleEventName = New System.Windows.Forms.ColumnHeader
        Me.colRuleAlert = New System.Windows.Forms.ColumnHeader
        Me.colRulePatient = New System.Windows.Forms.ColumnHeader
        Me.pnlDetails.SuspendLayout()
        Me.grpResult.SuspendLayout()
        Me.pnlSelect.SuspendLayout()
        Me.pnlPharase.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.grpCalculator.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.grpCondition.SuspendLayout()
        Me.pnlMainFooter.SuspendLayout()
        Me.pnlMainHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlDetails
        '
        Me.pnlDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDetails.Controls.Add(Me.grpResult)
        Me.pnlDetails.Controls.Add(Me.lblPatientName)
        Me.pnlDetails.Controls.Add(Me.lblPatientSearch)
        Me.pnlDetails.Controls.Add(Me.chkPatientWise)
        Me.pnlDetails.Controls.Add(Me.chkAlert)
        Me.pnlDetails.Controls.Add(Me.cboPriority)
        Me.pnlDetails.Controls.Add(Me.Label3)
        Me.pnlDetails.Controls.Add(Me.btnClose)
        Me.pnlDetails.Controls.Add(Me.btnSave)
        Me.pnlDetails.Controls.Add(Me.txtRuleName)
        Me.pnlDetails.Controls.Add(Me.Label2)
        Me.pnlDetails.Controls.Add(Me.Label1)
        Me.pnlDetails.Controls.Add(Me.cboEvents)
        Me.pnlDetails.Controls.Add(Me.lblEditCondition)
        Me.pnlDetails.Controls.Add(Me.lblDeleteCondition)
        Me.pnlDetails.Controls.Add(Me.lblAddCondition)
        Me.pnlDetails.Controls.Add(Me.lsvCondition)
        Me.pnlDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlDetails.ForeColor = System.Drawing.Color.DarkGreen
        Me.pnlDetails.Location = New System.Drawing.Point(0, 0)
        Me.pnlDetails.Name = "pnlDetails"
        Me.pnlDetails.Size = New System.Drawing.Size(742, 558)
        Me.pnlDetails.TabIndex = 0
        Me.pnlDetails.Visible = False
        '
        'grpResult
        '
        Me.grpResult.Controls.Add(Me.pnlSelect)
        Me.grpResult.Controls.Add(Me.txtResult)
        Me.grpResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpResult.ForeColor = System.Drawing.Color.DarkGreen
        Me.grpResult.Location = New System.Drawing.Point(8, 304)
        Me.grpResult.Name = "grpResult"
        Me.grpResult.Size = New System.Drawing.Size(728, 169)
        Me.grpResult.TabIndex = 35
        Me.grpResult.TabStop = False
        Me.grpResult.Text = "Alert Text"
        '
        'pnlSelect
        '
        Me.pnlSelect.Controls.Add(Me.lstRisk)
        Me.pnlSelect.Controls.Add(Me.Label5)
        Me.pnlSelect.Controls.Add(Me.lstComplexity)
        Me.pnlSelect.Controls.Add(Me.Label4)
        Me.pnlSelect.Controls.Add(Me.lstDiagnoses)
        Me.pnlSelect.Controls.Add(Me.Label10)
        Me.pnlSelect.Location = New System.Drawing.Point(375, 20)
        Me.pnlSelect.Name = "pnlSelect"
        Me.pnlSelect.Size = New System.Drawing.Size(347, 143)
        Me.pnlSelect.TabIndex = 1
        Me.pnlSelect.Visible = False
        '
        'lstRisk
        '
        Me.lstRisk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstRisk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstRisk.Items.AddRange(New Object() {"Minimal", "Low", "Moderate", "High"})
        Me.lstRisk.Location = New System.Drawing.Point(233, 68)
        Me.lstRisk.Name = "lstRisk"
        Me.lstRisk.Size = New System.Drawing.Size(104, 54)
        Me.lstRisk.TabIndex = 173
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(233, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 40)
        Me.Label5.TabIndex = 174
        Me.Label5.Text = "Risk of Complications"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lstComplexity
        '
        Me.lstComplexity.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstComplexity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstComplexity.Items.AddRange(New Object() {"Minimal or None", "Limited", "Moderate", "Extensive"})
        Me.lstComplexity.Location = New System.Drawing.Point(121, 68)
        Me.lstComplexity.Name = "lstComplexity"
        Me.lstComplexity.Size = New System.Drawing.Size(104, 54)
        Me.lstComplexity.TabIndex = 171
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(121, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 40)
        Me.Label4.TabIndex = 172
        Me.Label4.Text = "Amount / Complexity of Data Reviewed"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lstDiagnoses
        '
        Me.lstDiagnoses.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstDiagnoses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstDiagnoses.Items.AddRange(New Object() {"Minimal", "Limited", "Multiple", "Extensive"})
        Me.lstDiagnoses.Location = New System.Drawing.Point(9, 68)
        Me.lstDiagnoses.Name = "lstDiagnoses"
        Me.lstDiagnoses.Size = New System.Drawing.Size(104, 54)
        Me.lstDiagnoses.TabIndex = 169
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(9, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 40)
        Me.Label10.TabIndex = 170
        Me.Label10.Text = "Number of Diagnoses/ Management Options"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'txtResult
        '
        Me.txtResult.BackColor = System.Drawing.Color.MintCream
        Me.txtResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtResult.Dock = System.Windows.Forms.DockStyle.Left
        Me.txtResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResult.Location = New System.Drawing.Point(3, 16)
        Me.txtResult.Multiline = True
        Me.txtResult.Name = "txtResult"
        Me.txtResult.Size = New System.Drawing.Size(350, 150)
        Me.txtResult.TabIndex = 0
        '
        'lblPatientName
        '
        Me.lblPatientName.AutoSize = True
        Me.lblPatientName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPatientName.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblPatientName.Location = New System.Drawing.Point(136, 536)
        Me.lblPatientName.Name = "lblPatientName"
        Me.lblPatientName.Size = New System.Drawing.Size(0, 15)
        Me.lblPatientName.TabIndex = 154
        Me.lblPatientName.Visible = False
        '
        'lblPatientSearch
        '
        Me.lblPatientSearch.Enabled = False
        Me.lblPatientSearch.ImageIndex = 5
        Me.lblPatientSearch.ImageList = Me.ImageList1
        Me.lblPatientSearch.Location = New System.Drawing.Point(232, 504)
        Me.lblPatientSearch.Name = "lblPatientSearch"
        Me.lblPatientSearch.Size = New System.Drawing.Size(16, 24)
        Me.lblPatientSearch.TabIndex = 153
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'chkPatientWise
        '
        Me.chkPatientWise.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPatientWise.ForeColor = System.Drawing.Color.DarkGreen
        Me.chkPatientWise.Location = New System.Drawing.Point(120, 504)
        Me.chkPatientWise.Name = "chkPatientWise"
        Me.chkPatientWise.Size = New System.Drawing.Size(104, 24)
        Me.chkPatientWise.TabIndex = 152
        Me.chkPatientWise.Text = "Patient Alert"
        '
        'chkAlert
        '
        Me.chkAlert.AutoSize = True
        Me.chkAlert.Checked = True
        Me.chkAlert.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAlert.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAlert.ForeColor = System.Drawing.Color.DarkGreen
        Me.chkAlert.Location = New System.Drawing.Point(467, 504)
        Me.chkAlert.Name = "chkAlert"
        Me.chkAlert.Size = New System.Drawing.Size(55, 19)
        Me.chkAlert.TabIndex = 151
        Me.chkAlert.Text = "Alert"
        '
        'cboPriority
        '
        Me.cboPriority.BackColor = System.Drawing.Color.White
        Me.cboPriority.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPriority.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cboPriority.Location = New System.Drawing.Point(467, 480)
        Me.cboPriority.Name = "cboPriority"
        Me.cboPriority.Size = New System.Drawing.Size(62, 21)
        Me.cboPriority.TabIndex = 149
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label3.Location = New System.Drawing.Point(384, 484)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 150
        Me.Label3.Text = "Event Priority"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnClose.Location = New System.Drawing.Point(680, 480)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(56, 23)
        Me.btnClose.TabIndex = 35
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSave.Location = New System.Drawing.Point(616, 480)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(56, 23)
        Me.btnSave.TabIndex = 35
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'txtRuleName
        '
        Me.txtRuleName.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.txtRuleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRuleName.Location = New System.Drawing.Point(95, 6)
        Me.txtRuleName.Name = "txtRuleName"
        Me.txtRuleName.Size = New System.Drawing.Size(641, 20)
        Me.txtRuleName.TabIndex = 39
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label2.Location = New System.Drawing.Point(14, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 15)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Alert Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label1.Location = New System.Drawing.Point(4, 483)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Event to be Raised"
        '
        'cboEvents
        '
        Me.cboEvents.BackColor = System.Drawing.Color.White
        Me.cboEvents.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEvents.Location = New System.Drawing.Point(120, 480)
        Me.cboEvents.Name = "cboEvents"
        Me.cboEvents.Size = New System.Drawing.Size(250, 21)
        Me.cboEvents.TabIndex = 36
        '
        'lblEditCondition
        '
        Me.lblEditCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEditCondition.ForeColor = System.Drawing.Color.Red
        Me.lblEditCondition.ImageIndex = 10
        Me.lblEditCondition.ImageList = Me.ImageList2
        Me.lblEditCondition.Location = New System.Drawing.Point(702, 32)
        Me.lblEditCondition.Name = "lblEditCondition"
        Me.lblEditCondition.Size = New System.Drawing.Size(16, 16)
        Me.lblEditCondition.TabIndex = 34
        Me.ToolTip1.SetToolTip(Me.lblEditCondition, "Edit the selected Phrase")
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "")
        Me.ImageList2.Images.SetKeyName(1, "")
        Me.ImageList2.Images.SetKeyName(2, "")
        Me.ImageList2.Images.SetKeyName(3, "")
        Me.ImageList2.Images.SetKeyName(4, "")
        Me.ImageList2.Images.SetKeyName(5, "")
        Me.ImageList2.Images.SetKeyName(6, "")
        Me.ImageList2.Images.SetKeyName(7, "")
        Me.ImageList2.Images.SetKeyName(8, "")
        Me.ImageList2.Images.SetKeyName(9, "")
        Me.ImageList2.Images.SetKeyName(10, "")
        Me.ImageList2.Images.SetKeyName(11, "")
        '
        'lblDeleteCondition
        '
        Me.lblDeleteCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteCondition.ForeColor = System.Drawing.Color.Red
        Me.lblDeleteCondition.ImageIndex = 8
        Me.lblDeleteCondition.ImageList = Me.ImageList2
        Me.lblDeleteCondition.Location = New System.Drawing.Point(720, 32)
        Me.lblDeleteCondition.Name = "lblDeleteCondition"
        Me.lblDeleteCondition.Size = New System.Drawing.Size(16, 16)
        Me.lblDeleteCondition.TabIndex = 33
        Me.ToolTip1.SetToolTip(Me.lblDeleteCondition, "Delete the Selected Phrase")
        '
        'lblAddCondition
        '
        Me.lblAddCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddCondition.ForeColor = System.Drawing.Color.Red
        Me.lblAddCondition.ImageIndex = 11
        Me.lblAddCondition.ImageList = Me.ImageList2
        Me.lblAddCondition.Location = New System.Drawing.Point(682, 32)
        Me.lblAddCondition.Name = "lblAddCondition"
        Me.lblAddCondition.Size = New System.Drawing.Size(16, 16)
        Me.lblAddCondition.TabIndex = 32
        Me.lblAddCondition.Tag = "ICD"
        Me.ToolTip1.SetToolTip(Me.lblAddCondition, "Add a new Phrase to the Alert Name")
        '
        'lsvCondition
        '
        Me.lsvCondition.BackColor = System.Drawing.Color.MintCream
        Me.lsvCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lsvCondition.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colFieldArea, Me.colFieldName, Me.colFieldDesp, Me.colOperation, Me.colFieldValue, Me.colAndOr})
        Me.lsvCondition.ContextMenu = Me.ContextMenu1
        Me.lsvCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvCondition.FullRowSelect = True
        Me.lsvCondition.GridLines = True
        Me.lsvCondition.Location = New System.Drawing.Point(8, 56)
        Me.lsvCondition.Name = "lsvCondition"
        Me.lsvCondition.Size = New System.Drawing.Size(728, 250)
        Me.lsvCondition.TabIndex = 0
        Me.lsvCondition.UseCompatibleStateImageBehavior = False
        Me.lsvCondition.View = System.Windows.Forms.View.Details
        '
        'colFieldArea
        '
        Me.colFieldArea.Text = "FldArea"
        Me.colFieldArea.Width = 0
        '
        'colFieldName
        '
        Me.colFieldName.Text = "FldName"
        Me.colFieldName.Width = 0
        '
        'colFieldDesp
        '
        Me.colFieldDesp.Text = "Field Name"
        Me.colFieldDesp.Width = 250
        '
        'colOperation
        '
        Me.colOperation.Text = "Operation"
        Me.colOperation.Width = 75
        '
        'colFieldValue
        '
        Me.colFieldValue.Text = "Values"
        Me.colFieldValue.Width = 300
        '
        'colAndOr
        '
        Me.colAndOr.Text = "AND / OR"
        Me.colAndOr.Width = 75
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAND, Me.mnuOR})
        '
        'mnuAND
        '
        Me.mnuAND.Index = 0
        Me.mnuAND.Text = "AND"
        '
        'mnuOR
        '
        Me.mnuOR.Index = 1
        Me.mnuOR.Text = "OR"
        '
        'pnlPharase
        '
        Me.pnlPharase.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.pnlPharase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPharase.Controls.Add(Me.lblClosePhrase)
        Me.pnlPharase.Controls.Add(Me.btnOK)
        Me.pnlPharase.Controls.Add(Me.btnOR)
        Me.pnlPharase.Controls.Add(Me.btnAND)
        Me.pnlPharase.Controls.Add(Me.lblCalculator)
        Me.pnlPharase.Controls.Add(Me.txtValue)
        Me.pnlPharase.Controls.Add(Me.cboOperation)
        Me.pnlPharase.Controls.Add(Me.Panel3)
        Me.pnlPharase.Controls.Add(Me.cboFields)
        Me.pnlPharase.Controls.Add(Me.chklstValues)
        Me.pnlPharase.Location = New System.Drawing.Point(136, 128)
        Me.pnlPharase.Name = "pnlPharase"
        Me.pnlPharase.Size = New System.Drawing.Size(400, 247)
        Me.pnlPharase.TabIndex = 1
        Me.pnlPharase.Visible = False
        '
        'lblClosePhrase
        '
        Me.lblClosePhrase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblClosePhrase.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClosePhrase.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblClosePhrase.ImageList = Me.ImageList2
        Me.lblClosePhrase.Location = New System.Drawing.Point(382, 1)
        Me.lblClosePhrase.Name = "lblClosePhrase"
        Me.lblClosePhrase.Size = New System.Drawing.Size(16, 23)
        Me.lblClosePhrase.TabIndex = 148
        Me.lblClosePhrase.Text = "X"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnOK.Location = New System.Drawing.Point(360, 53)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(36, 23)
        Me.btnOK.TabIndex = 35
        Me.btnOK.Tag = ""
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnOR
        '
        Me.btnOR.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnOR.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnOR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOR.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnOR.Location = New System.Drawing.Point(324, 53)
        Me.btnOR.Name = "btnOR"
        Me.btnOR.Size = New System.Drawing.Size(36, 23)
        Me.btnOR.TabIndex = 35
        Me.btnOR.Tag = "OR"
        Me.btnOR.Text = "OR"
        Me.btnOR.UseVisualStyleBackColor = False
        '
        'btnAND
        '
        Me.btnAND.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAND.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnAND.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAND.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnAND.Location = New System.Drawing.Point(283, 53)
        Me.btnAND.Name = "btnAND"
        Me.btnAND.Size = New System.Drawing.Size(41, 23)
        Me.btnAND.TabIndex = 35
        Me.btnAND.Tag = "AND"
        Me.btnAND.Text = "AND"
        Me.btnAND.UseVisualStyleBackColor = False
        '
        'lblCalculator
        '
        Me.lblCalculator.Enabled = False
        Me.lblCalculator.Image = CType(resources.GetObject("lblCalculator.Image"), System.Drawing.Image)
        Me.lblCalculator.Location = New System.Drawing.Point(262, 58)
        Me.lblCalculator.Name = "lblCalculator"
        Me.lblCalculator.Size = New System.Drawing.Size(16, 16)
        Me.lblCalculator.TabIndex = 144
        '
        'txtValue
        '
        Me.txtValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtValue.Location = New System.Drawing.Point(4, 56)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(248, 20)
        Me.txtValue.TabIndex = 7
        '
        'cboOperation
        '
        Me.cboOperation.Location = New System.Drawing.Point(260, 32)
        Me.cboOperation.Name = "cboOperation"
        Me.cboOperation.Size = New System.Drawing.Size(72, 21)
        Me.cboOperation.TabIndex = 6
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.tbarOptions)
        Me.Panel3.Location = New System.Drawing.Point(8, 8)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(177, 24)
        Me.Panel3.TabIndex = 5
        '
        'tbarOptions
        '
        Me.tbarOptions.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.tbarOptions.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbtnPatient, Me.tbtnICD, Me.tbtnCPT, Me.tbtnMeds, Me.tbtnEncounter, Me.tbtnLabs, Me.tbtnLabResult})
        Me.tbarOptions.Divider = False
        Me.tbarOptions.DropDownArrows = True
        Me.tbarOptions.ImageList = Me.ImageList3
        Me.tbarOptions.Location = New System.Drawing.Point(0, 0)
        Me.tbarOptions.Name = "tbarOptions"
        Me.tbarOptions.ShowToolTips = True
        Me.tbarOptions.Size = New System.Drawing.Size(177, 26)
        Me.tbarOptions.TabIndex = 0
        '
        'tbtnPatient
        '
        Me.tbtnPatient.ImageIndex = 0
        Me.tbtnPatient.Name = "tbtnPatient"
        Me.tbtnPatient.Tag = "PATIENT"
        Me.tbtnPatient.ToolTipText = "Patient Details"
        '
        'tbtnICD
        '
        Me.tbtnICD.ImageIndex = 1
        Me.tbtnICD.Name = "tbtnICD"
        Me.tbtnICD.Tag = "ICD"
        Me.tbtnICD.ToolTipText = "ICD Code"
        '
        'tbtnCPT
        '
        Me.tbtnCPT.ImageIndex = 2
        Me.tbtnCPT.Name = "tbtnCPT"
        Me.tbtnCPT.Tag = "EXAMELEMENTS"
        Me.tbtnCPT.ToolTipText = "EXAM ELEMENTS"
        '
        'tbtnMeds
        '
        Me.tbtnMeds.ImageIndex = 3
        Me.tbtnMeds.Name = "tbtnMeds"
        Me.tbtnMeds.Tag = "MEDS"
        Me.tbtnMeds.ToolTipText = "Medications"
        '
        'tbtnEncounter
        '
        Me.tbtnEncounter.ImageIndex = 4
        Me.tbtnEncounter.Name = "tbtnEncounter"
        Me.tbtnEncounter.Tag = "ENCOUNTER"
        Me.tbtnEncounter.ToolTipText = "Encounter"
        '
        'tbtnLabs
        '
        Me.tbtnLabs.ImageIndex = 6
        Me.tbtnLabs.Name = "tbtnLabs"
        Me.tbtnLabs.Tag = "LABS"
        Me.tbtnLabs.ToolTipText = "Labs"
        '
        'tbtnLabResult
        '
        Me.tbtnLabResult.ImageIndex = 7
        Me.tbtnLabResult.Name = "tbtnLabResult"
        Me.tbtnLabResult.Tag = "LABRESULT"
        Me.tbtnLabResult.ToolTipText = "Lab Results"
        '
        'ImageList3
        '
        Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList3.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList3.Images.SetKeyName(0, "")
        Me.ImageList3.Images.SetKeyName(1, "")
        Me.ImageList3.Images.SetKeyName(2, "")
        Me.ImageList3.Images.SetKeyName(3, "")
        Me.ImageList3.Images.SetKeyName(4, "")
        Me.ImageList3.Images.SetKeyName(5, "")
        Me.ImageList3.Images.SetKeyName(6, "")
        Me.ImageList3.Images.SetKeyName(7, "LabTests.png")
        Me.ImageList3.Images.SetKeyName(8, "SIGNLRG.ICO")
        '
        'cboFields
        '
        Me.cboFields.Location = New System.Drawing.Point(4, 32)
        Me.cboFields.Name = "cboFields"
        Me.cboFields.Size = New System.Drawing.Size(250, 21)
        Me.cboFields.TabIndex = 0
        '
        'chklstValues
        '
        Me.chklstValues.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.chklstValues.CheckOnClick = True
        Me.chklstValues.Location = New System.Drawing.Point(4, 75)
        Me.chklstValues.Name = "chklstValues"
        Me.chklstValues.Size = New System.Drawing.Size(248, 167)
        Me.chklstValues.TabIndex = 8
        Me.chklstValues.Visible = False
        '
        'lblEditRule
        '
        Me.lblEditRule.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEditRule.ForeColor = System.Drawing.Color.Red
        Me.lblEditRule.ImageIndex = 10
        Me.lblEditRule.ImageList = Me.ImageList2
        Me.lblEditRule.Location = New System.Drawing.Point(690, 4)
        Me.lblEditRule.Name = "lblEditRule"
        Me.lblEditRule.Size = New System.Drawing.Size(14, 16)
        Me.lblEditRule.TabIndex = 152
        Me.ToolTip1.SetToolTip(Me.lblEditRule, "Edit the selected Rule")
        '
        'lblDeleteRule
        '
        Me.lblDeleteRule.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteRule.ForeColor = System.Drawing.Color.Red
        Me.lblDeleteRule.ImageIndex = 8
        Me.lblDeleteRule.ImageList = Me.ImageList2
        Me.lblDeleteRule.Location = New System.Drawing.Point(712, 0)
        Me.lblDeleteRule.Name = "lblDeleteRule"
        Me.lblDeleteRule.Size = New System.Drawing.Size(16, 20)
        Me.lblDeleteRule.TabIndex = 151
        Me.ToolTip1.SetToolTip(Me.lblDeleteRule, "Delete the Selected Rule")
        '
        'lblAddRule
        '
        Me.lblAddRule.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddRule.ForeColor = System.Drawing.Color.Red
        Me.lblAddRule.ImageIndex = 11
        Me.lblAddRule.ImageList = Me.ImageList2
        Me.lblAddRule.Location = New System.Drawing.Point(664, 4)
        Me.lblAddRule.Name = "lblAddRule"
        Me.lblAddRule.Size = New System.Drawing.Size(16, 16)
        Me.lblAddRule.TabIndex = 150
        Me.lblAddRule.Tag = "ICD"
        Me.ToolTip1.SetToolTip(Me.lblAddRule, "Add a new Rule")
        '
        'grpCalculator
        '
        Me.grpCalculator.BackColor = System.Drawing.Color.LavenderBlush
        Me.grpCalculator.Controls.Add(Me.btnCalc5)
        Me.grpCalculator.Controls.Add(Me.btnCalc6)
        Me.grpCalculator.Controls.Add(Me.btnCalc7)
        Me.grpCalculator.Controls.Add(Me.btnCalc8)
        Me.grpCalculator.Controls.Add(Me.btnCalc9)
        Me.grpCalculator.Controls.Add(Me.btnCalc0)
        Me.grpCalculator.Controls.Add(Me.btnCalcBackSpace)
        Me.grpCalculator.Controls.Add(Me.btnCalcOK)
        Me.grpCalculator.Controls.Add(Me.btnCalc1)
        Me.grpCalculator.Controls.Add(Me.btnCalc2)
        Me.grpCalculator.Controls.Add(Me.btnCalc3)
        Me.grpCalculator.Controls.Add(Me.btnCalc4)
        Me.grpCalculator.Controls.Add(Me.txtCalcResult)
        Me.grpCalculator.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.grpCalculator.Location = New System.Drawing.Point(328, 136)
        Me.grpCalculator.Name = "grpCalculator"
        Me.grpCalculator.Size = New System.Drawing.Size(88, 144)
        Me.grpCalculator.TabIndex = 6
        Me.grpCalculator.TabStop = False
        Me.grpCalculator.Text = "Select Value"
        Me.grpCalculator.Visible = False
        '
        'btnCalc5
        '
        Me.btnCalc5.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc5.Location = New System.Drawing.Point(32, 66)
        Me.btnCalc5.Name = "btnCalc5"
        Me.btnCalc5.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc5.TabIndex = 16
        Me.btnCalc5.Text = "5"
        '
        'btnCalc6
        '
        Me.btnCalc6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc6.Location = New System.Drawing.Point(60, 66)
        Me.btnCalc6.Name = "btnCalc6"
        Me.btnCalc6.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc6.TabIndex = 17
        Me.btnCalc6.Text = "6"
        '
        'btnCalc7
        '
        Me.btnCalc7.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc7.Location = New System.Drawing.Point(4, 92)
        Me.btnCalc7.Name = "btnCalc7"
        Me.btnCalc7.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc7.TabIndex = 18
        Me.btnCalc7.Text = "7"
        '
        'btnCalc8
        '
        Me.btnCalc8.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc8.Location = New System.Drawing.Point(32, 92)
        Me.btnCalc8.Name = "btnCalc8"
        Me.btnCalc8.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc8.TabIndex = 19
        Me.btnCalc8.Text = "8"
        '
        'btnCalc9
        '
        Me.btnCalc9.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc9.Location = New System.Drawing.Point(60, 92)
        Me.btnCalc9.Name = "btnCalc9"
        Me.btnCalc9.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc9.TabIndex = 20
        Me.btnCalc9.Text = "9"
        '
        'btnCalc0
        '
        Me.btnCalc0.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc0.Location = New System.Drawing.Point(4, 118)
        Me.btnCalc0.Name = "btnCalc0"
        Me.btnCalc0.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc0.TabIndex = 21
        Me.btnCalc0.Text = "0"
        '
        'btnCalcBackSpace
        '
        Me.btnCalcBackSpace.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalcBackSpace.Location = New System.Drawing.Point(32, 118)
        Me.btnCalcBackSpace.Name = "btnCalcBackSpace"
        Me.btnCalcBackSpace.Size = New System.Drawing.Size(20, 20)
        Me.btnCalcBackSpace.TabIndex = 22
        Me.btnCalcBackSpace.Text = "C"
        '
        'btnCalcOK
        '
        Me.btnCalcOK.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalcOK.Location = New System.Drawing.Point(56, 118)
        Me.btnCalcOK.Name = "btnCalcOK"
        Me.btnCalcOK.Size = New System.Drawing.Size(24, 20)
        Me.btnCalcOK.TabIndex = 23
        Me.btnCalcOK.Text = "OK"
        '
        'btnCalc1
        '
        Me.btnCalc1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc1.Location = New System.Drawing.Point(4, 41)
        Me.btnCalc1.Name = "btnCalc1"
        Me.btnCalc1.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc1.TabIndex = 12
        Me.btnCalc1.Text = "1"
        '
        'btnCalc2
        '
        Me.btnCalc2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc2.Location = New System.Drawing.Point(32, 41)
        Me.btnCalc2.Name = "btnCalc2"
        Me.btnCalc2.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc2.TabIndex = 13
        Me.btnCalc2.Text = "2"
        '
        'btnCalc3
        '
        Me.btnCalc3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc3.Location = New System.Drawing.Point(60, 41)
        Me.btnCalc3.Name = "btnCalc3"
        Me.btnCalc3.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc3.TabIndex = 14
        Me.btnCalc3.Text = "3"
        '
        'btnCalc4
        '
        Me.btnCalc4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCalc4.Location = New System.Drawing.Point(4, 66)
        Me.btnCalc4.Name = "btnCalc4"
        Me.btnCalc4.Size = New System.Drawing.Size(20, 20)
        Me.btnCalc4.TabIndex = 15
        Me.btnCalc4.Text = "4"
        '
        'txtCalcResult
        '
        Me.txtCalcResult.BackColor = System.Drawing.Color.DarkOrange
        Me.txtCalcResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCalcResult.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtCalcResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCalcResult.Location = New System.Drawing.Point(3, 16)
        Me.txtCalcResult.Multiline = True
        Me.txtCalcResult.Name = "txtCalcResult"
        Me.txtCalcResult.Size = New System.Drawing.Size(82, 20)
        Me.txtCalcResult.TabIndex = 3
        Me.txtCalcResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.grpCondition)
        Me.pnlMain.Controls.Add(Me.pnlMainFooter)
        Me.pnlMain.Controls.Add(Me.pnlMainHeader)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(742, 558)
        Me.pnlMain.TabIndex = 7
        '
        'grpCondition
        '
        Me.grpCondition.Controls.Add(Me.rtfCondition)
        Me.grpCondition.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCondition.ForeColor = System.Drawing.Color.DarkGreen
        Me.grpCondition.Location = New System.Drawing.Point(0, 224)
        Me.grpCondition.Name = "grpCondition"
        Me.grpCondition.Size = New System.Drawing.Size(742, 302)
        Me.grpCondition.TabIndex = 153
        Me.grpCondition.TabStop = False
        Me.grpCondition.Text = "Condition"
        '
        'rtfCondition
        '
        Me.rtfCondition.BackColor = System.Drawing.Color.MintCream
        Me.rtfCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rtfCondition.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtfCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtfCondition.Location = New System.Drawing.Point(3, 16)
        Me.rtfCondition.Name = "rtfCondition"
        Me.rtfCondition.Size = New System.Drawing.Size(736, 283)
        Me.rtfCondition.TabIndex = 1
        Me.rtfCondition.Text = ""
        '
        'pnlMainFooter
        '
        Me.pnlMainFooter.Controls.Add(Me.btnSelect)
        Me.pnlMainFooter.Controls.Add(Me.btnEvaluate)
        Me.pnlMainFooter.Controls.Add(Me.btnExit)
        Me.pnlMainFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlMainFooter.Location = New System.Drawing.Point(0, 526)
        Me.pnlMainFooter.Name = "pnlMainFooter"
        Me.pnlMainFooter.Size = New System.Drawing.Size(742, 32)
        Me.pnlMainFooter.TabIndex = 156
        '
        'btnSelect
        '
        Me.btnSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelect.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnSelect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelect.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSelect.Location = New System.Drawing.Point(592, 6)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(56, 23)
        Me.btnSelect.TabIndex = 35
        Me.btnSelect.Tag = ""
        Me.btnSelect.Text = "Select"
        Me.btnSelect.UseVisualStyleBackColor = False
        Me.btnSelect.Visible = False
        '
        'btnEvaluate
        '
        Me.btnEvaluate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEvaluate.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnEvaluate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEvaluate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnEvaluate.Location = New System.Drawing.Point(608, 6)
        Me.btnEvaluate.Name = "btnEvaluate"
        Me.btnEvaluate.Size = New System.Drawing.Size(56, 23)
        Me.btnEvaluate.TabIndex = 35
        Me.btnEvaluate.Tag = ""
        Me.btnEvaluate.Text = "Evaluate"
        Me.btnEvaluate.UseVisualStyleBackColor = False
        Me.btnEvaluate.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnExit.Location = New System.Drawing.Point(672, 6)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(56, 23)
        Me.btnExit.TabIndex = 35
        Me.btnExit.Tag = ""
        Me.btnExit.Text = "Close"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'pnlMainHeader
        '
        Me.pnlMainHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.pnlMainHeader.Controls.Add(Me.lblDeleteRule)
        Me.pnlMainHeader.Controls.Add(Me.lblEditRule)
        Me.pnlMainHeader.Controls.Add(Me.lsvRules)
        Me.pnlMainHeader.Controls.Add(Me.lblAddRule)
        Me.pnlMainHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainHeader.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainHeader.Name = "pnlMainHeader"
        Me.pnlMainHeader.Size = New System.Drawing.Size(742, 224)
        Me.pnlMainHeader.TabIndex = 155
        '
        'lsvRules
        '
        Me.lsvRules.BackColor = System.Drawing.Color.MintCream
        Me.lsvRules.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lsvRules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colRuleId, Me.colRuleEventID, Me.colRuleResult, Me.colRulePriorityID, Me.colRuleName, Me.colRuleEventName, Me.colRuleAlert, Me.colRulePatient})
        Me.lsvRules.ContextMenu = Me.ContextMenu1
        Me.lsvRules.Dock = System.Windows.Forms.DockStyle.Left
        Me.lsvRules.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvRules.FullRowSelect = True
        Me.lsvRules.GridLines = True
        Me.lsvRules.Location = New System.Drawing.Point(0, 0)
        Me.lsvRules.MultiSelect = False
        Me.lsvRules.Name = "lsvRules"
        Me.lsvRules.Size = New System.Drawing.Size(656, 224)
        Me.lsvRules.TabIndex = 1
        Me.lsvRules.UseCompatibleStateImageBehavior = False
        Me.lsvRules.View = System.Windows.Forms.View.Details
        '
        'colRuleId
        '
        Me.colRuleId.Text = "                 RuleId"
        Me.colRuleId.Width = 0
        '
        'colRuleEventID
        '
        Me.colRuleEventID.Text = "EventId"
        Me.colRuleEventID.Width = 0
        '
        'colRuleResult
        '
        Me.colRuleResult.Text = "Result"
        Me.colRuleResult.Width = 0
        '
        'colRulePriorityID
        '
        Me.colRulePriorityID.Text = "Priority"
        Me.colRulePriorityID.Width = 0
        '
        'colRuleName
        '
        Me.colRuleName.Text = "Rule Description"
        Me.colRuleName.Width = 400
        '
        'colRuleEventName
        '
        Me.colRuleEventName.Text = "Event to be Raised"
        Me.colRuleEventName.Width = 194
        '
        'colRuleAlert
        '
        Me.colRuleAlert.Text = "Show Alert"
        Me.colRuleAlert.Width = 0
        '
        'colRulePatient
        '
        Me.colRulePatient.Text = "Patient Rule"
        Me.colRulePatient.Width = 0
        '
        'frmInferenceEngine
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(742, 558)
        Me.Controls.Add(Me.pnlPharase)
        Me.Controls.Add(Me.pnlDetails)
        Me.Controls.Add(Me.grpCalculator)
        Me.Controls.Add(Me.pnlMain)
        Me.Name = "frmInferenceEngine"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inference Engine"
        Me.pnlDetails.ResumeLayout(False)
        Me.pnlDetails.PerformLayout()
        Me.grpResult.ResumeLayout(False)
        Me.grpResult.PerformLayout()
        Me.pnlSelect.ResumeLayout(False)
        Me.pnlPharase.ResumeLayout(False)
        Me.pnlPharase.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.grpCalculator.ResumeLayout(False)
        Me.grpCalculator.PerformLayout()
        Me.pnlMain.ResumeLayout(False)
        Me.grpCondition.ResumeLayout(False)
        Me.pnlMainFooter.ResumeLayout(False)
        Me.pnlMainHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region

    Private m_ConnectionString As String
    Public Property ConnectionString() As String
        Get
            Return m_ConnectionString
        End Get
        Set(ByVal value As String)
            m_ConnectionString = value
        End Set
    End Property
    Private m_MDBFilePath As String
    Public Property MDBFilePath() As String
        Get
            Return m_MDBFilePath
        End Get
        Set(ByVal value As String)
            m_MDBFilePath = value
        End Set
    End Property
    Private m_OLEDBConstr As String
    Public Property OLEDbConnectionString() As String
        Get
            Return m_OLEDBConstr
        End Get
        Set(ByVal value As String)
            m_OLEDBConstr = value
        End Set
    End Property
    Private m_UserId As Integer
    Public Property UserId() As Integer
        Get
            Return m_UserId
        End Get
        Set(ByVal value As Integer)
            m_UserId = value
        End Set
    End Property

    Dim tblParams, tblValues As DataTable
    Dim dRow As DataRow
    Dim fldArea As String = ""
    Public strMode As String = "ADD"
    Public iRuleId As Integer = -1
    Public m_Patientid As Integer
    Dim FldParamList, RuleList, RulePhrasesList As ArrayList
    Dim iPatientId As Integer = 0
    Dim iEncounterId As Integer = 284043
    Public Result As String = ""

    Public Structure FldParamInfo
        Public FldArea, FldName As String
        Public FldValue As ArrayList
        Public FldType As Integer
    End Structure
    Public Structure RuleInfo
        Public RuleId As Integer
        Public RuleName As String
        Public Result As String
        Public EventId As Integer
        Public RuleValue As Boolean
    End Structure
    Public Structure RulePhraseInfo
        Public RuleId, Slno As Integer
        Public FldArea, FldName, Operation As String
        Public FldValue As ArrayList
        Public AndOrFlag As String
        Public PhraseValue As Boolean
    End Structure

    Private Sub LoadLabTestCodes()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlAdapt As SqlDataAdapter
        Dim dValRow As DataRow
        Try
            cboFields.Items.Clear()
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter("Select distinct TestCode,TestDesc from HL7_Observation_Details Order By 1", sqlConn)
            tblValues = New DataTable("FldValues")
            sqlAdapt.Fill(tblValues)
            For Each dValRow In tblValues.Rows
                cboFields.Items.Add(dValRow.Item(0) & " : " & dValRow.Item(1))
            Next
            lblCalculator.Enabled = True
            chklstValues.Hide()
            txtValue.Show()
            txtValue.Focus()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub tbarOptions_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbarOptions.ButtonClick
        Try
            If Not tblParams Is Nothing Then tblParams.Dispose() : tblParams = Nothing
            If Not tblValues Is Nothing Then tblValues.Dispose() : tblValues = Nothing
            fldArea = e.Button.Tag
            txtValue.Text = ""
            cboFields.Items.Clear()
            cboOperation.Items.Clear()
            cboOperation.Items.Add("IN")
            cboOperation.Items.Add("Not IN")
            Select Case e.Button.Tag
                Case "PATIENT", "ENCOUNTER"
                    cboOperation.Items.Clear()
                    cboOperation.Items.Add("=")
                    cboOperation.Items.Add("<>")
                    cboOperation.Items.Add("<")
                    cboOperation.Items.Add("<=")
                    cboOperation.Items.Add(">")
                    cboOperation.Items.Add(">=")
                    AddFields(e.Button.Tag) '"PATIENT"
                Case "ICD"
                    cboFields.Items.Add("ICD.ICDCode")
                    LoadICDCodes()
                Case "EXAMELEMENTS"
                    cboFields.Items.Add("ExamElements.Name")
                    LoadCPTCodes()
                Case "MEDS"
                    cboFields.Items.Add("MEDS.MedicationName")
                    LoadDITMedications()
                Case "LABS"
                    cboFields.Items.Add("LABS.LabTest")
                    LoadLabTests()
                Case "LABRESULT"
                    cboFields.Items.Add("LABTESTS.TestCode")
                    cboOperation.Items.Clear()
                    cboOperation.Items.Add("=")
                    cboOperation.Items.Add("<>")
                    cboOperation.Items.Add("<")
                    cboOperation.Items.Add("<=")
                    cboOperation.Items.Add(">")
                    cboOperation.Items.Add(">=")
                    LoadLabTestCodes()
            End Select
            If cboFields.Items.Count > 0 Then cboFields.SelectedIndex = 0
            cboOperation.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Sub AddFields(ByVal fldArea As String)
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlAdapt As SqlDataAdapter
        Try
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter("Select FldName,FldDesc,FldType,LookupId from IE_FieldParams Where FldArea='" & fldArea & "' Order by 2", sqlConn)
            tblParams = New DataTable("FldParams")
            sqlAdapt.Fill(tblParams)
            For Each drRow In tblParams.Rows
                cboFields.Items.Add(drRow.Item(1))
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboFields_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFields.SelectedValueChanged
        If Not tblParams Is Nothing Then
            dRow = tblParams.Rows(cboFields.SelectedIndex)
            lblCalculator.Enabled = IIf(dRow.Item(2) = 1, True, False)
            If dRow.Item(3) > 0 Then
                If cboOperation.Items.IndexOf("IN") = -1 Then
                    cboOperation.Items.Add("IN")
                    cboOperation.Items.Add("Not IN")
                End If
                LoadLookupValues(dRow.Item(3))
            Else
                If cboOperation.Items.IndexOf("IN") <> -1 Then cboOperation.Items.Remove("IN")
                If cboOperation.Items.IndexOf("Not IN") <> -1 Then cboOperation.Items.RemoveAt(cboOperation.Items.IndexOf("Not IN"))
                chklstValues.Items.Clear()
                chklstValues.Hide()
            End If
            txtValue.Text = ""
        End If
    End Sub

    Private Sub LoadLookupValues(ByVal iLookupId As Integer)
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlAdapt As SqlDataAdapter
        Dim dValRow As DataRow
        Try
            chklstValues.Items.Clear()
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter("Select LookupValue from LookupValues Where LookupId=" & iLookupId & " Order By 1", sqlConn)
            tblValues = New DataTable("FldValues")
            sqlAdapt.Fill(tblValues)
            For Each dValRow In tblValues.Rows
                chklstValues.Items.Add(dValRow.Item(0))
            Next
            txtValue.Focus()
            chklstValues.Show()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadICDCodes()
        Dim strConn As String = ""
        strConn = m_OLEDBConstr & m_MDBFilePath.Trim & "\DiagnosisMaster.MDB"
        Dim dValRow As DataRow
        Dim OLEDbConn As New OleDbConnection(strConn)
        Dim OLEDbAdapt As OleDbDataAdapter
        Try
            chklstValues.Items.Clear()
            OLEDbAdapt = New OleDbDataAdapter("Select distinct DiagnosisNextLevel AS ICDCode,ReviewSystemLingo AS ICDDescription from PrimaryDiagnosisTable order by DiagnosisNextLevel", OLEDbConn)
            tblValues = New DataTable("FldValues")
            OLEDbAdapt.Fill(tblValues)
            For Each dValRow In tblValues.Rows
                chklstValues.Items.Add(dValRow.Item(0) & " - " & dValRow.Item(1))
            Next
            lblCalculator.Enabled = False
            chklstValues.Show()
            txtValue.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub LoadCPTCodes()
        Dim strConn As String = ""
        strConn = m_OLEDBConstr & m_MDBFilePath.Trim & "\DynamicForms.MDB"
        Dim OLEDbConn As New OleDbConnection(strConn)
        Dim OLEDbAdapt As OleDbDataAdapter
        Dim dValRow As DataRow
        Try
            chklstValues.Items.Clear()
            OLEDbConn.Open()
            OLEDbAdapt = New OleDbDataAdapter("Select Question from Questions WHERE QuestionParty='T' order by question", OLEDbConn)
            tblValues = New DataTable("FldValues")
            OLEDbAdapt.Fill(tblValues)
            For Each dValRow In tblValues.Rows
                chklstValues.Items.Add(dValRow.Item(0))
            Next
            lblCalculator.Enabled = False
            chklstValues.Show()
            txtValue.Focus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub LoadMedications()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlAdapt As SqlDataAdapter
        Dim dValRow As DataRow
        Try
            chklstValues.Items.Clear()
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter("Select Distinct IngredientName from Medications Where IngredientName<>'' Order By IngredientName", sqlConn)
            tblValues = New DataTable("FldValues")
            sqlAdapt.Fill(tblValues)
            For Each dValRow In tblValues.Rows
                chklstValues.Items.Add(dValRow.Item(0))
            Next
            lblCalculator.Enabled = False
            chklstValues.Show()
            txtValue.Focus()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadDITMedications()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlAdapt As SqlDataAdapter
        Dim dValRow As DataRow
        Try
            chklstValues.Items.Clear()
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter("Select DISTINCT MED_ROUTED_DF_MED_ID_DESC as Med_Desc from FDB.dbo.tblCompositeDrug order by MED_ROUTED_DF_MED_ID_DESC", sqlConn)
            tblValues = New DataTable("FldValues")
            sqlAdapt.Fill(tblValues)
            For Each dValRow In tblValues.Rows
                chklstValues.Items.Add(dValRow.Item(0))
            Next
            lblCalculator.Enabled = False
            chklstValues.Show()
            txtValue.Focus()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub LoadLabTests()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlRead As SqlDataReader
        'Dim dValRow As DataRow
        Try
            chklstValues.Items.Clear()
            sqlConn.Open()
            Dim str As String = "Select ProviderLabOrder from Provider_LabOrder "
            Dim sqlcmd As New SqlCommand(str, sqlConn)
            sqlRead = sqlcmd.ExecuteReader
            Dim strLabs As String = ""
            If sqlRead.Read Then
                strLabs = sqlRead("ProviderLabOrder")
            End If
            sqlRead.Close()

            If Not strLabs Is Nothing Then
                If strLabs <> "" Then
                    Dim TmpDoc As XmlDocument
                    Dim TmpNode As XmlNode
                    Dim TmpNodelist As XmlNodeList

                    TmpDoc = New XmlDocument
                    Try
                        TmpDoc.LoadXml(strLabs)
                        TmpNodelist = TmpDoc.SelectNodes("//SuperBill/section/ItemInfo")
                        If TmpNodelist.Count > 0 Then
                            For Each TmpNode In TmpNodelist
                                Dim TmpStr As String = ""
                                TmpStr += TmpNode.ChildNodes(1).InnerText
                                TmpStr += "-" + TmpNode.ChildNodes(2).InnerText
                                chklstValues.Items.Add(TmpStr)
                            Next
                        End If
                    Catch ex As Exception
                    End Try
                End If
            End If

            lblCalculator.Enabled = False
            chklstValues.Show()
            txtValue.Focus()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.TextChanged
        If chklstValues.Enabled And chklstValues.Items.Count > 0 Then
            Dim K As Integer
            K = chklstValues.FindString(txtValue.Text)
            If K = -1 Then K = 0
            chklstValues.SelectedIndex = K
        End If
    End Sub

    Private Sub txtValue_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtValue.KeyDown
        If e.KeyCode = Keys.Enter And chklstValues.Items.Count > 0 Then chklstValues.Focus()
    End Sub

    Private Sub lblAddCondition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblAddCondition.Click
        If lsvCondition.SelectedItems.Count > 0 Then
            Dim lsvItem As ListViewItem
            For Each lsvItem In lsvCondition.SelectedItems
                lsvItem.Selected = False
            Next
        End If
        pnlPharase.Show()
        pnlPharase.BringToFront()
        pnlDetails.Enabled = False
        txtValue.Text = ""
        chklstValues.Hide()
    End Sub


    Private Sub lblEditCondition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblEditCondition.Click
        If lsvCondition.SelectedItems.Count = 0 Then
            MsgBox("Select a Phrase to Edit", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        End If
        Try
            Dim myBtnClick As System.Windows.Forms.ToolBarButtonClickEventArgs
            fldArea = lsvCondition.SelectedItems(0).SubItems(0).Text
            Select Case fldArea
                Case "PATIENT"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnPatient)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                    cboFields.Text = lsvCondition.SelectedItems(0).SubItems(2).Text
                Case "ENCOUNTER"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnEncounter)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                    cboFields.Text = lsvCondition.SelectedItems(0).SubItems(2).Text
                Case "ICD"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnICD)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                Case "CPT"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnCPT)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                Case "MEDS"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnMeds)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                Case "LABS"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnLabs)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                Case "LABRESULT"
                    myBtnClick = New System.Windows.Forms.ToolBarButtonClickEventArgs(tbtnLabResult)
                    tbarOptions_ButtonClick(tbarOptions, myBtnClick)
                    cboFields.Text = lsvCondition.SelectedItems(0).SubItems(2).Text
            End Select
            cboOperation.Text = lsvCondition.SelectedItems(0).SubItems(3).Text
            txtValue.Text = ""
            If cboOperation.Text = "IN" Or cboOperation.Text = "Not IN" Then
                Dim strValues As String = lsvCondition.SelectedItems(0).SubItems(4).Text
                Dim strValue As String
                Dim iPos As Integer
                iPos = strValues.IndexOf("','")
                While iPos <> -1
                    strValue = strValues.Substring(1, iPos - 1)
                    strValues = strValues.Substring(iPos + 2, strValues.Length - iPos - 2)
                    iPos = strValues.IndexOf("','")
                    CheckTheItem(strValue)
                End While
                strValue = strValues.Substring(1, strValues.Length - 2)
                CheckTheItem(strValue)
            Else
                Dim strValue As String = lsvCondition.SelectedItems(0).SubItems(4).Text
                If strValue.Substring(0, 1) = "'" Then strValue = strValue.Substring(1, strValue.Length - 1)
                If strValue.Substring(strValue.Length - 1, 1) = "'" Then strValue = strValue.Substring(0, strValue.Length - 1)
                If chklstValues.Items.Count > 0 Then CheckTheItem(strValue)
                txtValue.Text = strValue
            End If
            pnlDetails.Enabled = False
            pnlPharase.Show()
            pnlPharase.BringToFront()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub lblDeleteCondition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblDeleteCondition.Click
        If lsvCondition.SelectedItems.Count = 0 Then
            MsgBox("Select a Phrase to Delete", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        End If
        lsvCondition.SelectedItems(0).Remove()
        If lsvCondition.Items.Count > 0 Then lsvCondition.Items(lsvCondition.Items.Count - 1).SubItems(5).Text = ""
    End Sub

    Private Sub CheckTheItem(ByVal strValue As String)
        Dim K As Integer
        K = chklstValues.Items.IndexOf(strValue)
        If K = -1 Then K = chklstValues.FindString(strValue)
        If K <> -1 Then chklstValues.SetItemChecked(K, True)
    End Sub

    Private Sub btnAND_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAND.Click, btnOR.Click, btnOK.Click
        Dim lsvItem As ListViewItem
        Dim strValues, strValue As String

        If Not CheckValidation() Then Exit Sub
        lsvItem = New ListViewItem(fldArea) 'FieldArea
        If Not tblParams Is Nothing Then
            lsvItem.SubItems.Add(tblParams.Rows(cboFields.SelectedIndex).Item(0)) 'FieldName
        Else
            If cboFields.Text.IndexOf(".") <> -1 Then
                lsvItem.SubItems.Add(cboFields.Text.Substring(0, cboFields.Text.IndexOf(".")).ToUpper) 'FieldName
            Else
                lsvItem.SubItems.Add(cboFields.Text.Substring(0, cboFields.Text.IndexOf(":")).Trim.ToUpper) 'FieldName
            End If
        End If
        lsvItem.SubItems.Add(cboFields.Text.ToUpper) 'FieldDesp
        lsvItem.SubItems.Add(cboOperation.Text)
        If chklstValues.Visible Then
            strValues = ""
            If fldArea = "ICD" Then
                For Each strValue In chklstValues.CheckedItems
                    strValue = strValue.Substring(0, strValue.IndexOf("-")).Trim
                    strValues &= "'" & strValue & "',"
                Next
            Else
                Try
                    For Each strValue In chklstValues.CheckedItems
                        strValues &= "'" & strValue & "',"
                    Next
                Catch ex As Exception

                End Try

            End If
            strValues = strValues.Substring(0, strValues.Length - 1)
        Else
            strValues = txtValue.Text
        End If
        lsvItem.SubItems.Add(strValues) 'FieldValue
        lsvItem.SubItems.Add(sender.Tag) 'And/OR/OK
        If lsvCondition.SelectedItems.Count = 0 Then
            lsvCondition.Items.Add(lsvItem)
        Else
            lsvCondition.Items(lsvCondition.SelectedItems(0).Index) = lsvItem
            pnlPharase.Hide()
            pnlPharase.SendToBack()
            lsvCondition.Focus()
            pnlDetails.Enabled = True
        End If
        If sender.Tag = "" Then
            pnlPharase.Hide()
            pnlPharase.SendToBack()
            lsvCondition.Focus()
            pnlDetails.Enabled = True
        End If
    End Sub

    Private Function CheckValidation() As Boolean
        Dim bResult As Boolean = True
        If fldArea = "" Then
            MsgBox("Select a Field", MsgBoxStyle.Information, "Inference Engine")
            bResult = False
        ElseIf cboOperation.Text = "" Then
            MsgBox("Select an Operation", MsgBoxStyle.Information, "Inference Engine")
            bResult = False
        ElseIf (chklstValues.Visible And chklstValues.CheckedItems.Count = 0) Or (chklstValues.Visible = False And txtValue.Text = "") Then
            MsgBox("Select a Value", MsgBoxStyle.Information, "Inference Engine")
            bResult = False
        End If
        Return bResult
    End Function

    Private Sub lblCalculator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblCalculator.Click
        pnlPharase.Enabled = False
        grpCalculator.Show()
        grpCalculator.BringToFront()
        txtCalcResult.Text = ""
    End Sub

    Private Sub btnCalc1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalc1.Click, btnCalc2.Click, btnCalc3.Click, btnCalc4.Click, btnCalc5.Click, btnCalc6.Click, btnCalc7.Click, btnCalc8.Click, btnCalc9.Click, btnCalc0.Click
        txtCalcResult.Text &= CType(sender, Button).Text
    End Sub

    Private Sub btnCalcBackSpace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcBackSpace.Click
        txtCalcResult.Text = ""
    End Sub

    Private Sub btnCalcOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcOK.Click
        txtValue.Text = txtCalcResult.Text
        grpCalculator.Hide()
        pnlPharase.Enabled = True
    End Sub

    Private Sub lblClosePhrase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblClosePhrase.Click
        pnlPharase.Hide()
        pnlDetails.Enabled = True
        lsvCondition.Focus()
    End Sub

    Private Sub frmInferenceEngine_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadRules()
        LoadEvents()
    End Sub

    Private Sub LoadEvents()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlAdapt As SqlDataAdapter
        Dim tblEvents As New DataTable("Events")
        Try
            sqlConn.Open()
            sqlAdapt = New SqlDataAdapter("Select EventId,EventName from IE_Events Order By EventId", sqlConn)
            sqlAdapt.Fill(tblEvents)
            cboEvents.DisplayMember = "EventName"
            cboEvents.ValueMember = "EventId"
            cboEvents.DataSource = tblEvents
            cboEvents.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub LoadRules()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlstr As String = "Select IER.RuleId,IER.RuleName,IER.Result,IER.EventId,IER.Priority,IEE.EventName,"
        sqlstr &= " IsNull(IER.ShowAlert_YN,0),IsNull(IER.PatientRule_YN,0), isnull(IER.PatientID,0) as PatientID , "
        sqlstr &= "ISNULL(PA.FirstName + ' ' + Pa.LastName ,'') as PatientName, IsNull(IER.EMCoder_Result,'')  "
        sqlstr &= " from IE_Rules IER LEFT JOIN IE_Events IEE ON IER.EventId=IEE.EventId "
        sqlstr &= " LEFT JOIN PatientDemographics PA ON PA.Patientid = IER.Patientid "
        If btnSelect.Visible Then
            If m_Patientid > 0 Then
                sqlstr &= " where IER.PatientId = " & m_Patientid
            End If
        End If
        sqlstr &= " Order By IER.RuleName"
        Dim sqlComm As New SqlCommand(sqlstr, sqlConn)
        Dim sqlRead As SqlDataReader
        Dim lsvItem As ListViewItem
        Try
            lsvRules.Items.Clear()
            rtfCondition.Text = ""
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            With sqlRead
                While .Read
                    lsvItem = New ListViewItem(CType(.GetValue(0), String))
                    lsvItem.SubItems.Add(.GetValue(3))
                    lsvItem.SubItems.Add(.GetString(2))
                    lsvItem.SubItems.Add(.GetValue(4))
                    lsvItem.SubItems.Add(.GetString(1))
                    lsvItem.SubItems.Add(.GetString(5))
                    lsvItem.SubItems.Add(CType(.GetValue(6), Integer))
                    lsvItem.SubItems.Add(CType(.GetValue(7), Integer))
                    lsvItem.SubItems.Add(CType(.GetValue(8), Integer))
                    lsvItem.SubItems.Add(.GetString(9))
                    lsvItem.SubItems.Add(.GetString(10))
                    Select Case .GetValue(4)
                        Case 1
                            lsvItem.BackColor = Color.OrangeRed
                        Case 2
                            lsvItem.BackColor = Color.Orange
                        Case 3
                            lsvItem.BackColor = Color.Yellow
                        Case 4
                            lsvItem.BackColor = Color.Plum
                    End Select
                    lsvRules.Items.Add(lsvItem)
                End While
            End With
        Catch ex As Exception
        End Try
        If lsvRules.Items.Count > 0 Then
            lsvRules.Items(0).Selected = True
            lsvRules_Click(Nothing, Nothing)
            lsvRules.Items(0).Selected = False
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        LoadRules()
        pnlMain.Show()
        pnlMain.BringToFront()
        pnlDetails.Hide()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lsvItem As ListViewItem

        If txtRuleName.Text = "" Then
            MsgBox("Alert Name Can't be NULL", MsgBoxStyle.Information, "Inference Engine")
            txtRuleName.Focus()
            Exit Sub
        ElseIf txtResult.Text = "" Then
            MsgBox("Please enter a message in the Alert Text box", MsgBoxStyle.Information, "Inference Engine")
            txtResult.Focus()
            Exit Sub
        ElseIf cboEvents.SelectedItem Is Nothing Then
            MsgBox("Select Event to be Raised", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        ElseIf cboPriority.SelectedItem Is Nothing Then
            MsgBox("Select Priority", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        ElseIf lsvCondition.Items.Count = 0 Then
            MsgBox("Enter Atleast 1 Alert Name", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        ElseIf DuplicateRuleName() Then
            MsgBox("Duplicate Alert Name", MsgBoxStyle.Information, "Inference Engine")
            txtRuleName.Focus()
            Exit Sub
        End If
        For Each lsvItem In lsvCondition.Items
            If lsvItem.SubItems(5).Text = "" Then lsvItem.SubItems(5).Text = "AND"
        Next
        lsvCondition.Items(lsvCondition.Items.Count - 1).SubItems(5).Text = ""
        If SaveRule() Then
            If strMode = "ADD" Then
                strMode = "UPDATE"
                MsgBox("Rule Saved Successfully", MsgBoxStyle.Information, "Inference Engine")
            Else
                MsgBox("Rule Updated Successfully", MsgBoxStyle.Information, "Inference Engine")
            End If
        End If
    End Sub

    Private Function DuplicateRuleName() As Boolean
        Dim bResult As Boolean = False
        Dim strSql As String = "Select Count(*) TotCount from IE_Rules Where RuleName='" & txtRuleName.Text & "' "
        If strMode <> "ADD" Then strSql = "And RuleId=" & iRuleId
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand(strSql, sqlConn)
        Dim K As Integer
        Try
            sqlConn.Open()
            K = sqlComm.ExecuteScalar()
            If K > 0 Then bResult = True
        Catch ex As Exception
        Finally
            sqlComm = Nothing
            sqlConn.Close()
            sqlConn = Nothing
        End Try
        Return bResult
    End Function

    Private Function SaveRule() As Boolean
        Dim bResult As Boolean = True
        Dim l_sqlConn As SqlConnection = Nothing
        Dim l_sqlComm As SqlCommand
        Dim strSql As String
        Dim lsvItem As ListViewItem
        Dim K As Integer

        If strMode = "ADD" Then
            strSql = "Insert Into IE_Rules(RuleName,EventId,Result,Priority,ShowAlert_YN,PatientRule_YN,CreateBy,CreateDt,VersionNo, PatientID,EMCoder_Result) "
            strSql &= " Values(@RuleName,@EventId,@Result,@Priority,@ShowAlert_YN,@PatientRule_YN,@UserId,Getdate(),'',@PatientId, @EMCoderRes);Select Ident_Current('IE_Rules');"
        Else
            strSql = "Update IE_Rules Set RuleName=@RuleName,EventId=@EventId,Result=@Result,Priority=@Priority,"
            strSql &= " ShowAlert_YN=@ShowAlert_YN,PatientRule_YN=@PatientRule_YN,ModifyBy=@UserId,ModifyDt=GetDate(), "
            strSql &= " PatientID=@PatientID,EMCoder_result=@EMCoderRes Where RuleId=" & iRuleId & ";Select " & iRuleId & ";"
        End If
        Try
            l_sqlConn = New SqlConnection(m_ConnectionString)
            l_sqlComm = New SqlCommand(strSql, l_sqlConn)
            l_sqlConn.Open()
            l_sqlComm.Parameters.Clear()

            l_sqlComm.Parameters.Add("@RuleName", SqlDbType.VarChar).Value = txtRuleName.Text

            l_sqlComm.Parameters.Add("@EventId", SqlDbType.Int).Value = cboEvents.SelectedValue

            l_sqlComm.Parameters.Add("@Result", SqlDbType.Text).Value = txtResult.Text

            l_sqlComm.Parameters.Add("@Priority", SqlDbType.Int).Value = cboPriority.Text

            l_sqlComm.Parameters.Add("@ShowAlert_YN", SqlDbType.Bit).Value = chkAlert.Checked

            l_sqlComm.Parameters.Add("@PatientRule_YN", SqlDbType.Bit).Value = chkPatientWise.Checked

            'old l_sqlComm.Parameters.Add("@UserId", SqlDbType.Int).Value = CUser.GetInstance.UserID
            l_sqlComm.Parameters.Add("@UserId", SqlDbType.Int).Value = m_UserId

            l_sqlComm.Parameters.Add("@PatientID", SqlDbType.Int).Value = m_Patientid

            l_sqlComm.Parameters.Add("@EMCoderRes", SqlDbType.VarChar).Value = lstDiagnoses.SelectedItem & "," & lstComplexity.SelectedItem & "," & lstRisk.SelectedItem

            iRuleId = l_sqlComm.ExecuteScalar

            K = 1
            strSql = "Delete from IE_RulesPhrases Where RuleId=" & iRuleId & "; "
            For Each lsvItem In lsvCondition.Items
                strSql &= "Insert Into IE_RulesPhrases(RuleId,Slno,FieldArea,FieldName,FieldDesp,Operation,FieldValue,AndOrFlag) Values("
                strSql &= iRuleId & "," & K & ",'" & lsvItem.SubItems(0).Text & "','" & lsvItem.SubItems(1).Text & "','" & lsvItem.SubItems(2).Text & "','" & lsvItem.SubItems(3).Text & "','" & Replace(lsvItem.SubItems(4).Text, "'", "''") & "','" & lsvItem.SubItems(5).Text & "'); "
                K += 1
            Next
            l_sqlComm.Parameters.Clear()
            l_sqlComm.CommandText = strSql
            l_sqlComm.ExecuteNonQuery()
        Catch ex As Exception
            bResult = False
        Finally
            l_sqlComm = Nothing
            If l_sqlConn.State = ConnectionState.Open Then l_sqlConn.Close()
            l_sqlConn = Nothing
        End Try
        Return bResult
    End Function

    Private Sub lsvCondition_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvCondition.Click
        lsvCondition.ContextMenu = Nothing
        If lsvCondition.SelectedItems.Count > 0 Then
            If lsvCondition.SelectedItems(0).Index < lsvCondition.Items.Count - 1 Then lsvCondition.ContextMenu = ContextMenu1
        End If
    End Sub

    Private Sub mnuAND_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAND.Click, mnuOR.Click
        If lsvCondition.SelectedItems.Count = 0 Then Exit Sub
        lsvCondition.SelectedItems(0).SubItems(5).Text = sender.Text
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub lblAddRule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblAddRule.Click
        pnlMain.Hide()
        pnlMain.SendToBack()
        pnlDetails.Show()
        pnlDetails.BringToFront()
        iRuleId = -1
        strMode = "ADD"
        pnlPharase.Hide()
        grpCalculator.Hide()
        txtRuleName.Text = ""
        lsvCondition.Items.Clear()
        pnlSelect.Visible = False
        txtResult.Text = ""
        If Not cboEvents.SelectedIndex = -1 Then
            cboEvents.SelectedIndex = 0
        End If
        'cboPriority.SelectedIndex = 0
        chkAlert.Checked = True
        chkPatientWise.Checked = False
        txtRuleName.Focus()
    End Sub

    Private Sub lblEditRule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblEditRule.Click
        If lsvRules.SelectedItems.Count = 0 Then
            MsgBox("Select a Rule", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        End If
        strMode = "UPDATE"
        iRuleId = lsvRules.SelectedItems(0).SubItems(0).Text
        cboEvents.SelectedValue = lsvRules.SelectedItems(0).SubItems(1).Text
        txtResult.Text = lsvRules.SelectedItems(0).SubItems(2).Text
        cboPriority.Text = lsvRules.SelectedItems(0).SubItems(3).Text
        txtRuleName.Text = lsvRules.SelectedItems(0).SubItems(4).Text
        chkAlert.Checked = CType(lsvRules.SelectedItems(0).SubItems(6).Text, Boolean)
        chkPatientWise.Checked = CType(lsvRules.SelectedItems(0).SubItems(7).Text, Boolean)
        m_Patientid = CType(lsvRules.SelectedItems(0).SubItems(8).Text, Integer)
        If chkPatientWise.Checked Then
            lblPatientName.Visible = True
            lblPatientName.Text = lsvRules.SelectedItems(0).SubItems(9).Text.Trim
        Else
            lblPatientName.Visible = False
            lblPatientName.Text = ""
        End If
        GetRuleDetails()
        pnlMain.Hide()
        pnlMain.SendToBack()
        pnlDetails.Show()
        pnlDetails.BringToFront()
        pnlPharase.Hide()
        grpCalculator.Hide()
        lsvCondition.Focus()
    End Sub

    Private Sub lblDeleteRule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblDeleteRule.Click
        If lsvRules.SelectedItems.Count = 0 Then
            MsgBox("Select a Rule", MsgBoxStyle.Information, "Inference Engine")
            Exit Sub
        End If
        If MsgBox("Delete the Rule", MsgBoxStyle.YesNo, "Inference Engine") = MsgBoxResult.Yes Then
            DeleteRule(lsvRules.SelectedItems(0).SubItems(0).Text)
            lsvRules.SelectedItems(0).Remove()
            rtfCondition.Text = ""
            If lsvRules.Items.Count > 0 Then
                'lsvRules.Items(0).Selected = True

            End If
        End If
    End Sub

    Private Sub GetRuleDetails()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand("Select FieldArea,FieldName,FieldDesp,Operation,FieldValue,AndOrFlag from IE_RulesPhrases Where RuleId=" & iRuleId & " Order By SlNo", sqlConn)
        Dim sqlRead As SqlDataReader
        Dim lsvItem As ListViewItem
        Try
            lsvCondition.Items.Clear()
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            With sqlRead
                While .Read
                    lsvItem = New ListViewItem(.GetString(0))
                    lsvItem.SubItems.Add(.GetString(1))
                    lsvItem.SubItems.Add(.GetString(2))
                    lsvItem.SubItems.Add(.GetString(3))
                    lsvItem.SubItems.Add(.GetString(4))
                    lsvItem.SubItems.Add(.GetString(5))
                    lsvCondition.Items.Add(lsvItem)
                End While
            End With
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            sqlConn = Nothing
        End Try
    End Sub

    Private Sub DeleteRule(ByVal RuleId As Integer)
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand("Delete from IE_RulesPhrases Where RuleId=@RuleId; Delete from IE_Rules Where RuleId=@RuleId;", sqlConn)
        Try
            sqlConn.Open()
            sqlComm.Parameters.Add("@RuleId", SqlDbType.Int)
            sqlComm.Parameters("@RuleId").Value = RuleId
            sqlComm.ExecuteNonQuery()
        Catch ex As Exception
        Finally
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub

    Private Sub lsvRules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvRules.Click
        Try
            Dim myFont As New System.Drawing.Font("Arial Rounded MT Bold", 9, FontStyle.Regular)
            Dim myResultFont As New System.Drawing.Font("Tahoma", 10, FontStyle.Bold)

            If lsvRules.SelectedItems.Count = 0 Then Exit Sub
            Dim sqlConn As New SqlConnection(m_ConnectionString)
            Dim sqlComm As New SqlCommand("Select FieldDesp,Operation,FieldValue,AndOrFlag from IE_RulesPhrases Where RuleId=" & lsvRules.SelectedItems(0).SubItems(0).Text & " Order By SlNo", sqlConn)
            Dim sqlRead As SqlDataReader
            ' Dim strValue As String
            Try
                rtfCondition.Text = ""
                sqlConn.Open()
                sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
                rtfCondition.Select(0, 0)
                With sqlRead
                    While .Read
                        rtfCondition.SelectionFont = myFont
                        rtfCondition.SelectionColor = Color.Red
                        rtfCondition.SelectedText = .GetString(0) & " "
                        rtfCondition.SelectionColor = Color.Black
                        rtfCondition.SelectedText = .GetString(1) & " "
                        rtfCondition.SelectionColor = Color.Green
                        rtfCondition.SelectedText = .GetString(2) & " "
                        rtfCondition.SelectionColor = Color.Black
                        rtfCondition.SelectedText = .GetString(3) & vbCrLf
                    End While
                End With
                rtfCondition.SelectionFont = myResultFont
                rtfCondition.SelectionColor = Color.Blue
                rtfCondition.SelectedText = vbCrLf & lsvRules.SelectedItems(0).SubItems(2).Text & vbCrLf
            Catch ex As Exception
            Finally
                sqlRead = Nothing
                sqlComm = Nothing
                sqlConn = Nothing
            End Try
        Catch ex As Exception
        End Try
    End Sub


#Region " Evaluation of Inference Engine "
    Private Sub btnEvaluate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEvaluate.Click
        RuleList = New ArrayList
        RulePhrasesList = New ArrayList
        FldParamList = New ArrayList
        GetRules()
        GetRulePhrases()
        GetFieldParams()
        GetFieldValues()
        ResetRuleStatus()
        Dim lsvItem As ListViewItem
        For Each lsvItem In lsvRules.CheckedItems
            EvaluateRule(lsvItem.SubItems(0).Text)
        Next
    End Sub

    Private Sub GetFieldParams()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand("Select FldArea,FldName,FldType from IE_FieldParams Order by 1,2", sqlConn)
        Dim sqlRead As SqlDataReader
        Dim myFldParam As FldParamInfo
        Try
            FldParamList.Clear()
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            With sqlRead
                While .Read
                    myFldParam = New FldParamInfo
                    myFldParam.FldArea = .GetString(0)
                    myFldParam.FldName = .GetString(1)
                    myFldParam.FldType = .GetValue(2)
                    myFldParam.FldValue = New ArrayList
                    FldParamList.Add(myFldParam)
                End While
            End With
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub
    Private Sub GetFieldValues()
        Dim strSql As String = "Select "
        Dim myFldParam As FldParamInfo
        For Each myFldParam In FldParamList
            If myFldParam.FldArea = "PATIENT" Then
                If myFldParam.FldName.ToUpper = "AGE" Then
                    strSql &= "DateDiff(Year,DOB,GetDate()) " & myFldParam.FldName & ","
                ElseIf myFldParam.FldName.ToUpper = "GENDER" Then
                    strSql &= "Case When Gender=1 Then 'MALE' Else 'FEMALE' End  " & myFldParam.FldName & ","
                Else
                    strSql &= myFldParam.FldName & ","
                End If

            End If
        Next
        strSql = strSql.Substring(0, strSql.Length - 1) & " from PatientDemographics Where PatientId=" & iPatientId

        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand(strSql, sqlConn)
        Dim sqlRead As SqlDataReader
        Try
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            If sqlRead.Read Then
                For Each myFldParam In FldParamList
                    If myFldParam.FldArea = "PATIENT" Then
                        myFldParam.FldValue.Add(CType(sqlRead(myFldParam.FldName), String))
                    ElseIf myFldParam.FldArea = "ICD" Or myFldParam.FldArea = "CPT" Or myFldParam.FldArea = "MEDS" Then
                        myFldParam.FldValue = GetICDCPTMEDSInformation(myFldParam.FldArea)
                    End If
                Next
            End If
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub
    Private Function GetICDCPTMEDSInformation(ByVal strCodes As String) As ArrayList
        Dim strSql As String = ""
        Select Case strCodes
            Case "ICD"
                strSql = "Select ICDCode from Encounter_ICD9 Where EncounterId=" & iEncounterId
            Case "CPT"
                strSql = "Select Distinct CPTCode from Encounter_CPT Where EncounterId=" & iEncounterId
            Case "MEDS"
                strSql = "Select Distinct Meds.IngredientName MedName from Encounter_Medications EM,Medication_Formulation MF,Medications MEDS "
                strSql &= "Where EM.EncounterId = " & iEncounterId & " And EM.FormulationId=MF.FormulationID And MF.IngredientId=MEDS.IngredientId"
        End Select
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand(strSql, sqlConn)
        Dim sqlRead As SqlDataReader
        Dim ICDList As New ArrayList

        Try
            ICDList.Clear()
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            With sqlRead
                While .Read
                    ICDList.Add(.GetString(0))
                End While
            End With
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
        Return ICDList
    End Function
    Private Sub GetRules()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand("Select RuleId,RuleName,EventID,Result from IE_Rules", sqlConn)
        Dim sqlRead As SqlDataReader
        Dim myRule As RuleInfo
        Try
            RuleList.Clear()
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            With sqlRead
                While .Read
                    myRule = New RuleInfo
                    myRule.RuleId = .GetValue(0)
                    myRule.RuleName = .GetValue(1)
                    myRule.EventId = .GetValue(2)
                    myRule.Result = .GetString(3)
                    myRule.RuleValue = False
                    RuleList.Add(myRule)
                End While
            End With
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub
    Private Sub GetRulePhrases()
        Dim sqlConn As New SqlConnection(m_ConnectionString)
        Dim sqlComm As New SqlCommand("Select RuleId,Slno,FieldArea,FieldName,Operation,FieldValue,AndOrFlag from IE_RulesPhrases Order By 1,2", sqlConn)
        Dim sqlRead As SqlDataReader
        Dim myRulePhrase As RulePhraseInfo
        Dim strValues, strValue As String
        Dim iPos As Integer

        Try
            RulePhrasesList.Clear()
            sqlConn.Open()
            sqlRead = sqlComm.ExecuteReader(CommandBehavior.CloseConnection)
            With sqlRead
                While .Read
                    myRulePhrase = New RulePhraseInfo
                    myRulePhrase.RuleId = .GetValue(0)
                    myRulePhrase.Slno = .GetValue(1)
                    myRulePhrase.FldArea = .GetString(2)
                    myRulePhrase.FldName = .GetString(3)
                    myRulePhrase.Operation = .GetString(4)
                    myRulePhrase.FldValue = New ArrayList
                    If myRulePhrase.Operation = "IN" Or myRulePhrase.Operation = "Not IN" Then
                        strValues = .GetString(5)
                        iPos = strValues.IndexOf("','")
                        While iPos <> -1
                            strValue = strValues.Substring(1, iPos - 1)
                            strValues = strValues.Substring(iPos + 2, strValues.Length - iPos - 2)
                            iPos = strValues.IndexOf("','")
                            myRulePhrase.FldValue.Add(strValue)
                        End While
                        strValue = strValues.Substring(1, strValues.Length - 2)
                        myRulePhrase.FldValue.Add(strValue)
                    Else
                        strValue = CType(.GetString(5), String)
                        If strValue.Substring(0, 1) = "'" Then strValue = strValue.Substring(1, strValue.Length - 1)
                        If strValue.Substring(strValue.Length - 1, 1) = "'" Then strValue = strValue.Substring(0, strValue.Length - 1)
                        myRulePhrase.FldValue.Add(strValue)
                    End If
                    myRulePhrase.AndOrFlag = .GetString(6)
                    myRulePhrase.PhraseValue = False
                    RulePhrasesList.Add(myRulePhrase)
                End While
            End With
        Catch ex As Exception
        Finally
            sqlRead = Nothing
            sqlComm = Nothing
            If sqlConn.State = ConnectionState.Open Then sqlConn.Close()
            sqlConn = Nothing
        End Try
    End Sub
    Private Sub ResetRuleStatus()
        Dim myRule As RuleInfo
        Dim myRulePhrase As RulePhraseInfo
        For Each myRule In RuleList
            myRule.RuleValue = False
        Next
        For Each myRulePhrase In RulePhrasesList
            myRulePhrase.PhraseValue = False
        Next
    End Sub
    Private Sub EvaluateRule(ByVal RuleId As Integer)
        Dim myRule As RuleInfo
        Dim myRulePhrase As RulePhraseInfo
        Dim myFldParam As FldParamInfo
        Dim bPhraseValue As Boolean
        Dim myFldValue As ArrayList
        Dim myFldType As Integer
        Dim strAndOrExpression As String = ""
        Dim iPos As Integer
        Dim strLeft, strMiddle, strRight As String

        For Each myRulePhrase In RulePhrasesList
            If myRulePhrase.RuleId = RuleId Then
                bPhraseValue = True
                myFldValue = Nothing
                myFldType = 0
                'Get the FieldValue and FieldType
                For Each myFldParam In FldParamList
                    If myFldParam.FldArea = myRulePhrase.FldArea And myFldParam.FldName = myRulePhrase.FldName Then
                        myFldValue = myFldParam.FldValue
                        myFldType = myFldParam.FldType
                        Exit For
                    End If
                Next
                If myFldValue.Count = 0 Or myRulePhrase.FldValue.Count = 0 Then
                    bPhraseValue = False
                Else
                    Select Case myRulePhrase.Operation
                        Case "=", "<", ">", "<=", ">=", "<>"
                            bPhraseValue = EvaluatePhrase(myFldValue, myRulePhrase.Operation, myRulePhrase.FldValue)
                        Case "IN"
                            bPhraseValue = EvaluatePhrase(myFldValue, "=", myRulePhrase.FldValue)
                        Case "Not IN"
                            bPhraseValue = Not EvaluatePhrase(myFldValue, "=", myRulePhrase.FldValue)
                        Case Else
                            bPhraseValue = False
                    End Select
                End If
                myRulePhrase.PhraseValue = bPhraseValue
            End If
        Next
        For Each myRulePhrase In RulePhrasesList
            If myRulePhrase.RuleId = RuleId Then strAndOrExpression &= IIf(myRulePhrase.PhraseValue = True, "1 ", "0 ") & myRulePhrase.AndOrFlag & " "
        Next
        strAndOrExpression = strAndOrExpression.Trim.ToUpper
        'Evaluate AND Operators
        iPos = strAndOrExpression.IndexOf("AND")
        While iPos <> -1
            strLeft = ""
            strRight = ""
            strMiddle = ""
            If iPos > 2 Then strLeft = strAndOrExpression.Substring(0, iPos - 2).Trim
            If iPos + 6 < strAndOrExpression.Length Then strRight = strAndOrExpression.Substring(iPos + 6, strAndOrExpression.Length - iPos - 6).Trim
            strMiddle = strAndOrExpression.Substring(iPos - 2, 7).Trim
            Select Case strMiddle
                Case "1 AND 1"
                    strMiddle = " 1 "
                Case "1 AND 0", "0 AND 1", "0 AND 0"
                    strMiddle = " 0 "
            End Select
            strAndOrExpression = strLeft & strMiddle & strRight
            strAndOrExpression = strAndOrExpression.Trim
            iPos = strAndOrExpression.IndexOf("AND")
        End While
        'Evaluate OR Operators
        iPos = strAndOrExpression.IndexOf("OR")
        While iPos <> -1
            strLeft = ""
            strRight = ""
            strMiddle = ""
            If iPos > 2 Then strLeft = strAndOrExpression.Substring(0, iPos - 2).Trim
            If iPos + 5 < strAndOrExpression.Length Then strRight = strAndOrExpression.Substring(iPos + 5, strAndOrExpression.Length - iPos - 5).Trim
            strMiddle = strAndOrExpression.Substring(iPos - 2, 6).Trim
            Select Case strMiddle
                Case "1 OR 1", "1 OR 0", "0 OR 1"
                    strMiddle = " 1 "
                Case "0 OR 0"
                    strMiddle = " 0 "
            End Select
            strAndOrExpression = strLeft & strMiddle & strRight
            strAndOrExpression = strAndOrExpression.Trim
            iPos = strAndOrExpression.IndexOf("OR")
        End While
        If strAndOrExpression = "1" Then
            For Each myRule In RuleList
                If myRule.RuleId = RuleId Then
                    myRule.RuleValue = True
                    MsgBox("[" & myRule.RuleName & "] Evaluated as TRUE" & vbCrLf & myRule.Result, MsgBoxStyle.Information, "Inference Engine")
                    Exit For
                End If
            Next
        Else
            For Each myRule In RuleList
                If myRule.RuleId = RuleId Then
                    myRule.RuleValue = True
                    MsgBox("[" & myRule.RuleName & "] Evaluated as FALSE", MsgBoxStyle.Information, "Inference Engine")
                    Exit For
                End If
            Next
        End If
    End Sub
    Private Function EvaluatePhrase(ByVal LeftValue As ArrayList, ByVal [Operator] As String, ByVal RightValue As ArrayList) As Boolean
        Dim bResult As Boolean = False
        Dim strLeft, strRight As String
        For Each strLeft In LeftValue
            For Each strRight In RightValue
                Select Case [Operator]
                    Case "="
                        If strLeft = strRight Then bResult = True : Exit For
                    Case "<"
                        If strLeft < strRight Then bResult = True : Exit For
                    Case ">"
                        If strLeft > strRight Then bResult = True : Exit For
                    Case "<="
                        If strLeft <= strRight Then bResult = True : Exit For
                    Case ">="
                        If strLeft >= strRight Then bResult = True : Exit For
                    Case "<>"
                        If strLeft <> strRight Then bResult = True : Exit For
                End Select
            Next
            If bResult Then Exit For
        Next
        Return bResult
    End Function
#End Region

    Private Sub lblPatientSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblPatientSearch.Click
        lblPatientName.Text = ""
        Dim frmPatSearch As New frmPatientSearchScreen
        frmPatientSearchScreen.CONNECTION_STRING = m_ConnectionString
        frmPatSearch.btnSelect.Visible = True
        If frmPatSearch.ShowDialog = DialogResult.OK Then
            m_Patientid = frmPatSearch.PatientId
            lblPatientName.Visible = True
            lblPatientName.Text = frmPatSearch.pFirstName + "  " + frmPatSearch.pLastName
        End If
    End Sub

    Private Sub chkPatientWise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPatientWise.CheckedChanged
        If chkPatientWise.Checked Then
            lblPatientSearch.Enabled = True
            lblPatientName.Visible = True
        Else
            lblPatientSearch.Enabled = False
            lblPatientName.Visible = False
            m_Patientid = 0
        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If lsvRules.SelectedItems.Count > 0 Then
            If lsvRules.SelectedItems.Count > 1 Then
                MsgBox("Please select only one rule", MsgBoxStyle.Information, "Import Rules")
                Exit Sub
            End If
            Result = lsvRules.SelectedItems(0).SubItems(2).Text

            DialogResult = DialogResult.OK
        End If
    End Sub


    Private Sub rtfCondition_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles rtfCondition.LinkClicked
        Try
            System.Diagnostics.Process.Start(e.LinkText)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub lsvRules_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvRules.ItemChecked
        For Each item As ListViewItem In lsvRules.SelectedItems
            If e.Item.Checked Then
                item.Checked = False
            End If
        Next
        If e.Item.Checked Then
            e.Item.Selected = True
        Else
            e.Item.Selected = False
        End If
    End Sub
End Class




