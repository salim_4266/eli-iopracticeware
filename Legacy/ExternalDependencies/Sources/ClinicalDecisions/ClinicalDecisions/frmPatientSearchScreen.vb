Imports System.Data.Sqlclient
Imports System.Xml
Imports System.Text
Imports System.IO
Imports System.Configuration
Public Class frmPatientSearchScreen
    Inherits System.Windows.Forms.Form
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblClear As System.Windows.Forms.Label
    Friend WithEvents btnGetResults As System.Windows.Forms.Button
    Friend WithEvents btnSelect As System.Windows.Forms.Button
    Friend WithEvents inkLName As System.Windows.Forms.TextBox
    Friend WithEvents inkFName As System.Windows.Forms.TextBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents lsvPatientDetails As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblSSN As System.Windows.Forms.Label
    Friend WithEvents inkMInitial As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents inkDOB As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnclose As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPatientSearchScreen))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.inkDOB = New System.Windows.Forms.TextBox
        Me.lblSSN = New System.Windows.Forms.Label
        Me.inkMInitial = New System.Windows.Forms.TextBox
        Me.btnGetResults = New System.Windows.Forms.Button
        Me.lblClear = New System.Windows.Forms.Label
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.lblLastName = New System.Windows.Forms.Label
        Me.inkLName = New System.Windows.Forms.TextBox
        Me.inkFName = New System.Windows.Forms.TextBox
        Me.btnclose = New System.Windows.Forms.Button
        Me.btnSelect = New System.Windows.Forms.Button
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.lsvPatientDetails = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.inkDOB)
        Me.GroupBox1.Controls.Add(Me.lblSSN)
        Me.GroupBox1.Controls.Add(Me.inkMInitial)
        Me.GroupBox1.Controls.Add(Me.btnGetResults)
        Me.GroupBox1.Controls.Add(Me.lblClear)
        Me.GroupBox1.Controls.Add(Me.lblFirstName)
        Me.GroupBox1.Controls.Add(Me.lblLastName)
        Me.GroupBox1.Controls.Add(Me.inkLName)
        Me.GroupBox1.Controls.Add(Me.inkFName)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkGreen
        Me.GroupBox1.Location = New System.Drawing.Point(14, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(664, 75)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Patient Search"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(267, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 13)
        Me.Label1.TabIndex = 66
        Me.Label1.Text = "DOB"
        '
        'inkDOB
        '
        Me.inkDOB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.inkDOB.Location = New System.Drawing.Point(334, 47)
        Me.inkDOB.Name = "inkDOB"
        Me.inkDOB.Size = New System.Drawing.Size(168, 20)
        Me.inkDOB.TabIndex = 65
        '
        'lblSSN
        '
        Me.lblSSN.AutoSize = True
        Me.lblSSN.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSN.Location = New System.Drawing.Point(4, 47)
        Me.lblSSN.Name = "lblSSN"
        Me.lblSSN.Size = New System.Drawing.Size(83, 13)
        Me.lblSSN.TabIndex = 63
        Me.lblSSN.Text = "Middle Initial "
        '
        'inkMInitial
        '
        Me.inkMInitial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.inkMInitial.Location = New System.Drawing.Point(93, 45)
        Me.inkMInitial.Name = "inkMInitial"
        Me.inkMInitial.Size = New System.Drawing.Size(168, 20)
        Me.inkMInitial.TabIndex = 61
        '
        'btnGetResults
        '
        Me.btnGetResults.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnGetResults.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnGetResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetResults.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnGetResults.Location = New System.Drawing.Point(529, 37)
        Me.btnGetResults.Name = "btnGetResults"
        Me.btnGetResults.Size = New System.Drawing.Size(123, 28)
        Me.btnGetResults.TabIndex = 3
        Me.btnGetResults.Text = "Get Results >>"
        Me.btnGetResults.UseVisualStyleBackColor = False
        '
        'lblClear
        '
        Me.lblClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblClear.Image = CType(resources.GetObject("lblClear.Image"), System.Drawing.Image)
        Me.lblClear.Location = New System.Drawing.Point(548, 24)
        Me.lblClear.Name = "lblClear"
        Me.lblClear.Size = New System.Drawing.Size(16, 16)
        Me.lblClear.TabIndex = 60
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(261, 22)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(67, 13)
        Me.lblFirstName.TabIndex = 57
        Me.lblFirstName.Text = "First Name"
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(4, 21)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(67, 13)
        Me.lblLastName.TabIndex = 55
        Me.lblLastName.Text = "Last Name"
        '
        'inkLName
        '
        Me.inkLName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.inkLName.Location = New System.Drawing.Point(93, 20)
        Me.inkLName.Name = "inkLName"
        Me.inkLName.Size = New System.Drawing.Size(168, 20)
        Me.inkLName.TabIndex = 0
        '
        'inkFName
        '
        Me.inkFName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.inkFName.Location = New System.Drawing.Point(333, 20)
        Me.inkFName.Name = "inkFName"
        Me.inkFName.Size = New System.Drawing.Size(168, 20)
        Me.inkFName.TabIndex = 1
        '
        'btnclose
        '
        Me.btnclose.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnclose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnclose.Location = New System.Drawing.Point(547, 324)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(120, 26)
        Me.btnclose.TabIndex = 3
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = False
        '
        'btnSelect
        '
        Me.btnSelect.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnSelect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelect.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSelect.Location = New System.Drawing.Point(377, 324)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(120, 26)
        Me.btnSelect.TabIndex = 3
        Me.btnSelect.Text = "Select"
        Me.btnSelect.UseVisualStyleBackColor = False
        '
        'lsvPatientDetails
        '
        Me.lsvPatientDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader5, Me.ColumnHeader4})
        Me.lsvPatientDetails.FullRowSelect = True
        Me.lsvPatientDetails.GridLines = True
        Me.lsvPatientDetails.Location = New System.Drawing.Point(12, 93)
        Me.lsvPatientDetails.MultiSelect = False
        Me.lsvPatientDetails.Name = "lsvPatientDetails"
        Me.lsvPatientDetails.Size = New System.Drawing.Size(654, 225)
        Me.lsvPatientDetails.TabIndex = 4
        Me.lsvPatientDetails.UseCompatibleStateImageBehavior = False
        Me.lsvPatientDetails.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "LastName"
        Me.ColumnHeader1.Width = 170
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "FirstName"
        Me.ColumnHeader2.Width = 170
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "MiddleInitial"
        Me.ColumnHeader3.Width = 134
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "DOB"
        Me.ColumnHeader5.Width = 150
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "PatientId"
        Me.ColumnHeader4.Width = 0
        '
        'frmPatientSearchScreen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(680, 355)
        Me.Controls.Add(Me.lsvPatientDetails)
        Me.Controls.Add(Me.btnSelect)
        Me.Controls.Add(Me.btnclose)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPatientSearchScreen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Patient Search Screen"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
#Region "Variables"
    Dim sqlConn As SqlConnection
    Dim ds As DataSet
    Dim lsvitem As ListViewItem
    Dim da As SqlDataAdapter
    Dim pLName As String
    Dim pFName As String
    Dim pid As Integer
    Public Shared CONNECTION_STRING As String

#End Region
#Region "Properties"

    Public Property pLastName() As String
        Get
            Return pLName
        End Get
        Set(ByVal Value As String)
            pLName = Value
        End Set
    End Property
    Public Property pFirstName() As String
        Get
            Return pFName
        End Get
        Set(ByVal Value As String)
            pFName = Value
        End Set
    End Property
    Public Property PatientId() As Integer
        Get
            Return pid
        End Get
        Set(ByVal Value As Integer)
            pid = Value
        End Set
    End Property
#End Region
    Private Sub frmPatientSearchScreen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetPatientDetails()
    End Sub
    Public Sub GetPatientDetails()
        Dim strCmdText As String
        strCmdText = "Select Top 500 LastName,FirstName,MiddleInitial,BirthDate as DOB,PatientId From PatientDemographics "
        strCmdText &= "where "
        strCmdText &= " LastName like '" & inkLName.Text & "%' and FirstName like '" & inkFName.Text & "%'  "
        strCmdText &= "and Middleinitial like '" & inkMInitial.Text & "%'  "
        strCmdText &= " and BirthDate like'" & inkDOB.Text & "%' order by LastName"
        Try
            sqlConn = New SqlConnection(CONNECTION_STRING)
            da = New SqlDataAdapter(strCmdText, sqlConn)
            ds = New DataSet
            da.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    lsvitem = New ListViewItem(ds.Tables(0).Rows(i).Item("LastName").ToString)
                    lsvitem.SubItems.Add(ds.Tables(0).Rows(i).Item("FirstName").ToString)
                    lsvitem.SubItems.Add(ds.Tables(0).Rows(i).Item("MiddleInitial").ToString)
                    lsvitem.SubItems.Add(ds.Tables(0).Rows(i).Item("DOB").ToString)
                    lsvitem.SubItems.Add(ds.Tables(0).Rows(i).Item("PatientId").ToString)
                    lsvPatientDetails.Items.Add(lsvitem)
                Next
            Else
                lsvPatientDetails.Items.Clear()
                MsgBox("No Records Found", MsgBoxStyle.OkOnly)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            sqlConn.Close()
            sqlConn = Nothing
            strCmdText = ""
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub
    Private Sub lblClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblClear.Click
        inkFName.Text = String.Empty
        inkLName.Text = String.Empty
        inkDOB.Text = String.Empty
        inkMInitial.Text = String.Empty
    End Sub
    Private Sub btnGetResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetResults.Click
        Try
            lsvPatientDetails.Items.Clear()
            GetPatientDetails()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            If lsvPatientDetails.SelectedItems.Count = Nothing Then
                MessageBox.Show(" Please Select a Patient.. ")
            Else
                Dim objRst As New frmInferenceEngine
                pLastName = lsvPatientDetails.SelectedItems(0).SubItems(0).Text
                pFirstName = lsvPatientDetails.SelectedItems(0).SubItems(1).Text
                PatientId = lsvPatientDetails.SelectedItems(0).SubItems(3).Text
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub inkFName_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles inkFName.KeyDown
        If e.KeyCode = Keys.Enter Then GetPatientDetails()
    End Sub
    Private Sub inkLName_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles inkLName.KeyDown
        If e.KeyCode = Keys.Enter Then GetPatientDetails()
    End Sub
    Private Sub inkMInitial_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles inkMInitial.KeyDown
        If e.KeyCode = Keys.Enter Then GetPatientDetails()
    End Sub
    Private Sub inkDOB_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles inkDOB.KeyDown
        If e.KeyCode = Keys.Enter Then GetPatientDetails()
    End Sub
    Private Sub lsvPatientDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvPatientDetails.DoubleClick
        Try
            If lsvPatientDetails.SelectedItems.Count = Nothing Then
                MessageBox.Show(" Please Select a Patient... ")
            Else
                Dim objRst As New frmInferenceEngine
                pLastName = lsvPatientDetails.SelectedItems(0).SubItems(0).Text
                pFirstName = lsvPatientDetails.SelectedItems(0).SubItems(1).Text
                PatientId = lsvPatientDetails.SelectedItems(0).SubItems(4).Text
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class
