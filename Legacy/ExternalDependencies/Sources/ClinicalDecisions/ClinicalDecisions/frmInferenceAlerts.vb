<System.Runtime.InteropServices.ComVisible(True)> _
Public Class frmInferenceAlerts
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtMessage As System.Windows.Forms.RichTextBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnOverride As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents pnlComments As System.Windows.Forms.Panel
    Friend WithEvents txtComments As System.Windows.Forms.RichTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnsave As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtMessage = New System.Windows.Forms.RichTextBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnOverride = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.pnlComments = New System.Windows.Forms.Panel
        Me.btnsave = New System.Windows.Forms.Button
        Me.txtComments = New System.Windows.Forms.RichTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.pnlComments.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtMessage
        '
        Me.txtMessage.BackColor = System.Drawing.Color.MintCream
        Me.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMessage.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMessage.ForeColor = System.Drawing.Color.Red
        Me.txtMessage.Location = New System.Drawing.Point(1, 22)
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.Size = New System.Drawing.Size(391, 141)
        Me.txtMessage.TabIndex = 3
        Me.txtMessage.Text = ""
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.Label2)
        Me.pnlMain.Controls.Add(Me.btnOverride)
        Me.pnlMain.Controls.Add(Me.btnOK)
        Me.pnlMain.Controls.Add(Me.txtMessage)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(394, 195)
        Me.pnlMain.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label2.Location = New System.Drawing.Point(2, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Alert Message"
        '
        'btnOverride
        '
        Me.btnOverride.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnOverride.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnOverride.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOverride.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnOverride.Location = New System.Drawing.Point(312, 168)
        Me.btnOverride.Name = "btnOverride"
        Me.btnOverride.Size = New System.Drawing.Size(75, 24)
        Me.btnOverride.TabIndex = 35
        Me.btnOverride.Text = "Override"
        Me.btnOverride.UseVisualStyleBackColor = False
        Me.btnOverride.Visible = False
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnOK.Location = New System.Drawing.Point(224, 168)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 24)
        Me.btnOK.TabIndex = 35
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'pnlComments
        '
        Me.pnlComments.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.pnlComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlComments.Controls.Add(Me.btnsave)
        Me.pnlComments.Controls.Add(Me.txtComments)
        Me.pnlComments.Controls.Add(Me.Label1)
        Me.pnlComments.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlComments.Location = New System.Drawing.Point(0, 195)
        Me.pnlComments.Name = "pnlComments"
        Me.pnlComments.Size = New System.Drawing.Size(394, 181)
        Me.pnlComments.TabIndex = 4
        Me.pnlComments.Visible = False
        '
        'btnsave
        '
        Me.btnsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnsave.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnsave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnsave.Location = New System.Drawing.Point(144, 152)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(104, 28)
        Me.btnsave.TabIndex = 35
        Me.btnsave.Text = "Save & Close"
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'txtComments
        '
        Me.txtComments.BackColor = System.Drawing.Color.MintCream
        Me.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtComments.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.ForeColor = System.Drawing.Color.Black
        Me.txtComments.Location = New System.Drawing.Point(2, 22)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(390, 125)
        Me.txtComments.TabIndex = 1
        Me.txtComments.Text = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label1.Location = New System.Drawing.Point(2, 2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 15)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Comments"
        '
        'frmInferenceAlerts
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(394, 413)
        Me.Controls.Add(Me.pnlComments)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmInferenceAlerts"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " Inference Engine Alerts"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlComments.ResumeLayout(False)
        Me.pnlComments.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnOverride_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOverride.Click
        If Not pnlComments.Visible Then
            Me.Height += pnlComments.Height
            Me.Top -= pnlComments.Height / 2
            pnlComments.Show()
            pnlComments.BringToFront()
            txtComments.Clear()
        End If
        txtComments.Focus()
    End Sub


    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.DialogResult = DialogResult.Ignore
    End Sub

    Private Sub txtMessage_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles txtMessage.LinkClicked, txtComments.LinkClicked
        Try
            System.Diagnostics.Process.Start(e.LinkText)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If txtComments.Text = "" Then
            MsgBox("Comments Can't be NULL", MsgBoxStyle.Information, "Alert Comments")
            txtComments.Focus()
            Exit Sub
        End If
        Dim Result As Integer = MsgBox("Override will disable the alert for all patients and all appointments.  Are you sure you want to continue?", MsgBoxStyle.YesNo, "Inference Alerts")
        If MsgBoxResult.Yes = Result Then
            Me.DialogResult = DialogResult.OK
        End If
    End Sub
End Class
