VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmHome 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Patient"
   ClientHeight    =   8595
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H008D312C&
   Icon            =   "PatientInterface.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer2 
      Interval        =   2
      Left            =   360
      Top             =   1920
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Left            =   360
      Top             =   1200
   End
   Begin VB.ListBox lstPrint 
      Height          =   690
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLang1 
      Height          =   1575
      Left            =   1920
      TabIndex        =   3
      Top             =   3480
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientInterface.frx":0442
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLang2 
      Height          =   1575
      Left            =   6480
      TabIndex        =   4
      Top             =   3480
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientInterface.frx":0626
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLang3 
      Height          =   1575
      Left            =   1920
      TabIndex        =   5
      Top             =   5160
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientInterface.frx":080A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLang4 
      Height          =   1575
      Left            =   6480
      TabIndex        =   6
      Top             =   5160
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientInterface.frx":09EE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLang5 
      Height          =   1575
      Left            =   4200
      TabIndex        =   7
      Top             =   6840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientInterface.frx":0BD2
   End
   Begin VB.Label lblVersion 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "Version"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   225
      Left            =   5640
      TabIndex        =   9
      Top             =   480
      Width           =   840
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "IO PracticeWare"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   360
      Left            =   4680
      TabIndex        =   8
      Top             =   120
      Width           =   2655
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "A Really Good Eye Clinic"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   1170
      Left            =   3480
      TabIndex        =   1
      Tag             =   "-"
      Top             =   1680
      Width           =   5175
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblWelcome 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "Welcome to"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   585
      Left            =   4560
      TabIndex        =   0
      Tag             =   "-"
      Top             =   960
      Width           =   2940
   End
End
Attribute VB_Name = "frmHome"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public MyKey As Long
Public QueryType As String
Public LevelType As Integer
Public ChangeRequest As Boolean
Public CloseDown As Boolean
Public ExitFast As Boolean

Private TimeOutExit As Boolean
Private Starting As Boolean
Private Language As String
Private PrintReport As Boolean

Private Const MaxElements As Integer = 300
Private Const MaxDiagnosis As Integer = 100

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private TheStationId As Integer
Private TheQuestionSet As String

Private TimerBase As Integer
Private TimerBaseCount As Integer
Private MaxComplaints As Integer
Private ComplaintList(MaxElements) As String
Private LeftComps As Integer
Private LeftComplaintList(MaxElements) As String
Private RightComps As Integer
Private RightComplaintList(MaxElements) As String
Private BothComps As Integer
Private BothComplaintList(MaxElements) As String

Private ActivityId As Long
Private PatientId As Long
Private AppointmentId As Long
Private QuestionSet As String
Private PatientName As String
Private PatientPhone As String
Private PatientBirthDate As String
Private PatientGender As String
Private PatientOccupation As String
Private Appointment As String
Private AppointmentType As String
Private SchedulerDoctor As String
Private AppointmentReferral As String
Private AppointmentReferralFrom As String
Private FileName As String
Private ThePatient As String

Private DoConfirmOn As Boolean
Private CurrentRecapLocation As Integer
Private RetrieveClass As DynamicClass


Private Type LASTINPUTINFO
cbSize As Long
dwTime As Long
End Type

Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Declare Function GetLastInputInfo Lib "user32" (plii As Any) As Long
Private Declare Sub ExitProcess Lib "kernel32" (ByVal uExitCode As Long)
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function GetCurrentProcess Lib "kernel32" () As Long

Private Function UpdateActivity(ActivityId As Long, Status As String, Revert As Boolean)
Dim PostIt As Boolean
Dim TimeIn As String
Dim RetrieveActivity As PracticeActivity
UpdateActivity = False
PostIt = False
Set RetrieveActivity = New PracticeActivity
RetrieveActivity.ActivityId = ActivityId
If (RetrieveActivity.RetrieveActivity) Then
    Call FormatTimeNow(TimeIn)
    If (Revert) Then
        RetrieveActivity.ActivityStationId = 0
' test to make sure the activity record is in the right state.
        If (InStrPS("EHD", RetrieveActivity.ActivityStatus) = 0) Then
            RetrieveActivity.ActivityStatus = Status
        End If
        PostIt = True
    Else
        If (RetrieveActivity.ActivityStationId <> 0) Then
            RetrieveActivity.ActivityStationId = 999
            PostIt = True
        End If
        If (RetrieveActivity.ActivityStatus <> Status) Then
' test to make sure the activity record is in the right state.
            If (InStrPS("EHD", RetrieveActivity.ActivityStatus) = 0) Then
                RetrieveActivity.ActivityStatus = Status
            End If
            RetrieveActivity.ActivityStatusTime = TimeIn
            RetrieveActivity.ActivityStationId = 0
            PostIt = True
        End If
    End If
    If (PostIt) Then
        If (RetrieveActivity.ApplyActivity) Then
            UpdateActivity = True
        End If
    Else
        UpdateActivity = True
    End If
End If
Set RetrieveActivity = Nothing
End Function

Private Function IsThisYou(TheName As String, Ref As Integer, TotalAppts As Integer) As Boolean
IsThisYou = True
If (TotalAppts > 1) And (Trim(TheName) <> "") Then
    frmEventMsgs.Header = "Are you " + Trim(TheName)
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result <> 2) Then
        IsThisYou = False
    End If
End If
End Function

Private Function AvailableAppointments(ApptDate As String, BirthDate As String) As Integer
Dim k As Integer
Dim PatRef As Integer
Dim RetrievePatient As Patient
Dim RetrieveDoctor As SchedulerResource
Dim RetrieveActivity As PracticeActivity
Dim RetrieveAppointment As SchedulerAppointment
Dim RetrieveAppointmentType As SchedulerAppointmentType
AvailableAppointments = -1
PatientId = 0
AppointmentId = 0
ActivityId = 0
QuestionSet = ""
Set RetrievePatient = New Patient
Set RetrieveActivity = New PracticeActivity
Set RetrieveAppointment = New SchedulerAppointment
Set RetrieveDoctor = New SchedulerResource
Set RetrieveAppointmentType = New SchedulerAppointmentType
PatRef = 0
RetrieveActivity.ActivityLocationId = dbPracticeId
RetrieveActivity.ActivityResourceId = 0
RetrieveActivity.ActivityDate = ApptDate
RetrieveActivity.ActivityStationId = -1
RetrieveActivity.ActivityStatus = "W"
AvailableAppointments = RetrieveActivity.FindActivity
If (AvailableAppointments > 0) Then
    k = 1
    Do Until Not (RetrieveActivity.SelectActivity(k))
        RetrievePatient.PatientId = RetrieveActivity.ActivityPatientId
        If (RetrievePatient.RetrievePatient) Then
            If (Left(BirthDate, 2) = Mid(RetrievePatient.BirthDate, 5, 2)) And _
               (Mid(BirthDate, 4, 2) = Mid(RetrievePatient.BirthDate, 7, 2)) Then
                PatRef = PatRef + 1
                PatientName = RetrievePatient.FirstName + " " _
                            + RetrievePatient.MiddleInitial + " " _
                            + RetrievePatient.LastName + " " _
                            + RetrievePatient.NameRef
                If (IsThisYou(PatientName, PatRef, AvailableAppointments)) Then
                    PatientBirthDate = RetrievePatient.BirthDate
                    PatientId = RetrievePatient.PatientId
                    AppointmentId = RetrieveActivity.ActivityAppointmentId
                    ActivityId = RetrieveActivity.ActivityId
                    QuestionSet = RetrieveActivity.ActivityQuestionSet
                    PatientOccupation = RetrievePatient.Occupation
                    PatientGender = RetrievePatient.Gender
                    AppointmentType = ""
                    SchedulerDoctor = ""
                    RetrieveAppointment.AppointmentId = RetrieveActivity.ActivityAppointmentId
                    If (RetrieveAppointment.RetrieveSchedulerAppointment) Then
                        RetrieveDoctor.ResourceId = RetrieveAppointment.AppointmentResourceId1
                        If (RetrieveDoctor.RetrieveSchedulerResource) Then
                            SchedulerDoctor = RetrieveDoctor.ResourceName
                        End If
                        RetrieveAppointmentType.AppointmentTypeId = RetrieveAppointment.AppointmentTypeId
                        If (RetrieveAppointmentType.RetrieveSchedulerAppointmentType) Then
                            AppointmentType = RetrieveAppointmentType.AppointmentType
                        End If
                    End If
                    Exit Do
                End If
            End If
        End If
        k = k + 1
    Loop
End If
Set RetrieveActivity = Nothing
Set RetrieveDoctor = Nothing
Set RetrievePatient = Nothing
Set RetrieveAppointment = Nothing
Set RetrieveAppointmentType = Nothing
End Function

Private Sub Startup()
On Error GoTo UI_ErrorHandler
Dim i As Integer, k As Integer
Dim TOExit As Boolean
Dim Temp As String, FileFamily As String
Dim MatchingAppointments As Integer
Dim IODateTime As New CIODateTime
Dim RetrieveActivity As PracticeActivity
LevelType = 1
ExitFast = False
CloseDown = False
CurrentRecapLocation = 0
IODateTime.MyDateTime = Now
frmPatientMonth.Language = Language
frmPatientMonth.Show 1
If (frmPatientMonth.ExitFast) Then
    Call TriggerNewLanguage(cmdLang1.ToolTipText)
    Exit Sub
End If

frmPatientDayYear.Language = Language
frmPatientDayYear.TheMonth = frmPatientMonth.TheMonthName
frmPatientDayYear.Show 1
If (frmPatientDayYear.ExitFast) Then
    Call TriggerNewLanguage(cmdLang1.ToolTipText)
    Exit Sub
End If
Temp = frmPatientMonth.TheMonth + "/" + frmPatientDayYear.TheDay
PatientBirthDate = Temp
PatientId = 0
AppointmentId = 0
ActivityId = 0
QuestionSet = ""
MatchingAppointments = AvailableAppointments(IODateTime.GetIODate, PatientBirthDate)
Set IODateTime = Nothing
If (MatchingAppointments < 1) Then
    Call TriggerNewLanguage(cmdLang1.ToolTipText)
    Exit Sub
End If

Set RetrieveActivity = New PracticeActivity
RetrieveActivity.ActivityId = ActivityId
If (RetrieveActivity.RetrieveActivity) Then
    If (RetrieveActivity.ActivityStationId <> 0) Then
        Call TriggerNewLanguage(cmdLang1.ToolTipText)
        Set RetrieveActivity = Nothing
        Exit Sub
    End If
    ActivityId = RetrieveActivity.ActivityId
End If
Set RetrieveActivity = Nothing

TOExit = False
If (PatientId > 0) Then
    Call InsertLog("FrmHome.frm", "Patient Logged In " & CStr(PatientId) & " ", LoginCatg.PatientLogIn, "", "", PatientId, 0)
    frmHome.lblWelcome.Visible = False
    frmHome.Label2.Visible = False
    Call UpdateActivity(ActivityId, "W", False)
    FileFamily = "*" + "_" + Trim(Str(AppointmentId)) + "_" + Trim(Str(PatientId)) + ".txt"
    Call CleanUp(PatientInterfaceDirectory, FileFamily, False)
    TheQuestionSet = QuestionSet
    ThePatient = Trim(Str(PatientId))
    If (PatientQuery("I", QuestionSet, TOExit)) Then
        If Not (TOExit) Then
            TOExit = False
            If (PatientQuery("P", QuestionSet, TOExit)) Then
                If Not (ExitFast) Then
                    If (SubmitTransaction) Then
                        Call UpdateActivity(ActivityId, "M", False)
                        Call PrintPIAppointment
                    Else
                        Call UpdateActivity(ActivityId, "W", True)
                        Call PinpointError("PI-Submit", "Transaction Not Submitted")
                    End If
                Else
                    Call UpdateActivity(ActivityId, "W", True)
                End If
            Else
                Call UpdateActivity(ActivityId, "W", True)
            End If
        Else
            If (SubmitTransaction) Then
                Call UpdateActivity(ActivityId, "M", False)
                Call PrintPIAppointment
            Else
                Call UpdateActivity(ActivityId, "W", True)
            End If
        End If
    Else
        Call UpdateActivity(ActivityId, "W", True)
    End If
End If
frmHome.lblWelcome.Visible = True
frmHome.Label2.Visible = True
If (PatientId > 0) Then
    FileFamily = "*" + "_" + Trim(Str(AppointmentId)) + "_" + Trim(Str(PatientId)) + ".txt"
    Call CleanUp(PatientInterfaceDirectory, FileFamily, False)
End If
Call TriggerNewLanguage(cmdLang1.ToolTipText)
If SubmitTransaction And PatientId > 0 Then
    Call InsertLog("FrmClinicalEntry.frm", "Patient Logged Out " & CStr(ThePatient) & "", LoginCatg.PatientLogOut, "", "", PatientId, 0)
End If
Exit Sub
UI_ErrorHandler:
    Call PinpointError("PI-Start", Error(Err))
    Call UpdateActivity(ActivityId, "W", True)
    Resume LeaveFast
LeaveFast:
End Sub

Private Function PatientQuery(ClassParty As String, QuestionSet As String, TimedOutExitPost As Boolean) As Boolean
Dim ClinicalClass As DynamicClass
Dim TimeoutTriggered As Boolean
Dim i As Integer, ForkMax As Integer
Dim Tick As Integer, TotalForms As Integer
Dim ATime As Integer, BTime As Integer
Dim TempMaxComplaints As Integer
Dim Temp As String, TempFN As String
Dim TheFile As String, OrderRef As String
Dim TheTime As String, ControlName As String
Dim DoIt As Boolean, TimeOutOn As Boolean
Dim ForkQuestion(10) As Boolean
Dim ForkQuestionCheck(10) As Boolean
Dim ProcessQuestion As Boolean, ForkOn As Boolean
Dim ForkQuestionOff(10) As Long
Dim ForkQuestionRef(10) As String
PatientQuery = True
Timer1.Enabled = False
TimeOutOn = False
TimedOutExitPost = False
TimeoutTriggered = False
ForkOn = False
ForkMax = 0
For i = 0 To 2
    ForkQuestion(i) = False
    ForkQuestionCheck(i) = False
    ForkQuestionRef(i) = ""
    ForkQuestionOff(i) = 0
Next i
TimerBase = 1
i = 0
CloseDown = False
ExitFast = False
MaxComplaints = 0
Set ClinicalClass = New DynamicClass
ClinicalClass.QuestionParty = ClassParty
ClinicalClass.QuestionSet = QuestionSet
TotalForms = ClinicalClass.FindClassForms
If (TotalForms > 0) Then
    i = 1
    TheTime = ""
    Call FormatTimeNow(TheTime)
    ATime = ConvertTimeToMinutes(TheTime)
    Call SetInterfaceControlsVisible(False)
    Do Until Not (ClinicalClass.SelectClassForm(i))
        If (ExitFast) Or (TimeoutTriggered) Then
            Exit Do
        End If
'        Timer1.Interval = 30000
'        TimerBaseCount = 1
'        TimerBase = ClinicalClass.TimeOut
'        If (TimerBase < 1) Then
'            TimerBase = 3
'        ElseIf (TimerBase > 10) Then
'            TimerBase = 10
'        End If
'        If (ClassParty = "I") And (i = 1) Then
'            TimerBase = 1
'        End If
        If (i = TotalForms) Then
'            TimerBase = 1
'            Timer1.Interval = 15000
'            Timer1.Enabled = True
'            Timer1.Enabled = False
        End If
' turn off time for now
        Timer1.Enabled = False
        ProcessQuestion = True
        If (Trim(Left(ClinicalClass.ControlName, 1)) = "T") Then
            ForkOn = True
            ForkMax = ForkMax + 1
            ForkQuestion(ForkMax) = False
            ForkQuestionCheck(ForkMax) = True
            ForkQuestionRef(ForkMax) = Trim(Mid(ClinicalClass.ControlName, 2, 2))
            ForkQuestionOff(ForkMax) = Val(Mid(ClinicalClass.ControlName, 4, 3))
            If (ForkQuestionOff(ForkMax) < 1) Or (Trim(ForkQuestionRef(ForkMax)) = "") Then
                ForkQuestion(ForkMax) = False
                ForkQuestionCheck(ForkMax) = False
                ForkQuestionRef(ForkMax) = ""
                ForkQuestionOff(ForkMax) = 0
                ForkMax = ForkMax - 1
            End If
        End If
        If (ClinicalClass.QuestionFamily <> 10) Then
            TempMaxComplaints = 0
            ProcessQuestion = False
            If (ForkQuestion(1)) Then
                If (ForkQuestionRef(1) = Trim(ClinicalClass.ControlName)) Then
                    ProcessQuestion = True
                End If
                If (ClinicalClass.ClassId = ForkQuestionOff(1)) Then
                    ForkQuestion(1) = False
                End If
            End If
            If (ForkQuestion(2)) Then
                If (ForkQuestionRef(2) = Trim(ClinicalClass.ControlName)) Then
                    ProcessQuestion = True
                End If
                If (ClinicalClass.ClassId = ForkQuestionOff(2)) Then
                    ForkQuestion(2) = False
                End If
            End If
            If (ForkQuestion(1)) And (ForkQuestion(2)) Then
                ProcessQuestion = True
            ElseIf Not (ForkQuestion(1)) And Not (ForkQuestion(2)) And Not (ForkOn) Then
                ProcessQuestion = True
            End If
            If Not (ProcessQuestion) Then
                If (ForkOn) Then
                    If (Len(Trim(ClinicalClass.ControlName)) >= 3) Then
                        ProcessQuestion = True
                    End If
                End If
                If (ForkQuestionOff(1) < ClinicalClass.ClassId) Then
                    ProcessQuestion = True
                End If
                If (Trim(ClinicalClass.ControlName) = "") Then
                    ProcessQuestion = True
                End If
            End If
            If (ProcessQuestion) Then
                TheFile = PatientInterfaceDirectory + ClassParty + ClinicalClass.QuestionOrder + "_" + Trim(Str(AppointmentId)) + "_" + ThePatient + ".txt"
                ChangeRequest = False
                frmClinicalEntry.BarOn = False
                If (ClassParty = "I") And (i = 2) Then
                    frmClinicalEntry.BarOn = True
                End If
                frmHome.Enabled = False
                Set frmClinicalEntry.InitialForm = frmHome
                frmClinicalEntry.Language = ""
                If (Language <> "English") Then
                    frmClinicalEntry.Language = Language
                End If
                frmClinicalEntry.ChangeOn = False
                frmClinicalEntry.LevelType = LevelType
                frmClinicalEntry.StoreResults = TheFile
                frmClinicalEntry.Question = UCase(Trim(ClinicalClass.Question))
                frmClinicalEntry.QuestionFamily = ClinicalClass.QuestionFamily
                frmClinicalEntry.ButtonSelectionColor = ButtonSelectionColor
                frmClinicalEntry.TextSelectionColor = TextSelectionColor
                frmClinicalEntry.FormOn = True
'                If (TimerBase > 0) Then
'                    Timer1.Enabled = True
'                    Timer1.Enabled = False
'                End If
                frmClinicalEntry.Show
                Tick = 0
                While (frmClinicalEntry.FormOn) And Not (frmClinicalEntry.CloseDown)
                    Tick = Tick + 1
                    If (Tick = 1000) Then
                        DoEvents
                        Tick = 0
                    End If
                Wend
                Timer1.Enabled = False
                Set frmClinicalEntry.InitialForm = Nothing
                frmHome.Enabled = True
                If (ClassParty = "P") Then
                    If (ChangeRequest) Then
                        TempMaxComplaints = MaxComplaints
                    End If
                    Call PushList(TheFile, MaxComplaints)
                End If
                If (ChangeRequest) Then
                    If (DoConfirmOn) Then
                        Call DoPatientRecap
                    End If
                    MaxComplaints = TempMaxComplaints
                    i = i - 1
                Else
                    If (ForkQuestionCheck(ForkMax)) Then
                        If (IsResponseTrue(TheFile)) Then
                            ForkQuestion(ForkMax) = True
                            ForkQuestionCheck(ForkMax) = False
                        Else
                            ForkQuestion(ForkMax) = False
                            ForkQuestionCheck(ForkMax) = False
                            ForkQuestionRef(ForkMax) = ""
                        End If
                    End If
'                    TheTime = ""
'                    Call FormatTimeNow(TheTime)
'                    BTime = ConvertTimeToMinutes(TheTime)
'                    If (TimerBase > 0) And Not TimeOutOn Then
'                        If ((BTime - ATime) >= TimerBase) Then
'                            i = TotalForms - 1
'                            TimeOutOn = True
'                            If (ClassParty = "I") Then
'                                ClassParty = "P"
'                                i = 1
'                                TimedOutExitPost = True
'                                Exit Do
'                            End If
'                        End If
'                    ElseIf (TimerBase > 0) And (ClassParty = "I") And (TimeOutExit) Then
'                        TimedOutExitPost = True
'                        Exit Do
'                    End If
                End If
            End If
        End If
        i = i + 1
        If (CloseDown) Then
'            If (TimeoutTriggered) Then
'                ExitFast = False
'                Timer1.Enabled = False
'                Exit Do
'            End If
            i = TotalForms
            CloseDown = False
            ExitFast = True
'            If (TimeOutExit) Then
'                ExitFast = False
'                TimeoutTriggered = True
'            End If
            Timer1.Enabled = False
        End If
    Loop
    Call SetInterfaceControlsVisible(True)
End If
Set ClinicalClass = Nothing
If (i < 1) And (ClassParty = "P") Then
    PatientQuery = False
End If
End Function

Private Sub PushList(FileName As String, ListItem As Integer)
Dim FileNum As Integer
Dim MatchingLevel As Integer
Dim CurrentLevel As Integer
Dim Level1 As Integer
Dim GotIt As Integer
Dim k As Integer, w As Integer
Dim g As Integer, p As Integer
Dim q As Integer, r As Integer
Dim MatchRef As String
Dim LevelMatch(50) As String
Dim Recap(50) As String
Dim DiRecap(50) As String
Dim TheRecord As String
Dim Question As String
Dim NotSureOn As Boolean
Dim ControlLevel1 As DynamicControls
Dim ControlLevel2 As DynamicSecondaryControls
Dim ControlLevel3 As DynamicThirdControls
Dim ControlLevel4 As DynamicFourthControls
p = 1
FileNum = 0
Level1 = 0
MatchingLevel = 0
Erase Recap
Erase DiRecap
If (FM.IsFileThere(FileName)) Then
    FileNum = FreeFile
    FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until (EOF(FileNum))
        Line Input #FileNum, TheRecord
        If (InStrPS(TheRecord, "QUESTION=") <> 0) Then
            Question = Trim(Mid(TheRecord, 10, Len(TheRecord) - 9))
            ComplaintList(ListItem) = Question
            If (Left(Question, 1) = "/") Then
                CurrentLevel = 1
            ElseIf (Left(Question, 1) = "?") Then
                CurrentLevel = 2
            ElseIf (Left(Question, 1) = "]") Then
                CurrentLevel = 3
            ElseIf (Left(Question, 1) = "[") Then
                CurrentLevel = 4
            Else
                CurrentLevel = 0
            End If
        Else
            Set ControlLevel1 = New DynamicControls
            Set ControlLevel2 = New DynamicSecondaryControls
            Set ControlLevel3 = New DynamicThirdControls
            Set ControlLevel4 = New DynamicFourthControls
            NotSureOn = False
            If (InStrPS(TheRecord, "NOTSURE=T") > 0) Then
                NotSureOn = True
            End If
            r = InStrPS(TheRecord, "=")
            If (r = 0) Then
                r = Len(TheRecord) + 1
            End If
            r = r - 1
            w = r + 3
            If (CurrentLevel = 1) Then
                GotIt = InStrPS(w, TheRecord, "<")
                If (GotIt <> 0) Then
                    MatchRef = Mid(TheRecord, GotIt, Len(TheRecord) - (GotIt - 1))
                Else
                    MatchRef = ""
                End If
                If Not (NotSureOn) Then
                    ControlLevel1.Language = Language
                    ControlLevel1.ControlId = Val(Trim(Mid(TheRecord, w, (GotIt - w))))
                    ControlLevel1.FormId = 0
                    ControlLevel1.ControlName = ""
                    ControlLevel1.IEChoiceName = ""
                    If (ControlLevel1.RetrieveControl) Then
                        If (Trim(ControlLevel1.ConfirmLingo) <> "") Then
                            Level1 = Level1 + 1
                            Recap(Level1) = Trim(ControlLevel1.ConfirmLingo)
                        Else
                            Level1 = Level1 + 1
                            Recap(Level1) = "-"
                        End If
                        If (Trim(ControlLevel1.DoctorLingo) <> "") Then
                            DiRecap(Level1) = Trim(ControlLevel1.DoctorLingo)
                        Else
                            DiRecap(Level1) = "-"
                        End If
                        LevelMatch(Level1) = MatchRef
                    End If
                Else
                    Level1 = Level1 + 1
                    Recap(Level1) = "For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                    DiRecap(Level1) = "For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                End If
            ElseIf (CurrentLevel = 2) Then
                GotIt = InStrPS(w, TheRecord, "<")
                If (GotIt <> 0) Then
                    MatchRef = Mid(TheRecord, GotIt, Len(TheRecord) - (GotIt - 1))
                Else
                    MatchRef = ""
                End If
                MatchingLevel = 0
                For k = 1 To Level1
                    If (MatchRef = LevelMatch(k)) Then
                        MatchingLevel = k
                        k = Level1
                    End If
                Next k
                If Not (NotSureOn) Then
                    ControlLevel2.Language = Language
                    ControlLevel2.SecondaryControlId = Val(Trim(Mid(TheRecord, w, (GotIt - w))))
                    ControlLevel2.SecondaryFormId = 0
                    ControlLevel2.ControlName = ""
                    ControlLevel2.IEChoiceName = ""
                    If (ControlLevel2.RetrieveControl) Then
                        If (Trim(ControlLevel2.ConfirmLingo) <> "") Then
                            If (MatchingLevel <> 0) Then
                                Recap(MatchingLevel) = Recap(MatchingLevel) + " " + Trim(ControlLevel2.ConfirmLingo)
                            End If
                        End If
                        If (Trim(ControlLevel2.DoctorLingo) <> "") Then
                            If (MatchingLevel <> 0) Then
                                DiRecap(MatchingLevel) = DiRecap(MatchingLevel) + " " + Trim(ControlLevel2.DoctorLingo)
                            End If
                        End If
                    End If
                Else
                    If (MatchingLevel <> 0) Then
                        Recap(MatchingLevel) = Recap(MatchingLevel) + " For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                        DiRecap(MatchingLevel) = DiRecap(MatchingLevel) + " For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                    End If
                End If
            ElseIf (CurrentLevel = 3) Then
                GotIt = InStrPS(w, TheRecord, "<")
                If (GotIt <> 0) Then
                    MatchRef = Mid(TheRecord, GotIt, Len(TheRecord) - (GotIt - 1))
                Else
                    MatchRef = ""
                End If
                MatchingLevel = 0
                For k = 1 To Level1
                    If (MatchRef = LevelMatch(k)) Then
                        MatchingLevel = k
                        k = Level1
                    End If
                Next k
                If Not (NotSureOn) Then
                    ControlLevel3.Language = Language
                    ControlLevel3.ThirdControlId = Val(Trim(Mid(TheRecord, w, (GotIt - w))))
                    ControlLevel3.ThirdFormId = 0
                    ControlLevel3.ControlName = ""
                    ControlLevel3.IEChoiceName = ""
                    If (ControlLevel3.RetrieveControl) Then
                        If (Trim(ControlLevel3.ConfirmLingo) <> "") Then
                            If (MatchingLevel <> 0) Then
                                Recap(MatchingLevel) = Recap(MatchingLevel) + " " + Trim(ControlLevel3.ConfirmLingo)
                            End If
                        End If
                        If (Trim(ControlLevel3.DoctorLingo) <> "") Then
                            If (MatchingLevel <> 0) Then
                                DiRecap(MatchingLevel) = DiRecap(MatchingLevel) + " " + Trim(ControlLevel3.DoctorLingo)
                            End If
                        End If
                    End If
                Else
                    If (MatchingLevel <> 0) Then
                        Recap(MatchingLevel) = Recap(MatchingLevel) + " For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                        DiRecap(MatchingLevel) = DiRecap(MatchingLevel) + " For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                    End If
                End If
            ElseIf (CurrentLevel = 4) Then
                GotIt = InStrPS(w, TheRecord, "<")
                If (GotIt <> 0) Then
                    MatchRef = Mid(TheRecord, GotIt, Len(TheRecord) - (GotIt - 1))
                Else
                    MatchRef = ""
                End If
                MatchingLevel = 0
                For k = 1 To Level1
                    If (MatchRef = LevelMatch(k)) Then
                        MatchingLevel = k
                        k = Level1
                    End If
                Next k
                If Not (NotSureOn) Then
                    ControlLevel4.Language = Language
                    ControlLevel4.FourthControlId = Val(Trim(Mid(TheRecord, w, (GotIt - w))))
                    ControlLevel4.FourthFormId = 0
                    ControlLevel4.ControlName = ""
                    ControlLevel4.IEChoiceName = ""
                    If (ControlLevel4.RetrieveControl) Then
                        If (Trim(ControlLevel4.ConfirmLingo) <> "") Then
                            If (MatchingLevel <> 0) Then
                                Recap(MatchingLevel) = Recap(MatchingLevel) + " " + Trim(ControlLevel4.ConfirmLingo)
                            End If
                        End If
                        If (Trim(ControlLevel4.DoctorLingo) <> "") Then
                            If (MatchingLevel <> 0) Then
                                DiRecap(MatchingLevel) = DiRecap(MatchingLevel) + " " + Trim(ControlLevel4.DoctorLingo)
                            End If
                        End If
                    End If
                Else
                    If (MatchingLevel <> 0) Then
                        Recap(MatchingLevel) = Recap(MatchingLevel) + " For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                        DiRecap(MatchingLevel) = DiRecap(MatchingLevel) + " For " + Mid(Question, 2, Len(Question) - 1) + " Patient is Not Sure"
                   End If
                End If
            End If
            ComplaintList(ListItem) = ">" + Trim(TheRecord)
            p = p + 1
            Set ControlLevel1 = Nothing
            Set ControlLevel2 = Nothing
            Set ControlLevel3 = Nothing
            Set ControlLevel4 = Nothing
        End If
        ListItem = ListItem + 1
    Loop
    FM.CloseFile CLng(FileNum)

' Build Recap Values
    FM.OpenFile FileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
    For g = 1 To Level1
        If (Trim(Recap(g)) <> "") And (Trim(Recap(g)) <> "-") Then
            Print #FileNum, "Recap=" + Recap(g)
        End If
    Next g
    For g = 1 To Level1
        If (Trim(DiRecap(g)) <> "") And (Trim(DiRecap(g)) <> "-") Then
            Print #FileNum, "DiRecap=" + DiRecap(g)
        End If
    Next g
    FM.CloseFile CLng(FileNum)
End If
End Sub

Private Function SubmitTransaction() As Boolean
Dim i As Integer
Dim k As Integer
Dim q As Integer
Dim Symptom As String
Dim Finding As String
Dim ClinicalRec As PatientClinical
SubmitTransaction = False
Call CreateComplaints
Set ClinicalRec = New PatientClinical
For i = 1 To BothComps Step 2
    If (Trim(BothComplaintList(i)) <> "") Then
        ClinicalRec.ClinicalId = 0
        Call ClinicalRec.RetrievePatientClinical
        ClinicalRec.AppointmentId = AppointmentId
        ClinicalRec.PatientId = PatientId
        ClinicalRec.ClinicalType = "C"
        ClinicalRec.EyeContext = "OU"
        ClinicalRec.Symptom = Trim(BothComplaintList(i))
        If (Trim(BothComplaintList(i + 1)) <> "") Then
            Finding = Mid(BothComplaintList(i + 1), 2, Len(BothComplaintList(i + 1)) - 1)
            k = InStrPS(Finding, "=")
            If (k <> 0) Then
                q = InStrPS(k + 1, Finding, "<")
                If (q <> 0) Then
                    ClinicalRec.Findings = Trim(Left(Finding, q - 1))
                Else
                    ClinicalRec.Findings = Trim(Finding)
                End If
                If Not (ClinicalRec.ApplyPatientClinical) Then
                    Call PinpointError("PI-Submit", "Left Complaint Not Posted *")
                End If
            Else
                Call PinpointError("PI-Submit", "Left Complaint did not have an =")
            End If
        End If
    End If
Next i
For i = 1 To LeftComps Step 2
    If (Trim(LeftComplaintList(i)) <> "") Then
        ClinicalRec.ClinicalId = 0
        Call ClinicalRec.RetrievePatientClinical
        ClinicalRec.AppointmentId = AppointmentId
        ClinicalRec.PatientId = PatientId
        ClinicalRec.ClinicalType = "C"
        ClinicalRec.EyeContext = "OS"
        ClinicalRec.Symptom = Trim(LeftComplaintList(i))
        If (Trim(LeftComplaintList(i + 1)) <> "") Then
            Finding = Mid(LeftComplaintList(i + 1), 2, Len(LeftComplaintList(i + 1)) - 1)
            k = InStrPS(Finding, "=")
            If (k <> 0) Then
                q = InStrPS(k + 1, Finding, "<")
                If (q <> 0) Then
                    ClinicalRec.Findings = Trim(Left(Finding, q - 1))
                Else
                    ClinicalRec.Findings = Trim(Finding)
                End If
                If Not (ClinicalRec.ApplyPatientClinical) Then
                    Call PinpointError("PI-Submit", "Left Complaint Not Posted *")
                End If
            Else
                Call PinpointError("PI-Submit", "Left Complaint did not have an =")
            End If
        End If
    End If
Next i
For i = 1 To RightComps Step 2
    If (Trim(RightComplaintList(i)) <> "") Then
        ClinicalRec.ClinicalId = 0
        Call ClinicalRec.RetrievePatientClinical
        ClinicalRec.AppointmentId = AppointmentId
        ClinicalRec.PatientId = PatientId
        ClinicalRec.ClinicalType = "C"
        ClinicalRec.EyeContext = "OD"
        ClinicalRec.Symptom = Trim(RightComplaintList(i))
        If (Trim(RightComplaintList(i + 1)) <> "") Then
            Finding = Mid(RightComplaintList(i + 1), 2, Len(RightComplaintList(i + 1)) - 1)
            k = InStrPS(Finding, "=")
            If (k <> 0) Then
                q = InStrPS(k + 1, Finding, "<")
                If (q <> 0) Then
                    ClinicalRec.Findings = Trim(Left(Finding, q - 1))
                Else
                    ClinicalRec.Findings = Trim(Finding)
                End If
                If Not (ClinicalRec.ApplyPatientClinical) Then
                    Call PinpointError("PI-Submit", "Right Complaint Not Posted *")
                End If
            Else
                Call PinpointError("PI-Submit", "Right Complaint did not have an =")
            End If
        End If
    End If
Next i
Set ClinicalRec = Nothing
SubmitTransaction = True
End Function

Private Function Login() As Boolean
    If LenB(UserLogin.sNameLong) Then
        Login = True
    Else
        Call UserLogin.DisplayLoginScreen
        If LenB(UserLogin.sNameLong) Then
            Login = True
            If UserLogin.HasPermission(epPowerUser) Then
                frmHome.BorderStyle = 1
                frmHome.ClipControls = True
                frmHome.Caption = Mid$(frmHome.Name, 4, Len(frmHome.Name) - 3)
                frmHome.AutoRedraw = True
                frmHome.Refresh
            End If
        End If
    End If
End Function

Private Sub Form_Load()
Call EstablishDirectory

Set UserLogin = New CUserLogin
If Not (UserLogin.IsLoggedIn) Then
    If (Not Login) Then
        End
    End If
End If

Dim Temp As String
Dim Practice As PracticeName
Timer2.Interval = 2
Timer2.Enabled = True
TimeOutExit = False
frmHome.ZOrder 0
ButtonSelectionColor = 14745312
TextSelectionColor = 0
frmHome.MousePointer = 1
frmHome.KeyPreview = True
Label2.Caption = ""
LoadConfigCollection
SetGlobalDefaults
TheStationId = 0
PrintReport = CheckConfigCollection("PRINTPI") = "T"
DoConfirmOn = CheckConfigCollection("CONFIRMON") = "T"
Label2.Caption = "IO PracticeWare Patient " + dbSoftwareLocation
Set Practice = New PracticeName
Practice.PracticeType = "P"
Practice.PracticeName = Chr(1)
Practice.PracticeLocationReference = Replace(dbSoftwareLocation, "Office-", "")
If (Practice.FindPractice > 0) Then
    If (Practice.SelectPractice(1)) Then
        Label2.Caption = Practice.PracticeName + " " + dbSoftwareLocation
        If (Trim(dbSoftwareLocation) <> "") Then
            dbPracticeId = Practice.PracticeId + 1000
        End If
    End If
End If
Set Practice = Nothing
lblVersion.Caption = "9.1" 'PatientInterfaceVersionId
Starting = True
Language = ""
Call cmdLang1_Click
If (Starting) Then
    Starting = False
End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    Call dbPinpointClose
    End
End If
End Sub

Public Sub DoPatientRecap()
Dim ChangeRequest As Boolean
ChangeRequest = True
While (ChangeRequest)
    ChangeRequest = False
    If (LoadRecap) Then
        frmPatientChange.Language = Language
        frmPatientChange.PatientId = PatientId
        frmPatientChange.ItemChosen = ""
        frmPatientChange.ChangeContent = False
        If (frmPatientChange.LoadPatientChangeDisplay) Then
            frmPatientChange.Show 1
            ChangeRequest = frmPatientChange.ChangeContent
            If (ChangeRequest) Then
                If (frmPatientChange.ItemChosen <> "") Then
                    Call ProcessForm(frmPatientChange.ItemChosen, TheQuestionSet)
                End If
            End If
        End If
    End If
Wend
End Sub

Private Function LoadRecap() As Boolean
Dim i As Integer
Dim MyFile As String
LoadRecap = False
frmPatientChange.lstOrderRef.Clear
MyFile = Dir(PatientInterfaceDirectory + "P*" + "_" + Trim(Str(AppointmentId)) + "_" + Trim(Str(PatientId)) + ".txt")
Do Until (MyFile = "")
    i = InStrPS(MyFile, "_")
    If (i <> 0) Then
        i = i - 1
        frmPatientChange.lstOrderRef.AddItem Mid(MyFile, 2, i - 1)
        LoadRecap = True
    End If
    MyFile = Dir
Loop
End Function

Private Sub ProcessForm(FormRef As String, QuestionSet As String)
Dim Tick As Integer
Dim TheFile As String
Dim OrderRef As String
Dim ClinicalClass As DynamicClass
Set ClinicalClass = New DynamicClass
ClinicalClass.QuestionParty = "P"
ClinicalClass.QuestionOrder = FormRef
ClinicalClass.QuestionSet = QuestionSet
If (ClinicalClass.FindClassForms > 0) Then
    If (ClinicalClass.SelectClassForm(1)) Then
        TheFile = PatientInterfaceDirectory + "P" + ClinicalClass.QuestionOrder + "_" + Trim(Str(AppointmentId)) + "_" + ThePatient + ".txt"
        ChangeRequest = False
        frmHome.Enabled = False
        frmHome.Visible = False
        Set frmClinicalEntry.InitialForm = frmHome
        frmClinicalEntry.ChangeRequest = False
        frmClinicalEntry.BarOn = False
        frmClinicalEntry.ChangeOn = True
        frmClinicalEntry.LevelType = LevelType
        frmClinicalEntry.StoreResults = TheFile
        frmClinicalEntry.Question = UCase(Trim(ClinicalClass.Question))
        frmClinicalEntry.ButtonSelectionColor = ButtonSelectionColor
        frmClinicalEntry.TextSelectionColor = TextSelectionColor
        frmClinicalEntry.FormOn = True
        frmClinicalEntry.Show
        Tick = 0
        While (frmClinicalEntry.FormOn) And Not (frmClinicalEntry.CloseDown)
            Tick = Tick + 1
            If (Tick = 1000) Then
                DoEvents
                Tick = 0
            End If
        Wend
        Set frmClinicalEntry.InitialForm = Nothing
        frmHome.Enabled = True
        frmHome.Visible = True
        Call PushList(TheFile, MaxComplaints)
    End If
End If
Set ClinicalClass = Nothing
End Sub

Private Function GetItem(Code As String, StartingPoint As Integer, EndPoint As Integer) As Integer
Dim q As Integer
GetItem = 0
For q = StartingPoint To EndPoint
    If (InStrPS(ComplaintList(q), Code) <> 0) Then
        GetItem = q
        Exit For
    End If
Next q
End Function

Private Function WhichEye(StartingPoint As Integer, EndPoint As Integer) As Boolean
Dim f As Integer
Dim q As Integer, z As Integer
Dim i As Integer, j As Integer
Dim p As Integer, w As Integer
Dim TheChar As String
Dim AChoice As String
Dim TheChoice As String
Dim TheQuestion As String
Dim PrevQuestion As String
Dim TheChoiceQuestion As String
Dim SecondaryList(50) As String
Dim BothOn As Boolean, LeftOn As Boolean, RightOn As Boolean
WhichEye = False
If (StartingPoint < 0) Then
    Exit Function
End If
TheQuestion = ComplaintList(StartingPoint)
TheChar = "?"
q = StartingPoint + 1
While Not (WhichEye) And (TheChar <> "-")
    While (InStrPS(ComplaintList(q), TheChar) = 0) And (q < EndPoint)
        Erase SecondaryList
        f = InStrPS(ComplaintList(q), "=")
        If (f <> 0) Then
            i = InStrPS(f, ComplaintList(q), "<")
            j = InStrPS(i + 1, ComplaintList(q), ">")
            If (i = 0) Or (j = 0) Then
                Exit Function
            End If
            TheChoiceQuestion = ComplaintList(q)
            TheChoice = Mid(ComplaintList(q), i, j - (i - 1))
            BothOn = True
            LeftOn = True
            RightOn = True
            p = 0
            PrevQuestion = ""
            w = GetItem(TheChoice, q + 1, EndPoint)
            While (w <> 0)
                p = p + 1
                If (InStrPS("?[]", Left(ComplaintList(w - 1), 1)) = 0) Then
                    SecondaryList(p) = PrevQuestion
                Else
                    PrevQuestion = ComplaintList(w - 1)
                    SecondaryList(p) = ComplaintList(w - 1)
                End If
                p = p + 1
                SecondaryList(p) = ComplaintList(w)
                w = GetItem(TheChoice, w + 1, EndPoint)
            Wend

            For z = 1 To p
                If (InStrPS(SecondaryList(z), "*Left-") <> 0) Then
                    WhichEye = True
                    BothOn = False
                    LeftOn = True
                    RightOn = False
                ElseIf (InStrPS(SecondaryList(z), "*Right-") <> 0) Then
                    WhichEye = True
                    BothOn = False
                    LeftOn = False
                    RightOn = True
                ElseIf (InStrPS(SecondaryList(z), "*Both-") <> 0) Then
                    WhichEye = True
                    BothOn = True
                    LeftOn = True
                    RightOn = True
                End If
            Next z
    
            If (BothOn) Then
                BothComps = BothComps + 1
                BothComplaintList(BothComps) = TheQuestion
                BothComps = BothComps + 1
                BothComplaintList(BothComps) = TheChoiceQuestion
            Else
                If (LeftOn) Then
                    LeftComps = LeftComps + 1
                    LeftComplaintList(LeftComps) = TheQuestion
                    LeftComps = LeftComps + 1
                    LeftComplaintList(LeftComps) = TheChoiceQuestion
                End If
                If (RightOn) Then
                    RightComps = RightComps + 1
                    RightComplaintList(RightComps) = TheQuestion
                    RightComps = RightComps + 1
                    RightComplaintList(RightComps) = TheChoiceQuestion
                End If
            End If

            For z = 1 To p
                If (BothOn) Then
                    BothComps = BothComps + 1
                    BothComplaintList(BothComps) = SecondaryList(z)
                Else
                    If (LeftOn) Then
                        LeftComps = LeftComps + 1
                        LeftComplaintList(LeftComps) = SecondaryList(z)
                    End If
                    If (RightOn) Then
                        RightComps = RightComps + 1
                        RightComplaintList(RightComps) = SecondaryList(z)
                    End If
                End If
            Next z
        Else
            Call PinpointError("PI-ProcessForm", " does not have an =")
        End If
        q = q + 1
    Wend
    If Not (WhichEye) Then
        q = StartingPoint + 1
        If (TheChar = "$") Then
            TheChar = "["
        ElseIf (TheChar = "[") Then
            TheChar = "]"
        Else
            TheChar = "-"
        End If
    End If
Wend
End Function

Private Sub CreateComplaints()
Dim i As Integer
Dim k As Integer
Dim StartingPoint As Integer, EndPoint As Integer
BothComps = 0
LeftComps = 0
RightComps = 0
StartingPoint = -1
EndPoint = -1
If (MaxComplaints > 0) Then
    For k = 0 To MaxComplaints
        If (Trim(ComplaintList(k)) <> "") Then
            If (Left(ComplaintList(k), 1) = "/") Then
                If (StartingPoint <> -1) Then
                    EndPoint = k
                    Call WhichEye(StartingPoint, EndPoint)
                    StartingPoint = k
                    EndPoint = -1
                Else
                    StartingPoint = k
                    EndPoint = -1
                End If
            ElseIf (k = MaxComplaints) Then
                If (StartingPoint <> 0) And (k = MaxComplaints) Then
                    EndPoint = k
                    Call WhichEye(StartingPoint, EndPoint)
                End If
            End If
        Else
            If (StartingPoint <> 0) And (k = MaxComplaints) Then
                EndPoint = k
                Call WhichEye(StartingPoint, EndPoint)
            End If
        End If
    Next k
    If (StartingPoint = 0) And (k > MaxComplaints) Then
        EndPoint = MaxComplaints
        Call WhichEye(StartingPoint, EndPoint)
    ElseIf (StartingPoint > 0) And (EndPoint = -1) Then
        EndPoint = MaxComplaints
        Call WhichEye(StartingPoint, EndPoint)
    End If
End If
End Sub

Private Sub Label1_Click()
Call Form_KeyPress(vbKeyEscape)
End Sub

Private Sub PrintPIAppointment()
Dim Resultfeild As String
Dim Resultvalue As String
Dim resultstring As String
Dim i As Integer, FileNum As Integer
Dim k As Integer, u As Integer
Dim TheRecord As String
Dim MyFile As String
Dim NewFile As String
Dim TempFile As String
Dim PrintFileName As String
Dim TheDate As String
Dim YourDate As String
Dim PatientName As String
Dim DisplayString As String
PrintFileName = PatientInterfaceDirectory + Trim(Str(PatientId)) + PatientName + ".txt"
lstPrint.Clear
If (PrintReport) Then
    Call FormatTodaysDate(TheDate, False)
    lstPrint.AddItem "01:" + PatientName
    lstPrint.AddItem "02:" + TheDate
    k = GetAge(PatientBirthDate)
    lstPrint.AddItem "03:" + Trim(Str(k))
    YourDate = Mid(PatientBirthDate, 5, 2) + "/" + Mid(PatientBirthDate, 7, 2) + "/" + Left(PatientBirthDate, 4)
    lstPrint.AddItem "04:" + YourDate
    lstPrint.AddItem "05:" + Trim(AppointmentType)
    lstPrint.AddItem "06:" + Trim(SchedulerDoctor)
    lstPrint.AddItem "07:" + Trim(PatientOccupation)
    k = 0
    FileNum = FreeFile
   
    MyFile = Dir(PatientInterfaceDirectory + "P*" + "_" + Trim(Str(AppointmentId)) + "_" + Trim(Str(PatientId)) + ".txt")
    Do Until (MyFile = "")
        FileName = PatientInterfaceDirectory + MyFile
        FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
        Do Until (EOF(FileNum)) Or (LOF(FileNum) < 3)
            Line Input #FileNum, TheRecord
            If (Left(TheRecord, 8) = "DiRecap=") Then
                k = k + 1
                DisplayString = Mid(TheRecord, 9, Len(TheRecord) - 8)
                Call StripCharacters(DisplayString, "*")
                While (Len(DisplayString) > 70)
                    If (k < 27) Then
                        lstPrint.AddItem "08" + Chr(64 + k) + ":" + Trim(Left(DisplayString, 70))
                        k = k + 1
                    End If
                    DisplayString = Mid(DisplayString, 71, Len(DisplayString) - 70)
                Wend
                If (k < 27) Then
                    lstPrint.AddItem "08" + Chr(64 + k) + ":" + Trim(DisplayString)
                End If
            End If
        Loop
        FM.CloseFile CLng(FileNum)
        MyFile = Dir
    Loop
    If (FM.IsFileThere(PrintFileName)) Then
        FM.Kill PrintFileName
    End If
    TempFile = TemplateDirectory + "PIPrint.doc"
    FM.FileCopy TemplateDirectory + "PIPrint.txt", PrintFileName
    FM.OpenFile PrintFileName, FileOpenMode.Binary, FileAccess.ReadWrite, CLng(500)
    FM.CloseFile CLng(500)
    FM.OpenFile PrintFileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(500)
    For i = 0 To lstPrint.ListCount - 1
        u = InStrPS(lstPrint.List(i), ":")
        If (u > 0) Then
        If Resultvalue = "" Then
            
             Resultvalue = Mid(lstPrint.List(i), u + 1, Len(lstPrint.List(i)) - u) & vbTab
             
             Else
                   
                Resultvalue = Resultvalue & Mid(lstPrint.List(i), u + 1, Len(lstPrint.List(i)) - u) & vbTab
           
        End If
         Print #500, Mid(lstPrint.List(i), u + 1, Len(lstPrint.List(i)) - u); vbTab;
         End If
    Next i
    Print #500, ""
    FM.CloseFile CLng(500)
       Call createresultfield(Resultfeild)
         resultstring = Resultfeild & vbCrLf & Resultvalue
    NewFile = Left(PrintFileName, Len(PrintFileName) - 4) + ".xls"
     NewFile = resultstring & "@" & NewFile
'    If (FM.IsFileThere(NewFile)) Then
'        Kill NewFile
'    End If
    'Name PrintFileName As NewFile
    Call PrintTemplate(NewFile, TempFile, PrintReport, False, 0, "", "", 0, False)
End If
End Sub
Private Sub createresultfield(Resultfeild As String)

        Resultfeild = ":1:" & vbTab & ":2:" & vbTab & ":3:" & vbTab & ":4:" & vbTab & ":4A:" & vbTab & ":5:" & vbTab & ":6:" & vbTab & ":7:" & vbTab & ":8:" & vbTab & ":9A:" & vbTab & ":10A:" & vbTab & ":11A:" & vbTab & ":12A:" & vbTab & ":13A:" & vbTab & ":14A:" & vbTab & ":15A:" & vbTab & ":15B:" & vbTab & ":15C:" & vbTab & ":16A:" & vbTab & ":17A:" & vbTab & ":18:" & vbTab & ":19:" & vbTab & ":20A:" & vbTab & ":20B:" & vbTab & ":20C:" & vbTab & ":20D:" & vbTab & ":20E:" & vbTab & ":21:" & vbTab & ":22:" & vbTab & ":23:" & vbTab & ":24A:" & vbTab & ":24B:" & vbTab & ":24C:" & vbTab & ":25A:" & vbTab & ":26A:" & vbTab & ":27A:" & vbTab & ":27B:" & vbTab & ":27C:" & vbTab & ":27D:" & vbTab & ":27E:" & vbTab & ":27F:" & vbTab & ":27G:" & vbTab & ":27H:"
        Resultfeild = Resultfeild + vbTab & ":28:" & vbTab & ":29A:" & vbTab & ":29B:" & vbTab & ":29C:" & vbTab & ":29D:" & vbTab & ":29E:" & vbTab & ":29F:"
        Resultfeild = Resultfeild + vbTab & ":30:" & vbTab & ":31a:" & vbTab & ":31b:" & vbTab & ":31c:" & vbTab & ":31d:" & vbTab & ":31e:" & vbTab & ":31f:" & vbTab & ":31g:" & vbTab & ":31h:"
        Resultfeild = Resultfeild + vbTab & ":31i:" & vbTab & ":31j" & vbTab & ":31k:" & vbTab & ":31l:" & vbTab & ":31m:" & vbTab & ":31n:" & vbTab & ":31o:" & vbTab & ":32:" & vbTab & ":33:" & vbTab & ":34:" & vbTab & ":35:" & vbTab & ":36:" & vbTab & ":37:"
        
End Sub
Private Sub cmdLang1_Click()
If (cmdLang1.ToolTipText = "") Then
    cmdLang1.ToolTipText = "English"
End If
Call TriggerNewLanguage(cmdLang1.ToolTipText)
If Not (Starting) Then
    Call Startup
End If
End Sub

Private Sub cmdLang2_Click()
Call TriggerNewLanguage(cmdLang2.ToolTipText)
Call Startup
End Sub

Private Sub cmdLang3_Click()
Call TriggerNewLanguage(cmdLang3.ToolTipText)
Call Startup
End Sub

Private Sub cmdLang4_Click()
Call TriggerNewLanguage(cmdLang4.ToolTipText)
Call Startup
End Sub

Private Sub cmdLang5_Click()
Call TriggerNewLanguage(cmdLang5.ToolTipText)
Call Startup
End Sub

Private Function TriggerNewLanguage(Lang As String) As Boolean
Dim i As Integer
Dim CId As Long
Dim CText As String
Dim CLingo As String
Dim CTag As String
TriggerNewLanguage = True
If (Lang <> Language) Then
    For i = 0 To frmHome.Controls.Count - 1
        If (SetControlLanguage(frmHome.Controls(i).Name, -1, Lang, CId, CText, CLingo, CTag)) Then
            frmHome.Controls(i).Visible = True
            If (Trim(CText) <> "") Then
                If (Left(frmHome.Controls(i).Name, 3) = "lbl") Then
                    frmHome.Controls(i).Caption = CText
                ElseIf (Left(frmHome.Controls(i).Name, 3) = "cmd") Then
                    frmHome.Controls(i).Text = CText
                Else
                    frmHome.Controls(i).Text = CText
                End If
            End If
            frmHome.Controls(i).Tag = Trim(Str(CId))
            If (Lang = "English") Then
                frmHome.Controls(i).ToolTipText = Trim(CTag)
            End If
        End If
    Next i
    Language = Lang
    Call SetDynamicButtons(Lang)
End If
End Function

Public Function SetControlLanguage(CtrlName As String, FormId As Long, Lang As String, CtrlId As Long, CtrlText As String, CtrlLingo As String, CtrlTag As String) As Boolean
Dim RetLang As DynamicLanguage
Dim RetCtrls As DynamicControls
CtrlText = ""
CtrlLingo = ""
CtrlTag = ""
Set RetLang = New DynamicLanguage
Set RetCtrls = New DynamicControls
SetControlLanguage = False
RetCtrls.FormId = FormId
RetCtrls.ControlName = CtrlName
RetCtrls.IEChoiceName = ""
If (RetCtrls.FindControl > 0) Then
    If (RetCtrls.SelectControl(1)) Then
        RetLang.ControlId = RetCtrls.ControlId
        RetLang.Language = Lang
        If (RetLang.FindLanguageControl > 0) Then
            If (RetLang.SelectLanguageControl(1)) Then
                CtrlId = RetCtrls.ControlId
                CtrlText = RetLang.ControlText
                CtrlLingo = RetLang.ConfirmLingo
                CtrlTag = RetCtrls.ControlAlternateTrigger
                SetControlLanguage = True
            End If
        Else
            CtrlId = RetCtrls.ControlId
            CtrlText = RetCtrls.ControlText
            CtrlLingo = RetCtrls.ConfirmLingo
            CtrlTag = RetCtrls.ControlAlternateTrigger
            SetControlLanguage = True
        End If
    End If
End If
Set RetCtrls = Nothing
Set RetLang = Nothing
End Function

Private Function SetDynamicButtons(Lang As String) As Boolean
Dim i As Integer
Dim CId As Long
Dim CText As String
Dim CLingo As String
Dim RetLang As DynamicLanguage
Dim RetCtrls As DynamicControls
SetDynamicButtons = True
Set RetLang = New DynamicLanguage
Set RetCtrls = New DynamicControls
RetCtrls.FormId = -7
RetCtrls.ControlName = ""
RetCtrls.IEChoiceName = ""
If (RetCtrls.FindControl > 0) Then
    i = 1
    While (RetCtrls.SelectControl(i))
        RetLang.ControlId = RetCtrls.ControlId
        RetLang.Language = Lang
        If (RetLang.FindLanguageControl > 0) Then
            If (RetLang.SelectLanguageControl(1)) Then
                If (RetCtrls.ControlName = "cmdApply") Then
                    DoneButtonText = Trim(RetLang.ControlText)
                ElseIf (RetCtrls.ControlName = "cmdHelp") Then
                    HelpButtonText = Trim(RetLang.ControlText)
                ElseIf (RetCtrls.ControlName = "cmdBack") Then
                    ChangeButtonText = Trim(RetLang.ControlText)
                ElseIf (RetCtrls.ControlName = "cmdDontKnow") Then
                    DontKnowButtonText = Trim(RetLang.ControlText)
                ElseIf (RetCtrls.ControlName = "cmdNo") Then
                    NoToAllButtonText = Trim(RetLang.ControlText)
                End If
            End If
        Else
            If (RetCtrls.ControlName = "cmdApply") Then
                DoneButtonText = Trim(RetCtrls.ControlText)
            ElseIf (RetCtrls.ControlName = "cmdHelp") Then
                HelpButtonText = Trim(RetCtrls.ControlText)
            ElseIf (RetCtrls.ControlName = "cmdBack") Then
                ChangeButtonText = Trim(RetCtrls.ControlText)
            ElseIf (RetCtrls.ControlName = "cmdDontKnow") Then
                DontKnowButtonText = Trim(RetCtrls.ControlText)
            ElseIf (RetCtrls.ControlName = "cmdNo") Then
                NoToAllButtonText = Trim(RetCtrls.ControlText)
            End If
        End If
        i = i + 1
    Wend
End If
Set RetCtrls = Nothing
Set RetLang = Nothing
End Function

Private Function SetInterfaceControlsVisible(IHow As Boolean) As Boolean
Dim i As Integer
SetInterfaceControlsVisible = True
For i = 0 To frmHome.Controls.Count - 1
    If (frmHome.Controls(i).Tag <> "") Then
        frmHome.Controls(i).Visible = IHow
    End If
Next i
End Function

Private Function IsResponseTrue(AFile As String) As Boolean
Dim Rec As String
IsResponseTrue = False
FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
Do Until EOF(101)
    Line Input #101, Rec
    If (InStrPS(UCase(Rec), "YES") > 0) Then
        IsResponseTrue = True
        Exit Do
    End If
Loop
FM.CloseFile CLng(101)
End Function

Public Function GetConfirmOn() As Boolean
GetConfirmOn = DoConfirmOn
End Function

Private Sub Timer1_Timer()
'Timer1.Enabled = False
'If (MyKey = 0) Then
'    TimerBaseCount = TimerBaseCount + 1
'    If (TimerBaseCount >= TimerBase) Then
'        On Error GoTo UrFKed
'        ExitFast = True
'        CloseDown = True
'        frmClinicalEntry4.CloseDown = True
'        frmClinicalEntry4.FormOn = False
'        Unload frmClinicalEntry4
'        DoEvents
'        frmClinicalEntry3.ExitFast = True
'        frmClinicalEntry3.CloseDown = True
'        frmClinicalEntry3.FormOn = False
'        Unload frmClinicalEntry3
'        DoEvents
'        frmClinicalEntry2.ExitFast = True
'        frmClinicalEntry2.CloseDown = True
'        frmClinicalEntry2.FormOn = False
'        Unload frmClinicalEntry2
'        DoEvents
'        frmClinicalEntry.CloseDown = True
'        frmClinicalEntry.FormOn = False
'        Unload frmClinicalEntry
'        TimeOutExit = True
'        DoEvents
'    Else
'        Timer1.Enabled = True
'        Timer1.Enabled = False
'        MyKey = 0
'    End If
'Else
''    Timer1.Enabled = True
'    Timer1.Enabled = False
'    MyKey = 0
'End If
'Exit Sub
'UrFKed:
'    ExitFast = True
'    Resume Next
End Sub
Public Sub CloseAllForms()
Dim i As Integer
Dim iLoop As Integer
Dim iHighestForm As Integer
If Not Screen.ActiveForm Is Nothing Then
    iHighestForm = Screen.ActiveForm.Count
    iLoop = iHighestForm
    On Error Resume Next
    Do Until iLoop = 0
    Call Forms(iLoop).FrmClose
         iLoop = iLoop - 1
     Loop
End If
  End Sub
  Private Sub Timer2_Timer()
Dim lii As LASTINPUTINFO
lii.cbSize = Len(lii)
Dim strLogoffTime As String
strLogoffTime = CheckConfigCollection("AUTOLOGOFF")
If strLogoffTime <> "" Then
'strLogoffTime = 0.15
Call GetLastInputInfo(lii)
If (FormatNumber((GetTickCount() - lii.dwTime) / 1000, 2) / 60) = strLogoffTime Then
Call CloseAllForms
Unload Me
'ExitProcess GetExitCodeProcess(GetCurrentProcess, 0)
 End If
End If

End Sub
