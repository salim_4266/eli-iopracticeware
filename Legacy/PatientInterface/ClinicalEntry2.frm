VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmClinicalEntry2 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Lucida Sans"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   1335
      Left            =   10080
      TabIndex        =   5
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry2.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDontKnow 
      Height          =   1335
      Left            =   5205
      TabIndex        =   6
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry2.frx":0233
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   1335
      Left            =   360
      TabIndex        =   7
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry2.frx":046A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNo 
      Height          =   1335
      Left            =   7635
      TabIndex        =   8
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry2.frx":06B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHelp 
      Height          =   1335
      Left            =   2760
      TabIndex        =   9
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry2.frx":08E8
   End
   Begin VB.Label lblSwitch 
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   11640
      TabIndex        =   10
      Top             =   600
      Visible         =   0   'False
      Width           =   180
   End
   Begin VB.Label lblMedicalHistory 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Medical History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   6840
      TabIndex        =   3
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label lblFamilyHistory 
      Alignment       =   2  'Center
      BackColor       =   &H002E46BC&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Family Medical History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   9000
      TabIndex        =   4
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label lblEyeHistory 
      Alignment       =   2  'Center
      BackColor       =   &H009D6DA7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Eye History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   4920
      TabIndex        =   2
      Top             =   120
      Width           =   1935
   End
   Begin VB.Label lblLenses 
      Alignment       =   2  'Center
      BackColor       =   &H007A8435&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Glasses/Contacts"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   2520
      TabIndex        =   1
      Top             =   120
      Width           =   2415
   End
   Begin VB.Label lblCurrentSymptom 
      Alignment       =   2  'Center
      BackColor       =   &H00CFA076&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current Symptoms"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "frmClinicalEntry2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public NotSure As Boolean
Public ExitFast As Boolean
Public ButtonSelectionColor As Long
Public TextSelectionColor As Long
Public CloseDown As Boolean
Public ChangeOn As Boolean
Public ChangeRequest As Boolean
Public LevelType As Integer
Public StoreResults As String
Public FormOn As Boolean
Public FormColor As Long
Public FirstFormId As Long
Public FirstOrder As String
Public FirstControlName As String
Public FirstControlRef As String
Public Question As String
Public QuestionFamily As Long
Public Language As String

Private Const EventCounter = 100
Private Const SelectionColor As Long = &HC0C000

'Hidden
Private CurrentFormId As Long
Private CurrentControlName As String
Private TheButtons(15) As Boolean
Private TheButtonName(15) As String
Private TheButtonNest(15) As String
Private TheButtonTrigger(15) As String * 64
Private TheButtonTriggerText(15) As String * 64
Private TheButtonVisible(15) As Boolean
Private TheButtonTextColor(15) As Long
Private TheButtonColor(15) As Long
Private TheButtonId(15) As Long

Private MaxControls As Integer
Private MaxButtons As Integer
Private MaxLabel As Integer

Private TheForm As DynamicSecondaryForms
Private TheControl As DynamicSecondaryControls
Private TheThirdForm As DynamicThirdForms
Private TheThirdControl As DynamicThirdControls

' you can have any number of Labels
' code is not required for Label processing
Private WithEvents lblLabel As Label
Attribute lblLabel.VB_VarHelpID = -1
' you can have 30 CommandButtons per form
' unique code is required for each Button
Private WithEvents cmdButton1 As fpBtn
Attribute cmdButton1.VB_VarHelpID = -1
Private WithEvents cmdButton2 As fpBtn
Attribute cmdButton2.VB_VarHelpID = -1
Private WithEvents cmdButton3 As fpBtn
Attribute cmdButton3.VB_VarHelpID = -1
Private WithEvents cmdButton4 As fpBtn
Attribute cmdButton4.VB_VarHelpID = -1
Private WithEvents cmdButton5 As fpBtn
Attribute cmdButton5.VB_VarHelpID = -1
Private WithEvents cmdButton6 As fpBtn
Attribute cmdButton6.VB_VarHelpID = -1
Private WithEvents cmdButton7 As fpBtn
Attribute cmdButton7.VB_VarHelpID = -1
Private WithEvents cmdButton8 As fpBtn
Attribute cmdButton8.VB_VarHelpID = -1
Private WithEvents cmdButton9 As fpBtn
Attribute cmdButton9.VB_VarHelpID = -1
Private WithEvents cmdButton10 As fpBtn
Attribute cmdButton10.VB_VarHelpID = -1
Private WithEvents cmdButton11 As fpBtn
Attribute cmdButton11.VB_VarHelpID = -1
Private WithEvents cmdButton12 As fpBtn
Attribute cmdButton12.VB_VarHelpID = -1
Private WithEvents cmdButton13 As fpBtn
Attribute cmdButton13.VB_VarHelpID = -1
Private WithEvents cmdButton14 As fpBtn
Attribute cmdButton14.VB_VarHelpID = -1
Private WithEvents cmdButton15 As fpBtn
Attribute cmdButton15.VB_VarHelpID = -1

Private Sub cmdApply_Click()
On Error GoTo UI_ErrorHandler
Dim w As Integer, p As Integer
Dim TestStore As String
Dim ThePatient As String
Dim FileNum As Integer
Dim ButtonName As String, TextName As String
Dim ImageName As String, SliderName As String
Dim PostedResults As Boolean
If (Trim(StoreResults) = "") Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    frmClinicalEntry.Enabled = True
    frmClinicalEntry.ZOrder 0
    Unload frmClinicalEntry2
    FormOn = False
    Exit Sub
End If
p = InStrPS(StoreResults, "_")
If (p = 0) Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    frmClinicalEntry.Enabled = True
    frmClinicalEntry.ZOrder 0
    Unload frmClinicalEntry2
    FormOn = False
    Exit Sub
Else
    w = InStrPS(p, StoreResults, ".")
    If (w = 0) Then
        Set TheForm = Nothing
        Set TheControl = Nothing
        frmClinicalEntry.Enabled = True
        frmClinicalEntry.ZOrder 0
        Unload frmClinicalEntry2
        FormOn = False
        Exit Sub
    End If
    ThePatient = Mid(StoreResults, p + 1, w - (p + 1))
End If
PostedResults = False
FileNum = FreeFile
FM.OpenFile StoreResults, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
Print #FileNum, "QUESTION=" + "?" + Question
If (NotSure) Then
    PostedResults = True
    Print #FileNum, "NOTSURE=T" + Trim(Str(CurrentFormId)) + "<0>"
    FM.CloseFile CLng(FileNum)
Else
    For p = 1 To MaxButtons
        If (TheButtons(p)) Then
            ButtonName = TheButtonName(p) + "=T" + Trim(Str(TheButtonId(p))) + " <" + FirstControlRef + ">"
            PostedResults = True
            Print #FileNum, ButtonName
        End If
    Next p
    FM.CloseFile CLng(FileNum)
    ButtonName = ""
    If Not (frmClinicalEntry.ChangeRequest) Then
        For p = 1 To MaxButtons
            If (TheButtons(p)) Then
                Call ButtonCheckLevel(p)
            End If
        Next p
    End If
End If
Set TheForm = Nothing
Set TheControl = Nothing
frmClinicalEntry.Enabled = True
frmClinicalEntry.ZOrder 0
Unload frmClinicalEntry2
FormOn = False
Exit Sub
UI_ErrorHandler:
    Set TheForm = Nothing
    Set TheControl = Nothing
    Resume LeaveFast
LeaveFast:
    frmClinicalEntry.Enabled = True
    frmClinicalEntry.ZOrder 0
    Unload frmClinicalEntry2
    FormOn = False
End Sub

Private Sub cmdBack_Click()
If (CloseDown) Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    frmClinicalEntry.Enabled = True
    frmClinicalEntry.ZOrder 0
    Unload frmClinicalEntry2
    FormOn = False
Else
    frmClinicalEntry.ChangeRequest = True
    Call cmdApply_Click
End If
End Sub

Private Sub cmdDontKnow_Click()
NotSure = Not (NotSure)
If (NotSure) Then
    cmdDontKnow.BackColor = ButtonSelectionColor
    cmdDontKnow.ForeColor = TextSelectionColor
    frmHelp.Language = Language
    frmHelp.HelpOption = False
    frmHelp.Show 1
    If (frmHelp.ExitFast) Then
        frmClinicalEntry.CloseDown = True
        CloseDown = True
        Call cmdBack_Click
    Else
        Call cmdApply_Click
    End If
Else
    cmdDontKnow.BackColor = TheButtonColor(1)
    cmdDontKnow.ForeColor = TheButtonTextColor(1)
End If
End Sub

Private Sub cmdHelp_Click()
frmHelp.Language = Language
frmHelp.HelpOption = True
frmHelp.Show 1
If (frmHelp.ExitFast) Then
    frmClinicalEntry.CloseDown = True
    CloseDown = True
    Call cmdBack_Click
End If
End Sub

Private Sub cmdNo_Click()
Set TheForm = Nothing
Set TheControl = Nothing
frmClinicalEntry.Enabled = True
frmClinicalEntry.ZOrder 0
Unload frmClinicalEntry2
FormOn = False
End Sub

Private Sub TriggerThirdForm(TheFormId As Long, TheControlName As String, TheQuestion As String, OrderRef As String)
Dim Tick As Integer
If (Trim(TheControlName) <> "") And (TheFormId > 0) Then
    frmClinicalEntry3.Language = Language
    frmClinicalEntry3.ChangeOn = ChangeOn
    frmClinicalEntry3.StoreResults = StoreResults
    frmClinicalEntry3.LevelType = LevelType
    frmClinicalEntry3.Question = TheQuestion
    frmClinicalEntry3.QuestionFamily = QuestionFamily
    frmClinicalEntry3.FirstFormId = TheFormId
    frmClinicalEntry3.FirstControlName = TheControlName
    frmClinicalEntry3.FirstControlRef = FirstControlRef
    frmClinicalEntry3.FirstOrder = OrderRef
    frmClinicalEntry3.ButtonSelectionColor = ButtonSelectionColor
    frmClinicalEntry3.TextSelectionColor = TextSelectionColor
    frmClinicalEntry3.FormOn = True
    frmClinicalEntry2.Enabled = False
    frmClinicalEntry2.Visible = False
    frmClinicalEntry3.Show
    Tick = 0
    While (frmClinicalEntry3.FormOn) And Not (frmClinicalEntry3.CloseDown)
        Tick = Tick + 1
        If (Tick = EventCounter) Then
            DoEvents
            Tick = 0
        End If
    Wend
    frmClinicalEntry2.Enabled = True
    frmClinicalEntry2.Visible = True
    If (CloseDown) Then
        Call cmdBack_Click
    ElseIf (ChangeRequest) Then
        Call cmdBack_Click
    ElseIf (frmClinicalEntry3.NotSure) Or (frmClinicalEntry3.ExitFast) Then
        ExitFast = True
        Call cmdApply_Click
    End If
End If
End Sub

Private Function FindAlternateButton(AName As String) As Boolean
Dim k As Integer
FindAlternateButton = False
For k = 1 To MaxButtons
    If (AName = TheButtonName(k)) Then
        Call ApplyAlternateButton(k)
        FindAlternateButton = True
    End If
Next k
End Function

Private Sub ApplyAlternateButton(Counter As Integer)
    If (Counter = 1) Then
        With cmdButton1
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 2) Then
        With cmdButton2
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 3) Then
        With cmdButton3
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 4) Then
        With cmdButton4
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 5) Then
        With cmdButton5
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 6) Then
        With cmdButton6
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 7) Then
        With cmdButton7
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 8) Then
        With cmdButton8
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 9) Then
        With cmdButton9
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 10) Then
        With cmdButton10
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 11) Then
        With cmdButton11
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 12) Then
        With cmdButton12
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 13) Then
        With cmdButton13
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 14) Then
        With cmdButton14
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 15) Then
        With cmdButton15
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    Else
        Exit Sub
    End If
End Sub

Private Sub ButtonCheckLevel(Ref As Integer)
Dim k As Integer
Dim AButton As fpBtn
Dim NestedQuestion As String
If (TheButtons(Ref)) Then
    Set TheThirdForm = New DynamicThirdForms
    TheThirdForm.FormId = CurrentFormId
    TheThirdForm.ControlName = TheButtonNest(Ref)
    TheThirdForm.QuestionOrder = ""
    If (TheThirdForm.RetrieveForm) Then
        k = 1
        While (TheThirdForm.SelectForm(k)) And Not (ExitFast)
            NestedQuestion = TheThirdForm.Question
            If (Trim(TheThirdForm.IEQuestion) <> "") Then
                NestedQuestion = TheThirdForm.IEQuestion
            End If
            Call TriggerThirdForm(CurrentFormId, TheButtonNest(Ref), NestedQuestion, TheThirdForm.QuestionOrder)
            k = k + 1
        Wend
' This implies that the selection was De-Referenced
        If (k < 1) Then
            If (LevelType = 2) Then
                AButton.BackColor = TheButtonColor(Ref)
            End If
            TheButtons(Ref) = False
        End If
    End If
    Set TheThirdForm = Nothing
End If
End Sub

Private Sub ButtonAction(AButton As fpBtn, Ref As Integer)
Dim k As Integer
If (TheButtonColor(Ref) = AButton.BackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = TheButtonColor(Ref)
    AButton.ForeColor = TheButtonTextColor(Ref)
End If
TheButtons(Ref) = Not (TheButtons(Ref))
cmdNo.Enabled = True
For k = 1 To MaxButtons
    If (TheButtons(k)) Then
        cmdNo.Enabled = False
        k = MaxButtons
    End If
Next k
' Set the Process for Trigger Conditions
' The way the trigger works is a button will trigger
' other buttons that match this button's name.
If (TheButtons(Ref)) Then
    If (Trim(TheButtonTrigger(Ref)) <> "") Then
        If (FindAlternateButton(TheButtonName(Ref))) Then
            AButton.Visible = TheButtonVisible(Ref)
        End If
    End If
End If
' Check to see if single selection is on
If (TheButtons(Ref)) Then
    If Not (cmdApply.Visible) Then
        Call cmdApply_Click
    End If
End If
End Sub

Private Sub cmdButton1_Click()
Call ButtonAction(cmdButton1, 1)
End Sub

Private Sub cmdButton2_Click()
Call ButtonAction(cmdButton2, 2)
End Sub

Private Sub cmdButton3_Click()
Call ButtonAction(cmdButton3, 3)
End Sub

Private Sub cmdButton4_Click()
Call ButtonAction(cmdButton4, 4)
End Sub

Private Sub cmdButton5_Click()
Call ButtonAction(cmdButton5, 5)
End Sub

Private Sub cmdButton6_Click()
Call ButtonAction(cmdButton6, 6)
End Sub

Private Sub cmdButton7_Click()
Call ButtonAction(cmdButton7, 7)
End Sub

Private Sub cmdButton8_Click()
Call ButtonAction(cmdButton8, 8)
End Sub

Private Sub cmdButton9_Click()
Call ButtonAction(cmdButton9, 9)
End Sub

Private Sub cmdButton10_Click()
Call ButtonAction(cmdButton10, 10)
End Sub

Private Sub cmdButton11_Click()
Call ButtonAction(cmdButton11, 11)
End Sub

Private Sub cmdButton12_Click()
Call ButtonAction(cmdButton12, 12)
End Sub

Private Sub cmdButton13_Click()
Call ButtonAction(cmdButton13, 13)
End Sub

Private Sub cmdButton14_Click()
Call ButtonAction(cmdButton14, 14)
End Sub

Private Sub cmdButton15_Click()
Call ButtonAction(cmdButton15, 15)
End Sub

Private Sub SetButtonCharacteristics(RefControl As DynamicSecondaryControls, AButton As fpBtn)
Dim q As Integer
Dim VisualText As String
With AButton
    If (Trim(RefControl.ControlTemplateName) <> "") Then
        .TemplateName = BtnDir + Trim(RefControl.ControlTemplateName) + ".btn"
        .Action = fpActionLoadTemplate
        .CellIndex = 2
        .SegmentApplyTo = fpSegmentApplyToButton
        .BackStyle = fpBackStyleTransparentGrayArea
        RefControl.BackColor = .BackColor
    Else
        .BackColor = RefControl.BackColor
    End If
    If (Trim(RefControl.ControlText) <> "") Then
        q = InStrPS(RefControl.ControlText, "^")
        If (q <> 0) Then
            VisualText = Left(RefControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(RefControl.ControlText, q + 1, Len(RefControl.ControlText) - q)
        Else
            VisualText = RefControl.ControlText
        End If
        .Text = VisualText
    Else
        .Text = RefControl.ControlName
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        .Tag = RefControl.IEChoiceName
    Else
        .Tag = RefControl.ControlName
    End If
    .ForeColor = RefControl.ForeColor
    .Left = RefControl.ControlLeft
    .Top = RefControl.ControlTop
    .Height = RefControl.ControlHeight
    .Width = RefControl.ControlWidth
    .TabIndex = RefControl.TabIndex
    .Visible = RefControl.ControlVisible
    .Enabled = True
    .Font.Size = RefControl.ResponseLength
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
    End If
    .FontBold = False
    .ToolTipText = ""
End With
End Sub

Private Sub PlantButton(RefControl As DynamicSecondaryControls, Counter As Integer)
    Dim LibType As String
    LibType = "fpBtn.fpBtn.2"
    Counter = Counter + 1
    If (Counter = 1) Then
        Set cmdButton1 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton1)
    ElseIf (Counter = 2) Then
        Set cmdButton2 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton2)
    ElseIf (Counter = 3) Then
        Set cmdButton3 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton3)
    ElseIf (Counter = 4) Then
        Set cmdButton4 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton4)
    ElseIf (Counter = 5) Then
        Set cmdButton5 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton5)
    ElseIf (Counter = 6) Then
        Set cmdButton6 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton6)
    ElseIf (Counter = 7) Then
        Set cmdButton7 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton7)
    ElseIf (Counter = 8) Then
        Set cmdButton8 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton8)
    ElseIf (Counter = 9) Then
        Set cmdButton9 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton9)
    ElseIf (Counter = 10) Then
        Set cmdButton10 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton10)
    ElseIf (Counter = 11) Then
        Set cmdButton11 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton11)
    ElseIf (Counter = 12) Then
        Set cmdButton12 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton12)
    ElseIf (Counter = 13) Then
        Set cmdButton13 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton13)
    ElseIf (Counter = 14) Then
        Set cmdButton14 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton14)
    ElseIf (Counter = 15) Then
        Set cmdButton15 = frmClinicalEntry2.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton15)
    Else
        Exit Sub
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        TheButtonName(Counter) = Trim(RefControl.IEChoiceName)
    Else
        TheButtonName(Counter) = "*" + Trim(RefControl.ControlName)
    End If
    TheButtonNest(Counter) = Trim(RefControl.ControlName)
    TheButtonTrigger(Counter) = UCase(Trim(RefControl.ControlAlternateTrigger))
    TheButtonTriggerText(Counter) = UCase(Trim(RefControl.ControlAlternateText))
    TheButtonVisible(Counter) = RefControl.ControlVisibleAfterTrigger
    TheButtonColor(Counter) = RefControl.BackColor
    TheButtonTextColor(Counter) = RefControl.ForeColor
    TheButtons(Counter) = False
End Sub

Private Sub PlantLabel(RefControl As DynamicSecondaryControls, Counter As Integer)
Dim VisualText As String
Dim q As Integer
    Counter = Counter + 1
    Set lblLabel = frmClinicalEntry2.Controls.Add("VB.Label", "Label" + Trim(Str(Counter)))
    With lblLabel
        .Top = RefControl.ControlTop
        If (.Top < 1) Then
            .Top = 1
        End If
        .Left = RefControl.ControlLeft
        If (.Left < 1) Then
            .Left = 1
        End If
        If (RefControl.ControlWidth > 0) Then
            .AutoSize = False
            .Width = RefControl.ControlWidth
            If (.Left + .Width > frmClinicalEntry2.Width) Then
                .Width = (frmClinicalEntry2.Width - (.Left + 2))
            End If
        Else
            .AutoSize = True
        End If
        .Alignment = 2
        .WordWrap = True
        .BackColor = RefControl.BackColor
        .ForeColor = RefControl.ForeColor
        If (Trim(RefControl.ControlText) = "") Then
            .Caption = RefControl.ControlName
        Else
            q = InStrPS(RefControl.ControlText, "^")
            If (q = 0) Then
                VisualText = RefControl.ControlText
            Else
                VisualText = RefControl.ControlText
                While (q <> 0)
                    .AutoSize = True
                    VisualText = Left(VisualText, q - 1) + Chr(13) + Chr(10) + Mid(VisualText, q + 1, Len(VisualText) - q)
                    q = InStrPS(VisualText, "^")
                Wend
            End If
            .Caption = VisualText
        End If
        .TabIndex = RefControl.TabIndex
        .Visible = True
        .Enabled = True
        If (RefControl.ResponseLength > 100) Then
            .Height = RefControl.ResponseLength
        Else
            .Height = 24 * RefControl.ControlHeight
        End If
        .Font.Size = RefControl.ControlHeight
        If (Trim(RefControl.ControlFont) = "") Then
            .Font.Name = "Lucida Sans"
        Else
            .Font.Name = Trim(RefControl.ControlFont)
        End If
        .Font.Bold = True
    End With
End Sub

Private Function DynamicEntry() As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer
Dim MaxStatic As Integer
DynamicEntry = True
ExitFast = False
Set TheForm = New DynamicSecondaryForms
Set TheControl = New DynamicSecondaryControls
TheForm.FormId = FirstFormId
TheForm.ControlName = FirstControlName
TheForm.QuestionOrder = FirstOrder
If (TheForm.RetrieveForm) And (TheForm.FormId > 0) Then
    CurrentFormId = TheForm.FormId
    FormColor = TheForm.BackColor
    If (QuestionFamily = 1) Or (QuestionFamily = 7) Then
        lblCurrentSymptom.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 2) Then
        lblLenses.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 3) Then
        lblEyeHistory.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 4) Or (QuestionFamily = 8) Or (QuestionFamily = 9) Then
        lblMedicalHistory.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 5) Then
        lblFamilyHistory.ForeColor = &HFFFFFF
    Else
        lblCurrentSymptom.Visible = False
        lblLenses.Visible = False
        lblEyeHistory.Visible = False
        lblMedicalHistory.Visible = False
        lblFamilyHistory.Visible = False
    End If
    Question = TheForm.Question
    If (Trim(TheForm.IEQuestion) <> "") Then
        Question = Trim(TheForm.IEQuestion)
    End If
    TheForm.WindowState = 2
    frmClinicalEntry2.BackColor = TheForm.BackColor
    frmClinicalEntry2.ForeColor = TheForm.ForeColor
    frmClinicalEntry2.Caption = "Secondary Form"
    frmClinicalEntry2.WindowState = TheForm.WindowState
    If (TheForm.WindowState <> 2) Then
        frmClinicalEntry2.Height = TheForm.WindowHeight
        frmClinicalEntry2.Width = TheForm.WindowWidth
    End If
    frmClinicalEntry2.BorderStyle = 0
    frmClinicalEntry2.Enabled = True
    If (TheForm.DontKnowButton) Then
        frmClinicalEntry2.cmdDontKnow.GrayAreaColor = frmClinicalEntry2.BackColor
        frmClinicalEntry2.cmdDontKnow.Enabled = True
        frmClinicalEntry2.cmdDontKnow.Visible = True
        If (TheForm.WindowState <> 2) Then
            frmClinicalEntry2.cmdDontKnow.Left = 2250
            frmClinicalEntry2.cmdDontKnow.Top = TheForm.WindowHeight - 1300
        End If
    Else
        frmClinicalEntry2.cmdDontKnow.Enabled = False
        frmClinicalEntry2.cmdDontKnow.Visible = False
    End If
    If (TheForm.ApplyButton) Then
        frmClinicalEntry2.cmdApply.GrayAreaColor = frmClinicalEntry2.BackColor
        frmClinicalEntry2.cmdApply.Enabled = True
        frmClinicalEntry2.cmdApply.Visible = True
        If (TheForm.WindowState <> 2) Then
            frmClinicalEntry2.cmdApply.Left = 4070
            frmClinicalEntry2.cmdApply.Top = TheForm.WindowHeight - 1300
        End If
    Else
        frmClinicalEntry2.cmdApply.Enabled = False
        frmClinicalEntry2.cmdApply.Visible = False
        frmClinicalEntry2.cmdNo.Enabled = False
        frmClinicalEntry2.cmdNo.Visible = False
        frmClinicalEntry2.cmdHelp.Enabled = False
        frmClinicalEntry2.cmdHelp.Visible = False
    End If
    If (TheForm.ExitButton) And Not (ChangeOn) Then
        frmClinicalEntry2.cmdBack.GrayAreaColor = frmClinicalEntry2.BackColor
        frmClinicalEntry2.cmdBack.Enabled = True
        frmClinicalEntry2.cmdBack.Visible = True
        If (TheForm.WindowState <> 2) Then
            frmClinicalEntry2.cmdBack.Left = 50
            frmClinicalEntry2.cmdBack.Top = TheForm.WindowHeight - 1300
        End If
    Else
        frmClinicalEntry2.cmdBack.Enabled = False
        frmClinicalEntry2.cmdBack.Visible = False
    End If
    If (TheForm.NoToAllButton) Then
        frmClinicalEntry2.cmdNo.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry2.cmdNo.Enabled = True
        frmClinicalEntry2.cmdNo.Visible = True
    Else
        frmClinicalEntry2.cmdNo.Enabled = False
        frmClinicalEntry2.cmdNo.Visible = False
    End If
    If (TheForm.HelpButton) Then
        frmClinicalEntry2.cmdHelp.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry2.cmdHelp.Enabled = True
        frmClinicalEntry2.cmdHelp.Visible = True
    Else
        frmClinicalEntry2.cmdHelp.Enabled = False
        frmClinicalEntry2.cmdHelp.Visible = False
    End If
' Build the form's controls,
' They are different based on the Question that drives the Form
    TheControl.Language = Language
    TheControl.SecondaryFormId = TheForm.SecondaryFormId
    MaxControls = TheControl.FindControl
    If (MaxControls > 0) Then
        MaxLabel = 0
        MaxButtons = 0
        For i = 1 To MaxControls
            If (TheControl.SelectControl(i)) Then
                If (TheControl.ControlType = "L") Then
                    Call PlantLabel(TheControl, MaxLabel)
                ElseIf (TheControl.ControlType = "B") Then
                    Call PlantButton(TheControl, MaxButtons)
                    TheButtonId(MaxButtons) = TheControl.SecondaryControlId
                End If
            End If
        Next i
    End If
' Check for Static Label
    If (Trim(TheForm.StaticLabel) <> "") Then
        TheControl.SecondaryFormId = 99999
        TheControl.ControlName = Trim(TheForm.StaticLabel)
        MaxStatic = TheControl.FindControl
        If (MaxStatic > 0) Then
            If (TheControl.SelectControl(1)) Then
                If (TheControl.ControlType = "L") Then
                    Call PlantLabel(TheControl, MaxLabel)
                End If
            End If
        End If
    End If
Else
    DynamicEntry = False
End If
If (DynamicEntry) Then
    frmClinicalEntry2.Visible = True
End If
cmdBack.Visible = frmHome.GetConfirmOn
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
    DynamicEntry = False
    Set TheForm = Nothing
    Set TheControl = Nothing
End Function

Private Sub Form_Load()
On Error GoTo UI_ErrorHandler
Dim FileNum As Integer
frmClinicalEntry2.Visible = False
frmClinicalEntry2.ZOrder 0
frmClinicalEntry2.KeyPreview = True
If (Trim(DoneButtonText) <> "") Then
    cmdApply.Text = DoneButtonText
End If
If (Trim(ChangeButtonText) <> "") Then
    cmdBack.Text = ChangeButtonText
End If
If (Trim(NoToAllButtonText) <> "") Then
    cmdNo.Text = NoToAllButtonText
End If
If (Trim(HelpButtonText) <> "") Then
    cmdHelp.Text = HelpButtonText
End If
If (Trim(DontKnowButtonText) <> "") Then
    cmdDontKnow.Text = DontKnowButtonText
End If
NotSure = False
CloseDown = False
ChangeRequest = False
FileNum = FreeFile
frmClinicalEntry2.MousePointer = 2
If Not (DynamicEntry()) Then
    frmClinicalEntry.Enabled = True
    Unload frmClinicalEntry2
    FormOn = False
End If
Exit Sub
UI_ErrorHandler:
    If (Err = 53) Then
        Resume Next
    End If
    Resume LeaveFast
LeaveFast:
    frmClinicalEntry.Enabled = True
    Unload frmClinicalEntry2
    FormOn = False
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    frmClinicalEntry.CloseDown = True
    CloseDown = True
    Call cmdBack_Click
Else
    frmHome.MyKey = frmHome.MyKey + 1
End If
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
frmHome.MyKey = frmHome.MyKey + 1
End Sub

Private Sub lblSwitch_Click()
Call Form_KeyPress(Val(vbKeyEscape))
End Sub
Public Sub FrmClose()
Unload Me
End Sub

