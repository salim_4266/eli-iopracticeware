VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPatientDayYear 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11520
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   ScaleHeight     =   11520
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdOne 
      Height          =   990
      Left            =   900
      TabIndex        =   0
      Top             =   3120
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTwo 
      Height          =   990
      Left            =   2580
      TabIndex        =   1
      Top             =   3120
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":01DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFour 
      Height          =   990
      Left            =   900
      TabIndex        =   2
      Top             =   4200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":03B8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFive 
      Height          =   990
      Left            =   2580
      TabIndex        =   3
      Top             =   4200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":0594
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSeven 
      Height          =   990
      Left            =   900
      TabIndex        =   4
      Top             =   5280
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":0770
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEight 
      Height          =   990
      Left            =   2580
      TabIndex        =   5
      Top             =   5280
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":094C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZero 
      Height          =   990
      Left            =   2580
      TabIndex        =   6
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":0B28
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdThree 
      Height          =   990
      Left            =   4260
      TabIndex        =   7
      Top             =   3120
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":0D04
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSix 
      Height          =   990
      Left            =   4260
      TabIndex        =   8
      Top             =   4200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":0EE0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNine 
      Height          =   990
      Left            =   4260
      TabIndex        =   9
      Top             =   5280
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":10BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   1350
      Left            =   840
      TabIndex        =   11
      Top             =   6480
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   2381
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":1298
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHelp 
      Height          =   1335
      Left            =   5160
      TabIndex        =   15
      Top             =   9480
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":1479
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   2895
      Left            =   6720
      TabIndex        =   16
      Top             =   5160
      Width           =   4845
      _Version        =   131072
      _ExtentX        =   8546
      _ExtentY        =   5106
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientDayYear.frx":16AC
   End
   Begin VB.Label lblMonth 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   705
      Left            =   390
      TabIndex        =   14
      Top             =   2040
      Width           =   6225
   End
   Begin VB.Label lblDay1 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   705
      Left            =   6990
      TabIndex        =   13
      Top             =   2040
      Width           =   795
   End
   Begin VB.Label lblDay2 
      Alignment       =   2  'Center
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   705
      Left            =   8070
      TabIndex        =   12
      Top             =   2040
      Width           =   795
   End
   Begin VB.Label lblLabel1 
      Alignment       =   2  'Center
      BackColor       =   &H008D312C&
      Caption         =   "Please enter the DAY of your birth then press DONE"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   495
      Left            =   1440
      TabIndex        =   10
      Top             =   720
      Width           =   10575
   End
End
Attribute VB_Name = "frmPatientDayYear"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public TheMonth As String
Public TheDay As String
Public ExitFast As Boolean
Public Language As String

Private TheEntry As String
Private CurrentEntry As Integer
Private MaxEntry As Integer

Private Sub cmdDelete_Click()
If (CurrentEntry < 2) Then
    Exit Sub
End If
CurrentEntry = CurrentEntry - 1
If (CurrentEntry = 1) Then
    lblDay1.Caption = ""
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = ""
End If
End Sub

Private Sub cmdDone_Click()
TheDay = ""
TheEntry = Trim(lblDay1.Caption + lblDay2.Caption)
If (Len(TheEntry) = 1) Then
    TheEntry = "0" + TheEntry
End If
If (Len(TheEntry) > 0) Then
    TheDay = TheEntry
    Unload frmPatientDayYear
End If
End Sub

Private Sub cmdHelp_Click()
frmHelp.Language = Language
frmHelp.HelpOption = True
frmHelp.Show 1
If (frmHelp.ExitFast) Then
    ExitFast = True
    Unload frmPatientDayYear
End If
End Sub

Private Sub cmdZero_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "0"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "0"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdOne_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "1"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "1"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdTwo_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "2"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "2"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdThree_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "3"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "3"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdFour_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "4"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "4"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdFive_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "5"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "5"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdSix_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "6"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "6"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdSeven_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "7"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "7"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdEight_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "8"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "8"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdNine_Click()
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
If (CurrentEntry = 1) Then
    lblDay1.Caption = "9"
ElseIf (CurrentEntry = 2) Then
    lblDay2.Caption = "9"
End If
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    ExitFast = True
    Unload frmPatientDayYear
End If
End Sub

Private Sub Form_Load()
ExitFast = False
frmPatientDayYear.KeyPreview = True
CurrentEntry = 1
TheEntry = ""
lblMonth = TheMonth
lblDay1.Caption = ""
lblDay2.Caption = ""
TheDay = ""
MaxEntry = 2
Call TriggerNewLanguage(Language)
End Sub

Private Function TriggerNewLanguage(Lang As String) As Boolean
Dim i As Integer
Dim CId As Long
Dim CTag As String
Dim CText As String
Dim CLingo As String
TriggerNewLanguage = True
For i = 0 To frmPatientDayYear.Controls.Count - 1
    If (frmHome.SetControlLanguage(frmPatientDayYear.Controls(i).Name, -3, Lang, CId, CText, CLingo, CTag)) Then
        frmPatientDayYear.Controls(i).Visible = True
        If (Trim(CText) <> "") Then
            If (Left(frmPatientDayYear.Controls(i).Name, 3) = "lbl") Then
                frmPatientDayYear.Controls(i).Caption = CText
            Else
                frmPatientDayYear.Controls(i).Text = CText
            End If
        End If
        frmPatientDayYear.Controls(i).Tag = Trim(Str(CId))
        If (Lang = "English") Then
            frmPatientDayYear.Controls(i).ToolTipText = Trim(CTag)
        End If
    End If
Next i
Language = Lang
End Function
Public Sub FrmClose()
Unload Me
End Sub
