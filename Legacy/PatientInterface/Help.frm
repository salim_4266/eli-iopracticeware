VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmHelp 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Lucida Sans"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H008D312C&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   1080
      Top             =   1080
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResume 
      Height          =   2055
      Left            =   3833
      TabIndex        =   1
      Top             =   5040
      Width           =   4335
      _Version        =   131072
      _ExtentX        =   7646
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Help.frx":0000
   End
   Begin VB.Label lblLabel2 
      Alignment       =   2  'Center
      BackColor       =   &H008D312C&
      Caption         =   "Please see the Receptionist for assistance"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   1095
      Left            =   3000
      TabIndex        =   2
      Top             =   2280
      Width           =   6015
   End
   Begin VB.Label lblLabel1 
      Alignment       =   2  'Center
      BackColor       =   &H008D312C&
      Caption         =   "Please see the Receptionist for assistance"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   1095
      Left            =   2993
      TabIndex        =   0
      Top             =   2280
      Width           =   6015
   End
End
Attribute VB_Name = "frmHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public HelpOption As Boolean
Public ExitFast As Boolean
Public Language As String
Private TimerTrigger As Integer

Private Sub cmdResume_Click()
Timer1.Enabled = False
TimerTrigger = 0
ExitFast = False
Unload frmHelp
End Sub

Private Sub Form_Load()
cmdResume.Text = "Press here when you are ready to continue answering the questions"
lblLabel1.Caption = "Please see the Receptionist for Assistance"
lblLabel2.Caption = "The doctor will discuss this question with you later"
If (HelpOption) Then
    lblLabel1.Visible = True
    lblLabel2.Visible = False
Else
    lblLabel1.Visible = False
    lblLabel2.Visible = True
End If
Timer1.Enabled = False
ExitFast = False
TimerTrigger = 0
Timer1.Enabled = True
Call TriggerNewLanguage(Language)
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    Timer1.Enabled = False
    TimerTrigger = 0
    ExitFast = True
    Unload frmHelp
End If
End Sub

Private Sub Label1_Click()
Call Form_KeyPress(vbKeyEscape)
End Sub

Private Sub Timer1_Timer()
If (TimerTrigger > tmTimer) Then
    ExitFast = True
    Timer1.Enabled = False
    Unload frmHelp
Else
    TimerTrigger = TimerTrigger + 1
End If
End Sub

Private Function TriggerNewLanguage(Lang As String) As Boolean
Dim i As Integer
Dim CId As Long
Dim CText As String
Dim CLingo As String
TriggerNewLanguage = True
For i = 0 To frmHelp.Controls.Count - 1
    If (SetControlLanguage(frmHelp.Controls(i).Name, -9, Lang, CId, CText, CLingo)) Then
        If (Trim(CText) <> "") Then
            If (Left(frmHelp.Controls(i).Name, 3) = "lbl") Then
                frmHelp.Controls(i).Caption = CText
            Else
                frmHelp.Controls(i).Text = CText
            End If
        End If
        frmHelp.Controls(i).Tag = Trim(Str(CId))
        If (Lang = "English") Or (Lang = "") Then
            frmHelp.Controls(i).ToolTipText = Trim(CText)
        End If
    End If
Next i
Language = Lang
End Function

Private Function SetControlLanguage(CtrlName As String, FormId As Long, Lang As String, CtrlId As Long, CtrlText As String, CtrlLingo As String) As Boolean
Dim RetLang As DynamicLanguage
Dim RetCtrls As DynamicControls
CtrlText = ""
CtrlLingo = ""
Set RetLang = New DynamicLanguage
Set RetCtrls = New DynamicControls
SetControlLanguage = False
RetCtrls.FormId = FormId
RetCtrls.ControlName = CtrlName
RetCtrls.IEChoiceName = ""
If (RetCtrls.FindControl > 0) Then
    If (RetCtrls.SelectControl(1)) Then
        RetLang.ControlId = RetCtrls.ControlId
        RetLang.Language = Lang
        If (RetLang.FindLanguageControl > 0) Then
            If (RetLang.SelectLanguageControl(1)) Then
                CtrlId = RetCtrls.ControlId
                CtrlText = RetLang.ControlText
                CtrlLingo = RetLang.ConfirmLingo
                SetControlLanguage = True
            End If
        Else
            CtrlId = RetCtrls.ControlId
            CtrlText = RetCtrls.ControlText
            CtrlLingo = RetCtrls.ConfirmLingo
            SetControlLanguage = True
        End If
    End If
End If
Set RetCtrls = Nothing
Set RetLang = Nothing
End Function
Public Sub FrmClose()
Call cmdResume_Click
Unload Me
End Sub

