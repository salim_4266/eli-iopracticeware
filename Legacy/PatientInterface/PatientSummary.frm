VERSION 5.00
Begin VB.Form frmPatientSummary 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstRightDiagnosis 
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   2760
      ItemData        =   "PatientSummary.frx":0000
      Left            =   6240
      List            =   "PatientSummary.frx":0002
      TabIndex        =   9
      Top             =   4320
      Width           =   5295
   End
   Begin VB.ListBox lstRightComplaints 
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   2760
      ItemData        =   "PatientSummary.frx":0004
      Left            =   6240
      List            =   "PatientSummary.frx":0006
      TabIndex        =   7
      Top             =   960
      Width           =   5295
   End
   Begin VB.CommandButton cmdReplay 
      BackColor       =   &H00C0C000&
      Caption         =   "Replay"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3720
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   8160
      Width           =   2055
   End
   Begin VB.CommandButton cmdExit 
      BackColor       =   &H00C0C000&
      Caption         =   "Touch When Done"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   6840
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   8160
      Width           =   2055
   End
   Begin VB.ListBox lstLeftDiagnosis 
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   2760
      ItemData        =   "PatientSummary.frx":0008
      Left            =   360
      List            =   "PatientSummary.frx":000A
      TabIndex        =   3
      Top             =   4320
      Width           =   5415
   End
   Begin VB.ListBox lstLeftComplaints 
      BackColor       =   &H00F0FFFE&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   2760
      ItemData        =   "PatientSummary.frx":000C
      Left            =   360
      List            =   "PatientSummary.frx":000E
      TabIndex        =   2
      Top             =   960
      Width           =   5415
   End
   Begin VB.Label lblRightDiagnosis 
      Alignment       =   2  'Center
      BackColor       =   &H00808000&
      Caption         =   "Possible Right Eye Diagnosis"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   6240
      TabIndex        =   10
      Top             =   3840
      Width           =   4100
   End
   Begin VB.Label lblRightComplaints 
      Alignment       =   2  'Center
      BackColor       =   &H00808000&
      Caption         =   "Right Eye Patient Complaints"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   6240
      TabIndex        =   8
      Top             =   480
      Width           =   4100
   End
   Begin VB.Label InProgressLabel 
      AutoSize        =   -1  'True
      BackColor       =   &H00FF80FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Inference Analysis In Progress"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   450
      Left            =   3960
      TabIndex        =   5
      Top             =   7440
      Width           =   4755
   End
   Begin VB.Label lblLeftDiagnosis 
      Alignment       =   2  'Center
      BackColor       =   &H00808000&
      Caption         =   "Possible Left Eye Diagnosis"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   360
      TabIndex        =   1
      Top             =   3840
      Width           =   4100
   End
   Begin VB.Label lblLeftComplaints 
      Alignment       =   2  'Center
      BackColor       =   &H00808000&
      Caption         =   "Left Eye Patient Complaints"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   4100
   End
End
Attribute VB_Name = "frmPatientSummary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public InferenceDb As String
Public DisplayOn
Public ReplayOn

Private Sub cmdExit_Click()
DisplayOn = False
ReplayOn = False
Unload frmPatientSummary
End Sub

Private Sub cmdReplay_Click()
DisplayOn = False
ReplayOn = True
Unload frmPatientSummary
End Sub
