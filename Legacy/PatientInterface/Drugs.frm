VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmDrugs 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdButton16 
      Height          =   1095
      Left            =   8880
      TabIndex        =   26
      Top             =   1560
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton12 
      Height          =   1095
      Left            =   6000
      TabIndex        =   24
      Top             =   2760
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":0237
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton9 
      Height          =   1095
      Left            =   3120
      TabIndex        =   17
      Top             =   5160
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":046E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton5 
      Height          =   1095
      Left            =   240
      TabIndex        =   13
      Top             =   6360
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":06A5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton4 
      Height          =   1095
      Left            =   240
      TabIndex        =   12
      Top             =   5160
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":08DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton3 
      Height          =   1095
      Left            =   240
      TabIndex        =   11
      Top             =   3960
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":0B13
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton2 
      Height          =   1095
      Left            =   240
      TabIndex        =   10
      Top             =   2760
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":0D4A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   1335
      Left            =   10080
      TabIndex        =   0
      Top             =   7560
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":0F81
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDontKnow 
      Height          =   1335
      Left            =   5205
      TabIndex        =   1
      Top             =   7560
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":11B4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNo 
      Height          =   1335
      Left            =   7635
      TabIndex        =   2
      Top             =   7560
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":13EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHelp 
      Height          =   1335
      Left            =   2760
      TabIndex        =   3
      Top             =   7560
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":1623
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton10 
      Height          =   1095
      Left            =   3120
      TabIndex        =   9
      Top             =   6360
      Visible         =   0   'False
      Width           =   2835
      _Version        =   131072
      _ExtentX        =   5001
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":1856
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton6 
      Height          =   1095
      Left            =   3120
      TabIndex        =   14
      Top             =   1560
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":1A8D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton7 
      Height          =   1095
      Left            =   3120
      TabIndex        =   15
      Top             =   2760
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":1CC4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton8 
      Height          =   1095
      Left            =   3120
      TabIndex        =   16
      Top             =   3960
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":1EFB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton1 
      Height          =   1095
      Left            =   240
      TabIndex        =   18
      Top             =   1560
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":2132
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton11 
      Height          =   1095
      Left            =   6000
      TabIndex        =   19
      Top             =   1560
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":2369
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1095
      Left            =   8880
      TabIndex        =   21
      Top             =   6360
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":25A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton14 
      Height          =   1095
      Left            =   6000
      TabIndex        =   22
      Top             =   5160
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":27D9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton15 
      Height          =   1095
      Left            =   6000
      TabIndex        =   23
      Top             =   6360
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":2A10
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton13 
      Height          =   1095
      Left            =   6000
      TabIndex        =   25
      Top             =   3960
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":2C47
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton19 
      Height          =   1095
      Left            =   8880
      TabIndex        =   27
      Top             =   5160
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":2E7E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton17 
      Height          =   1095
      Left            =   8880
      TabIndex        =   28
      Top             =   2760
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":30B5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdButton18 
      Height          =   1095
      Left            =   8880
      TabIndex        =   29
      Top             =   3960
      Visible         =   0   'False
      Width           =   2805
      _Version        =   131072
      _ExtentX        =   4948
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Drugs.frx":32EC
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H008D312C&
      Caption         =   "Select any drugs you may have been prescribed"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   3000
      TabIndex        =   20
      Top             =   720
      Width           =   5895
   End
   Begin VB.Label lblCurrentSymptom 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current Symptoms"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   2295
   End
   Begin VB.Label lblLenses 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Glasses/Contacts"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   2400
      TabIndex        =   7
      Top             =   120
      Width           =   2535
   End
   Begin VB.Label lblEyeHistory 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Eye History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   4920
      TabIndex        =   6
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lblMedicalHistory 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Medical History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label lblFamilyHistory 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Family Medical History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   9120
      TabIndex        =   4
      Top             =   120
      Width           =   2775
   End
End
Attribute VB_Name = "frmDrugs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public BarOn As Boolean
Public StoreResults As String
Public DrugAlias1 As String
Public DrugAlias2 As String
Public DrugAlias3 As String
Public DrugAlias4 As String
Public QuestionFamily As Long
Public ChangeRequest As Boolean

Private Const MaxDrugsTaken As Integer = 20
Private Type ExternalReference
    Name As String
    Dosage As String
    LastTaken As String
    Frequency As String
    Ref As Long
End Type
Private Const ButtonSelectionColor As Long = 14745312
Private Const TextSelectionColor As Long = 0

Private AliasOn As Boolean
Private AliasSet As Boolean
Private TotalDrugs As Long
Private CurrentIndex As Integer
Private CurrentRxIndex As Integer
Private Question As String
Private OriginalButtonColor As Long
Private OriginalTextColor As Long
Private NotSure As Boolean
Private RetrieveDrugs As DiagnosisMasterDrugs
Private DrugsTaken(MaxDrugsTaken) As ExternalReference

Private Sub cmdApply_Click()
On Error GoTo UI_ErrorHandler
Dim p As Integer
Dim DrugStore As String
Dim FileNum As Integer
Dim ButtonName As String
Dim PostedResults As Boolean
If (Trim(StoreResults) = "") Then
    Unload frmDrugs
    Exit Sub
End If
PostedResults = False
FileNum = FreeFile
If (FM.IsFileThere(StoreResults)) Then
    If (ChangeRequest) Then
        FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
        Print #FileNum, "QUESTION=" + "/" + Question
        FM.CloseFile CLng(FileNum)
    End If
Else
    FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    Print #FileNum, "QUESTION=" + "/" + Question
    FM.CloseFile CLng(FileNum)
End If
If (NotSure) Then
    PostedResults = True
    FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    Print #FileNum, "QUESTION=" + "/" + Question
    Print #FileNum, "DONTKNOW=T" + Trim(Str(0))
    FM.CloseFile CLng(FileNum)
Else
    For p = 1 To MaxDrugsTaken
        If (Trim(DrugsTaken(p).Name) <> "") Then
            ButtonName = "DRUGTBL:" + Trim(DrugsTaken(p).Name) + "=T" + Trim(Str(DrugsTaken(p).Ref)) + " <" + Trim(DrugsTaken(p).Dosage) + " " + Trim(DrugsTaken(p).LastTaken) + " " + Trim(DrugsTaken(p).Frequency) + ">"
            If Not (IsDrugDuplicate(ButtonName, StoreResults)) Then
                PostedResults = True
                FM.OpenFile StoreResults, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
                Print #FileNum, ButtonName
                FM.CloseFile CLng(FileNum)
            End If
        End If
    Next p
    FM.CloseFile CLng(FileNum)
End If
ChangeRequest = PostedResults
Unload frmDrugs
Exit Sub
UI_ErrorHandler:
    Call PinpointError("PI-Drugs", Error(Err))
    Resume LeaveFast
LeaveFast:
    Unload frmDrugs
End Sub

Private Sub cmdDontKnow_Click()
NotSure = Not (NotSure)
If (NotSure) Then
    cmdDontKnow.BackColor = ButtonSelectionColor
    cmdDontKnow.ForeColor = TextSelectionColor
    frmHelp.HelpOption = False
    frmHelp.Show 1
    Call cmdApply_Click
Else
    cmdDontKnow.BackColor = OriginalButtonColor
    cmdDontKnow.ForeColor = OriginalTextColor
End If
End Sub

Private Sub cmdHelp_Click()
frmHelp.HelpOption = True
frmHelp.Show 1
End Sub

Private Sub cmdMore_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > TotalDrugs) Then
    CurrentIndex = 1
End If
Call LoadDrugs
End Sub

Private Sub cmdNo_Click()
Call cmdApply_Click
End Sub

Private Sub ButtonAction(AButton As fpBtn, Ref As Integer)
Dim i As Integer
Dim ClearRx As Integer
If Not (AliasOn) Then
    If (AButton.BackColor <> ButtonSelectionColor) Then
        AButton.BackColor = ButtonSelectionColor
        AButton.ForeColor = TextSelectionColor
        DrugsTaken(CurrentRxIndex).Name = AButton.Text
        DrugsTaken(CurrentRxIndex).Dosage = ""
        DrugsTaken(CurrentRxIndex).LastTaken = ""
        DrugsTaken(CurrentRxIndex).Ref = Val(AButton.Tag)
        If Not (BarOn) Then
            Call SetRxDetails(CurrentRxIndex)
        End If
        CurrentRxIndex = CurrentRxIndex + 1
    Else
        AButton.BackColor = OriginalButtonColor
        AButton.ForeColor = OriginalTextColor
        For i = 1 To CurrentRxIndex
            If (Trim(AButton.Tag) = Trim(Str(DrugsTaken(i).Ref))) Then
                DrugsTaken(i).Name = ""
                DrugsTaken(i).Dosage = ""
                DrugsTaken(i).LastTaken = ""
                DrugsTaken(i).Ref = 0
                ClearRx = i
                Exit For
            End If
        Next i
        For i = ClearRx To CurrentRxIndex
            DrugsTaken(i).Name = DrugsTaken(i + 1).Name
            DrugsTaken(i).Dosage = DrugsTaken(i + 1).Dosage
            DrugsTaken(i).LastTaken = DrugsTaken(i + 1).LastTaken
            DrugsTaken(i).Ref = DrugsTaken(i + 1).Ref
        Next i
        CurrentRxIndex = CurrentRxIndex - 1
        If (CurrentRxIndex < 1) Then
            CurrentRxIndex = 1
        End If
    End If
Else
    AliasSet = True
    DrugAlias1 = AButton.Text
End If
End Sub

Private Sub cmdButton1_Click()
Call ButtonAction(cmdButton1, 1)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton2_Click()
Call ButtonAction(cmdButton2, 2)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton3_Click()
Call ButtonAction(cmdButton3, 3)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton4_Click()
Call ButtonAction(cmdButton4, 4)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton5_Click()
Call ButtonAction(cmdButton5, 5)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton6_Click()
Call ButtonAction(cmdButton6, 6)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton7_Click()
Call ButtonAction(cmdButton7, 7)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton8_Click()
Call ButtonAction(cmdButton8, 8)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton9_Click()
Call ButtonAction(cmdButton9, 9)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton10_Click()
Call ButtonAction(cmdButton10, 10)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton11_Click()
Call ButtonAction(cmdButton11, 11)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub


Private Sub cmdButton12_Click()
Call ButtonAction(cmdButton12, 12)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton13_Click()
Call ButtonAction(cmdButton13, 13)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton14_Click()
Call ButtonAction(cmdButton14, 14)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton15_Click()
Call ButtonAction(cmdButton15, 15)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton16_Click()
Call ButtonAction(cmdButton16, 16)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton17_Click()
Call ButtonAction(cmdButton17, 17)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton18_Click()
Call ButtonAction(cmdButton18, 18)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Private Sub cmdButton19_Click()
Call ButtonAction(cmdButton19, 19)
If (AliasOn) And (AliasSet) Then
    Call cmdApply_Click
End If
End Sub

Public Function LoadDrugs() As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer
Dim ButtonCnt As Integer
LoadDrugs = False
i = 0
cmdButton1.Visible = False
cmdButton2.Visible = False
cmdButton3.Visible = False
cmdButton4.Visible = False
cmdButton5.Visible = False
cmdButton6.Visible = False
cmdButton7.Visible = False
cmdButton8.Visible = False
cmdButton9.Visible = False
cmdButton10.Visible = False
cmdButton11.Visible = False
cmdButton12.Visible = False
cmdButton13.Visible = False
cmdButton14.Visible = False
cmdButton15.Visible = False
cmdButton16.Visible = False
cmdButton17.Visible = False
cmdButton18.Visible = False
cmdButton19.Visible = False
cmdMore.Visible = False
If (QuestionFamily = 1) Then
    lblCurrentSymptom.ForeColor = &HFFFFFF
ElseIf (QuestionFamily = 2) Then
    lblLenses.ForeColor = &HFFFFFF
ElseIf (QuestionFamily = 3) Then
    lblEyeHistory.ForeColor = &HFFFFFF
ElseIf (QuestionFamily = 4) Then
    lblMedicalHistory.ForeColor = &HFFFFFF
ElseIf (QuestionFamily = 5) Then
    lblFamilyHistory.ForeColor = &HFFFFFF
End If
lblCurrentSymptom.Visible = BarOn
lblLenses.Visible = BarOn
lblEyeHistory.Visible = BarOn
lblMedicalHistory.Visible = BarOn
lblFamilyHistory.Visible = BarOn
AliasOn = False
AliasSet = False
Set RetrieveDrugs = New DiagnosisMasterDrugs
If (Trim(DrugAlias1) <> "") Or (Trim(DrugAlias2) <> "") Or _
   (Trim(DrugAlias3) <> "") Then
    RetrieveDrugs.DrugDiagnosisAlias1 = DrugAlias1
    RetrieveDrugs.DrugDiagnosisAlias2 = DrugAlias2
    RetrieveDrugs.DrugDiagnosisAlias3 = DrugAlias3
    RetrieveDrugs.DrugName = ""
    RetrieveDrugs.DrugCompany = ""
    RetrieveDrugs.DrugGenericName = ""
    RetrieveDrugs.DrugName = ""
    RetrieveDrugs.DrugRank = 0
    TotalDrugs = RetrieveDrugs.FindPrimaryDrugsDistinct
    If (TotalDrugs > 0) Then
        If (CurrentIndex < 1) Then
            CurrentIndex = 1
        End If
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveDrugs.SelectDrugDistinct(i)) And (ButtonCnt < 20)
            If (ButtonCnt = 1) Then
                cmdButton1.Text = RetrieveDrugs.DrugName
                cmdButton1.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdButton2.Text = RetrieveDrugs.DrugName
                cmdButton2.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdButton3.Text = RetrieveDrugs.DrugName
                cmdButton3.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdButton4.Text = RetrieveDrugs.DrugName
                cmdButton4.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdButton5.Text = RetrieveDrugs.DrugName
                cmdButton5.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdButton6.Text = RetrieveDrugs.DrugName
                cmdButton6.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdButton7.Text = RetrieveDrugs.DrugName
                cmdButton7.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdButton8.Text = RetrieveDrugs.DrugName
                cmdButton8.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdButton9.Text = RetrieveDrugs.DrugName
                cmdButton9.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdButton10.Text = RetrieveDrugs.DrugName
                cmdButton10.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdButton11.Text = RetrieveDrugs.DrugName
                cmdButton11.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdButton12.Text = RetrieveDrugs.DrugName
                cmdButton12.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdButton13.Text = RetrieveDrugs.DrugName
                cmdButton13.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdButton14.Text = RetrieveDrugs.DrugName
                cmdButton14.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdButton15.Text = RetrieveDrugs.DrugName
                cmdButton15.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdButton16.Text = RetrieveDrugs.DrugName
                cmdButton16.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdButton17.Text = RetrieveDrugs.DrugName
                cmdButton17.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdButton18.Text = RetrieveDrugs.DrugName
                cmdButton18.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton18.Visible = True
            ElseIf (ButtonCnt = 19) Then
                cmdButton19.Text = RetrieveDrugs.DrugName
                cmdButton19.Tag = Trim(Str(RetrieveDrugs.DrugId))
                cmdButton19.Visible = True
            End If
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
    End If
Else
    If Not (BarOn) Then
        AliasOn = True
        Label1.Caption = "Select appropriate drug family"
        RetrieveDrugs.DrugDiagnosisAlias1 = Chr(1)
        RetrieveDrugs.DrugDiagnosisAlias2 = Chr(1)
        RetrieveDrugs.DrugDiagnosisAlias3 = Chr(1)
        RetrieveDrugs.DrugName = ""
        RetrieveDrugs.DrugCompany = ""
        RetrieveDrugs.DrugGenericName = ""
        RetrieveDrugs.DrugName = ""
        RetrieveDrugs.DrugRank = -1
        TotalDrugs = RetrieveDrugs.FindDrugAlias
        If (TotalDrugs > 0) Then
            If (CurrentIndex < 1) Then
                CurrentIndex = 1
            End If
            i = CurrentIndex
            ButtonCnt = 1
            While (RetrieveDrugs.SelectDrugAlias(i)) And (ButtonCnt < 12)
                If (ButtonCnt = 1) Then
                    cmdButton1.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton1.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton1.Visible = True
                ElseIf (ButtonCnt = 2) Then
                    cmdButton2.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton2.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton2.Visible = True
                ElseIf (ButtonCnt = 3) Then
                    cmdButton3.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton3.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton3.Visible = True
                ElseIf (ButtonCnt = 4) Then
                    cmdButton4.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton4.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton4.Visible = True
                ElseIf (ButtonCnt = 5) Then
                    cmdButton5.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton5.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton5.Visible = True
                ElseIf (ButtonCnt = 6) Then
                    cmdButton6.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton6.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton6.Visible = True
                ElseIf (ButtonCnt = 7) Then
                    cmdButton7.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton7.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton7.Visible = True
                ElseIf (ButtonCnt = 8) Then
                    cmdButton8.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton8.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton8.Visible = True
                ElseIf (ButtonCnt = 9) Then
                    cmdButton9.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton9.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton9.Visible = True
                ElseIf (ButtonCnt = 10) Then
                    cmdButton10.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton10.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton10.Visible = True
                ElseIf (ButtonCnt = 11) Then
                    cmdButton11.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton11.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton11.Visible = True
                ElseIf (ButtonCnt = 12) Then
                    cmdButton12.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton12.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton12.Visible = True
                ElseIf (ButtonCnt = 13) Then
                    cmdButton13.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton13.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton13.Visible = True
                ElseIf (ButtonCnt = 14) Then
                    cmdButton14.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton14.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton14.Visible = True
                ElseIf (ButtonCnt = 15) Then
                    cmdButton15.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton15.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton15.Visible = True
                ElseIf (ButtonCnt = 16) Then
                    cmdButton16.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton16.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton16.Visible = True
                ElseIf (ButtonCnt = 17) Then
                    cmdButton17.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton17.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton17.Visible = True
                ElseIf (ButtonCnt = 18) Then
                    cmdButton18.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton18.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton18.Visible = True
                ElseIf (ButtonCnt = 19) Then
                    cmdButton19.Text = RetrieveDrugs.DrugDiagnosisAlias1
                    cmdButton19.Tag = Trim(Str(RetrieveDrugs.DrugId))
                    cmdButton19.Visible = True
                End If
                i = i + 1
                ButtonCnt = ButtonCnt + 1
            Wend
        End If
    End If
End If
CurrentIndex = i
Set RetrieveDrugs = Nothing
If (TotalDrugs > 0) And (CurrentIndex > 0) Then
    LoadDrugs = True
End If
cmdMore.Visible = False
If (TotalDrugs > 20) Then
    cmdMore.Visible = True
    If (CurrentIndex < TotalDrugs) Then
        If (Trim(DrugAlias1) = "") And (Trim(DrugAlias2) = "") And _
           (Trim(DrugAlias3) = "") And (Trim(DrugAlias4) = "") Then
            cmdMore.Text = "More Meds"
        Else
            cmdMore.Text = "More Drugs"
        End If
    Else
        cmdMore.Text = "Start Over"
    End If
End If
Exit Function
UI_ErrorHandler:
    Set RetrieveDrugs = Nothing
    Call PinpointError("PI-LoadDrugs", Error(Err))
    Resume LeaveFast
LeaveFast:
End Function

Private Sub Form_Load()
CurrentIndex = 1
CurrentRxIndex = 1
frmDrugs.ZOrder 0
frmDrugs.KeyPreview = True
frmDrugs.MousePointer = 2
OriginalButtonColor = cmdButton1.BackColor
OriginalTextColor = cmdButton1.ForeColor
Question = "Drugs Taken"
NotSure = False
End Sub

Private Sub SetRxDetails(TheRef As Integer)
Dim TheName As String
Dim TheDetails As String
Dim RetDrug As DiagnosisMasterDrugs
TheDetails = ""
If (TheRef > 0) Then
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugId = DrugsTaken(TheRef).Ref
    If Not (RetDrug.RetrieveDrug) Then
        Exit Sub
    End If
    TheName = Trim(RetDrug.DrugName)
'Dosage
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Field = "Dosage for " + TheName
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_VEOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_ChoiceOn1 = False
    frmNumericPad.NumPad_Choice1 = "mg"
    frmNumericPad.NumPad_ChoiceOn2 = False
    frmNumericPad.NumPad_Choice2 = "g"
    frmNumericPad.NumPad_ChoiceOn3 = False
    frmNumericPad.NumPad_Choice3 = "ml"
    frmNumericPad.NumPad_ChoiceOn4 = False
    frmNumericPad.NumPad_Choice4 = "IV"
    frmNumericPad.NumPad_ChoiceOn5 = False
    frmNumericPad.NumPad_Choice5 = "mcg"
    frmNumericPad.NumPad_ChoiceOn6 = False
    frmNumericPad.NumPad_Choice6 = "cc"
    frmNumericPad.NumPad_ChoiceOn7 = False
    frmNumericPad.NumPad_Choice7 = "T gtt"
    frmNumericPad.NumPad_ChoiceOn8 = False
    frmNumericPad.NumPad_Choice8 = "T tblt"
    frmNumericPad.NumPad_ChoiceOn9 = False
    frmNumericPad.NumPad_Choice9 = "Over the Counter"
    frmNumericPad.NumPad_ChoiceOn10 = False
    frmNumericPad.NumPad_Choice10 = "Samples"
    frmNumericPad.NumPad_ChoiceOn11 = False
    frmNumericPad.NumPad_Choice11 = Trim(RetDrug.DrugDispense1)
    frmNumericPad.NumPad_ChoiceOn12 = False
    frmNumericPad.NumPad_Choice12 = Trim(RetDrug.DrugDispense2)
    frmNumericPad.NumPad_ChoiceOn13 = False
    frmNumericPad.NumPad_Choice13 = Trim(RetDrug.DrugDispense3)
    frmNumericPad.NumPad_ChoiceOn14 = False
    frmNumericPad.NumPad_Choice14 = Trim(RetDrug.DrugDispense4)
    frmNumericPad.NumPad_ChoiceOn15 = False
    frmNumericPad.NumPad_Choice15 = Trim(RetDrug.DrugDispense5)
    frmNumericPad.NumPad_ChoiceOn16 = False
    frmNumericPad.NumPad_Choice16 = Trim(RetDrug.DrugDispense6)
    frmNumericPad.NumPad_ChoiceOn17 = False
    frmNumericPad.NumPad_Choice17 = Trim(RetDrug.DrugDispense7)
    frmNumericPad.NumPad_ChoiceOn18 = False
    frmNumericPad.NumPad_Choice18 = Trim(RetDrug.DrugDispense8)
    frmNumericPad.NumPad_ChoiceOn19 = False
    frmNumericPad.NumPad_Choice19 = ""
    frmNumericPad.NumPad_ChoiceOn20 = False
    frmNumericPad.NumPad_Choice20 = ""
    frmNumericPad.Show 1
    TheDetails = Trim(frmNumericPad.NumPad_Result)
    If (frmNumericPad.NumPad_ChoiceOn1) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice1 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn2) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice2 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn3) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice3 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn4) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice4 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn5) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice5 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn6) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice6 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn7) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice7 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn8) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice8 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn9) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice9 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn11) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice11 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn12) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice12 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn13) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice13 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn14) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice14 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn15) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice15 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn16) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice16 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn17) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice17 + ") "
    End If
    If (frmNumericPad.NumPad_ChoiceOn18) Then
        TheDetails = TheDetails + " (" + frmNumericPad.NumPad_Choice18 + ") "
    End If
    DrugsTaken(TheRef).Dosage = TheDetails
'Last Taken
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Field = "Last Date " + Trim(TheName) + " was taken"
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_VEOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_ChoiceOn1 = False
    frmNumericPad.NumPad_Choice1 = "AM"
    frmNumericPad.NumPad_ChoiceOn2 = False
    frmNumericPad.NumPad_Choice2 = "PM"
    frmNumericPad.NumPad_ChoiceOn3 = False
    frmNumericPad.NumPad_Choice3 = ""
    frmNumericPad.NumPad_ChoiceOn4 = False
    frmNumericPad.NumPad_Choice4 = ""
    frmNumericPad.NumPad_ChoiceOn5 = False
    frmNumericPad.NumPad_Choice5 = ""
    frmNumericPad.NumPad_ChoiceOn6 = False
    frmNumericPad.NumPad_Choice6 = ""
    frmNumericPad.NumPad_ChoiceOn7 = False
    frmNumericPad.NumPad_Choice7 = ""
    frmNumericPad.NumPad_ChoiceOn8 = False
    frmNumericPad.NumPad_Choice8 = ""
    frmNumericPad.NumPad_ChoiceOn9 = False
    frmNumericPad.NumPad_Choice9 = ""
    frmNumericPad.NumPad_ChoiceOn10 = False
    frmNumericPad.NumPad_Choice10 = ""
    frmNumericPad.NumPad_ChoiceOn11 = False
    frmNumericPad.NumPad_Choice11 = ""
    frmNumericPad.NumPad_ChoiceOn12 = False
    frmNumericPad.NumPad_Choice12 = ""
    frmNumericPad.NumPad_ChoiceOn13 = False
    frmNumericPad.NumPad_Choice13 = ""
    frmNumericPad.NumPad_ChoiceOn14 = False
    frmNumericPad.NumPad_Choice14 = ""
    frmNumericPad.NumPad_ChoiceOn15 = False
    frmNumericPad.NumPad_Choice15 = ""
    frmNumericPad.NumPad_ChoiceOn16 = False
    frmNumericPad.NumPad_Choice16 = ""
    frmNumericPad.NumPad_ChoiceOn17 = False
    frmNumericPad.NumPad_Choice17 = ""
    frmNumericPad.NumPad_ChoiceOn18 = False
    frmNumericPad.NumPad_Choice18 = ""
    frmNumericPad.NumPad_ChoiceOn19 = False
    frmNumericPad.NumPad_Choice19 = ""
    frmNumericPad.NumPad_ChoiceOn20 = False
    frmNumericPad.NumPad_Choice20 = ""
    frmNumericPad.Show 1
    If (Trim(frmNumericPad.NumPad_Result) <> "") Then
        DrugsTaken(TheRef).LastTaken = "LT=" + Trim(frmNumericPad.NumPad_Result)
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            DrugsTaken(TheRef).LastTaken = DrugsTaken(TheRef).LastTaken + " (" + frmNumericPad.NumPad_Choice1 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            DrugsTaken(TheRef).LastTaken = DrugsTaken(TheRef).LastTaken + " (" + frmNumericPad.NumPad_Choice2 + ") "
        End If
    End If
    Set RetDrug = Nothing
End If
End Sub

Private Function IsDrugDuplicate(TheText As String, TheFile As String) As Boolean
Dim Rec As String
Dim AFile As Long
IsDrugDuplicate = False
If (FM.IsFileThere(TheFile)) Then
    AFile = 501
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(AFile)
    Do Until (EOF(AFile))
        Line Input #AFile, Rec
        If (Trim(Rec) = Trim(TheText)) Then
            IsDrugDuplicate = True
            Exit Do
        End If
    Loop
    FM.CloseFile CLng(AFile)
End If
End Function
