VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmClinicalEntry 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Lucida Sans"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   1335
      Left            =   10080
      TabIndex        =   5
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDontKnow 
      Height          =   1335
      Left            =   5205
      TabIndex        =   6
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry.frx":0233
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBack 
      Height          =   1335
      Left            =   360
      TabIndex        =   7
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry.frx":046A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNo 
      Height          =   1335
      Left            =   7635
      TabIndex        =   8
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry.frx":06B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHelp 
      Height          =   1335
      Left            =   2760
      TabIndex        =   9
      Top             =   7440
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalEntry.frx":08E8
   End
   Begin VB.Label lblSwitch 
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   11640
      TabIndex        =   10
      Top             =   600
      Visible         =   0   'False
      Width           =   180
   End
   Begin VB.Label lblFamilyHistory 
      Alignment       =   2  'Center
      BackColor       =   &H002E46BC&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Family Medical History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   9120
      TabIndex        =   4
      Top             =   120
      Width           =   2775
   End
   Begin VB.Label lblMedicalHistory 
      Alignment       =   2  'Center
      BackColor       =   &H000A70A7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Medical History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   6960
      TabIndex        =   3
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label lblEyeHistory 
      Alignment       =   2  'Center
      BackColor       =   &H009D6DA7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Eye History"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   4920
      TabIndex        =   2
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lblLenses 
      Alignment       =   2  'Center
      BackColor       =   &H007A8435&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Glasses/Contacts"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   2400
      TabIndex        =   1
      Top             =   120
      Width           =   2535
   End
   Begin VB.Label lblCurrentSymptom 
      Alignment       =   2  'Center
      BackColor       =   &H00CFA076&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current Symptoms"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   11.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmClinicalEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public BarOn As Boolean
Public NotSure As Boolean
Public ButtonSelectionColor As Long
Public TextSelectionColor As Long
Public CloseDown As Boolean
Public ChangeOn As Boolean
Public ChangeRequest As Boolean
Public LevelType As Integer
Public StoreResults As String
Public FormOn As Boolean
Public FormColor As Long
Public Question As String
Public QuestionFamily As Long
Public InitialForm As Form
Public Language As String

Private Const EventCounter = 100
Private Const SelectionColor As Long = &HC0C000

'Hidden
Private ReloadForm As Boolean
Private CurrentFormId As Long
Private CurrentControlName As String
Private TheButtons(35) As Boolean
Private TheButtonName(35) As String
Private TheButtonNest(35) As String
Private TheButtonTrigger(35) As String * 64
Private TheButtonTriggerText(35) As String * 64
Private TheButtonVisible(35) As Boolean
Private TheButtonTextColor(35) As Long
Private TheButtonColor(35) As Long
Private TheButtonId(35) As Long

Private ExitFast As Boolean
Private MaxControls As Integer
Private MaxButtons As Integer
Private MaxLabel As Integer

Private TheForm As DynamicForms
Private TheControl As DynamicControls
Private TheSecondaryForm As DynamicSecondaryForms
Private TheSecondaryControl As DynamicSecondaryControls

' you can have any number of Labels
' code is not required for Label processing
Private WithEvents lblLabel As Label
Attribute lblLabel.VB_VarHelpID = -1
' you can have 35 CommandButtons per form
' unique code is required for each Button
Private WithEvents cmdButton1 As fpBtn
Attribute cmdButton1.VB_VarHelpID = -1
Private WithEvents cmdButton2 As fpBtn
Attribute cmdButton2.VB_VarHelpID = -1
Private WithEvents cmdButton3 As fpBtn
Attribute cmdButton3.VB_VarHelpID = -1
Private WithEvents cmdButton4 As fpBtn
Attribute cmdButton4.VB_VarHelpID = -1
Private WithEvents cmdButton5 As fpBtn
Attribute cmdButton5.VB_VarHelpID = -1
Private WithEvents cmdButton6 As fpBtn
Attribute cmdButton6.VB_VarHelpID = -1
Private WithEvents cmdButton7 As fpBtn
Attribute cmdButton7.VB_VarHelpID = -1
Private WithEvents cmdButton8 As fpBtn
Attribute cmdButton8.VB_VarHelpID = -1
Private WithEvents cmdButton9 As fpBtn
Attribute cmdButton9.VB_VarHelpID = -1
Private WithEvents cmdButton10 As fpBtn
Attribute cmdButton10.VB_VarHelpID = -1
Private WithEvents cmdButton11 As fpBtn
Attribute cmdButton11.VB_VarHelpID = -1
Private WithEvents cmdButton12 As fpBtn
Attribute cmdButton12.VB_VarHelpID = -1
Private WithEvents cmdButton13 As fpBtn
Attribute cmdButton13.VB_VarHelpID = -1
Private WithEvents cmdButton14 As fpBtn
Attribute cmdButton14.VB_VarHelpID = -1
Private WithEvents cmdButton15 As fpBtn
Attribute cmdButton15.VB_VarHelpID = -1
Private WithEvents cmdButton16 As fpBtn
Attribute cmdButton16.VB_VarHelpID = -1
Private WithEvents cmdButton17 As fpBtn
Attribute cmdButton17.VB_VarHelpID = -1
Private WithEvents cmdButton18 As fpBtn
Attribute cmdButton18.VB_VarHelpID = -1
Private WithEvents cmdButton19 As fpBtn
Attribute cmdButton19.VB_VarHelpID = -1
Private WithEvents cmdButton20 As fpBtn
Attribute cmdButton20.VB_VarHelpID = -1
Private WithEvents cmdButton21 As fpBtn
Attribute cmdButton21.VB_VarHelpID = -1
Private WithEvents cmdButton22 As fpBtn
Attribute cmdButton22.VB_VarHelpID = -1
Private WithEvents cmdButton23 As fpBtn
Attribute cmdButton23.VB_VarHelpID = -1
Private WithEvents cmdButton24 As fpBtn
Attribute cmdButton24.VB_VarHelpID = -1
Private WithEvents cmdButton25 As fpBtn
Attribute cmdButton25.VB_VarHelpID = -1
Private WithEvents cmdButton26 As fpBtn
Attribute cmdButton26.VB_VarHelpID = -1
Private WithEvents cmdButton27 As fpBtn
Attribute cmdButton27.VB_VarHelpID = -1
Private WithEvents cmdButton28 As fpBtn
Attribute cmdButton28.VB_VarHelpID = -1
Private WithEvents cmdButton29 As fpBtn
Attribute cmdButton29.VB_VarHelpID = -1
Private WithEvents cmdButton30 As fpBtn
Attribute cmdButton30.VB_VarHelpID = -1
Private WithEvents cmdButton31 As fpBtn
Attribute cmdButton31.VB_VarHelpID = -1
Private WithEvents cmdButton32 As fpBtn
Attribute cmdButton32.VB_VarHelpID = -1
Private WithEvents cmdButton33 As fpBtn
Attribute cmdButton33.VB_VarHelpID = -1
Private WithEvents cmdButton34 As fpBtn
Attribute cmdButton34.VB_VarHelpID = -1
Private WithEvents cmdButton35 As fpBtn
Attribute cmdButton35.VB_VarHelpID = -1

Private Sub cmdApply_Click()
On Error GoTo UI_ErrorHandler
Dim w As Integer, p As Integer
Dim TestStore As String
Dim ThePatient As String
Dim FileNum As Integer
Dim ButtonName As String, TextName As String
Dim ImageName As String, SliderName As String
Dim PostedResults As Boolean
If (Trim(StoreResults) = "") Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    InitialForm.ZOrder 0
    InitialForm.Enabled = True
    Unload frmClinicalEntry
    Exit Sub
End If
p = InStrPS(StoreResults, "_")
If (p = 0) Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    InitialForm.ZOrder 0
    InitialForm.Enabled = True
    Unload frmClinicalEntry
    Exit Sub
Else
    w = InStrPS(p, StoreResults, ".")
    If (w = 0) Then
        Set TheForm = Nothing
        Set TheControl = Nothing
        InitialForm.ZOrder 0
        InitialForm.Enabled = True
        Unload frmClinicalEntry
        Exit Sub
    End If
    ThePatient = Mid(StoreResults, p + 1, w - (p + 1))
End If
PostedResults = False
FileNum = FreeFile
FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
Print #FileNum, "QUESTION=" + "/" + Question
If (NotSure) Then
    PostedResults = True
    Print #FileNum, "NOTSURE=T" + Trim(Str(CurrentFormId)) + "<0>"
    FM.CloseFile CLng(FileNum)
Else
    For p = 1 To MaxButtons
        If (TheButtons(p)) Then
            ButtonName = TheButtonName(p) + "=T" + Trim(Str(TheButtonId(p))) + " <" + TheButtonNest(p) + ">"
            PostedResults = True
            Print #FileNum, ButtonName
        End If
    Next p
    FM.CloseFile CLng(FileNum)
    If Not (InitialForm.ChangeRequest) Then
        For p = 1 To MaxButtons
            If (TheButtons(p)) Then
                Call ButtonCheckLevel(p)
            End If
        Next p
    End If
End If
If Not (PostedResults) Then
    FM.Kill (StoreResults)
End If
Set TheForm = Nothing
Set TheControl = Nothing
InitialForm.Enabled = True
InitialForm.ZOrder 0
Unload frmClinicalEntry
FormOn = False
Exit Sub
UI_ErrorHandler:
    Set TheForm = Nothing
    Set TheControl = Nothing
    Resume LeaveFast
LeaveFast:
    InitialForm.Enabled = True
    InitialForm.ZOrder 0
    Unload frmClinicalEntry
    FormOn = False
End Sub

Private Sub cmdBack_Click()
If (CloseDown) Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    InitialForm.Enabled = True
    InitialForm.ZOrder 0
    Unload frmClinicalEntry
    FormOn = False
Else
    InitialForm.ChangeRequest = True
    Call cmdApply_Click
End If
End Sub

Private Sub cmdDontKnow_Click()
NotSure = Not (NotSure)
If (NotSure) Then
    cmdDontKnow.BackColor = ButtonSelectionColor
    cmdDontKnow.ForeColor = TextSelectionColor
    frmHelp.Language = Language
    frmHelp.HelpOption = False
    frmHelp.Show 1
    If (frmHelp.ExitFast) Then
        InitialForm.ExitFast = True
        InitialForm.CloseDown = True
        CloseDown = True
        Call cmdBack_Click
    Else
        Call cmdApply_Click
    End If
Else
    cmdDontKnow.BackColor = TheButtonColor(1)
    cmdDontKnow.ForeColor = TheButtonTextColor(1)
End If
End Sub

Private Sub cmdHelp_Click()
frmHelp.Language = Language
frmHelp.HelpOption = True
frmHelp.Show 1
If (frmHelp.ExitFast) Then
    InitialForm.ExitFast = True
    InitialForm.CloseDown = True
    CloseDown = True
    Call cmdBack_Click
End If
End Sub

Private Sub cmdNo_Click()
If (FM.IsFileThere(StoreResults)) Then
    FM.Kill StoreResults
End If
Set TheForm = Nothing
Set TheControl = Nothing
InitialForm.Enabled = True
InitialForm.ZOrder 0
Unload frmClinicalEntry
FormOn = False
End Sub

Private Sub TriggerSecondaryForm(TheFormId As Long, TheControlName As String, TheQuestion As String, OrderRef As String)
Dim Tick As Integer
If (Trim(TheControlName) <> "") And (TheFormId > 0) Then
    frmClinicalEntry2.Language = Language
    frmClinicalEntry2.ChangeOn = ChangeOn
    frmClinicalEntry2.StoreResults = StoreResults
    frmClinicalEntry2.LevelType = LevelType
    frmClinicalEntry2.Question = TheQuestion
    frmClinicalEntry2.QuestionFamily = QuestionFamily
    frmClinicalEntry2.FirstFormId = TheFormId
    frmClinicalEntry2.FirstControlName = TheControlName
    frmClinicalEntry2.FirstControlRef = TheControlName
    frmClinicalEntry2.FirstOrder = OrderRef
    frmClinicalEntry2.ButtonSelectionColor = ButtonSelectionColor
    frmClinicalEntry2.TextSelectionColor = TextSelectionColor
    frmClinicalEntry2.FormOn = True
    frmClinicalEntry.Enabled = False
    frmClinicalEntry2.Show
    frmClinicalEntry.Enabled = False
    frmClinicalEntry.Visible = False
    Tick = 0
    While (frmClinicalEntry2.FormOn) And Not (frmClinicalEntry2.CloseDown)
        Tick = Tick + 1
        If (Tick = EventCounter) Then
            DoEvents
            Tick = 0
        End If
    Wend
    frmClinicalEntry.Enabled = True
    frmClinicalEntry.Visible = True
    If (CloseDown) Then
        Call cmdBack_Click
    ElseIf (ChangeRequest) Then
        Call cmdBack_Click
    ElseIf (frmClinicalEntry2.NotSure) Or (frmClinicalEntry2.ExitFast) Then
        ExitFast = True
        Call cmdApply_Click
    End If
End If
End Sub

Private Function FindAlternateButton(AName As String) As Boolean
Dim k As Integer
FindAlternateButton = False
For k = 1 To MaxButtons
    If (AName = TheButtonName(k)) Then
        Call ApplyAlternateButton(k)
        FindAlternateButton = True
    End If
Next k
End Function

Private Sub ApplyAlternateButton(Counter As Integer)
    If (Counter = 1) Then
        With cmdButton1
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 2) Then
        With cmdButton2
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 3) Then
        With cmdButton3
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 4) Then
        With cmdButton4
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 5) Then
        With cmdButton5
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 6) Then
        With cmdButton6
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 7) Then
        With cmdButton7
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 8) Then
        With cmdButton8
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 9) Then
        With cmdButton9
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 10) Then
        With cmdButton10
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 11) Then
        With cmdButton11
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 12) Then
        With cmdButton12
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 13) Then
        With cmdButton13
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 14) Then
        With cmdButton14
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 15) Then
        With cmdButton15
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 16) Then
        With cmdButton16
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 17) Then
        With cmdButton17
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 18) Then
        With cmdButton18
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 19) Then
        With cmdButton19
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 20) Then
        With cmdButton20
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 21) Then
        With cmdButton21
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 22) Then
        With cmdButton22
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 23) Then
        With cmdButton23
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 24) Then
        With cmdButton24
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 25) Then
        With cmdButton25
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 26) Then
        With cmdButton26
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 27) Then
        With cmdButton27
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 28) Then
        With cmdButton28
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 29) Then
        With cmdButton29
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 30) Then
        With cmdButton30
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 31) Then
        With cmdButton31
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 32) Then
        With cmdButton32
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 33) Then
        With cmdButton33
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 34) Then
        With cmdButton34
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    ElseIf (Counter = 35) Then
        With cmdButton35
            If (TheButtonTriggerText(Counter) <> "") Then
                .Text = TheButtonTriggerText(Counter)
            End If
            .Visible = Not (.Visible)
        End With
    Else
        Exit Sub
    End If
End Sub

Private Sub ButtonCheckLevel(Ref As Integer)
Dim k As Integer
Dim AButton As fpBtn
Dim NestedQuestion As String
If (TheButtons(Ref)) Then
    Set TheSecondaryForm = New DynamicSecondaryForms
    TheSecondaryForm.FormId = CurrentFormId
    TheSecondaryForm.ControlName = TheButtonNest(Ref)
    TheSecondaryForm.QuestionOrder = ""
    If (TheSecondaryForm.RetrieveForm) Then
        k = 1
        While (TheSecondaryForm.SelectForm(k)) And Not (ExitFast)
            NestedQuestion = TheSecondaryForm.Question
            If (Trim(TheSecondaryForm.IEQuestion) <> "") Then
                NestedQuestion = TheSecondaryForm.IEQuestion
            End If
            Call TriggerSecondaryForm(CurrentFormId, TheButtonNest(Ref), NestedQuestion, TheSecondaryForm.QuestionOrder)
            k = k + 1
        Wend
' This implies that the selection was De-Referenced
        If (k < 1) Then
            If (LevelType = 2) Then
                AButton.BackColor = TheButtonColor(Ref)
            End If
            TheButtons(Ref) = False
        End If
    End If
    Set TheSecondaryForm = Nothing
End If
End Sub

Private Sub ButtonAction(AButton As fpBtn, Ref As Integer)
Dim k As Integer
If (TheButtonColor(Ref) = AButton.BackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = TheButtonColor(Ref)
    AButton.ForeColor = TheButtonTextColor(Ref)
End If
TheButtons(Ref) = Not (TheButtons(Ref))
cmdNo.Enabled = True
For k = 1 To MaxButtons
    If (TheButtons(k)) Then
        cmdNo.Enabled = False
        Exit For
    End If
Next k
' Determine if by selecting this Button Secondary Forms are triggered
If (LevelType = 2) Then
    Call ButtonCheckLevel(Ref)
End If
' Set the Process for Trigger Conditions
' The way the trigger works is a button will trigger
' other buttons that match this button's name.
If (TheButtons(Ref)) Then
    If (Trim(TheButtonTrigger(Ref)) <> "") Then
        If (FindAlternateButton(TheButtonName(Ref))) Then
            AButton.Visible = TheButtonVisible(Ref)
        End If
    End If
End If
' Check to see if single selection is on
If (TheButtons(Ref)) Then
    If Not (cmdApply.Visible) Then
        Call cmdApply_Click
    End If
End If
End Sub

Private Sub cmdButton1_Click()
Call ButtonAction(cmdButton1, 1)
End Sub

Private Sub cmdButton2_Click()
Call ButtonAction(cmdButton2, 2)
End Sub

Private Sub cmdButton3_Click()
Call ButtonAction(cmdButton3, 3)
End Sub

Private Sub cmdButton4_Click()
Call ButtonAction(cmdButton4, 4)
End Sub

Private Sub cmdButton5_Click()
Call ButtonAction(cmdButton5, 5)
End Sub

Private Sub cmdButton6_Click()
Call ButtonAction(cmdButton6, 6)
End Sub

Private Sub cmdButton7_Click()
Call ButtonAction(cmdButton7, 7)
End Sub

Private Sub cmdButton8_Click()
Call ButtonAction(cmdButton8, 8)
End Sub

Private Sub cmdButton9_Click()
Call ButtonAction(cmdButton9, 9)
End Sub

Private Sub cmdButton10_Click()
Call ButtonAction(cmdButton10, 10)
End Sub

Private Sub cmdButton11_Click()
Call ButtonAction(cmdButton11, 11)
End Sub

Private Sub cmdButton12_Click()
Call ButtonAction(cmdButton12, 12)
End Sub

Private Sub cmdButton13_Click()
Call ButtonAction(cmdButton13, 13)
End Sub

Private Sub cmdButton14_Click()
Call ButtonAction(cmdButton14, 14)
End Sub

Private Sub cmdButton15_Click()
Call ButtonAction(cmdButton15, 15)
End Sub

Private Sub cmdButton16_Click()
Call ButtonAction(cmdButton16, 16)
End Sub

Private Sub cmdButton17_Click()
Call ButtonAction(cmdButton17, 17)
End Sub

Private Sub cmdButton18_Click()
Call ButtonAction(cmdButton18, 18)
End Sub

Private Sub cmdButton19_Click()
Call ButtonAction(cmdButton19, 19)
End Sub

Private Sub cmdButton20_Click()
Call ButtonAction(cmdButton20, 20)
End Sub

Private Sub cmdButton21_Click()
Call ButtonAction(cmdButton21, 21)
End Sub

Private Sub cmdButton22_Click()
Call ButtonAction(cmdButton22, 22)
End Sub

Private Sub cmdButton23_Click()
Call ButtonAction(cmdButton23, 23)
End Sub

Private Sub cmdButton24_Click()
Call ButtonAction(cmdButton24, 24)
End Sub

Private Sub cmdButton25_Click()
Call ButtonAction(cmdButton25, 25)
End Sub

Private Sub cmdButton26_Click()
Call ButtonAction(cmdButton26, 26)
End Sub

Private Sub cmdButton27_Click()
Call ButtonAction(cmdButton27, 27)
End Sub

Private Sub cmdButton28_Click()
Call ButtonAction(cmdButton28, 28)
End Sub

Private Sub cmdButton29_Click()
Call ButtonAction(cmdButton29, 29)
End Sub

Private Sub cmdButton30_Click()
Call ButtonAction(cmdButton30, 30)
End Sub

Private Sub cmdButton31_Click()
Call ButtonAction(cmdButton31, 31)
End Sub

Private Sub cmdButton32_Click()
Call ButtonAction(cmdButton32, 32)
End Sub

Private Sub cmdButton33_Click()
Call ButtonAction(cmdButton33, 33)
End Sub

Private Sub cmdButton34_Click()
Call ButtonAction(cmdButton34, 34)
End Sub

Private Sub cmdButton35_Click()
Call ButtonAction(cmdButton35, 35)
End Sub

Private Sub SetButtonCharacteristics(RefControl As DynamicControls, AButton As fpBtn)
Dim q As Integer
Dim VisualText As String
With AButton
    If (Trim(RefControl.ControlTemplateName) <> "") Then
        .TemplateName = BtnDir + Trim(RefControl.ControlTemplateName) + ".btn"
        .Action = fpActionLoadTemplate
        .CellIndex = 2
        .SegmentApplyTo = fpSegmentApplyToButton
        .BackStyle = fpBackStyleTransparentGrayArea
        RefControl.BackColor = .BackColor
    Else
        .BackColor = RefControl.BackColor
    End If
    If (Trim(RefControl.ControlText) <> "") Then
        q = InStrPS(RefControl.ControlText, "^")
        If (q <> 0) Then
            VisualText = Left(RefControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(RefControl.ControlText, q + 1, Len(RefControl.ControlText) - q)
        Else
            VisualText = RefControl.ControlText
        End If
        .Text = VisualText
    Else
        .Text = RefControl.ControlName
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        .Tag = RefControl.IEChoiceName
    Else
        .Tag = RefControl.ControlName
    End If
    .ForeColor = RefControl.ForeColor
    .Left = RefControl.ControlLeft
    .Top = RefControl.ControlTop
    .Height = RefControl.ControlHeight
    .Width = RefControl.ControlWidth
    .TabIndex = RefControl.TabIndex
    .Visible = RefControl.ControlVisible
    .Enabled = True
    .Font.Size = RefControl.ResponseLength
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
    End If
    .FontBold = False
    .ToolTipText = ""
End With
End Sub

Private Sub PlantButton(RefControl As DynamicControls, Counter As Integer)
    Dim LibType As String
    LibType = "fpBtn.fpBtn.2"
    Counter = Counter + 1
    If (Counter = 1) Then
        Set cmdButton1 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton1)
    ElseIf (Counter = 2) Then
        Set cmdButton2 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton2)
    ElseIf (Counter = 3) Then
        Set cmdButton3 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton3)
    ElseIf (Counter = 4) Then
        Set cmdButton4 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton4)
    ElseIf (Counter = 5) Then
        Set cmdButton5 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton5)
    ElseIf (Counter = 6) Then
        Set cmdButton6 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton6)
    ElseIf (Counter = 7) Then
        Set cmdButton7 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton7)
    ElseIf (Counter = 8) Then
        Set cmdButton8 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton8)
    ElseIf (Counter = 9) Then
        Set cmdButton9 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton9)
    ElseIf (Counter = 10) Then
        Set cmdButton10 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton10)
    ElseIf (Counter = 11) Then
        Set cmdButton11 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton11)
    ElseIf (Counter = 12) Then
        Set cmdButton12 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton12)
    ElseIf (Counter = 13) Then
        Set cmdButton13 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton13)
    ElseIf (Counter = 14) Then
        Set cmdButton14 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton14)
    ElseIf (Counter = 15) Then
        Set cmdButton15 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton15)
    ElseIf (Counter = 16) Then
        Set cmdButton16 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton16)
    ElseIf (Counter = 17) Then
        Set cmdButton17 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton17)
    ElseIf (Counter = 18) Then
        Set cmdButton18 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton18)
    ElseIf (Counter = 19) Then
        Set cmdButton19 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton19)
    ElseIf (Counter = 20) Then
        Set cmdButton20 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton20)
    ElseIf (Counter = 21) Then
        Set cmdButton21 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton21)
    ElseIf (Counter = 22) Then
        Set cmdButton22 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton22)
    ElseIf (Counter = 23) Then
        Set cmdButton23 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton23)
    ElseIf (Counter = 24) Then
        Set cmdButton24 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton24)
    ElseIf (Counter = 25) Then
        Set cmdButton25 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton25)
    ElseIf (Counter = 26) Then
        Set cmdButton26 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton26)
    ElseIf (Counter = 27) Then
        Set cmdButton27 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton27)
    ElseIf (Counter = 28) Then
        Set cmdButton28 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton28)
    ElseIf (Counter = 29) Then
        Set cmdButton29 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton29)
    ElseIf (Counter = 30) Then
        Set cmdButton30 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton30)
    ElseIf (Counter = 31) Then
        Set cmdButton31 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton31)
    ElseIf (Counter = 32) Then
        Set cmdButton32 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton32)
    ElseIf (Counter = 33) Then
        Set cmdButton33 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton33)
    ElseIf (Counter = 34) Then
        Set cmdButton34 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton34)
    ElseIf (Counter = 35) Then
        Set cmdButton35 = frmClinicalEntry.Controls.Add(LibType, "cmdButton" + Trim(Str(Counter)))
        Call SetButtonCharacteristics(RefControl, cmdButton35)
    Else
        Exit Sub
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        TheButtonName(Counter) = Trim(RefControl.IEChoiceName)
    Else
        TheButtonName(Counter) = "*" + Trim(RefControl.ControlName)
    End If
    TheButtonNest(Counter) = Trim(RefControl.ControlName)
    TheButtonTrigger(Counter) = UCase(Trim(RefControl.ControlAlternateTrigger))
    TheButtonTriggerText(Counter) = UCase(Trim(RefControl.ControlAlternateText))
    TheButtonVisible(Counter) = RefControl.ControlVisibleAfterTrigger
    TheButtonColor(Counter) = RefControl.BackColor
    TheButtonTextColor(Counter) = RefControl.ForeColor
    TheButtons(Counter) = False
End Sub

Private Sub PlantLabel(RefControl As DynamicControls, Counter As Integer)
Dim VisualText As String
Dim q As Integer
    Counter = Counter + 1
    Set lblLabel = frmClinicalEntry.Controls.Add("VB.Label", "Label" + Trim(Str(Counter)))
    With lblLabel
        .Top = RefControl.ControlTop
        If (.Top < 1) Then
            .Top = 1
        End If
        .Left = RefControl.ControlLeft
        If (.Left < 1) Then
            .Left = 1
        End If
        If (RefControl.ControlWidth > 0) Then
            .AutoSize = False
            .Width = RefControl.ControlWidth
            If (.Left + .Width > frmClinicalEntry.Width) Then
                .Width = (frmClinicalEntry.Width - (.Left + 2))
            End If
            .Alignment = 2
        Else
            .AutoSize = True
        End If
        .Alignment = 2
        .WordWrap = True
        .BackColor = RefControl.BackColor
        .ForeColor = RefControl.ForeColor
        If (Trim(RefControl.ControlText) = "") Then
            .Caption = RefControl.ControlName
        Else
            q = InStrPS(RefControl.ControlText, "^")
            If (q = 0) Then
                VisualText = RefControl.ControlText
            Else
                VisualText = RefControl.ControlText
                While (q <> 0)
                    .AutoSize = True
                    VisualText = Left(VisualText, q - 1) + Chr(13) + Chr(10) + Mid(VisualText, q + 1, Len(VisualText) - q)
                    q = InStrPS(VisualText, "^")
                Wend
            End If
            .Caption = VisualText
        End If
        .TabIndex = RefControl.TabIndex
        .Visible = True
        .Enabled = True
        If (RefControl.ResponseLength > 100) Then
            .Height = RefControl.ResponseLength
        Else
            .Height = 24 * RefControl.ControlHeight
        End If
        .Font.Size = RefControl.ControlHeight
        If (Trim(RefControl.ControlFont) = "") Then
            .Font.Name = "Lucida Sans"
        Else
            .Font.Name = Trim(RefControl.ControlFont)
        End If
        .Font.Bold = True
    End With
End Sub

Private Function DynamicEntry(Question As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer
Dim MaxStatic As Integer
DynamicEntry = True
ExitFast = False
Set TheForm = New DynamicForms
Set TheControl = New DynamicControls
TheForm.Question = UCase(Trim(Question))
If (TheForm.RetrieveForm) And (TheForm.FormId > 0) Then
    CurrentFormId = TheForm.FormId
    FormColor = TheForm.BackColor
    If (QuestionFamily = 1) Or (QuestionFamily = 7) Then
        lblCurrentSymptom.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 2) Then
        lblLenses.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 3) Then
        lblEyeHistory.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 4) Or (QuestionFamily = 8) Or (QuestionFamily = 9) Then
        lblMedicalHistory.ForeColor = &HFFFFFF
    ElseIf (QuestionFamily = 5) Then
        lblFamilyHistory.ForeColor = &HFFFFFF
    Else
        If (BarOn) Then
            lblCurrentSymptom.Visible = BarOn
            lblLenses.Visible = BarOn
            lblEyeHistory.Visible = BarOn
            lblMedicalHistory.Visible = BarOn
            lblFamilyHistory.Visible = BarOn
        Else
            lblCurrentSymptom.Visible = False
            lblLenses.Visible = False
            lblEyeHistory.Visible = False
            lblMedicalHistory.Visible = False
            lblFamilyHistory.Visible = False
        End If
    End If
    frmClinicalEntry.BorderStyle = 0
    frmClinicalEntry.BackColor = TheForm.BackColor
    frmClinicalEntry.ForeColor = TheForm.ForeColor
    frmClinicalEntry.Caption = TheForm.FormName
    frmClinicalEntry.WindowState = TheForm.WindowState
    frmClinicalEntry.BorderStyle = 0
    frmClinicalEntry.Enabled = True
    If (TheForm.DontKnowButton) Then
        frmClinicalEntry.cmdDontKnow.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry.cmdDontKnow.Enabled = True
        frmClinicalEntry.cmdDontKnow.Visible = True
    Else
        frmClinicalEntry.cmdDontKnow.Enabled = False
        frmClinicalEntry.cmdDontKnow.Visible = False
    End If
    If (TheForm.ApplyButton) Then
        frmClinicalEntry.cmdApply.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry.cmdApply.Enabled = True
        frmClinicalEntry.cmdApply.Visible = True
    Else
        frmClinicalEntry.cmdApply.Enabled = False
        frmClinicalEntry.cmdApply.Visible = False
    End If
    If (TheForm.ExitButton) And Not (ChangeOn) Then
        frmClinicalEntry.cmdBack.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry.cmdBack.Enabled = True
        frmClinicalEntry.cmdBack.Visible = True
    Else
        frmClinicalEntry.cmdBack.Enabled = False
        frmClinicalEntry.cmdBack.Visible = False
    End If
    If (TheForm.NoToAllButton) Then
        frmClinicalEntry.cmdNo.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry.cmdNo.Enabled = True
        frmClinicalEntry.cmdNo.Visible = True
    Else
        frmClinicalEntry.cmdNo.Enabled = False
        frmClinicalEntry.cmdNo.Visible = False
    End If
    If (TheForm.HelpButton) Then
        frmClinicalEntry.cmdHelp.GrayAreaColor = frmClinicalEntry.BackColor
        frmClinicalEntry.cmdHelp.Enabled = True
        frmClinicalEntry.cmdHelp.Visible = True
    Else
        frmClinicalEntry.cmdHelp.Enabled = False
        frmClinicalEntry.cmdHelp.Visible = False
    End If

' Build the form's controls,
' They are different based on the Question that drives the Form
    TheControl.Language = Language
    TheControl.FormId = TheForm.FormId
    MaxControls = TheControl.FindControl
    If (MaxControls > 0) Then
        MaxLabel = 0
        MaxButtons = 0
        For i = 1 To MaxControls
            If (TheControl.SelectControl(i)) Then
                If (TheControl.ControlType = "L") Then
                    Call PlantLabel(TheControl, MaxLabel)
                ElseIf (TheControl.ControlType = "B") Then
                    Call PlantButton(TheControl, MaxButtons)
                    TheButtonId(MaxButtons) = TheControl.ControlId
                End If
            End If
        Next i
    End If
' Check for Static Label
    If (Trim(TheForm.StaticLabel) <> "") Then
        TheControl.FormId = 99999
        TheControl.ControlName = Trim(TheForm.StaticLabel)
        MaxStatic = TheControl.FindControl
        If (MaxStatic > 0) Then
            If (TheControl.SelectControl(1)) Then
                If (TheControl.ControlType = "L") Then
                    Call PlantLabel(TheControl, MaxLabel)
                End If
            End If
        End If
    End If
Else
    DynamicEntry = False
    ReloadForm = False
End If
If (ReloadForm) Then
    Call FormReLoad(StoreResults)
End If
If (DynamicEntry) Then
    frmClinicalEntry.Visible = True
End If
If (Trim(Question) = "PATIENT QUERY COMPLETE") Then

End If
cmdBack.Visible = frmHome.GetConfirmOn
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
    DynamicEntry = False
    Set TheForm = Nothing
    Set TheControl = Nothing
End Function

Private Sub Form_Load()
On Error GoTo UI_ErrorHandler
Dim MyFile As String
Dim FileNum As Integer
frmClinicalEntry.ZOrder 0
frmClinicalEntry.Visible = False
frmClinicalEntry.KeyPreview = True
If (Trim(DoneButtonText) <> "") Then
    cmdApply.Text = DoneButtonText
End If
If (Trim(ChangeButtonText) <> "") Then
    cmdBack.Text = ChangeButtonText
End If
If (Trim(NoToAllButtonText) <> "") Then
    cmdNo.Text = NoToAllButtonText
End If
If (Trim(HelpButtonText) <> "") Then
    cmdHelp.Text = HelpButtonText
End If
If (Trim(DontKnowButtonText) <> "") Then
    cmdDontKnow.Text = DontKnowButtonText
End If
NotSure = False
CloseDown = False
ChangeRequest = False
FormColor = 0
ReloadForm = False
If (Trim(StoreResults) <> "") Then
    MyFile = Dir(StoreResults)
    If (MyFile <> "") Then
        ReloadForm = True
    End If
End If
frmClinicalEntry.Visible = True
If Not (ReloadForm) Then
    FileNum = FreeFile
    FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    FM.CloseFile CLng(FileNum)
End If
frmClinicalEntry.MousePointer = 2
If Not (DynamicEntry(Question)) Then
    Unload frmClinicalEntry
    FM.Kill (StoreResults)
    InitialForm.Enabled = True
    FormOn = False
End If
Exit Sub
UI_ErrorHandler:
    If (Err = 53) Then
        Resume Next
    End If
    InitialForm.Enabled = True
    FormOn = False
    Unload frmClinicalEntry
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    InitialForm.ExitFast = True
    InitialForm.CloseDown = True
    CloseDown = True
    Call cmdBack_Click
Else
    frmHome.MyKey = frmHome.MyKey + 1
End If
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
frmHome.MyKey = frmHome.MyKey + 1
End Sub

Private Sub FormReLoad(FileName As String)
Dim Ref As Integer
Dim g, r, i As Integer
Dim FileNum As String
Dim TheRecord As String
Dim ButtonName As String
Dim ButtonNameRef As String
Dim NewQuestion As Boolean
NewQuestion = False
' You can only do a Reload if the Done Button is present
FileNum = FreeFile
If (cmdApply.Visible) Then
    FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
' Skip the first question
    Line Input #FileNum, TheRecord
    While (Not (EOF(FileNum)) And Not (NewQuestion))
        Line Input #FileNum, TheRecord
        If (Left(TheRecord, 9) = "QUESTION=") Then
            NewQuestion = True
        ElseIf (Left(TheRecord, 6) = "Recap=") Then
            NewQuestion = True
        ElseIf (InStrPS("?[]", Left(TheRecord, 1)) <> 0) Then
            NewQuestion = False
        Else
            r = InStrPS(TheRecord, "=")
            ButtonName = Left(TheRecord, r - 1)
            If (Mid(ButtonName, 1, 1) = "*") Then
                ButtonNameRef = Mid(ButtonName, 2, Len(ButtonName) - 1)
            Else
                ButtonNameRef = ButtonName
            End If
            For i = 0 To frmClinicalEntry.Controls.Count - 1
                If (UCase(Trim(frmClinicalEntry.Controls(i).Tag)) = UCase(Trim(ButtonNameRef))) Then
                    For g = 1 To MaxButtons
                        If (UCase(TheButtonName(g)) = UCase(ButtonName)) Then
                            Ref = g
                            Exit For
                        End If
                    Next g
                    If (Ref <> 0) Then
                        Call ButtonAction(frmClinicalEntry.Controls(i), Ref)
                    End If
                End If
            Next i
        End If
    Wend
    FM.CloseFile CLng(FileNum)
End If
' Initialize New Storage File
FM.OpenFile FileName, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
FM.CloseFile CLng(FileNum)
End Sub

Private Sub lblSwitch_Click()
Call Form_KeyPress(Val(vbKeyEscape))
End Sub
Public Sub FrmClose()
Unload Me
End Sub
