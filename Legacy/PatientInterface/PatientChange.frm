VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPatientChange 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11520
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   BeginProperty Font 
      Name            =   "Lucida Sans"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   ScaleHeight     =   11520
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1095
      Left            =   1080
      TabIndex        =   4
      Top             =   7680
      Width           =   7605
      _Version        =   131072
      _ExtentX        =   13414
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":0000
   End
   Begin VB.ListBox lstOrderRef 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "PatientChange.frx":0233
      Left            =   10440
      List            =   "PatientChange.frx":0235
      TabIndex        =   8
      Top             =   840
      Visible         =   0   'False
      Width           =   1455
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChangeButton2 
      Height          =   975
      Left            =   1058
      TabIndex        =   1
      Top             =   3264
      Width           =   9885
      _Version        =   131072
      _ExtentX        =   17436
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":0237
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChangeButton3 
      Height          =   975
      Left            =   1058
      TabIndex        =   2
      Top             =   4368
      Width           =   9885
      _Version        =   131072
      _ExtentX        =   17436
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":046A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChangeButton4 
      Height          =   975
      Left            =   1080
      TabIndex        =   3
      Top             =   5475
      Width           =   9885
      _Version        =   131072
      _ExtentX        =   17436
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":069D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChangeButton5 
      Height          =   975
      Left            =   1058
      TabIndex        =   5
      Top             =   6576
      Width           =   9885
      _Version        =   131072
      _ExtentX        =   17436
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":08D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChangeButton1 
      Height          =   975
      Left            =   1080
      TabIndex        =   0
      Top             =   2160
      Width           =   9885
      _Version        =   131072
      _ExtentX        =   17436
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":0B03
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1095
      Left            =   8760
      TabIndex        =   9
      Top             =   7680
      Width           =   2085
      _Version        =   131072
      _ExtentX        =   3678
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientChange.frx":0D36
   End
   Begin VB.Label lblSwitch 
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   11520
      TabIndex        =   10
      Top             =   480
      Visible         =   0   'False
      Width           =   180
   End
   Begin VB.Label lblLabel2 
      Alignment       =   2  'Center
      BackColor       =   &H008D312C&
      Caption         =   "(If no changes are needed, just press done)"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H005ED2F0&
      Height          =   855
      Left            =   4140
      TabIndex        =   7
      Top             =   1200
      Width           =   3615
   End
   Begin VB.Label lblLabel1 
      Alignment       =   2  'Center
      BackColor       =   &H008D312C&
      Caption         =   "Press the button with the answer you want to change"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   855
      Left            =   3593
      TabIndex        =   6
      Top             =   240
      Width           =   4815
   End
End
Attribute VB_Name = "frmPatientChange"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public ItemChosen As String
Public ChangeContent As Boolean
Public Language As String

Private Type ButtonDisplay
    Name As String * 64
    FileId As String * 8
End Type

Private CurrentIndex As Integer
Private TotalAvailableButtons As Integer
Private AvailableButtons(300) As ButtonDisplay

Private Sub cmdChangeButton1_Click()
    ItemChosen = cmdChangeButton1.Tag
    ChangeContent = True
    Unload frmPatientChange
End Sub

Private Sub cmdChangeButton2_Click()
    ItemChosen = cmdChangeButton2.Tag
    ChangeContent = True
    Unload frmPatientChange
End Sub

Private Sub cmdChangeButton3_Click()
    ItemChosen = cmdChangeButton3.Tag
    ChangeContent = True
    Unload frmPatientChange
End Sub

Private Sub cmdChangeButton4_Click()
    ItemChosen = cmdChangeButton4.Tag
    ChangeContent = True
    Unload frmPatientChange
End Sub

Private Sub cmdChangeButton5_Click()
    ItemChosen = cmdChangeButton5.Tag
    ChangeContent = True
    Unload frmPatientChange
End Sub

Private Sub cmdDone_Click()
ItemChosen = ""
ChangeContent = False
Unload frmPatientChange
End Sub

Private Sub cmdMore_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > TotalAvailableButtons) Then
    CurrentIndex = 1
End If
Call LoadButtons
End Sub

Private Sub LoadButtons()
Dim p As Integer
Dim ButtonCnt As Integer
p = CurrentIndex
ButtonCnt = 1
cmdChangeButton1.Visible = False
cmdChangeButton2.Visible = False
cmdChangeButton3.Visible = False
cmdChangeButton4.Visible = False
cmdChangeButton5.Visible = False
While (ButtonCnt < 6) And (p <= TotalAvailableButtons)
    If (ButtonCnt = 1) Then
        cmdChangeButton1.Text = AvailableButtons(p).Name
        cmdChangeButton1.Tag = AvailableButtons(p).FileId
        cmdChangeButton1.Visible = True
    ElseIf (ButtonCnt = 2) Then
        cmdChangeButton2.Text = AvailableButtons(p).Name
        cmdChangeButton2.Tag = AvailableButtons(p).FileId
        cmdChangeButton2.Visible = True
    ElseIf (ButtonCnt = 3) Then
        cmdChangeButton3.Text = AvailableButtons(p).Name
        cmdChangeButton3.Tag = AvailableButtons(p).FileId
        cmdChangeButton3.Visible = True
    ElseIf (ButtonCnt = 4) Then
        cmdChangeButton4.Text = AvailableButtons(p).Name
        cmdChangeButton4.Tag = AvailableButtons(p).FileId
        cmdChangeButton4.Visible = True
    ElseIf (ButtonCnt = 5) Then
        cmdChangeButton5.Text = AvailableButtons(p).Name
        cmdChangeButton5.Tag = AvailableButtons(p).FileId
        cmdChangeButton5.Visible = True
    End If
    p = p + 1
    ButtonCnt = ButtonCnt + 1
Wend
CurrentIndex = p - 1
If (TotalAvailableButtons > 5) Then
    cmdMore.Visible = True
    If (CurrentIndex >= TotalAvailableButtons) Then
        cmdMore.Text = "Start of List"
        cmdDone.Visible = True
    Else
        cmdMore.Text = "More"
        cmdDone.Visible = False
    End If
Else
    cmdMore.Visible = False
    cmdDone.Visible = True
End If
End Sub

Public Function LoadPatientChangeDisplay() As Boolean
Dim i As Integer
Dim TheFile As String
Dim TheRecord As String
Dim FileNum As Integer
Dim RecapDisplay As String
LoadPatientChangeDisplay = False
TotalAvailableButtons = 0
For i = 1 To 300
    AvailableButtons(i).Name = ""
    AvailableButtons(i).FileId = ""
Next i
FileNum = FreeFile
For i = 0 To lstOrderRef.ListCount - 1
    TheFile = PatientInterfaceDirectory + "P" + lstOrderRef.List(i) + "_" + Trim(Str(PatientId)) + ".txt"
    If (FM.IsFileThere(TheFile)) Then
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
        Do Until (EOF(FileNum)) Or (LOF(FileNum) < 3)
            Line Input #FileNum, TheRecord
            If (Left(TheRecord, 6) = "Recap=") Then
                RecapDisplay = Mid(TheRecord, 7, Len(TheRecord) - 6)
                Call StripCharacters(RecapDisplay, "*")
                If (InStrPS(UCase(TheRecord), "NOT SURE") = 0) Then
                    TotalAvailableButtons = TotalAvailableButtons + 1
                    AvailableButtons(TotalAvailableButtons).Name = RecapDisplay
                    AvailableButtons(TotalAvailableButtons).FileId = lstOrderRef.List(i)
                End If
            End If
        Loop
        FM.CloseFile CLng(FileNum)
    End If
Next i
If (TotalAvailableButtons > 0) Then
    CurrentIndex = 1
    Call LoadButtons
    LoadPatientChangeDisplay = True
End If
'Call TriggerNewLanguage(Language)
End Function

Private Function TriggerNewLanguage(Lang As String) As Boolean
Dim i As Integer
Dim CId As Long
Dim CText As String
Dim CLingo As String
Dim CTag As String
TriggerNewLanguage = True
For i = 0 To frmPatientChange.Controls.Count - 1
    If (frmHome.SetControlLanguage(frmPatientChange.Controls(i).Name, -2, Lang, CId, CText, CLingo, CTag)) Then
        frmPatientChange.Controls(i).Visible = True
        If (Trim(CText) <> "") Then
            If (Left(frmPatientChange.Controls(i).Name, 3) = "lbl") Then
                frmPatientChange.Controls(i).Caption = CText
            Else
                frmPatientChange.Controls(i).Text = CText
            End If
        End If
        frmPatientChange.Controls(i).Tag = Trim(Str(CId))
        If (Lang = "English") Then
            frmPatientChange.Controls(i).ToolTipText = Trim(CText)
        End If
    End If
Next i
Language = Lang
End Function

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    frmHome.ExitFast = True
    frmHome.CloseDown = True
    Unload frmPatientChange
End If
End Sub

Private Sub lblSwitch_Click()
Call Form_KeyPress(Val(vbKeyEscape))
End Sub
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub


