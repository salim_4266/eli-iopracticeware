VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmPatientRecap 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstOrderRef 
      Height          =   1425
      ItemData        =   "PatientRecap.frx":0000
      Left            =   10200
      List            =   "PatientRecap.frx":0002
      TabIndex        =   5
      Top             =   7320
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtComplaints 
      BackColor       =   &H00999900&
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFFF&
      Height          =   5775
      Left            =   360
      MaxLength       =   1024
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   840
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdYes 
      Height          =   1095
      Left            =   3120
      TabIndex        =   2
      Top             =   7440
      Width           =   1845
      _Version        =   131072
      _ExtentX        =   3254
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientRecap.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNo 
      Height          =   1095
      Left            =   7560
      TabIndex        =   3
      Top             =   7440
      Width           =   1725
      _Version        =   131072
      _ExtentX        =   3043
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientRecap.frx":023A
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "Is there anything that you want to change ?"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFFF&
      Height          =   465
      Left            =   1440
      TabIndex        =   1
      Top             =   6720
      Width           =   9390
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "This is what you told us about yourself"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFFF&
      Height          =   465
      Left            =   1920
      TabIndex        =   0
      Top             =   240
      Width           =   8475
   End
End
Attribute VB_Name = "frmPatientRecap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ChangeContent As Boolean

Private Sub cmdNo_Click()
ChangeContent = False
frmPatientRecap.Hide
End Sub

Private Sub cmdYes_Click()
ChangeContent = True
frmPatientRecap.Hide
End Sub
