VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmPatientMonth 
   BackColor       =   &H008D312C&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11520
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   ForeColor       =   &H008D312C&
   LinkTopic       =   "Form1"
   ScaleHeight     =   11520
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdJan 
      Height          =   1095
      Left            =   1373
      TabIndex        =   0
      Top             =   1440
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFeb 
      Height          =   1095
      Left            =   1373
      TabIndex        =   1
      Top             =   3000
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":01E2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMar 
      Height          =   1095
      Left            =   1373
      TabIndex        =   2
      Top             =   4560
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":03C5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApr 
      Height          =   1095
      Left            =   1373
      TabIndex        =   3
      Top             =   6120
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":05A5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMay 
      Height          =   1095
      Left            =   4733
      TabIndex        =   4
      Top             =   1440
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":0785
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdJun 
      Height          =   1095
      Left            =   4733
      TabIndex        =   5
      Top             =   3000
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":0963
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdJul 
      Height          =   1095
      Left            =   4733
      TabIndex        =   6
      Top             =   4560
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":0B42
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAug 
      Height          =   1095
      Left            =   4733
      TabIndex        =   7
      Top             =   6120
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":0D21
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSep 
      Height          =   1095
      Left            =   8093
      TabIndex        =   8
      Top             =   1440
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":0F02
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOct 
      Height          =   1095
      Left            =   8093
      TabIndex        =   9
      Top             =   3000
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":10E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNov 
      Height          =   1095
      Left            =   8093
      TabIndex        =   10
      Top             =   4560
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":12C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDec 
      Height          =   1095
      Left            =   8093
      TabIndex        =   11
      Top             =   6120
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":14AB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHelp 
      Height          =   1335
      Left            =   5198
      TabIndex        =   13
      Top             =   7440
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   9253164
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientMonth.frx":168E
   End
   Begin VB.Label lblLabel1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H008D312C&
      Caption         =   "Please enter the MONTH of your birth"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00B6EEFC&
      Height          =   480
      Left            =   2168
      TabIndex        =   12
      Top             =   480
      Width           =   7665
   End
End
Attribute VB_Name = "frmPatientMonth"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ExitFast As Boolean
Public TheMonthName As String
Public TheMonth As String
Public Language As String

Private Sub cmdHelp_Click()
frmHelp.Language = Language
frmHelp.HelpOption = True
frmHelp.Show 1
If (frmHelp.ExitFast) Then
    ExitFast = True
    Unload frmPatientMonth
End If
End Sub

Private Sub cmdJan_Click()
    TheMonth = "01"
    TheMonthName = cmdJan.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdFeb_Click()
    TheMonth = "02"
    TheMonthName = cmdFeb.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdMar_Click()
    TheMonth = "03"
    TheMonthName = cmdMar.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdApr_Click()
    TheMonth = "04"
    TheMonthName = cmdApr.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdMay_Click()
    TheMonth = "05"
    TheMonthName = cmdMay.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdJun_Click()
    TheMonth = "06"
    TheMonthName = cmdJun.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdJul_Click()
    TheMonth = "07"
    TheMonthName = cmdJul.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdAug_Click()
    TheMonth = "08"
    TheMonthName = cmdAug.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdSep_Click()
    TheMonth = "09"
    TheMonthName = cmdSep.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdOct_Click()
    TheMonth = "10"
    TheMonthName = cmdOct.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdNov_Click()
    TheMonth = "11"
    TheMonthName = cmdNov.Text
    Unload frmPatientMonth
End Sub

Private Sub cmdDec_Click()
    TheMonth = "12"
    TheMonthName = cmdDec.Text
    Unload frmPatientMonth
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (KeyAscii = vbKeyEscape) Then
    ExitFast = True
    Unload frmPatientMonth
End If
End Sub

Private Sub Form_Load()
ExitFast = False
TheMonth = ""
TheMonthName = ""
frmPatientMonth.KeyPreview = True
Call TriggerNewLanguage(Language)
End Sub

Private Function TriggerNewLanguage(Lang As String) As Boolean
Dim i As Integer
Dim CId As Long
Dim CTag As String
Dim CText As String
Dim CLingo As String
TriggerNewLanguage = True
For i = 0 To frmPatientMonth.Controls.Count - 1
    If (frmHome.SetControlLanguage(frmPatientMonth.Controls(i).Name, -2, Lang, CId, CText, CLingo, CTag)) Then
        frmPatientMonth.Controls(i).Visible = True
        If (Trim(CText) <> "") Then
            If (Left(frmPatientMonth.Controls(i).Name, 3) = "lbl") Then
                frmPatientMonth.Controls(i).Caption = CText
            Else
                frmPatientMonth.Controls(i).Text = CText
            End If
        End If
        frmPatientMonth.Controls(i).Tag = Trim(Str(CId))
        If (Lang = "English") Then
            frmPatientMonth.Controls(i).ToolTipText = Trim(CTag)
        End If
    End If
Next i
Language = Lang
End Function
Public Sub FrmClose()
Unload Me
End Sub

