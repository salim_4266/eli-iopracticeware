Attribute VB_Name = "ARK530A"
Option Explicit

Private ArrayMark As Integer

Public Function ParseARK530AOutput(StringToParse As String, numTests As Integer) As String()
Dim OutputArray(50, 3) As String
Dim TestMark As Integer
Dim i As Integer, j As Integer
Dim tempSubParse As String
Dim RMdone As Boolean
Dim KMdone As Boolean
RMdone = False
KMdone = False
ArrayMark = 0
TestMark = 0
For i = 1 To Len(StringToParse)
    If (Mid$(StringToParse, i, 3) = "DRM") Then
        GoSub ParseRM
    ElseIf (Mid$(StringToParse, i, 3) = "DKM") Then
        GoSub ParseKM
    End If
Next i

ParseARK530AOutput = OutputArray
numTests = TestMark
Exit Function

' Each of these parsing routines grabs the data for a specific test and creates
' a 3 column array with control tag (ICDAlias4 in DF), OD data and OS data.
ParseRM:
    'Autorefractor element
    TestMark = TestMark + 1
    tempSubParse = StringToParse
    'SCA
    If (InStr(1, tempSubParse, "OR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "OL", vbBinaryCompare) <> 0) Then
        ' Add the Sphere
        j = InStr(1, tempSubParse, "OR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMSPH"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 6)
        j = InStr(1, tempSubParse, "OL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 6)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Cylinder
        j = InStr(1, tempSubParse, "OR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMCYL"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 8, 6)
        j = InStr(1, tempSubParse, "OL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 8, 6)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Axis
        j = InStr(1, tempSubParse, "OR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMAXI"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 14, 3)
        j = InStr(1, tempSubParse, "OL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 14, 3)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    ElseIf (InStr(1, tempSubParse, "OR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "OL", vbBinaryCompare) = 0) Then
        ' Add the Sphere
        j = InStr(1, tempSubParse, "OR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMSPH"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 6)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Cylinder
        j = InStr(1, tempSubParse, "OR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMCYL"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 8, 6)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Axis
        j = InStr(1, tempSubParse, "OR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMAXI"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 14, 3)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    ElseIf (InStr(1, tempSubParse, "OR", vbBinaryCompare) = 0 And InStr(1, tempSubParse, "OL", vbBinaryCompare) <> 0) Then
        ' Add the Sphere
        j = InStr(1, tempSubParse, "OL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMSPH"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 6)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Cylinder
        j = InStr(1, tempSubParse, "OL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMCYL"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 8, 6)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Axis
        j = InStr(1, tempSubParse, "OL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMAXI"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 14, 3)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    End If
    ' Visual Acuity (right)
    If (InStr(1, tempSubParse, "VR", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "VR", vbBinaryCompare)
        Select Case Trim(Mid$(tempSubParse, j + 2, 5))
            Case "15"
                OutputArray(ArrayMark, 0) = "RM15"
            Case "20"
                OutputArray(ArrayMark, 0) = "RM20"
            Case "25"
                OutputArray(ArrayMark, 0) = "RM25"
            Case "30"
                OutputArray(ArrayMark, 0) = "RM30"
            Case "40"
                OutputArray(ArrayMark, 0) = "RM40"
            Case "50"
                OutputArray(ArrayMark, 0) = "RM50"
            Case "60"
                OutputArray(ArrayMark, 0) = "RM60"
            Case "70"
                OutputArray(ArrayMark, 0) = "RM70"
            Case "80"
                OutputArray(ArrayMark, 0) = "RM80"
            Case "100"
                OutputArray(ArrayMark, 0) = "RM100"
            Case "150"
                OutputArray(ArrayMark, 0) = "RM150"
            Case "200"
                OutputArray(ArrayMark, 0) = "RM200"
            Case "400"
                OutputArray(ArrayMark, 0) = "RM400"
            Case "800"
                OutputArray(ArrayMark, 0) = "RM800"
            Case "FC1"
                OutputArray(ArrayMark, 0) = "RMFC1"
            Case "FC3"
                OutputArray(ArrayMark, 0) = "RMFC3"
            Case "HM"
                OutputArray(ArrayMark, 0) = "RMHM"
            Case "ERR"
                OutputArray(ArrayMark, 0) = "RMERR"
        End Select
        OutputArray(ArrayMark, 1) = "1"
        OutputArray(ArrayMark, 2) = "0"
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    End If
    ' Visual Acuity (left)
    If (InStr(1, tempSubParse, "VL", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "VL", vbBinaryCompare)
        Select Case Trim(Mid$(tempSubParse, j + 2, 5))
            Case "15"
                OutputArray(ArrayMark, 0) = "RM15"
            Case "20"
                OutputArray(ArrayMark, 0) = "RM20"
            Case "25"
                OutputArray(ArrayMark, 0) = "RM25"
            Case "30"
                OutputArray(ArrayMark, 0) = "RM30"
            Case "40"
                OutputArray(ArrayMark, 0) = "RM40"
            Case "50"
                OutputArray(ArrayMark, 0) = "RM50"
            Case "60"
                OutputArray(ArrayMark, 0) = "RM60"
            Case "70"
                OutputArray(ArrayMark, 0) = "RM70"
            Case "80"
                OutputArray(ArrayMark, 0) = "RM80"
            Case "100"
                OutputArray(ArrayMark, 0) = "RM100"
            Case "150"
                OutputArray(ArrayMark, 0) = "RM150"
            Case "200"
                OutputArray(ArrayMark, 0) = "RM200"
            Case "400"
                OutputArray(ArrayMark, 0) = "RM400"
            Case "800"
                OutputArray(ArrayMark, 0) = "RM800"
            Case "FC1"
                OutputArray(ArrayMark, 0) = "RMFC1"
            Case "FC3"
                OutputArray(ArrayMark, 0) = "RMFC3"
            Case "HM"
                OutputArray(ArrayMark, 0) = "RMHM"
            Case "ERR"
                OutputArray(ArrayMark, 0) = "RMERR"
        End Select
        OutputArray(ArrayMark, 1) = "0"
        OutputArray(ArrayMark, 2) = "1"
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    End If
    ' PD
    If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMPD"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 2)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
    End If
    If Not (RMdone) Then
        TestMark = TestMark - 1
    End If
    Return
    
ParseKM:
    ' Autokeratometry element
    TestMark = TestMark + 1
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) + 1)
    
'    'mm Data
'    If (InStr(1, tempSubParse, " R", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, " L", vbBinaryCompare)) Then
'        ' R1
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMR1"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 5)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 5)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' R2
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMR2"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 7, 5)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 7, 5)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Axis
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 12, 3)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 12, 3)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Ave
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAVE"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 15, 5)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 15, 5)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        KMdone = True
'    ElseIf (InStr(1, tempSubParse, " R", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, " L", vbBinaryCompare) = 0) Then
'        ' R1
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMR1"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 5)
'        OutputArray(ArrayMark, 2) = ""
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' R2
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMR2"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 7, 5)
'        OutputArray(ArrayMark, 2) = ""
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Axis
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 12, 3)
'        OutputArray(ArrayMark, 2) = ""
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Ave
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAVE"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 15, 5)
'        OutputArray(ArrayMark, 2) = ""
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        KMdone = True
'    ElseIf (InStr(1, tempSubParse, " R", vbBinaryCompare) = 0 And InStr(1, tempSubParse, " L", vbBinaryCompare) <> 0) Then
'        ' R1
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMR1"
'        OutputArray(ArrayMark, 1) = ""
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 5)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' R2
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMR2"
'        OutputArray(ArrayMark, 1) = ""
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 7, 5)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Axis
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        OutputArray(ArrayMark, 1) = ""
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 12, 3)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Ave
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAVE"
'        OutputArray(ArrayMark, 1) = ""
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 15, 5)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        KMdone = True
'    End If

    'D data
    If (InStr(1, tempSubParse, "DR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "DL", vbBinaryCompare)) Then
        ' R1
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR1"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 5)
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 5)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' R2
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR2"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 7, 5)
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 7, 5)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Axis
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAXI"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 12, 3)
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 12, 3)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Ave
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAVE"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 15, 5)
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 15, 5)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        KMdone = True
    ElseIf (InStr(1, tempSubParse, "DR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "DL", vbBinaryCompare) = 0) Then
        ' R1
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR1"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 5)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' R2
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR2"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 7, 5)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Axis
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAXI"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 12, 3)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Ave
        j = InStr(1, tempSubParse, "DR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAVE"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 15, 5)
        OutputArray(ArrayMark, 2) = ""
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        KMdone = True
    ElseIf (InStr(1, tempSubParse, "DR", vbBinaryCompare) = 0 And InStr(1, tempSubParse, "DL", vbBinaryCompare) <> 0) Then
        ' R1
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR1"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 5)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' R2
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR2"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 7, 5)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Axis
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAXI"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 12, 3)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Ave
        j = InStr(1, tempSubParse, "DL", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAVE"
        OutputArray(ArrayMark, 1) = ""
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 15, 5)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        KMdone = True
    End If
    
    If Not (KMdone) Then
        TestMark = TestMark - 1
    End If
    Return
    
End Function

