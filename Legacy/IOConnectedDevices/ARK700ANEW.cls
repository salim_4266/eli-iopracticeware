VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CARK700ANEW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice

Private oResults As CDeviceResults
Const strTestDelimiter1    As String = "R S"
Const strTestDelimiter2    As String = "R1"

Public Function IDevice_ParseMachineOutput(StringToParse As String, _
                                   numTests As Integer) As String()

Dim i      As Integer   'Counter for characters in string
Dim RMdone As Boolean   'Autorefractometer  (Objective data)
Dim KMdone As Boolean   'Keratometer

    On Error GoTo lIDevice_ParseMachineOutput_Error

    Set oResults = New CDeviceResults

    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 3) = strTestDelimiter1) Then
            RMdone = ParseRM(StringToParse, i)
            If Not RMdone Then oResults.PrevTest
        ElseIf (Mid$(StringToParse, i, 2) = strTestDelimiter2) Then
            KMdone = ParseKM(StringToParse, i)
            If Not KMdone Then oResults.PrevTest
        End If
    Next i
    
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CARK700ANEW", "IDevice_ParseMachineOutput", Err, Err.Description

End Function

Public Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean

    Dim iR As Long                     'Position of indicator for Right Eye
    Dim iL As Long                     'Position of indicator for Left Eye
    Dim tempSubParse As String

    '    Machine specific constants
    Const sSCAR As String = "R S"          'SCA right eye
    Const sSCAL As String = "L S"          'SCA left eye

    On Error GoTo lParseRM_Error

    ParseRM = False
    oResults.NextTest
    
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)


    ' SCA
    iR = InStr(1, tempSubParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, tempSubParse, sSCAL, vbBinaryCompare)
    
    If iR + iL > 0 Then
        ' Add the Sphere
         oResults.AddValues "RMSPH", tempSubParse, iR, iL, 3, 8

        ' Add the Cylinder
         oResults.AddValues "RMCYL", tempSubParse, iR, iL, 13, 8

        ' Add the Axis
         oResults.AddValues "RMAXI", tempSubParse, iR, iL, 23, 6

        ParseRM = True
    End If
    Exit Function

lParseRM_Error:

    LogError "CARK700ANEW", "ParseLM", Err, Err.Description

End Function

Public Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean

    On Error GoTo lParseKM_Error


    Dim intR As Long                 'Position of indicator for Right Eye
    Dim intL As Long                 'Position of indicator for Left Eye
    Dim tempSubParse As String
    Dim j As Integer

    '    Machine specific constants
    Const strR1 As String = "R1"

    ParseKM = False
    oResults.NextTest
    
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    
    'There is no acutal char to differentiate R and L.Consider 'R1' as delimiter and
    'start R from 1 and L from 2 so that it exculdes R.
    intR = InStr(1, tempSubParse, strR1, vbBinaryCompare)
    intL = InStr(2, tempSubParse, strR1, vbBinaryCompare)

    If intR > 0 And intL > 0 Then
        'R1
        oResults.AddValues "KMR1", tempSubParse, intR, intL, 2, 8


        oResults.AddValues "KMAXI", tempSubParse, intR, intL, 42, 6

        'R2
        oResults.AddValues "KMR2", tempSubParse, intR, intL, 12, 8
        

        oResults.AddValues "KMCYL", tempSubParse, intR, intL, 52, 6


        oResults.AddValues "KMAVE", tempSubParse, intR, intL, 72, 6

        ParseKM = True
    End If
    Exit Function
    
lParseKM_Error:
    LogError "CARK700ANEW", "ParseKM", Err, Err.Description
End Function
