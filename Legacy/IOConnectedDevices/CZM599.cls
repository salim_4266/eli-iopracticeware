VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCZM599"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice

Private oResults As CDeviceResults
Const strTestDelimiter    As String = vbCrLf & vbCrLf   'Chr(1)

Private Function ParseKM(ByVal StringToParse As String, _
                        ByVal i As Integer) As Boolean

Dim intR         As Long  'Position of indicator for Right Eye
Dim intL         As Long  'Position of indicator for Left Eye
Dim tempSubParse As String
Dim intAVER      As Long
Dim intAVEL      As Long
Const strDioR    As String = "R"   'diopter data right eye
Const strDioL    As String = "L"   'diopter data left eye

    On Error GoTo lParseKM_Error

    LogError "CCZM599", "ParseKM", 0, "", "Parsing KM Data"

    ParseKM = False
    oResults.NextTest
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    
    ' D data
    intR = InStr(1, tempSubParse, strDioR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strDioL, vbBinaryCompare)
    If intR + intL > 0 Then
        ' R1
        oResults.AddValues "KMR1", tempSubParse, intR, intL, 4, 5
        ' Axis
        oResults.AddValues "KMAXI", tempSubParse, intR, intL, 19, 3
        ' R2
        oResults.AddValues "KMR2", tempSubParse, intR, intL, 28, 5
        ParseKM = True
    End If

    ' AVE
    If intR > 0 Then
        intAVER = InStr(intR + 1, tempSubParse, "AVG", vbBinaryCompare)
    End If
    If intL > 0 Then
        intAVEL = InStr(intL + 1, tempSubParse, "AVG", vbBinaryCompare)
    End If
    If intAVER + intAVEL > 0 Then
        If intAVER = intAVEL Then
            intAVER = 0
        End If
        oResults.AddValues "KMAVE", tempSubParse, intAVER, intAVEL, 8, 5
        ParseKM = True
    End If

    Exit Function

lParseKM_Error:

    LogError "CCZM599", "ParseKM", Err, Err.Description

End Function

Public Function IDevice_ParseMachineOutput(StringToParse As String, _
                                   numTests As Integer) As String()

Dim i      As Integer   'Counter for characters in string
Dim RMdone As Boolean   'Autorefractometer  (Objective data)
Dim KMdone As Boolean   'Keratometer

    On Error GoTo lIDevice_ParseMachineOutput_Error
    Set oResults = New CDeviceResults

    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, Len(strTestDelimiter)) = strTestDelimiter) Then
            Select Case Mid$(StringToParse, i + Len(strTestDelimiter), 3)
                Case "Sph"
                    RMdone = ParseRM(StringToParse, i)
                    If Not RMdone Then
                        oResults.PrevTest
                    End If
                Case "Ker"
                    KMdone = ParseKM(StringToParse, i)
                    If Not KMdone Then
                        oResults.PrevTest
                    End If
            End Select
        End If
    Next i
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CCZM599", "IDevice_ParseMachineOutput", Err, Err.Description

End Function

Private Function ParseRM(ByVal StringToParse As String, _
                        ByVal i As Integer) As Boolean

Dim intR         As Long                'Position of indicator for Right Eye
Dim intL         As Long                'Position of indicator for Left Eye
Dim intCR        As Long                'Position of end of line
Dim tempSubParse As String
Dim sName As String

Const strObjSCAR As String = "RIGHT EYE"   'objective SCA right eye
Const strObjSCAL As String = "LEFT EYE"    'objective SCA left eye
Const strPD      As String = "PD"          'pupil distance
    On Error GoTo lParseRM_Error

    LogError "CCZM599", "ParseRM", 0, "", "Parsing RM Data"
    ParseRM = False
    oResults.NextTest
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    'SCA
    intR = InStr(1, tempSubParse, strObjSCAR, vbBinaryCompare)
    If intR > 0 Then
        intR = InStr(intR + 1, tempSubParse, "Obj", vbBinaryCompare)
    End If
    intL = InStr(1, tempSubParse, strObjSCAL, vbBinaryCompare)
    If intL > 0 Then
        intL = InStr(intL + 1, tempSubParse, "Obj", vbBinaryCompare)
    End If
    If intR + intL > 0 Then
        ' Add the Sphere
        oResults.AddValues "RMSPH", tempSubParse, intR, intL, 5, 6
        ' Add the Cylinder
        oResults.AddValues "RMCYL", tempSubParse, intR, intL, 12, 6
        ' Add the Axis
        oResults.AddValues "RMAXI", tempSubParse, intR, intL, 19, 3
        ParseRM = True
    End If
    
    ' Visual Acuity (right)
    sName = PickVAButton(tempSubParse, intR)
    If sName <> "" Then
        oResults.AddValues sName, "10", 1, 2, 0, 1
        ParseRM = True
    End If
    ' Visual Acuity (left)
    sName = PickVAButton(tempSubParse, intL)
    If sName <> "" Then
        oResults.AddValues sName, "01", 1, 2, 0, 1
        ParseRM = True
    End If
    
    ' PD
    intR = InStr(1, tempSubParse, strPD, vbBinaryCompare)
    intCR = InStr(intR + 1, tempSubParse, vbNewLine, vbBinaryCompare)
    If intR > 0 Then
        If intR < intCR Then
            oResults.AddValues "RMPD", tempSubParse, intR, 0, 9, 2
            ParseRM = True
        End If
    End If

    Exit Function

lParseRM_Error:

    LogError "CCZM599", "ParseRM", Err, Err.Description

End Function

Private Function PickVAButton(sToParse As String, iPos As Long) As String
Dim sName As String
    sName = ""
    If Mid$(sToParse, iPos + 22, 4) <> strTestDelimiter Then
        Select Case Trim$(Mid$(sToParse, iPos + 26, InStr(iPos + 26, sToParse, vbCrLf) - iPos - 26))
            Case "15"
                sName = "RM15"
            Case "20"
                sName = "RM20"
            Case "25"
                sName = "RM25"
            Case "30"
                sName = "RM30"
            Case "40"
                sName = "RM40"
            Case "50"
                sName = "RM50"
            Case "60"
                sName = "RM60"
            Case "70"
                sName = "RM70"
            Case "80"
                sName = "RM80"
            Case "100"
                sName = "RM100"
            Case "150"
                sName = "RM150"
            Case "200"
                sName = "RM200"
            Case "400"
                sName = "RM400"
            Case "800"
                sName = "RM800"
        End Select
    End If
    PickVAButton = sName
End Function


