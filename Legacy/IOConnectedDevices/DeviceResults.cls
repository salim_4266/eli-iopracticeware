VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDeviceResults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private miTestMark As Long
Private miArrayMark As Long
Private masOutput() As String

Public Sub AddValues(sKeyName As String, sParseInput As String, iStartRight As Long, iStartLeft As Long, iOffset As Long, iLength As Long)

    On Error GoTo lAddValues_Error
    
    masOutput(miArrayMark, 0) = sKeyName
    
    If iStartRight > 0 Then
        masOutput(miArrayMark, 1) = Mid$(sParseInput, iStartRight + iOffset, iLength)
    Else
        masOutput(miArrayMark, 1) = ""
    End If
    
    If iStartLeft > 0 Then
        masOutput(miArrayMark, 2) = Mid$(sParseInput, iStartLeft + iOffset, iLength)
    Else
        masOutput(miArrayMark, 2) = ""
    End If
    
    masOutput(miArrayMark, 3) = Str$(miTestMark)
    miArrayMark = miArrayMark + 1

    Exit Sub

lAddValues_Error:

    LogError "CDeviceResults", "AddValues", Err, Err.Description
End Sub

Public Sub NextTest()
    miTestMark = miTestMark + 1
End Sub

Public Sub PrevTest()
    miTestMark = miTestMark - 1
End Sub

Private Sub Class_Initialize()
    miArrayMark = 0
    miTestMark = 0
    ReDim masOutput(0 To 50, 0 To 3) As String
End Sub

Public Property Get asOutput() As String()
    asOutput = masOutput
End Property

Public Property Get iTestMark() As Long
    iTestMark = miTestMark
End Property
