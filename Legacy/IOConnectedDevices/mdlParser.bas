Attribute VB_Name = "mdlParser"
Option Explicit

Public OutputArray(0 To 50, 0 To 3) As String
Public ArrayMark As Integer                    'Record number of OutputArray
Public TestMark As Integer                     'Test number
'Public intIdx As Integer
'Public blnAppend As Boolean
'Public TimeToUnload As Boolean
Public blnUnLoad As Boolean

    
 Sub ParseSubString(ByVal intR As Integer, ByVal intL As Integer, ByVal tempSubParse As String, _
    ByVal intOffset As Integer, ByVal intLength As Integer, Optional ByVal intPrismDir As Integer)
    
        Dim strPrismDir As String
    
        If intR > 0 Then
            If intPrismDir Then
                strPrismDir = CStr(Mid$(tempSubParse, intR + intOffset + intPrismDir, 1))
            End If
            
            OutputArray(ArrayMark, 1) = strPrismDir & _
                Mid$(tempSubParse, intR + intOffset, intLength)
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        
        If intL > 0 Then
            If intPrismDir Then
                strPrismDir = CStr(Mid$(tempSubParse, intL + intOffset + intPrismDir, 1))
            End If
            OutputArray(ArrayMark, 2) = strPrismDir & _
                Mid$(tempSubParse, intL + intOffset, intLength)
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        
    End Sub






