VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form frmIOCD 
   BackColor       =   &H0077742D&
   Caption         =   "Data Device Import"
   ClientHeight    =   2505
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   ScaleHeight     =   2505
   ScaleWidth      =   5655
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   15000
      Left            =   1320
      Top             =   2280
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   240
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      CommPort        =   3
      DTREnable       =   -1  'True
      BaudRate        =   2400
      DataBits        =   7
      StopBits        =   2
   End
End
Attribute VB_Name = "frmIOCD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public myPort As String
Public mySettings As String
Public myDevice As String
Public ReceivedData As String
Public IOCDClass As IOConnectedDevices.ConnectedDevices

Private TimeToUnload As Boolean
Private debugMe As Boolean

Private Sub Form_Load()
    ReceivedData = ""
    TimeToUnload = False
    Timer1.Enabled = True
    Timer1.Interval = 15000
    debugMe = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer
If (MSComm1.PortOpen = True) Then
    MSComm1.PortOpen = False
End If
Set IOCDClass = Nothing
End Sub

Public Sub CheckDevices()
Dim deviceCount As String
    On Error GoTo lCheckDevices_Error

If WinRegLib.QueryHKLMValue("DeviceCount", deviceCount) Then
    If Val(deviceCount) > 1 Then
        FStatus.ShowDeviceList Val(deviceCount)
        FStatus.Status = "Which machine would you like to import data from?"
        FStatus.Show vbModal
        LoadCommSettings FStatus.DeviceChoice
    Else
        LoadCommSettings 1
    End If
Else
    LoadCommSettings 1
End If

    Exit Sub

lCheckDevices_Error:

    LogError "frmIOCD", "CheckDevices", Err, Err.Description
End Sub

Private Function COMCheckPort(Port As Integer) As Boolean
'Returns false if port cannot be opened or does not exist
'Returns true if port is available and can be opened
On Error GoTo OpenCom_Error

'Set port number for test
MSComm1.CommPort = Port
If MSComm1.PortOpen = True Then
    COMCheckPort = False
    Exit Function
Else
    'Test the port by opening and closing it
    MSComm1.PortOpen = True
    MSComm1.PortOpen = False
    COMCheckPort = True
    Exit Function
End If

OpenCom_Error:
   COMCheckPort = False
End Function

Private Function LoadCommSettings(deviceNumber As Integer) As Long
Dim ComPort As Integer
Dim DeviceArray(1) As String
On Error GoTo CommError

If (WinRegLib.QueryHKLMValue("ComPort" & Trim(Str(deviceNumber)), myPort) And _
    WinRegLib.QueryHKLMValue("ComSettings" & Trim(Str(deviceNumber)), mySettings) And _
    WinRegLib.QueryHKLMValue("DeviceName" & Trim(Str(deviceNumber)), myDevice)) Then
    ComPort = Val(myPort)
    If (COMCheckPort(ComPort)) Then
        With MSComm1
            .CommPort = ComPort
            .Handshaking = comNone
            .RThreshold = 1
            .RTSEnable = True
            .Settings = mySettings
            .SThreshold = 1
            .InputLen = 0
            .InBufferSize = 1024
            .PortOpen = True
        End With
        IOCDClass.resultStatus = EStatus.esNoDataRead
    Else
        IOCDClass.resultStatus = EStatus.esCommPortUnavailable
        Exit Function
    End If
Else
    IOCDClass.resultStatus = EStatus.esDeviceNotConfigured
    Exit Function
End If
    
Exit Function

CommError:
    IOCDClass.resultStatus = EStatus.esCommPortUnavailable

End Function

Public Sub MSComm1_OnComm()
Dim InBuff As String

    On Error GoTo lMSComm1_OnComm_Error

Select Case MSComm1.CommEvent
' Handle each event or error by placing
' code below each case statement.

' Errors
    Case comEventBreak   ' A Break was received.
    Case comEventCDTO    ' CD (RLSD) Timeout.
    Case comEventCTSTO   ' CTS Timeout.
    Case comEventDSRTO   ' DSR Timeout.
    Case comEventFrame   ' Framing Error
    Case comEventOverrun ' Data Lost.
    Case comEventRxOver  ' Receive buffer overflow.
    Case comEventRxParity   ' Parity Error.
    Case comEventTxFull  ' Transmit buffer full.
    Case comEventDCB  ' Unexpected error retrieving DCB

' Events
    Case comEvCD   ' Change in the CD line.
    Case comEvCTS  ' Change in the CTS line.
    Case comEvDSR  ' Change in the DSR line.
    Case comEvRing ' Change in the Ring Indicator.
    Case comEvReceive ' Received RThreshold # of chars.
    
        InBuff = MSComm1.Input
        Call ParseChars(InBuff)
    Case comEvSend ' There are SThreshold number of
                    ' characters in the transmit
                    ' buffer.
    Case comEvEOF   ' An EOF character was found in
                    ' the input stream.
End Select
If (TimeToUnload) Then
' unload frmiocd How can I make this work here?
    Unload FStatus
End If

    Exit Sub

lMSComm1_OnComm_Error:

    LogError "frmIOCD", "MSComm1_OnComm", Err, Err.Description, InBuff
End Sub

Private Sub HandleInput(InBuff As String)

    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Static l As Integer
    Dim strEnq As String
    Dim strEnd As String
    Dim blnStart As Boolean
    
    Static strHello As String
    On Error GoTo lHandleInput_Error

    strEnq = Chr(5) & Chr(5) & Chr(0)
    strEnd = Chr$(4)
    ReceivedData = ReceivedData & InBuff
    
'    Handshaking sequences
        
    Select Case myDevice
        Case "TopconCL100", "TopconCL200", "TopconEZ200"
            If mySettings = "2400,e,7,2" Then
                i = 0
                Do
                    i = InStr(i + 1, InBuff, Chr$(5) & vbCr, vbBinaryCompare)
                    If i > 0 Then
                        MSComm1.Output = Chr(6) & vbCr
                    End If
                Loop Until i = 0
                myDevice = "TopconNewFormat"
            Else
                myDevice = "TopconSTD1"
            End If
        Case "CanonRKF1"
            If Right$(InBuff, 2) = Chr$(5) & vbCr Then
                MSComm1.Output = Chr$(6) & vbCr
                LogDebugOnlyOutput Chr$(6) & vbCr
            ElseIf Right$(InBuff, 2) = Chr$(4) & vbCr Then
                MSComm1.Output = Chr$(6) & vbCr
                LogDebugOnlyOutput Chr$(6) & vbCr
            ElseIf Right$(InBuff, 1) = Chr$(3) Then
                MSComm1.Output = Chr$(6)
                LogDebugOnlyOutput Chr$(6)
            ElseIf Right$(InBuff, 1) = Chr$(5) Then
                MSComm1.Output = Chr$(6)
                LogDebugOnlyOutput Chr$(6)
            ElseIf Right$(InBuff, 1) = Chr$(23) Then
                MSComm1.Output = Chr$(6)
                LogDebugOnlyOutput Chr$(6)
            End If
                
            
        Case "ARK-700A", "ARK760A", "ARK-2000", "ARK-10000", "ARK-900", _
                "ARK-30", "VL3000Spectrum", "ARK560A"
            
            i = 0
'            l = 0
            If blnStart = False Then
                Do
                    i = InStr(i + 1, InBuff, "C**", vbBinaryCompare)
                    If i > 0 Then
                        LogDebugOnlyOutput Now & InBuff & ReceivedData
                        MSComm1.Output = Chr$(1) & "CRK" & Chr$(2) & "SD" & Chr$(23) & Chr$(4)
                        LogDebugOnlyOutput i & strHello & Chr$(1) & "CRK" & Chr$(2) & "SD" & Chr$(23) & Chr$(4)
                        blnStart = True
                        Exit Do
                    End If
               Loop Until i = 0
           End If
           
        Case "KR8000"
        
            i = 0
            j = 0
            Do
                i = InStr(i + 1, InBuff, Chr$(5), vbBinaryCompare)
                If i > 0 Then
                    j = j + 1
                    If j = 1 Then MSComm1.Output = Chr(6) & vbCr
                End If
            Loop Until i = 0
            
        Case "SpeedyK"
        
            Dim ii As Integer
            ii = 0
            i = 0
            j = 0
            k = 0
            l = 0
            Do
                i = InStr(i + 1, InBuff, Chr$(23), vbBinaryCompare)
                ii = InStr(i + 1, InBuff, Chr$(3), vbBinaryCompare)
                j = InStr(j + 1, InBuff, strEnq, vbBinaryCompare)
                k = InStr(k + 1, InBuff, Chr(5), vbBinaryCompare)
                If k > 0 Then
                    l = l + 1
                    If l = 2 Then
                        MSComm1.Output = Chr(6) & Chr(6) & Chr(0)
                        LogDebugOnlyOutput "k= " & k & vbCrLf & InBuff & vbCrLf & Chr(6) & Chr(6) & Chr(0)
                    End If
                End If
                If i > 0 Or j > 0 Then
                    MSComm1.Output = Chr(6) & Chr(6) & Chr(0)
                    LogDebugOnlyOutput "i= " & i & vbCrLf & "j= " & j & vbCrLf & Chr(6) & Chr(6) & Chr(0)
        '
        '            MSComm1.RThreshold = 1
                ElseIf ii > 0 Then
                    MSComm1.Output = Chr(6) & Chr(6) & Chr(0)
                    LogDebugOnlyOutput "ii= " & ii & vbCrLf & "i= " & i & vbCrLf & "j= " & j & vbCrLf & Chr(6) & Chr(6) & Chr(0)
                End If
            Loop Until i = 0 'And j = 0
        Case "HFA740", "HFA745", "HFA750"
            myDevice = "HFA7XX"
            strEnd = Chr$(3)
            
        Case Else
'            NoOp
    End Select

'   Check for Ascii character 4 (EOT) or character 3 (ETX) -- for Zeiss --
'    to signal that the device has transferred all its data.
'    If (InStr(1, InBuff, Chr$(4))) Then 'Used "0")) then as testing

    If (InStr(1, InBuff, strEnd)) Then 'Used "0")) then as testing
        
        Me.Timer1.Enabled = False
        If (debugMe) Then       'For debugging purposes, set debugMe = true to enable (in formLoad)
            LogDebugOnlyOutput Now & vbCrLf & ReceivedData
        End If
        Call IOCDClass.ParseDeviceOutput(myDevice, ReceivedData)
        TimeToUnload = True
        MSComm1.PortOpen = False
    End If

    Exit Sub

lHandleInput_Error:

    LogError "frmIOCD", "HandleInput", Err, Err.Description, ReceivedData
End Sub

Private Sub ParseChars(ByVal InString As String)

    Dim Temp As String
    Dim x As Long
    Dim OutString As String

    On Error GoTo lParseChars_Error

    For x = 1 To Len(InString)
        Temp = Mid$(InString, x, 1)
        Select Case myDevice
            Case "ARK-700A", "ARK760A", "ARK560A", "ARK-2000", "ARK-10000", "ARK-900", _
                "ARK-30", "VL3000Spectrum"
                If Temp = Chr(4) And x < 20 Then
                    Temp = ""
                End If
            Case Else
'                No Op
        End Select
        OutString = OutString & Temp
        Temp = ""
    Next x
    Call HandleInput(OutString)

    Exit Sub

lParseChars_Error:

    LogError "frmIOCD", "ParseChars", Err, Err.Description, InString
End Sub

Private Sub Timer1_Timer()
    On Error GoTo lTimer1_Timer_Error

    If (debugMe) Then ' Added for debugging purposes, set debugMe = true to enable (in formLoad).
        LogDebugOnlyOutput Now & vbCrLf & ReceivedData
    End If
    
    Call IOCDClass.ParseDeviceOutput(myDevice, ReceivedData)
    TimeToUnload = True
    If MSComm1.PortOpen Then MSComm1.PortOpen = False
    Timer1.Enabled = False
    Unload FStatus
    Unload Me

    Exit Sub

lTimer1_Timer_Error:

    LogError "frmIOCD", "Timer1_Timer", Err, Err.Description, ReceivedData
End Sub
