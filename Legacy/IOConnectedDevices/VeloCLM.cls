VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVeloCLM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice

Private oResults As CDeviceResults
Const strTestDelimiter As String = "$"

Public Function IDevice_ParseMachineOutput(StringToParse As String, numTests As Integer) As String()
Dim i As Integer                        'Counter for characters in string
Dim LMdone As Boolean                   'Lensmeter
    
    On Error GoTo lIDevice_ParseMachineOutput_Error
    Set oResults = New CDeviceResults
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            LMdone = ParseLM(StringToParse, i)
            If Not LMdone Then
                oResults.PrevTest
            End If
        End If
    Next i
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing
    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CTopconSTD1", "IDevice_ParseMachineOutput", Err, Err.Description
End Function

Private Function ParseLM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
Dim intR As Long                     'Position of indicator for Right Eye
Dim intL As Long                     'Position of indicator for Left Eye
Dim tempSubParse As String
    
'    Machine specific constants
Const sSCAR As String = "R:"          'SCA right eye
Const sSCAL As String = "L:"          'SCA left eye
'    Const strAddR As String = "AR"          'add right eye
'    Const strAddL As String = "AL"          'add left eye
'    Const strPrismR As String = "PR"        'prism right eye
'    Const strPrismL As String = "PL"        'prism left eye
'    Const strObjD As String = "PD"          'pupil distance
    
    On Error GoTo lParseLM_Error

    ParseLM = False
    oResults.NextTest
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    ' SCA
    intR = InStr(1, tempSubParse, sSCAR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, sSCAL, vbBinaryCompare)
    If intR + intL > 0 Then
        ' Add the Sphere
        oResults.AddValues "LMSPH", tempSubParse, intR, intL, 5, 6
        ' Add the Cylinder
        oResults.AddValues "LMCYL", tempSubParse, intR, intL, 14, 6
        ' Add the Axis
        oResults.AddValues "LMAXI", tempSubParse, intR, intL, 23, 3
        ' ADD
        oResults.AddValues "LMADD", tempSubParse, intR, intL, 59, 5
        ' Prism
        ' Add H Prism
        oResults.AddValues "LMPRH", tempSubParse, intR, intL, 30, 6
        ' Add V Prism
        oResults.AddValues "LMPRV", tempSubParse, intR, intL, 40, 6
        ' PD Values
        oResults.AddValues "LMPD", tempSubParse, intR, intL, 50, 4
        
        ParseLM = True
    End If

    Exit Function

lParseLM_Error:

    LogError "CTopconSTD1", "ParseLM", Err, Err.Description
    
    End Function

