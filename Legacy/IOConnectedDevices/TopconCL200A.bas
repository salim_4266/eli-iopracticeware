Attribute VB_Name = "TopconCL200A"
Option Explicit


'Machine specific constants
Private Const strTestDelimiter As String = "C"
Private oResults As CDeviceResults


Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer                        'Counter for characters in string
    
    Dim tempSubParse As String
    
    Dim LMdone As Boolean                   'Lensmeter
    
'    Initialize variables
    LMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray

    
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            LMdone = ParseLM(StringToParse, i)
            If Not LMdone Then TestMark = TestMark - 1
        End If
    Next i
    
    ParseMachineOutput = OutputArray
    numTests = TestMark
    Exit Function
End Function

Private Sub StripCharacters(TheText As String, StripChar As String)
    Dim i As Integer
    Dim NewText As String
    NewText = ""
    If Trim$(TheText) <> "" And Len(StripChar) > 0 Then
        For i = 1 To Len(TheText)
            If Mid$(TheText, i, 1) <> StripChar Then
                NewText = NewText & Mid$(TheText, i, 1)
            End If
        Next i
        TheText = NewText
    End If
End Sub

Function ParseLM(ByVal StringToParse As String, ByVal i As Integer) As Boolean

Dim intR As Integer                     'Position of indicator for Right Eye
Dim intL As Integer                     'Position of indicator for Left Eye
Dim intCR As Integer                    'Position of end of line
Dim tempSubParse As String

'Machine specific constants
Const strSCAR As String = "R"          'SCA right eye
Const strSCAL As String = "L"          'SCA left eye

On Error GoTo lParseLM_Error
    
ParseLM = False

TestMark = TestMark + 1
If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
    tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
Else
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
End If

'Remove the newline and extrta spaces
Call StripCharacters(tempSubParse, Chr(13))
Call StripCharacters(tempSubParse, Chr(0))

' SCA
intR = InStr(1, tempSubParse, strSCAR, vbBinaryCompare)
intL = InStr(1, tempSubParse, strSCAL, vbBinaryCompare)
If intR + intL > 0 Then

    ' Add the Sphere
    OutputArray(ArrayMark, 0) = "LMSPH"
    mdlParser.ParseSubString intR, intL, tempSubParse, 1, 6
    
    ' Add the Cylinder
    OutputArray(ArrayMark, 0) = "LMCYL"
    mdlParser.ParseSubString intR, intL, tempSubParse, 7, 6
    
    ' Add the Axis
    OutputArray(ArrayMark, 0) = "LMAXI"
    mdlParser.ParseSubString intR, intL, tempSubParse, 13, 3
    
    ' Add
    OutputArray(ArrayMark, 0) = "LMADD"
    mdlParser.ParseSubString intR, intL, tempSubParse, 16, 6
    
    ParseLM = True
End If
Exit Function

lParseLM_Error:
    LogError "TopConCL200A", "ParseLM", Err, Err.Description
End Function


