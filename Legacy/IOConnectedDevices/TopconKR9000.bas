Attribute VB_Name = "TopconKR9000"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer                    'Record number of OutputArray
'Private TestMark As Integer                     'Test numbervbcrlf
'Machine specific constants
Private Const strTestType1 As String = "A"
Private Const strTestType2 As String = "K"
Private oResults As CDeviceResults

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()


    Dim i As Integer

    Dim RMdone As Boolean                   'Autorefractometer  (Objective data)
    Dim KMdone As Boolean                   'Keratometer


    '    Initialize variables
    RMdone = False
    KMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray

    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestType1) Then
            RMdone = ParseRM(StringToParse, i)
            If Not RMdone Then TestMark = TestMark - 1
        ElseIf (Mid$(StringToParse, i, 1) = strTestType2) Then
            KMdone = ParseKM(StringToParse, i)
            If Not KMdone Then TestMark = TestMark - 1
        End If
    Next i

    ParseMachineOutput = OutputArray
    numTests = TestMark

    Exit Function
End Function
Private Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean

    Dim iR As Long                     'Position of indicator for Right Eye
    Dim iL As Long                     'Position of indicator for Left Eye
    Dim tempSubParse As String

    '    Machine specific constants
    Const sSCAR As String = "R"          'SCA right eye
    Const sSCAL As String = "L"          'SCA left eye

    On Error GoTo lParseRM_Error

    TestMark = TestMark + 1
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)

    'Remove the newline and extrta spaces
    Call StripCharacters(tempSubParse, Chr(13))
    Call StripCharacters(tempSubParse, Chr(0))
    ' SCA
    iR = InStr(1, tempSubParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, tempSubParse, sSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "RMSPH"
        mdlParser.ParseSubString iR, iL, tempSubParse, 1, 6 

        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "RMCYL"
        mdlParser.ParseSubString iR, iL, tempSubParse, 7, 6

        ' Add the Axis
        OutputArray(ArrayMark, 0) = "RMAXI"
        mdlParser.ParseSubString iR, iL, tempSubParse, 13, 3

        ParseRM = True
    End If
    Exit Function

lParseRM_Error:

    LogError "TopConKR9000", "ParseLM", Err, Err.Description

End Function

Private Sub StripCharacters(TheText As String, StripChar As String)
    Dim i As Integer
    Dim NewText As String
    NewText = ""
    If Trim$(TheText) <> "" And Len(StripChar) > 0 Then
        For i = 1 To Len(TheText)
            If Mid$(TheText, i, 1) <> StripChar Then
                NewText = NewText & Mid$(TheText, i, 1)
            End If
        Next i
        TheText = NewText
    End If
End Sub

Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean

    On Error GoTo lParseKM_Error


    Dim intR As Integer                 'Position of indicator for Right Eye
    Dim intL As Integer                 'Position of indicator for Left Eye
    Dim tempSubParse As String
    Dim j As Integer

    '    Machine specific constants
    Const strMmR As String = "R"       'mm data right eye
    Const strMmL As String = "L"       'mm data left eye

    ParseKM = False

    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)

    TestMark = TestMark + 1
    'Remove the newline and extrta spaces
    Call StripCharacters(tempSubParse, Chr(13))


    intR = InStr(1, tempSubParse, strMmR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strMmL, vbBinaryCompare)

    If intR + intL > 0 Then
        'R1
        OutputArray(ArrayMark, 0) = "KMR1"
        mdlParser.ParseSubString intR, intL, tempSubParse, 2, 5


        OutputArray(ArrayMark, 0) = "KMAXI"
        mdlParser.ParseSubString intR, intL, tempSubParse, 8, 5


        OutputArray(ArrayMark, 0) = "KMCYL"
        mdlParser.ParseSubString intR, intL, tempSubParse, 14, 3


        OutputArray(ArrayMark, 0) = "KMAVE"
        mdlParser.ParseSubString intR, intL, tempSubParse, 18, 5

        'R2
        OutputArray(ArrayMark, 0) = "KMR2"
        mdlParser.ParseSubString intR, intL, tempSubParse, 24, 5


        OutputArray(ArrayMark, 0) = "KMAXI2"
        mdlParser.ParseSubString intR, intL, tempSubParse, 30, 3


        OutputArray(ArrayMark, 0) = "KMCYL"
        mdlParser.ParseSubString intR, intL, tempSubParse, 34, 5


        OutputArray(ArrayMark, 0) = "KMAVE"
        mdlParser.ParseSubString intR, intL, tempSubParse, 40, 5


        OutputArray(ArrayMark, 0) = "KMDiopters"
        mdlParser.ParseSubString intR, intL, tempSubParse, 45, 7


        OutputArray(ArrayMark, 0) = "KMRadius"
        mdlParser.ParseSubString intR, intL, tempSubParse, 53, 3

        ParseKM = True
    End If

lParseKM_Error:

    LogError "TopConKR9000", "ParseKM", Err, Err.Description
End Function

