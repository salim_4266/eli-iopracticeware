VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmWhichEye 
   BackColor       =   &H0077742D&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Measured Eye"
   ClientHeight    =   5055
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   4170
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   4170
   Begin fpBtnAtlLibCtl.fpBtn btnWhichEye 
      Height          =   975
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmWhichEye.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn btnWhichEye 
      Height          =   975
      Index           =   1
      Left            =   2160
      TabIndex        =   1
      Top             =   720
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmWhichEye.frx":01E9
   End
   Begin fpBtnAtlLibCtl.fpBtn btnWhichEye 
      Height          =   975
      Index           =   2
      Left            =   240
      TabIndex        =   2
      Top             =   1800
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmWhichEye.frx":03D1
   End
   Begin fpBtnAtlLibCtl.fpBtn btnWhichEye 
      Height          =   975
      Index           =   3
      Left            =   2160
      TabIndex        =   3
      Top             =   1800
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmWhichEye.frx":05B2
   End
   Begin fpBtnAtlLibCtl.fpBtn btnOverwrite 
      Height          =   1095
      Index           =   0
      Left            =   240
      TabIndex        =   4
      Top             =   3840
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmWhichEye.frx":0793
   End
   Begin fpBtnAtlLibCtl.fpBtn btnOverwrite 
      Height          =   1095
      Index           =   1
      Left            =   2160
      TabIndex        =   5
      Top             =   3840
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmWhichEye.frx":0985
   End
   Begin VB.Label lblOverwrite 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Overwrite or Append Existing Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   465
      TabIndex        =   7
      Top             =   3000
      Width           =   3255
   End
   Begin VB.Label lblWhichEye 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Select which pair"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   398
      TabIndex        =   6
      Top             =   120
      Width           =   3375
   End
End
Attribute VB_Name = "frmWhichEye"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub btnOverwrite_Click(Index As Integer)

    Select Case Index
        Case 0
            frmIOCD.IOCDClass.blnAppend = False
        Case 1
            frmIOCD.IOCDClass.blnAppend = True
    End Select
    
    Unload Me

End Sub

Private Sub btnWhichEye_Click(Index As Integer)

    Dim i As Integer

    frmIOCD.IOCDClass.intIdx = Index
    
    btnWhichEye(Index).BackColor = vbWhite
    btnWhichEye(Index).ForeColor = vbBlack
    
    lblOverwrite.Visible = True
    For i = 0 To 1
        Me.btnOverwrite(i).Visible = True
    Next i
    
    Me.Height = 5430
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Select Case UnloadMode
        Case 0
            Me.Hide
            Unload Me
'            TimeToUnload = True
            frmIOCD.Timer1.Enabled = False
            If frmIOCD.MSComm1.PortOpen = True Then
                frmIOCD.MSComm1.PortOpen = False
            End If
            blnUnLoad = True
            Unload frmIOCD
'            End
'            frmIOCD.Visible = True
        Case Else
    End Select
End Sub
