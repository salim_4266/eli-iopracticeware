VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CARK760A"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice

Private oResults As CDeviceResults
Const strTestDelimiter    As String = ""    'Chr(1)

Public Function IDevice_ParseMachineOutput(StringToParse As String, _
                                   numTests As Integer) As String()

Dim i      As Integer   'Counter for characters in string
Dim RMdone As Boolean   'Autorefractometer  (Objective data)
Dim KMdone As Boolean   'Keratometer

    On Error GoTo lIDevice_ParseMachineOutput_Error

    Set oResults = New CDeviceResults
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            Select Case Mid$(StringToParse, i + 1, 3)
            Case "DRM"
                RMdone = ParseRM(StringToParse, i)
                If Not RMdone Then
                    oResults.PrevTest
                End If
            Case "DKM"
                KMdone = ParseKM(StringToParse, i)
                If Not KMdone Then
                    oResults.PrevTest
                End If
            End Select
        End If
    Next i
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CARK760A", "IDevice_ParseMachineOutput", Err, Err.Description

End Function

Private Function ParseKM(ByVal StringToParse As String, _
                        ByVal i As Integer) As Boolean

Dim intR         As Long      'Position of indicator for Right Eye
Dim intL         As Long      'Position of indicator for Left Eye
Dim tempSubParse As String

Const strDioR    As String = "DR" 'diopter data right eye
Const strDioL    As String = "DL" 'diopter data left eye
    '    Machine specific constants
    '    Const strMmR As String = "CR"       'mm data right eye
    '    Const strMmL As String = "CL"       'mm data left eye
    ParseKM = False
    oResults.NextTest
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else 'NOT (INSTR(I...
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    '    'mm Data
    '    intR = InStr(1, tempSubParse, "CR", vbBinaryCompare)
    '    intL = InStr(1, tempSubParse, "CL", vbBinaryCompare)
    '    If intR + intL > 0 Then
    '
    '        OutputArray(ArrayMark, 0) = "KMAXI"
    '        mdlparser.ParseSubString intR, intL, tempSubParse, 12, 3
    '
    '        ParseKM = True
    '    End If
    'D data
    ' R1
    intR = InStr(1, tempSubParse, strDioR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strDioL, vbBinaryCompare)
    If intR + intL > 0 Then
        ' R1
        oResults.AddValues "KMR1", tempSubParse, intR, intL, 2, 5
        ' Axis
        oResults.AddValues "KMAXI", tempSubParse, intR, intL, 12, 3
        ' R2
        oResults.AddValues "KMR2", tempSubParse, intR, intL, 7, 5
        ' AVE
        oResults.AddValues "KMAVE", tempSubParse, intR, intL, 15, 5
        ParseKM = True
    End If

End Function

Private Function ParseRM(ByVal StringToParse As String, _
                        ByVal i As Integer) As Boolean

Dim intR         As Long         'Position of indicator for Right Eye
Dim intL         As Long         'Position of indicator for Left Eye
Dim sName As String

Dim tempSubParse As String
Dim j            As Integer
Const strObjSCAR As String = "OR"   'objective SCA right eye
Const strObjSCAL As String = "OL"   'objective SCA left eye
Const strCorVAR  As String = "WR"   'corrected visual acuity right eye
Const strCorVAL  As String = "WL"   'corrected visual acuity left eye
    'Const strPD As String = "PD"                'pupil distance

    ParseRM = False
    oResults.NextTest
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    'SCA
    intR = InStr(1, tempSubParse, strObjSCAR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strObjSCAL, vbBinaryCompare)
    If intR + intL > 0 Then
        ' Add the Sphere
        oResults.AddValues "RMSPH", tempSubParse, intR, intL, 2, 6
        ' Add the Cylinder
        oResults.AddValues "RMCYL", tempSubParse, intR, intL, 8, 6
        ' Add the Axis
        oResults.AddValues "RMAXI", tempSubParse, intR, intL, 14, 3
        ' Add the Reliability (From the first test)
        oResults.AddValues "RMREL", tempSubParse, intR, intL, 35, 1
        ParseRM = True
    End If

    intR = InStr(1, tempSubParse, strCorVAR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strCorVAL, vbBinaryCompare)
    ' Visual Acuity (right)
    sName = PickVAButton(tempSubParse, intR)
    If sName <> "" Then
        oResults.AddValues sName, "10", 1, 2, 0, 1
        ParseRM = True
    End If
    ' Visual Acuity (left)
    sName = PickVAButton(tempSubParse, intL)
    If sName <> "" Then
        oResults.AddValues sName, "01", 1, 2, 0, 1
        ParseRM = True
    End If
    
    '    ' PD
    '    intR = InStr(1, tempSubParse, strPD, vbBinaryCompare)
    '    intCR = InStr(intR + 1, tempSubParse, vbCrLf, vbBinaryCompare)
    '    intR = InStr(intR + 6, tempSubParse, ".", vbTextCompare)
    '    If intR > 0 And intR < intCR Then
    '        intL = InStr(intR + 1, tempSubParse, ".", vbTextCompare)
    '        OutputArray(ArrayMark, 0) = "RMPD"
    '        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR - 2, 4)
    '        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, intL - 2, 4)
    '        OutputArray(ArrayMark, 3) = Str(TestMark)
    '        ArrayMark = ArrayMark + 1
    ''        mdlparser.ParseSubString intR, intL, tempSubParse, 6, 4
    ''        mdlparser.ParseSubString intR, intL, tempSubParse, 10, 4
    '        ParseRM = True
    '    End If

End Function

Private Function PickVAButton(sToParse As String, iPos As Long) As String
Dim sName As String
    sName = ""
    Select Case Trim$(Mid$(sToParse, iPos + 2, 4))
        Case "15"
            sName = "RM15"
        Case "20"
            sName = "RM20"
        Case "25"
            sName = "RM25"
        Case "30"
            sName = "RM30"
        Case "40"
            sName = "RM40"
        Case "50"
            sName = "RM50"
        Case "60"
            sName = "RM60"
        Case "70"
            sName = "RM70"
        Case "80"
            sName = "RM80"
        Case "100"
            sName = "RM100"
        Case "150"
            sName = "RM150"
        Case "200"
            sName = "RM200"
        Case "400"
            sName = "RM400"
        Case "800"
            sName = "RM800"
    End Select
    PickVAButton = sName
End Function



