VERSION 5.00
Begin VB.Form FCDTest 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCommTest 
      Caption         =   "Comm Test"
      Height          =   1335
      Left            =   2280
      TabIndex        =   1
      Top             =   1560
      Width           =   1815
   End
   Begin VB.CommandButton cmdTest 
      Caption         =   "Parse Hardcoded Data"
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "FCDTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCommTest_Click()
Dim GlassesOn As Boolean
Dim ContOn As Boolean
Dim InitialPull As Boolean
Dim FrmId As Long, RscId As Long
Dim w As Integer
Dim r As Integer, X As Integer
Dim g As Integer, f As Integer
Dim z As Integer, u As Integer
Dim Temp As String
Dim Ques As String, OrdId As String
Dim AName As String, TestTime As String
Dim APiece1(50) As String
Dim APiece2(50) As String
Dim APiece3(50) As String
Dim AContent() As String
Dim MyFile As String
Dim AFileName As String
Dim AllFiles(4) As String
Dim AllOrders(4) As String
Dim CurrCnt(4) As Integer
Dim CurODEyeSet(4) As Boolean
Dim CurOSEyeSet(4) As Boolean
Dim DL As IOConnectedDevices.ConnectedDevices
Dim fTestData As Boolean
    fTestData = False
    GlassesOn = False
    InitialPull = True
    For f = 1 To 4
        CurODEyeSet(f) = True
        CurOSEyeSet(f) = True
    Next
    f = 0
    
    
    Set DL = New IOConnectedDevices.ConnectedDevices
    If fTestData Then    ' Loopback Function
        Call DL.SetDummyData
    Else
        ContOn = True
        Do Until Not (ContOn)
            Call DL.GetDeviceData
            DoEvents
            For u = 1 To 20 ' SLEEPTIME in the PIC
                Call DoSleep(1000)
                If (DL.resultStatus <> 1) Then
                    Exit For
                End If
            Next u
            If (DL.resultStatus = 0) Then
                ContOn = False
            Else
                If MsgBox("Data not received, try again?", vbYesNo) = vbYes Then
                    ContOn = True
                Else
                    ContOn = False
                End If
            End If
        Loop
    End If
    
    If (DL.resultStatus = 0) Then
        ' NO OP
    Else
    ' Class ConnectedDevices: The class is called ConnectedDevices in dll called IOConnectedDevices.
    ' integer resultStatus: There is a public variable resultStatus which is set to 1 initially.
    ' Statuses are: 0 - Done
    '     1 - No Data Read
    '     2 - Comm Port Unavailable
    '     3 - Device Not Configured
        If (DL.resultStatus = 2) Then
            MsgBox "Data not received: Error - Comm Port Not Available"
        ElseIf (DL.resultStatus = 3) Then
            MsgBox "Data not received: Error - Device Not Configurated"
        Else
            MsgBox "Data not received: Error = " & Trim$(Str$(DL.resultStatus))
        End If
    End If
    Set DL = Nothing
End Sub

Private Sub cmdTest_Click()
    Dim RTTest As ConnectedDevices
    Dim ContOn As Boolean
    Dim u As Integer
    Dim TestData As String
    Dim OutputData As String
    
    Set RTTest = New ConnectedDevices
    Open ".\NidekRT2100-IOCDdebug.log" For Input As #207
    TestData = Input(LOF(207), 207)
    Close #207
    RTTest.ParseDeviceOutput "NidekRT2100", TestData
    
    
    
    
    
    
    
    
'    ContOn = True
'    Do Until Not (ContOn)
'        Call RTTest.GetDeviceData
'        DoEvents
'        For u = 1 To 20     '15
'            Call DoSleep(1000)
'            If (RTTest.resultStatus <> 1) Then
'                Exit For
'            End If
'        Next u
'        If (RTTest.resultStatus = 0) Then
'            ContOn = False
'        Else
''            frmEventMsgs.Header = "Data Not Received, Try Again ?"
''            frmEventMsgs.AcceptText = ""
''            frmEventMsgs.RejectText = "Yes"
''            frmEventMsgs.CancelText = "No"
''            frmEventMsgs.Other0Text = ""
''            frmEventMsgs.Other1Text = ""
''            frmEventMsgs.Other2Text = ""
''            frmEventMsgs.Other3Text = ""
''            frmEventMsgs.Other4Text = ""
''            frmEventMsgs.Show 1
''            If (frmEventMsgs.Result <> 2) Then
'                ContOn = False
''            End If
'        End If
'    Loop

End Sub

Public Sub DoSleep(AMill As Long)
Sleep AMill
DoEvents
End Sub
