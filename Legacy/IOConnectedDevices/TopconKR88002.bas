Attribute VB_Name = "TopConKR88002"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer                    'Record number of OutputArray
'Private TestMark As Integer                     'Test number
    
'Machine specific constants
Const strTestDelimiter As String = "*"

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer                        'Counter for characters in string
    
    Dim RMdone As Boolean                   'Autorefractometer  (Objective data)
    Dim KMdone As Boolean                   'Keratometer
    
'    Initialize variables
    RMdone = False
    KMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray
    
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            If Not RMdone Or Not KMdone Then
                RMdone = ParseRM(StringToParse, i)
                If Not RMdone Then TestMark = TestMark - 1
                KMdone = ParseKM(StringToParse, i)
                If Not KMdone Then TestMark = TestMark - 1
            End If
        End If
    Next i
    
    ParseMachineOutput = OutputArray
    numTests = TestMark
    Exit Function
End Function

Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    Dim j As Integer
    
    ParseRM = False
    
    TestMark = TestMark + 1
    
    'SCA
    ' Add the Sphere
    OutputArray(ArrayMark, 0) = "RMSPH"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 22, 6)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 187, 6)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ' Add the Cylinder
    OutputArray(ArrayMark, 0) = "RMCYL"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 28, 6)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 193, 6)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ' Add the Axis
    OutputArray(ArrayMark, 0) = "RMAXI"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 34, 3)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 199, 3)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ParseRM = True
    
    ' PD
    OutputArray(ArrayMark, 0) = "RMPD"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 352, 2)
    OutputArray(ArrayMark, 2) = ""
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1

    ParseRM = True
    End Function
    
    Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    Dim j As Integer
    
    ParseKM = False
    
    TestMark = TestMark + 1

'
'    'mm Data
'    intR = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'    If intR + intL > 0 Then
'
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        ParseSubString intR, intL, tempSubParse, 12, 3
'
'        ParseKM = True
'    End If
    
    'D data
    
        ' R1
    OutputArray(ArrayMark, 0) = "KMR1"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 50, 5)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 215, 5)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1

    '    Axis

    OutputArray(ArrayMark, 0) = "KMAXI"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 55, 3)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 220, 3)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1

    
        ' R2

    OutputArray(ArrayMark, 0) = "KMR2"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 63, 5)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 228, 5)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
        '    Axis2

    OutputArray(ArrayMark, 0) = "KMAXI2"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 68, 3)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 233, 3)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
   
    ParseKM = True
End Function





