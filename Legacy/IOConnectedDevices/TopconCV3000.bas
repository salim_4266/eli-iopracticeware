Attribute VB_Name = "TopconCV3000"
Option Explicit

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()
Dim OutputArray(50, 3) As String
Dim ArrayMark As Integer
Dim TestMark As Integer
Dim i As Integer, j As Integer
Dim tempSubParse As String
Dim CVdone As Boolean
Dim FCdone As Boolean
Dim RMdone As Boolean
Dim KRdone As Boolean
Dim CLdone As Boolean
CVdone = False
FCdone = False
RMdone = False
KRdone = False
CLdone = False
ArrayMark = 0
TestMark = 0
For i = 1 To Len(StringToParse) - 3
    If (Mid$(StringToParse, i, 1) = "*") Then
        Select Case Mid$(StringToParse, i + 2, 2)
            Case "CV"
                GoSub ParseCV
            Case "FC"
                'GoSub ParseFC
            Case "RM"
                GoSub ParseRM
            Case "KR"
                GoSub ParseKR
            Case "CL"
                GoSub ParseCL
        End Select
    End If
Next i
ParseMachineOutput = OutputArray
numTests = TestMark
Exit Function

' Each of these parsing routines grabs the data for a specific test and creates
' a 3 column array with control tag (ICDAlias4 in DF), OD data and OS data.
ParseCV:
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, "*") <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "*") - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    ' SCA
    If (InStr(1, tempSubParse, "FR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "FL", vbBinaryCompare) <> 0) Then
        ' Add the Sphere
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RTSPH"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 2, 6))
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 2, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Cylinder
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RTCYL"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 8, 6))
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 8, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Axis
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RTAXI"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 14, 3))
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 14, 3))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    'ADD
    If (InStr(1, tempSubParse, "AR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "AL", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "AR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RTADD"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 2, 6))
        j = InStr(1, tempSubParse, "AL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 2, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    'Prism
    If (InStr(1, tempSubParse, "PR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "PL", vbBinaryCompare) <> 0) Then
        ' Add H Prism
        j = InStr(1, tempSubParse, "PR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RTPRH"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 2, 6))
        j = InStr(1, tempSubParse, "PL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 2, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add V Prism
        j = InStr(1, tempSubParse, "PR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RTPRV"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 8, 6))
        j = InStr(1, tempSubParse, "PL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 8, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    ' Visual Acuity (right)
    If (InStr(1, tempSubParse, "vR", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "vR", vbBinaryCompare)
        Select Case Trim(Mid$(tempSubParse, j + 2, 5))
            Case "PLANO"
                OutputArray(ArrayMark, 0) = "RTPLA"
            Case "BAL"
                OutputArray(ArrayMark, 0) = "RTBL"
            Case "15"
                OutputArray(ArrayMark, 0) = "RT15"
            Case "20"
                OutputArray(ArrayMark, 0) = "RT20"
            Case "25"
                OutputArray(ArrayMark, 0) = "RT25"
            Case "30"
                OutputArray(ArrayMark, 0) = "RT30"
            Case "40"
                OutputArray(ArrayMark, 0) = "RT40"
            Case "50"
                OutputArray(ArrayMark, 0) = "RT50"
            Case "60"
                OutputArray(ArrayMark, 0) = "RT60"
            Case "70"
                OutputArray(ArrayMark, 0) = "RT70"
            Case "80"
                OutputArray(ArrayMark, 0) = "RT80"
            Case "100"
                OutputArray(ArrayMark, 0) = "RT100"
            Case "150"
                OutputArray(ArrayMark, 0) = "RT150"
            Case "200"
                OutputArray(ArrayMark, 0) = "RT200"
            Case "400"
                OutputArray(ArrayMark, 0) = "RT400"
            Case "800"
                OutputArray(ArrayMark, 0) = "RT800"
            Case "+1"
                OutputArray(ArrayMark, 0) = "RT+1"
            Case "+2"
                OutputArray(ArrayMark, 0) = "RT+2"
            Case "-1"
                OutputArray(ArrayMark, 0) = "RT-1"
            Case "-2"
                OutputArray(ArrayMark, 0) = "RT-2"
            Case "FC1"
                OutputArray(ArrayMark, 0) = "RTFC1"
            Case "FC3"
                OutputArray(ArrayMark, 0) = "RTFC3"
            Case "HM"
                OutputArray(ArrayMark, 0) = "RTHM"
            Case "LP"
                OutputArray(ArrayMark, 0) = "RTLP"
        End Select
        OutputArray(ArrayMark, 1) = "1"
        OutputArray(ArrayMark, 2) = "0"
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    ' Visual Acuity (left)
    If (InStr(1, tempSubParse, "vL", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "vL", vbBinaryCompare)
        Select Case Trim(Mid$(tempSubParse, j + 2, 5))
            Case "PLANO"
                OutputArray(ArrayMark, 0) = "RTPLA"
            Case "BAL"
                OutputArray(ArrayMark, 0) = "RTBL"
            Case "15"
                OutputArray(ArrayMark, 0) = "RT15"
            Case "20"
                OutputArray(ArrayMark, 0) = "RT20"
            Case "25"
                OutputArray(ArrayMark, 0) = "RT25"
            Case "30"
                OutputArray(ArrayMark, 0) = "RT30"
            Case "40"
                OutputArray(ArrayMark, 0) = "RT40"
            Case "50"
                OutputArray(ArrayMark, 0) = "RT50"
            Case "60"
                OutputArray(ArrayMark, 0) = "RT60"
            Case "70"
                OutputArray(ArrayMark, 0) = "RT70"
            Case "80"
                OutputArray(ArrayMark, 0) = "RT80"
            Case "100"
                OutputArray(ArrayMark, 0) = "RT100"
            Case "150"
                OutputArray(ArrayMark, 0) = "RT150"
            Case "200"
                OutputArray(ArrayMark, 0) = "RT200"
            Case "400"
                OutputArray(ArrayMark, 0) = "RT400"
            Case "800"
                OutputArray(ArrayMark, 0) = "RT800"
            Case "+1"
                OutputArray(ArrayMark, 0) = "RT+1"
            Case "+2"
                OutputArray(ArrayMark, 0) = "RT+2"
            Case "-1"
                OutputArray(ArrayMark, 0) = "RT-1"
            Case "-2"
                OutputArray(ArrayMark, 0) = "RT-2"
            Case "FC1"
                OutputArray(ArrayMark, 0) = "RTFC1"
            Case "FC3"
                OutputArray(ArrayMark, 0) = "RTFC3"
            Case "HM"
                OutputArray(ArrayMark, 0) = "RTHM"
            Case "LP"
                OutputArray(ArrayMark, 0) = "RTLP"
        End Select
        OutputArray(ArrayMark, 1) = "0"
        OutputArray(ArrayMark, 2) = "1"
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "pD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "pD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "RTPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
    'End If
    If Not (CVdone) Then
        TestMark = TestMark - 1
    End If
    Return
    
'ParseFC:
'    TestMark = TestMark + 1
'    If (InStr(i + 1, StringToParse, "*") <> 0) Then
'        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "*") - i)
'    Else
'        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
'    End If
'    ' SCA
'    If (InStr(1, tempSubParse, " R", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, " L", vbBinaryCompare) <> 0) Then
'        ' Add the Sphere
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "LMSPH"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 6)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 6)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Add the Cylinder
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "LMCYL"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 8, 6)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 8, 6)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Add the Axis
'        j = InStr(1, tempSubParse, " R", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "LMAXI"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 14, 3)
'        j = InStr(1, tempSubParse, " L", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 14, 3)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        FCdone = True
'    End If
'    ' ADD
'    If (InStr(1, tempSubParse, "AR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "AL", vbBinaryCompare) <> 0) Then
'        j = InStr(1, tempSubParse, "AR", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "LMADD"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 6)
'        j = InStr(1, tempSubParse, "AL", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 6)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        FCdone = True
'    End If
'    ' Prism
'    If (InStr(1, tempSubParse, "PR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "PL", vbBinaryCompare) <> 0) Then
'        ' Add H Prism
'        j = InStr(1, tempSubParse, "PR", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "LMPRH"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 6)
'        j = InStr(1, tempSubParse, "PL", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 6)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        ' Add V Prism
'        j = InStr(1, tempSubParse, "PR", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "LMPRV"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 8, 6)
'        j = InStr(1, tempSubParse, "PL", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 8, 6)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        FCdone = True
'    End If
'    ' PD - not yet working
'    'If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
'    '    j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
'    '    OutputArray(ArrayMark, 0) = "LMPD"
'    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
'    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
'    '    OutputArray(ArrayMark, 3) = Str(TestMark)
'    '    ArrayMark = ArrayMark + 1
'    'End If
'    If Not (FCdone) Then
'        TestMark = TestMark - 1
'    End If
'    Return
    
ParseRM:
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, "*") <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "*") - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    'SCA
    If (InStr(1, tempSubParse, "FR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "FL", vbBinaryCompare) <> 0) Then
        ' Add the Sphere
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMSPH"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 6)
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 6)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Cylinder
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMCYL"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 8, 6)
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 8, 6)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Axis
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "RMAXI"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 14, 3)
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 14, 3)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    End If
    ' Visual Acuity (right)
    If (InStr(1, tempSubParse, "VR", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "VR", vbBinaryCompare)
        Select Case Trim(Mid$(tempSubParse, j + 2, 5))
            Case "15"
                OutputArray(ArrayMark, 0) = "RM15"
            Case "20"
                OutputArray(ArrayMark, 0) = "RM20"
            Case "25"
                OutputArray(ArrayMark, 0) = "RM25"
            Case "30"
                OutputArray(ArrayMark, 0) = "RM30"
            Case "40"
                OutputArray(ArrayMark, 0) = "RM40"
            Case "50"
                OutputArray(ArrayMark, 0) = "RM50"
            Case "60"
                OutputArray(ArrayMark, 0) = "RM60"
            Case "70"
                OutputArray(ArrayMark, 0) = "RM70"
            Case "80"
                OutputArray(ArrayMark, 0) = "RM80"
            Case "100"
                OutputArray(ArrayMark, 0) = "RM100"
            Case "150"
                OutputArray(ArrayMark, 0) = "RM150"
            Case "200"
                OutputArray(ArrayMark, 0) = "RM200"
            Case "400"
                OutputArray(ArrayMark, 0) = "RM400"
            Case "800"
                OutputArray(ArrayMark, 0) = "RM800"
            Case "FC1"
                OutputArray(ArrayMark, 0) = "RMFC1"
            Case "FC3"
                OutputArray(ArrayMark, 0) = "RMFC3"
            Case "HM"
                OutputArray(ArrayMark, 0) = "RMHM"
            Case "ERR"
                OutputArray(ArrayMark, 0) = "RMERR"
        End Select
        OutputArray(ArrayMark, 1) = "1"
        OutputArray(ArrayMark, 2) = "0"
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    End If
    ' Visual Acuity (left)
    If (InStr(1, tempSubParse, "VL", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "VL", vbBinaryCompare)
        Select Case Trim(Mid$(tempSubParse, j + 2, 5))
            Case "15"
                OutputArray(ArrayMark, 0) = "RM15"
            Case "20"
                OutputArray(ArrayMark, 0) = "RM20"
            Case "25"
                OutputArray(ArrayMark, 0) = "RM25"
            Case "30"
                OutputArray(ArrayMark, 0) = "RM30"
            Case "40"
                OutputArray(ArrayMark, 0) = "RM40"
            Case "50"
                OutputArray(ArrayMark, 0) = "RM50"
            Case "60"
                OutputArray(ArrayMark, 0) = "RM60"
            Case "70"
                OutputArray(ArrayMark, 0) = "RM70"
            Case "80"
                OutputArray(ArrayMark, 0) = "RM80"
            Case "100"
                OutputArray(ArrayMark, 0) = "RM100"
            Case "150"
                OutputArray(ArrayMark, 0) = "RM150"
            Case "200"
                OutputArray(ArrayMark, 0) = "RM200"
            Case "400"
                OutputArray(ArrayMark, 0) = "RM400"
            Case "800"
                OutputArray(ArrayMark, 0) = "RM800"
            Case "FC1"
                OutputArray(ArrayMark, 0) = "RMFC1"
            Case "FC3"
                OutputArray(ArrayMark, 0) = "RMFC3"
            Case "HM"
                OutputArray(ArrayMark, 0) = "RMHM"
            Case "ERR"
                OutputArray(ArrayMark, 0) = "RMERR"
        End Select
        OutputArray(ArrayMark, 1) = "0"
        OutputArray(ArrayMark, 2) = "1"
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        RMdone = True
    End If
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "RMPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
    'End If
    If Not (RMdone) Then
        TestMark = TestMark - 1
    End If
    Return

ParseKR:
    If (InStr(i + 1, StringToParse, "*") <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "*") - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    'D data
    If (InStr(1, tempSubParse, "HR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "HL", vbBinaryCompare) <> 0) Then
        ' R1
        j = InStr(1, tempSubParse, "HR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR1"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 7, 5))
        j = InStr(1, tempSubParse, "HL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 7, 5))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Axis 1
        j = InStr(1, tempSubParse, "HR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAXI"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 12, 3))
        j = InStr(1, tempSubParse, "HL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 12, 3))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' R2
        j = InStr(1, tempSubParse, "VR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMR2"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 7, 5))
        j = InStr(1, tempSubParse, "VL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 7, 5))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Axis 2
        j = InStr(1, tempSubParse, "VR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "KMAXI2"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 12, 3))
        j = InStr(1, tempSubParse, "VL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 12, 3))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        KRdone = True
    End If
    'mm Data
'    If (InStr(1, tempSubParse, "CR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "CL", vbBinaryCompare)) Then
'        ' R1 - Using D data instead of mm HG
'        'j = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'        'OutputArray(ArrayMark, 0) = "KMR1"
'        'OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 5)
'        'j = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'        'OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 5)
'        'OutputArray(ArrayMark, 3) = Str(TestMark)
'        'ArrayMark = ArrayMark + 1
'        ' R2 - Using D data instead of mm HG
'        'j = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'        'OutputArray(ArrayMark, 0) = "KMR2"
'        'OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 7, 5)
'        'j = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'        'OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 7, 5)
'        'OutputArray(ArrayMark, 3) = Str(TestMark)
'        'ArrayMark = ArrayMark + 1
'        ' Axis
'        j = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 12, 3)
'        j = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 12, 3)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        KRdone = True
'    ElseIf (InStr(1, tempSubParse, "CR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "CL", vbBinaryCompare) = 0) Then
'        ' R1 - Using D data instead of mm HG
'        'j = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'        'OutputArray(ArrayMark, 0) = "KMR1"
'        'OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 2, 5)
'        'OutputArray(ArrayMark, 2) = ""
'        'OutputArray(ArrayMark, 3) = Str(TestMark)
'        'ArrayMark = ArrayMark + 1
'        ' R2 - Using D data instead of mm HG
'        'j = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'        'OutputArray(ArrayMark, 0) = "KMR2"
'        'OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 7, 5)
'        'OutputArray(ArrayMark, 2) = ""
'        'OutputArray(ArrayMark, 3) = Str(TestMark)
'        'ArrayMark = ArrayMark + 1
'        ' Axis
'        j = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 12, 3)
'        OutputArray(ArrayMark, 2) = ""
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        KRdone = True
'    ElseIf (InStr(1, tempSubParse, "CR", vbBinaryCompare) = 0 And InStr(1, tempSubParse, "CL", vbBinaryCompare) <> 0) Then
'        ' R1 - Using D data instead of mm HG
'        'j = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'        'OutputArray(ArrayMark, 0) = "KMR1"
'        'OutputArray(ArrayMark, 1) = ""
'        'OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 2, 5)
'        'OutputArray(ArrayMark, 3) = Str(TestMark)
'        'ArrayMark = ArrayMark + 1
'        ' R2 - Using D data instead of mm HG
'        'j = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'        'OutputArray(ArrayMark, 0) = "KMR2"
'        'OutputArray(ArrayMark, 1) = ""
'        'OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 7, 5)
'        'OutputArray(ArrayMark, 3) = Str(TestMark)
'        'ArrayMark = ArrayMark + 1
'        ' Axis
'        j = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        OutputArray(ArrayMark, 1) = ""
'        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 12, 3)
'        OutputArray(ArrayMark, 3) = Str(TestMark)
'        ArrayMark = ArrayMark + 1
'        KRdone = True
'    End If
    If Not (KRdone) Then
        TestMark = TestMark - 1
    End If
    Return
    
ParseCL:
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, "*") <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "*") - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    ' SCA
    If (InStr(1, tempSubParse, "FR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "FL", vbBinaryCompare) <> 0) Then
        ' Add the Sphere
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "LMSPH"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 2, 6))
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 2, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Cylinder
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "LMCYL"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 8, 6))
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 8, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add the Axis
        j = InStr(1, tempSubParse, "FR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "LMAXI"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 14, 3))
        j = InStr(1, tempSubParse, "FL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 14, 3))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    'ADD
    If (InStr(1, tempSubParse, "AR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "AL", vbBinaryCompare) <> 0) Then
        j = InStr(1, tempSubParse, "AR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "LMADD"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 2, 6))
        j = InStr(1, tempSubParse, "AL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 2, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    'Prism
    If (InStr(1, tempSubParse, "PR", vbBinaryCompare) <> 0 And InStr(1, tempSubParse, "PL", vbBinaryCompare) <> 0) Then
        ' Add H Prism
        j = InStr(1, tempSubParse, "PR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "LMPRH"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 2, 6))
        j = InStr(1, tempSubParse, "PL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 2, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ' Add V Prism
        j = InStr(1, tempSubParse, "PR", vbBinaryCompare)
        OutputArray(ArrayMark, 0) = "LMPRV"
        OutputArray(ArrayMark, 1) = Trim$(Mid$(tempSubParse, j + 8, 6))
        j = InStr(1, tempSubParse, "PL", vbBinaryCompare)
        OutputArray(ArrayMark, 2) = Trim$(Mid$(tempSubParse, j + 8, 6))
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        CVdone = True
    End If
    Return
End Function


