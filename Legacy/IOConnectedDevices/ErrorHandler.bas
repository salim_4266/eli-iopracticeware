Attribute VB_Name = "MErrorHandler"
Public sLogPath As String
Public Const DEBUG_LOG As String = "CDdebug.log"
Public Const ERROR_LOG As String = "CDerror.log"

Public Sub LogError(ByVal sModule As String, _
                    ByVal sMethod As String, _
                    ByVal iErr As Long, _
                    ByVal sDescription As String, _
                    Optional ByVal sExtraInfo As String = vbNullString)

Dim sErrorMessage As String
Dim sErrorLog     As String
Dim iFileNum      As Integer

    On Error Resume Next
    Err.Raise (iErr)
    sErrorLog = sLogPath & ERROR_LOG
    sErrorMessage = "Product: " & Trim$(App.ProductName) & vbTab _
        & "Revision: " & Trim$(App.Revision) & vbTab _
        & "Date: " & Now & vbTab _
        & "Host: " & Trim$(MySystemName) & vbTab _
        & "Module: " & sModule & vbTab _
        & "Method: " & sMethod & vbTab _
        & "Number: " & Str$(Err.Number) & vbTab _
        & "Description: " & sDescription & vbTab _
        & "Details: " & sExtraInfo & vbTab
    iFileNum = FreeFile
    Open sErrorLog For Append Access Write Shared As #iFileNum
    Print #iFileNum, sErrorMessage
    Close #iFileNum
    Err.Clear
    On Error GoTo 0

End Sub

Public Sub LogDebug(ByVal sModule As String, _
                    ByVal sMethod As String, _
                    ByVal iErr As Long, _
                    ByVal sDescription As String, _
                    Optional ByVal sExtraInfo As String = vbNullString)

Dim sErrorMessage As String
Dim sErrorLog     As String
Dim iFileNum      As Integer

    On Error Resume Next
    Err.Raise (iErr)
    sErrorLog = sLogPath & DEBUG_LOG
    sErrorMessage = "Product: " & Trim$(App.ProductName) & vbTab _
        & "Revision: " & Trim$(App.Revision) & vbTab _
        & "Date: " & Now & vbTab _
        & "Host: " & Trim$(MySystemName) & vbTab _
        & "Module: " & sModule & vbTab _
        & "Method: " & sMethod & vbTab _
        & "Number: " & Str$(Err.Number) & vbTab _
        & "Description: " & sDescription & vbTab _
        & "Details: " & sExtraInfo & vbTab
    iFileNum = FreeFile
    Open sErrorLog For Append Access Write Shared As #iFileNum
    Print #iFileNum, sErrorMessage
    Close #iFileNum
    Err.Clear
    On Error GoTo 0

End Sub

Public Sub LogDebugOnlyOutput(ByVal sOutput As String)

Dim sErrorLog     As String
Dim iFileNum      As Integer

    On Error Resume Next
    sErrorLog = sLogPath & DEBUG_LOG
    iFileNum = FreeFile
    Open sErrorLog For Append Access Write Shared As #iFileNum
    Print #iFileNum, sOutput
    Close #iFileNum
    Err.Clear
    On Error GoTo 0

End Sub

