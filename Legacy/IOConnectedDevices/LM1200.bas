Attribute VB_Name = "LM1200"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer                    'Record number of OutputArray
'Private TestMark As Integer                     'Test number
    
'Machine specific constants
Const strTestDelimiter As String = ""          'Chr(2) -- "STX"

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer                        'Counter for characters in string
    
    Dim tempSubParse As String
    
    Dim LMdone As Boolean                   'Lensmeter
    
'    Initialize variables
    LMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray

'    Debug.Print StringToParse
    
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
'            Select Case Mid$(StringToParse, i + 1, 2)
'                Case "LM"
                    LMdone = ParseLM(StringToParse, i)
                    If Not LMdone Then TestMark = TestMark - 1
'                Case Else
'                    Stop
'            End Select
'            Debug.Print TestMark
        End If
    Next i
    
    ParseMachineOutput = OutputArray
    numTests = TestMark
'    Debug.Print numTests

End Function


Function ParseLM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer                     'Position of indicator for Right Eye
    Dim intL As Integer                     'Position of indicator for Left Eye
    Dim intCR As Integer                    'Position of end of line
    Dim tempSubParse As String
    
'    Machine specific constants
    Const strSCAR As String = " R"          'SCA right eye
    Const strSCAL As String = " L"          'SCA left eye
    Const strAddR As String = "AR"          'add right eye
    Const strAddL As String = "AL"          'add left eye
    Const strPrismR As String = "PR"        'prism right eye
    Const strPrismL As String = "PL"        'prism left eye
    Const strObjD As String = "PD"          'pupil distance
    
    ParseLM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    ' SCA
    intR = InStr(1, tempSubParse, " R", vbBinaryCompare)
    intL = InStr(1, tempSubParse, " L", vbBinaryCompare)
    If intR + intL > 0 Then
    
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "LMSPH"
        ParseSubString intR, intL, tempSubParse, 2, 6
        
        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "LMCYL"
        ParseSubString intR, intL, tempSubParse, 8, 6
        
        ' Add the Axis
        OutputArray(ArrayMark, 0) = "LMAXI"
        ParseSubString intR, intL, tempSubParse, 14, 3
        
        ParseLM = True
    End If
    
    ' ADD
    intR = InStr(1, tempSubParse, "AR", vbBinaryCompare)
    intL = InStr(1, tempSubParse, "AL", vbBinaryCompare)
    If intR + intL > 0 Then
    
        OutputArray(ArrayMark, 0) = "LMADD"
        ParseSubString intR, intL, tempSubParse, 2, 5
        
        ParseLM = True
    End If
    
    ' Prism
    intR = InStr(1, tempSubParse, "PR", vbBinaryCompare)
    intL = InStr(1, tempSubParse, "PL", vbBinaryCompare)
    If intR + intL > 0 Then
    
        ' Add H Prism
        OutputArray(ArrayMark, 0) = "LMPRH"
        ParseSubString intR, intL, tempSubParse, 2, 6
        
        ' Add V Prism
        OutputArray(ArrayMark, 0) = "LMPRV"
        ParseSubString intR, intL, tempSubParse, 12, 6
        ParseLM = True
    End If
    
    ' PD
    intR = InStr(1, tempSubParse, strObjD, vbBinaryCompare)
    intCR = InStr(intR + 1, tempSubParse, vbCrLf, vbBinaryCompare)
    intR = InStr(intR + 6, tempSubParse, ".", vbTextCompare)
    If intR > 0 And intR < intCR Then
        intL = InStr(intR + 1, tempSubParse, ".", vbTextCompare)
        OutputArray(ArrayMark, 0) = "LMPD"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR - 2, 4)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, intL - 2, 4)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, 6, 4
'        ParseSubString intR, intL, tempSubParse, 10, 4
        ParseLM = True
    End If
    
    End Function
    
 Sub ParseSubString(ByVal intR As Integer, ByVal intL As Integer, ByVal tempSubParse As String, _
    ByVal intOffset As Integer, ByVal intLength As Integer)

        If intR > 0 Then
            OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR + intOffset, intLength)
        Else
            OutputArray(ArrayMark, 1) = ""
        End If

        If intL > 0 Then
            OutputArray(ArrayMark, 2) = Mid$(tempSubParse, intL + intOffset, intLength)
        Else
            OutputArray(ArrayMark, 2) = ""
        End If

        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1

    End Sub



