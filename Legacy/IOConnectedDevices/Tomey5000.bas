Attribute VB_Name = "Tomey5000"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer                    'Record number of OutputArray
'Private TestMark As Integer                     'Test number
    
'Machine specific constants
Private Const strTestDelimiter As String = "[FM_IF],"
Private Const strTestType1 As String = "REF"
Private Const strTestType2 As String = "KERATO"
Private intLengthDelimiter As Integer
Private intTestType1 As Integer
Private intTestType2 As Integer
Private aCommas(0 To 4) As Integer

Public Function ParseTomey5000Output(StringToParse As String, numTests As Integer) As String()
    
    Dim tempSubParse As String

    Dim i As Integer                        'Counter for characters in string
    
    Dim RMdone As Boolean                   'Autorefractometer  (Objective data)
    Dim KMdone As Boolean                   'Keratometer
    
'    Initialize variables
    RMdone = False
    KMdone = False
    intLengthDelimiter = Len(strTestDelimiter)
    intTestType1 = Len(strTestType1)
    intTestType2 = Len(strTestType2)
'    NTdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray

'    Debug.Print StringToParse
    
    For i = 1 To Len(StringToParse)
        If Mid(StringToParse, i, intLengthDelimiter) = strTestDelimiter Then
            If Mid(StringToParse, i + intLengthDelimiter, intTestType1) = strTestType1 Then
                RMdone = ParseRM(StringToParse, i)
                If Not RMdone Then TestMark = TestMark - 1
            ElseIf Mid(StringToParse, i + intLengthDelimiter, intTestType2) = strTestType2 Then
                KMdone = ParseKM(StringToParse, i)
                If Not KMdone Then TestMark = TestMark - 1
            End If
'            Debug.Print TestMark
        End If
    Next i
    
    ParseTomey5000Output = OutputArray
    numTests = TestMark
'    Debug.Print numTests
    Exit Function
End Function

Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer                         'Position of indicator for Right Eye
    Dim intL As Integer                         'Position of indicator for Left Eye
    Dim intCRR As Integer                        'Position of end of line
    Dim intCRL As Integer                        'Position of end of line
    Dim tempSubParse As String
    Dim strParseR As String
    Dim strParseL As String
    Dim j As Integer
    
'    Machine specific constants
    Const strObjSCAR As String = "POWER_R],*,"           'objective SCA right eye
    Const strObjSCAL As String = "POWER_L],*,"           'objective SCA left eye
    Const strCorVAR As String = "VR"            'corrected visual acuity right eye
    Const strCorVAL As String = "VL"            'corrected visual acuity left eye
    Const strPD As String = "PD"                'pupil distance
    
    ParseRM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    'SCA
    intR = InStr(1, tempSubParse, strObjSCAR, vbBinaryCompare)
    If (intR > 0) Then
        intCRR = InStr(intR, tempSubParse, vbCr, vbBinaryCompare)
        strParseR = Mid(tempSubParse, intR + 1, intCRR - intR - 1)
    End If
    intL = InStr(1, tempSubParse, strObjSCAL, vbBinaryCompare)
    If (intL > 0) Then
        intCRL = InStr(intL, tempSubParse, vbCr, vbBinaryCompare)
        strParseL = Mid(tempSubParse, intL + 1, intCRL - intL - 1)
    End If
    If (intR + intL > 0) Then
    
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "RMSPH"
        If (intR > 0) Then
            FindCommas strParseR
            Call ParseSubString(1, 0, strParseR, aCommas(1) + 1, aCommas(2) - aCommas(1) - 1)
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            Call ParseSubString(0, 1, strParseL, aCommas(1) + 1, aCommas(2) - aCommas(1) - 1)
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        
        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "RMCYL"
        If (intR > 0) Then
            FindCommas strParseR
            Call ParseSubString(1, 0, strParseR, aCommas(2) + 1, aCommas(3) - aCommas(2) - 1)
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            Call ParseSubString(0, 1, strParseL, aCommas(2) + 1, aCommas(3) - aCommas(2) - 1)
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, Len(strObjSCAR) + 6, 6
        
        ' Add the Axis
        OutputArray(ArrayMark, 0) = "RMAXI"
        If (intR > 0) Then
            FindCommas strParseR
            Call ParseSubString(1, 0, strParseR, aCommas(3) + 1, aCommas(4) - aCommas(3) - 1)
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            Call ParseSubString(0, 1, strParseL, aCommas(3) + 1, aCommas(4) - aCommas(3) - 1)
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, Len(strObjSCAR) + 6 + 6, 3
        
        ParseRM = True
    End If
    
    ' Visual Acuity -- i=1 ==>right  --  i=2 ==>left
    
    intR = InStr(1, tempSubParse, strCorVAR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strCorVAL, vbBinaryCompare)
    
    j = intR
    For i = 1 To 2
        If j > 0 Then
            Select Case Trim(Mid$(tempSubParse, j + 2, 5))
                Case "PLANO"
                    OutputArray(ArrayMark, 0) = "RMPLA"
                Case "BAL"
                    OutputArray(ArrayMark, 0) = "RMBL"
                Case "15"
                    OutputArray(ArrayMark, 0) = "RM15"
                Case "20"
                    OutputArray(ArrayMark, 0) = "RM20"
                Case "25"
                    OutputArray(ArrayMark, 0) = "RM25"
                Case "30"
                    OutputArray(ArrayMark, 0) = "RM30"
                Case "40"
                    OutputArray(ArrayMark, 0) = "RM40"
                Case "50"
                    OutputArray(ArrayMark, 0) = "RM50"
                Case "60"
                    OutputArray(ArrayMark, 0) = "RM60"
                Case "70"
                    OutputArray(ArrayMark, 0) = "RM70"
                Case "80"
                    OutputArray(ArrayMark, 0) = "RM80"
                Case "100"
                    OutputArray(ArrayMark, 0) = "RM100"
                Case "150"
                    OutputArray(ArrayMark, 0) = "RM150"
                Case "200"
                    OutputArray(ArrayMark, 0) = "RM200"
                Case "400"
                    OutputArray(ArrayMark, 0) = "RM400"
                Case "800"
                    OutputArray(ArrayMark, 0) = "RM800"
                Case "+1"
                    OutputArray(ArrayMark, 0) = "RM+1"
                Case "+2"
                    OutputArray(ArrayMark, 0) = "RM+2"
                Case "-1"
                    OutputArray(ArrayMark, 0) = "RM-1"
                Case "-2"
                    OutputArray(ArrayMark, 0) = "RM-2"
                Case "FC1"
                    OutputArray(ArrayMark, 0) = "RMFC1"
                Case "FC3"
                    OutputArray(ArrayMark, 0) = "RMFC3"
                Case "HM"
                    OutputArray(ArrayMark, 0) = "RMHM"
                Case "LP"
                    OutputArray(ArrayMark, 0) = "RMLP"
            End Select
            OutputArray(ArrayMark, 1) = CStr(CInt(True * (i = 1)))
            OutputArray(ArrayMark, 2) = CStr(CInt(True * (i = 2)))
            OutputArray(ArrayMark, 3) = Str(TestMark)
            ArrayMark = ArrayMark + 1
        ParseRM = True
        End If
        j = intL
    Next i
    
    ' PD    --  Binocular only
    intR = InStr(1, tempSubParse, strPD, vbBinaryCompare)
    intCRR = InStr(intR + 1, tempSubParse, vbCrLf, vbBinaryCompare)
'    intR = InStr(intR + 6, tempSubParse, ".", vbTextCompare)
'    If intR > 0 And intR < intCRR Then
    If (intR > 0) And (intCRR > intR + 4) Then
'        intL = InStr(intR + 1, tempSubParse, ".", vbTextCompare)
'        If intL > 2 Then
            OutputArray(ArrayMark, 0) = "RMPD"
            OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR + 4, 4)
'            OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR - 2, 4)
            OutputArray(ArrayMark, 2) = ""
            OutputArray(ArrayMark, 3) = Str(TestMark)
            ArrayMark = ArrayMark + 1
'           ParseSubString intR, intL, tempSubParse, 6, 4
'           ParseSubString intR, intL, tempSubParse, 10, 4
            ParseRM = True
'        End If
    End If
    
    End Function
    
    Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer                 'Position of indicator for Right Eye
    Dim intL As Integer                 'Position of indicator for Left Eye
    Dim intCRR As Integer                        'Position of end of line
    Dim intCRL As Integer                        'Position of end of line
    Dim tempSubParse As String
    Dim strParseR As String
    Dim strParseL As String
    Dim j As Integer
    
'    Machine specific constants
    Const strMmR As String = "CR"       'mm data right eye
    Const strMmL As String = "CL"       'Mm data left eye
    Const strKStartR As String = "INF_R],*,"
    Const strKStartL As String = "INF_L],*,"
    Const strK1 As String = "K1"
    Const strK2 As String = "K2"
    Const strKAVE As String = "AV"
    
    ParseKM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
'        Debug.Print tempSubParse
'    tempSubParseR = Mid(tempSubParse, InStr(i, tempSubParse, strKStartR, vbBinaryCompare))
'    tempSubParseL = Mid(tempSubParse, InStr(i, tempSubParse, strKStartL, vbBinaryCompare))
    
    'mm Data
'    intR = InStr(1, tempSubParse, strMmR, vbBinaryCompare)
'    intL = InStr(1, tempSubParse, strMmL, vbBinaryCompare)
'    If intR + intL > 0 Then
'
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        ParseSubString intR, intL, tempSubParse, 12, 3
'
'        ParseKM = True
'    End If
    
    'D data
    
        ' R1
    intR = InStr(1, tempSubParse, strKStartR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strKStartL, vbBinaryCompare)
    If (intR > 0) Then
        intR = InStr(intR + 1, tempSubParse, strK1, vbBinaryCompare)
        intCRR = InStr(intR, tempSubParse, vbCr, vbBinaryCompare)
        strParseR = Mid(tempSubParse, intR + 1, intCRR - intR - 1)
    End If
    If (intL > 0) Then
        intL = InStr(intL + 1, tempSubParse, strK1, vbBinaryCompare)
        intCRL = InStr(intL, tempSubParse, vbCr, vbBinaryCompare)
        strParseL = Mid(tempSubParse, intL + 1, intCRL - intL - 1)
    End If
    If (intR + intL > 0) Then
        OutputArray(ArrayMark, 0) = "KMR1"
        If (intR > 0) Then
            FindCommas strParseR
            ParseSubString 1, 0, strParseR, aCommas(1) + 1, aCommas(2) - aCommas(1) - 1
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            ParseSubString 0, 1, strParseL, aCommas(1) + 1, aCommas(2) - aCommas(1) - 1
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, Len(strK1) + 11, 5
        
        ParseKM = True
    End If
    
    intR = InStr(1, tempSubParse, strKStartR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strKStartL, vbBinaryCompare)
    If (intR > 0) Then
        intR = InStr(intR + 1, tempSubParse, strK2, vbBinaryCompare)
        intCRR = InStr(intR, tempSubParse, vbCr, vbBinaryCompare)
        strParseR = Mid(tempSubParse, intR + 1, intCRR - intR - 1)
    End If
    If (intL > 0) Then
        intL = InStr(intL + 1, tempSubParse, strK2, vbBinaryCompare)
        intCRL = InStr(intL, tempSubParse, vbCr, vbBinaryCompare)
        strParseL = Mid(tempSubParse, intL + 1, intCRL - intL - 1)
    End If
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMR2"
        If (intR > 0) Then
            FindCommas strParseR
            Call ParseSubString(1, 0, strParseR, aCommas(1) + 1, aCommas(2) - aCommas(1) - 1)
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            Call ParseSubString(0, 1, strParseL, aCommas(1) + 1, aCommas(2) - aCommas(1) - 1)
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, Len(strK2) + 11, 5
        
        ParseKM = True
    End If
    
    intR = InStr(1, tempSubParse, strKStartR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strKStartL, vbBinaryCompare)
    If (intR > 0) Then
        intR = InStr(intR + 1, tempSubParse, strKAVE, vbBinaryCompare)
        intCRR = InStr(intR, tempSubParse, vbCr, vbBinaryCompare)
        strParseR = Mid(tempSubParse, intR + 1, intCRR - intR - 1)
    End If
    If (intL > 0) Then
        intL = InStr(intL + 1, tempSubParse, strKAVE, vbBinaryCompare)
        intCRL = InStr(intL, tempSubParse, vbCr, vbBinaryCompare)
        strParseL = Mid(tempSubParse, intL + 1, intCRL - intL - 1)
    End If
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMAVE"
        If (intR > 0) Then
            FindCommas strParseR
            ParseSubString 1, 0, strParseR, aCommas(1) + 1, intCRR - aCommas(1) - 1
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            ParseSubString 0, 1, strParseL, aCommas(1) + 1, intCRL - aCommas(1) - 1
        Else
            OutputArray(ArrayMark, 2) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, Len(strK2) + 11, 5
        
        ParseKM = True
    End If
    
    intR = InStr(intR + 1, tempSubParse, "[CYL_R]", vbBinaryCompare)
    If (intR > 0) Then
        intCRR = InStr(intR, tempSubParse, vbCr, vbBinaryCompare)
        strParseR = Mid(tempSubParse, intR + 1, intCRR - intR - 1)
    End If
    intL = InStr(intL + 1, tempSubParse, "[CYL_L]", vbBinaryCompare)
    If (intL > 0) Then
        intCRL = InStr(intL, tempSubParse, vbCr, vbBinaryCompare)
        strParseL = Mid(tempSubParse, intL + 1, intCRL - intL - 1)
    End If
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMAXI"
        If (intR > 0) Then
            FindCommas strParseR
            ParseSubString 1, 0, strParseR, aCommas(1) + 1, intCRR - aCommas(1) - intR - 1
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        If (intL > 0) Then
            FindCommas strParseL
            ParseSubString 0, 1, strParseL, aCommas(1) + 1, intCRL - aCommas(1) - intL - 1
        Else
            OutputArray(ArrayMark, 1) = ""
        End If
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, 14, 3
        
        ParseKM = True
    End If
    
End Function

Sub FindCommas(ByVal strTemp As String)
    Dim i As Integer
    Dim idx As Integer
    
    Erase aCommas
    i = 0
    idx = 0
    Do
        i = InStr(i + 1, strTemp, ",", vbBinaryCompare)
        If i > 0 Then
            aCommas(idx) = i
            idx = idx + 1
        End If
    Loop While i > 0
End Sub
    
Private Sub ParseSubString(ByVal intR As Integer, ByVal intL As Integer, _
    ByVal strParse As String, ByVal intOffset As Integer, ByVal intLength As Integer)

    If intR > 0 Then
        OutputArray(ArrayMark, 1) = Trim(Mid$(strParse, intOffset, intLength))
'    ElseIf OutputArray(ArrayMark, 1) = "" Then
'        OutputArray(ArrayMark, 1) = ""
    End If

    If intL > 0 Then
        OutputArray(ArrayMark, 2) = Trim(Mid$(strParse, intOffset, intLength))
'    Else
'        OutputArray(ArrayMark, 2) = ""
    End If

End Sub
