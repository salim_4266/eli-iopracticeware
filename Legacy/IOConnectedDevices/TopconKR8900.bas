Attribute VB_Name = "TopConKR8900"
Option Explicit

'Machine specific constants
Const strTestDelimiter As String = "R"
Private oResults As CDeviceResults

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()


    Dim i As Integer

    Dim RMdone As Boolean                   'Autorefractometer  (Objective data)

    '    Initialize variables
    RMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray
    Dim arrTestValues() As String
    
    arrTestValues = Split(StringToParse, vbNewLine)
    
    For i = 1 To UBound(arrTestValues)
        If Trim$(arrTestValues(i)) = strTestDelimiter Then
            RMdone = ParseRM(StringToParse, i)
            If Not RMdone Then TestMark = TestMark - 1
        End If
    Next i
    ParseMachineOutput = OutputArray
    numTests = TestMark

    Exit Function
End Function
Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
     Dim iR As Long                     'Position of indicator for Right Eye
    Dim iL As Long                     'Position of indicator for Left Eye
    Dim tempSubParse As String

    '    Machine specific constants
    Const sSCAR As String = "R"          'SCA right eye
    Const sSCAL As String = "L"          'SCA left eye

    On Error GoTo lParseRM_Error

    TestMark = TestMark + 1
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)

    'Remove Extra spaces
    Call StripCharacters(tempSubParse, Chr(0))

    ' SCA
    iR = InStr(1, tempSubParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, tempSubParse, sSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "RMSPH"
        mdlParser.ParseSubString iR, iL, tempSubParse, 1, 8

        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "RMCYL"
        mdlParser.ParseSubString iR, iL, tempSubParse, 9, 6

        ' Add the Axis
        OutputArray(ArrayMark, 0) = "RMAXI"
        mdlParser.ParseSubString iR, iL, tempSubParse, 16, 5
    
        'PD
        'OutputArray(ArrayMark, 0) = "RMPD"
        'mdlParser.ParseSubString iR, iL, tempSubParse, 25, 6
        ParseRM = True
    End If
    
 
 
    Exit Function
lParseRM_Error:

    LogError "TopConKR8900", "ParseRM", Err, Err.Description

    End Function

Private Sub StripCharacters(TheText As String, StripChar As String)
    Dim i As Integer
    Dim NewText As String
    NewText = ""
    If Trim$(TheText) <> "" And Len(StripChar) > 0 Then
        For i = 1 To Len(TheText)
            If Mid$(TheText, i, 1) <> StripChar Then
                NewText = NewText & Mid$(TheText, i, 1)
            End If
        Next i
        TheText = NewText
    End If
End Sub
