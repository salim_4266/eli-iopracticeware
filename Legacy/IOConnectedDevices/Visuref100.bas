Attribute VB_Name = "Visuref100"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer                    'Record number of OutputArray
'Private TestMark As Integer                     'Test number
    
'Machine specific constants
Const strTestDelimiter As String = "A"

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer                        'Counter for characters in string
    
    Dim RMdone As Boolean                   'Autorefractometer  (Objective data)
    Dim KMdone As Boolean                   'Keratometer
    
'    Initialize variables
    RMdone = False
    KMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray
    
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            If Not RMdone Or Not KMdone Then
                RMdone = ParseRM(StringToParse, i)
                If Not RMdone Then TestMark = TestMark - 1
                KMdone = ParseKM(StringToParse, i)
                If Not KMdone Then TestMark = TestMark - 1
            End If
        End If
    Next i
    
    ParseMachineOutput = OutputArray
    numTests = TestMark
    Exit Function
End Function

Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim j As Integer
    
    ParseRM = False
    
    TestMark = TestMark + 1
    
    'SCA
    ' Add the Sphere
    OutputArray(ArrayMark, 0) = "RMSPH"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 49, 6)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 132, 6)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ' Add the Cylinder
    OutputArray(ArrayMark, 0) = "RMCYL"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 56, 6)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 139, 6)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ' Add the Axis
    OutputArray(ArrayMark, 0) = "RMAXI"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 63, 3)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 146, 3)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ParseRM = True
    
    ' PD
    ' OutputArray(ArrayMark, 0) = "RMPD"
    ' OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 41, 2)
    ' OutputArray(ArrayMark, 2) = ""
    ' OutputArray(ArrayMark, 3) = Str(TestMark)
    ' ArrayMark = ArrayMark + 1

        
    End Function
    
    Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean

    Dim j As Integer
    
    ParseKM = False
    
    TestMark = TestMark + 1

'
'    'mm Data
'    intR = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'    If intR + intL > 0 Then
'
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        ParseSubString intR, intL, tempSubParse, 12, 3
'
'        ParseKM = True
'    End If
    
    'D data
    
        ' R1
    OutputArray(ArrayMark, 0) = "KMR1"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 78, 5)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 161, 5)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1

    '    Axis

    OutputArray(ArrayMark, 0) = "KMAXI"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 84, 3)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 167, 3)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1

    
        ' R2

    OutputArray(ArrayMark, 0) = "KMR2"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 94, 5)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 177, 5)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
        ' Axis2

    OutputArray(ArrayMark, 0) = "KMAXI2"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 100, 3)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 183, 3)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
        ' Ave

    OutputArray(ArrayMark, 0) = "KMAVE"
    OutputArray(ArrayMark, 1) = Mid$(StringToParse, i + 110, 5)
    OutputArray(ArrayMark, 2) = Mid$(StringToParse, i + 193, 5)
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ParseKM = True
    
End Function



