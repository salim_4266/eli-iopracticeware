Attribute VB_Name = "DGH550"
Option Explicit
    
'Machine specific constants
Const strTestDelimiter As String = vbCrLf

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer                        'Counter for characters in string
    
    Dim tempSubParse As String
    
    Dim PMdone As Boolean                   'Lensmeter
    
'    Initialize variables
    PMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray

'    Debug.Print StringToParse
    
'    For i = 1 To Len(StringToParse)
'        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
'            Select Case Mid$(StringToParse, i + 1, 2)
'                Case "LM"
                    PMdone = ParsePM(StringToParse)
                    If Not PMdone Then TestMark = TestMark - 1
'                Case Else
'                    Stop
'            End Select
''            Debug.Print TestMark
'        End If
'    Next i
    
    ParseMachineOutput = OutputArray
    numTests = TestMark
'    Debug.Print numTests
    Exit Function
End Function

Function ParsePM(ByVal StringToParse As String) As Boolean

    Dim i As Integer
    Dim strVel As String
    Dim strNum As String
    Dim strAvg As String
    Dim strSTD As String
    Dim aMeasures() As String
    
    ParsePM = False
    
    TestMark = TestMark + 1
'    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
'        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
'    Else
'        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
'    End If

    
    'Velocity, Number of Measurements, Average, Standard Deviation
    
    strVel = Mid(StringToParse, 3, 4)
    strNum = Mid(StringToParse, 7, 2)
    strAvg = Mid(StringToParse, 9, 4)
    strSTD = Mid(StringToParse, 14, 3)

'    Check Number of measurements against string length

    If CLng(strNum) <> (Len(StringToParse) - 4 - 14) / 4 Then
        MsgBox "No measurements transferred"
        Exit Function
    Else
        ReDim aMeasures(0 To CLng(strNum) - 1)
        For i = 0 To CLng(strNum) - 1
            aMeasures(i) = Mid(StringToParse, 17 + 4 * i, 4)
        Next i
    End If
    
    
'    Fix needed
    
    OutputArray(ArrayMark, 0) = "PMPM"
'    If intIdx = 0 Then
'        OutputArray(ArrayMark, 1) = strAvg
'    ElseIf intIdx = 1 Then
'        OutputArray(ArrayMark, 2) = strAvg
'    End If
    
    OutputArray(ArrayMark, 3) = Str(TestMark)
    ArrayMark = ArrayMark + 1
    
    ParsePM = True
    
End Function


