VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNidekTonoref2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice

Private oResults As CDeviceResults
Private Const TEST_DELIMITER As String = "" ' Chr$(1) SOH - start of heading

Public Function IDevice_ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

Dim i As Integer
    
    On Error GoTo lIDevice_ParseMachineOutput_Error
    
    Set oResults = New CDeviceResults

    For i = 1 To Len(StringToParse)
        If Mid$(StringToParse, i, 1) = TEST_DELIMITER Then
            Select Case Mid$(StringToParse, i + 1, 3)
                Case "DRM"
                    oResults.NextTest
                    If Not ParseRM(StringToParse, i) Then
                        oResults.PrevTest
                    End If
                Case "DKM"
                    oResults.NextTest
                    If Not ParseKM(StringToParse, i) Then
                        oResults.PrevTest
                    End If
            End Select
        End If
    Next i
    
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CNidekTonoref2", "IDevice_ParseMachineOutput", Err, Err.Description
End Function

Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
Dim iR As Long                         'Position of indicator for Right Eye
Dim iL As Long                         'Position of indicator for Left Eye
Dim intCR As Integer                        'Position of end of line
Dim sParse As String
Dim j As Integer
Dim sName As String
    
'    Machine specific constants
    Const strObjSCAR As String = "OR"           'objective SCA right eye
    Const strObjSCAL As String = "OL"           'objective SCA left eye
    Const strAddR As String = "AR"              'add right eye
    Const strAddL As String = "AL"              'add left eye
    Const strCorVAR As String = "WR"            'corrected visual acuity right eye
    Const strCorVAL As String = "WL"            'corrected visual acuity left eye
    Const strPD As String = "PD"                'pupil distance
    
    sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    
    'SCA
    iR = InStr(1, sParse, strObjSCAR, vbBinaryCompare)
    iL = InStr(1, sParse, strObjSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        oResults.AddValues "RMSPH", sParse, iR, iL, 3, 6
        ' Add the Cylinder
        oResults.AddValues "RMCYL", sParse, iR, iL, 9, 6
        ' Add the Axis
        oResults.AddValues "RMAXI", sParse, iR, iL, 15, 3
        ParseRM = True
    End If
    
'    'ADD -- do we want this?
'    intR = InStr(1, tempSubParse, strSubjAR, vbBinaryCompare)
'    intL = InStr(1, tempSubParse, strSubjAL, vbBinaryCompare)
'    If intR + intL > 0 Then
'
'        OutputArray(ArrayMark, 0) = "RTADD"
'        mdlparser.ParseSubString intR, intL, tempSubParse, 2, 6
'
'        ParseRT = True
'    End If
    
    ' Visual Acuity - Right and Left always the same
    iR = InStr(1, sParse, strCorVAR, vbBinaryCompare)
    sName = PickRMVAButton(sParse, iR)
    If sName <> "" Then
        'Right
        oResults.AddValues sName, "10", 1, 2, 0, 1
        'Left
        oResults.AddValues sName, "01", 1, 2, 0, 1
        ParseRM = True
    End If
    
End Function
    
Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
Dim iR As Long                 'Position of indicator for Right Eye
Dim iL As Long                 'Position of indicator for Left Eye
Dim sParse As String
Dim j As Integer
    
'    Machine specific constants
'    Const strMmR As String = "CR"       'mm data right eye
'    Const strMmL As String = "CL"       'mm data left eye
    Const strDioR As String = " R"      'diopter data right eye
    Const strDioL As String = " L"      'diopter data left eye
    
    sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    
'    'mm Data
'    intR = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'    If intR + intL > 0 Then
'
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        mdlparser.ParseSubString intR, intL, tempSubParse, 12, 3
'
'        ParseKM = True
'    End If
    
    'D data
    
        ' R1
    iR = InStr(1, sParse, strDioR, vbBinaryCompare)
    iL = InStr(1, sParse, strDioL, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "KMR1", sParse, iR, iL, 2, 5
        oResults.AddValues "KMR2", sParse, iR, iL, 7, 5
        oResults.AddValues "KMAXI", sParse, iR, iL, 12, 3
        oResults.AddValues "KMAVE", sParse, iR, iL, 15, 5
        ParseKM = True
    End If
End Function

Private Function PickRMVAButton(sToParse As String, iPos As Long) As String
    Select Case Trim$(Mid$(sToParse, iPos + 3, 4))
        Case "PLANO"
            PickRMVAButton = "RMPLA"
        Case "BAL"
            PickRMVAButton = "RMBL"
        Case "15"
            PickRMVAButton = "RM15"
        Case "20"
            PickRMVAButton = "RM20"
        Case "25"
            PickRMVAButton = "RM25"
        Case "30"
            PickRMVAButton = "RM30"
        Case "40"
            PickRMVAButton = "RM40"
        Case "50"
            PickRMVAButton = "RM50"
        Case "60"
            PickRMVAButton = "RM60"
        Case "70"
            PickRMVAButton = "RM70"
        Case "80"
            PickRMVAButton = "RM80"
        Case "100"
            PickRMVAButton = "RM100"
        Case "150"
            PickRMVAButton = "RM150"
        Case "200"
            PickRMVAButton = "RM200"
        Case "400"
            PickRMVAButton = "RM400"
        Case "800"
            PickRMVAButton = "RM800"
        Case "+1"
            PickRMVAButton = "RM+1"
        Case "+2"
            PickRMVAButton = "RM+2"
        Case "-1"
            PickRMVAButton = "RM-1"
        Case "-2"
            PickRMVAButton = "RM-2"
        Case "FC1"
            PickRMVAButton = "RMFC1"
        Case "FC3"
            PickRMVAButton = "RMFC3"
        Case "HM"
            PickRMVAButton = "RMHM"
        Case "LP"
            PickRMVAButton = "RMLP"
    End Select
End Function
