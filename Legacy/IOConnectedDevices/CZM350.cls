VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCZM350"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IDevice


Private Function ParseLM(ByVal StringToParse As String, _
                        ByVal i As Integer) As Boolean


Dim intR         As Integer     'Position of indicator for Right Eye
Dim intL         As Integer     'Position of indicator for Left Eye
Dim intSPHR      As Integer
Dim intSPHL      As Integer
Dim intCYLR      As Integer
Dim intCYLL      As Integer
Dim intADDR      As Integer
Dim intADDL      As Integer

    'Dim intPRR As Integer
    '<:-) :WARNING: Unused Dim commented out
    'Dim intPRL As Integer
    '<:-) :WARNING: Unused Dim commented out
    'Dim intCR As Integer                    'Position of end of line
    '<:-) :WARNING: Unused Dim commented out
    'Dim iPD As Integer
    '<:-) :WARNING: Unused Dim commented out
Dim tempSubParse As String
    '    Machine specific constants
    '    Const strSph As String = "SPH:"        'Sphere
    '    Const strCyl As String = "CYL:"        'Cylinder
    '    Const strAxis As String = "*"          'Axis
    '    Const strAdd As String = "ADD:"        'Add
    '    Const strPrism As String = "PSM"       'Prism
    '    Const strObjD As String = "]R"         'pupil distance Right eye
    '    Const strObjD As String = "+L"         'pupil distance Left eye
    ParseLM = False
    TestMark = TestMark + 1
    tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    ' SCA
    intR = InStr(1, tempSubParse, "RIGHT", vbBinaryCompare)
    intL = InStr(1, tempSubParse, "LEFT", vbBinaryCompare)
    If intR + intL > 0 Then
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "LMSPH"
        intSPHR = InStr(intR + 1, tempSubParse, "SPH:", vbBinaryCompare)
        intSPHL = InStr(intL + 1, tempSubParse, "SPH:", vbBinaryCompare)
        If intSPHR = intSPHL Then
            intSPHR = 0
        End If '<:-) Structure Expanded.
        mdlParser.ParseSubString intSPHR, intSPHL, tempSubParse, 5, 6
        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "LMCYL"
        intCYLR = InStr(intR + 1, tempSubParse, "CYL:", vbBinaryCompare)
        intCYLL = InStr(intL + 1, tempSubParse, "CYL:", vbBinaryCompare)
        If intCYLR = intCYLL Then
            intCYLR = 0
        End If '<:-) Structure Expanded.
        mdlParser.ParseSubString intCYLR, intCYLL, tempSubParse, 5, 6
        ' Add the Axis
        OutputArray(ArrayMark, 0) = "LMAXI"
        mdlParser.ParseSubString intCYLR, intCYLL, tempSubParse, 14, 3
        ParseLM = True
    End If
    ' ADD
    intADDR = InStr(intR + 1, tempSubParse, "ADD:", vbBinaryCompare)
    intADDL = InStr(intL + 1, tempSubParse, "ADD:", vbBinaryCompare)
    If intADDR + intADDL > 0 Then
        If intADDR = intADDL Then
            intADDR = 0
        End If '<:-) Structure Expanded.
        OutputArray(ArrayMark, 0) = "LMADD"
        mdlParser.ParseSubString intADDR, intADDL, tempSubParse, 5, 6
        ParseLM = True
    End If
    ' Prism
    '    intPRR = InStr(intR + 1, tempSubParse, "PSM:", vbBinaryCompare)
    '    intPRL = InStr(intL + 1, tempSubParse, "PSM:", vbBinaryCompare)
    '    If intPRR + intPRL > 0 Then
    '        If intPRR = intPRL Then intPRR = 0
    '
    '        ' Add V Prism
    '        OutputArray(ArrayMark, 0) = "LMPRV"
    '        mdlParser.ParseSubString intPRR, intPRL, tempSubParse, 6, 5, 6
    '
    '        ' Add H Prism
    '        OutputArray(ArrayMark, 0) = "LMPRH"
    '        mdlParser.ParseSubString intPRR, intPRL, tempSubParse, 18, 5, 6
    '        ParseLM = True
    '    End If
    ' PD
    'intR = InStr(1, tempSubParse, "]R", vbBinaryCompare)
    'intL = InStr(intR + 1, tempSubParse, "+L", vbTextCompare)
    '    Fix needed on Mid$
    '    iPD = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    '    If iPD > 0 Then
    '        OutputArray(ArrayMark, 0) = "LMPD"
    '        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, iPD + 9, 3)
    '        OutputArray(ArrayMark, 2) = "0"
    '        OutputArray(ArrayMark, 3) = Str$(TestMark)
    '        ArrayMark = ArrayMark + 1
    '        ParseLM = True
    '    End If

End Function

Public Function IDevice_ParseMachineOutput(StringToParse As String, _
                                   numTests As Integer) As String()

Dim i      As Integer   'Counter for characters in string
Dim LMdone As Boolean   'Lensmeter
    '    Initialize variables
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray
    '    Debug.Print StringToParse
    For i = 1 To Len(StringToParse) - 7
        '        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
        Select Case Mid$(StringToParse, i, 6)
        Case "LA 350"
            LMdone = ParseLM(StringToParse, i)
            If Not LMdone Then
                TestMark = TestMark - 1
            End If '<:-) Structure Expanded.
        Case Else
            '<:-) :WARNING: Empty 'Case Else' structure could be removed
        End Select
        '            Debug.Print TestMark
        '        End If
    Next i
    IDevice_ParseMachineOutput = OutputArray
    numTests = TestMark

End Function


