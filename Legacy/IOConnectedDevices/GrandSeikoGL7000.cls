VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGrandSeikoGL7000"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice
    
Private oResults As CDeviceResults

Public Function IDevice_ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

Dim i As Integer
Dim LMdone As Boolean                   'Lensmeter
    
    On Error GoTo lIDevice_ParseMachineOutput_Error

    Set oResults = New CDeviceResults
    
    LMdone = ParseLM(StringToParse, 1)
    If Not LMdone Then
        oResults.PrevTest
    End If
    
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CGrandSeikoGL7000", "IDevice_ParseMachineOutput", Err, Err.Description

End Function


Private Function ParseLM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim iR As Long                     'Position of indicator for Right Eye
    Dim iL As Long                     'Position of indicator for Left Eye
    Dim sParse As String
    Dim sParseSubR As String
    Dim iSubLStart As Long
    
    
'    Machine specific constants
    Const sSubR As String = "<R>"
    Const sSubL As String = "<L>"
    Const sSphere As String = "SPH:"
    Const sCylinder As String = "CYL:"
    Const sAxis As String = "AXS:"
    Const sADD As String = "ADD:"
    Const sPrism As String = "PSM:"
    
    On Error GoTo lParseLM_Error
    
    oResults.NextTest

    ' We want to make sure that the R and L values don't get confused if one or the other is missing.
    sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    iSubLStart = InStr(1, sParse, sSubL)
    If iSubLStart > 0 Then
        sParseSubR = Mid$(sParse, 1, iSubLStart)
    Else
        sParseSubR = sParse
        iSubLStart = Len(sParse)
    End If
    
    ' Sphere
    iR = InStr(1, sParseSubR, sSphere, vbBinaryCompare)
    iL = InStr(iSubLStart, sParse, sSphere, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "LMSPH", sParse, iR, iL, 5, 6
        ParseLM = True
    End If
    
    ' Cylinder
    iR = InStr(1, sParseSubR, sCylinder, vbBinaryCompare)
    iL = InStr(iSubLStart, sParse, sCylinder, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "LMCYL", sParse, iR, iL, 5, 6
        ParseLM = True
    End If
    
    ' Axis
    iR = InStr(1, sParseSubR, sAxis, vbBinaryCompare)
    iL = InStr(iSubLStart, sParse, sAxis, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "LMAXI", sParse, iR, iL, 5, 6
        ParseLM = True
    End If
    
    ' ADD
    iR = InStr(1, sParseSubR, sADD, vbBinaryCompare)
    iL = InStr(iSubLStart, sParse, sADD, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "LMADD", sParse, iR, iL, 5, 6
        ParseLM = True
    End If

    
    ' Prism
    iR = InStr(1, sParseSubR, sPrism, vbBinaryCompare)
    iL = InStr(iSubLStart, sParse, sPrism, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Horizontal
        oResults.AddValues "LMPRH", sParse, iR, iL, 6, 5
        ' Vertical
        oResults.AddValues "LMPRV", sParse, iR, iL, 13, 5
        ParseLM = True
    End If

    Exit Function

lParseLM_Error:

    LogError "CGrandSeikoGL7000", "ParseLM", Err, Err.Description
    
End Function


