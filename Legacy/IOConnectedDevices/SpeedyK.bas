Attribute VB_Name = "SpeedyK"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer
'Private TestMark As Integer

Public Function ParseSpeedyKOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer
    Dim j As Integer
    
    Dim tempSubParse As String
    
    Dim RTdone As Boolean
    Dim LMdone As Boolean
    Dim RMdone As Boolean
    Dim KMdone As Boolean
    Dim NTdone As Boolean
    
    RTdone = False
    LMdone = False
    RMdone = False
    KMdone = False
    NTdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray
'    Debug.Print StringToParse
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = "@") Then
            Select Case Mid$(StringToParse, i + 1, 2)
                Case "RM"
                    RMdone = ParseRM(StringToParse, i)
                    If Not RMdone Then TestMark = TestMark - 1
                Case "KM"
                    KMdone = ParseKM(StringToParse, i)
                    If Not KMdone Then TestMark = TestMark - 1
                Case Else
'                    Stop
            End Select
'            Debug.Print TestMark
        End If
    Next i
    ParseSpeedyKOutput = OutputArray
    numTests = TestMark
'    Debug.Print numTests
    Exit Function
End Function

Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer
    Dim intL As Integer
    Dim tempSubParse As String
    Dim j As Integer
    Dim intRStar As Integer
    Dim intLStar As Integer
    
    ParseRM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, "@") <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "@") - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    'SCA
    intR = InStr(1, tempSubParse, "OR", vbBinaryCompare)
    intL = InStr(1, tempSubParse, "OL", vbBinaryCompare)
    intRStar = InStr(intR, tempSubParse, "*", vbTextCompare)
    intLStar = InStr(intL, tempSubParse, "*", vbTextCompare)
    If intRStar > intR And intRStar < intL Then intR = intRStar
    If intLStar > intL Then intL = intLStar
    If intR + intL > 0 Then
    
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "RMSPH"
        mdlParser.ParseSubString intR, intL, tempSubParse, 2, 6
        
        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "RMCYL"
        mdlParser.ParseSubString intR, intL, tempSubParse, 8, 6
        
        ' Add the Axis
        OutputArray(ArrayMark, 0) = "RMAXI"
        mdlParser.ParseSubString intR, intL, tempSubParse, 14, 3
        
        ParseRM = True
    End If
    
    ' PD
    Dim intR1 As Integer
    intR = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    intR1 = intR
    intR = InStr(intR + 6, tempSubParse, ".", vbTextCompare)
    If intR > 0 Then
        intL = InStr(intR + 1, tempSubParse, ".", vbTextCompare)
        OutputArray(ArrayMark, 0) = "RMPD"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR - 2, 4)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, intL - 2, 4)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
        ParseRM = True
    Else
        intR = intR1
        OutputArray(ArrayMark, 0) = "RMPD"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR - 2, 4)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, 6, 4
'        ParseSubString intR, intL, tempSubParse, 10, 4
        ParseRM = True
    End If
    
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "RMPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
'    ParseRM = True
    'End If
'    If Not (RMdone) Then
'        TestMark = TestMark - 1
'    End If
    
    End Function
    
    Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer
    Dim intL As Integer
    Dim tempSubParse As String
    Dim j As Integer
    Dim sODR1 As Single
    Dim sODR2 As Single
    Dim sOSR1 As Single
    Dim sOSR2 As Single
    sODR1 = 0
    sODR2 = 0
    sOSR1 = 0
    sOSR2 = 0
    
    ParseKM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, "@") <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "@") - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If

    intR = InStr(1, tempSubParse, "CR", vbBinaryCompare)
    intL = InStr(1, tempSubParse, "CL", vbBinaryCompare)
    
'    axis
    
    If intR + intL > 0 Then
        
        OutputArray(ArrayMark, 0) = "KMAXI"
        mdlParser.ParseSubString intR, intL, tempSubParse, 12, 3
        
        ParseKM = True
    End If
    
    'mm Data
    
'    If intR + intL > 0 Then
'        OutputArray(ArrayMark, 0) = "KMR1"
'        ParseSubString intR, intL, tempSubParse, 2, 5
'
'        ParseKM = True
'    End If
'
'        ' R2
'    If intR + intL > 0 Then
'        OutputArray(ArrayMark, 0) = "KMR2"
'        ParseSubString intR, intL, tempSubParse, 15, 5
'
'        ParseKM = True
'    End If
    
    'D data
    
        ' R1
'    intR = InStr(1, tempSubParse, "DR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "DL", vbBinaryCompare)
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMR1"
        mdlParser.ParseSubString intR, intL, tempSubParse, 7, 5
        ParseKM = True
        sODR1 = OutputArray(ArrayMark - 1, 1)
        sOSR1 = OutputArray(ArrayMark - 1, 2)
    End If
    
        ' R2
'    intR = InStr(1, tempSubParse, "DR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "DL", vbBinaryCompare)
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMR2"
        mdlParser.ParseSubString intR, intL, tempSubParse, 20, 5
        ParseKM = True
        sODR2 = OutputArray(ArrayMark - 1, 1)
        sODR2 = OutputArray(ArrayMark - 1, 2)
    End If
    
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMAXI2"
        mdlParser.ParseSubString intR, intL, tempSubParse, 25, 3
        ParseKM = True
    End If
    
        ' AVE -- NO AVE D data, only mm
'    intR = InStr(1, tempSubParse, strDioR, vbBinaryCompare)
'    intL = InStr(1, tempSubParse, strDiol, vbBinaryCompare)
'    If intR + intL > 0 Then
'        OutputArray(ArrayMark, 0) = "KMAVE"
'        ParseSubString intR, intL, tempSubParse, 15, 5
'
'        ParseKM = True
'    End If

    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMCYL"
        'Left out the sign for doctors who want the cylinder to be positive.
        mdlParser.ParseSubString intR, intL, tempSubParse, 33, 5
        ParseKM = True
    End If
    
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMAVE"
        OutputArray(ArrayMark, 1) = Round((sODR1 + sODR2) / 2, 2)
        OutputArray(ArrayMark, 2) = Round((sOSR1 + sOSR2) / 2, 2)
        ArrayMark = ArrayMark + 1
    End If
    
End Function


