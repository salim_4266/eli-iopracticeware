Attribute VB_Name = "TopconRMA6000"
Option Explicit

'Private OutputArray(0 To 50, 0 To 3) As String
'Private ArrayMark As Integer                    'Record number of OutputArray
'Private TestMark As Integer                     'Test number
    
'Machine specific constants
Const strTestDelimiter As String = ""          'Chr(1)

Public Function ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

    Dim i As Integer                        'Counter for characters in string
    
    Dim tempSubParse As String
    
    Dim RMdone As Boolean                   'Autorefractometer  (Objective data)
    Dim KMdone As Boolean                   'Keratometer
    
'    Initialize variables
    RMdone = False
    KMdone = False
    ArrayMark = 0
    TestMark = 0
    Erase OutputArray


'    Debug.Print StringToParse
    
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            Select Case Mid$(StringToParse, i + 1, 2)
                Case "RM"
                    RMdone = ParseRM(StringToParse, i)
                    If Not RMdone Then TestMark = TestMark - 1
'                Case "KM"
'                    KMdone = ParseKM(StringToParse, i)
'                    If Not KMdone Then TestMark = TestMark - 1
                Case Else
'                    Stop
            End Select
'            Debug.Print TestMark
        End If
    Next i
    
    ParseMachineOutput = OutputArray
    numTests = TestMark
'    Debug.Print numTests
    Exit Function
End Function

Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer                         'Position of indicator for Right Eye
    Dim intL As Integer                         'Position of indicator for Left Eye
    Dim intCR As Integer                        'Position of end of line
    Dim tempSubParse As String
    Dim j As Integer
    
'    Machine specific constants
    Const strObjSCAR As String = "OR"           'objective SCA right eye
    Const strObjSCAL As String = "OL"           'objective SCA left eye
    Const strPD As String = "PD"                'pupil distance
    
    ParseRM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    'SCA
    intR = InStr(1, tempSubParse, strObjSCAR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strObjSCAL, vbBinaryCompare)
    If intR + intL > 0 Then
    
        ' Add the Sphere
        OutputArray(ArrayMark, 0) = "RMSPH"
        mdlParser.ParseSubString intR, intL, tempSubParse, 2, 6
        
        ' Add the Cylinder
        OutputArray(ArrayMark, 0) = "RMCYL"
        mdlParser.ParseSubString intR, intL, tempSubParse, 8, 6
        
        ' Add the Axis
        OutputArray(ArrayMark, 0) = "RMAXI"
        mdlParser.ParseSubString intR, intL, tempSubParse, 14, 3
        
        ParseRM = True
    End If
    
    ' PD
    intR = InStr(1, tempSubParse, strPD, vbBinaryCompare)
    intCR = InStr(intR + 1, tempSubParse, vbCrLf, vbBinaryCompare)
    intR = InStr(intR + 6, tempSubParse, ".", vbTextCompare)
    If intR > 0 And intR < intCR Then
        intL = InStr(intR + 1, tempSubParse, ".", vbTextCompare)
        OutputArray(ArrayMark, 0) = "RMPD"
        OutputArray(ArrayMark, 1) = Mid$(tempSubParse, intR - 2, 4)
        OutputArray(ArrayMark, 2) = Mid$(tempSubParse, intL - 2, 4)
        OutputArray(ArrayMark, 3) = Str(TestMark)
        ArrayMark = ArrayMark + 1
'        ParseSubString intR, intL, tempSubParse, 6, 4
'        ParseSubString intR, intL, tempSubParse, 10, 4
        ParseRM = True
    End If
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "RMPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
'    ParseRM = True
    'End If
'    If Not (RMdone) Then
'        TestMark = TestMark - 1
'    End If
    
    End Function
    
    Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim intR As Integer                 'Position of indicator for Right Eye
    Dim intL As Integer                 'Position of indicator for Left Eye
    Dim tempSubParse As String
    Dim j As Integer
    
'    Machine specific constants
'    Const strNmR As String = "CR"       'nm data right eye
'    Const strNmL As String = "CL"       'nm data left eye
    Const strDioR As String = "CR"      'diopter data right eye
    Const strDioL As String = "CL"      'diopter data left eye
    
    ParseKM = False
    
    TestMark = TestMark + 1
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
'
'    'mm Data
'    intR = InStr(1, tempSubParse, "CR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "CL", vbBinaryCompare)
'    If intR + intL > 0 Then
'
'        OutputArray(ArrayMark, 0) = "KMAXI"
'        ParseSubString intR, intL, tempSubParse, 12, 3
'
'        ParseKM = True
'    End If
    
    'D data
    
        ' R1
    intR = InStr(1, tempSubParse, strDioR, vbBinaryCompare)
    intL = InStr(1, tempSubParse, strDioL, vbBinaryCompare)
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMR1"
        mdlParser.ParseSubString intR, intL, tempSubParse, 2, 5
        
        ParseKM = True
    End If

'    Axis

    If intR + intL > 0 Then

        OutputArray(ArrayMark, 0) = "KMAXI"
        mdlParser.ParseSubString intR, intL, tempSubParse, 12, 3

        ParseKM = True
    End If

    
        ' R2
'    intR = InStr(1, tempSubParse, "DR", vbBinaryCompare)
'    intL = InStr(1, tempSubParse, "DL", vbBinaryCompare)
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMR2"
        mdlParser.ParseSubString intR, intL, tempSubParse, 15, 5
        
        ParseKM = True
    End If
    
        ' AVE
'    intR = InStr(1, tempSubParse, strDioR, vbBinaryCompare)
'    intL = InStr(1, tempSubParse, strDiol, vbBinaryCompare)
    If intR + intL > 0 Then
        OutputArray(ArrayMark, 0) = "KMAVE"
        mdlParser.ParseSubString intR, intL, tempSubParse, 28, 5
        
        ParseKM = True
    End If
    
End Function


