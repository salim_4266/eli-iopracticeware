Attribute VB_Name = "MConnectedDevices"
Option Explicit

Public Enum EStatus
    esDone = 0
    esNoDataRead = 1
    esCommPortUnavailable = 2
    esDeviceNotConfigured = 3
    esParserNotAvailable = 4
End Enum
