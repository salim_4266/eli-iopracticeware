VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RetinomaxK3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice

Private oResults As CDeviceResults
Const strTestDelimiter As String = "*"

Public Function IDevice_ParseMachineOutput(StringToParse As String, numTests As Integer) As String()
Dim i As Integer                        'Counter for characters in string
Dim done As Boolean                   'Lensmeter
    
    On Error GoTo lIDevice_ParseMachineOutput_Error
    Set oResults = New CDeviceResults
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            done = ParseK3(StringToParse, i)
            If Not done Then
                oResults.PrevTest
            End If
            Exit For
        End If
    Next i
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing
    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "RetinomaxK3", "IDevice_ParseMachineOutput", Err, Err.Description
End Function

Private Function ParseK3(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
Dim iTimeStart As Long
Dim intR As Long                     'Position of indicator for Right Eye
Dim intL As Long                     'Position of indicator for Left Eye
Dim iLineAfterOS As Long
Dim tempSubParse As String
    
    On Error GoTo lParseK3_Error

    ParseK3 = False
    oResults.NextTest
    If (InStr(i + 1, StringToParse, strTestDelimiter) <> 0) Then
        tempSubParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, strTestDelimiter) - i)
    Else
        tempSubParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    ' SCA
    intR = InStr(1, tempSubParse, "R" + Chr$(13), vbBinaryCompare)
    intL = InStr(1, tempSubParse, "L" + Chr$(13), vbBinaryCompare)

    If intR + intL > 0 Then
        ' Add the Sphere
        oResults.AddValues "RMSPH", tempSubParse, intR, intL, 2, 6
        ' Add the Cylinder
        oResults.AddValues "RMCYL", tempSubParse, intR, intL, 9, 6
        ' Add the Axis
        oResults.AddValues "RMAXI", tempSubParse, intR, intL, 16, 3
        ParseK3 = True
    End If

    Exit Function

lParseK3_Error:

    LogError "RetinomaxK3", "ParseK3", Err, Err.Description
    
End Function



