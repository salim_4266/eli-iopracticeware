VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ConnectedDevices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' resultStatuses: 0-Done, 1-NoDataRead, 2-CommPortUnavailable, 3-DeviceNotConfigured
Public resultStatus As Integer
Public numTestsDone As Integer

'Public intPair As Integer
'Public blnAppendData As Boolean
Public intIdx As Integer
Public blnAppend As Boolean

Private myDevice As String
Private mySettings As String
Private myPort As String
Private ParsedOutput() As String
Private debugMe As Boolean

Public Property Get TotalDevices() As Integer
    ' Deprecated
End Property

Private Sub Class_Initialize()
    numTestsDone = 0
    resultStatus = EStatus.esNoDataRead
    debugMe = True
    sLogPath = App.Path & "\"
End Sub


Public Function ListAllDevices() As String()
    'Deprecated
End Function

Public Function GetCommSettings(ByVal DeviceName As String) As String
    'Deprecated
End Function

Public Sub ParseDeviceOutput(DeviceName As String, OutputData As String)
    Dim noArray(1, 3) As String
    Dim oDevice As IDevice
    
    On Error GoTo lParseDeviceOutput_Error

    noArray(0, 0) = "No device"
    noArray(0, 1) = "No device"
    noArray(0, 2) = "No device"
    noArray(0, 3) = "No device"
    ParsedOutput = noArray
    
'intPair = intIdx
'blnAppendData = blnAppend
    If OutputData <> "" Then
        FStatus.Status = "Parsing diagnostic data"
        Select Case DeviceName
            Case "CanonRKF1"
                Set oDevice = New CCanonRKF1
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "GrandSeikoGL7000"
                Set oDevice = New CGrandSeikoGL7000
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "NidekRT2100", "NidekRT5100"
                Set oDevice = New CNidekRT2100
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "NidekLM1000"
                Set oDevice = New CNidekLM1000
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "NidekTonoref2"
                Set oDevice = New CNidekTonoref2
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "Tomey5000"
                ParsedOutput = Tomey5000.ParseTomey5000Output(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconCV3000"
                ParsedOutput = TopconCV3000.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconSTD1"
                Set oDevice = New CTopconSTD1
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconNewFormat"
                ParsedOutput = TopconNewFormat.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconKR8000"
                ParsedOutput = TopconKR8000.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconKR9000Diopter"
                'Created a new .bas file for TopconKR9000 to fix #3405
                ParsedOutput = TopconKR9000Diopters.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconKR9000mm"
                ParsedOutput = TopconKR9000mm.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconKR8800"
                ParsedOutput = TopconKR8800.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopConKR88002"
                ParsedOutput = TopConKR88002.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconKR8900"
                ParsedOutput = TopConKR8900.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconCL200A"
                ParsedOutput = TopconCL200A.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconRMA6000"
                ParsedOutput = TopconRMA6000.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconRMA7000"
                ParsedOutput = TopconRMA7000.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TopconRM8900"
                Set oDevice = New CTopconRM8900
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "SpeedyK"
                ParsedOutput = SpeedyK.ParseSpeedyKOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK30"
                ParsedOutput = ARK30.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK530A"
                ParsedOutput = ARK530A.ParseARK530AOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK700A"
                ParsedOutput = ARK700A.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK760A"
                Set oDevice = New CARK760A
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK900"
                ParsedOutput = ARK900.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK10000"
                ParsedOutput = ARK10000.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK2000"
                ParsedOutput = ARK2000.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "LM1200"
                ParsedOutput = LM1200.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "VL3000Spectrum"
                ParsedOutput = VL3000Spectrum.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "DGH550"
                ParsedOutput = DGH550.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "CZM340", "CZM350", "CZM360"
                Set oDevice = New CCZM350
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "CZM599"
                Set oDevice = New CCZM599
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "HFA740", "HFA745", "HFA750"
                ParsedOutput = HFA7XX.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "VeloCLM"
                Set oDevice = New CVeloCLM
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK560A"
                Set oDevice = New CARK560A
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "TomeyTL2000B"
                ParsedOutput = TomeyTL2000B.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "RetinomaxK3"
                Set oDevice = New RetinomaxK3
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "ARK700ANEW"
                Set oDevice = New CARK700ANEW
                ParsedOutput = oDevice.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case "Visuref100"
                ParsedOutput = Visuref100.ParseMachineOutput(OutputData, numTestsDone)
                resultStatus = EStatus.esDone
            Case Else
                LogError "ConnectedDevices", "ParseDeviceOutput", 0, "", "Device " & DeviceName & " not available in list."
                resultStatus = EStatus.esParserNotAvailable
        End Select
    End If
    LogDebug "ConnectedDevices", "ParseDeviceOutput", 0, "", "End of Select - device = " & DeviceName
    Set oDevice = Nothing
    If (debugMe) Then
        Dim i As Integer
        For i = 0 To UBound(ParsedOutput, 1)
            If (ParsedOutput(i, 0) <> "") Then
                LogDebugOnlyOutput ParsedOutput(i, 0) & ">" & ParsedOutput(i, 1) & "&" & ParsedOutput(i, 2) & "<" & ParsedOutput(i, 3)
            End If
        Next i
    End If


    Exit Sub

lParseDeviceOutput_Error:

    LogError "ConnectedDevices", "ParseDeviceOutput", Err, Err.Description, Str$(UBound(ParsedOutput, 1))
    
End Sub

Public Sub GetDeviceData()
    numTestsDone = 0
    resultStatus = EStatus.esNoDataRead
    Erase ParsedOutput
    Set frmIOCD.IOCDClass = Me
    frmIOCD.ReceivedData = ""
    frmIOCD.CheckDevices
    FStatus.Status = "Waiting for data transfer to complete"
    Load frmIOCD
    FStatus.Show 1
End Sub

Public Property Get DeviceOutput(testIndex As Integer) As String()
Dim i As Integer
Dim j As Integer
Dim rowCount As Integer
Dim tempValues() As String
ReDim tempValues(UBound(ParsedOutput, 1), 2)
If (testIndex <= numTestsDone) Then
    rowCount = 0
    For i = 0 To UBound(ParsedOutput, 1)
        If (Val(ParsedOutput(i, 3)) = testIndex) Then
            For j = 0 To 2
                tempValues(rowCount, j) = ParsedOutput(i, j)
            Next j
            rowCount = rowCount + 1
        End If
    Next i
    DeviceOutput = tempValues
Else
    ReDim DeviceOutput(0, 2)
    tempValues(0, 0) = "Empty"
    DeviceOutput = tempValues
End If
End Property

Public Sub SetDummyData()
    Dim tempValues(9, 3) As String
    resultStatus = EStatus.esDone
    numTestsDone = 2
    tempValues(0, 0) = "RMSPH"
    tempValues(0, 1) = "- 0.50"
    tempValues(0, 2) = "- 0.25"
    tempValues(0, 3) = "1"
    tempValues(1, 0) = "RMCYL"
    tempValues(1, 1) = "- 0.75"
    tempValues(1, 2) = "- 0.50"
    tempValues(1, 3) = "1"
    tempValues(2, 0) = "RMAXI"
    tempValues(2, 1) = " 93"
    tempValues(2, 2) = "124"
    tempValues(2, 3) = "1"
    tempValues(3, 0) = "RM20"
    tempValues(3, 1) = "1"
    tempValues(3, 2) = "0"
    tempValues(3, 3) = "1"
    tempValues(4, 0) = "RM20"
    tempValues(4, 1) = "0"
    tempValues(4, 2) = "1"
    tempValues(4, 3) = "1"
    tempValues(5, 0) = "RMPD"
    tempValues(5, 1) = "    "
    tempValues(5, 2) = "    "
    tempValues(5, 3) = "1"
    tempValues(6, 0) = "LMSPH"
    tempValues(6, 1) = "- 1.00"
    tempValues(6, 2) = "- 1.00"
    tempValues(6, 3) = "2"
    tempValues(7, 0) = "LMCYL"
    tempValues(7, 1) = "- 0.25"
    tempValues(7, 2) = "  0.00"
    tempValues(7, 3) = "2"
    tempValues(8, 0) = "LMAXI"
    tempValues(8, 1) = " 81"
    tempValues(8, 2) = "  0"
    tempValues(8, 3) = "2"
    tempValues(9, 0) = "LMPD"
    tempValues(9, 1) = "    "
    tempValues(9, 2) = "    "
    tempValues(9, 3) = "2"
    ParsedOutput = tempValues
End Sub
