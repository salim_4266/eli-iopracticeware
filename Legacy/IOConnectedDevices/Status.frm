VERSION 5.00
Begin VB.Form FStatus 
   BackColor       =   &H0077742D&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Diagnostic Machines"
   ClientHeight    =   2655
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2655
   ScaleWidth      =   5685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstDevices 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Visible         =   0   'False
      Width           =   5415
   End
   Begin VB.Label lblStatusMessage 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Waiting for data transfer to complete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1455
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   4815
   End
End
Attribute VB_Name = "FStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sStatus As String
Private m_iDeviceChoice As Integer

Public Property Let Status(ByVal sStatus As String)

    m_sStatus = sStatus
    lblStatusMessage.Caption = m_sStatus

End Property

Public Property Get DeviceChoice() As Integer
    DeviceChoice = m_iDeviceChoice
End Property

Private Sub lstDevices_Click()
    m_iDeviceChoice = lstDevices.ListIndex + 1
    lstDevices.Visible = False
    m_sStatus = "Waiting for data transfer to complete"
    DoEvents
    Unload Me
End Sub

Public Sub ShowDeviceList(deviceCount As Integer)
Dim sDeviceName As String
Dim iDeviceCounter As Integer

    On Error GoTo lShowDeviceList_Error

    lstDevices.Clear
    For iDeviceCounter = 1 To deviceCount
        If WinRegLib.QueryHKLMValue("DeviceName" & Trim(Str(iDeviceCounter)), sDeviceName) Then
            lstDevices.AddItem sDeviceName
        End If
    Next iDeviceCounter
    lstDevices.Visible = True
    DoEvents

    Exit Sub

lShowDeviceList_Error:

    LogError "FStatus", "ShowDeviceList", Err, Err.Description
End Sub
