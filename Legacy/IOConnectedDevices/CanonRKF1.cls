VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCanonRKF1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice
' Japan Medical-Optical Equipment Industrial Association format by Canon
' Format 4 on RK-F1
    
Private oResults As CDeviceResults

Public Function IDevice_ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

Dim i As Integer

    On Error GoTo lIDevice_ParseMachineOutput_Error
    
    Set oResults = New CDeviceResults

    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = "@") Then
            Select Case Mid$(StringToParse, i + 1, 2)
                Case "RT"
                    oResults.NextTest
                    If Not ParseRT(StringToParse, i) Then
                        oResults.PrevTest
                    End If
                Case "LM"
                    oResults.NextTest
                    If Not ParseLM(StringToParse, i) Then
                        oResults.PrevTest
                    End If
                Case "RM"
                    oResults.NextTest
                    If Not ParseRM(StringToParse, i) Then
                        oResults.PrevTest
                    End If
                Case "KM"
                    oResults.NextTest
                    If Not ParseKM(StringToParse, i) Then
                        oResults.PrevTest
                    End If
                Case "NT"
                    oResults.NextTest
                    If Not ParseNT(StringToParse, i) Then
                        oResults.PrevTest
                    End If
            End Select
        End If
    Next i

    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing
    
    Exit Function

' Each of these parsing routines grabs the data for a specific test and creates
' a 3 column array with control tag (ICDAlias4 in DF), OD data and OS data.

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CCanonRKF1", "IDevice_ParseMachineOutput", Err, Err.Description

End Function

Private Function ParseRT(ByVal StringToParse As String, ByVal i As Integer) As Boolean

Dim iR As Long                     'Position of indicator for Right Eye
Dim iL As Long                     'Position of indicator for Left Eye
Dim sParse As String
Dim sName As String

'    Machine specific constants
Const sSCAR As String = "fR"          'SCA right eye
Const sSCAL As String = "fL"          'SCA left eye
Const sVAR As String = "vR"
Const sVAL As String = "vL"
Const sAddR As String = "aR"        'add right eye
Const sAddL As String = "aL"        'add left eye
Const sVAB As String = "vB"
Const sPrismR As String = "pR"      'prism right eye
Const sPrismL As String = "pL"      'prism left eye
Const sPD As String = "pD"        'pupil distance

    On Error GoTo lParseRT_Error
    
    If (InStr(i + 1, StringToParse, "@") <> 0) Then
        sParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "@") - i)
    Else
        sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    ' SCA
    iR = InStr(1, sParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, sParse, sSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        oResults.AddValues "RTSPH", sParse, iR, iL, 2, 6
        ' Add the Cylinder
        oResults.AddValues "RTCYL", sParse, iR, iL, 8, 6
        ' Add the Axis
        oResults.AddValues "RTAXI", sParse, iR, iL, 14, 3
        ParseRT = True
    End If
    
    ' Visual Acuity (right)
    iR = InStr(1, sParse, sVAR, vbBinaryCompare)
    sName = PickVAButton(sParse, iR)
    If sName <> "" Then
        oResults.AddValues sName, "10", 1, 2, 0, 1
        ParseRT = True
    End If
    
    ' Visual Acuity (left)
    iL = InStr(1, sParse, sVAL, vbBinaryCompare)
    sName = PickVAButton(sParse, iL)
    If sName <> "" Then
        oResults.AddValues sName, "01", 1, 2, 0, 1
        ParseRT = True
    End If
    
    'ADD
    iR = InStr(1, sParse, sAddR, vbBinaryCompare)
    iL = InStr(1, sParse, sAddL, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "RTADD", sParse, iR, iL, 2, 6
        ParseRT = True
    End If
    
    ' Visual Acuity (ADD) - Right and Left always the same
    iR = InStr(1, sParse, sVAB, vbBinaryCompare)
    sName = PickVAButtonADD(sParse, iR)
    If sName <> "" Then
        'Right
        oResults.AddValues sName, "10", 1, 2, 0, 1
        'Left
        oResults.AddValues sName, "01", 1, 2, 0, 1
        ParseRT = True
    End If
    
    'Prism
    iR = InStr(1, sParse, sPrismR, vbBinaryCompare)
    iL = InStr(1, sParse, sPrismL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add H Prism
        oResults.AddValues "RTPRH", sParse, iR, iL, 2, 6
        ' Add V Prism
        oResults.AddValues "RTPRV", sParse, iR, iL, 8, 6
        ParseRT = True
    End If
    
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "pD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "pD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "RTPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
    'End If

    Exit Function

lParseRT_Error:

    LogError "CCanonRKF1", "ParseRT", Err, Err.Description
End Function

Private Function PickVAButton(sToParse As String, iPos As Long) As String
Dim sName As String
    sName = ""
    Select Case Trim$(Mid$(sToParse, iPos + 2, 5))
        Case "PLANO"
                sName = "RTPLA"
            Case "BAL"
                sName = "RTBL"
            Case "15"
                sName = "RT15"
            Case "20"
                sName = "RT20"
            Case "25"
                sName = "RT25"
            Case "30"
                sName = "RT30"
            Case "40"
                sName = "RT40"
            Case "50"
                sName = "RT50"
            Case "60"
                sName = "RT60"
            Case "70"
                sName = "RT70"
            Case "80"
                sName = "RT80"
            Case "100"
                sName = "RT100"
            Case "150"
                sName = "RT150"
            Case "200"
                sName = "RT200"
            Case "400"
                sName = "RT400"
            Case "800"
                sName = "RT800"
            Case "+1"
                sName = "RT+1"
            Case "+2"
                sName = "RT+2"
            Case "-1"
                sName = "RT-1"
            Case "-2"
                sName = "RT-2"
            Case "FC1"
                sName = "RTFC1"
            Case "FC3"
                sName = "RTFC3"
            Case "HM"
                sName = "RTHM"
            Case "LP"
                sName = "RTLP"
        End Select
    PickVAButton = sName
End Function

Private Function PickVAButtonADD(sToParse As String, iPos As Long) As String
Dim sName As String
    sName = ""
    Select Case Trim$(Mid$(sToParse, iPos + 2, 5))
        Case "PLANO"
                sName = "RTPLAC"
            Case "BAL"
                sName = "RTBLC"
            Case "15"
                sName = "RT15C"
            Case "20"
                sName = "RT20C"
            Case "25"
                sName = "RT25C"
            Case "30"
                sName = "RT30C"
            Case "40"
                sName = "RT40C"
            Case "50"
                sName = "RT50C"
            Case "60"
                sName = "RT60C"
            Case "70"
                sName = "RT70C"
            Case "80"
                sName = "RT80C"
            Case "100"
                sName = "RT100C"
            Case "150"
                sName = "RT150C"
            Case "200"
                sName = "RT200C"
            Case "400"
                sName = "RT400C"
            Case "800"
                sName = "RT800C"
            Case "+1"
                sName = "RT+1C"
            Case "+2"
                sName = "RT+2C"
            Case "-1"
                sName = "RT-1C"
            Case "-2"
                sName = "RT-2C"
            Case "FC1"
                sName = "RTFC1C"
            Case "FC3"
                sName = "RTFC3C"
            Case "HM"
                sName = "RTHMC"
            Case "LP"
                sName = "RTLPC"
        End Select
    PickVAButtonADD = sName
End Function

Private Function ParseLM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
Dim iR As Long                     'Position of indicator for Right Eye
Dim iL As Long                     'Position of indicator for Left Eye
Dim sParse As String

'    Machine specific constants
Const sSCAR As String = " R"          'SCA right eye
Const sSCAL As String = " L"          'SCA left eye
Const sAddR As String = "AR"        'add right eye
Const sAddL As String = "AL"        'add left eye
Const sPrismR As String = "PR"      'prism right eye
Const sPrismL As String = "PL"      'prism left eye


    On Error GoTo lParseLM_Error

    If (InStr(i + 1, StringToParse, "@") <> 0) Then
        sParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "@") - i)
    Else
        sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    ' SCA
    iR = InStr(1, sParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, sParse, sSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        oResults.AddValues "LMSPH", sParse, iR, iL, 2, 6
        ' Add the Cylinder
        oResults.AddValues "LMCYL", sParse, iR, iL, 8, 6
        ' Add the Axis
        oResults.AddValues "LMAXI", sParse, iR, iL, 14, 3
        ParseLM = True
    End If
    
    ' ADD
    iR = InStr(1, sParse, sAddR, vbBinaryCompare)
    iL = InStr(1, sParse, sAddL, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "LMADD", sParse, iR, iL, 2, 6
        ParseLM = True
    End If
    
    ' Prism
    iR = InStr(1, sParse, sPrismR, vbBinaryCompare)
    iL = InStr(1, sParse, sPrismL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add H Prism
        oResults.AddValues "LMPRH", sParse, iR, iL, 2, 6
        ' Add V Prism
        oResults.AddValues "LMPRV", sParse, iR, iL, 8, 6
        ParseLM = True
    End If
    
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "LMPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
    'End If

    Exit Function

lParseLM_Error:

    LogError "CCanonRKF1", "ParseLM", Err, Err.Description
End Function

Private Function ParseRM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
Dim iR As Long                     'Position of indicator for Right Eye
Dim iL As Long                     'Position of indicator for Left Eye
Dim sParse As String
Dim sName As String

'    Machine specific constants
Const sSCAR As String = "OR"          'SCA right eye
Const sSCAL As String = "OL"          'SCA left eye
Const sVAR As String = "VR"
Const sVAL As String = "VL"

    
    On Error GoTo lParseRM_Error

    If (InStr(i + 1, StringToParse, "@") <> 0) Then
        sParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "@") - i)
    Else
        sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    
    'SCA
    iR = InStr(1, sParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, sParse, sSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        oResults.AddValues "RMSPH", sParse, iR, iL, 2, 6
        ' Add the Cylinder
        oResults.AddValues "RMCYL", sParse, iR, iL, 8, 6
        ' Add the Axis
        oResults.AddValues "RMAXI", sParse, iR, iL, 14, 3
        ParseRM = True
    End If
    
    ' Visual Acuity (right)
    iR = InStr(1, sParse, sVAR, vbBinaryCompare)
    sName = PickRMVAButton(sParse, iR)
    If sName <> "" Then
        oResults.AddValues sName, "10", 1, 2, 0, 1
        ParseRM = True
    End If
    
    ' Visual Acuity (left)
    iL = InStr(1, sParse, sVAL, vbBinaryCompare)
    sName = PickRMVAButton(sParse, iL)
    If sName <> "" Then
        oResults.AddValues sName, "01", 1, 2, 0, 1
        ParseRM = True
    End If
    
    ' PD - not yet working
    'If (InStr(1, tempSubParse, "PD", vbBinaryCompare) <> 0) Then
    '    j = InStr(1, tempSubParse, "PD", vbBinaryCompare)
    '    OutputArray(ArrayMark, 0) = "RMPD"
    '    OutputArray(ArrayMark, 1) = Mid$(tempSubParse, j + 6, 4)
    '    OutputArray(ArrayMark, 2) = Mid$(tempSubParse, j + 10, 4)
    '    OutputArray(ArrayMark, 3) = Str(TestMark)
    '    ArrayMark = ArrayMark + 1
    'End If

    Exit Function

lParseRM_Error:

    LogError "CCanonRKF1", "ParseRM", Err, Err.Description

End Function

Private Function PickRMVAButton(sToParse As String, iPos As Long) As String
Dim sName As String
    sName = ""
    Select Case Trim(Mid$(sToParse, iPos + 2, 5))
        Case "15"
            sName = "RM15"
        Case "20"
            sName = "RM20"
        Case "25"
            sName = "RM25"
        Case "30"
            sName = "RM30"
        Case "40"
            sName = "RM40"
        Case "50"
            sName = "RM50"
        Case "60"
            sName = "RM60"
        Case "70"
            sName = "RM70"
        Case "80"
            sName = "RM80"
        Case "100"
            sName = "RM100"
        Case "150"
            sName = "RM150"
        Case "200"
            sName = "RM200"
        Case "400"
            sName = "RM400"
        Case "800"
            sName = "RM800"
        Case "FC1"
            sName = "RMFC1"
        Case "FC3"
            sName = "RMFC3"
        Case "HM"
            sName = "RMHM"
        Case "ERR"
            sName = "RMERR"
    End Select
    PickRMVAButton = sName
End Function

Private Function ParseKM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
Dim iR As Long                     'Position of indicator for Right Eye
Dim iL As Long                     'Position of indicator for Left Eye
Dim sParse As String

'    Machine specific constants
Const sDioR    As String = "DR" 'diopter data right eye
Const sDioL    As String = "DL" 'diopter data left eye
Const sMmR As String = "CR"
Const sMmL As String = "CL"


    On Error GoTo lParseKM_Error

    If (InStr(i + 1, StringToParse, "@") <> 0) Then
        sParse = Mid$(StringToParse, i, InStr(i + 1, StringToParse, "@") - i)
    Else
        sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    End If
    'D data
    iR = InStr(1, sParse, sDioR, vbBinaryCompare)
    iL = InStr(1, sParse, sDioL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' R1
        oResults.AddValues "KMR1", sParse, iR, iL, 2, 5
        ' R2
        oResults.AddValues "KMR2", sParse, iR, iL, 7, 5
        ParseKM = True
    End If
    'mm Data
    iR = InStr(1, sParse, sMmR, vbBinaryCompare)
    iL = InStr(1, sParse, sMmL, vbBinaryCompare)
    If iR + iL < 0 Then
        ' R1 - Using D data instead of mm HG
        'oResults.AddValues "KMR1", sParse, iR, iL, 2, 5
        ' R2 - Using D data instead of mm HG
        'oResults.AddValues "KMR2", sParse, iR, iL, 7, 5
        ' Axis
        oResults.AddValues "KMAXI", sParse, iR, iL, 12, 3
        ParseKM = True
    End If

    Exit Function

lParseKM_Error:

    LogError "CCanonRKF1", "ParseKM", Err, Err.Description
End Function

Private Function ParseNT(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    ' Currently we don't support NT.
End Function

