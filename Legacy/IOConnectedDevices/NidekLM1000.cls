VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNidekLM1000"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IDevice
    
Private oResults As CDeviceResults
Const strTestDelimiter As String = ""          'Chr(2) -- "STX"

Public Function IDevice_ParseMachineOutput(StringToParse As String, numTests As Integer) As String()

Dim i As Integer
Dim LMdone As Boolean                   'Lensmeter
    
    On Error GoTo lIDevice_ParseMachineOutput_Error

    Set oResults = New CDeviceResults
    
    For i = 1 To Len(StringToParse)
        If (Mid$(StringToParse, i, 1) = strTestDelimiter) Then
            LMdone = ParseLM(StringToParse, i)
            If Not LMdone Then
                oResults.PrevTest
            End If
        End If
    Next i
    
    IDevice_ParseMachineOutput = oResults.asOutput
    numTests = oResults.iTestMark
    Set oResults = Nothing

    Exit Function

lIDevice_ParseMachineOutput_Error:

    LogError "CNidekLM1000", "IDevice_ParseMachineOutput", Err, Err.Description

End Function


Private Function ParseLM(ByVal StringToParse As String, ByVal i As Integer) As Boolean
    
    Dim iR As Long                     'Position of indicator for Right Eye
    Dim iL As Long                     'Position of indicator for Left Eye
    Dim sParse As String
    
'    Machine specific constants
    Const sSCAR As String = " R"          'SCA right eye
    Const sSCAL As String = " L"          'SCA left eye
    Const sAddR As String = "AR"        'add right eye
    Const sAddL As String = "AL"        'add left eye
    Const sPrismR As String = "PR"      'prism right eye
    Const sPrismL As String = "PL"      'prism left eye
    Const sPD As String = "PD"        'pupil distance
    
    On Error GoTo lParseLM_Error
    
    oResults.NextTest

    sParse = Mid$(StringToParse, i, Len(StringToParse) - i)
    
    ' SCA
    iR = InStr(1, sParse, sSCAR, vbBinaryCompare)
    iL = InStr(1, sParse, sSCAL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add the Sphere
        oResults.AddValues "LMSPH", sParse, iR, iL, 2, 6
        ' Add the Cylinder
        oResults.AddValues "LMCYL", sParse, iR, iL, 8, 6
        ' Add the Axis
        oResults.AddValues "LMAXI", sParse, iR, iL, 14, 3
        ParseLM = True
    End If
    
    ' ADD
    iR = InStr(1, sParse, sAddR, vbBinaryCompare)
    iL = InStr(1, sParse, sAddL, vbBinaryCompare)
    If iR + iL > 0 Then
        oResults.AddValues "LMADD", sParse, iR, iL, 2, 5
        ParseLM = True
    End If
    
    ' Prism
    iR = InStr(1, sParse, sPrismR, vbBinaryCompare)
    iL = InStr(1, sParse, sPrismL, vbBinaryCompare)
    If iR + iL > 0 Then
        ' Add H Prism
        oResults.AddValues "LMPRH", sParse, iR, iL, 2, 6
        ' Add V Prism
        oResults.AddValues "LMPRV", sParse, iR, iL, 12, 6
        ParseLM = True
    End If
    
    ' PD
    iR = InStr(1, sParse, sPD, vbBinaryCompare)
    If iR > 0 Then
        oResults.AddValues "LMPD", sParse, iR, 0, 2, 4
        ParseLM = True
    End If

    Exit Function

lParseLM_Error:

    LogError "CNidekLM1000", "ParseLM", Err, Err.Description
    
End Function

