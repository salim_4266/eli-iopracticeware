VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeReports"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeReports Object Module
Option Explicit
Public ReportName As String
Public ReportOrder As Long
Public ReportQuery As String
Public ReportCriteria As String
Public ReportTemplate As String
Public ReportActive As Boolean
Public ReportAllowDate As Boolean
Public ReportAllowDoctor As Boolean
Public ReportDocBreak As Boolean
Public ReportAllowLocation As Boolean
Public ReportLocBreak As Boolean
Public ReportPatBreak As String
Public ReportAdminOn As Boolean

Public ReportDocRpt As String
Public ReportLocRpt As String
Public ReportDocLocRpt As String
Public ReportDocPatRpt As String
Public ReportLocPatRpt As String
Public ReportDocLocPatRpt As String

Public ReportDaily As String
Public ReportCriteriaDefaults As String

Private ModuleName As String
Private ReportTotal As Long
Private ReportTbl As ADODB.Recordset
Private ReportNew As Boolean

Private Sub Class_Initialize()
Set ReportTbl = CreateAdoRecordset
Call InitReport
End Sub

Private Sub Class_Terminate()
Call InitReport
Set ReportTbl = Nothing
End Sub

Private Sub InitReport()
ReportNew = True
ReportName = ""
ReportOrder = 0
ReportQuery = ""
ReportCriteria = String(50, "0")
ReportTemplate = ""
ReportActive = False
ReportAllowDate = False
ReportAllowDoctor = False
ReportDocBreak = False
ReportAllowLocation = False
ReportLocBreak = False
ReportPatBreak = ""
ReportDocRpt = ""
ReportLocRpt = ""
ReportDocLocRpt = ""
ReportDocPatRpt = ""
ReportLocPatRpt = ""
ReportDocLocPatRpt = ""
ReportDaily = ""
ReportCriteriaDefaults = String(50, "0")
ReportAdminOn = False
End Sub

Private Sub LoadReport()
ReportNew = False
If (ReportTbl("ReportName") <> "") Then
    ReportName = ReportTbl("ReportName")
Else
    ReportName = ""
End If
If (ReportTbl("ReportOrder") <> "") Then
    ReportOrder = ReportTbl("ReportOrder")
Else
    ReportOrder = 0
End If
If (ReportTbl("QueryName") <> "") Then
    ReportQuery = ReportTbl("QueryName")
Else
    ReportQuery = ""
End If
If (ReportTbl("CriteriaMap") <> "") Then
    ReportCriteria = ReportTbl("CriteriaMap")
Else
    ReportCriteria = ""
End If
If (ReportTbl("TemplateName") <> "") Then
    ReportTemplate = ReportTbl("TemplateName")
Else
    ReportTemplate = ""
End If
If (ReportTbl("Active") <> "") Then
    If (ReportTbl("Active") = "T") Then
        ReportActive = True
    Else
        ReportActive = False
    End If
Else
    ReportActive = False
End If
If (ReportTbl("AllowDate") <> "") Then
    If (ReportTbl("AllowDate") = "T") Then
        ReportAllowDate = True
    Else
        ReportAllowDate = False
    End If
Else
    ReportAllowDate = False
End If
If (ReportTbl("EntryScreenMap") <> "") Then
    If (Mid(ReportTbl("EntryScreenMap"), 1, 1) = "1") Then
        ReportAllowDoctor = True
    Else
        ReportAllowDoctor = False
    End If
    If (Mid(ReportTbl("EntryScreenMap"), 2, 1) = "1") Then
        ReportDocBreak = True
    Else
        ReportDocBreak = False
    End If
    If (Mid(ReportTbl("EntryScreenMap"), 3, 1) = "1") Then
        ReportAllowLocation = True
    Else
        ReportAllowLocation = False
    End If
    If (Mid(ReportTbl("EntryScreenMap"), 4, 1) = "1") Then
        ReportLocBreak = True
    Else
        ReportLocBreak = False
    End If
Else
    ReportAllowDoctor = False
    ReportDocBreak = False
    ReportAllowLocation = False
    ReportLocBreak = False
End If
If (ReportTbl("PatBreak") <> "") Then
    ReportPatBreak = ReportTbl("PatBreak")
Else
    ReportPatBreak = ""
End If
If (ReportTbl("DocBreak") <> "") Then
    ReportDocRpt = ReportTbl("DocBreak")
Else
    ReportDocRpt = ""
End If
If (ReportTbl("LocBreak") <> "") Then
    ReportLocRpt = ReportTbl("LocBreak")
Else
    ReportLocRpt = ""
End If
If (ReportTbl("DocLocBreak") <> "") Then
    ReportDocLocRpt = ReportTbl("DocLocBreak")
Else
    ReportDocLocRpt = ""
End If
If (ReportTbl("DocPatBreak") <> "") Then
    ReportDocPatRpt = ReportTbl("DocPatBreak")
Else
    ReportDocPatRpt = ""
End If
If (ReportTbl("LocPatBreak") <> "") Then
    ReportLocPatRpt = ReportTbl("LocPatBreak")
Else
    ReportLocPatRpt = ""
End If
If (ReportTbl("DocLocPatBreak") <> "") Then
    ReportDocLocPatRpt = ReportTbl("DocLocPatBreak")
Else
    ReportDocLocPatRpt = ""
End If
If (ReportTbl("Daily") <> "") Then
    ReportDaily = ReportTbl("Daily")
Else
    ReportDaily = ""
End If
If (ReportTbl("CriteriaMapDefaults") <> "") Then
    ReportCriteriaDefaults = ReportTbl("CriteriaMapDefaults")
Else
    ReportCriteriaDefaults = ""
End If
If (ReportTbl("AdminOn") <> "") Then
    If (ReportTbl("AdminOn") = "T") Then
        ReportAdminOn = True
    Else
        ReportAdminOn = False
    End If
Else
    ReportAdminOn = False
End If
End Sub

Private Function GetReport()
On Error GoTo DbErrorHandler
Dim OrderString As String
GetReport = False
OrderString = "SELECT * FROM PracticeReports WHERE ReportName = '" + Trim(ReportName) + "' "
Set ReportTbl = Nothing
Set ReportTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call ReportTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (ReportTbl.EOF) Then
    Call InitReport
Else
    Call LoadReport
End If
GetReport = True
Exit Function
DbErrorHandler:
    ModuleName = "GetReport"
    GetReport = False
    ReportName = ""
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetReportList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
ReportTotal = 0
GetReportList = -1
If (Len(ReportName) <= 1) Then
    OrderString = "Select * FROM PracticeReports WHERE ReportName >= '" + Trim(ReportName) + "' "
Else
    OrderString = "Select * FROM PracticeReports WHERE ReportName = '" + Trim(ReportName) + "' "
End If
If (IType = 1) Then
    OrderString = OrderString + "And Active = 'T' "
End If
OrderString = OrderString + "ORDER BY ReportOrder ASC, ReportName ASC "
Set ReportTbl = Nothing
Set ReportTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call ReportTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (ReportTbl.EOF) Then
    GetReportList = -1
Else
    ReportTbl.MoveLast
    ReportTotal = ReportTbl.RecordCount
    GetReportList = ReportTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetReportList"
    GetReportList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutReport()
On Error GoTo DbErrorHandler
Dim Temp As String
PutReport = True
If (ReportNew) Then
    ReportTbl.AddNew
End If
Temp = "0000"
ReportTbl("ReportName") = Trim(ReportName)
ReportTbl("ReportOrder") = ReportOrder
ReportTbl("QueryName") = Trim(ReportQuery)
ReportTbl("CriteriaMap") = Trim(ReportCriteria)
ReportTbl("TemplateName") = Trim(ReportTemplate)
ReportTbl("Active") = "F"
If (ReportActive) Then
    ReportTbl("Active") = "T"
End If
ReportTbl("AllowDate") = "F"
If (ReportAllowDate) Then
    ReportTbl("AllowDate") = "T"
End If
If (ReportAllowDoctor) Then
    Mid(Temp, 1, 1) = "1"
End If
If (ReportDocBreak) Then
    Mid(Temp, 2, 1) = "1"
End If
If (ReportAllowLocation) Then
    Mid(Temp, 3, 1) = "1"
End If
If (ReportLocBreak) Then
    Mid(Temp, 4, 1) = "1"
End If
ReportTbl("EntryScreenMap") = Temp
ReportTbl("PatBreak") = ReportPatBreak
ReportTbl("DocBreak") = ReportDocRpt
ReportTbl("LocBreak") = ReportLocRpt
ReportTbl("DocLocBreak") = ReportDocLocRpt
ReportTbl("DocPatBreak") = ReportDocPatRpt
ReportTbl("LocPatBreak") = ReportLocPatRpt
ReportTbl("DocLocPatBreak") = ReportDocLocPatRpt
ReportTbl("CriteriaMapDefaults") = Trim(ReportCriteriaDefaults)
ReportTbl("Daily") = Trim(ReportDaily)
ReportTbl("AdminOn") = "F"
If (ReportAdminOn) Then
    ReportTbl("AdminOn") = "T"
End If
ReportTbl.Update
If (ReportNew) Then
    ReportNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutReport"
    PutReport = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeReport() As Boolean
On Error GoTo DbErrorHandler
PurgeReport = True
ReportTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeReport"
    PurgeReport = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyReport() As Boolean
ApplyReport = PutReport
End Function

Public Function DeleteReport() As Boolean
DeleteReport = PurgeReport
End Function

Public Function RetrieveReport() As Boolean
RetrieveReport = GetReport
End Function

Public Function SelectReport(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectReport = False
If (ReportTbl.EOF) Or (ListItem < 1) Or (ListItem > ReportTotal) Then
    Exit Function
End If
ReportTbl.MoveFirst
ReportTbl.Move ListItem - 1
SelectReport = True
Call LoadReport
Exit Function
DbErrorHandler:
    ModuleName = "SelectReport"
    SelectReport = False
    ReportName = ""
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindReport() As Long
FindReport = GetReportList(0)
End Function

Public Function FindActiveReport() As Long
FindActiveReport = GetReportList(1)
End Function

