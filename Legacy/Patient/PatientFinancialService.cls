VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientFinancialService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public InsurerId As Long
Public StartDateTime As String
Public EndDateTime As String
Public InsuredPatientId As Long
Public InsuranceTypeId As String
Public PatientInsuranceId As Long
Public InsurerName As String
Public OrdinalId As Long
Private StrSql As String
Private RS As Recordset
Dim OrderBy As String
Private Sub Class_Initialize()
Call InitPatientFinancialService
End Sub
Private Sub InitPatientFinancialService()
InsurerId = 0
StartDateTime = ""
EndDateTime = ""
InsuredPatientId = 0
InsuranceTypeId = ""
PatientInsuranceId = 0
OrdinalId = 0
InsurerName = ""
OrderBy = ""
Set RS = Nothing
End Sub
Public Function GetPatientInsurerInfo(Optional FilterOnlyActive As Boolean = False) As Recordset
On Error GoTo lGetPatientFinancialInfo_Error

StrSql = " SELECT pi.Id,CASE WHEN EndDateTime IS NULL THEN '' ELSE CONVERT(NVARCHAR(10),pi.EndDateTime,112) END As EndDateTime,ISNULL(CONVERT(NVARCHAR(10),ip.StartDateTime,112),'') As StartDatetime, CASE WHEN (pi.EndDateTime Is Null Or pi.EndDateTime >= GETDATE()) And pi.IsDeleted = 0 THEN CONVERT(NVARCHAR(1),'C') ELSE CONVERT(NVARCHAR(1),'X') END AS Status " & _
          " ,pi.InsurancePolicyId,ISNULL(pi.OrdinalId,0) AS OrdinalId,ISNULL(pi.InsuredPatientId,0) As InsuredPatientId,ISNULL(pi.InsurancePolicyId,0) As InsurancePolicyId,pi.IsDeleted,ISNULL(pi.InsuranceTypeId,0) As InsuranceTypeId " & _
          " ,ISNULL(ip.Id,0) as PolicyId,ISNULL(ip.PolicyCode,'') As PolicyCode,ISNULL(ip.GroupCode,'') AS GroupCode,ISNULL(ip.Copay,0) As Copay,ISNULL(ip.PolicyHolderPatientId,0) As PolicyHolderPatientId,ISNULL(ip.InsurerId,0) As InsurerId " & _
          " ,ISNULL(i.Name,'') As Name,ISNULL(i.GroupName,'') As GroupName,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END)AS PlanName" & _
          " ,ISNULL(i.PayerCode,'') AS PayerCode,i.AllowDependents,ISNULL(i.PolicyNumberFormat,'') AS PolicyNumberFormat,ISNULL(i.ClaimFilingIndicatorCodeId,0) AS ClaimFilingIndicatorCodeId,ISNULL(i.ClaimFileReceiverId,0) AS ClaimFileReceiverId    " & _
          " ,'' AS InsurerGroupId ,i.IsReferralRequired AS InsurerReferralRequired ,CONVERT(REAL,0) AS OutOfPocket ,CASE i.IsSecondaryClaimPaper WHEN 1 THEN 'A' ELSE '-' END AS [Availability] " & _
          " ,'' AS InsurerClaimMap,i.SendZeroCharge AS InsurerServiceFilter,1 AS InsurerCrossOver,ISNULL(i.InsurerBusinessClassId,0) As InsurerBusinessClassId " & _
          " FROM model.PatientInsurances pi " & _
          " INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.Id " & _
          " INNER JOIN model.Insurers i  ON ip.InsurerId = i.Id " & _
          " WHERE pi.InsuredPatientId = " & InsuredPatientId

If StartDateTime <> "" Then
    StrSql = StrSql & " AND  CONVERT(NVARCHAR(8),ip.StartDateTime,112) > =' " & StartDateTime & "' "
End If
If EndDateTime <> "" Then
    StrSql = StrSql & " AND CONVERT(NVARCHAR(8),ip.EndDateTIme,112) < =' " & EndDateTime & "' "
End If
If InsuranceTypeId <> "" Then
    StrSql = StrSql & " AND pi.InsuranceTypeId  = " & ConvertLegacyToInsuranceType(InsuranceTypeId)
End If
If OrdinalId > 0 Then
    StrSql = StrSql & " AND pi.OrdinalId  = " & OrdinalId
End If
If FilterOnlyActive Then
    StrSql = StrSql & " AND (pi.EndDateTime Is Null Or pi.EndDateTime >= GETDATE()) And pi.IsDeleted = 0 "
End If
OrderBy = " ORDER BY  pi.InsuranceTypeId,pi.OrdinalId "
StrSql = StrSql & OrderBy
Set RS = GetRS(StrSql)
Set GetPatientInsurerInfo = RS
Exit Function
lGetPatientFinancialInfo_Error:
    LogError "PatientFinancialService", "GetPatientFinancialInfo", Err, Err.Description
End Function
Public Function GetInsurerInfoByName(ByVal IsDistinct As Boolean) As Recordset
On Error GoTo lGetInsurerInfoByName_Error
Dim Ref As String
Dim OrderBy As String
Ref = ">="
If Len(Trim(InsurerName)) > 1 Then
    Ref = "="
End If
If (IsDistinct) Then
    StrSql = " SELECT DISTINCT i.Name,i.Id As InsurerId FROM model.Insurers i" & _
             " WHERE i.Name " & Ref & "'" & Trim(InsurerName) & " '"
Else
    StrSql = " SELECT pi.Id,CASE WHEN EndDateTime IS NULL THEN '' ELSE CONVERT(NVARCHAR(10),pi.EndDateTime,112) END As EndDateTime,ISNULL(CONVERT(NVARCHAR(10),ip.StartDateTime,112),'') As StartDatetime, CASE WHEN (pi.EndDateTime Is Null Or pi.EndDateTime >= GETDATE()) And pi.IsDeleted = 0 THEN CONVERT(NVARCHAR(1),'C') ELSE CONVERT(NVARCHAR(1),'X') END AS Status " & _
            " ,pi.InsurancePolicyId,pi.OrdinalId,pi.InsuredPatientId,pi.InsurancePolicyId,pi.IsDeleted,pi.InsuranceTypeId " & _
            " ,ISNULL(i.Name,'') As Name,ISNULL(i.GroupName,'') As GroupName,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END)AS PlanName" & _
            " ,i.PayerCode,i.AllowDependents,i.PolicyNumberFormat,i.ClaimFilingIndicatorCodeId,i.ClaimFileReceiverId  " & _
            " ,CONVERT(NVARCHAR(16),CASE WHEN pt.Name <> '' AND pt.Name IS NOT NULL THEN pt.Name ELSE '' END) AS InsurerPlanType " & _
            " ,CONVERT(REAL,0) AS OutOfPocket,ISNULL(i.InsurerBusinessClassId,0) As InsurerBusinessClassId " & _
            " FROM model.PatientInsurances pi " & _
            " INNER JOIN model.Insurers i  ON ip.InsurerId = i.Id " & _
            " LEFT JOIN model.InsurerPlanTypes pt ON pt.Id = i.InsurerPlanTypeId " & _
            " WHERE pi.IsDeleted = 0 AND i.Name " & Ref & "'" & Trim(InsurerName) & " '"
End If

OrderBy = " ORDER BY i.Name ASC "

StrSql = StrSql & OrderBy

Set RS = GetRS(StrSql)
Set GetInsurerInfoByName = RS
Exit Function
lGetInsurerInfoByName_Error:
    LogError "PatientFinancialService", "GetInsurerInfoByName", Err, Err.Description
End Function
Public Function RetrievePatientInsurer() As Recordset
On Error GoTo lRetrievePatientInsurer_Error
Set RetrievePatientInsurer = Nothing

StrSql = " SELECT pi.Id,CASE WHEN EndDateTime IS NULL THEN '' ELSE CONVERT(NVARCHAR(10),pi.EndDateTime,112) END As EndDateTime,ISNULL(CONVERT(NVARCHAR(10),ip.StartDateTime,112),'') As StartDatetime, CASE WHEN (pi.EndDateTime Is Null Or pi.EndDateTime >= GETDATE()) And pi.IsDeleted = 0 THEN CONVERT(NVARCHAR(1),'C') ELSE CONVERT(NVARCHAR(1),'X') END AS Status " & _
          " ,pi.InsurancePolicyId,ISNULL(pi.OrdinalId,0) AS OrdinalId,ISNULL(pi.InsuredPatientId,0) As InsuredPatientId,ISNULL(pi.InsurancePolicyId,0) As InsurancePolicyId,pi.IsDeleted,ISNULL(pi.InsuranceTypeId,0) As InsuranceTypeId " & _
          " ,ISNULL(ip.Id,0) as PolicyId,ISNULL(ip.PolicyCode,'') As PolicyCode,ISNULL(ip.GroupCode,'') AS GroupCode,ISNULL(ip.Copay,0) As Copay,ISNULL(ip.PolicyHolderPatientId,0) As PolicyHolderPatientId,ISNULL(ip.InsurerId,0) As InsurerId " & _
          " ,ISNULL(i.Name,'') As Name,ISNULL(i.GroupName,'') As GroupName,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END)AS PlanName" & _
          " ,ISNULL(i.PayerCode,'') AS PayerCode,i.AllowDependents,ISNULL(i.PolicyNumberFormat,'') AS PolicyNumberFormat,ISNULL(i.ClaimFilingIndicatorCodeId,0) AS ClaimFilingIndicatorCodeId,ISNULL(i.ClaimFileReceiverId,0) AS ClaimFileReceiverId    " & _
          " ,'' AS InsurerGroupId ,i.IsReferralRequired AS InsurerReferralRequired ,CONVERT(REAL,0) AS OutOfPocket ,CASE i.IsSecondaryClaimPaper WHEN 1 THEN 'A' ELSE '-' END AS [Availability] " & _
          " ,'' AS InsurerClaimMap,i.SendZeroCharge AS InsurerServiceFilter,1 AS InsurerCrossOver,ISNULL(i.InsurerBusinessClassId,0) As InsurerBusinessClassId " & _
          " FROM model.PatientInsurances pi " & _
          " INNER JOIN model.InsurancePolicies ip ON ip.Id=pi.InsurancePolicyId " & _
          " INNER JOIN model.Insurers i  ON ip.InsurerId = i.Id " & _
          " WHERE IsDeleted = 0 AND pi.Id = " & PatientInsuranceId

OrderBy = " ORDER BY  pi.InsuranceTypeId,pi.OrdinalId "
StrSql = StrSql & OrderBy
Set RS = GetRS(StrSql)
Set RetrievePatientInsurer = RS
Exit Function
lRetrievePatientInsurer_Error:
    LogError "PatientFinancialService", "RetrievePatientInsurer", Err, Err.Description
End Function
Public Function GetPatientPrimaryInsurer() As Recordset
On Error GoTo lGetPatientPrimaryInsurer_Error

StrSql = " SELECT pi.Id,CASE WHEN EndDateTime IS NULL THEN '' ELSE CONVERT(NVARCHAR(10),pi.EndDateTime,112) END As EndDateTime,ISNULL(CONVERT(NVARCHAR(10),ip.StartDateTime,112),'') As StartDatetime, CASE WHEN (pi.EndDateTime Is Null Or pi.EndDateTime >= GETDATE()) And pi.IsDeleted = 0 THEN CONVERT(NVARCHAR(1),'C') ELSE CONVERT(NVARCHAR(1),'X') END AS Status " & _
          " ,pi.InsurancePolicyId,ISNULL(pi.OrdinalId,0) AS OrdinalId,ISNULL(pi.InsuredPatientId,0) As InsuredPatientId,ISNULL(pi.InsurancePolicyId,0) As InsurancePolicyId,pi.IsDeleted,ISNULL(pi.InsuranceTypeId,0) As InsuranceTypeId " & _
          " ,ISNULL(ip.Id,0) as PolicyId,ISNULL(ip.PolicyCode,'') As PolicyCode,ISNULL(ip.GroupCode,'') AS GroupCode,ISNULL(ip.Copay,0) As Copay,ISNULL(ip.PolicyHolderPatientId,0) As PolicyHolderPatientId,ISNULL(ip.InsurerId,0) As InsurerId " & _
          " ,ISNULL(i.Name,'') As Name,ISNULL(i.GroupName,'') As GroupName,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END)AS PlanName" & _
          " ,ISNULL(i.PayerCode,'') AS PayerCode,i.AllowDependents,ISNULL(i.PolicyNumberFormat,'') AS PolicyNumberFormat,ISNULL(i.ClaimFilingIndicatorCodeId,0) AS ClaimFilingIndicatorCodeId,ISNULL(i.ClaimFileReceiverId,0) AS ClaimFileReceiverId    " & _
          " ,'' AS InsurerGroupId ,i.IsReferralRequired AS InsurerReferralRequired ,CONVERT(REAL,0) AS OutOfPocket ,CASE i.IsSecondaryClaimPaper WHEN 1 THEN 'A' ELSE '-' END AS [Availability] " & _
          " ,'' AS InsurerClaimMap,i.SendZeroCharge AS InsurerServiceFilter,1 AS InsurerCrossOver,ISNULL(i.InsurerBusinessClassId,0) As InsurerBusinessClassId " & _
          " FROM model.PatientInsurances pi " & _
          " INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId " & _
          " INNER JOIN model.Insurers i on ip.InsurerId = i.Id " & _
          " WHERE pi.IsDeleted = 0 And pi.InsuredPatientId =" & InsuredPatientId
If InsuranceTypeId <> "" Then
    StrSql = StrSql & " AND pi.InsuranceTypeId = " & ConvertLegacyToInsuranceType(InsuranceTypeId)
End If
If OrdinalId > 0 Then
    StrSql = StrSql & " AND pi.OrdinalId  = " & OrdinalId
End If
If InsurerId > 0 Then
    StrSql = StrSql & " AND i.Id = " & InsurerId
End If
StrSql = StrSql & " ORDER BY  InsuranceTypeId,OrdinalId "
Set RS = GetRS(StrSql)
Set GetPatientPrimaryInsurer = RS
Exit Function
lGetPatientPrimaryInsurer_Error:
    LogError "PatientFinancialService", "GetPatientPrimaryInsurer", Err, Err.Description
End Function
Public Function RetrieveInsurer() As Recordset
On Error GoTo lRetrieveInsurer_Error

Set RetrieveInsurer = Nothing
StrSql = " SELECT ISNULL(i.Name,'') As Name,ISNULL(i.GroupName,'') As GroupName,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END)AS PlanName" & _
          " ,ISNULL(i.PayerCode,'') AS PayerCode,i.AllowDependents,ISNULL(i.PolicyNumberFormat,'') AS PolicyNumberFormat,ISNULL(i.ClaimFilingIndicatorCodeId,0) AS ClaimFilingIndicatorCodeId,ISNULL(i.ClaimFileReceiverId,0) AS ClaimFileReceiverId    " & _
          " ,'' AS InsurerGroupId ,i.IsReferralRequired AS InsurerReferralRequired ,CONVERT(REAL,0) AS OutOfPocket ,CASE i.IsSecondaryClaimPaper WHEN 1 THEN 'A' ELSE '-' END AS [Availability] " & _
          " ,'' AS InsurerClaimMap,i.SendZeroCharge AS InsurerServiceFilter,1 AS InsurerCrossOver,ISNULL(i.InsurerBusinessClassId,0) As InsurerBusinessClassId  " & _
         " FROM model.Insurers i WHERE i.Id = " & InsurerId
Set RS = GetRS(StrSql)
Set RetrieveInsurer = RS
Exit Function
lRetrieveInsurer_Error:
    LogError "PatientFinancialService", "RetrieveInsurer", Err, Err.Description
End Function
Public Function GetInsurerDetails() As Recordset
On Error GoTo lGetInsurerDetails_Error

Set GetInsurerDetails = Nothing
Dim StrSqlSubStatement As String
StrSqlSubStatement = " SET NOCOUNT ON " & _
                     " IF (OBJECT_ID('tempdb..#InsurerAddress') IS NOT NULL) EXEC ('DROP TABLE #InsurerAddress')" & _
                     " SELECT * INTO #InsurerAddress FROM model.InsurerAddresses WHERE Id " & _
                     " IN (SELECT MAX(a.Id) FROM model.InsurerAddresses a JOIN model.InsurerAddressTypes at ON a.InsurerAddressTypeId = at.Id WHERE at.Name LIKE '%Claim%' GROUP BY a.InsurerId) " & _
                     " AND InsurerId = " & InsurerId & " " & _
                     " IF (OBJECT_ID('tempdb..#InsurerPhone') IS NOT NULL) EXEC ('DROP TABLE #InsurerPhone')" & _
                     " SELECT * INTO #InsurerPhone FROM model.InsurerPhoneNumbers WHERE Id " & _
                     " IN (SELECT MAX(ip.Id) FROM model.InsurerPhoneNumbers ip WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Main') GROUP BY ip.InsurerId) " & _
                     " AND InsurerId = " & InsurerId & " " & _
                     " IF (OBJECT_ID('tempdb..#InsurerClaimPhone') IS NOT NULL) EXEC ('DROP TABLE #InsurerClaimPhone')" & _
                     " SELECT * INTO #InsurerClaimPhone FROM model.InsurerPhoneNumbers WHERE Id IN (SELECT MAX(ip.Id) FROM model.InsurerPhoneNumbers ip WHERE ip.InsurerPhoneNumberTypeId = (SELECT TOP 1 Id FROM model.InsurerPhoneNumberTypes WHERE Name = 'Claims') GROUP BY ip.InsurerId) " & _
                     " AND InsurerId = " & InsurerId & " "
StrSql = " SELECT i.Id,ISNULL(i.Name,'') As Name,ISNULL(i.GroupName,'') As GroupName,CONVERT(NVARCHAR(16),CASE WHEN i.PlanName <> '' AND i.PlanName IS NOT NULL THEN i.PlanName ELSE '' END) AS PlanName " & _
         " ,CONVERT(NVARCHAR(50),CASE WHEN ia.Line1 <> '' AND ia.Line1 IS NOT NULL THEN ia.Line1 ELSE '' END) AS InsurerAddress " & _
         " ,CONVERT(NVARCHAR(32),CASE WHEN ia.City <> '' AND ia.City IS NOT NULL THEN ia.City ELSE '' END) AS InsurerCity  " & _
         " ,CONVERT(NVARCHAR(2),CASE WHEN st.Abbreviation <> '' AND st.Abbreviation IS NOT NULL THEN st.Abbreviation ELSE '' END) AS InsurerState " & _
         " ,CONVERT(NVARCHAR(16),CASE WHEN ia.PostalCode  <> '' AND ia.PostalCode  IS NOT NULL THEN ia.PostalCode  ELSE '' END) AS InsurerZip  " & _
         " ,CONVERT(NVARCHAR(16),CONVERT(NVARCHAR(MAX),ISNULL(ipn.AreaCode,'')) + CONVERT(NVARCHAR(MAX),ISNULL(ipn.ExchangeAndSuffix,''))) AS InsurerPhone  " & _
         " ,CONVERT(NVARCHAR(32),CASE WHEN i.PayerCode <> '' AND i.PayerCode IS NOT NULL THEN i.PayerCode ELSE '' END) AS NEICNumber " & _
         " , SUBSTRING(ISNULL(pctClaimReceiver.Code,''), 1, 1) AS InsurerTFormat " & _
         " , CASE WHEN ipClaims.Id IS NOT NULL THEN CONVERT(NVARCHAR(MAX),ISNULL(ipClaims.AreaCode,'')) + CONVERT(NVARCHAR(MAX),ISNULL(ipClaims.ExchangeAndSuffix,'')) ELSE '' END AS InsurerClaimPhone " & _
         " , ISNULL(st.Abbreviation,'') AS StateJurisdiction " & _
         " FROM model.Insurers i " & _
         " LEFT OUTER JOIN #InsurerAddress ia ON ia.InsurerId = i.Id " & _
         " LEFT OUTER JOIN #InsurerPhone ipn ON ipn.InsurerId = i.Id " & _
         " LEFT OUTER JOIN #InsurerClaimPhone ipClaims ON ipClaims.InsurerId = i.Id " & _
         " INNER JOIN model.StateOrProvinces st ON st.Id = ia.StateOrProvinceId  " & _
         " LEFT JOIN model.ClaimFileReceivers cfr ON cfr.Id = i.ClaimFileReceiverId " & _
         " LEFT JOIN dbo.PracticeCodeTable pctClaimReceiver ON cfr.LegacyPracticeCodeTableId = pctClaimReceiver.Id " & _
         " AND pctClaimReceiver.ReferenceType = 'TRANSMITTYPE' " & _
         " WHERE i.Id = " & InsurerId
         
StrSql = StrSqlSubStatement & StrSql
Set RS = GetRS(StrSql)
Set GetInsurerDetails = RS

Exit Function
lGetInsurerDetails_Error:
    LogError "PatientFinancialService", "GetInsurerDetails", Err, Err.Description
End Function
Private Function ConvertLegacyToInsuranceType(InsuranceTypeId As String) As Long
ConvertLegacyToInsuranceType = 1
Select Case InsuranceTypeId
    Case "V"
        ConvertLegacyToInsuranceType = 2
    Case "A"
        ConvertLegacyToInsuranceType = 3
    Case "W"
        ConvertLegacyToInsuranceType = 4
End Select
End Function
Public Function ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass(ByVal InsurerBusinessClassId As String) As String
If InsurerBusinessClassId = "" Then InsurerBusinessClassId = "0"
Select Case CLng(InsurerBusinessClassId)
    Case 1
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "AMERIH"
    Case 2
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "BLUES"
    Case 3
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "COMM"
    Case 4
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCAIDFL"
    Case 5
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCAIDNC"
    Case 6
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCAIDNJ"
    Case 7
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCAIDNV"
    Case 8
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCAIDNY"
    Case 9
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCARENJ"
    Case 10
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCARENV"
    Case 11
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "MCARENY"
    Case 12
        ConvertInsurerBusinessClassIdToLegacyInsurerBusinessClass = "TEMPLATE"
End Select

End Function

