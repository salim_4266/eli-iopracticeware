VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PendingTrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Pending Transactions Class
Option Explicit
Public PendType As String
Public PendStatus As String
Public TotalRecs As Long
Public MaxPage As Integer
Public CurrentPage As Integer
Public LastPage As Boolean
Public SStatus As String
Public STranDate As String
Public SPatName As String
Public STranType As String
Public SDoctor As String
Public SRefDoc As String
Public STranId As Long
Public SPatId As String
Public STransTypeId As Long
Private rsPendingTrans As ADODB.Recordset
Private strSQL As String

Private Sub InitList()
PendType = ""
TotalRecs = 0
MaxPage = 0
CurrentPage = 1
LastPage = True
SStatus = ""
STranDate = ""
SPatName = ""
STranType = ""
SDoctor = ""
SRefDoc = ""
STranId = 0
SPatId = 0
STransTypeId = 0
End Sub

Private Sub Class_Initialize()
Set rsPendingTrans = Nothing
Set rsPendingTrans = CreateAdoRecordset
Call InitList
End Sub

Private Sub Class_Terminate()
Call InitList
Set rsPendingTrans = Nothing
End Sub

Private Sub LoadList()
Dim i As Integer
SPatId = rsPendingTrans("PatId")
STransTypeId = rsPendingTrans("TransTypeId")
SStatus = rsPendingTrans("Status")
STranDate = rsPendingTrans("TranDate")
SPatName = rsPendingTrans("PatName")
STranType = rsPendingTrans("TranType")
' strictly for CC support in Letters
i = InStrPS(STranType, "(")
If (i > 0) Then
    STranType = Left(STranType, i - 1)
End If
i = InStrPS(STranType, ",")
If (i > 0) Then
    STranType = Left(STranType, i - 1)
End If
If (Mid(STranType, Len(STranType), 1) = "-") Then
    STranType = Left(STranType, Len(STranType) - 1)
End If
If (Len(STranType) < Len(rsPendingTrans("TranType"))) Then
    STranType = STranType + Space(Len(rsPendingTrans("TranType")) - Len(STranType))
End If
If (rsPendingTrans("Doctor") <> "") Then
    SDoctor = rsPendingTrans("Doctor")
Else
    SDoctor = "Decide Later"
End If
If (rsPendingTrans("RefDoc") <> "") Then
    SRefDoc = rsPendingTrans("RefDoc")
Else
    SRefDoc = "Decide Later"
End If
If (rsPendingTrans("TranId") <> "") Then
    STranId = rsPendingTrans("TranId")
Else
    STranId = 0
End If
End Sub

Public Function DisplayList() As String
Dim i As Integer
Dim Bs As String
DisplayList = SStatus + " " + STranDate + " " + Left(SPatName, 20) + " "
Bs = Mid(STranType, 6, 15)
i = InStrPS(Bs, "/")
If (i > 0) Then
    Bs = Left(Bs, i - 1)
    Bs = Bs + Space(15 - Len(Bs))
Else
    i = InStrPS(Bs, "(")
    If (i > 0) Then
        Bs = Left(Bs, i - 1)
        Bs = Bs + Space(15 - Len(Bs))
    End If
End If
DisplayList = DisplayList + Bs + " " + Left(SDoctor, 30) + " " + Left(SRefDoc, 30)
If (Len(DisplayList) > 97) Then
    DisplayList = Left(DisplayList, 97)
Else
    DisplayList = DisplayList + Space(97 - Len(DisplayList))
End If
Bs = String(10 - Len(Trim(Str(STranId))), 48) + Trim(Str(STranId))
DisplayList = DisplayList + Bs
Bs = String(10 - Len(Trim(Str(SPatId))), 48) + Trim(Str(SPatId))
DisplayList = DisplayList + Bs
Bs = String(10 - Len(Trim(Str(STransTypeId))), 48) + Trim(Str(STransTypeId))
DisplayList = DisplayList + Bs
End Function

Public Sub RetrieveList(recsPerPage As Long, StatusFilter As String, DRFilter As String, LocFilter As String, SDate As String, EDate As String)
Dim recCount As Long
If (Trim(PendStatus) = "") Then
    PendStatus = "P"
End If
If (Trim(EDate) = "") Then
    EDate = "-"
End If
If (Trim(SDate) = "") Then
    SDate = "-"
End If
strSQL = "usp_PagedItems " + Str(CurrentPage) + "," + Str(recsPerPage) + "," + PendType + "," + PendStatus + "," + StatusFilter + "," + DRFilter + "," + LocFilter + "," + SDate + "," + EDate
Set rsPendingTrans = Nothing
Set rsPendingTrans = CreateAdoRecordset
Call rsPendingTrans.Open(strSQL, MyPracticeRepository)
If Not rsPendingTrans.EOF Then
    recCount = CInt(rsPendingTrans("AllRecords"))
    If (recCount - (CurrentPage * recsPerPage) > 0) Then
        LastPage = False
        If ((recCount / recsPerPage) - Int(recCount / recsPerPage) > 0) Then
            MaxPage = Int(recCount / recsPerPage) + 1
        Else
            MaxPage = Int(recCount / recsPerPage)
        End If
    Else
        LastPage = True
        MaxPage = CurrentPage
    End If
Else
    LastPage = True
    MaxPage = CurrentPage
End If
TotalRecs = recCount
If Not (rsPendingTrans.EOF) Then
    Call LoadList
End If
End Sub

Public Sub NextRec()
rsPendingTrans.MoveNext
If Not (rsPendingTrans.EOF) Then
    Call LoadList
End If
End Sub

Public Function EOF() As Boolean
EOF = rsPendingTrans.EOF
End Function

Public Function GetAllRecords() As Long
GetAllRecords = TotalRecs
End Function

