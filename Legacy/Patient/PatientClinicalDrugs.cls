VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientClinicalDrugs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Drugs Plan Object Module
Option Explicit
Public DrugId As Long
Public DrugApptId As Long
Public DrugName As String
Public DrugDosage As String
Public DrugFrequency As String
Public DrugEye As String
Public DrugDuration As String
Public DrugPrescribe As Boolean
Public DrugStartDate As String
Public DrugEndDate As String
Public DrugLastTakenDate As String
Public DrugLastTakenTime As String
Public DrugComment As String
Public DrugStatus As String

Private ModuleName As String
Private PatientClinicalDrugsTotal As Long
Private PatientClinicalDrugsTbl As ADODB.Recordset
Private PatientClinicalDrugsNew As Boolean

Private Sub Class_Initialize()
Set PatientClinicalDrugsTbl = New ADODB.Recordset
Call InitPatientClinicalDrugs
End Sub

Private Sub Class_Terminate()
Call InitPatientClinicalDrugs
Set PatientClinicalDrugsTbl = Nothing
End Sub

Private Sub InitPatientClinicalDrugs()
PatientClinicalDrugsNew = True
DrugId = 0
DrugApptId = 0
DrugName = ""
DrugDosage = ""
DrugFrequency = ""
DrugEye = ""
DrugDuration = ""
DrugPrescribe = False
DrugStartDate = ""
DrugEndDate = ""
DrugLastTakenDate = ""
DrugLastTakenTime = ""
DrugComment = ""
DrugStatus = "A"
End Sub

Private Sub LoadPatientClinicalDrugs()
PatientClinicalDrugsNew = False
If (PatientClinicalDrugsTbl("ClinicalDrugId") <> "") Then
    DrugId = PatientClinicalDrugsTbl("ClinicalDrugId")
Else
    DrugId = 0
End If
If (PatientClinicalDrugsTbl("ClinicalDrugsApptId") <> "") Then
    DrugApptId = PatientClinicalDrugsTbl("ClinicalApptId")
Else
    DrugApptId = 0
End If
If (PatientClinicalDrugsTbl("ClinicalDrugName") <> "") Then
    DrugName = PatientClinicalDrugsTbl("ClinicalDrugsName")
Else
    DrugName = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugDosage") <> "") Then
    DrugDosage = PatientClinicalDrugsTbl("ClinicalDrugDosage")
Else
    DrugDosage = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugFrequency") <> "") Then
    DrugFrequency = PatientClinicalDrugsTbl("ClinicalDrugFrequency")
Else
    DrugFrequency = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugDuration") <> "") Then
    DrugDuration = PatientClinicalDrugsTbl("ClinicalDrugDuration")
Else
    DrugDuration = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugEye") <> "") Then
    DrugEye = PatientClinicalDrugsTbl("ClinicalDrugEye")
Else
    DrugEye = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugStartDate") <> "") Then
    DrugStartDate = PatientClinicalDrugsTbl("ClinicalDrugStartDate")
Else
    DrugStartDate = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugEndDate") <> "") Then
    DrugEndDate = PatientClinicalDrugsTbl("ClinicalDrugEndDate")
Else
    DrugEndDate = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugLastTakenDate") <> "") Then
    DrugLastTakenDate = PatientClinicalDrugsTbl("ClinicalDrugLastTakenDate")
Else
    DrugLastTakenDate = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugLastTakenTime") <> "") Then
    DrugLastTakenTime = PatientClinicalDrugsTbl("ClinicalDrugLastTakenTime")
Else
    DrugLastTakenTime = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugPrescribe") <> "") Then
    If (PatientClinicalDrugsTbl("ClinicalDrugPrescribe") = "T") Then
        DrugPrescribe = True
    Else
        DrugPrescribe = False
    End If
Else
    DrugPrescribe = False
End If
If (PatientClinicalDrugsTbl("ClinicalDrugComment") <> "") Then
    DrugComment = PatientClinicalDrugsTbl("ClinicalDrugComment")
Else
    DrugComment = ""
End If
If (PatientClinicalDrugsTbl("ClinicalDrugStatus") <> "") Then
    DrugStatus = PatientClinicalDrugsTbl("ClinicalDrugStatus")
Else
    DrugStatus = ""
End If
End Sub

Private Function GetListPatientClinicalDrugs() As Long
On Error GoTo DbErrorHandler
Dim w As Integer
Dim Temp As String
Dim Ref As String
Dim OrderString As String
Ref = "And "
PatientClinicalDrugsTotal = 0
GetListPatientClinicalDrugs = -1
If (DrugApptId < 1) Then
    Exit Function
End If
OrderString = "Select * FROM PatientClinicalDrugs WHERE ClinicalApptId =" + Str(DrugApptId) + " "
If (Trim(DrugName) <> "") Then
    OrderString = OrderString + Ref + "ClinicalDrugName = '" + Trim(DrugName) + "' "
    Ref = "And "
End If
If (Trim(DrugEye) <> "") Then
    OrderString = OrderString + Ref + "ClinicalDrugEye = '" + Trim(DrugEye) + "' "
    Ref = "And "
End If
If (Trim(DrugStartDate) <> "") Then
    OrderString = OrderString + Ref + "ClinicalDrugStartDate >= '" + Trim(DrugStartDate) + "' "
    Ref = "And "
End If
If (Trim(DrugEndDate) <> "") Then
    OrderString = OrderString + Ref + "ClinicalDrugEndDate <= '" + Trim(DrugEndDate) + "' "
    Ref = "And "
End If
If (Trim(DrugStatus) <> "") Then
    If (Left(DrugStatus, 1) = "-") Then
        OrderString = OrderString + Ref + "ClinicalDrugStatus <> '" + Mid(DrugStatus, 2, 1) + "' "
    Else
        OrderString = OrderString + Ref + "ClinicalDrugStatus = '" + Trim(DrugStatus) + "' "
    End If
    Ref = "And "
End If
Set PatientClinicalDrugsTbl = Nothing
Set PatientClinicalDrugsTbl = New ADODB.Recordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientClinicalDrugsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientClinicalDrugsTbl.EOF) Then
    GetListPatientClinicalDrugs = -1
Else
    PatientClinicalDrugsTbl.MoveLast
    PatientClinicalDrugsTotal = PatientClinicalDrugsTbl.RecordCount
    GetListPatientClinicalDrugs = PatientClinicalDrugsTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetListPatientClinicalDrugs"
    GetListPatientClinicalDrugs = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientClinicalDrugs() As Boolean
On Error GoTo DbErrorHandler
GetPatientClinicalDrugs = True
Dim OrderString As String
OrderString = "SELECT * FROM PatientClinicalDrugs WHERE ClinicalDrugId =" + Str(DrugId)
Set PatientClinicalDrugsTbl = Nothing
Set PatientClinicalDrugsTbl = New ADODB.Recordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientClinicalDrugsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientClinicalDrugsTbl.EOF) Then
    Call InitPatientClinicalDrugs
Else
    Call LoadPatientClinicalDrugs
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientClinicalDrugs"
    GetPatientClinicalDrugs = False
    DrugId = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function AxePatientClinicalDrugs() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
AxePatientClinicalDrugs = True
PatientClinicalDrugsTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "AxePatientClinicalDrugs"
    AxePatientClinicalDrugs = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientClinicalDrugs() As Boolean
On Error GoTo DbErrorHandler
PutPatientClinicalDrugs = True
If (PatientClinicalDrugsNew) Or (DrugId < 1) Then
    PatientClinicalDrugsTbl.AddNew
End If
PatientClinicalDrugsTbl("ClinicalApptId") = DrugApptId
PatientClinicalDrugsTbl("ClinicalDrugName") = DrugName
PatientClinicalDrugsTbl("ClinicalDrugDosage") = DrugDosage
PatientClinicalDrugsTbl("ClinicalDrugFrequency") = DrugFrequency
PatientClinicalDrugsTbl("ClinicalDrugDuration") = DrugDuration
PatientClinicalDrugsTbl("ClinicalDrugEye") = DrugEye
PatientClinicalDrugsTbl("ClinicalDrugStartDate") = DrugStartDate
PatientClinicalDrugsTbl("ClinicalDrugEndDate") = DrugEndDate
PatientClinicalDrugsTbl("ClinicalDrugLastTakenDate") = DrugLastTakenDate
PatientClinicalDrugsTbl("ClinicalDrugLastTakenTime") = DrugLastTakenTime
PatientClinicalDrugsTbl("ClinicalDrugComment") = DrugComment
PatientClinicalDrugsTbl("ClinicalDrugPrescribe") = "F"
If (DrugPrescribe) Then
    PatientClinicalDrugsTbl("ClinicalDrugPrescribe") = "T"
End If
PatientClinicalDrugsTbl("ClinicalDrugStatus") = DrugStatus
PatientClinicalDrugsTbl.Update
If (PatientClinicalDrugsNew) Then
    PatientClinicalDrugsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientClinicalDrugs"
    PutPatientClinicalDrugs = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyPatientClinicalDrugs() As Boolean
ApplyPatientClinicalDrugs = PutPatientClinicalDrugs
End Function

Public Function DeletePatientClinicalDrugs() As Boolean
DeletePatientClinicalDrugs = PutPatientClinicalDrugs
End Function

Public Function KillPatientClinicalDrugs() As Boolean
KillPatientClinicalDrugs = AxePatientClinicalDrugs
End Function

Public Function RetrievePatientClinicalDrugs() As Boolean
RetrievePatientClinicalDrugs = GetPatientClinicalDrugs
End Function

Public Function SelectPatientClinicalDrugs(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPatientClinicalDrugs = False
If (PatientClinicalDrugsTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientClinicalDrugsTotal) Then
    Exit Function
End If
PatientClinicalDrugsTbl.MoveFirst
PatientClinicalDrugsTbl.Move ListItem - 1
SelectPatientClinicalDrugs = True
Call LoadPatientClinicalDrugs
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientClinicalDrugs"
    SelectPatientClinicalDrugs = False
    DrugId = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientClinicalDrugs() As Long
FindPatientClinicalDrugs = GetListPatientClinicalDrugs()
End Function
