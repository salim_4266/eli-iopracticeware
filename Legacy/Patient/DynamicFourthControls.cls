VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicFourthControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicControl Object Module
Option Explicit
Public Language As String
Public FourthControlId As Long
Public FourthFormId As Long
Public ControlName As String
Public ControlType As String
Public BackColor As Long
Public ForeColor As Long
Public TabIndex As Long
Public ResponseLength As Long
Public ControlVisible As Boolean
Public ControlTemplateName As String
Public ControlFont As String
Public ControlTop As Long
Public ControlLeft As Long
Public ControlHeight As Long
Public ControlWidth As Long
Public ControlText As String
Public ControlAlternateText As String
Public ControlAlternateTrigger As String
Public ControlVisibleAfterTrigger As Boolean
Public ConfirmLingo As String
Public DrugQuestion As Boolean
Public DoctorLingo As String
Public IEChoiceName As String
Public DecimalDefault As String
Public Exclusion As String
Public LetterLanguage As String
Public LetterLanguage1 As String
Public ICDAlias1 As String
Public ICDAlias2 As String
Public ICDAlias3 As String
Public ICDAlias4 As String
Public Ros As String
Public AIncrement As String

Private ModuleName As String
Private ClinicalControlsTbl As ADODB.Recordset
Private DynamicControlTotal As Long
Private ClinicalControlsNew As Boolean

Private Sub Class_Initialize()
Set ClinicalControlsTbl = New ADODB.Recordset
ClinicalControlsTbl.CursorLocation = adUseClient
Call InitControl
End Sub

Private Sub Class_Terminate()
Call InitControl
Set ClinicalControlsTbl = Nothing
End Sub

Private Sub InitControl()
ClinicalControlsNew = False
ControlName = ""
ControlType = ""
BackColor = 0
ForeColor = 0
TabIndex = 0
ResponseLength = 0
ControlVisible = True
ControlTemplateName = ""
ControlFont = ""
ControlTop = 0
ControlLeft = 0
ControlHeight = 0
ControlWidth = 0
ControlText = ""
ControlAlternateText = ""
ControlAlternateTrigger = ""
ControlVisibleAfterTrigger = False
ConfirmLingo = ""
DrugQuestion = False
DoctorLingo = ""
IEChoiceName = ""
DecimalDefault = ""
Exclusion = ""
LetterLanguage = ""
LetterLanguage1 = ""
Ros = ""
AIncrement = ""
End Sub

Private Sub LoadControl()
ClinicalControlsNew = False
If (ClinicalControlsTbl("FourthFormId") <> "") Then
    FourthFormId = ClinicalControlsTbl("FourthFormId")
Else
    FourthFormId = 0
End If
If (ClinicalControlsTbl("FourthControlId") <> "") Then
    FourthControlId = ClinicalControlsTbl("FourthControlId")
Else
    FourthControlId = 0
End If
If (ClinicalControlsTbl("ControlName") <> "") Then
    ControlName = ClinicalControlsTbl("ControlName")
Else
    ControlName = ""
End If
If (ClinicalControlsTbl("ControlType") <> "") Then
    ControlType = ClinicalControlsTbl("ControlType")
Else
    ControlType = ""
End If
If (ClinicalControlsTbl("BackColor") <> "") Then
    BackColor = ClinicalControlsTbl("BackColor")
Else
    BackColor = 0
End If
If (ClinicalControlsTbl("ForeColor") <> "") Then
    ForeColor = ClinicalControlsTbl("ForeColor")
Else
    ForeColor = 0
End If
If (ClinicalControlsTbl("TabIndex") <> "") Then
    TabIndex = ClinicalControlsTbl("TabIndex")
Else
    TabIndex = 0
End If
If (ClinicalControlsTbl("ResponseLength") <> "") Then
    ResponseLength = ClinicalControlsTbl("ResponseLength")
Else
    ResponseLength = 0
End If
If (ClinicalControlsTbl("ControlVisible") <> "") Then
    If (ClinicalControlsTbl("ControlVisible") = "T") Then
        ControlVisible = True
    Else
        ControlVisible = False
    End If
Else
    ControlVisible = True
End If
If (ClinicalControlsTbl("ControlTemplateName") <> "") Then
    ControlTemplateName = ClinicalControlsTbl("ControlTemplateName")
Else
    ControlTemplateName = ""
End If
If (ClinicalControlsTbl("ControlFont") <> "") Then
    ControlFont = ClinicalControlsTbl("ControlFont")
Else
    ControlFont = ""
End If
If (ClinicalControlsTbl("ControlTop") <> "") Then
    ControlTop = ClinicalControlsTbl("ControlTop")
Else
    ControlTop = 0
End If
If (ClinicalControlsTbl("ControlLeft") <> "") Then
    ControlLeft = ClinicalControlsTbl("ControlLeft")
Else
    ControlLeft = 0
End If
If (ClinicalControlsTbl("ControlHeight") <> "") Then
    ControlHeight = ClinicalControlsTbl("ControlHeight")
Else
    ControlHeight = 0
End If
If (ClinicalControlsTbl("ControlWidth") <> "") Then
    ControlWidth = ClinicalControlsTbl("ControlWidth")
Else
    ControlWidth = 0
End If
If (ClinicalControlsTbl("ControlText") <> "") Then
    ControlText = ClinicalControlsTbl("ControlText")
Else
    ControlText = ""
End If
If (ClinicalControlsTbl("ControlAlternateText") <> "") Then
    ControlAlternateText = ClinicalControlsTbl("ControlAlternateText")
Else
    ControlAlternateText = ""
End If
If (ClinicalControlsTbl("ControlAlternateTrigger") <> "") Then
    ControlAlternateTrigger = ClinicalControlsTbl("ControlAlternateTrigger")
Else
    ControlAlternateTrigger = ""
End If
If (ClinicalControlsTbl("ControlVisibleAfterTrigger") <> "") Then
    If (ClinicalControlsTbl("ControlVisibleAfterTrigger") = "T") Then
        ControlVisibleAfterTrigger = True
    Else
        ControlVisibleAfterTrigger = False
    End If
Else
    ControlVisibleAfterTrigger = False
End If
If (ClinicalControlsTbl("ConfirmLingo") <> "") Then
    ConfirmLingo = ClinicalControlsTbl("ConfirmLingo")
Else
    ConfirmLingo = ""
End If
If (ClinicalControlsTbl("DoctorLingo") <> "") Then
    DoctorLingo = ClinicalControlsTbl("DoctorLingo")
Else
    DoctorLingo = ""
End If
If (ClinicalControlsTbl("InferenceEngineChoiceName") <> "") Then
    IEChoiceName = ClinicalControlsTbl("InferenceEngineChoiceName")
Else
    IEChoiceName = ""
End If
If (ClinicalControlsTbl("DrugQuestion") <> "") Then
    If (ClinicalControlsTbl("DrugQuestion") = "T") Then
        DrugQuestion = True
    Else
        DrugQuestion = False
    End If
Else
    DrugQuestion = False
End If
If (ClinicalControlsTbl("DecimalDefault") <> "") Then
    DecimalDefault = ClinicalControlsTbl("DecimalDefault")
Else
    DecimalDefault = ""
End If
If (ClinicalControlsTbl("Exclusion") <> "") Then
    Exclusion = ClinicalControlsTbl("Exclusion")
Else
    Exclusion = ""
End If
If (ClinicalControlsTbl("LetterTranslation") <> "") Then
    LetterLanguage = ClinicalControlsTbl("LetterTranslation")
Else
    LetterLanguage = ""
End If
If (ClinicalControlsTbl("OtherLtrTrans") <> "") Then
    LetterLanguage1 = ClinicalControlsTbl("OtherLtrTrans")
Else
    LetterLanguage1 = ""
End If
If (ClinicalControlsTbl("ICDAlias3") <> "") Then
    ICDAlias3 = ClinicalControlsTbl("ICDAlias3")
Else
    ICDAlias3 = ""
End If
If (ClinicalControlsTbl("Ros") <> "") Then
    Ros = ClinicalControlsTbl("Ros")
Else
    Ros = ""
End If
If (ClinicalControlsTbl("Increment") <> "") Then
    AIncrement = ClinicalControlsTbl("Increment")
Else
    AIncrement = ""
End If
If (Trim(Language) <> "") Then
    Call ConvertToLanguage(Language)
End If
End Sub

Private Function ConvertToLanguage(TheLang) As Boolean
Dim RetLang As DynamicFourthLanguage
ConvertToLanguage = True
Set RetLang = New DynamicFourthLanguage
RetLang.ControlId = FourthControlId
RetLang.Language = TheLang
If (RetLang.FindLanguageControl > 0) Then
    If (RetLang.SelectLanguageControl(1)) Then
        If (Trim(RetLang.ControlText) <> "") Then
            ControlText = RetLang.ControlText
        End If
        If (Trim(RetLang.ConfirmLingo) <> "") Then
            ConfirmLingo = RetLang.ConfirmLingo
        End If
    End If
End If
Set RetLang = Nothing
End Function

Private Function GetControl()
On Error GoTo DbErrorHandler
GetControl = True
Dim OrderString As String
If (FourthControlId <= 0) Then
    Call InitControl
    Exit Function
End If
OrderString = "SELECT * FROM FourthFormsControls WHERE FourthControlId =" + Str(FourthControlId)
If (Trim(ControlName) <> "") Then
    OrderString = OrderString + "And ControlName = '" + Trim(ControlName) + "' "
End If
If (Trim(IEChoiceName) <> "") Then
    OrderString = OrderString + "And InferenceEngineChoiceName = '" + Trim(IEChoiceName) + "' "
End If
Set ClinicalControlsTbl = Nothing
Set ClinicalControlsTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalControlsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClinicalControlsTbl.EOF) Then
    Call InitControl
Else
    Call LoadControl
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetControl"
    GetControl = False
    FourthControlId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutControl()
On Error GoTo DbErrorHandler
PutControl = True
If (ClinicalControlsNew) Then
    ClinicalControlsTbl.AddNew
End If
ClinicalControlsTbl("FourthFormId") = FourthFormId
ClinicalControlsTbl("ControlName") = UCase(Trim(ControlName))
ClinicalControlsTbl("ControlType") = UCase(Trim(ControlType))
ClinicalControlsTbl("BackColor") = BackColor
ClinicalControlsTbl("ForeColor") = ForeColor
ClinicalControlsTbl("TabIndex") = TabIndex
ClinicalControlsTbl("ResponseLength") = ResponseLength
ClinicalControlsTbl("ControlVisible") = "F"
If (ControlVisible) Then
    ClinicalControlsTbl("ControlVisible") = "T"
End If
ClinicalControlsTbl("ControlTemplateName") = ControlTemplateName
ClinicalControlsTbl("ControlFont") = ControlFont
ClinicalControlsTbl("ControlTop") = ControlTop
ClinicalControlsTbl("ControlLeft") = ControlLeft
ClinicalControlsTbl("ControlHeight") = ControlHeight
ClinicalControlsTbl("ControlWidth") = ControlWidth
ClinicalControlsTbl("ControlText") = ControlText
ClinicalControlsTbl("ControlAlternateText") = ControlAlternateText
ClinicalControlsTbl("ControlAlternateTrigger") = ControlAlternateTrigger
ClinicalControlsTbl("ControlVisibleAfterTrigger") = "F"
If (ControlVisibleAfterTrigger) Then
    ClinicalControlsTbl("ControlVisibleAfterTrigger") = "T"
End If
ClinicalControlsTbl("ConfirmLingo") = Trim(ConfirmLingo)
ClinicalControlsTbl("DrugQuestion") = "F"
If (DrugQuestion) Then
    ClinicalControlsTbl("DrugQuestion") = "T"
End If
ClinicalControlsTbl("DoctorLingo") = Trim(DoctorLingo)
ClinicalControlsTbl("InferenceEngineChoiceName") = IEChoiceName
ClinicalControlsTbl("DecimalDefault") = DecimalDefault
ClinicalControlsTbl("Exclusion") = Exclusion
ClinicalControlsTbl("LetterTranslation") = LetterLanguage
ClinicalControlsTbl("OtherLtrTrans") = LetterLanguage1
ClinicalControlsTbl("Ros") = Ros
ClinicalControlsTbl("Increment") = AIncrement
ClinicalControlsTbl.Update
If (ClinicalControlsNew) Then
    FourthControlId = ClinicalControlsTbl("ControlId")
    ClinicalControlsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutControl"
    PutControl = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAnyControl() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicControlTotal = 0
GetAnyControl = -1
OrderString = "SELECT * FROM FourthFormsControls ORDER BY FourthFormId ASC, TabIndex ASC "
Set ClinicalControlsTbl = Nothing
Set ClinicalControlsTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalControlsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClinicalControlsTbl.EOF) Then
    GetAnyControl = -1
Else
    ClinicalControlsTbl.MoveLast
    DynamicControlTotal = ClinicalControlsTbl.RecordCount
    GetAnyControl = ClinicalControlsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAnyControl"
    GetAnyControl = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllControl() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicControlTotal = 0
GetAllControl = -1
If (FourthFormId < 1) Then
    Exit Function
End If
OrderString = "SELECT * FROM FourthFormsControls WHERE FourthFormId =" + Str(FourthFormId) + " "
If (Trim(ControlName) <> "") Then
    OrderString = OrderString + "And ControlName = '" + Trim(ControlName) + "' "
End If
If (Trim(IEChoiceName) <> "") Then
    OrderString = OrderString + "And InferenceEngineChoiceName = '" + Trim(IEChoiceName) + "' "
End If
OrderString = OrderString + "ORDER BY FourthFormId ASC, TabIndex ASC "
Set ClinicalControlsTbl = Nothing
Set ClinicalControlsTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalControlsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClinicalControlsTbl.EOF) Then
    GetAllControl = -1
Else
    ClinicalControlsTbl.MoveLast
    DynamicControlTotal = ClinicalControlsTbl.RecordCount
    GetAllControl = ClinicalControlsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllControl"
    GetAllControl = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyControl() As Boolean
ApplyControl = PutControl
End Function

Public Function RetrieveControl() As Boolean
RetrieveControl = GetControl
End Function

Public Function SelectControl(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectControl = False
If (ClinicalControlsTbl.EOF) Or (ListItem < 1) Or (ListItem > DynamicControlTotal) Then
    Exit Function
End If
ClinicalControlsTbl.MoveFirst
ClinicalControlsTbl.Move ListItem - 1
SelectControl = True
Call LoadControl
Exit Function
DbErrorHandler:
    ModuleName = "SelectControl"
    FourthControlId = -256
    SelectControl = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindControl() As Long
FindControl = GetAllControl()
End Function

Public Function FindAnyControl() As Long
FindAnyControl = GetAnyControl()
End Function

