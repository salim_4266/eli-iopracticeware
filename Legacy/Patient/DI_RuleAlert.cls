VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DI_RuleAlert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
 Option Explicit
 Private Const dbConnectionAccess As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
 Public IOPValues As String
 
 Public Sub RuleAlert(ByVal PatientId As Long, ByVal EncounterId As Long, ByVal EventID As Integer _
                       , Optional ByVal FieldArea As String = "", _
                       Optional ByVal FieldName As String = "", Optional ByVal FieldValue As String = "")
    Dim Helper As New ClinicalDecisions.DbHelper
    Helper.DbObject = IdbConnection
    
    Dim objalert As New ClinicalDecisions.clsAlert
    objalert.PatientId = PatientId
    objalert.EncounterId = EncounterId
    objalert.OLEDbConnectionString = dbConnectionAccess
    objalert.MDBFilePath = LocalPinPointDirectory
    objalert.bShowIEAlters = True
    objalert.UserId = UserLogin.iId
    
    'To invoke IOP alerts
    If IOPValues <> "" Then objalert.IOPValues = IOPValues
    
    objalert.InitializeRuleParamers
    If objalert.UpdateFieldValue(FieldArea, FieldName, FieldValue) Then
        objalert.EvaluateRules (EventID)
    End If
End Sub

