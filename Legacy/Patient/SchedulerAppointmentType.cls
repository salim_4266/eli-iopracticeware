VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SchedulerAppointmentType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Scheduler Appointment Type Object Module
Option Explicit
Public AppointmentTypeId As Long
Public AppointmentType As String
Public AppointmentTypeQuestion As String
Public AppointmentTypeDuration As Long
Public AppointmentTypeDurationOther As Long
Public AppointmentTypePeriod As String
Public AppointmentTypeRank As Integer
Public AppointmentTypeOtherRank As Integer
Public AppointmentTypeRecursion As Long
Public AppointmentTypeResourceId1 As Long
Public AppointmentTypeResourceId2 As Long
Public AppointmentTypeResourceId3 As Long
Public AppointmentTypeResourceId4 As Long
Public AppointmentTypeResourceId5 As Long
Public AppointmentTypeResourceId6 As Long
Public AppointmentTypeResourceId7 As Long
Public AppointmentTypeResourceId8 As Long
Public AppointmentTypeDrRequired As Boolean
Public AppointmentTypePcRequired As Boolean
Public AppointmentTypeRecallFreq As Integer
Public AppointmentTypeMaxRecalls As Integer
Public AppointmentTypeCategory As String
Public AppointmentTypeFirstVisit As Boolean
Public AppointmentTypeAvailableInPlan As Boolean
Public AppointmentTypeEMBasis As String

Private SchedulerAppTypeTotal As Long
Private AppointmentTypeStatus As String
Private ModuleName As String
Private SchedulerAppointmentTypeTbl As ADODB.Recordset
Private SchedulerAppointmentTypeNew As Boolean

Private Sub Class_Initialize()
Set SchedulerAppointmentTypeTbl = CreateAdoRecordset
Call InitAppointmentType
End Sub

Private Sub Class_Terminate()
Call InitAppointmentType
Set SchedulerAppointmentTypeTbl = Nothing
End Sub

Private Sub InitAppointmentType()
SchedulerAppointmentTypeNew = True
AppointmentType = ""
AppointmentTypeQuestion = ""
AppointmentTypeDuration = 0
AppointmentTypeDurationOther = 0
AppointmentTypePeriod = ""
AppointmentTypeRecursion = 0
AppointmentTypeRank = 0
AppointmentTypeOtherRank = 0
AppointmentTypeRecallFreq = 0
AppointmentTypeMaxRecalls = 0
AppointmentTypeResourceId1 = 0
AppointmentTypeResourceId2 = 0
AppointmentTypeResourceId3 = 0
AppointmentTypeResourceId4 = 0
AppointmentTypeResourceId5 = 0
AppointmentTypeResourceId6 = 0
AppointmentTypeResourceId7 = 0
AppointmentTypeResourceId8 = 0
AppointmentTypeDrRequired = False
AppointmentTypePcRequired = False
AppointmentTypeFirstVisit = False
AppointmentTypeCategory = ""
AppointmentTypeAvailableInPlan = True
AppointmentTypeEMBasis = ""
AppointmentTypeStatus = "A"
End Sub

Private Sub LoadAppointmentType()
SchedulerAppointmentTypeNew = False
If (SchedulerAppointmentTypeTbl("AppTypeId") <> "") Then
    AppointmentTypeId = SchedulerAppointmentTypeTbl("AppTypeId")
Else
    AppointmentTypeId = 0
End If
If (SchedulerAppointmentTypeTbl("AppointmentType") <> "") Then
    AppointmentType = SchedulerAppointmentTypeTbl("AppointmentType")
Else
    AppointmentType = ""
End If
If (SchedulerAppointmentTypeTbl("Question") <> "") Then
    AppointmentTypeQuestion = SchedulerAppointmentTypeTbl("Question")
Else
    AppointmentTypeQuestion = ""
End If
If (SchedulerAppointmentTypeTbl("Duration") <> "") Then
    AppointmentTypeDuration = SchedulerAppointmentTypeTbl("Duration")
Else
    AppointmentTypeDuration = 0
End If
If (SchedulerAppointmentTypeTbl("DurationOther") <> "") Then
    AppointmentTypeDurationOther = SchedulerAppointmentTypeTbl("DurationOther")
Else
    AppointmentTypeDurationOther = 0
End If
If (SchedulerAppointmentTypeTbl("Period") <> "") Then
    AppointmentTypePeriod = SchedulerAppointmentTypeTbl("Period")
Else
    AppointmentTypePeriod = ""
End If
If (SchedulerAppointmentTypeTbl("Rank") <> "") Then
    AppointmentTypeRank = SchedulerAppointmentTypeTbl("Rank")
Else
    AppointmentTypeRank = 0
End If
If (SchedulerAppointmentTypeTbl("OtherRank") <> "") Then
    AppointmentTypeOtherRank = SchedulerAppointmentTypeTbl("OtherRank")
Else
    AppointmentTypeOtherRank = 0
End If
If (SchedulerAppointmentTypeTbl("RecallFreq") <> "") Then
    AppointmentTypeRecallFreq = SchedulerAppointmentTypeTbl("RecallFreq")
Else
    AppointmentTypeRecallFreq = 0
End If
If (SchedulerAppointmentTypeTbl("RecallMax") <> "") Then
    AppointmentTypeMaxRecalls = SchedulerAppointmentTypeTbl("RecallMax")
Else
    AppointmentTypeMaxRecalls = 0
End If
If (SchedulerAppointmentTypeTbl("Recursion") <> "") Then
    AppointmentTypeRecursion = SchedulerAppointmentTypeTbl("Recursion")
Else
    AppointmentTypeRecursion = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId1") <> "") Then
    AppointmentTypeResourceId1 = SchedulerAppointmentTypeTbl("ResourceId1")
Else
    AppointmentTypeResourceId1 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId2") <> "") Then
    AppointmentTypeResourceId2 = SchedulerAppointmentTypeTbl("ResourceId2")
Else
    AppointmentTypeResourceId2 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId3") <> "") Then
    AppointmentTypeResourceId3 = SchedulerAppointmentTypeTbl("ResourceId3")
Else
    AppointmentTypeResourceId3 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId4") <> "") Then
    AppointmentTypeResourceId4 = SchedulerAppointmentTypeTbl("ResourceId4")
Else
    AppointmentTypeResourceId4 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId5") <> "") Then
    AppointmentTypeResourceId5 = SchedulerAppointmentTypeTbl("ResourceId5")
Else
    AppointmentTypeResourceId5 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId6") <> "") Then
    AppointmentTypeResourceId6 = SchedulerAppointmentTypeTbl("ResourceId6")
Else
    AppointmentTypeResourceId6 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId7") <> "") Then
    AppointmentTypeResourceId7 = SchedulerAppointmentTypeTbl("ResourceId7")
Else
    AppointmentTypeResourceId7 = 0
End If
If (SchedulerAppointmentTypeTbl("ResourceId8") <> "") Then
    AppointmentTypeResourceId8 = SchedulerAppointmentTypeTbl("ResourceId8")
Else
    AppointmentTypeResourceId8 = 0
End If
If (SchedulerAppointmentTypeTbl("DrRequired") <> "") Then
    If (SchedulerAppointmentTypeTbl("DrRequired") = "N") Then
        AppointmentTypeDrRequired = False
    Else
        AppointmentTypeDrRequired = True
    End If
Else
    AppointmentTypeDrRequired = False
End If
If (SchedulerAppointmentTypeTbl("FirstVisitIndicator") <> "") Then
    If (SchedulerAppointmentTypeTbl("FirstVisitIndicator") = "N") Then
        AppointmentTypeFirstVisit = False
    Else
        AppointmentTypeFirstVisit = True
    End If
Else
    AppointmentTypeFirstVisit = False
End If
If (SchedulerAppointmentTypeTbl("PcRequired") <> "") Then
    If (SchedulerAppointmentTypeTbl("PcRequired") = "N") Then
        AppointmentTypePcRequired = False
    Else
        AppointmentTypePcRequired = True
    End If
Else
    AppointmentTypePcRequired = False
End If
If (SchedulerAppointmentTypeTbl("Category") <> "") Then
    AppointmentTypeCategory = SchedulerAppointmentTypeTbl("Category")
Else
    AppointmentTypeCategory = ""
End If
If (SchedulerAppointmentTypeTbl("EMBasis") <> "") Then
    AppointmentTypeEMBasis = SchedulerAppointmentTypeTbl("EMBasis")
Else
    AppointmentTypeEMBasis = ""
End If
If (SchedulerAppointmentTypeTbl("Status") <> "") Then
    AppointmentTypeStatus = SchedulerAppointmentTypeTbl("Status")
Else
    AppointmentTypeStatus = ""
End If
If (SchedulerAppointmentTypeTbl("AvailableInPlan") <> "") Then
    If (SchedulerAppointmentTypeTbl("AvailableInPlan") = "F") Then
        AppointmentTypeAvailableInPlan = False
    Else
        AppointmentTypeAvailableInPlan = True
    End If
Else
    AppointmentTypeAvailableInPlan = True
End If
End Sub

Private Function GetAppointmentType() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerAppTypeTotal = 0
GetAppointmentType = -1
If (Len(AppointmentType) <= 1) Then
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType >= '" + UCase(Trim(AppointmentType)) + "' "
Else
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType = '" + UCase(Trim(AppointmentType)) + "' "
End If
Set SchedulerAppointmentTypeTbl = Nothing
Set SchedulerAppointmentTypeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerAppointmentTypeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerAppointmentTypeTbl.EOF) Then
    GetAppointmentType = -1
Else
    SchedulerAppointmentTypeTbl.MoveLast
    SchedulerAppTypeTotal = SchedulerAppointmentTypeTbl.RecordCount
    GetAppointmentType = SchedulerAppointmentTypeTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAppointmentType"
    GetAppointmentType = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function getAppTypeId(AppId As Long) As String
On Error GoTo DbErrorHandler
getAppTypeId = "ERROR"
Dim SQL As String
Dim RS As Recordset
SQL = " select AT.ResourceId8 from Appointments AP " & _
      " inner join AppointmentType AT on AP.AppTypeId = AT.AppTypeId " & _
      " where AppointmentId = " & AppId

Set RS = GetRS(SQL)

If Not RS.EOF Then
    getAppTypeId = RS(0)
End If

Exit Function
DbErrorHandler:
    ModuleName = "getAppTypeId"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:

End Function


Private Function GetAppointmentTypebyRank() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerAppTypeTotal = 0
GetAppointmentTypebyRank = -1
If (Len(AppointmentType) <= 1) Then
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType >= '" + UCase(Trim(AppointmentType)) + "' "
Else
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType = '" + UCase(Trim(AppointmentType)) + "' "
End If
OrderString = OrderString + "ORDER BY Rank ASC, AppointmentType ASC"
Set SchedulerAppointmentTypeTbl = Nothing
Set SchedulerAppointmentTypeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerAppointmentTypeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerAppointmentTypeTbl.EOF) Then
    GetAppointmentTypebyRank = -1
Else
    SchedulerAppointmentTypeTbl.MoveLast
    SchedulerAppTypeTotal = SchedulerAppointmentTypeTbl.RecordCount
    GetAppointmentTypebyRank = SchedulerAppointmentTypeTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAppointmentTypebyRank"
    GetAppointmentTypebyRank = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAppointmentTypebyRankInPlan() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerAppTypeTotal = 0
GetAppointmentTypebyRankInPlan = -1
If (Len(AppointmentType) <= 1) Then
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType >= '" + UCase(Trim(AppointmentType)) + "' And AvailableInPlan <> 'F' "
Else
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType = '" + UCase(Trim(AppointmentType)) + "' And AvailableInPlan <> 'F' "
End If
OrderString = OrderString + "ORDER BY Rank ASC, AppointmentType ASC"
Set SchedulerAppointmentTypeTbl = Nothing
Set SchedulerAppointmentTypeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerAppointmentTypeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerAppointmentTypeTbl.EOF) Then
    GetAppointmentTypebyRankInPlan = -1
Else
    SchedulerAppointmentTypeTbl.MoveLast
    SchedulerAppTypeTotal = SchedulerAppointmentTypeTbl.RecordCount
    GetAppointmentTypebyRankInPlan = SchedulerAppointmentTypeTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAppointmentTypebyRankInPlan"
    GetAppointmentTypebyRankInPlan = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAppointmentTypebyOtherRank() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerAppTypeTotal = 0
GetAppointmentTypebyOtherRank = -1
If (Len(AppointmentType) <= 1) Then
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType >= '" + UCase(Trim(AppointmentType)) + "' "
Else
    OrderString = "Select * FROM AppointmentType WHERE AppointmentType = '" + UCase(Trim(AppointmentType)) + "' "
End If
OrderString = OrderString + "ORDER BY OtherRank ASC, AppointmentType ASC"
Set SchedulerAppointmentTypeTbl = Nothing
Set SchedulerAppointmentTypeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerAppointmentTypeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerAppointmentTypeTbl.EOF) Then
    GetAppointmentTypebyOtherRank = -1
Else
    SchedulerAppointmentTypeTbl.MoveLast
    SchedulerAppTypeTotal = SchedulerAppointmentTypeTbl.RecordCount
    GetAppointmentTypebyOtherRank = SchedulerAppointmentTypeTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAppointmentTypebyOtherRank"
    GetAppointmentTypebyOtherRank = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetSchedulerAppointmentType() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetSchedulerAppointmentType = True
OrderString = "SELECT * FROM AppointmentType WHERE AppTypeId =" + Trim(Str(AppointmentTypeId)) + " "
Set SchedulerAppointmentTypeTbl = Nothing
Set SchedulerAppointmentTypeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerAppointmentTypeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerAppointmentTypeTbl.EOF) Then
    Call InitAppointmentType
Else
    Call LoadAppointmentType
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetSchedulerAppointmentType"
    AppointmentTypeId = -256
    GetSchedulerAppointmentType = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeSchedulerAppointmentType() As Boolean
On Error GoTo DbErrorHandler
PurgeSchedulerAppointmentType = True
SchedulerAppointmentTypeTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeSchedulerAppointmentType"
    PurgeSchedulerAppointmentType = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutSchedulerAppointmentType() As Boolean
On Error GoTo DbErrorHandler
PutSchedulerAppointmentType = True
If (SchedulerAppointmentTypeNew) Then
    SchedulerAppointmentTypeTbl.AddNew
End If
SchedulerAppointmentTypeTbl("AppointmentType") = UCase(Trim(AppointmentType))
SchedulerAppointmentTypeTbl("Question") = Trim(AppointmentTypeQuestion)
SchedulerAppointmentTypeTbl("Duration") = AppointmentTypeDuration
SchedulerAppointmentTypeTbl("DurationOther") = AppointmentTypeDurationOther
SchedulerAppointmentTypeTbl("Period") = AppointmentTypePeriod
SchedulerAppointmentTypeTbl("Recursion") = AppointmentTypeRecursion
SchedulerAppointmentTypeTbl("Rank") = AppointmentTypeRank
SchedulerAppointmentTypeTbl("OtherRank") = AppointmentTypeOtherRank
SchedulerAppointmentTypeTbl("RecallFreq") = AppointmentTypeRecallFreq
SchedulerAppointmentTypeTbl("RecallMax") = AppointmentTypeMaxRecalls
SchedulerAppointmentTypeTbl("ResourceId1") = AppointmentTypeResourceId1
SchedulerAppointmentTypeTbl("ResourceId2") = AppointmentTypeResourceId2
SchedulerAppointmentTypeTbl("ResourceId3") = AppointmentTypeResourceId3
SchedulerAppointmentTypeTbl("ResourceId4") = AppointmentTypeResourceId4
SchedulerAppointmentTypeTbl("ResourceId5") = AppointmentTypeResourceId5
SchedulerAppointmentTypeTbl("ResourceId6") = AppointmentTypeResourceId6
SchedulerAppointmentTypeTbl("ResourceId7") = AppointmentTypeResourceId7
SchedulerAppointmentTypeTbl("ResourceId8") = AppointmentTypeResourceId8
If (AppointmentTypeDrRequired) Then
    SchedulerAppointmentTypeTbl("DrRequired") = "Y"
Else
    SchedulerAppointmentTypeTbl("DrRequired") = "N"
End If
If (AppointmentTypeFirstVisit) Then
    SchedulerAppointmentTypeTbl("FirstVisitIndicator") = "Y"
Else
    SchedulerAppointmentTypeTbl("FirstVisitIndicator") = "N"
End If
If (AppointmentTypePcRequired) Then
    SchedulerAppointmentTypeTbl("PcRequired") = "Y"
Else
    SchedulerAppointmentTypeTbl("PcRequired") = "N"
End If
If (AppointmentTypeAvailableInPlan) Then
    SchedulerAppointmentTypeTbl("AvailableInPlan") = "T"
Else
    SchedulerAppointmentTypeTbl("AvailableInPlan") = "F"
End If
SchedulerAppointmentTypeTbl("Category") = AppointmentTypeCategory
SchedulerAppointmentTypeTbl("EMBasis") = AppointmentTypeEMBasis
SchedulerAppointmentTypeTbl("Status") = AppointmentTypeStatus
SchedulerAppointmentTypeTbl.Update
If (SchedulerAppointmentTypeNew) Then
    AppointmentTypeId = GetJustAddedRecord
    SchedulerAppointmentTypeNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutSchedulerAppointmentType"
    PutSchedulerAppointmentType = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetAppointmentType > 0) Then
    If (SelectAppointmentType(1)) Then
        GetJustAddedRecord = SchedulerAppointmentTypeTbl("AppTypeId")
    End If
End If
End Function

Public Function ApplySchedulerAppointmentType() As Boolean
ApplySchedulerAppointmentType = PutSchedulerAppointmentType
End Function

Public Function DeleteSchedulerAppointmentType() As Boolean
DeleteSchedulerAppointmentType = PurgeSchedulerAppointmentType
End Function

Public Function RetrieveSchedulerAppointmentType() As Boolean
RetrieveSchedulerAppointmentType = GetSchedulerAppointmentType
End Function

Public Function SelectAppointmentType(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectAppointmentType = False
If (SchedulerAppointmentTypeTbl.EOF) Or (ListItem < 1) Or (ListItem > SchedulerAppTypeTotal) Then
    Exit Function
End If
SchedulerAppointmentTypeTbl.MoveFirst
SchedulerAppointmentTypeTbl.Move ListItem - 1
SelectAppointmentType = True
Call LoadAppointmentType
Exit Function
DbErrorHandler:
    ModuleName = "SelectAppointmentType"
    AppointmentTypeId = -256
    SelectAppointmentType = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindAppointmentType() As Long
FindAppointmentType = GetAppointmentType()
End Function

Public Function FindAppointmentTypebyRank() As Long
FindAppointmentTypebyRank = GetAppointmentTypebyRank()
End Function

Public Function FindAppointmentTypebyRankInPlan() As Long
FindAppointmentTypebyRankInPlan = GetAppointmentTypebyRankInPlan()
End Function

Public Function FindAppointmentTypebyOtherRank() As Long
FindAppointmentTypebyOtherRank = GetAppointmentTypebyOtherRank()
End Function

