VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeTransactionJournal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeTransactionJournal Object Module
Option Explicit
Public TransactionJournalId As Long
Public TransactionJournalType As String
Public TransactionJournalTypeId As Long
Public TransactionJournalStatus As String
Private mTransactionJournalStatus As String
Public TransactionJournalDate As String
Public TransactionJournalEndDate As String
Public TransactionJournalTime As Long
Public TransactionJournalReference As String
Public TransactionJournalRemark As String
Public TransactionJournalAction As String
Public TransactionJournalBatch As String
Public TransactionJournalServiceItem As Long
Public TransactionJournalRcvrId As Long
Public TransactionJournalAssign As String

Private ModuleName As String
Private TransactionJournalTotal As Long
Private TransactionJournalTbl As ADODB.Recordset
Private TransactionJournalNew As Boolean

Private Sub Class_Initialize()
Set TransactionJournalTbl = CreateAdoRecordset
Call InitTransactionJournal
End Sub

Private Sub Class_Terminate()
Call InitTransactionJournal
Set TransactionJournalTbl = Nothing
End Sub

Private Sub InitTransactionJournal()
TransactionJournalNew = True
TransactionJournalType = ""
TransactionJournalTypeId = 0
TransactionJournalDate = ""
TransactionJournalEndDate = ""
TransactionJournalTime = 0
TransactionJournalReference = ""
TransactionJournalRemark = ""
TransactionJournalStatus = "P"
TransactionJournalAction = ""
TransactionJournalBatch = ""
TransactionJournalServiceItem = 0
TransactionJournalRcvrId = 0
TransactionJournalAssign = ""
End Sub

Private Sub LoadTransactionJournal()
TransactionJournalNew = False
If (TransactionJournalTbl("TransactionId") <> "") Then
    TransactionJournalId = TransactionJournalTbl("TransactionId")
Else
    TransactionJournalId = 0
End If
If (TransactionJournalTbl("TransactionType") <> "") Then
    TransactionJournalType = TransactionJournalTbl("TransactionType")
Else
    TransactionJournalType = ""
End If
If (TransactionJournalTbl("TransactionTypeId") <> "") Then
    TransactionJournalTypeId = TransactionJournalTbl("TransactionTypeId")
Else
    TransactionJournalTypeId = 0
End If
If (TransactionJournalTbl("TransactionStatus") <> "") Then
    TransactionJournalStatus = TransactionJournalTbl("TransactionStatus")
Else
    TransactionJournalStatus = ""
End If
mTransactionJournalStatus = TransactionJournalStatus
If (TransactionJournalTbl("TransactionDate") <> "") Then
    TransactionJournalDate = TransactionJournalTbl("TransactionDate")
Else
    TransactionJournalDate = ""
End If
If (TransactionJournalTbl("TransactionTime") <> "") Then
    TransactionJournalTime = TransactionJournalTbl("TransactionTime")
Else
    TransactionJournalTime = 0
End If
If (TransactionJournalTbl("TransactionRef") <> "") Then
    TransactionJournalReference = TransactionJournalTbl("TransactionRef")
Else
    TransactionJournalReference = ""
End If
If (TransactionJournalTbl("TransactionRemark") <> "") Then
    TransactionJournalRemark = TransactionJournalTbl("TransactionRemark")
Else
    TransactionJournalRemark = ""
End If
If (TransactionJournalTbl("TransactionAction") <> "") Then
    TransactionJournalAction = TransactionJournalTbl("TransactionAction")
Else
    TransactionJournalAction = ""
End If
If (TransactionJournalTbl("TransactionBatch") <> "") Then
    TransactionJournalBatch = TransactionJournalTbl("TransactionBatch")
Else
    TransactionJournalBatch = ""
End If
If (TransactionJournalTbl("TransactionServiceItem") <> "") Then
    TransactionJournalServiceItem = TransactionJournalTbl("TransactionServiceItem")
Else
    TransactionJournalServiceItem = 0
End If
If (TransactionJournalTbl("TransactionRcvrId") <> "") Then
    TransactionJournalRcvrId = TransactionJournalTbl("TransactionRcvrId")
Else
    TransactionJournalRcvrId = 0
End If
If (TransactionJournalTbl("TransactionAssignment") <> "") Then
    TransactionJournalAssign = TransactionJournalTbl("TransactionAssignment")
Else
    TransactionJournalAssign = ""
End If
End Sub

Private Function GetTransactionJournal()
On Error GoTo DbErrorHandler
GetTransactionJournal = False
Dim OrderString As String
OrderString = "SELECT * FROM PracticeTransactionJournal WHERE TransactionId =" + Str(TransactionJournalId)
Set TransactionJournalTbl = Nothing
Set TransactionJournalTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call TransactionJournalTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (TransactionJournalTbl.EOF) Then
    Call InitTransactionJournal
Else
    Call LoadTransactionJournal
End If
GetTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournal"
    GetTransactionJournal = False
    TransactionJournalId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetTransactionJournalListMonthly() As Long
On Error GoTo DbErrorHandler
Dim l As Integer
Dim OrderString As String
TransactionJournalTotal = 0
GetTransactionJournalListMonthly = -1
If (Trim(TransactionJournalType) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeTransactionJournal WHERE TransactionType = '" + UCase(Trim(TransactionJournalType)) + "' "
OrderString = OrderString + " And TransactionStatus <> 'Z' "
OrderString = OrderString + " And TransactionRef = 'P' "
OrderString = OrderString + " And TransactionAction = 'P' "
OrderString = OrderString + "ORDER BY TransactionDate DESC, TransactionTime ASC "
Set TransactionJournalTbl = Nothing
Set TransactionJournalTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call TransactionJournalTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (TransactionJournalTbl.EOF) Then
    GetTransactionJournalListMonthly = -1
    Call InitTransactionJournal
Else
    TransactionJournalTbl.MoveLast
    TransactionJournalTotal = TransactionJournalTbl.RecordCount
    GetTransactionJournalListMonthly = TransactionJournalTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournalListMonthly"
    GetTransactionJournalListMonthly = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetTransactionJournalList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim l As Integer
Dim Ref As String
Dim OrderString As String
Ref = ""
TransactionJournalTotal = 0
GetTransactionJournalList = -1
If (Trim(TransactionJournalType) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeTransactionJournal WHERE "
If (TransactionJournalTypeId > 0) Then
    OrderString = OrderString + Ref + "TransactionTypeId =" + Str(TransactionJournalTypeId) + " "
    Ref = "And "
End If
If (Trim(TransactionJournalAction) <> "") Then
    OrderString = OrderString + Ref + "TransactionAction = '" + Trim(TransactionJournalAction) + "' "
    Ref = "And "
End If
If (Trim(TransactionJournalType) <> "") Then
    If (Len(TransactionJournalType) > 1) Then
        OrderString = OrderString + Ref + "("
        For l = 1 To Len(TransactionJournalType)
            If (l <> Len(TransactionJournalType)) Then
                OrderString = OrderString + "TransactionType = '" + Mid(TransactionJournalType, l, 1) + "' Or "
            Else
                OrderString = OrderString + "TransactionType = '" + Mid(TransactionJournalType, l, 1) + "'"
            End If
        Next l
        OrderString = OrderString + ") "
        Ref = "And "
    Else
        OrderString = OrderString + Ref + "TransactionType = '" + Trim(TransactionJournalType) + "' "
        Ref = "And "
    End If
End If
If (Trim(TransactionJournalReference) <> "") Then
    OrderString = OrderString + Ref + "TransactionRef = '" + Trim(TransactionJournalReference) + "' "
    Ref = "And "
End If
If (Trim(TransactionJournalDate) <> "") And (Trim(TransactionJournalEndDate) <> "") Then
    OrderString = OrderString + Ref + "TransactionDate <= '" + Trim(TransactionJournalDate) + "'"
    Ref = "And "
    OrderString = OrderString + Ref + "TransactionDate >= '" + Trim(TransactionJournalEndDate) + "' "
    Ref = "And "
ElseIf (Trim(TransactionJournalDate) <> "") Then
    Select Case IType
    Case -1
        OrderString = OrderString + Ref + "TransactionDate = '" + Trim(TransactionJournalDate) + "' "
    Case -2
        OrderString = OrderString + Ref + "TransactionDate >= '" + Trim(TransactionJournalDate) + "' "
    Case Else
        OrderString = OrderString + Ref + "TransactionDate <= '" + Trim(TransactionJournalDate) + "' "
    End Select
    Ref = "And "
End If
If (Trim(TransactionJournalRcvrId) > 0) Then
    OrderString = OrderString + Ref + "TransactionRcvrId = " + Trim(TransactionJournalRcvrId) + " "
    Ref = "And "
End If
If (Trim(TransactionJournalAssign) <> "") Then
    OrderString = OrderString + Ref + "TransactionAssignment = '" + Trim(TransactionJournalAssign) + "' "
    Ref = "And "
End If
If (Trim(TransactionJournalStatus) <> "") Then
    If (Left(TransactionJournalStatus, 1) = "-") Then
        OrderString = OrderString + "And ("
        For l = 2 To Len(TransactionJournalStatus)
            If (l <> Len(TransactionJournalStatus)) Then
                OrderString = OrderString + "TransactionStatus <> '" + Mid(TransactionJournalStatus, l, 1) + "' And "
            Else
                OrderString = OrderString + "TransactionStatus <> '" + Mid(TransactionJournalStatus, l, 1) + "'"
            End If
        Next l
        OrderString = OrderString + ") "
    Else
        OrderString = OrderString + Ref + "TransactionStatus = '" + Trim(TransactionJournalStatus) + "' "
    End If
    Ref = "And "
End If
If (IType = -1) Then
    OrderString = OrderString + "ORDER BY TransactionId DESC "
Else
    OrderString = OrderString + "ORDER BY TransactionDate DESC, TransactionTime ASC "
End If
Set TransactionJournalTbl = Nothing
Set TransactionJournalTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call TransactionJournalTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (TransactionJournalTbl.EOF) Then
    GetTransactionJournalList = -1
    Call InitTransactionJournal
Else
    TransactionJournalTbl.MoveLast
    TransactionJournalTotal = TransactionJournalTbl.RecordCount
    GetTransactionJournalList = TransactionJournalTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournalList"
    GetTransactionJournalList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutTransactionJournal()
On Error GoTo DbErrorHandler
PutTransactionJournal = True
If (TransactionJournalNew) Then
    TransactionJournalTbl.AddNew
End If
TransactionJournalTbl("TransactionType") = UCase(Trim(TransactionJournalType))
TransactionJournalTbl("TransactionTypeId") = TransactionJournalTypeId
TransactionJournalStatus = UCase(Trim(TransactionJournalStatus))
TransactionJournalTbl("TransactionStatus") = TransactionJournalStatus
TransactionJournalTbl("TransactionDate") = Trim(TransactionJournalDate)
TransactionJournalTbl("TransactionTime") = TransactionJournalTime
TransactionJournalTbl("TransactionRef") = Trim(TransactionJournalReference)
TransactionJournalTbl("TransactionRemark") = Trim(TransactionJournalRemark)
TransactionJournalTbl("TransactionAction") = UCase(TransactionJournalAction)
TransactionJournalTbl("TransactionBatch") = Trim(TransactionJournalBatch)
TransactionJournalTbl("TransactionServiceItem") = TransactionJournalServiceItem
TransactionJournalTbl("TransactionRcvrId") = TransactionJournalRcvrId
TransactionJournalTbl("TransactionAssignment") = Trim(TransactionJournalAssign)
TransactionJournalTbl.Update
If TransactionJournalStatus = "Z" _
And Not mTransactionJournalStatus = "Z" Then
'autoclose
    Call CloseTransportAction(TransactionJournalId, mTransactionJournalStatus)
End If
mTransactionJournalStatus = TransactionJournalStatus
If (TransactionJournalNew) Then
    TransactionJournalNew = False
    TransactionJournalId = GetJustAddedRecord
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutTransactionJournal"
    PutTransactionJournal = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Sub CloseTransportAction(TransactionJournalId As Long, oldTransactionJournalStatus As String)
Dim RS(1) As Recordset
Dim oldTransportAction As String
Dim newTransportAction As String
Dim tCombo As String

Set RS(0) = Nothing
Set RS(0) = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandText = _
"select distinct transportaction from ServiceTransactions " & _
"where TransactionId = " & TransactionJournalId
Call RS(0).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)

Do Until RS(0).EOF
    oldTransportAction = RS(0)("TransportAction")
    
    tCombo = UCase(Trim(oldTransactionJournalStatus)) & UCase(Trim(oldTransportAction))
    Select Case tCombo
    Case "PP", "PB", "SV", "PV", "P0"
        newTransportAction = "0"
    Case "SP", "SB", "S0"
        newTransportAction = "1"
    Case "SX"
        newTransportAction = "2"
    Case Else
        newTransportAction = ""
        MsgBox "error 3100 " & tCombo
    End Select
    
    Set RS(1) = Nothing
    Set RS(1) = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandText = _
    "update ServiceTransactions set TransportAction = '" & newTransportAction & "' " & _
    "where TransactionId = " & TransactionJournalId & _
    " and TransportAction = '" & oldTransportAction & "'"
    Call RS(1).Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)

    RS(0).MoveNext
Loop
End Sub


Private Function KillJournals() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillJournals = True
OrderString = "SELECT * FROM PracticeTransactionJounral "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set TransactionJournalTbl = Nothing
Set TransactionJournalTbl = CreateAdoRecordset
Call TransactionJournalTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (TransactionJournalTbl.EOF) Then
    TransactionJournalTbl.MoveFirst
    While Not (TransactionJournalTbl.EOF)
        TransactionJournalTbl.Delete
        TransactionJournalTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillJournals"
    KillJournals = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetTransactionJournalList(-1) > 0) Then
    If (SelectTransactionJournal(1)) Then
        GetJustAddedRecord = TransactionJournalTbl("TransactionId")
    End If
End If
End Function

Private Function PurgeTransactionJournal() As Boolean
On Error GoTo DbErrorHandler
PurgeTransactionJournal = True
TransactionJournalTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeTransactionJournal"
    PurgeTransactionJournal = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function KillJournal() As Boolean
KillJournal = KillJournals
End Function

Public Function ApplyTransactionJournal() As Boolean
ApplyTransactionJournal = PutTransactionJournal
End Function

Public Function DeleteTransactionJournal() As Boolean
DeleteTransactionJournal = PurgeTransactionJournal
End Function

Public Function RetrieveTransactionJournal() As Boolean
RetrieveTransactionJournal = GetTransactionJournal
End Function

Public Function SelectTransactionJournal(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectTransactionJournal = False
If (TransactionJournalTbl.EOF) Or (ListItem < 1) Or (ListItem > TransactionJournalTotal) Then
    Exit Function
End If
TransactionJournalTbl.MoveFirst
TransactionJournalTbl.Move ListItem - 1
SelectTransactionJournal = True
Call LoadTransactionJournal
Exit Function
DbErrorHandler:
    ModuleName = "SelectTransactionJournal"
    SelectTransactionJournal = False
    TransactionJournalId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindTransactionJournal() As Long
FindTransactionJournal = GetTransactionJournalList(0)
End Function

Public Function FindTransactionJournal2() As Long
FindTransactionJournal2 = GetTransactionJournalList(-2)
End Function

Public Function FindTransactionJournalbyExactDate() As Long
FindTransactionJournalbyExactDate = GetTransactionJournalList(-1)
End Function


Public Function FindTransactionJournalbyDate() As Long
FindTransactionJournalbyDate = GetTransactionJournalList(0)
End Function

Public Function FindTransactionJournalbyMonthly() As Long
FindTransactionJournalbyMonthly = GetTransactionJournalListMonthly
End Function

