VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeName"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeName Object Module
Option Explicit
Public PracticeId As Long
Public PracticeType As String
Public PracticeLocationReference As String
Public PracticeName As String
Public PracticeAddress As String
Public PracticeSuite As String
Public PracticeCity As String
Public PracticeState As String
Public PracticeZip As String
Public PracticePhone As String
Public PracticeOtherPhone As String
Public PracticeFax As String
Public PracticeEmail As String
Public PracticeTaxId As String
Public PracticeOtherId As String
Public PracticeVacation As String
Public PracticeBilling As Boolean
Public PracticeStartTime1 As String
Public PracticeStartTime2 As String
Public PracticeStartTime3 As String
Public PracticeStartTime4 As String
Public PracticeStartTime5 As String
Public PracticeStartTime6 As String
Public PracticeStartTime7 As String
Public PracticeEndTime1 As String
Public PracticeEndTime2 As String
Public PracticeEndTime3 As String
Public PracticeEndTime4 As String
Public PracticeEndTime5 As String
Public PracticeEndTime6 As String
Public PracticeEndTime7 As String
Public PracticeCat1 As String
Public PracticeCat2 As String
Public PracticeCat3 As String
Public PracticeCat4 As String
Public PracticeCat5 As String
Public PracticeCat6 As String
Public PracticeCat7 As String
Public PracticeCat8 As String
Public PracticeLoginType As String
Public PracticeNPI As String
Public PracticeTaxonomy As String

Private ModuleName As String
Private PracticeNameTotal As Integer
Private PracticeNameTbl As ADODB.Recordset
Private PracticeNameNew As Boolean

Private Sub Class_Initialize()
Set PracticeNameTbl = CreateAdoRecordset
Call InitPractice
End Sub

Private Sub Class_Terminate()
Call InitPractice
Set PracticeNameTbl = Nothing
End Sub

Private Sub InitPractice()
PracticeNameNew = True
PracticeType = "0"
PracticeLocationReference = ""
PracticeBilling = False
PracticeName = ""
PracticeAddress = ""
PracticeSuite = ""
PracticeCity = ""
PracticeState = ""
PracticeZip = ""
PracticePhone = ""
PracticeOtherPhone = ""
PracticeFax = ""
PracticeEmail = ""
PracticeTaxId = ""
PracticeOtherId = ""
PracticeVacation = ""
PracticeStartTime1 = ""
PracticeStartTime2 = ""
PracticeStartTime3 = ""
PracticeStartTime4 = ""
PracticeStartTime5 = ""
PracticeStartTime6 = ""
PracticeStartTime7 = ""
PracticeEndTime1 = ""
PracticeEndTime2 = ""
PracticeEndTime3 = ""
PracticeEndTime4 = ""
PracticeEndTime5 = ""
PracticeEndTime6 = ""
PracticeEndTime7 = ""
PracticeCat1 = ""
PracticeCat2 = ""
PracticeCat3 = ""
PracticeCat4 = ""
PracticeCat5 = ""
PracticeCat6 = ""
PracticeCat7 = ""
PracticeCat8 = ""
PracticeLoginType = ""
PracticeNPI = ""
PracticeTaxonomy = ""
End Sub

Private Sub LoadPractice()
PracticeNameNew = False
If (PracticeNameTbl("PracticeId") <> "") Then
    PracticeId = PracticeNameTbl("PracticeId")
Else
    PracticeId = 0
End If
If (PracticeNameTbl("PracticeType") <> "") Then
    PracticeType = PracticeNameTbl("PracticeType")
Else
    PracticeType = ""
End If
If (PracticeNameTbl("LocationReference") <> "") Then
    PracticeLocationReference = PracticeNameTbl("LocationReference")
Else
    PracticeLocationReference = ""
End If
If (PracticeNameTbl("PracticeName") <> "") Then
    PracticeName = PracticeNameTbl("PracticeName")
Else
    PracticeName = ""
End If
If (PracticeNameTbl("PracticeAddress") <> "") Then
    PracticeAddress = PracticeNameTbl("PracticeAddress")
Else
    PracticeAddress = ""
End If
If (PracticeNameTbl("PracticeSuite") <> "") Then
    PracticeSuite = PracticeNameTbl("PracticeSuite")
Else
    PracticeSuite = ""
End If
If (PracticeNameTbl("PracticeCity") <> "") Then
    PracticeCity = PracticeNameTbl("PracticeCity")
Else
    PracticeCity = ""
End If
If (PracticeNameTbl("PracticeState") <> "") Then
    PracticeState = PracticeNameTbl("PracticeState")
Else
    PracticeState = ""
End If
If (PracticeNameTbl("PracticeZip") <> "") Then
    PracticeZip = PracticeNameTbl("PracticeZip")
Else
    PracticeZip = ""
End If
If (PracticeNameTbl("PracticePhone") <> "") Then
    PracticePhone = PracticeNameTbl("PracticePhone")
Else
    PracticePhone = ""
End If
If (PracticeNameTbl("PracticeOtherPhone") <> "") Then
    PracticeOtherPhone = PracticeNameTbl("PracticeOtherPhone")
Else
    PracticeOtherPhone = ""
End If
If (PracticeNameTbl("PracticeFax") <> "") Then
    PracticeFax = PracticeNameTbl("PracticeFax")
Else
    PracticeFax = ""
End If
If (PracticeNameTbl("PracticeEmail") <> "") Then
    PracticeEmail = PracticeNameTbl("PracticeEmail")
Else
    PracticeEmail = ""
End If
If (PracticeNameTbl("PracticeEmail") <> "") Then
    PracticeEmail = PracticeNameTbl("PracticeEmail")
Else
    PracticeEmail = ""
End If
If (PracticeNameTbl("PracticeTaxId") <> "") Then
    PracticeTaxId = PracticeNameTbl("PracticeTaxId")
Else
    PracticeTaxId = ""
End If
If (PracticeNameTbl("PracticeOtherId") <> "") Then
    PracticeOtherId = PracticeNameTbl("PracticeOtherId")
Else
    PracticeOtherId = ""
End If
If (PracticeNameTbl("StartTime1") <> "") Then
    PracticeStartTime1 = PracticeNameTbl("StartTime1")
Else
    PracticeStartTime1 = ""
End If
If (PracticeNameTbl("StartTime2") <> "") Then
    PracticeStartTime2 = PracticeNameTbl("StartTime2")
Else
    PracticeStartTime2 = ""
End If
If (PracticeNameTbl("StartTime3") <> "") Then
    PracticeStartTime3 = PracticeNameTbl("StartTime3")
Else
    PracticeStartTime3 = ""
End If
If (PracticeNameTbl("StartTime4") <> "") Then
    PracticeStartTime4 = PracticeNameTbl("StartTime4")
Else
    PracticeStartTime4 = ""
End If
If (PracticeNameTbl("StartTime5") <> "") Then
    PracticeStartTime5 = PracticeNameTbl("StartTime5")
Else
    PracticeStartTime5 = ""
End If
If (PracticeNameTbl("StartTime6") <> "") Then
    PracticeStartTime6 = PracticeNameTbl("StartTime6")
Else
    PracticeStartTime6 = ""
End If
If (PracticeNameTbl("StartTime7") <> "") Then
    PracticeStartTime7 = PracticeNameTbl("StartTime7")
Else
    PracticeStartTime7 = ""
End If
If (PracticeNameTbl("EndTime1") <> "") Then
    PracticeEndTime1 = PracticeNameTbl("EndTime1")
Else
    PracticeEndTime1 = ""
End If
If (PracticeNameTbl("EndTime2") <> "") Then
    PracticeEndTime2 = PracticeNameTbl("EndTime2")
Else
    PracticeEndTime2 = ""
End If
If (PracticeNameTbl("EndTime3") <> "") Then
    PracticeEndTime3 = PracticeNameTbl("EndTime3")
Else
    PracticeEndTime3 = ""
End If
If (PracticeNameTbl("EndTime4") <> "") Then
    PracticeEndTime4 = PracticeNameTbl("EndTime4")
Else
    PracticeEndTime4 = ""
End If
If (PracticeNameTbl("EndTime5") <> "") Then
    PracticeEndTime5 = PracticeNameTbl("EndTime5")
Else
    PracticeEndTime5 = ""
End If
If (PracticeNameTbl("EndTime6") <> "") Then
    PracticeEndTime6 = PracticeNameTbl("EndTime6")
Else
    PracticeEndTime6 = ""
End If
If (PracticeNameTbl("EndTime7") <> "") Then
    PracticeEndTime7 = PracticeNameTbl("EndTime7")
Else
    PracticeEndTime7 = ""
End If
If (PracticeNameTbl("Vacation") <> "") Then
    PracticeVacation = PracticeNameTbl("Vacation")
Else
    PracticeVacation = ""
End If
If (PracticeNameTbl("LoginType") <> "") Then
    PracticeLoginType = PracticeNameTbl("LoginType")
Else
    PracticeLoginType = "1"
End If
If (PracticeNameTbl("CatArray1") <> "") Then
    PracticeCat1 = PracticeNameTbl("CatArray1")
Else
    PracticeCat1 = ""
End If
If (PracticeNameTbl("CatArray2") <> "") Then
    PracticeCat2 = PracticeNameTbl("CatArray2")
Else
    PracticeCat2 = ""
End If
If (PracticeNameTbl("CatArray3") <> "") Then
    PracticeCat3 = PracticeNameTbl("CatArray3")
Else
    PracticeCat3 = ""
End If
If (PracticeNameTbl("CatArray4") <> "") Then
    PracticeCat4 = PracticeNameTbl("CatArray4")
Else
    PracticeCat4 = ""
End If
If (PracticeNameTbl("CatArray5") <> "") Then
    PracticeCat5 = PracticeNameTbl("CatArray5")
Else
    PracticeCat5 = ""
End If
If (PracticeNameTbl("CatArray6") <> "") Then
    PracticeCat6 = PracticeNameTbl("CatArray6")
Else
    PracticeCat6 = ""
End If
If (PracticeNameTbl("CatArray7") <> "") Then
    PracticeCat7 = PracticeNameTbl("CatArray7")
Else
    PracticeCat7 = ""
End If
If (PracticeNameTbl("CatArray8") <> "") Then
    PracticeCat8 = PracticeNameTbl("CatArray8")
Else
    PracticeCat8 = ""
End If
If (PracticeNameTbl("PracticeNPI") <> "") Then
    PracticeNPI = PracticeNameTbl("PracticeNPI")
Else
    PracticeNPI = ""
End If
If (PracticeNameTbl("PracticeTaxonomy") <> "") Then
    PracticeTaxonomy = PracticeNameTbl("PracticeTaxonomy")
Else
    PracticeTaxonomy = ""
End If
If (PracticeNameTbl("BillingOffice") <> "") Then
    If (PracticeNameTbl("BillingOffice") = "T") Then
        PracticeBilling = True
    Else
        PracticeBilling = False
    End If
Else
    PracticeBilling = False
End If
End Sub
Public Function EmptyLocationReferenceexist() As Boolean
Dim OrderString As String
OrderString = "SELECT PracticeType from PracticeName WHERE PracticeType = 'P' AND LocationReference = '' GROUP BY PracticeTYPE HAVING COUNT(*) > 1"
Set PracticeNameTbl = Nothing
Set PracticeNameTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeNameTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeNameTbl.EOF) Then
    EmptyLocationReferenceexist = False
Else
   EmptyLocationReferenceexist = True
End If
Exit Function
End Function


Private Function GetPracticebyName() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
PracticeNameTotal = 0
GetPracticebyName = -1
OrderString = "Select * FROM PracticeName WHERE "
If (Len(Trim(PracticeName)) > 1) Then
    OrderString = OrderString + "PracticeName = '" + UCase(Trim(PracticeName)) + "' "
Else
    OrderString = OrderString + "PracticeName >= '" + UCase(Trim(PracticeName)) + "' "
End If
If (Trim(PracticeType) <> "") Then
    OrderString = OrderString + "And PracticeType = '" + Trim(PracticeType) + "' "
End If
If (Trim(PracticeLocationReference) <> "") Then
    OrderString = OrderString + "And LocationReference = '" + Trim(PracticeLocationReference) + "' "
End If
If (PracticeBilling) Then
    OrderString = OrderString + "And BillingOffice = 'T' "
End If
If (Trim(PracticeLocationReference) = "") Then
    OrderString = OrderString + " ORDER BY LocationReference ASC"
End If
Set PracticeNameTbl = Nothing
Set PracticeNameTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeNameTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeNameTbl.EOF) Then
    GetPracticebyName = -1
Else
    PracticeNameTbl.MoveLast
    PracticeNameTotal = PracticeNameTbl.RecordCount
    GetPracticebyName = PracticeNameTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPracticebyName"
    GetPracticebyName = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPracticeName() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPracticeName = True
OrderString = "SELECT * FROM PracticeName WHERE PracticeId =" + str(PracticeId) + " "
Set PracticeNameTbl = Nothing
Set PracticeNameTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeNameTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeNameTbl.EOF) Then
    Call InitPractice
Else
    Call LoadPractice
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPracticeName"
    GetPracticeName = False
    PracticeId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function getPracticeId() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
Dim Ref As String

OrderString = "SELECT * FROM PracticeName WHERE "

If PracticeId <> 0 Then
    OrderString = OrderString + Ref + "PracticeId = " & PracticeId & " "
    Ref = "And "
End If
If PracticeType <> "" Then
    OrderString = OrderString + Ref + "PracticeType = '" & PracticeType & "' "
    Ref = "And "
End If
If PracticeLocationReference <> "" Then
    If PracticeLocationReference = "BLANK" Then PracticeLocationReference = ""
    OrderString = OrderString + Ref + "LocationReference = '" & PracticeLocationReference & "' "
    Ref = "And "
End If

Set PracticeNameTbl = Nothing
Set PracticeNameTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeNameTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeNameTbl.EOF) Then
    Call InitPractice
Else
    getPracticeId = True
    Call LoadPractice
End If
Exit Function
DbErrorHandler:
    ModuleName = "getPracticeId"
    getPracticeId = False
    PracticeId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPracticeName() As Boolean
On Error GoTo DbErrorHandler
PutPracticeName = True
If (PracticeNameNew) Then
    PracticeNameTbl.AddNew
End If
PracticeNameTbl("PracticeType") = Trim(PracticeType)
PracticeNameTbl("LocationReference") = Trim(PracticeLocationReference)
PracticeNameTbl("PracticeName") = Trim(PracticeName)
PracticeNameTbl("PracticeAddress") = Trim(PracticeAddress)
PracticeNameTbl("PracticeSuite") = Trim(PracticeSuite)
PracticeNameTbl("PracticeCity") = Trim(PracticeCity)
PracticeNameTbl("PracticeState") = UCase(Trim(PracticeState))
PracticeNameTbl("PracticeZip") = Trim(PracticeZip)
PracticeNameTbl("PracticePhone") = Trim(PracticePhone)
PracticeNameTbl("PracticeOtherPhone") = Trim(PracticeOtherPhone)
PracticeNameTbl("PracticeFax") = Trim(PracticeFax)
PracticeNameTbl("PracticeEmail") = Trim(PracticeEmail)
PracticeNameTbl("PracticeTaxId") = Trim(PracticeTaxId)
PracticeNameTbl("PracticeOtherId") = Trim(PracticeOtherId)
PracticeNameTbl("StartTime1") = UCase(Trim(PracticeStartTime1))
PracticeNameTbl("StartTime2") = UCase(Trim(PracticeStartTime2))
PracticeNameTbl("StartTime3") = UCase(Trim(PracticeStartTime3))
PracticeNameTbl("StartTime4") = UCase(Trim(PracticeStartTime4))
PracticeNameTbl("StartTime5") = UCase(Trim(PracticeStartTime5))
PracticeNameTbl("StartTime6") = UCase(Trim(PracticeStartTime6))
PracticeNameTbl("StartTime7") = UCase(Trim(PracticeStartTime7))
PracticeNameTbl("EndTime1") = UCase(Trim(PracticeEndTime1))
PracticeNameTbl("EndTime2") = UCase(Trim(PracticeEndTime2))
PracticeNameTbl("EndTime3") = UCase(Trim(PracticeEndTime3))
PracticeNameTbl("EndTime4") = UCase(Trim(PracticeEndTime4))
PracticeNameTbl("EndTime5") = UCase(Trim(PracticeEndTime5))
PracticeNameTbl("EndTime6") = UCase(Trim(PracticeEndTime6))
PracticeNameTbl("EndTime7") = UCase(Trim(PracticeEndTime7))
PracticeNameTbl("Vacation") = Trim(PracticeVacation)
PracticeNameTbl("LoginType") = Trim(PracticeLoginType)
PracticeNameTbl("CatArray1") = PracticeCat1
PracticeNameTbl("CatArray2") = PracticeCat2
PracticeNameTbl("CatArray3") = PracticeCat3
PracticeNameTbl("CatArray4") = PracticeCat4
PracticeNameTbl("CatArray5") = PracticeCat5
PracticeNameTbl("CatArray6") = PracticeCat6
PracticeNameTbl("CatArray7") = PracticeCat7
PracticeNameTbl("CatArray8") = PracticeCat8
PracticeNameTbl("PracticeNPI") = PracticeNPI
PracticeNameTbl("PracticeTaxonomy") = PracticeTaxonomy
PracticeNameTbl("BillingOffice") = "F"
If (PracticeBilling) Then
    PracticeNameTbl("BillingOffice") = "T"
End If
PracticeNameTbl.Update
If (PracticeNameNew) Then
    PracticeId = GetJustAddedRecord
    PracticeNameNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPracticeName"
    PutPracticeName = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeTemplate() As Boolean
On Error GoTo DbErrorHandler
PurgeTemplate = True
PracticeNameTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeTemplate"
    PurgeTemplate = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPracticebyName > 0) Then
    If (SelectPractice(1)) Then
        GetJustAddedRecord = PracticeNameTbl("PracticeId")
    End If
End If
End Function

Public Function SelectPractice(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPractice = False
If (PracticeNameTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeNameTotal) Then
    Exit Function
End If
PracticeNameTbl.MoveFirst
PracticeNameTbl.Move ListItem - 1
SelectPractice = True
Call LoadPractice
Exit Function
DbErrorHandler:
    ModuleName = "SelectPractice"
    SelectPractice = False
    PracticeId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
Public Function BlankLocationRefernceExist() As Boolean
BlankLocationRefernceExist = EmptyLocationReferenceexist
End Function
Public Function FindPractice() As Long
PracticeBilling = False
FindPractice = GetPracticebyName
End Function

Public Function FindPracticeNameBilling() As Long
PracticeBilling = True
FindPracticeNameBilling = GetPracticebyName
End Function

Public Function ApplyPracticeName() As Boolean
ApplyPracticeName = PutPracticeName
End Function

Public Function DeleteTemplate() As Boolean
DeleteTemplate = PurgeTemplate
End Function

Public Function DeletePracticeName() As Boolean
DeletePracticeName = PutPracticeName
End Function

Public Function RetrievePracticeName() As Boolean
RetrievePracticeName = GetPracticeName
End Function
