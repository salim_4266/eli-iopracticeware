VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientClinicalSurgeryPlan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Surgery Plan Object Module
Option Explicit
Public SurgeryId As Long
Public SurgeryClnId As Long
Public SurgeryRefType As String
Public SurgeryValue As String
Public SurgeryStatus As String

Private ModuleName As String
Private PatientClinicalPlanTotal As Long
Private PatientClinicalPlanTbl As ADODB.Recordset
Private PatientClinicalPlanNew As Boolean

Private Sub Class_Initialize()
Set PatientClinicalPlanTbl = CreateAdoRecordset
Call InitPatientClinicalPlan
End Sub

Private Sub Class_Terminate()
Call InitPatientClinicalPlan
Set PatientClinicalPlanTbl = Nothing
End Sub

Private Sub InitPatientClinicalPlan()
PatientClinicalPlanNew = True
SurgeryId = 0
SurgeryClnId = 0
SurgeryRefType = ""
SurgeryValue = ""
SurgeryStatus = "A"
End Sub

Private Sub LoadPatientClinicalPlan()
PatientClinicalPlanNew = False
If (PatientClinicalPlanTbl("SurgeryId") <> "") Then
    SurgeryId = PatientClinicalPlanTbl("SurgeryId")
Else
    SurgeryId = 0
End If
If (PatientClinicalPlanTbl("SurgeryClnId") <> "") Then
    SurgeryClnId = PatientClinicalPlanTbl("SurgeryClnId")
Else
    SurgeryClnId = 0
End If
If (PatientClinicalPlanTbl("SurgeryRefType") <> "") Then
    SurgeryRefType = PatientClinicalPlanTbl("SurgeryRefType")
Else
    SurgeryRefType = ""
End If
If (PatientClinicalPlanTbl("SurgeryValue") <> "") Then
    SurgeryValue = PatientClinicalPlanTbl("SurgeryValue")
Else
    SurgeryValue = ""
End If
If (PatientClinicalPlanTbl("SurgeryStatus") <> "") Then
    SurgeryStatus = PatientClinicalPlanTbl("SurgeryStatus")
Else
    SurgeryStatus = ""
End If
End Sub

Private Function GetListPatientClinicalPlan() As Long
On Error GoTo DbErrorHandler
Dim w As Integer
Dim Temp As String
Dim Ref As String
Dim OrderString As String
Ref = "And "
PatientClinicalPlanTotal = 0
GetListPatientClinicalPlan = -1
If (SurgeryClnId < 1) Then
    Exit Function
End If
OrderString = "Select * FROM PatientClinicalSurgeryPlan WHERE SurgeryClnId =" + Str(SurgeryClnId) + " "
If (Trim(SurgeryRefType) <> "") Then
    If (Len(SurgeryRefType) = 1) Then
        OrderString = OrderString + Ref + "SurgeryRefType = '" + UCase(Trim(SurgeryRefType)) + "' "
        Ref = "And "
    Else
        Temp = ""
        For w = 1 To Len(SurgeryRefType)
            If (w = Len(SurgeryRefType)) Then
                Temp = Temp + "SurgeryRefType = '" + UCase(Mid(SurgeryRefType, w, 1)) + "') "
            ElseIf (w = 1) Then
                Temp = Temp + "(SurgeryRefType = '" + UCase(Mid(SurgeryRefType, w, 1)) + "' Or "
            Else
                Temp = Temp + "SurgeryRefType = '" + UCase(Mid(SurgeryRefType, w, 1)) + "' Or "
            End If
        Next w
        OrderString = OrderString + Ref + Temp
        Ref = "And "
    End If
End If
If (Trim(SurgeryStatus) <> "") Then
    If (Left(SurgeryStatus, 1) = "-") Then
        OrderString = OrderString + Ref + "SurgeryStatus <> '" + Mid(SurgeryStatus, 2, 1) + "' "
    ElseIf (SurgeryStatus = "]") Then
        OrderString = OrderString + Ref + "SurgeryStatus = ']' "
    Else
        OrderString = OrderString + Ref + "SurgeryStatus <> 'D' "
    End If
    Ref = "And "
End If
If (Trim(SurgeryValue) <> "") Then
    OrderString = OrderString + Ref + "SurgeryValue = '" + Trim(SurgeryValue) + "' "
    Ref = "And "
End If
Set PatientClinicalPlanTbl = Nothing
Set PatientClinicalPlanTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientClinicalPlanTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientClinicalPlanTbl.EOF) Then
    GetListPatientClinicalPlan = -1
Else
    PatientClinicalPlanTbl.MoveLast
    PatientClinicalPlanTotal = PatientClinicalPlanTbl.RecordCount
    GetListPatientClinicalPlan = PatientClinicalPlanTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetListPatientClinicalPlan"
    GetListPatientClinicalPlan = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetListPatientClinicalPlanbyValue() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
PatientClinicalPlanTotal = 0
GetListPatientClinicalPlanbyValue = -1
If (Trim(SurgeryValue) <> "") Then
    OrderString = "Select * FROM PatientClinicalSurgeryPlan WHERE SurgeryValue = '" + Trim(SurgeryValue) + "' And SurgeryStatus <> 'D' "
    If (Trim(SurgeryRefType) <> "") Then
        OrderString = OrderString + "And SurgeryRefType = '" + UCase(Trim(SurgeryRefType)) + "' "
    End If
    Set PatientClinicalPlanTbl = Nothing
    Set PatientClinicalPlanTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientClinicalPlanTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (PatientClinicalPlanTbl.EOF) Then
        PatientClinicalPlanTbl.MoveLast
        PatientClinicalPlanTotal = PatientClinicalPlanTbl.RecordCount
        GetListPatientClinicalPlanbyValue = PatientClinicalPlanTotal
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetListPatientClinicalPlanbyValue"
    GetListPatientClinicalPlanbyValue = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientClinicalPlan() As Boolean
On Error GoTo DbErrorHandler
GetPatientClinicalPlan = True
Dim OrderString As String
OrderString = "SELECT * FROM PatientClinicalSurgeryPlan WHERE SurgeryId =" + Str(SurgeryId)
Set PatientClinicalPlanTbl = Nothing
Set PatientClinicalPlanTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientClinicalPlanTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientClinicalPlanTbl.EOF) Then
    Call InitPatientClinicalPlan
Else
    Call LoadPatientClinicalPlan
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientClinicalPlan"
    GetPatientClinicalPlan = False
    SurgeryId = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function AxePatientClinicalPlan() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
AxePatientClinicalPlan = True
PatientClinicalPlanTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "AxePatientClinicalPlan"
    AxePatientClinicalPlan = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientClinicalPlan() As Boolean
On Error GoTo DbErrorHandler
PutPatientClinicalPlan = True
If (PatientClinicalPlanNew) Or (SurgeryId < 1) Then
    PatientClinicalPlanTbl.AddNew
End If
PatientClinicalPlanTbl("SurgeryClnId") = SurgeryClnId
PatientClinicalPlanTbl("SurgeryRefType") = UCase(Trim(SurgeryRefType))
PatientClinicalPlanTbl("SurgeryValue") = UCase(Trim(SurgeryValue))
PatientClinicalPlanTbl("SurgeryStatus") = SurgeryStatus
PatientClinicalPlanTbl.Update
If (PatientClinicalPlanNew) Then
    PatientClinicalPlanNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientClinicalPlan"
    PutPatientClinicalPlan = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyPatientClinicalPlan() As Boolean
ApplyPatientClinicalPlan = PutPatientClinicalPlan
End Function

Public Function DeletePatientClinicalPlan() As Boolean
SurgeryStatus = "D"
DeletePatientClinicalPlan = PutPatientClinicalPlan
End Function

Public Function KillPatientClinicalPlan() As Boolean
KillPatientClinicalPlan = AxePatientClinicalPlan
End Function

Public Function RetrievePatientClinicalPlan() As Boolean
RetrievePatientClinicalPlan = GetPatientClinicalPlan
End Function

Public Function SelectPatientClinicalPlan(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPatientClinicalPlan = False
If (PatientClinicalPlanTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientClinicalPlanTotal) Then
    Exit Function
End If
PatientClinicalPlanTbl.MoveFirst
PatientClinicalPlanTbl.Move ListItem - 1
SelectPatientClinicalPlan = True
Call LoadPatientClinicalPlan
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientClinicalPlan"
    SelectPatientClinicalPlan = False
    SurgeryId = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientClinicalPlan() As Long
FindPatientClinicalPlan = GetListPatientClinicalPlan()
End Function

Public Function FindPatientClinicalPlanbyValue() As Long
FindPatientClinicalPlanbyValue = GetListPatientClinicalPlanbyValue()
End Function

Public Function DeleteClinicalByType() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
DeleteClinicalByType = True
OrderString = "DELETE * From PatientClinicalSurgeryPlan WHERE PatientClinicalPlan.Status=']' "
Set PatientClinicalPlanTbl = Nothing
Set PatientClinicalPlanTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientClinicalPlanTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
Exit Function
DbErrorHandler:
    DeleteClinicalByType = False
    Resume LeaveFast
LeaveFast:
End Function

