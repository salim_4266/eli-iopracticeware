VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLOrders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Inventory Object Module
Option Explicit
Public OrderId As Long
Public OrderType As String
Public InventoryId As Long
Public ReceivableId As Long
Public PatientId As Long
Public AppointmentId As Long
Public ShipTo As String
Public ShipAddress1 As String
Public ShipAddress2 As String
Public ShipCity As String
Public ShipState As String
Public ShipZip As String
Public Eye As String
Public Qty As Long
Public Cost As Single
Public OrderDate As String
Public OrderComplete As String
Public OrderReference As String
Public OrderPONumber As String
Public OrderAlternateReference As String

Public Status As Long
Public SingleFieldValue As String
Public CLOrdersTotal As Long
Public FieldSelect As String
Public Criteria As String
Public OrderBy As String
Private SingleField As Boolean
Private OrderQtys As Boolean

Private ModuleName As String
Private CLOrdersTbl As ADODB.Recordset
Private CLOrdersNew As Boolean

Private Sub Class_Initialize()
Set CLOrdersTbl = CreateAdoRecordset
Call InitCLOrders
End Sub

Private Sub Class_Terminate()
Call InitCLOrders
Set CLOrdersTbl = Nothing
End Sub

Private Sub InitCLOrders()
CLOrdersNew = True
OrderId = 0
OrderType = ""
InventoryId = 0
ReceivableId = 0
PatientId = 0
AppointmentId = 0
ShipTo = ""
ShipAddress1 = ""
ShipAddress2 = ""
ShipCity = ""
ShipState = ""
ShipZip = ""
Eye = ""
Qty = 0
Cost = 0
OrderDate = ""
OrderComplete = ""
OrderReference = ""
OrderPONumber = ""
OrderAlternateReference = ""
Status = 0
End Sub

Private Sub LoadCLOrders()
CLOrdersNew = False
If (SingleField) Then
    SingleFieldValue = CLOrdersTbl(FieldSelect)
ElseIf (OrderQtys) Then
    InventoryId = CLOrdersTbl("InventoryId")
    Qty = CLOrdersTbl(1)
Else
    OrderId = CLOrdersTbl("OrderId")
    If (CLOrdersTbl("OrderType") <> "") Then
        OrderType = CLOrdersTbl("OrderType")
    Else
        OrderType = ""
    End If
    If (CLOrdersTbl("InventoryId") <> "") Then
        InventoryId = CLOrdersTbl("InventoryId")
    Else
        InventoryId = 0
    End If
    If (CLOrdersTbl("ReceivableId") <> "") Then
        ReceivableId = CLOrdersTbl("ReceivableId")
    Else
        ReceivableId = 0
    End If
    If (CLOrdersTbl("PatientId") <> "") Then
        PatientId = CLOrdersTbl("PatientId")
    Else
        PatientId = 0
    End If
    If (CLOrdersTbl("AppointmentId") <> "") Then
        AppointmentId = CLOrdersTbl("AppointmentId")
    Else
        AppointmentId = 0
    End If
    If (CLOrdersTbl("ShipTo") <> "") Then
        ShipTo = CLOrdersTbl("ShipTo")
    Else
        ShipTo = ""
    End If
    If (CLOrdersTbl("ShipAddress1") <> "") Then
        ShipAddress1 = CLOrdersTbl("ShipAddress1")
    Else
        ShipAddress1 = ""
    End If
    If (CLOrdersTbl("ShipAddress2") <> "") Then
        ShipAddress2 = CLOrdersTbl("ShipAddress2")
    Else
        ShipAddress2 = ""
    End If
    If (CLOrdersTbl("ShipCity") <> "") Then
        ShipCity = CLOrdersTbl("ShipCity")
    Else
        ShipCity = ""
    End If
    If (CLOrdersTbl("ShipState") <> "") Then
        ShipState = CLOrdersTbl("ShipState")
    Else
        ShipState = ""
    End If
    If (CLOrdersTbl("ShipZip") <> "") Then
        ShipZip = CLOrdersTbl("ShipZip")
    Else
        ShipZip = ""
    End If
    If (CLOrdersTbl("Qty") <> "") Then
        Qty = CLOrdersTbl("Qty")
    Else
        Qty = 0
    End If
    If (CLOrdersTbl("Cost") <> "") Then
        Cost = CLOrdersTbl("Cost")
    Else
        Cost = 0
    End If
    If (CLOrdersTbl("OrderDate") <> "") Then
        OrderDate = CLOrdersTbl("OrderDate")
    Else
        OrderDate = ""
    End If
    If (CLOrdersTbl("OrderComplete") <> "") Then
        OrderComplete = CLOrdersTbl("OrderComplete")
    Else
        OrderComplete = ""
    End If
    If (CLOrdersTbl("Status") <> "") Then
        Status = CLOrdersTbl("Status")
    Else
        Status = 0
    End If
    If (CLOrdersTbl("Eye") <> "") Then
        Eye = CLOrdersTbl("Eye")
    Else
        Eye = "OU"
    End If
    If (CLOrdersTbl("Reference") <> "") Then
        OrderReference = CLOrdersTbl("Reference")
    Else
        OrderReference = ""
    End If
    If (CLOrdersTbl("PONumber") <> "") Then
        OrderPONumber = CLOrdersTbl("PONumber")
    Else
        OrderPONumber = ""
    End If
    If (CLOrdersTbl("AlternateReference") <> "") Then
        OrderAlternateReference = CLOrdersTbl("AlternateReference")
    Else
        OrderAlternateReference = ""
    End If
End If
End Sub

Public Function PutCLOrder() As Boolean
On Error GoTo DbErrorHandler
PutCLOrder = True
If (CLOrdersNew) Then
    CLOrdersTbl.AddNew
End If
CLOrdersTbl("OrderType") = OrderType
CLOrdersTbl("InventoryId") = InventoryId
CLOrdersTbl("ReceivableId") = ReceivableId
CLOrdersTbl("PatientId") = PatientId
CLOrdersTbl("AppointmentId") = AppointmentId
CLOrdersTbl("ShipTo") = ShipTo
CLOrdersTbl("ShipAddress1") = ShipAddress1
CLOrdersTbl("ShipAddress2") = ShipAddress2
CLOrdersTbl("ShipCity") = ShipCity
CLOrdersTbl("ShipState") = ShipState
CLOrdersTbl("ShipZip") = ShipZip
CLOrdersTbl("Eye") = Eye
CLOrdersTbl("Qty") = Qty
CLOrdersTbl("Cost") = Cost
If (OrderDate <> "") Then
    CLOrdersTbl("OrderDate") = OrderDate
End If
If (OrderComplete <> "") Then
    CLOrdersTbl("OrderComplete") = OrderComplete
End If
CLOrdersTbl("Reference") = OrderReference
CLOrdersTbl("AlternateReference") = OrderAlternateReference
CLOrdersTbl("PONumber") = OrderPONumber
CLOrdersTbl("Status") = Status
CLOrdersTbl.Update
If (CLOrdersNew) Then
    CLOrdersTbl.MoveLast
    Call LoadCLOrders
    CLOrdersNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutCLOrder"
    Call PinpointPRError(ModuleName, Err)
    PutCLOrder = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveCLOrders() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
RetrieveCLOrders = False
If (OrderId > 0) Then
    OrderString = "SELECT * FROM CLOrders WHERE OrderID = " + Trim(Str(OrderId)) + " "
    Set CLOrdersTbl = Nothing
    Set CLOrdersTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call CLOrdersTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (CLOrdersTbl.EOF) Then
        RetrieveCLOrders = True
        Call LoadCLOrders
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveCLOrders"
    RetrieveCLOrders = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLOrders() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCLOrders = True
OrderString = "SELECT * FROM CLOrders"
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE OrderType = '" + OrderType + "' And " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy
End If
Set CLOrdersTbl = Nothing
Set CLOrdersTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLOrdersTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CLOrdersTbl.EOF) Then
    Call InitCLOrders
Else
    CLOrdersTbl.MoveLast
    CLOrdersTotal = CLOrdersTbl.RecordCount
    Call LoadCLOrders
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCLOrders"
    GetCLOrders = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPCOrders() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPCOrders = True
OrderString = "SELECT * FROM CLOrders "
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE OrderType = '" + OrderType + "' And " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy
End If
Set CLOrdersTbl = Nothing
Set CLOrdersTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLOrdersTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CLOrdersTbl.EOF) Then
    Call InitCLOrders
Else
    CLOrdersTbl.MoveLast
    CLOrdersTotal = CLOrdersTbl.RecordCount
    Call LoadCLOrders
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPCOrders"
    GetPCOrders = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLOrdersSingleField() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCLOrdersSingleField = True
OrderString = "SELECT DISTINCT " + FieldSelect + " FROM CLOrders "
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE OrderType = '" + OrderType + "' And " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy + " ASC"
End If
Set CLOrdersTbl = Nothing
Set CLOrdersTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLOrdersTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CLOrdersTbl.EOF) Then
    Call InitCLOrders
Else
    CLOrdersTbl.MoveLast
    CLOrdersTotal = CLOrdersTbl.RecordCount
    Call LoadCLOrders
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCLOrdersSingleField"
    GetCLOrdersSingleField = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectCLOrders(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectCLOrders = True
If (CLOrdersTbl.EOF) Or (ListItem < 1) Or (ListItem > CLOrdersTotal) Then
    SelectCLOrders = False
    Exit Function
End If
CLOrdersTbl.MoveFirst
CLOrdersTbl.Move ListItem - 1
Call LoadCLOrders
Exit Function
DbErrorHandler:
    ModuleName = "SelectCLOrders"
    Call PinpointPRError(ModuleName, Err)
    SelectCLOrders = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLOrders() As Boolean
On Error GoTo DbErrorHandler
SingleField = False
OrderQtys = False
OrderType = "C"
FindCLOrders = GetCLOrders
Exit Function
DbErrorHandler:
    ModuleName = "FindCLOrders"
    Call PinpointPRError(ModuleName, Err)
    FindCLOrders = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPCOrders() As Boolean
On Error GoTo DbErrorHandler
SingleField = False
OrderQtys = False
OrderType = "G"
FindPCOrders = GetPCOrders
Exit Function
DbErrorHandler:
    ModuleName = "FindPCOrders"
    Call PinpointPRError(ModuleName, Err)
    FindPCOrders = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLOrdersSingleField() As Boolean
On Error GoTo DbErrorHandler
SingleField = True
OrderQtys = False
OrderType = "C"
FindCLOrdersSingleField = GetCLOrdersSingleField
Exit Function
DbErrorHandler:
    ModuleName = "FindCLOrdersSingleField"
    Call PinpointPRError(ModuleName, Err)
    FindCLOrdersSingleField = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPCOrdersSingleField() As Boolean
On Error GoTo DbErrorHandler
SingleField = True
OrderQtys = False
OrderType = "G"
FindPCOrdersSingleField = GetCLOrdersSingleField
Exit Function
DbErrorHandler:
    ModuleName = "FindPCOrdersSingleField"
    Call PinpointPRError(ModuleName, Err)
    FindPCOrdersSingleField = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function AxeCLOrders() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
AxeCLOrders = True
CLOrdersTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "AxeCLOrders"
    Call PinpointPRError(ModuleName, Err)
    AxeCLOrders = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function DeleteCLOrders() As Boolean
DeleteCLOrders = AxeCLOrders
End Function
