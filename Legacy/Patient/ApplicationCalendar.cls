VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ApplicationCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public lstStartTime As ListBox
Public CalendarId As Long
Public ResourceId As Long
Public DisplayColor As String
Public TemplateLocation As Long
Public Purpose1 As String
Public Purpose2 As String
Public Purpose3 As String
Public Purpose4 As String
Public Purpose5 As String
Public Purpose6 As String
Public Purpose7 As String
Public Location1 As Long
Public Location2 As Long
Public Location3 As Long
Public Location4 As Long
Public Location5 As Long
Public Location6 As Long
Public Location7 As Long
Public CalendarCategory1 As String
Public CalendarCategory2 As String
Public CalendarCategory3 As String
Public CalendarCategory4 As String
Public CalendarCategory5 As String
Public CalendarCategory6 As String
Public CalendarCategory7 As String
Public CalendarCategory8 As String
Public CalendarComment As String
Public CalendarDate As String
Public Vacation As Boolean
Public Holiday As Boolean
Public BreakDown As Integer

Private Type SlotDayArray
    TheDays(7) As String * 288
End Type

Public Function ApplPostCalendar() As Boolean
Dim k As Integer
Dim i As Integer
Dim CalId As Long
Dim VerifyCalendar As SchedulerCalendar
Dim CalPost As SchedulerCalendar
ApplPostCalendar = False
CalId = 0
Set VerifyCalendar = New SchedulerCalendar
VerifyCalendar.CalendarLoc1 = -1
VerifyCalendar.CalendarResourceId = ResourceId
VerifyCalendar.CalendarDate = CalendarDate
If (VerifyCalendar.FindCalendar > 0) Then
    If (VerifyCalendar.SelectCalendar(1)) Then
        CalId = VerifyCalendar.CalendarId
    End If
End If
Set CalPost = New SchedulerCalendar
CalPost.CalendarId = CalId
If (CalPost.RetrieveSchedulerCalendar) Then
    CalPost.CalendarResourceId = ResourceId
    CalPost.CalendarDate = CalendarDate
    CalPost.CalendarLoc1 = Location1
    CalPost.CalendarLoc2 = Location2
    CalPost.CalendarLoc3 = Location3
    CalPost.CalendarLoc4 = Location4
    CalPost.CalendarLoc5 = Location5
    CalPost.CalendarLoc6 = Location6
    CalPost.CalendarLoc7 = Location7
    CalPost.CalendarColor = Val(Trim(DisplayColor))
    CalPost.CalendarComment = CalendarComment
    CalPost.CalendarVacation = Vacation
    CalPost.CalendarHoliday = Holiday
    k = 0
    CalPost.CalendarPurpose1 = ""
    CalPost.CalendarPurpose2 = ""
    CalPost.CalendarPurpose3 = ""
    CalPost.CalendarPurpose4 = ""
    CalPost.CalendarPurpose5 = ""
    CalPost.CalendarPurpose6 = ""
    CalPost.CalendarPurpose7 = ""
    CalPost.CalendarStartTime1 = ""
    CalPost.CalendarEndTime1 = ""
    CalPost.CalendarStartTime2 = ""
    CalPost.CalendarEndTime2 = ""
    CalPost.CalendarStartTime3 = ""
    CalPost.CalendarEndTime3 = ""
    CalPost.CalendarStartTime4 = ""
    CalPost.CalendarEndTime4 = ""
    CalPost.CalendarStartTime5 = ""
    CalPost.CalendarEndTime5 = ""
    CalPost.CalendarStartTime6 = ""
    CalPost.CalendarEndTime6 = ""
    CalPost.CalendarStartTime7 = ""
    CalPost.CalendarEndTime7 = ""
    For i = 0 To lstStartTime.ListCount - 1
        If (lstStartTime.Selected(i)) Then
            k = k + 1
            If (k = 1) Then
                CalPost.CalendarStartTime1 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose1 = Trim(Purpose1)
            ElseIf (k = 2) Then
                CalPost.CalendarEndTime1 = Trim(lstStartTime.List(i))
            ElseIf (k = 3) Then
                CalPost.CalendarStartTime2 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose2 = Trim(Purpose2)
            ElseIf (k = 4) Then
                CalPost.CalendarEndTime2 = Trim(lstStartTime.List(i))
            ElseIf (k = 5) Then
                CalPost.CalendarStartTime3 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose3 = Trim(Purpose3)
            ElseIf (k = 6) Then
                CalPost.CalendarEndTime3 = Trim(lstStartTime.List(i))
            ElseIf (k = 7) Then
                CalPost.CalendarStartTime4 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose4 = Trim(Purpose4)
            ElseIf (k = 8) Then
                CalPost.CalendarEndTime4 = Trim(lstStartTime.List(i))
            ElseIf (k = 9) Then
                CalPost.CalendarStartTime5 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose5 = Trim(Purpose5)
            ElseIf (k = 10) Then
                CalPost.CalendarEndTime5 = Trim(lstStartTime.List(i))
            ElseIf (k = 11) Then
                CalPost.CalendarStartTime6 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose6 = Trim(Purpose6)
            ElseIf (k = 12) Then
                CalPost.CalendarEndTime6 = Trim(lstStartTime.List(i))
            ElseIf (k = 13) Then
                CalPost.CalendarStartTime7 = Trim(lstStartTime.List(i))
                CalPost.CalendarPurpose7 = Trim(Purpose7)
            ElseIf (k = 14) Then
                CalPost.CalendarEndTime7 = Trim(lstStartTime.List(i))
            End If
        End If
    Next i
    CalPost.CalendarCat1 = CalendarCategory1
    CalPost.CalendarCat2 = CalendarCategory2
    CalPost.CalendarCat3 = CalendarCategory3
    CalPost.CalendarCat4 = CalendarCategory4
    CalPost.CalendarCat5 = CalendarCategory5
    CalPost.CalendarCat6 = CalendarCategory6
    CalPost.CalendarCat7 = CalendarCategory7
    CalPost.CalendarCat8 = CalendarCategory8
    If (CalPost.ApplySchedulerCalendar) Then
        CalId = CalPost.CalendarId
    End If
End If
Set CalPost = Nothing
Set VerifyCalendar = Nothing
ApplPostCalendar = True
End Function

Public Function ApplIsDateSetable(TheDate As String) As Boolean
Dim Temp1 As String
Dim Temp2 As String
Dim LocalDate1 As ManagedDate
Dim LocalDate2 As ManagedDate
ApplIsDateSetable = False
Temp1 = CheckConfigCollection("CALENDARDATE")
Call AdjustDate(Temp1, 365 * 5, Temp2)
Set LocalDate1 = New ManagedDate
Set LocalDate2 = New ManagedDate
LocalDate1.ExposedDate = Temp2
Call LocalDate1.ConvertDisplayDateToManagedDate
LocalDate2.ExposedDate = TheDate
Call LocalDate2.ConvertDisplayDateToManagedDate
If (LocalDate2.ExposedDate <= LocalDate1.ExposedDate) Then
    ApplIsDateSetable = True
End If
Set LocalDate1 = Nothing
Set LocalDate2 = Nothing
End Function

Public Function LoadCalendarDate() As Boolean
Dim Temp As String
Dim i As Integer
Dim BDate As String
Dim LocalDate As ManagedDate
Dim RetCal As SchedulerCalendar
Set RetCal = New SchedulerCalendar
Set LocalDate = New ManagedDate
LoadCalendarDate = True
CalendarId = 0
Vacation = False
Holiday = False
DisplayColor = ""
Location1 = 0
Location2 = 0
Location3 = 0
Location4 = 0
Location5 = 0
Location6 = 0
Location7 = 0
Purpose1 = ""
Purpose2 = ""
Purpose3 = ""
Purpose4 = ""
Purpose5 = ""
Purpose6 = ""
Purpose7 = ""
If (BreakDown < 1) Then
    BreakDown = 15
End If
LocalDate.ExposedDate = CalendarDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    Call LoadTime(lstStartTime, "", "", BreakDown)
    For i = 0 To lstStartTime.ListCount - 1
        lstStartTime.Selected(i) = False
    Next i
    RetCal.CalendarResourceId = ResourceId
    RetCal.CalendarDate = LocalDate.ExposedDate
    If (RetCal.FindCalendarGeneral > 0) Then
        If (RetCal.SelectCalendar(1)) Then
            CalendarId = RetCal.CalendarId
            CalendarCategory1 = RetCal.CalendarCat1
            CalendarCategory2 = RetCal.CalendarCat2
            CalendarCategory3 = RetCal.CalendarCat3
            CalendarCategory4 = RetCal.CalendarCat4
            CalendarCategory5 = RetCal.CalendarCat5
            CalendarCategory6 = RetCal.CalendarCat6
            CalendarCategory7 = RetCal.CalendarCat7
            CalendarCategory8 = RetCal.CalendarCat8
            CalendarComment = RetCal.CalendarComment
            Vacation = RetCal.CalendarVacation
            Holiday = RetCal.CalendarHoliday
            Purpose1 = RetCal.CalendarPurpose1
            Temp = RetCal.CalendarStartTime1
            GoSub SetTime
            Purpose2 = RetCal.CalendarPurpose2
            Temp = RetCal.CalendarStartTime2
            GoSub SetTime
            Purpose3 = RetCal.CalendarPurpose3
            Temp = RetCal.CalendarStartTime3
            GoSub SetTime
            Purpose4 = RetCal.CalendarPurpose4
            Temp = RetCal.CalendarStartTime4
            GoSub SetTime
            Purpose5 = RetCal.CalendarPurpose5
            Temp = RetCal.CalendarStartTime5
            GoSub SetTime
            Purpose6 = RetCal.CalendarPurpose6
            Temp = RetCal.CalendarStartTime6
            GoSub SetTime
            Purpose7 = RetCal.CalendarPurpose7
            Temp = RetCal.CalendarStartTime7
            GoSub SetTime
            Temp = RetCal.CalendarEndTime1
            GoSub SetTime
            Temp = RetCal.CalendarEndTime2
            GoSub SetTime
            Temp = RetCal.CalendarEndTime3
            GoSub SetTime
            Temp = RetCal.CalendarEndTime4
            GoSub SetTime
            Temp = RetCal.CalendarEndTime5
            GoSub SetTime
            Temp = RetCal.CalendarEndTime6
            GoSub SetTime
            Temp = RetCal.CalendarEndTime7
            GoSub SetTime
            DisplayColor = Trim(Str(RetCal.CalendarColor))
            Location1 = RetCal.CalendarLoc1
            Location2 = RetCal.CalendarLoc2
            Location3 = RetCal.CalendarLoc3
            Location4 = RetCal.CalendarLoc4
            Location5 = RetCal.CalendarLoc5
            Location6 = RetCal.CalendarLoc6
            Location7 = RetCal.CalendarLoc7
        End If
    End If
End If
For i = 0 To lstStartTime.ListCount - 1
    If (lstStartTime.Selected(i)) Then
        lstStartTime.ListIndex = i
        Exit For
    End If
Next i
Set LocalDate = Nothing
Set RetCal = Nothing
Exit Function
SetTime:
    If (Trim(Temp) <> "") Then
        For i = 0 To lstStartTime.ListCount - 1
            If (Temp = lstStartTime.List(i)) Then
                lstStartTime.Selected(i) = True
                Exit For
            End If
        Next i
    End If
    Return
End Function

Public Function PurgeCalendarDate() As Boolean
Dim VerifyCalendar As SchedulerCalendar
Dim CalPost As SchedulerCalendar
If (Trim(CalendarDate) <> "") And (ResourceId > 0) Then
    Set VerifyCalendar = New SchedulerCalendar
    VerifyCalendar.CalendarLoc1 = -1
    VerifyCalendar.CalendarResourceId = ResourceId
    VerifyCalendar.CalendarDate = CalendarDate
    If (VerifyCalendar.FindCalendar > 0) Then
        If (VerifyCalendar.SelectCalendar(1)) Then
            Call VerifyCalendar.DeleteSchedulerCalendar
        End If
    End If
    Set VerifyCalendar = Nothing
End If
End Function

Public Sub SetUpTemplates(ATemp As String)
Dim i As Integer
Dim Temp As String
Dim DisplayName As String
Dim PracticeFind As PracticeName
Set PracticeFind = New PracticeName
lstStartTime.Clear
If (ATemp = "T") Then
    lstStartTime.AddItem " Create New Daily Template"
Else
    lstStartTime.AddItem " Create New Weekly Template"
End If
PracticeFind.PracticeName = Chr(1)
PracticeFind.PracticeType = ATemp
If (PracticeFind.FindPractice > 0) Then
    i = 1
    While (PracticeFind.SelectPractice(i))
        DisplayName = Space(75)
        Mid(DisplayName, 1, Len(PracticeFind.PracticeName)) = PracticeFind.PracticeName
        Mid(DisplayName, 70, 5) = Trim(Str(PracticeFind.PracticeId))
        lstStartTime.AddItem DisplayName
        i = i + 1
    Wend
End If
Set PracticeFind = Nothing
End Sub

Public Sub LoadTemplate(PracticeId As Long)
Dim i As Integer
Dim PracticeFind As PracticeName
If (PracticeId > 0) Then
    Set PracticeFind = New PracticeName
    PracticeFind.PracticeId = PracticeId
    If (PracticeFind.RetrievePracticeName) Then
        DisplayColor = Trim(PracticeFind.PracticeFax)
        TemplateLocation = -1
        If (Trim(PracticeFind.PracticeEmail) <> "") Then
            TemplateLocation = Val(Trim(PracticeFind.PracticeEmail))
        End If
        Location1 = -1
        Location2 = -1
        Location3 = -1
        Location4 = -1
        Location5 = -1
        Location6 = -1
        Location7 = -1
        If (Trim(PracticeFind.PracticeCity) <> "") Then
            If (Len(PracticeFind.PracticeCity) > 0) Then
               Location1 = Val(Trim(Mid(PracticeFind.PracticeCity, 1, 4)))
            End If
            If (Len(PracticeFind.PracticeCity) > 4) Then
               Location2 = Val(Trim(Mid(PracticeFind.PracticeCity, 5, 4)))
            End If
            If (Len(PracticeFind.PracticeCity) > 8) Then
                Location3 = Val(Trim(Mid(PracticeFind.PracticeCity, 9, 4)))
            End If
            If (Len(PracticeFind.PracticeCity) > 12) Then
                Location4 = Val(Trim(Mid(PracticeFind.PracticeCity, 13, 4)))
            End If
            If (Len(PracticeFind.PracticeCity) > 16) Then
                Location5 = Val(Trim(Mid(PracticeFind.PracticeCity, 17, 4)))
            End If
            If (Len(PracticeFind.PracticeCity) > 20) Then
                Location6 = Val(Mid(PracticeFind.PracticeCity, 21, 4))
            End If
            If (Len(PracticeFind.PracticeCity) > 24) Then
                Location7 = Val(Mid(PracticeFind.PracticeCity, 25, 4))
            End If
        End If
' Always Set to the Date user selected
        i = GetTime(Trim(PracticeFind.PracticeStartTime1), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeStartTime2), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeStartTime3), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeStartTime4), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeStartTime5), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeStartTime6), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeStartTime7), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime1), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime2), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime3), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime4), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime5), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime6), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        i = GetTime(Trim(PracticeFind.PracticeEndTime7), lstStartTime)
        If (i > 0) Then
            lstStartTime.Selected(i) = True
        End If
        CalendarCategory1 = PracticeFind.PracticeCat1
        CalendarCategory2 = PracticeFind.PracticeCat2
        CalendarCategory3 = PracticeFind.PracticeCat3
        CalendarCategory4 = PracticeFind.PracticeCat4
        CalendarCategory5 = PracticeFind.PracticeCat5
        CalendarCategory6 = PracticeFind.PracticeCat6
        CalendarCategory7 = PracticeFind.PracticeCat7
        CalendarCategory8 = PracticeFind.PracticeCat8
    End If
    Set PracticeFind = Nothing
End If
End Sub

Private Sub ReloadSlotsFromAppointments(ResId As Long, Loc As Long, BreakDown As Integer, StartOfWeek As String, EndOfWeek, TheSlots As SlotDayArray)
Dim k As Integer, q As Integer
Dim ADate As Date
Dim ApptDay As String
Dim ApptDate As String
Dim ApptTime As Integer
Dim TheDay As Integer
Dim TimeBit As Integer
Dim Duration As Integer
Dim DurationBits As Integer
Dim KeepGoing As Boolean
Dim LocalDate As ManagedDate
Set LocalDate = New ManagedDate
Dim RetrieveAppt As SchedulerAppointment
Dim RetrieveApptType As SchedulerAppointmentType
Set RetrieveAppt = New SchedulerAppointment
Set RetrieveApptType = New SchedulerAppointmentType
RetrieveAppt.ResourceId = ResId
RetrieveAppt.AppointmentDate = StartOfWeek
RetrieveAppt.AppointmentResourceId2 = Loc
If (RetrieveAppt.FindAppointmentNextbyDateSlots > 0) Then
    k = 1
    KeepGoing = True
    If (BreakDown < 5) Then
        BreakDown = SetBreakdown(0)
    End If
    While (RetrieveAppt.SelectAppointment(k)) And (KeepGoing)
        If (RetrieveAppt.AppointmentDate < EndOfWeek) Then
            If (InStrPS("RPAD", RetrieveAppt.AppointmentStatus) > 0) And (Trim(RetrieveAppt.AppointmentComments) <> "ADDED VIA BILLING") And (Trim(RetrieveAppt.AppointmentComments) <> "ADD VIA BILLING") Then
                Duration = BreakDown
                RetrieveApptType.AppointmentTypeId = RetrieveAppt.AppointmentTypeId
                If (RetrieveApptType.AppointmentTypeId > 0) Then
                    If (RetrieveApptType.RetrieveSchedulerAppointmentType) Then
                        Duration = RetrieveApptType.AppointmentTypeDuration
                    End If
                End If
                DurationBits = Duration / BreakDown
                ApptDate = RetrieveAppt.AppointmentDate
                ApptTime = RetrieveAppt.AppointmentTime
                TheDay = 0
                LocalDate.ExposedDate = ApptDate
                If (LocalDate.ConvertManagedDateToDisplayDate) Then
                    TheDay = Weekday(LocalDate.ExposedDate)
                End If
                TimeBit = ApptTime / BreakDown
                ApptDay = ""
                For q = TimeBit To TimeBit + (DurationBits - 1)
                    If (q <= Len(TheSlots.TheDays(TheDay))) Then
                        If (q > 0) And (q <= Len(TheSlots.TheDays(TheDay))) Then
                            Mid(TheSlots.TheDays(TheDay), q, 1) = "1"
                        End If
                    End If
                Next q
            End If
        Else
            KeepGoing = False
        End If
        k = k + 1
    Wend
End If
Set LocalDate = Nothing
Set RetrieveAppt = Nothing
Set RetrieveApptType = Nothing
Exit Sub
UI_ErrorHandler:
    Set LocalDate = Nothing
    Set RetrieveAppt = Nothing
    Set RetrieveApptType = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Public Function SetHolidays(HolDay As String) As Boolean
Dim j As Integer
Dim BDate As String
Dim RetCal As SchedulerCalendar
SetHolidays = False
Set RetCal = New SchedulerCalendar
BDate = Mid(HolDay, 7, 4) + Left(HolDay, 2) + Mid(HolDay, 4, 2)
RetCal.CalendarDate = BDate
If (RetCal.FindCalendarbyDate > 0) Then
    j = 1
    While (RetCal.SelectCalendar(j))
        RetCal.CalendarHoliday = True
        RetCal.ApplySchedulerCalendar
        j = j + 1
    Wend
End If
Set RetCal = Nothing
SetHolidays = True
End Function

Public Function SetDoctorNote(ANoteId As Long, ANote As String) As Boolean
Dim RetCal As SchedulerCalendar
If (ANoteId > 0) Then
    Set RetCal = New SchedulerCalendar
    RetCal.CalendarId = ANoteId
    If (RetCal.RetrieveSchedulerCalendar) Then
        RetCal.CalendarComment = Trim(ANote)
        Call RetCal.ApplySchedulerCalendar
    End If
    Set RetCal = Nothing
End If
End Function
