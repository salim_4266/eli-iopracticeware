VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicSecondaryLanguage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicLanguage Object Module
Option Explicit
Public LanguageId As Long
Public Language As String
Public ControlId As Long
Public ControlText As String
Public ConfirmLingo As String

Private ModuleName As String
Private ClinicalLanguageTbl As ADODB.Recordset
Private DynamicLanguageTotal As Long
Private ClinicalLanguageNew As Boolean

Private Sub Class_Initialize()
Set ClinicalLanguageTbl = New ADODB.Recordset
ClinicalLanguageTbl.CursorLocation = adUseClient
Call InitLanguageControl
End Sub

Private Sub Class_Terminate()
Call InitLanguageControl
Set ClinicalLanguageTbl = Nothing
End Sub

Private Sub InitLanguageControl()
ClinicalLanguageNew = False
ControlId = 0
Language = ""
ControlText = ""
ConfirmLingo = ""
End Sub

Private Sub LoadLanguageControl()
ClinicalLanguageNew = False
LanguageId = ClinicalLanguageTbl("LanguageId")
If (ClinicalLanguageTbl("ControlId") <> "") Then
    ControlId = ClinicalLanguageTbl("ControlId")
Else
    ControlId = 0
End If
If (ClinicalLanguageTbl("Language") <> "") Then
    Language = ClinicalLanguageTbl("Language")
Else
    Language = ""
End If
If (ClinicalLanguageTbl("ControlText") <> "") Then
    ControlText = ClinicalLanguageTbl("ControlText")
Else
    ControlText = ""
End If
If (ClinicalLanguageTbl("ConfirmLingo") <> "") Then
    ConfirmLingo = ClinicalLanguageTbl("ConfirmLingo")
Else
    ConfirmLingo = ""
End If
End Sub

Private Function GetLanguageControl()
On Error GoTo DbErrorHandler
GetLanguageControl = True
Dim OrderString As String
OrderString = "SELECT * FROM SecondaryFormsLanguage WHERE LanguageId =" + Str(LanguageId) + " "
Set ClinicalLanguageTbl = Nothing
Set ClinicalLanguageTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalLanguageTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClinicalLanguageTbl.EOF) Then
    Call InitLanguageControl
    ClinicalLanguageNew = True
Else
    Call LoadLanguageControl
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetLanguageControl"
    GetLanguageControl = False
    LanguageId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutLanguageControl()
On Error GoTo DbErrorHandler
PutLanguageControl = True
If (ClinicalLanguageNew) Then
    ClinicalLanguageTbl.AddNew
End If
ClinicalLanguageTbl("ControlId") = ControlId
ClinicalLanguageTbl("Language") = Language
ClinicalLanguageTbl("ControlText") = ControlText
ClinicalLanguageTbl("ConfirmLingo") = Trim(ConfirmLingo)
ClinicalLanguageTbl.Update
If (ClinicalLanguageNew) Then
    LanguageId = GetJustAddedRecord
    ClinicalLanguageNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutLanguageControl"
    PutLanguageControl = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetLanguageControlExclusive) Then
    GetJustAddedRecord = ClinicalLanguageTbl("LanguageId")
End If
End Function

Private Function GetLanguageControlExclusive() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
DynamicLanguageTotal = 0
Ref = ""
GetLanguageControlExclusive = -1
If (ControlId < 1) And (Trim(Language) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM SecondaryFormsLanguage WHERE "
If (ControlId > 0) Then
    OrderString = OrderString + Ref + "ControlId = " + Trim(Str(ControlId)) + " "
    Ref = "And "
End If
If (Trim(Language) <> "") Then
    OrderString = OrderString + Ref + "[Language] = '" + Trim(Language) + "' "
End If
OrderString = OrderString + "ORDER BY [Language] ASC, ControlId ASC "
Set ClinicalLanguageTbl = Nothing
Set ClinicalLanguageTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalLanguageTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (ClinicalLanguageTbl.EOF) Then
    ClinicalLanguageTbl.MoveLast
    DynamicLanguageTotal = ClinicalLanguageTbl.RecordCount
    GetLanguageControlExclusive = ClinicalLanguageTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetLanguageControlExclusive"
    GetLanguageControlExclusive = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyLanguageControl() As Boolean
ApplyLanguageControl = PutLanguageControl
End Function

Public Function RetrieveLanguageControl() As Boolean
RetrieveLanguageControl = GetLanguageControl
End Function

Public Function SelectLanguageControl(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectLanguageControl = False
If (ClinicalLanguageTbl.EOF) Or (ListItem < 1) Or (ListItem > DynamicLanguageTotal) Then
    Exit Function
End If
ClinicalLanguageTbl.MoveFirst
ClinicalLanguageTbl.Move ListItem - 1
SelectLanguageControl = True
Call LoadLanguageControl
Exit Function
DbErrorHandler:
    ModuleName = "SelectLanguageControl"
    LanguageId = -256
    SelectLanguageControl = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindLanguageControl() As Long
FindLanguageControl = GetLanguageControlExclusive()
End Function

