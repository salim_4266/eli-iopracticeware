VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ListFilters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'List Filters Class
'R(esource)=Staff, L=Loc, S=Status
Option Explicit
Public ListType As String

Private strSQL As String
Private rsListFilters As ADODB.Recordset

Private Sub Class_InitList()
ListType = ""
strSQL = ""
End Sub

Private Sub Class_Initialize()
Set rsListFilters = Nothing
Call Class_InitList
End Sub

Public Sub PopList(lstBox As ListBox)
If (ListType = "R") Then
    strSQL = "usp_ListStaff " + "D"
    Set rsListFilters = CreateAdoRecordset
    Call rsListFilters.Open(strSQL, MyPracticeRepository)
    While Not (rsListFilters.EOF)
        lstBox.AddItem (rsListFilters("ResourceName") + Str(rsListFilters("ResourceId")))
        rsListFilters.MoveNext
    Wend
    strSQL = "usp_ListStaff " + "Q"
    Set rsListFilters = CreateAdoRecordset
    Call rsListFilters.Open(strSQL, MyPracticeRepository)
    While Not (rsListFilters.EOF)
        lstBox.AddItem (rsListFilters("ResourceName") + Str(rsListFilters("ResourceId")))
        rsListFilters.MoveNext
    Wend
ElseIf (ListType = "L") Then
    strSQL = "usp_ListLocs"
    Set rsListFilters = CreateAdoRecordset
    Call rsListFilters.Open(strSQL, MyPracticeRepository)
    Dim tmp As String
    While Not (rsListFilters.EOF)
'        If (Trim(rsListFilters("LocRef")) = "") Then
'            lstBox.AddItem (rsListFilters("ResourceName") + rsListFilters("TableId") + "0")
'        Else
'            lstBox.AddItem (rsListFilters("ResourceName") + rsListFilters("TableId") + Str(rsListFilters("LocId")))
'        End If
        If (Trim(rsListFilters("LocRef")) = "") Then
            tmp = (rsListFilters("ResourceName") + rsListFilters("TableId") + "0")
        Else
            tmp = (rsListFilters("ResourceName") + rsListFilters("TableId") + Str(rsListFilters("LocId")))
        End If
        If rsListFilters("TableId") = "P" Then
            If (Trim(rsListFilters("LocRef")) = "") Then
                Mid(tmp, 1, 20) = Left("OFFICE" & Space(20), 20)
            Else
                Mid(tmp, 1, 20) = Left("OFFICE-" & rsListFilters("LocRef") & Space(20), 20)
            End If
        End If
        lstBox.AddItem tmp
        rsListFilters.MoveNext
    Wend
ElseIf (ListType = "S") Then
    Dim RefType As String
    RefType = "LETTERSTATUS"
    strSQL = "usp_ListStatus " + RefType
    Set rsListFilters = CreateAdoRecordset
    Call rsListFilters.Open(strSQL, MyPracticeRepository)
    While Not (rsListFilters.EOF)
        lstBox.AddItem (rsListFilters("Code"))
        rsListFilters.MoveNext
    Wend
End If
End Sub

