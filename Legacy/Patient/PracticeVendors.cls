VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeVendors"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeVendors Object Module
Option Explicit
Public VendorId As Long
Public VendorName As String
Public VendorFirmName As String
Public VendorLastName As String
Public VendorFirstName As String
Public VendorMI As String
Public VendorSalutation As String
Public VendorTitle As String
Public VendorType As String
Public VendorAddress As String
Public VendorSuite As String
Public VendorCity As String
Public VendorState As String
Public VendorZip As String
Public VendorPhone As String
Public VendorFax As String
Public VendorEmail As String
Public VendorTaxId As String
Public VendorUpin As String
Public VendorSpecialty As String
Public VendorOtherOffice1 As String
Public VendorOtherOffice2 As String
Public VendorTaxonomy As String
Public VendorExcludeRefDr As Boolean
Public VendorNPI As String

Private ModuleName As String
Private VendorTbl As ADODB.Recordset
Private VendorNew As Boolean

Private Sub Class_Initialize()
Set VendorTbl = CreateAdoRecordset
Call InitVendor
End Sub

Private Sub Class_Terminate()
Call InitVendor
Set VendorTbl = Nothing
End Sub

Private Sub InitVendor()
VendorNew = True
VendorName = ""
VendorFirmName = ""
VendorLastName = ""
VendorFirstName = ""
VendorMI = ""
VendorSalutation = ""
VendorTitle = ""
VendorType = ""
VendorTaxId = ""
VendorAddress = ""
VendorSuite = ""
VendorCity = ""
VendorState = ""
VendorZip = ""
VendorPhone = ""
VendorFax = ""
VendorEmail = ""
VendorSpecialty = ""
VendorUpin = ""
VendorOtherOffice1 = ""
VendorOtherOffice2 = ""
VendorTaxonomy = ""
VendorExcludeRefDr = False
VendorNPI = ""
End Sub

Private Sub LoadVendor()
VendorNew = False
If (VendorTbl("VendorId") <> "") Then
    VendorId = VendorTbl("VendorId")
Else
    VendorId = 0
End If
If (VendorTbl("VendorName") <> "") Then
    VendorName = VendorTbl("VendorName")
Else
    VendorName = ""
End If
If (VendorTbl("VendorFirmName") <> "") Then
    VendorFirmName = VendorTbl("VendorFirmName")
Else
    VendorFirmName = ""
End If
If (VendorTbl("VendorLastName") <> "") Then
    VendorLastName = VendorTbl("VendorLastName")
Else
    VendorLastName = ""
End If
If (VendorTbl("VendorFirstName") <> "") Then
    VendorFirstName = VendorTbl("VendorFirstName")
Else
    VendorFirstName = ""
End If
If (VendorTbl("VendorMI") <> "") Then
    VendorMI = VendorTbl("VendorMI")
Else
    VendorMI = ""
End If
If (VendorTbl("VendorSalutation") <> "") Then
    VendorSalutation = VendorTbl("VendorSalutation")
Else
    VendorSalutation = ""
End If
If (VendorTbl("VendorTitle") <> "") Then
    VendorTitle = VendorTbl("VendorTitle")
Else
    VendorTitle = ""
End If
If (VendorTbl("VendorType") <> "") Then
    VendorType = VendorTbl("VendorType")
Else
    VendorType = ""
End If
If (VendorTbl("VendorAddress") <> "") Then
    VendorAddress = VendorTbl("VendorAddress")
Else
    VendorAddress = ""
End If
If (VendorTbl("VendorSuite") <> "") Then
    VendorSuite = VendorTbl("VendorSuite")
Else
    VendorSuite = ""
End If
If (VendorTbl("VendorCity") <> "") Then
    VendorCity = VendorTbl("VendorCity")
Else
    VendorCity = ""
End If
If (VendorTbl("VendorState") <> "") Then
    VendorState = VendorTbl("VendorState")
Else
    VendorState = ""
End If
If (VendorTbl("VendorZip") <> "") Then
    VendorZip = VendorTbl("VendorZip")
Else
    VendorZip = ""
End If
If (VendorTbl("VendorPhone") <> "") Then
    VendorPhone = VendorTbl("VendorPhone")
Else
    VendorPhone = ""
End If
If (VendorTbl("VendorFax") <> "") Then
    VendorFax = VendorTbl("VendorFax")
Else
    VendorFax = ""
End If
If (VendorTbl("VendorEmail") <> "") Then
    VendorEmail = VendorTbl("VendorEmail")
Else
    VendorEmail = ""
End If
If (VendorTbl("VendorTaxId") <> "") Then
    VendorTaxId = VendorTbl("VendorTaxId")
Else
    VendorTaxId = ""
End If
If (VendorTbl("VendorUPIN") <> "") Then
    VendorUpin = VendorTbl("VendorUPIN")
Else
    VendorUpin = ""
End If
If (VendorTbl("VendorSpecialty") <> "") Then
    VendorSpecialty = VendorTbl("VendorSpecialty")
Else
    VendorSpecialty = ""
End If
If (VendorTbl("VendorOtherOffice1") <> "") Then
    VendorOtherOffice1 = VendorTbl("VendorOtherOffice1")
Else
    VendorOtherOffice1 = ""
End If
If (VendorTbl("VendorOtherOffice2") <> "") Then
    VendorOtherOffice2 = VendorTbl("VendorOtherOffice2")
Else
    VendorOtherOffice2 = ""
End If
If (VendorTbl("VendorExcRefDr") <> "") Then
    If (VendorTbl("VendorExcRefDr") = "T") Then
        VendorExcludeRefDr = True
    Else
        VendorExcludeRefDr = False
    End If
Else
    VendorExcludeRefDr = False
End If
If (VendorTbl("VendorTaxonomy") <> "") Then
    VendorTaxonomy = VendorTbl("VendorTaxonomy")
Else
    VendorTaxonomy = ""
End If
If (VendorTbl("VendorNPI") <> "") Then
    VendorNPI = VendorTbl("VendorNPI")
Else
    VendorNPI = ""
End If
End Sub

Private Function getVendor()
On Error GoTo DbErrorHandler
getVendor = False
Dim OrderString As String
OrderString = "SELECT * FROM PracticeVendors WHERE VendorId =" + Str(VendorId) + " "
Set VendorTbl = Nothing
Set VendorTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call VendorTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (VendorTbl.EOF) Then
    Call InitVendor
Else
    Call LoadVendor
End If
getVendor = True
Exit Function
DbErrorHandler:
    ModuleName = "GetVendor"
    getVendor = False
    VendorId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetVendorList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
Ref = ""
GetVendorList = -1
If (IType <> 2) And (IType <> 3) Then
    OrderString = "Select * FROM PracticeVendors WHERE "
    If (Len(VendorName) >= 1) Then
        OrderString = OrderString + "(VendorName Like '" + UCase(VendorName) + "%') "
        Ref = "Or "
    End If
    If (Len(VendorLastName) >= 1) Then
        OrderString = OrderString + Ref + "(VendorLastName Like '" + UCase(VendorLastName) + "%') "
    End If
ElseIf (IType = 3) Then
    OrderString = "Select * FROM PracticeVendors WHERE (VendorLastName = '" + UCase(Trim(VendorName)) + "') "
    OrderString = OrderString + "Or ((VendorLastName = '" + UCase(Trim(VendorLastName)) + "') And (VendorFirstName = '" + UCase(Trim(VendorFirstName)) + "')) "
    Ref = "And "
Else
    OrderString = "Select * FROM PracticeVendors WHERE (VendorLastName = '" + UCase(Trim(VendorName)) + "') "
    OrderString = OrderString + "Or (VendorName = '" + UCase(Trim(VendorLastName)) + "') "
    Ref = "And "
End If
If (Trim(VendorType) <> "") And (Trim(VendorType) <> "~") Then
    OrderString = OrderString + Ref + "VendorType = '" + UCase(Trim(VendorType)) + "' "
End If
If (IType = 3) Then
    OrderString = OrderString + "ORDER BY VendorId DESC, VendorLastName ASC, VendorFirstName ASC, VendorName ASC "
Else
    OrderString = OrderString + "ORDER BY VendorLastName ASC, VendorFirstName ASC, VendorName ASC "
End If
Set VendorTbl = Nothing
Set VendorTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call VendorTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (VendorTbl.EOF) Then
    GetVendorList = -1
    Call InitVendor
Else
    If (IType = 1) Then
        GetVendorList = VendorTbl.RecordCount
    Else
        GetVendorList = 1
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetVendorList"
    GetVendorList = -256
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetVendorbyRefId() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
GetVendorbyRefId = -1
Ref = ""
If (Trim(VendorOtherOffice1) = "") And (Trim(VendorOtherOffice2) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeVendors WHERE "
If (Trim(VendorOtherOffice1) <> "") Then
    OrderString = OrderString + Ref + "VendorOtherOffice1 = '" + UCase(Trim(VendorOtherOffice1)) + "' "
    Ref = "And "
End If
If (Trim(VendorOtherOffice2) <> "") Then
    OrderString = OrderString + Ref + "VendorOtherOffice2 = '" + UCase(Trim(VendorOtherOffice2)) + "' "
End If
OrderString = OrderString + "ORDER BY VendorName ASC "
Set VendorTbl = Nothing
Set VendorTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call VendorTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (VendorTbl.EOF) Then
    GetVendorbyRefId = -1
    Call InitVendor
Else
    GetVendorbyRefId = VendorTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetVendorbyRefId"
    GetVendorbyRefId = -256
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetVendorZipList() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetVendorZipList = -1
OrderString = "Select DISTINCT VendorZip FROM PracticeVendors WHERE VendorZip <> '' "
OrderString = OrderString + "ORDER BY VendorZip ASC "
Set VendorTbl = Nothing
Set VendorTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call VendorTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (VendorTbl.EOF) Then
    GetVendorZipList = -1
    Call InitVendor
Else
    GetVendorZipList = VendorTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetVendorZipList"
    GetVendorZipList = -256
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetVendorbyLastName(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
Ref = ""
GetVendorbyLastName = -1
If (Len(VendorType) < 1) And (Len(VendorLastName) < 1) Then
    Exit Function
End If
OrderString = "SELECT * FROM (SELECT ROW_NUMBER() OVER (PARTITION BY VendorId ORDER BY VendorName) as RowNum, * FROM PracticeVendors) r WHERE r.RowNum = 1 AND "
If (Trim(VendorLastName) <> "") Then
    OrderString = OrderString + "r.VendorLastName >= '" + UCase(Trim(VendorLastName)) + "' "
    Ref = " And"
End If
If (Trim(VendorFirstName) <> "") Then
    OrderString = OrderString + Ref + " r.VendorFirstName = '" + UCase(Trim(VendorFirstName)) + "' "
    Ref = " And"
End If
If (Trim(VendorType) <> "") Then
    OrderString = OrderString + Ref + " r.VendorType = '" + UCase(Trim(VendorType)) + "' "
End If
If (Trim(VendorName) <> "") Then
    OrderString = OrderString + "ORDER BY r.VendorName ASC "
Else
    OrderString = OrderString + "ORDER BY r.VendorLastName ASC, r.VendorFirstName ASC "
End If
Set VendorTbl = Nothing
Set VendorTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call VendorTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (VendorTbl.EOF) Then
    GetVendorbyLastName = -1
    Call InitVendor
Else
    If (IType = 1) Then
        GetVendorbyLastName = VendorTbl.RecordCount
    Else
        GetVendorbyLastName = 1
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetVendorbyLastName"
    GetVendorbyLastName = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutVendor()
On Error GoTo DbErrorHandler
PutVendor = True
If (VendorNew) Then
    VendorTbl.AddNew
End If
VendorTbl("VendorName") = Trim(VendorName)
VendorTbl("VendorFirmName") = Trim(VendorFirmName)
VendorTbl("VendorLastName") = UCase(Trim(VendorLastName))
VendorTbl("VendorFirstName") = UCase(Trim(VendorFirstName))
VendorTbl("VendorMI") = UCase(Trim(Left(VendorMI, 1)))
VendorTbl("VendorTitle") = Trim(VendorTitle)
VendorTbl("VendorType") = UCase(Trim(VendorType))
VendorTbl("VendorSalutation") = Trim(VendorSalutation)
VendorTbl("VendorAddress") = Trim(VendorAddress)
VendorTbl("VendorSuite") = Trim(VendorSuite)
VendorTbl("VendorCity") = UCase(Trim(VendorCity))
VendorTbl("VendorState") = UCase(Trim(VendorState))
VendorTbl("VendorZip") = Trim(VendorZip)
VendorTbl("VendorPhone") = Trim(VendorPhone)
VendorTbl("VendorFax") = Trim(VendorFax)
VendorTbl("VendorEmail") = Trim(VendorEmail)
VendorTbl("VendorTaxId") = Trim(VendorTaxId)
VendorTbl("VendorUPIN") = Trim(VendorUpin)
VendorTbl("VendorSpecialty") = UCase(Trim(VendorSpecialty))
VendorTbl("VendorOtherOffice1") = UCase(Trim(VendorOtherOffice1))
VendorTbl("VendorOtherOffice2") = UCase(Trim(VendorOtherOffice2))
VendorTbl("VendorTaxonomy") = UCase(Trim(VendorTaxonomy))
VendorTbl("VendorExcRefDr") = "F"
VendorTbl("VendorNPI") = UCase(Trim(VendorNPI))
If (VendorExcludeRefDr) Then
    VendorTbl("VendorExcRefDr") = "T"
End If
VendorTbl.Update
If (VendorNew) Then
    VendorNew = False
    VendorId = GetJustAddedRecord
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutVendor"
    PutVendor = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetVendorList(3) > 0) Then
    If (SelectVendor(1)) Then
        GetJustAddedRecord = VendorTbl("VendorId")
    End If
End If
End Function

Private Function KillVendors() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillVendors = True
OrderString = "SELECT * FROM PracticeVendors "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set VendorTbl = Nothing
Set VendorTbl = CreateAdoRecordset
Call VendorTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (VendorTbl.EOF) Then
    VendorTbl.MoveFirst
    While Not (VendorTbl.EOF)
        VendorTbl.Delete
        VendorTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillVendors"
    KillVendors = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeVendor() As Boolean
On Error GoTo DbErrorHandler
PurgeVendor = True
VendorTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeVendor"
    PurgeVendor = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyVendor() As Boolean
ApplyVendor = PutVendor
End Function

Public Function KillVendor() As Boolean
KillVendor = KillVendors
End Function

Public Function DeleteVendor() As Boolean
DeleteVendor = PurgeVendor
End Function

Public Function RetrieveVendor() As Boolean
RetrieveVendor = getVendor
End Function

Public Function SelectVendor(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectVendor = False
If (VendorTbl.EOF) Or (ListItem < 1) Then
    Exit Function
End If
VendorTbl.MoveFirst
VendorTbl.Move ListItem - 1
SelectVendor = True
Call LoadVendor
Exit Function
DbErrorHandler:
    ModuleName = "SelectVendor"
    SelectVendor = False
    VendorId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectVendorZipList(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectVendorZipList = False
If (VendorTbl.EOF) Or (ListItem < 1) Then
    Exit Function
End If
VendorTbl.MoveFirst
VendorTbl.Move ListItem - 1
SelectVendorZipList = True
If (VendorTbl("VendorZip") <> "") Then
    VendorZip = VendorTbl("VendorZip")
Else
    VendorZip = ""
End If
Exit Function
DbErrorHandler:
    ModuleName = "SelectVendorZipList"
    SelectVendorZipList = False
    VendorId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindVendor() As Long
FindVendor = GetVendorList(0)
End Function

Public Function FindVendorbyRefId() As Long
FindVendorbyRefId = GetVendorbyRefId
End Function

Public Function FindVendorbyLastName() As Long
FindVendorbyLastName = GetVendorbyLastName(0)
End Function

Public Function FindVendorDirect() As Long
FindVendorDirect = GetVendorList(3)
End Function

Public Function FindVendorT() As Long
FindVendorT = GetVendorList(1)
End Function

Public Function FindVendorbyLastNameT() As Long
FindVendorbyLastNameT = GetVendorbyLastName(1)
End Function

Public Function FindVendorZipList() As Long
FindVendorZipList = GetVendorZipList
End Function
