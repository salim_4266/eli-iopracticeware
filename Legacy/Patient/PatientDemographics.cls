VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientDemoGraphics"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public Function DisplayPatientInfoScreen()
On Error GoTo lDisplayPatientInfoScreen_Error
    Dim arguments() As Variant
    Dim manager
    DisplayPatientInfoScreen = False
    
    Dim PatientInfoComWrapper As New comWrapper
    Call PatientInfoComWrapper.Create(PatientInfoLoadArgumentsType, emptyArgs)
    
    Dim loadArguments As Object
    Set loadArguments = PatientInfoComWrapper.Instance
    loadArguments.PatientId = PatientId
    
    arguments = Array(loadArguments)
    
    Set manager = PatientDemoGraphicsComWrapper.Instance
    Call manager.ShowPatientInfo(loadArguments)
    
    DisplayPatientInfoScreen = True
Exit Function
lDisplayPatientInfoScreen_Error:
    DisplayPatientInfoScreen = False
    LogError "PatientDemographics.cls", "DisplayPatientInfoScreen", Err, Err.Description
End Function
Public Function DisplayReferralsScreen(ByVal ApptId As Long, ByVal PatId As Long, ByVal RefId As Long) As Object
On Error GoTo lDisplayReferralsScreen_Error
Dim ReferralsComWrapper As New comWrapper
Dim loadArguments As Object
Dim args() As Variant
Dim ReturnArguments As Object
Dim AppDate As String

AppDate = CDate(Now)

If RefId > 0 And ApptId > 0 Then
    Dim RetAppointment As New SchedulerAppointment
    RetAppointment.AppointmentId = ApptId
    If (RetAppointment.RetrieveSchedulerAppointment) Then
        AppDate = Mid(RetAppointment.AppointmentDate, 5, 2) + "/" + Mid(RetAppointment.AppointmentDate, 7, 2) + "/" + Left(RetAppointment.AppointmentDate, 4)
    End If
    Set RetAppointment = Nothing
End If

Call ReferralsComWrapper.Create(ReferralLoadArgumentsType, emptyArgs)
Set loadArguments = ReferralsComWrapper.Instance
loadArguments.PatientId = PatId

If RefId > 0 Then loadArguments.ReferralId = RefId

loadArguments.AppointmentDate = CDate(AppDate)

args = Array(loadArguments)

Call ReferralsComWrapper.Create(ReferralsViewManagerType, emptyArgs)
Set ReturnArguments = ReferralsComWrapper.InvokeMethod("ShowReferrals", args)
Set DisplayReferralsScreen = ReturnArguments
Exit Function
lDisplayReferralsScreen_Error:
    Set DisplayReferralsScreen = Nothing
    LogError "PatientDemographics.cls", "DisplayReferralsScreen", Err, Err.Description
End Function
Public Function DisplayPreAuthorizationScreen(ByVal AppointmentId As Long, ThePre As Long) As Object
On Error GoTo lDisplayPreAuthorizationScreen_Error

Dim PreCertComWrapper As New comWrapper
Dim loadArguments As Object
Dim ReturnArguments As Object
Dim args() As Variant

Call PreCertComWrapper.Create(PatientInsuranceAuthorizationsLoadArgumentsType, emptyArgs)
Set loadArguments = PreCertComWrapper.Instance

loadArguments.EncounterId = AppointmentId
loadArguments.PatientInsuranceAuthorizationId = Nothing
If (ThePre > 0) Then
    loadArguments.PatientInsuranceAuthorizationId = ThePre
End If
args = Array(loadArguments)
Call PreCertComWrapper.Create(PatientInsuranceAuthorizationsViewManagerType, emptyArgs)
Set ReturnArguments = PreCertComWrapper.InvokeMethod("ShowPatientInsuranceAuthorization", args)
Set PreCertComWrapper = Nothing
Set DisplayPreAuthorizationScreen = ReturnArguments
Exit Function
lDisplayPreAuthorizationScreen_Error:
    Set DisplayPreAuthorizationScreen = Nothing
    LogError "PatientDemographics.cls", "DisplayPreAuthorizationScreen", Err, Err.Description
End Function
Public Function DisplayPatientFinancialScreen()
On Error GoTo lDisplayPatientFinancialScreen_Error
    Dim arguments() As Variant
    Dim manager
    DisplayPatientFinancialScreen = False
    
    Dim PatientFinancialComWrapper As New comWrapper
    Call PatientFinancialComWrapper.Create(PatientInfoLoadArgumentsType, emptyArgs)
    
    Dim loadArguments As Object
    Set loadArguments = PatientFinancialComWrapper.Instance
    loadArguments.PatientId = PatientId
    loadArguments.TabToOpenInt = 4
    arguments = Array(loadArguments)
    
    Set manager = PatientDemoGraphicsComWrapper.Instance
    Call manager.ShowPatientInfo(loadArguments)
    
    DisplayPatientFinancialScreen = True
Exit Function
lDisplayPatientFinancialScreen_Error:
    LogError "PatientDemographics.cls", "DisplayPatientFinancialScreen", Err, Err.Description
End Function
Public Function DisplayInvoiceEditAndBillScreen(ByVal ReceivableId As Long) As Boolean
On Error GoTo lDisplayInvoiceEditAndBillScreen_Error
    Dim arguments() As Variant
    Dim manager
    Dim ReturnArguments As Object
    Dim InvoiceId As Long
    Dim StrSql As String
    Dim RS As Recordset
    
    DisplayInvoiceEditAndBillScreen = False
    
    'Get Back InvoiceId from model.InvoiceReceivables Table using the ReceivabledId
    If ReceivableId > 0 Then
        StrSql = " SELECT TOP 1 InvoiceId FROM model.InvoiceReceivables WHERE ID =" & ReceivableId
        Set RS = GetRS(StrSql)
    
        If RS Is Nothing Then Exit Function
        InvoiceId = RS(0)
    End If
    
    Dim EditInvoiceComWrapper As New comWrapper
    Call EditInvoiceComWrapper.Create(EditInvoiceLoadArgumentsType, emptyArgs)
    
    Dim loadArguments As Object
    Set loadArguments = EditInvoiceComWrapper.Instance
    loadArguments.PatientId = PatientId
    loadArguments.InvoiceId = InvoiceId
    
    arguments = Array(loadArguments)
    
    Call EditInvoiceComWrapper.Create(EditInvoiceViewManagerType, emptyArgs)
    Set manager = EditInvoiceComWrapper.Instance
    Call manager.ShowEditInvoice(loadArguments)
    
    DisplayInvoiceEditAndBillScreen = True
Exit Function
lDisplayInvoiceEditAndBillScreen_Error:
    LogError "PatientDemographics.cls", "DisplayInvoiceEditAndBillScreen", Err, Err.Description
End Function
Public Sub DisplayInsurancePlanScreen(Optional PlanName As String)
On Error GoTo lDisplayInsurancePlanScreen

Dim InsurancePlanComWrapper As New comWrapper
Dim loadArguments As Object
Dim arguments() As Variant
Call InsurancePlanComWrapper.Create(ManageInsurancePlansLoadArgumentsType, emptyArgs)
Set loadArguments = InsurancePlanComWrapper.Instance

If PlanName <> "" Then loadArguments.Filter = PlanName

arguments = Array(loadArguments)
Call InsurancePlanComWrapper.Create(InsurancePlansSetupViewManagerType, emptyArgs)
Call InsurancePlanComWrapper.InvokeMethod("ManageInsurancePlans", arguments)

Exit Sub
lDisplayInsurancePlanScreen:
    LogError "frmSchedulerSetup", "DisplayInsurancePlanScreen", Err, Err.Description
End Sub
