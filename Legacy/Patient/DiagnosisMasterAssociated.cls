VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DiagnosisMasterAssociated"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Associated Diagnosis Object Module
Option Explicit
Public AssociatedId As Long
Public AssociatedSystem As String
Public AssociatedDiagnosis As String
Public AssociatedRank As Long
Public AssociatedLingo As String
Public PrimaryDiagnosis As String
Public AssociatedBrushId As String
Public AssociatedDefaultColor As String
Public AssociatedLocationSupport As String

' Hidden
Private ModuleName As String
Private AssociatedTotal As Integer
Private AssociatedTbl As ADODB.Recordset
Private AssociatedNew As Boolean

Private Sub Class_Initialize()
Set AssociatedTbl = New ADODB.Recordset
AssociatedTbl.CursorLocation = adUseClient
Call InitAssociated
End Sub

Private Sub Class_Terminate()
Call InitAssociated
Set AssociatedTbl = Nothing
End Sub

Private Sub InitAssociated()
AssociatedNew = True
AssociatedDiagnosis = ""
AssociatedRank = 0
AssociatedLingo = ""
PrimaryDiagnosis = ""
AssociatedBrushId = ""
AssociatedDefaultColor = ""
AssociatedLocationSupport = ""
End Sub

Private Sub LoadAssociated()
AssociatedNew = False
AssociatedId = AssociatedTbl("AssociatedId")
If (AssociatedTbl("AssociatedMedicalSystem") <> "") Then
    AssociatedSystem = AssociatedTbl("AssociatedMedicalSystem")
Else
    AssociatedSystem = ""
End If
If (AssociatedTbl("AssociatedDiagnosis") <> "") Then
    AssociatedDiagnosis = AssociatedTbl("AssociatedDiagnosis")
Else
    AssociatedDiagnosis = ""
End If
If (AssociatedTbl("AssociatedRank") <> "") Then
    AssociatedRank = AssociatedTbl("AssociatedRank")
Else
    AssociatedRank = 0
End If
If (AssociatedTbl("ButtonLingo") <> "") Then
    AssociatedLingo = AssociatedTbl("ButtonLingo")
Else
    AssociatedLingo = ""
End If
If (AssociatedTbl("PrimaryDiagnosis") <> "") Then
    PrimaryDiagnosis = AssociatedTbl("PrimaryDiagnosis")
Else
    PrimaryDiagnosis = ""
End If
If (AssociatedTbl("BrushId") <> "") Then
    AssociatedBrushId = AssociatedTbl("BrushId")
Else
    AssociatedBrushId = ""
End If
If (AssociatedTbl("DefaultColor") <> "") Then
    AssociatedDefaultColor = AssociatedTbl("DefaultColor")
Else
    AssociatedDefaultColor = ""
End If
If (AssociatedTbl("LocationSupport") <> "") Then
    AssociatedLocationSupport = AssociatedTbl("LocationSupport")
Else
    AssociatedLocationSupport = ""
End If
End Sub

Private Function GetAssociated()
On Error GoTo DbErrorHandler
Dim OrderString As String
GetAssociated = True
OrderString = "SELECT * FROM AssociatedDiagnosisTable WHERE AssociatedId =" + Str(AssociatedId) + " "
Set AssociatedTbl = Nothing
Set AssociatedTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call AssociatedTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (AssociatedTbl.EOF) Then
    Call InitAssociated
Else
    Call LoadAssociated
End If
Exit Function
DbErrorHandler:
    GetAssociated = False
    ModuleName = "GetAssociated"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
    AssociatedId = -256
End Function

Private Function GetAllAssociated() As Long
On Error GoTo DbErrorHandler
Dim DumpOn, RefOn As Boolean
Dim OrderString, Ref, SortBy As String
Ref = "And "
RefOn = False
AssociatedTotal = 0
GetAllAssociated = -1
If (Trim(PrimaryDiagnosis) = "") And (Trim(AssociatedDiagnosis) = "") Then
    Exit Function
End If
DumpOn = False
If (Len(Trim(PrimaryDiagnosis)) = 1) Or (Len(Trim(AssociatedDiagnosis)) = 1) Then
    DumpOn = True
End If
OrderString = "SELECT * FROM AssociatedDiagnosisTable WHERE "
If (Trim(PrimaryDiagnosis) <> "") Then
    If (DumpOn) Then
        OrderString = OrderString + "PrimaryDiagnosis >= '" + UCase(Trim(PrimaryDiagnosis)) + "' "
    Else
        OrderString = OrderString + "PrimaryDiagnosis = '" + UCase(Trim(PrimaryDiagnosis)) + "' "
    End If
    SortBy = "PrimaryDiagnosis"
    RefOn = True
End If
If (Trim(AssociatedDiagnosis) <> "") Then
    If (RefOn) Then
        If (DumpOn) Then
            OrderString = OrderString + Ref + "AssociatedDiagnosis >= '" + UCase(Trim(AssociatedDiagnosis)) + "' "
        Else
            OrderString = OrderString + Ref + "AssociatedDiagnosis = '" + UCase(Trim(AssociatedDiagnosis)) + "' "
        End If
    Else
        If (DumpOn) Then
            OrderString = OrderString + "AssociatedDiagnosis >= '" + UCase(Trim(AssociatedDiagnosis)) + "' "
        Else
            OrderString = OrderString + "AssociatedDiagnosis = '" + UCase(Trim(AssociatedDiagnosis)) + "' "
        End If
        RefOn = True
    End If
    SortBy = "AssociatedDiagnosis"
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY " + SortBy + " ASC, AssociatedRank ASC "
Set AssociatedTbl = Nothing
Set AssociatedTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call AssociatedTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (AssociatedTbl.EOF) Then
    GetAllAssociated = -1
Else
    AssociatedTbl.MoveLast
    AssociatedTotal = AssociatedTbl.RecordCount
    GetAllAssociated = AssociatedTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllAssociated"
    GetAllAssociated = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutAssociated()
On Error GoTo DbErrorHandler
PutAssociated = True
If (AssociatedNew) Then
    AssociatedTbl.AddNew
End If
AssociatedTbl("AssociatedDiagnosis") = UCase(Trim(AssociatedDiagnosis))
AssociatedTbl("AssociatedRank") = AssociatedRank
AssociatedTbl("ButtonLingo") = Trim(AssociatedLingo)
AssociatedTbl("PrimaryDiagnosis") = UCase(Trim(PrimaryDiagnosis))
AssociatedTbl("BrushId") = Trim(AssociatedBrushId)
AssociatedTbl("DefaultColor") = Trim(AssociatedDefaultColor)
AssociatedTbl("LocationSupport") = Trim(AssociatedLocationSupport)
AssociatedTbl.Update
If (AssociatedNew) Then
'    AssociatedTbl.Move 0, AssociatedTbl.LastModified
    AssociatedId = AssociatedTbl("AssociatedId")
    AssociatedNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutAssociated"
    PutAssociated = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeAssociated()
On Error GoTo DbErrorHandler
PurgeAssociated = True
If Not (AssociatedNew) Then
    AssociatedTbl.Delete
End If
Exit Function
DbErrorHandler:
    PurgeAssociated = False
    ModuleName = "PurgeAssociated"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectAssociated(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectAssociated = False
If (AssociatedTbl.EOF) Or (ListItem < 1) Or (ListItem > AssociatedTotal) Then
    Exit Function
End If
AssociatedTbl.MoveFirst
AssociatedTbl.Move ListItem - 1
SelectAssociated = True
Call LoadAssociated
Exit Function
DbErrorHandler:
    ModuleName = "SelectAssociated"
    AssociatedId = -256
    SelectAssociated = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyAssociated() As Boolean
ApplyAssociated = PutAssociated
End Function

Public Function RetrieveAssociated() As Boolean
RetrieveAssociated = GetAssociated
End Function

Public Function DeleteAssociated() As Boolean
DeleteAssociated = PurgeAssociated
End Function

Public Function FindAssociated() As Long
FindAssociated = GetAllAssociated()
End Function

Public Function FindAssociatedbyPrimary() As Long
FindAssociatedbyPrimary = GetAllAssociated()
End Function
