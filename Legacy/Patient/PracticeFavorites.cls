VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Favorites Object Module
Option Explicit
Public FavoritesId As Long
Public FavoritesSystem As String
Public FavoritesDiagnosis As String
Public FavoritesRank As Integer
Public FavoritesAlternate As String

Private ModuleName As String
Private PracticeFavoritesTotal As Long
Private PracticeFavoritesTbl As ADODB.Recordset
Private PracticeFavoritesNew As Boolean

Private Sub Class_Initialize()
Set PracticeFavoritesTbl = CreateAdoRecordset
Call InitFavorites
End Sub

Private Sub Class_Terminate()
Call InitFavorites
Set PracticeFavoritesTbl = Nothing
End Sub

Private Sub InitFavorites()
PracticeFavoritesNew = True
FavoritesId = 0
FavoritesSystem = ""
FavoritesDiagnosis = ""
FavoritesRank = 0
FavoritesAlternate = ""
End Sub

Private Sub LoadFavorites()
PracticeFavoritesNew = False
If (PracticeFavoritesTbl("Id") <> "") Then
    FavoritesId = PracticeFavoritesTbl("Id")
Else
    FavoritesId = 0
End If
If (PracticeFavoritesTbl("System") <> "") Then
    FavoritesSystem = PracticeFavoritesTbl("System")
Else
    FavoritesSystem = ""
End If
If (PracticeFavoritesTbl("Diagnosis") <> "") Then
    FavoritesDiagnosis = PracticeFavoritesTbl("Diagnosis")
Else
    FavoritesDiagnosis = ""
End If
If (PracticeFavoritesTbl("Rank") <> "") Then
    FavoritesRank = PracticeFavoritesTbl("Rank")
Else
    FavoritesRank = 0
End If
If (PracticeFavoritesTbl("Alternate") <> "") Then
    FavoritesAlternate = PracticeFavoritesTbl("Alternate")
Else
    FavoritesAlternate = ""
End If
End Sub

Private Function GetFavorite() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
PracticeFavoritesTotal = 0
GetFavorite = False
OrderString = "Select * FROM PracticeFavorites WHERE Id =" + Str(FavoritesId) + " "
OrderString = OrderString + "ORDER BY Rank ASC "
Set PracticeFavoritesTbl = Nothing
Set PracticeFavoritesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeFavoritesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeFavoritesTbl.EOF) Then
    GetFavorite = True
    Call InitFavorites
Else
    GetFavorite = True
    Call LoadFavorites
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetFavorite"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetFavorites(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Srt As String
Dim OrderString As String
PracticeFavoritesTotal = 0
GetFavorites = -1
If (Trim(FavoritesSystem) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeFavorites WHERE System = '" + UCase(Trim(FavoritesSystem)) + "' "
If (Trim(FavoritesDiagnosis) <> "") Then
    OrderString = OrderString + "And Diagnosis >= '" + UCase(Trim(FavoritesDiagnosis)) + "' "
End If
If (FavoritesRank > 0) Then
    OrderString = OrderString + "And Rank >= " + Trim(Str(FavoritesRank)) + " "
End If
If (IType = 0) Then
    OrderString = OrderString + " ORDER BY Rank ASC "
Else
    OrderString = OrderString + " ORDER BY Diagnosis ASC "
End If
Set PracticeFavoritesTbl = Nothing
Set PracticeFavoritesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeFavoritesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeFavoritesTbl.EOF) Then
    GetFavorites = -1
    Call InitFavorites
Else
    PracticeFavoritesTbl.MoveLast
    PracticeFavoritesTotal = PracticeFavoritesTbl.RecordCount
    GetFavorites = PracticeFavoritesTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetFavorites"
    GetFavorites = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutFavorites() As Boolean
On Error GoTo DbErrorHandler
PutFavorites = True
If (PracticeFavoritesNew) Then
    PracticeFavoritesTbl.AddNew
End If
PracticeFavoritesTbl("Diagnosis") = UCase(Trim(FavoritesDiagnosis))
PracticeFavoritesTbl("System") = UCase(FavoritesSystem)
PracticeFavoritesTbl("Rank") = FavoritesRank
PracticeFavoritesTbl("Alternate") = FavoritesAlternate
PracticeFavoritesTbl.Update
If (PracticeFavoritesNew) Then
    FavoritesId = GetJustAddedRecord
    PracticeFavoritesNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutFavorites"
    PutFavorites = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetFavorites(0) > 0) Then
    If (SelectFavorites(1)) Then
        GetJustAddedRecord = PracticeFavoritesTbl("Id")
    End If
End If
End Function

Private Function PurgeFavorites() As Boolean
On Error GoTo DbErrorHandler
PurgeFavorites = True
PracticeFavoritesTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeFavorites"
    PurgeFavorites = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyFavorites() As Boolean
ApplyFavorites = PutFavorites
End Function

Public Function DeleteFavorites() As Boolean
DeleteFavorites = PurgeFavorites
End Function

Public Function RetrieveFavorites() As Boolean
RetrieveFavorites = GetFavorite
End Function

Public Function SelectFavorites(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectFavorites = False
If (PracticeFavoritesTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeFavoritesTotal) Then
    Exit Function
End If
PracticeFavoritesTbl.MoveFirst
PracticeFavoritesTbl.Move ListItem - 1
SelectFavorites = True
Call LoadFavorites
Exit Function
DbErrorHandler:
    ModuleName = "SelectFavorites"
    SelectFavorites = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindFavorites() As Long
FindFavorites = GetFavorites(0)
End Function

Public Function FindFavoritesbyDiagnosis() As Long
FindFavoritesbyDiagnosis = GetFavorites(1)
End Function

Public Function DeleteAllFavorites() As Boolean
    DeleteAllFavorites = PurgeAllFavorites()
End Function

Private Function PurgeAllFavorites() As Boolean
On Error GoTo DbErrorHandler
PracticeFavoritesTbl.MoveFirst
While Not (PracticeFavoritesTbl.EOF)
    PracticeFavoritesTbl.Delete
    PracticeFavoritesTbl.MoveNext
Wend
PurgeAllFavorites = True
Exit Function
DbErrorHandler:
    ModuleName = "PurgeAllFavorites"
    PurgeAllFavorites = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
