VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientDemoAlt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public PatientId As Long
Public AltId As Integer
Public Selected As Boolean
Public Address As String
Public Suite As String
Public City As String
Public State As String
Public Zip As String
Public HomePhone As String
Public WorkPhone As String
Public CellPhone As String
Public Email As String
Public Pharmacy As Long
Public PhoneType1 As Integer
Public PhoneType2 As Integer
Public PhoneType3 As Integer
Private ModuleName As String
Private PatientDemoAltTbl As ADODB.Recordset

Private Sub Class_Initialize()
    Set PatientDemoAltTbl = CreateAdoRecordset
    Call InitPatientAlt
End Sub

Private Sub Class_Terminate()
    Call InitPatientAlt
    Set PatientDemoAltTbl = Nothing
End Sub

Private Sub InitPatientAlt()
    PatientId = 0
    AltId = 0
    Selected = False
    Address = ""
    Suite = ""
    City = ""
    State = ""
    Zip = ""
    HomePhone = ""
    WorkPhone = ""
    CellPhone = ""
    Email = ""
    Pharmacy = 0
    PhoneType1 = 0
    PhoneType2 = 0
    PhoneType3 = 0
End Sub

Private Sub loadPatientAlt()
    On Error GoTo lloadPatientAlt_Error

    If IsNull(PatientDemoAltTbl("PatientId")) Then
        PatientId = 0
    Else
        PatientId = PatientDemoAltTbl("PatientId")
    End If
    If IsNull(PatientDemoAltTbl("AltId")) Then
        AltId = 0
    Else
        AltId = PatientDemoAltTbl("AltId")
    End If
    If IsNull(PatientDemoAltTbl("Selected")) Then
        Selected = False
    Else
        Selected = PatientDemoAltTbl("Selected")
    End If
    If IsNull(PatientDemoAltTbl("Address")) Then
        Address = ""
    Else
        Address = PatientDemoAltTbl("Address")
    End If
    If IsNull(PatientDemoAltTbl("Suite")) Then
        Suite = ""
    Else
        Suite = PatientDemoAltTbl("Suite")
    End If
    If IsNull(PatientDemoAltTbl("City")) Then
        City = ""
    Else
        City = PatientDemoAltTbl("City")
    End If
    If IsNull(PatientDemoAltTbl("State")) Then
        State = ""
    Else
        State = PatientDemoAltTbl("State")
    End If
    If IsNull(PatientDemoAltTbl("Zip")) Then
        Zip = ""
    Else
        Zip = PatientDemoAltTbl("Zip")
    End If
    If IsNull(PatientDemoAltTbl("HomePhone")) Then
        HomePhone = ""
    Else
        HomePhone = PatientDemoAltTbl("HomePhone")
    End If
    If IsNull(PatientDemoAltTbl("WorkPhone")) Then
        WorkPhone = ""
    Else
        WorkPhone = PatientDemoAltTbl("WorkPhone")
    End If
    If IsNull(PatientDemoAltTbl("CellPhone")) Then
        CellPhone = ""
    Else
        CellPhone = PatientDemoAltTbl("CellPhone")
    End If
    If IsNull(PatientDemoAltTbl("Email")) Then
        Email = ""
    Else
        Email = PatientDemoAltTbl("Email")
    End If
    If IsNull(PatientDemoAltTbl("Pharmacy")) Then
        Pharmacy = 0
    Else
        Pharmacy = PatientDemoAltTbl("Pharmacy")
    End If
    'PhoneType
    If IsNull(PatientDemoAltTbl("PhoneType1")) Then
        PhoneType1 = 0
    Else
        PhoneType1 = PatientDemoAltTbl("PhoneType1")
        If PhoneType1 < 0 Then PhoneType1 = 0
    End If
    If IsNull(PatientDemoAltTbl("PhoneType2")) Then
        PhoneType2 = 0
    Else
        PhoneType2 = PatientDemoAltTbl("PhoneType2")
        If PhoneType2 < 0 Then PhoneType2 = 0
    End If
    If IsNull(PatientDemoAltTbl("PhoneType3")) Then
        PhoneType3 = 0
    Else
        PhoneType3 = PatientDemoAltTbl("PhoneType3")
        If PhoneType3 < 0 Then PhoneType3 = 0
    End If
    
    Exit Sub

lloadPatientAlt_Error:

    LogError "PatientDemoAlt", "loadPatientAlt", Err, Err.Description, , PatientId

End Sub

Public Function GetPatientAlt(fSelected As Boolean) As Boolean
Dim OrderString As String
    On Error GoTo DbErrorHandler
    
    OrderString = "SELECT * FROM PatientDemoAlt WHERE PatientId = " & PatientId & " "
    If Not AltId = 0 Then
        OrderString = OrderString & " and AltId = " & AltId
    End If
    If fSelected Then
        OrderString = OrderString & " and Selected < 0 "
    End If
    OrderString = OrderString & " and altid > 0 order by AltId "
    
    Set PatientDemoAltTbl = Nothing
    Set PatientDemoAltTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientDemoAltTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (PatientDemoAltTbl.EOF) Then
        Call InitPatientAlt
    Else
        Call loadPatientAlt
        GetPatientAlt = True
    End If
    Exit Function
DbErrorHandler:
        ModuleName = "GetPatientAlt"
        GetPatientAlt = False
        Call PinpointPRError(ModuleName, Err)
End Function

Public Function PutPatientAlt() As Boolean
Dim OrderString As String

    On Error GoTo DbErrorHandler
    
    OrderString = "insert into PatientDemoAlt (PatientId, altid, selected) values(" & _
                  PatientId & ", " & AltId & ", -1)"
    Set PatientDemoAltTbl = Nothing
    Set PatientDemoAltTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientDemoAltTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    
    OrderString = "SELECT * FROM PatientDemoAlt WHERE PatientId = " & _
                    PatientId & " and AltId = " & AltId
    Set PatientDemoAltTbl = Nothing
    Set PatientDemoAltTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientDemoAltTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    
    PatientDemoAltTbl("Selected") = Selected
    PatientDemoAltTbl("Address") = UCase(Trim(Address))
    PatientDemoAltTbl("Suite") = UCase(Trim(Suite))
    PatientDemoAltTbl("City") = UCase(Trim(City))
    PatientDemoAltTbl("State") = UCase(Trim(State))
    PatientDemoAltTbl("Zip") = UCase(Trim(Zip))
    PatientDemoAltTbl("HomePhone") = UCase(Trim(HomePhone))
    PatientDemoAltTbl("WorkPhone") = UCase(Trim(WorkPhone))
    PatientDemoAltTbl("CellPhone") = UCase(Trim(CellPhone))
    PatientDemoAltTbl("Email") = Trim(Email)
    PatientDemoAltTbl("Pharmacy") = Pharmacy
    'PhoneType
    PatientDemoAltTbl("PhoneType1") = PhoneType1
    PatientDemoAltTbl("PhoneType2") = PhoneType2
    PatientDemoAltTbl("PhoneType3") = PhoneType3
    PatientDemoAltTbl.Update
    PutPatientAlt = True
    Exit Function
    
DbErrorHandler:
    ModuleName = "PutPatientAlt"
    Call PinpointPRError(ModuleName, Err)

End Function

Public Function KillPatientAlt() As Boolean
Dim OrderString As String
    
    OrderString = "DELETE FROM PatientDemoAlt where PatientID = " & PatientId & " "
    Select Case AltId
    Case 1
        OrderString = OrderString & " and AltId > 0"
    Case -1
        OrderString = OrderString & " and AltId < 1"
    End Select
    
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Set PatientDemoAltTbl = Nothing
    Set PatientDemoAltTbl = CreateAdoRecordset
    
    Call PatientDemoAltTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    KillPatientAlt = True
    Exit Function

DbErrorHandler:
    ModuleName = "KillPatientAlt"
    Call PinpointPRError(ModuleName, Err)
End Function

Public Function SelectPatientAlt(ListItem As Integer) As Boolean
    On Error GoTo DbErrorHandler
    
    If (PatientDemoAltTbl.EOF) Or (ListItem < 1) Then
        Exit Function
    End If
    PatientDemoAltTbl.MoveFirst
    PatientDemoAltTbl.Move ListItem - 1
    SelectPatientAlt = True
    Call loadPatientAlt
    Exit Function
    
DbErrorHandler:
    ModuleName = "SelectPatientAlt"
    SelectPatientAlt = False
    PatientId = -256
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
End Function
