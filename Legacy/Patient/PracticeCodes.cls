VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeCodes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Practice Code Table Object Module
Option Explicit
Public ItemId As Long
Public ReferenceType As String
Public ReferenceCode As String
Public ReferenceAlternateCode As String
Public Exclusion As String
Public Signature As String
Public LetterLanguage As String
Public LetterLanguage1 As String
Public FormatMask As String
Public TestOrder As String
Public FollowUp As String
Public Rank As Long

Private ModuleName As String
Private CodeTbl As ADODB.Recordset
Private CodeTotal As Long
Private CodeNew As Boolean
Private PracticeTokenTbl As ADODB.Recordset

Private Sub Class_Initialize()
Set CodeTbl = CreateAdoRecordset
Call InitCode
End Sub

Public Function GetPracticeToken(AppName) As String
    On Error GoTo DbErrorHandler

    Dim OrderString As String

    OrderString = "SELECT TOP 1 Value FROM Model.ApplicationSettings WHERE Name  ='" + AppName + "'  "
    Set PracticeTokenTbl = Nothing
    Set PracticeTokenTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PracticeTokenTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)

    If Not (PracticeTokenTbl.EOF) Then
        PracticeTokenTbl.MoveLast
        GetPracticeToken = PracticeTokenTbl.Fields(0).Value
    End If

    Exit Function
DbErrorHandler:
    ModuleName = "DrFirst"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:

End Function

Private Sub Class_Terminate()
Call InitCode
Set CodeTbl = Nothing
End Sub

Private Sub InitCode()
CodeNew = True
ReferenceType = ""
ReferenceCode = ""
ReferenceAlternateCode = ""
Signature = ""
Exclusion = ""
LetterLanguage = ""
LetterLanguage1 = ""
FormatMask = ""
TestOrder = ""
FollowUp = ""
Rank = 0
End Sub

Private Sub LoadCode()
CodeNew = False

ItemId = CodeTbl("Id")

If (CodeTbl("ReferenceType") <> "") Then
    ReferenceType = CodeTbl("ReferenceType")
Else
    ReferenceType = ""
End If
If (CodeTbl("Code") <> "") Then
    ReferenceCode = CodeTbl("Code")
Else
    ReferenceCode = ""
End If
If (CodeTbl("AlternateCode") <> "") Then
    ReferenceAlternateCode = CodeTbl("AlternateCode")
Else
    ReferenceAlternateCode = ""
End If
If (CodeTbl("Signature") <> "") Then
    Signature = CodeTbl("Signature")
Else
    Signature = ""
End If
If (CodeTbl("Exclusion") <> "") Then
    Exclusion = CodeTbl("Exclusion")
Else
    Exclusion = ""
End If
If (CodeTbl("LetterTranslation") <> "") Then
    LetterLanguage = CodeTbl("LetterTranslation")
Else
    LetterLanguage = ""
End If
If (CodeTbl("OtherLtrTrans") <> "") Then
    LetterLanguage1 = CodeTbl("OtherLtrTrans")
Else
    LetterLanguage1 = ""
End If
If (CodeTbl("FormatMask") <> "") Then
    FormatMask = CodeTbl("FormatMask")
Else
    FormatMask = ""
End If
If (CodeTbl("TestOrder") <> "") Then
    TestOrder = CodeTbl("TestOrder")
Else
    TestOrder = ""
End If
If (CodeTbl("FollowUp") <> "") Then
    FollowUp = CodeTbl("FollowUp")
Else
    FollowUp = ""
End If
If (CodeTbl("Rank") <> "") Then
    Rank = CodeTbl("Rank")
Else
    Rank = 0
End If
End Sub

Private Function GetCode(IType As Integer, Optional Id As Long = 0) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCode = -1
CodeTotal = 0
OrderString = "Select * FROM PracticeCodeTable WHERE ReferenceType = '" + UCase(Trim(ReferenceType)) + "' "
If (ItemId > 0) Then
    OrderString = OrderString + "And Id = " + CStr(ItemId) + " "
Else
    If (Trim(ReferenceCode) <> "" And Id = 0) Then
    '    If (Len(Trim(ReferenceCode)) = 1) Then
    '        OrderString = OrderString + "And Code >= '" + Trim(ReferenceCode) + "' "
    '    Else
            If (IType = 3) Then
                OrderString = OrderString + "And Code >= '" + Trim(ReferenceCode) + "' And Code <= '" + Chr(Asc(Left(ReferenceCode, 1)) + 1) + "' "
            ElseIf (IType = 4) Then
                OrderString = OrderString + "And SUBSTRING(Code,0,CHARINDEX('-',Code,0)) = '" + Trim(ReferenceCode) + "' "
            Else
                OrderString = OrderString + "And Code = '" + Trim(ReferenceCode) + "' "
            End If
    End If
     If (Trim(ReferenceCode) <> "" And Id > 0) Then
    '    If (Len(Trim(ReferenceCode)) = 1) Then
    '        OrderString = OrderString + "And Code >= '" + Trim(ReferenceCode) + "' "
    '    Else
            If (IType = 3) Then
                OrderString = OrderString + "And Code >= '" + Trim(ReferenceCode) + "' And Code <= '" + Chr(Asc(Left(ReferenceCode, 1)) + 1) + "' "
            Else
                OrderString = OrderString + "And Code = '" + Trim(ReferenceCode) + "' And Id <> " + CStr(Id) + " "
            End If
    End If
    'End If
    If (Trim(ReferenceAlternateCode) <> "") Then
        If (Len(Trim(ReferenceAlternateCode)) = 1) And (ReferenceAlternateCode < " ") Then
            OrderString = OrderString + "And AlternateCode >= '" + Trim(ReferenceAlternateCode) + "' "
        Else
            OrderString = OrderString + "And AlternateCode = '" + Trim(ReferenceAlternateCode) + "' "
        End If
    End If
End If

If (IType = 1) Then
    OrderString = OrderString + "ORDER BY ReferenceType ASC, Rank ASC, Code DESC"
ElseIf (IType = 2) Then
    OrderString = OrderString + "ORDER BY ReferenceType ASC, Rank ASC, AlternateCode ASC, Code ASC "
Else
    OrderString = OrderString + "ORDER BY ReferenceType ASC, Rank ASC, Code ASC"
End If
Set CodeTbl = Nothing
Set CodeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CodeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CodeTbl.EOF) Then
    Call InitCode
    GetCode = -1
Else
    CodeTbl.MoveLast
    CodeTotal = CodeTbl.RecordCount
    GetCode = CodeTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    GetCode = -1
    ModuleName = "GetCode"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCodebyPaymentType(ByVal PaymentType As String) As Long
On Error GoTo DbErrorHandler
Dim i As Integer
Dim UTemp As String
Dim OrderString As String
GetCodebyPaymentType = -1
CodeTotal = 0
If (Trim(ReferenceType) <> "") Then
    OrderString = "Select * FROM PracticeCodeTable WHERE ReferenceType = '" + UCase(Trim(ReferenceType)) + "' "
    If (Trim(PaymentType) <> "") Then
        OrderString = OrderString + " And SUBSTRING(Code, LEN(Code), 1) = '" + Trim(PaymentType) + "' "
    End If
    OrderString = OrderString + "ORDER BY Code ASC "
    Set CodeTbl = Nothing
    Set CodeTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call CodeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (CodeTbl.EOF) Then
        Call InitCode
        GetCodebyPaymentType = -1
    Else
        CodeTbl.MoveLast
        CodeTotal = CodeTbl.RecordCount
        GetCodebyPaymentType = CodeTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    GetCodebyPaymentType = -1
    ModuleName = "GetCodebyPaymentType"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
Private Function CheckPracticeCode() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
CheckPracticeCode = False

If (Trim(ReferenceCode) <> "") Then
    If (UCase(Trim(ReferenceType)) = "PAYABLETYPE") Then
        OrderString = "Select * FROM PracticeCodeTable WHERE  SUBSTRING(Code, LEN(Code),1)=  '" + Mid$(Trim(ReferenceCode), Len(Trim(ReferenceCode)), 1) + "'  "
        OrderString = OrderString + " And  ReferenceType='" + Trim(ReferenceType) + "' and ReferenceType in('APPOINTMENT CATEGORY COLORS','CLINICALFOLLOWUPSTATUS','CONFIRMSTATUS','CONTACTTYPE','INSURERREF','LETTERSTATUS','MARITAL','NOTECATEGORY','PATIENTSTATUS','PAYABLETYPE','RELATIONSHIP','RESOURCETYPE','SUBFORMAT','SURGERYCASETYPE','SURGERYSTATUS','TRANSMITTYPE','VENDORTYPE') "
    Else
        OrderString = "Select * FROM PracticeCodeTable WHERE  SUBSTRING(Code,1,1)=  '" + Mid$(Trim(ReferenceCode), 1, 1) + "'  "
        OrderString = OrderString + " And  ReferenceType='" + Trim(ReferenceType) + "' and ReferenceType in('APPOINTMENT CATEGORY COLORS','CLINICALFOLLOWUPSTATUS','CONFIRMSTATUS','CONTACTTYPE','INSURERREF','LETTERSTATUS','MARITAL','NOTECATEGORY','PATIENTSTATUS','PAYABLETYPE','RELATIONSHIP','RESOURCETYPE','SUBFORMAT','SURGERYCASETYPE','SURGERYSTATUS','TRANSMITTYPE','VENDORTYPE') "
    End If
    
    Set CodeTbl = Nothing
    Set CodeTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call CodeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (CodeTbl.EOF) Then
       CheckPracticeCode = True
    End If
End If
    Exit Function
DbErrorHandler:
    ModuleName = "CheckPracticeCode"
 
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCodebyDiagRef() As Long
On Error GoTo DbErrorHandler
Dim i As Integer
Dim UTemp As String
Dim OrderString As String
GetCodebyDiagRef = -1
CodeTotal = 0
If (Trim(ReferenceType) <> "") Then
    OrderString = "Select * FROM PracticeCodeTable WHERE ReferenceType = '" + UCase(Trim(ReferenceType)) + "' "
    If (Trim(ReferenceAlternateCode) <> "") Then
        OrderString = OrderString + " And (AlternateCode Like '%" + Trim(ReferenceAlternateCode) + "%' "
        OrderString = OrderString + " Or AlternateCode = '*.*') "
    End If
    OrderString = OrderString + "ORDER BY Code ASC "
    Set CodeTbl = Nothing
    Set CodeTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call CodeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (CodeTbl.EOF) Then
        Call InitCode
        GetCodebyDiagRef = -1
    Else
        CodeTbl.MoveLast
        CodeTotal = CodeTbl.RecordCount
        GetCodebyDiagRef = CodeTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    GetCodebyDiagRef = -1
    ModuleName = "GetCode"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutCode() As Boolean
On Error GoTo DbErrorHandler
PutCode = True
If (CodeNew) Then
    CodeTbl.AddNew
End If
CodeTbl("ReferenceType") = UCase(Trim(ReferenceType))
CodeTbl("Code") = Trim(ReferenceCode)
CodeTbl("AlternateCode") = Trim(ReferenceAlternateCode)
CodeTbl("Exclusion") = Trim(Exclusion)
CodeTbl("Signature") = Trim(Signature)
CodeTbl("LetterTranslation") = Trim(LetterLanguage)
CodeTbl("OtherLtrTrans") = Trim(LetterLanguage1)
CodeTbl("FormatMask") = Trim(FormatMask)
CodeTbl("TestOrder") = Trim(TestOrder)
CodeTbl("FollowUp") = Trim(FollowUp)
CodeTbl("Rank") = Rank
CodeTbl.Update
If (CodeNew) Then
    CodeNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutCode"
    PutCode = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeCode() As Boolean
On Error GoTo DbErrorHandler
PurgeCode = True
CodeTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeCode"
    PurgeCode = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectCode(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectCode = False
If (CodeTbl.EOF) Or (ListItem < 1) Or (ListItem > CodeTotal) Then
    Exit Function
End If
CodeTbl.MoveFirst
CodeTbl.Move ListItem - 1
SelectCode = True
Call LoadCode
Exit Function
DbErrorHandler:
    ModuleName = "SelectCode"
    SelectCode = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCodeOrderbyAlternate() As Long
FindCodeOrderbyAlternate = GetCode(2)
End Function

Public Function FindCodeReverse() As Long
FindCodeReverse = GetCode(1)
End Function

Public Function FindCode(Optional ItemId As Long) As Long
FindCode = GetCode(0, ItemId)
End Function

Public Function FindCodeByPaymentType(ByVal Payment As String) As Long
FindCodeByPaymentType = GetCodebyPaymentType(Payment)
End Function

Public Function FindCodePartial() As Long
FindCodePartial = GetCode(3)
End Function

Public Function FindCodeByServiceMods() As Long
FindCodeByServiceMods = GetCode(4)
End Function
Public Function FindAlternateCode() As Long
ReferenceCode = ""
FindAlternateCode = GetCode(0)
End Function

Public Function FindCodebyDiagRef() As Long
FindCodebyDiagRef = GetCodebyDiagRef
End Function

Public Function ApplyCode() As Boolean
ApplyCode = PutCode()
End Function

Public Function DeleteCode() As Boolean
DeleteCode = PurgeCode()
End Function

Public Function DuplicatePracticeCode() As Boolean
DuplicatePracticeCode = CheckPracticeCode
End Function


Public Function FindCodeLike() As Long
On Error GoTo DbErrorHandler
Dim i As Integer
Dim UTemp As String
Dim OrderString As String
FindCodeLike = -1
CodeTotal = 0
OrderString = "Select * FROM PracticeCodeTable WHERE ReferenceType = '" + UCase(Trim(ReferenceType)) + "' " & _
    " and '" & ReferenceCode & "' like code + '%'"
Set CodeTbl = Nothing
Set CodeTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CodeTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CodeTbl.EOF) Then
    Call InitCode
    FindCodeLike = -1
Else
    CodeTbl.MoveLast
    CodeTotal = CodeTbl.RecordCount
    FindCodeLike = CodeTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    FindCodeLike = -1
    ModuleName = "FindCodeLike"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:

End Function
