VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DiagnosisMasterPrimary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Primary Diagnosis Object Module
Option Explicit
Public PrimaryId                       As Long
Public PrimarySystem                   As String
Public PrimaryDiagnosis                As String
Public PrimaryRank                     As Long
Public PrimaryLevel                    As Long
Public PrimaryNextLevelDiagnosis       As String
Public PrimaryName                     As String
Public PrimaryLingo                    As String
Public PrimaryImage1                   As String
Public PrimaryImage2                   As String
Public PrimaryNextLevelOn              As Boolean
Public PrimaryBilling                  As Boolean
Public PrimaryUnspecifiedOn            As Boolean
Public PrimaryDiscipline               As String
Public PrimaryShortName                As String
Public PrimaryExclusion                As String
Public PrimaryPermanent                As Boolean
Public PrimaryImpression               As Boolean
Public PrimaryLanguageTranslation      As String
Public PrimaryLanguageTranslation1     As String
Private ModuleName                     As String
Private PrimaryTotal                   As Long
Private PrimaryTbl                     As ADODB.Recordset
Private PrimaryNew                     As Boolean


Public Function ApplyPrimary() As Boolean

    ApplyPrimary = PutPrimary
End Function


Private Sub Class_Initialize()

    Set PrimaryTbl = New ADODB.Recordset
    PrimaryTbl.CursorLocation = adUseClient
    InitPrimary
End Sub


Private Sub Class_Terminate()

    InitPrimary
    Set PrimaryTbl = Nothing
End Sub


Public Function DeletePrimary() As Boolean

    DeletePrimary = PurgePrimary
End Function


Public Function FindPrimary() As Long

    FindPrimary = GetAllPrimary(0)
End Function

Public Function FindPrimaryFiltered() As Long

    FindPrimaryFiltered = GetAllPrimaryFitered
End Function


Public Function FindPrimarybyAnyDiagnosisDistinct() As Long

    FindPrimarybyAnyDiagnosisDistinct = GetAllPrimarybyDiagnosisDistinct(1)
End Function


Public Function FindPrimarybyDiagnosis() As Long

    If LenB(Trim$(PrimarySystem)) Then
        FindPrimarybyDiagnosis = GetAllPrimarybyDiagnosis(0)
    Else 'LENB(TRIM$(PRIMARYSYSTEM)) = FALSE/0
        FindPrimarybyDiagnosis = GetAllPrimarybyDiagnosis(1)
    End If
End Function


Public Function FindPrimarybyDiagnosisDistinct() As Long

    FindPrimarybyDiagnosisDistinct = GetAllPrimarybyDiagnosisDistinct(0)
End Function


Public Function FindPrimaryOrderBySystem() As Long

    FindPrimaryOrderBySystem = GetAllPrimary(2)
End Function


Public Function FindRealPrimarybyDiagnosis() As Long
Dim i    As Long
Dim j    As Long
Dim Temp As String

    Temp = PrimaryName
    i = GetRealPrimarybyDiagnosis(0)
    FindRealPrimarybyDiagnosis = i
    If i > 1 Then
        If LenB(Trim$(Temp)) Then
            If (Left$(PrimaryName, 1) <> "*") Then
                For j = 1 To i
                    If SelectPrimary(j) Then
'            If (UCase(Temp) <> UCase(Left(PrimaryName, Len(Temp)))) Then
                        If (InStrPS(UCase$(PrimaryName), UCase$(Temp)) <> 0) Then
                            If (Asc(Mid$(Temp, 1, 1)) < 32) Then
                                FindRealPrimarybyDiagnosis = i
                            Else 'NOT (ASC(MID$(TEMP,...
                                FindRealPrimarybyDiagnosis = j - 1
                            End If
                            Exit For
                        End If
                    End If
                Next j
            End If
        End If
    End If
End Function


Public Function FindRealPrimarybyDiagnosisDistinct() As Long

Dim i    As Long
Dim j    As Long
Dim Temp As String

    Temp = PrimaryName
    i = GetRealPrimarybyDiagnosis(1)
    FindRealPrimarybyDiagnosisDistinct = i
    If LenB(Trim$(PrimaryName)) Then
        For j = 1 To i
            If SelectPrimary(j) Then
                If (UCase$(Temp) <> UCase$(Left$(PrimaryName, Len(Temp)))) Then
                    FindRealPrimarybyDiagnosisDistinct = j - 1
                    Exit For
                End If
            End If
        Next j
    End If
End Function

Public Function FindRealPrimarybyDiagnosisFiltered() As Long
Dim i    As Long
Dim j    As Long
Dim Temp As String

    Temp = PrimaryName
    i = GetRealPrimarybyDiagnosisFiltered(0)
    FindRealPrimarybyDiagnosisFiltered = i
    If i > 1 Then
        If LenB(Trim$(Temp)) Then
            If (Left$(PrimaryName, 1) <> "*") Then
                For j = 1 To i
                    If SelectPrimary(j) Then
'            If (UCase(Temp) <> UCase(Left(PrimaryName, Len(Temp)))) Then
                        If (InStrPS(UCase$(PrimaryName), UCase$(Temp)) <> 0) Then
                            If (Asc(Mid$(Temp, 1, 1)) < 32) Then
                                FindRealPrimarybyDiagnosisFiltered = i
                            Else 'NOT (ASC(MID$(TEMP,...
                                FindRealPrimarybyDiagnosisFiltered = j - 1
                            End If
                            Exit For
                        End If
                    End If
                Next j
            End If
        End If
    End If
End Function

Public Function FindRealPrimarybyDiagnosisNumeric() As Long

Dim i    As Long
Dim j    As Long
Dim Temp As String

    Temp = PrimaryNextLevelDiagnosis
    i = GetRealPrimarybyDiagnosis(1)
    FindRealPrimarybyDiagnosisNumeric = i
    For j = 1 To i
        If SelectPrimary(j) Then
            If (Temp <> Left$(PrimaryNextLevelDiagnosis, Len(Temp))) Then
                FindRealPrimarybyDiagnosisNumeric = j - 1
                Exit For
            End If
        End If
    Next j
End Function

Public Function FindRealPrimarybyDiagnosisNumericFiltered() As Long

Dim i    As Long
Dim j    As Long
Dim Temp As String

    Temp = PrimaryNextLevelDiagnosis
    i = GetRealPrimarybyDiagnosisFiltered(1)
    FindRealPrimarybyDiagnosisNumericFiltered = i
    For j = 1 To i
        If SelectPrimary(j) Then
            If (Temp <> Left$(PrimaryNextLevelDiagnosis, Len(Temp))) Then
                FindRealPrimarybyDiagnosisNumericFiltered = j - 1
                Exit For
            End If
        End If
    Next j
End Function


Private Function GetAllPrimary(ByVal IType As Long) As Long

Dim i           As Long
Dim OrderString As String
Dim Ref         As String
Dim asInArray() As String

    On Error GoTo DbErrorHandler
    PrimaryTotal = 0
    GetAllPrimary = -1
    If LenB(Trim$(PrimaryNextLevelDiagnosis)) = 0 Then
        If LenB(Trim$(PrimarySystem)) = 0 Then
            If LenB(Trim$(PrimaryDiagnosis)) = 0 Then
                Exit Function
            End If
        End If
    End If
    OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE "
    If LenB(Trim$(PrimarySystem)) Then
        If Len(PrimarySystem) = 1 Then
            OrderString = OrderString & "MedicalSystem = '" & UCase$(Trim$(PrimarySystem)) & "' "
        Else
            For i = 1 To Len(PrimarySystem)
                If i = 1 Then
                    OrderString = OrderString & "(MedicalSystem = '" & UCase$(Mid$(PrimarySystem, i, 1)) & "' Or "
                ElseIf (i = Len(PrimarySystem)) Then
                    OrderString = OrderString & "MedicalSystem = '" & UCase$(Mid$(PrimarySystem, i, 1)) & "') "
                Else
                    OrderString = OrderString & "MedicalSystem = '" & UCase$(Mid$(PrimarySystem, i, 1)) & "' Or  "
                End If
            Next i
        End If
        Ref = "And "
    End If
    If LenB(Trim$(PrimaryDiagnosis)) Then
        OrderString = OrderString & Ref & "Diagnosis = '" & UCase$(Trim$(PrimaryDiagnosis)) & "' "
        Ref = "And "
    ElseIf (Trim$(PrimaryNextLevelDiagnosis) <> "") Then
        If IType = 2 Then
            asInArray = Split(PrimaryNextLevelDiagnosis, ",")
            OrderString = OrderString & Ref & "DiagnosisNextLevel in ('" & asInArray(0) & "'"
            For i = 1 To UBound(asInArray)
                OrderString = OrderString & ", '" & asInArray(i) & "'"
            Next i
            OrderString = OrderString & ") "
        Else
            OrderString = OrderString & Ref & "DiagnosisNextLevel = '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
        End If
        Ref = "And "
    End If
    If LenB(Trim$(PrimaryDiscipline)) Then
        OrderString = OrderString & Ref & "Discipline = '" & UCase$(Trim$(PrimaryDiscipline)) & "' "
    End If
    If PrimaryRank > 0 Then
        OrderString = OrderString & "And DiagnosisRank =" & Str$(PrimaryRank) & " "
    ElseIf (PrimaryRank = -1) Then
        OrderString = OrderString & "And DiagnosisRank = 0 "
    Else
        OrderString = OrderString & "And DiagnosisRank > 0 "
    End If
    If IType = 0 Then
        If PrimaryLevel > 0 Then
            OrderString = OrderString & "And DiagnosisDrillDownLevel =" & Str$(PrimaryLevel) & " "
        ElseIf (PrimaryLevel = -1) Then
            OrderString = OrderString & "And DiagnosisDrillDownLevel = 0 "
        Else
            OrderString = OrderString & "And DiagnosisDrillDownLevel > 0 "
        End If
    Else
        If PrimaryLevel > 0 Then
            OrderString = OrderString & "And DiagnosisDrillDownLevel =" & Str$(PrimaryLevel) & " "
        End If
    End If
    If PrimaryBilling Then
        OrderString = OrderString & "And (Billable = 'T' Or Billable = 'U')"
    End If
    If IType = 2 Then
        OrderString = OrderString & "ORDER BY MedicalSystem, DiagnosisNextLevel"
    Else
        OrderString = OrderString & "ORDER BY MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC, Diagnosis ASC "
    End If
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        GetAllPrimary = -1
    Else
        PrimaryTbl.MoveLast
        PrimaryTotal = PrimaryTbl.RecordCount
        GetAllPrimary = PrimaryTotal
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetAllPrimary"
    GetAllPrimary = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllPrimaryFitered() As Long
Dim OrderString As String

    On Error GoTo DbErrorHandler
    PrimaryTotal = 0
    GetAllPrimaryFitered = -1
    If LenB(Trim$(PrimaryNextLevelDiagnosis)) = 0 Then
        If LenB(Trim$(PrimarySystem)) = 0 Then
            If LenB(Trim$(PrimaryDiagnosis)) = 0 Then
                Exit Function
            End If
        End If
    End If
    OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE "
    OrderString = OrderString & " MedicalSystem = '" & UCase$(Trim$(PrimarySystem)) & "' "
    OrderString = OrderString & " And not (Diagnosis = '' And DiagnosisRank = 1 And DiagnosisDrillDownLevel = 2)"
    OrderString = OrderString & " And not (DiagnosisRank = 113 and DiagnosisDrillDownLevel = 5) "
    OrderString = OrderString & " ORDER BY MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC, Diagnosis ASC "
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        GetAllPrimaryFitered = -1
    Else
        PrimaryTbl.MoveLast
        PrimaryTotal = PrimaryTbl.RecordCount
        GetAllPrimaryFitered = PrimaryTotal
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetAllPrimaryFitered"
    GetAllPrimaryFitered = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetAllPrimarybyDiagnosis(ByVal IType As Long) As Long

Dim SortBy      As String
Dim WhereString As String
Dim OrderString As String

    On Error GoTo DbErrorHandler
    PrimaryTotal = 0
    GetAllPrimarybyDiagnosis = -1
    If LenB(Trim$(PrimaryNextLevelDiagnosis)) = 0 Then
        If LenB(Trim$(PrimaryDiagnosis)) = 0 Then
            Exit Function
        End If
    End If
    SortBy = "ORDER BY Diagnosis ASC, MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    If IType = 1 Then
        SortBy = "ORDER BY Diagnosis ASC, MedicalSystem ASC "
    End If
    OrderString = "SELECT * FROM PrimaryDiagnosisTable "
    If Len(Trim$(PrimaryDiagnosis)) = 1 Then
        WhereString = "WHERE Diagnosis >= '" & UCase$(Trim$(PrimaryDiagnosis)) & "' "
    ElseIf (Len(Trim$(PrimaryNextLevelDiagnosis)) = 1) Then
        WhereString = "WHERE DiagnosisNextLevel >= '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
    ElseIf (Trim$(PrimaryDiagnosis) <> "") Then
        WhereString = "WHERE Diagnosis = '" & UCase$(Trim$(PrimaryDiagnosis)) & "' "
    ElseIf (Trim$(PrimaryNextLevelDiagnosis) <> "") Then
        WhereString = "WHERE DiagnosisNextLevel = '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
    End If
    If LenB(Trim$(PrimarySystem)) Then
        WhereString = WhereString & "And MedicalSystem = '" & UCase$(Trim$(PrimarySystem)) & "' "
    End If
    If LenB(Trim$(PrimaryDiscipline)) Then
        WhereString = WhereString & "And Discipline = '" & UCase$(Trim$(PrimaryDiscipline)) & "' "
    End If
    If PrimaryRank > 0 Then
        WhereString = WhereString & "And DiagnosisRank =" & Str$(PrimaryRank) & " "
    Else
        WhereString = WhereString & "And DiagnosisRank > 0 "
    End If
    If PrimaryLevel > 0 Then
        WhereString = WhereString & "And DiagnosisDrillDownLevel =" & Str$(PrimaryLevel) & " "
    ElseIf (PrimaryLevel = -1) Then
        WhereString = WhereString & "And DiagnosisDrillDownLevel = 0 "
    Else
        WhereString = WhereString & "And DiagnosisDrillDownLevel >= 0 "
    End If
    If PrimaryBilling Then
        WhereString = WhereString & "And (Billable = 'T' Or Billable = 'U')"
    End If
    OrderString = OrderString + WhereString + SortBy
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        GetAllPrimarybyDiagnosis = -1
    Else
        PrimaryTbl.MoveLast
        PrimaryTotal = PrimaryTbl.RecordCount
        GetAllPrimarybyDiagnosis = PrimaryTotal
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetAllPrimarybyDiagnosis"
    GetAllPrimarybyDiagnosis = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetAllPrimarybyDiagnosisDistinct(ByVal IType As Long) As Long

Dim i           As Long
Dim Ref         As String
Dim SortBy      As String
Dim WhereString As String
Dim OrderString As String

    On Error GoTo DbErrorHandler
    PrimaryTotal = 0
    GetAllPrimarybyDiagnosisDistinct = -1
    If LenB(Trim$(PrimarySystem)) = 0 Then
        If LenB(Trim$(PrimaryName)) = 0 Then
            If LenB(Trim$(PrimaryNextLevelDiagnosis)) = 0 Then
                Exit Function
            End If
        End If
    End If
    OrderString = "SELECT DISTINCT DiagnosisNextLevel, DiagnosisName, ReviewSystemLingo FROM PrimaryDiagnosisTable WHERE "
    SortBy = "ORDER BY DiagnosisNextLevel ASC "
    If LenB(Trim$(PrimaryNextLevelDiagnosis)) Then
        WhereString = WhereString & "DiagnosisNextLevel >= '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(PrimarySystem)) Then
        If Len(PrimarySystem) = 1 Then
            OrderString = OrderString & "MedicalSystem = '" & UCase$(Trim$(PrimarySystem)) & "' "
        Else
            For i = 1 To Len(PrimarySystem)
                If i = 1 Then
                    OrderString = OrderString & "(MedicalSystem = '" & UCase$(Mid$(PrimarySystem, i, 1)) & "' Or "
                ElseIf (i = Len(PrimarySystem)) Then 'NOT I...
                    OrderString = OrderString & "MedicalSystem = '" & UCase$(Mid$(PrimarySystem, i, 1)) & "') "
                Else
                    OrderString = OrderString & "MedicalSystem = '" & UCase$(Mid$(PrimarySystem, i, 1)) & "' Or  "
                End If
            Next i
        End If
        Ref = "And "
    End If
    If LenB(Trim$(PrimaryDiscipline)) Then
        WhereString = WhereString + Ref & "Discipline = '" & UCase$(Trim$(PrimaryDiscipline)) & "' "
        Ref = "And "
    End If
    If LenB(Trim$(PrimaryName)) Then
        If IType = 1 Then
            WhereString = WhereString + Ref & "((DiagnosisName >= '" & Trim$(PrimaryName) & "' And DiagnosisName < '" & Chr$(Asc(Left$(PrimaryName, 1)) + 1) & "') Or " & _
                           "(ReviewSystemLingo >= '" & Trim$(PrimaryName) & "' And ReviewSystemLingo < '" & Chr$(Asc(Left$(PrimaryName, 1)) + 1) & "')) "
            Ref = "And "
            SortBy = "ORDER BY ReviewSystemLingo ASC "
        Else
            If (Left$(PrimaryName, 1) < "Z") Then
                WhereString = WhereString + Ref & "(DiagnosisName >= '" & Trim$(PrimaryName) & "' And DiagnosisName < '" & Chr$(Asc(Left$(PrimaryName, 1)) + 1) & "') "
            Else
                WhereString = WhereString + Ref & "DiagnosisName >= '" & Trim$(PrimaryName) & "' "
            End If
            Ref = "And "
            SortBy = "ORDER BY DiagnosisName ASC "
        End If
    End If
    If PrimaryBilling Then
        WhereString = WhereString & "And (Billable = 'T' Or Billable = 'U')"
    End If
    OrderString = OrderString + WhereString + SortBy
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        GetAllPrimarybyDiagnosisDistinct = -1
    Else
        PrimaryTbl.MoveLast
        PrimaryTotal = PrimaryTbl.RecordCount
        GetAllPrimarybyDiagnosisDistinct = PrimaryTotal
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetAllPrimarybyDiagnosisDistinct"
    GetAllPrimarybyDiagnosisDistinct = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetPrimary() As Boolean

Dim OrderString As String

    On Error GoTo DbErrorHandler
    GetPrimary = True
    OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE PrimaryDrillId =" & Trim$(Str$(PrimaryId)) & " "
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        InitPrimary
    Else
        LoadPrimary 0
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetPrimary"
    GetPrimary = False
    PrimaryId = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetRealPrimarybyDiagnosis(ByVal IType As Long) As Long
Dim SortBy      As String
Dim OrderString As String

    On Error GoTo DbErrorHandler
    PrimaryTotal = 0
    GetRealPrimarybyDiagnosis = -1
    If LenB(Trim$(PrimaryLingo)) = 0 Then
        If LenB(Trim$(PrimaryName)) = 0 Then
            If LenB(Trim$(PrimaryDiagnosis)) = 0 Then
                If LenB(Trim$(PrimaryNextLevelDiagnosis)) = 0 Then
                    Exit Function
                End If
            End If
        End If
    End If
    SortBy = "ORDER BY MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    If Len(Trim$(PrimaryDiagnosis)) = 1 Then
        OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE Diagnosis < 'X' And Diagnosis >= '" & UCase$(PrimaryDiagnosis) & "' "
        If IType <> 1 Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName < 'X' And DiagnosisName >= '" & UCase$(PrimaryDiagnosis) & "' "
            SortBy = "ORDER BY DiagnosisName ASC "
        Else
            SortBy = "ORDER BY Diagnosis ASC "
        End If
    ElseIf (Len(Trim$(PrimaryNextLevelDiagnosis)) = 1) Then
        OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel < 'X' And DiagnosisNextLevel >= '" & UCase$(PrimaryNextLevelDiagnosis) & "' "
        If IType <> 1 Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName < 'X' And DiagnosisName >= '" & UCase$(PrimaryNextLevelDiagnosis) & "' "
            SortBy = "ORDER BY DiagnosisName ASC "
        Else
            SortBy = "ORDER BY DiagnosisNextLevel ASC "
        End If
    ElseIf (Trim$(PrimaryDiagnosis) <> "") Then
        OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE Diagnosis >= '" & UCase$(Trim$(PrimaryDiagnosis)) & "' "
        SortBy = "ORDER BY Diagnosis ASC, MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    ElseIf (Trim$(PrimaryNextLevelDiagnosis) <> "") Then
        If IType <> 1 Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel = '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel >= '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
        End If
        SortBy = "ORDER BY DiagnosisNextLevel ASC, MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    ElseIf (Trim$(PrimaryName) <> "") Then
        If (Left$(PrimaryName, 1) = "*") Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName Like '%" & LCase$(Trim$(Mid$(PrimaryName, 2, Len(PrimaryName) - 1))) & "%' "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName >= '" & UCase$(Trim$(PrimaryName)) & "' "
        End If
        If IType <> 1 Then
            SortBy = "ORDER BY DiagnosisName ASC "
        Else
            SortBy = "ORDER BY DiagnosisNextLevel ASC "
        End If
        SortBy = "ORDER BY DiagnosisName ASC "
    ElseIf (Trim$(PrimaryLingo) <> "") Then
        If (Left$(PrimaryLingo, 1) = "*") Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE ReviewSystemLingo Like '%" & LCase$(Trim$(Mid$(PrimaryLingo, 2, Len(PrimaryLingo) - 1))) & "%' "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE ReviewSystemLingo >= '" & UCase$(Trim$(PrimaryLingo)) & "' "
        End If
        If IType <> 1 Then
            SortBy = "ORDER BY ReviewSystemLingo ASC "
        Else
            SortBy = "ORDER BY DiagnosisNextLevel ASC "
        End If
        SortBy = "ORDER BY ReviewSystemLingo ASC "
    End If
    If LenB(Trim$(PrimarySystem)) Then
        OrderString = OrderString & "And MedicalSystem = '" & UCase$(Trim$(PrimarySystem)) & "' "
    End If
    If PrimaryRank > 0 Then
        OrderString = OrderString & "And DiagnosisRank =" & Str$(PrimaryRank) & " "
    Else
        OrderString = OrderString & "And DiagnosisRank > 0 "
    End If
    If PrimaryLevel > 0 Then
        OrderString = OrderString & "And DiagnosisDrillDownLevel =" & Str$(PrimaryLevel) & " "
    ElseIf (PrimaryLevel = -1) Then
        OrderString = OrderString & "And DiagnosisDrillDownLevel = 0 "
    Else
        OrderString = OrderString & "And DiagnosisDrillDownLevel >= 0 "
    End If
    If LenB(Trim$(PrimaryDiscipline)) Then
        OrderString = OrderString & "And Discipline = '" & Trim$(PrimaryDiscipline) & "' "
    End If
    If PrimaryBilling Then
        OrderString = OrderString & "And Billable = 'T' "
    End If
    OrderString = OrderString + SortBy
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        GetRealPrimarybyDiagnosis = -1
    Else
        PrimaryTbl.MoveLast
        PrimaryTotal = PrimaryTbl.RecordCount
        GetRealPrimarybyDiagnosis = PrimaryTotal
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetRealPrimarybyDiagnosis"
    GetRealPrimarybyDiagnosis = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetRealPrimarybyDiagnosisFiltered(ByVal IType As Long) As Long
Dim SortBy      As String
Dim OrderString As String

    On Error GoTo DbErrorHandler
    PrimaryTotal = 0
    GetRealPrimarybyDiagnosisFiltered = -1
    If LenB(Trim$(PrimaryLingo)) = 0 Then
        If LenB(Trim$(PrimaryName)) = 0 Then
            If LenB(Trim$(PrimaryDiagnosis)) = 0 Then
                If LenB(Trim$(PrimaryNextLevelDiagnosis)) = 0 Then
                    Exit Function
                End If
            End If
        End If
    End If
    SortBy = "ORDER BY MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    
    
    If Len(Trim$(PrimaryDiagnosis)) = 1 Then
        If IType = 1 Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE Diagnosis < 'X' And Diagnosis >= '" & UCase$(PrimaryDiagnosis) & "' "
            SortBy = "ORDER BY Diagnosis ASC "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName < 'X' And DiagnosisName >= '" & UCase$(PrimaryDiagnosis) & "' "
            SortBy = "ORDER BY DiagnosisName ASC "
        End If
    ElseIf (Len(Trim$(PrimaryNextLevelDiagnosis)) = 1) Then
        If IType = 1 Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel < 'X' And DiagnosisNextLevel >= '" & UCase$(PrimaryNextLevelDiagnosis) & "' "
            SortBy = "ORDER BY DiagnosisNextLevel ASC "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName < 'X' And DiagnosisName >= '" & UCase$(PrimaryNextLevelDiagnosis) & "' "
            SortBy = "ORDER BY DiagnosisName ASC "
        End If
    ElseIf (Trim$(PrimaryDiagnosis) <> "") Then
        OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE Diagnosis >= '" & UCase$(Trim$(PrimaryDiagnosis)) & "' "
        SortBy = "ORDER BY Diagnosis ASC, MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    ElseIf (Trim$(PrimaryNextLevelDiagnosis) <> "") Then
        If IType <> 1 Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel = '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel >= '" & UCase$(Trim$(PrimaryNextLevelDiagnosis)) & "' "
        End If
        SortBy = "ORDER BY DiagnosisNextLevel ASC, MedicalSystem ASC, DiagnosisDrillDownLevel ASC, DiagnosisRank ASC "
    ElseIf (Trim$(PrimaryName) <> "") Then
        If (Left$(PrimaryName, 1) = "*") Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName Like '%" & LCase$(Trim$(Mid$(PrimaryName, 2, Len(PrimaryName) - 1))) & "%' "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisName >= '" & UCase$(Trim$(PrimaryName)) & "' "
        End If
        If IType <> 1 Then
            SortBy = "ORDER BY DiagnosisName ASC "
        Else
            SortBy = "ORDER BY DiagnosisNextLevel ASC "
        End If
        SortBy = "ORDER BY DiagnosisName ASC "
    ElseIf (Trim$(PrimaryLingo) <> "") Then
        If (Left$(PrimaryLingo, 1) = "*") Then
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE ReviewSystemLingo Like '%" & LCase$(Trim$(Mid$(PrimaryLingo, 2, Len(PrimaryLingo) - 1))) & "%' "
        Else
            OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE ReviewSystemLingo >= '" & UCase$(Trim$(PrimaryLingo)) & "' "
        End If
        SortBy = "ORDER BY ReviewSystemLingo ASC "
    End If
    
    OrderString = OrderString & " And Not ( DiagnosisRank = 113 And DiagnosisDrillDownLevel = 5)"
    
    OrderString = OrderString + SortBy
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.EOF Then
        GetRealPrimarybyDiagnosisFiltered = -1
    Else
        PrimaryTbl.MoveLast
        PrimaryTotal = PrimaryTbl.RecordCount
        GetRealPrimarybyDiagnosisFiltered = PrimaryTotal
    End If
Exit Function


DbErrorHandler:
    ModuleName = "GetRealPrimarybyDiagnosisFiltered"
    GetRealPrimarybyDiagnosisFiltered = -256
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Private Sub InitPrimary()

    PrimaryNew = True
    PrimarySystem = vbNullString
    PrimaryDiagnosis = vbNullString
    PrimaryName = vbNullString
    PrimaryLingo = vbNullString
    PrimaryRank = 0
    PrimaryLevel = 0
    PrimaryNextLevelDiagnosis = vbNullString
    PrimaryImage1 = vbNullString
    PrimaryImage2 = vbNullString
    PrimaryDiscipline = vbNullString
    PrimaryShortName = vbNullString
    PrimaryExclusion = vbNullString
    PrimaryLanguageTranslation = vbNullString
    PrimaryLanguageTranslation1 = vbNullString
    PrimaryBilling = False
    PrimaryUnspecifiedOn = False
    PrimaryNextLevelOn = False
    PrimaryPermanent = False
    PrimaryImpression = False
End Sub


Public Function IsNextLevelOn() As Boolean

    IsNextLevelOn = False
    If GetAllPrimary(1) > 0 Then
        IsNextLevelOn = True
    End If
End Function


Private Sub LoadPrimary(ByVal IType As Long)


    PrimaryNew = False
    PrimaryUnspecifiedOn = False
    If LenB(PrimaryTbl("DiagnosisNextLevel")) Then
        PrimaryNextLevelDiagnosis = PrimaryTbl("DiagnosisNextLevel")
    Else
        PrimaryNextLevelDiagnosis = vbNullString
    End If
    If LenB(PrimaryTbl("DiagnosisName")) Then
        PrimaryName = PrimaryTbl("DiagnosisName")
    Else
        PrimaryName = vbNullString
    End If
    If LenB(PrimaryTbl("ReviewSystemLingo")) Then
        PrimaryLingo = PrimaryTbl("ReviewSystemLingo")
    Else
        PrimaryLingo = vbNullString
    End If
    If Not IType = 1 Then
        PrimaryId = PrimaryTbl("PrimaryDrillId")
        If LenB(PrimaryTbl("ReviewSystemLingo")) Then
            PrimaryLingo = PrimaryTbl("ReviewSystemLingo")
        Else
            PrimaryLingo = vbNullString
        End If
        If LenB(PrimaryTbl("MedicalSystem")) Then
            PrimarySystem = PrimaryTbl("MedicalSystem")
        Else
            PrimarySystem = vbNullString
        End If
        If LenB(PrimaryTbl("Diagnosis")) Then
            PrimaryDiagnosis = PrimaryTbl("Diagnosis")
        Else
            PrimaryDiagnosis = vbNullString
        End If
        If LenB(PrimaryTbl("DiagnosisRank")) Then
            PrimaryRank = PrimaryTbl("DiagnosisRank")
        Else
            PrimaryRank = 0
        End If
        If LenB(PrimaryTbl("DiagnosisDrillDownLevel")) Then
            PrimaryLevel = PrimaryTbl("DiagnosisDrillDownLevel")
        Else
            PrimaryLevel = 0
        End If
        If LenB(PrimaryTbl("ReviewSystemLingo")) Then
            PrimaryLingo = PrimaryTbl("ReviewSystemLingo")
        Else
            PrimaryLingo = vbNullString
        End If
        If LenB(PrimaryTbl("ShortName")) Then
            PrimaryShortName = PrimaryTbl("ShortName")
        Else
            PrimaryShortName = vbNullString
        End If
        If LenB(PrimaryTbl("Image1")) Then
            PrimaryImage1 = PrimaryTbl("Image1")
        Else
            PrimaryImage1 = vbNullString
        End If
        If LenB(PrimaryTbl("Image2")) Then
            PrimaryImage2 = PrimaryTbl("Image2")
        Else
            PrimaryImage2 = vbNullString
        End If
        If LenB(PrimaryTbl("Discipline")) Then
            PrimaryDiscipline = PrimaryTbl("Discipline")
        Else
            PrimaryDiscipline = vbNullString
        End If
        If LenB(PrimaryTbl("Billable")) Then
            If PrimaryTbl("Billable") = "T" Then
                PrimaryBilling = True
            ElseIf (PrimaryTbl("Billable") = "U") Then
                PrimaryBilling = True
                PrimaryUnspecifiedOn = True
            Else
                PrimaryBilling = False
            End If
        Else
            PrimaryBilling = False
        End If
        If LenB(PrimaryTbl("NextLevel")) Then
            PrimaryNextLevelOn = PrimaryTbl("NextLevel") = "T"
        Else
            PrimaryNextLevelOn = False
        End If
        If LenB(PrimaryTbl("PermanentCondition")) Then
            PrimaryPermanent = PrimaryTbl("PermanentCondition") = "T"
        Else
            PrimaryPermanent = False
        End If
        If LenB(PrimaryTbl("ImpressionOn")) Then
            PrimaryImpression = PrimaryTbl("ImpressionOn") = "T"
        Else
            PrimaryImpression = False
        End If
        If LenB(PrimaryTbl("Exclusion")) Then
            PrimaryExclusion = PrimaryTbl("Exclusion")
        Else
            PrimaryExclusion = vbNullString
        End If
        If LenB(PrimaryTbl("LetterTranslation")) Then
            PrimaryLanguageTranslation = PrimaryTbl("LetterTranslation")
        Else
            PrimaryLanguageTranslation = vbNullString
        End If
        If LenB(PrimaryTbl("OtherLtrTrans")) Then
            PrimaryLanguageTranslation1 = PrimaryTbl("OtherLtrTrans")
        Else
            PrimaryLanguageTranslation1 = vbNullString
        End If
    End If
End Sub


Private Function PurgePrimary() As Boolean

    On Error GoTo DbErrorHandler
    PurgePrimary = True
    If Not (PrimaryNew) Then
        PrimaryTbl.Delete
    End If
Exit Function


DbErrorHandler:
    PurgePrimary = False
    ModuleName = "PurgePrimary"
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Private Function PutPrimary() As Boolean

    On Error GoTo DbErrorHandler
    PutPrimary = True
    If PrimaryNew Then
        PrimaryTbl.AddNew
    End If
    PrimaryTbl("MedicalSystem") = UCase$(Trim$(PrimarySystem))
    PrimaryTbl("Diagnosis") = UCase$(Trim$(PrimaryDiagnosis))
    PrimaryTbl("DiagnosisRank") = PrimaryRank
    PrimaryTbl("DiagnosisDrillDownLevel") = PrimaryLevel
    PrimaryTbl("DiagnosisNextLevel") = UCase$(Trim$(PrimaryNextLevelDiagnosis))
    PrimaryTbl("DiagnosisName") = Trim$(PrimaryName)
    PrimaryTbl("ReviewSystemLingo") = Trim$(PrimaryLingo)
    PrimaryTbl("Image1") = UCase$(Trim$(PrimaryImage1))
    PrimaryTbl("Image2") = UCase$(Trim$(PrimaryImage2))
    PrimaryTbl("Discipline") = UCase$(Trim$(PrimaryDiscipline))
    PrimaryTbl("ShortName") = UCase$(Trim$(PrimaryShortName))
    PrimaryTbl("Exclusion") = UCase$(Trim$(PrimaryExclusion))
    PrimaryTbl("LetterTranslation") = UCase$(Trim$(PrimaryLanguageTranslation))
    PrimaryTbl("OtherLtrTrans") = UCase$(Trim$(PrimaryLanguageTranslation1))
    If PrimaryBilling Then
        PrimaryTbl("Billable") = "T"
    ElseIf (PrimaryUnspecifiedOn) Then
        PrimaryTbl("Billable") = "U"
    Else
        PrimaryTbl("Billable") = "F"
    End If
    If PrimaryNextLevelOn Then
        PrimaryTbl("NextLevel") = "T"
    Else
        PrimaryTbl("NextLevel") = "F"
    End If
    If PrimaryPermanent Then
        PrimaryTbl("PermanentCondition") = "T"
    Else
        PrimaryTbl("PermanentCondition") = "F"
    End If
    If PrimaryImpression Then
        PrimaryTbl("ImpressionOn") = "T"
    Else
        PrimaryTbl("ImpressionOn") = "F"
    End If
    PrimaryTbl.Update
    If PrimaryNew Then
        PrimaryId = PrimaryTbl("PrimaryDrillId")
        PrimaryNew = False
    End If
Exit Function


DbErrorHandler:
    ModuleName = "PutPrimary"
    PutPrimary = False
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Public Function RetrievePrimary() As Boolean

    RetrievePrimary = GetPrimary
End Function


Public Function SelectPrimary(ByVal ListItem As Long) As Boolean

    On Error GoTo DbErrorHandler
    SelectPrimary = False
    With PrimaryTbl
        If Not (.EOF Or (ListItem < 1) Or (ListItem > PrimaryTotal)) Then
            .MoveFirst
            .Move ListItem - 1
            SelectPrimary = True
            LoadPrimary 0
        End If
    End With
Exit Function


DbErrorHandler:
    ModuleName = "SelectPrimary"
    PrimaryId = -256
    SelectPrimary = False
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Public Function SelectPrimaryDistinct(ByVal ListItem As Long) As Boolean

    On Error GoTo DbErrorHandler
    SelectPrimaryDistinct = False
    With PrimaryTbl
        If Not (.EOF Or (ListItem < 1) Or (ListItem > PrimaryTotal)) Then
            .MoveFirst
            .Move ListItem - 1
            SelectPrimaryDistinct = True
            LoadPrimary 1
        End If
    End With
Exit Function


DbErrorHandler:
    ModuleName = "SelectPrimaryDistinct"
    PrimaryId = -256
    SelectPrimaryDistinct = False
    PinpointPRError ModuleName, Err.Number
    Resume LeaveFast
LeaveFast:
End Function


Public Function VerifyDiagnosis() As Boolean

Dim OrderString As String

    OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel = '" & PrimaryNextLevelDiagnosis & "' And DiagnosisRank > 0 And DiagnosisDrillDownLevel >= 0 And (Billable = 'T' Or Billable = 'U')ORDER BY Diagnosis ASC, MedicalSystem ASC"
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    VerifyDiagnosis = (PrimaryTbl.RecordCount > 0)
End Function

Public Function GetDiagName(NextDiag As String) As String
Dim OrderString As String
    GetDiagName = ""
    
    OrderString = "SELECT * FROM PrimaryDiagnosisTable WHERE DiagnosisNextLevel = '" & NextDiag & "'"
    Set PrimaryTbl = Nothing
    Set PrimaryTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    PrimaryTbl.Open MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1
    If PrimaryTbl.RecordCount = 0 Then
        Dim RetDiag As New Service
        RetDiag.Service = NextDiag
        RetDiag.ServiceType = "D"
        If (RetDiag.FindDiagnosis > 0) Then
            If (RetDiag.SelectService(1)) Then
                GetDiagName = Trim(RetDiag.ServiceDescription)
            End If
        End If
        Set RetDiag = Nothing
    Else
        GetDiagName = PrimaryTbl("DiagnosisName")
    End If
    
    If GetDiagName = "" Then
        GetDiagName = "Diagnosis is not listed. No info available"
    End If

End Function
