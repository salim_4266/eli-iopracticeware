VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PCInventory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Inventory Object Module
Option Explicit
Public InventoryId As Long
Public SKU As String
Public Type_ As String
Public Manufacturer As String
Public Model As String
Public PCColor As String
Public PCSize As String
Public LensCoating As String
Public LensTint As String
Public PDD As String
Public PDN As String
Public CEGHeight As String
Public Qty As Long
Public Cost As Single
Public WCost As Single
Public SingleFieldValue As String
Public PCInventoryTotal As Long
Public FieldSelect As String
Public Criteria As String
Public OrderBy As String
Private SingleField As Boolean

Private ModuleName As String
Private PCInventoryTbl As ADODB.Recordset
Private PCInventoryNew As Boolean

Private Sub Class_Initialize()
Set PCInventoryTbl = CreateAdoRecordset
Call InitPCInventory
End Sub

Private Sub Class_Terminate()
Call InitPCInventory
Set PCInventoryTbl = Nothing
End Sub

Private Sub InitPCInventory()
PCInventoryNew = True
InventoryId = 0
SKU = ""
Type_ = ""
Manufacturer = ""
Model = ""
PCColor = ""
PCSize = ""
LensCoating = ""
LensTint = ""
PDD = ""
PDN = ""
CEGHeight = ""
Qty = 0
Cost = 0
WCost = 0
SingleFieldValue = ""
End Sub

Private Sub LoadPCInventory()
PCInventoryNew = False
If (SingleField) Then
    SingleFieldValue = PCInventoryTbl(FieldSelect)
Else
    InventoryId = PCInventoryTbl("InventoryID")
    If (PCInventoryTbl("SKU") <> "") Then
        SKU = PCInventoryTbl("SKU")
    Else
        SKU = ""
    End If
    If (PCInventoryTbl("Type_") <> "") Then
        Type_ = PCInventoryTbl("Type_")
    Else
        Type_ = ""
    End If
    If (PCInventoryTbl("Manufacturer") <> "") Then
        Manufacturer = PCInventoryTbl("Manufacturer")
    Else
        Manufacturer = ""
    End If
    If (PCInventoryTbl("Model") <> "") Then
        Model = PCInventoryTbl("Model")
    Else
        Model = ""
    End If
    If (PCInventoryTbl("Color") <> "") Then
        PCColor = PCInventoryTbl("Color")
    Else
        PCColor = ""
    End If
    If (PCInventoryTbl("PCSize") <> "") Then
        PCSize = PCInventoryTbl("PCSize")
    Else
        PCSize = ""
    End If
    If (PCInventoryTbl("LensCoating") <> "") Then
        LensCoating = PCInventoryTbl("LensCoating")
    Else
        LensCoating = ""
    End If
    If (PCInventoryTbl("LensTint") <> "") Then
        LensTint = PCInventoryTbl("LensTint")
    Else
        LensTint = ""
    End If
'    If (PCInventoryTbl("PD-D") <> "") Then
'        PDD = PCInventoryTbl("PD-D")
'    Else
'        PDD = ""
'    End If
'    If (PCInventoryTbl("PD-N") <> "") Then
'        PDN = PCInventoryTbl("PD-N")
'    Else
'        PDN = ""
'    End If
    If (PCInventoryTbl("CEGHeight") <> "") Then
        CEGHeight = PCInventoryTbl("CEGHeight")
    Else
        CEGHeight = ""
    End If
    If (PCInventoryTbl("Qty") <> "") Then
        Qty = PCInventoryTbl("Qty")
    Else
        Qty = -1
    End If
    If (PCInventoryTbl("Cost") <> "") Then
        Cost = PCInventoryTbl("Cost")
    Else
        Cost = -1
    End If
    If (PCInventoryTbl("WCost") <> "") Then
        WCost = PCInventoryTbl("WCost")
    Else
        WCost = -1
    End If
End If
End Sub

Public Function PutPCInventory() As Boolean
On Error GoTo DbErrorHandler
PutPCInventory = True
If (PCInventoryNew) Then
    PCInventoryTbl.AddNew
    PCInventoryTbl("SKU") = SKU
End If
PCInventoryTbl("Type_") = Type_
PCInventoryTbl("Manufacturer") = Manufacturer
PCInventoryTbl("Model") = Model
PCInventoryTbl("Color") = PCColor
'PCInventoryTbl("PCSize") = PCSize
PCInventoryTbl("LensCoating") = LensCoating
PCInventoryTbl("LensTint") = LensTint
'PCInventoryTbl("PD-D") = PDD
'PCInventoryTbl("PD-N") = PDN
'PCInventoryTbl("CEGHeight") = CEGHeight
PCInventoryTbl("Qty") = Qty
PCInventoryTbl("Cost") = Cost
PCInventoryTbl("WCost") = WCost
PCInventoryTbl.Update
If (PCInventoryNew) Then
    PCInventoryTbl.MoveLast
    Call LoadPCInventory
    PCInventoryNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPCInventory"
    PutPCInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPCInventorySingleField() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPCInventorySingleField = True
OrderString = "SELECT DISTINCT " + FieldSelect + " FROM PCInventory"
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy + " ASC"
End If
Set PCInventoryTbl = Nothing
Set PCInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PCInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PCInventoryTbl.EOF) Then
    Call InitPCInventory
Else
    PCInventoryTbl.MoveLast
    PCInventoryTotal = PCInventoryTbl.RecordCount
    Call LoadPCInventory
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPCInventorySingleField"
    GetPCInventorySingleField = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPCInventory() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPCInventory = True
OrderString = "SELECT * FROM PCInventory"
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy + " ASC"
End If
Set PCInventoryTbl = Nothing
Set PCInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PCInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PCInventoryTbl.EOF) Then
    Call InitPCInventory
Else
    PCInventoryTbl.MoveLast
    PCInventoryTotal = PCInventoryTbl.RecordCount
    Call LoadPCInventory
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPCInventory"
    GetPCInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrievePCInventory() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
RetrievePCInventory = False
OrderString = "SELECT * FROM PCInventory WHERE InventoryID = " + Trim(Str(InventoryId)) + " "
Set PCInventoryTbl = Nothing
Set PCInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PCInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PCInventoryTbl.EOF) Then
    RetrievePCInventory = True
    Call LoadPCInventory
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrievePCInventory"
    RetrievePCInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPCInventory(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectPCInventory = True
If (PCInventoryTbl.EOF) Or (ListItem < 1) Or (ListItem > PCInventoryTotal) Then
    SelectPCInventory = False
    Exit Function
End If
PCInventoryTbl.MoveFirst
PCInventoryTbl.Move ListItem - 1
Call LoadPCInventory
Exit Function
DbErrorHandler:
    ModuleName = "SelectPCInventory"
    SelectPCInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPCInventorySingleField() As Boolean
On Error GoTo DbErrorHandler
SingleField = True
FindPCInventorySingleField = GetPCInventorySingleField
Exit Function
DbErrorHandler:
    ModuleName = "FindPCInventorySingleField"
    FindPCInventorySingleField = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPCInventory() As Boolean
On Error GoTo DbErrorHandler
SingleField = False
FindPCInventory = GetPCInventory
Exit Function
DbErrorHandler:
    ModuleName = "FindPCInventory"
    FindPCInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPCManufacturers(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectPCManufacturers = True
If (PCInventoryTbl.EOF) Or (ListItem < 1) Or (ListItem > PCInventoryTotal) Then
    SelectPCManufacturers = False
    Exit Function
End If
PCInventoryTbl.MoveFirst
PCInventoryTbl.Move ListItem - 1
If (PCInventoryTbl("Manufacturer") <> "") Then
    Manufacturer = PCInventoryTbl("Manufacturer")
Else
    Manufacturer = ""
End If
Exit Function
DbErrorHandler:
    ModuleName = "SelectPCManufacturers"
    SelectPCManufacturers = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPCManufacturers() As Long
On Error GoTo DbErrorHandler
FindPCManufacturers = GetPCManufacturers
Exit Function
DbErrorHandler:
    ModuleName = "FindPCManufacturers"
    FindPCManufacturers = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPCManufacturers() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPCManufacturers = -1
OrderString = "SELECT DISTINCT Manufacturer FROM PCInventory "
OrderString = OrderString + " WHERE Manufacturer >= '" + Criteria + "' "
OrderString = OrderString + " ORDER BY Manufacturer ASC "
Set PCInventoryTbl = Nothing
Set PCInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PCInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PCInventoryTbl.EOF) Then
    PCInventoryTbl.MoveLast
    PCInventoryTotal = PCInventoryTbl.RecordCount
    GetPCManufacturers = PCInventoryTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPCManufacturers"
    GetPCManufacturers = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

