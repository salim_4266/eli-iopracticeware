Attribute VB_Name = "EClaimsLib"
Option Explicit
Const MODULE_NAME As String = "EClaimsLib"
Private Const NoDenialsMsg As String = "Congratulations!  There are no denials in this file."

Public Function ParseSumForDenials(SummaryReport As String, Optional Recursed As Boolean = False) As Boolean
    On Error GoTo lErrorHandler
Dim Rec As String
ParseSumForDenials = False
If (FM.IsFileThere(SummaryReport)) Then
    FM.OpenFile SummaryReport, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    Line Input #101, Rec
    If (Mid$(Rec, 36, 14) = "SUBMITTER ID: ") Then ' Put all the 1st line recognitions here.
        FM.CloseFile CLng(101)
        ParseSumForDenials = True
        Call ParseSumGHI(SummaryReport)
    ElseIf InStrPS(1, Rec, "NO FILES QUEUED") Then
        FM.CloseFile CLng(101)
        ParseSumForDenials = True
        Call FileCopy(SummaryReport, PinPointDirectory & "SummaryReports\ParsedDenials.txt")
    Else
        If EOF(101) Then
            Rec = Replace(Rec, vbLf, vbCrLf)
            FM.CloseFile CLng(101)
            FM.OpenFile SummaryReport, FileOpenMode.Output, FileAccess.ReadWrite, CLng(101)
            Print #101, Rec
            FM.CloseFile CLng(101)
            ' Ok, now that we fixed the file, we gotta reload it and start the recognition process again.
            If Not (Recursed) Then
                ParseSumForDenials = ParseSumForDenials(SummaryReport, True)
            Else
                MsgBox ("This file has only one line.  Please call IO if you feel this is in error.")
            End If
            Exit Function
        End If
        Line Input #101, Rec
        Line Input #101, Rec
        If (InStrPS(Mid$(Rec, 54, 24), "MEDICARE-B EMC INPUT")) Then ' Put all the 3rd line recognitions here.
            FM.CloseFile CLng(101)
            ParseSumForDenials = True
            Call ParseSumMedicareDirect(SummaryReport)
        Else
            Line Input #101, Rec
            If (Mid$(Rec, 51, 26) = "BLUE SHIELD RECEIPT REPORT") Then ' Put all the 4th line recognitions here.
                Line Input #101, Rec
                Line Input #101, Rec
                Line Input #101, Rec
                Line Input #101, Rec
                Line Input #101, Rec
                If (InStrPS(1, Rec, "TRANSMISSION STATUS:      REJECT")) Then
                    FM.CloseFile CLng(101)
                    ParseSumForDenials = True
                    Call FileCopy(SummaryReport, PinPointDirectory & "SummaryReports\ParsedDenials.txt")
                Else
                    FM.CloseFile CLng(101)
                    ParseSumForDenials = True
                    Call ParseSumEmpireBSNY(SummaryReport)
                End If
            ElseIf (Mid$(Rec, 49, 30) = "MEDICARE PART B RECEIPT REPORT") Then
                Line Input #101, Rec
                Line Input #101, Rec
                Line Input #101, Rec
                Line Input #101, Rec
                Line Input #101, Rec
                If (InStrPS(1, Rec, "TRANSMISSION STATUS: REJECT")) Then
                    FM.CloseFile CLng(101)
                    ParseSumForDenials = True
                    Call FileCopy(SummaryReport, PinPointDirectory & "SummaryReports\ParsedDenials.txt")
                Else
                    FM.CloseFile CLng(101)
                    ParseSumForDenials = True
                    Call ParseSumMedicareNY
                End If
            Else
                Line Input #101, Rec
                If (Mid$(Rec, 25, 31) = "HDIG CLAIMS CONFIRMATION REPORT") Then ' Put all the 5th line recognitions here.
                    FM.CloseFile CLng(101)
                    ParseSumForDenials = True
                    Call ParseSumHDIG(SummaryReport)
                Else
                    Line Input #101, Rec
                    Line Input #101, Rec
                    Line Input #101, Rec
                    Line Input #101, Rec
                    If (Mid$(Rec, 25, 31) = "HDIG CLAIMS CONFIRMATION REPORT") Then ' Put all the 9th line recognitions here.
                        FM.CloseFile CLng(101)
                        ParseSumForDenials = True
                        Call ParseSumHDIG(SummaryReport)
                    Else
                        FM.CloseFile CLng(101)
                        MsgBox ("File not recognized.  Please call IO Practiceware to have them take a look.  Thank you.")
                    End If
                End If
            End If
        End If
    End If
End If
Exit Function
lErrorHandler:
    Const FUNCTION_NAME As String = "ParseSumForDenials"
    Call LogFileError(MODULE_NAME, FUNCTION_NAME, Err, SummaryReport)
End Function

Private Sub ParseSumMedicareDirect(SummaryReport As String)
Dim Recs(50) As String
Dim i As Integer, j As Integer
Dim NoDenials As Boolean
NoDenials = True
i = 1
If (FM.IsFileThere(SummaryReport)) Then
    FM.OpenFile SummaryReport, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    FM.OpenFile PinPointDirectory & "SummaryReports\ParsedDenials.txt", FileOpenMode.Output, FileAccess.ReadWrite, CLng(102)
    Line Input #101, Recs(1)
    While (Not (EOF(101)) And NoDenials)
        Line Input #101, Recs(1)
        If (InStrPS(1, Recs(1), "PROCESS DATE")) Then
            Print #102, Mid$(Recs(1), InStrPS(1, Recs(1), "PROCESS DATE"), 24)
            Print #102, ""
            'Set NoDenials To False to exit loop
            NoDenials = False
        End If
    Wend
    NoDenials = True
    While (Not (EOF(101))) And InStrPS(1, (Mid$(Recs(1), 8, 23)), "SUBMITTER TOTAL CHARGED") = 0
        Line Input #101, Recs(1)
        If InStrPS(1, (Mid$(Recs(1), 55, 16)), "BATCH NUMBER") Then
            GoSub CheckForDenial
        End If
    Wend
Else
    MsgBox ("File " & SummaryReport & " doesn't exist.")
End If
If (NoDenials) Then
    Print #102, NoDenialsMsg
End If
FM.CloseFile CLng(101)
FM.CloseFile CLng(102)
Exit Sub

CheckForDenial:
While InStrPS(1, (Mid$(Recs(i), 6, 25)), "TOTAL CLAIMS ACCEPTED") = 0 And InStrPS(1, (Mid$(Recs(i), 53, 29)), "TOTALS FOR THIS SUBMITTER") = 0 And (Not (EOF(101)))
    i = i + 1
    Line Input #101, Recs(i)
    If InStrPS(1, (Mid$(Recs(i), 58, 20)), "TOTALS FOR THIS FILE") > 0 Then
        i = 1
        Return
    End If
Wend
If Val(Mid$(Recs(i), 46, 5)) = 0 Then
    GoSub PrintDenial
    NoDenials = False
End If
i = 1
Return

PrintDenial:
For j = 1 To i - 2
If InStrPS(1, Recs(j), "NGS INC NJ") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "PROFESSIONAL EMC PROGRAM") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "MEDICARE-B EMC INPUT") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "BATCH DETAIL CONTROL LISTING") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "SUBMITTER NAME:") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "ADDRESS:") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "CITY:") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "STATE/ZIP:") Then
    'Do nothing
ElseIf InStrPS(1, Recs(j), "PROCESS DATE:") Then
    'Do nothing
ElseIf Trim$(Recs(j)) = "" Then
    'Do nothing
Else
    Print #102, Recs(j)
End If
Next j
Print #102, ""
Print #102, ""
Print #102, ""
Print #102, ""
Return
End Sub

Private Sub ParseSumEmpireBSNY(SummaryReport As String)
Dim Recs(50) As String
Dim i As Integer, j As Integer
Dim NoDenials As Boolean, recordLine As Boolean
    On Error GoTo lParseSumEmpireBSNY_Error

NoDenials = True
i = 1
If (FM.IsFileThere(SummaryReport)) Then
    FM.OpenFile SummaryReport, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    FM.OpenFile PinPointDirectory & "SummaryReports\ParsedDenials.txt", FileOpenMode.Output, FileAccess.ReadWrite, CLng(102)
    Line Input #101, Recs(1)
    While (Not (EOF(101)))
        Line Input #101, Recs(1)
        If (Mid$(Recs(1), 46, 6) = "FAILED") Then
            NoDenials = False
            GoSub CheckForDenialFailed
        End If
        If (Mid$(Recs(1), 46, 7) = "PARTIAL") Then
            NoDenials = False
            GoSub CheckForDenialPartial
        End If
    Wend
Else
    MsgBox ("File " & SummaryReport & " doesn't exist.")
End If
If (NoDenials) Then
    Print #102, NoDenialsMsg
End If
FM.CloseFile CLng(101)
FM.CloseFile CLng(102)
Exit Sub

CheckForDenialPartial:
Line Input #101, Recs(i)
While (Mid$(Recs(i), 26, 24) <> "MESSAGE005 END OF REPORT") And (Not (EOF(101)))
    If (InStrPS(Recs(i), "ERROR KEY")) Then
        While (Mid$(Recs(i), 1, 1) <> "1")
            i = i + 1
            Line Input #101, Recs(i)
        Wend
        GoSub PrintDenial
        i = 1
    End If
    If (InStrPS(Recs(i), "RECEIVED   REJECTED")) Then
        Line Input #101, Recs(i)
            If (Val(Mid$(Recs(i), 102, 8)) > 0) Then
            Line Input #101, Recs(i)
            While (Mid$(Recs(i), 1, 1) <> "1" And (Not EOF(101)))
                ' Don't want the first line so Line Input then increment i
                i = i + 1
                Line Input #101, Recs(i)
            Wend
            'Line Input #101, Recs(i)
            GoSub PrintDenial
            i = 1
        End If
    End If
    If Not EOF(101) Then Line Input #101, Recs(i)
Wend

Return

CheckForDenialFailed:
recordLine = False
While (Mid$(Recs(i), 26, 24) <> "MESSAGE005 END OF REPORT") And (Not (EOF(101)))
    If (recordLine) Then
        i = i + 1
    End If
    Line Input #101, Recs(i)
    If (Mid$(Recs(i), 1, 33) = "0 ---------- ERROR KEY ----------") Or (Mid$(Recs(i), 51, 8) = "PROVIDER" And Mid$(Recs(i), 70, 7) = "SUMMARY") Then
        recordLine = True
    End If
    If (Mid$(Recs(i), 1, 1) = "1") Then
        recordLine = False
    End If
Wend
GoSub PrintDenial
i = 1
Return

PrintDenial:
For j = 1 To i - 1
    Print #102, Recs(j)
Next j
Print #102, ""
Print #102, ""
Print #102, ""
Return

    Exit Sub

lParseSumEmpireBSNY_Error:

    LogFileError "EClaimsLib", "ParseSumEmpireBSNY", Err, Err.Description
End Sub

Public Sub ParseSumMedicareNY()
FM.OpenFile PinPointDirectory & "SummaryReports\ParsedDenials.txt", FileOpenMode.Output, FileAccess.ReadWrite, CLng(102)
Print #102, NoDenialsMsg
FM.CloseFile CLng(102)
End Sub

Public Sub DisplayTextFile(DispBox As TextBox, TextFile As String)
Dim DispText As String
If (FM.IsFileThere(TextFile)) Then
    FM.OpenFile TextFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    DispBox.Text = ""
    Do While (Not (EOF(101)))
        Line Input #101, DispText
        DispBox.Text = DispBox.Text & DispText & vbCrLf
    Loop
Else
    DispBox.Text = "No Denial Worksheet currently in use."
End If
FM.CloseFile CLng(101)
End Sub

Private Sub ParseSumGHI(SummaryReport As String)
Dim Recs(50) As String
Dim i As Integer, j As Integer
Dim NoDenials As Boolean
NoDenials = True
i = 1
If (FM.IsFileThere(SummaryReport)) Then
    FM.OpenFile SummaryReport, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    FM.OpenFile PinPointDirectory & "SummaryReports\ParsedDenials.txt", FileOpenMode.Output, FileAccess.ReadWrite, CLng(102)
    Line Input #101, Recs(1)
    While (Not (EOF(101)) And NoDenials)
        Line Input #101, Recs(1)
        If (InStrPS(1, Recs(1), "PROCESS DATE")) Then
            Print #102, Mid$(Recs(1), InStrPS(1, Recs(1), "PROCESS DATE"), 29)
            Print #102, ""
            'Set NoDenials To False to exit loop
            NoDenials = False
        End If
    Wend
    NoDenials = True
    While (Not (EOF(101))) And (Mid$(Recs(1), 54, 25) <> "TOTALS FOR THIS SUBMITTER")
        Line Input #101, Recs(1)
        If (Mid$(Recs(1), 3, 14) = "EMC PROVIDER :") Then
            GoSub CheckForDenial
        End If
    Wend
Else
    MsgBox ("File " & SummaryReport & " doesn't exist.")
End If
If (NoDenials) Then
    Print #102, NoDenialsMsg
End If
FM.CloseFile CLng(101)
FM.CloseFile CLng(102)
Exit Sub

CheckForDenial:
i = i + 1
Line Input #101, Recs(i)
'If (Trim$(Recs(i)) <> "") Then
'    i = 1
'    Return
'End If
While (Mid$(Recs(i), 7, 21) <> "TOTAL CLAIMS ACCEPTED") And (Mid$(Recs(i), 54, 25) <> "TOTALS FOR THIS SUBMITTER") And (Not (EOF(101)))
    i = i + 1
    Line Input #101, Recs(i)
    If (Mid$(Recs(i), 58, 20) = "TOTALS FOR THIS FILE") Then
        i = 1
        Return
    End If
Wend
If (Mid$(Recs(i), 48, 1) = "0") Then
    GoSub PrintDenial
    NoDenials = False
End If
i = 1
Return

PrintDenial:
For j = 1 To i - 2
    Select Case Mid$(Recs(j), 1, 30)
        Case "  H99RAR04                    "
            ' Do nothing
        Case "                              "
            ' Do nothing
        Case Else
            Print #102, Recs(j)
    End Select
Next j
Print #102, ""
Print #102, ""
Print #102, ""
Return
End Sub

Private Sub ParseSumHDIG(SummaryReport As String)
Dim Recs(50) As String
Dim i As Integer, j As Integer
Dim NoDenials As Boolean
NoDenials = True
i = 1
If (FM.IsFileThere(SummaryReport)) Then
    FM.OpenFile SummaryReport, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    FM.OpenFile PinPointDirectory & "SummaryReports\ParsedDenials.txt", FileOpenMode.Output, FileAccess.ReadWrite, CLng(102)
    Line Input #101, Recs(1)
    While (Not (EOF(101)) And NoDenials)
        Line Input #101, Recs(1)
        If (InStrPS(1, Recs(1), "PROCESSING DATE")) Then
            Print #102, Mid$(Recs(1), InStrPS(1, Recs(1), "PROCESSING DATE"), 29)
            Print #102, ""
            'Set NoDenials To False to exit loop
            NoDenials = False
        End If
    Wend
    NoDenials = True
    ' First find File Level Errors
    While (Not (EOF(101)))
        Line Input #101, Recs(1)
        If (Mid$(Recs(1), 1, 26) = "START OF FILE LEVEL ERRORS") Then
            GoSub CheckForDenial
        End If
    Wend
    FM.CloseFile CLng(101)
    FM.OpenFile SummaryReport, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    ' Next find Batch Level Errors
    While (Not (EOF(101)))
        Line Input #101, Recs(1)
        If (Mid$(Recs(1), 1, 27) = "START OF CLAIMS WITH ERRORS") Then
            GoSub CheckForDenial
        End If
    Wend
Else
    MsgBox ("File " & SummaryReport & " doesn't exist.")
End If
If (NoDenials) Then
    Print #102, NoDenialsMsg
End If
FM.CloseFile CLng(101)
FM.CloseFile CLng(102)
Exit Sub

CheckForDenial:
While (Mid$(Recs(i), 1, 20) <> "====================") And (Mid$(Recs(i), 1, 20) <> "____________________") And (Not (EOF(101)))
    i = i + 1
    Line Input #101, Recs(i)
    If (Mid$(Recs(i), 1, 21) = "NO CLAIMS WITH ERRORS") Then
        i = 1
        Return
    End If
Wend
GoSub PrintDenial
NoDenials = False
i = 1
Return

PrintDenial:
For j = 1 To i - 3
    If (Mid$(Recs(j), 1, 15) = "START OF CLAIMS") Then
        'Do nothing
    ElseIf (Mid$(Recs(j), 1, 20) = "--------------------") Then
        'Do nothing
    Else
        Print #102, Recs(j)
    End If
Next j
Return
End Sub
