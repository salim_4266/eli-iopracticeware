VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ZipCodes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Zips Object Module
Option Explicit
Public ZipCode As String
Public ZipCity As String
Public ZipState As String

Private ModuleName As String
Private PracticeZipTotal As Long
Private PracticeZipTbl As ADODB.Recordset
Private ZipNew As Boolean

Private Sub Class_Initialize()
Set PracticeZipTbl = CreateAdoRecordset
Call InitZip
End Sub

Private Sub Class_Terminate()
Call InitZip
Set PracticeZipTbl = Nothing
End Sub

Private Sub InitZip()
ZipNew = True
ZipCode = ""
ZipCity = ""
ZipState = ""
End Sub

Private Sub LoadZip()
If (PracticeZipTbl("ZIPCode") <> "") Then
    ZipCode = PracticeZipTbl("ZIPCode")
Else
    ZipCode = ""
End If
If (PracticeZipTbl("CityName") <> "") Then
    ZipCity = PracticeZipTbl("CityName")
Else
    ZipCity = ""
End If
If (PracticeZipTbl("StateAbbr") <> "") Then
    ZipState = PracticeZipTbl("StateAbbr")
Else
    ZipState = ""
End If
End Sub

Private Function GetZips() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
PracticeZipTotal = 0
GetZips = -1
OrderString = "Select * FROM ZipCodes WHERE "
If (Trim(ZipCode) <> "") Then
    OrderString = OrderString + "ZIPCode = '" + UCase(Trim(ZipCode)) + "' "
    Ref = "And "
End If
If (Trim(ZipCity) <> "") Then
    OrderString = OrderString + Ref + "CityName = '" + UCase(Trim(ZipCity)) + "' "
    Ref = "And "
End If
If (Trim(ZipState) <> "") Then
    OrderString = OrderString + Ref + "StateAbbr = '" + UCase(Trim(ZipState)) + "' "
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY ZipCode ASC "
Set PracticeZipTbl = Nothing
Set PracticeZipTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeZipTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeZipTbl.EOF) Then
    Call InitZip
Else
    PracticeZipTbl.MoveLast
    PracticeZipTotal = PracticeZipTbl.RecordCount
    GetZips = PracticeZipTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetZips"
    GetZips = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectZip(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectZip = False
If (PracticeZipTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeZipTotal) Then
    Exit Function
End If
PracticeZipTbl.MoveFirst
PracticeZipTbl.Move ListItem - 1
SelectZip = True
Call LoadZip
Exit Function
DbErrorHandler:
    ModuleName = "SelectZip"
    SelectZip = False
    ZipCode = "-"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindZip() As Long
FindZip = GetZips
End Function

Public Function FindZipbyCity() As Long
FindZipbyCity = GetZips
End Function

Public Function FindZipList() As Long
FindZipList = GetZips
End Function


