VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PrimaryDiagnosis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public PrimaryDrillID As Long
Public MedicalSystem As String
Public Diagnosis As String
Public DiagnosisRank As Long
Public DiagnosisDrillDownLevel As Long
Public DiagnosisNextLevel As String
Public DiagnosisName As String
Public ReviewSystemLingo As String
Public Image1 As String
Public Image2 As String
Public Discipline As String
Public Billable As String
Public NextLevel As String
Public ShortName As String
Public Exclusion As Boolean
Public LetterTranslation As String
Public OtherLtrTrans As String
Public PermanentCondition As Boolean

Private tblPrimaryDiagnosis As ADODB.Recordset


Public Function FindPrimaryDiagnoses() As Long
FindPrimaryDiagnoses = GetPrimaryDiagnoses()
End Function


Private Function GetPrimaryDiagnoses() As Long

Dim i As Integer
Dim QueryString As String
Dim OrderBy As String
GetPrimaryDiagnoses = -1

i = 0
QueryString = "SELECT MedicalSystem, Diagnosis, DiagnosisNextLevel, DiagnosisRank, DiagnosisDrillDownLevel, ReviewSystemLingo, NextLevel, LetterTranslation, OtherLtrTrans, ShortName, Image2 FROM PrimaryDiagnosisTable WHERE"

If (MedicalSystem <> "") Then
    QueryString = QueryString + " MedicalSystem = '" + MedicalSystem + "'"
    i = 1
End If
If (Diagnosis <> "") Then
    If (i = 1) Then
        QueryString = QueryString + " AND"
    End If
    QueryString = QueryString + " Diagnosis = '" + Diagnosis + "'"
    i = 1
End If
If (DiagnosisNextLevel <> "") Then
    If (i = 1) Then
        QueryString = QueryString + " AND"
    End If
    QueryString = QueryString + " DiagnosisNextLevel = '" + DiagnosisNextLevel + "'"
    i = 1
End If
If (DiagnosisRank <> 0) Then
    If (i = 1) Then
        QueryString = QueryString + " AND"
    End If
    QueryString = QueryString + " Diagnosisrank = " + DiagnosisRank
    i = 1
End If
If (DiagnosisDrillDownLevel <> 0) Then
    If (i = 1) Then
        QueryString = QueryString + " AND"
    End If
    QueryString = QueryString + " DiagnosisDrillDownLevel = " + Trim(Str(DiagnosisDrillDownLevel))
    i = 1
End If
If (ReviewSystemLingo <> "") Then
    If (i = 1) Then
        QueryString = QueryString + " AND"
    End If
    QueryString = QueryString + " ReviewSystemLingo = '" + ReviewSystemLingo + "'"
    i = 1
End If
If (NextLevel = "T") Then
    If (i = 1) Then
        QueryString = QueryString + " AND"
    End If
    QueryString = QueryString + " NextLevel = 'T'"
End If
OrderBy = " ORDER BY MedicalSystem, DiagnosisDrillDownLevel, DiagnosisRank"
QueryString = QueryString + OrderBy
Set tblPrimaryDiagnosis = Nothing
Set tblPrimaryDiagnosis = CreateAdoRecordset
DMCmd.CommandText = QueryString
Call tblPrimaryDiagnosis.Open(DMCmd, , adOpenStatic, adLockOptimistic, -1)
If (tblPrimaryDiagnosis.EOF) Then
    GetPrimaryDiagnoses = -1
Else
    tblPrimaryDiagnosis.MoveFirst
    GetPrimaryDiagnoses = tblPrimaryDiagnosis.RecordCount
End If
Exit Function


End Function

Public Function SelectPrimaryDiagnosis(ListItem As Long) As Boolean
Dim u As Integer

If (tblPrimaryDiagnosis.EOF) Or (ListItem < 1) Or (ListItem > tblPrimaryDiagnosis.RecordCount) Then
    Exit Function
End If
tblPrimaryDiagnosis.MoveFirst
tblPrimaryDiagnosis.Move ListItem - 1

Call LoadPrimaryDiagnosis
Exit Function

End Function

Private Sub LoadPrimaryDiagnosis()

MedicalSystem = tblPrimaryDiagnosis("MedicalSystem")
If (IsNull(tblPrimaryDiagnosis("Diagnosis"))) Then
    Diagnosis = ""
Else
    Diagnosis = tblPrimaryDiagnosis("Diagnosis")
End If
DiagnosisNextLevel = tblPrimaryDiagnosis("DiagnosisNextLevel")
DiagnosisRank = tblPrimaryDiagnosis("DiagnosisRank")
DiagnosisDrillDownLevel = tblPrimaryDiagnosis("DiagnosisDrillDownLevel")
ReviewSystemLingo = tblPrimaryDiagnosis("ReviewSystemLingo")
If (IsNull(tblPrimaryDiagnosis("NextLevel"))) Then
    NextLevel = ""
Else
    NextLevel = tblPrimaryDiagnosis("NextLevel")
End If
If (IsNull(tblPrimaryDiagnosis("LetterTranslation"))) Then
    LetterTranslation = ""
Else
    LetterTranslation = tblPrimaryDiagnosis("LetterTranslation")
End If
If (IsNull(tblPrimaryDiagnosis("OtherLtrTrans"))) Then
    OtherLtrTrans = ""
Else
    OtherLtrTrans = tblPrimaryDiagnosis("OtherLtrTrans")
End If
If (IsNull(tblPrimaryDiagnosis("Image2"))) Then
    Image2 = ""
Else
    Image2 = tblPrimaryDiagnosis("Image2")
End If
If (IsNull(tblPrimaryDiagnosis("ShortName"))) Then
    ShortName = ""
Else
    ShortName = tblPrimaryDiagnosis("ShortName")
End If

End Sub


