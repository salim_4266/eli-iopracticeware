VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicThirdForms"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicThirdForms Object Module
Option Explicit
Public ThirdFormId As Long
Public FormId As Long
Public ControlName As String
Public BackColor As Long
Public ForeColor As Long
Public WindowState As Long
Public WindowHeight As Long
Public WindowWidth As Long
Public ExitButton As Boolean
Public ApplyButton As Boolean
Public DontKnowButton As Boolean
Public Question As String
Public QuestionOrder As String
Public IEQuestion As String
Public IEType As String
Public NoToAllButton As Boolean
Public HelpButton As Boolean
Public StaticLabel As String

Private ModuleName As String
Public ClinicalTotalForms As Integer
Private ClinicalFormsTbl As ADODB.Recordset
Private ClinicalFormsNew As Boolean

Private Sub Class_Initialize()
Set ClinicalFormsTbl = New ADODB.Recordset
ClinicalFormsTbl.CursorLocation = adUseClient
Call InitForm
End Sub

Private Sub Class_Terminate()
Call InitForm
Set ClinicalFormsTbl = Nothing
End Sub

Private Sub InitForm()
ClinicalFormsNew = False
FormId = 0
ControlName = ""
BackColor = 0
ForeColor = 0
WindowState = 0
WindowHeight = 0
WindowWidth = 0
ExitButton = False
ApplyButton = False
DontKnowButton = False
HelpButton = False
NoToAllButton = False
Question = ""
QuestionOrder = ""
End Sub

Private Function LoadForm() As Boolean
ClinicalFormsNew = False
ThirdFormId = ClinicalFormsTbl("ThirdFormId")
If (ClinicalFormsTbl("FormId") <> "") Then
    FormId = ClinicalFormsTbl("FormId")
Else
    FormId = 0
End If
If (ClinicalFormsTbl("FormControlName") <> "") Then
    ControlName = ClinicalFormsTbl("FormControlName")
Else
    ControlName = ""
End If
If (ClinicalFormsTbl("BackColor") <> "") Then
    BackColor = ClinicalFormsTbl("BackColor")
Else
    BackColor = 0
End If
If (ClinicalFormsTbl("ForeColor") <> "") Then
    ForeColor = ClinicalFormsTbl("ForeColor")
Else
    ForeColor = 0
End If
If (ClinicalFormsTbl("WindowState") <> "") Then
    WindowState = ClinicalFormsTbl("WindowState")
Else
    WindowState = 0
End If
If (ClinicalFormsTbl("DontKnowButton") <> "") Then
    If (ClinicalFormsTbl("DontKnowButton") = "T") Then
        DontKnowButton = True
    Else
        DontKnowButton = False
    End If
Else
    DontKnowButton = False
End If
If (ClinicalFormsTbl("ExitButton") <> "") Then
    If (ClinicalFormsTbl("ExitButton") = "T") Then
        ExitButton = True
    Else
        ExitButton = False
    End If
Else
    ExitButton = False
End If
If (ClinicalFormsTbl("ApplyButton") <> "") Then
    If (ClinicalFormsTbl("ApplyButton") = "T") Then
        ApplyButton = True
    Else
        ApplyButton = False
    End If
Else
    ApplyButton = False
End If
If (ClinicalFormsTbl("NoToAllButton") <> "") Then
    If (ClinicalFormsTbl("NoToAllButton") = "T") Then
        NoToAllButton = True
    Else
        NoToAllButton = False
    End If
Else
    NoToAllButton = False
End If
If (ClinicalFormsTbl("HelpButton") <> "") Then
    If (ClinicalFormsTbl("HelpButton") = "T") Then
        HelpButton = True
    Else
        HelpButton = False
    End If
Else
    HelpButton = False
End If
If (ClinicalFormsTbl("StaticLabel") <> "") Then
    StaticLabel = ClinicalFormsTbl("StaticLabel")
Else
    StaticLabel = ""
End If
If (ClinicalFormsTbl("Question") <> "") Then
    Question = ClinicalFormsTbl("Question")
Else
    Question = ""
End If
If (ClinicalFormsTbl("QuestionOrder") <> "") Then
    QuestionOrder = ClinicalFormsTbl("QuestionOrder")
Else
    QuestionOrder = ""
End If
If (ClinicalFormsTbl("InferenceQuestion") <> "") Then
    IEQuestion = ClinicalFormsTbl("InferenceQuestion")
Else
    IEQuestion = ""
End If
If (ClinicalFormsTbl("InferenceType") <> "") Then
    IEType = ClinicalFormsTbl("InferenceType")
Else
    IEType = ""
End If
If (ClinicalFormsTbl("FormHeight") <> "") Then
    WindowHeight = ClinicalFormsTbl("FormHeight")
Else
    WindowHeight = 0
End If
If (ClinicalFormsTbl("FormWidth") <> "") Then
    WindowWidth = ClinicalFormsTbl("FormWidth")
Else
    WindowWidth = 0
End If
End Function

Private Function GetForm() As Boolean
On Error GoTo DbErrorHandler
GetForm = False
ClinicalTotalForms = -1
Dim OrderString As String
If (ThirdFormId = -9) Then
    OrderString = "SELECT * FROM ThirdForms WHERE ThirdFormId > " + Str(ThirdFormId)
ElseIf (FormId < 1) And (ThirdFormId < 1) Then
    Exit Function
ElseIf (ThirdFormId < 1) Then
    OrderString = "SELECT * FROM ThirdForms WHERE FormId =" + Str(FormId)
ElseIf (FormId < 1) Then
    OrderString = "SELECT * FROM ThirdForms WHERE ThirdFormId =" + Str(ThirdFormId)
End If
If (Trim(ControlName) <> "") Then
    OrderString = OrderString + " And FormControlName = '" + Trim(ControlName) + "' "
End If
If (Trim(QuestionOrder) <> "") Then
    OrderString = OrderString + " And QuestionOrder = '" + UCase(Trim(QuestionOrder)) + "' "
End If
OrderString = OrderString + " ORDER BY QuestionOrder ASC"
Set ClinicalFormsTbl = Nothing
Set ClinicalFormsTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalFormsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClinicalFormsTbl.EOF) Then
    ClinicalTotalForms = -1
    Call InitForm
Else
    GetForm = True
    Call LoadForm
    ClinicalFormsTbl.MoveLast
    ClinicalTotalForms = ClinicalFormsTbl.RecordCount
    ClinicalFormsTbl.MoveFirst
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetForm"
    Call PinpointPRError(ModuleName, Err)
    GetForm = False
    ThirdFormId = -256
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutForm()
On Error GoTo DbErrorHandler
PutForm = True
If (ClinicalFormsNew) Then
    ClinicalFormsTbl.AddNew
End If
ClinicalFormsTbl("FormId") = FormId
ClinicalFormsTbl("FormControlName") = Trim(ControlName)
ClinicalFormsTbl("BackColor") = BackColor
ClinicalFormsTbl("ForeColor") = ForeColor
ClinicalFormsTbl("WindowState") = WindowState
ClinicalFormsTbl("FormHeight") = WindowHeight
ClinicalFormsTbl("FormWidth") = WindowWidth
ClinicalFormsTbl("NoToAllButton") = "F"
If (NoToAllButton) Then
    ClinicalFormsTbl("NoToAllButton") = "T"
End If
ClinicalFormsTbl("HelpButton") = "F"
If (HelpButton) Then
    ClinicalFormsTbl("HelpButton") = "T"
End If
ClinicalFormsTbl("ExitButton") = "F"
If (ExitButton) Then
    ClinicalFormsTbl("ExitButton") = "T"
End If
ClinicalFormsTbl("ApplyButton") = "F"
If (ApplyButton) Then
    ClinicalFormsTbl("ApplyButton") = "T"
End If
ClinicalFormsTbl("DontKnowButton") = "F"
If (DontKnowButton) Then
    ClinicalFormsTbl("DontKnowButton") = "T"
End If
ClinicalFormsTbl("StaticLabel") = UCase(Trim(StaticLabel))
ClinicalFormsTbl("Question") = UCase(Trim(Question))
ClinicalFormsTbl("QuestionOrder") = UCase(Trim(QuestionOrder))
ClinicalFormsTbl("InferenceQuestion") = UCase(Trim(IEQuestion))
ClinicalFormsTbl("InferenceType") = UCase(Trim(IEType))
ClinicalFormsTbl.Update
If (ClinicalFormsNew) Then
    FormId = ClinicalFormsTbl("ThirdFormId")
    ClinicalFormsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutForm"
    Call PinpointPRError(ModuleName, Err)
    PutForm = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectForm(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectForm = False
If (ClinicalFormsTbl.EOF) Or (ListItem < 1) Or (ListItem > ClinicalTotalForms) Then
    Exit Function
End If
ClinicalFormsTbl.MoveFirst
ClinicalFormsTbl.Move ListItem - 1
SelectForm = True
Call LoadForm
Exit Function
DbErrorHandler:
    ModuleName = "SelectForm"
    Call PinpointPRError(ModuleName, Err)
    SelectForm = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyForm() As Boolean
ApplyForm = PutForm
End Function

Public Function RetrieveForm() As Boolean
RetrieveForm = GetForm
End Function

