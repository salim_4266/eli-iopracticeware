VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Service"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Services Object Module
Option Explicit
Public CodeId As String
Public Service As String
Public ServiceType As String
Public ServiceCategory As String
Public ServiceDescription As String
Public ServiceFee As Single
Public ServiceTOS As String
Public ServicePOS As String
Public ServiceNDC As String
Public ServiceOrderDoc As Boolean
Public ServiceConsultOn As Boolean
Public ServiceStartDate As String
Public ServiceEndDate As String
Public ServiceRVU As Single
Public ServiceClaimNote As String
Public ServiceFilter As Boolean

Private ModuleName As String
Private PracticeServiceTotal As Long
Private PracticeServiceTbl As ADODB.Recordset
Private PracticeServiceNew As Boolean

Private Sub Class_Initialize()
Set PracticeServiceTbl = CreateAdoRecordset
Call InitService
End Sub

Private Sub Class_Terminate()
Call InitService
Set PracticeServiceTbl = Nothing
End Sub

Private Sub InitService()
PracticeServiceNew = True
ServiceType = ""
ServiceCategory = ""
ServiceDescription = ""
ServiceFee = 0
ServiceTOS = ""
ServicePOS = ""
ServiceNDC = ""
ServiceOrderDoc = False
ServiceConsultOn = False
ServiceStartDate = ""
ServiceEndDate = ""
ServiceRVU = 0
ServiceClaimNote = ""
ServiceFilter = False
End Sub

Private Sub LoadService()
PracticeServiceNew = False
CodeId = PracticeServiceTbl("CodeID")

If (PracticeServiceTbl("Code") <> "") Then
    Service = PracticeServiceTbl("Code")
Else
    Service = ""
End If
If (PracticeServiceTbl("CodeType") <> "") Then
    ServiceType = PracticeServiceTbl("CodeType")
Else
    ServiceType = "D"
End If
If (PracticeServiceTbl("CodeCategory") <> "") Then
    ServiceCategory = PracticeServiceTbl("CodeCategory")
Else
    ServiceCategory = ""
End If
If (PracticeServiceTbl("Description") <> "") Then
    ServiceDescription = PracticeServiceTbl("Description")
Else
    ServiceDescription = ""
End If
If (PracticeServiceTbl("Fee") <> "") Then
    ServiceFee = PracticeServiceTbl("Fee")
Else
    ServiceFee = 0
End If
If (PracticeServiceTbl("TOS") <> "") Then
    ServiceTOS = PracticeServiceTbl("TOS")
Else
    ServiceTOS = ""
End If
If (PracticeServiceTbl("POS") <> "") Then
    ServicePOS = PracticeServiceTbl("POS")
Else
    ServicePOS = ""
End If

If (PracticeServiceTbl("NDC") <> "") Then
    ServiceNDC = PracticeServiceTbl("NDC")
Else
    ServiceNDC = ""
End If


If (PracticeServiceTbl("OrderDoc") <> "") Then
    If (PracticeServiceTbl("OrderDoc") = "T") Then
        ServiceOrderDoc = True
    Else
        ServiceOrderDoc = False
    End If
Else
    ServiceOrderDoc = False
End If
If (PracticeServiceTbl("ConsultOn") <> "") Then
    If (PracticeServiceTbl("ConsultOn") = "T") Then
        ServiceConsultOn = True
    Else
        ServiceConsultOn = False
    End If
Else
    ServiceConsultOn = False
End If
If (PracticeServiceTbl("StartDate") <> "") Then
    ServiceStartDate = PracticeServiceTbl("StartDate")
Else
    ServiceStartDate = ""
End If
If (PracticeServiceTbl("EndDate") <> "") Then
    ServiceEndDate = PracticeServiceTbl("EndDate")
Else
    ServiceEndDate = ""
End If
If (PracticeServiceTbl("RelativeValueUnit") <> "") Then
    ServiceRVU = PracticeServiceTbl("RelativeValueUnit")
Else
    ServiceRVU = 0
End If
If (PracticeServiceTbl("ClaimNote") <> "") Then
    ServiceClaimNote = PracticeServiceTbl("ClaimNote")
Else
    ServiceClaimNote = ""
End If
If (PracticeServiceTbl("ServiceFilter") <> "") Then
    If (PracticeServiceTbl("ServiceFilter") = "T") Then
        ServiceFilter = True
    Else
        ServiceFilter = False
    End If
Else
    ServiceFilter = False
End If
End Sub

Private Function GetDiagnosis() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
PracticeServiceTotal = 0
GetDiagnosis = -1
OrderString = "Select * FROM PracticeServices WHERE "
If (Trim(Service) <> "") Then
    If (Len(Trim(Service)) = 1) Then
        OrderString = OrderString + "Code >= '" + UCase(Trim(Service)) + "' "
    Else
        OrderString = OrderString + "Code = '" + UCase(Trim(Service)) + "' "
    End If
    Ref = "And "
End If
OrderString = OrderString + Ref + "CodeType = 'D' "
OrderString = OrderString + "ORDER BY Code ASC, CodeType ASC "
Set PracticeServiceTbl = Nothing
Set PracticeServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeServiceTbl.EOF) Then
    GetDiagnosis = -1
    Call InitService
Else
    PracticeServiceTbl.MoveLast
    PracticeServiceTotal = PracticeServiceTbl.RecordCount
    GetDiagnosis = PracticeServiceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDiagnosis"
    GetDiagnosis = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetServices(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
PracticeServiceTotal = 0
GetServices = -1
OrderString = "Select * FROM PracticeServices WHERE "
If (Trim(Service) <> "") Then
    OrderString = OrderString + "Code >= '" + UCase(Trim(Service)) + "' "
    Ref = "And "
End If
If (Trim(ServiceType) <> "*") And (Trim(ServiceType) <> "") Then
    OrderString = OrderString + Ref + "CodeType = '" + UCase(Trim(ServiceType)) + "' And CodeType <> 'D' "
ElseIf (Trim(ServiceType) = "") Then
    OrderString = OrderString + Ref + "CodeType <> 'D' "
ElseIf (Trim(ServiceType) = "*") Then
    ServiceType = ""
End If
If (IType = 1) Then
    OrderString = OrderString + "ORDER BY Description ASC, Code ASC, CodeType ASC "
ElseIf (IType = 2) Then
    OrderString = OrderString + "ORDER BY Code DESC, CodeType ASC "
Else
    OrderString = OrderString + "ORDER BY Code ASC, CodeType ASC "
End If
Set PracticeServiceTbl = Nothing
Set PracticeServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeServiceTbl.EOF) Then
    GetServices = -1
    Call InitService
Else
    PracticeServiceTbl.MoveLast
    PracticeServiceTotal = PracticeServiceTbl.RecordCount
    GetServices = PracticeServiceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetService"
    GetServices = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
Private Function GetServicesFast(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
PracticeServiceTotal = 0
GetServicesFast = -1
OrderString = "Select * FROM PracticeServices WHERE "
If (Trim(Service) <> "") Then
    If (IType = 1) Then
    
        Ref = "="
    
    End If

    OrderString = OrderString + "Code " + Ref + " '" + UCase(Trim(Service)) + "' "
    Ref = "And "
End If
If (Trim(ServiceType) <> "*") And (Trim(ServiceType) <> "") Then
    OrderString = OrderString + Ref + "CodeType = '" + UCase(Trim(ServiceType)) + "' And CodeType <> 'D' "
ElseIf (Trim(ServiceType) = "") Then
    OrderString = OrderString + Ref + "CodeType <> 'D' "
ElseIf (Trim(ServiceType) = "*") Then
    ServiceType = ""
End If
If (IType = 1) Then
    OrderString = OrderString + "ORDER BY Description ASC, Code ASC, CodeType ASC "
ElseIf (IType = 2) Then
    OrderString = OrderString + "ORDER BY Code DESC, CodeType ASC "
Else
    OrderString = OrderString + "ORDER BY Code ASC, CodeType ASC "
End If
Set PracticeServiceTbl = Nothing
Set PracticeServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeServiceTbl.EOF) Then
    GetServicesFast = -1
    Call InitService
Else
    PracticeServiceTbl.MoveLast
    PracticeServiceTotal = PracticeServiceTbl.RecordCount
    GetServicesFast = PracticeServiceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetServicesFast"
    GetServicesFast = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetService() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetService = True
OrderString = "SELECT * FROM PracticeServices WHERE Code = '" + UCase(Trim(Service)) + "' ORDER BY Code ASC "
Set PracticeServiceTbl = Nothing
Set PracticeServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeServiceTbl.EOF) Then
    Call InitService
Else
    Call LoadService
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetService"
    GetService = False
    Service = "-"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutService() As Boolean
On Error GoTo DbErrorHandler
PutService = True
If (PracticeServiceNew) Then
    PracticeServiceTbl.AddNew
End If
PracticeServiceTbl("Code") = UCase(Trim(Service))
PracticeServiceTbl("CodeType") = UCase(Trim(ServiceType))
PracticeServiceTbl("CodeCategory") = Trim(ServiceCategory)
PracticeServiceTbl("Description") = Trim(ServiceDescription)
PracticeServiceTbl("Fee") = ServiceFee
PracticeServiceTbl("TOS") = ServiceTOS
PracticeServiceTbl("POS") = ServicePOS
PracticeServiceTbl("NDC") = ServiceNDC
PracticeServiceTbl("OrderDoc") = "F"
If (ServiceOrderDoc) Then
    PracticeServiceTbl("OrderDoc") = "T"
End If
PracticeServiceTbl("ConsultOn") = "F"
If (ServiceConsultOn) Then
    PracticeServiceTbl("ConsultOn") = "T"
End If
PracticeServiceTbl("ServiceFilter") = "F"
If (ServiceFilter) Then
    PracticeServiceTbl("ServiceFilter") = "T"
End If
PracticeServiceTbl("StartDate") = ServiceStartDate
PracticeServiceTbl("EndDate") = ServiceEndDate
PracticeServiceTbl("RelativeValueUnit") = ServiceRVU
PracticeServiceTbl("ClaimNote") = ServiceClaimNote
PracticeServiceTbl.Update
If (PracticeServiceNew) Then
    Service = GetJustAddedRecord
    PracticeServiceNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutService"
    PutService = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As String
GetJustAddedRecord = 0
If (GetServices(0) > 0) Then
    If (SelectService(1)) Then
        GetJustAddedRecord = PracticeServiceTbl("Code")
    End If
End If
End Function

Private Function PurgeService() As Boolean
On Error GoTo DbErrorHandler
PurgeService = True
PracticeServiceTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeService"
    PurgeService = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyService() As Boolean
ApplyService = PutService
End Function

Public Function DeleteService() As Boolean
DeleteService = PurgeService
End Function

Public Function RetrieveService() As Boolean
GetService
RetrieveService = Not PracticeServiceNew
End Function

Public Function SelectService(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectService = False
If (PracticeServiceTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeServiceTotal) Then
    Exit Function
End If
PracticeServiceTbl.MoveFirst
PracticeServiceTbl.Move ListItem - 1
SelectService = True
Call LoadService
Exit Function
DbErrorHandler:
    ModuleName = "SelectService"
    SelectService = False
    Service = "-"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindService() As Long
FindService = GetServices(0)
End Function

Public Function FindServiceArranged() As Long
ServiceType = ""
FindServiceArranged = GetServices(1)
End Function

Public Function FindServiceReverse() As Long
ServiceType = ""
FindServiceReverse = GetServices(2)
End Function
Public Function FindServicesFast() As Long
FindServicesFast = GetServicesFast(1)
End Function


Public Function FindDiagnosis() As Long
FindDiagnosis = GetDiagnosis
End Function
