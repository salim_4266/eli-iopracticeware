VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeTransactionJournalLink"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeTransactionJournalLink Object Module
Option Explicit
Public TransLinkId As Long
Public TransLinkTransactionId As Long
Public TransLinkDate As String
Public TransLinkSrvId As Long
Public TransLinkAction As String
Public TransLinkStatus As String

Private ModuleName As String
Private TransactionJournalTotal As Long
Private TransactionJournalLinkTbl As ADODB.Recordset
Private TransactionJournalNew As Boolean

Private Sub Class_Initialize()
Set TransactionJournalLinkTbl = New ADODB.Recordset
Call InitTransactionJournal(True)
End Sub

Private Sub Class_Terminate()
Call InitTransactionJournal(False)
Set TransactionJournalLinkTbl = Nothing
End Sub

Private Sub InitTransactionJournal(WriteOn As Boolean)
TransactionJournalNew = WriteOn
TransLinkTransactionId = 0
TransLinkDate = ""
TransLinkSrvId = 0
TransLinkAction = ""
TransLinkStatus = "A"
End Sub

Private Sub LoadTransactionJournal()
TransactionJournalNew = False
If (TransactionJournalLinkTbl("TransLinkId") <> "") Then
    TransLinkId = TransactionJournalLinkTbl("TransLinkId")
End If
If (TransactionJournalLinkTbl("TransactionId") <> "") Then
    TransLinkTransactionId = TransactionJournalLinkTbl("TransactionId")
End If
If (TransactionJournalLinkTbl("TransLinkStatus") <> "") Then
    TransLinkStatus = TransactionJournalLinkTbl("TransLinkStatus")
End If
If (TransactionJournalLinkTbl("TransLinkAction") <> "") Then
    TransLinkAction = TransactionJournalLinkTbl("TransLinkAction")
End If
If (TransactionJournalLinkTbl("TransLinkDate") <> "") Then
    TransLinkDate = TransactionJournalLinkTbl("TransLinkDate")
End If
If (TransactionJournalLinkTbl("TransLinkSrvId") <> "") Then
    TransLinkSrvId = TransactionJournalLinkTbl("TranslinkSrvId")
End If
End Sub

Private Function GetTransactionJournal()
On Error GoTo DbErrorHandler
GetTransactionJournal = False
Dim strSQL As String
strSQL = "SELECT * FROM PracticeTransactionJournalLink WHERE TransLinkId = " + Trim(Str(TransLinkId)) + " "
Set TransactionJournalLinkTbl = Nothing
Set TransactionJournalLinkTbl = New ADODB.Recordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = strSQL
Call TransactionJournalLinkTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (TransactionJournalLinkTbl.EOF) Then
    Call InitTransactionJournal(True)
Else
    Call InitTransactionJournal(False)
    Call LoadTransactionJournal
End If
GetTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetTransactionJournalList() As Long
On Error GoTo DbErrorHandler
Dim l As Integer
Dim Ref As String
Dim strSQL As String
Ref = ""
TransactionJournalTotal = 0
GetTransactionJournalList = -1
If (Trim(TransLinkTransactionId) <> "") Then
    strSQL = "Select * FROM PracticeTransactionJournalLink WHERE "
    strSQL = strSQL + Ref + "TransactionId = " + Trim(Str(TransLinkTransactionId)) + " "
    Ref = "And "
    If (Trim(TransLinkSrvId) <> "") Then
        strSQL = strSQL + Ref + "TransLinkSrvId = " + Trim(Str(TransLinkSrvId)) + " "
        Ref = "And "
    End If
    If (Trim(TransLinkAction) <> "") Then
        strSQL = strSQL + Ref + "TransLinkAction = '" + Trim(TransLinkAction) + "' "
        Ref = "And "
    End If
    If (Trim(TransLinkStatus) <> "") Then
        If (Left(TransLinkStatus, 1) = "-") Then
            strSQL = strSQL + Ref + "TransLinkStatus <> '" + Trim(Mid(TransLinkStatus, 2, 1)) + "' "
        Else
            strSQL = strSQL + Ref + "TransLinkStatus = '" + Trim(TransLinkStatus) + "' "
        End If
        Ref = "And "
    End If
    strSQL = strSQL + "ORDER BY ModTime ASC "
    Set TransactionJournalLinkTbl = Nothing
    Set TransactionJournalLinkTbl = New ADODB.Recordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = strSQL
    Call TransactionJournalLinkTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (TransactionJournalLinkTbl.EOF) Then
        TransactionJournalLinkTbl.MoveLast
        TransactionJournalTotal = TransactionJournalLinkTbl.RecordCount
        GetTransactionJournalList = TransactionJournalLinkTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournalList"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutTransactionJournal()
On Error GoTo DbErrorHandler
PutTransactionJournal = False
If (TransactionJournalNew) Then
    TransactionJournalLinkTbl.AddNew
End If
TransactionJournalLinkTbl("TransactionId") = TransLinkTransactionId
TransactionJournalLinkTbl("TransLinkSrvId") = TransLinkSrvId
TransactionJournalLinkTbl("TransLinkDate") = UCase(Trim(TransLinkDate))
TransactionJournalLinkTbl("TransLinkAction") = UCase(Trim(TransLinkAction))
TransactionJournalLinkTbl("TransLinkStatus") = UCase(Trim(TransLinkStatus))
TransactionJournalLinkTbl.Update
PutTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "PutTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetTransactionJournalList > 0) Then
    If (SelectTransactionJournal(1)) Then
        GetJustAddedRecord = TransactionJournalLinkTbl("TransLinkId")
    End If
End If
End Function

Private Function PurgeTransactionJournal() As Boolean
On Error GoTo DbErrorHandler
PurgeTransactionJournal = False
TransactionJournalLinkTbl.Delete
PurgeTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "PurgeTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyTransactionJournal() As Boolean
ApplyTransactionJournal = PutTransactionJournal
End Function

Public Function DeleteTransactionJournal() As Boolean
DeleteTransactionJournal = PurgeTransactionJournal
End Function

Public Function RetrieveTransactionJournal() As Boolean
RetrieveTransactionJournal = GetTransactionJournal
End Function

Public Function SelectTransactionJournal(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectTransactionJournal = False
If (TransactionJournalLinkTbl.EOF) Or (ListItem < 1) Or (ListItem > TransactionJournalTotal) Then
    Exit Function
End If
TransactionJournalLinkTbl.MoveFirst
TransactionJournalLinkTbl.Move ListItem - 1
Call InitTransactionJournal(False)
Call LoadTransactionJournal
SelectTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "SelectTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindTransactionJournal() As Long
FindTransactionJournal = GetTransactionJournalList
End Function

