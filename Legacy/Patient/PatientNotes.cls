VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientNotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PatientNotes Object Module
Option Explicit
Public NotesId As Long
Public NotesType As String
Public NotesPatientId As Long
Public NotesAppointmentId As Long
Public NotesText1 As String
Public NotesText2 As String
Public NotesText3 As String
Public NotesText4 As String
Public NotesUser As String
Public NotesDate As String
Public NotesSystem As String
Public NotesEye As String
Public NotesCategory As String
Public NotesOffDate As String
Public NotesCommentOn As Boolean
Public NotesClaimOn As Boolean
Public NotesAudioOn As Boolean
Public NotesHighlight As String
Public NotesILPNRef As String
Public NotesAlertMask As String

Private ModuleName As String
Private NotesTotal As Long
Private NotesTbl As ADODB.Recordset
Private NotesNew As Boolean

Private Sub Class_Initialize()
Set NotesTbl = CreateAdoRecordset
Call InitNotes
End Sub

Private Sub Class_Terminate()
Call InitNotes
Set NotesTbl = Nothing
End Sub

Private Sub InitNotes()
NotesNew = True
NotesType = ""
NotesPatientId = 0
NotesAppointmentId = 0
NotesText1 = ""
NotesText2 = ""
NotesText3 = ""
NotesText4 = ""
NotesUser = ""
NotesDate = ""
NotesSystem = ""
NotesEye = ""
NotesCategory = ""
NotesOffDate = ""
NotesCommentOn = False
NotesClaimOn = False
NotesAudioOn = False
NotesHighlight = ""
NotesILPNRef = ""
NotesAlertMask = ""
End Sub

Private Sub LoadNotes()
NotesNew = False
If (NotesTbl("NoteId") <> "") Then
    NotesId = NotesTbl("NoteId")
Else
    NotesId = 0
End If
If (NotesTbl("PatientId") <> "") Then
    NotesPatientId = NotesTbl("PatientId")
Else
    NotesPatientId = 0
End If
If (NotesTbl("AppointmentId") <> "") Then
    NotesAppointmentId = NotesTbl("AppointmentId")
Else
    NotesAppointmentId = 0
End If
    If (NotesTbl("Note1") <> "") Then
        NotesText1 = Replace(NotesTbl("Note1"), vbTab, "  ")
    Else
        NotesText1 = ""
    End If
    If (NotesTbl("Note2") <> "") Then
        NotesText2 = Replace(NotesTbl("Note2"), vbTab, "  ")
    Else
        NotesText2 = ""
    End If
    If (NotesTbl("Note3") <> "") Then
        NotesText3 = Replace(NotesTbl("Note3"), vbTab, "  ")
    Else
        NotesText3 = ""
    End If
    If (NotesTbl("Note4") <> "") Then
        NotesText4 = Replace(NotesTbl("Note4"), vbTab, "  ")
    Else
        NotesText4 = ""
    End If
If (NotesTbl("NoteType") <> "") Then
    NotesType = NotesTbl("NoteType")
Else
    NotesType = ""
End If
If (NotesTbl("UserName") <> "") Then
    NotesUser = NotesTbl("UserName")
Else
    NotesUser = ""
End If
If (NotesTbl("NoteDate") <> "") Then
    NotesDate = NotesTbl("NoteDate")
Else
    NotesDate = ""
End If
If (NotesTbl("NoteSystem") <> "") Then
    NotesSystem = NotesTbl("NoteSystem")
Else
    NotesSystem = ""
End If
If (NotesTbl("NoteEye") <> "") Then
    NotesEye = NotesTbl("NoteEye")
Else
    NotesEye = ""
End If
If (NotesTbl("NoteOffDate") <> "") Then
    NotesOffDate = NotesTbl("NoteOffDate")
Else
    NotesOffDate = ""
End If
If (NotesTbl("NoteCategory") <> "") Then
    NotesCategory = NotesTbl("NoteCategory")
Else
    NotesCategory = ""
End If
If (NotesTbl("NoteHighlight") <> "") Then
    NotesHighlight = NotesTbl("NoteHighlight")
Else
    NotesHighlight = ""
End If
If (NotesTbl("NoteILPNRef") <> "") Then
    NotesILPNRef = NotesTbl("NoteILPNRef")
Else
    NotesILPNRef = ""
End If
If (NotesTbl("NoteAlertType") <> "") Then
    NotesAlertMask = NotesTbl("NoteAlertType")
Else
    NotesAlertMask = ""
End If
If (NotesTbl("NoteCommentOn") <> "") Then
    If (NotesTbl("NoteCommentOn") = "T") Then
        NotesCommentOn = True
    Else
        NotesCommentOn = False
    End If
Else
    NotesCommentOn = False
End If
If (NotesTbl("NoteClaimOn") <> "") Then
    If (NotesTbl("NoteClaimOn") = "T") Then
        NotesClaimOn = True
    Else
        NotesClaimOn = False
    End If
Else
    NotesClaimOn = False
End If
If (NotesTbl("NoteAudioOn") <> "") Then
    If (NotesTbl("NoteAudioOn") = "T") Then
        NotesAudioOn = True
    Else
        NotesAudioOn = False
    End If
Else
    NotesAudioOn = False
End If
End Sub

Private Function GetNotes()
On Error GoTo DbErrorHandler
GetNotes = False
Dim OrderString As String
OrderString = "SELECT * FROM PatientNotes WHERE NoteId =" + Trim(Str(NotesId)) + " "
Set NotesTbl = Nothing
Set NotesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call NotesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (NotesTbl.EOF) Then
    Call InitNotes
Else
    Call LoadNotes
End If
GetNotes = True
Exit Function
DbErrorHandler:
    ModuleName = "GetNotes"
    GetNotes = False
    NotesId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetNotesList() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
Ref = ""
NotesTotal = 0
GetNotesList = -1
If (Trim(NotesAppointmentId) < 1) And (NotesPatientId < 1) And (Trim(NotesSystem) = "") And (Trim(NotesType) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PatientNotes WHERE "
If (NotesAppointmentId > 0) Then
    OrderString = OrderString + Ref + "AppointmentId =" + Str(NotesAppointmentId) + " "
    Ref = "And "
End If
If (NotesPatientId > 0) Then
    OrderString = OrderString + Ref + "PatientId =" + Str(NotesPatientId) + " "
    Ref = "And "
End If
If (Trim(NotesType) <> "") Then
    OrderString = OrderString + Ref + "NoteType = '" + UCase(Trim(NotesType)) + "' "
    Ref = "And "
End If
If (Trim(NotesSystem) <> "") Then
    OrderString = OrderString + Ref + "NoteSystem = '" + UCase(Trim(NotesSystem)) + "' "
    Ref = "And "
End If
If (Trim(NotesDate) <> "") Then
    OrderString = OrderString + Ref + "NoteDate = '" + UCase(Trim(NotesDate)) + "' "
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY NoteType ASC, NoteId ASC "
Set NotesTbl = Nothing
Set NotesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call NotesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (NotesTbl.EOF) Then
    GetNotesList = -1
    Call InitNotes
Else
    NotesTbl.MoveLast
    NotesTotal = NotesTbl.RecordCount
    GetNotesList = NotesTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetNotesList"
    GetNotesList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetNotesforComplaints() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
Ref = ""
NotesTotal = 0
GetNotesforComplaints = -1
If (Trim(NotesAppointmentId) < 1) And (NotesPatientId < 1) And (Trim(NotesSystem) = "") And (Trim(NotesType) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PatientNotes WHERE "
If (NotesAppointmentId > 0) Then
    OrderString = OrderString + Ref + "AppointmentId =" + Str(NotesAppointmentId) + " "
    Ref = "And "
End If
If (NotesPatientId > 0) Then
    OrderString = OrderString + Ref + "PatientId =" + Str(NotesPatientId) + " "
    Ref = "And "
End If
If (Trim(NotesType) <> "") Then
    OrderString = OrderString + Ref + "NoteType = '" + UCase(Trim(NotesType)) + "' "
    Ref = "And "
End If
If (Trim(NotesSystem) <> "") Then
    OrderString = OrderString + Ref + "NoteSystem = '" + UCase(Left(Trim(NotesSystem), 32)) + "' "
    Ref = "And "
Else
    OrderString = OrderString + Ref + "(NoteSystem >= '1' And NoteSystem <= '9') "
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY NoteType ASC "
Set NotesTbl = Nothing
Set NotesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call NotesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (NotesTbl.EOF) Then
    GetNotesforComplaints = -1
    Call InitNotes
Else
    NotesTbl.MoveLast
    NotesTotal = NotesTbl.RecordCount
    GetNotesforComplaints = NotesTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetNotesforComplaints"
    GetNotesforComplaints = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetNotesbyPatient(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
NotesTotal = 0
GetNotesbyPatient = -1
OrderString = "Select * FROM PatientNotes WHERE PatientId =" + Str(NotesPatientId) + " "
If (NotesAppointmentId > 0) Then
    OrderString = OrderString + "And AppointmentId =" + Str(NotesAppointmentId) + " "
End If
If (Trim(NotesType) <> "") Then
    OrderString = OrderString + "And NoteType = '" + UCase(Trim(NotesType)) + "' "
End If
If (Trim(NotesSystem) <> "") Then
    If (InStrPS(NotesSystem, "~") <> 0) Then
        OrderString = OrderString + "And NoteSystem >= '" + UCase(Left(Trim(NotesSystem), Len(Trim(NotesSystem)) - 1)) + String(5, Chr(1)) + "' "
        OrderString = OrderString + "And NoteSystem <= '" + UCase(Left(Trim(NotesSystem), Len(Trim(NotesSystem)) - 1)) + String(5, Chr(255)) + "' "
    Else
        OrderString = OrderString + "And (NoteSystem = '" + UCase(Trim(NotesSystem)) + "' Or NoteSystem Like '%" + UCase(Trim(NotesSystem)) + "%') "
    End If
End If
If (Trim(NotesEye) <> "") Then
    OrderString = OrderString + "And NoteEye = '" + UCase(Trim(NotesEye)) + "' "
End If
If (IType = 1) Then
    OrderString = OrderString + "And NoteAlertType <> '' "
    OrderString = OrderString + "And (Not NoteOffDate <> '') "
End If
OrderString = OrderString + "ORDER BY NoteType ASC, NoteDate DESC, NoteId ASC "
Set NotesTbl = Nothing
Set NotesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call NotesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (NotesTbl.EOF) Then
    GetNotesbyPatient = -1
    Call InitNotes
Else
    NotesTbl.MoveLast
    NotesTotal = NotesTbl.RecordCount
    GetNotesbyPatient = NotesTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetNotesbyPatient"
    GetNotesbyPatient = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetNotesbyHighlight() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
NotesTotal = 0
GetNotesbyHighlight = -1
OrderString = "Select * FROM PatientNotes WHERE PatientId =" + Str(NotesPatientId) + " "
If (NotesAppointmentId > 0) Then
    OrderString = OrderString + "And AppointmentId =" + Str(NotesAppointmentId) + " "
End If
If (Trim(NotesType) <> "") Then
    OrderString = OrderString + "And NoteType = '" + UCase(Trim(NotesType)) + "' "
End If
If (Trim(NotesSystem) <> "") Then
    If (InStrPS(NotesSystem, "~") <> 0) Then
        OrderString = OrderString + "And NoteSystem >= '" + UCase(Left(Trim(NotesSystem), Len(Trim(NotesSystem)) - 1)) + String(5, Chr(1)) + "' "
        OrderString = OrderString + "And NoteSystem <= '" + UCase(Left(Trim(NotesSystem), Len(Trim(NotesSystem)) - 1)) + String(5, Chr(255)) + "' "
    Else
        OrderString = OrderString + "And NoteSystem = '" + UCase(Trim(NotesSystem)) + "' "
    End If
End If
If (Trim(NotesHighlight) <> "") Then
    If (NotesHighlight = "]") Then
        OrderString = OrderString + "And NoteHighlight = ']' "
    Else
        OrderString = OrderString + "And NoteHighlight = 'V' "
    End If
End If
OrderString = OrderString + "ORDER BY NoteType ASC, NoteDate DESC, NoteId ASC "
Set NotesTbl = Nothing
Set NotesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call NotesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (NotesTbl.EOF) Then
    GetNotesbyHighlight = -1
    Call InitNotes
Else
    NotesTbl.MoveLast
    NotesTotal = NotesTbl.RecordCount
    GetNotesbyHighlight = NotesTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetNotesbyHighlight"
    GetNotesbyHighlight = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutNotes()
On Error GoTo DbErrorHandler
PutNotes = True
If (NotesNew) Then
    NotesTbl.AddNew
End If
NotesTbl("PatientId") = NotesPatientId
NotesTbl("AppointmentId") = NotesAppointmentId
NotesTbl("Note1") = NotesText1
NotesTbl("Note2") = NotesText2
NotesTbl("Note3") = NotesText3
NotesTbl("Note4") = NotesText4
NotesTbl("NoteType") = NotesType
NotesTbl("UserName") = Trim(NotesUser)
NotesTbl("NoteDate") = Trim(NotesDate)
NotesTbl("NoteSystem") = Trim(NotesSystem)
NotesTbl("NoteEye") = Trim(NotesEye)
NotesTbl("NoteOffDate") = Trim(NotesOffDate)
NotesTbl("NoteCategory") = Trim(NotesCategory)
NotesTbl("NoteAlertType") = Trim(NotesAlertMask)
NotesTbl("NoteHighlight") = NotesHighlight
NotesTbl("NoteILPNRef") = NotesILPNRef
NotesTbl("NoteCommentOn") = "F"
If (NotesCommentOn) Then
    NotesTbl("NoteCommentOn") = "T"
End If
NotesTbl("NoteClaimOn") = "F"
If (NotesClaimOn) Then
    NotesTbl("NoteClaimOn") = "T"
End If
NotesTbl("NoteAudioOn") = "F"
If (NotesAudioOn) Then
    NotesTbl("NoteAudioOn") = "T"
End If
NotesTbl.Update
If (NotesNew) Then
    NotesNew = False
    NotesId = GetJustAddedRecord
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutNotes"
    PutNotes = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetNotesList > 0) Then
    If (SelectNotes(1)) Then
        GetJustAddedRecord = NotesTbl("NoteId")
    End If
End If
End Function

Public Function ApplyNotes() As Boolean
ApplyNotes = PutNotes
End Function

Public Function RetrieveNotes() As Boolean
RetrieveNotes = GetNotes
End Function

Public Function DeleteNotes() As Boolean
DeleteNotes = PurgeNote
End Function

Public Function SelectNotes(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectNotes = False
If (NotesTbl.EOF) Or (ListItem < 1) Or (ListItem > NotesTotal) Then
    Exit Function
End If
NotesTbl.MoveFirst
NotesTbl.Move ListItem - 1
SelectNotes = True
Call LoadNotes
Exit Function
DbErrorHandler:
    ModuleName = "SelectNotes"
    SelectNotes = False
    NotesId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindNotes() As Long
FindNotes = GetNotesList()
End Function

Public Function FindNotesforComplaints() As Long
FindNotesforComplaints = GetNotesforComplaints()
End Function

Public Function FindNotesbyPatient() As Long
FindNotesbyPatient = GetNotesbyPatient(0)
End Function

Public Function FindNotesbyPatientAlert() As Long
FindNotesbyPatientAlert = GetNotesbyPatient(1)
End Function

Public Function FindNotesbyHighlight() As Long
FindNotesbyHighlight = GetNotesbyHighlight()
End Function

Public Function IsPrivateNotes() As Boolean
Dim ThoseFound As Integer
IsPrivateNotes = False
If (NotesAppointmentId < 1) Then
    Exit Function
End If
NotesType = "P"
ThoseFound = GetNotesList()
If (ThoseFound > 0) Then
    IsPrivateNotes = True
End If
End Function

Public Function IsChartNotes() As Boolean
Dim ThoseFound As Integer
IsChartNotes = False
If (NotesAppointmentId < 1) Then
    Exit Function
End If
NotesType = "C"
ThoseFound = GetNotesList()
If (ThoseFound > 0) Then
    IsChartNotes = True
End If
End Function

Private Function PurgeNote() As Boolean
On Error GoTo DbErrorHandler
PurgeNote = True
NotesTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeNote"
    PurgeNote = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
