VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeModifierRules"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Modifierss Object Module
Option Explicit
Public ModifierId As Long
Public ModifierClassId As String
Public ModifierCPT As String
Public ModifierRule1 As String
Public ModifierRule1Criteria As String
Public ModifierRule2 As String
Public ModifierRule2Criteria As String
Public ModifierRule3 As String
Public ModifierRule3Criteria As String
Public ModifierRule4 As String
Public ModifierRule4Criteria As String
Public ModifierRule5 As String
Public ModifierRule5Criteria As String
Public ModifierRule6 As String
Public ModifierRule6Criteria As String
Public ModifierRule7 As String
Public ModifierRule7Criteria As String
Public ModifierRule8 As String
Public ModifierRule8Criteria As String
Public ModifierRule9 As String
Public ModifierRule9Criteria As String
Public ModifierRule10 As String
Public ModifierRule10Criteria As String
Public ModifierRule11 As String
Public ModifierRule11Criteria As String
Public ModifierRule12 As String
Public ModifierRule12Criteria As String
Public ModifierRule13 As String
Public ModifierRule13Criteria As String
Public ModifierRule14 As String
Public ModifierRule14Criteria As String
Public ModifierRule15 As String
Public ModifierRule15Criteria As String
Public ModifierRuleStatus As String

Private ModuleName As String
Private PracticeModifiersTotal As Long
Private PracticeModifiersTbl As ADODB.Recordset
Private PracticeModifiersNew As Boolean

Private Sub Class_Initialize()
Set PracticeModifiersTbl = CreateAdoRecordset
Call InitModifierRule
End Sub

Private Sub Class_Terminate()
Call InitModifierRule
Set PracticeModifiersTbl = Nothing
End Sub

Private Sub InitModifierRule()
PracticeModifiersNew = True
ModifierClassId = ""
ModifierCPT = ""
ModifierRule1 = ""
ModifierRule1Criteria = ""
ModifierRule2 = ""
ModifierRule2Criteria = ""
ModifierRule3 = ""
ModifierRule3Criteria = ""
ModifierRule4 = ""
ModifierRule4Criteria = ""
ModifierRule5 = ""
ModifierRule5Criteria = ""
ModifierRule6 = ""
ModifierRule6Criteria = ""
ModifierRule7 = ""
ModifierRule7Criteria = ""
ModifierRule8 = ""
ModifierRule8Criteria = ""
ModifierRule9 = ""
ModifierRule9Criteria = ""
ModifierRule10 = ""
ModifierRule10Criteria = ""
ModifierRule11 = ""
ModifierRule11Criteria = ""
ModifierRule12 = ""
ModifierRule12Criteria = ""
ModifierRule13 = ""
ModifierRule13Criteria = ""
ModifierRule14 = ""
ModifierRule14Criteria = ""
ModifierRule15 = ""
ModifierRule15Criteria = ""
ModifierRuleStatus = "A"
End Sub

Private Sub LoadModifierRule()
PracticeModifiersNew = False
ModifierId = PracticeModifiersTbl("RefId")
If (PracticeModifiersTbl("BusinessClassId") <> "") Then
    ModifierClassId = PracticeModifiersTbl("BusinessClassId")
Else
    ModifierClassId = ""
End If
If (PracticeModifiersTbl("CPT") <> "") Then
    ModifierCPT = PracticeModifiersTbl("CPT")
Else
    ModifierCPT = ""
End If
If (PracticeModifiersTbl("Rule1") <> "") Then
    ModifierRule1 = PracticeModifiersTbl("Rule1")
Else
    ModifierRule1 = ""
End If
If (PracticeModifiersTbl("Description1") <> "") Then
    ModifierRule1Criteria = PracticeModifiersTbl("Description1")
Else
    ModifierRule1Criteria = ""
End If
If (PracticeModifiersTbl("Rule2") <> "") Then
    ModifierRule2 = PracticeModifiersTbl("Rule2")
Else
    ModifierRule2 = ""
End If
If (PracticeModifiersTbl("Description2") <> "") Then
    ModifierRule2Criteria = PracticeModifiersTbl("Description2")
Else
    ModifierRule2Criteria = ""
End If
If (PracticeModifiersTbl("Rule3") <> "") Then
    ModifierRule3 = PracticeModifiersTbl("Rule3")
Else
    ModifierRule3 = ""
End If
If (PracticeModifiersTbl("Description3") <> "") Then
    ModifierRule3Criteria = PracticeModifiersTbl("Description3")
Else
    ModifierRule3Criteria = ""
End If
If (PracticeModifiersTbl("Rule4") <> "") Then
    ModifierRule4 = PracticeModifiersTbl("Rule4")
Else
    ModifierRule4 = ""
End If
If (PracticeModifiersTbl("Description4") <> "") Then
    ModifierRule4Criteria = PracticeModifiersTbl("Description4")
Else
    ModifierRule4Criteria = ""
End If
If (PracticeModifiersTbl("Rule5") <> "") Then
    ModifierRule5 = PracticeModifiersTbl("Rule5")
Else
    ModifierRule5 = ""
End If
If (PracticeModifiersTbl("Description5") <> "") Then
    ModifierRule5Criteria = PracticeModifiersTbl("Description5")
Else
    ModifierRule5Criteria = ""
End If
If (PracticeModifiersTbl("Rule6") <> "") Then
    ModifierRule6 = PracticeModifiersTbl("Rule6")
Else
    ModifierRule6 = ""
End If
If (PracticeModifiersTbl("Description6") <> "") Then
    ModifierRule6Criteria = PracticeModifiersTbl("Description6")
Else
    ModifierRule6Criteria = ""
End If
If (PracticeModifiersTbl("Rule7") <> "") Then
    ModifierRule7 = PracticeModifiersTbl("Rule7")
Else
    ModifierRule7 = ""
End If
If (PracticeModifiersTbl("Description7") <> "") Then
    ModifierRule7Criteria = PracticeModifiersTbl("Description7")
Else
    ModifierRule7Criteria = ""
End If
If (PracticeModifiersTbl("Rule8") <> "") Then
    ModifierRule8 = PracticeModifiersTbl("Rule8")
Else
    ModifierRule8 = ""
End If
If (PracticeModifiersTbl("Description8") <> "") Then
    ModifierRule8Criteria = PracticeModifiersTbl("Description8")
Else
    ModifierRule8Criteria = ""
End If
If (PracticeModifiersTbl("Rule9") <> "") Then
    ModifierRule9 = PracticeModifiersTbl("Rule9")
Else
    ModifierRule9 = ""
End If
If (PracticeModifiersTbl("Description9") <> "") Then
    ModifierRule9Criteria = PracticeModifiersTbl("Description9")
Else
    ModifierRule9Criteria = ""
End If
If (PracticeModifiersTbl("Rule10") <> "") Then
    ModifierRule10 = PracticeModifiersTbl("Rule10")
Else
    ModifierRule10 = ""
End If
If (PracticeModifiersTbl("Description10") <> "") Then
    ModifierRule10Criteria = PracticeModifiersTbl("Description10")
Else
    ModifierRule10Criteria = ""
End If
If (PracticeModifiersTbl("Rule11") <> "") Then
    ModifierRule11 = PracticeModifiersTbl("Rule11")
Else
    ModifierRule11 = ""
End If
If (PracticeModifiersTbl("Description11") <> "") Then
    ModifierRule11Criteria = PracticeModifiersTbl("Description11")
Else
    ModifierRule11Criteria = ""
End If
If (PracticeModifiersTbl("Rule12") <> "") Then
    ModifierRule12 = PracticeModifiersTbl("Rule12")
Else
    ModifierRule12 = ""
End If
If (PracticeModifiersTbl("Description12") <> "") Then
    ModifierRule12Criteria = PracticeModifiersTbl("Description12")
Else
    ModifierRule12Criteria = ""
End If
If (PracticeModifiersTbl("Rule13") <> "") Then
    ModifierRule13 = PracticeModifiersTbl("Rule13")
Else
    ModifierRule13 = ""
End If
If (PracticeModifiersTbl("Description13") <> "") Then
    ModifierRule13Criteria = PracticeModifiersTbl("Description13")
Else
    ModifierRule13Criteria = ""
End If
If (PracticeModifiersTbl("Rule14") <> "") Then
    ModifierRule14 = PracticeModifiersTbl("Rule14")
Else
    ModifierRule14 = ""
End If
If (PracticeModifiersTbl("Description14") <> "") Then
    ModifierRule14Criteria = PracticeModifiersTbl("Description14")
Else
    ModifierRule14Criteria = ""
End If
If (PracticeModifiersTbl("Rule15") <> "") Then
    ModifierRule15 = PracticeModifiersTbl("Rule15")
Else
    ModifierRule15 = ""
End If
If (PracticeModifiersTbl("Description15") <> "") Then
    ModifierRule15Criteria = PracticeModifiersTbl("Description15")
Else
    ModifierRule15Criteria = ""
End If
If (PracticeModifiersTbl("Status") <> "") Then
    ModifierRuleStatus = PracticeModifiersTbl("Status")
Else
    ModifierRuleStatus = 0
End If
End Sub

Private Function GetModifierRule() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
PracticeModifiersTotal = 0
GetModifierRule = -1
OrderString = "Select * FROM ModifierRules WHERE "
If (Trim(ModifierClassId) <> "") Then
    OrderString = OrderString + "BusinessClassId = '" + UCase(Trim(ModifierClassId)) + "' "
    Ref = "And "
End If
If (Trim(ModifierCPT) <> "") Then
    OrderString = OrderString + Ref + "CPT = '" + UCase(Trim(ModifierCPT)) + "' "
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY BusinessClassId ASC, CPT ASC "
Set PracticeModifiersTbl = Nothing
Set PracticeModifiersTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeModifiersTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeModifiersTbl.EOF) Then
    Call InitModifierRule
Else
    PracticeModifiersTbl.MoveLast
    PracticeModifiersTotal = PracticeModifiersTbl.RecordCount
    GetModifierRule = PracticeModifiersTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetModifierRule"
    GetModifierRule = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAModifierRule() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetAModifierRule = True
OrderString = "SELECT * FROM ModifierRules WHERE RefId = " + Trim(Str(ModifierId)) + " "
Set PracticeModifiersTbl = Nothing
Set PracticeModifiersTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeModifiersTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeModifiersTbl.EOF) Then
    Call InitModifierRule
Else
    Call LoadModifierRule
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAModifierRule"
    GetAModifierRule = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutModifierRule() As Boolean
On Error GoTo DbErrorHandler
PutModifierRule = True
If (PracticeModifiersNew) Then
    PracticeModifiersTbl.AddNew
End If
PracticeModifiersTbl("BusinessClassId") = ModifierClassId
PracticeModifiersTbl("CPT") = ModifierCPT
PracticeModifiersTbl("Rule1") = ModifierRule1
PracticeModifiersTbl("Rule2") = ModifierRule2
PracticeModifiersTbl("Rule3") = ModifierRule3
PracticeModifiersTbl("Rule4") = ModifierRule4
PracticeModifiersTbl("Rule5") = ModifierRule5
PracticeModifiersTbl("Rule6") = ModifierRule6
PracticeModifiersTbl("Rule7") = ModifierRule7
PracticeModifiersTbl("Rule8") = ModifierRule8
PracticeModifiersTbl("Rule9") = ModifierRule9
PracticeModifiersTbl("Rule10") = ModifierRule10
PracticeModifiersTbl("Rule11") = ModifierRule11
PracticeModifiersTbl("Rule12") = ModifierRule12
PracticeModifiersTbl("Rule13") = ModifierRule13
PracticeModifiersTbl("Rule14") = ModifierRule14
PracticeModifiersTbl("Rule15") = ModifierRule15
PracticeModifiersTbl("Description1") = ModifierRule1Criteria
PracticeModifiersTbl("Description2") = ModifierRule2Criteria
PracticeModifiersTbl("Description3") = ModifierRule3Criteria
PracticeModifiersTbl("Description4") = ModifierRule4Criteria
PracticeModifiersTbl("Description5") = ModifierRule5Criteria
PracticeModifiersTbl("Description6") = ModifierRule6Criteria
PracticeModifiersTbl("Description7") = ModifierRule7Criteria
PracticeModifiersTbl("Description8") = ModifierRule8Criteria
PracticeModifiersTbl("Description9") = ModifierRule9Criteria
PracticeModifiersTbl("Description10") = ModifierRule10Criteria
PracticeModifiersTbl("Description11") = ModifierRule11Criteria
PracticeModifiersTbl("Description12") = ModifierRule12Criteria
PracticeModifiersTbl("Description13") = ModifierRule13Criteria
PracticeModifiersTbl("Description14") = ModifierRule14Criteria
PracticeModifiersTbl("Description15") = ModifierRule15Criteria
PracticeModifiersTbl("Status") = ModifierRuleStatus
PracticeModifiersTbl.Update
If (PracticeModifiersNew) Then
    ModifierId = GetJustAddedRecord
    PracticeModifiersNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutModifierRule"
    PutModifierRule = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As String
GetJustAddedRecord = 0
If (GetModifierRule > 0) Then
    If (SelectModifierRule(1)) Then
        GetJustAddedRecord = PracticeModifiersTbl("RefId")
    End If
End If
End Function

Private Function PurgeModifierRule() As Boolean
On Error GoTo DbErrorHandler
PurgeModifierRule = True
PracticeModifiersTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeModifierRule"
    PurgeModifierRule = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyModifierRule() As Boolean
ApplyModifierRule = PutModifierRule
End Function

Public Function DeleteModifierRule() As Boolean
DeleteModifierRule = PurgeModifierRule
End Function

Public Function RetrieveModifierRule() As Boolean
RetrieveModifierRule = GetAModifierRule
End Function

Public Function FindModifierRule() As Long
FindModifierRule = GetModifierRule
End Function

Public Function SelectModifierRule(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectModifierRule = False
If (PracticeModifiersTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeModifiersTotal) Then
    Exit Function
End If
PracticeModifiersTbl.MoveFirst
PracticeModifiersTbl.Move ListItem - 1
SelectModifierRule = True
Call LoadModifierRule
Exit Function
DbErrorHandler:
    ModuleName = "SelectModifierRule"
    SelectModifierRule = False
    ModifierId = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
