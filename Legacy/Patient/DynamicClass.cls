VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicClass Object Module
Option Explicit
Public ClassId As Long
Public QuestionSet As String
Public QuestionParty As String
Public Question As String
Public QuestionOrder As String
Public ControlName As String
Public IEQuestion As String
Public IEQuestionType As String
Public QuestionFamily As Integer
Public TimeOut As Integer
Public MergeId As Integer
Public FormatMaskId As Integer
Public Highlight As String
Public FollowUp As String
Public QuestionIndicator As String
Public DIPrintOrder As Integer

Private ModuleName As String
Private DynamicClassTotal As Integer
Private ClassQuestionsTbl As ADODB.Recordset
Private ClassQuestionsNew As Boolean

Private Sub Class_Initialize()
Set ClassQuestionsTbl = New ADODB.Recordset
ClassQuestionsTbl.CursorLocation = adUseClient
Call InitClass
End Sub

Private Sub Class_Terminate()
Call InitClass
Set ClassQuestionsTbl = Nothing
End Sub

Private Sub InitClass()
ClassQuestionsNew = False
Question = ""
QuestionParty = ""
QuestionOrder = ""
QuestionSet = ""
ControlName = ""
IEQuestion = ""
IEQuestionType = ""
QuestionFamily = 0
TimeOut = 0
MergeId = 0
FormatMaskId = 0
Highlight = ""
FollowUp = ""
QuestionIndicator = ""
DIPrintOrder = 0
End Sub

Private Sub LoadClass()
ClassQuestionsNew = False
ClassId = ClassQuestionsTbl("ClassId")
If (ClassQuestionsTbl("Question") <> "") Then
    Question = ClassQuestionsTbl("Question")
Else
    Question = ""
End If
If (ClassQuestionsTbl("QuestionOrder") <> "") Then
    QuestionOrder = ClassQuestionsTbl("QuestionOrder")
Else
    QuestionOrder = 0
End If
If (ClassQuestionsTbl("QuestionParty") <> "") Then
    QuestionParty = ClassQuestionsTbl("QuestionParty")
Else
    QuestionParty = ""
End If
If (ClassQuestionsTbl("QuestionSet") <> "") Then
    QuestionSet = ClassQuestionsTbl("QuestionSet")
Else
    QuestionSet = ""
End If
If (ClassQuestionsTbl("ControlName") <> "") Then
    ControlName = ClassQuestionsTbl("ControlName")
Else
    ControlName = ""
End If
If (ClassQuestionsTbl("InferenceEngineQuestion") <> "") Then
    IEQuestion = ClassQuestionsTbl("InferenceEngineQuestion")
Else
    IEQuestion = ""
End If
If (ClassQuestionsTbl("InferenceEngineQuestionType") <> "") Then
    IEQuestionType = ClassQuestionsTbl("InferenceEngineQuestionType")
Else
    IEQuestionType = ""
End If
If (ClassQuestionsTbl("ProcOrTest") <> "") Then
    QuestionIndicator = ClassQuestionsTbl("ProcOrTest")
Else
    QuestionIndicator = ""
End If
If (ClassQuestionsTbl("Highlight") <> "") Then
    Highlight = ClassQuestionsTbl("Highlight")
Else
    Highlight = ""
End If
If (ClassQuestionsTbl("Followup") <> "") Then
    FollowUp = ClassQuestionsTbl("Followup")
Else
    FollowUp = ""
End If
If (ClassQuestionsTbl("Family") <> "") Then
    QuestionFamily = ClassQuestionsTbl("Family")
Else
    QuestionFamily = 0
End If
If (ClassQuestionsTbl("Timeout") <> "") Then
    TimeOut = ClassQuestionsTbl("Timeout")
Else
    TimeOut = 0
End If
If (ClassQuestionsTbl("MergeId") <> "") Then
    MergeId = ClassQuestionsTbl("MergeId")
Else
    MergeId = 0
End If
If (ClassQuestionsTbl("FormatMaskId") <> "") Then
    FormatMaskId = ClassQuestionsTbl("FormatMaskId")
Else
    FormatMaskId = 0
End If
If (ClassQuestionsTbl("PrintOrder") <> "") Then
    DIPrintOrder = ClassQuestionsTbl("PrintOrder")
Else
    DIPrintOrder = 0
End If
End Sub

Private Function GetClass()
On Error GoTo DbErrorHandler
Dim OrderString As String
GetClass = True
If (ClassId < 1) Then
    Call InitClass
    Exit Function
End If
OrderString = "SELECT * FROM Questions WHERE ClassId =" + Str(ClassId) + " "
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    Call InitClass
Else
    Call LoadClass
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetClass"
    Call PinpointPRError(ModuleName, Err)
    ClassId = -256
    GetClass = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetClassbyQuestion() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
Dim Ref As String
GetClassbyQuestion = False
If (Trim(Question) = "") And (Trim(QuestionSet) = "") Then
    Exit Function
End If
Ref = ""
OrderString = "SELECT * FROM Questions WHERE "
If (Trim(Question) <> "") Then
    OrderString = OrderString + "Question = '" + Trim(Question) + "' "
    Ref = "And "
End If
If (Trim(QuestionSet) <> "") Then
    OrderString = OrderString + Ref + "QuestionSet = '" + Trim(QuestionSet) + "' "
    Ref = "And "
End If
If (Trim(QuestionOrder) <> "") Then
    OrderString = OrderString + Ref + "QuestionOrder = '" + Trim(QuestionOrder) + "' "
    Ref = "And "
End If
If (Trim(QuestionParty) <> "") Then
    If (QuestionParty = "T") Then
        OrderString = OrderString + Ref + "((QuestionParty = 'T') Or (QuestionParty = 'X') Or (QuestionParty = 'Y')) "
    Else
        OrderString = OrderString + Ref + "QuestionParty = '" + Trim(QuestionParty) + "' "
    End If
End If
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (ClassQuestionsTbl.EOF) Then
    GetClassbyQuestion = True
    Call LoadClass
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetClassbyQuestion"
    Call PinpointPRError(ModuleName, Err)
    GetClassbyQuestion = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllClassForms(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetAllClassForms = -1
If (Trim(QuestionParty) = "") And (Trim(QuestionSet) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM Questions WHERE QuestionParty = '" + UCase(Trim(QuestionParty)) + "' "
If (Trim(Question) <> "") Then
    OrderString = OrderString + "And Question = '" + Trim(Question) + "' "
End If
If (Trim(QuestionSet) <> "") Then
    OrderString = OrderString + "And QuestionSet = '" + Trim(QuestionSet) + "' "
End If
If (Trim(QuestionOrder) <> "") Then
    OrderString = OrderString + "And QuestionOrder = '" + Trim(QuestionOrder) + "' "
End If
If (IType = 0) Then
    OrderString = OrderString + "ORDER BY QuestionOrder ASC "
Else
    OrderString = OrderString + "ORDER BY Question ASC "
End If
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetAllClassForms = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetAllClassForms = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllClassForms"
    Call PinpointPRError(ModuleName, Err)
    GetAllClassForms = -256
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetQuestionForms(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetQuestionForms = -1
If (Trim(Question) = "") Then
    Exit Function
End If
If (Len(Question) > 1) Then
    OrderString = "SELECT * FROM Questions WHERE Question = '" + Trim(Question) + "' "
Else
    OrderString = "SELECT * FROM Questions WHERE Question >= '" + Trim(Question) + "' "
End If
If (Trim(QuestionSet) <> "") Then
    OrderString = OrderString + "And QuestionSet = '" + Trim(QuestionSet) + "' "
End If
If (Trim(QuestionParty) <> "") Then
    OrderString = OrderString + "And QuestionParty = '" + Trim(QuestionParty) + "' "
End If
If (Trim(QuestionOrder) <> "") Then
    OrderString = OrderString + "And QuestionOrder = '" + Trim(QuestionOrder) + "' "
End If
If (IType = 1) Then
    OrderString = OrderString + "ORDER BY Question ASC "
End If
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetQuestionForms = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetQuestionForms = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetQuestionForms"
    Call PinpointPRError(ModuleName, Err)
    GetQuestionForms = -256
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetQuestionbyFamily() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetQuestionbyFamily = -1
If (Trim(QuestionFamily) < 1) And (Trim(QuestionSet) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM Questions WHERE Family =" + Str(QuestionFamily) + " And QuestionSet = '" + Trim(QuestionSet) + "' "
OrderString = OrderString + "ORDER BY QuestionOrder ASC "
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetQuestionbyFamily = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetQuestionbyFamily = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetQuestionbyFamily"
    If (Err <> 3705) Then
        Call PinpointPRError(ModuleName, Err)
        GetQuestionbyFamily = -256
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetQuestionbyMergeId() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetQuestionbyMergeId = -1
If (Trim(QuestionParty) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM Questions WHERE QuestionParty = '" + Trim(QuestionParty) + "' "
OrderString = OrderString + "ORDER BY MergeId ASC, ClassId ASC "
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetQuestionbyMergeId = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetQuestionbyMergeId = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetQuestionbyMergeId"
    If (Err <> 3705) Then
        Call PinpointPRError(ModuleName, Err)
        GetQuestionbyMergeId = -256
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetQuestionbyFormatMask() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetQuestionbyFormatMask = -1
If (Trim(QuestionParty) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM Questions WHERE QuestionParty = '" + Trim(QuestionParty) + "' "
OrderString = OrderString + "ORDER BY FormatMaskId ASC, ClassId ASC "
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetQuestionbyFormatMask = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetQuestionbyFormatMask = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetQuestionbyFormatMask"
    If (Err <> 3705) Then
        Call PinpointPRError(ModuleName, Err)
        GetQuestionbyFormatMask = -256
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetQuestionbyControlName() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetQuestionbyControlName = -1
If (Trim(ControlName) <> "") Then
    OrderString = "SELECT * FROM Questions WHERE QuestionParty = 'T' And ControlName Like '%" + Trim(ControlName) + "%' "
    OrderString = OrderString + "ORDER BY QuestionOrder ASC "
    MyDynamicFormsCmd.CommandText = OrderString
    Set ClassQuestionsTbl = Nothing
    Set ClassQuestionsTbl = New ADODB.Recordset
    Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
    If (ClassQuestionsTbl.EOF) Then
        GetQuestionbyControlName = -1
    Else
        ClassQuestionsTbl.MoveLast
        DynamicClassTotal = ClassQuestionsTbl.RecordCount
        GetQuestionbyControlName = ClassQuestionsTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetQuestionbyControlName"
    If (Err <> 3705) Then
        Call PinpointPRError(ModuleName, Err)
        GetQuestionbyControlName = -256
    End If
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetDistinctControlNames() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetDistinctControlNames = True
OrderString = "SELECT DISTINCT ControlName FROM Questions WHERE ControlName <> ''"
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetDistinctControlNames = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetDistinctControlNames = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDistinctControlNames"
    Call PinpointPRError(ModuleName, Err)
    ClassId = -256
    GetDistinctControlNames = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetClassesByControlName() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
DynamicClassTotal = 0
GetClassesByControlName = -1
OrderString = "SELECT * FROM Questions WHERE "
If (Trim(ControlName) <> "") Then
    OrderString = OrderString + "ControlName LIKE '" + Trim(ControlName) + "' "
Else
    Exit Function
End If
MyDynamicFormsCmd.CommandText = OrderString
Set ClassQuestionsTbl = Nothing
Set ClassQuestionsTbl = New ADODB.Recordset
Call ClassQuestionsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClassQuestionsTbl.EOF) Then
    GetClassesByControlName = -1
Else
    ClassQuestionsTbl.MoveLast
    DynamicClassTotal = ClassQuestionsTbl.RecordCount
    GetClassesByControlName = ClassQuestionsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetClassesByControlName"
    Call PinpointPRError(ModuleName, Err)
    GetClassesByControlName = -256
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutClass()
On Error GoTo DbErrorHandler
PutClass = True
If (ClassQuestionsNew) Then
    ClassQuestionsTbl.AddNew
End If
ClassQuestionsTbl("QuestionParty") = UCase(Trim(QuestionParty))
ClassQuestionsTbl("Question") = UCase(Trim(Question))
ClassQuestionsTbl("QuestionOrder") = UCase(Trim(QuestionOrder))
ClassQuestionsTbl("QuestionSet") = Trim(QuestionSet)
ClassQuestionsTbl("InferenceEngineQuestion") = Trim(IEQuestion)
ClassQuestionsTbl("InferenceEngineQuestionType") = UCase(Trim(IEQuestionType))
ClassQuestionsTbl("ProcOrTest") = QuestionIndicator
ClassQuestionsTbl("Highlight") = Highlight
ClassQuestionsTbl("Followup") = FollowUp
ClassQuestionsTbl("ControlName") = Trim(ControlName)
ClassQuestionsTbl("Family") = QuestionFamily
ClassQuestionsTbl("Timeout") = TimeOut
ClassQuestionsTbl("MergeId") = MergeId
ClassQuestionsTbl("FormatMaskId") = FormatMaskId
ClassQuestionsTbl("PrintOrder") = DIPrintOrder
ClassQuestionsTbl.Update
If (ClassQuestionsNew) Then
    ClassId = GetJustAddedRecord
    ClassQuestionsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutClass"
    Call PinpointPRError(ModuleName, Err)
    PutClass = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetClassbyQuestion > 0) Then
    If (SelectClassForm(1)) Then
        GetJustAddedRecord = ClassQuestionsTbl("ClassId")
    End If
End If
End Function

Public Function SelectClassForm(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectClassForm = False
If (ClassQuestionsTbl.EOF) Or (ListItem < 1) Or (ListItem > DynamicClassTotal) Then
    Exit Function
End If
ClassQuestionsTbl.MoveFirst
ClassQuestionsTbl.Move ListItem - 1
SelectClassForm = True
Call LoadClass
Exit Function
DbErrorHandler:
    ModuleName = "SelectClassForm"
    Call PinpointPRError(ModuleName, Err)
    ClassId = -256
    SelectClassForm = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectControlName(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectControlName = False
If (ClassQuestionsTbl.EOF) Or (ListItem < 1) Or (ListItem > DynamicClassTotal) Then
    Exit Function
End If
ClassQuestionsTbl.MoveFirst
ClassQuestionsTbl.Move ListItem - 1
SelectControlName = True
If (ClassQuestionsTbl("ControlName") <> "") Then
    ControlName = ClassQuestionsTbl("ControlName")
Else
    ControlName = ""
End If
Exit Function
DbErrorHandler:
    ModuleName = "SelectControlName"
    Call PinpointPRError(ModuleName, Err)
    ClassId = -256
    SelectControlName = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyClass() As Boolean
ApplyClass = PutClass
End Function

Public Function RetrieveClass() As Boolean
RetrieveClass = GetClass
End Function

Public Function RetrieveClassbyQuestion() As Boolean
RetrieveClassbyQuestion = GetClassbyQuestion
End Function

Public Function FindClassForms() As Long
FindClassForms = GetAllClassForms(0)
End Function

Public Function FindClassFormsbyName() As Long
FindClassFormsbyName = GetAllClassForms(1)
End Function

Public Function FindQuestionForms() As Long
FindQuestionForms = GetQuestionForms(0)
End Function

Public Function FindQuestionFormsBlind() As Long
FindQuestionFormsBlind = GetQuestionForms(1)
End Function

Public Function FindQuestionbyFamily() As Long
FindQuestionbyFamily = GetQuestionbyFamily()
End Function

Public Function FindQuestionbyMergeId() As Long
FindQuestionbyMergeId = GetQuestionbyMergeId()
End Function

Public Function FindQuestionbyFormatMask() As Long
FindQuestionbyFormatMask = GetQuestionbyFormatMask()
End Function

Public Function FindQuestionbyControlName() As Long
FindQuestionbyControlName = GetQuestionbyControlName()
End Function

Public Function FindDistinctControlNames() As Long
FindDistinctControlNames = GetDistinctControlNames()
End Function

Public Function FindClassesByControlName() As Long
FindClassesByControlName = GetClassesByControlName()
End Function
