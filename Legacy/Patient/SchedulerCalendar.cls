VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SchedulerCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Scheduler Calendar Object Module
Option Explicit
Public CalendarId As Long
Public CalendarResourceId As Long
Public CalendarDate As String
Public CalendarColor As Long
Public CalendarVacation As Boolean
Public CalendarHoliday As Boolean
Public CalendarPurpose1 As String
Public CalendarPurpose2 As String
Public CalendarPurpose3 As String
Public CalendarPurpose4 As String
Public CalendarPurpose5 As String
Public CalendarPurpose6 As String
Public CalendarPurpose7 As String
Public CalendarStartTime1 As String
Public CalendarStartTime2 As String
Public CalendarStartTime3 As String
Public CalendarStartTime4 As String
Public CalendarStartTime5 As String
Public CalendarStartTime6 As String
Public CalendarStartTime7 As String
Public CalendarEndTime1 As String
Public CalendarEndTime2 As String
Public CalendarEndTime3 As String
Public CalendarEndTime4 As String
Public CalendarEndTime5 As String
Public CalendarEndTime6 As String
Public CalendarEndTime7 As String
Public CalendarLoc1 As Long
Public CalendarLoc2 As Long
Public CalendarLoc3 As Long
Public CalendarLoc4 As Long
Public CalendarLoc5 As Long
Public CalendarLoc6 As Long
Public CalendarLoc7 As Long
Public CalendarCat1 As String
Public CalendarCat2 As String
Public CalendarCat3 As String
Public CalendarCat4 As String
Public CalendarCat5 As String
Public CalendarCat6 As String
Public CalendarCat7 As String
Public CalendarCat8 As String
Public CalendarComment As String

Private ModuleName As String
Private SchedulerCalendarTbl As ADODB.Recordset
Private SchedulerCalendarTotal As Long
Private SchedulerCalendarNew As Boolean

Private Sub Class_Initialize()
Set SchedulerCalendarTbl = CreateAdoRecordset
Call InitCalendar
End Sub

Private Sub Class_Terminate()
Call InitCalendar
Set SchedulerCalendarTbl = Nothing
End Sub

Private Sub InitCalendar()
SchedulerCalendarNew = True
CalendarResourceId = 0
CalendarDate = ""
CalendarColor = 0
CalendarVacation = False
CalendarHoliday = False
CalendarPurpose1 = ""
CalendarPurpose2 = ""
CalendarPurpose3 = ""
CalendarPurpose4 = ""
CalendarPurpose5 = ""
CalendarPurpose6 = ""
CalendarPurpose7 = ""
CalendarStartTime1 = ""
CalendarStartTime2 = ""
CalendarStartTime3 = ""
CalendarStartTime4 = ""
CalendarStartTime5 = ""
CalendarStartTime6 = ""
CalendarStartTime7 = ""
CalendarEndTime1 = ""
CalendarEndTime2 = ""
CalendarEndTime3 = ""
CalendarEndTime4 = ""
CalendarEndTime5 = ""
CalendarEndTime6 = ""
CalendarEndTime7 = ""
CalendarLoc1 = 0
CalendarLoc2 = 0
CalendarLoc3 = 0
CalendarLoc4 = 0
CalendarLoc5 = 0
CalendarLoc6 = 0
CalendarLoc7 = 0
CalendarCat1 = ""
CalendarCat2 = ""
CalendarCat3 = ""
CalendarCat4 = ""
CalendarCat5 = ""
CalendarCat6 = ""
CalendarCat7 = ""
CalendarCat8 = ""
CalendarComment = ""
End Sub

Private Sub LoadCalendar()
Dim Temp As String
SchedulerCalendarNew = False
If (SchedulerCalendarTbl("CalendarId") <> "") Then
    CalendarId = SchedulerCalendarTbl("CalendarId")
Else
    CalendarId = 0
End If
If (SchedulerCalendarTbl("CalendarResourceId") <> "") Then
    CalendarResourceId = SchedulerCalendarTbl("CalendarResourceId")
Else
    CalendarResourceId = 0
End If
If (SchedulerCalendarTbl("CalendarDate") <> "") Then
    CalendarDate = SchedulerCalendarTbl("CalendarDate")
Else
    CalendarDate = ""
End If
If (SchedulerCalendarTbl("CalendarColor") <> "") Then
    CalendarColor = SchedulerCalendarTbl("CalendarColor")
Else
    CalendarColor = 0
End If
If (SchedulerCalendarTbl("CalendarLoc1") <> "") Then
    CalendarLoc1 = SchedulerCalendarTbl("CalendarLoc1")
Else
    CalendarLoc1 = 0
End If
If (SchedulerCalendarTbl("CalendarLoc2") <> "") Then
    CalendarLoc2 = SchedulerCalendarTbl("CalendarLoc2")
Else
    CalendarLoc2 = 0
End If
If (SchedulerCalendarTbl("CalendarLoc3") <> "") Then
    CalendarLoc3 = SchedulerCalendarTbl("CalendarLoc3")
Else
    CalendarLoc3 = 0
End If
If (SchedulerCalendarTbl("CalendarLoc4") <> "") Then
    CalendarLoc4 = SchedulerCalendarTbl("CalendarLoc4")
Else
    CalendarLoc4 = 0
End If
If (SchedulerCalendarTbl("CalendarLoc5") <> "") Then
    CalendarLoc5 = SchedulerCalendarTbl("CalendarLoc5")
Else
    CalendarLoc5 = 0
End If
If (SchedulerCalendarTbl("CalendarLoc6") <> "") Then
    CalendarLoc6 = SchedulerCalendarTbl("CalendarLoc6")
Else
    CalendarLoc6 = 0
End If
If (SchedulerCalendarTbl("CalendarLoc7") <> "") Then
    CalendarLoc7 = SchedulerCalendarTbl("CalendarLoc7")
Else
    CalendarLoc7 = 0
End If
If (SchedulerCalendarTbl("CalendarPurpose") <> "") Then
    CalendarPurpose1 = Left(SchedulerCalendarTbl("CalendarPurpose"), 32)
    CalendarPurpose2 = Mid(SchedulerCalendarTbl("CalendarPurpose"), 33, 32)
    CalendarPurpose3 = Mid(SchedulerCalendarTbl("CalendarPurpose"), 65, 32)
    CalendarPurpose4 = Mid(SchedulerCalendarTbl("CalendarPurpose"), 97, 32)
    CalendarPurpose5 = Mid(SchedulerCalendarTbl("CalendarPurpose"), 129, 32)
    CalendarPurpose6 = Mid(SchedulerCalendarTbl("CalendarPurpose"), 161, 32)
    CalendarPurpose7 = Mid(SchedulerCalendarTbl("CalendarPurpose"), 193, 32)
    If (Mid(SchedulerCalendarTbl("CalendarPurpose"), 254, 1) = "T") Then
        CalendarHoliday = True
    Else
        CalendarHoliday = False
    End If
    If (Mid(SchedulerCalendarTbl("CalendarPurpose"), 255, 1) = "T") Then
        CalendarVacation = True
    Else
        CalendarVacation = False
    End If
Else
    CalendarPurpose1 = ""
    CalendarPurpose2 = ""
    CalendarPurpose3 = ""
    CalendarPurpose4 = ""
    CalendarPurpose5 = ""
    CalendarPurpose6 = ""
    CalendarPurpose7 = ""
    CalendarVacation = False
End If
If (SchedulerCalendarTbl("CalendarStartTime") <> "") Then
    Temp = Trim(SchedulerCalendarTbl("CalendarStartTime"))
    CalendarStartTime1 = Left(Temp, 8)
    If (Len(Temp) > 9) Then
        CalendarStartTime2 = Mid(Temp, 9, 8)
    Else
        CalendarStartTime2 = ""
    End If
    If (Len(Temp) > 17) Then
        CalendarStartTime3 = Mid(Temp, 17, 8)
    Else
        CalendarStartTime3 = ""
    End If
    If (Len(Temp) > 25) Then
        CalendarStartTime4 = Mid(Temp, 25, 8)
    Else
        CalendarStartTime4 = ""
    End If
    If (Len(Temp) > 33) Then
        CalendarStartTime5 = Mid(Temp, 33, 8)
    Else
        CalendarStartTime5 = ""
    End If
    If (Len(Temp) > 41) Then
        CalendarStartTime6 = Mid(Temp, 41, 8)
    Else
        CalendarStartTime6 = ""
    End If
    If (Len(Temp) > 49) Then
        CalendarStartTime7 = Mid(Temp, 49, 8)
    Else
        CalendarStartTime7 = ""
    End If
Else
    CalendarStartTime1 = ""
    CalendarStartTime2 = ""
    CalendarStartTime3 = ""
    CalendarStartTime4 = ""
    CalendarStartTime5 = ""
    CalendarStartTime6 = ""
    CalendarStartTime7 = ""
End If
If (SchedulerCalendarTbl("CalendarEndTime") <> "") Then
    Temp = Trim(SchedulerCalendarTbl("CalendarEndTime"))
    CalendarEndTime1 = Left(Temp, 8)
    If (Len(Temp) > 9) Then
        CalendarEndTime2 = Mid(Temp, 9, 8)
    Else
        CalendarEndTime2 = ""
    End If
    If (Len(Temp) > 17) Then
        CalendarEndTime3 = Mid(Temp, 17, 8)
    Else
        CalendarEndTime3 = ""
    End If
    If (Len(Temp) > 25) Then
        CalendarEndTime4 = Mid(Temp, 25, 8)
    Else
        CalendarEndTime4 = ""
    End If
    If (Len(Temp) > 33) Then
        CalendarEndTime5 = Mid(Temp, 33, 8)
    Else
        CalendarEndTime5 = ""
    End If
    If (Len(Temp) > 41) Then
        CalendarEndTime6 = Mid(Temp, 41, 8)
    Else
        CalendarEndTime6 = ""
    End If
    If (Len(Temp) > 49) Then
        CalendarEndTime7 = Mid(Temp, 49, 8)
    Else
        CalendarEndTime7 = ""
    End If
Else
    CalendarEndTime1 = ""
    CalendarEndTime2 = ""
    CalendarEndTime3 = ""
    CalendarEndTime4 = ""
    CalendarEndTime5 = ""
    CalendarEndTime6 = ""
    CalendarEndTime7 = ""
End If
If (SchedulerCalendarTbl("CatArray1") <> "") Then
    CalendarCat1 = SchedulerCalendarTbl("CatArray1")
Else
    CalendarCat1 = ""
End If
If (SchedulerCalendarTbl("CatArray2") <> "") Then
    CalendarCat2 = SchedulerCalendarTbl("CatArray2")
Else
    CalendarCat2 = ""
End If
If (SchedulerCalendarTbl("CatArray3") <> "") Then
    CalendarCat3 = SchedulerCalendarTbl("CatArray3")
Else
    CalendarCat3 = ""
End If
If (SchedulerCalendarTbl("CatArray4") <> "") Then
    CalendarCat4 = SchedulerCalendarTbl("CatArray4")
Else
    CalendarCat4 = ""
End If
If (SchedulerCalendarTbl("CatArray5") <> "") Then
    CalendarCat5 = SchedulerCalendarTbl("CatArray5")
Else
    CalendarCat5 = ""
End If
If (SchedulerCalendarTbl("CatArray6") <> "") Then
    CalendarCat6 = SchedulerCalendarTbl("CatArray6")
Else
    CalendarCat6 = ""
End If
If (SchedulerCalendarTbl("CatArray7") <> "") Then
    CalendarCat7 = SchedulerCalendarTbl("CatArray7")
Else
    CalendarCat7 = ""
End If
If (SchedulerCalendarTbl("CatArray8") <> "") Then
    CalendarCat8 = SchedulerCalendarTbl("CatArray8")
Else
    CalendarCat8 = ""
End If
If (SchedulerCalendarTbl("CalendarComment") <> "") Then
    CalendarComment = SchedulerCalendarTbl("CalendarComment")
Else
    CalendarComment = ""
End If
End Sub

Private Function GetSchedulerCalendar() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetSchedulerCalendar = True
OrderString = "SELECT * FROM PracticeCalendar WHERE CalendarId =" + Str(CalendarId) + " "
Set SchedulerCalendarTbl = Nothing
Set SchedulerCalendarTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerCalendarTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerCalendarTbl.EOF) Then
    Call InitCalendar
Else
    Call LoadCalendar
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetSchedulerCalendar"
    GetSchedulerCalendar = False
    CalendarId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetSchedulerCalendarbyDate() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerCalendarTotal = 0
GetSchedulerCalendarbyDate = -1
If (Trim(CalendarDate) = "") Then
    Exit Function
End If
If (Len(Trim(CalendarDate)) = 1) Then
    OrderString = "Select * FROM PracticeCalendar WHERE CalendarDate >= '" + Trim(CalendarDate) + "' "
Else
    OrderString = "Select * FROM PracticeCalendar WHERE CalendarDate = '" + Trim(CalendarDate) + "' "
End If
Set SchedulerCalendarTbl = Nothing
Set SchedulerCalendarTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerCalendarTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerCalendarTbl.EOF) Then
    GetSchedulerCalendarbyDate = -1
    Call InitCalendar
Else
    SchedulerCalendarTbl.MoveLast
    SchedulerCalendarTotal = SchedulerCalendarTbl.RecordCount
    GetSchedulerCalendarbyDate = SchedulerCalendarTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetSchedulerCalendarbyDate"
    GetSchedulerCalendarbyDate = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetSchedulerCalendarbyDoctor() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerCalendarTotal = 0
GetSchedulerCalendarbyDoctor = -1
If (Trim(CalendarDate) = "") Then
    Exit Function
End If
If (Len(Trim(CalendarDate)) = 1) Then
    OrderString = "Select * FROM PracticeCalendar WHERE CalendarDate >= '" + Trim(CalendarDate) + "' "
Else
    OrderString = "Select * FROM PracticeCalendar WHERE CalendarDate = '" + Trim(CalendarDate) + "' "
End If
If (CalendarResourceId > 0) Then
    OrderString = OrderString + "And CalendarResourceId = " + Trim(Str(CalendarResourceId)) + " "
End If
Set SchedulerCalendarTbl = Nothing
Set SchedulerCalendarTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerCalendarTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerCalendarTbl.EOF) Then
    GetSchedulerCalendarbyDoctor = -1
    Call InitCalendar
Else
    SchedulerCalendarTbl.MoveLast
    SchedulerCalendarTotal = SchedulerCalendarTbl.RecordCount
    GetSchedulerCalendarbyDoctor = SchedulerCalendarTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetSchedulerCalendarbyDoctor"
    GetSchedulerCalendarbyDoctor = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCalendar(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerCalendarTotal = 0
GetCalendar = -1
If (CalendarResourceId < 1) Then
    Exit Function
End If
OrderString = "Select * FROM PracticeCalendar WHERE CalendarResourceId =" + Str(CalendarResourceId) + " "
If (Trim(CalendarDate) <> "") Then
    OrderString = OrderString + "And CalendarDate = '" + Trim(CalendarDate) + "' "
End If
If (IType <> 1) Then
    If (Trim(CalendarLoc1) = -1) Then
        OrderString = OrderString + "And ((CalendarLoc1 = -1" _
                                  + " Or CalendarLoc2 = -1" + " Or CalendarLoc3 = -1" _
                                  + " Or CalendarLoc4 = -1" + " Or CalendarLoc5 = -1" _
                                  + " Or CalendarLoc6 = -1" + " Or CalendarLoc7 = -1) "
        OrderString = OrderString + " Or (CalendarLoc1 = 0" _
                                  + " Or CalendarLoc2 = 0" + " Or CalendarLoc3 = 0" _
                                  + " Or CalendarLoc4 = 0" + " Or CalendarLoc5 = 0" _
                                  + " Or CalendarLoc6 = 0" + " Or CalendarLoc7 = 0)) "
    ElseIf (CalendarLoc1 >= 0) Then
        OrderString = OrderString + "And (CalendarLoc1 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc2 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc3 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc4 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc5 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc6 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc7 = " + Trim(Str(CalendarLoc1)) + ") "
    End If
Else
    If (Trim(CalendarLoc1) = -1) Then
        OrderString = OrderString + "And ((CalendarLoc1 = -1" _
                                  + " Or CalendarLoc2 = -1" + " Or CalendarLoc3 = -1" _
                                  + " Or CalendarLoc4 = -1" + " Or CalendarLoc5 = -1" _
                                  + " Or CalendarLoc6 = -1" + " Or CalendarLoc7 = -1) "
        OrderString = OrderString + " Or (CalendarLoc1 = 0" _
                                  + " Or CalendarLoc2 = 0" + " Or CalendarLoc3 = 0" _
                                  + " Or CalendarLoc4 = 0" + " Or CalendarLoc5 = 0" _
                                  + " Or CalendarLoc6 = 0" + " Or CalendarLoc7 = 0)) "
    ElseIf (CalendarLoc1 > 0) Then
        OrderString = OrderString + "And (CalendarLoc1 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc2 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc3 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc4 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc5 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc6 = " + Trim(Str(CalendarLoc1)) _
                                  + " Or CalendarLoc7 = " + Trim(Str(CalendarLoc1)) + ") "
    End If
End If
Set SchedulerCalendarTbl = Nothing
Set SchedulerCalendarTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerCalendarTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerCalendarTbl.EOF) Then
    GetCalendar = -1
    Call InitCalendar
Else
    SchedulerCalendarTbl.MoveLast
    SchedulerCalendarTotal = SchedulerCalendarTbl.RecordCount
    GetCalendar = SchedulerCalendarTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCalendar"
    GetCalendar = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutSchedulerCalendar() As Boolean
Dim Temp
On Error GoTo DbErrorHandler
PutSchedulerCalendar = True
If (SchedulerCalendarNew) Then
    SchedulerCalendarTbl.AddNew
End If
SchedulerCalendarTbl("CalendarResourceId") = CalendarResourceId
SchedulerCalendarTbl("CalendarDate") = CalendarDate
SchedulerCalendarTbl("CalendarColor") = CalendarColor
SchedulerCalendarTbl("CalendarComment") = CalendarComment
SchedulerCalendarTbl("CalendarLoc1") = Trim(CalendarLoc1)
SchedulerCalendarTbl("CalendarLoc2") = Trim(CalendarLoc2)
SchedulerCalendarTbl("CalendarLoc3") = Trim(CalendarLoc3)
SchedulerCalendarTbl("CalendarLoc4") = Trim(CalendarLoc4)
SchedulerCalendarTbl("CalendarLoc5") = Trim(CalendarLoc5)
SchedulerCalendarTbl("CalendarLoc6") = Trim(CalendarLoc6)
SchedulerCalendarTbl("CalendarLoc7") = Trim(CalendarLoc7)
Temp = ""
If (Trim(CalendarPurpose1) <> "") Then
    If (Len(CalendarPurpose1) < 32) Then
        CalendarPurpose1 = CalendarPurpose1 + Space(32 - Len(CalendarPurpose1))
    End If
    Temp = Temp + CalendarPurpose1
Else
    Temp = Temp + Space(32)
End If
If (Trim(CalendarPurpose2) <> "") Then
    If (Len(CalendarPurpose2) < 32) Then
        CalendarPurpose2 = CalendarPurpose2 + Space(32 - Len(CalendarPurpose2))
    End If
    Temp = Temp + CalendarPurpose2
Else
    Temp = Temp + Space(32)
End If
If (Trim(CalendarPurpose3) <> "") Then
    If (Len(CalendarPurpose3) < 32) Then
        CalendarPurpose3 = CalendarPurpose3 + Space(32 - Len(CalendarPurpose3))
    End If
    Temp = Temp + CalendarPurpose3
Else
    Temp = Temp + Space(32)
End If
If (Trim(CalendarPurpose4) <> "") Then
    If (Len(CalendarPurpose4) < 32) Then
        CalendarPurpose4 = CalendarPurpose4 + Space(32 - Len(CalendarPurpose4))
    End If
    Temp = Temp + CalendarPurpose4
Else
    Temp = Temp + Space(32)
End If
If (Trim(CalendarPurpose5) <> "") Then
    If (Len(CalendarPurpose5) < 32) Then
        CalendarPurpose5 = CalendarPurpose5 + Space(32 - Len(CalendarPurpose5))
    End If
    Temp = Temp + CalendarPurpose5
Else
    Temp = Temp + Space(32)
End If
If (Trim(CalendarPurpose6) <> "") Then
    If (Len(CalendarPurpose6) < 32) Then
        CalendarPurpose6 = CalendarPurpose6 + Space(32 - Len(CalendarPurpose6))
    End If
    Temp = Temp + CalendarPurpose6
Else
    Temp = Temp + Space(32)
End If
If (Trim(CalendarPurpose7) <> "") Then
    If (Len(CalendarPurpose7) < 32) Then
        CalendarPurpose7 = CalendarPurpose7 + Space(32 - Len(CalendarPurpose7))
    End If
    Temp = Temp + CalendarPurpose7
Else
    Temp = Temp + Space(32)
End If
Temp = Temp + Space(255 - Len(Temp))
If (CalendarVacation) Then
    Mid(Temp, 255, 1) = "T"
Else
    Mid(Temp, 255, 1) = "N"
End If
If (CalendarHoliday) Then
    Mid(Temp, 254, 1) = "T"
Else
    Mid(Temp, 254, 1) = "N"
End If
SchedulerCalendarTbl("CalendarPurpose") = Temp
Temp = ""
If (Trim(CalendarStartTime1) <> "") Then
    Temp = Temp + CalendarStartTime1
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarStartTime2) <> "") Then
    Temp = Temp + CalendarStartTime2
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarStartTime3) <> "") Then
    Temp = Temp + CalendarStartTime3
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarStartTime4) <> "") Then
    Temp = Temp + CalendarStartTime4
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarStartTime5) <> "") Then
    Temp = Temp + CalendarStartTime5
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarStartTime6) <> "") Then
    Temp = Temp + CalendarStartTime6
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarStartTime7) <> "") Then
    Temp = Temp + CalendarStartTime7
Else
    Temp = Temp + Space(8)
End If
SchedulerCalendarTbl("CalendarStartTime") = Temp
Temp = ""
If (Trim(CalendarEndTime1) <> "") Then
    Temp = Temp + CalendarEndTime1
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarEndTime2) <> "") Then
    Temp = Temp + CalendarEndTime2
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarEndTime3) <> "") Then
    Temp = Temp + CalendarEndTime3
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarEndTime4) <> "") Then
    Temp = Temp + CalendarEndTime4
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarEndTime5) <> "") Then
    Temp = Temp + CalendarEndTime5
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarEndTime6) <> "") Then
    Temp = Temp + CalendarEndTime6
Else
    Temp = Temp + Space(8)
End If
If (Trim(CalendarEndTime7) <> "") Then
    Temp = Temp + CalendarEndTime7
Else
    Temp = Temp + Space(8)
End If
SchedulerCalendarTbl("CalendarEndTime") = Temp
SchedulerCalendarTbl("CatArray1") = CalendarCat1
SchedulerCalendarTbl("CatArray2") = CalendarCat2
SchedulerCalendarTbl("CatArray3") = CalendarCat3
SchedulerCalendarTbl("CatArray4") = CalendarCat4
SchedulerCalendarTbl("CatArray5") = CalendarCat5
SchedulerCalendarTbl("CatArray6") = CalendarCat6
SchedulerCalendarTbl("CatArray7") = CalendarCat7
SchedulerCalendarTbl("CatArray8") = CalendarCat8
SchedulerCalendarTbl.Update
If (SchedulerCalendarNew) Then
    CalendarId = GetJustAddedRecord
    SchedulerCalendarNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutSchedulerCalendar"
    PutSchedulerCalendar = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetCalendar(0) > 0) Then
    If (SelectCalendar(1)) Then
        GetJustAddedRecord = SchedulerCalendarTbl("CalendarId")
    End If
End If
End Function

Private Function PurgeSchedulerCalendar() As Boolean
On Error GoTo DbErrorHandler
PurgeSchedulerCalendar = True
SchedulerCalendarTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeSchedulerCalendar"
    PurgeSchedulerCalendar = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplySchedulerCalendar() As Boolean
ApplySchedulerCalendar = PutSchedulerCalendar
End Function

Public Function DeleteSchedulerCalendar() As Boolean
DeleteSchedulerCalendar = PurgeSchedulerCalendar
End Function

Public Function RetrieveSchedulerCalendar() As Boolean
RetrieveSchedulerCalendar = GetSchedulerCalendar
End Function

Public Function SelectCalendar(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectCalendar = False
If (SchedulerCalendarTbl.EOF) Or (ListItem < 1) Or (ListItem > SchedulerCalendarTotal) Then
    Exit Function
End If
SchedulerCalendarTbl.MoveFirst
SchedulerCalendarTbl.Move ListItem - 1
SelectCalendar = True
Call LoadCalendar
Exit Function
DbErrorHandler:
    ModuleName = "SelectCalendar"
    SelectCalendar = False
    CalendarId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCalendar() As Long
FindCalendar = GetCalendar(0)
End Function

Public Function FindCalendarGeneral() As Long
FindCalendarGeneral = GetCalendar(1)
End Function

Public Function FindCalendarbyDate() As Long
FindCalendarbyDate = GetSchedulerCalendarbyDate()
End Function

Public Function FindCalendarbyDoctor() As Long
FindCalendarbyDoctor = GetSchedulerCalendarbyDoctor()
End Function

