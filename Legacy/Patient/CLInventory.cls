VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLInventory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Inventory Object Module
Option Explicit
Public InventoryId As Long
Public SKU As String
Public Type_ As String
Public Manufacturer As String
Public Series As String
Public Material As String
Public Disposable As String
Public WearTime As String
Public Tint As String
Public BaseCurve As String
Public Diameter As String
Public SpherePower As String
Public CylinderPower As String
Public Axis As String
Public PeriphCurve As String
Public AddReading As String
Public Qty As Long
Public Cost As Single
Public WCost As Single
Public SingleFieldValue As String
Public CLInventoryTotal As Long
Public FieldSelect As String
Public Criteria As String
Public OrderBy As String
Private SingleField As Boolean

Private ModuleName As String
Private CLInventoryTbl As ADODB.Recordset
Private CLInventoryNew As Boolean

Private Sub Class_Initialize()
Set CLInventoryTbl = CreateAdoRecordset
Call InitCLInventory
End Sub

Private Sub Class_Terminate()
Call InitCLInventory
Set CLInventoryTbl = Nothing
End Sub

Private Sub InitCLInventory()
CLInventoryNew = True
InventoryId = 0
SKU = ""
Type_ = ""
Manufacturer = ""
Series = ""
Material = ""
Disposable = ""
WearTime = ""
Tint = ""
BaseCurve = ""
Diameter = ""
SpherePower = ""
CylinderPower = ""
AddReading = ""
PeriphCurve = ""
Axis = ""
Qty = 0
Cost = 0
WCost = 0
SingleFieldValue = ""
End Sub

Private Sub LoadCLInventory()
CLInventoryNew = False
If (SingleField) Then
    If (CLInventoryTbl(FieldSelect) <> "") Then
        SingleFieldValue = CLInventoryTbl(FieldSelect)
    Else
        SingleFieldValue = ""
    End If
Else
    InventoryId = CLInventoryTbl("InventoryID")
    If (CLInventoryTbl("SKU") <> "") Then
        SKU = CLInventoryTbl("SKU")
    Else
        SKU = ""
    End If
    If (CLInventoryTbl("Type_") <> "") Then
        Type_ = CLInventoryTbl("Type_")
    Else
        Type_ = ""
    End If
    If (CLInventoryTbl("Manufacturer") <> "") Then
        Manufacturer = CLInventoryTbl("Manufacturer")
    Else
        Manufacturer = ""
    End If
    If (CLInventoryTbl("Series") <> "") Then
        Series = CLInventoryTbl("Series")
    Else
        Series = ""
    End If
    If (CLInventoryTbl("Material") <> "") Then
        Material = CLInventoryTbl("Material")
    Else
        Material = ""
    End If
    If (CLInventoryTbl("Disposable") <> "") Then
        Disposable = CLInventoryTbl("Disposable")
    Else
        Disposable = ""
    End If
    If (CLInventoryTbl("WearTime") <> "") Then
        WearTime = CLInventoryTbl("WearTime")
    Else
        WearTime = ""
    End If
    If (CLInventoryTbl("Tint") <> "") Then
        Tint = CLInventoryTbl("Tint")
    Else
        Tint = ""
    End If
    If (CLInventoryTbl("BaseCurve") <> "") Then
        BaseCurve = CLInventoryTbl("BaseCurve")
    Else
        BaseCurve = ""
    End If
    If (CLInventoryTbl("PeriphCurve") <> "") Then
        PeriphCurve = CLInventoryTbl("PeriphCurve")
    Else
        PeriphCurve = ""
    End If
    If (CLInventoryTbl("AddReading") <> "") Then
        AddReading = CLInventoryTbl("AddReading")
    Else
        AddReading = ""
    End If
    If (CLInventoryTbl("Diameter") <> "") Then
        Diameter = CLInventoryTbl("Diameter")
    Else
        Diameter = ""
    End If
    If (CLInventoryTbl("SpherePower") <> "") Then
        SpherePower = CLInventoryTbl("SpherePower")
    Else
        SpherePower = ""
    End If
    If (CLInventoryTbl("CylinderPower") <> "") Then
        CylinderPower = CLInventoryTbl("CylinderPower")
    Else
        CylinderPower = ""
    End If
    If (CLInventoryTbl("Axis") <> "") Then
        Axis = CLInventoryTbl("Axis")
    Else
        Axis = ""
    End If
    If (CLInventoryTbl("Qty") <> "") Then
        Qty = CLInventoryTbl("Qty")
    Else
        Qty = -1
    End If
    If (CLInventoryTbl("Cost") <> "") Then
        Cost = CLInventoryTbl("Cost")
    Else
        Cost = -1
    End If
    If (CLInventoryTbl("WCost") <> "") Then
        WCost = CLInventoryTbl("WCost")
    Else
        WCost = -1
    End If
End If
End Sub

Public Function PutCLInventory() As Boolean
On Error GoTo DbErrorHandler
PutCLInventory = True
If (InventoryId < 1) Then
    CLInventoryNew = True
End If
If (CLInventoryNew) Then
    CLInventoryTbl.AddNew
    CLInventoryTbl("SKU") = SKU
End If
CLInventoryTbl("Type_") = Type_
CLInventoryTbl("Manufacturer") = Manufacturer
CLInventoryTbl("Series") = Series
CLInventoryTbl("Disposable") = Disposable
CLInventoryTbl("Material") = Material
CLInventoryTbl("WearTime") = WearTime
CLInventoryTbl("Tint") = Tint
CLInventoryTbl("BaseCurve") = BaseCurve
CLInventoryTbl("Diameter") = Diameter
CLInventoryTbl("SpherePower") = SpherePower
CLInventoryTbl("CylinderPower") = CylinderPower
CLInventoryTbl("Axis") = Axis
CLInventoryTbl("AddReading") = AddReading
CLInventoryTbl("PeriphCurve") = PeriphCurve
CLInventoryTbl("Qty") = Qty
CLInventoryTbl("Cost") = Cost
CLInventoryTbl("WCost") = WCost
CLInventoryTbl.Update
If (CLInventoryNew) Then
    CLInventoryTbl.MoveLast
    Call LoadCLInventory
    CLInventoryNew = True
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutCLInventory"
    Call PinpointPRError(ModuleName, Err)
    PutCLInventory = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLInventorySingleField() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCLInventorySingleField = True
OrderString = "SELECT DISTINCT " + FieldSelect + " FROM CLInventory"
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy + " ASC"
End If
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CLInventoryTbl.EOF) Then
    Call InitCLInventory
Else
    CLInventoryTbl.MoveLast
    CLInventoryTotal = CLInventoryTbl.RecordCount
    Call LoadCLInventory
End If
Exit Function
DbErrorHandler:
    GetCLInventorySingleField = False
    ModuleName = "GetCLInventorySingleField"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLInventorySingleFieldwithTotal() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCLInventorySingleFieldwithTotal = -1
OrderString = "SELECT DISTINCT " + FieldSelect + " FROM CLInventory"
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy + " ASC"
End If
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CLInventoryTbl.EOF) Then
    Call InitCLInventory
Else
    CLInventoryTbl.MoveLast
    CLInventoryTotal = CLInventoryTbl.RecordCount
    GetCLInventorySingleFieldwithTotal = CLInventoryTotal
End If
Exit Function
DbErrorHandler:
    GetCLInventorySingleFieldwithTotal = -1
    ModuleName = "GetCLInventorySingleFieldwithTotal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLInventory() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCLInventory = True
OrderString = "SELECT * FROM CLInventory"
If (Criteria <> "") Then
    OrderString = OrderString + " WHERE " + Criteria
End If
If (OrderBy <> "") Then
    OrderString = OrderString + " ORDER BY " + OrderBy + " ASC"
End If
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (CLInventoryTbl.EOF) Then
    Call InitCLInventory
Else
    CLInventoryTbl.MoveLast
    CLInventoryTotal = CLInventoryTbl.RecordCount
    Call LoadCLInventory
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCLInventory"
    GetCLInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveCLInventory() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
RetrieveCLInventory = False
If (InventoryId > 0) Then
    OrderString = "SELECT * FROM CLInventory WHERE InventoryID = " + Trim(Str(InventoryId)) + " "
    Set CLInventoryTbl = Nothing
    Set CLInventoryTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (CLInventoryTbl.EOF) Then
        RetrieveCLInventory = True
        Call LoadCLInventory
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveCLInventory"
    RetrieveCLInventory = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectCLInventory(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectCLInventory = True
If (CLInventoryTbl.EOF) Or (ListItem < 1) Or (ListItem > CLInventoryTotal) Then
    SelectCLInventory = False
    Exit Function
End If
CLInventoryTbl.MoveFirst
CLInventoryTbl.Move ListItem - 1
Call LoadCLInventory
Exit Function
DbErrorHandler:
    ModuleName = "SelectCLInventory"
    Call PinpointPRError(ModuleName, Err)
    SelectCLInventory = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLInventorySingleField() As Boolean
On Error GoTo DbErrorHandler
SingleField = True
FindCLInventorySingleField = GetCLInventorySingleField
Exit Function
DbErrorHandler:
    ModuleName = "FindCLInventorySingleField"
    Call PinpointPRError(ModuleName, Err)
    FindCLInventorySingleField = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLInventorySingleFieldwithTotal() As Long
On Error GoTo DbErrorHandler
SingleField = True
FindCLInventorySingleFieldwithTotal = GetCLInventorySingleFieldwithTotal
Exit Function
DbErrorHandler:
    ModuleName = "FindCLInventorySingleFieldwithTotal"
    Call PinpointPRError(ModuleName, Err)
    FindCLInventorySingleFieldwithTotal = -1
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLInventory() As Boolean
On Error GoTo DbErrorHandler
SingleField = False
FindCLInventory = GetCLInventory
Exit Function
DbErrorHandler:
    ModuleName = "FindCLInventory"
    Call PinpointPRError(ModuleName, Err)
    FindCLInventory = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectCLManufacturers(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectCLManufacturers = True
If (CLInventoryTbl.EOF) Or (ListItem < 1) Or (ListItem > CLInventoryTotal) Then
    SelectCLManufacturers = False
    Exit Function
End If
CLInventoryTbl.MoveFirst
CLInventoryTbl.Move ListItem - 1
If (CLInventoryTbl("Manufacturer") <> "") Then
    Manufacturer = CLInventoryTbl("Manufacturer")
Else
    Manufacturer = ""
End If
Exit Function
DbErrorHandler:
    ModuleName = "SelectCLManufacturers"
    Call PinpointPRError(ModuleName, Err)
    SelectCLManufacturers = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLManufacturers() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetCLManufacturers = -1
OrderString = "SELECT DISTINCT Manufacturer FROM CLInventory "
OrderString = OrderString + " WHERE Manufacturer >= '" + Criteria + "' "
OrderString = OrderString + " ORDER BY Manufacturer ASC "
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (CLInventoryTbl.EOF) Then
    CLInventoryTbl.MoveLast
    CLInventoryTotal = CLInventoryTbl.RecordCount
    GetCLManufacturers = CLInventoryTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCLManufacturers"
    GetCLManufacturers = -1
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectCLSeries(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
SelectCLSeries = False
If (CLInventoryTbl.EOF) Or (ListItem < 1) Or (ListItem > CLInventoryTotal) Then
    Exit Function
End If
CLInventoryTbl.MoveFirst
CLInventoryTbl.Move ListItem - 1
If (CLInventoryTbl("Series") <> "") Then
    Series = CLInventoryTbl("Series")
Else
    Series = ""
End If
If (CLInventoryTbl("InventoryId") <> "") Then
    InventoryId = CLInventoryTbl("InventoryId")
Else
    InventoryId = 0
End If
SelectCLSeries = True
Exit Function
DbErrorHandler:
    ModuleName = "SelectCLSeries"
    Call PinpointPRError(ModuleName, Err)
    SelectCLSeries = False
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLSeries() As Long
On Error GoTo DbErrorHandler
FindCLSeries = GetCLSeries
Exit Function
DbErrorHandler:
    ModuleName = "FindCLSeries"
    Call PinpointPRError(ModuleName, Err)
    FindCLSeries = -1
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindCLManufacturers() As Long
On Error GoTo DbErrorHandler
FindCLManufacturers = GetCLManufacturers
Exit Function
DbErrorHandler:
    ModuleName = "FindCLManufacturers"
    Call PinpointPRError(ModuleName, Err)
    FindCLManufacturers = -1
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetCLSeries() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
    GetCLSeries = -1
    OrderString = "SELECT Min(Series) as Series, MIN(InventoryId) as InventoryId FROM CLInventory "
    OrderString = OrderString + " WHERE Series >= '" + Criteria + "' "
    OrderString = OrderString + " Group BY Series ORDER BY Series ASC "
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (CLInventoryTbl.EOF) Then
    CLInventoryTbl.MoveLast
    CLInventoryTotal = CLInventoryTbl.RecordCount
    GetCLSeries = CLInventoryTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCLSeries"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetCLInventoryIdGeneric(inp As String) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String

OrderString = "SELECT * FROM CLInventory  where " & _
              "LTRIM(RTRIM(" & _
              "    LTRIM(RTRIM(ISNULL(Series,''))) + ' ' +" & _
              "    LTRIM(RTRIM(ISNULL(type_,''))) + ' ' +" & _
              "    LTRIM(RTRIM(ISNULL(WearTime,'')))" & _
              "))" & _
              " = '" & inp & "' order by 1 "
              
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not CLInventoryTbl.EOF Then
    GetCLInventoryIdGeneric = CLInventoryTbl("InventoryId")
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetCLInventoryIdGeneric"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function CntRecs() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
CntRecs = 0
OrderString = "SELECT Count(InventoryId) As Bullshit FROM CLInventory "
Set CLInventoryTbl = Nothing
Set CLInventoryTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call CLInventoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (CLInventoryTbl.EOF) Then
    CLInventoryTbl.MoveFirst
    If (CLInventoryTbl("Bullshit") <> "") Then
        CntRecs = CLInventoryTbl("Bullshit")
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = ""
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
