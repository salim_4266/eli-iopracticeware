VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceTransactions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The ServiceTransactions Object Module
Option Explicit
Public ServTranID As String
Public TransLinkTransactionId As Long
Public ServTranTransactionDate As String
Public ServTranServiceId As Long
Public ServTranTransportAction As String
Public ServTranAmount As String

Private ModuleName As String
Private TransactionJournalTotal As Long

Public TransactionJournalLinkTbl As ADODB.Recordset
Private TransactionJournalNew As Boolean

Public TransQueryTransactionDate As String
Public TransQueryInsurerName As String
Public TransQueryAmount As String
Public TransQueryStatus As String
Public TransQueryAction As String
Private TransQuery As ADODB.Recordset

Private Sub Class_Initialize()
Set TransactionJournalLinkTbl = CreateAdoRecordset
Call InitTransactionJournal(True)
End Sub

Private Sub Class_Terminate()
Call InitTransactionJournal(False)
Set TransactionJournalLinkTbl = Nothing
Set TransQuery = Nothing
End Sub

Private Sub InitTransactionJournal(WriteOn As Boolean)
TransactionJournalNew = WriteOn
TransLinkTransactionId = 0
ServTranTransactionDate = ""
ServTranServiceId = 0
ServTranTransportAction = ""
ServTranAmount = 0
End Sub

Private Sub LoadTransactionJournal()
TransactionJournalNew = False
If (TransactionJournalLinkTbl("ID") <> "") Then
    ServTranID = TransactionJournalLinkTbl("ID")
End If
If (TransactionJournalLinkTbl("TransactionId") <> "") Then
    TransLinkTransactionId = TransactionJournalLinkTbl("TransactionId")
End If
If (TransactionJournalLinkTbl("Amount") <> "") Then
    ServTranAmount = TransactionJournalLinkTbl("Amount")
End If
If (TransactionJournalLinkTbl("TransportAction") <> "") Then
    ServTranTransportAction = TransactionJournalLinkTbl("TransportAction")
End If
If (TransactionJournalLinkTbl("TransactionDate") <> "") Then
    ServTranTransactionDate = TransactionJournalLinkTbl("TransactionDate")
End If
If (TransactionJournalLinkTbl("ServiceId") <> "") Then
    ServTranServiceId = TransactionJournalLinkTbl("ServiceId")
End If
End Sub

Private Function GetTransactionJournal()
On Error GoTo DbErrorHandler
GetTransactionJournal = False
Dim strSQL As String
strSQL = "SELECT * FROM ServiceTransactions WHERE ID = '" + Trim(ServTranID) + "' "
Set TransactionJournalLinkTbl = Nothing
Set TransactionJournalLinkTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = strSQL
Call TransactionJournalLinkTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (TransactionJournalLinkTbl.EOF) Then
    Call InitTransactionJournal(True)
Else
    Call InitTransactionJournal(False)
    Call LoadTransactionJournal
End If
GetTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetTransactionJournalList() As Long
On Error GoTo DbErrorHandler
Dim l As Integer
Dim Ref As String
Dim strSQL As String
Ref = ""
TransactionJournalTotal = 0
GetTransactionJournalList = -1
If (Trim(TransLinkTransactionId) <> "") Then
    strSQL = "Select * FROM ServiceTransactions WHERE "
    strSQL = strSQL + Ref + "TransactionId = " + Trim(Str(TransLinkTransactionId)) + " "
    Ref = "And "
    If ((ServTranServiceId) <> 0) Then
        strSQL = strSQL + Ref + "ServiceId = " + Trim(Str(ServTranServiceId)) + " "
        Ref = "And "
    End If
    If (Trim(ServTranTransportAction) <> "") Then
        strSQL = strSQL + Ref + "TransportAction = '" + Trim(ServTranTransportAction) + "' "
        Ref = "And "
    End If
'    If (Trim(ServTranTransactionStatus) <> "") Then
'        If (Left(ServTranTransactionStatus, 1) = "-") Then
'            strSQL = strSQL + Ref + "TransactionStatus <> '" + Trim(Mid(ServTranTransactionStatus, 2, 1)) + "' "
'        Else
'            strSQL = strSQL + Ref + "TransactionStatus = '" + Trim(ServTranTransactionStatus) + "' "
'        End If
'        Ref = "And "
'    End If
    strSQL = strSQL + "ORDER BY ModTime ASC "
    Set TransactionJournalLinkTbl = Nothing
    Set TransactionJournalLinkTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = strSQL
    Call TransactionJournalLinkTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (TransactionJournalLinkTbl.EOF) Then
        TransactionJournalLinkTbl.MoveLast
        TransactionJournalTotal = TransactionJournalLinkTbl.RecordCount
        GetTransactionJournalList = TransactionJournalLinkTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetTransactionJournalList"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutTransactionJournal()
On Error GoTo DbErrorHandler
PutTransactionJournal = False
If (TransactionJournalNew) Then
    TransactionJournalLinkTbl.AddNew
End If
TransactionJournalLinkTbl("TransactionId") = TransLinkTransactionId
TransactionJournalLinkTbl("ServiceId") = ServTranServiceId
TransactionJournalLinkTbl("TransactionDate") = UCase(Trim(ServTranTransactionDate))
TransactionJournalLinkTbl("TransportAction") = UCase(Trim(ServTranTransportAction))
TransactionJournalLinkTbl("Amount") = UCase(Trim(ServTranAmount))
TransactionJournalLinkTbl.Update
PutTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "PutTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetTransactionJournalList > 0) Then
    If (SelectTransactionJournal(1)) Then
        GetJustAddedRecord = TransactionJournalLinkTbl("ID")
    End If
End If
End Function

Private Function PurgeTransactionJournal() As Boolean
On Error GoTo DbErrorHandler
PurgeTransactionJournal = False
TransactionJournalLinkTbl.Delete
PurgeTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "PurgeTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyServiceTransaction() As Boolean
ApplyServiceTransaction = PutTransactionJournal
End Function

Public Function DeleteTransactionJournal() As Boolean
DeleteTransactionJournal = PurgeTransactionJournal
End Function

Public Function RetrieveTransactionJournal() As Boolean
RetrieveTransactionJournal = GetTransactionJournal
End Function

Public Function SelectTransactionJournal(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectTransactionJournal = False
If (TransactionJournalLinkTbl.EOF) Or (ListItem < 1) Or (ListItem > TransactionJournalTotal) Then
    Exit Function
End If
TransactionJournalLinkTbl.MoveFirst
TransactionJournalLinkTbl.Move ListItem - 1
Call InitTransactionJournal(False)
Call LoadTransactionJournal
SelectTransactionJournal = True
Exit Function
DbErrorHandler:
    ModuleName = "SelectTransactionJournal"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindTransactionJournal() As Long
FindTransactionJournal = GetTransactionJournalList
End Function

Public Function GetTransQuery(Invoice As String, ServiceID As Long) As Long
On Error GoTo DbErrorHandler
GetTransQuery = 0

Dim strSQL As String
strSQL = "select pr.invoice " & _
        ", ptj.transactionid, ptj.transactionstatus ,ptj.transactiondate, ptj.transactionref, ptj.TransactionAction  " & _
        ", prs.Service " & _
        ", st.serviceid, st.transportaction, st.amount " & _
        ", i.Name AS InsurerName " & _
        "from patientreceivables pr " & _
        "    inner join patientreceivableservices prs on prs.invoice = pr.invoice " & _
        "    left outer join model.Insurers i on i.Id= pr.insurerid " & _
        "    inner join practicetransactionjournal ptj on ptj.transactiontypeid = pr.receivableid " & _
        "        and ptj.transactiontype = 'r' " & _
        "    inner join servicetransactions st on st.transactionid = ptj.transactionid " & _
        "        and st.serviceid = prs.itemid " & _
        "Where ptj.transactiontype = 'r' " & _
        "and pr.invoice = '" & Invoice & "' " & _
        "and prs.itemid = " & ServiceID & " " & _
        "order by st.modtime desc"

Set TransQuery = Nothing
Set TransQuery = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = strSQL
Call TransQuery.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (TransQuery.EOF) Then
    TransQuery.MoveLast
    GetTransQuery = TransQuery.RecordCount
End If

Exit Function
DbErrorHandler:
    ModuleName = "GetTransQuery"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:

End Function

Public Function LoadTransQuery(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
LoadTransQuery = False
If (TransQuery.EOF) Or (ListItem < 1) Or (ListItem > TransQuery.RecordCount) Then
    Exit Function
End If
TransQuery.MoveFirst
TransQuery.Move ListItem - 1

TransQueryTransactionDate = TransQuery("transactiondate")

If TransQuery("transactionref") = "I" Then
    If IsNull(TransQuery("insurername")) Then
        TransQueryInsurerName = "error"
    Else
        TransQueryInsurerName = TransQuery("insurername")
    End If
Else
    TransQueryInsurerName = "PATIENT"
End If

If IsNull(TransQuery("Amount")) Then
    TransQueryAmount = "0"
Else
    TransQueryAmount = TransQuery("Amount")
End If


Dim TransQuery_transportaction As String
If IsNull(TransQuery("transportaction")) Then
    TransQuery_transportaction = "null"
Else
    TransQuery_transportaction = TransQuery("transportaction")
End If

Call TranslateTransAction(TransQuery("transactionstatus"), TransQuery("transactionref"), TransQuery("TransactionAction"), TransQuery_transportaction, _
                          TransQueryStatus, TransQueryAction)

LoadTransQuery = True
Exit Function
DbErrorHandler:
    ModuleName = "LoadTransQuery"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Sub TranslateTransAction(transactionstatus As String, transactionref As String, transactionAction As String, TransportAction As String, _
sTransQueryStatus As String, sTransQueryAction As String)

Dim sPrevStat As String

Select Case transactionstatus
Case "P"
    Select Case transactionref
    Case "I"
        sTransQueryStatus = "Claim"
    Case "P"
        sTransQueryStatus = "Statement"
    Case Else
        sTransQueryStatus = "error"
    End Select
    
    Select Case TransportAction
    Case "P", "B", "", "null"
        sTransQueryAction = "Qd"
    Case "V"
        sTransQueryAction = "Wtng"
    Case Else
        sTransQueryAction = "error"
    End Select

Case "S"
    Select Case transactionref
    Case "I"
        sTransQueryStatus = "Claim"
    Case "P"
        sTransQueryStatus = "Statement"
    Case Else
        sTransQueryStatus = "error"
    End Select
    
    Select Case TransportAction
    Case "P", "B", "", "null"
        sTransQueryAction = "Snt"
    Case "X"
        sTransQueryAction = "Snt X"
    Case "V"
        sTransQueryAction = "Wtng"
    Case "Y"
        sTransQueryAction = "Apl"
    Case Else
        sTransQueryAction = "error"
    End Select
    
Case "Z"
    Select Case transactionref
    Case "I"
        sTransQueryStatus = "Claim"
    Case "P"
        sTransQueryStatus = "Statement"
    Case "G"
        sTransQueryStatus = "Monthly Stmt"
    Case Else
        sTransQueryStatus = "error"
    End Select
    
    
    Select Case TransportAction
    Case "P", "B", "V", "U", "Y", "0", "1"
        sTransQueryAction = "Clsd"
    Case "2"
        sTransQueryAction = "Clsd X"
    Case "", "null"
        Select Case transactionref
        Case "I", "P"
            sTransQueryAction = "Clsd"
        Case "G"
            sTransQueryAction = ""
        Case Else
            sTransQueryAction = "error"
        End Select
    Case Else
        sTransQueryAction = "error"
    End Select
    
    Select Case TransportAction
    Case "0"
        sPrevStat = " NotSnt"
    Case "1"
        sPrevStat = " Snt"
    End Select
Case Else
    sTransQueryAction = "error"
End Select

Select Case sTransQueryStatus
Case "Claim"
    Select Case transactionAction
    Case "P"
        sTransQueryAction = sTransQueryAction & " P"
    Case "B"
        If Not Right(sTransQueryAction, 1) = "X" Then
            sTransQueryAction = sTransQueryAction & " E"
        End If
    End Select
End Select

sTransQueryAction = sTransQueryAction & sPrevStat
End Sub




