VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynamicForms"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' DynamicForms Object Module
Option Explicit
Public FormId As Long
Public FormName As String
Public FormInterface As String
Public BackColor As Long
Public ForeColor As Long
Public WindowState As Long
Public WindowHeight As Long
Public WindowWidth As Long
Public ExitButton As Boolean
Public ApplyButton As Boolean
Public DontKnowButton As Boolean
Public Question As String
Public QuestionType As String
Public NoToAllButton As Boolean
Public HelpButton As Boolean
Public EditType As Boolean
Public StaticLabel As String

Private ModuleName As String
Private ClinicalTotalForms As Integer
Private ClinicalFormsTbl As ADODB.Recordset
Private ClinicalFormsNew As Boolean

Private Sub Class_Initialize()
Set ClinicalFormsTbl = New ADODB.Recordset
ClinicalFormsTbl.CursorLocation = adUseClient
Call InitForm
End Sub

Private Sub Class_Terminate()
Call InitForm
Set ClinicalFormsTbl = Nothing
End Sub

Private Sub InitForm()
ClinicalFormsNew = False
FormName = ""
FormInterface = ""
BackColor = 0
ForeColor = 0
WindowState = 0
WindowHeight = 0
WindowWidth = 0
ExitButton = False
ApplyButton = False
DontKnowButton = False
HelpButton = False
NoToAllButton = False
Question = ""
QuestionType = ""
EditType = False
End Sub

Private Function LoadForm() As Boolean
ClinicalFormsNew = False
FormId = ClinicalFormsTbl("FormId")
If (ClinicalFormsTbl("FormName") <> "") Then
    FormName = ClinicalFormsTbl("FormName")
Else
    FormName = ""
End If
If (ClinicalFormsTbl("Forminterface") <> "") Then
    FormInterface = ClinicalFormsTbl("FormInterface")
Else
    FormInterface = ""
End If
If (ClinicalFormsTbl("BackColor") <> "") Then
    BackColor = ClinicalFormsTbl("BackColor")
    BackColor = &HC19B49
Else
    BackColor = 0
End If
If (ClinicalFormsTbl("ForeColor") <> "") Then
    ForeColor = ClinicalFormsTbl("ForeColor")
Else
    ForeColor = 0
End If
If (ClinicalFormsTbl("WindowState") <> "") Then
    WindowState = ClinicalFormsTbl("WindowState")
Else
    WindowState = 0
End If
If (ClinicalFormsTbl("FormHeight") <> "") Then
    WindowHeight = ClinicalFormsTbl("FormHeight")
Else
    WindowHeight = 0
End If
If (ClinicalFormsTbl("FormWidth") <> "") Then
    WindowWidth = ClinicalFormsTbl("FormWidth")
Else
    WindowWidth = 0
End If
If (ClinicalFormsTbl("ExitButton") <> "") Then
    If (ClinicalFormsTbl("ExitButton") = "T") Then
        ExitButton = True
    Else
        ExitButton = False
    End If
Else
    ExitButton = False
End If
If (ClinicalFormsTbl("ApplyButton") <> "") Then
    If (ClinicalFormsTbl("ApplyButton") = "T") Then
        ApplyButton = True
    Else
        ApplyButton = False
    End If
Else
    ApplyButton = False
End If
If (ClinicalFormsTbl("DontKnowButton") <> "") Then
    If (ClinicalFormsTbl("DontKnowButton") = "T") Then
        DontKnowButton = True
    Else
        DontKnowButton = False
    End If
Else
    DontKnowButton = False
End If
If (ClinicalFormsTbl("NoToAllButton") <> "") Then
    If (ClinicalFormsTbl("NoToAllButton") = "T") Then
        NoToAllButton = True
    Else
        NoToAllButton = False
    End If
Else
    NoToAllButton = False
End If
If (ClinicalFormsTbl("HelpButton") <> "") Then
    If (ClinicalFormsTbl("HelpButton") = "T") Then
        HelpButton = True
    Else
        HelpButton = False
    End If
Else
    HelpButton = False
End If
If (ClinicalFormsTbl("StaticLabel") <> "") Then
    StaticLabel = ClinicalFormsTbl("StaticLabel")
Else
    StaticLabel = ""
End If
If (ClinicalFormsTbl("Question") <> "") Then
    Question = ClinicalFormsTbl("Question")
Else
    Question = ""
End If
If (ClinicalFormsTbl("QuestionType") <> "") Then
    QuestionType = ClinicalFormsTbl("QuestionType")
Else
    QuestionType = ""
End If
If (ClinicalFormsTbl("EditStyle") <> "") Then
    If (ClinicalFormsTbl("EditStyle") = "T") Then
        EditType = True
    Else
        EditType = False
    End If
Else
    EditType = False
End If
End Function

Private Function GetForm() As Boolean
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
GetForm = True
If (Trim(Question) = "") And (FormId < 1) And (Trim(FormName) = "") Then
    Call InitForm
    Exit Function
End If
Ref = ""
OrderString = "SELECT * FROM Forms WHERE "
If (FormId > 0) Then
    OrderString = OrderString + "FormId =" + Str(FormId) + " "
    Ref = "And "
End If
If (Trim(Question) <> "") Then
    If (Len(Question) = 1) Then
        OrderString = OrderString + Ref + "Question >= '" + Question + "' "
    Else
        OrderString = OrderString + Ref + "Question = '" + Question + "' "
    End If
End If
If (Trim(FormName) <> "") Then
    OrderString = OrderString + Ref + "FormName = '" + FormName + "' "
End If
Set ClinicalFormsTbl = Nothing
Set ClinicalFormsTbl = New ADODB.Recordset
MyDynamicFormsCmd.CommandText = OrderString
Call ClinicalFormsTbl.Open(MyDynamicFormsCmd, , adOpenStatic, adLockOptimistic, -1)
If (ClinicalFormsTbl.EOF) Then
    Call InitForm
Else
    Call LoadForm
    ClinicalFormsTbl.MoveLast
    ClinicalTotalForms = ClinicalFormsTbl.RecordCount
    ClinicalFormsTbl.MoveFirst
End If
Exit Function
DbErrorHandler:
    GetForm = False
    FormId = -256
    ModuleName = "GetForm"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutForm()
On Error GoTo DbErrorHandler
PutForm = True
If (ClinicalFormsNew) Then
    ClinicalFormsTbl.AddNew
End If
ClinicalFormsTbl("FormName") = UCase(Trim(FormName))
ClinicalFormsTbl("FormInterface") = UCase(Trim(FormInterface))
ClinicalFormsTbl("BackColor") = BackColor
ClinicalFormsTbl("ForeColor") = ForeColor
ClinicalFormsTbl("WindowState") = WindowState
ClinicalFormsTbl("FormHeight") = WindowHeight
ClinicalFormsTbl("FormWidth") = WindowWidth
ClinicalFormsTbl("ExitButton") = "F"
If (ExitButton) Then
    ClinicalFormsTbl("ExitButton") = "T"
End If
ClinicalFormsTbl("ApplyButton") = "F"
If (ApplyButton) Then
    ClinicalFormsTbl("ApplyButton") = "T"
End If
ClinicalFormsTbl("DontKnowButton") = "F"
If (DontKnowButton) Then
    ClinicalFormsTbl("DontKnowButton") = "T"
End If
ClinicalFormsTbl("HelpButton") = "F"
If (HelpButton) Then
    ClinicalFormsTbl("HelpButton") = "T"
End If
ClinicalFormsTbl("NoToAllButton") = "F"
If (NoToAllButton) Then
    ClinicalFormsTbl("NoToAllButton") = "T"
End If
ClinicalFormsTbl("StaticLabel") = StaticLabel
ClinicalFormsTbl("Question") = UCase(Trim(Question))
ClinicalFormsTbl("QuestionType") = UCase(Trim(QuestionType))
ClinicalFormsTbl("EditStyle") = "F"
If (EditType) Then
    ClinicalFormsTbl("EditStyle") = "T"
End If
ClinicalFormsTbl.Update
If (ClinicalFormsNew) Then
    FormId = GetJustAddedRecord
    ClinicalFormsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutForm"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
    PutForm = False
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
End Function

Public Function ApplyForm() As Boolean
ApplyForm = PutForm
End Function

Public Function RetrieveForm() As Boolean
RetrieveForm = GetForm
End Function

Public Function SelectForm(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectForm = False
If (ClinicalFormsTbl.EOF) Or (ListItem < 1) Or (ListItem > ClinicalTotalForms) Then
    Exit Function
End If
ClinicalFormsTbl.MoveFirst
ClinicalFormsTbl.Move ListItem - 1
SelectForm = True
Call LoadForm
Exit Function
DbErrorHandler:
    SelectForm = False
    ModuleName = "SelectForm"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

