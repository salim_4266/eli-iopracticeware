VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DiagnosisMasterProcedures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Procedure Object Module
Option Explicit
Public ProcedureId As Long
Public ProcedureCPT As String
Public ProcedureDiagnosis As String
Public ProcedureInsClass1 As String
Public ProcedureInsClass2 As String
Public ProcedureInsClass3 As String
Public ProcedureInsClass4 As String
Public ProcedureInsClass5 As String
Public ProcedureInsClass6 As String
Public ProcedureRank As Long
Public ProcedureName As String
Public ProcedureEM As Boolean
Public ProcedureOrder As Long
Public ProcedureOrderDoc As Boolean
Public ProcedureConsultOn As Boolean
Public ProcedureLinkType As String
Public ProcedureLtrTrans As String

Private ModuleName As String
Private ProcedureTotal As Integer
Private ProcedureTbl As ADODB.Recordset
Private ProcedureNew As Boolean

Private Sub Class_Initialize()
Set ProcedureTbl = New ADODB.Recordset
ProcedureTbl.CursorLocation = adUseClient
Call InitProcedure
End Sub

Private Sub Class_Terminate()
Call InitProcedure
Set ProcedureTbl = Nothing
End Sub

Private Sub InitProcedure()
ProcedureNew = False
ProcedureDiagnosis = ""
ProcedureName = ""
ProcedureCPT = ""
ProcedureInsClass1 = ""
ProcedureInsClass2 = ""
ProcedureInsClass3 = ""
ProcedureInsClass4 = ""
ProcedureInsClass5 = ""
ProcedureInsClass6 = ""
ProcedureRank = 0
ProcedureEM = False
ProcedureOrder = 0
ProcedureOrderDoc = False
ProcedureConsultOn = False
ProcedureLinkType = ""
ProcedureLtrTrans = ""
End Sub

Private Sub LoadProcedure(IType As Integer)
If (ProcedureTbl("CPT") <> "") Then
    ProcedureCPT = ProcedureTbl("CPT")
Else
    ProcedureCPT = ""
End If
If (IType = 0) Then
    ProcedureId = ProcedureTbl("CPTLinkId")
    If (ProcedureTbl("ICD9") <> "") Then
        ProcedureDiagnosis = ProcedureTbl("ICD9")
    Else
        ProcedureDiagnosis = ""
    End If
    If (ProcedureTbl("InsClass1") <> "") Then
        ProcedureInsClass1 = ProcedureTbl("InsClass1")
    Else
        ProcedureInsClass1 = ""
    End If
    If (ProcedureTbl("InsClass2") <> "") Then
        ProcedureInsClass2 = ProcedureTbl("InsClass2")
    Else
        ProcedureInsClass2 = ""
    End If
    If (ProcedureTbl("InsClass3") <> "") Then
        ProcedureInsClass3 = ProcedureTbl("InsClass3")
    Else
        ProcedureInsClass3 = ""
    End If
    If (ProcedureTbl("InsClass4") <> "") Then
        ProcedureInsClass4 = ProcedureTbl("InsClass4")
    Else
        ProcedureInsClass4 = ""
    End If
    If (ProcedureTbl("InsClass5") <> "") Then
        ProcedureInsClass5 = ProcedureTbl("InsClass5")
    Else
        ProcedureInsClass5 = ""
    End If
    If (ProcedureTbl("InsClass6") <> "") Then
        ProcedureInsClass6 = ProcedureTbl("InsClass6")
    Else
        ProcedureInsClass6 = ""
    End If
    If (ProcedureTbl("Description") <> "") Then
        ProcedureName = ProcedureTbl("Description")
    Else
        ProcedureName = ""
    End If
    If (ProcedureTbl("Rank") <> "") Then
        ProcedureRank = ProcedureTbl("Rank")
    Else
        ProcedureRank = 0
    End If
    If (ProcedureTbl("EM") <> "") Then
        If (ProcedureTbl("EM") = "T") Then
            ProcedureEM = True
        Else
            ProcedureEM = False
        End If
    Else
        ProcedureEM = False
    End If
    If (ProcedureTbl("Extra") <> "") Then
        ProcedureOrder = ProcedureTbl("Extra")
    Else
        ProcedureOrder = 0
    End If
    If (ProcedureTbl("SurgOrOffice") <> "") Then
        ProcedureLinkType = ProcedureTbl("SurgOrOffice")
    Else
        ProcedureLinkType = ""
    End If
    If (ProcedureTbl("LetterTranslation") <> "") Then
        ProcedureLtrTrans = ProcedureTbl("LetterTranslation")
    Else
        ProcedureLtrTrans = ""
    End If
    If (ProcedureTbl("OrderDoc") <> "") Then
        If (ProcedureTbl("OrderDoc") = "T") Then
            ProcedureOrderDoc = True
        Else
            ProcedureOrderDoc = False
        End If
    Else
        ProcedureOrderDoc = False
    End If
    If (ProcedureTbl("ConsultOn") <> "") Then
        If (ProcedureTbl("ConsultOn") = "T") Then
            ProcedureConsultOn = True
        Else
            ProcedureConsultOn = False
        End If
    Else
        ProcedureConsultOn = False
    End If
End If
End Sub

Private Function GetProcedure()
On Error GoTo DbErrorHandler
Dim OrderString As String
GetProcedure = True
OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE CPTLinkId =" + Trim(Str(ProcedureId)) + " "
Set ProcedureTbl = Nothing
Set ProcedureTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (ProcedureTbl.EOF) Then
    Call InitProcedure
    ProcedureNew = True
Else
    ProcedureNew = False
    Call LoadProcedure(0)
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetProcedure"
    GetProcedure = False
    ProcedureId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllProcedure(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim SortBy As String
Dim OrderString As String
ProcedureTotal = 0
GetAllProcedure = -1
If (Trim(ProcedureCPT) = "") Then
    Exit Function
End If
If (Len(ProcedureCPT) = 1) Then
    OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE CPT >= '" + UCase(Trim(ProcedureCPT)) + "' "
Else
    OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE CPT = '" + UCase(Trim(ProcedureCPT)) + "' "
End If
If (Trim(ProcedureDiagnosis) <> "") Then
    OrderString = OrderString + "And ICD9 = '" + UCase(Trim(ProcedureDiagnosis)) + "' "
End If
If (ProcedureRank > 0) Then
    OrderString = OrderString + "And Rank =" + Str(ProcedureRank) + " "
ElseIf (ProcedureRank = -1) Then
    OrderString = OrderString + "And Rank = 0 "
ElseIf (ProcedureRank = -2) Then
    OrderString = OrderString + "And Rank >= 0 "
Else
    OrderString = OrderString + "And Rank >= 0 "
End If
If (ProcedureRank <> -99) Then
    OrderString = OrderString + "And (EM <> 'T') "
End If
If (Len(ProcedureCPT) = 1) Then
    If (IType = 1) Then
        OrderString = OrderString + "ORDER BY Description ASC "
    Else
        OrderString = OrderString + "ORDER BY CPT ASC "
    End If
Else
    OrderString = OrderString + "ORDER BY ICD9 ASC, Rank ASC "
End If
Set ProcedureTbl = Nothing
Set ProcedureTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (ProcedureTbl.EOF) Then
    GetAllProcedure = -1
Else
    ProcedureTbl.MoveLast
    ProcedureTotal = ProcedureTbl.RecordCount
    GetAllProcedure = ProcedureTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllProcedure"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
    GetAllProcedure = -256
End Function

Private Function GetProcedureAllDistinct(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim SortBy As String
Dim OrderString As String
ProcedureTotal = 0
GetProcedureAllDistinct = -1
If (Trim(ProcedureCPT) = "") Then
    Exit Function
End If
OrderString = "SELECT DISTINCT CPT FROM ICD9CPTLinkTable WHERE CPT >= '" + UCase(Trim(ProcedureCPT)) + "' "
If (IType = 1) Then
    OrderString = "SELECT DISTINCT Description, CPT FROM ICD9CPTLinkTable WHERE Description >= '" + UCase(Trim(ProcedureCPT)) + "' "
    If (Left(ProcedureCPT, 1) = "*") Then
        OrderString = "SELECT DISTINCT Description, CPT FROM ICD9CPTLinkTable WHERE Description Like '%" + UCase(Trim(Mid(ProcedureCPT, 2, Len(ProcedureCPT) - 1))) + "%' "
    End If
End If
If (Trim(ProcedureDiagnosis) <> "") Then
    OrderString = OrderString + "And ICD9 = '" + UCase(Trim(ProcedureDiagnosis)) + "' "
End If
If (ProcedureRank > 0) Then
    OrderString = OrderString + "And Rank =" + Str(ProcedureRank) + " "
ElseIf (ProcedureRank = -1) Then
    OrderString = OrderString + "And Rank = 0 "
Else
    OrderString = OrderString + "And Rank >= 0 "
End If
If (Trim(ProcedureLinkType) <> "") Then
    OrderString = OrderString + "And SurgOrOffice = '" + Trim(ProcedureLinkType) + "' "
End If
OrderString = OrderString + "And EM <> 'T' "
If (IType = 1) Then
    OrderString = OrderString + "ORDER BY Description ASC "
Else
    OrderString = OrderString + "ORDER BY CPT ASC "
End If
Set ProcedureTbl = Nothing
Set ProcedureTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (ProcedureTbl.EOF) Then
    GetProcedureAllDistinct = -1
Else
    ProcedureTbl.MoveLast
    ProcedureTotal = ProcedureTbl.RecordCount
    GetProcedureAllDistinct = ProcedureTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetProcedureAllDistinct"
    GetProcedureAllDistinct = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetProcedurebyInsClass() As Long
On Error GoTo DbErrorHandler
Dim Temp As String
Dim Ref As String
Dim Ref1 As String
Dim OrderString As String
Ref = ""
Ref1 = ""
Temp = ""
ProcedureTotal = 0
GetProcedurebyInsClass = -1
OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE "
If ((Trim(ProcedureInsClass1) <> "") Or (Trim(ProcedureInsClass2) <> "") Or (Trim(ProcedureInsClass3) <> "") _
 Or (Trim(ProcedureInsClass4) <> "") Or (Trim(ProcedureInsClass5) <> "") Or (Trim(ProcedureInsClass6) <> "")) Or (Trim(ProcedureCPT) <> "") Or (Trim(ProcedureDiagnosis) <> "") Then
    If (Len(Trim(ProcedureCPT)) > 1) Then
        OrderString = OrderString + "CPT = '" + UCase(Trim(ProcedureCPT)) + "' "
        Ref = "And "
    ElseIf (Trim(ProcedureCPT) <> "") Then
        OrderString = OrderString + "CPT >= '" + UCase(Trim(ProcedureCPT)) + "' "
        Ref = "And "
    Else
        OrderString = OrderString + "CPT >= '" + Chr(1) + "' "
        Ref = "And "
    End If
    If (Trim(ProcedureDiagnosis) <> "") Then
        OrderString = OrderString + Ref + "ICD9 = '" + UCase(Trim(ProcedureDiagnosis)) + "' "
        Ref = "And "
    End If
    If (ProcedureRank > 0) Then
        OrderString = OrderString + Ref + "Rank =" + Str(ProcedureRank) + " "
        Ref = "And "
    ElseIf (ProcedureRank = -1) Then
        OrderString = OrderString + Ref + "Rank = 0 "
        Ref = "And "
    Else
        OrderString = OrderString + Ref + "Rank >= 0 "
        Ref = "And "
    End If
    If (Trim(ProcedureLinkType) <> "") Then
        OrderString = OrderString + Ref + "SurgOrOffice = '" + Trim(ProcedureLinkType) + "' "
        Ref = "And "
    End If
    OrderString = OrderString + Ref + "(EM <> 'T') "
    If (Trim(ProcedureInsClass1) <> "") Then
        Temp = "(InsClass1 = '" + Trim(ProcedureInsClass1) + "') "
        Ref1 = "Or "
    End If
    If (Trim(ProcedureInsClass2) <> "") Then
        Temp = Temp + Ref1 + "(InsClass2 = '" + Trim(ProcedureInsClass2) + "') "
        Ref1 = "Or "
    End If
    If (Trim(ProcedureInsClass3) <> "") Then
        Temp = Temp + Ref1 + "(InsClass3 = '" + Trim(ProcedureInsClass3) + "') "
        Ref1 = "Or "
    End If
    If (Trim(ProcedureInsClass4) <> "") Then
        Temp = Temp + Ref1 + "(InsClass4 = '" + Trim(ProcedureInsClass4) + "') "
        Ref1 = "Or "
    End If
    If (Trim(ProcedureInsClass5) <> "") Then
        Temp = Temp + Ref1 + "(InsClass5 = '" + Trim(ProcedureInsClass5) + "') "
        Ref1 = "Or "
    End If
    If (Trim(ProcedureInsClass6) <> "") Then
        Temp = Temp + Ref1 + "(InsClass6 = '" + Trim(ProcedureInsClass6) + "') "
        Ref1 = "Or "
    End If
    If (Temp <> "") Then
        Temp = "(" + Temp + ") "
        OrderString = OrderString + Ref + Temp
    End If
    OrderString = OrderString + "ORDER BY CPT ASC "
    Set ProcedureTbl = Nothing
    Set ProcedureTbl = New ADODB.Recordset
    MyDiagnosisMasterCmd.CommandText = OrderString
    Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
    If Not (ProcedureTbl.EOF) Then
        ProcedureTbl.MoveLast
        ProcedureTotal = ProcedureTbl.RecordCount
        GetProcedurebyInsClass = ProcedureTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetProcedurebyInsClass"
    GetProcedurebyInsClass = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetProcedureAll(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim SortBy As String
Dim OrderString As String
ProcedureTotal = 0
GetProcedureAll = -1
If (Trim(ProcedureCPT) = "") Then
    Exit Function
End If
If (Len(Trim(ProcedureCPT)) > 1) Then
    OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE CPT = '" + UCase(Trim(ProcedureCPT)) + "' "
Else
    OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE CPT >= '" + UCase(Trim(ProcedureCPT)) + "' "
End If
If (IType = 1) Then
    OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE Description >= '" + UCase(Trim(ProcedureCPT)) + "' "
End If
If (Trim(ProcedureDiagnosis) <> "") Then
    OrderString = OrderString + "And ICD9 = '" + UCase(Trim(ProcedureDiagnosis)) + "' "
End If
If (ProcedureRank > 0) Then
    OrderString = OrderString + "And Rank =" + Str(ProcedureRank) + " "
ElseIf (ProcedureRank = -1) Then
    OrderString = OrderString + "And Rank = 0 "
Else
    OrderString = OrderString + "And Rank >= 0 "
End If
If (Trim(ProcedureLinkType) <> "") Then
    OrderString = OrderString + "And SurgOrOffice = '" + Trim(ProcedureLinkType) + "' "
End If
OrderString = OrderString + "And (EM <> 'T') "
If (IType = 1) Then
    OrderString = OrderString + "ORDER BY Description ASC "
Else
    OrderString = OrderString + "ORDER BY CPT ASC "
End If
Set ProcedureTbl = Nothing
Set ProcedureTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (ProcedureTbl.EOF) Then
    GetProcedureAll = -1
Else
    ProcedureTbl.MoveLast
    ProcedureTotal = ProcedureTbl.RecordCount
    GetProcedureAll = ProcedureTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllProcedure"
    GetProcedureAll = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetProcedurebyDiagnosis() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
ProcedureTotal = 0
GetProcedurebyDiagnosis = -1
If (Trim(ProcedureDiagnosis) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE ICD9 = '" + UCase(Trim(ProcedureDiagnosis)) + "' And EM <> 'T' "
If (Trim(ProcedureLinkType) <> "") And (ProcedureLinkType <> "A") Then
    If (ProcedureLinkType <> "S") Then
        OrderString = OrderString + "And SurgOrOffice <> 'S' "
    Else
        OrderString = OrderString + "And SurgOrOffice = 'S' "
    End If
End If
OrderString = OrderString + "ORDER BY Rank ASC "
Set ProcedureTbl = Nothing
Set ProcedureTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (ProcedureTbl.EOF) Then
    GetProcedurebyDiagnosis = -1
Else
    ProcedureTbl.MoveLast
    ProcedureTotal = ProcedureTbl.RecordCount
    GetProcedurebyDiagnosis = ProcedureTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetProcedurebyDiagnosis"
    GetProcedurebyDiagnosis = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetProcedurebyEM() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
ProcedureTotal = 0
GetProcedurebyEM = -1
If (Trim(ProcedureCPT) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM ICD9CPTLinkTable WHERE (EM = 'T') And (Extra > 0) "
If (Len(Trim(ProcedureCPT)) = 1) Then
    OrderString = OrderString + "And CPT >= '" + Trim(ProcedureCPT) + "' "
Else
    OrderString = OrderString + "And CPT = '" + Trim(ProcedureCPT) + "' "
End If
If (ProcedureRank > 0) Then
    OrderString = OrderString + "And Rank = " + Str(ProcedureRank) + " "
End If
OrderString = OrderString + "ORDER BY Extra ASC, CPT ASC "
Set ProcedureTbl = Nothing
Set ProcedureTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call ProcedureTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (ProcedureTbl.EOF) Then
    GetProcedurebyEM = -1
Else
    ProcedureTbl.MoveLast
    ProcedureTotal = ProcedureTbl.RecordCount
    GetProcedurebyEM = ProcedureTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetProcedurebyEM"
    GetProcedurebyEM = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutProcedure()
On Error GoTo DbErrorHandler
PutProcedure = True
If (ProcedureNew) Then
    ProcedureTbl.AddNew
End If
ProcedureTbl("CPT") = UCase(Trim(ProcedureCPT))
ProcedureTbl("ICD9") = UCase(Trim(ProcedureDiagnosis))
ProcedureTbl("Rank") = ProcedureRank
ProcedureTbl("Description") = Trim(ProcedureName)
ProcedureTbl("EM") = "F"
If (ProcedureEM) Then
    ProcedureTbl("EM") = "T"
End If
ProcedureTbl("OrderDoc") = "F"
If (ProcedureOrderDoc) Then
    ProcedureTbl("OrderDoc") = "T"
End If
ProcedureTbl("ConsultOn") = "F"
If (ProcedureConsultOn) Then
    ProcedureTbl("ConsultOn") = "T"
End If
ProcedureTbl("Extra") = ProcedureOrder
ProcedureTbl("SurgOrOffice") = ProcedureLinkType
ProcedureTbl("LetterTranslation") = ProcedureLtrTrans
ProcedureTbl.Update
If (ProcedureNew) Then
'    ProcedureTbl.Move 0, ProcedureTbl.LastModified
    ProcedureId = ProcedureTbl("CPTLinkId")
    ProcedureNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutProcedure"
    PutProcedure = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeProcedure()
On Error GoTo DbErrorHandler
PurgeProcedure = True
If Not (ProcedureNew) Then
    ProcedureTbl.Delete
End If
Exit Function
DbErrorHandler:
    PurgeProcedure = False
    ModuleName = "PurgeProcedure"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectProcedure(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectProcedure = False
If (ProcedureTbl.EOF) Or (ListItem < 1) Or (ListItem > ProcedureTotal) Then
    Exit Function
End If
ProcedureTbl.MoveFirst
ProcedureTbl.Move ListItem - 1
SelectProcedure = True
ProcedureNew = False
Call LoadProcedure(0)
Exit Function
DbErrorHandler:
    ModuleName = "SelectProcedure"
    ProcedureId = -256
    SelectProcedure = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectProcedureDistinct(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectProcedureDistinct = False
If (ProcedureTbl.EOF) Or (ListItem < 1) Or (ListItem > ProcedureTotal) Then
    Exit Function
End If
ProcedureTbl.MoveFirst
ProcedureTbl.Move ListItem - 1
SelectProcedureDistinct = True
ProcedureNew = False
Call LoadProcedure(1)
Exit Function
DbErrorHandler:
    ModuleName = "SelectProcedureDistinct"
    ProcedureId = -256
    SelectProcedureDistinct = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyProcedure() As Boolean
ApplyProcedure = PutProcedure
End Function

Public Function RetrieveProcedure() As Boolean
RetrieveProcedure = GetProcedure
End Function

Public Function DeleteProcedure() As Boolean
DeleteProcedure = PurgeProcedure
End Function

Public Function FindProcedure() As Long
FindProcedure = GetAllProcedure(0)
End Function

Public Function FindProcedurebyNumericDistinct() As Long
FindProcedurebyNumericDistinct = GetProcedureAllDistinct(0)
End Function

Public Function FindProcedurebyAlphaDistinct() As Long
FindProcedurebyAlphaDistinct = GetProcedureAllDistinct(1)
End Function

Public Function FindProcedurebyNumeric() As Long
FindProcedurebyNumeric = GetProcedureAll(0)
End Function

Public Function FindProcedurebyAlpha() As Long
FindProcedurebyAlpha = GetProcedureAll(1)
End Function

Public Function FindProcedurebyDiagnosis() As Long
FindProcedurebyDiagnosis = GetProcedurebyDiagnosis
End Function

Public Function FindProcedurebyEM() As Long
FindProcedurebyEM = GetProcedurebyEM
End Function

Public Function FindProcedurebyInsClass() As Long
FindProcedurebyInsClass = GetProcedurebyInsClass
End Function

