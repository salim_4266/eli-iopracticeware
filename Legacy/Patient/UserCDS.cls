VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UserCDS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' CDS User Class
Option Explicit
'(Id INT PRIMARY KEY IDENTITY(1,1),ResourceType varchar(10),CDSConfiguration bit,Alerts bit,Info bit)
Public ID As Integer
Public ResourceType As String
Public CDSConfiguration As Integer
Public Alerts As Integer
Public info As Integer
Public Count As Integer
Private UserCDSTbl As ADODB.Recordset



Public Function GetCDSByRole(ResourceId As Integer) As Integer
 On Error GoTo CDS_Error
    Dim OrderString As String
    OrderString = "select Alerts FROM CDS_UserPermissions WHERE ResourceId=" + Trim(str(ResourceId))
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call UserCDSTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    GetCDSByRole = UserCDSTbl.RecordCount
'    Dim OrderString As String
'    OrderString = "Select Id,ResourceType,CDSConfiguration,Alerts,Info from CDSRoles where Resourcetype='" + Trim(ResourceType) + "'"
'    MyPracticeRepositoryCmd.CommandText = OrderString
'    Call UserCDSTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
'    GetCDSByRole = UserCDSTbl.RecordCount
    Exit Function
CDS_Error:
    GetCDSByRole = 0
     LogError "CDS", "GetCDS", Err, Err.Description
End Function

Public Function GetCds() As Integer
 On Error GoTo CDS_Error
    Dim OrderString As String
    OrderString = "Select * from CDSRoles "
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call UserCDSTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    GetCds = UserCDSTbl.RecordCount
    Exit Function
CDS_Error:
    GetCds = 0
     LogError "CDS", "GetCDS", Err, Err.Description
End Function

Public Function SelectCDS(ListItem As Integer) As Boolean
On Error GoTo CDS_Error
If (UserCDSTbl.EOF) Or (ListItem < 1) Or (ListItem > UserCDSTbl.RecordCount) Then
    Exit Function
End If
UserCDSTbl.MoveFirst
UserCDSTbl.Move ListItem - 1
Call LoadCDS
Exit Function
CDS_Error:
 LogError "CDS_Error", "CDS_Error", Err, Err.Description
End Function

Public Function UpdateCDS(objCDS As UserCDS) As Boolean
On Error GoTo CDS_Error
Dim OrderString As String
    OrderString = "UPDATE CDSRoles SET CDSConfiguration=" + str(objCDS.CDSConfiguration) + " ,Alerts=" + str(objCDS.Alerts) + " , Info=" + str(objCDS.info) + " where ResourceType  ='" + objCDS.ResourceType + "'"
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call UserCDSTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    UpdateCDS = True
Exit Function
CDS_Error:
UpdateCDS = False
 LogError "CDS_Error", "UpdateCDS", Err, Err.Description
End Function


Private Sub Class_Initialize()
Set UserCDSTbl = CreateAdoRecordset
'Call InitUserCDSTbl
End Sub
'Load Record
Private Sub LoadCDS()
'If UserCDSTbl("Id") <> "" Then
'    ID = UserCDSTbl("Id")
'Else
'    ID = 0
'    Exit Sub
'End If
'If UserCDSTbl("ResourceType") <> "" Then
'    ResourceType = UserCDSTbl("ResourceType")
'Else
'    ResourceType = ""
'End If
'If UserCDSTbl("CDSConfiguration") <> False Then
'    CDSConfiguration = 1
'Else
'    CDSConfiguration = 0
'End If
'If UserCDSTbl("Alerts") <> False Then
'    Alerts = 1
'Else
'    Alerts = 0
'End If
'If UserCDSTbl("Info") <> False Then
'    Info = 1
'Else
'    Info = 0
'End If
If UserCDSTbl("Alerts") <> False Then
    Alerts = 1
Else
    Alerts = 0
End If
End Sub
