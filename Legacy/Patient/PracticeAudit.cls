VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeAudit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Practice Audit Class
Option Explicit
Public AuditId As Long
Public AuditType As String
Public AuditDate As String
Public AuditTime As Long
Public AuditUserId As Long
Public AuditTableId As Integer
Public AuditRecordId As Long
Public AuditPrevRec As String
Public AuditCurrentRec As String

Private ModuleName As String
Private PracticeAuditTotal As Long
Private PracticeAuditTbl As ADODB.Recordset
Private PracticeAuditNew As Boolean

Private Sub Class_Initialize()
Set PracticeAuditTbl = CreateAdoRecordset
Call InitAudit
End Sub

Private Sub Class_Terminate()
Call InitAudit
Set PracticeAuditTbl = Nothing
End Sub

Private Sub InitAudit()
PracticeAuditNew = True
AuditId = 0
AuditType = ""
AuditDate = ""
AuditTime = 0
AuditUserId = 0
AuditTableId = 0
AuditRecordId = 0
AuditPrevRec = ""
AuditCurrentRec = ""
End Sub

Private Sub LoadAudit()
PracticeAuditNew = False
If (PracticeAuditTbl("AuditId") <> "") Then
    AuditId = PracticeAuditTbl("AuditId")
Else
    AuditId = 0
End If
If (PracticeAuditTbl("Action") <> "") Then
    AuditType = PracticeAuditTbl("Action")
Else
    AuditType = ""
End If
If (PracticeAuditTbl("Date") <> "") Then
    AuditDate = PracticeAuditTbl("Date")
Else
    AuditDate = ""
End If
If (PracticeAuditTbl("Time") <> "") Then
    AuditTime = PracticeAuditTbl("Time")
Else
    AuditTime = 0
End If
If (PracticeAuditTbl("UserId") <> "") Then
    AuditUserId = PracticeAuditTbl("UserId")
Else
    AuditUserId = 0
End If
If (PracticeAuditTbl("TableId") <> "") Then
    AuditTableId = PracticeAuditTbl("TableId")
Else
    AuditTableId = 0
End If
If (PracticeAuditTbl("RecordId") <> "") Then
    AuditRecordId = PracticeAuditTbl("RecordId")
Else
    AuditRecordId = 0
End If
If (PracticeAuditTbl("PrevRec") <> "") Then
    AuditPrevRec = PracticeAuditTbl("PrevRec")
Else
    AuditPrevRec = ""
End If
If (PracticeAuditTbl("CurrentRec") <> "") Then
    AuditCurrentRec = PracticeAuditTbl("CurrentRec")
Else
    AuditCurrentRec = ""
End If
End Sub

Private Function GetAudit() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
PracticeAuditTotal = 0
GetAudit = False
OrderString = "Select * FROM PracticeAudit WHERE AuditId =" + Str(AuditId) + " "
Set PracticeAuditTbl = Nothing
Set PracticeAuditTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeAuditTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeAuditTbl.EOF) Then
    GetAudit = True
    Call InitAudit
Else
    GetAudit = True
    Call LoadAudit
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAudit"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAuditList() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
PracticeAuditTotal = 0
Ref = ""
GetAuditList = -1
If (Trim(AuditUserId) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeAudit WHERE "
If (Trim(AuditDate) <> "") Then
    OrderString = OrderString + Ref + "Date = '" + UCase(Trim(AuditDate)) + "' "
    Ref = "And "
End If
If (AuditTime > 0) Then
    OrderString = OrderString + Ref + "Time >= " + Trim(Str(AuditTime)) + " "
    Ref = "And "
End If
If (Trim(AuditUserId) <> "") Then
    OrderString = OrderString + Ref + "UserId =" + Str(AuditUserId) + " "
    Ref = "And "
End If
If (Trim(AuditTableId) > 0) Then
    OrderString = OrderString + Ref + "TableId = " + Trim(Str(AuditTableId)) + " "
    Ref = "And "
End If
If (Trim(AuditRecordId) > 0) Then
    OrderString = OrderString + Ref + "RecordId = " + Trim(Str(AuditRecordId)) + " "
    Ref = "And "
End If
If (Trim(AuditType) <> "") Then
    OrderString = OrderString + Ref + "Action = '" + Trim(AuditType) + "' "
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY Date ASC, Time ASC "
Set PracticeAuditTbl = Nothing
Set PracticeAuditTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeAuditTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeAuditTbl.EOF) Then
    GetAuditList = -1
    Call InitAudit
Else
    PracticeAuditTbl.MoveLast
    PracticeAuditTotal = PracticeAuditTbl.RecordCount
    GetAuditList = PracticeAuditTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAuditList"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutAudit() As Boolean
On Error GoTo DbErrorHandler
PutAudit = True
If (PracticeAuditNew) Then
    PracticeAuditTbl.AddNew
End If
PracticeAuditTbl("Action") = UCase(AuditType)
PracticeAuditTbl("Date") = UCase(AuditDate)
PracticeAuditTbl("Time") = UCase(Trim(AuditTime))
PracticeAuditTbl("UserId") = AuditUserId
PracticeAuditTbl("TableId") = AuditTableId
PracticeAuditTbl("RecordId") = AuditRecordId
PracticeAuditTbl("PrevRec") = AuditPrevRec
PracticeAuditTbl("CurrentRec") = AuditCurrentRec
PracticeAuditTbl.Update
If (PracticeAuditNew) Then
    AuditId = GetJustAddedRecord
    PracticeAuditNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutAudit"
    PutAudit = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetAuditList > 0) Then
    If (SelectAudit(1)) Then
        GetJustAddedRecord = PracticeAuditTbl("AuditId")
    End If
End If
End Function

Private Function PurgeAudit() As Boolean
On Error GoTo DbErrorHandler
PurgeAudit = True
PracticeAuditTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeAudit"
    PurgeAudit = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectAudit(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectAudit = False
If (PracticeAuditTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeAuditTotal) Then
    Exit Function
End If
PracticeAuditTbl.MoveFirst
PracticeAuditTbl.Move ListItem - 1
SelectAudit = True
Call LoadAudit
Exit Function
DbErrorHandler:
    ModuleName = "SelectAudit"
    SelectAudit = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyAudit() As Boolean
ApplyAudit = PutAudit
End Function

Public Function DeleteAudit() As Boolean
DeleteAudit = PurgeAudit
End Function

Public Function FindAudit() As Long
FindAudit = GetAuditList
End Function

Public Function RetrieveAudit() As Long
RetrieveAudit = GetAudit()
End Function

