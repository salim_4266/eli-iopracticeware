Attribute VB_Name = "Library"
Option Explicit

Public MyParameters As Variant
Public dbFeeSchedule As String
Public NotesDirectory As String

' For Dynamic Screens
Public HelpButtonText As String
Public ChangeButtonText As String
Public DoneButtonText As String
Public DontKnowButtonText As String
Public NoToAllButtonText As String

' For Aborting Scheduler Search
Public AbortSlotSearch As Boolean

Public Const PRINTER_Main As Integer = 1
Public Const PRINTER_Other As Integer = 2
Public Const PRINTER_Rx As Integer = 3
Public Const PRINTER_WrittenDocuments As Integer = 4
Public Const PRINTER_Image As Integer = 5
Public Const PRINTER_Wear As Integer = 6

' For killing processes ONLY
Public Const TH32CS_SNAPALL = TH32CS_SNAPHEAPLIST Or TH32CS_SNAPPROCESS Or TH32CS_SNAPTHREAD Or TH32CS_SNAPMODULE
Public Const TH32CS_INHERIT As Long = 8&
Public Const HF32_DEFAULT As Long = 1&
Public Const HF32_SHARED As Long = 2&
Public Const LF32_FIXED As Long = 1&
Public Const LF32_FREE As Long = 2&
Public Const LF32_MOVEABLE As Long = 4&
Public Const PROCESS_TERMINATE As Long = 1&

Public Type HEAPLIST
   dwSize As Long
   th32ProcessID As Long
   th32HeapId As Long
   dwFlags As Long
End Type

Public Type HEAPENTRY
   dwSize As Long
   hHandle As Long
   dwAddress As Long
   dwBlockSize As Long
   dwFlags As Long
   dwLockCount As Long
   dwResvd As Long
   th32ProcessID As Long
   th32HeapId As Long
End Type

Public Type PROCESSENTRY
   dwSize As Long
   cntUsage As Long
   th32ProcessID As Long
   th32DefaultHeapID As Long
   th32ModuleID As Long
   cntThreads As Long
   th32ParentProcessID As Long
   pcPriClassBase As Long
   dwFlags As Long
   szExeFile As String * MAX_PATH
End Type

Public Type THREADENTRY
   dwSize As Long
   cntUsage As Long
   th32ThreadID As Long
   th32OwnerProcessID As Long
   tpBasePri As Long
   tpDeltaPri As Long
   dwFlags As Long
End Type

Public Type MODULEENTRY
   dwSize As Long
   th32ModuleID As Long
   th32ProcessID As Long
   GlblcntUsage As Long
   ProccntUsage As Long
   modBaseAddr As Long
   modBaseSize As Long
   hModule As Long
   szModule As String * MAX_MODULE_NAME32
   szExePath As String * MAX_PATH
End Type
 
Private Type POINTAPI
    X As Long
    Y As Long
End Type

Private Type RECT
    Left    As Long
    Top     As Long
    Right   As Long
    Bottom  As Long
End Type

Public Enum EFormResults
    efrHome = 1
    efrDone = 2
End Enum

Public Enum EEyeRef
    eerNone = 0
    eerOD = 1
    eerOS = 2
    eerOU = 3
End Enum

' For Form Alert Display
Public Enum EAlertType
    eatDemo = 1
    eatFinancial = 2
    eatSchedule = 3
    eatClinical = 4
    eatCheckout = 5
    eatExam = 6
    eatPlan = 7
    eatHighlight = 8
    eatInsBatch = 9
    eatEM = 10
    eatCheckin = 11
End Enum

Public Declare Function ClipCursor Lib "user32" (lpRect As RECT) As Long
Public Declare Function ClipCursorByNum Lib "user32" Alias "ClipCursor" (ByVal lpRect As Long) As Long
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer
Public Declare Function GetClientRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long
Public Declare Function ClientToScreen Lib "user32" (ByVal hWnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function InvalidateRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT, ByVal bErase As Long) As Long
Public Declare Function InvalidateRectByNum Lib "user32" Alias "InvalidateRect" (ByVal hWnd As Long, ByVal lpRect As Long, ByVal bErase As Long) As Long


Public Declare Function CreateToolhelpSnapshot Lib "kernel32" Alias "CreateToolhelp32Snapshot" (ByVal dwFlags As Long, ByVal dwProcessID As Long) As Long
Public Declare Function HeapListFirst Lib "kernel32" Alias "Heap32ListFirst" (ByVal hSnapShot As Long, lphl As HEAPLIST) As Long
Public Declare Function HeapListNext Lib "kernel32" Alias "Heap32ListNext" (ByVal hSnapShot As Long, lphl As HEAPLIST) As Long
Public Declare Function HeapFirst Lib "kernel32" Alias "Heap32First" (ByRef lphe As HEAPENTRY, ByVal dwProcessID As Long, ByVal dwHeapId As Long) As Long
Public Declare Function HeapNext Lib "kernel32" Alias "Heap32Next" (ByRef lphe As HEAPENTRY) As Long
Public Declare Function ToolhelpReadProcessMem Lib "kernel32" Alias "Toolhelp32ReadProcessMemory" (ByVal dwProcessID As Long, ByVal lpBaseAddress As Long, ByRef lpBuffer As Long, ByVal cbRead As Long, ByRef lpNumberOfBytesRead As Long) As Long
Public Declare Function ProcessFirst Lib "kernel32" Alias "Process32First" (ByVal hSnapShot As Long, lppe As PROCESSENTRY) As Long
Public Declare Function ProcessNext Lib "kernel32" Alias "Process32Next" (ByVal hSnapShot As Long, lppe As PROCESSENTRY) As Long
Public Declare Function ThreadFirst Lib "kernel32" Alias "Thread32First" (ByVal hSnapShot As Long, lpte As THREADENTRY) As Long
Public Declare Function ThreadNext Lib "kernel32" Alias "Thread32Next" (ByVal hSnapShot As Long, lpte As THREADENTRY) As Long
Public Declare Function ModuleFirst Lib "kernel32" Alias "Module32First" (ByVal hSnapShot As Long, lpme As MODULEENTRY) As Long
Public Declare Function ModuleNext Lib "kernel32" Alias "Module32Next" (ByVal hSnapShot As Long, lpme As MODULEENTRY) As Long
Public Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessID As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
Public Const VK_SNAPSHOT As Byte = 44 ' PrintScreen virtual keycode
Public Const PS_TheForm = 0
Public Const PS_TheScreen As Byte = 1

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private ProcessArray(1000) As String


Public Sub DoSleep(AMill As Long)
Sleep AMill
DoEvents
End Sub

Public Function DisplayEyeRef(EyeRef As EEyeRef) As String
    Select Case EyeRef
        Case EEyeRef.eerNone
            DisplayEyeRef = ""
        Case EEyeRef.eerOD
            DisplayEyeRef = "OD"
        Case EEyeRef.eerOS
            DisplayEyeRef = "OS"
        Case EEyeRef.eerOU
            DisplayEyeRef = "OU"
    End Select
End Function

Public Sub SaveScreenToFile(AFile As String)
On Error GoTo l_SaveScreenToFileErrorHandler
If (Trim(AFile) <> "") Then
    Dim ScreenCaptureComWrapper As New comWrapper
    Dim arguments() As Variant
    arguments = Array(AFile)
    Call ScreenCaptureComWrapper.InvokeStaticMethod("IO.Practiceware.ScreenCapture", "TryCaptureAndSaveScreen", arguments)
    Exit Sub
l_SaveScreenToFileErrorHandler:
    LogError "Library", "SaveScreenToFile", Err, Err.Description
End If
End Sub

Public Sub PinpointError(ModName As String, ErrorText As String)
    On Error Resume Next
    Err.Raise 17 'Can't perform requested operation
    LogError ModName, "", Err, ErrorText, "PinpointError"
    Err.Clear
End Sub

Public Sub CleanUp(TheDirectory As String, FileFamily As String, BackUpOn As Boolean)
On Error GoTo UI_ErrorHandler
Dim OtherDirectory As String
Dim MyFile As String
OtherDirectory = TheDirectory + "Backup\"
Close
MyFile = Dir(TheDirectory + FileFamily)
Do Until (MyFile = "")
    If (BackUpOn) Then
        FM.Name (TheDirectory + MyFile), (OtherDirectory + MyFile)
    Else
        FM.Kill TheDirectory + MyFile
    End If

    MyFile = Dir
Loop
Exit Sub
UI_ErrorHandler:
    LogError "Library", "CleanUp", Err, Err.Description
    Resume LeaveFast
LeaveFast:
End Sub

Public Function AppendFiles(FromFile As String, ToFile As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Long
Dim Rec As String
Dim FileNum1 As Long
Dim FileNum2 As Long
AppendFiles = False
If (Trim$(FromFile) = "") Or (Trim$(ToFile) = "") Then
    Exit Function
End If
FileNum1 = FreeFile
FileNum2 = FileNum1 + 1
FM.OpenFile FromFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum1)
FM.OpenFile ToFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum2)
i = LOF(FileNum2)
Do Until (EOF(FileNum1))
    Line Input #FileNum1, Rec
    If (Len(Rec) > 10) Then
        Print #FileNum2, Rec
    ElseIf (i < 10) Then
        Print #FileNum2, Rec
    End If
Loop
FM.CloseFile CLng(FileNum1)
FM.CloseFile CLng(FileNum2)
AppendFiles = True
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function CopyFiles(TheDirectory As String, FileFamily As String, FromExtention As String, ToExtention As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim q As Integer
Dim MyFile As String
Dim OldFileName As String
Dim NewFileName As String
CopyFiles = False
If (Trim$(FromExtention) = "") Or (Trim$(ToExtention) = "") Then
    Exit Function
End If
MyFile = Dir(FileFamily + Trim$(FromExtention))
Do Until (MyFile = "")
    OldFileName = TheDirectory + MyFile
    q = InStrPS(MyFile, ".")
    If (q = 0) Then
        q = Len(MyFile) + 1
    End If
    OldFileName = TheDirectory + MyFile
    NewFileName = TheDirectory + Left(MyFile, q - 1) + Trim$(ToExtention)
    Call FileCopy(OldFileName, NewFileName)
    MyFile = Dir
Loop
CopyFiles = True
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function RenameFiles(TheDirectory As String, FileFamily As String, FromExtention As String, ToExtention As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim q As Integer
Dim MyFile As String
Dim OldFileName As String
Dim NewFileName As String
RenameFiles = False
If (Trim$(FromExtention) = "") Or (Trim$(ToExtention) = "") Then
    Exit Function
End If
MyFile = Dir(FileFamily + Trim$(FromExtention))
Do Until (MyFile = "")
    OldFileName = TheDirectory + MyFile
    q = InStrPS(MyFile, ".")
    If (q = 0) Then
        q = Len(MyFile) + 1
    End If
    OldFileName = TheDirectory + MyFile
    NewFileName = TheDirectory + Left(MyFile, q - 1) + Trim$(ToExtention)
    FM.Name OldFileName, NewFileName
    MyFile = Dir
Loop
RenameFiles = True
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Sub NSFDollarValue(TheText As String, NewText As String)
Dim q As Integer
Dim w As Single
Dim BuildText As String
NewText = ""
If (Trim$(TheText) = "") Then
    NewText = "0000000"
    Exit Sub
End If
If (Val(TheText) < 0) Then
    w = Val(TheText) - 0.004
Else
    w = Val(TheText) + 0.004
End If
BuildText = Trim$(str$(w))
q = InStrPS(BuildText, ".")
If (q = 0) Then
    BuildText = BuildText + ".00"
ElseIf (Len(BuildText) > q + 2) Then
    BuildText = Left(BuildText, q + 2)
ElseIf (Len(BuildText) = q + 1) Then
    BuildText = Left(BuildText, q + 1) + "0"
End If
Call StripCharacters(BuildText, ".")
If (Len(BuildText) <= 7) Then
    NewText = String$(7 - Len(BuildText), 48) + BuildText
ElseIf (Len(BuildText) < 11) And (Len(BuildText) > 7) Then
    NewText = String$(11 - Len(BuildText), 48) + BuildText
Else
    NewText = BuildText
End If
End Sub

Public Sub NSFPadValue(TheText As String, NumChar As Integer)
TheText = Trim$(TheText)
If (Trim$(TheText) = "") Then
    TheText = String$(NumChar, 32)
Else
    If (Len(TheText) > NumChar) Then
        TheText = Left(TheText, NumChar)
    Else
        TheText = TheText + String$(NumChar - Len(TheText), 32)
    End If
End If
End Sub

Public Sub NSFFillValue(TheText As String, NumChar As Integer)
TheText = Trim$(TheText)
If (Trim$(TheText) = "") Then
    TheText = String$(NumChar, 48)
Else
    If (Len(TheText) > NumChar) Then
        TheText = Left(TheText, NumChar)
    Else
        TheText = String$(NumChar - Len(TheText), 48) + TheText
    End If
End If
End Sub

Public Sub DisplayDate(TheText As String, NewText As String)
Dim yy As Integer
Dim TempText As String
NewText = ""
TempText = TheText
Call StripCharacters(TempText, "/")
Call StripCharacters(TempText, "-")
TempText = Trim$(TempText)
If (Len(TempText) = 4) Then
    If (Left(TempText, 1) <> "0") Then
        TempText = "0" + Left(TempText, 1) + "/" + "0" + Mid(TempText, 2, 1) + "/" + Mid(TempText, 3, 2)
    Else
        yy = Year(Date)
        TempText = Left(TempText, 2) + "/" + Mid(TempText, 3, 2) + "/" + Trim$(str$(yy))
    End If
ElseIf (Len(TempText) = 5) Then
    TempText = "0" + Left(TempText, 1) + "/" + Mid(TempText, 2, 2) + "/" + Mid(TempText, 4, 2)
ElseIf (Len(TempText) = 6) Then
    TempText = Left(TempText, 2) + "/" + Mid(TempText, 3, 2) + "/" + Mid(TempText, 5, 2)
ElseIf (Len(TempText) = 7) Then
    TempText = "0" + Left(TempText, 1) + "/" + Mid(TempText, 2, 2) + "/" + Mid(TempText, 4, 4)
ElseIf (Len(TempText) = 8) Then
    TempText = Left(TempText, 2) + "/" + Mid(TempText, 3, 2) + "/" + Mid(TempText, 5, 4)
Else
    TempText = ""
End If
NewText = TempText
End Sub

Public Function IsCurrency(TheText As String) As Boolean
Dim i As Integer
IsCurrency = False
If (Len(TheText) = 0) Then
    Exit Function
End If
For i = 1 To Len(TheText)
    If Not (IsNumeric(Mid(TheText, i, 1))) Then
        If (Mid(TheText, i, 1) <> Chr(127)) And (Mid(TheText, i, 1) <> Chr(8)) Then
            If (Mid(TheText, i, 1) <> ".") Then
                If (i = 1) Then
                    If (Mid(TheText, 1, 1) <> "-") Then
                        Exit Function
                    End If
                Else
                    Exit Function
                End If
            End If
        End If
    End If
Next i
IsCurrency = True
End Function

Public Function FormatJulianDate(TheDate As String) As String
Dim i As Integer
Dim mm As Integer
Dim TheDays As Integer
Dim Temp As String
Dim DaysPerMonth(12) As Integer
FormatJulianDate = "000"
If (Trim$(TheDate) <> "") Then
    DaysPerMonth(1) = 31
    DaysPerMonth(2) = 28
    DaysPerMonth(3) = 31
    DaysPerMonth(4) = 30
    DaysPerMonth(5) = 31
    DaysPerMonth(6) = 30
    DaysPerMonth(7) = 31
    DaysPerMonth(8) = 31
    DaysPerMonth(9) = 30
    DaysPerMonth(10) = 31
    DaysPerMonth(11) = 30
    DaysPerMonth(12) = 31
    mm = Val(Left(TheDate, 2))
    TheDays = 0
    For i = 1 To mm - 1
        TheDays = TheDays + DaysPerMonth(i)
    Next i
    TheDays = TheDays + Val(Mid(TheDate, 4, 2))
    Temp = Trim$(str$(TheDays))
    FormatJulianDate = String$(3 - Len(Temp), "0") + Temp
End If
End Function

Public Sub GetWeekStart(TheDate As String, NewDate As String)
Dim TheDay As Integer
Dim DD As Integer, d As String
Dim mm As Integer, m As String
Dim yy As Integer, Y  As String
Dim NumDays As Long
Dim TempDate As Date
NewDate = ""
If (Trim$(TheDate) = "") Then
    Exit Sub
End If
If (OkDate(TheDate)) Then
    TheDay = Weekday(TheDate)
    If (TheDay <> vbSunday) Then
        NumDays = vbSunday - TheDay
        TempDate = DateAdd("d", NumDays, TheDate)
        DD = Day(TempDate)
        If (DD < 10) Then
            d = "0" + Trim$(str$(DD))
        Else
            d = Trim$(str$(DD))
        End If
        mm = Month(TempDate)
        If (mm < 10) Then
            m = "0" + Trim$(str$(mm))
        Else
            m = Trim$(str$(mm))
        End If
        yy = Year(TempDate)
        If (yy < 10) Then
            Y = "000" + Trim$(str$(yy))
        ElseIf (yy < 100) Then
            Y = "00" + Trim$(str$(yy))
        ElseIf (yy < 1000) Then
            Y = "0" + Trim$(str$(yy))
        Else
            Y = Trim$(str$(yy))

        End If
        NewDate = m + "/" + d + "/" + Y
    Else
        NewDate = TheDate
    End If
End If
End Sub

Public Sub GetStartOfWeek(TheWeek As Integer, TheYear As String, TheDate As String)
Dim DayOfWeek As Integer
Dim StartDate As String
Dim DD As Integer, d As String
Dim mm As Integer, m As String
Dim yy As Integer, Y  As String
Dim NumDays As Long
Dim TempDate2 As Date
TheDate = ""
If (Trim$(TheYear) = "") Or (TheWeek < 1) Or (TheWeek > 52 * 10) Then
    Exit Sub
End If
DayOfWeek = Weekday("01/01/" + TheYear)
If (DayOfWeek <> vbSunday) Then
    NumDays = (7 + vbSunday) - DayOfWeek
    StartDate = "01/0" + Trim$(str$(NumDays + 1)) + "/" + TheYear
End If
NumDays = (TheWeek - 1) * 7
TempDate2 = DateAdd("d", NumDays, StartDate)
DD = Day(TempDate2)
If (DD < 10) Then
    d = "0" + Trim$(str$(DD))
Else
    d = Trim$(str$(DD))
End If
mm = Month(TempDate2)
If (mm < 10) Then
    m = "0" + Trim$(str$(mm))
Else
    m = Trim$(str$(mm))
End If
yy = Year(TempDate2)
If (yy < 10) Then
    Y = "000" + Trim$(str$(yy))
ElseIf (yy < 100) Then
    Y = "00" + Trim$(str$(yy))
ElseIf (yy < 1000) Then
    Y = "0" + Trim$(str$(yy))
Else
    Y = Trim$(str$(yy))

End If
TheDate = m + "/" + d + "/" + Y
End Sub

Public Sub AdjustDate(ADate As String, NumberOfDays As Integer, NewDate As String)
Dim UDate As Date
Dim LDate As Date
Dim DD As Integer, d As String
Dim mm As Integer, m As String
Dim yy As Integer, Y  As String
NewDate = ""
If (NumberOfDays = 0) Then
    NewDate = ADate
ElseIf (Trim$(ADate) <> "") Then
    If (OkDate(ADate)) Then
        LDate = DateValue(ADate)
        UDate = DateAdd("d", NumberOfDays, LDate)
        DD = Day(UDate)
        If (DD < 10) Then
            d = "0" + Trim$(str$(DD))
        Else
            d = Trim$(str$(DD))
        End If
        mm = Month(UDate)
        If (mm < 10) Then
            m = "0" + Trim$(str$(mm))
        Else
            m = Trim$(str$(mm))
        End If
        yy = Year(UDate)
        If (yy < 10) Then
            Y = "000" + Trim$(str$(yy))

        ElseIf (yy < 100) Then
            Y = "00" + Trim$(str$(yy))

        ElseIf (yy < 1000) Then
            Y = "0" + Trim$(str$(yy))

        Else
            Y = Trim$(str$(yy))
        End If
        NewDate = m + "/" + d + "/" + Y
    End If
End If
End Sub

Public Sub BackDate(ADate As String, NumberOfDays As Integer, NewDate As String)
Dim UDate As Date
Dim LDate As Date
Dim DD As Integer, d As String
Dim mm As Integer, m As String
Dim yy As Integer, Y  As String
NewDate = ""
If (Trim$(ADate) = "") Then
    Exit Sub
End If
If (NumberOfDays = 0) Then
    NewDate = ADate
Else
    If (OkDate(ADate)) Then
        LDate = DateValue(ADate)
        UDate = DateAdd("d", -1 * NumberOfDays, LDate)
        DD = Day(UDate)
        If (DD < 10) Then
            d = "0" + Trim$(str$(DD))
        Else
            d = Trim$(str$(DD))
        End If
        mm = Month(UDate)
        If (mm < 10) Then
            m = "0" + Trim$(str$(mm))
        Else
            m = Trim$(str$(mm))
        End If
        yy = Year(UDate)
        If (yy < 10) Then
            Y = "000" + Trim$(str$(yy))

        ElseIf (yy < 100) Then
            Y = "00" + Trim$(str$(yy))

        ElseIf (yy < 1000) Then
            Y = "0" + Trim$(str$(yy))

        Else
            Y = Trim$(str$(yy))

        End If
        NewDate = m + "/" + d + "/" + Y
    End If
End If
End Sub

Public Sub SetMinMaxDates(MinDate As String, MaxDate As String)
Dim Y As Integer
Dim y1 As Integer
Dim y2 As Integer
Dim ADate As String
MinDate = ""
MaxDate = ""
ADate = CheckConfigCollection("CALENDARDATE")
Call FormatTodaysDate(ADate, False)
Y = Val(Mid(ADate, Len(ADate) - 3, 4))
y1 = Y
y2 = Y + 10
MinDate = Left(ADate, Len(ADate) - 4) + Trim$(str$(y1))
MaxDate = Left(ADate, Len(ADate) - 4) + Trim$(str$(y2))
End Sub

Public Sub AddTime(TheTime As String, TheDuration As String, NewTime As String)
Dim NewTim As Integer
NewTim = ConvertTimeToMinutes(TheTime)
NewTim = NewTim + Val(TheDuration)
Call ConvertMinutesToTime(NewTim, NewTime)
End Sub

Public Sub SubTime(TheTime As String, TheDuration As String, NewTime As String)
Dim NewTim As Integer
NewTim = ConvertTimeToMinutes(TheTime)
NewTim = NewTim - Val(TheDuration)
Call ConvertMinutesToTime(NewTim, NewTime)
End Sub

Public Function CompareTime(TheTime1 As String, TheTime2 As String) As Integer
Dim NewTim1 As Integer
Dim NewTim2 As Integer
NewTim1 = ConvertTimeToMinutes(TheTime1)
NewTim2 = ConvertTimeToMinutes(TheTime2)
If (NewTim1 > NewTim2) Then
    CompareTime = 1
ElseIf (NewTim1 < NewTim2) Then
    CompareTime = -1
Else
    CompareTime = 0
End If
End Function

Public Function CompareDate(TheDate1 As String, TheDate2 As String) As Integer
Dim oDate1 As New CIODateTime
Dim oDate2 As New CIODateTime
    CompareDate = 0
    oDate1.SetDateFromDisplay TheDate1
    oDate2.SetDateFromDisplay TheDate2
    If oDate1.MyDateTime > oDate2.MyDateTime Then
        CompareDate = 1
    ElseIf oDate1.MyDateTime < oDate2.MyDateTime Then
        CompareDate = -1
    End If
    Set oDate1 = Nothing
    Set oDate2 = Nothing
End Function

Public Function IsChoiceOn(TheFile As String, TheControl As String) As Boolean
Dim k As Integer, FileNum As Integer
Dim RecString As String, RefString As String
On Error GoTo BadNews
IsChoiceOn = False
TheControl = UCase(Trim$(TheControl))
FileNum = FreeFile
FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
While Not EOF(FileNum)
    Line Input #FileNum, RecString
    k = InStrPS(RecString, "=")
    If (k = 0) Then
        k = Len(RecString)
    End If
    RefString = UCase(Trim$(Left(RecString, k - 1)))
    If (RefString = TheControl) Then
        If (TheControl = "NO") Then
            If (Mid(RecString, k + 1, 1) = "T") Then
                IsChoiceOn = True
                FM.CloseFile CLng(FileNum)
                Exit Function
            End If
        Else
            IsChoiceOn = True
            FM.CloseFile CLng(FileNum)
            Exit Function
        End If
    ElseIf (TheControl = "PRESENCE") Then
        IsChoiceOn = True
        FM.CloseFile CLng(FileNum)
        Exit Function
    End If
Wend
FM.CloseFile CLng(FileNum)
Exit Function
BadNews:
    Resume LeaveFast
LeaveFast:
    FM.CloseFile CLng(FileNum)
End Function

Public Function PracticeTimes(PracticeId As Long, TheDate As String, StartTime As String, EndTime As String) As Boolean
Dim PracticeFind As PracticeName
Dim DayNumber As Integer
PracticeTimes = False
If (TheDate = "") Then
    TheDate = Date
End If
If (IsDate(TheDate)) Then
    DayNumber = Weekday(CDate(TheDate))
Else
    DayNumber = 1
End If
Set PracticeFind = New PracticeName
If (PracticeId < 1) Then
    PracticeFind.PracticeName = Chr(1)
    PracticeFind.PracticeType = "P"
    If (PracticeFind.FindPractice > 0) Then
        If (PracticeFind.SelectPractice(1)) Then
            PracticeId = PracticeFind.PracticeId
            If (DayNumber = 1) Then
                StartTime = PracticeFind.PracticeStartTime1
                EndTime = PracticeFind.PracticeEndTime1
            ElseIf (DayNumber = 2) Then
                StartTime = PracticeFind.PracticeStartTime2
                EndTime = PracticeFind.PracticeEndTime2
            ElseIf (DayNumber = 3) Then
                StartTime = PracticeFind.PracticeStartTime3
                EndTime = PracticeFind.PracticeEndTime3
            ElseIf (DayNumber = 4) Then
                StartTime = PracticeFind.PracticeStartTime4
                EndTime = PracticeFind.PracticeEndTime4
            ElseIf (DayNumber = 5) Then
                StartTime = PracticeFind.PracticeStartTime5
                EndTime = PracticeFind.PracticeEndTime5
            ElseIf (DayNumber = 6) Then
                StartTime = PracticeFind.PracticeStartTime6
                EndTime = PracticeFind.PracticeEndTime6
            Else
                StartTime = PracticeFind.PracticeStartTime7
                EndTime = PracticeFind.PracticeEndTime7
            End If
        End If
    End If
Else
    PracticeFind.PracticeId = PracticeId
    If (PracticeFind.RetrievePracticeName) Then
        If (DayNumber = 1) Then
            StartTime = PracticeFind.PracticeStartTime1
            EndTime = PracticeFind.PracticeEndTime1
        ElseIf (DayNumber = 2) Then
            StartTime = PracticeFind.PracticeStartTime2
            EndTime = PracticeFind.PracticeEndTime2
        ElseIf (DayNumber = 3) Then
            StartTime = PracticeFind.PracticeStartTime3
            EndTime = PracticeFind.PracticeEndTime3
        ElseIf (DayNumber = 4) Then
            StartTime = PracticeFind.PracticeStartTime4
            EndTime = PracticeFind.PracticeEndTime4
        ElseIf (DayNumber = 5) Then
            StartTime = PracticeFind.PracticeStartTime5
            EndTime = PracticeFind.PracticeEndTime5
        ElseIf (DayNumber = 6) Then
            StartTime = PracticeFind.PracticeStartTime6
            EndTime = PracticeFind.PracticeEndTime6
        Else
            StartTime = PracticeFind.PracticeStartTime7
            EndTime = PracticeFind.PracticeEndTime7
        End If
    Else
        StartTime = "12:00 AM"
        EndTime = "11:45 PM"
    End If
End If
Set PracticeFind = Nothing
PracticeTimes = True
End Function

Public Sub LoadTime(lstBox As ListBox, StartTime As String, EndTime As String, BreakDown As Integer)
Dim f As Integer, q As Integer, i As Integer
Dim Init1 As Integer, Init2 As Integer
Dim StartHour As Integer, EndHour As Integer
Dim StartMn As Integer, EndMn As Integer
Dim Pr As String, StartPr As String, EndPr As String, Hrs As String, Mns As String
lstBox.Clear
lstBox.AddItem "        "
q = InStrPS(StartTime, ":")
If (q = 0) Then
    StartTime = "12:00 AM"
    q = 3
End If
StartHour = Val(Left(StartTime, q - 1))
StartMn = Val(Mid(StartTime, q + 1, 2))
StartPr = Mid(StartTime, Len(StartTime) - 1, 2)
q = InStrPS(EndTime, ":")
If (q = 0) Then
    EndTime = "11:45 PM"
    q = 3
End If
EndHour = Val(Left(EndTime, q - 1))
EndMn = Val(Mid(EndTime, q + 1, 2))
EndPr = Mid(EndTime, Len(EndTime) - 1, 2)
If (StartPr = "PM") Then
    StartHour = StartHour + 12
Else
    If (StartHour = 12) Then
        StartHour = 0
    End If
End If
If (EndPr = "PM") Then
    EndHour = EndHour + 12
Else
    If (EndHour = 12) Then
        EndHour = 0
    End If
End If
For i = StartHour To EndHour
    If (i > 11) Then
        Pr = EndPr
    Else
        Pr = StartPr
    End If
    Hrs = Trim$(str$(i))
    If (Pr = "PM") And (i <> 12) Then
        Hrs = Trim$(str$(i - 12))
    End If
    If (i = 0) Then
        Hrs = "12"
    End If
    If (Len(Hrs) = 1) Then
        Hrs = "0" + Hrs
    End If
    If (i = StartHour) Then
        Init1 = StartMn
        Init2 = 59
    ElseIf (i = EndHour) Then
        Init1 = 0
        Init2 = EndMn
    Else
        Init1 = 0
        Init2 = 59
    End If
    For f = Init1 To Init2 Step BreakDown
        Mns = Trim$(str$(f))
        If (Len(Mns) < 2) Then
            Mns = "0" + Mns
        End If
        lstBox.AddItem Hrs + ":" + Mns + " " + Pr
    Next f
Next i
End Sub

Public Function GetTime(TheTime As String, TheList As ListBox) As Integer
Dim k As Integer
If (TheTime = "") Then
    GetTime = 0
    Exit Function
End If
For k = 1 To TheList.ListCount - 1
    If (TheTime = TheList.List(k)) Then
        GetTime = k
        Exit Function
    End If
Next k
GetTime = 0
End Function

Public Function GetAge(BirthDate As String) As Integer
Dim Offset As Integer
Dim LclDate1 As String
Dim LclDate2 As String
Dim BirthYear As Integer
Dim CurrentYear As Integer
Dim MyDate As Date
Offset = 0
LclDate1 = ""
MyDate = Date
If (Trim$(BirthDate) = "") Then
    GetAge = -1
    Exit Function
End If
Call FormatTodaysDate(LclDate1, False)
LclDate1 = Left(LclDate1, 2) + Mid(LclDate1, 4, 2)
LclDate2 = Mid(BirthDate, 5, 4)
CurrentYear = Year(MyDate)
BirthYear = Val(Left(BirthDate, 4))
If (InStrPS(BirthDate, "/") > 0) Then
    LclDate2 = Left(BirthDate, 2) + Mid(BirthDate, 4, 2)
    BirthYear = Val(Mid(BirthDate, 7, 4))
End If
If (LclDate2 > LclDate1) Then
    Offset = 1
End If
GetAge = (CurrentYear - BirthYear) - Offset
End Function

Public Function GetAgePrint(BirthDate As String, AppDate As String) As Integer
Dim Offset As Integer
Dim LclDate1 As String
Dim LclDate2 As String
Dim BirthYear As Integer
    Dim CurrentYear As Integer
    Dim StartDate As String
    Dim StartMonth As String
    Dim StartYear As String
    Dim EndDate As String
    Dim EndMonth As String
    Dim EndYear As String
Dim MyDate As Date
Offset = 0
LclDate1 = ""
MyDate = Date
If (Trim$(BirthDate) = "") Then
    GetAgePrint = -1
    Exit Function
End If
'Call FormatTodaysDate(LclDate1, False)
LclDate1 = AppDate

'LclDate1 = Left(LclDate1, 2) + Mid(LclDate1, 4, 2)
LclDate1 = Left(LclDate1, 4)

LclDate2 = Mid(BirthDate, 5, 4)

CurrentYear = Year(MyDate)
BirthYear = Val(Left(BirthDate, 4))
If (InStrPS(BirthDate, "/") > 0) Then
    LclDate2 = Left(BirthDate, 2) + Mid(BirthDate, 4, 2)
    BirthYear = Val(Mid(BirthDate, 7, 4))
End If
If (LclDate2 > LclDate1) Then
    Offset = 1
    End If

    StartMonth = Mid(BirthDate, 5, 2)
    StartYear = Val(Left(BirthDate, 4))
    StartDate = Mid(BirthDate, 7, 2)


    EndMonth = Mid(AppDate, 5, 2)
    EndYear = Val(Left(AppDate, 4))
    EndDate = Mid(AppDate, 7, 2)


    If ((EndMonth < StartMonth) Or (EndMonth = StartMonth) And (EndDate < StartDate)) Then
        GetAgePrint = (EndYear - StartYear) - 1
    Else
        GetAgePrint = (EndYear - StartYear)
    End If

    'GetAgePrint = (LclDate1 - BirthYear) - Offset
End Function

Public Sub GetAgeRange(BirthDate As String, DateRange As String)
Dim Age As Integer
Dim BirthYear As Integer
Dim CurrentYear As Integer
Dim MyDate As Date
MyDate = Date
If (Trim$(BirthDate) = "") Then
    DateRange = ""
    Exit Sub
End If
CurrentYear = Year(MyDate)
BirthYear = Val(Left(BirthDate, 4))
Age = CurrentYear - BirthYear
If (Age < 1) Then
    DateRange = "0-1"
ElseIf (Age > 0) And (Age < 10) Then
    DateRange = "1-9"
ElseIf (Age > 11) And (Age < 19) Then
    DateRange = "12-18"
ElseIf (Age > 18) And (Age < 26) Then
    DateRange = "19-25"
ElseIf (Age > 25) And (Age < 36) Then
    DateRange = "26-35"
ElseIf (Age > 35) And (Age < 51) Then
    DateRange = "36-50"
ElseIf (Age > 50) And (Age < 66) Then
    DateRange = "51-65"
ElseIf (Age > 65) Then
    DateRange = "Over 65"
Else
    DateRange = "Under 12"
End If
End Sub

Public Function IsTestFileThere(TheFile As String) As Boolean
Dim t As Integer
Dim Rec As String
IsTestFileThere = False
If (FM.IsFileThere(TheFile)) Then
    FM.OpenFile TheFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(101)
    While (Not EOF(101))
        Line Input #101, Rec
        t = t + 1
    Wend
    FM.CloseFile CLng(101)
    If (t > 2) Then
        IsTestFileThere = True
    End If
End If
End Function

Public Sub ConvertButtonDisplay(Button1 As String, Button2 As String)

If Len(Button1) > 2 Then
    If Left(UCase(Button1), 2) = "XX" Then
        Button1 = Mid(Button1, 2)
    End If
End If


Dim u As Integer
Dim CurTime As String
Dim Temp As String
Temp = ""
Button2 = ""
For u = 1 To Len(Button1)
    If (Mid(Button1, u, 1) = "^") Then
        Temp = Temp + Chr(13) + Chr(10)
    ElseIf (Mid(Button1, u, 1) = "~") Then
        Temp = Temp + " "
    Else
        Temp = Temp + Mid(Button1, u, 1)
    End If
    Temp = Replace(Temp, "  ", " ")
Next u
u = InStrPS(Temp, "/Time/")
If (u <> 0) Then
    Call FormatTimeNow(CurTime)
    CurTime = Replace(CurTime, ":", " ")
    CurTime = "/" + Trim$(CurTime) + "/"
    Temp = Replace(Temp, "/Time/", CurTime)
End If
Button2 = Temp
End Sub

Public Sub SetPadValue(Button1 As String, Button2 As String)
Dim u As Integer
Dim w As Integer
Dim Temp As String
u = InStrPS(Button1, "!")
If (u = 0) Then
    Button1 = Button1 + " " + Button2
Else
    If (Trim$(Button2) = "") Then
        Temp = ""
    ElseIf (Trim$(Button2) = ";") Then
        Temp = Left(Button1, u - 1)
        w = InStrPS(u + 1, Button1, ",")
        If (w > 0) Then
            Temp = Temp + Mid(Button1, w + 1, Len(Button1) - w)
        End If
    Else
        If (InStrPS(Button2, "; ;") > 0) Then
            Temp = ""
        Else
            Temp = Left(Button1, u - 1) + Button2 + Mid(Button1, u + 1, Len(Button1) - u)
        End If
    End If
    Button1 = Temp
End If
End Sub

Public Sub GetPadValue(Button1 As String, Button2 As String)
Dim u As Integer
Dim Temp As String
Button2 = ""
u = InStrPS(Button1, "@")
If (u > 0) Then
    Temp = Left(Button1, u - 1)
    Button2 = Mid(Button1, u + 1, Len(Button1) - u)
    Button1 = Temp
End If
End Sub

Public Function ParseLine(StartAt As Integer, LineMax As Integer, TheText As String, TheLine As String) As Integer
Dim q As Integer
Dim LocalAdjustment As Integer
Dim LocalLine As String
ParseLine = 0
TheLine = ""
LocalLine = ""
LocalAdjustment = 0
If (StartAt < 1) Or (StartAt > Len(TheText)) Or (LineMax < 1) Then
    Exit Function
End If
If (LineMax > Len(TheText)) Then
    LineMax = Len(TheText)
End If
If (StartAt + LineMax <= Len(TheText)) Then
    LocalLine = Mid(TheText, StartAt, LineMax)
Else
    LocalLine = Mid(TheText, StartAt, (Len(TheText) + 1) - StartAt)
    TheLine = Trim$(LocalLine)
    ParseLine = Len(TheText) + 1
    Exit Function
End If
If (Mid(TheText, StartAt + LineMax, 1) <> " ") Then
    For q = Len(LocalLine) To 1 Step -1
        If (Mid(LocalLine, q, 1) = " ") Then
            LocalAdjustment = q
            Exit For
        End If
    Next q
End If
If (LocalAdjustment < 1) Then
    LocalAdjustment = Len(LocalLine)
End If
TheLine = Trim$(Left(LocalLine, LocalAdjustment))
ParseLine = StartAt + LineMax - (Len(LocalLine) - LocalAdjustment)
End Function

Public Function SendViaFax(TheFile As String, ToFaxNumber As String, ToName As String) As Integer
On Error GoTo UI_ErrorHandler
Dim FaxName, ActualFilePath As String
Dim FaxServer As FaxComLib.FaxServer
Dim FaxDoc As FaxComLib.FaxDoc
SendViaFax = -1
If (ToFaxNumber = "P") Then
    SendViaFax = 0
ElseIf (Trim$(ToFaxNumber) <> "") And (Trim$(ToName) <> "") Then
    If (FM.IsFileThere(TheFile)) Then
        ActualFilePath = FM.GetPathForDirectIO(TheFile, True)
        FaxName = CheckConfigCollection("FAXNAME")
        Set FaxServer = New FaxComLib.FaxServer
        FaxServer.Connect FaxName
        Set FaxDoc = FaxServer.CreateDocument(ActualFilePath)
        FaxDoc.FaxNumber = "+1 " + Trim$(ToFaxNumber)
        FaxDoc.RecipientName = Trim$(ToName)
        FaxDoc.CoverpageName = "Office Cover Page"
        FaxDoc.SendCoverpage = 1
        FaxDoc.Send
        SendViaFax = 0
        Set FaxDoc = Nothing
        FaxServer.Disconnect
        Set FaxServer = Nothing
    Else
        SendViaFax = -3
    End If
Else
    SendViaFax = -1
End If
Exit Function
UI_ErrorHandler:
    SendViaFax = -103
    Resume LeaveFast
LeaveFast:
End Function

Public Function SendToPrinter(TheFile As String, Optional FontSize As Integer = 11) As Boolean
On Error GoTo UI_ErrorHandler
Dim TheRec As String
SendToPrinter = False
Printer.FontName = "Courier New"
Printer.FontSize = FontSize
If (Trim$(TheFile) <> "") Then
    If (InStrPS(TheFile, ".bmp") = 0) Then
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(500)
        While Not (EOF(500))
            Line Input #500, TheRec
            Printer.Print TheRec
        Wend
        FM.CloseFile CLng(500)
        Printer.EndDoc
    End If
End If
SendToPrinter = True
Exit Function
UI_ErrorHandler:
    Resume LeaveFast:
LeaveFast:
End Function

Public Function IsUpper(TheText As String) As Boolean
Dim i As Integer
IsUpper = True
If (Len(TheText) = 0) Then
    Exit Function
End If
For i = 1 To Len(TheText)
    If (Mid(TheText, i, 1) > "Z") Then
        IsUpper = False
    End If
Next i
If (InStrPS(TheText, "*") <> 0) Then
    IsUpper = True
End If
End Function

Public Function IsAlpha(TheText As String) As Boolean
Dim i As Integer
IsAlpha = True
If (Len(TheText) > 0) Then
    For i = 1 To Len(TheText)
        If (UCase$(Mid$(TheText, i, 1)) < "A") Or (UCase$(Mid$(TheText, i, 1)) > "Z") Then
            IsAlpha = False
            Exit For
        End If
    Next i
End If
End Function

Public Function LoadSelectionList(DialogueType As String, AListbox As ListBox, IHow As Boolean, Filter As String, _
Optional SortFgc As Object) As Boolean

If Not SortFgc Is Nothing Then
    SortFgc.Rows = 0
End If

Dim z1 As Integer
Dim z As Integer
Dim q As Integer, p As Integer
Dim i As Integer, k As Integer
Dim Temp As String
Dim RetrieveCodes As PracticeCodes
LoadSelectionList = False
AListbox.Clear
If (IHow) Then
    AListbox.AddItem "Close List"
End If
Set RetrieveCodes = New PracticeCodes
RetrieveCodes.ReferenceType = UCase(Trim$(DialogueType))
If (Trim$(Filter) = "") Then
    If (RetrieveCodes.FindCode > 0) Then
        i = 1
        While (RetrieveCodes.SelectCode(i))
            AListbox.AddItem RetrieveCodes.ReferenceCode
            i = i + 1
        Wend
    End If
Else
    RetrieveCodes.ReferenceAlternateCode = Filter
    If (RetrieveCodes.FindCodebyDiagRef > 0) Then
        i = 1
        While (RetrieveCodes.SelectCode(i))
            If SortFgc Is Nothing Then
                AListbox.AddItem RetrieveCodes.ReferenceCode
            Else
                GoSub AddToPool
            End If
            i = i + 1
        Wend
    End If
    z = InStrPS(Filter, ".")
    If (z > 0) Then
        p = Len(Filter) - (z - 1)
        For q = 0 To p - 1
            Temp = Left(Filter, z + q) + "*"
            RetrieveCodes.ReferenceType = UCase(Trim$(DialogueType))
            RetrieveCodes.ReferenceAlternateCode = Temp
            If (RetrieveCodes.FindCodebyDiagRef > 0) Then
                 i = 1
                While (RetrieveCodes.SelectCode(i))
                    If (Trim$(RetrieveCodes.ReferenceAlternateCode) <> "*.*") Then
                        If SortFgc Is Nothing Then
                            k = 0
                            For z1 = 0 To AListbox.ListCount - 1
                                If (Trim$(AListbox.List(z1)) = Trim$(RetrieveCodes.ReferenceCode)) Then
                                    k = z1
                                    Exit For
                                End If
                            Next z1
                            If (k = 0) Then
                                AListbox.AddItem RetrieveCodes.ReferenceCode
                            End If
                        Else
                            GoSub AddToPool
                        End If
                    End If
                    i = i + 1
                Wend
            End If
        Next q
    End If
    If Not SortFgc Is Nothing Then
        GoSub LoadFromPool
    End If
End If
Set RetrieveCodes = Nothing
Exit Function
'---------------------------
AddToPool:
SortFgc.AddItem (RetrieveCodes.Rank & vbTab & RetrieveCodes.ReferenceCode)
Return

LoadFromPool:
SortFgc.col = 0
SortFgc.ColSel = 1
SortFgc.Sort = 1
For i = 0 To SortFgc.Rows - 1
    If i > 0 Then
        If Not SortFgc.TextMatrix(i - 1, 1) = SortFgc.TextMatrix(i, 1) Then
            AListbox.AddItem (SortFgc.TextMatrix(i, 1))
        End If
    Else
        AListbox.AddItem (SortFgc.TextMatrix(i, 1))
    End If
Next
Return
'---------------------------
ErrorHandler:
    Set RetrieveCodes = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Public Function VerifyDIBackup() As Boolean
On Error GoTo UI_ErrorHandler
Dim MyDir As String
Dim OtherDirectory As String
VerifyDIBackup = True
OtherDirectory = DoctorInterfaceDirectory + "Backup\"
MyDir = Dir(Left(OtherDirectory, Len(OtherDirectory) - 1), vbDirectory)
If (MyDir = "") Then
    VerifyDIBackup = False
End If
Exit Function
UI_ErrorHandler:
    VerifyDIBackup = False
    Resume LeaveFast
LeaveFast:
End Function

Public Sub CreateDIBackup()
On Error GoTo UI_ErrorHandler
Dim MyDir As String
Dim OtherDirectory As String
    OtherDirectory = DoctorInterfaceDirectory + "Backup\"
    MyDir = Dir(Left(OtherDirectory, Len(OtherDirectory) - 1), vbDirectory)
    If (MyDir = "") Then
        FM.MkDir OtherDirectory
    End If
Exit Sub
UI_ErrorHandler:
    MsgBox "Backup Folder Error!!!"
    Resume LeaveFast
LeaveFast:
End Sub

Public Sub PushToServer(InterfaceName As String, PatId As Long, ApptId As Long, BackUpOn As Boolean)
On Error GoTo UI_ErrorHandler
Dim AOk As Boolean
Dim MyDir As String, MyFile As String
Dim TheFile As String, DestFile As String, SourceFile As String
Dim OtherFile As String, OtherDirectory As String

    If (InterfaceName = "D") Then
        OtherDirectory = DoctorInterfaceDirectory + "Backup\"
        GoSub VerifyDirectory
        If Not (AOk) Then
            MsgBox "Backup Directory not present. Backup not taken. Push occurs without backup."
            BackUpOn = False
        End If
        TheFile = DoctorInterfaceDirectory + "*" + "_" + Trim$(str$(ApptId)) + "_" + Trim$(PatId) + ".txt"
        MyFile = Dir(TheFile)
        While (MyFile <> "")
            SourceFile = DoctorInterfaceDirectory + MyFile
            DestFile = ServerDoctorInterfaceDirectory + MyFile
            OtherFile = OtherDirectory + MyFile
            
            ' FileManager performs retries, so no need to retry here
            If (BackUpOn) Then
                FM.FileCopy SourceFile, DestFile
                FM.Name SourceFile, OtherFile
            Else
                FM.Name SourceFile, DestFile
            End If
ToNextFile:
            MyFile = Dir
        Wend
    End If
Exit Sub

VerifyDirectory:
    AOk = False
    If (Not FM.DirectoryExists(OtherDirectory)) Then
        FM.MkDir OtherDirectory
        AOk = True
    Else
        AOk = True
    End If
    Return
UI_ErrorHandler:
    LogError "Library", "PushToServer", Err, Err.Description
    Resume ToNextFile
End Sub

Public Sub PullFromServer(InterfaceName As String, PatId As Long, ApptId As Long)
'On Error GoTo UI_ErrorHandler
Dim MyFile As String
Dim TheFile As String
Dim SourceFile As String
Dim DestFile As String
    If (InterfaceName = "D") Then
        TheFile = ServerDoctorInterfaceDirectory + "*" + "_" + Trim$(str$(ApptId)) + "_" + Trim$(PatId) + ".txt"
        MyFile = Dir(TheFile)
        While (MyFile <> "")
            SourceFile = ServerDoctorInterfaceDirectory + MyFile
            DestFile = DoctorInterfaceDirectory + MyFile
            FM.Name SourceFile, DestFile
            MyFile = Dir
        Wend
    End If
Exit Sub
UI_ErrorHandler:
    MsgBox "Copying Errors"
    Resume LeaveFast
LeaveFast:
End Sub

Public Function SetBreakdown(ApptPerHour As Integer) As Integer
Dim i As Integer
Dim j As Integer
SetBreakdown = Val(CheckConfigCollection("SLUG", "15"))

If (ApptPerHour > 0) Then
    i = ApptPerHour
    j = 60 / i
    If (j >= 60) Then
        SetBreakdown = 60
    ElseIf (j >= 30) And (j < 60) Then
        SetBreakdown = 30
    ElseIf (j >= 15) And (j < 30) Then
        SetBreakdown = 15
    ElseIf (j >= 10) And (j < 15) Then
        SetBreakdown = 10
    Else
        SetBreakdown = 5
    End If
End If
End Function

Public Function PrintContent(ABox As ListBox, Ref As String) As Boolean
Dim i As Integer
Dim TheFile As String
On Error GoTo UI_ErrorHandler
PrintContent = False
TheFile = ""
Call FormatTodaysDate(TheFile, False)
TheFile = Mid(TheFile, 1, 2) + Mid(TheFile, 4, 2) + Mid(TheFile, 7, 4)
TheFile = TheFile + ".txt"
FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(500)
Print #500, ""
Print #500, ""
Print #500, ""
Print #500, "Print Contents from " + Trim$(Ref)
For i = 0 To ABox.ListCount - 1
    Print #500, ABox.List(i)
Next i
FM.CloseFile CLng(500)
Call PrintDocument("Content", TheFile, "")
FM.Kill TheFile
PrintContent = True
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function ConvertToLetterName(TheText As String, NewText As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim q As Integer
Dim ITemp As String
Dim NewTemp As String
Dim CapOn As Boolean
NewText = ""
CapOn = False
j = 1
i = InStrPS(TheText, " ")
While (i > 0)
    ITemp = Mid(TheText, j, i - (j - 1))
    GoSub ConvertIt
    NewText = NewText + Trim$(NewTemp) + " "
    j = i + 1
    i = InStrPS(j, TheText, " ")
Wend
If (j < Len(TheText)) Then
    ITemp = Mid(TheText, j, Len(TheText) - (j - 1))
    GoSub ConvertIt
    NewText = NewText + NewTemp + " "
End If
NewText = Trim$(NewText)
Exit Function
ConvertIt:
    NewTemp = ""
    For q = 1 To Len(ITemp)
        If ((q = 1) Or (CapOn)) And (InStrPS(" .,:;", Mid(ITemp, q, 1)) = 0) Then
            NewTemp = NewTemp + UCase(Mid(ITemp, q, 1))
            If (CapOn) Then
                CapOn = False
            End If
        ElseIf (InStrPS(" .,:;", Mid(ITemp, q, 1)) <> 0) Then
            CapOn = True
            NewTemp = NewTemp + Mid(ITemp, q, 1)
        ElseIf (q = 4) Then
            If (UCase(Left(ITemp, 3)) = "MAC") Then
                NewTemp = NewTemp + UCase(Mid(ITemp, q, 1))
            Else
                NewTemp = NewTemp + LCase$(Mid(ITemp, q, 1))
            End If
        ElseIf (q = 3) Then
            If (UCase(Left(ITemp, 2)) = "MC") Or _
               (UCase(Left(ITemp, 2)) = "DE") Or _
               (UCase(Left(ITemp, 2)) = "DU") Or _
               (UCase(Left(ITemp, 2)) = "DI") Then
                NewTemp = NewTemp + UCase(Mid(ITemp, q, 1))
            Else
                NewTemp = NewTemp + LCase$(Mid(ITemp, q, 1))
            End If
        Else
            NewTemp = NewTemp + LCase$(Mid(ITemp, q, 1))
        End If
    Next q
    Return
End Function

Public Function KillDocumentProcessing(AFile As String, ProcessKillOn As Boolean, FileToo As Boolean) As Boolean

    On Error GoTo lKillDocumentProcessing_Error

    If ProcessKillOn Then
        If FillArrayWithProcessNames > 0 Then
            KillAppByName "Winword.exe"
            KillAppByName "Excel.exe"
        End If
    End If
    If FileToo Then
        KillFile AFile
    End If
    KillDocumentProcessing = True

    Exit Function

lKillDocumentProcessing_Error:

    LogError "Library", "KillDocumentProcessing", Err, Err.Description
End Function

Private Sub KillAppByName(AppName As String)
Dim hProcess As Long
Dim hSnapShot As Long
Dim uProcess As PROCESSENTRY
Dim MyString As String
Dim MyPos As Integer
Dim r As Long
    On Error GoTo lKillAppByName_Error

    hSnapShot = CreateToolhelpSnapshot(TH32CS_SNAPPROCESS, 0&)
    If hSnapShot = 0 Then
        Exit Sub
    Else
        uProcess.dwSize = Len(uProcess)
        r = ProcessFirst(hSnapShot, uProcess)
        Do While r
            MyPos = InStrPS(uProcess.szExeFile, Chr(0))
            MyString = Left(uProcess.szExeFile, MyPos - 1)
            If (UCase(MyString) = UCase(Trim$(AppName))) Then
                hProcess = OpenProcess(PROCESS_TERMINATE, 0, uProcess.th32ProcessID)
                If hProcess <> 0 Then
                    Call TerminateProcess(hProcess, 0)
                    Call CloseHandle(hProcess)
                End If
                Exit Do
            End If
            r = ProcessNext(hSnapShot, uProcess)
        Loop
        Call CloseHandle(hSnapShot)
    End If

    Exit Sub

lKillAppByName_Error:

    LogError "Library", "KillAppByName", Err, Err.Description
End Sub

Private Function FillArrayWithProcessNames() As Integer
Dim hSnapShot As Long
Dim uProcess As PROCESSENTRY
Dim k As Integer
Dim r As Long
FillArrayWithProcessNames = 0
k = 0
Erase ProcessArray
hSnapShot = CreateToolhelpSnapshot(TH32CS_SNAPPROCESS, 0&)
If hSnapShot = 0 Then
    Exit Function
Else
    uProcess.dwSize = Len(uProcess)
    r = ProcessFirst(hSnapShot, uProcess)
    Do While r
        k = k + 1
        ProcessArray(k) = uProcess.szExeFile
        r = ProcessNext(hSnapShot, uProcess)
    Loop
    Call CloseHandle(hSnapShot)
    FillArrayWithProcessNames = k
End If
End Function

Public Function IsDocumentProcessingOn() As Boolean
    On Error GoTo lIsDocumentProcessingOn_Error

    If FillArrayWithProcessNames > 0 Then
        IsDocumentProcessingOn = IsAppOn("Winword.exe")
        If Not IsDocumentProcessingOn Then
            IsDocumentProcessingOn = IsAppOn("Excel.exe")
        End If
    End If

    Exit Function

lIsDocumentProcessingOn_Error:

    LogError "Library", "IsDocumentProcessingOn", Err, Err.Description
End Function

Public Function IsDIRunning() As Boolean
    On Error GoTo lIsDIRunning_Error

    
    Dim IsDIRunningComWrapper As New comWrapper
    Dim arguments() As Variant
    arguments = Array(True)
    Call IsDIRunningComWrapper.InvokeStaticMethod("IO.Practiceware.Processes", "EnsureSingleInstance", arguments)

    Exit Function

lIsDIRunning_Error:

    LogError "Library", "IsDIRunning", Err, Err.Description
End Function

Private Function IsAppOnOccurrances(AppName As String) As Integer
Dim hSnapShot As Long
Dim uProcess As PROCESSENTRY
Dim MyString As String
Dim MyPos As Integer
Dim r As Long
IsAppOnOccurrances = 0
hSnapShot = CreateToolhelpSnapshot(TH32CS_SNAPPROCESS, 0&)
If hSnapShot = 0 Then
    Exit Function
Else
    uProcess.dwSize = Len(uProcess)
    r = ProcessFirst(hSnapShot, uProcess)
    Do While r
        MyPos = InStrPS(uProcess.szExeFile, Chr(0))
        MyString = Left(uProcess.szExeFile, MyPos - 1)
        If (UCase(MyString) = UCase(Trim$(AppName))) Then
            IsAppOnOccurrances = IsAppOnOccurrances + 1
        End If
        r = ProcessNext(hSnapShot, uProcess)
    Loop
    Call CloseHandle(hSnapShot)
End If
End Function

Private Function IsAppOn(AppName As String) As Boolean
Dim hSnapShot As Long
Dim uProcess As PROCESSENTRY
Dim MyString As String
Dim MyPos As Integer
Dim r As Long
    On Error GoTo lIsAppOn_Error

    hSnapShot = CreateToolhelpSnapshot(TH32CS_SNAPPROCESS, 0&)
    If hSnapShot = 0 Then
        Exit Function
    Else
        uProcess.dwSize = Len(uProcess)
        r = ProcessFirst(hSnapShot, uProcess)
        Do While r
            MyPos = InStrPS(uProcess.szExeFile, Chr(0))
            MyString = Left(uProcess.szExeFile, MyPos - 1)
            If (UCase(MyString) = UCase(Trim$(AppName))) Then
                IsAppOn = True
                Exit Do
            End If
            r = ProcessNext(hSnapShot, uProcess)
        Loop
        Call CloseHandle(hSnapShot)
    End If

    Exit Function

lIsAppOn_Error:

    LogError "Library", "IsAppOn", Err, Err.Description
End Function

Public Function ConvertOldZeros_6(ATemp As String) As Boolean

'ConvertOldZeros ATemp
'Exit Function

If (IsNumeric(ATemp)) Then
    ATemp = ATemp & "~"
End If
End Function
Public Function ConvertOldZeros(ATemp As String) As Boolean
ATemp = Trim$(ATemp)
If (IsNumeric(ATemp)) Then
    If (InStrPS(ATemp, "0") > 0) Then
        Call ReplaceCharacters(ATemp, "0", "O")
    ElseIf (InStrPS(ATemp, "1") > 0) Then
        Call ReplaceCharacters(ATemp, "1", "l")
    ElseIf (Len(ATemp) > 9) Then
'        ATemp = ATemp + " " + Chr(2)
    End If
    If (Left(ATemp, 1) = "+") Then
        ATemp = "^" + ATemp
    ElseIf (Left(ATemp, 1) = "-") Then
        ATemp = "^" + ATemp
    End If
End If
End Function

Public Function TriggerINet(ByVal sSiteName As String) As Boolean
    On Error GoTo lTriggerINet_Error

    If Trim$(sSiteName) <> "" Then
        If Left$(sSiteName, 4) <> "http" Then
            sSiteName = "http://" & sSiteName
        End If
        TriggerINet = FileOpenDefaultApp(Trim$(Left$(sSiteName, 128)))
    End If

    Exit Function

lTriggerINet_Error:

    LogError "Library", "TriggerINet", Err, Err.Description
End Function

Public Function TriggerExplorer(Temp As String) As Boolean
    On Error GoTo UI_ErrorHandler
    TriggerExplorer = False
    If (Trim$(Temp) <> "") Then
        TriggerExplorer = True
        Temp = Trim$(Temp)
        Shell "explorer /select, " + Temp, vbNormalFocus
    End If
    Exit Function
UI_ErrorHandler:
    Resume Next
End Function

Public Function TriggerPDF(sPdfFile As String) As Boolean
    On Error GoTo lTriggerPDF_Error

    If FM.IsFileThere(sPdfFile) Then
        Dim arguments() As Variant
        arguments = Array(sPdfFile)
        Dim DocumentComWrapper As New comWrapper
        Call DocumentComWrapper.Create(DocumentViewManagerType, emptyArgs)
        DocumentComWrapper.InvokeMethod "ViewDocument", arguments
    End If
    Exit Function

lTriggerPDF_Error:

    LogError "Library", "TriggerPDF", Err, Err.Description
End Function

Public Function FileOpenDefaultApp(sFile As String) As Boolean
Dim iResult As Long
    iResult = ShellExecute(0, "", sFile, "", "", 10)
    If iResult > 32 Then FileOpenDefaultApp = True

End Function

Public Function TriggerWord(ResultFile As String) As Boolean
On Error GoTo UI_ErrorHandler

    If FM.IsFileThere(ResultFile) Then
        Dim arguments() As Variant
        arguments = Array(ResultFile)
        Dim DocumentComWrapper As New comWrapper
        Call DocumentComWrapper.Create(DocumentViewManagerType, emptyArgs)
        DocumentComWrapper.InvokeMethod "ViewDocument", arguments
    End If
    Exit Function
UI_ErrorHandler: LogError "Library", "TriggerWord", Err, Err.Description

End Function

Public Function PrintDocument(TemplateName As String, ResultFile As String, PrtName As String) As Boolean
On Error GoTo UI_ErrorHandler
    If FM.IsFileThere(ResultFile) Then
        Dim arguments() As Variant
        arguments = Array(TemplateName, ResultFile)
        Dim DocumentComWrapper As New comWrapper
        Call DocumentComWrapper.Create(DocumentViewManagerType, emptyArgs)
        DocumentComWrapper.InvokeMethod "PrintDocument", arguments
        PrintDocument = True
    End If
        
Exit Function
UI_ErrorHandler: LogError "Library", "PrintDocument", Err, Err.Description
End Function

Public Function PrintTemplate(ResultFile As String, TemplateName As String, DoPrint As Boolean, AnyReplacement As Boolean, TCopy As Integer, PrtName As String, PrintSignature As String, HowMany As Integer, AmountReplacement As Boolean) As Boolean
    On Error Resume Next
Dim k As Integer
Dim LocalFile As String
Dim resultoutputfile As String
Dim resultfile1 As String
Dim strsplit() As String
Dim l As Integer
Dim LocalFiletemp As String

l = InStrRev(ResultFile, "@")

strsplit() = Split(ResultFile, "@")
If l = 0 Then
    LocalFiletemp = strsplit(UBound(strsplit))
Else
    LocalFiletemp = Mid(ResultFile, l + 1, Len(ResultFile))
End If

For k = 0 To UBound(strsplit) - 1
    If Not LocalFiletemp = strsplit(k) Then
        If k = 0 Then
            resultfile1 = strsplit(k)
        Else
            resultfile1 = resultfile1 & "@" & strsplit(k)
        End If
    End If
Next

LocalFile = ""

Dim LegacyDocuments As Boolean
LegacyDocuments = Not (CheckConfigCollection("LEGACYDOCUMENTS") = "F")

If (TCopy < 1) Then
    TCopy = 1
End If

    k = InStrPS(LocalFiletemp, ".")
    If (k > 0) Then
        LocalFile = Left(LocalFiletemp, k - 1) + ".doc"
    End If
    
        If (FM.IsFileThere(LocalFile)) Then
            FM.Kill LocalFile
        End If
        
        If Dir(TemplateName) = "" Then
            frmEventMsgs.InfoMessage "Template is in Use or Doesn't exist"
            frmEventMsgs.Show 1
            PrintTemplate = False
            Exit Function
        End If
        
        Dim emptyArguments() As Variant
        Dim arguments() As Variant
        arguments = Array(TemplateName, resultfile1, LocalFile, PrintSignature, AnyReplacement, AmountReplacement)
        
        Dim DocumentComWrapper As New comWrapper
        DocumentComWrapper.Create DocumentGenerationServiceType, emptyArguments
        DocumentComWrapper.InvokeMethod "PerformMailMerge", arguments
        
        If Err.Number <> 0 Then
            LogError "Library", "PrintTemplate", Err, Err.Description
            Err.Clear
            PrintTemplate = False
            Exit Function
        End If
        
        If DoPrint Then
            arguments = Array(TemplateName, LocalFile)
            DocumentComWrapper.Create DocumentViewManagerType, emptyArguments
            DocumentComWrapper.InvokeMethod "PrintDocument", arguments
        End If

        If Err.Number <> 0 Then
            LogError "Library", "PrintTemplate", Err, Err.Description
            Err.Clear
            PrintTemplate = False
        ElseIf (FM.IsFileThere(LocalFile)) Then
           PrintTemplate = True
        End If
     
End Function

Private Function mTextBox(inp As Object) As Boolean
Dim tst As String
On Error GoTo exitfunction
tst = inp.TextFrame.TextRange.Text
mTextBox = True
Exit Function

exitfunction:
mTextBox = False
End Function


Public Function LoadAsposeTemplates() As String
Dim PracticeCode As New PracticeCodes
Dim iIndex As Integer
Dim sTemplateList As String

    On Error GoTo lLoadAsposeTemplates_Error
    PracticeCode.ReferenceType = "ASPOSEEXCLUSIONS"
    If PracticeCode.FindCode > 0 Then
        iIndex = 1
        While PracticeCode.SelectCode(iIndex)
            sTemplateList = sTemplateList & "[" & PracticeCode.ReferenceCode & "]"
            iIndex = iIndex + 1
        Wend
        LoadAsposeTemplates = sTemplateList
    End If

    Exit Function

lLoadAsposeTemplates_Error:

    LogError "Library", "LoadAsposeTemplates", Err, Err.Description
End Function

Public Function GetFileNameFromFullPath(ByVal sFullPath As String) As String
    GetFileNameFromFullPath = FM.GetFileName(sFullPath)
End Function
'=======
'    App.Quit
'    Set App = Nothing
'    For r = 0 To 250
'        DoEvents
'    Next r
'    PrintTemplate = True
'    If Not DoPrint Then
'        KillFile LocalFile
'        MoveFile LocalFile1, LocalFile
'    End If
'End If
'If DoPrint Then
'    KillFile LocalFile
'End If
'KillFile ResultFile
'For r = 0 To 250
'    DoEvents
'Next r
'Exit Function
'UI_ErrorHandler:
'    If Err.Number = 1120 Then
'        LogError "Library", "PrintTemplate", Err, Err.Description, PrtName
'    Else
'        LogError "Library", "PrintTemplate", Err, Err.Description, LocalFile
'    End If
'    On Error Resume Next
'    If (i <> 0) Then
'        Call App.Documents.Close(SaveChanges:=False)
'        App.Quit
'    End If
'    Set App = Nothing
'    For r = 0 To 250
'        DoEvents
'    Next r
'
'End Function
''
''Private Function mTextBox(inp As Object) As Boolean
''Dim tst As String
''On Error GoTo exitfunction
''tst = inp.TextFrame.TextRange.Text
''mTextBox = True
''Exit Function
''
''exitfunction:
''mTextBox = False
''End Function
'
'
'Public Function LoadAsposeTemplates() As String
'Dim PracticeCode As New PracticeCodes
'Dim iIndex As Integer
'Dim sTemplateList As String
'
'    On Error GoTo lLoadAsposeTemplates_Error
'    PracticeCode.ReferenceType = "ASPOSEEXCLUSIONS"
'    If PracticeCode.FindCode > 0 Then
'        iIndex = 1
'        While PracticeCode.SelectCode(iIndex)
'            sTemplateList = sTemplateList & "[" & PracticeCode.ReferenceCode & "]"
'            iIndex = iIndex + 1
'        Wend
'        LoadAsposeTemplates = sTemplateList
'    End If
'
'    Exit Function
'
'lLoadAsposeTemplates_Error:
'
'    LogError "Library", "LoadAsposeTemplates", Err, Err.Description
'End Function
'
'
Public Function SetTemplateLanguage(Language As String, TheFile As String) As Boolean
Dim Temp As String
SetTemplateLanguage = True
If (Trim$(Language) <> "") Then
    If (Trim$(Language) <> "ENGLISH") Then
        Temp = TheFile
        Call ReplaceCharacters(Temp, ".", "-" + Trim$(Language) + ".")
        If (FM.IsFileThere(Temp)) Then
            TheFile = Temp
        End If
    End If
End If
End Function
'
'Public Function ConvertNumberToWord(TheText As String, NewText As String) As Boolean
'Dim k As Integer
'Dim AWordOnes(20) As String
'Dim AWordTens(10) As String
'ConvertNumberToWord = False
'NewText = ""
'TheText = Trim$(TheText)
'If (Len(TheText) < 5) Then
'    ConvertNumberToWord = True
'    AWordOnes(0) = "Zero"
'    AWordOnes(1) = "One"
'    AWordOnes(2) = "Two"
'    AWordOnes(3) = "Three"
'    AWordOnes(4) = "Four"
'    AWordOnes(5) = "Five"
'    AWordOnes(6) = "Six"
'    AWordOnes(7) = "Seven"
'    AWordOnes(8) = "Eight"
'    AWordOnes(9) = "Nine"
'    AWordOnes(10) = "Ten"
'    AWordOnes(11) = "Eleven"
'    AWordOnes(12) = "Twelve"
'    AWordOnes(13) = "Thirteen"
'    AWordOnes(14) = "Fourteen"
'    AWordOnes(15) = "Fifteen"
'    AWordOnes(16) = "Sixteen"
'    AWordOnes(17) = "Seventeen"
'    AWordOnes(18) = "Eighteen"
'    AWordOnes(19) = "Ninteen"
'    AWordTens(0) = ""
'    AWordTens(1) = ""
'    AWordTens(2) = "Twenty"
'    AWordTens(3) = "Thirty"
'    AWordTens(4) = "Forty"
'    AWordTens(5) = "Fifty"
'    AWordTens(6) = "Sixty"
'    AWordTens(7) = "Seventy"
'    AWordTens(8) = "Eighty"
'    AWordTens(9) = "Ninty"
'    If (Len(TheText) = 1) Then
'        k = Val(TheText)
'        NewText = AWordOnes(k)
'    ElseIf (Len(TheText) = 2) Then
'        If (Val(TheText) < 20) Then
'            NewText = AWordOnes(Val(TheText))
'        Else
'            k = Val(Mid(TheText, 1, 1)) - 10
'            NewText = AWordTens(k)
'            k = Val(Mid(TheText, 2, 1)) - 10
'            If (k > 0) Then
'                NewText = NewText + "-" + AWordOnes(k)
'            End If
'        End If
'    ElseIf (Len(TheText) = 3) Then
'        k = Val(Mid(TheText, 1, 1)) - 10
'        NewText = AWordOnes(k) + " Hundred"
'        k = Val(Mid(TheText, 2, 1))
'        If (k < 20) Then
'            NewText = NewText + "-" + AWordOnes(k)
'        Else
'            NewText = NewText + "-" + AWordTens(k - 10)
'            k = Val(Mid(TheText, 3, 1)) - 10
'            If (k > 0) Then
'                NewText = NewText + "-" + AWordOnes(k)
'            End If
'        End If
'    ElseIf (Len(TheText) = 4) Then
'        k = Val(Mid(TheText, 1, 1)) - 10
'        NewText = AWordOnes(k) + " Thousand"
'        k = Val(Mid(TheText, 2, 1))
'        If (k > 0) Then
'            NewText = NewText + "-" + AWordOnes(k) + " Hundred"
'        Else
'            k = Val(Mid(TheText, 3, 1))
'            If (k < 20) Then
'                NewText = NewText + "-" + AWordOnes(k)
'            Else
'                NewText = NewText + "-" + AWordTens(k - 10)
'                k = Val(Mid(TheText, 4, 1)) - 10
'                If (k > 0) Then
'                    NewText = NewText + "-" + AWordOnes(k)
'                End If
'            End If
'        End If
'    End If
'End If
'End Function

Public Function ConvertUnitsToWord(TheText As String, NewText As String) As Boolean
NewText = ""
TheText = UCase(Trim$(TheText))
NewText = TheText
If (TheText = "IN") Then
    NewText = "inches "
ElseIf (TheText = "MM") Then
    NewText = "mm "
ElseIf (TheText = "CM") Then
    NewText = "cm "
ElseIf (TheText = "TR") Then
    NewText = "trace "
ElseIf (TheText = "DE") Then
    NewText = "dense "
ElseIf (TheText = "SE") Then
    NewText = "severe "
ElseIf (TheText = "RA") Then
    NewText = "rare "
ElseIf (TheText = "SB") Then
    NewText = "stable "
ElseIf (TheText = "ST") Then
    NewText = "stage "
ElseIf (TheText = "MI") Then
    NewText = "mild "
ElseIf (TheText = "MO") Then
    NewText = "moderate "
ElseIf (TheText = "LI") Then
    NewText = "light "
ElseIf (TheText = "DI") Then
    NewText = "diffuse "
ElseIf (TheText = "CE") Then
    NewText = "central "
ElseIf (TheText = "RS") Then
    NewText = "raised "
ElseIf (TheText = "FL") Then
    NewText = "flat "
ElseIf (TheText = "QUE") Then
    NewText = "questionable "
ElseIf (TheText = "%") Then
    NewText = "% "
ElseIf (TheText = "DA") Then
    NewText = "disc area "
ElseIf (TheText = "DD") Then
    NewText = "disc diameter "
ElseIf (TheText = "LU") Then
    NewText = "left upper "
ElseIf (TheText = "LL") Then
    NewText = "left lower "
ElseIf (TheText = "RU") Then
    NewText = "right upper "
ElseIf (TheText = "RL") Then
    NewText = "right lower "
End If
End Function

Public Function IsAlertPresent(PatId As Long, AlertId As Integer) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
IsAlertPresent = False
If (PatId > 0) And (AlertId >= 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatId
    If (RetNote.FindNotesbyPatientAlert > 0) Then
        i = 1
        Do Until Not (RetNote.SelectNotes(i))
            If (AlertId = 0) Then
                If (InStrPS(17, RetNote.NotesAlertMask, "1") > 0) Then
                    IsAlertPresent = True
                    Exit Do
                End If
            ElseIf (Mid(RetNote.NotesAlertMask, AlertId, 1) <> "0") Then
                IsAlertPresent = True
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetNote = Nothing
End If
End Function

Public Function ConvertInteger(IInt As Long, ReverseIt As Boolean) As String
Dim ATemp As String
ConvertInteger = ""
If (ReverseIt) Then
    ATemp = Trim$(str$(IInt))
    ATemp = String$(10 - Len(ATemp), "0") + Trim$(ATemp)
Else
    ATemp = Trim$(str$(-1 * IInt))
    ATemp = String$(10 - Len(ATemp), Chr(255)) + Trim$(ATemp)
End If
ConvertInteger = ATemp
End Function

Public Function GetPrinter(PrtId As Integer) As String
Dim i As Integer
Dim Temp As String
Dim UTemp As String
Dim ATemp As String
GetPrinter = ""
Temp = ""
ATemp = ""
If (PrtId = 1) Then
    UTemp = "PRINTER1"
ElseIf (PrtId = 2) Then
    UTemp = "PRINTER2"
ElseIf (PrtId = 3) Then
    UTemp = "PRINTER3"
ElseIf (PrtId = 4) Then
    UTemp = "PRINTER4"
ElseIf (PrtId = 5) Then
    UTemp = "PRINTER5"
ElseIf (PrtId = 6) Then
    UTemp = "PRINTER6"
Else
    UTemp = "PRINTER1"
End If
Temp = CheckConfigCollection(UTemp)
If (Trim$(Temp) <> "") Then
    ATemp = Trim$(Temp)
    If (Trim$(dbTerminal) <> "") Then
        i = InStrPS(ATemp, "\\" + dbTerminal)
        If (i > 0) Then
            ATemp = Mid(ATemp, 4 + Len(dbTerminal), Len(ATemp) - (3 + Len(dbTerminal)))
        End If
    End If
End If
GetPrinter = ATemp
End Function

Public Function ResetPrinter() As Boolean
On Error Resume Next
ResetPrinter = True
Printer.Orientation = dfPrinterOrientation
'Printer.DriverName = dfPrinterDriver
'Printer.DeviceName = dfPrinter
End Function

Public Function VerifyDrawFile(MyFile As String, DrawTag As String) As String
Dim OtherTag As String
Dim ATemp As String
Dim NewFile As String
VerifyDrawFile = ""
If (Trim$(MyFile) <> "") And (Trim$(DrawTag) <> "") Then
    OtherTag = ""
    If (DrawTag = "I-02") Then
        OtherTag = "I-08"
    ElseIf (DrawTag = "I-08") Then
        OtherTag = "I-02"
    End If
    ATemp = Dir(MyFile)
    If (ATemp = "") And (Trim$(OtherTag) <> "") Then
        NewFile = MyFile
        Call ReplaceCharacters(NewFile, DrawTag, OtherTag)
        ATemp = Dir(NewFile)
        If (ATemp = "") Then
            NewFile = ""
        End If
    Else
        NewFile = MyFile
    End If
    VerifyDrawFile = NewFile
End If
End Function

Public Function GetCityStatebyZip(AZip As String, ACity As String, AState As String) As Boolean
Dim rsZip As ZipCodes
GetCityStatebyZip = False
ACity = ""
AState = ""
If (Trim$(AZip) <> "") Then
    Set rsZip = New ZipCodes
    rsZip.ZipCode = AZip
    If (rsZip.FindZip > 0) Then
        If (rsZip.SelectZip(1)) Then
            ACity = rsZip.ZipCity
            AState = rsZip.ZipState
            GetCityStatebyZip = True
        End If
    End If
    Set rsZip = Nothing
End If
End Function

Public Function GetZipList(AList As ListBox, ACity As String, AState As String) As Boolean
Dim i As Integer
Dim rsZip As ZipCodes
AList.Clear
If (Trim$(ACity) <> "") Then
    Set rsZip = New ZipCodes
    rsZip.ZipCity = ACity
    If (Trim$(AState) <> "") Then
        rsZip.ZipState = AState
    End If
    If (rsZip.FindZipbyCity > 0) Then
        i = 1
        While (rsZip.SelectZip(i))
            AList.AddItem Trim$(rsZip.ZipCode)
            i = i + 1
        Wend
    End If
    Set rsZip = Nothing
End If
End Function

' Drag a control until the user releases all mouse buttons
'
' You should call this routine from the MouseDown event procedures
' of the controls that you want to make draggable, after
' you determine that the user has initiated a drag operation.
' For example, if you want to let the user drag controls
' using the Ctrl+Right button combination, add this code
' to their MouseDown procedure:
'
' Private Sub Command1_MouseDown(...)
'    If Button = vbRightButton And Shift = vbCtrlMask Then
'        DragControl Command1
'    End If
' End Sub
'
' From that point on, this procedure takes the control and
' exits only when the user releases all mouse buttons

Public Sub DragControl(ctrl As Control)
    Dim startButton As Integer
    Dim startPoint As POINTAPI
    Dim currPoint As POINTAPI
    Dim contRect As RECT
    Dim contScaleMode As Integer

    ' get mouse position and buttons pressed
    GetCursorPos startPoint
    If GetAsyncKeyState(vbLeftButton) Then startButton = vbLeftButton
    If GetAsyncKeyState(vbRightButton) Then startButton = startButton Or _
        vbRightButton
    If GetAsyncKeyState(vbMiddleButton) Then startButton = startButton Or _
        vbMiddleButton

    ' get container upper-left corner position
    ' in screen coordinates (currPoint is Zero)
    ClientToScreen ctrl.Container.hWnd, currPoint
    ' get container size
    GetClientRect ctrl.Container.hWnd, contRect
    ' convert to screen coordintes
    contRect.Left = currPoint.X
    contRect.Top = currPoint.Y
    contRect.Right = contRect.Right + currPoint.X
    contRect.Bottom = contRect.Bottom + currPoint.Y
    ' limit the cursor within the parent control
    ClipCursor contRect

    ' get the ScaleMode that is active for the control
    ' this is the ScaleMode of its container, or it
    ' is vbTwips if its container does not support
    ' the ScaleMode property
    On Error Resume Next
    contScaleMode = vbTwips
    ' ignore next assignement if the container
    ' dows not support ScaleMode property
    contScaleMode = ctrl.Container.ScaleMode

    Do
        ' exit if all mouse buttons are released
        If (startButton And vbLeftButton) = 0 Or GetAsyncKeyState(vbLeftButton) _
            = 0 Then
            If (startButton And vbRightButton) = 0 Or GetAsyncKeyState _
                (vbRightButton) = 0 Then
                If (startButton And vbMiddleButton) = 0 Or GetAsyncKeyState _
                    (vbMiddleButton) = 0 Then
                    Exit Do
                End If
            End If
        End If

        ' get current mouse position
        GetCursorPos currPoint

        ' move the control if they are different
        If currPoint.X <> startPoint.X Or currPoint.Y <> startPoint.Y Then
            ' move the control
            With ctrl.Parent
                ctrl.Move ctrl.Left + .ScaleX(currPoint.X - startPoint.X, _
                    vbPixels, contScaleMode), ctrl.Top + .ScaleY(currPoint.Y - _
                    startPoint.Y, vbPixels, contScaleMode)
                ' refresh container
                InvalidateRectByNum .hWnd, 0, False
                .Refresh
            End With
            LSet startPoint = currPoint
        End If

        ' allow background processing
        DoEvents
    Loop

    ' restore full mouse movement
    ClipCursorByNum 0
End Sub

Public Function StringToEyeRef(sEye As String) As EEyeRef
    Select Case UCase(Trim(sEye))
        Case "OD"
            StringToEyeRef = eerOD
        Case "OS"
            StringToEyeRef = eerOS
        Case "OU"
            StringToEyeRef = eerOU
        Case Else
            StringToEyeRef = eerNone
    End Select
End Function


