VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeFeeSchedules"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Practice Fee Schedule Services Object Module
Option Explicit
Public SchServiceId As Long
Public SchService As String
Public SchDoctorId As Long
Public SchPlanId As Long
Public SchLocId As Long
Public SchFee As Single
Public SchFacFee As Single
Public SchStartDate As String
Public SchEndDate As String
Public SchAdjustmentRate As Single
Public SchAdjustmentAllowed As Single

Private ModuleName As String
Private FeeScheduleTotal As Long
Private FeeScheduleTbl As ADODB.Recordset
Private FeeScheduleNew As Boolean

Private Sub Class_Initialize()
Set FeeScheduleTbl = CreateAdoRecordset
Call InitService
End Sub

Private Sub Class_Terminate()
Call InitService
Set FeeScheduleTbl = Nothing
End Sub

Private Sub InitService()
FeeScheduleNew = True
SchServiceId = 0
SchDoctorId = 0
SchPlanId = 0
SchLocId = 0
SchFee = 0
SchFacFee = 0
SchStartDate = ""
SchEndDate = ""
SchAdjustmentRate = 0
SchAdjustmentAllowed = 0
End Sub

Private Sub LoadService()
FeeScheduleNew = False
SchServiceId = FeeScheduleTbl("ServiceId")
If (FeeScheduleTbl("CPT") <> "") Then
    SchService = FeeScheduleTbl("CPT")
Else
    SchService = ""
End If
If (FeeScheduleTbl("DoctorId") <> "") Then
    SchDoctorId = FeeScheduleTbl("DoctorId")
Else
    SchDoctorId = 0
End If
If (FeeScheduleTbl("PlanId") <> "") Then
    SchPlanId = FeeScheduleTbl("PlanId")
Else
    SchPlanId = 0
End If
If (FeeScheduleTbl("LocId") <> "") Then
    SchLocId = FeeScheduleTbl("LocId")
Else
    SchLocId = 0
End If
If (FeeScheduleTbl("Fee") <> "") Then
    SchFee = Round(FeeScheduleTbl("Fee"), 2)
Else
    SchFee = 0
End If
If (FeeScheduleTbl("FacilityFee") <> "") Then
    SchFacFee = Round(FeeScheduleTbl("FacilityFee"), 2)
Else
    SchFacFee = 0
End If
If (FeeScheduleTbl("StartDate") <> "") Then
    SchStartDate = FeeScheduleTbl("StartDate")
Else
    SchStartDate = ""
End If
If (FeeScheduleTbl("EndDate") <> "") Then
    SchEndDate = FeeScheduleTbl("EndDate")
Else
    SchEndDate = ""
End If
If (FeeScheduleTbl("AdjustmentRate") <> "") Then
    SchAdjustmentRate = FeeScheduleTbl("AdjustmentRate")
Else
    SchAdjustmentRate = 0
End If
If (FeeScheduleTbl("AdjustmentAllowed") <> "") Then
    SchAdjustmentAllowed = Round(FeeScheduleTbl("AdjustmentAllowed"), 2)
Else
    SchAdjustmentAllowed = 0
End If
End Sub

Private Function GetServices(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
FeeScheduleTotal = 0
GetServices = -1
If (Trim(SchService) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeFeeSchedules WHERE CPT >= '" + UCase(Trim(SchService)) + "' "
If (IType = 1) And (Len(SchService) <> 1) Then
    OrderString = "Select * FROM PracticeFeeSchedules WHERE CPT = '" + UCase(Trim(SchService)) + "' "
End If
If (SchDoctorId > 0) Or (SchDoctorId = -1) Then
    OrderString = OrderString + "And DoctorId = " + Trim(Str(SchDoctorId)) + " "
End If
If (SchPlanId > 0) Or (SchPlanId = -1) Then
    OrderString = OrderString + "And PlanId = " + Trim(Str(SchPlanId)) + " "
End If
If (SchLocId > 0) Or (SchLocId = -1) Then
    OrderString = OrderString + "And LocId = " + Trim(Str(SchLocId)) + " "
End If
If (Trim(SchStartDate) <> "") Then
    OrderString = OrderString + "And StartDate <= '" + Trim(SchStartDate) + "' "
End If
If (Trim(SchEndDate) <> "") Then
'    OrderString = OrderString + "And EndDate >= '" + Trim(SchEndDate) + "' "
End If
OrderString = OrderString + "ORDER BY CPT ASC, StartDate DESC "
Set FeeScheduleTbl = Nothing
Set FeeScheduleTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call FeeScheduleTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (FeeScheduleTbl.EOF) Then
    GetServices = -1
    Call InitService
Else
    FeeScheduleTbl.MoveLast
    FeeScheduleTotal = FeeScheduleTbl.RecordCount
    GetServices = FeeScheduleTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetService"
    GetServices = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetServicesbyInsurer() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
FeeScheduleTotal = 0
GetServicesbyInsurer = -1
If (Trim(SchPlanId) = 0) Then
    Exit Function
End If
OrderString = "Select * FROM PracticeFeeSchedules WHERE PlanId =" + Str(SchPlanId) + " "
OrderString = OrderString + "ORDER BY CPT ASC, StartDate DESC "
Set FeeScheduleTbl = Nothing
Set FeeScheduleTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call FeeScheduleTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (FeeScheduleTbl.EOF) Then
    GetServicesbyInsurer = -1
    Call InitService
Else
    FeeScheduleTbl.MoveLast
    FeeScheduleTotal = FeeScheduleTbl.RecordCount
    GetServicesbyInsurer = FeeScheduleTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetServicebyInsurer"
    GetServicesbyInsurer = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetService() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetService = True
OrderString = "SELECT * FROM PracticeFeeSchedules WHERE ServiceId = " + Trim(Str(SchServiceId)) + " ORDER BY CPT ASC "
Set FeeScheduleTbl = Nothing
Set FeeScheduleTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call FeeScheduleTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (FeeScheduleTbl.EOF) Then
    Call InitService
Else
    Call LoadService
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetService"
    GetService = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutService() As Boolean
On Error GoTo DbErrorHandler
PutService = True
If (FeeScheduleNew) Then
    FeeScheduleTbl.AddNew
End If
FeeScheduleTbl("CPT") = UCase(Trim(SchService))
FeeScheduleTbl("DoctorId") = SchDoctorId
FeeScheduleTbl("PlanId") = SchPlanId
FeeScheduleTbl("LocId") = SchLocId
FeeScheduleTbl("Fee") = Round(SchFee, 2)
FeeScheduleTbl("FacilityFee") = Round(SchFacFee, 2)
FeeScheduleTbl("StartDate") = SchStartDate
FeeScheduleTbl("EndDate") = SchEndDate
FeeScheduleTbl("AdjustmentRate") = SchAdjustmentRate
FeeScheduleTbl("AdjustmentAllowed") = Round(SchAdjustmentAllowed, 2)
FeeScheduleTbl.Update
If (FeeScheduleNew) Then
    FeeScheduleNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutService"
    PutService = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeService() As Boolean
On Error GoTo DbErrorHandler
PurgeService = True
FeeScheduleTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeService"
    PurgeService = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyService() As Boolean
ApplyService = PutService
End Function

Public Function DeleteService() As Boolean
DeleteService = PurgeService
End Function

Public Function RetrieveService() As Boolean
RetrieveService = GetService
End Function

Public Function FindService() As Long
FindService = GetServices(0)
End Function

Public Function FindServicebyInsurer() As Long
FindServicebyInsurer = GetServicesbyInsurer
End Function

Public Function FindServiceDirect() As Long
FindServiceDirect = GetServices(1)
End Function

Public Function SelectService(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectService = False
If (FeeScheduleTbl.EOF) Or (ListItem < 1) Or (ListItem > FeeScheduleTotal) Then
    Exit Function
End If
FeeScheduleTbl.MoveFirst
FeeScheduleTbl.Move ListItem - 1
SelectService = True
Call LoadService
Exit Function
DbErrorHandler:
    ModuleName = "SelectService"
    SelectService = False
    SchService = "-"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectServiceAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectServiceAggregate = False
If (FeeScheduleTbl.EOF) Or (ListItem < 1) Or (ListItem > FeeScheduleTotal) Then
    Exit Function
End If
FeeScheduleTbl.MoveFirst
FeeScheduleTbl.Move ListItem - 1
SelectServiceAggregate = True
Call LoadService
Exit Function
DbErrorHandler:
    ModuleName = "SelectServiceAggregate"
    SelectServiceAggregate = False
    SchService = "-"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

