VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Audit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public AuditRecId As Long
Public AuditUser As Long
Public AuditType As String
Public AuditTable As String
Public AuditRecord As Long
Public AuditPrevious As String
Public AuditCurrent As String
Public AuditDate As String
Public AuditTime As Integer
Private PrcAudit As PracticeAudit

Private Sub Class_Initialize()
AuditRecId = 0
AuditUser = 0
AuditType = ""
AuditTable = ""
AuditRecord = 0
AuditPrevious = ""
AuditCurrent = ""
Set PrcAudit = Nothing
End Sub

Private Sub Class_Terminate()
AuditRecId = 0
AuditUser = 0
AuditType = ""
AuditTable = ""
AuditRecord = 0
AuditPrevious = ""
AuditCurrent = ""
AuditDate = ""
Set PrcAudit = Nothing
End Sub

Private Function TableReference(TableName As String) As Integer
TableReference = 0
If (TableName = "Appointments") Then
    TableReference = 1
ElseIf (TableName = "AppointmentSlots") Then
    TableReference = 2
ElseIf (TableName = "AppointmentType") Then
    TableReference = 3
ElseIf (TableName = "PatientClinical") Then
    TableReference = 4
ElseIf (TableName = "PatientDemographics") Then
    TableReference = 5
ElseIf (TableName = "PatientFinancial") Then
    TableReference = 6
ElseIf (TableName = "PatientNotes") Then
    TableReference = 7
ElseIf (TableName = "PatientPreCerts") Then
    TableReference = 8
ElseIf (TableName = "PatientReceivablePayments") Then
    TableReference = 9
ElseIf (TableName = "PatientReceivables") Then
    TableReference = 10
ElseIf (TableName = "PatientReceivableServices") Then
    TableReference = 11
ElseIf (TableName = "PatientReferral") Then
    TableReference = 12
ElseIf (TableName = "PracticeActivity") Then
    TableReference = 13
ElseIf (TableName = "PracticeAffiliations") Then
    TableReference = 14
ElseIf (TableName = "PracticeCalendar") Then
    TableReference = 15
ElseIf (TableName = "PracticeCodeTable") Then
    TableReference = 16
ElseIf (TableName = "PracticeFavorites") Then
    TableReference = 17
ElseIf (TableName = "PracticeFeeSchedules") Then
    TableReference = 18
ElseIf (TableName = "PracticeInsurers") Then
    TableReference = 19
ElseIf (TableName = "PracticeInterfaceConfiguration") Then
    TableReference = 20
ElseIf (TableName = "PracticeMessages") Then
    TableReference = 21
ElseIf (TableName = "PracticeName") Then
    TableReference = 22
ElseIf (TableName = "PracticePayables") Then
    TableReference = 23
ElseIf (TableName = "PracticeReports") Then
    TableReference = 24
ElseIf (TableName = "PracticeServices") Then
    TableReference = 25
ElseIf (TableName = "PracticeTransactionJournal") Then
    TableReference = 26
ElseIf (TableName = "PracticeVendors") Then
    TableReference = 27
ElseIf (TableName = "Resources") Then
    TableReference = 28
ElseIf (TableName = "Audit") Then
    TableReference = 29
End If
End Function

Private Function TableName(TableRef As Integer) As String
TableName = ""
If (TableRef = 1) Then
    TableName = "Appointments"
ElseIf (TableRef = 2) Then
    TableName = "AppointmentSlots"
ElseIf (TableRef = 3) Then
    TableName = "AppointmentType"
ElseIf (TableRef = 4) Then
    TableName = "PatientClinical"
ElseIf (TableRef = 5) Then
    TableName = "PatientDemographics"
ElseIf (TableRef = 6) Then
    TableName = "PatientFinancial"
ElseIf (TableRef = 7) Then
    TableName = "PatientNotes"
ElseIf (TableRef = 8) Then
    TableName = "PatientPreCerts"
ElseIf (TableRef = 9) Then
    TableName = "PatientReceivablePayments"
ElseIf (TableRef = 10) Then
    TableName = "PatientReceivables"
ElseIf (TableRef = 11) Then
    TableName = "PatientReceivableServices"
ElseIf (TableRef = 12) Then
    TableName = "PatientReferral"
ElseIf (TableRef = 13) Then
    TableName = "PracticeActivity"
ElseIf (TableRef = 14) Then
    TableName = "PracticeAffiliations"
ElseIf (TableRef = 15) Then
    TableName = "PracticeCalendar"
ElseIf (TableRef = 16) Then
    TableName = "PracticeCodeTable"
ElseIf (TableRef = 17) Then
    TableName = "PracticeFavorites"
ElseIf (TableRef = 18) Then
    TableName = "PracticeFeeSchedules"
ElseIf (TableRef = 19) Then
    TableName = "PracticeInsurers"
ElseIf (TableRef = 20) Then
    TableName = "PracticeInterfaceConfiguration"
ElseIf (TableRef = 21) Then
    TableName = "PracticeMessages"
ElseIf (TableRef = 22) Then
    TableName = "PracticeName"
ElseIf (TableRef = 23) Then
    TableName = "PracticePayables"
ElseIf (TableRef = 24) Then
    TableName = "PracticeReports"
ElseIf (TableRef = 25) Then
    TableName = "PracticeServices"
ElseIf (TableRef = 26) Then
    TableName = "PracticeTransactionJournal"
ElseIf (TableRef = 27) Then
    TableName = "PracticeVendors"
ElseIf (TableRef = 28) Then
    TableName = "Resources"
ElseIf (TableRef = 29) Then
    TableName = "Audit"
End If
End Function

Private Function CompressRecord(TheRecord As String, NewRecord As String) As Boolean
NewRecord = ""
CompressRecord = True
End Function

Private Function DecompressRecord(TheRecord As String, NewRecord As String) As Boolean
NewRecord = ""
DecompressRecord = True
End Function

Public Function ApplyAudit() As Boolean
Dim ATime As Integer
Dim ADate As String
Dim TheTime As String
Dim TheDate As String
Dim CmpPrevRec As String
Dim CmpCurrRec As String
ApplyAudit = False
If (Trim(AuditUser) <> "") And (Trim(AuditTable) <> "") And _
   (AuditRecord > 0) And (CompressRecord(AuditCurrent, CmpCurrRec)) And _
   (CompressRecord(AuditPrevious, CmpPrevRec)) Then
    Set PrcAudit = New PracticeAudit
    PrcAudit.AuditId = AuditRecId
    If (PrcAudit.RetrieveAudit) Then
        PrcAudit.AuditType = AuditType
        PrcAudit.AuditUserId = AuditUser
        PrcAudit.AuditTableId = TableReference(AuditTable)
        PrcAudit.AuditRecordId = AuditRecord
        PrcAudit.AuditCurrentRec = CmpCurrRec
        PrcAudit.AuditPrevRec = CmpPrevRec
        Call FormatTodaysDate(TheDate, False)
        ADate = Mid(TheDate, 7, 4) + Left(TheDate, 2) + Mid(TheDate, 4, 2)
        PrcAudit.AuditDate = ADate
        Call FormatTimeNow(TheTime)
        ATime = ConvertTimeToMinutes(TheTime)
        PrcAudit.AuditTime = ATime
        If ((AuditType = "I") Or (AuditType = "O")) And (ATime < 0) Then
        
        Else
            Call PrcAudit.ApplyAudit
        End If
    End If
    Set PrcAudit = Nothing
    ApplyAudit = True
End If
End Function

Public Function DeleteAudit() As Boolean
DeleteAudit = False
If (AuditRecId <> "") Then
    Set PrcAudit = New PracticeAudit
    PrcAudit.AuditId = AuditRecId
    If (PrcAudit.RetrieveAudit) Then
        Call PrcAudit.DeleteAudit
    End If
    Set PrcAudit = Nothing
    DeleteAudit = True
End If
End Function

Public Function FindAudit() As Long
FindAudit = -1
Set PrcAudit = New PracticeAudit
PrcAudit.AuditType = AuditType
PrcAudit.AuditDate = AuditDate
PrcAudit.AuditTime = 0
PrcAudit.AuditUserId = AuditUser
PrcAudit.AuditTableId = TableReference(AuditTable)
PrcAudit.AuditRecordId = AuditRecord
FindAudit = PrcAudit.FindAudit
Set PrcAudit = Nothing
End Function

Public Function SelectAudit(TheItem As Integer) As Boolean
SelectAudit = False
Set PrcAudit = New PracticeAudit
If (PrcAudit.SelectAudit(TheItem)) Then
    AuditType = PrcAudit.AuditType
    AuditUser = PrcAudit.AuditUserId
    AuditDate = PrcAudit.AuditDate
    AuditTime = PrcAudit.AuditTime
    AuditTable = TableName(PrcAudit.AuditTableId)
    AuditRecord = PrcAudit.AuditRecordId
    Call DecompressRecord(PrcAudit.AuditCurrentRec, AuditCurrent)
    Call DecompressRecord(PrcAudit.AuditPrevRec, AuditPrevious)
    SelectAudit = True
End If
Set PrcAudit = Nothing
End Function

Public Function RetrieveAudit() As Boolean
RetrieveAudit = False
If (AuditRecId <> "") Then
    Set PrcAudit = New PracticeAudit
    PrcAudit.AuditId = AuditRecId
    If (PrcAudit.RetrieveAudit) Then
        AuditType = PrcAudit.AuditType
        AuditUser = PrcAudit.AuditUserId
        AuditTable = TableName(PrcAudit.AuditTableId)
        AuditDate = PrcAudit.AuditDate
        AuditTime = PrcAudit.AuditTime
        AuditRecord = PrcAudit.AuditRecordId
        Call DecompressRecord(PrcAudit.AuditCurrentRec, AuditCurrent)
        Call DecompressRecord(PrcAudit.AuditPrevRec, AuditPrevious)
        RetrieveAudit = True
    End If
    Set PrcAudit = Nothing
End If
End Function

Public Function PostAudit(ActId As String, UserId As Long, RecId As Long, TblId As String) As Boolean
PostAudit = True
If CheckConfigCollection("AUDIT") = "T" Then
    AuditType = ActId
    AuditUser = UserId
    AuditRecord = RecId
    AuditTable = TblId
    AuditPrevious = ""
    AuditCurrent = ""
    PostAudit = ApplyAudit
End If
End Function

Public Function PostLogin(UserId As Long) As Boolean
Dim Temp As String
PostLogin = True
AuditType = "I"
AuditUser = UserId
AuditRecord = 1
AuditTable = "Audit"
AuditPrevious = ""
AuditCurrent = ""
PostLogin = ApplyAudit
End Function

Public Function PostLogout(UserId As Long) As Boolean
Dim Temp As String
PostLogout = True
AuditType = "O"
AuditUser = UserId
AuditRecord = 1
AuditTable = "Audit"
AuditPrevious = ""
AuditCurrent = ""
PostLogout = ApplyAudit
End Function

Public Function PostClocking(UserId As Long, IType As String) As Boolean
Dim Temp As String
PostClocking = True
If (IType = "I") Or (IType = "O") Then
    AuditType = IType
    AuditUser = UserId
    AuditRecord = 1
    AuditTable = "Audit"
    AuditPrevious = ""
    AuditCurrent = ""
    PostClocking = ApplyAudit
End If
End Function

Public Function GetResourceHours(ResourceId As Long, TheDate As String, InTime As String, OutTime As String, IAdtId As Long, OAdtId As Long, WhichItem As Integer) As Boolean
Dim i As Integer
Dim TestDate As String
GetResourceHours = False
InTime = ""
OutTime = ""
IAdtId = 0
OAdtId = 0
If (Trim(TheDate) <> "") And (ResourceId > 0) Then
    Set PrcAudit = New PracticeAudit
    TestDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
    PrcAudit.AuditType = "I"
    PrcAudit.AuditDate = TestDate
    PrcAudit.AuditTime = 0
    PrcAudit.AuditUserId = ResourceId
    PrcAudit.AuditTableId = 0
    PrcAudit.AuditRecordId = -1
    If (PrcAudit.FindAudit > 0) Then
        If (PrcAudit.SelectAudit(WhichItem)) Then
            If (PrcAudit.AuditTime >= 0) Then
                Call ConvertMinutesToTime(PrcAudit.AuditTime, InTime)
                IAdtId = PrcAudit.AuditId
            End If
        End If
    End If
    PrcAudit.AuditType = "O"
    PrcAudit.AuditDate = TestDate
    PrcAudit.AuditTime = 0
    PrcAudit.AuditUserId = ResourceId
    PrcAudit.AuditTableId = 0
    PrcAudit.AuditRecordId = -1
    If (PrcAudit.FindAudit > 0) Then
        If (PrcAudit.SelectAudit(WhichItem)) Then
            If (PrcAudit.AuditTime >= 0) Then
                Call ConvertMinutesToTime(PrcAudit.AuditTime, OutTime)
                OAdtId = PrcAudit.AuditId
            End If
        End If
    End If
    GetResourceHours = True
    Set PrcAudit = Nothing
End If
End Function

Public Function GetClockStatus(ResourceId As Long, TheStatus As String) As Boolean
Dim i As Integer
Dim InTime As Integer
Dim InId As Long
Dim OutTime As Integer
Dim OutId As Long
Dim TestDate As String
GetClockStatus = False
InTime = 0
OutTime = 0
TheStatus = "Clocked Out"
If (ResourceId > 0) Then
    InId = 0
    OutId = 0
    InTime = 0
    OutTime = 0
    TestDate = ""
    Call FormatTodaysDate(TestDate, False)
    Set PrcAudit = New PracticeAudit
    TestDate = Mid(TestDate, 7, 4) + Mid(TestDate, 1, 2) + Mid(TestDate, 4, 2)
    PrcAudit.AuditType = "I"
    PrcAudit.AuditDate = TestDate
    PrcAudit.AuditTime = 0
    PrcAudit.AuditUserId = ResourceId
    PrcAudit.AuditTableId = TableReference("Audit")
    PrcAudit.AuditRecordId = 1
    If (PrcAudit.FindAudit > 0) Then
        i = 1
        While (PrcAudit.SelectAudit(i))
            If (TestDate = PrcAudit.AuditDate) Then
                InTime = PrcAudit.AuditTime
                InId = PrcAudit.AuditId
            End If
            i = i + 1
        Wend
    End If
    PrcAudit.AuditType = "O"
    PrcAudit.AuditDate = TestDate
    PrcAudit.AuditTime = 0
    PrcAudit.AuditUserId = ResourceId
    PrcAudit.AuditTableId = TableReference("Audit")
    PrcAudit.AuditRecordId = 1
    If (PrcAudit.FindAudit > 0) Then
        i = 1
        While (PrcAudit.SelectAudit(i))
            If (TestDate = PrcAudit.AuditDate) Then
                OutTime = PrcAudit.AuditTime
                OutId = PrcAudit.AuditId
            End If
            i = i + 1
        Wend
    End If
    Set PrcAudit = Nothing
    GetClockStatus = True
    If (InTime = 0) And (OutTime = 0) Then
        TheStatus = "Clocked Out"
    Else
        If (InTime <= OutTime) Then
            If (InId < OutId) Then
                TheStatus = "Clocked Out"
            Else
                TheStatus = "Clocked In"
            End If
        Else
            TheStatus = "Clocked In"
        End If
    End If
End If
End Function

