VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DI_ExamClinical"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Class for Clinical Info in Exam/Highlights screen
Option Explicit
' Clinical Content
Public ClinicalId As Long
Public ClinicalPatientId As Long
Public ClinicalApptId As Long
Public ClinicalType As String
Public ClinicalEyeContext As String
Public ClinicalSymptom As String
Public ClinicalFindings As String
Public ClinicalDescriptor As String
Public ClinicalInstructions As String
Public ClinicalDraw As String
Public ClinicalStatus As String
Public ClinicalHighlights As String
Public ClinicalFollowup As String
Public ClinicalPermanent As String
Public ClinicalPostOpPeriod As Long
' Clinical Note Content
Public ClinicalNotesId As Long
Public ClinicalNotesType As String
Public ClinicalNotesPatientId As Long
Public ClinicalNotesAppointmentId As Long
Public ClinicalNotesText1 As String
Public ClinicalNotesText2 As String
Public ClinicalNotesText3 As String
Public ClinicalNotesText4 As String
Public ClinicalNotesUser As String
Public ClinicalNotesDate As String
Public ClinicalNotesSystem As String
Public ClinicalNotesEye As String
Public ClinicalNotesCategory As String
Public ClinicalNotesOffDate As String
Public ClinicalNotesCommentOn As Boolean
Public ClinicalNotesClaimOn As Boolean
Public ClinicalNotesAudioOn As Boolean
Public ClinicalNotesHighlight As String
Public ClinicalNotesILPNRef As String
Public ClinicalNotesAlertMask As String
' Clinical Highlight Additional Content
Public ClinicalApptDate As String
Public ClinicalResourceName As String

Private ModuleName As String
Private rsExamCln As ADODB.Recordset
Private rsExamClnNote As ADODB.Recordset

Private Sub Class_Initialize()
Set rsExamCln = CreateAdoRecordset
Set rsExamClnNote = CreateAdoRecordset
Call InitExamClinical
End Sub

Private Sub Class_Terminate()
Call InitExamClinical
Set rsExamCln = Nothing
Set rsExamClnNote = Nothing
End Sub

Private Function InitExamClinical() As Boolean
ClinicalId = 0
ClinicalPatientId = 0
ClinicalApptId = 0
ClinicalType = ""
ClinicalEyeContext = ""
ClinicalSymptom = ""
ClinicalFindings = ""
ClinicalDescriptor = ""
ClinicalInstructions = ""
ClinicalDraw = ""
ClinicalStatus = "A"
ClinicalHighlights = ""
ClinicalFollowup = ""
ClinicalPermanent = ""
ClinicalPostOpPeriod = 0

ClinicalNotesType = ""
ClinicalNotesPatientId = 0
ClinicalNotesAppointmentId = 0
ClinicalNotesText1 = ""
ClinicalNotesText2 = ""
ClinicalNotesText3 = ""
ClinicalNotesText4 = ""
ClinicalNotesUser = ""
ClinicalNotesDate = ""
ClinicalNotesSystem = ""
ClinicalNotesEye = ""
ClinicalNotesCategory = ""
ClinicalNotesOffDate = ""
ClinicalNotesCommentOn = False
ClinicalNotesClaimOn = False
ClinicalNotesAudioOn = False
ClinicalNotesHighlight = ""
ClinicalNotesILPNRef = ""
ClinicalNotesAlertMask = ""

ClinicalApptDate = ""
ClinicalResourceName = ""
End Function

Private Sub LoadExamClinicalNotes()
On Error GoTo UI_ErrorHandler
If (rsExamClnNote("NoteId") <> "") Then
    ClinicalNotesId = rsExamClnNote("NoteId")
Else
    ClinicalNotesId = 0
End If
If (rsExamClnNote("PatientId") <> "") Then
    ClinicalNotesPatientId = rsExamClnNote("PatientId")
Else
    ClinicalNotesPatientId = 0
End If
If (rsExamClnNote("AppointmentId") <> "") Then
    ClinicalNotesAppointmentId = rsExamClnNote("AppointmentId")
Else
    ClinicalNotesAppointmentId = 0
End If
If (rsExamClnNote("Note1") <> "") Then
    ClinicalNotesText1 = rsExamClnNote("Note1")
Else
    ClinicalNotesText1 = ""
End If
If (rsExamClnNote("Note2") <> "") Then
    ClinicalNotesText2 = rsExamClnNote("Note2")
Else
    ClinicalNotesText2 = ""
End If
If (rsExamClnNote("Note3") <> "") Then
    ClinicalNotesText3 = rsExamClnNote("Note3")
Else
    ClinicalNotesText3 = ""
End If
If (rsExamClnNote("Note4") <> "") Then
    ClinicalNotesText4 = rsExamClnNote("Note4")
Else
    ClinicalNotesText4 = ""
End If
If (rsExamClnNote("NoteType") <> "") Then
    ClinicalNotesType = rsExamClnNote("NoteType")
Else
    ClinicalNotesType = ""
End If
If (rsExamClnNote("UserName") <> "") Then
    ClinicalNotesUser = rsExamClnNote("UserName")
Else
    ClinicalNotesUser = ""
End If
If (rsExamClnNote("NoteDate") <> "") Then
    ClinicalNotesDate = rsExamClnNote("NoteDate")
Else
    ClinicalNotesDate = ""
End If
If (rsExamClnNote("NoteSystem") <> "") Then
    ClinicalNotesSystem = rsExamClnNote("NoteSystem")
Else
    ClinicalNotesSystem = ""
End If
If (rsExamClnNote("NoteEye") <> "") Then
    ClinicalNotesEye = rsExamClnNote("NoteEye")
Else
    ClinicalNotesEye = ""
End If
If (rsExamClnNote("NoteOffDate") <> "") Then
    ClinicalNotesOffDate = rsExamClnNote("NoteOffDate")
Else
    ClinicalNotesOffDate = ""
End If
If (rsExamClnNote("NoteCategory") <> "") Then
    ClinicalNotesCategory = rsExamClnNote("NoteCategory")
Else
    ClinicalNotesCategory = ""
End If
If (rsExamClnNote("NoteHighlight") <> "") Then
    ClinicalNotesHighlight = rsExamClnNote("NoteHighlight")
Else
    ClinicalNotesHighlight = ""
End If
If (rsExamClnNote("NoteILPNRef") <> "") Then
    ClinicalNotesILPNRef = rsExamClnNote("NoteILPNRef")
Else
    ClinicalNotesILPNRef = ""
End If
If (rsExamClnNote("NoteAlertType") <> "") Then
    ClinicalNotesAlertMask = rsExamClnNote("NoteAlertType")
Else
    ClinicalNotesAlertMask = ""
End If
If (rsExamClnNote("NoteCommentOn") <> "") Then
    If (rsExamClnNote("NoteCommentOn") = "T") Then
        ClinicalNotesCommentOn = True
    Else
        ClinicalNotesCommentOn = False
    End If
Else
    ClinicalNotesCommentOn = False
End If
If (rsExamClnNote("NoteClaimOn") <> "") Then
    If (rsExamClnNote("NoteClaimOn") = "T") Then
        ClinicalNotesClaimOn = True
    Else
        ClinicalNotesClaimOn = False
    End If
Else
    ClinicalNotesClaimOn = False
End If
If (rsExamClnNote("NoteAudioOn") <> "") Then
    If (rsExamClnNote("NoteAudioOn") = "T") Then
        ClinicalNotesAudioOn = True
    Else
        ClinicalNotesAudioOn = False
    End If
Else
    ClinicalNotesAudioOn = False
End If
If (rsExamClnNote("AppDate") <> "") Then
    ClinicalApptDate = rsExamClnNote("AppDate")
Else
    ClinicalApptDate = ""
End If
Exit Sub
UI_ErrorHandler:
    Resume Next
End Sub

Private Sub LoadExamClinicalRecord(MyType As Integer)
On Error GoTo UI_ErrorHandler
If (rsExamCln("ClinicalId") <> "") Then
    ClinicalId = rsExamCln("ClinicalId")
Else
    ClinicalId = 0
End If
If (rsExamCln("PatientId") <> "") Then
    ClinicalPatientId = rsExamCln("PatientId")
Else
    ClinicalPatientId = 0
End If
If (rsExamCln("AppointmentId") <> "") Then
    ClinicalApptId = rsExamCln("AppointmentId")
Else
    ClinicalApptId = 0
End If
If (rsExamCln("ClinicalType") <> "") Then
    ClinicalType = rsExamCln("ClinicalType")
Else
    ClinicalType = ""
End If
If (rsExamCln("EyeContext") <> "") Then
    ClinicalEyeContext = rsExamCln("EyeContext")
Else
    ClinicalEyeContext = ""
End If
If (rsExamCln("Symptom") <> "") Then
    ClinicalSymptom = rsExamCln("Symptom")
Else
    ClinicalSymptom = ""
End If
If (rsExamCln("FindingDetail") <> "") Then
    ClinicalFindings = rsExamCln("FindingDetail")
Else
    ClinicalFindings = ""
End If
If (rsExamCln("ImageDescriptor") <> "") Then
    ClinicalDescriptor = rsExamCln("ImageDescriptor")
Else
    ClinicalDescriptor = ""
End If
If (rsExamCln("ImageInstructions") <> "") Then
    ClinicalInstructions = rsExamCln("ImageInstructions")
Else
    ClinicalInstructions = ""
End If
If (rsExamCln("DrawFileName") <> "") Then
    ClinicalDraw = rsExamCln("DrawFileName")
Else
    ClinicalDraw = ""
End If
If (rsExamCln("Highlights") <> "") Then
    ClinicalHighlights = rsExamCln("Highlights")
Else
    ClinicalHighlights = ""
End If
If (rsExamCln("Followup") <> "") Then
    ClinicalFollowup = rsExamCln("Followup")
Else
    ClinicalFollowup = ""
End If
If (rsExamCln("PermanentCondition") <> "") Then
    ClinicalPermanent = rsExamCln("PermanentCondition")
Else
    ClinicalPermanent = ""
End If
If (rsExamCln("PostOpPeriod") <> "") Then
    ClinicalPostOpPeriod = rsExamCln("PostOpPeriod")
Else
    ClinicalPostOpPeriod = 0
End If
If (rsExamCln("Status") <> "") Then
    ClinicalStatus = rsExamCln("Status")
Else
    ClinicalStatus = ""
End If
If (MyType <> 0) Then
    If (rsExamCln("AppDate") <> "") Then
        ClinicalApptDate = rsExamCln("AppDate")
    Else
        ClinicalApptDate = ""
    End If
    If (rsExamCln("ResourceName") <> "") Then
        ClinicalResourceName = rsExamCln("ResourceName")
    Else
        ClinicalResourceName = ""
    End If
End If
Exit Sub
UI_ErrorHandler:
    Resume Next
End Sub

Public Function LoadClinicalData(PatId As Long, ApptId As Long, AptDate As String) As Boolean
On Error GoTo DbErrorHandler
Dim strSQL As String
LoadClinicalData = False
If (ApptId > 0) And (PatId > 0) And (Trim(AptDate) <> "") Then
    Call InitExamClinical
    strSQL = "usp_DI_ExamContent " + str(PatId) + ", " + str(ApptId) + ", " + Trim(AptDate)
    Set rsExamCln = Nothing
    Set rsExamCln = CreateAdoRecordset
    Call rsExamCln.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    LoadClinicalData = Not (rsExamCln.EOF)
End If
Exit Function
DbErrorHandler:
    ModuleName = "LoadClinicalData"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadPreviousClinicalDiagnosis(ApptId As Long, MyEye As String) As Boolean
On Error GoTo DbErrorHandler
Dim Temp As String
Dim strSQL As String
LoadPreviousClinicalDiagnosis = False
If (ApptId > 0) Then
    Call InitExamClinical
    Temp = str(ApptId) + ", " + Trim(MyEye) + " "
    strSQL = "usp_DI_ExamPrevDiags " + Temp
    Set rsExamCln = Nothing
    Set rsExamCln = CreateAdoRecordset
    Call rsExamCln.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    LoadPreviousClinicalDiagnosis = Not (rsExamCln.EOF)
End If
Exit Function
DbErrorHandler:
    ModuleName = "LoadPreviousClinicalDiagnosis"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadPreviousClinicalImprPlan(ApptId As Long) As Boolean
On Error GoTo DbErrorHandler
Dim Temp As String
Dim strSQL As String
LoadPreviousClinicalImprPlan = False
If (ApptId > 0) Then
    Call InitExamClinical
    Temp = str(ApptId) + " "
    strSQL = "usp_DI_PrevImprPlan " + Temp
    Set rsExamCln = Nothing
    Set rsExamCln = CreateAdoRecordset
    Call rsExamCln.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    LoadPreviousClinicalImprPlan = Not (rsExamCln.EOF)
End If
Exit Function
DbErrorHandler:
    ModuleName = "LoadPreviousClinicalImprPlan"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadHighlightsClinicalData(PatId As Long, AptDate As String) As Boolean
On Error GoTo DbErrorHandler
Dim Temp As String
Dim strSQL As String
LoadHighlightsClinicalData = False
If (PatId > 0) And (Trim(AptDate) <> "") Then
    Call InitExamClinical
    Temp = str(PatId) + ", " + Trim(AptDate) + " "
    strSQL = "usp_DI_ExamHighlights " + Temp
    Set rsExamCln = Nothing
    Set rsExamCln = CreateAdoRecordset
    Call rsExamCln.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    LoadHighlightsClinicalData = Not (rsExamCln.EOF)
End If
Exit Function
DbErrorHandler:
    ModuleName = "LoadHighlightsClinicalData"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadHighlightsImpressions(ApptId As Long) As Boolean
On Error GoTo DbErrorHandler
Dim Temp As String
Dim strSQL As String
LoadHighlightsImpressions = False
If (ApptId > 0) Then
    Call InitExamClinical
    Temp = str(ApptId) + " "
    strSQL = "usp_DI_ExamHighlightImprs " + Temp
    Set rsExamCln = Nothing
    Set rsExamCln = CreateAdoRecordset
    Call rsExamCln.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    LoadHighlightsImpressions = Not (rsExamCln.EOF)
End If
Exit Function
DbErrorHandler:
    ModuleName = "LoadHighlightsImpressions"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadClinicalNoteData(PatId As Long, ApptId As Long, AptDate As String) As Boolean
On Error GoTo DbErrorHandler
Dim Temp As String
Dim strSQL As String
LoadClinicalNoteData = False
If (ApptId > 0) And (PatId > 0) And (Trim(AptDate) <> "") Then
    Call InitExamClinical
    Temp = str(PatId) + ", " + str(ApptId) + ", " + Trim(AptDate)
    strSQL = "usp_DI_ExamNoteContent " + Temp
    Set rsExamClnNote = Nothing
    Set rsExamClnNote = CreateAdoRecordset
    Call rsExamClnNote.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    LoadClinicalNoteData = Not (rsExamClnNote.EOF)
End If
Exit Function
DbErrorHandler:
    ModuleName = "LoadClinicalNoteData"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function LocateStartingRecord(AType As String) As Boolean
On Error GoTo DbErrorHandler
' Really sucks ... need to figure out why there is a rowset failure on *.find method
LocateStartingRecord = False
If (Trim(AType) <> "") Then
    If rsExamCln.RecordCount > 0 Then
        rsExamCln.MoveFirst
    End If
'    rsExamCln.Find "ClinicalType = '" + AType + "'", 0, adSearchForward
    If (Not (rsExamCln.EOF)) Then
        Do Until (rsExamCln.EOF)
            If (rsExamCln("ClinicalType") = AType) Then
                Exit Do
            End If
            rsExamCln.MoveNext
        Loop
        If Not (rsExamCln.EOF) Then
            LocateStartingRecord = True
        End If
    End If
Else
    rsExamCln.MoveFirst
    LocateStartingRecord = True
End If
Exit Function
DbErrorHandler:
    ModuleName = "LocateStartingRecord"
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Public Function LocateStartingNoteRecord(AType As String) As Boolean
On Error GoTo DbErrorHandler
LocateStartingNoteRecord = False
If (Trim(AType) <> "") Then
    rsExamClnNote.MoveFirst
    If (Not (rsExamClnNote.EOF)) Then
'        If (AType = "C") Then
'            rsExamClnNote.Find "NoteType = 'C' And (NoteSystem >='1') And (NoteSystem <= '10')", 0, adSearchForward
'        ElseIf (AType = "F") Then
'            rsExamClnNote.Find "NoteType = 'C' And (Left(NoteSystem,1) = 'T')", 0, adSearchForward
'        ElseIf (AType = "Q") Then
'            rsExamClnNote.Find "NoteType = 'C' And (Len(NoteSystem) > 2)", 0, adSearchForward
'        ElseIf (AType = "A") Then
'            rsExamClnNote.Find "NoteType = 'R'", 0, adSearchForward
'        End If
        Do Until (rsExamClnNote.EOF)
            If (AType = "C") Then
                If (rsExamClnNote("NoteType") = "C") And ((rsExamClnNote("NoteSystem") >= "1") And (rsExamClnNote("NoteSystem") <= "9")) Or (rsExamClnNote("NoteSystem") = "10") Then
                    Exit Do
                End If
            ElseIf (AType = "F") Then
                If (rsExamClnNote("NoteType") = "C") And (Left(rsExamClnNote("NoteSystem"), 1) = "T") Then
                    Exit Do
                End If
            ElseIf (AType = "Q") Then
                If (rsExamClnNote("NoteType") = "C") And (Len(rsExamClnNote("NoteSystem")) > 2) Then
                    Exit Do
                End If
            ElseIf (AType = "A") Then
                If (rsExamClnNote("NoteType") = "R") Then
                    Exit Do
                End If
            End If
            rsExamClnNote.MoveNext
        Loop
        If Not (rsExamClnNote.EOF) Then
            LocateStartingNoteRecord = True
        End If
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "LocateStartingNoteRecord"
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveClinicalItem(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
RetrieveClinicalItem = False
If (Not rsExamCln.EOF) Then
    If (ListItem > 1) Then
        rsExamCln.MoveNext
    End If
    Call InitExamClinical
    Call LoadExamClinicalRecord(1)
    RetrieveClinicalItem = True
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveClinicalItem"
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveHighlightClinicalItem(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
RetrieveHighlightClinicalItem = False
If (Not rsExamCln.EOF) Then
    If (ListItem <> 1) Then
        rsExamCln.MoveNext
    End If
    Call InitExamClinical
    Call LoadExamClinicalRecord(1)
    RetrieveHighlightClinicalItem = True
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveHighlightClinicalItem"
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveClinicalNoteItem(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
RetrieveClinicalNoteItem = False
If (Not rsExamClnNote.EOF) Then
    If (ListItem <> 1) Then
        rsExamClnNote.MoveNext
    End If
    If Not (rsExamClnNote.EOF) Then
        Call LoadExamClinicalNotes
        RetrieveClinicalNoteItem = True
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveClinicalNoteItem"
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveProcedureCount(PatId As Long, TstId As String, ADate As String) As Long
On Error GoTo DbErrorHandler
Dim Temp As String
Dim strSQL As String
RetrieveProcedureCount = 0
If (PatId > 0) And (Trim(TstId) <> "") Then
    Temp = str(PatId) + ", " + TstId
    If (Trim(ADate) <> "") Then
        Temp = str(PatId) + ", " + ADate + ", " + TstId + " "
    Else
        Temp = str(PatId) + ", 00000000, " + TstId + " "
    End If
    strSQL = "usp_DI_ProcsBilled " + Temp
    Set rsExamCln = Nothing
    Set rsExamCln = CreateAdoRecordset
    Call rsExamCln.Open(strSQL, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    Do Until (rsExamClnNote.EOF)
        RetrieveProcedureCount = RetrieveProcedureCount + 1
        rsExamClnNote.MoveNext
    Loop
End If
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveProcedureCount"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function CurrentAppointment(PatId As Long) As Long

On Error GoTo DbErrorHandler
Dim strSQL As String
strSQL = "select top 1 appointmentid from PracticeActivity  where PatientId='" & PatId & "' order by AppointmentId desc"
Set rsExamCln = Nothing
Set rsExamCln = CreateAdoRecordset
Call rsExamCln.Open(strSQL, MyPracticeRepository)
CurrentAppointment = rsExamCln(0)
Exit Function
DbErrorHandler:
End Function
Public Function CurrentAppointmentQuestion(PatId As Long) As String
On Error GoTo DbErrorHandler
Dim strSQL As String
strSQL = "select top 1 questionset from PracticeActivity  where PatientId='" & PatId & "' order by AppointmentId desc"
Set rsExamCln = Nothing
Set rsExamCln = CreateAdoRecordset
Call rsExamCln.Open(strSQL, MyPracticeRepository)
CurrentAppointmentQuestion = rsExamCln(0)
Exit Function
DbErrorHandler:
End Function
Public Function CurrentAppointmentReferal(PatId As Long) As Long
On Error GoTo DbErrorHandler
Dim strSQL As String
strSQL = "select top 1 ReferralId from Appointments  where PatientId='" & PatId & "' order by AppointmentId desc"
Set rsExamCln = Nothing
Set rsExamCln = CreateAdoRecordset
Call rsExamCln.Open(strSQL, MyPracticeRepository)
CurrentAppointmentReferal = rsExamCln(0)
Exit Function
DbErrorHandler:
End Function
