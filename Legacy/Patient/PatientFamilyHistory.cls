VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientFamilyHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PatientFamilyHistory Object Module
Option Explicit
Public Id As Long
Public History As String
Public Comment As String
Private PatientFamilyHistoryTbl As ADODB.Recordset



Private Sub Class_Initialize()
Set PatientFamilyHistoryTbl = CreateAdoRecordset
End Sub

Private Sub InItPatientFamilyHistoryTbl()
Id = 0
History = ""
Comment = ""
End Sub

Private Sub LoadFamilyHistory()
If PatientFamilyHistoryTbl("Id") <> "" Then
    Id = PatientFamilyHistoryTbl("Id")
Else
    Id = 0
    Exit Sub
End If
If PatientFamilyHistoryTbl("History") <> "" Then
    History = PatientFamilyHistoryTbl("History")
Else
    History = ""
End If
If PatientFamilyHistoryTbl("Comment") <> "" Then
    Comment = PatientFamilyHistoryTbl("Comment")
Else
    Comment = ""
End If
End Sub


Public Function GetHistoryCount(PatientId As String, ApptId As String) As Integer
On Error GoTo PatientFamilyHistoryErr
Dim OrderString As String
OrderString = "GetFMX " + ApptId + "," + PatientId
 Set PatientFamilyHistoryTbl = Nothing
    Set PatientFamilyHistoryTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientFamilyHistoryTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFamilyHistoryTbl.EOF) Then
    GetHistoryCount = -1
   ' Call InitPatientProblemPerformedTbl
Else
    PatientFamilyHistoryTbl.MoveLast
    GetHistoryCount = PatientFamilyHistoryTbl.RecordCount
End If
Exit Function
PatientFamilyHistoryErr:
 LogError "PatientFamilyHistory", "GetHistoryCount", Err, Err.Description + " With PatientId  : " + Str(PatientId)
End Function


Public Function SelectFamilyHistory(ListItem As Integer) As Boolean
On Error GoTo PatientFamilyHistoryErr
If (PatientFamilyHistoryTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientFamilyHistoryTbl.RecordCount) Then
    Exit Function
End If
PatientFamilyHistoryTbl.MoveFirst
PatientFamilyHistoryTbl.Move ListItem - 1
LoadFamilyHistory
SelectFamilyHistory = True
Exit Function
PatientFamilyHistoryErr:
 LogError "SelectFamilyHistoryT", "SelectFamilyHistoryT", Err, Err.Description
End Function
