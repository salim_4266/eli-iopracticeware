VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Insurer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Insurer Object Module
Option Explicit
Public InsurerId As Long
Public InsurerName As String
Public InsurerPlanId As String
Public InsurerPlanName As String
Public InsurerPlanType As String
Public InsurerGroupId As String
Public InsurerGroupName As String
Public InsurerAddress As String
Public InsurerCity As String
Public InsurerState As String
Public InsurerZip As String
Public InsurerPhone As String
Public InsurerRefCode As String
Public InsurerPType As String
Public InsurerReferralRequired As Boolean
Public InsurerPreCertRequired As Boolean
Public InsurerCrossOver As Boolean
Public InsurerFeeSchedule As String
Public InsurerTOS As String
Public InsurerPOS As String
Public InsurerNEICNumber As String
Public InsurerENumber As String
Public InsurerEFormat As String
Public InsurerTFormat As String
Public InsurerPcPhone As String
Public InsurerElPhone As String
Public InsurerClPhone As String
Public InsurerPrPhone As String
Public InsurerMedicareXOverId As String
Public InsurerAdjustmentDefault As String
Public InsurerComment As String
Public InsurerPlanFormat As String
Public InsurerClaimMap As String
Public InsurerBusinessClass As String
Public InsurerCPTClass As String
Public InsurerOCodeOn As Boolean
Public Availability As String
Public Copay As Single
Public OutOfPocket As Single
Public InsurerServiceFilter As Boolean
Public StateJurisdiction As String

Private JustName As Integer
Private ModuleName As String
Private PracticeInsurerTbl As ADODB.Recordset
Private PracticeInsurerNew As Boolean

Private Sub Class_Initialize()
Set PracticeInsurerTbl = CreateAdoRecordset
Call InitInsurer
End Sub

Private Sub Class_Terminate()
Call InitInsurer
Set PracticeInsurerTbl = Nothing
End Sub

Private Sub InitInsurer()
PracticeInsurerNew = True
InsurerName = ""
InsurerGroupId = ""
InsurerGroupName = ""
InsurerPlanId = ""
InsurerPlanName = ""
InsurerPlanType = ""
InsurerAddress = ""
InsurerCity = ""
InsurerState = ""
InsurerZip = ""
InsurerPhone = ""
InsurerRefCode = ""
InsurerPType = ""
InsurerReferralRequired = False
InsurerPreCertRequired = False
InsurerCrossOver = False
InsurerFeeSchedule = ""
InsurerTOS = ""
InsurerPOS = ""
InsurerNEICNumber = ""
InsurerENumber = ""
InsurerEFormat = ""
InsurerTFormat = ""
InsurerPcPhone = ""
InsurerPrPhone = ""
InsurerElPhone = ""
InsurerClPhone = ""
InsurerMedicareXOverId = ""
InsurerAdjustmentDefault = ""
InsurerComment = ""
InsurerPlanFormat = ""
InsurerClaimMap = ""
InsurerBusinessClass = ""
InsurerCPTClass = ""
InsurerOCodeOn = False
Copay = 0
OutOfPocket = 0
Availability = "A"
InsurerServiceFilter = False
StateJurisdiction = ""
JustName = 0
End Sub

Private Sub LoadInsurer()
PracticeInsurerNew = False
If (PracticeInsurerTbl("InsurerName") <> "") Then
    InsurerName = PracticeInsurerTbl("InsurerName")
Else
    InsurerName = ""
End If
If (JustName = 1) Then
    Exit Sub
End If
If (PracticeInsurerTbl("InsurerId") <> "") Then
    InsurerId = PracticeInsurerTbl("InsurerId")
Else
    InsurerId = 0
End If
If (JustName = 2) Then
    Exit Sub
End If
If (PracticeInsurerTbl("InsurerAddress") <> "") Then
    InsurerAddress = PracticeInsurerTbl("InsurerAddress")
Else
    InsurerAddress = ""
End If
If (PracticeInsurerTbl("InsurerCity") <> "") Then
    InsurerCity = PracticeInsurerTbl("InsurerCity")
Else
    InsurerCity = ""
End If
If (PracticeInsurerTbl("InsurerState") <> "") Then
    InsurerState = PracticeInsurerTbl("InsurerState")
Else
    InsurerState = ""
End If
If (PracticeInsurerTbl("InsurerZip") <> "") Then
    InsurerZip = PracticeInsurerTbl("InsurerZip")
Else
    InsurerZip = ""
End If
If (PracticeInsurerTbl("InsurerPhone") <> "") Then
    InsurerPhone = PracticeInsurerTbl("InsurerPhone")
Else
    InsurerPhone = ""
End If
If (PracticeInsurerTbl("InsurerReferenceCode") <> "") Then
    InsurerRefCode = PracticeInsurerTbl("InsurerReferenceCode")
Else
    InsurerRefCode = "O"
End If
If (PracticeInsurerTbl("InsurerPayType") <> "") Then
    InsurerPType = PracticeInsurerTbl("InsurerPayType")
Else
    InsurerPType = ""
End If
If (PracticeInsurerTbl("InsurerPlanId") <> "") Then
    InsurerPlanId = PracticeInsurerTbl("InsurerPlanId")
Else
    InsurerPlanId = ""
End If
If (PracticeInsurerTbl("InsurerPlanName") <> "") Then
    InsurerPlanName = PracticeInsurerTbl("InsurerPlanName")
Else
    InsurerPlanName = ""
End If
If (PracticeInsurerTbl("InsurerPlanType") <> "") Then
    InsurerPlanType = PracticeInsurerTbl("InsurerPlanType")
Else
    InsurerPlanType = ""
End If
If (PracticeInsurerTbl("InsurerGroupId") <> "") Then
    InsurerGroupId = PracticeInsurerTbl("InsurerGroupId")
Else
    InsurerGroupId = ""
End If
If (PracticeInsurerTbl("InsurerGroupName") <> "") Then
    InsurerGroupName = PracticeInsurerTbl("InsurerGroupName")
Else
    InsurerGroupName = ""
End If
If (PracticeInsurerTbl("ReferralRequired") <> "") Then
    If (PracticeInsurerTbl("ReferralRequired") = "N") Then
        InsurerReferralRequired = False
    Else
        InsurerReferralRequired = True
    End If
Else
    InsurerReferralRequired = False
End If
If (PracticeInsurerTbl("PrecertRequired") <> "") Then
    If (PracticeInsurerTbl("PreCertRequired") = "N") Then
        InsurerPreCertRequired = False
    Else
        InsurerPreCertRequired = True
    End If
Else
    InsurerPreCertRequired = False
End If
If (PracticeInsurerTbl("InsurerCrossOver") <> "") Then
    If (PracticeInsurerTbl("InsurerCrossOver") = "N") Then
        InsurerCrossOver = False
    Else
        InsurerCrossOver = True
    End If
Else
    InsurerCrossOver = False
End If
If (PracticeInsurerTbl("OCode") <> "") Then
    If (PracticeInsurerTbl("OCode") = "N") Then
        InsurerOCodeOn = False
    Else
        InsurerOCodeOn = True
    End If
Else
    InsurerOCodeOn = True
End If
If (PracticeInsurerTbl("Copay") <> "") Then
    Copay = PracticeInsurerTbl("Copay")
Else
    Copay = 0
End If
If (PracticeInsurerTbl("OutOfPocket") <> "") Then
    OutOfPocket = PracticeInsurerTbl("OutOfPocket")
Else
    OutOfPocket = 0
End If
If (PracticeInsurerTbl("InsurerFeeSchedule") <> "") Then
    InsurerFeeSchedule = PracticeInsurerTbl("InsurerFeeSchedule")
Else
    InsurerFeeSchedule = ""
End If
If (PracticeInsurerTbl("InsurerTOSTbl") <> "") Then
    InsurerTOS = PracticeInsurerTbl("InsurerTOSTbl")
Else
    InsurerTOS = ""
End If
If (PracticeInsurerTbl("InsurerPOSTbl") <> "") Then
    InsurerPOS = PracticeInsurerTbl("InsurerPOSTbl")
Else
    InsurerPOS = ""
End If
If (PracticeInsurerTbl("MedicareCrossOverId") <> "") Then
    InsurerMedicareXOverId = PracticeInsurerTbl("MedicareCrossOverId")
Else
    InsurerMedicareXOverId = ""
End If
If (PracticeInsurerTbl("NEICNumber") <> "") Then
    InsurerNEICNumber = PracticeInsurerTbl("NEICNumber")
Else
    InsurerNEICNumber = ""
End If
If (PracticeInsurerTbl("InsurerENumber") <> "") Then
    InsurerENumber = PracticeInsurerTbl("InsurerENumber")
Else
    InsurerENumber = ""
End If
If (PracticeInsurerTbl("InsurerEFormat") <> "") Then
    InsurerEFormat = PracticeInsurerTbl("InsurerEFormat")
Else
    InsurerEFormat = ""
End If
If (PracticeInsurerTbl("InsurerTFormat") <> "") Then
    InsurerTFormat = PracticeInsurerTbl("InsurerTFormat")
Else
    InsurerTFormat = ""
End If
If (PracticeInsurerTbl("InsurerPrecPhone") <> "") Then
    InsurerPcPhone = PracticeInsurerTbl("InsurerPrecPhone")
Else
    InsurerPcPhone = ""
End If
If (PracticeInsurerTbl("InsurerEligPhone") <> "") Then
    InsurerElPhone = PracticeInsurerTbl("InsurerEligPhone")
Else
    InsurerElPhone = ""
End If
If (PracticeInsurerTbl("InsurerClaimPhone") <> "") Then
    InsurerClPhone = PracticeInsurerTbl("InsurerClaimPhone")
Else
    InsurerClPhone = ""
End If
If (PracticeInsurerTbl("InsurerProvPhone") <> "") Then
    InsurerPrPhone = PracticeInsurerTbl("InsurerProvPhone")
Else
    InsurerPrPhone = ""
End If
If (PracticeInsurerTbl("InsurerAdjustmentDefault") <> "") Then
    InsurerAdjustmentDefault = PracticeInsurerTbl("InsurerAdjustmentDefault")
Else
    InsurerAdjustmentDefault = ""
End If
If (PracticeInsurerTbl("InsurerPlanFormat") <> "") Then
    InsurerPlanFormat = PracticeInsurerTbl("InsurerPlanFormat")
Else
    InsurerPlanFormat = ""
End If
If (PracticeInsurerTbl("InsurerClaimMap") <> "") Then
    InsurerClaimMap = PracticeInsurerTbl("InsurerClaimMap")
Else
    InsurerClaimMap = ""
End If
If (PracticeInsurerTbl("InsurerBusinessClass") <> "") Then
    InsurerBusinessClass = PracticeInsurerTbl("InsurerBusinessClass")
Else
    InsurerBusinessClass = ""
End If
If (PracticeInsurerTbl("InsurerCPTClass") <> "") Then
    InsurerCPTClass = PracticeInsurerTbl("InsurerCPTClass")
Else
    InsurerCPTClass = ""
End If
If (PracticeInsurerTbl("InsurerComment") <> "") Then
    InsurerComment = PracticeInsurerTbl("InsurerComment")
Else
    InsurerComment = ""
End If
If (PracticeInsurerTbl("Availability") <> "") Then
    Availability = PracticeInsurerTbl("Availability")
Else
    Availability = ""
End If
If (PracticeInsurerTbl("StateJurisdiction") <> "") Then
    StateJurisdiction = PracticeInsurerTbl("StateJurisdiction")
Else
    StateJurisdiction = ""
End If
If (PracticeInsurerTbl("InsurerServiceFilter") <> "") Then
    If (PracticeInsurerTbl("InsurerServiceFilter") = "N") Then
        InsurerServiceFilter = False
    Else
        InsurerServiceFilter = True
    End If
Else
    InsurerServiceFilter = False
End If
End Sub

Private Function GetInsurer() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
JustName = False
GetInsurer = True
OrderString = "SELECT * FROM PracticeInsurers WHERE InsurerId =" + Str(InsurerId)
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.CommandType = adCmdText
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeInsurerTbl.EOF) Then
    Call InitInsurer
Else
    Call LoadInsurer
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetInsurer"
    GetInsurer = False
    InsurerId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetInsurerList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
JustName = False
GetInsurerList = -1
If (Trim(InsurerName) = "") Then
    Exit Function
End If
Ref = ">="
If (IType = 1) Then
    Ref = "="
End If
OrderString = "Select * FROM PracticeInsurers WHERE InsurerName " + Ref + " '" + UCase(Trim(InsurerName)) + "' "
If (IType = 2) Then
    JustName = 1
    OrderString = "Select DISTINCT InsurerName FROM PracticeInsurers WHERE InsurerName " + Ref + "'" + UCase(Trim(InsurerName)) + "' "
End If
If (IType = 3) Then
    JustName = 2
    OrderString = "Select DISTINCT InsurerName, InsurerId FROM PracticeInsurers WHERE InsurerName " + Ref + "'" + UCase(Trim(InsurerName)) + "' "
End If
If (Trim(InsurerGroupId) <> "") Then
    OrderString = OrderString + "And InsurerGroupId = '" + UCase(Trim(InsurerGroupId)) + "' "
End If
If (Trim(InsurerGroupName) <> "") Then
    OrderString = OrderString + "And InsurerGroupName = '" + UCase(Trim(InsurerGroupName)) + "' "
End If
If (Trim(InsurerPlanId) <> "") Then
    OrderString = OrderString + "And InsurerPlanId = '" + UCase(Trim(InsurerPlanId)) + "' "
End If
If (Trim(InsurerPlanName) <> "") Then
    OrderString = OrderString + "And InsurerPlanName = '" + UCase(Trim(InsurerPlanName)) + "' "
End If
If (Trim(InsurerPlanType) <> "") Then
    OrderString = OrderString + "And InsurerPlanType = '" + UCase(Trim(InsurerPlanType)) + "' "
End If
If (Trim(InsurerAddress) <> "") Then
    OrderString = OrderString + "And InsurerAddress = '" + UCase(Trim(InsurerAddress)) + "' "
End If
If (Trim(InsurerCity) <> "") Then
    OrderString = OrderString + "And InsurerCity = '" + UCase(Trim(InsurerCity)) + "' "
End If
If (Trim(InsurerState) <> "") Then
    OrderString = OrderString + "And InsurerState = '" + UCase(Trim(InsurerState)) + "' "
End If
If (Trim(InsurerZip) <> "") Then
    OrderString = OrderString + "And InsurerZip = '" + UCase(Trim(InsurerZip)) + "' "
End If
If (Trim(InsurerPlanType) <> "") Then
    OrderString = OrderString + "And InsurerPlanType = '" + UCase(Trim(InsurerPlanType)) + "' "
End If
If (Trim(InsurerRefCode) <> "") Then
    OrderString = OrderString + "And InsurerReferenceCode = '" + UCase(Trim(InsurerRefCode)) + "' "
End If
If (JustName) Then
    OrderString = OrderString + "ORDER BY InsurerName ASC "
Else
    OrderString = OrderString + "ORDER BY InsurerName ASC, InsurerAddress ASC, InsurerPlanId ASC, InsurerGroupId ASC "
End If
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeInsurerTbl.EOF) Then
    GetInsurerList = -1
    Call InitInsurer
Else
    If (IType = 1) Then
        GetInsurerList = PracticeInsurerTbl.RecordCount
    Else
        If (Left(InsurerName, 8) = "MEDICARE") Then
            GetInsurerList = PracticeInsurerTbl.RecordCount
        Else
            GetInsurerList = 1
        End If
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetInsurerList"
    GetInsurerList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetInsurerListbyProvPhone() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetInsurerListbyProvPhone = -1
If (Trim(InsurerPrPhone) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeInsurers WHERE InsurerProvPhone = '" + UCase(Trim(InsurerPrPhone)) + "' "
OrderString = OrderString + "ORDER BY InsurerName ASC "
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeInsurerTbl.EOF) Then
    GetInsurerListbyProvPhone = -1
    Call InitInsurer
Else
    GetInsurerListbyProvPhone = PracticeInsurerTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetInsurerListbyProvPhone"
    GetInsurerListbyProvPhone = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetInsurerListbyGroupId() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetInsurerListbyGroupId = -1
If (Trim(InsurerGroupId) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeInsurers WHERE InsurerGroupId = '" + UCase(Trim(InsurerGroupId)) + "' "
OrderString = OrderString + "ORDER BY InsurerName ASC "
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeInsurerTbl.EOF) Then
    GetInsurerListbyGroupId = -1
    Call InitInsurer
Else
    GetInsurerListbyGroupId = PracticeInsurerTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetInsurerListbyGroupId"
    GetInsurerListbyGroupId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetInsurerbyFeeSchedule() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetInsurerbyFeeSchedule = -1
If (Trim(InsurerFeeSchedule) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PracticeInsurers WHERE InsurerFeeSchedule = '" + UCase(Trim(InsurerFeeSchedule)) + "' "
OrderString = OrderString + "ORDER BY InsurerFeeSchedule ASC "
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticeInsurerTbl.EOF) Then
    GetInsurerbyFeeSchedule = -1
    Call InitInsurer
Else
    GetInsurerbyFeeSchedule = PracticeInsurerTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetInsurerbyFeeSchedule"
    GetInsurerbyFeeSchedule = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutInsurer() As Boolean
On Error GoTo DbErrorHandler
PutInsurer = True
If (PracticeInsurerNew) Then
    PracticeInsurerTbl.AddNew
End If
PracticeInsurerTbl("InsurerName") = UCase(Trim(InsurerName))
PracticeInsurerTbl("InsurerAddress") = Trim(InsurerAddress)
PracticeInsurerTbl("InsurerCity") = Trim(InsurerCity)
PracticeInsurerTbl("InsurerState") = UCase(Trim(InsurerState))
PracticeInsurerTbl("InsurerZip") = Trim(InsurerZip)
PracticeInsurerTbl("InsurerPhone") = Trim(InsurerPhone)
PracticeInsurerTbl("InsurerPlanId") = Trim(InsurerPlanId)
PracticeInsurerTbl("InsurerPlanName") = Trim(InsurerPlanName)
PracticeInsurerTbl("InsurerPlanType") = UCase(InsurerPlanType)
PracticeInsurerTbl("InsurerGroupId") = Trim(InsurerGroupId)
PracticeInsurerTbl("InsurerGroupName") = Trim(InsurerGroupName)
If (InsurerReferralRequired) Then
    PracticeInsurerTbl("ReferralRequired") = "Y"
Else
    PracticeInsurerTbl("ReferralRequired") = "N"
End If
If (InsurerPreCertRequired) Then
    PracticeInsurerTbl("PrecertRequired") = "Y"
Else
    PracticeInsurerTbl("PrecertRequired") = "N"
End If
If (InsurerCrossOver) Then
    PracticeInsurerTbl("InsurerCrossOver") = "Y"
Else
    PracticeInsurerTbl("InsurerCrossOver") = "N"
End If
If (InsurerServiceFilter) Then
    PracticeInsurerTbl("InsurerServiceFilter") = "Y"
Else
    PracticeInsurerTbl("InsurerServiceFilter") = "N"
End If
If (InsurerOCodeOn) Then
    PracticeInsurerTbl("OCode") = "Y"
Else
    PracticeInsurerTbl("OCode") = "N"
End If
PracticeInsurerTbl("Copay") = Copay
PracticeInsurerTbl("OutOfPocket") = OutOfPocket
PracticeInsurerTbl("Availability") = "A"
If (Availability <> "A") Then
    PracticeInsurerTbl("Availability") = "-"
End If
PracticeInsurerTbl("InsurerFeeSchedule") = Trim(InsurerFeeSchedule)
PracticeInsurerTbl("InsurerTOSTbl") = Trim(InsurerTOS)
PracticeInsurerTbl("InsurerPOSTbl") = Trim(InsurerPOS)
PracticeInsurerTbl("MedicareCrossOverId") = Trim(InsurerMedicareXOverId)
PracticeInsurerTbl("NEICNumber") = Trim(InsurerNEICNumber)
PracticeInsurerTbl("InsurerENumber") = Trim(InsurerENumber)
PracticeInsurerTbl("InsurerEFormat") = UCase(InsurerEFormat)
PracticeInsurerTbl("InsurerTFormat") = UCase(InsurerTFormat)
PracticeInsurerTbl("InsurerPrecPhone") = Trim(InsurerPcPhone)
PracticeInsurerTbl("InsurerProvPhone") = Trim(InsurerPrPhone)
PracticeInsurerTbl("InsurerEligPhone") = Trim(InsurerElPhone)
PracticeInsurerTbl("InsurerClaimPhone") = Trim(InsurerClPhone)
PracticeInsurerTbl("InsurerReferenceCode") = Trim(InsurerRefCode)
PracticeInsurerTbl("InsurerPayType") = Trim(InsurerPType)
PracticeInsurerTbl("InsurerAdjustmentDefault") = Trim(InsurerAdjustmentDefault)
PracticeInsurerTbl("InsurerComment") = Trim(InsurerComment)
PracticeInsurerTbl("InsurerPlanFormat") = Trim(InsurerPlanFormat)
PracticeInsurerTbl("InsurerClaimMap") = Trim(InsurerClaimMap)
PracticeInsurerTbl("InsurerBusinessClass") = Trim(InsurerBusinessClass)
PracticeInsurerTbl("InsurerCPTClass") = Trim(InsurerCPTClass)
PracticeInsurerTbl("StateJurisdiction") = UCase(Trim(StateJurisdiction))
PracticeInsurerTbl.Update
If (PracticeInsurerNew) Then
    InsurerId = GetJustAddedRecord
    PracticeInsurerNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutInsurer"
    PutInsurer = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
JustName = False
GetJustAddedRecord = 0
If (GetInsurerList(1) > 0) Then
    If (SelectInsurer(1)) Then
        GetJustAddedRecord = PracticeInsurerTbl("InsurerId")
    End If
End If
End Function

Private Function KillInsurers() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillInsurers = True
OrderString = "SELECT * FROM PracticeInsurers "
MyPracticeRepositoryCmd.CommandText = OrderString
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PracticeInsurerTbl.EOF) Then
    PracticeInsurerTbl.MoveFirst
    While Not (PracticeInsurerTbl.EOF)
        PracticeInsurerTbl.Delete
        PracticeInsurerTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillInsurers"
    KillInsurers = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeInsurer() As Boolean
On Error GoTo DbErrorHandler
PurgeInsurer = True
PracticeInsurerTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeInsurer"
    PurgeInsurer = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectInsurer(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectInsurer = False
If (PracticeInsurerTbl.EOF) Or (ListItem < 1) Then
    Exit Function
End If
PracticeInsurerTbl.MoveFirst
PracticeInsurerTbl.Move ListItem - 1
SelectInsurer = True
Call LoadInsurer
Exit Function
DbErrorHandler:
    ModuleName = "SelectInsurer"
    InsurerId = -256
    SelectInsurer = False
    If (Err <> 3021) Then
        Call PinpointPRError(ModuleName, Err)
    End If
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyInsurer() As Boolean
ApplyInsurer = PutInsurer
End Function

Public Function DeleteInsurer() As Boolean
DeleteInsurer = PurgeInsurer
End Function

Public Function RetrieveInsurer() As Boolean
RetrieveInsurer = GetInsurer
End Function

Public Function KillInsurer() As Boolean
KillInsurer = KillInsurers
End Function

Public Function FindInsurer() As Long
JustName = 0
FindInsurer = GetInsurerList(0)
End Function

Public Function FindInsurerbyProvPhone() As Long
FindInsurerbyProvPhone = GetInsurerListbyProvPhone
End Function

Public Function FindInsurerbyGroupId() As Long
FindInsurerbyGroupId = GetInsurerListbyGroupId
End Function

Public Function FindInsurerbyName() As Long
JustName = 0
InsurerPlanName = ""
If (Len(InsurerName) > 1) Then
    FindInsurerbyName = GetInsurerList(1)
ElseIf (Len(InsurerName) = 1) Then
    FindInsurerbyName = GetInsurerList(0)
Else
    InsurerName = Chr(1)
    FindInsurerbyName = GetInsurerList(2)
End If
End Function

Public Function FindInsurerbyNameandAddress() As Long
JustName = 0
InsurerPlanName = ""
If (Len(InsurerName) > 1) Then
    FindInsurerbyNameandAddress = GetInsurerList(5)
ElseIf (Len(InsurerName) = 1) Then
    FindInsurerbyNameandAddress = GetInsurerList(0)
Else
    InsurerName = Chr(1)
    FindInsurerbyNameandAddress = GetInsurerList(2)
End If
End Function

Public Function FindInsurerbyPlanName() As Long
JustName = 0
If (Len(InsurerPlanName) > 1) Then
    FindInsurerbyPlanName = GetInsurerList(4)
ElseIf (Len(InsurerPlanName) = 1) Then
    FindInsurerbyPlanName = GetInsurerList(4)
Else
    InsurerPlanName = Chr(1)
    FindInsurerbyPlanName = GetInsurerList(4)
End If
End Function

Public Function FindInsurerbyNameDistinct() As Long
JustName = 0
InsurerPlanName = ""
If (Len(InsurerName) < 1) Then
    InsurerName = Chr(1)
End If
FindInsurerbyNameDistinct = GetInsurerList(2)
End Function

Public Function FindInsurerbyNameDistinctOther() As Long
JustName = 0
InsurerPlanName = ""
If (Len(InsurerName) < 1) Then
    InsurerName = Chr(1)
End If
FindInsurerbyNameDistinctOther = GetInsurerList(3)
End Function

Public Function FindInsurerMedicare() As Long
JustName = 0
InsurerPlanName = ""
FindInsurerMedicare = GetInsurerList(0)
End Function

Public Function FindInsurerByFeeSchedule() As Long
FindInsurerByFeeSchedule = GetInsurerbyFeeSchedule
End Function

Public Function MachInsurers(ins1 As String, ins2 As String) As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
OrderString = "Select * FROM PracticeInsurers as a WHERE a.insurerid = " & ins1 & _
              " and a.InsurerName in (" & _
              "select insurername from PracticeInsurers as b where b.insurerid = " & ins2 & ")"
Set PracticeInsurerTbl = Nothing
Set PracticeInsurerTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticeInsurerTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
MachInsurers = Not PracticeInsurerTbl.EOF

Exit Function
DbErrorHandler:
    ModuleName = "MachInsurers"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
