VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Reporting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Public Function ViewReport(ReportName As String, ReportParameters() As Variant, Optional Documentation As String = "") As Boolean
On Error GoTo lViewReport_Error

ViewReport = False
Dim ReportViewerComWrapper As New comWrapper
Dim ReportViewerArguments As Variant
Dim i As Integer
Dim arguments() As Variant
        
'Create a HashTable to pass Parameters.
ReportViewerComWrapper.Create "System.Collections.Hashtable", emptyArgs
Set ReportViewerArguments = ReportViewerComWrapper.Instance

If ArrayExists(ArrPtr(ReportParameters)) Then
    Dim Parameterargs() As Variant
    For i = 0 To UBound(ReportParameters)
        Parameterargs = ReportParameters(i)
        Call ReportViewerComWrapper.InvokeMethod("Add", Parameterargs)
    Next
    arguments = Array(ReportName, Documentation, ReportViewerArguments)
Else
    arguments = Array(ReportName, Documentation, Nothing)
End If

Set ReportViewerArguments = Nothing
Call ReportViewerComWrapper.Create(ReportViewManagerType, emptyArgs)
Call ReportViewerComWrapper.InvokeMethod("ViewReport", arguments)

ViewReport = True
Exit Function
lViewReport_Error:
    LogError "Reporting.Cls", "ViewReport", Err, Err.Description
End Function

Public Function PrintReport(ReportName As String, ReportParameters() As Variant) As Boolean
On Error GoTo lPrintReport_Error

PrintReport = False
Dim ReportViewerComWrapper As New comWrapper
Dim ReportViewerArguments As Variant
Dim i As Integer
Dim arguments() As Variant
        
'Create a HashTable to pass Parameters.
ReportViewerComWrapper.Create "System.Collections.Hashtable", emptyArgs
Set ReportViewerArguments = ReportViewerComWrapper.Instance

If ArrayExists(ArrPtr(ReportParameters)) Then
    Dim Parameterargs() As Variant
    For i = 0 To UBound(ReportParameters)
        Parameterargs = ReportParameters(i)
        Call ReportViewerComWrapper.InvokeMethod("Add", Parameterargs)
    Next
    arguments = Array(ReportName, ReportViewerArguments)
Else
    arguments = Array(ReportName, Nothing)
End If

Set ReportViewerArguments = Nothing
Call ReportViewerComWrapper.Create(ReportViewManagerType, emptyArgs)
Call ReportViewerComWrapper.InvokeMethod("PrintReport", arguments)

PrintReport = True
Exit Function
lPrintReport_Error:
    LogError "Reporting.Cls", "PrintReport", Err, Err.Description
End Function

