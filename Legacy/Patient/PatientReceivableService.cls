VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientReceivableService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Receivable Service Object Module
Option Explicit
Public ItemId As Long
Public Invoice As String
Public Service As String
Public ServiceModifier As String
Public ServiceCharge As Single
Public ServiceFeeCharge As Single
Public ServiceQuantity As Single
Public ServiceDate As String
Public ServiceLinkedDiag As String
Public ServiceTOS As String
Public ServicePOS As String
Public ServiceOrder As Long
Public ServiceOrderDoc As Boolean
Public ServiceConsultOn As Boolean
Public ServiceECode As Long
Public ServiceCreateDate As String
Public ServiceStatus As String
Public ServiceExternalRefInfo As String

Private ModuleName As String
Private PatientReceivableServiceTotal As Long
Private PatientReceivableServiceTbl As ADODB.Recordset
Private PatientReceivableServiceNew As Boolean

Private Sub Class_Initialize()
Set PatientReceivableServiceTbl = CreateAdoRecordset
Call InitPatientReceivableService
End Sub

Private Sub Class_Terminate()
Call InitPatientReceivableService
Set PatientReceivableServiceTbl = Nothing
End Sub

Private Sub InitPatientReceivableService()
PatientReceivableServiceNew = True
Invoice = ""
Service = ""
ServiceModifier = ""
ServiceCharge = 0
ServiceFeeCharge = 0
ServiceQuantity = 0
ServiceDate = ""
ServiceTOS = ""
ServicePOS = ""
ServiceOrder = 0
ServiceOrderDoc = False
ServiceConsultOn = False
ServiceLinkedDiag = ""
ServiceECode = 0
ServiceCreateDate = ""
ServiceStatus = "A"
End Sub

Private Sub LoadPatientReceivableService()
PatientReceivableServiceNew = False
If (PatientReceivableServiceTbl("ItemId") <> "") Then
    ItemId = PatientReceivableServiceTbl("ItemId")
Else
    ItemId = 0
End If
If (PatientReceivableServiceTbl("Invoice") <> "") Then
    Invoice = PatientReceivableServiceTbl("Invoice")
Else
    Invoice = ""
End If
If (PatientReceivableServiceTbl("Service") <> "") Then
    Service = PatientReceivableServiceTbl("Service")
Else
    Service = ""
End If
If (PatientReceivableServiceTbl("ServiceDate") <> "") Then
    ServiceDate = PatientReceivableServiceTbl("ServiceDate")
Else
    ServiceDate = ""
End If
If (PatientReceivableServiceTbl("Modifier") <> "") Then
    ServiceModifier = PatientReceivableServiceTbl("Modifier")
Else
    ServiceModifier = ""
End If
If (PatientReceivableServiceTbl("Charge") <> "") Then
    ServiceCharge = PatientReceivableServiceTbl("Charge")
Else
    ServiceCharge = 0
End If
If (PatientReceivableServiceTbl("FeeCharge") <> "") Then
    ServiceFeeCharge = PatientReceivableServiceTbl("FeeCharge")
Else
    ServiceFeeCharge = 0
End If
If (PatientReceivableServiceTbl("Quantity") <> "") Then
    ServiceQuantity = PatientReceivableServiceTbl("Quantity")
Else
    ServiceQuantity = 0
End If
If (PatientReceivableServiceTbl("TypeOfService") <> "") Then
    ServiceTOS = PatientReceivableServiceTbl("TypeOfService")
Else
    ServiceTOS = ""
End If
If (PatientReceivableServiceTbl("PlaceOfService") <> "") Then
    ServicePOS = PatientReceivableServiceTbl("PlaceOfService")
Else
    ServicePOS = ""
End If
If (PatientReceivableServiceTbl("ItemOrder") <> "") Then
    ServiceOrder = PatientReceivableServiceTbl("ItemOrder")
Else
    ServiceOrder = 0
End If
If (PatientReceivableServiceTbl("OrderDoc") <> "") Then
    If (PatientReceivableServiceTbl("OrderDoc") = "T") Then
        ServiceOrderDoc = True
    Else
        ServiceOrderDoc = False
    End If
Else
    ServiceOrderDoc = False
End If
If (PatientReceivableServiceTbl("ConsultOn") <> "") Then
    If (PatientReceivableServiceTbl("ConsultOn") = "T") Then
        ServiceConsultOn = True
    Else
        ServiceConsultOn = False
    End If
Else
    ServiceConsultOn = False
End If
If (PatientReceivableServiceTbl("LinkedDiag") <> "") Then
    ServiceLinkedDiag = PatientReceivableServiceTbl("LinkedDiag")
Else
    ServiceLinkedDiag = ""
End If
If (PatientReceivableServiceTbl("ECode") <> "") Then
    ServiceECode = PatientReceivableServiceTbl("ECode")
Else
    ServiceECode = 0
End If
If (PatientReceivableServiceTbl("PostDate") <> "") Then
    ServiceCreateDate = PatientReceivableServiceTbl("PostDate")
Else
    ServiceCreateDate = ""
End If
If (PatientReceivableServiceTbl("Status") <> "") Then
    ServiceStatus = PatientReceivableServiceTbl("Status")
Else
    ServiceStatus = ""
End If
If (PatientReceivableServiceTbl("ExternalRefInfo") <> "") Then
    ServiceExternalRefInfo = PatientReceivableServiceTbl("ExternalRefInfo")
Else
    ServiceExternalRefInfo = ""
End If
End Sub

Private Function GetPatientReceivableTotalCharge() As Single
On Error GoTo DbErrorHandler
Dim Amt As Single
Dim OrderString As String
GetPatientReceivableTotalCharge = 0
If (Trim(Invoice) = "") Then
    Exit Function
End If
OrderString = "SELECT Charge, Status, Quantity FROM PatientReceivableServices WHERE Invoice = '" + Trim(Invoice) + "' "
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
PatientReceivableServiceTbl.MoveFirst
Do Until (PatientReceivableServiceTbl.EOF)
    If (PatientReceivableServiceTbl("Charge") <> "") And (PatientReceivableServiceTbl("Status") <> "X") Then
        If (PatientReceivableServiceTbl("Quantity") <> "") Then
            If (PatientReceivableServiceTbl("Quantity") > 0) Then
                Amt = PatientReceivableServiceTbl("Charge") * PatientReceivableServiceTbl("Quantity")
            Else
                Amt = PatientReceivableServiceTbl("Charge")
            End If
        Else
            Amt = PatientReceivableServiceTbl("Charge")
        End If
        GetPatientReceivableTotalCharge = GetPatientReceivableTotalCharge + Amt
    End If
    PatientReceivableServiceTbl.MoveNext
Loop
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableTotalCharge"
    GetPatientReceivableTotalCharge = 0
    ItemId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReceivableFeeCharge() As Single
On Error GoTo DbErrorHandler
Dim Amt As Single
Dim OrderString As String
GetPatientReceivableFeeCharge = 0
If (Trim(Invoice) = "") Then
    Exit Function
End If
OrderString = "SELECT FeeCharge, Status, Quantity, Charge FROM PatientReceivableServices WHERE Invoice = '" + Trim(Invoice) + "' "
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
PatientReceivableServiceTbl.MoveFirst
Do Until (PatientReceivableServiceTbl.EOF)
    If (PatientReceivableServiceTbl("FeeCharge") <> "") And (PatientReceivableServiceTbl("Status") <> "X") Then
    'calcallowed old
'        If (PatientReceivableServiceTbl("Quantity") <> "") Then
'            If (PatientReceivableServiceTbl("Quantity") > 0) Then
'                Amt = PatientReceivableServiceTbl("FeeCharge") * PatientReceivableServiceTbl("Quantity")
'            Else
'                Amt = PatientReceivableServiceTbl("FeeCharge")
'            End If
'        Else
'            Amt = PatientReceivableServiceTbl("FeeCharge")
'        End If
''
'' ugly but necessary ... if charge is less than fee charge then the practice
'' is charging less than the allowed ... use the charge then
''
'        If (PatientReceivableServiceTbl("FeeCharge") > (PatientReceivableServiceTbl("Charge"))) And (PatientReceivableServiceTbl("Status") <> "X") Then
'            If (PatientReceivableServiceTbl("Quantity") <> "") Then
'                If (PatientReceivableServiceTbl("Quantity") > 0) Then
'                    Amt = PatientReceivableServiceTbl("Charge") * PatientReceivableServiceTbl("Quantity")
'                Else
'                    Amt = PatientReceivableServiceTbl("Charge")
'                End If
'            Else
'                Amt = PatientReceivableServiceTbl("Charge")
'            End If
'        End If
    'calcallowed new
        Amt = PatientReceivableServiceTbl("FeeCharge")
        GetPatientReceivableFeeCharge = GetPatientReceivableFeeCharge + Amt
    End If
    PatientReceivableServiceTbl.MoveNext
Loop
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableFeeCharge"
    GetPatientReceivableFeeCharge = 0
    ItemId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReceivableServices() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientReceivableServices = -1
PatientReceivableServiceTotal = 0
OrderString = "Select * FROM PatientReceivableServices WHERE Invoice = '" + Trim(Invoice) + "' "
If (Trim(Service) <> "") Then
    OrderString = OrderString + "And Service = '" + Trim(Service) + "' "
End If
OrderString = OrderString + "And Status = 'A' ORDER BY ItemOrder ASC, ItemId DESC "
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReceivableServiceTbl.EOF) Then
    GetPatientReceivableServices = -1
Else
    PatientReceivableServiceTbl.MoveLast
    PatientReceivableServiceTotal = PatientReceivableServiceTbl.RecordCount
    GetPatientReceivableServices = PatientReceivableServiceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableServices"
    GetPatientReceivableServices = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReceivableServiceAny() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientReceivableServiceAny = -1
PatientReceivableServiceTotal = 0
If (Trim(Service) <> "") And (Trim(Invoice) <> "") Then
    OrderString = "Select * FROM PatientReceivableServices WHERE Invoice = '" + Trim(Invoice) + "' "
    OrderString = OrderString + "And (Service Like '" + Trim(Service) + "%') "
    OrderString = OrderString + "And Status = 'A' ORDER BY ItemOrder ASC, ItemId DESC "
    Set PatientReceivableServiceTbl = Nothing
    Set PatientReceivableServiceTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (PatientReceivableServiceTbl.EOF) Then
        GetPatientReceivableServiceAny = -1
    Else
        PatientReceivableServiceTbl.MoveLast
        PatientReceivableServiceTotal = PatientReceivableServiceTbl.RecordCount
        GetPatientReceivableServiceAny = PatientReceivableServiceTbl.RecordCount
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableServiceAny"
    GetPatientReceivableServiceAny = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReceivableServicesAscending() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientReceivableServicesAscending = -1
PatientReceivableServiceTotal = 0
OrderString = "Select * FROM PatientReceivableServices WHERE Invoice = '" + Trim(Invoice) + "' "
If (Trim(Service) <> "") Then
    OrderString = OrderString + "And Service = '" + Trim(Service) + "' "
End If
OrderString = OrderString + "And (Status <> 'X') ORDER BY ItemOrder ASC, ItemId ASC "
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReceivableServiceTbl.EOF) Then
    GetPatientReceivableServicesAscending = -1
Else
    PatientReceivableServiceTbl.MoveLast
    PatientReceivableServiceTotal = PatientReceivableServiceTbl.RecordCount
    GetPatientReceivableServicesAscending = PatientReceivableServiceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableServicesAscending"
    GetPatientReceivableServicesAscending = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReceivableServicesActive() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientReceivableServicesActive = -1
PatientReceivableServiceTotal = 0
OrderString = "Select * FROM PatientReceivableServices WHERE Status = 'A' ORDER BY Invoice ASC, ItemOrder ASC "
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReceivableServiceTbl.EOF) Then
    GetPatientReceivableServicesActive = -1
Else
    PatientReceivableServiceTbl.MoveLast
    PatientReceivableServiceTotal = PatientReceivableServiceTbl.RecordCount
    GetPatientReceivableServicesActive = PatientReceivableServiceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableServicesActive"
    GetPatientReceivableServicesActive = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function


Public Function ActivePatientReceivableServices(srvc As String) As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
OrderString = "Select * FROM PatientReceivableServices WHERE Status = 'A' " & _
              " and service = '" & srvc & "'"
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
ActivePatientReceivableServices = Not PatientReceivableServiceTbl.EOF
Exit Function
DbErrorHandler:
    ModuleName = "ActivePatientReceivableServices"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetPatientReceivableService() As Boolean
On Error GoTo DbErrorHandler
GetPatientReceivableService = True
Dim OrderString As String, WhereString As String
OrderString = "SELECT * FROM PatientReceivableServices WHERE ItemId=" + Str(ItemId)
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReceivableServiceTbl.EOF) Then
    Call InitPatientReceivableService
Else
    Call LoadPatientReceivableService
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableService"
    GetPatientReceivableService = False
    ItemId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetServiceByRcvId(RcvId As Long) As Boolean
On Error GoTo DbErrorHandler

Dim OrderString As String
OrderString = " select prs.* from PatientReceivableServices PRS " & _
              " inner join PatientReceivables PR on PR.Invoice = PRS.Invoice " & _
              " where PR.ReceivableId = " & RcvId
        
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReceivableServiceTbl.EOF) Then
    Call InitPatientReceivableService
Else
    PatientReceivableServiceTotal = PatientReceivableServiceTbl.RecordCount
    Call LoadPatientReceivableService
End If
GetServiceByRcvId = Not PatientReceivableServiceTbl.EOF
Exit Function

DbErrorHandler:
    ModuleName = "GetServiceByRcvId"
    GetServiceByRcvId = False
    ItemId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientReceivableService() As Boolean
On Error GoTo DbErrorHandler
PutPatientReceivableService = True
If (PatientReceivableServiceNew) Then
    If (Trim(Invoice) = "") Then
        PutPatientReceivableService = False
    End If
    PatientReceivableServiceTbl.AddNew
    PatientReceivableServiceTbl("Invoice") = Invoice
End If
PatientReceivableServiceTbl("Service") = UCase(Trim(Service))
PatientReceivableServiceTbl("Modifier") = Trim(ServiceModifier)
PatientReceivableServiceTbl("Charge") = ServiceCharge
PatientReceivableServiceTbl("FeeCharge") = ServiceFeeCharge
PatientReceivableServiceTbl("Quantity") = ServiceQuantity
PatientReceivableServiceTbl("ServiceDate") = Trim(ServiceDate)
PatientReceivableServiceTbl("TypeOfService") = Trim(ServiceTOS)
PatientReceivableServiceTbl("PlaceOfService") = Trim(ServicePOS)
PatientReceivableServiceTbl("LinkedDiag") = Trim(ServiceLinkedDiag)
PatientReceivableServiceTbl("ItemOrder") = ServiceOrder
PatientReceivableServiceTbl("OrderDoc") = "F"
If (ServiceOrderDoc) Then
    PatientReceivableServiceTbl("OrderDoc") = "T"
End If
PatientReceivableServiceTbl("ConsultOn") = "F"
If (ServiceConsultOn) Then
    PatientReceivableServiceTbl("ConsultOn") = "T"
End If
PatientReceivableServiceTbl("ECode") = ServiceECode
PatientReceivableServiceTbl("PostDate") = ServiceCreateDate
PatientReceivableServiceTbl("Status") = ServiceStatus
PatientReceivableServiceTbl("ExternalRefInfo") = UCase(Trim(ServiceExternalRefInfo))
PatientReceivableServiceTbl.Update
If (PatientReceivableServiceNew) Then
    ItemId = GetJustAddedRecord
    PatientReceivableServiceNew = False
End If

Exit Function
DbErrorHandler:
    ModuleName = "PutPatientReceivableService"
    PutPatientReceivableService = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function CleanUpServices(InvId As String) As Boolean
Dim RS(1) As Recordset
Set RS(0) = GetRS(" select prs.itemid from PatientReceivableServices PRS " & _
               " inner join PracticeServices PS on PS.Code = PRS.Service " & _
               " where PRS.Invoice = '" & InvId & _
               "' and PS.POS like '98%' ")

Do Until RS(0).EOF
    Set RS(1) = GetRS(" delete from PatientReceivableServices where itemid = " & RS(0)(0))
    RS(0).MoveNext
Loop
End Function


Private Function KillServices() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillServices = True
OrderString = "SELECT * FROM PatientReceivableServices "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set PatientReceivableServiceTbl = Nothing
Set PatientReceivableServiceTbl = CreateAdoRecordset
Call PatientReceivableServiceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PatientReceivableServiceTbl.EOF) Then
    PatientReceivableServiceTbl.MoveFirst
    While Not (PatientReceivableServiceTbl.EOF)
        PatientReceivableServiceTbl.Delete
        PatientReceivableServiceTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillServices"
    KillServices = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPatientReceivableServices > 0) Then
    If (SelectPatientReceivableService(1)) Then
        GetJustAddedRecord = PatientReceivableServiceTbl("ItemId")
    End If
End If
End Function

Public Function ApplyPatientReceivableService() As Boolean
ApplyPatientReceivableService = PutPatientReceivableService
End Function

Public Function KillService() As Boolean
KillService = KillServices
End Function

Public Function DeletePatientReceivableService() As Boolean
ServiceStatus = "D"
DeletePatientReceivableService = PutPatientReceivableService
End Function

Public Function RetrievePatientReceivableService() As Boolean
RetrievePatientReceivableService = GetPatientReceivableService
End Function

Public Function RetrievePatientReceivableTotalCharge() As Single
RetrievePatientReceivableTotalCharge = GetPatientReceivableTotalCharge
End Function

Public Function RetrievePatientReceivableFeeCharge() As Single
RetrievePatientReceivableFeeCharge = GetPatientReceivableFeeCharge
End Function

Public Function SelectPatientReceivableService(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectPatientReceivableService = False
If (PatientReceivableServiceTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientReceivableServiceTotal) Then
    Exit Function
End If
PatientReceivableServiceTbl.MoveFirst
If (ListItem > 1) Then
    PatientReceivableServiceTbl.Move ListItem - 1
End If
SelectPatientReceivableService = True
Call LoadPatientReceivableService
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientReceivableService"
    SelectPatientReceivableService = False
    ItemId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPatientReceivableServiceAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
SelectPatientReceivableServiceAggregate = False
If (PatientReceivableServiceTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientReceivableServiceTotal) Then
    Exit Function
End If
PatientReceivableServiceTbl.MoveFirst
If (ListItem > 1) Then
    PatientReceivableServiceTbl.Move ListItem - 1
End If
SelectPatientReceivableServiceAggregate = True
Call LoadPatientReceivableService
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientReceivableServiceAggregate"
    SelectPatientReceivableServiceAggregate = False
    ItemId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientReceivableService() As Long
FindPatientReceivableService = GetPatientReceivableServices()
End Function

Public Function FindPatientReceivableServiceAscending() As Long
FindPatientReceivableServiceAscending = GetPatientReceivableServicesAscending()
End Function

Public Function FindPatientReceivableServiceActive() As Long
FindPatientReceivableServiceActive = GetPatientReceivableServicesActive()
End Function

Public Function FindPatientReceivableServiceAny() As Long
FindPatientReceivableServiceAny = GetPatientReceivableServiceAny()
End Function
