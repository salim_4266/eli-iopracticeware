VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeMedicaidLocations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Practice Medicaid Locations Object Module
Option Explicit
Public MedId As Long
Public MedDoctorId As Long
Public MedLocId As Long
Public MedLocCode As String

Private ModuleName As String
Private MedicaidLocTotal As Long
Private MedicaidLocTbl As ADODB.Recordset
Private MedicaidLocNew As Boolean

Private Sub Class_Initialize()
Set MedicaidLocTbl = CreateAdoRecordset
Call InitMedLoc
End Sub

Private Sub Class_Terminate()
Call InitMedLoc
Set MedicaidLocTbl = Nothing
End Sub

Private Sub InitMedLoc()
MedicaidLocNew = True
MedDoctorId = 0
MedLocId = 0
MedLocCode = ""
End Sub

Private Sub LoadMedLoc()
MedicaidLocNew = False
MedId = MedicaidLocTbl("MedId")
If (MedicaidLocTbl("DoctorId") <> "") Then
    MedDoctorId = MedicaidLocTbl("DoctorId")
Else
    MedDoctorId = 0
End If
If (MedicaidLocTbl("LocationId") <> "") Then
    MedLocId = MedicaidLocTbl("LocationId")
Else
    MedLocId = 0
End If
If (MedicaidLocTbl("LocationCode") <> "") Then
    MedLocCode = MedicaidLocTbl("LocationCode")
Else
    MedLocCode = ""
End If
End Sub

Private Function GetMedLocs() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
MedicaidLocTotal = 0
GetMedLocs = -1
Ref = ""
If (Trim(MedDoctorId) < 1) Then
    Exit Function
End If
OrderString = "Select * FROM MedicaidLocationCodes WHERE DoctorId = " + Trim(Str(MedDoctorId)) + " "
If (MedLocId <> 0) Then
    OrderString = OrderString + "And LocationId = " + Trim(Str(MedLocId)) + " "
End If
OrderString = OrderString + "ORDER BY DoctorId ASC, LocationId ASC "
Set MedicaidLocTbl = Nothing
Set MedicaidLocTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call MedicaidLocTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (MedicaidLocTbl.EOF) Then
    GetMedLocs = -1
    Call InitMedLoc
Else
    MedicaidLocTbl.MoveLast
    MedicaidLocTotal = MedicaidLocTbl.RecordCount
    GetMedLocs = MedicaidLocTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetMedLoc"
    GetMedLocs = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetMedLoc() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetMedLoc = True
OrderString = "SELECT * FROM MedicaidLocationCodes WHERE MedId = " + Trim(Str(MedId)) + " "
Set MedicaidLocTbl = Nothing
Set MedicaidLocTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call MedicaidLocTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (MedicaidLocTbl.EOF) Then
    Call InitMedLoc
Else
    Call LoadMedLoc
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetMedLoc"
    GetMedLoc = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutMedLoc() As Boolean
On Error GoTo DbErrorHandler
PutMedLoc = True
If (MedicaidLocNew) Then
    MedicaidLocTbl.AddNew
End If
MedicaidLocTbl("DoctorId") = MedDoctorId
MedicaidLocTbl("LocationId") = MedLocId
MedicaidLocTbl("LocationCode") = MedLocCode
MedicaidLocTbl.Update
If (MedicaidLocNew) Then
    MedicaidLocNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutMedLoc"
    PutMedLoc = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeMedLoc() As Boolean
On Error GoTo DbErrorHandler
PurgeMedLoc = True
MedicaidLocTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeMedLoc"
    PurgeMedLoc = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyMedLoc() As Boolean
ApplyMedLoc = PutMedLoc
End Function

Public Function DeleteMedLoc() As Boolean
DeleteMedLoc = PurgeMedLoc
End Function

Public Function RetrieveMedLoc() As Boolean
RetrieveMedLoc = GetMedLoc
End Function

Public Function FindMedLoc() As Long
FindMedLoc = GetMedLocs
End Function

Public Function SelectMedLoc(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectMedLoc = False
If (MedicaidLocTbl.EOF) Or (ListItem < 1) Or (ListItem > MedicaidLocTotal) Then
    Exit Function
End If
MedicaidLocTbl.MoveFirst
MedicaidLocTbl.Move ListItem - 1
SelectMedLoc = True
Call LoadMedLoc
Exit Function
DbErrorHandler:
    ModuleName = "SelectMedLoc"
    SelectMedLoc = False
    MedId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

