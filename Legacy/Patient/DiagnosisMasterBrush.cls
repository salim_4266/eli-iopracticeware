VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DiagnosisMasterBrush"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Paint Brush Object Module
Option Explicit
Public BrushId As Long
Public BrushName As String
Public BrushDiagnosis As String
Public BrushLingo As String
Public BrushMinValue As Integer
Public BrushMaxValue As Integer
Public BrushStep As Integer
Public BrushType As String
Public BrushRotateOn As Boolean
Public BrushSizingOn As Boolean
Public BrushScalingOn As Boolean
Public BrushLocateXOn As Integer
Public BrushLocateYOn As Integer
Public BrushFrameWidth As Integer
Public BrushFrameHeight As Integer
Public BrushLeftRight As String
Public BrushTestTrigger As String
Public BrushDefaultColor As String
Public BrushDefaultWidth As String
Public BrushLocationSupport As String

Private ModuleName As String
Private BrushTotal As Integer
Private BrushTbl As ADODB.Recordset
Private BrushNew As Boolean

Private Sub Class_Initialize()
Set BrushTbl = New ADODB.Recordset
BrushTbl.CursorLocation = adUseClient
Call InitBrush
End Sub

Private Sub Class_Terminate()
Call InitBrush
Set BrushTbl = Nothing
End Sub

Private Sub InitBrush()
BrushNew = False
BrushName = ""
BrushDiagnosis = ""
BrushLingo = ""
BrushMinValue = 0
BrushMaxValue = 0
BrushStep = 0
BrushType = "B"
BrushRotateOn = False
BrushSizingOn = False
BrushScalingOn = False
BrushLocateXOn = 0
BrushLocateYOn = 0
BrushFrameWidth = 0
BrushFrameHeight = 0
BrushLeftRight = ""
BrushTestTrigger = ""
BrushDefaultColor = ""
BrushDefaultWidth = ""
BrushLocationSupport = ""
End Sub

Private Sub LoadBrush()
BrushNew = False
BrushId = BrushTbl("BrushId")
If (BrushTbl("BrushName") <> "") Then
    BrushName = BrushTbl("BrushName")
Else
    BrushName = ""
End If
If (BrushTbl("Diagnosis") <> "") Then
    BrushDiagnosis = BrushTbl("Diagnosis")
Else
    BrushDiagnosis = ""
End If
If (BrushTbl("BrushLingo") <> "") Then
    BrushLingo = BrushTbl("BrushLingo")
Else
    BrushLingo = ""
End If
If (BrushTbl("MinValue") <> "") Then
    BrushMinValue = BrushTbl("MinValue")
Else
    BrushMinValue = 0
End If
If (BrushTbl("MaxValue") <> "") Then
    BrushMaxValue = BrushTbl("MaxValue")
Else
    BrushMaxValue = 0
End If
If (BrushTbl("Steps") <> "") Then
    BrushStep = BrushTbl("Steps")
Else
    BrushStep = 0
End If
If (BrushTbl("ImageType") <> "") Then
    BrushType = BrushTbl("ImageType")
Else
    BrushType = "B"
End If
If (BrushTbl("RotateOn") <> "") Then
    If (BrushTbl("RotateOn") = "T") Then
        BrushRotateOn = True
    Else
        BrushRotateOn = False
    End If
Else
    BrushRotateOn = False
End If
If (BrushTbl("SizingOn") <> "") Then
    If (BrushTbl("SizingOn") = "T") Then
        BrushSizingOn = True
    Else
        BrushSizingOn = False
    End If
Else
    BrushSizingOn = False
End If
If (BrushTbl("ScalingOn") <> "") Then
    If (BrushTbl("ScalingOn") = "T") Then
        BrushScalingOn = True
    Else
        BrushScalingOn = False
    End If
Else
    BrushScalingOn = False
End If
If (BrushTbl("LocateXOn") <> "") Then
    BrushLocateXOn = BrushTbl("LocateXOn")
Else
    BrushLocateXOn = -1
End If
If (BrushTbl("LocateYOn") <> "") Then
    BrushLocateYOn = BrushTbl("LocateYOn")
Else
    BrushLocateYOn = -1
End If
If (BrushTbl("FrameWidth") <> "") Then
    BrushFrameWidth = BrushTbl("FrameWidth")
Else
    BrushFrameWidth = 0
End If
If (BrushTbl("FrameHeight") <> "") Then
    BrushFrameHeight = BrushTbl("FrameHeight")
Else
    BrushFrameHeight = 0
End If
If (BrushTbl("LeftRight") <> "") Then
    BrushLeftRight = BrushTbl("LeftRight")
Else
    BrushLeftRight = ""
End If
If (BrushTbl("TestTrigger") <> "") Then
    BrushTestTrigger = BrushTbl("TestTrigger")
Else
    BrushTestTrigger = ""
End If
If (BrushTbl("LocationSupport") <> "") Then
    BrushLocationSupport = BrushTbl("LocationSupport")
Else
    BrushLocationSupport = "T"
End If
If (BrushTbl("DefaultColor") <> "") Then
    BrushDefaultColor = Mid(BrushTbl("DefaultColor"), 1, 1)
    BrushDefaultWidth = Mid(BrushTbl("DefaultColor"), 2, 1)
Else
    BrushDefaultColor = ""
    BrushDefaultWidth = ""
End If
End Sub

Private Function GetBrush()
On Error GoTo DbErrorHandler
Dim OrderString As String
GetBrush = True
If (BrushId < 1) Then
    Call InitBrush
    Exit Function
End If
OrderString = "SELECT * FROM PaintbrushTable WHERE BrushId =" + Trim(Str(BrushId)) + " "
Set BrushTbl = Nothing
Set BrushTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call BrushTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (BrushTbl.EOF) Then
    Call InitBrush
Else
    Call LoadBrush
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetBrush"
    GetBrush = False
    BrushId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllBrush() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
BrushTotal = 0
GetAllBrush = -1
Ref = ""
If (Trim(BrushName) = "") And (Trim(BrushDiagnosis) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM PaintbrushTable WHERE "
If (Len(BrushName) = 1) Then
    OrderString = OrderString + "BrushName >= '" + UCase(Trim(BrushName)) + "' "
    Ref = "And "
ElseIf (Trim(BrushName) <> "") Then
    OrderString = OrderString + "BrushName = '" + UCase(Trim(BrushName)) + "' "
    Ref = "And "
End If
If (Trim(BrushDiagnosis) <> "") Then
    OrderString = OrderString + Ref + "Diagnosis = '" + UCase(Trim(BrushDiagnosis)) + "' "
    Ref = "And "
End If
If (Trim(BrushLeftRight) <> "") Then
    OrderString = OrderString + Ref + "(LeftRight = '" + UCase(Trim(BrushLeftRight)) + "' Or LeftRight = 'B') "
End If
OrderString = OrderString + "ORDER BY BrushName ASC"
Set BrushTbl = Nothing
Set BrushTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call BrushTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (BrushTbl.EOF) Then
    GetAllBrush = -1
Else
    BrushTbl.MoveLast
    BrushTotal = BrushTbl.RecordCount
    GetAllBrush = BrushTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllBrush"
    GetAllBrush = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutBrush()
On Error GoTo DbErrorHandler
PutBrush = True
If (BrushNew) Or (BrushId < 1) Then
    BrushTbl.AddNew
End If
BrushTbl("BrushName") = UCase(Trim(BrushName))
BrushTbl("BrushLingo") = Trim(BrushLingo)
BrushTbl("Diagnosis") = Trim(BrushDiagnosis)
BrushTbl("MaxValue") = BrushMinValue
BrushTbl("MinValue") = BrushMaxValue
BrushTbl("Steps") = BrushStep
BrushTbl("ImageType") = UCase(BrushType)
BrushTbl("RotateOn") = "F"
If (BrushRotateOn) Then
    BrushTbl("RotateOn") = "T"
End If
BrushTbl("SizingOn") = "F"
If (BrushSizingOn) Then
    BrushTbl("SizingOn") = "T"
End If
BrushTbl("ScalingOn") = "F"
If (BrushScalingOn) Then
    BrushTbl("ScalingOn") = "T"
End If
BrushTbl("LocateXOn") = BrushLocateXOn
BrushTbl("LocateYOn") = BrushLocateYOn
BrushTbl("FrameWidth") = BrushFrameWidth
BrushTbl("FrameHeight") = BrushFrameHeight
BrushTbl("LeftRight") = BrushLeftRight
BrushTbl("TestTrigger") = BrushTestTrigger
BrushTbl("DefaultColor") = BrushDefaultColor + BrushDefaultWidth
BrushTbl.Update
If (BrushNew) Then
'    BrushTbl.Move 0, BrushTbl.LastModified
    BrushId = BrushTbl("BrushId")
    BrushNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutBrush"
    PutBrush = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeBrush()
On Error GoTo DbErrorHandler
PurgeBrush = True
If Not (BrushNew) Then
    BrushTbl.Delete
End If
Exit Function
DbErrorHandler:
    PurgeBrush = False
    ModuleName = "PurgeBrush"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectBrush(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectBrush = False
If (BrushTbl.EOF) Or (ListItem < 1) Or (ListItem > BrushTotal) Then
    Exit Function
End If
BrushTbl.MoveFirst
BrushTbl.Move ListItem - 1
SelectBrush = True
Call LoadBrush
Exit Function
DbErrorHandler:
    ModuleName = "SelectBrush"
    BrushId = -256
    SelectBrush = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function DeleteBrush() As Boolean
DeleteBrush = PurgeBrush
End Function

Public Function ApplyBrush() As Boolean
ApplyBrush = PutBrush
End Function

Public Function RetrieveBrush() As Boolean
RetrieveBrush = GetBrush
End Function

Public Function FindBrush() As Long
FindBrush = GetBrush()
End Function

Public Function FindBrushbyName() As Long
FindBrushbyName = GetAllBrush()
End Function

Public Function FindBrushbyDiagnosis() As Long
FindBrushbyDiagnosis = GetAllBrush()
End Function
