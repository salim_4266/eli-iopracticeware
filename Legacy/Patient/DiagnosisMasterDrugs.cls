VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DiagnosisMasterDrugs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Drugs Object Module
Option Explicit
Public DrugId As Long
Public DrugDiagnosisAlias1 As String
Public DrugDiagnosisAlias2 As String
Public DrugDiagnosisAlias3 As String
Public DrugSpecialtyType As String
Public DrugRank As Long
Public DrugName As String
Public DrugGenericName As String
Public DrugCompany As String
Public DrugCompany2 As String
Public DrugDispense1 As String
Public DrugDispense2 As String
Public DrugDispense3 As String
Public DrugDispense4 As String
Public DrugDispense5 As String
Public DrugDispense6 As String
Public DrugDispense7 As String
Public DrugDispense8 As String
Public DrugPackage1 As String
Public DrugPackage2 As String
Public DrugPackage3 As String
Public DrugPackage4 As String
Public DrugPackage5 As String
Public DrugPackage6 As String

' Hidden
Private ModuleName As String
Private DrugTotal As Integer
Private DrugTbl As ADODB.Recordset
Private DrugNew As Boolean

Private Sub Class_Initialize()
Set DrugTbl = New ADODB.Recordset
DrugTbl.CursorLocation = adUseClient
Call InitDrug
End Sub

Private Sub Class_Terminate()
Call InitDrug
Set DrugTbl = Nothing
End Sub

Private Sub InitDrug()
DrugNew = False
DrugDiagnosisAlias1 = ""
DrugDiagnosisAlias2 = ""
DrugDiagnosisAlias3 = ""
DrugSpecialtyType = ""
DrugRank = 0
DrugName = ""
DrugGenericName = ""
DrugCompany = ""
DrugCompany2 = ""
DrugDispense1 = ""
DrugDispense2 = ""
DrugDispense3 = ""
DrugDispense4 = ""
DrugDispense5 = ""
DrugDispense6 = ""
DrugDispense7 = ""
DrugDispense8 = ""
DrugPackage1 = ""
DrugPackage2 = ""
DrugPackage3 = ""
DrugPackage4 = ""
DrugPackage5 = ""
DrugPackage6 = ""
End Sub

Private Sub LoadDrug(IType As Integer)
If (IType = 2) Then
    If (DrugTbl("DrugName") <> "") Then
        DrugName = DrugTbl("DrugName")
    Else
        DrugName = ""
    End If
    DrugId = DrugTbl("DrugId")
    Exit Sub
End If
If (IType = 1) Then
    If (DrugTbl("ICDAlias1") <> "") Then
        DrugDiagnosisAlias1 = DrugTbl("ICDAlias1")
    Else
        DrugDiagnosisAlias1 = ""
    End If
'    DrugId = DrugTbl("DrugId")
    Exit Sub
End If
If (IType = 0) Then
    DrugId = DrugTbl("DrugId")
    If (DrugTbl("ICDAlias1") <> "") Then
        DrugDiagnosisAlias1 = DrugTbl("ICDAlias1")
    Else
        DrugDiagnosisAlias1 = ""
    End If
    If (DrugTbl("ICDAlias2") <> "") Then
        DrugDiagnosisAlias2 = DrugTbl("ICDAlias2")
    Else
        DrugDiagnosisAlias2 = ""
    End If
    If (DrugTbl("ICDAlias3") <> "") Then
        DrugDiagnosisAlias3 = DrugTbl("ICDAlias3")
    Else
        DrugDiagnosisAlias3 = ""
    End If
    If (DrugTbl("ICDAlias4") <> "") Then
        DrugSpecialtyType = DrugTbl("ICDAlias4")
    Else
        DrugSpecialtyType = ""
    End If
    If (DrugTbl("Rank") <> "") Then
        DrugRank = DrugTbl("Rank")
    Else
        DrugRank = 0
    End If
    If (DrugTbl("DrugName") <> "") Then
        DrugName = DrugTbl("DrugName")
    Else
        DrugName = ""
    End If
    If (DrugTbl("GenericName") <> "") Then
        DrugGenericName = DrugTbl("GenericName")
    Else
        DrugGenericName = ""
    End If
    If (DrugTbl("Company") <> "") Then
        DrugCompany = DrugTbl("Company")
    Else
        DrugCompany = ""
    End If
    If (DrugTbl("Company2") <> "") Then
        DrugCompany2 = DrugTbl("Company2")
    Else
        DrugCompany2 = ""
    End If
    If (DrugTbl("Dispense1") <> "") Then
        DrugDispense1 = DrugTbl("Dispense1")
    Else
        DrugDispense1 = ""
    End If
    If (DrugTbl("Dispense2") <> "") Then
        DrugDispense2 = DrugTbl("Dispense2")
    Else
        DrugDispense2 = ""
    End If
    If (DrugTbl("Dispense3") <> "") Then
        DrugDispense3 = DrugTbl("Dispense3")
    Else
        DrugDispense3 = ""
    End If
    If (DrugTbl("Dispense4") <> "") Then
        DrugDispense4 = DrugTbl("Dispense4")
    Else
        DrugDispense4 = ""
    End If
    If (DrugTbl("Dispense5") <> "") Then
        DrugDispense5 = DrugTbl("Dispense5")
    Else
        DrugDispense5 = ""
    End If
    If (DrugTbl("Dispense6") <> "") Then
        DrugDispense6 = DrugTbl("Dispense6")
    Else
        DrugDispense6 = ""
    End If
    If (DrugTbl("Dispense7") <> "") Then
        DrugDispense7 = DrugTbl("Dispense7")
    Else
        DrugDispense7 = ""
    End If
    If (DrugTbl("Dispense8") <> "") Then
        DrugDispense8 = DrugTbl("Dispense8")
    Else
        DrugDispense8 = ""
    End If
    If (DrugTbl("Package1") <> "") Then
        DrugPackage1 = DrugTbl("Package1")
    Else
        DrugPackage1 = ""
    End If
    If (DrugTbl("Package2") <> "") Then
        DrugPackage2 = DrugTbl("Package2")
    Else
        DrugPackage2 = ""
    End If
    If (DrugTbl("Package3") <> "") Then
        DrugPackage3 = DrugTbl("Package3")
    Else
        DrugPackage3 = ""
    End If
    If (DrugTbl("Package4") <> "") Then
        DrugPackage4 = DrugTbl("Package4")
    Else
        DrugPackage4 = ""
    End If
    If (DrugTbl("Package5") <> "") Then
        DrugPackage5 = DrugTbl("Package5")
    Else
        DrugPackage5 = ""
    End If
    If (DrugTbl("Package6") <> "") Then
        DrugPackage6 = DrugTbl("Package6")
    Else
        DrugPackage6 = ""
    End If
End If
End Sub

Private Function GetDrug()
On Error GoTo DbErrorHandler
Dim OrderString As String
GetDrug = True
OrderString = "SELECT * FROM DrugsTable WHERE DrugId =" + Trim(Str(DrugId)) + " "
Set DrugTbl = Nothing
Set DrugTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call DrugTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (DrugTbl.EOF) Then
    Call InitDrug
    DrugNew = True
Else
    Call LoadDrug(0)
    DrugNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDrug"
    GetDrug = False
    DrugId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllDrug() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
Dim SortBy As String
Dim Ref As String
Ref = ""
SortBy = ""
DrugTotal = 0
GetAllDrug = -1
If (Trim(DrugDiagnosisAlias1) = "") And (Trim(DrugDiagnosisAlias2) = "") And (Trim(DrugDiagnosisAlias3) = "") Then
    Exit Function
End If
OrderString = "SELECT * FROM DrugsTable WHERE "
If (Trim(DrugDiagnosisAlias1) <> "") Then
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias1 >= '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
        Ref = "Or "
    End If
    SortBy = SortBy + "ICDAlias1 ASC, "
End If
If (Trim(DrugDiagnosisAlias2) <> "") Then
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias2 >= '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Ref = "Or "
    Else
        OrderString = OrderString + Ref + "ICDAlias2 >= '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
    End If
    SortBy = SortBy + "ICDAlias2 ASC, "
End If
If (Trim(DrugDiagnosisAlias3) <> "") Then
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias3 >= '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Ref = "Or "
    Else
        OrderString = OrderString + Ref + "ICDAlias3 >= '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
    End If
    SortBy = SortBy + "ICDAlias3 ASC, "
End If
If (DrugRank > 0) Then
    OrderString = OrderString + "And Rank =" + Str(DrugRank) + " "
ElseIf (DrugRank = -1) Then
    OrderString = OrderString + "And Rank = 0 "
End If
OrderString = OrderString + "ORDER BY " + SortBy + "Rank ASC "
Set DrugTbl = Nothing
Set DrugTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call DrugTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (DrugTbl.EOF) Then
    GetAllDrug = -1
Else
    DrugTbl.MoveLast
    DrugTotal = DrugTbl.RecordCount
    GetAllDrug = DrugTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllDrug"
    GetAllDrug = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetDrugbyName(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OneOn As Boolean
Dim Filter As String
Dim Ref As String
Dim SortBy As String
Dim OrderString As String
Ref = ""
OneOn = False
SortBy = ""
DrugTotal = 0
Filter = "="
If (IType = 1) Then
    Filter = ">="
End If
GetDrugbyName = -1
OrderString = "SELECT * FROM DrugsTable WHERE "
If (Trim(DrugName) <> "") Then
    If (Len(Trim(DrugName)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "DrugName " + Filter + " '" + UCase(Trim(DrugName)) + "' "
        Ref = "And "
    Else
        OrderString = OrderString + "And DrugName " + Filter + " '" + UCase(Trim(DrugName)) + "' "
    End If
    SortBy = "DrugName ASC, "
End If
If (Trim(DrugGenericName) <> "") Then
    If (Len(Trim(DrugGenericName)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "DrugGenericName " + Filter + " '" + UCase(Trim(DrugGenericName)) + "' "
        Ref = "And "
    Else
        OrderString = OrderString + "And DrugGenericName " + Filter + " '" + UCase(Trim(DrugGenericName)) + "' "
    End If
    SortBy = SortBy + "DrugGenericName ASC, "
End If
If (Trim(DrugCompany) <> "") Then
    If (Len(Trim(DrugCompany)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "DrugCompany " + Filter + " '" + UCase(Trim(DrugCompany)) + "' "
        Ref = "And "
    Else
        OrderString = OrderString + "And DrugCompany " + Filter + " '" + UCase(Trim(DrugCompany)) + "' "
    End If
    SortBy = SortBy + "DrugCompany ASC, "
End If
If (Trim(DrugDiagnosisAlias1) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias1)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias1 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
        Ref = "Or "
        OneOn = True
    Else
        OrderString = OrderString + "And ICDAlias1 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
    End If
    OneOn = True
    SortBy = SortBy + "ICDAlias1 ASC, "
End If
If (Trim(DrugDiagnosisAlias2) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias2)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Ref = "Or "
    Else
        If (OneOn) Then
            OrderString = OrderString + "Or ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Else
            OrderString = OrderString + "And ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        End If
    End If
    OneOn = True
    SortBy = SortBy + "ICDAlias2 ASC, "
End If
If (Trim(DrugDiagnosisAlias3) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias3)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Ref = "And "
    Else
        If (OneOn) Then
            OrderString = OrderString + "Or ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Else
            OrderString = OrderString + "And ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        End If
    End If
    OneOn = True
    SortBy = SortBy + "ICDAlias3 ASC, "
End If
If (DrugRank > 0) Then
    If (Trim(Ref) <> "") Then
        OrderString = OrderString + "And Rank =" + Str(DrugRank) + " "
    Else
        OrderString = OrderString + Ref + "Rank =" + Str(DrugRank) + " "
    End If
ElseIf (DrugRank = -1) Then
    If (Trim(Ref) <> "") Then
        OrderString = OrderString + "And Rank = 0 "
    Else
        OrderString = OrderString + Ref + "Rank = 0 "
    End If
End If
If (InStrPS(SortBy, "DrugName") = 0) Then
    SortBy = "DrugName ASC, " + SortBy
End If
OrderString = OrderString + "ORDER BY " + SortBy + "Rank ASC "
Set DrugTbl = Nothing
Set DrugTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call DrugTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (DrugTbl.EOF) Then
    GetDrugbyName = -1
Else
    DrugTbl.MoveLast
    DrugTotal = DrugTbl.RecordCount
    GetDrugbyName = DrugTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugbyName"
    GetDrugbyName = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetDrugByPartName(PartName As String) As Recordset
On Error GoTo DbErrorHandler
Dim SQL As String
SQL = "SELECT * FROM Drug WHERE " & _
      "DisplayName like '" & PartName & "%' " & _
      "ORDER BY DisplayName "

Set GetDrugByPartName = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugByPartName"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
Public Function GetDrugByFamily(Family As String) As Recordset
'On Error GoTo DbErrorHandler
Dim SQL As String
SQL = "SELECT  * from Drug WHERE " & _
      "Family1 = '" & Family & "' or " & _
      "Family2 = '" & Family & "' " & _
      "ORDER BY DisplayName "

Set GetDrugByFamily = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugbyFamily"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetDrugbyNameAny(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OneOn As Boolean
Dim Filter As String
Dim Ref As String
Dim SortBy As String
Dim OrderString As String
Ref = ""
OneOn = False
SortBy = ""
DrugTotal = 0
Filter = ">="
GetDrugbyNameAny = -1
OrderString = "SELECT * FROM DrugsTable WHERE "
If (Trim(DrugName) <> "") Then
    If (Len(Trim(DrugName)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "DrugName " + Filter + " '" + Trim(DrugName) + "' "
        Ref = "Or "
    Else
        OrderString = OrderString + "Or DrugName " + Filter + " '" + UCase(Trim(DrugName)) + "' "
    End If
    SortBy = "DrugName ASC, "
End If
If (Trim(DrugGenericName) <> "") Then
    If (Len(Trim(DrugGenericName)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "GenericName " + Filter + " '" + Trim(DrugGenericName) + "' "
        Ref = "Or "
    Else
        OrderString = OrderString + "Or GenericName " + Filter + " '" + Trim(DrugGenericName) + "' "
    End If
    SortBy = SortBy + "GenericName ASC, "
End If
If (Trim(DrugDiagnosisAlias1) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias1)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias1 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
        Ref = "Or "
        OneOn = True
    Else
        OrderString = OrderString + "And ICDAlias1 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
    End If
    OneOn = True
    SortBy = SortBy + "ICDAlias1 ASC, "
End If
If (Trim(DrugDiagnosisAlias2) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias2)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Ref = "Or "
    Else
        If (OneOn) Then
            OrderString = OrderString + "Or ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Else
            OrderString = OrderString + "And ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        End If
    End If
    OneOn = True
    SortBy = SortBy + "ICDAlias2 ASC, "
End If
If (Trim(DrugDiagnosisAlias3) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias3)) <= 1) Then
        Filter = ">="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Ref = "And "
    Else
        If (OneOn) Then
            OrderString = OrderString + "Or ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Else
            OrderString = OrderString + "And ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        End If
    End If
    OneOn = True
    SortBy = SortBy + "ICDAlias3 ASC, "
End If
If (DrugRank > 0) Then
    If (Trim(Ref) <> "") Then
        OrderString = OrderString + "And Rank =" + Str(DrugRank) + " "
    Else
        OrderString = OrderString + Ref + "Rank =" + Str(DrugRank) + " "
    End If
ElseIf (DrugRank = -1) Then
    If (Trim(Ref) <> "") Then
        OrderString = OrderString + "And Rank = 0 "
    Else
        OrderString = OrderString + Ref + "Rank = 0 "
    End If
End If
If (InStrPS(SortBy, "DrugName") = 0) Then
    SortBy = "DrugName ASC, " + SortBy
End If
OrderString = OrderString + "ORDER BY " + SortBy + "Rank ASC "
Set DrugTbl = Nothing
Set DrugTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call DrugTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (DrugTbl.EOF) Then
    GetDrugbyNameAny = -1
Else
    DrugTbl.MoveLast
    DrugTotal = DrugTbl.RecordCount
    GetDrugbyNameAny = DrugTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugbyNameAny"
    GetDrugbyNameAny = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetDrugbyNameDistinct() As Long
On Error GoTo DbErrorHandler
Dim OneOn As Boolean
Dim Filter As String
Dim Ref As String
Dim SortBy As String
Dim OrderString As String
Ref = ""
OneOn = False
SortBy = ""
DrugTotal = 0
Filter = "="
GetDrugbyNameDistinct = -1
OrderString = "SELECT DISTINCT DrugName, DrugId, Rank, ICDAlias1, ICDAlias2, ICDAlias3 FROM DrugsTable WHERE "
If (Trim(DrugName) <> "") Then
    If (Len(Trim(DrugName)) = 1) Then
        Filter = ">="
    Else
        Filter = "="
    End If
    OrderString = OrderString + "DrugName " + Filter + " '" + UCase(Trim(DrugName)) + "' "
    Ref = "And "
End If
If (Trim(DrugDiagnosisAlias1) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias1)) = 1) Then
        Filter = ">="
    Else
        Filter = "="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias1 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
        Ref = "Or "
        OneOn = True
    Else
        OrderString = OrderString + "And ICDAlias1 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
    End If
    OneOn = True
End If
If (Trim(DrugDiagnosisAlias2) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias2)) = 1) Then
        Filter = ">="
    Else
        Filter = "="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Ref = "Or "
    Else
        If (OneOn) Then
            OrderString = OrderString + "Or ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        Else
            OrderString = OrderString + "And ICDAlias2 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias2)) + "' "
        End If
    End If
    OneOn = True
End If
If (Trim(DrugDiagnosisAlias3) <> "") Then
    If (Len(Trim(DrugDiagnosisAlias3)) = 1) Then
        Filter = ">="
    Else
        Filter = "="
    End If
    If (Ref = "") Then
        OrderString = OrderString + "ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Ref = "And "
    Else
        If (OneOn) Then
            OrderString = OrderString + "Or ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        Else
            OrderString = OrderString + "And ICDAlias3 " + Filter + " '" + UCase(Trim(DrugDiagnosisAlias3)) + "' "
        End If
    End If
    OneOn = True
End If
SortBy = "Rank ASC "
OrderString = OrderString + "ORDER BY " + SortBy
Set DrugTbl = Nothing
Set DrugTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call DrugTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (DrugTbl.EOF) Then
    GetDrugbyNameDistinct = -1
Else
    DrugTbl.MoveLast
    DrugTotal = DrugTbl.RecordCount
    GetDrugbyNameDistinct = DrugTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugbyNameDistinct"
    GetDrugbyNameDistinct = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAlias() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetAlias = -1
If (Trim(DrugDiagnosisAlias1) = "") Then
    DrugDiagnosisAlias1 = Chr(1)
End If
OrderString = "SELECT DISTINCT ICDAlias1 FROM DrugsTable WHERE " _
            + "ICDAlias1 >= '" + UCase(Trim(DrugDiagnosisAlias1)) + "' "
OrderString = OrderString + "ORDER BY ICDAlias1 ASC "
Set DrugTbl = Nothing
Set DrugTbl = New ADODB.Recordset
MyDiagnosisMasterCmd.CommandText = OrderString
Call DrugTbl.Open(MyDiagnosisMasterCmd, , adOpenStatic, adLockOptimistic, -1)
If (DrugTbl.EOF) Then
    GetAlias = -1
Else
    DrugTbl.MoveLast
    DrugTotal = DrugTbl.RecordCount
    GetAlias = DrugTotal
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAlias"
    GetAlias = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutDrug()
On Error GoTo DbErrorHandler
PutDrug = True
If (DrugNew) Then
    DrugTbl.AddNew
End If
DrugTbl("ICDAlias1") = Trim(DrugDiagnosisAlias1)
DrugTbl("ICDAlias2") = Trim(DrugDiagnosisAlias2)
DrugTbl("ICDAlias3") = Trim(DrugDiagnosisAlias3)
DrugTbl("ICDAlias4") = Trim(DrugSpecialtyType)
DrugTbl("Rank") = DrugRank
DrugTbl("DrugName") = Trim(DrugName)
DrugTbl("GenericName") = Trim(DrugGenericName)
DrugTbl("Company") = Trim(DrugCompany)
DrugTbl("Dispense1") = Trim(DrugDispense1)
DrugTbl("Dispense2") = Trim(DrugDispense2)
DrugTbl("Dispense3") = Trim(DrugDispense3)
DrugTbl("Dispense4") = Trim(DrugDispense4)
DrugTbl("Dispense5") = Trim(DrugDispense5)
DrugTbl("Dispense6") = Trim(DrugDispense6)
DrugTbl("Dispense7") = Trim(DrugDispense7)
DrugTbl("Dispense8") = Trim(DrugDispense8)
DrugTbl("Package1") = Trim(DrugPackage1)
DrugTbl("Package2") = Trim(DrugPackage2)
DrugTbl("Package3") = Trim(DrugPackage3)
DrugTbl("Package4") = Trim(DrugPackage4)
DrugTbl("Package5") = Trim(DrugPackage5)
DrugTbl("Package6") = Trim(DrugPackage6)
DrugTbl.Update
If (DrugNew) Then
'    DrugTbl.Move 0, DrugTbl.LastModified
    DrugId = DrugTbl("DrugId")
    DrugNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutDrug"
    PutDrug = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeDrug()
On Error GoTo DbErrorHandler
PurgeDrug = True
If Not (DrugNew) Then
    DrugTbl.Delete
End If
Exit Function
DbErrorHandler:
    PurgeDrug = False
    ModuleName = "PurgeDrug"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectDrug(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectDrug = False
If (DrugTbl.EOF) Or (ListItem < 1) Or (ListItem > DrugTotal) Then
    Exit Function
End If
DrugTbl.MoveFirst
DrugTbl.Move ListItem - 1
SelectDrug = True
DrugNew = False
Call LoadDrug(0)
Exit Function
DbErrorHandler:
    ModuleName = "SelectDrug"
    DrugId = -256
    SelectDrug = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectDrugAlias(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectDrugAlias = False
If (DrugTbl.EOF) Or (ListItem < 1) Or (ListItem > DrugTotal) Then
    Exit Function
End If
DrugTbl.MoveFirst
DrugTbl.Move ListItem - 1
SelectDrugAlias = True
DrugNew = False
Call LoadDrug(1)
Exit Function
DbErrorHandler:
    ModuleName = "SelectDrugAlias"
    DrugId = -256
    SelectDrugAlias = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectDrugDistinct(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectDrugDistinct = False
If (DrugTbl.EOF) Or (ListItem < 1) Or (ListItem > DrugTotal) Then
    Exit Function
End If
DrugTbl.MoveFirst
DrugTbl.Move ListItem - 1
SelectDrugDistinct = True
DrugNew = False
Call LoadDrug(2)
Exit Function
DbErrorHandler:
    ModuleName = "SelectDrugDistinct"
    DrugId = -256
    SelectDrugDistinct = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplyDrug() As Boolean
ApplyDrug = PutDrug
End Function

Public Function RetrieveDrug() As Boolean
RetrieveDrug = GetDrug
End Function

Public Function RetrieveDrug_6() As Recordset
On Error GoTo DbErrorHandler
Dim OrderString As String
OrderString = "SELECT * FROM Drug WHERE DrugId =" + Trim(Str(DrugId)) + " "
Set RetrieveDrug_6 = GetRS(OrderString)
Exit Function
DbErrorHandler:
    ModuleName = "RetrieveDrug_6"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function DeleteDrug() As Boolean
DeleteDrug = PurgeDrug
End Function

Public Function FindDrug() As Long
FindDrug = GetAllDrug
End Function

Public Function FindDrugbyName() As Long
FindDrugbyName = GetDrugbyName(0)
End Function

Public Function FindDrugbyNamePartial() As Long
FindDrugbyNamePartial = GetDrugbyName(1)
End Function

Public Function FindDrugbyNamePartialAny() As Long
FindDrugbyNamePartialAny = GetDrugbyNameAny(1)
End Function

Public Function FindPrimaryDrugs() As Long
FindPrimaryDrugs = GetDrugbyName(0)
End Function

Public Function FindPrimaryDrugsDistinct() As Long
FindPrimaryDrugsDistinct = GetDrugbyNameDistinct
End Function

Public Function FindDrugAlias() As Long
FindDrugAlias = GetAlias
End Function

Public Function FindDrugFamilies() As Recordset
Dim SQL As String
SQL = "Select * FROM PracticeCodeTable WHERE ReferenceType = 'DrugFamily' " & _
"and code is not null order by 1"
Set FindDrugFamilies = GetRS(SQL)
End Function

