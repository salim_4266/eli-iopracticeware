VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Letters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Application Lists Class
Option Explicit
Public ApplPatId As Long
Public ApplDate As String
Public ApplEndDate As String
Public ApplResourceId As Long
Public ApplTransactionType As String
Public ApplTransactionStatus As String
Public ApplList As ListBox
Public ApplStartAt As Integer
Public ApplReturnCount As Integer

Private ApplPatientId As Long
Private ApplLastName As String
Private ApplFirstName As String
Private ApplMiddleName As String
Private ApplNameRef As String
Private ApplBirthDate As String
Private ApplHPhone As String
Private ApplBPhone As String
Private ApplApptId As Long
Private ApplApptDate As String
Private ApplApptTime As Integer
Private ApplApptComments As String
Private ApplReferralId As Long
Private ApplApptType As String
Private ApplResource As String
Private ApplStatus As String
Private ApplTransRemark As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset

Private Sub InitList()
ApplDate = ""
ApplPatientId = 0
ApplLastName = ""
ApplFirstName = ""
ApplMiddleName = ""
ApplNameRef = ""
ApplBirthDate = ""
ApplHPhone = ""
ApplBPhone = ""
ApplApptId = 0
ApplApptDate = ""
ApplApptTime = 0
ApplApptType = ""
ApplStatus = ""
End Sub

Private Sub Class_Initialize()
Set ApplListTbl = Nothing
Set ApplListTbl = CreateAdoRecordset
Call InitList
End Sub

Private Sub Class_Terminate()
Call InitList
Set ApplListTbl = Nothing
End Sub

Private Sub LoadApplList()
If (ApplListTbl("AppointmentId") <> "") Then
    ApplApptId = ApplListTbl("AppointmentId")
Else
    ApplApptId = 0
End If
If (ApplListTbl("AppDate") <> "") Then
    ApplApptDate = ApplListTbl("AppDate")
Else
    ApplApptDate = ""
End If
If (ApplListTbl("AppTime") <> "") Then
    ApplApptTime = ApplListTbl("AppTime")
Else
    ApplApptTime = 0
End If
If (ApplListTbl("PatientId") <> "") Then
    ApplPatientId = ApplListTbl("PatientId")
Else
    ApplPatientId = 0
End If
If (ApplListTbl("LastName") <> "") Then
    ApplLastName = ApplListTbl("LastName")
Else
    ApplLastName = 0
End If
If (ApplListTbl("FirstName") <> "") Then
    ApplFirstName = ApplListTbl("FirstName")
Else
    ApplFirstName = ""
End If
If (ApplListTbl("MiddleInitial") <> "") Then
    ApplMiddleName = ApplListTbl("MiddleInitial")
Else
    ApplMiddleName = ""
End If
If (ApplListTbl("NameReference") <> "") Then
    ApplNameRef = ApplListTbl("NameReference")
Else
    ApplNameRef = ""
End If
If (ApplListTbl("BirthDate") <> "") Then
    ApplBirthDate = ApplListTbl("BirthDate")
Else
    ApplBirthDate = ""
End If
If (ApplListTbl("AppointmentType") <> "") Then
    ApplApptType = ApplListTbl("AppointmentType")
Else
    ApplApptType = ""
End If
If (ApplListTbl("ResourceName") <> "") Then
    ApplResource = ApplListTbl("ResourceName")
Else
    ApplResource = ""
End If
If (ApplListTbl("ScheduleStatus") <> "") Then
    ApplStatus = ApplListTbl("ScheduleStatus")
Else
    ApplStatus = ""
End If
If (ApplListTbl("HomePhone") <> "") Then
    ApplHPhone = ApplListTbl("HomePhone")
Else
    ApplHPhone = ""
End If
If (ApplListTbl("BusinessPhone") <> "") Then
    ApplBPhone = ApplListTbl("BusinessPhone")
Else
    ApplBPhone = ""
End If
End Sub

Private Function GetLettersList() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim Temp As String
ApplListTotal = 0
GetLettersList = -1
Set ApplListTbl = Nothing
Set ApplListTbl = CreateAdoRecordset
Ref = ""
Temp = "WHERE "
If (Trim(ApplDate) <> "") Then
    Temp = Temp + Ref + "Appointments.AppDate <= '" + Trim(ApplDate) + "' "
    Ref = "And "
End If
If (ApplPatId > 0) Then
    Temp = Temp + Ref + "Appointments.PatientId = " + Trim(Str(ApplPatId)) + " "
    Ref = "And "
End If
If (Trim(Temp) <> "") Then
    Temp = Temp + Ref + "Appointments.ScheduleStatus = 'D' "
Else
    Temp = "Appointments.ScheduleStatus = 'D' "
End If
MyPracticeRepositoryCmd.CommandText _
    = "SELECT Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, " _
    + "Appointments.ScheduleStatus, Appointments.TechApptTypeId, " _
    + "PatientDemographics.LastName, PatientDemographics.FirstName, PatientDemographics.MiddleInitial, " _
    + "PatientDemographics.NameReference, PatientDemographics.BirthDate, PatientDemographics.PatientId, " _
    + "PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, " _
    + "PatientDemographics.PolicyPatientId, " _
    + "AppointmentType.AppointmentType, Resources.ResourceName " _
    + "FROM Resources " _
    + "INNER JOIN (PatientDemographics " _
    + "INNER JOIN (AppointmentType " _
    + "INNER JOIN Appointments " _
    + "ON AppointmentType.AppTypeId = Appointments.AppTypeId) " _
    + "ON PatientDemographics.PatientId = Appointments.PatientId) " _
    + "ON Resources.ResourceId = Appointments.ResourceId1 " _
    + Temp _
    + "ORDER BY Appointments.AppDate DESC, Appointments.AppTime DESC "
Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (ApplListTbl.EOF) Then
    ApplListTbl.MoveLast
    ApplListTotal = ApplListTbl.RecordCount
    GetLettersList = ApplListTotal
End If
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Function SelectList(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectList = False
If (ApplListTbl.EOF) Or (ListItem < 1) Or (ListItem > ApplListTotal) Then
    Exit Function
End If
ApplListTbl.MoveFirst
ApplListTbl.Move ListItem - 1
Call LoadApplList
SelectList = True
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplRetrieveLettersList() As Long
Dim i As Integer
Dim Age As Integer
Dim Temp As String
Dim PatName As String
Dim DisplayItem As String
ApplRetrieveLettersList = -1
If (ApplPatId > 0) Then
    ApplRetrieveLettersList = GetLettersList
    If (ApplReturnCount < 1) Then
        ApplReturnCount = ApplRetrieveLettersList
    End If
    If (ApplStartAt < 1) Or (ApplStartAt > ApplRetrieveLettersList) Then
        ApplStartAt = 1
    End If
    ApplList.Clear
    For i = ApplStartAt To ApplReturnCount + (ApplStartAt - 1)
        Call SelectList(i)
        DisplayItem = Space(75)
        Mid(DisplayItem, 1, 10) = Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4)
        Call ConvertMinutesToTime(ApplApptTime, Temp)
        Mid(DisplayItem, 12, 8) = Temp
        Mid(DisplayItem, 22, Len(ApplApptType)) = ApplApptType
        PatName = Trim(ApplFirstName) + " " + ApplMiddleName + " " _
                + Trim(ApplLastName) + " " + Trim(ApplNameRef)
        Mid(DisplayItem, 38, Len(Trim(PatName))) = Trim(PatName)
        Mid(DisplayItem, 60, 10) = ApplResource
        Mid(DisplayItem, 71, 1) = ApplStatus
        DisplayItem = DisplayItem + Trim(Str(ApplApptId)) + "/" + Trim(Str(ApplPatientId))
        ApplList.AddItem DisplayItem
    Next i
End If
End Function

Public Function ApplLettersApptId(TheItem As String) As Long
Dim k As Integer
Dim u As Integer
ApplLettersApptId = 0
If (Trim(TheItem) > 75) Then
    k = InStrPS(75, TheItem, "/")
    If (k > 0) Then
            ApplLettersApptId = Val(Trim(Mid(TheItem, 76, (k - 1) - 75)))
    End If
End If
End Function

Public Function ApplLettersPatientId(TheItem As String) As Long
Dim k As Integer
ApplLettersPatientId = 0
If (Trim(TheItem) > 75) Then
    k = InStrPS(75, TheItem, "/")
    If (k > 0) Then
            ApplLettersPatientId = Val(Trim(Mid(TheItem, k + 1, Len(TheItem) - k)))
    End If
End If
End Function

Public Function ApplPostLetter(PatId As Long, ApptId As Long, TheName As String, TheOther As String, VndId As Long, TheType As String) As Boolean
Dim i As Integer
Dim RetPat As Patient
Dim RetTrans As PracticeTransactionJournal
Dim IODateTime As New CIODateTime
ApplPostLetter = False
If (PatId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        If (VndId < 1) Then
            VndId = RetPat.PrimaryCarePhysician
        End If
        If (TheType <> "Y") Then
            If (VndId < 1) Then
                VndId = RetPat.ReferringPhysician
            End If
        End If
    End If
    Set RetPat = Nothing
End If
IODateTime.MyDateTime = Now
Set RetTrans = New PracticeTransactionJournal
RetTrans.TransactionJournalId = 0
If (RetTrans.RetrieveTransactionJournal) Then
    RetTrans.TransactionJournalType = TheType
    RetTrans.TransactionJournalTypeId = ApptId
    RetTrans.TransactionJournalStatus = "P"
    RetTrans.TransactionJournalAction = "B"
    RetTrans.TransactionJournalDate = IODateTime.GetIODate
    RetTrans.TransactionJournalTime = IODateTime.GetIOTime
    RetTrans.TransactionJournalRcvrId = VndId
    RetTrans.TransactionJournalReference = ""
    RetTrans.TransactionJournalRemark = "/Ltr/" + TheName
    RetTrans.TransactionJournalBatch = "N"
    If (TheType = "Y") Then
        RetTrans.TransactionJournalReference = TheName
        RetTrans.TransactionJournalRemark = "/Ltr/" + TheOther
        RetTrans.TransactionJournalBatch = "P"
    End If
    ApplPostLetter = RetTrans.ApplyTransactionJournal
End If
Set RetTrans = Nothing
Set IODateTime = Nothing
End Function

Public Function ApplLettersFile(TheItem As Long) As String
Dim i As Integer
Dim RetTrans As PracticeTransactionJournal
Set RetTrans = New PracticeTransactionJournal
ApplLettersFile = ""
If (TheItem > 0) Then
    RetTrans.TransactionJournalId = TheItem
    If (RetTrans.RetrieveTransactionJournal) Then
        If (Trim(RetTrans.TransactionJournalBatch) <> "*") Then
            ApplLettersFile = Trim(RetTrans.TransactionJournalRemark)
            i = InStrPS(ApplLettersFile, "(")
            If (i > 0) Then
                ApplLettersFile = Left(ApplLettersFile, i - 1)
            End If
            i = InStrPS(ApplLettersFile, ",")
            If (i > 0) Then
                ApplLettersFile = Left(ApplLettersFile, i - 1)
            End If
            If (Mid(ApplLettersFile, Len(ApplLettersFile), 1) = "-") Then
                ApplLettersFile = Left(ApplLettersFile, Len(ApplLettersFile) - 1)
            End If
        Else
            ApplLettersFile = "-"
        End If
    End If
End If
Set RetTrans = Nothing
End Function

