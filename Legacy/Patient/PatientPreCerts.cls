VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientPreCerts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient PreCerts Object Module
Option Explicit
Public PatientId As Long
Public PreCertsId As Long
Public PreCerts As String
Public PreCertsDate As String
Public PreCertsInsId As Long
Public PreCertsApptId As Long
Public PreCertsComments As String
Public PreCertsLocation As Long

Private PreCertsStatus As String
Private ModuleName As String
Private PatientPreCertsTotal As Long
Private PatientPreCertsTbl As ADODB.Recordset
Private PatientPreCertsNew As Boolean

Private Sub Class_Initialize()
Set PatientPreCertsTbl = CreateAdoRecordset
Call InitPatientPreCerts
End Sub

Private Sub Class_Terminate()
Call InitPatientPreCerts
Set PatientPreCertsTbl = Nothing
End Sub

Private Sub InitPatientPreCerts()
PatientPreCertsNew = True
PreCerts = ""
PreCertsDate = ""
PreCertsInsId = 0
PreCertsApptId = 0
PreCertsStatus = "A"
PreCertsComments = ""
PreCertsLocation = 0
End Sub

Private Sub LoadPatientPreCerts()
PatientPreCertsNew = False
If (PatientPreCertsTbl("PreCertId") <> "") Then
    PreCertsId = PatientPreCertsTbl("PreCertId")
Else
    PreCertsId = 0
End If
If (PatientPreCertsTbl("PreCert") <> "") Then
    PreCerts = PatientPreCertsTbl("PreCert")
Else
    PreCerts = ""
End If
If (PatientPreCertsTbl("PreCertDate") <> "") Then
    PreCertsDate = PatientPreCertsTbl("PreCertDate")
Else
    PreCertsDate = ""
End If
If (PatientPreCertsTbl("PatientId") <> "") Then
    PatientId = PatientPreCertsTbl("PatientId")
Else
    PatientId = 0
End If
If (PatientPreCertsTbl("PreCertInsId") <> "") Then
    PreCertsInsId = PatientPreCertsTbl("PreCertInsId")
Else
    PreCertsInsId = 0
End If
If (PatientPreCertsTbl("PreCertApptId") <> "") Then
    PreCertsApptId = PatientPreCertsTbl("PreCertApptId")
Else
    PreCertsApptId = ""
End If
If (PatientPreCertsTbl("PreCertComment") <> "") Then
    PreCertsComments = PatientPreCertsTbl("PreCertComment")
Else
    PreCertsComments = ""
End If
If (PatientPreCertsTbl("PreCertLocationId") <> "") Then
    PreCertsLocation = PatientPreCertsTbl("PreCertLocationId")
Else
    PreCertsLocation = 0
End If
If (PatientPreCertsTbl("PreCertStatus") <> "") Then
    PreCertsStatus = PatientPreCertsTbl("PreCertStatus")
Else
    PreCertsStatus = ""
End If
End Sub

Private Function GetPatientPreCertsList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim Ref1 As String
Dim OrderString As String
Dim WhereString As String
PatientPreCertsTotal = 0
Ref1 = ""
GetPatientPreCertsList = -1
If (PatientId < 1) And (PreCertsApptId < 1) And (Trim(PreCertsDate) = "") And (PreCertsInsId < 1) Then
    Exit Function
End If
If (IType = 1) Then
    Ref = ">="
ElseIf (IType = 0) Then
    Ref = "="
Else
    Ref = "<="
End If
OrderString = "Select * FROM PatientPreCerts WHERE "
If (PatientId > 0) Then
    OrderString = OrderString + Ref1 + "PatientId =" + Str(PatientId) + " "
    Ref1 = "And "
End If
If (PreCertsApptId > 0) Then
    OrderString = OrderString + Ref1 + "PreCertApptId =" + Str(PreCertsApptId) + " "
    Ref1 = "And "
End If
If (PreCertsInsId > 0) Then
    OrderString = OrderString + Ref1 + "PreCertInsId =" + Str(PreCertsInsId) + " "
    Ref1 = "And "
End If
If (Trim(PreCertsDate) <> "") Then
    OrderString = OrderString + Ref1 + "PreCertDate " + Ref + " '" + Trim(PreCertsDate) + "' "
    Ref1 = "And "
End If
OrderString = OrderString + " ORDER BY PreCertDate ASC "
Set PatientPreCertsTbl = Nothing
Set PatientPreCertsTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientPreCertsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientPreCertsTbl.EOF) Then
    GetPatientPreCertsList = -1
Else
    PatientPreCertsTbl.MoveLast
    PatientPreCertsTotal = PatientPreCertsTbl.RecordCount
    GetPatientPreCertsList = PatientPreCertsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientPreCertsList"
    GetPatientPreCertsList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientPreCerts() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientPreCerts = True
OrderString = "SELECT * FROM PatientPreCerts WHERE PreCertId =" + Str(PreCertsId)
Set PatientPreCertsTbl = Nothing
Set PatientPreCertsTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientPreCertsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientPreCertsTbl.EOF) Then
    Call InitPatientPreCerts
    GetPatientPreCerts = False
Else
    Call LoadPatientPreCerts
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientPreCerts"
    GetPatientPreCerts = False
    PreCertsId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientPreCerts() As Boolean
On Error GoTo DbErrorHandler
PutPatientPreCerts = True
If (PatientPreCertsNew) Then
    PatientPreCertsTbl.AddNew
End If
PatientPreCertsTbl("PreCert") = UCase(Trim(PreCerts))
PatientPreCertsTbl("PreCertDate") = PreCertsDate
PatientPreCertsTbl("PatientId") = PatientId
PatientPreCertsTbl("PreCertInsId") = PreCertsInsId
PatientPreCertsTbl("PreCertApptId") = PreCertsApptId
PatientPreCertsTbl("PreCertComment") = PreCertsComments
PatientPreCertsTbl("PreCertLocationId") = PreCertsLocation
PatientPreCertsTbl("PreCertStatus") = PreCertsStatus
PatientPreCertsTbl.Update
If (PatientPreCertsNew) Then
    PreCertsId = GetJustAddedRecord
    PatientPreCertsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientPreCerts"
    PutPatientPreCerts = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPatientPreCertsList(0) > 0) Then
    If (SelectPatientPreCerts(1)) Then
        GetJustAddedRecord = PatientPreCertsTbl("PreCertId")
    End If
End If
End Function

Public Function ApplyPatientPreCerts() As Boolean
ApplyPatientPreCerts = PutPatientPreCerts
End Function

Public Function DeletePatientPreCerts() As Boolean
PreCertsStatus = "D"
DeletePatientPreCerts = PutPatientPreCerts
End Function

Public Function RetrievePatientPreCerts() As Boolean
RetrievePatientPreCerts = GetPatientPreCerts
End Function

Public Function SelectPatientPreCerts(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPatientPreCerts = False
If (PatientPreCertsTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientPreCertsTotal) Then
    Exit Function
End If
PatientPreCertsTbl.MoveFirst
PatientPreCertsTbl.Move ListItem - 1
SelectPatientPreCerts = True
Call LoadPatientPreCerts
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientPreCerts"
    SelectPatientPreCerts = False
    PreCertsId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPatientPreCertsAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPatientPreCertsAggregate = False
If (PatientPreCertsTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientPreCertsTotal) Then
    Exit Function
End If
PatientPreCertsTbl.MoveFirst
PatientPreCertsTbl.Move ListItem - 1
SelectPatientPreCertsAggregate = True
Call LoadPatientPreCerts
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientPreCertsAggregate"
    SelectPatientPreCertsAggregate = False
    PreCertsId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientPreCerts() As Long
FindPatientPreCerts = GetPatientPreCertsList(0)
End Function

Public Function FindPatientPreCertsbyDate() As Long
FindPatientPreCertsbyDate = GetPatientPreCertsList(1)
End Function

Public Function FindPatientPreCertsbyPrevDate() As Long
FindPatientPreCertsbyPrevDate = GetPatientPreCertsList(-1)
End Function

