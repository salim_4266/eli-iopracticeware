VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DI_ExamActivityList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Exam Activity List Class
Option Explicit
Public ActivityId As Long
Public AppointmentId As Long
Public AppointmentDate As String
Public AppointmentTime As String
Public AppointmentType As String
Public ScheduledDoctor As String
Public Comments As String

Private rsActivityList As ADODB.Recordset
Private strSQL As String

Private Sub InitList()
ActivityId = 0
AppointmentId = 0
AppointmentDate = ""
AppointmentTime = ""
AppointmentType = ""
ScheduledDoctor = ""
Comments = ""
strSQL = ""
End Sub

Private Sub Class_Initialize()
Set rsActivityList = Nothing
Set rsActivityList = CreateAdoRecordset
Call InitList
End Sub

Private Sub Class_Terminate()
Call InitList
Set rsActivityList = Nothing
End Sub

Private Sub LoadList()
If (rsActivityList("ActivityId") <> "") Then
    ActivityId = rsActivityList("ActivityId")
End If
If (rsActivityList("AppointmentId") <> "") Then
    AppointmentId = rsActivityList("AppointmentId")
End If
If (rsActivityList("AppDate") <> "") Then
    AppointmentDate = rsActivityList("AppDate")
End If
If (rsActivityList("AppTime") <> "") Then
    AppointmentTime = rsActivityList("AppTime")
End If
If (rsActivityList("AppointmentType") <> "") Then
    AppointmentType = rsActivityList("AppointmentType")
End If
If (rsActivityList("ScheduledDoctor") <> "") Then
    ScheduledDoctor = rsActivityList("ScheduledDoctor")
End If
If (rsActivityList("Comments") <> "") Then
    Comments = rsActivityList("Comments")
End If
End Sub

Public Function PreviousActivity(PatId As Long, CurDate As String, StatusFilter As String) As Boolean
On Error GoTo DbErrorHandler
PreviousActivity = False
strSQL = "usp_DI_ActivityList " & str(PatId) & ",'" & CurDate & "','" & StatusFilter & "'"
Set rsActivityList = Nothing
Set rsActivityList = CreateAdoRecordset
Call rsActivityList.Open(strSQL, MyPracticeRepository)
If (rsActivityList.EOF) Then
    PreviousActivity = False
Else
    PreviousActivity = True
    Call LoadList
End If
Exit Function
DbErrorHandler:
    Call PinpointPRError("PreviousActivity", Err)
    Resume LeaveFast
LeaveFast:
End Function
Public Function CurrentActivity(PatId As Long) As Long
On Error GoTo DbErrorHandler
strSQL = " SELECT TOP 1 pa.ActivityId FROM PracticeActivity pa " & _
         " INNER JOIN Appointments ap ON ap.AppointmentID=pa.AppointmentId " & _
         " WHERE pa.PatientId='" & PatId & "' AND pa.status='D' " & _
         " ORDER BY ap.AppDate Desc "
Set rsActivityList = Nothing
Set rsActivityList = CreateAdoRecordset
Call rsActivityList.Open(strSQL, MyPracticeRepository)
CurrentActivity = rsActivityList(0)
Exit Function
DbErrorHandler:
End Function

Public Function NextRec() As Boolean
NextRec = False
rsActivityList.MoveNext
If Not (rsActivityList.EOF) Then
    Call LoadList
    NextRec = True
End If
End Function
