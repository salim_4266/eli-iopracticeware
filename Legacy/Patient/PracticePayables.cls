VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticePayables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Payable Object Module
Option Explicit
Public PayableId As Long
Public PayableInvoice As String
Public PayableType As String
Public PayablePayerType As String
Public PayableDescription As String
Public PayableAmount As Single
Public PayableDate As String
Public PayableVendorId As Long
Public PayablePaidAmount As Single
Public PayablePaidDate As String
Public APAccount As String

Private PayableStatus As String
Private ModuleName As String
Private PracticePayableTotal As Long
Private PracticePayableTbl As ADODB.Recordset
Private PracticePayableNew As Boolean

Private Sub Class_Initialize()
Set PracticePayableTbl = CreateAdoRecordset
Call InitPracticePayable
End Sub

Private Sub Class_Terminate()
Call InitPracticePayable
Set PracticePayableTbl = Nothing
End Sub

Private Sub InitPracticePayable()
PracticePayableNew = True
PayableInvoice = ""
PayableType = ""
PayablePayerType = ""
PayableDescription = ""
PayableAmount = 0
PayableDate = ""
PayablePaidAmount = 0
PayablePaidDate = ""
APAccount = ""
PayableStatus = "A"
End Sub

Private Sub LoadPracticePayable()
PracticePayableNew = False
If (PracticePayableTbl("PayableId") <> "") Then
    PayableId = PracticePayableTbl("PayableId")
Else
    PayableId = 0
End If
If (PracticePayableTbl("PayableInvoice") <> "") Then
    PayableInvoice = PracticePayableTbl("PayableInvoice")
Else
    PayableInvoice = ""
End If
If (PracticePayableTbl("PayableType") <> "") Then
    PayableType = PracticePayableTbl("PayableType")
Else
    PayableType = ""
End If
If (PracticePayableTbl("PayablePayerType") <> "") Then
    PayablePayerType = PracticePayableTbl("PayablePayerType")
Else
    PayablePayerType = ""
End If
If (PracticePayableTbl("PayableDescription") <> "") Then
    PayableDescription = PracticePayableTbl("PayableDescription")
Else
    PayableDescription = ""
End If
If (PracticePayableTbl("PayableAmount") <> "") Then
    PayableAmount = PracticePayableTbl("PayableAmount")
Else
    PayableAmount = 0
End If
If (PracticePayableTbl("PayableDate") <> "") Then
    PayableDate = PracticePayableTbl("PayableDate")
Else
    PayableDate = ""
End If
If (PracticePayableTbl("PayablePaidAmount") <> "") Then
    PayablePaidAmount = PracticePayableTbl("PayablePaidAmount")
Else
    PayablePaidAmount = 0
End If
If (PracticePayableTbl("PayablePaidDate") <> "") Then
    PayablePaidDate = PracticePayableTbl("PayablePaidDate")
Else
    PayablePaidDate = ""
End If
If (PracticePayableTbl("APAcct") <> "") Then
    APAccount = PracticePayableTbl("APAcct")
Else
    APAccount = ""
End If
If (PracticePayableTbl("Status") <> "") Then
    PayableStatus = PracticePayableTbl("Status")
Else
    PayableStatus = ""
End If
End Sub

Private Function GetPayables() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
Ref = ""
GetPayables = -1
PracticePayableTotal = 0
OrderString = "Select * FROM PracticePayables WHERE "
If (Trim(PayableInvoice) = "") And (PayableVendorId < 1) Then
    Exit Function
End If
If (Trim(PayableInvoice) <> "") Then
    OrderString = OrderString + "PayableInvoice >= '" + PayableInvoice + "' "
    Ref = "And "
End If
If (PayableVendorId > 0) Then
    OrderString = OrderString + Ref + "PayableVendorId =" + Str(PayableVendorId) + " "
End If
If (Trim(PayablePayerType) <> "") Then
    OrderString = OrderString + "And " + "PayablePayerType = '" + PayablePayerType + "' "
End If
Set PracticePayableTbl = Nothing
Set PracticePayableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticePayableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticePayableTbl.EOF) Then
    GetPayables = -1
Else
    PracticePayableTbl.MoveLast
    PracticePayableTotal = PracticePayableTbl.RecordCount
    GetPayables = PracticePayableTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPayables"
    GetPayables = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPracticePayable() As Boolean
On Error GoTo DbErrorHandler
GetPracticePayable = True
Dim OrderString As String, WhereString As String
OrderString = "SELECT * FROM PracticePayables WHERE PayableId =" + Str(PayableId)
Set PracticePayableTbl = Nothing
Set PracticePayableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PracticePayableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PracticePayableTbl.EOF) Then
    Call InitPracticePayable
Else
    Call LoadPracticePayable
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPracticePayable"
    GetPracticePayable = False
    PayableId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPracticePayable() As Boolean
On Error GoTo DbErrorHandler
PutPracticePayable = True
If (PracticePayableNew) Then
    PracticePayableTbl.AddNew
End If
PracticePayableTbl("PayableInvoice") = Trim(PayableInvoice)
PracticePayableTbl("PayableType") = UCase(Trim(PayableType))
PracticePayableTbl("PayablePayerType") = UCase(Trim(PayablePayerType))
PracticePayableTbl("PayableDescription") = Trim(PayableDescription)
PracticePayableTbl("PayableAmount") = PayableAmount
PracticePayableTbl("PayableDate") = Trim(PayableDate)
PracticePayableTbl("PayablePaidAmount") = PayablePaidAmount
PracticePayableTbl("PayablePaidDate") = Trim(PayablePaidDate)
PracticePayableTbl("PayableVendorId") = PayableVendorId
PracticePayableTbl("APAcct") = Trim(APAccount)
PracticePayableTbl("Status") = PayableStatus
PracticePayableTbl.Update
If (PracticePayableNew) Then
'    PracticePayableTbl.Move 0, PracticePayableTbl.LastModified
    PayableId = PracticePayableTbl("PayableId")
    PracticePayableNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPracticePayable"
    PutPracticePayable = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPayables > 0) Then
    If (SelectPayables(1)) Then
        GetJustAddedRecord = PracticePayableTbl("PayableId")
    End If
End If
End Function

Public Function ApplyPracticePayable() As Boolean
ApplyPracticePayable = PutPracticePayable
End Function

Public Function DeletePracticePayable() As Boolean
PayableStatus = "D"
DeletePracticePayable = PutPracticePayable
End Function

Public Function RetrievePracticePayable() As Boolean
RetrievePracticePayable = GetPracticePayable
End Function

Public Function SelectPayables(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPayables = False
If (PracticePayableTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticePayableTotal) Then
    Exit Function
End If
PracticePayableTbl.MoveFirst
PracticePayableTbl.Move ListItem - 1
SelectPayables = True
Call LoadPracticePayable
Exit Function
DbErrorHandler:
    ModuleName = "SelectPayables"
    SelectPayables = False
    PayableId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPayables() As Long
FindPayables = GetPayables()
End Function

Public Function FindPayablesbyVendor() As Long
FindPayablesbyVendor = GetPayables()
End Function

