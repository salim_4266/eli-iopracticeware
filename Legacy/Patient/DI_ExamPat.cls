VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DI_ExamPat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Exam Activity List Class
Option Explicit
Public ActivityDate As String
Public PatientId As Long
Public MiddleInitial As String
Public FirstName As String
Public LastName As String
Public NameReference As String
Public PolicyPatientId As Long
Public SecondPolicyPatientId As Long
Public BirthDate As String
Public Gender As String
Public NationalOrigin As String
Public PatType As String
Public AppointmentId As Long
Public AppointmentDate As String
Public VisitReason As String
Public AppDate As String
Public AppTime As Integer
Public VendorName As String
Public EMBasis As String
Public AppointmentType As String
Public DrRequired As Boolean
Public QuestionSet As String
Public RedNotesExist As Boolean
Public PreviousAppointmentId As Long
Public BusinessClassId As String
Public InsurerClassId As String
Public PrimaryInsurer As String
Public SecondaryInsurer As String

Private rsExamPat As ADODB.Recordset
Private strSQL As String

Private Sub InitList()
ActivityDate = ""
PatientId = 0
MiddleInitial = ""
FirstName = ""
LastName = ""
NameReference = ""
PolicyPatientId = 0
SecondPolicyPatientId = 0
BirthDate = ""
Gender = ""
NationalOrigin = ""
PatType = ""
AppointmentId = 0
VisitReason = ""
AppDate = ""
AppTime = 0
VendorName = ""
EMBasis = ""
AppointmentType = ""
DrRequired = 0
QuestionSet = ""
RedNotesExist = False
PreviousAppointmentId = 0
BusinessClassId = ""
InsurerClassId = ""
PrimaryInsurer = ""
SecondaryInsurer = ""
strSQL = ""
End Sub

Private Sub Class_Initialize()
Set rsExamPat = Nothing
Set rsExamPat = CreateAdoRecordset
Call InitList
End Sub

Private Sub Class_Terminate()
Call InitList
Set rsExamPat = Nothing
End Sub

Private Sub LoadList()
If (rsExamPat("appointmentid") <> "") Then
    AppointmentId = rsExamPat("appointmentid")
End If
If (rsExamPat("appdate") <> "") Then
    AppDate = rsExamPat("appdate")
End If
If (rsExamPat("apptime") <> "") Then
    AppTime = rsExamPat("apptime")
End If
If (rsExamPat("appointmenttype") <> "") Then
    AppointmentType = rsExamPat("appointmenttype")
End If
If (rsExamPat("activitydate") <> "") Then
    ActivityDate = rsExamPat("activitydate")
End If
If (rsExamPat("patientid") <> "") Then
    PatientId = rsExamPat("patientid")
End If
If (rsExamPat("middleinitial") <> "") Then
    MiddleInitial = rsExamPat("middleinitial")
End If
If (rsExamPat("firstName") <> "") Then
    FirstName = rsExamPat("firstname")
End If
If (rsExamPat("lastname") <> "") Then
    LastName = rsExamPat("lastname")
End If
If (rsExamPat("namereference") <> "") Then
    NameReference = rsExamPat("namereference")
End If
If (rsExamPat("policypatientid") <> "") Then
    PolicyPatientId = rsExamPat("policypatientid")
End If
If (rsExamPat("secondpolicypatientid") <> "") Then
    SecondPolicyPatientId = rsExamPat("secondpolicypatientid")
End If
If (rsExamPat("birthdate") <> "") Then
    BirthDate = rsExamPat("birthdate")
End If
If (rsExamPat("gender") <> "") Then
    Gender = rsExamPat("gender")
End If
If (rsExamPat("nationalorigin") <> "") Then
    NationalOrigin = rsExamPat("nationalorigin")
End If
If (rsExamPat("pattype") <> "") Then
    PatType = rsExamPat("pattype")
End If
If (rsExamPat("visitreason") <> "") Then
    VisitReason = rsExamPat("visitreason")
End If
If (rsExamPat("VendorName") <> "") Then
    VendorName = rsExamPat("VendorName")
End If
If (rsExamPat("drrequired") <> "") Then
    If (rsExamPat("drrequired") <> "N") Then
        DrRequired = True
    End If
End If
If (rsExamPat("embasis") <> "") Then
    EMBasis = rsExamPat("embasis")
End If
If (rsExamPat("questionset") <> "") Then
    QuestionSet = rsExamPat("questionset")
End If
If (rsExamPat("RedNotes") <> "") Then
    If (rsExamPat("RedNotes") <> "No") Then
        RedNotesExist = True
    End If
End If
If (rsExamPat("PreviousAppointmentId") <> "") Then
    PreviousAppointmentId = rsExamPat("PreviousAppointmentId")
End If
If (rsExamPat("PrimaryBusinessClass") <> "") Then
    BusinessClassId = rsExamPat("PrimaryBusinessClass")
End If
If (rsExamPat("PrimaryInsurer") <> "") Then
    PrimaryInsurer = rsExamPat("PrimaryInsurer")
End If
If (rsExamPat("SecondaryInsurer") <> "") Then
    SecondaryInsurer = rsExamPat("SecondaryInsurer")
End If
If (rsExamPat("InsurerCPTClass") <> "") Then
    InsurerClassId = rsExamPat("InsurerCPTClass")
End If
End Sub

Public Function RetrieveExamPatInfo(ActId As Long) As Boolean
On Error GoTo DbErrorHandler
RetrieveExamPatInfo = False
    strSQL = "usp_DI_ExamPatInfo " & Str(ActId) & " "
Set rsExamPat = Nothing
Set rsExamPat = CreateAdoRecordset
Call rsExamPat.Open(strSQL, MyPracticeRepository)
If Not (rsExamPat.EOF) Then
    RetrieveExamPatInfo = True
    Call LoadList
End If
Exit Function
DbErrorHandler:
    Call PinpointPRError("RetrieveExamPatInfo", Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function checkAppintmentTypeId(ActId As Long) As Boolean
On Error GoTo DbErrorHandler
strSQL = " select * from PracticeActivity pa " & _
" inner join Appointments ap on ap.AppointmentId = pa.AppointmentId  " & _
" inner join AppointmentType apt on apt.AppTypeId = ap.AppTypeId  " & _
" where ActivityId = " & ActId
Set rsExamPat = Nothing
Set rsExamPat = CreateAdoRecordset
Call rsExamPat.Open(strSQL, MyPracticeRepository)

checkAppintmentTypeId = Not rsExamPat.EOF
Exit Function
DbErrorHandler:
    Call PinpointPRError("checkAppintmentTypeId", Err)
    Resume LeaveFast
LeaveFast:
End Function

