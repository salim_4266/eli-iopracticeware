VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeActivity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeActivity Object Module
Option Explicit
Public ActivityId As Long
Public ActivityPatientId As Long
Public ActivityStationId As Long
Public ActivityAppointmentId As Long
Public ActivityQuestionSet As String
Public ActivityStatus As String
Public ActivityStatusTime As String
Public ActivityStatusTRef As Integer
Public ActivityDate As String
Public ActivityResourceId As Long
Public ActivityLocationId As Long
Public ActivityTechConfirm As Boolean
Public ActivityCurrResId As Long
Public ActivityPayAmount As Single
Public ActivityPayMethod As String
Public ActivityPayRef As String

Private ModuleName As String
Private ActivityTotal As Long
Private ActivityTbl As ADODB.Recordset
Private ActivityNew As Boolean

Private EmergencypermissionTotal As Long
Private EmergencypermissionTbl As ADODB.Recordset



Private Sub Class_Initialize()
Set ActivityTbl = CreateAdoRecordset
Call InitActivity
End Sub

Private Sub Class_Terminate()
Call InitActivity
Set ActivityTbl = Nothing
End Sub

Private Sub InitActivity()
ActivityNew = True
ActivityAppointmentId = 0
ActivityPatientId = 0
ActivityStationId = 0
ActivityStatus = "W"
ActivityStatusTime = ""
ActivityStatusTRef = 0
ActivityDate = ""
ActivityQuestionSet = ""
ActivityResourceId = 0
ActivityLocationId = 0
ActivityTechConfirm = False
ActivityCurrResId = 0
ActivityPayAmount = 0
ActivityPayMethod = ""
ActivityPayRef = ""
End Sub

Private Sub LoadActivity()
ActivityNew = False
ActivityId = ActivityTbl("ActivityId")
If (ActivityTbl("AppointmentId") <> "") Then
    ActivityAppointmentId = ActivityTbl("AppointmentId")
Else
    ActivityAppointmentId = 0
End If
If (ActivityTbl("PatientId") <> "") Then
    ActivityPatientId = ActivityTbl("PatientId")
Else
    ActivityPatientId = 0
End If
If (ActivityTbl("StationId") <> "") Then
    ActivityStationId = ActivityTbl("StationId")
Else
    ActivityStationId = 0
End If
If (ActivityTbl("ActivityDate") <> "") Then
    ActivityDate = ActivityTbl("ActivityDate")
Else
    ActivityDate = ""
End If
If (ActivityTbl("Status") <> "") Then
    ActivityStatus = ActivityTbl("Status")
Else
    ActivityStatus = ""
End If
If (ActivityTbl("ActivityStatusTime") <> "") Then
    ActivityStatusTime = ActivityTbl("ActivityStatusTime")
Else
    ActivityStatusTime = ""
End If
If (ActivityTbl("ActivityStatusTRef") <> "") Then
    ActivityStatusTRef = ActivityTbl("ActivityStatusTRef")
Else
    ActivityStatusTRef = 0
End If
If (ActivityTbl("ResourceId") <> "") Then
    ActivityResourceId = ActivityTbl("ResourceId")
Else
    ActivityResourceId = 0
End If
If (ActivityTbl("LocationId") <> "") Then
    ActivityLocationId = ActivityTbl("LocationId")
Else
    ActivityLocationId = 0
End If
If (ActivityTbl("QuestionSet") <> "") Then
    ActivityQuestionSet = ActivityTbl("QuestionSet")
Else
    ActivityQuestionSet = ""
End If
If (ActivityTbl("TechConfirmed") <> "") Then
    If (ActivityTbl("TechConfirmed") = "N") Then
        ActivityTechConfirm = False
    Else
        ActivityTechConfirm = True
    End If
Else
    ActivityTechConfirm = False
End If
If (ActivityTbl("CurrentRId") <> "") Then
    ActivityCurrResId = ActivityTbl("CurrentRId")
Else
    ActivityCurrResId = 0
End If
If (ActivityTbl("PayAmount") <> "") Then
    ActivityPayAmount = ActivityTbl("PayAmount")
Else
    ActivityPayAmount = 0
End If
If (ActivityTbl("PayMethod") <> "") Then
    ActivityPayMethod = ActivityTbl("PayMethod")
Else
    ActivityPayMethod = ""
End If
If (ActivityTbl("PayReference") <> "") Then
    ActivityPayRef = ActivityTbl("PayReference")
Else
    ActivityPayRef = ""
End If
End Sub

Private Function GetActivity()
On Error GoTo DbErrorHandler
GetActivity = False
Dim OrderString As String
    OrderString = "SELECT * FROM PracticeActivity WHERE ActivityId =" + Trim(str(ActivityId))
If (ActivityAppointmentId > 0) Then
        OrderString = "SELECT * FROM PracticeActivity WHERE AppointmentId =" + Trim(str(ActivityAppointmentId))
End If
Set ActivityTbl = Nothing
Set ActivityTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call ActivityTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (ActivityTbl.EOF) Then
    Call InitActivity
Else
    Call LoadActivity
End If
GetActivity = True
Exit Function
DbErrorHandler:
    ModuleName = "GetActivity"
    GetActivity = False
    ActivityId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetActivityList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim i As Integer
Dim Ref As String
Dim Temp As String
Dim OrderString As String
ActivityTotal = 0
Ref = ""
GetActivityList = -1
If (Trim(ActivityDate) = "") And (ActivityAppointmentId < 1) And (ActivityPatientId < 1) Then
    Exit Function
End If
OrderString = "Select * FROM PracticeActivity WHERE "
If (Len(ActivityDate) > 1) Then
    OrderString = OrderString + "ActivityDate = '" + UCase(Trim(ActivityDate)) + "' "
    Ref = "And "
ElseIf (Len(ActivityDate) = 1) Then
    OrderString = OrderString + "ActivityDate >= '" + UCase(Trim(ActivityDate)) + "' "
    Ref = "And "
End If
If (ActivityPatientId > 0) Then
        OrderString = OrderString + Ref + "PatientId =" + str(ActivityPatientId) + " "
    Ref = "And "
End If
If (Trim(ActivityStatus) <> "") Then
    If (Left(ActivityStatus, 1) = "-") Then
        ActivityStatus = Mid(ActivityStatus, 2, Len(ActivityStatus) - 1)
        If (Len(Trim(ActivityStatus)) = 1) Then
            OrderString = OrderString + Ref + "Status <> '" + UCase(Trim(ActivityStatus)) + "' "
            Ref = "And "
        Else
            Temp = ""
            For i = 1 To Len(ActivityStatus)
                If (i = 1) Then
                    Temp = Temp + "(Status <> '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "' "
                ElseIf (i = Len(ActivityStatus)) Then
                    Temp = Temp + "And Status <> '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "') "
                Else
                    Temp = Temp + "And Status <> '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "' "
                End If
            Next i
            OrderString = OrderString + Ref + Temp
            Ref = "And "
        End If
    Else
        If (Len(Trim(ActivityStatus)) = 1) Then
            OrderString = OrderString + Ref + "Status = '" + UCase(Trim(ActivityStatus)) + "' "
            Ref = "And "
        Else
            Temp = ""
            For i = 1 To Len(ActivityStatus)
                If (i = 1) Then
                    Temp = Temp + "(Status = '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "' "
                ElseIf (i = Len(ActivityStatus)) Then
                    Temp = Temp + "Or Status = '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "') "
                Else
                    Temp = Temp + "Or Status = '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "' "
                End If
            Next i
            OrderString = OrderString + Ref + Temp
            Ref = "And "
        End If
    End If
End If
If IType = 2 Then
    Select Case ActivityLocationId
    Case Is < 0
        OrderString = OrderString
    Case 0
        OrderString = OrderString & Ref & "(LocationId = 0 OR LocationId > 1000)" & " "
        Ref = "And "
    Case Else
                OrderString = OrderString + Ref + "LocationId =" + str(ActivityLocationId) + " "
        Ref = "And "
    End Select
Else
    If (ActivityLocationId >= 0) Then
            OrderString = OrderString + Ref + "LocationId =" + str(ActivityLocationId) + " "
        Ref = "And "
    End If
End If
If (ActivityResourceId > 0) Then
        OrderString = OrderString + Ref + "ResourceId =" + str(ActivityResourceId) + " "
    Ref = "And "
ElseIf (ActivityResourceId < 0) Then
        OrderString = OrderString + Ref + "ResourceId =" + str(-1 * ActivityResourceId) + " "
    Ref = "And "
End If
If (ActivityAppointmentId <> 0) Then
        OrderString = OrderString + Ref + "AppointmentId =" + str(ActivityAppointmentId) + " "
    Ref = "And "
Else
    OrderString = OrderString + Ref + "AppointmentId <> 0 "
End If
If (ActivityStationId > 0) Then
        OrderString = OrderString + Ref + "StationId =" + str(ActivityStationId) + " "
    Ref = "And "
ElseIf (ActivityStationId = -1) Then
    OrderString = OrderString + Ref + "StationId = 0 "
    Ref = "And "
End If
If (IType = 0) Or IType = 2 Then
    OrderString = OrderString + "ORDER BY ActivityDate ASC, ActivityStatusTRef ASC, ActivityId ASC "
Else
    OrderString = OrderString + "ORDER BY ActivityId DESC "
End If
Set ActivityTbl = Nothing
Set ActivityTbl = CreateAdoRecordset
If MyPracticeRepository Is Nothing Then
    GetActivityList = -1
    Exit Function
End If
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call ActivityTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (ActivityTbl.EOF) Then
    GetActivityList = -1
    Call InitActivity
Else
    ActivityTbl.MoveLast
    ActivityTotal = ActivityTbl.RecordCount
    GetActivityList = ActivityTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetActivityList"
    GetActivityList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetActivitybyDate(Ref As Integer) As Long
On Error GoTo DbErrorHandler
Dim i As Integer
Dim Temp As String
Dim FindIt As String
Dim OrderIt As String
Dim OrderString As String
ActivityTotal = 0
GetActivitybyDate = -1
If (Trim(ActivityDate) = "") Then
    Exit Function
End If
If (Ref = 0) Then
    FindIt = "="
    OrderIt = "ASC"
ElseIf (Ref = -1) Then
    FindIt = "<="
    OrderIt = "DESC"
ElseIf (Ref = 1) Then
    FindIt = ">="
    OrderIt = "ASC"
Else
    FindIt = "="
    OrderIt = "ASC"
End If
OrderString = "Select * FROM PracticeActivity WHERE ActivityDate " + FindIt + "'" + UCase(Trim(ActivityDate)) + "' "
If (ActivityPatientId > 0) Then
        OrderString = OrderString + "And PatientId =" + str(ActivityPatientId) + " "
End If
If (Trim(ActivityStatus) <> "") Then
    If (Len(Trim(ActivityStatus)) = 1) Then
        OrderString = OrderString + "And Status = '" + UCase(Trim(ActivityStatus)) + "' "
    Else
        Temp = ""
        For i = 1 To Len(ActivityStatus)
            If (i = 1) Then
                Temp = Temp + "(Status = '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "' "
            ElseIf (i = Len(ActivityStatus)) Then
                Temp = Temp + "Or Status = '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "') "
            Else
                Temp = Temp + "Or Status = '" + UCase(Trim(Mid(ActivityStatus, i, 1))) + "' "
            End If
        Next i
        OrderString = OrderString + "And " + Temp
    End If
End If
OrderString = OrderString + "ORDER BY ActivityDate " + OrderIt + ", ActivityId DESC "
Set ActivityTbl = Nothing
Set ActivityTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call ActivityTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (ActivityTbl.EOF) Then
    GetActivitybyDate = -1
    Call InitActivity
Else
    ActivityTbl.MoveLast
    ActivityTotal = ActivityTbl.RecordCount
    GetActivitybyDate = ActivityTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetActivitybyDate"
    GetActivitybyDate = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutActivity()
On Error GoTo DbErrorHandler
PutActivity = True
If (ActivityNew) Then
    ActivityTbl.AddNew
End If
ActivityTbl("AppointmentId") = ActivityAppointmentId
ActivityTbl("PatientId") = ActivityPatientId
ActivityTbl("StationId") = ActivityStationId
ActivityTbl("ActivityDate") = ActivityDate
ActivityTbl("Status") = ActivityStatus
ActivityTbl("ActivityStatusTime") = ActivityStatusTime
ActivityTbl("ActivityStatusTRef") = ActivityStatusTRef
ActivityTbl("ResourceId") = ActivityResourceId
ActivityTbl("LocationId") = ActivityLocationId
ActivityTbl("QuestionSet") = Trim(ActivityQuestionSet)
If (ActivityTechConfirm) Then
    ActivityTbl("TechConfirmed") = "Y"
Else
    ActivityTbl("TechConfirmed") = "N"
End If
ActivityTbl("CurrentRId") = ActivityCurrResId
ActivityTbl("PayAmount") = ActivityPayAmount
ActivityTbl("PayMethod") = Trim(ActivityPayMethod)
ActivityTbl("PayReference") = Trim(ActivityPayRef)
ActivityTbl.Update
If (ActivityNew) Then
    ActivityNew = False
    ActivityId = GetJustAddedRecord
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutActivity"
    PutActivity = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetActivityList(1) > 0) Then
    If (SelectActivity(1)) Then
        GetJustAddedRecord = ActivityTbl("ActivityId")
    End If
End If
End Function

Public Function ApplyActivity() As Boolean
ApplyActivity = PutActivity
End Function

Public Function DeleteActivity() As Boolean
DeleteActivity = PutActivity
End Function

Public Function RetrieveActivity() As Boolean
RetrieveActivity = GetActivity
End Function

Public Function SelectActivity(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectActivity = False
If (ActivityTbl.EOF) Or (ListItem < 1) Or (ListItem > ActivityTotal) Then
    Exit Function
End If
SelectActivity = True
ActivityTbl.MoveFirst
ActivityTbl.Move ListItem - 1
Call LoadActivity
Exit Function
DbErrorHandler:
    ModuleName = "SelectActivity"
    SelectActivity = False
    ActivityId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindNextActivity() As Long
FindNextActivity = GetActivitybyDate(1)
End Function

Public Function FindPreviousActivity() As Long
FindPreviousActivity = GetActivitybyDate(-1)
End Function

Public Function FindActivityforCopay() As Long
FindActivityforCopay = GetActivityList(1)
End Function

Public Function FindActivity() As Long
FindActivity = GetActivityList(0)
End Function

Public Function FindActivityAnywhere() As Long
FindActivityAnywhere = GetActivityList(2)
End Function

Public Function IsRoomOpen() As Boolean
Dim i As Integer
Dim ThoseFound As Integer
IsRoomOpen = False
If (ActivityResourceId < 1) Then
    Exit Function
End If
If (Trim(ActivityDate) = "") Then
    Exit Function
End If
ActivityAppointmentId = 0
ActivityStationId = 0
ActivityPatientId = 0
ThoseFound = GetActivityList(0)
If (ThoseFound < 1) Then
    IsRoomOpen = True
    Exit Function
Else
    i = 1
    Do Until Not (SelectActivity(i))
        If (ActivityStatus = "E") Then
            Exit Function
        End If
        i = i + 1
    Loop
    IsRoomOpen = True
End If
End Function

Public Function RetrievePreviousActivity() As Boolean
Dim OrgDate As String
Dim OrgActId As Long
Dim TotalFound As Long
RetrievePreviousActivity = False
OrgDate = ActivityDate
OrgActId = ActivityId
TotalFound = GetActivitybyDate(-1)
If (TotalFound > 1) Then
    RetrievePreviousActivity = SelectActivity(1)
    If (ActivityId = OrgActId) Then
        RetrievePreviousActivity = SelectActivity(2)
    End If
ElseIf (TotalFound = 1) Then
    RetrievePreviousActivity = SelectActivity(1)
    If (OrgDate = ActivityDate) Then
        RetrievePreviousActivity = False
    End If
End If
End Function

Public Function RetrieveNextActivity() As Boolean
Dim TotalFound As Long
RetrieveNextActivity = False
TotalFound = GetActivitybyDate(1)
If (TotalFound > 1) Then
    RetrieveNextActivity = SelectActivity(2)
End If
End Function

Public Function SetPermission(UserId As String) As Boolean
    On Error GoTo DbErrorHandler
    SetPermission = False
    Dim OrderString As String
    Dim AccessValue As Integer

Set EmergencypermissionTbl = Nothing
Set EmergencypermissionTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdStoredProc
    MyPracticeRepositoryCmd.CommandText = "USP_EmergencyPermission"

 Set MyParameters = Nothing
    Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@Flag")
    MyParameters.Type = adVarChar
    MyParameters.Direction = adParamInput
    MyParameters.Size = 10
    MyParameters.Value = "set"
    MyPracticeRepositoryCmd.Parameters()(0) = MyParameters
    
   Set MyParameters = Nothing
    Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@ResourceId")
    MyParameters.Type = adInteger
    MyParameters.Direction = adParamInput
    MyParameters.Size = 10
    MyParameters.Value = Int(val(Trim(UserId)))
    MyPracticeRepositoryCmd.Parameters()(1) = MyParameters

    Call EmergencypermissionTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, adCmdStoredProc)

    If Not (EmergencypermissionTbl.EOF) Then
        AccessValue = EmergencypermissionTbl.Fields("val").Value
    End If

    If (AccessValue = 1) Then
        SetPermission = True
    End If

    Exit Function
DbErrorHandler:
    ModuleName = "SetPermission"
    GetActivity = False
    ActivityId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
