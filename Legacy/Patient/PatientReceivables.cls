VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientReceivables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Receivables Object Module
Option Explicit
Public PatientId As Long
Public InsurerId As Long
Public InsuredId As Long
Public AppointmentId As Long
Public ReceivableId As Long
Public ReceivableType As String
Public ReceivableInvoice As String
Public ReceivableInvoiceDate As String
Public ReceivableAmount As Single
Public ReceivableFeeAmount As Single
Public ReceivableBalance As Single
Public ReceivableUnAllocated As Single
Public ReceivableBillToDr As Long
Public ReceivableRefDr As Long
Public ReceivableAccidentType As String
Public ReceivableAccidentState As String
Public ReceivableHospitalAdmission As String
Public ReceivableHospitalDismissal As String
Public ReceivableDisability As String
Public ReceivableRsnType As String
Public ReceivableRsnDate As String
Public ReceivableConsDate As String
Public ReceivablePrevCond As String
Public ReceivableExternalRefInfo As String
Public ReceivableOver90 As String
Public ReceivableWriteOff As String
Public ReceivableBillOffice As Long
Public ReceivablePatientBillDate As String
Public ReceivableLastPayDate As String
Public ReceivableInsurerDesignation As String
Private mReceivableInsurerDesignation As String
Public ReceivableStatus As String

Private ModuleName As String
Private PatientReceivableTotal As Long
Private PatientReceivableTbl As ADODB.Recordset
Private PatientReceivableNew As Boolean

Private Sub Class_Initialize()
Set PatientReceivableTbl = CreateAdoRecordset
Call InitPatientReceivable
End Sub


Private Sub Class_Terminate()
Call InitPatientReceivable
Set PatientReceivableTbl = Nothing
End Sub

Private Sub InitPatientReceivable()
PatientReceivableNew = True
PatientId = 0
InsurerId = 0
InsuredId = 0
AppointmentId = 0
ReceivableType = ""
ReceivableInvoice = ""
ReceivableInvoiceDate = ""
ReceivableAmount = 0
ReceivableFeeAmount = 0
ReceivableBalance = 0
ReceivableUnAllocated = 0
ReceivableBillToDr = 0
ReceivableRefDr = 0
ReceivableAccidentType = ""
ReceivableAccidentState = ""
ReceivableHospitalAdmission = ""
ReceivableHospitalDismissal = ""
ReceivableDisability = ""
ReceivableRsnType = ""
ReceivableRsnDate = ""
ReceivableConsDate = ""
ReceivablePrevCond = ""
ReceivableExternalRefInfo = ""
ReceivableOver90 = ""
ReceivableBillOffice = 0
ReceivablePatientBillDate = ""
ReceivableLastPayDate = ""
ReceivableInsurerDesignation = ""
mReceivableInsurerDesignation = ""
ReceivableStatus = "A"
End Sub

Private Sub LoadPatientReceivable()
PatientReceivableNew = False
If (PatientReceivableTbl("ReceivableId") <> "") Then
    ReceivableId = PatientReceivableTbl("ReceivableId")
Else
    ReceivableId = 0
End If
If (PatientReceivableTbl("PatientId") <> "") Then
    PatientId = PatientReceivableTbl("PatientId")
Else
    PatientId = 0
End If
If (PatientReceivableTbl("AppointmentId") <> "") Then
    AppointmentId = PatientReceivableTbl("AppointmentId")
Else
    AppointmentId = 0
End If
If (PatientReceivableTbl("InsurerId") <> "") Then
    InsurerId = PatientReceivableTbl("InsurerId")
Else
    InsurerId = 0
End If
If (PatientReceivableTbl("InsuredId") <> "") Then
    InsuredId = PatientReceivableTbl("InsuredId")
Else
    InsuredId = 0
End If
If (PatientReceivableTbl("ReceivableType") <> "") Then
    ReceivableType = PatientReceivableTbl("ReceivableType")
Else
    ReceivableType = ""
End If
If (PatientReceivableTbl("Invoice") <> "") Then
    ReceivableInvoice = PatientReceivableTbl("Invoice")
Else
    ReceivableInvoice = ""
End If
If (PatientReceivableTbl("InvoiceDate") <> "") Then
    ReceivableInvoiceDate = PatientReceivableTbl("InvoiceDate")
Else
    ReceivableInvoiceDate = ""
End If
If (PatientReceivableTbl("PatientBillDate") <> "") Then
    ReceivablePatientBillDate = PatientReceivableTbl("PatientBillDate")
Else
    ReceivablePatientBillDate = ""
End If
If (PatientReceivableTbl("Charge") <> "") Then
    ReceivableAmount = PatientReceivableTbl("Charge")
Else
    ReceivableAmount = 0
End If
If (PatientReceivableTbl("ChargesByFee") <> "") Then
    ReceivableFeeAmount = PatientReceivableTbl("ChargesByFee")
Else
    ReceivableFeeAmount = 0
End If
If (PatientReceivableTbl("OpenBalance") <> "") Then
    ReceivableBalance = PatientReceivableTbl("OpenBalance")
Else
    ReceivableBalance = 0
End If
If (PatientReceivableTbl("UnallocatedBalance") <> "") Then
    ReceivableUnAllocated = PatientReceivableTbl("UnallocatedBalance")
Else
    ReceivableUnAllocated = 0
End If
If (PatientReceivableTbl("BillToDr") <> "") Then
    ReceivableBillToDr = PatientReceivableTbl("BillToDr")
Else
    ReceivableBillToDr = 0
End If
If (PatientReceivableTbl("ReferDr") <> "") Then
    ReceivableRefDr = PatientReceivableTbl("ReferDr")
Else
    ReceivableRefDr = 0
End If
If (PatientReceivableTbl("AccType") <> "") Then
    ReceivableAccidentType = PatientReceivableTbl("AccType")
Else
    ReceivableAccidentType = ""
End If
If (PatientReceivableTbl("AccState") <> "") Then
    ReceivableAccidentState = PatientReceivableTbl("AccState")
Else
    ReceivableAccidentState = ""
End If
If (PatientReceivableTbl("HspAdmDate") <> "") Then
    ReceivableHospitalAdmission = PatientReceivableTbl("HspAdmDate")
Else
    ReceivableHospitalAdmission = ""
End If
If (PatientReceivableTbl("HspDisDate") <> "") Then
    ReceivableHospitalDismissal = PatientReceivableTbl("HspDisDate")
Else
    ReceivableHospitalDismissal = ""
End If
If (PatientReceivableTbl("Disability") <> "") Then
    ReceivableDisability = PatientReceivableTbl("Disability")
Else
    ReceivableDisability = ""
End If
If (PatientReceivableTbl("RsnType") <> "") Then
    ReceivableRsnType = PatientReceivableTbl("RsnType")
Else
    ReceivableRsnType = ""
End If
If (PatientReceivableTbl("RsnDate") <> "") Then
    ReceivableRsnDate = PatientReceivableTbl("RsnDate")
Else
    ReceivableRsnDate = ""
End If
If (PatientReceivableTbl("FirstConsDate") <> "") Then
    ReceivableConsDate = PatientReceivableTbl("FirstConsDate")
Else
    ReceivableConsDate = ""
End If
If (PatientReceivableTbl("PrevCond") <> "") Then
    ReceivablePrevCond = PatientReceivableTbl("PrevCond")
Else
    ReceivablePrevCond = ""
End If
If (PatientReceivableTbl("ExternalRefInfo") <> "") Then
    ReceivableExternalRefInfo = PatientReceivableTbl("ExternalRefInfo")
Else
    ReceivableExternalRefInfo = ""
End If
If (PatientReceivableTbl("Over90") <> "") Then
    ReceivableOver90 = PatientReceivableTbl("Over90")
Else
    ReceivableOver90 = ""
End If
If (PatientReceivableTbl("WriteOff") <> "") Then
    ReceivableWriteOff = PatientReceivableTbl("WriteOff")
Else
    ReceivableWriteOff = ""
End If
If (PatientReceivableTbl("BillingOffice") <> "") Then
    ReceivableBillOffice = PatientReceivableTbl("BillingOffice")
Else
    ReceivableBillOffice = 0
End If
If (PatientReceivableTbl("LastPayDate") <> "") Then
    ReceivableLastPayDate = PatientReceivableTbl("LastPayDate")
Else
    ReceivableLastPayDate = ""
End If
If (PatientReceivableTbl("InsurerDesignation") <> "") Then
    ReceivableInsurerDesignation = PatientReceivableTbl("InsurerDesignation")
Else
    ReceivableInsurerDesignation = ""
End If
mReceivableInsurerDesignation = ReceivableInsurerDesignation
If (PatientReceivableTbl("Status") <> "") Then
    ReceivableStatus = PatientReceivableTbl("Status")
Else
    ReceivableStatus = ""
End If
End Sub

Private Function GetReceivablesbyDoctor() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetReceivablesbyDoctor = -1
OrderString = "Select * FROM PatientReceivables WHERE BillToDr = " + Str(ReceivableBillToDr) + " "
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If (PatientReceivableTbl.EOF) Then
    GetReceivablesbyDoctor = -1
Else
    PatientReceivableTbl.MoveLast
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
    GetReceivablesbyDoctor = PatientReceivableTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetReceivablesbyDoctor"
    GetReceivablesbyDoctor = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetReceivablesbyRefDoctor() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetReceivablesbyRefDoctor = -1
OrderString = "Select * FROM PatientReceivables WHERE ReferDr = " + Str(ReceivableRefDr) + " "
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If (PatientReceivableTbl.EOF) Then
    GetReceivablesbyRefDoctor = -1
Else
    PatientReceivableTbl.MoveLast
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
    GetReceivablesbyRefDoctor = PatientReceivableTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetReceivablesbyDoctor"
    GetReceivablesbyRefDoctor = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetReceivablesbyInsurer() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetReceivablesbyInsurer = -1
OrderString = "Select * FROM PatientReceivables WHERE InsurerId = " + Str(InsurerId) + " "
If (PatientId > 0) Then
    OrderString = OrderString + "And PatientId = " + Str(PatientId) + " "
End If
If (InsuredId > 0) Then
    OrderString = OrderString + "And InsuredId = " + Str(InsuredId) + " "
End If
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If (PatientReceivableTbl.EOF) Then
    GetReceivablesbyInsurer = -1
Else
    PatientReceivableTbl.MoveLast
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
    GetReceivablesbyInsurer = PatientReceivableTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetReceivablesbyInsurer"
    GetReceivablesbyInsurer = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetReceivables(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
Dim Srt As String
Dim Ref As String
Srt = ""
Ref = ""
PatientReceivableTotal = 0
GetReceivables = -1
If (PatientId < 1) And (AppointmentId < 1) And (Trim(ReceivableInvoice) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PatientReceivables WHERE "
If (PatientId > 0) Then
    OrderString = OrderString + Ref + "PatientId = " + Str(PatientId) + " "
    Ref = "And "
    Srt = "ORDER BY InsurerDesignation DESC, InvoiceDate DESC, Invoice DESC "
    If (IType = 1) Then
        Srt = "ORDER BY Invoice DESC, InvoiceDate DESC "
    End If
End If
If (AppointmentId > 0) Then
    OrderString = OrderString + Ref + "AppointmentId = " + Str(AppointmentId) + " "
    Ref = "And "
    Srt = "ORDER BY PatientId ASC, AppointmentId DESC, InsurerDesignation DESC "
End If
If (Trim(ReceivableInvoice) <> "") Then
    If (Len(ReceivableInvoice) = 1) Then
        OrderString = OrderString + Ref + "Invoice >= '" + Trim(ReceivableInvoice) + "' "
        Srt = "ORDER BY PatientId ASC, Invoice DESC, InsurerDesignation DESC, ReceivableId ASC "
    Else
        OrderString = OrderString + Ref + "Invoice = '" + Trim(ReceivableInvoice) + "' "
        Srt = "ORDER BY InsurerDesignation DESC, ReceivableId ASC, PatientId ASC, Invoice DESC"
    End If
    Ref = "And "
End If
If (Trim(ReceivableType) <> "") Then
    OrderString = OrderString + Ref + "ReceivableType = '" + Trim(ReceivableType) + "' "
    Ref = "And "
    Srt = "ORDER BY PatientId ASC, AppointmentId DESC, InsurerDesignation DESC "
End If
If (Trim(ReceivableInsurerDesignation) <> "") Then
    OrderString = OrderString + Ref + "InsurerDesignation = '" + Trim(ReceivableInsurerDesignation) + "' "
    Ref = "And "
    Srt = "ORDER BY PatientId ASC, AppointmentId DESC "
End If
If (Trim(ReceivableStatus) <> "") Then
    OrderString = OrderString + Ref + "Status = '" + Trim(ReceivableStatus) + "' "
    Ref = "And "
    Srt = "ORDER BY PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC, ReceivableId ASC "
End If
If (Trim(ReceivableInvoiceDate) <> "") Then
    OrderString = OrderString + Ref + "InvoiceDate = '" + Trim(ReceivableInvoiceDate) + "' "
    Ref = "And "
    Srt = "ORDER BY PatientId ASC, InvoiceDate DESC, InsurerDesignation DESC "
End If
If (InsurerId > 0) Then
    OrderString = OrderString + Ref + "InsurerId = " + Str(InsurerId) + " "
    Srt = "ORDER BY InsurerId ASC"
    Ref = "And "
End If
If (InsuredId > 0) Then
    OrderString = OrderString + Ref + "InsuredId = " + Str(InsuredId) + " "
    If (InsuredId <> PatientId) Then
        Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC "
    End If
    Ref = "And "
End If
If (ReceivableBalance > 0) And (InsurerId > 0) Then
    OrderString = OrderString + Ref + "OpenBalance <> 0 "
    Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC "
    Ref = "And "
End If
If (IType = -9) Then
    OrderString = OrderString + Ref + "((OpenBalance > 0.01) Or (OpenBalance < -0.01)) "
    Srt = "ORDER BY InsurerId ASC, PatientId ASC, InvoiceDate DESC, Invoice DESC, InsurerDesignation DESC "
    Ref = "And "
End If
OrderString = OrderString + Srt
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset

MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If (PatientReceivableTbl.EOF) Then
    GetReceivables = -1
Else
    PatientReceivableTbl.MoveLast
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
    GetReceivables = PatientReceivableTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetReceivables"
    GetReceivables = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetReceivablesbyDate(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
Dim Srt As String
Dim Ref As String
Srt = ""
Ref = ""
PatientReceivableTotal = 0
GetReceivablesbyDate = -1
If (Trim(ReceivableInvoiceDate) = "") Then
    Exit Function
End If
If (IType = -1) Then
    OrderString = "Select * FROM PatientReceivables WHERE InvoiceDate = '" + Trim(ReceivableInvoiceDate) + "' "
Else
    OrderString = "Select * FROM PatientReceivables WHERE InvoiceDate <= '" + Trim(ReceivableInvoiceDate) + "' "
End If
If (PatientId > 0) Then
    OrderString = OrderString + "And PatientId = " + Str(PatientId) + " "
End If
If (Trim(ReceivableType) <> "") Then
    OrderString = OrderString + "And ReceivableType = '" + Trim(ReceivableType) + "' "
End If
Srt = "ORDER BY InvoiceDate DESC "
OrderString = OrderString + Srt
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If (PatientReceivableTbl.EOF) Then
    GetReceivablesbyDate = -1
Else
    PatientReceivableTbl.MoveLast
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
    GetReceivablesbyDate = PatientReceivableTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetReceivablesbyDate"
    GetReceivablesbyDate = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function


Public Function GetPatientReceivableByInvoice(Invoice As String) As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
OrderString = "SELECT * FROM PatientReceivables WHERE invoice = '" & Invoice & "'"
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend

If (PatientReceivableTbl.EOF) Then
    GetPatientReceivableByInvoice = False
Else
    GetPatientReceivableByInvoice = True
    PatientReceivableTbl.MoveLast
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
End If


Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivableByInvoice"
    GetPatientReceivableByInvoice = False
    ReceivableId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function


Private Function GetPatientReceivable() As Boolean
On Error GoTo DbErrorHandler
GetPatientReceivable = True
Dim OrderString As String
If Trim(Str(ReceivableId)) = "0" Then
OrderString = "SELECT * FROM PatientReceivables WHERE 1 = 0"
Else
OrderString = "SELECT * FROM PatientReceivables WHERE ReceivableId = " + Trim(Str(ReceivableId))
End If
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If (PatientReceivableTbl.EOF) Then
    Call InitPatientReceivable
Else
    PatientReceivableTotal = PatientReceivableTbl.RecordCount
    Call LoadPatientReceivable
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReceivable"
    GetPatientReceivable = False
    ReceivableId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientReceivable() As Boolean
On Error GoTo DbErrorHandler
PutPatientReceivable = True
If (PatientReceivableNew) Then
    PatientReceivableTbl.AddNew
End If
PatientReceivableTbl("PatientId") = PatientId
PatientReceivableTbl("AppointmentId") = AppointmentId
PatientReceivableTbl("InsurerId") = InsurerId
PatientReceivableTbl("InsuredId") = InsuredId
PatientReceivableTbl("ReceivableType") = UCase(Trim(ReceivableType))
PatientReceivableTbl("Invoice") = Trim(ReceivableInvoice)
PatientReceivableTbl("InvoiceDate") = Trim(ReceivableInvoiceDate)
PatientReceivableTbl("PatientBillDate") = Trim(ReceivablePatientBillDate)
PatientReceivableTbl("BillToDr") = ReceivableBillToDr
PatientReceivableTbl("ReferDr") = ReceivableRefDr
PatientReceivableTbl("AccType") = ReceivableAccidentType
PatientReceivableTbl("AccState") = ReceivableAccidentState
PatientReceivableTbl("HspAdmDate") = ReceivableHospitalAdmission
PatientReceivableTbl("HspDisDate") = ReceivableHospitalDismissal
PatientReceivableTbl("Disability") = ReceivableDisability
PatientReceivableTbl("RsnType") = ReceivableRsnType
PatientReceivableTbl("RsnDate") = ReceivableRsnDate
PatientReceivableTbl("FirstConsDate") = ReceivableConsDate
PatientReceivableTbl("PrevCond") = ReceivablePrevCond
PatientReceivableTbl("ExternalRefInfo") = ReceivableExternalRefInfo
PatientReceivableTbl("Over90") = ReceivableOver90
PatientReceivableTbl("WriteOff") = ReceivableWriteOff
PatientReceivableTbl("BillingOffice") = ReceivableBillOffice
PatientReceivableTbl("LastPayDate") = ReceivableLastPayDate
PatientReceivableTbl("InsurerDesignation") = ReceivableInsurerDesignation
' These fields are calculated at the DB, unnecessary to update.
' PatientReceivableTbl("OpenBalance") = Round(ReceivableBalance, 2)
' PatientReceivableTbl("UnallocatedBalance") = Round(ReceivableUnAllocated, 2)
' PatientReceivableTbl("Status") = ReceivableStatus
' PatientReceivableTbl("ChargesByFee") = ReceivableFeeAmount
' PatientReceivableTbl("Charge") = ReceivableAmount

Dim tbl As IO_Practiceware_Interop.AdoRecordset
Set tbl = PatientReceivableTbl
tbl.UpdateIfChanged


If (PatientReceivableNew) Then
    ReceivableId = GetJustAddedRecord
    PatientReceivableNew = False
    Call InsertLog("PatientReceivables.cls", "Created New Bill for Patient" & CStr(PatientId) & "", LoginCatg.BillGen, "", "", 0, AppointmentId)
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientReceivable"
    PutPatientReceivable = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function KillReceivables() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillReceivables = True
OrderString = "SELECT * FROM PatientReceivables "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PatientReceivableTbl.EOF) Then
    PatientReceivableTbl.MoveFirst
    While Not (PatientReceivableTbl.EOF)
        PatientReceivableTbl.Delete
        PatientReceivableTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillReceivables"
    KillReceivables = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetReceivables(0) > 0) Then
    If (SelectPatientReceivable(1)) Then
        GetJustAddedRecord = PatientReceivableTbl("ReceivableId")
    End If
End If
End Function

Public Function ApplyPatientReceivable() As Boolean
ApplyPatientReceivable = PutPatientReceivable
End Function

Public Function KillReceivable() As Boolean
KillReceivable = KillReceivables
End Function

Public Function DeletePatientReceivable() As Boolean
ReceivableStatus = "D"
DeletePatientReceivable = PutPatientReceivable
End Function

Public Function RetrievePatientReceivable() As Boolean
RetrievePatientReceivable = GetPatientReceivable
End Function

Public Function SelectPatientReceivable(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectPatientReceivable = False
If (PatientReceivableTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientReceivableTotal) Then
    Exit Function
End If
PatientReceivableTbl.MoveFirst
PatientReceivableTbl.Move ListItem - 1
SelectPatientReceivable = True
Call LoadPatientReceivable
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientReceivable"
    SelectPatientReceivable = False
    ReceivableId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPatientReceivableAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
SelectPatientReceivableAggregate = False
If (PatientReceivableTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientReceivableTotal) Then
    Exit Function
End If
PatientReceivableTbl.MoveFirst
PatientReceivableTbl.Move ListItem - 1
SelectPatientReceivableAggregate = True
Call LoadPatientReceivable
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientReceivableAggregate"
    SelectPatientReceivableAggregate = False
    ReceivableId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientReceivableBlast() As Long
FindPatientReceivableBlast = GetReceivables(-9)
End Function

Public Function FindPatientReceivable() As Long
FindPatientReceivable = GetReceivables(0)
End Function

Public Function FindPatientReceivablebyDate() As Long
FindPatientReceivablebyDate = GetReceivablesbyDate(0)
End Function

Public Function FindPatientReceivablebyExactDate() As Long
FindPatientReceivablebyExactDate = GetReceivablesbyDate(-1)
End Function

Public Function FindPatientReceivablebyDoctor() As Long
FindPatientReceivablebyDoctor = GetReceivablesbyDoctor
End Function

Public Function FindPatientReceivablebyRefDoctor() As Long
FindPatientReceivablebyRefDoctor = GetReceivablesbyRefDoctor
End Function

Public Function FindPatientReceivablebyInsurer() As Long
FindPatientReceivablebyInsurer = GetReceivablesbyInsurer
End Function

Public Function FindPatientReceivablebyInvoice() As Long
FindPatientReceivablebyInvoice = GetReceivables(1)
End Function


Public Function GetDummyReceivable() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetDummyReceivable = 0
OrderString = "Select * FROM PatientReceivables WHERE appointmentid = 0 " & _
        " and patientid = " + Str(PatientId) + " "

Set PatientReceivableTbl = Nothing
Set PatientReceivableTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
MyPracticeRepositoryCmd.ConvertToParameterizedSql
Call PatientReceivableTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
While MyPracticeRepositoryCmd.Parameters().Count > 0
    MyPracticeRepositoryCmd.Parameters().Delete (0)
Wend
If Not PatientReceivableTbl.EOF Then
    GetDummyReceivable = PatientReceivableTbl("Receivableid")
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDummyReceivable"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function UpdateInvoiceDr(ByVal DrId As Long, ByVal InvoiceId As String, ByVal IsBillingDr As Boolean) As Boolean
On Error GoTo DbErrorHandler
Dim SQLString As String
UpdateInvoiceDr = False
Dim RS As Recordset

If (IsBillingDr) Then
    SQLString = "UPDATE PatientReceivables Set BillToDr= " & DrId & " " & _
        " Where Invoice='" & InvoiceId & "' "
Else
    SQLString = "UPDATE PatientReceivables Set ReferDr= " & DrId & " " & _
        " Where Invoice='" & InvoiceId & "' "
End If
Set RS = GetRS(SQLString)
UpdateInvoiceDr = True
Exit Function
DbErrorHandler:
    ModuleName = "UpdateInvoiceDr"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function UpdateInvoiceDate(ByVal InvoiceId As String, ByVal Invoicedate As String) As Boolean
On Error GoTo DbErrorHandler
Dim SQLString As String
UpdateInvoiceDate = False
Dim RS As Recordset

SQLString = "UPDATE PatientReceivables Set InvoiceDate= " & Invoicedate & " " & _
        " Where Invoice='" & InvoiceId & "' "
Set RS = GetRS(SQLString)
UpdateInvoiceDate = True
Exit Function
DbErrorHandler:
    ModuleName = "UpdateInvoiceDate"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function
