VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ManagedDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The ManagedDate Object Module
' Public Interface
Option Explicit
Public ExposedDate As String
Public ExposedDate1 As String
Public ExposedDate2 As String

' Hidden
Private TheDate As String

Private Sub Class_Initialize()
TheDate = ""
End Sub

Private Sub Class_Terminate()
TheDate = ""
End Sub

Private Function VerifyDisplayDate() As Boolean
Dim k As Integer, y As Integer
Dim Day As String
Dim Month As String
Dim Year As String
Dim Leap As Boolean
Month = ""
Year = ""
Day = ""
Leap = False
VerifyDisplayDate = False
k = InStrPS(TheDate, "/")
If (k = 0) Or ((k <> 2) And (k <> 3)) Then
    Exit Function
End If
Month = Mid(TheDate, 1, k - 1)
y = InStrPS(k + 1, TheDate, "/")
If (y = 0) Then
    Exit Function
Else
    Day = Mid(TheDate, k + 1, y - (k + 1))
    Year = Mid(TheDate, y + 1, Len(TheDate) - y)
End If
If (Len(Month) = 1) Then
    Month = "0" + Left(Month, 1)
End If
If (Len(Day) = 1) Then
    Day = "0" + Left(Day, 1)
End If
If (Len(Year) <> 4) Or (Len(Day) <> 2) Or (Len(Month) <> 2) Then
    Exit Function
Else
    k = Val(Year) / 4
    If ((k * 4) = Val(Year)) Then
        Leap = True
    End If
End If
If (Val(Month) < 1) Or (Val(Month) > 12) Then
    Exit Function
End If
If (Val(Day) < 1) Or (Val(Day) > 31) Then
    Exit Function
End If
If (Val(Month) = 2) Then
    If (Leap) And (Val(Day) > 29) Then
        Exit Function
    ElseIf Not (Leap) And (Val(Day) > 28) Then
        Exit Function
    End If
End If
TheDate = Month + "/" + Day + "/" + Year
VerifyDisplayDate = True
End Function

Private Function VerifyManagedDate() As Boolean
Dim k As Integer
Dim Day As String
Dim Month As String
Dim Year As String
Dim Leap As Boolean
Month = ""
Year = ""
Day = ""
Leap = False
VerifyManagedDate = False
If (Len(Trim(TheDate)) <> 8) Then
    Exit Function
End If
For k = 1 To Len(TheDate)
    If (Mid(TheDate, k, 1) < "0") Or (Mid(TheDate, k, 1) > "9") Then
        k = Len(TheDate)
        Exit Function
    End If
Next k
Month = Mid(TheDate, 5, 2)
Day = Mid(TheDate, 7, 2)
Year = Mid(TheDate, 1, 4)
k = Val(Year) / 4
If ((k * 4) = Val(Year)) Then
    Leap = True
End If
If (Val(Month) < 1) Or (Val(Month) > 12) Then
    Exit Function
End If
If (Val(Day) < 1) Or (Val(Day) > 31) Then
    Exit Function
End If
If (Val(Month) = 2) Then
    If (Leap) And (Val(Day) > 29) Then
        Exit Function
    ElseIf Not (Leap) And (Val(Day) > 28) Then
        Exit Function
    End If
End If
VerifyManagedDate = True
End Function

Public Function ConvertManagedDateToDisplayDate() As Boolean
Dim Day As String
Dim Month As String
Dim Year As String
ConvertManagedDateToDisplayDate = False
TheDate = ExposedDate
If (VerifyManagedDate()) Then
    Month = Mid(TheDate, 5, 2)
    Day = Mid(TheDate, 7, 2)
    Year = Left(TheDate, 4)
    TheDate = Month + "/" + Day + "/" + Year
    ConvertManagedDateToDisplayDate = True
    ExposedDate = TheDate
Else
    ExposedDate = ""
End If
End Function

Public Function ConvertDisplayDateToManagedDate() As Boolean
Dim Day As String
Dim Month As String
Dim Year As String
ConvertDisplayDateToManagedDate = False
TheDate = ExposedDate
If (VerifyDisplayDate()) Then
    Month = Left(TheDate, 2)
    Day = Mid(TheDate, 4, 2)
    Year = Mid(TheDate, 7, 4)
    TheDate = Year + Month + Day
    ConvertDisplayDateToManagedDate = True
    ExposedDate = TheDate
Else
    ExposedDate = ""
End If
End Function

Public Function DaysBetweenManagedDates() As Long
DaysBetweenManagedDates = -1
End Function

Public Function DaysBetweenDisplayDates() As Long
DaysBetweenDisplayDates = -1
End Function

