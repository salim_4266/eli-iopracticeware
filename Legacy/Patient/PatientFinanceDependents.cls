VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientFinanceDependents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Financial Dependents Object Module
Option Explicit
Public DependentId As Long
Public DependentPatientId As Long
Public DependentPolicyHolderId As Long
Public DependentInsurerId As Long
Public DependentMember As String
Public DependentSubscriber As String

Private ModuleName As String
Private PatientFinancialDependentsTotal As Long
Private PatientFinancialDependentsTbl As ADODB.Recordset
Private PatientFinancialDependentsNew As Boolean

Private Sub Class_Initialize()
Set PatientFinancialDependentsTbl = CreateAdoRecordset
Call InitPatientFinance
End Sub

Private Sub Class_Terminate()
Call InitPatientFinance
Set PatientFinancialDependentsTbl = Nothing
End Sub

Private Sub InitPatientFinance()
PatientFinancialDependentsNew = True
DependentPatientId = 0
DependentPolicyHolderId = 0
DependentInsurerId = 0
DependentMember = ""
DependentSubscriber = ""
End Sub

Private Sub LoadPatientFinance()
PatientFinancialDependentsNew = False
If (PatientFinancialDependentsTbl("DependentId") <> "") Then
    DependentId = PatientFinancialDependentsTbl("DependentId")
Else
    DependentId = 0
End If
If (PatientFinancialDependentsTbl("PatientId") <> "") Then
    DependentPatientId = PatientFinancialDependentsTbl("PatientId")
Else
    DependentPatientId = 0
End If
If (PatientFinancialDependentsTbl("PolicyHolderId") <> "") Then
    DependentPolicyHolderId = PatientFinancialDependentsTbl("PolicyHolderId")
Else
    DependentPolicyHolderId = 0
End If
If (PatientFinancialDependentsTbl("InsurerId") <> "") Then
    DependentInsurerId = PatientFinancialDependentsTbl("InsurerId")
Else
    DependentInsurerId = 0
End If
If (PatientFinancialDependentsTbl("MemberId") <> "") Then
    DependentMember = PatientFinancialDependentsTbl("MemberId")
Else
    DependentMember = ""
End If
If (PatientFinancialDependentsTbl("SubscriberId") <> "") Then
    DependentSubscriber = PatientFinancialDependentsTbl("SubscriberId")
Else
    DependentSubscriber = ""
End If
End Sub

Private Function GetPatientFinancialDependents() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
Dim Ref As String
Ref = ""
PatientFinancialDependentsTotal = 0
GetPatientFinancialDependents = -1
OrderString = "Select * FROM PatientFinancialDependents WHERE "
If (DependentPatientId > 0) Then
    OrderString = OrderString + Ref + "PatientId =" + Str(DependentPatientId) + " "
    Ref = "And "
End If
If (DependentInsurerId > 0) Then
    OrderString = OrderString + Ref + "InsurerId =" + Str(DependentInsurerId) + " "
    Ref = "And "
End If
If (DependentPolicyHolderId > 0) Then
    OrderString = OrderString + Ref + "PolicyHolderId =" + Str(DependentPolicyHolderId) + " "
End If
OrderString = OrderString + "ORDER BY PatientId ASC"
Set PatientFinancialDependentsTbl = Nothing
Set PatientFinancialDependentsTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientFinancialDependentsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialDependentsTbl.EOF) Then
    GetPatientFinancialDependents = -1
    Call InitPatientFinance
Else
    PatientFinancialDependentsTbl.MoveLast
    PatientFinancialDependentsTotal = PatientFinancialDependentsTbl.RecordCount
    GetPatientFinancialDependents = PatientFinancialDependentsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientFinancialDependents"
    GetPatientFinancialDependents = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientFinancialDependentsbyPolicy() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
PatientFinancialDependentsTotal = 0
GetPatientFinancialDependentsbyPolicy = -1
If (Trim(DependentMember) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PatientFinancialDependents WHERE MemberId = '" + Trim(DependentMember) + "' "
OrderString = OrderString + "ORDER BY PatientId ASC"
Set PatientFinancialDependentsTbl = Nothing
Set PatientFinancialDependentsTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientFinancialDependentsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialDependentsTbl.EOF) Then
    GetPatientFinancialDependentsbyPolicy = -1
    Call InitPatientFinance
Else
    PatientFinancialDependentsTbl.MoveLast
    PatientFinancialDependentsTotal = PatientFinancialDependentsTbl.RecordCount
    GetPatientFinancialDependentsbyPolicy = PatientFinancialDependentsTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientFinancialDependentsbyPolicy"
    GetPatientFinancialDependentsbyPolicy = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientFinance() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientFinance = True
OrderString = "SELECT * FROM PatientFinancialDependents WHERE DependentId =" + Str(DependentId) + " "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set PatientFinancialDependentsTbl = Nothing
Set PatientFinancialDependentsTbl = CreateAdoRecordset
Call PatientFinancialDependentsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialDependentsTbl.EOF) Then
    Call InitPatientFinance
Else
    Call LoadPatientFinance
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientFinance"
    GetPatientFinance = False
    DependentId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientFinance() As Boolean
On Error GoTo DbErrorHandler
PutPatientFinance = True
If (PatientFinancialDependentsNew) Then
    PatientFinancialDependentsTbl.AddNew
End If
PatientFinancialDependentsTbl("PatientId") = DependentPatientId
PatientFinancialDependentsTbl("PolicyHolderId") = DependentPolicyHolderId
PatientFinancialDependentsTbl("InsurerId") = DependentInsurerId
PatientFinancialDependentsTbl("MemberId") = UCase(Trim(DependentMember))
PatientFinancialDependentsTbl("SubscriberId") = UCase(Trim(DependentSubscriber))
PatientFinancialDependentsTbl.Update
If (PatientFinancialDependentsNew) Then
    DependentId = GetJustAddedRecord
    PatientFinancialDependentsNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientFinance"
    PutPatientFinance = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function AxePatientFinance() As Boolean
On Error GoTo DbErrorHandler
AxePatientFinance = True
PatientFinancialDependentsTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "AxePatientFinance"
    AxePatientFinance = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function KillPatientFinance() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillPatientFinance = True
OrderString = "SELECT * FROM PatientFinancialDependents "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set PatientFinancialDependentsTbl = Nothing
Set PatientFinancialDependentsTbl = CreateAdoRecordset
Call PatientFinancialDependentsTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PatientFinancialDependentsTbl.EOF) Then
    PatientFinancialDependentsTbl.MoveFirst
    While Not (PatientFinancialDependentsTbl.EOF)
        PatientFinancialDependentsTbl.Delete
        PatientFinancialDependentsTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillPatientFinance"
    KillPatientFinance = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPatientFinancialDependents > 0) Then
    If (SelectPatientFinancialDependents(1)) Then
        GetJustAddedRecord = PatientFinancialDependentsTbl("DependentId")
    End If
End If
End Function

Public Function ApplyPatientFinancialDependents() As Boolean
ApplyPatientFinancialDependents = PutPatientFinance
End Function

Public Function DeletePatientFinancialDependents() As Boolean
DeletePatientFinancialDependents = AxePatientFinance
End Function

Public Function KillFinancialDependent() As Boolean
KillFinancialDependent = KillPatientFinance
End Function

Public Function RetrievePatientFinancialDependents() As Boolean
RetrievePatientFinancialDependents = GetPatientFinance
End Function

Public Function FindPatientFinancialDependents() As Long
FindPatientFinancialDependents = GetPatientFinancialDependents
End Function

Public Function SelectPatientFinancialDependents(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectPatientFinancialDependents = False
If (PatientFinancialDependentsTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientFinancialDependentsTotal) Then
    Exit Function
End If
PatientFinancialDependentsTbl.MoveFirst
PatientFinancialDependentsTbl.Move ListItem - 1
SelectPatientFinancialDependents = True
Call LoadPatientFinance
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientFinancialDependents"
    SelectPatientFinancialDependents = False
    DependentId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPatientFinancialDependentsAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
SelectPatientFinancialDependentsAggregate = False
If (PatientFinancialDependentsTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientFinancialDependentsTotal) Then
    Exit Function
End If
PatientFinancialDependentsTbl.MoveFirst
PatientFinancialDependentsTbl.Move ListItem - 1
SelectPatientFinancialDependentsAggregate = True
Call LoadPatientFinance
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientFinancialDependentsAggregate"
    SelectPatientFinancialDependentsAggregate = False
    DependentId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientFinancialDependentsbyPolicy() As Long
FindPatientFinancialDependentsbyPolicy = GetPatientFinancialDependentsbyPolicy
End Function
