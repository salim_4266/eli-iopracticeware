VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeAffiliations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Practice Affiliation Table Object Module
Option Explicit
Public AffiliationId As Long
Public AffiliationResourceId As Long
Public AffiliationResourceType As String
Public AffiliationPlanId As Long
Public AffiliationPin As String
Public AffiliationOtherPin As String
Public AffiliationOverride As Boolean
Public AffiliationLocation As Long
Public AffiliationOrgOverride As Boolean

Private ModuleName As String
Private AffiliationTbl As ADODB.Recordset
Private AffiliationTotal As Long
Private AffiliationNew As Boolean

Private Sub Class_Initialize()
Set AffiliationTbl = CreateAdoRecordset
Call InitAffiliation
End Sub

Private Sub Class_Terminate()
Call InitAffiliation
Set AffiliationTbl = Nothing
End Sub

Private Sub InitAffiliation()
AffiliationNew = True
AffiliationId = 0
AffiliationResourceId = 0
AffiliationResourceType = ""
AffiliationPlanId = 0
AffiliationPin = ""
AffiliationOtherPin = ""
AffiliationOverride = False
AffiliationOrgOverride = False
AffiliationLocation = 0
End Sub

Private Sub LoadAffiliation()
AffiliationNew = False
If (AffiliationTbl("AffiliationId") <> "") Then
    AffiliationId = AffiliationTbl("AffiliationId")
Else
    AffiliationId = 0
End If
If (AffiliationTbl("ResourceId") <> "") Then
    AffiliationResourceId = AffiliationTbl("ResourceId")
Else
    AffiliationId = 0
End If
If (AffiliationTbl("LocationId") <> "") Then
    AffiliationLocation = AffiliationTbl("LocationId")
Else
    AffiliationLocation = 0
End If
If (AffiliationTbl("ResourceType") <> "") Then
    AffiliationResourceType = AffiliationTbl("ResourceType")
Else
    AffiliationResourceType = ""
End If
If (AffiliationTbl("PlanId") <> "") Then
    AffiliationPlanId = AffiliationTbl("PlanId")
Else
    AffiliationPlanId = ""
End If
If (AffiliationTbl("Pin") <> "") Then
    AffiliationPin = AffiliationTbl("Pin")
Else
    AffiliationPin = ""
End If
If (AffiliationTbl("AlternatePin") <> "") Then
    AffiliationOtherPin = AffiliationTbl("AlternatePin")
Else
    AffiliationOtherPin = ""
End If
If (AffiliationTbl("Override") <> "") Then
    If (AffiliationTbl("Override") = "N") Then
        AffiliationOverride = False
    Else
        AffiliationOverride = True
    End If
Else
    AffiliationOverride = False
End If
If (AffiliationTbl("OrgOverride") <> "") Then
    If (AffiliationTbl("OrgOverride") = "N") Then
        AffiliationOrgOverride = False
    Else
        AffiliationOrgOverride = True
    End If
Else
    AffiliationOrgOverride = False
End If
End Sub

Public Function GetAffiliation() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetAffiliation = False
OrderString = "Select * FROM PracticeAffiliations WHERE AffiliationId =" + Str(AffiliationId) + " "
Set AffiliationTbl = Nothing
Set AffiliationTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call AffiliationTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (AffiliationTbl.EOF) Then
    Call InitAffiliation
    GetAffiliation = True
Else
    GetAffiliation = True
    Call LoadAffiliation
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAffiliation"
    GetAffiliation = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAffiliations() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim OrderString As String
Ref = ""
GetAffiliations = -1
AffiliationTotal = 0
OrderString = "Select * FROM PracticeAffiliations WHERE "
If (AffiliationResourceId <> 0) Then
    OrderString = OrderString + Ref + "ResourceId =" + Str(AffiliationResourceId) + " "
    Ref = "And "
End If
If (Trim(AffiliationResourceType) <> "") Then
    OrderString = OrderString + Ref + "ResourceType = '" + Trim(AffiliationResourceType) + "' "
    Ref = "And "
End If
If (AffiliationPlanId <> 0) Then
    OrderString = OrderString + Ref + "PlanId =" + Str(AffiliationPlanId) + " "
    Ref = "And "
End If
If (Trim(AffiliationPin) <> "") Then
    OrderString = OrderString + Ref + "Pin = '" + Trim(AffiliationPin) + "' "
    Ref = "And "
End If
If (AffiliationLocation <> 0) Then
    OrderString = OrderString + Ref + "LocationId =" + Str(AffiliationLocation) + " "
    Ref = "And "
End If
OrderString = OrderString + "ORDER BY ResourceId ASC, PlanId ASC"
Set AffiliationTbl = Nothing
Set AffiliationTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call AffiliationTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (AffiliationTbl.EOF) Then
    Call InitAffiliation
    GetAffiliations = -1
Else
    AffiliationTbl.MoveLast
    AffiliationTotal = AffiliationTbl.RecordCount
    GetAffiliations = AffiliationTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    GetAffiliations = -1
    ModuleName = "GetAffiliations"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutAffiliation() As Boolean
On Error GoTo DbErrorHandler
PutAffiliation = True
If (AffiliationNew) Then
    AffiliationTbl.AddNew
End If
AffiliationTbl("ResourceId") = AffiliationResourceId
AffiliationTbl("ResourceType") = AffiliationResourceType
AffiliationTbl("PlanId") = AffiliationPlanId
AffiliationTbl("LocationId") = AffiliationLocation
AffiliationTbl("Pin") = AffiliationPin
AffiliationTbl("AlternatePin") = AffiliationOtherPin
If (AffiliationOverride) Then
    AffiliationTbl("Override") = "Y"
Else
    AffiliationTbl("Override") = "N"
End If
If (AffiliationOrgOverride) Then
    AffiliationTbl("OrgOverride") = "Y"
Else
    AffiliationTbl("OrgOverride") = "N"
End If
AffiliationTbl.Update
If (AffiliationNew) Then
    AffiliationId = GetJustAddedRecord
    AffiliationNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutAffiliation"
    PutAffiliation = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PurgeAffiliation() As Boolean
On Error GoTo DbErrorHandler
PurgeAffiliation = True
AffiliationTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeAffiliation"
    PurgeAffiliation = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetAffiliations > 0) Then
    If (SelectAffiliation(1)) Then
        GetJustAddedRecord = AffiliationTbl("AffiliationId")
    End If
End If
End Function

Public Function SelectAffiliation(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectAffiliation = False
If (AffiliationTbl.EOF) Or (ListItem < 1) Or (ListItem > AffiliationTotal) Then
    Exit Function
End If
AffiliationTbl.MoveFirst
AffiliationTbl.Move ListItem - 1
SelectAffiliation = True
Call LoadAffiliation
Exit Function
DbErrorHandler:
    ModuleName = "SelectAffiliation"
    SelectAffiliation = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectAffiliationAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectAffiliationAggregate = False
If (AffiliationTbl.EOF) Or (ListItem < 1) Or (ListItem > AffiliationTotal) Then
    Exit Function
End If
AffiliationTbl.MoveFirst
AffiliationTbl.Move ListItem - 1
SelectAffiliationAggregate = True
Call LoadAffiliation
Exit Function
DbErrorHandler:
    ModuleName = "SelectAffiliationAggregate"
    SelectAffiliationAggregate = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function RetrieveAffiliation() As Long
RetrieveAffiliation = GetAffiliation()
End Function

Public Function FindAffiliation() As Long
FindAffiliation = GetAffiliations()
End Function

Public Function ApplyAffiliation() As Boolean
ApplyAffiliation = PutAffiliation()
End Function

Public Function DeleteAffiliation() As Boolean
DeleteAffiliation = PurgeAffiliation()
End Function

