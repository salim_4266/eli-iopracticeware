VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DILists"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Joined Lists DI
Option Explicit
Public ApplList As ListBox
Public ApplPatientId As Long
Public ApplDate As String
Public ApplStartAt As Integer
Public ApplPatName As String
Public ApplReturnCount As Integer

Private ApplDrName As String
Private ApplBillName As String
Private ApplRefDr As Long
Private ApplRefName As String
Private ApplPcpName As String
Private ApplLastName As String
Private ApplFirstName As String
Private ApplMiddleInitial As String
Private ApplApptId As Long
Private ApplApptDate As String
Private ApplClnId As Long
Private ApplNoteId As Long
Private ApplEyeContext As String
Private ApplFindingDetails As String
Private ApplNoteSystem As String
Private ApplNote1 As String
Private ApplNote2 As String
Private ApplNote3 As String
Private ApplNote4 As String
Private ApplDiagId As String
Private ApplLtr As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset

Private Sub InitList()
ApplApptId = 0
ApplDate = ""
ApplDrName = ""
ApplBillName = ""
ApplRefDr = 0
ApplRefName = ""
ApplPcpName = ""
ApplApptDate = ""
ApplClnId = 0
ApplNoteId = 0
ApplEyeContext = ""
ApplFindingDetails = ""
ApplNoteSystem = ""
ApplNote1 = ""
ApplNote2 = ""
ApplNote3 = ""
ApplNote4 = ""
ApplDiagId = ""
ApplLtr = ""
End Sub

Private Sub Class_Initialize()
Set ApplListTbl = Nothing
Set ApplListTbl = CreateAdoRecordset
Call InitList
End Sub

Private Sub Class_Terminate()
Call InitList
Set ApplListTbl = Nothing
End Sub

Private Sub LoadApplList(IType As String)
If (IType = "AD") Then
    If (ApplListTbl("AppointmentId") <> "") Then
        ApplApptId = ApplListTbl("AppointmentId")
    Else
        ApplApptId = 0
    End If
    If (ApplListTbl("LastName") <> "") Then
        ApplLastName = ApplListTbl("LastName")
    Else
        ApplLastName = ""
    End If
    If (ApplListTbl("FirstName") <> "") Then
        ApplFirstName = ApplListTbl("FirstName")
    Else
        ApplFirstName = ""
    End If
    If (ApplListTbl("MiddleInitial") <> "") Then
        ApplMiddleInitial = ApplListTbl("MiddleInitial")
    Else
        ApplMiddleInitial = ""
    End If
    If (ApplListTbl("ResourceName") <> "") Then
        ApplDrName = ApplListTbl("ResourceName")
    Else
        ApplDrName = ""
    End If
    If (ApplListTbl("RefDr") <> "") Then
        ApplRefDr = ApplListTbl("RefDr")
    Else
        ApplRefDr = 0
    End If
    If (ApplListTbl("RefName") <> "") Then
        ApplRefName = ApplListTbl("RefName")
    Else
        ApplRefName = ""
    End If
    If (ApplListTbl("AppDate") <> "") Then
        ApplApptDate = ApplListTbl("AppDate")
    Else
        ApplApptDate = ""
    End If
    If (ApplListTbl("ResourceId8") <> "") Then
        If (ApplListTbl("ResourceId8") = 999) Then
            ApplLtr = "Letter"
        Else
            ApplLtr = "No"
        End If
    Else
        ApplLtr = "No"
    End If
ElseIf (IType = "LN") Then
    If (ApplListTbl("AppDate") <> "") Then
        ApplApptDate = ApplListTbl("AppDate")
    Else
        ApplApptDate = ""
    End If
    If (ApplListTbl("AppointmentId") <> "") Then
        ApplApptId = ApplListTbl("AppointmentId")
    Else
        ApplApptId = 0
    End If
    If (ApplListTbl("ClinicalId") <> "") Then
        ApplClnId = ApplListTbl("ClinicalId")
    Else
        ApplClnId = 0
    End If
    If (ApplListTbl("NoteId") <> "") Then
        ApplNoteId = ApplListTbl("NoteId")
    Else
        ApplNoteId = 0
    End If
    If (ApplListTbl("FindingDetail") <> "") Then
        ApplFindingDetails = ApplListTbl("FindingDetail")
    Else
        ApplFindingDetails = ""
    End If
    If (ApplListTbl("NoteEye") <> "") Then
        ApplEyeContext = ApplListTbl("NoteEye")
    Else
        ApplEyeContext = ""
    End If
    If (ApplListTbl("NoteSystem") <> "") Then
        ApplNoteSystem = ApplListTbl("NoteSystem")
    Else
        ApplNoteSystem = ""
    End If
    If (ApplListTbl("Note1") <> "") Then
        ApplNote1 = ApplListTbl("Note1")
    Else
        ApplNote1 = ""
    End If
    If (ApplListTbl("Note2") <> "") Then
        ApplNote2 = ApplListTbl("Note2")
    Else
        ApplNote2 = ""
    End If
    If (ApplListTbl("Note3") <> "") Then
        ApplNote3 = ApplListTbl("Note3")
    Else
        ApplNote3 = ""
    End If
    If (ApplListTbl("Note4") <> "") Then
        ApplNote4 = ApplListTbl("Note4")
    Else
        ApplNote4 = ""
    End If
ElseIf (IType = "IM") Then
    If (ApplListTbl("AppDate") <> "") Then
        ApplApptDate = ApplListTbl("AppDate")
    Else
        ApplApptDate = ""
    End If
    If (ApplListTbl("AppointmentId") <> "") Then
        ApplApptId = ApplListTbl("AppointmentId")
    Else
        ApplApptId = 0
    End If
    If (ApplListTbl("ClinicalId") <> "") Then
        ApplClnId = ApplListTbl("ClinicalId")
    Else
        ApplClnId = 0
    End If
    If (ApplListTbl("EyeContext") <> "") Then
        ApplEyeContext = ApplListTbl("EyeContext")
    Else
        ApplEyeContext = ""
    End If
    If (ApplListTbl("FindingDetail") <> "") Then
        ApplFindingDetails = ApplListTbl("FindingDetail")
    Else
        ApplFindingDetails = ""
    End If
End If
End Sub

Private Function SelectList(ListItem As Integer, TheType As String) As Boolean
On Error GoTo DbErrorHandler
SelectList = False
If (ApplListTbl.EOF) Or (ListItem < 1) Then
    Exit Function
End If
If (TheType <> "AD") And (TheType <> "IM") And (TheType <> "LN") Then
    ApplListTbl.MoveFirst
    ApplListTbl.Move ListItem - 1
Else
    If (ListItem > 1) Then
        ApplListTbl.MoveNext
    End If
End If
Call LoadApplList(TheType)
SelectList = True
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetApptDrList() As Long
On Error GoTo DbErrorHandler
Dim PTemp(2) As String
GetApptDrList = -1
ApplListTotal = 0
If (Trim(ApplDate) <> "") And (ApplPatientId > 0) Then
    Set ApplListTbl = Nothing
    Set ApplListTbl = CreateAdoRecordset
    PTemp(1) = Trim(ApplDate)
    PTemp(2) = Trim(ApplPatientId)
    MyPracticeRepositoryCmd.CommandType = adCmdStoredProc
    MyPracticeRepositoryCmd.CommandText = "DI_ApptDrList"
    Set MyParameters = Nothing
    Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@AptDate")
    MyParameters.Type = adVarChar
    MyParameters.Direction = adParamInput
    MyParameters.Size = Len(Trim(PTemp(1)))
    MyParameters.Value = Trim(PTemp(1))
    MyPracticeRepositoryCmd.Parameters()(0) = MyParameters
    
    Set MyParameters = Nothing
    Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@PatId")
    MyParameters.Type = adInteger
    MyParameters.Direction = adParamInput
    MyParameters.Size = 4
    MyParameters.Value = Val(Trim(PTemp(2)))
    MyPracticeRepositoryCmd.Parameters()(1) = MyParameters
    Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, adCmdStoredProc)
    If Not (ApplListTbl.EOF) Then
        ApplListTotal = 1
        GetApptDrList = ApplListTotal
    End If
    Set MyParameters = Nothing
End If
Exit Function
DbErrorHandler:
    Set MyParameters = Nothing
End Function

Public Function ApplApptDrList() As Long
Dim i As Integer
Dim Ltr As String
Dim PatName As String
Dim DisplayItem As String
ApplApptDrList = GetApptDrList
If (ApplStartAt < 1) Or (ApplStartAt > ApplApptDrList) Then
    ApplStartAt = 1
End If
ApplList.Clear
i = 1
Do Until Not (SelectList(i, "AD"))
    If (Trim(ApplPatName) = "") Then
        ApplPatName = Trim(ApplFirstName) + " " + Trim(ApplMiddleInitial) + " " + Trim(ApplLastName)
    End If
    DisplayItem = Space(75)
    Mid(DisplayItem, 1, 10) = Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4)
    Mid(DisplayItem, 12, Len(Trim(ApplDrName))) = Trim(ApplDrName)
    If (ApplLtr <> "No") Then
        Mid(DisplayItem, 32, 3) = "Ltr"
    End If
    If (ApplRefDr > 0) And (i = 1) Then
        Mid(DisplayItem, 36, Len(Trim(ApplRefName)) + Len(Trim(str(ApplRefDr))) + 1) = Trim(ApplRefName) + "-" + Trim(str(ApplRefDr))
    Else
        Mid(DisplayItem, 36, Len(Trim(ApplRefName))) = Trim(ApplRefName)
    End If
    ApplList.AddItem Trim(DisplayItem)
    ApplList.ItemData(ApplList.NewIndex) = ApplApptId
    i = i + 1
Loop
ApplReturnCount = i
End Function

Private Function GetLinkNotesList() As Long
On Error GoTo DbErrorHandler
Dim Temp As String
GetLinkNotesList = -1
ApplListTotal = 0
Temp = ""
If (ApplPatientId > 0) And (Trim(ApplDate) <> "") Then
    Set ApplListTbl = Nothing
    Set ApplListTbl = CreateAdoRecordset
    Temp = "DI_LinkNotesList " + Trim(ApplDate) + ", " + Trim(str(ApplPatientId)) + " "
    Call ApplListTbl.Open(Temp, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    If Not (ApplListTbl.EOF) Then
        GetLinkNotesList = 1
    End If
End If
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplLinkNotesList() As Long
Dim i As Integer
Dim j As Integer
Dim z As Integer
Dim PostIt As Boolean
Dim Temp As String
Dim Temp1 As String
Dim DisplayItem As String
Dim RetDiag As DiagnosisMasterPrimary
ApplLinkNotesList = GetLinkNotesList
If (ApplStartAt < 1) Or (ApplStartAt > ApplLinkNotesList) Then
    ApplStartAt = 1
End If
If (ApplReturnCount < 1) Then
    ApplReturnCount = ApplLinkNotesList
End If
ApplList.Clear
i = ApplStartAt
While (SelectList(i, "LN"))
    PostIt = True
    If (InStrPS(Trim(ApplNoteSystem), Trim(ApplFindingDetails)) = 0) Or (Trim(ApplNoteSystem) = "") Then
        PostIt = False
    End If
    For j = 0 To ApplList.ListCount - 1
        If (Val(Trim(Mid(ApplList.List(j), 76, 10))) = ApplNoteId) Then
            PostIt = False
            Exit For
        End If
    Next j
    If (PostIt) Then
        Temp = ""
        DisplayItem = Space(75)
        Set RetDiag = New DiagnosisMasterPrimary
        RetDiag.PrimaryNextLevelDiagnosis = Trim(ApplFindingDetails)
        If (RetDiag.FindRealPrimarybyDiagnosisDistinct > 0) Then
            If (RetDiag.SelectPrimary(1)) Then
                If (Trim(ApplEyeContext) <> "") Then
                    Temp = ApplEyeContext + " " + Trim(RetDiag.PrimaryLingo)
                Else
                    Temp = Trim(RetDiag.PrimaryLingo)
                End If
            End If
        End If
        Set RetDiag = Nothing
        Mid(DisplayItem, 1, 10) = Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4)
        Temp = Temp + "-" + Trim(Trim(ApplNote1) + " " + Trim(ApplNote2) + " " + Trim(ApplNote3) + " " + Trim(ApplNote4))
        Call StripCharacters(Temp, Chr(13))
        Call StripCharacters(Temp, Chr(10))
        j = 1
        If (Len(Temp) < 50) Then
            Mid(DisplayItem, 12, Len(Temp)) = Temp
                DisplayItem = DisplayItem + Trim(Str(ApplNoteId))
            ApplList.AddItem DisplayItem
            ApplList.ItemData(ApplList.NewIndex) = ApplClnId
            DisplayItem = Space(75)
        Else
            While j < Len(Temp)
                Temp1 = Mid(Temp, j, 50)
                If (Mid(Temp1, Len(Temp1), 1) <> " ") Or (Len(Temp1) < 50) Then
                    For z = Len(Temp1) To 1 Step -1
                        If (Mid(Temp1, z, 1) = " ") Then
                            Temp1 = Left(Temp1, z)
                            j = j + z
                            Exit For
                        End If
                    Next z
                    If (z < 1) Then
                        j = Len(Temp) + 1
                    End If
                Else
                    j = j + 50
                End If
                Mid(DisplayItem, 12, Len(Temp1)) = Temp1
                    DisplayItem = DisplayItem + Trim(Str(ApplNoteId))
                ApplList.AddItem DisplayItem
                ApplList.ItemData(ApplList.NewIndex) = ApplClnId
                DisplayItem = Space(75)
            Wend
        End If
    End If
    i = i + 1
Wend
ApplReturnCount = i
End Function

Private Function GetImpressionsList() As Long
On Error GoTo DbErrorHandler
Dim Temp As String
GetImpressionsList = -1
ApplListTotal = 0
Temp = ""
If (ApplPatientId > 0) And (Trim(ApplDate) <> "") Then
    Set ApplListTbl = Nothing
    Set ApplListTbl = CreateAdoRecordset
    Temp = "DI_ImpressionsList " + Trim(ApplDate) + ", " + Trim(str(ApplPatientId)) + " "
    Call ApplListTbl.Open(Temp, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    If Not (ApplListTbl.EOF) Then
        GetImpressionsList = 1
    End If
End If
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplImpressionsList() As Long
Dim i As Integer
Dim j As Integer
Dim z As Integer
Dim Temp As String
Dim Temp1 As String
Dim DisplayItem As String
Dim RetDiag As DiagnosisMasterPrimary
ApplImpressionsList = GetImpressionsList
If (ApplStartAt < 1) Or (ApplStartAt > ApplImpressionsList) Then
    ApplStartAt = 1
End If
If (ApplReturnCount < 1) Then
    ApplReturnCount = ApplImpressionsList
End If
ApplList.Clear
i = 1
While (SelectList(i, "IM"))
    Temp = ""
    DisplayItem = Space(75)
    Set RetDiag = New DiagnosisMasterPrimary
    RetDiag.PrimaryNextLevelDiagnosis = Trim(ApplFindingDetails)
    If (RetDiag.FindRealPrimarybyDiagnosisDistinct > 0) Then
        If (RetDiag.SelectPrimary(1)) Then
            Temp = Trim(RetDiag.PrimaryLingo)
        End If
    End If
    Set RetDiag = Nothing
    If (Trim(Temp) <> "") Then
        Mid(DisplayItem, 1, 10) = Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4)
        Mid(DisplayItem, 12, 2) = ApplEyeContext
        Mid(DisplayItem, 15, Len(Temp)) = Temp
        ApplList.AddItem DisplayItem
        ApplList.ItemData(ApplList.NewIndex) = ApplClnId
    End If
    i = i + 1
Wend
ApplReturnCount = ApplList.ListCount
End Function

