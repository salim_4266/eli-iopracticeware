VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeConfigureInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Practice Interface Configuration Object Module
Option Explicit
Public ConfigureInterface         As String
Public ConfigureInterfaceField    As String
Public ConfigureInterfaceValue    As String
Private ModuleName                As String
Private PracticeConfigureTotal    As Long
Private PracticeConfigureTbl      As ADODB.Recordset
Private PracticeConfigureNew      As Boolean

Private Sub Class_Initialize()

    Set PracticeConfigureTbl = CreateAdoRecordset
    InitConfigure
End Sub

Private Sub Class_Terminate()

    InitConfigure
    Set PracticeConfigureTbl = Nothing
End Sub

Private Sub InitConfigure()

    PracticeConfigureNew = True
    ConfigureInterface = ""
    ConfigureInterfaceField = ""
    ConfigureInterfaceValue = ""
End Sub

Private Sub LoadConfigure()

    PracticeConfigureNew = False
    If (PracticeConfigureTbl("Interface") <> "") Then
        ConfigureInterface = PracticeConfigureTbl("Interface")
    Else
        ConfigureInterface = ""
    End If
    If (PracticeConfigureTbl("FieldReference") <> "") Then
        ConfigureInterfaceField = PracticeConfigureTbl("FieldReference")
    Else
        ConfigureInterfaceField = ""
    End If
    If (PracticeConfigureTbl("FieldValue") <> "") Then
        ConfigureInterfaceValue = PracticeConfigureTbl("FieldValue")
    Else
        ConfigureInterfaceValue = ""
    End If
End Sub

Private Function GetConfigure() As Long

Dim Ref          As String
Dim OrderString  As String
Dim sWhereClause As String

    On Error GoTo DbErrorHandler
    PracticeConfigureTotal = 0
    GetConfigure = -1
    OrderString = "Select * From PracticeInterfaceConfiguration"
    If Trim$(ConfigureInterface) <> "" Then
        sWhereClause = sWhereClause & "Interface = '" & UCase$(Trim$(ConfigureInterface)) & "' "
        Ref = "And "
    End If
    If Trim$(ConfigureInterfaceField) <> "" Then
        sWhereClause = sWhereClause & Ref & "FieldReference = '" & UCase$(Trim$(ConfigureInterfaceField)) & "' "
        Ref = "And "
    End If
    If LenB(sWhereClause) > 0 Then
        OrderString = OrderString & " Where " & sWhereClause
    End If
    OrderString = OrderString & " ORDER BY FieldReference ASC "
    Set PracticeConfigureTbl = Nothing
    Set PracticeConfigureTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    PracticeConfigureTbl.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If PracticeConfigureTbl.EOF Then
        InitConfigure
    Else
        PracticeConfigureNew = False
        PracticeConfigureTbl.MoveLast
        PracticeConfigureTotal = PracticeConfigureTbl.RecordCount
        GetConfigure = PracticeConfigureTbl.RecordCount
    End If
Exit Function

DbErrorHandler:
    ModuleName = "GetConfigure"
    PinpointPRError ModuleName, Err
End Function

Private Function PutConfigure() As Boolean

    On Error GoTo DbErrorHandler
    PutConfigure = True
    If PracticeConfigureNew Then
        PracticeConfigureTbl.AddNew
    End If
    PracticeConfigureTbl("Interface") = UCase$(Trim$(ConfigureInterface))
    PracticeConfigureTbl("FieldReference") = UCase$(Trim$(ConfigureInterfaceField))
    If (Trim$(Left$(ConfigureInterfaceField, 6)) = "SYSTEM") Then
        PracticeConfigureTbl("FieldValue") = Trim$(ConfigureInterfaceValue)
    Else
        PracticeConfigureTbl("FieldValue") = UCase$(Trim$(ConfigureInterfaceValue))
    End If
    PracticeConfigureTbl.Update
    PracticeConfigureNew = False
Exit Function

DbErrorHandler:
    ModuleName = "PutConfigure"
    PutConfigure = False
    PinpointPRError ModuleName, Err
End Function

Public Function ApplyConfigure() As Boolean

    ApplyConfigure = PutConfigure
End Function

Public Function SelectConfigure(ListItem As Long) As Boolean

    On Error GoTo DbErrorHandler
    SelectConfigure = False
    If (PracticeConfigureTbl.EOF) Or (ListItem < 1) Or (ListItem > PracticeConfigureTotal) Then
        Exit Function
    End If
    PracticeConfigureTbl.MoveFirst
    PracticeConfigureTbl.Move ListItem - 1
    SelectConfigure = True
    LoadConfigure
Exit Function

DbErrorHandler:
    ModuleName = "SelectConfigure"
    SelectConfigure = False
    PinpointPRError ModuleName, Err
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindConfigure() As Long

    FindConfigure = GetConfigure()
End Function

Public Function getFieldValue(FieldReference As String) As String
Dim SQL As String

SQL = "Select * From PracticeInterfaceConfiguration where FieldReference = '" & _
        FieldReference & "'"
Set PracticeConfigureTbl = GetRS(SQL)

If Not PracticeConfigureTbl.EOF Then
    LoadConfigure
    getFieldValue = ConfigureInterfaceValue
End If
End Function
