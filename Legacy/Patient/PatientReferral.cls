VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientReferral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Referral Object Module
Option Explicit
Public PatientId As Long
Public ReferralId As Long
Public Referral As String
Public ReferralDate As String
Public ReferralExpireDate As String
Public ReferralFromId As Long
Public ReferralToId As Long
Public ReferralVisits As Long
Public ReferralVisitsLeft As Long
Public ReferralReason As String
Public ReferralPlan As Long

Private ReferralStatus As String
Private ModuleName As String
Private PatientReferralTotal As Long
Private PatientReferralTbl As ADODB.Recordset
Private PatientReferralNew As Boolean

Private Sub Class_Initialize()
Set PatientReferralTbl = CreateAdoRecordset
Call InitPatientReferral
End Sub

Private Sub Class_Terminate()
Call InitPatientReferral
Set PatientReferralTbl = Nothing
End Sub

Private Sub InitPatientReferral()
PatientReferralNew = True
PatientId = 0
Referral = ""
ReferralDate = ""
ReferralVisits = 0
ReferralVisitsLeft = 0
ReferralToId = 0
ReferralFromId = 0
ReferralReason = ""
ReferralPlan = 0
ReferralStatus = "A"
End Sub

Private Sub LoadPatientReferral()
PatientReferralNew = False
If (PatientReferralTbl("ReferralId") <> "") Then
    ReferralId = PatientReferralTbl("ReferralId")
Else
    ReferralId = 0
End If
If (PatientReferralTbl("PatientId") <> "") Then
    PatientId = PatientReferralTbl("PatientId")
Else
    PatientId = 0
End If
If (PatientReferralTbl("Referral") <> "") Then
    Referral = PatientReferralTbl("Referral")
Else
    Referral = ""
End If
If (PatientReferralTbl("ReferralDate") <> "") Then
    ReferralDate = PatientReferralTbl("ReferralDate")
Else
    ReferralDate = ""
End If
If (PatientReferralTbl("ReferralExpireDate") <> "") Then
    ReferralExpireDate = PatientReferralTbl("ReferralExpireDate")
Else
    ReferralExpireDate = ""
End If
If (PatientReferralTbl("ReferredFromId") <> "") Then
    ReferralFromId = PatientReferralTbl("ReferredFromId")
Else
    ReferralFromId = 0
End If
If (PatientReferralTbl("ReferralVisits") <> "") Then
    ReferralVisits = PatientReferralTbl("ReferralVisits")
Else
    ReferralVisits = 0
End If
If (PatientReferralTbl("ReferralVisitsLeft") <> "") Then
    ReferralVisitsLeft = PatientReferralTbl("ReferralVisitsLeft")
Else
    ReferralVisitsLeft = 0
End If
If (PatientReferralTbl("ReferredTo") <> "") Then
    ReferralToId = PatientReferralTbl("ReferredTo")
Else
    ReferralToId = 0
End If
If (PatientReferralTbl("Reason") <> "") Then
    ReferralReason = PatientReferralTbl("Reason")
Else
    ReferralReason = ""
End If
If (PatientReferralTbl("ReferredInsurer") <> "") Then
    ReferralPlan = PatientReferralTbl("ReferredInsurer")
Else
    ReferralPlan = 0
End If
End Sub

Private Function GetPatientReferralList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim OrderString As String, WhereString As String
Dim Ref As String
Ref = ""
PatientReferralTotal = 0
GetPatientReferralList = -1
OrderString = "Select * FROM PatientReferral WHERE "
If (PatientId > 0) Then
    OrderString = OrderString + Ref + "PatientId = " + Trim(Str(PatientId)) + " "
    Ref = "And "
End If
If (Trim(ReferralExpireDate) <> "") Then
    OrderString = OrderString + Ref + "ReferralExpireDate >= '" + Trim(ReferralExpireDate) + "' "
    Ref = "And "
End If
If (Trim(ReferralDate) <> "") Then
    OrderString = OrderString + Ref + "ReferralDate <= '" + Trim(ReferralDate) + "' "
    Ref = "And "
End If
If (Trim(ReferralPlan) > 0) Then
    OrderString = OrderString + Ref + "ReferredInsurer = " + Trim(Str(ReferralPlan)) + " "
    Ref = "And "
End If
If (IType = 1) Then
    OrderString = OrderString + " ORDER BY ReferralExpireDate DESC "
ElseIf (IType = 2) Then
    OrderString = OrderString + " ORDER BY ReferralId DESC "
Else
    OrderString = OrderString + " ORDER BY ReferralDate ASC "
End If
Set PatientReferralTbl = Nothing
Set PatientReferralTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReferralTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReferralTbl.EOF) Then
    GetPatientReferralList = -1
Else
    PatientReferralTbl.MoveLast
    PatientReferralTotal = PatientReferralTbl.RecordCount
    GetPatientReferralList = PatientReferralTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReferralList"
    GetPatientReferralList = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReferralListbyDoctor() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String, WhereString As String
PatientReferralTotal = 0
GetPatientReferralListbyDoctor = -1
If (ReferralFromId < 1) Then
    Exit Function
End If
OrderString = "Select * FROM PatientReferral WHERE ReferredFromId =" + Str(ReferralFromId) + " "
OrderString = OrderString + " ORDER BY ReferralDate ASC "
Set PatientReferralTbl = Nothing
Set PatientReferralTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientReferralTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientReferralTbl.EOF) Then
    GetPatientReferralListbyDoctor = -1
Else
    PatientReferralTbl.MoveLast
    PatientReferralTotal = PatientReferralTbl.RecordCount
    GetPatientReferralListbyDoctor = PatientReferralTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReferralListbyDoctor"
    GetPatientReferralListbyDoctor = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientReferral() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientReferral = False
    OrderString = "SELECT * FROM PatientReferral WHERE ReferralId =" & Str(ReferralId)
    If (PatientId > 0) Then
        OrderString = OrderString + " And PatientId =" & Str(PatientId)
    End If
    Set PatientReferralTbl = Nothing
    Set PatientReferralTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = OrderString
    Call PatientReferralTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
    If (PatientReferralTbl.EOF) Then
        Call InitPatientReferral
    Else
        Call LoadPatientReferral
    End If
    GetPatientReferral = True
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientReferral"
    GetPatientReferral = False
    ReferralId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientReferral() As Boolean
On Error GoTo DbErrorHandler
PutPatientReferral = True
If (PatientReferralNew) Then
    PatientReferralTbl.AddNew
End If
PatientReferralTbl("PatientId") = PatientId
PatientReferralTbl("Referral") = Trim(Referral)
PatientReferralTbl("ReferralDate") = UCase(Trim(ReferralDate))
PatientReferralTbl("ReferralExpireDate") = UCase(Trim(ReferralExpireDate))
PatientReferralTbl("ReferredFromId") = ReferralFromId
PatientReferralTbl("ReferralVisits") = ReferralVisits
PatientReferralTbl("ReferralVisitsLeft") = ReferralVisitsLeft
PatientReferralTbl("ReferredTo") = ReferralToId
PatientReferralTbl("Reason") = UCase(Trim(ReferralReason))
PatientReferralTbl("ReferredInsurer") = ReferralPlan
PatientReferralTbl("Status") = ReferralStatus
PatientReferralTbl.Update
If (PatientReferralNew) Then
    PatientReferralNew = False
    ReferralId = GetJustAddedRecord
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientReferral"
    PutPatientReferral = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPatientReferralList(2) > 0) Then
    If (SelectPatientReferral(1)) Then
        GetJustAddedRecord = PatientReferralTbl("ReferralId")
    End If
End If
End Function

Public Function ApplyPatientReferral() As Boolean
ApplyPatientReferral = PutPatientReferral
End Function

Public Function DeletePatientReferral() As Boolean
ReferralStatus = "D"
DeletePatientReferral = PutPatientReferral
End Function

Public Function RetrievePatientReferral() As Boolean
RetrievePatientReferral = GetPatientReferral
End Function

Public Function SelectPatientReferral(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPatientReferral = False
If (PatientReferralTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientReferralTotal) Then
    Exit Function
End If
PatientReferralTbl.MoveFirst
PatientReferralTbl.Move ListItem - 1
SelectPatientReferral = True
Call LoadPatientReferral
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientReferral"
    SelectPatientReferral = False
    ReferralId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPatientReferralAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectPatientReferralAggregate = False
If (PatientReferralTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientReferralTotal) Then
    Exit Function
End If
PatientReferralTbl.MoveFirst
PatientReferralTbl.Move ListItem - 1
SelectPatientReferralAggregate = True
Call LoadPatientReferral
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientReferralAggregate"
    SelectPatientReferralAggregate = False
    ReferralId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientReferral() As Long
FindPatientReferral = GetPatientReferralList(0)
End Function

Public Function FindPatientReferralReverse() As Long
FindPatientReferralReverse = GetPatientReferralList(1)
End Function

Public Function FindPatientReferralbyDoctor() As Long
FindPatientReferralbyDoctor = GetPatientReferralListbyDoctor()
End Function
