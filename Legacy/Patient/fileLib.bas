Attribute VB_Name = "MFileLib"
Option Explicit
Const MAX_KILL_ATTEMPTS As Long = 5

Public Function OpenSingleFile(msCDlg As CommonDialog, DefaultDirectory As String) As String
    On Error GoTo lOpenSingleFile_Error

    msCDlg.InitDir = DefaultDirectory
    msCDlg.ShowOpen
    If (msCDlg.FileName = "") Then
        MsgBox ("No File Selected")
    Else
        OpenSingleFile = msCDlg.FileName
    End If

    Exit Function

lOpenSingleFile_Error:
    If Err.Number <> 32755 Then 'User selected Cancel
        LogError "MFileLib", "OpenSingleFile", Err, Err.Description
    End If
End Function

Public Sub SyncKill(FileToKill As String)
    KillFile FileToKill
End Sub

Public Function KillFile(sFileToKill As String)
Dim iCount As Long
    On Error GoTo lKillFile_Error

    iCount = 0
    While iCount < MAX_KILL_ATTEMPTS
        If FM.IsFileThere(sFileToKill) Then
            FM.Kill sFileToKill
        Else
            iCount = 5
        End If
        iCount = iCount + 1
    Wend

    Exit Function

lKillFile_Error:

    LogError "MFileLib", "KillFile", Err, Err.Description, "File = " & sFileToKill
End Function

Public Function MoveFile(sFileToMove As String, sNewFileName As String)
    On Error GoTo lMoveFile_Error

    FM.Name sFileToMove, sNewFileName

    Exit Function

lMoveFile_Error:

    LogError "MFileLib", "MoveFile", Err, Err.Description, sFileToMove & " to " & sNewFileName
End Function

