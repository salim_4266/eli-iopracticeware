VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientFinance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Patient Financial Object Module
Option Explicit
Public PatientId As Long
Public FinancialId As Long
Public PrimaryInsurerId As Long
Public PrimaryPerson As String
Public PrimaryGroup As String
Public PrimaryStartDate As String
Public PrimaryEndDate As String
Public PrimaryPIndicator As Integer
Public PrimaryInsType As String
Public FinancialCopay As Single
Public FinancialStatus As String
Public TRecCount As Long


Private ModuleName As String
Private PatientFinancialTotal As Long
Private PatientFinancialTbl As ADODB.Recordset
Private PatientFinancialNew As Boolean

Private Sub Class_Initialize()
Set PatientFinancialTbl = CreateAdoRecordset
Call InitPatientFinance
End Sub

Private Sub Class_Terminate()
Call InitPatientFinance
Set PatientFinancialTbl = Nothing
End Sub

Private Sub InitPatientFinance()
PatientFinancialNew = True
PrimaryInsurerId = 0
PrimaryPerson = ""
PrimaryGroup = ""
PrimaryStartDate = ""
PrimaryEndDate = ""
PrimaryPIndicator = 0
PrimaryInsType = ""
FinancialStatus = "C"
End Sub

Private Sub LoadPatientFinance()
PatientFinancialNew = False
If (PatientFinancialTbl("FinancialId") <> "") Then
    FinancialId = PatientFinancialTbl("FinancialId")
Else
    FinancialId = 0
End If
If (PatientFinancialTbl("PatientId") <> "") Then
    PatientId = PatientFinancialTbl("PatientId")
Else
    PatientId = 0
End If
If (PatientFinancialTbl("FinancialInsurerId") <> "") Then
    PrimaryInsurerId = PatientFinancialTbl("FinancialInsurerId")
Else
    PrimaryInsurerId = 0
End If
If (PatientFinancialTbl("FinancialPerson") <> "") Then
    PrimaryPerson = PatientFinancialTbl("FinancialPerson")
Else
    PrimaryPerson = ""
End If
If (PatientFinancialTbl("FinancialGroupId") <> "") Then
    PrimaryGroup = PatientFinancialTbl("FinancialGroupId")
Else
    PrimaryGroup = ""
End If
If (PatientFinancialTbl("FinancialPIndicator") <> "") Then
    PrimaryPIndicator = PatientFinancialTbl("FinancialPIndicator")
Else
    PrimaryPIndicator = 0
End If
If (PatientFinancialTbl("FinancialStartDate") <> "") Then
    PrimaryStartDate = PatientFinancialTbl("FinancialStartDate")
Else
    PrimaryStartDate = ""
End If
If (PatientFinancialTbl("FinancialEndDate") <> "") Then
    PrimaryEndDate = PatientFinancialTbl("FinancialEndDate")
Else
    PrimaryEndDate = ""
End If
If (PatientFinancialTbl("FinancialCopay") <> "") Then
    FinancialCopay = PatientFinancialTbl("FinancialCopay")
Else
    FinancialCopay = 0
End If
If (PatientFinancialTbl("Status") <> "") Then
    FinancialStatus = PatientFinancialTbl("Status")
Else
    FinancialStatus = ""
End If
If (PatientFinancialTbl("FinancialInsType") <> "") Then
    PrimaryInsType = PatientFinancialTbl("FinancialInsType")
Else
    PrimaryInsType = ""
End If
End Sub

Private Function GetPatientbyPerson() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
PatientFinancialTotal = 0
GetPatientbyPerson = -1
If (Trim(PrimaryPerson) = "") Then
    Exit Function
End If
OrderString = "Select * FROM PatientFinancial WHERE FinancialPerson = '" + Trim(PrimaryPerson) + "' "
If (Trim(PrimaryStartDate) <> "") Then
    OrderString = OrderString + "And FinancialStartDate <= '" + Trim(PrimaryStartDate) + "' "
End If
If (Trim(FinancialStatus) <> "") Then
    OrderString = OrderString + "And Status = '" + Trim(FinancialStatus) + "' "
End If
Set PatientFinancialTbl = Nothing
Set PatientFinancialTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientFinancialTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialTbl.EOF) Then
    GetPatientbyPerson = -1
    Call InitPatientFinance
Else
    PatientFinancialTbl.MoveLast
    PatientFinancialTotal = PatientFinancialTbl.RecordCount
    GetPatientbyPerson = PatientFinancialTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientbyPerson"
    GetPatientbyPerson = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientFinancial() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
PatientFinancialTotal = 0
GetPatientFinancial = -1
OrderString = "Select * FROM PatientFinancial WHERE PatientId =" + Str(PatientId) + " "
If (Trim(PrimaryInsType) <> "") Then
    OrderString = OrderString + " And FinancialInsType = '" + Trim(PrimaryInsType) + "' "
End If
If (Trim(FinancialStatus) <> "") Then
    If (FinancialStatus <> "C") Then
        OrderString = OrderString + " And Status <> 'C' "
    Else
        OrderString = OrderString + " And Status = 'C' "
    End If
End If
If (PrimaryInsurerId > 0) Then
    OrderString = OrderString + " And FinancialInsurerId =" + Str(PrimaryInsurerId) + " "
End If
If (Trim(PrimaryStartDate) <> "") Then
    OrderString = OrderString + " And FinancialStartDate <= '" + Trim(PrimaryStartDate) + "' "
'    If (Trim(PrimaryEndDate) = "") Then
'        OrderString = OrderString + " And (FinancialEndDate >= '" + Trim(PrimaryStartDate) + "' Or FinancialEndDate = '') "
'    End If
End If
If (Trim(PrimaryEndDate) <> "") Then
    OrderString = OrderString + " And FinancialEndDate >= '" + Trim(PrimaryEndDate) + "' "
End If
If (PrimaryPIndicator > 0) Then
    OrderString = OrderString + " And FinancialPIndicator =" + Str(PrimaryPIndicator) + " "
ElseIf (PrimaryPIndicator = -1) Then
    OrderString = OrderString + " And FinancialPIndicator > 1 "
ElseIf (PrimaryPIndicator = 0) Then
    OrderString = OrderString + " And FinancialPIndicator >= 1 "
End If
OrderString = OrderString + "ORDER BY FinancialInsType ASC, FinancialPIndicator ASC, Status ASC"
Set PatientFinancialTbl = Nothing
Set PatientFinancialTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString

Call PatientFinancialTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialTbl.EOF) Then
    GetPatientFinancial = -1
    Call InitPatientFinance
Else
    Dim Cntr As Long
    Cntr = recCount(PatientFinancialTbl)
    
    PatientFinancialTotal = Cntr
    GetPatientFinancial = Cntr
    
    PatientFinancialTbl.MoveLast
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientFinancial"
    GetPatientFinancial = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetAllFinancialRecords() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
PatientFinancialTotal = 0
GetAllFinancialRecords = -1
OrderString = "Select * FROM PatientFinancial "
Set PatientFinancialTbl = Nothing
Set PatientFinancialTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientFinancialTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialTbl.EOF) Then
    GetAllFinancialRecords = -1
    Call InitPatientFinance
Else
    PatientFinancialTbl.MoveLast
    PatientFinancialTotal = PatientFinancialTbl.RecordCount
    GetAllFinancialRecords = PatientFinancialTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetAllFinancialRecords"
    GetAllFinancialRecords = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientFinancialbyInsurer() As Long
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientFinancialbyInsurer = -1
OrderString = "Select * FROM PatientFinancial WHERE FinancialInsurerId =" + Str(PrimaryInsurerId) + " "
If (PatientId > 0) Then
    OrderString = OrderString + "And PatientId =" + Str(PatientId) + " "
End If
If (Trim(FinancialStatus) <> "") Then
    If (FinancialStatus = "C") Then
        OrderString = OrderString + " And Status = 'C' "
    Else
        OrderString = OrderString + " And Status <> 'C' "
    End If
End If
If (Trim(PrimaryStartDate) <> "") Then
    OrderString = OrderString + " ORDER BY Status ASC"
End If
Set PatientFinancialTbl = Nothing
Set PatientFinancialTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call PatientFinancialTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (PatientFinancialTbl.EOF) Then
    GetPatientFinancialbyInsurer = -1
    Call InitPatientFinance
Else
    PatientFinancialTbl.MoveLast
    PatientFinancialTotal = PatientFinancialTbl.RecordCount
    GetPatientFinancialbyInsurer = PatientFinancialTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientFinancialbyInsurer"
    GetPatientFinancialbyInsurer = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetPatientFinance() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetPatientFinance = True
OrderString = "SELECT * FROM PatientFinancial WHERE FinancialId =" + Str(FinancialId) + " "
If (Trim(FinancialStatus) <> "") Then
    OrderString = OrderString + "And Status = '" + UCase(Trim(FinancialStatus)) + "' "
End If
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set PatientFinancialTbl = Nothing
Set PatientFinancialTbl = CreateAdoRecordset
Call PatientFinancialTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
TRecCount = PatientFinancialTbl.RecordCount
If (PatientFinancialTbl.EOF) Then
    Call InitPatientFinance
Else
    Call LoadPatientFinance
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetPatientFinance"
    GetPatientFinance = False
    FinancialId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function PutPatientFinance() As Boolean
On Error GoTo DbErrorHandler
PutPatientFinance = True
If (PatientFinancialNew) Then
    PatientFinancialTbl.AddNew
End If
PatientFinancialTbl("PatientId") = PatientId
PatientFinancialTbl("FinancialInsurerId") = PrimaryInsurerId
PatientFinancialTbl("FinancialPerson") = UCase(Trim(PrimaryPerson))
PatientFinancialTbl("FinancialGroupId") = UCase(Trim(PrimaryGroup))
PatientFinancialTbl("FinancialPIndicator") = PrimaryPIndicator
PatientFinancialTbl("FinancialStartDate") = PrimaryStartDate
PatientFinancialTbl("FinancialEndDate") = PrimaryEndDate
PatientFinancialTbl("FinancialCopay") = FinancialCopay
PatientFinancialTbl("FinancialInsType") = PrimaryInsType
PatientFinancialTbl("Status") = FinancialStatus
PatientFinancialTbl.Update
If (PatientFinancialNew) Then
    FinancialId = GetJustAddedRecord
    PatientFinancialNew = False
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutPatientFinance"
    PutPatientFinance = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function AxePatientFinance() As Boolean
On Error GoTo DbErrorHandler
AxePatientFinance = True
PatientFinancialTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "AxePatientFinance"
    AxePatientFinance = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function KillPatientFinance() As Boolean
Dim OrderString As String
On Error GoTo DbErrorHandler
KillPatientFinance = True
OrderString = "SELECT * FROM PatientFinancial "
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set PatientFinancialTbl = Nothing
Set PatientFinancialTbl = CreateAdoRecordset
Call PatientFinancialTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (PatientFinancialTbl.EOF) Then
    PatientFinancialTbl.MoveFirst
    While Not (PatientFinancialTbl.EOF)
        PatientFinancialTbl.Delete
        PatientFinancialTbl.MoveNext
    Wend
End If
Exit Function
DbErrorHandler:
    ModuleName = "KillPatientFinance"
    KillPatientFinance = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetPatientFinancial > 0) Then
    If (SelectPatientFinancial(1)) Then
        GetJustAddedRecord = PatientFinancialTbl("FinancialId")
    End If
End If
End Function

Public Function ApplyPatientFinance() As Boolean
ApplyPatientFinance = PutPatientFinance
End Function

Public Function DeletePatientFinance() As Boolean
DeletePatientFinance = AxePatientFinance
End Function

Public Function KillFinance() As Boolean
KillFinance = KillPatientFinance
End Function

Public Function RetrievePatientFinance() As Boolean
RetrievePatientFinance = GetPatientFinance
End Function

Public Function SelectPatientFinancial(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectPatientFinancial = False
If (PatientFinancialTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientFinancialTotal) Then
    Exit Function
End If
PatientFinancialTbl.MoveFirst
PatientFinancialTbl.Move ListItem - 1
SelectPatientFinancial = True
Call LoadPatientFinance
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientFinancial"
    SelectPatientFinancial = False
    FinancialId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function SelectPatientFinancialAggregate(ListItem As Long) As Boolean
On Error GoTo DbErrorHandler
SelectPatientFinancialAggregate = False
If (PatientFinancialTbl.EOF) Or (ListItem < 1) Or (ListItem > PatientFinancialTotal) Then
    Exit Function
End If
PatientFinancialTbl.MoveFirst
PatientFinancialTbl.Move ListItem - 1
SelectPatientFinancialAggregate = True
Call LoadPatientFinance
Exit Function
DbErrorHandler:
    ModuleName = "SelectPatientFinancialAggregate"
    SelectPatientFinancialAggregate = False
    FinancialId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindPatientFinancial() As Long
FindPatientFinancial = GetPatientFinancial()
End Function

Public Function FindPatientFinancialbyInsurer() As Long
FindPatientFinancialbyInsurer = GetPatientFinancialbyInsurer()
End Function

Public Function RetrievePatientFinancialPrimary() As Boolean
Dim i As Integer
FinancialId = 0
PrimaryPIndicator = 1
FinancialStatus = "C"
RetrievePatientFinancialPrimary = False
If (GetPatientFinancial() > 0) Then
    i = PatientFinancialTotal
    If (SelectPatientFinancial(i)) Then
        RetrievePatientFinancialPrimary = True
    End If
End If
End Function

Public Function RetrievePatientFinancialAny() As Boolean
FinancialId = 0
FinancialStatus = "C"
PrimaryPIndicator = 0
PrimaryStartDate = ""
PrimaryEndDate = ""
RetrievePatientFinancialAny = False
If (GetPatientFinancial() > 0) Then
    RetrievePatientFinancialAny = True
End If
End Function

Public Function RetrievePatientFinancialOther() As Boolean
FinancialId = 0
FinancialStatus = "C"
PrimaryPIndicator = -1
RetrievePatientFinancialOther = False
If (GetPatientFinancial() > 0) Then
    RetrievePatientFinancialOther = True
End If
End Function

Public Function RetrievePatientbyPolicy(IRef As Integer) As Long
Dim MyTotal As Long
RetrievePatientbyPolicy = 0
If (Trim(PrimaryPerson) <> "") Then
    MyTotal = GetPatientbyPerson
    If (MyTotal > 0) Then
        If (IRef < 1) Then
            IRef = 1
        End If
        If (SelectPatientFinancial(IRef)) Then
            RetrievePatientbyPolicy = MyTotal
        End If
    End If
End If
End Function

Public Function FindAllFinancialRecords() As Long
FindAllFinancialRecords = GetAllFinancialRecords
End Function

