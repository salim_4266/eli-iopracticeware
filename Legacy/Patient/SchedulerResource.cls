VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SchedulerResource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The Scheduler Resource Object Module
Option Explicit
Public ResourceId As Long
Public ResourceName As String
Public ResourceType As String
Public ResourceDescription As String
Public ResourceLastName As String
Public ResourceFirstName As String
Public ResourcePid As String
Public ResourcePermissions As String
Public ResourceAddress As String
Public ResourceSuite As String
Public ResourceCity As String
Public ResourceState As String
Public ResourceZip As String
Public ResourcePhone As String
Public ResourceTaxId As String
Public ResourceEmail As String
Public ResourceServiceCode As String
Public PracticeId As Long
Public ResourceColor As Long
Public ResourceOverLoad As Long
Public ResourceCost As Single
Public ResourceVacation As String
Public ResourceSpecialty As String
Public ResourceUpin As String
Public ResourceDea As String
Public ResourceLic As String
Public ResourceDisplayCalendar As Long
Public ResourceScheduleTemplate2 As Long
Public ResourceScheduleTemplate3 As Long
Public ResourceStartTime1 As String
Public ResourceStartTime2 As String
Public ResourceStartTime3 As String
Public ResourceStartTime4 As String
Public ResourceStartTime5 As String
Public ResourceStartTime6 As String
Public ResourceStartTime7 As String
Public ResourceEndTime1 As String
Public ResourceEndTime2 As String
Public ResourceEndTime3 As String
Public ResourceEndTime4 As String
Public ResourceEndTime5 As String
Public ResourceEndTime6 As String
Public ResourceEndTime7 As String
Public ResourceSignatureFile As String
Public ResourcePOS As String
Public ResourceBillable As Boolean
Public ResourceGoDirect As Boolean
Public ResourceNPI As String

Private ResourceStatus As String
Private ModuleName As String
Private SchedulerResourceTbl As ADODB.Recordset
Private SchedulerResourceTotal As Long
Private SchedulerResourceNew As Boolean

Private Sub Class_Initialize()
Set SchedulerResourceTbl = CreateAdoRecordset
Call InitResource
End Sub

Private Sub Class_Terminate()
Call InitResource
Set SchedulerResourceTbl = Nothing
End Sub

Private Sub InitResource()
SchedulerResourceNew = True
ResourceName = ""
ResourceType = ""
ResourceDescription = ""
ResourceLastName = ""
ResourceFirstName = ""
ResourcePid = ""
ResourcePermissions = ""
ResourceAddress = ""
ResourceSuite = ""
ResourceCity = ""
ResourceState = ""
ResourceZip = ""
ResourcePhone = ""
ResourceTaxId = ""
ResourceEmail = ""
ResourceColor = 0
ResourceOverLoad = 0
ResourceCost = 0
ResourceServiceCode = ""
PracticeId = 0
ResourceSpecialty = ""
ResourceUpin = ""
ResourceDea = ""
ResourceLic = ""
ResourceDisplayCalendar = 0
ResourceScheduleTemplate2 = 0
ResourceScheduleTemplate3 = 0
ResourceVacation = ""
ResourceStartTime1 = ""
ResourceStartTime2 = ""
ResourceStartTime3 = ""
ResourceStartTime4 = ""
ResourceStartTime5 = ""
ResourceStartTime6 = ""
ResourceStartTime7 = ""
ResourceEndTime1 = ""
ResourceEndTime2 = ""
ResourceEndTime3 = ""
ResourceEndTime4 = ""
ResourceEndTime5 = ""
ResourceEndTime6 = ""
ResourceEndTime7 = ""
ResourceSignatureFile = ""
ResourcePOS = ""
ResourceBillable = True
ResourceGoDirect = True
ResourceNPI = ""
ResourceStatus = "A"
End Sub

Private Sub LoadResource()
SchedulerResourceNew = False
If (SchedulerResourceTbl("ResourceId") <> "") Then
    ResourceId = SchedulerResourceTbl("ResourceId")
Else
    ResourceId = 0
End If
If (SchedulerResourceTbl("ResourceName") <> "") Then
    ResourceName = SchedulerResourceTbl("ResourceName")
Else
    ResourceName = ""
End If
If (SchedulerResourceTbl("ResourceType") <> "") Then
    ResourceType = SchedulerResourceTbl("ResourceType")
Else
    ResourceType = ""
End If
If (SchedulerResourceTbl("ResourceDescription") <> "") Then
    ResourceDescription = SchedulerResourceTbl("ResourceDescription")
Else
    ResourceDescription = ""
End If
If (SchedulerResourceTbl("ResourceLastName") <> "") Then
    ResourceLastName = SchedulerResourceTbl("ResourceLastName")
Else
    ResourceLastName = ""
End If
If (SchedulerResourceTbl("ResourceFirstName") <> "") Then
    ResourceFirstName = SchedulerResourceTbl("ResourceFirstName")
Else
    ResourceFirstName = ""
End If
If (SchedulerResourceTbl("ResourcePid") <> "") Then
    ResourcePid = SchedulerResourceTbl("ResourcePid")
Else
    ResourcePid = ""
End If
If (SchedulerResourceTbl("ResourcePermissions") <> "") Then
    ResourcePermissions = SchedulerResourceTbl("ResourcePermissions")
Else
    ResourcePermissions = ""
End If
If (SchedulerResourceTbl("ResourceAddress") <> "") Then
    ResourceAddress = SchedulerResourceTbl("ResourceAddress")
Else
    ResourceAddress = ""
End If
If (SchedulerResourceTbl("ResourceSuite") <> "") Then
    ResourceSuite = SchedulerResourceTbl("ResourceSuite")
Else
    ResourceSuite = ""
End If
If (SchedulerResourceTbl("ResourceCity") <> "") Then
    ResourceCity = SchedulerResourceTbl("ResourceCity")
Else
    ResourceCity = ""
End If
If (SchedulerResourceTbl("ResourceState") <> "") Then
    ResourceState = SchedulerResourceTbl("ResourceState")
Else
    ResourceState = ""
End If
If (SchedulerResourceTbl("ResourceZip") <> "") Then
    ResourceZip = SchedulerResourceTbl("ResourceZip")
Else
    ResourceZip = ""
End If
If (SchedulerResourceTbl("ResourcePhone") <> "") Then
    ResourcePhone = SchedulerResourceTbl("ResourcePhone")
Else
    ResourcePhone = ""
End If
If (SchedulerResourceTbl("ResourceTaxId") <> "") Then
    ResourceTaxId = SchedulerResourceTbl("ResourceTaxId")
Else
    ResourceTaxId = ""
End If
If (SchedulerResourceTbl("ResourceEmail") <> "") Then
    ResourceEmail = SchedulerResourceTbl("ResourceEmail")
Else
    ResourceEmail = ""
End If
If (SchedulerResourceTbl("ServiceCode") <> "") Then
    ResourceServiceCode = SchedulerResourceTbl("ServiceCode")
Else
    ResourceServiceCode = ""
End If
If (SchedulerResourceTbl("PracticeId") <> "") Then
    PracticeId = SchedulerResourceTbl("PracticeId")
Else
    PracticeId = 0
End If
If (SchedulerResourceTbl("ResourceColor") <> "") Then
    ResourceColor = SchedulerResourceTbl("ResourceColor")
Else
    ResourceColor = 0
End If
If (SchedulerResourceTbl("ResourceOverLoad") <> "") Then
    ResourceOverLoad = SchedulerResourceTbl("ResourceOverLoad")
Else
    ResourceOverLoad = 0
End If
If (SchedulerResourceTbl("ResourceCost") <> "") Then
    ResourceCost = SchedulerResourceTbl("ResourceCost")
Else
    ResourceCost = 0
End If
' being used for Taxonomy Number
If (SchedulerResourceTbl("Vacation") <> "") Then
    ResourceVacation = SchedulerResourceTbl("Vacation")
Else
    ResourceVacation = ""
End If
If (SchedulerResourceTbl("ResourceSpecialty") <> "") Then
    ResourceSpecialty = SchedulerResourceTbl("ResourceSpecialty")
Else
    ResourceSpecialty = ""
End If
If (SchedulerResourceTbl("ResourceUPIN") <> "") Then
    ResourceUpin = SchedulerResourceTbl("ResourceUPIN")
Else
    ResourceUpin = ""
End If
If (SchedulerResourceTbl("ResourceDEA") <> "") Then
    ResourceDea = SchedulerResourceTbl("ResourceDEA")
Else
    ResourceDea = ""
End If
If (SchedulerResourceTbl("ResourceLicence") <> "") Then
    ResourceLic = SchedulerResourceTbl("ResourceLicence")
Else
    ResourceLic = ""
End If
If (SchedulerResourceTbl("ResourceScheduleTemplate1") <> "") Then
    ResourceDisplayCalendar = SchedulerResourceTbl("ResourceScheduleTemplate1")
Else
    ResourceDisplayCalendar = 0
End If
If (SchedulerResourceTbl("ResourceScheduleTemplate2") <> "") Then
    ResourceScheduleTemplate2 = SchedulerResourceTbl("ResourceScheduleTemplate2")
Else
    ResourceScheduleTemplate2 = 0
End If
If (SchedulerResourceTbl("ResourceScheduleTemplate3") <> "") Then
    ResourceScheduleTemplate3 = SchedulerResourceTbl("ResourceScheduleTemplate3")
Else
    ResourceScheduleTemplate3 = 0
End If
If (SchedulerResourceTbl("StartTime1") <> "") Then
    ResourceStartTime1 = SchedulerResourceTbl("StartTime1")
Else
    ResourceStartTime1 = ""
End If
If (SchedulerResourceTbl("StartTime2") <> "") Then
    ResourceStartTime2 = SchedulerResourceTbl("StartTime2")
Else
    ResourceStartTime2 = ""
End If
If (SchedulerResourceTbl("StartTime3") <> "") Then
    ResourceStartTime3 = SchedulerResourceTbl("StartTime3")
Else
    ResourceStartTime3 = ""
End If
If (SchedulerResourceTbl("StartTime4") <> "") Then
    ResourceStartTime4 = SchedulerResourceTbl("StartTime4")
Else
    ResourceStartTime4 = ""
End If
If (SchedulerResourceTbl("StartTime5") <> "") Then
    ResourceStartTime5 = SchedulerResourceTbl("StartTime5")
Else
    ResourceStartTime5 = ""
End If
If (SchedulerResourceTbl("StartTime6") <> "") Then
    ResourceStartTime6 = SchedulerResourceTbl("StartTime6")
Else
    ResourceStartTime6 = ""
End If
If (SchedulerResourceTbl("StartTime7") <> "") Then
    ResourceStartTime7 = SchedulerResourceTbl("StartTime7")
Else
    ResourceStartTime7 = ""
End If
If (SchedulerResourceTbl("EndTime1") <> "") Then
    ResourceEndTime1 = SchedulerResourceTbl("EndTime1")
Else
    ResourceEndTime1 = ""
End If
If (SchedulerResourceTbl("EndTime2") <> "") Then
    ResourceEndTime2 = SchedulerResourceTbl("EndTime2")
Else
    ResourceEndTime2 = ""
End If
If (SchedulerResourceTbl("EndTime3") <> "") Then
    ResourceEndTime3 = SchedulerResourceTbl("EndTime3")
Else
    ResourceEndTime3 = ""
End If
If (SchedulerResourceTbl("EndTime4") <> "") Then
    ResourceEndTime4 = SchedulerResourceTbl("EndTime4")
Else
    ResourceEndTime4 = ""
End If
If (SchedulerResourceTbl("EndTime5") <> "") Then
    ResourceEndTime5 = SchedulerResourceTbl("EndTime5")
Else
    ResourceEndTime5 = ""
End If
If (SchedulerResourceTbl("EndTime6") <> "") Then
    ResourceEndTime6 = SchedulerResourceTbl("EndTime6")
Else
    ResourceEndTime6 = ""
End If
If (SchedulerResourceTbl("EndTime7") <> "") Then
    ResourceEndTime7 = SchedulerResourceTbl("EndTime7")
Else
    ResourceEndTime7 = ""
End If
If (SchedulerResourceTbl("SignatureFile") <> "") Then
    ResourceSignatureFile = SchedulerResourceTbl("SignatureFile")
Else
    ResourceSignatureFile = ""
End If
If (SchedulerResourceTbl("PlaceOfService") <> "") Then
    ResourcePOS = SchedulerResourceTbl("PlaceOfService")
Else
    ResourcePOS = ""
End If
If (SchedulerResourceTbl("Billable") <> "") Then
    If (SchedulerResourceTbl("Billable") = "N") Then
        ResourceBillable = False
    Else
        ResourceBillable = True
    End If
Else
    ResourceBillable = True
End If
If (SchedulerResourceTbl("GoDirect") <> "") Then
    If (SchedulerResourceTbl("GoDirect") = "N") Then
        ResourceGoDirect = False
    Else
        ResourceGoDirect = True
    End If
Else
    ResourceGoDirect = True
End If
If (SchedulerResourceTbl("NPI") <> "") Then
    ResourceNPI = SchedulerResourceTbl("NPI")
Else
    ResourceNPI = ""
End If
If (SchedulerResourceTbl("Status") <> "") Then
    ResourceStatus = SchedulerResourceTbl("Status")
Else
    ResourceStatus = ""
End If
End Sub

Private Function GetSchedulerResource() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
GetSchedulerResource = True
OrderString = "SELECT * FROM Resources WHERE ResourceId =" + Str(ResourceId) + " "
Set SchedulerResourceTbl = Nothing
Set SchedulerResourceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerResourceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerResourceTbl.EOF) Then
    Call InitResource
Else
    Call LoadResource
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetSchedulerResource"
    GetSchedulerResource = False
    ResourceId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetResource() As Long
On Error GoTo DbErrorHandler
Dim i As Integer
Dim Ref As String
Dim OrderString As String
SchedulerResourceTotal = 0
GetResource = -1
If (Len(Trim(ResourceName)) > 1) Then
    OrderString = "Select * FROM Resources WHERE ResourceName = '" + UCase(Trim(ResourceName)) + "' "
Else
    OrderString = "Select * FROM Resources WHERE ResourceName >= '" + UCase(Trim(ResourceName)) + "' "
End If
If (Trim(ResourceType) <> "") Then
    If (Len(ResourceType) = 1) Then
        If (ResourceType = "S") Then
            OrderString = OrderString + "And ResourceType <> 'R' "
        ElseIf (ResourceType = "s") Then
            OrderString = OrderString + "And ResourceType <> 'R' And ResourceType <> 'B' "
        Else
            OrderString = OrderString + "And ResourceType = '" + UCase(Trim(ResourceType)) + "' "
        End If
    Else
        Ref = ""
        OrderString = OrderString + "And ("
        For i = 1 To Len(ResourceType)
            OrderString = OrderString + Ref + "ResourceType = '" + UCase(Mid(ResourceType, i, 1)) + "' "
            Ref = "Or "
        Next i
        OrderString = OrderString + ") "
    End If
End If
If (PracticeId > 0) Then
    OrderString = OrderString + "And PracticeId = " + Trim(Str(PracticeId)) + " "
ElseIf (PracticeId = -1) Then
    OrderString = OrderString + "And ServiceCode = '01' "
End If
OrderString = OrderString + "ORDER BY ResourceName ASC "
Set SchedulerResourceTbl = Nothing
Set SchedulerResourceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerResourceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerResourceTbl.EOF) Then
    GetResource = -1
    Call InitResource
Else
    SchedulerResourceTbl.MoveLast
    SchedulerResourceTotal = SchedulerResourceTbl.RecordCount
    GetResource = SchedulerResourceTbl.RecordCount
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetResource"
    GetResource = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetResourcebyPid() As Boolean
On Error GoTo DbErrorHandler
Dim OrderString As String
SchedulerResourceTotal = 0
GetResourcebyPid = False
If dbConnection = "" Then
    dbConnection = ServerConnectionString
End If
OrderString = "Select * FROM Resources WHERE ResourcePid = '" + Trim(ResourcePid) + "' AND ResourceType not in ('X','Y','Z') "
Set SchedulerResourceTbl = Nothing
Set SchedulerResourceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call SchedulerResourceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If Not (SchedulerResourceTbl.EOF) Then
    SchedulerResourceTbl.MoveLast
    SchedulerResourceTotal = SchedulerResourceTbl.RecordCount
    GetResourcebyPid = True
Else
    Call InitResource
End If

Exit Function
DbErrorHandler:
    ModuleName = "GetResourcebyPid"
    GetResourcebyPid = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetFacilityServiceLocation() As Boolean
On Error GoTo DbErrorHandler
Dim SQLString As String
GetFacilityServiceLocation = False
Dim sqlStr As String

sqlStr = " SELECT * FROM Resources re " & _
        " INNER JOIN PracticeName pn ON pn.PracticeId = re.PracticeId " & _
        " INNER JOIN PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC' " & _
        " WHERE re.ResourceType = 'R' AND re.ServiceCode = '02' " & _
        " AND pn.PracticeType = 'P' AND pn.LocationReference <> '' " & _
        " AND pic.FieldValue = 'T' AND re.Billable = 'Y' "

Set SchedulerResourceTbl = Nothing
Set SchedulerResourceTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = sqlStr

Call SchedulerResourceTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (SchedulerResourceTbl.EOF) Then
    GetFacilityServiceLocation = False
    Call InitResource
Else
    SchedulerResourceTotal = SchedulerResourceTbl.RecordCount
    GetFacilityServiceLocation = True
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetFacilityServiceLocation"
    GetFacilityServiceLocation = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:

End Function

Private Function PutSchedulerResource() As Boolean
On Error GoTo DbErrorHandler
PutSchedulerResource = True
If (SchedulerResourceNew) Then
    SchedulerResourceTbl.AddNew
End If
SchedulerResourceTbl("ResourceName") = UCase(Trim(ResourceName))
SchedulerResourceTbl("ResourceType") = UCase(Trim(ResourceType))
SchedulerResourceTbl("ResourceDescription") = UCase(Trim(ResourceDescription))
SchedulerResourceTbl("ResourceLastName") = UCase(Trim(ResourceLastName))
SchedulerResourceTbl("ResourceFirstName") = UCase(Trim(ResourceFirstName))
SchedulerResourceTbl("ResourcePid") = Trim(ResourcePid)
SchedulerResourceTbl("ResourcePermissions") = Trim(ResourcePermissions)
SchedulerResourceTbl("ResourceAddress") = UCase(Trim(ResourceAddress))
SchedulerResourceTbl("ResourceSuite") = UCase(Trim(ResourceSuite))
SchedulerResourceTbl("ResourceCity") = UCase(Trim(ResourceCity))
SchedulerResourceTbl("ResourceState") = UCase(Trim(ResourceState))
SchedulerResourceTbl("ResourceZip") = UCase(Trim(ResourceZip))
SchedulerResourceTbl("ResourcePhone") = UCase(Trim(ResourcePhone))
SchedulerResourceTbl("ResourceTaxId") = UCase(Trim(ResourceTaxId))
SchedulerResourceTbl("ResourceEmail") = UCase(Trim(ResourceEmail))
SchedulerResourceTbl("ServiceCode") = UCase(Trim(ResourceServiceCode))
SchedulerResourceTbl("PracticeId") = PracticeId
SchedulerResourceTbl("ResourceColor") = ResourceColor
SchedulerResourceTbl("ResourceOverLoad") = ResourceOverLoad
SchedulerResourceTbl("ResourceCost") = ResourceCost
SchedulerResourceTbl("Vacation") = ResourceVacation
SchedulerResourceTbl("ResourceSpecialty") = ResourceSpecialty
SchedulerResourceTbl("ResourceUPIN") = ResourceUpin
SchedulerResourceTbl("ResourceDEA") = ResourceDea
SchedulerResourceTbl("ResourceLicence") = ResourceLic
SchedulerResourceTbl("ResourceScheduleTemplate1") = ResourceDisplayCalendar
SchedulerResourceTbl("ResourceScheduleTemplate2") = ResourceScheduleTemplate2
SchedulerResourceTbl("ResourceScheduleTemplate3") = ResourceScheduleTemplate3
SchedulerResourceTbl("StartTime1") = UCase(Trim(ResourceStartTime1))
SchedulerResourceTbl("StartTime2") = UCase(Trim(ResourceStartTime2))
SchedulerResourceTbl("StartTime3") = UCase(Trim(ResourceStartTime3))
SchedulerResourceTbl("StartTime4") = UCase(Trim(ResourceStartTime4))
SchedulerResourceTbl("StartTime5") = UCase(Trim(ResourceStartTime5))
SchedulerResourceTbl("StartTime6") = UCase(Trim(ResourceStartTime6))
SchedulerResourceTbl("StartTime7") = UCase(Trim(ResourceStartTime7))
SchedulerResourceTbl("EndTime1") = UCase(Trim(ResourceEndTime1))
SchedulerResourceTbl("EndTime2") = UCase(Trim(ResourceEndTime2))
SchedulerResourceTbl("EndTime3") = UCase(Trim(ResourceEndTime3))
SchedulerResourceTbl("EndTime4") = UCase(Trim(ResourceEndTime4))
SchedulerResourceTbl("EndTime5") = UCase(Trim(ResourceEndTime5))
SchedulerResourceTbl("EndTime6") = UCase(Trim(ResourceEndTime6))
SchedulerResourceTbl("EndTime7") = UCase(Trim(ResourceEndTime7))
SchedulerResourceTbl("SignatureFile") = ResourceSignatureFile
SchedulerResourceTbl("PlaceOfService") = ResourcePOS
If (ResourceBillable) Then
    SchedulerResourceTbl("Billable") = "Y"
Else
    SchedulerResourceTbl("Billable") = "N"
End If
If (ResourceGoDirect) Then
    SchedulerResourceTbl("GoDirect") = "Y"
Else
    SchedulerResourceTbl("GoDirect") = "N"
End If
SchedulerResourceTbl("Status") = ResourceStatus
SchedulerResourceTbl("NPI") = ResourceNPI
SchedulerResourceTbl.Update
If (SchedulerResourceNew) Then
    ResourceId = GetJustAddedRecord
    SchedulerResourceNew = False
    If UCase(Trim(ResourceType)) = "R" Then
        Call InsertLog("SchedulerResource.Cls", "Added New Room  " & CStr(ResourceId) & "", LoginCatg.RoomInsert, "RoomId", CStr(ResourceId), 0, 0)
    Else
        Call InsertLog("SchedulerResource.Cls", "Added New Staff  " & CStr(ResourceId) & "", LoginCatg.StaffInsert, "UserId", CStr(ResourceId), 0, 0)
    End If
    Else
    If UCase(Trim(ResourceType)) = "R" Then
        Call InsertLog("SchedulerResource.Cls", "Updated Room Information  " & CStr(ResourceId) & "", LoginCatg.RoomEdit, "RoomId", CStr(ResourceId), 0, 0)
    Else
        Call InsertLog("SchedulerResource.Cls", "Updated User Information  " & CStr(ResourceId) & "", LoginCatg.StaffEdit, "UserId", CStr(ResourceId), 0, 0)
    End If
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutSchedulerResource"
    PutSchedulerResource = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetResource > 0) Then
    If (SelectResource(1)) Then
        GetJustAddedRecord = SchedulerResourceTbl("ResourceId")
    End If
End If
End Function

Private Function PurgeSchedulerResource() As Boolean
On Error GoTo DbErrorHandler
PurgeSchedulerResource = True
SchedulerResourceTbl.Delete
Exit Function
DbErrorHandler:
    ModuleName = "PurgeSchedulerResource"
    PurgeSchedulerResource = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplySchedulerResource() As Boolean
ApplySchedulerResource = PutSchedulerResource
End Function

Public Function DeleteSchedulerResource() As Boolean
DeleteSchedulerResource = PurgeSchedulerResource
End Function

Public Function RetrieveSchedulerResource() As Boolean
RetrieveSchedulerResource = GetSchedulerResource
End Function

Public Function SelectResource(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
Dim u As Integer
SelectResource = False
If (SchedulerResourceTbl.EOF) Or (ListItem < 1) Or (ListItem > SchedulerResourceTotal) Then
    Exit Function
End If
SchedulerResourceTbl.MoveFirst
SchedulerResourceTbl.Move ListItem - 1
SelectResource = True
Call LoadResource
Exit Function
DbErrorHandler:
    ModuleName = "SelectResource"
    SelectResource = False
    ResourceId = -256
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function FindResource() As Long
FindResource = GetResource()
End Function

Public Function FindResourcebyRoom() As Long
ResourceType = "R"
FindResourcebyRoom = GetResource()
End Function

Public Function FindResourcebyStaff() As Long
ResourceType = "S"
FindResourcebyStaff = GetResource()
End Function

Public Function FindResourcebyDoctor() As Long
ResourceType = "DBQ"
FindResourcebyDoctor = GetResource()
End Function

Public Function FindResourcebyDoctorTech() As Long
ResourceType = "DBT"
FindResourcebyDoctorTech = GetResource()
End Function

Public Function ResourceLoginVerification() As Boolean
ResourceLoginVerification = GetResourcebyPid()
If (ResourceLoginVerification) Then
    ResourceLoginVerification = SelectResource(1)
End If
End Function

