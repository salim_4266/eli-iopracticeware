VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DI_ListFacAdm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Application Lists Class
Option Explicit
Public ApplDate As String
Public ApplStartDate As String
Public ApplEndDate As String
Public ApplList As ListBox
Public ApplLocList As ListBox
Public ApplGrid As MSFlexGrid
Public ApplGrid1 As MSFlexGrid
Public ApplStartAt As Integer
Public ApplReturnCount As Integer
Public ApplPatId As Long
Public ApplInsurerId As Long
Public ApplResourceId As Long
Public ApplLocId As Long
Public ApplConfirmStat As String
Public ApplConfirmStatus As String
Public ApplPayStatus As String
Public ApplPatientType As Boolean
Public ApplTransactionType As String
Public ApplTransactionAction As String
Public ApplTransactionStatus As String
Public ApplTransactionRef As String
Public ApplOtherFilter As String
Public ApplReceivableType As String
Public ApplSendOn As Boolean

Private ApplReferralRequired As Boolean
Private ApplServiceCode As String
Private ApplInsType As String
Private ApplSurgeryApptId As Long
Private ApplPatientId As Long
Private ApplInsdId As Long
Private ApplLastName As String
Private ApplFirstName As String
Private ApplMiddleName As String
Private ApplNameRef As String
Private ApplSal As String
Private ApplBirthDate As String
Private ApplHPhone As String
Private ApplBPhone As String
Private ApplApptId As Long
Private ApplOrderDate As String
Private ApplApptDate As String
Private ApplApptTime As Integer
Private ApplApptComments As String
Private ApplReferralId As Long
Private ApplApptType As String
Private ApplTypeQuestion As String
Private ApplPolicyPatientId As Long
Private ApplCopay As Single
Private ApplInsCopay As Single
Private ApplResource As String
Private ApplTechId As Long
Private ApplPatType As String
Private ApplStatus As String
Private ApplActId As Long
Private ApplActStatus As String
Private ApplLocName As String
Private ApplInsName As String
Private ApplTransRcvrId As Long

Private ApplClinicalId As Long
Private ApplSymptom As String
Private ApplFinding As String
Private ApplDetail1 As String
Private ApplDetail2 As String
Private ApplFollowUp As String
Private ApplSurgery As String
Private ApplClnType As String
Private ApplClnStatus As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset

Private Sub InitList()
ApplDate = ""
ApplInsType = ""
ApplPatientId = 0
ApplInsdId = 0
ApplSurgeryApptId = 0
ApplLastName = ""
ApplFirstName = ""
ApplMiddleName = ""
ApplNameRef = ""
ApplBirthDate = ""
ApplHPhone = ""
ApplBPhone = ""
ApplApptId = 0
ApplOrderDate = ""
ApplApptDate = ""
ApplApptTime = 0
ApplApptType = ""
ApplPolicyPatientId = 0
ApplCopay = 0
ApplResource = ""
ApplTechId = 0
ApplInsCopay = 0
ApplStatus = ""
ApplActStatus = ""
ApplConfirmStatus = ""
ApplPayStatus = ""
ApplApptComments = ""
ApplTypeQuestion = ""
ApplReferralId = 0
ApplClinicalId = 0
ApplSymptom = ""
ApplFinding = ""
ApplDetail1 = ""
ApplDetail2 = ""
ApplFollowUp = ""
ApplClnType = ""
ApplClnStatus = ""
End Sub

Private Sub Class_Initialize()
Set ApplListTbl = Nothing
Set ApplListTbl = CreateAdoRecordset
Call InitList
End Sub

Private Sub Class_Terminate()
Call InitList
Set ApplListTbl = Nothing
End Sub

Private Sub LoadSurgeryList(IType As Integer)
If (ApplListTbl("AppointmentId") <> "") Then
    ApplApptId = ApplListTbl("AppointmentId")
Else
    ApplApptId = 0
End If
If (ApplListTbl("PatientId") <> "") Then
    ApplPatientId = ApplListTbl("PatientId")
Else
    ApplPatientId = 0
End If
If (ApplListTbl("ClinicalId") <> "") Then
    ApplClinicalId = ApplListTbl("ClinicalId")
Else
    ApplClinicalId = 0
End If
If (ApplListTbl("LastName") <> "") Then
    ApplLastName = ApplListTbl("LastName")
Else
    ApplLastName = ""
End If
If (ApplListTbl("FirstName") <> "") Then
    ApplFirstName = ApplListTbl("FirstName")
Else
    ApplFirstName = ""
End If
If (ApplListTbl("MiddleInitial") <> "") Then
    ApplMiddleName = ApplListTbl("MiddleInitial")
Else
    ApplMiddleName = ""
End If
If (ApplListTbl("HomePhone") <> "") Then
    ApplHPhone = ApplListTbl("HomePhone")
Else
    ApplHPhone = ""
End If
If (ApplListTbl("Symptom") <> "") Then
    ApplSymptom = ApplListTbl("Symptom")
Else
    ApplSymptom = ""
End If
If (ApplListTbl("FindingDetail") <> "") Then
    ApplFinding = ApplListTbl("FindingDetail")
Else
    ApplFinding = ""
End If
If (ApplListTbl("ImageDescriptor") <> "") Then
    ApplDetail1 = ApplListTbl("ImageDescriptor")
Else
    ApplDetail1 = ""
End If
If (ApplListTbl("ImageInstructions") <> "") Then
    ApplDetail2 = ApplListTbl("ImageInstructions")
Else
    ApplDetail2 = ""
End If
If (ApplListTbl("ClinicalType") <> "") Then
    ApplClnType = ApplListTbl("ClinicalType")
Else
    ApplClnType = ""
End If
If (ApplListTbl("Surgery") <> "") Then
    ApplSurgery = ApplListTbl("Surgery")
Else
    ApplSurgery = ""
End If
If (ApplListTbl("Status") <> "") Then
    ApplClnStatus = ApplListTbl("Status")
Else
    ApplClnStatus = ""
End If
If (ApplListTbl("AppDate") <> "") Then
    ApplApptDate = ApplListTbl("AppDate")
Else
    ApplApptDate = ""
End If
If (IType = 0) Then
    If (ApplListTbl("OrderDate") <> "") Then
        ApplOrderDate = ApplListTbl("OrderDate")
    Else
        ApplOrderDate = ""
    End If
Else
    ApplOrderDate = ApplApptDate
End If
If (IType = 0) Then
    If (ApplListTbl("SurgeryApptId") <> "") Then
        ApplSurgeryApptId = ApplListTbl("SurgeryApptId")
    Else
        ApplSurgeryApptId = 0
    End If
Else
    ApplSurgeryApptId = 0
End If
If (ApplListTbl("ResourceId2") <> "") Then
    ApplLocId = ApplListTbl("ResourceId2")
Else
    ApplLocId = 0
End If
If (ApplListTbl("ResourceName") <> "") Then
    ApplResource = ApplListTbl("ResourceName")
Else
    ApplResource = ""
End If
End Sub

Private Function SelectListFast(ListItem As Integer, IType As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectListFast = False
If Not (ApplListTbl.EOF) Then
    If (ListItem > 1) Then
        ApplListTbl.MoveNext
    End If
    Call LoadSurgeryList(IType)
    SelectListFast = True
End If
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetFacilityAdmissionsList(IType As Integer) As Long
On Error GoTo DbErrorHandler
Dim strSQL As String
Dim PTemp(5) As String
ApplListTotal = 0
GetFacilityAdmissionsList = -1
PTemp(1) = "0"
PTemp(2) = "0"
PTemp(3) = "-1"
PTemp(4) = "-1"
PTemp(5) = "0"
Set ApplListTbl = Nothing
Set ApplListTbl = CreateAdoRecordset
If (Trim(ApplEndDate) <> "") Then
    PTemp(1) = Trim(ApplEndDate)
End If
If (Trim(ApplStartDate) <> "") Then
    PTemp(2) = Trim(ApplStartDate)
End If
If (IType) Then
    If (ApplLocId > -1) Then
        PTemp(3) = Trim(str(ApplLocId))
    End If
    If (ApplResourceId > 0) Then
        PTemp(4) = Trim(str(ApplResourceId))
    End If
End If
If (ApplConfirmStat <> "") Then
    PTemp(5) = ApplConfirmStat
End If
If (IType = 0) Then
    strSQL = "usp_SurgeryTransactions " + PTemp(1) + "," + PTemp(2) + "," + PTemp(3) + "," + PTemp(4) + "," + PTemp(5)
ElseIf (IType = 1) Then
    strSQL = "usp_PendingSurgery " + PTemp(1) + "," + PTemp(2) + "," + PTemp(3) + "," + PTemp(4) + "," + PTemp(5)
Else
    strSQL = "usp_PendingSurgeryAged " + PTemp(1) + "," + PTemp(2) + "," + PTemp(3) + "," + PTemp(4) + "," + PTemp(5)
End If
Call ApplListTbl.Open(strSQL, MyPracticeRepository)
If Not (ApplListTbl.EOF) Then
    ApplListTotal = 1
    GetFacilityAdmissionsList = ApplListTotal
End If
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function ApplRetrieveFacilityAdminsList(IType As Integer) As Long
Dim ProceedOn As Boolean
Dim ALoc1 As Long
Dim k As Integer, Cnt As Integer
Dim i As Integer, j As Integer
Dim MyStatus As String
Dim ADr As String
Dim ALoc As String, ADate As String
Dim ARem As String, ARef As String
Dim AEye As String, AName As String
Dim Temp As String, PatName As String
Dim DisplayItem As String
Dim RetApp As SchedulerAppointment
Dim RetSCln As PatientClinicalSurgeryPlan
MyStatus = ""
If (ApplConfirmStat = "0") Then
    MyStatus = "0"
    ApplConfirmStat = ""
End If
ALoc1 = ApplLocId
ApplRetrieveFacilityAdminsList = GetFacilityAdmissionsList(IType)
ApplGrid.Clear
DisplayItem = "S" + vbTab _
            + "St" + vbTab _
            + "Order Date" + vbTab _
            + "PatId" + vbTab _
            + "Patient Name        " + vbTab _
            + "Surgery                         " + vbTab _
            + "Surgery Date" + vbTab _
            + "Doctor   " + vbTab _
            + "Location    " + vbTab _
            + "PatId" + vbTab _
            + "AptId" + vbTab _
            + "AptSId" + vbTab _
            + "TrnId"
ApplGrid.FormatString = DisplayItem
ApplGrid.Rows = 2
Cnt = 0
k = 1
While (SelectListFast(k, IType))
    ProceedOn = True
    If (IType = 0) Then
        If ((ApplConfirmStat = "") Or (ApplConfirmStat = "0")) And (InStrPS("CZ", ApplSurgery) > 0) Then
            ProceedOn = False
        End If
    End If
    If (ProceedOn) Then
        ADr = ""
        ARem = ""
        ALoc = ""
        ADate = ""
        PatName = Trim(Trim(ApplLastName) + ", " + Trim(ApplFirstName) + " " + Trim(ApplMiddleName))
        ARef = Trim(ApplFinding)
        i = InStrPS(ARef, "-3/")
        If (i > 0) Then
            ARef = Mid(ARef, i + 3, Len(ARef) - (i + 2))
        End If
        i = InStrPS(ARef, "-F/")
        If (i > 0) Then
            ARem = Mid(ARef, 1, i - 1)
        End If
        i = InStrPS(ARef, "-1/")
        If (i > 0) Then
            If (i + 2 = Len(ARef)) Then
                j = InStrPS(ARef, "-3/")
                If (j > 0) Then
                    ADate = Mid(ARef, j + 3, (Len(ARef) - 3) - (j + 2))
                End If
            ElseIf (InStrPS(i, ARef, "-7/") = Len(ARef) - 2) Then
                j = InStrPS(ARef, "-3/")
                If (j > 0) Then
                    ADate = Mid(ARef, j + 3, (i - 1) - (j + 2))
                End If
            Else
                j = InStrPS(ARef, "-7/")
                If (j > 0) Then
                    ADate = Mid(ARef, i + 3, (j - 1) - (i + 2))
                Else
                    ADate = Mid(ARef, i + 3, Len(ARef) - (i + 2))
                End If
            End If
        End If
        Call ReplaceCharacters(ADate, "- ", "~")
        Call StripCharacters(ADate, "~")
        i = InStrPS(ARef, "-7/")
        If (i > 0) Then
            If (i + 2 = Len(ARef)) Then
                j = InStrPS(ARef, "-1/")
                If (j > 0) Then
                    ALoc = Mid(ARef, j + 3, (i - 1) - (j + 2))
                    i = InStrPS(ALoc, "(")
                    If (i > 0) Then
                        j = InStrPS(i, ALoc, ")")
                        If (j > 0) Then
                            ADr = Mid(ALoc, i + 1, (j - 1) - i)
                        Else
                            ADr = Mid(ALoc, i + 1, Len(ALoc) - i)
                        End If
                    End If
                End If
            Else
                ALoc = Mid(ARef, i + 3, Len(ARef) - (i + 2))
                i = InStrPS(ALoc, "(")
                If (i > 0) Then
                    j = InStrPS(i, ALoc, ")")
                    If (j > 0) Then
                        ADr = Mid(ALoc, i + 1, (j - 1) - i)
                    Else
                        ADr = Mid(ALoc, i + 1, Len(ALoc) - i)
                    End If
                End If
            End If
        End If
        If (Trim(ADr) <> "") Then
            ApplResource = ADr
        Else
            If (IType <> 0) Then
                ApplResource = ""
            End If
        End If
        If (InStrPS(UCase(ARem), UCase(ApplOtherFilter)) > 0) Or (Trim(ApplOtherFilter) = "") Then
            If (IType = 0) Then
                DisplayItem = " " + vbTab _
                            + ApplSurgery + vbTab _
                            + Mid(ApplOrderDate, 5, 2) + "/" + Mid(ApplOrderDate, 7, 2) + "/" + Left(ApplOrderDate, 4) + vbTab _
                            + Trim(str(ApplPatientId)) + vbTab _
                            + PatName + vbTab _
                            + ARem + vbTab _
                            + ADate + vbTab _
                            + ApplResource + vbTab _
                            + ALoc + vbTab _
                            + Trim(str(ApplApptId)) + vbTab _
                            + Trim(str(ApplSurgeryApptId)) + vbTab _
                            + Trim(str(ApplClinicalId))
            Else
                DisplayItem = " " + vbTab _
                            + ApplSurgery + vbTab _
                            + Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4) + vbTab _
                            + Trim(str(ApplPatientId)) + vbTab _
                            + PatName + vbTab _
                            + ARem + vbTab _
                            + ADate + vbTab _
                            + ApplResource + vbTab _
                            + ALoc + vbTab _
                            + Trim(str(ApplApptId)) + vbTab _
                            + Trim(str(ApplSurgeryApptId)) + vbTab _
                            + Trim(str(ApplClinicalId))
            End If
            Cnt = Cnt + 1
            ApplGrid.AddItem DisplayItem, Cnt
        End If
    End If
    k = k + 1
Wend
' Retrieve any surgery dates
' Also verify facility
If (IType = 0) Then
    For j = 1 To ApplGrid.Rows - 1
        If (Val(ApplGrid.TextMatrix(j, 11)) > 0) Then
            Set RetSCln = New PatientClinicalSurgeryPlan
            RetSCln.SurgeryClnId = Val(ApplGrid.TextMatrix(j, 11))
            RetSCln.SurgeryRefType = "F"
            RetSCln.SurgeryStatus = "A"
            If (RetSCln.FindPatientClinicalPlan > 0) Then
                If (RetSCln.SelectPatientClinicalPlan(1)) Then
                    Set RetApp = New SchedulerAppointment
                    RetApp.AppointmentId = Val(RetSCln.SurgeryValue)
                    If (RetApp.RetrieveSchedulerAppointment) Then
                        If (InStrPS("PRAD", RetApp.AppointmentStatus) > 0) Then
                            Temp = Mid(RetApp.AppointmentDate, 5, 2) + "/" + Mid(RetApp.AppointmentDate, 7, 2) + "/" + Left(RetApp.AppointmentDate, 4)
                            ApplGrid.TextMatrix(j, 6) = Temp
                            Call GetDoctor(RetApp.AppointmentResourceId1, Temp)
                            ApplGrid.TextMatrix(j, 7) = Temp
                            Call GetLocation(RetApp.AppointmentResourceId2, Temp)
                            ApplGrid.TextMatrix(j, 8) = Temp
                        Else
                            ApplGrid.TextMatrix(j, 0) = "D"
                        End If
                    End If
' Filter by Surgery Date
                    If (Trim(ApplStartDate) <> "") And (Trim(ApplEndDate) <> "") And (Trim(ApplGrid.TextMatrix(j, 0)) = "") Then
                        If (ApplStartDate <= RetApp.AppointmentDate) And (ApplEndDate >= RetApp.AppointmentDate) Then
                            ApplGrid.TextMatrix(j, 0) = ""
                        Else
                            ApplGrid.TextMatrix(j, 0) = "D"
                        End If
                    Else
                        If (MyStatus = "0") Then
                            ApplGrid.TextMatrix(j, 0) = "D"
                        End If
                    End If
' Filter by Location if necessary
                    If (ALoc1 > 0) And (ApplGrid.TextMatrix(j, 0) = "") Then
                        If (ALoc1 <> RetApp.AppointmentResourceId2) Then
                            ApplGrid.TextMatrix(j, 0) = "D"
                        End If
                    End If
' Filter by Doctor if necessary
'                If (ApplGrid.TextMatrix(j, 0) = "") Then
'                    If (ApplResourceId <> RetApp.AppointmentResourceId1) Then
'                        ApplGrid.TextMatrix(j, 0) = "D"
'                    End If
'                End If
                    Set RetApp = Nothing
                End If
            Else
                ApplGrid.TextMatrix(j, 0) = "N"
            End If
            Set RetSCln = Nothing
        End If
    Next j
    For j = ApplGrid.Rows - 1 To 1 Step -1
        If (ApplGrid.TextMatrix(j, 0) = "D") Then
            ApplGrid.RemoveItem j
        ElseIf (ApplGrid.TextMatrix(j, 0) = "N") And (ApplStartDate <> "") Then
            ApplGrid.RemoveItem j
        Else
            ApplGrid.TextMatrix(j, 0) = ""
        End If
    Next j
Else        ' this pass is necessary to extract any stray scheduled items in progress
    For j = 1 To ApplGrid.Rows - 1
        If (Val(ApplGrid.TextMatrix(j, 11)) > 0) Then
            Set RetSCln = New PatientClinicalSurgeryPlan
            RetSCln.SurgeryClnId = Val(ApplGrid.TextMatrix(j, 11))
            RetSCln.SurgeryRefType = "F"
            RetSCln.SurgeryStatus = "A"
            If (RetSCln.FindPatientClinicalPlan > 0) Then
                ApplGrid.TextMatrix(j, 0) = "D"
            End If
            Set RetSCln = Nothing
        End If
    Next j
    For j = ApplGrid.Rows - 1 To 1 Step -1
        If (ApplGrid.TextMatrix(j, 0) = "D") Then
            ApplGrid.RemoveItem j
        Else
            ApplGrid.TextMatrix(j, 0) = ""
        End If
    Next j
End If
End Function

Public Function ApplGetCodesbyList(CodeType As String, SetIt As Boolean, ViewAll As Boolean, ListBox As ListBox) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
Set RetrieveCode = New PracticeCodes
ListBox.Clear
If (ViewAll) Then
    ListBox.AddItem "All Status"
End If
RetrieveCode.ReferenceType = Trim(CodeType)
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(40)
        If (SetIt) Then
            Mid(DisplayText, 1, Len(RetrieveCode.ReferenceCode) - 3) = Left(RetrieveCode.ReferenceCode, Len(RetrieveCode.ReferenceCode) - 3)
            Mid(DisplayText, 40, 1) = Mid(RetrieveCode.ReferenceCode, Len(RetrieveCode.ReferenceCode), 1)
        Else
            DisplayText = Trim(RetrieveCode.ReferenceCode)
        End If
        ListBox.AddItem DisplayText
        k = k + 1
    Loop
End If
Set RetrieveCode = Nothing
End Function

Public Function ApplLoadLocation(WithAppts As Boolean, AllLocs As Boolean) As Boolean
Dim i As Integer
Dim Appt As Integer
Dim DisplayText As String
Dim RetPrc As PracticeName
Dim RetDr As SchedulerResource
ApplLoadLocation = True
ApplList.Clear
If Not (WithAppts) Then
    DisplayText = Space(75)
    If (AllLocs) Then
        Mid(DisplayText, 1, 13) = "All Locations"
    Else
        Mid(DisplayText, 1, 13) = "All Facility"
    End If
    DisplayText = DisplayText + Trim(str(-1))
    ApplList.AddItem DisplayText
End If
If (AllLocs) Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        i = 1
        While (RetPrc.SelectPractice(i))
            DisplayText = Space(56)
            Mid(DisplayText, 1, 6) = "Office"
            If (Trim(RetPrc.PracticeLocationReference) <> "") Then
                Mid(DisplayText, 7, Len(Trim(RetPrc.PracticeLocationReference)) + 1) = "-" + Trim(RetPrc.PracticeLocationReference)
                DisplayText = DisplayText + Trim(str(1000 + RetPrc.PracticeId))
            Else
                DisplayText = DisplayText + Trim(str(0))
            End If
            ApplList.AddItem DisplayText
            i = i + 1
        Wend
    End If
    Set RetPrc = Nothing
End If
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyRoom) Then
    i = 1
    While (RetDr.SelectResource(i))
        If (RetDr.ResourceServiceCode = "02") Then
            DisplayText = Space(75)
            Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
            If (WithAppts) Then
                Appt = AppointmentsAssigned(RetDr.ResourceId)
                Mid(DisplayText, 22, Len(Trim(str(Appt)))) = Trim(str(Appt))
                Mid(DisplayText, 45, Len(Trim(str(RetDr.ResourceColor)))) = Trim(str(RetDr.ResourceColor))
            End If
            DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
            ApplList.AddItem DisplayText
        End If
        i = i + 1
    Wend
End If
Set RetDr = Nothing
End Function

Public Function ApplLoadStaff(WithAppts As Boolean) As Boolean
Dim i As Integer
Dim Appt As Integer
Dim DisplayText As String
Dim RetDr As SchedulerResource
ApplLoadStaff = True
ApplList.Clear
If Not (WithAppts) Then
    DisplayText = Space(75)
    Mid(DisplayText, 1, 13) = "All Resources"
    DisplayText = DisplayText + Trim(str(-1))
    ApplList.AddItem DisplayText
End If
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyDoctor) Then
    i = 1
    While (RetDr.SelectResource(i))
        DisplayText = Space(75)
        Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
        If (WithAppts) Then
            Appt = AppointmentsAssigned(RetDr.ResourceId)
            Mid(DisplayText, 22, Len(Trim(str(Appt)))) = Trim(str(Appt))
            Mid(DisplayText, 45, Len(Trim(str(RetDr.ResourceColor)))) = Trim(str(RetDr.ResourceColor))
        End If
        DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
        If (RetDr.ResourceAddress <> "B") Then
            ApplList.AddItem DisplayText
        End If
        i = i + 1
    Wend
End If
Set RetDr = Nothing
End Function

Public Function ApplGetListResourceId(TheItem As String) As Long
ApplGetListResourceId = 0
If (Len(Trim(TheItem)) > 75) Then
    ApplGetListResourceId = Val(Trim(Mid(TheItem, 76, Len(TheItem) - 75)))
ElseIf (Len(Trim(TheItem)) > 56) Then
    ApplGetListResourceId = Val(Trim(Mid(TheItem, 56, Len(TheItem) - 55)))
End If
End Function

Private Function GetLocation(LocId As Long, LocName As String) As Boolean
Dim RetPrc As PracticeName
Dim RetResource As SchedulerResource
GetLocation = False
If (LocId > 0) Then
    If (LocId < 1000) Then
        Set RetResource = New SchedulerResource
        RetResource.ResourceId = LocId
        If (RetResource.RetrieveSchedulerResource) Then
            LocName = RetResource.ResourceDescription
        End If
        Set RetResource = Nothing
    ElseIf (LocId > 1000) Then
        Set RetPrc = New PracticeName
        RetPrc.PracticeId = LocId
        If (RetPrc.RetrievePracticeName) Then
            LocName = RetPrc.PracticeLocationReference
        End If
        Set RetPrc = Nothing
    End If
Else
    LocName = "Office"
End If
GetLocation = True
End Function

Private Function GetDoctor(DrId As Long, DrName As String) As Boolean
Dim RetResource As SchedulerResource
GetDoctor = False
If (DrId > 0) Then
    Set RetResource = New SchedulerResource
    RetResource.ResourceId = DrId
    If (RetResource.RetrieveSchedulerResource) Then
        DrName = Trim(RetResource.ResourceName)
    End If
    Set RetResource = Nothing
    GetDoctor = True
End If
End Function

Private Function AppointmentsAssigned(ResourceId As Long) As Integer
Dim oDate As New CIODateTime
Dim oAppointment As New SchedulerAppointment
    On Error GoTo lAppointmentsAssigned_Error

    oAppointment.ResourceId = ResourceId
    oAppointment.AppointmentStatus = "P"
    oDate.SetDateUnknown ApplDate
    oAppointment.AppointmentDate = oDate.GetIODate
    AppointmentsAssigned = oAppointment.FindAppointmentbyResourceExclusive
    If AppointmentsAssigned < 0 Then AppointmentsAssigned = 0
    Set oAppointment = Nothing
    Set oDate = Nothing

    Exit Function

lAppointmentsAssigned_Error:

    LogError "DI_ListFacAdm", "AppointmentsAssigned", Err, Err.Description
End Function


