VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PracticeMessages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The PracticeMessages Object Module
Option Explicit
Public MessagesId As Long
Public MessagesSenderId As Long
Public MessagesReceiverId As Long
Public MessagesDate As String
Public MessagesTime As Long
Public MessagesText As String
Public MessagesStatus As String

Private ModuleName As String
Private MessagesTotal As Long
Private MessagesTbl As ADODB.Recordset
Private MessagesNew As Boolean

Private Sub Class_Initialize()
Set MessagesTbl = CreateAdoRecordset
Call InitMessages
End Sub

Private Sub Class_Terminate()
Call InitMessages
Set MessagesTbl = Nothing
End Sub

Private Sub InitMessages()
MessagesNew = True
MessagesSenderId = 0
MessagesReceiverId = 0
MessagesDate = ""
MessagesTime = 0
MessagesText = ""
MessagesStatus = ""
End Sub

Private Sub LoadMessages()
MessagesNew = False
If (MessagesTbl("MessageId") <> "") Then
    MessagesId = MessagesTbl("MessageId")
Else
    MessagesId = 0
End If
If (MessagesTbl("SenderResourceId") <> "") Then
    MessagesSenderId = MessagesTbl("SenderResourceId")
Else
    MessagesSenderId = 0
End If
If (MessagesTbl("ReceiverResourceId") <> "") Then
    MessagesReceiverId = MessagesTbl("ReceiverResourceId")
Else
    MessagesReceiverId = 0
End If
If (MessagesTbl("MessageDate") <> "") Then
    MessagesDate = MessagesTbl("MessageDate")
Else
    MessagesDate = ""
End If
If (MessagesTbl("MessageTime") <> "") Then
    MessagesTime = MessagesTbl("MessageTime")
Else
    MessagesTime = 0
End If
If (MessagesTbl("Message") <> "") Then
    MessagesText = MessagesTbl("Message")
Else
    MessagesText = ""
End If
If (MessagesTbl("Status") <> "") Then
    MessagesStatus = MessagesTbl("Status")
Else
    MessagesStatus = ""
End If
End Sub

Private Function GetMessages()
On Error GoTo DbErrorHandler
GetMessages = False
Dim OrderString As String
OrderString = "SELECT * FROM PracticeMessages WHERE MessageId =" + Trim(Str(MessagesId)) + " "
Set MessagesTbl = Nothing
Set MessagesTbl = CreateAdoRecordset
MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Call MessagesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
If (MessagesTbl.EOF) Then
    Call InitMessages
Else
    Call LoadMessages
End If
GetMessages = True
Exit Function
DbErrorHandler:
    ModuleName = "GetMessages"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
    GetMessages = False
    MessagesId = -256
End Function

Private Function GetMessagesList() As Long
'On Error GoTo DbErrorHandler
'Dim OrderString As String
'MessagesTotal = 0
'GetMessagesList = -1
'OrderString = "Select * FROM PracticeMessages WHERE MessageDate = '" + UCase(Trim(MessagesDate)) + "' "
'If (MessagesReceiverId > 0) Then
'    OrderString = OrderString + "And ReceiverResourceId =" + Str(MessagesReceiverId) + " "
'End If
'If (Trim(MessagesStatus) <> "") Then
'    OrderString = OrderString + "And Status = '" + UCase(Trim(MessagesStatus)) + "' "
'End If
'If (MessagesSenderId > 0) Then
'    OrderString = OrderString + " And SenderResourceId =" + Str(MessagesSenderId) + " "
'End If
'OrderString = OrderString + "ORDER BY MessageDate ASC, MessageTime ASC "
'Set MessagesTbl = Nothing
'Set MessagesTbl = CreateAdoRecordset
'MyPracticeRepositoryCmd.CommandType = adCmdText
'MyPracticeRepositoryCmd.CommandText = OrderString
'Call MessagesTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
'If (MessagesTbl.EOF) Then
'    GetMessagesList = -1
'    Call InitMessages
'Else
'    MessagesTbl.MoveLast
'    MessagesTotal = MessagesTbl.RecordCount
'    GetMessagesList = MessagesTbl.RecordCount
'End If
'Exit Function
'DbErrorHandler:
'    ModuleName = "GetMessagesList"
'    Call PinpointPRError(ModuleName, Err)
'    Resume LeaveFast
'LeaveFast:
'    GetMessagesList = -256
End Function

Private Function PutMessages()
On Error GoTo DbErrorHandler
PutMessages = True
If (MessagesNew) Then
    MessagesTbl.AddNew
End If
MessagesTbl("SenderResourceId") = MessagesSenderId
MessagesTbl("ReceiverResourceId") = MessagesReceiverId
MessagesTbl("Message") = MessagesText
MessagesTbl("MessageDate") = MessagesDate
MessagesTbl("MessageTime") = MessagesTime
MessagesTbl("Status") = MessagesStatus
MessagesTbl.Update
If (MessagesNew) Then
    MessagesNew = False
    MessagesId = GetJustAddedRecord
End If
Exit Function
DbErrorHandler:
    ModuleName = "PutMessages"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
    PutMessages = False
End Function

Private Function GetJustAddedRecord() As Long
GetJustAddedRecord = 0
If (GetMessagesList > 0) Then
    If (SelectMessages(1)) Then
        GetJustAddedRecord = MessagesTbl("MessageId")
    End If
End If
End Function

Public Function ApplyMessages() As Boolean
ApplyMessages = PutMessages
End Function

Public Function DeleteMessages() As Boolean
DeleteMessages = PutMessages
End Function

Public Function RetrieveMessages() As Boolean
RetrieveMessages = GetMessages
End Function

Public Function SelectMessages(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectMessages = False
If (MessagesTbl.EOF) Or (ListItem < 1) Or (ListItem > MessagesTotal) Then
    Exit Function
End If
MessagesTbl.MoveFirst
MessagesTbl.Move ListItem - 1
SelectMessages = True
Call LoadMessages
Exit Function
DbErrorHandler:
    ModuleName = "SelectMessages"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
    SelectMessages = False
    MessagesId = -256
End Function

Public Function FindMessages() As Long
FindMessages = GetMessagesList()
End Function

Public Function IsMessages() As Boolean
Dim i As Integer
Dim ThoseFound As Integer
IsMessages = False
If (MessagesReceiverId < 1) Then
    Exit Function
End If
If (Trim(MessagesDate) = "") Then
    Exit Function
End If
ThoseFound = GetMessagesList()
If (ThoseFound < 1) Then
    Exit Function
Else
    Do Until Not (SelectMessages(i))
        If (MessagesStatus <> "R") Then
            IsMessages = True
            Exit Function
        End If
    Loop
End If
End Function

