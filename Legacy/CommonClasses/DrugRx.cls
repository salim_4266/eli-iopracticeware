VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDrugRx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public Id As String
Public Name As String
'LegacyDosage
Public Dosage As String
Public EyeRef As EEyeRef
'LegacyFrequency
Public Frequency As String
Public PO As String
Public PRN As Boolean
Public OverTheCounter As Boolean
Public stat As String
Public Samples As String
Public NoRx As String
Public AsPrescribed As Boolean
'LegacyDuration
Public Duration As String
'LegacyRefills
Public Refills As String
Public Daw As Boolean
Public PillPackage As String
Public LastTaken As String
Public DoctorNotes As String
Public PharmacyNotes As String
Public Strength As String
Public DaysSupply As String

Private Sub Class_Initialize()
    Id = ""
    Name = ""
    Dosage = ""
    PO = ""
    PRN = False
    AsPrescribed = False
    Duration = ""
    Frequency = ""
    Refills = ""
    Daw = False
    PillPackage = ""
    LastTaken = ""
    DoctorNotes = ""
    PharmacyNotes = ""
    EyeRef = eerNone
    OverTheCounter = False
    Samples = ""
    Strength = ""
    DaysSupply = ""
    stat = ""
    NoRx = ""
End Sub

Public Sub Clear(ClearAll As Boolean)
    If ClearAll Then
        Id = ""
        Name = ""
        NoRx = ""
        DoctorNotes = ""
        PharmacyNotes = ""
        Dosage = ""
    End If
    PO = ""
    PRN = False
    Duration = ""
    Frequency = ""
    Refills = ""
    Daw = False
    PillPackage = ""
    LastTaken = ""
    EyeRef = eerNone
    OverTheCounter = False
    Samples = ""
    Strength = ""
    DaysSupply = ""
    stat = ""
    AsPrescribed = False
End Sub

Private Function GetRxNotes(iApptId As Long) As Boolean
Dim iCount As Integer
Dim oNote As New PatientNotes
    If iApptId > 0 Then
        If Name <> "" Then
            oNote.NotesType = "R"
            oNote.NotesAppointmentId = iApptId
            oNote.NotesSystem = Name
            If oNote.FindNotes > 0 Then
                iCount = 1
                While oNote.SelectNotes(iCount) And Len(Trim$(DoctorNotes)) < 255
                    If Trim$(oNote.NotesText1) <> "" Then
                        DoctorNotes = DoctorNotes + Trim$(oNote.NotesText1) + " "
                    End If
                    If Trim$(oNote.NotesText2) <> "" Then
                        DoctorNotes = DoctorNotes + Trim$(oNote.NotesText2) + " "
                    End If
                    If Trim$(oNote.NotesText3) <> "" Then
                        DoctorNotes = DoctorNotes + Trim$(oNote.NotesText3) = " "
                    End If
                    If Trim$(oNote.NotesText4) <> "" Then
                        DoctorNotes = DoctorNotes + Trim$(oNote.NotesText4) = " "
                    End If
                    iCount = iCount + 1
                Wend
            End If
            Set oNote = New PatientNotes
            oNote.NotesType = "X"
            oNote.NotesAppointmentId = iApptId
            oNote.NotesSystem = Name
            If oNote.FindNotes > 0 Then
                iCount = 1
                While oNote.SelectNotes(iCount) And Len(Trim$(PharmacyNotes)) < 255
                    If Trim$(oNote.NotesText1) <> "" Then
                         PharmacyNotes = PharmacyNotes + Trim$(oNote.NotesText1) + " "
                    End If
                    If Trim$(oNote.NotesText2) <> "" Then
                        PharmacyNotes = PharmacyNotes + Trim$(oNote.NotesText2) + " "
                    End If
                    If Trim$(oNote.NotesText3) <> "" Then
                        PharmacyNotes = PharmacyNotes + Trim$(oNote.NotesText3) = " "
                    End If
                    If Trim$(oNote.NotesText4) <> "" Then
                        PharmacyNotes = PharmacyNotes + Trim$(oNote.NotesText4) = " "
                    End If
                    iCount = iCount + 1
                Wend
            End If
        End If
    End If
    Set oNote = Nothing
End Function

Public Sub Copy(oDrugRx As CDrugRx)
    Me.Id = oDrugRx.Id
    Me.Name = oDrugRx.Name
    Me.Dosage = oDrugRx.Dosage
    Me.PO = oDrugRx.PO
    Me.PRN = oDrugRx.PRN
    Me.AsPrescribed = oDrugRx.AsPrescribed
    Me.Duration = oDrugRx.Duration
    Me.Frequency = oDrugRx.Frequency
    Me.Refills = oDrugRx.Refills
    Me.Daw = oDrugRx.Daw
    Me.PillPackage = oDrugRx.PillPackage
    Me.LastTaken = oDrugRx.LastTaken
    Me.DoctorNotes = oDrugRx.DoctorNotes
    Me.PharmacyNotes = oDrugRx.PharmacyNotes
    Me.EyeRef = oDrugRx.EyeRef
    Me.OverTheCounter = oDrugRx.OverTheCounter
    Me.stat = oDrugRx.stat
    Me.Samples = oDrugRx.Samples
    Me.NoRx = oDrugRx.NoRx
    Me.DaysSupply = oDrugRx.DaysSupply
    Me.Strength = oDrugRx.Strength
End Sub

Public Function LoadRxFromDB(Findings As String) As Boolean
On Error GoTo subError

Dim itm(6, 6) As String
Dim p(1) As Integer
Dim tmp As String
Dim b As String
Dim e As Integer
Dim i As Integer

    'find beginning
    If UCase(Left(Findings, 5)) = "RX-8/" Then
        itm(0, 0) = Mid(Findings, 6)
    Else
        itm(0, 0) = Findings
    End If
    
    'find ending
    For e = 6 To 2 Step -1
        tmp = "-" & e & "/"
        i = InStrPS(1, itm(0, 0), tmp)
        If i > 0 Then
            itm(0, 0) = Mid(itm(0, 0), 1, i + 2)
            Exit For
        End If
    Next
    
    'parse
    
    p(1) = InStrPS(1, itm(0, 0), "-2/")
    Me.Name = Mid(itm(0, 0), 1, p(1) - 1)
    
    p(0) = InStrPS(1, itm(0, 0), "-2/")
    p(1) = InStrPS(1, itm(0, 0), "-3/")
    If p(1) > p(0) Then
        itm(3, 0) = Mid(itm(0, 0), p(0) + 3, p(1) - p(0) - 3)
    End If
    
    p(0) = InStrPS(1, itm(0, 0), "-3/")
    p(1) = InStrPS(1, itm(0, 0), "-4/")
    If p(1) > p(0) Then
        itm(4, 0) = Mid(itm(0, 0), p(0) + 3, p(1) - p(0) - 3)
    End If
    
    p(0) = InStrPS(1, itm(0, 0), "-4/")
    p(1) = InStrPS(1, itm(0, 0), "-5/")
    If p(1) > p(0) Then
        itm(5, 0) = Mid(itm(0, 0), p(0) + 3, p(1) - p(0) - 3)
    End If
    
    p(0) = InStrPS(1, itm(0, 0), "-5/")
    p(1) = InStrPS(1, itm(0, 0), "-6/")
    If p(1) > p(0) Then
        itm(6, 0) = Mid(itm(0, 0), p(0) + 3, p(1) - p(0) - 3)
    End If
    
    'recognize legacy drug
    If InStrPS(1, Findings, "()") > 0 Then
        'parse values
        For e = 3 To 6
            i = 0
            tmp = itm(e, 0)
            Do Until tmp = ""
                b = Mid(tmp, 1, 1)
                Select Case b
                Case "("
                    i = i + 1
                Case ")"
                Case Else
                    itm(e, i) = itm(e, i) & b
                End Select
                tmp = Mid(tmp, 2)
            Loop
        Next
        
        'assign values
        Me.Dosage = itm(3, 1)
        Me.Strength = itm(3, 2)
        Me.EyeRef = StringToEyeRef(itm(3, 3))
        
        
        
        If UCase(itm(4, 1)) = "AS PRESCRIBED" Then
            Me.Frequency = ""
            Me.AsPrescribed = True
        Else
            Me.Frequency = itm(4, 1)
            Me.AsPrescribed = False
        End If
        
        
        Me.PRN = Not Trim(itm(4, 2) = "")
        Me.PO = itm(4, 3)
        Me.OverTheCounter = Not Trim(itm(4, 4) = "")
        Me.stat = itm(4, 5)
        Me.Samples = itm(4, 6)
        
        Me.Refills = itm(5, 1)
        Me.Daw = Not Trim(itm(5, 2) = "")
        Me.PillPackage = itm(5, 3)
        Me.LastTaken = itm(5, 4)
        
        Me.Duration = itm(6, 1)
        Me.DaysSupply = itm(6, 2)
    Else
        If Trim(itm(3, 0)) <> "" Then ParseLegacyDosage itm(3, 0)
        If Trim(itm(4, 0)) <> "" Then ParseLegacyFrequency itm(4, 0)
        If Trim(itm(5, 0)) <> "" Then ParseLegacyRefills itm(5, 0)
        If Trim(itm(6, 0)) <> "" Then ParseLegacyDuration itm(6, 0)
    End If
    LoadRxFromDB = True
    Exit Function
    'REFACTOR: We can't just pop msgboxes when something goes wrong in production (especially with uninformative messages).
subError:
    MsgBox "error loading record"
End Function

Public Function GetDrugRxFormat(mFormat As String) As String
    'REFACTOR: The prefix "f" denotes a flag (or boolean).  Strings should have an "s"
Dim sName As String
Dim sStrength As String
Dim sDosage As String
Dim sEyeRef As String
Dim sAsPrescribed As String
Dim sFrequency As String
Dim sPRN As String
Dim sPO As String
Dim sDuration As String
Dim sOverTheCounter As String
Dim sStat As String
Dim sSamples As String
Dim sLastTaken As String
Dim sDaw As String
Dim sRefills As String
Dim sDaysSupply As String
Dim sPillPackage As String

    sName = Me.Name
    sStrength = Me.Strength
    sDosage = Me.Dosage
    sEyeRef = DisplayEyeRef(Me.EyeRef)
    If Me.AsPrescribed Then sAsPrescribed = "As Prescribed"
    sFrequency = Me.Frequency
    If Me.PRN Then sPRN = "PRN"
    sPO = Me.PO
    sDuration = Me.Duration
    If Me.OverTheCounter Then sOverTheCounter = "OTC"
    sStat = Me.stat
    sSamples = Me.Samples
    sLastTaken = Me.LastTaken
    If Me.Daw Then sDaw = "DAW"
    sRefills = Me.Refills
    sDaysSupply = Me.DaysSupply
    sPillPackage = Me.PillPackage
    'Why Dim these in the middle?  And what do they mean?
    Dim EOL As String
    Dim p(1) As String
    
    Dim mF As String
    Select Case mFormat
    Case "display", "list"
        If mFormat = "display" Then
            EOL = vbCrLf
        Else
            EOL = " "
        End If
        mF = mF & Trim(sName & " " & sStrength) & EOL
        mF = mF & Trim(sDosage & " " & sEyeRef) & EOL
        
        If sFrequency <> "" Then sFrequency = " " & sFrequency
        If sPRN <> "" Then sPRN = " " & sPRN
        If sPO <> "" Then sPO = " " & sPO
        If sDuration <> "" Then sDuration = " " & sDuration
        mF = mF & Trim(sAsPrescribed & sFrequency & sPRN & sPO & sDuration) & EOL
        
        If sStat <> "" Then sStat = " " & sStat
        If sSamples <> "" Then sSamples = " " & sSamples
        If sLastTaken <> "" Then sLastTaken = " " & sLastTaken
        mF = mF & Trim(sOverTheCounter & sStat & sSamples & sLastTaken) & EOL
        
        If sRefills <> "" Then sRefills = " " & sRefills
        If sDaysSupply <> "" Then sDaysSupply = " " & sDaysSupply
        If sPillPackage <> "" Then sPillPackage = " " & sPillPackage
        mF = mF & Trim(sDaw & sRefills & sDaysSupply & sPillPackage) & EOL
    Case "record1", "record2"
        If mFormat = "record1" Then
            p(1) = " "
        Else
            p(0) = "("
            p(1) = ")"
        End If
        If AsPrescribed Then
            sFrequency = sAsPrescribed
        End If
        mF = mF & sName & "-2/"
        mF = mF & LTrim(p(0) & sDosage & p(1)) & LTrim(p(0) & sStrength & p(1)) & LTrim(p(0) & sEyeRef & p(1)) & "-3/"
        mF = mF & LTrim(p(0) & sFrequency & p(1)) & LTrim(p(0) & sPRN & p(1)) & LTrim(p(0) & sPO & p(1)) & LTrim(p(0) & sOverTheCounter & p(1)) & LTrim(p(0) & sStat & p(1)) & LTrim(p(0) & sSamples & p(1)) & "-4/"
        mF = mF & LTrim(p(0) & sRefills & p(1)) & LTrim(p(0) & sDaw & p(1)) & LTrim(p(0) & sPillPackage & p(1)) & LTrim(p(0) & sLastTaken & p(1)) & "-5/"
        mF = mF & LTrim(p(0) & sDuration & p(1)) & LTrim(p(0) & sDaysSupply & p(1)) & "-6/"
    Case "ImprPlan"
        EOL = vbCrLf
        
        mF = mF & Trim(sName & " " & sStrength) & EOL

        If sDosage <> "" Then sDosage = " " & sDosage
        If sEyeRef <> "" Then sEyeRef = " " & sEyeRef
        If sPO <> "" Then sPO = " " & sPO
        If sFrequency <> "" Then sFrequency = " " & sFrequency
        If sPRN <> "" Then sPRN = " " & sPRN
        If sDuration <> "" Then sDuration = " " & sDuration
        mF = mF & Trim(sDosage & sEyeRef & sPO & sFrequency & sPRN & sDuration) & EOL

        If sPillPackage <> "" Then sPillPackage = " " & sPillPackage
        If sDaysSupply <> "" Then sDaysSupply = " " & sDaysSupply
        If sOverTheCounter <> "" Then sOverTheCounter = " " & sOverTheCounter
        mF = mF & Trim(sPillPackage & sDaysSupply & sOverTheCounter) & EOL

        mF = mF & sRefills & EOL
        
        mF = mF & Trim(sDaw & " " & sAsPrescribed) & EOL
        
    Case Else
        MsgBox "invald sormat"
        mF = "ERROR"
    End Select

    GetDrugRxFormat = mF
End Function

Public Function GetLegacyDosage() As String
Dim sDosage As String
    If Trim$(Me.Dosage) <> "" Then
        sDosage = "(" & Me.Dosage & ")"
    End If
    If Me.EyeRef <> eerNone Then
        sDosage = sDosage & " (" & DisplayEyeRef(Me.EyeRef) & ")"
    End If
    GetLegacyDosage = sDosage
End Function

Public Function GetLegacyFrequency() As String
Dim sFrequency As String
    If Trim$(Me.Frequency) <> "" Then
        sFrequency = "(" & Me.Frequency & ")"
    End If
    If Me.PRN Then
        sFrequency = sFrequency & " (PRN) "
    End If
    If Me.PO <> "" Then
        sFrequency = sFrequency & "(" & Me.PO & ")"
    End If
    If Me.OverTheCounter Then
        sFrequency = sFrequency & " (Over the Counter)"
    End If
    If Me.stat <> "" Then
       sFrequency = sFrequency & " (" & Me.stat & ")"
    End If
    If Me.Samples <> "" Then
       sFrequency = sFrequency & " (" & Me.Samples & ")"
    End If
    If Me.NoRx <> "" Then
       sFrequency = sFrequency & " (" & Me.NoRx & ")"
    End If
    GetLegacyFrequency = sFrequency
End Function

Public Function GetLegacyDuration() As String
    If Duration <> "" Then
        GetLegacyDuration = "(" & Duration & ")"
    End If
End Function

Public Function GetLegacyRefills() As String
    If Refills <> "" Then
        GetLegacyRefills = "(" & Refills & ")"
    End If
    If Daw Then
        GetLegacyRefills = GetLegacyRefills & " (daw)"
    End If
    If PillPackage <> "" Then
        GetLegacyRefills = GetLegacyRefills & " (" & PillPackage & ")"
    End If
    If Trim$(LastTaken) <> "" Then
        GetLegacyRefills = GetLegacyRefills & " [" & LastTaken & "]"
    End If
    GetLegacyRefills = Trim$(GetLegacyRefills)
End Function

Public Function GetFavoriteString() As String
Dim BuildFavoriteString As String

    BuildFavoriteString = "+" & GetLegacyDosage
    If GetLegacyFrequency <> "" Then
        BuildFavoriteString = BuildFavoriteString & " -" & GetLegacyFrequency
    End If
    If GetLegacyDuration <> "" Then
        BuildFavoriteString = BuildFavoriteString & " *" & GetLegacyDuration
    End If
    If GetLegacyRefills <> "" Then
        BuildFavoriteString = BuildFavoriteString & " /" & GetLegacyRefills
    End If
    GetFavoriteString = BuildFavoriteString
End Function

Public Sub ParseLegacyDosage(sLegacyDosage As String) '-3/
Dim i As Long
    'REFACTOR: Avoid using labels. I don't even think this one does anything. Why is it here?
startPoint:

    i = InStrPS(1, sLegacyDosage, "(OD)")
    If i > 0 Then
        EyeRef = eerOD
        Mid$(sLegacyDosage, i, 4) = Space(4)
        GoTo startPoint
    End If
    i = InStrPS(1, sLegacyDosage, "(OS)")
    If i > 0 Then
        EyeRef = eerOS
        Mid$(sLegacyDosage, i, 4) = Space(4)
        GoTo startPoint
    End If
    i = InStrPS(1, sLegacyDosage, "(OU)")
    If i > 0 Then
        EyeRef = eerOU
        Mid$(sLegacyDosage, i, 4) = Space(4)
        GoTo startPoint
    End If
    sLegacyDosage = Trim$(sLegacyDosage)
    If Left$(sLegacyDosage, 1) = "(" Then
        sLegacyDosage = Mid$(sLegacyDosage, 2, Len(sLegacyDosage) - 2)
    End If
    Dosage = Trim$(sLegacyDosage)
End Sub

Public Sub ParseLegacyFrequency(sLegacyFrequency As String) '-4/
Dim i As Long
    i = InStrPS(1, UCase(sLegacyFrequency), "(STAT)")
    If i > 0 Then
        stat = "STAT"
        Mid$(sLegacyFrequency, i, 7) = Space(7)
    End If
    i = InStrPS(1, UCase(sLegacyFrequency), "(SAMPLES)")
    If i > 0 Then
        Samples = "SAMPLES"
        Mid$(sLegacyFrequency, i, 9) = Space(9)
    End If
    i = InStrPS(1, UCase(sLegacyFrequency), "(N0RX)")
    If i > 0 Then
        NoRx = "NoRx"
        Mid$(sLegacyFrequency, i, 7) = Space(7)
    End If
    i = InStrPS(1, UCase(sLegacyFrequency), "(OVER THE COUNTER)")
    If i > 0 Then
        OverTheCounter = True
        Mid$(sLegacyFrequency, i, 18) = Space(18)
    End If
    i = InStrPS(1, UCase(sLegacyFrequency), "(PO)")
    If i > 0 Then
        PO = True
        Mid$(sLegacyFrequency, i, 4) = Space(4)
    End If
    i = InStrPS(1, UCase(sLegacyFrequency), "(PRN)")
    If i > 0 Then
        PRN = True
        Mid$(sLegacyFrequency, i, 5) = Space(5)
    End If
    sLegacyFrequency = Trim$(sLegacyFrequency)
    If Left$(sLegacyFrequency, 1) = "(" Then
        sLegacyFrequency = Mid$(sLegacyFrequency, 2, Len(sLegacyFrequency) - 2)
    End If
    Frequency = sLegacyFrequency
End Sub

Public Sub ParseLegacyDuration(sLegacyDuration As String) '-6/
    If Left$(sLegacyDuration, 1) = "(" Then
        sLegacyDuration = Mid$(sLegacyDuration, 2, Len(sLegacyDuration) - 2)
    End If
    Duration = sLegacyDuration
End Sub

Public Sub ParseLegacyRefills(sRefills As String) '-5/
Dim i As Long
Dim j As Long
    On Error GoTo lParseLegacyRefills_Error

    i = InStrPS(1, UCase(sRefills), "[LAST")
    If i > 0 Then
        j = InStrPS(i, sRefills, "]")
        If j > 0 Then
            LastTaken = Trim$(Mid$(sRefills, i + 1, j - i - 1))
            Mid$(sRefills, i, j - i + 1) = Space(j - i + 1)
        End If
    End If
    i = InStrPS(1, UCase(sRefills), "(PILL")
    If i > 0 Then
        j = InStrPS(i, Mid(sRefills, i), ")") + i - 1
        If j > 0 Then
            PillPackage = Trim$(Mid$(sRefills, i + 1, j - i - 1))
            Mid$(sRefills, i, j - i + 1) = Space(j - i + 1)
        End If
    End If
    i = InStrPS(1, UCase(sRefills), "(DAW)")
    If i > 0 Then
        Daw = True
        Mid$(sRefills, i, 5) = Space(5)
    End If
    sRefills = Trim$(sRefills)
    If Len(sRefills) <> 0 Then
        sRefills = Replace(sRefills, "(", "")
        sRefills = Replace(sRefills, ")", "")
    End If
    Refills = sRefills
    
    Exit Sub

lParseLegacyRefills_Error:

    LogError "CDrugRx", "ParseLegacyRefills", Err, Err.Description
End Sub

Public Function SetFromFavorite(FavoriteDosage As String)
Dim i As Integer
Dim sTempDosage
    sTempDosage = FavoriteDosage
    If LenB(sTempDosage) > 0 Then
        i = InStrPS(1, sTempDosage, "/")
        If i > 0 Then
            ParseLegacyRefills Trim$(Mid$(sTempDosage, i + 1, Len(sTempDosage) - i))
            sTempDosage = Trim$(Mid$(sTempDosage, 1, i - 1))
        End If
        i = InStrPS(1, sTempDosage, "*")
        If i > 0 Then
            ParseLegacyDuration Trim$(Mid$(sTempDosage, i + 1, Len(sTempDosage) - i))
            sTempDosage = Trim$(Mid$(sTempDosage, 1, i - 1))
        End If
        i = InStrPS(1, sTempDosage, "-")
        If i > 0 Then
            ParseLegacyFrequency Trim$(Mid$(sTempDosage, i + 1, Len(sTempDosage) - i))
            sTempDosage = Trim$(Mid$(sTempDosage, 1, i - 1))
        End If
        i = InStrPS(1, sTempDosage, "+")
        If i > 0 Then
            ParseLegacyDosage Trim$(Mid$(sTempDosage, i + 1, Len(sTempDosage) - 1))
        End If
    End If
End Function

Public Function PrintRx() As Boolean
Dim iPos As Long
Dim SigFile As String
Dim sMergeDataFile As String
Dim sMergeTemplateFile As String

    'Don't print Over the counter
    'Don't print samples
    'Don't print Frequency if the note duplicates it

    sMergeDataFile = DoctorInterfaceDirectory & "DrugRx" & "_" & globalExam.sAppPatIds & ".txt"
    If FM.IsFileThere(sMergeDataFile) Then
        'KillDocumentProcessing sMergeDataFile, True, True
        FM.Kill sMergeDataFile
    End If
    GetRxNotes globalExam.iAppointmentId
    StripCharacters DoctorNotes, Chr(13)
    ReplaceCharacters DoctorNotes, Chr(10), " "
    StripCharacters PharmacyNotes, Chr(13)
    ReplaceCharacters PharmacyNotes, Chr(10), " "
                           
Dim resultstring As String
Dim ResultFile As String
    CreateMergeDataFile sMergeDataFile, resultstring
    
    'FM.Name sMergeDataFile, Left$(sMergeDataFile, Len(sMergeDataFile) - 4) & ".xls"

    If InStrPS(sMergeDataFile, ".txt") > 0 Then
        ReplaceCharacters sMergeDataFile, ".txt", ".xls"
    End If
  
    ResultFile = resultstring & "@" & sMergeDataFile
    If (sMergeDataFile <> "") Then
        SigFile = GetResourceRxSigFile
        sMergeTemplateFile = TemplateDirectory & "DrugRx.doc"
        PrintTemplate ResultFile, sMergeTemplateFile, True, False, 1, "", SigFile, 1, False
    End If

End Function

Private Function CreateMergeDataFile(sMergeDataFile As String, resultstring As String) As String
Dim FileNum As Integer
Dim i As Integer, q As Integer
Dim Temp As String, ADate As String
Dim Resultfeild As String
Dim Resultvalue As String
Dim TempZip As String
Dim dte As String
Dim MF12 As String
Dim MF14 As String
Dim iResourceId As Long
Dim asMergeData(100) As String
Dim oPatient As New Patient
Dim oPractice As New PracticeName
Dim oDoctor As New SchedulerResource
Dim oAppointment As New SchedulerAppointment
Dim oActivity As New PracticeActivity

    oPatient.PatientId = globalExam.iPatientId
    If oPatient.RetrievePatient Then
        oActivity.ActivityId = globalExam.iActivityId
        If oActivity.RetrieveActivity Then
            iResourceId = oActivity.ActivityCurrResId
            If iResourceId > 0 Then
                oDoctor.ResourceId = iResourceId
                If oDoctor.RetrieveSchedulerResource Then
                    If Trim$(oDoctor.ResourceSignatureFile) = "" Then
                        iResourceId = 0
                    End If
                End If
            End If
        End If
        oAppointment.AppointmentId = globalExam.iAppointmentId
        If oAppointment.RetrieveSchedulerAppointment Then
            If iResourceId = 0 Then
                If oAppointment.AppointmentResourceId1 > 0 Then
                    oDoctor.ResourceId = oAppointment.AppointmentResourceId1
                    oDoctor.RetrieveSchedulerResource
                ElseIf oPatient.SchedulePrimaryDoctor > 0 Then
                    oDoctor.ResourceId = oPatient.SchedulePrimaryDoctor
                    oDoctor.RetrieveSchedulerResource
                End If
            End If
        End If
    End If
    asMergeData(0) = ValueOrFourSpaces("01:", oDoctor.ResourceDescription)
    oPractice.PracticeId = oDoctor.PracticeId
    oPractice.RetrievePracticeName
    asMergeData(1) = ValueOrFourSpaces("02:", FormatPhoneNumber(oPractice.PracticePhone, True))
    asMergeData(2) = ValueOrFourSpaces("03:", oDoctor.ResourceDea)
    asMergeData(3) = ValueOrFourSpaces("04:", oDoctor.ResourceLic)
    asMergeData(4) = "04a:    "
    asMergeData(5) = "04b:    "
    asMergeData(6) = ValueOrFourSpaces("05:", oPractice.PracticeAddress)
    asMergeData(7) = "05a:    "
    asMergeData(8) = ValueOrFourSpaces("06:", oPractice.PracticeCity & ", " & oPractice.PracticeState & " " & FormatZip(oPractice.PracticeZip, True))
    asMergeData(9) = "06a:    "
    Temp = Trim(Trim(oPatient.FirstName) + " " + Trim(oPatient.MiddleInitial) + " " + Trim(oPatient.LastName) + " " + Trim(oPatient.NameRef))
    StripCharacters Temp, "'"
    StripCharacters Temp, Chr(34)
    asMergeData(10) = "07:" + Temp
    asMergeData(11) = "07a:    "
    i = GetAge(oPatient.BirthDate)
    asMergeData(12) = "08:" + Trim(str(i))
    asMergeData(13) = "08a:    "
    asMergeData(14) = "09:" + Trim(oPatient.Address)
    asMergeData(15) = "09a:    "
    asMergeData(16) = "09b:    "
    TempZip = ""
    DisplayZip oPatient.Zip, TempZip
    Temp = Trim(oPatient.City) + ", " + oPatient.State + " " + TempZip
    asMergeData(17) = "10:" + Temp
    
    '11  drug Name + Strength + OTC
    '12  Dosage + Eye + PO/Apply to lids/Instill in Eyes + Frequency + PRN + Duration
    '13  Refills
    '14  Daw
    '16  drug Note
    '20  Pills/Package + Days supply
    
    MF12 = Dosage
    MF12 = Trim(MF12) & " " & DisplayEyeRef(EyeRef)
    MF12 = Trim(MF12) & " " & PO
    MF12 = Trim(MF12) & " " & Frequency
    If PRN Then MF12 = Trim(MF12) & " PRN"
    MF12 = Trim(MF12) & " " & Duration

    If Daw Then MF14 = "DAW"
    
    asMergeData(18) = ValueOrFourSpaces("11:", Trim$(Me.Name & " " & Me.Strength))
    asMergeData(19) = "011a:    "
    asMergeData(20) = "011b:    "
    asMergeData(21) = ValueOrFourSpaces("12:", MF12)
    asMergeData(22) = ValueOrFourSpaces("13:", Me.Refills)
    asMergeData(23) = ValueOrFourSpaces("14:", MF14)
    
    Call FormatTodaysDate(ADate, False)
    asMergeData(24) = "15:" + ADate
    asMergeData(25) = ValueOrFourSpaces("16:", DoctorNotes)
    If (Trim(oDoctor.ResourceSignatureFile) <> "") Then
        If (FM.IsFileThere(Trim(oDoctor.ResourceSignatureFile))) Then
            asMergeData(26) = "17:" + Trim(oDoctor.ResourceSignatureFile)
        Else
            asMergeData(26) = "17:    "
        End If
    Else
        asMergeData(26) = "17:    "
    End If
    If (Trim(oPatient.BirthDate) <> "") Then
        asMergeData(27) = "18:" + Mid(oPatient.BirthDate, 5, 2) + "/" + Mid(oPatient.BirthDate, 7, 2) + "/" + Left(oPatient.BirthDate, 4)
    Else
        asMergeData(27) = "18:    "
    End If
    asMergeData(28) = "18a:    "
    asMergeData(29) = "18b:    "
    asMergeData(30) = "18c:    "
    asMergeData(31) = "18d:    "
    asMergeData(32) = "18e:    "
    asMergeData(33) = "18f:    "
    asMergeData(34) = ValueOrFourSpaces("19:", oPatient.Gender)
    asMergeData(35) = ValueOrFourSpaces("20:", Trim$(Me.PillPackage & " " & Me.DaysSupply))
    asMergeData(36) = ValueOrFourSpaces("21:", oDoctor.ResourceNPI)
    
    If (Trim(oAppointment.AppointmentDate) <> "") Then
        dte = Trim(oAppointment.AppointmentDate)
        asMergeData(37) = "22:" & Mid(dte, 5, 2) & "/" & Mid(dte, 7, 2) & "/" & Mid(dte, 1, 4)
    Else
        asMergeData(37) = "22:    "
    End If
'    FileCopy TemplateDirectory + "Letters.txt", sMergeDataFile
'    FileNum = FreeFile
'    Open sMergeDataFile For Binary As #FileNum
'    Close #FileNum
'    Open sMergeDataFile For Append As #FileNum
    For i = 0 To 100
        q = InStrPS(asMergeData(i), ":")
        If q > 0 Then
         If Resultvalue = "" Then
            
             Resultvalue = Mid$(asMergeData(i), q + 1, Len(asMergeData(i)) - q) & vbTab
             
             Else
                   
                Resultvalue = Resultvalue & Mid$(asMergeData(i), q + 1, Len(asMergeData(i)) - q) & vbTab
            
             End If
           ' Print #FileNum, Mid$(asMergeData(i), q + 1, Len(asMergeData(i)) - q); vbTab;
        End If
    Next i
'    Print #FileNum, ""
'    Close #FileNum

         Call createresultfield(Resultfeild)
         resultstring = Resultfeild & vbCrLf & Resultvalue
         
    Set oAppointment = Nothing
    Set oActivity = Nothing
    Set oPatient = Nothing
    Set oPractice = Nothing
    Set oDoctor = Nothing

End Function
Private Sub createresultfield(Resultfeild As String)

        Resultfeild = ":1:" & vbTab & ":2:" & vbTab & ":3:" & vbTab & ":4:" & vbTab & ":4a:" & vbTab & ":4b:" & vbTab & ":5:" & vbTab & ":5a:" & vbTab & ":6:" & vbTab & ":6a:" & vbTab & ":7:" & vbTab & ":7a:" & vbTab & ":8:" & vbTab & ":8a:" & vbTab & ":9:" & vbTab & ":9a:" & vbTab & ":9b:" & vbTab & ":10:" & vbTab & ":11:" & vbTab & ":11a:" & vbTab & ":11b:" & vbTab & ":12:" & vbTab & ":13:" & vbTab & ":14:" & vbTab & ":15:" & vbTab & ":16:" & vbTab & ":17:" & vbTab & ":18:" & vbTab & ":18a:" & vbTab & ":18b:" & vbTab & ":18c:" & vbTab & ":18d:" & vbTab & ":18e:" & vbTab & ":18f:" & vbTab & ":19:" & vbTab & ":20:" & vbTab & ":21:" & vbTab & ":22:" & vbTab & ":23:" & vbTab & ":24:" & vbTab & ":25:" & vbTab & ":26:" & vbTab & ":27:" & vbTab & ":28:" & vbTab & ":29:"
        Resultfeild = Resultfeild + vbTab & ":30:" & vbTab & ":31:" & vbTab & ":32:" & vbTab & ":33:" & vbTab & ":34:" & vbTab & ":35:" & vbTab & ":36:" & vbTab & ":37:" & vbTab & ":38:" & vbTab & ":39:" & vbTab & ":40:" & vbTab & ":41:" & vbTab & ":42:" & vbTab & ":43:" & vbTab & ":44:" & vbTab & ":45:" & vbTab & ":46:" & vbTab & ":47:" & vbTab & ":48:" & vbTab & ":49:" & vbTab & ":50:" & vbTab & ":51:" & vbTab & ":52:" & vbTab & ":53:" & vbTab & ":54:" & vbTab & ":55:" & vbTab & ":56:" & vbTab & ":57:" & vbTab & ":58:" & vbTab & ":59:" & vbTab & ":60:" & vbTab & ":61:" & vbTab & ":62:" & vbTab & ":63:" & vbTab & ":64:" & vbTab & ":65:" & vbTab & ":66:" & vbTab & ":67:" & vbTab & ":68:" & vbTab & ":69:" & vbTab & ":70:" & vbTab & ":70a:"
        Resultfeild = Resultfeild + vbTab & ":71:" & vbTab & ":71a:" & vbTab & ":72:" & vbTab & ":72a:" & vbTab & ":73:" & vbTab & ":73a:" & vbTab & ":74:" & vbTab & ":74a:" & vbTab & ":75:" & vbTab & ":75a:" & vbTab & ":76:" & vbTab & ":76a:" & vbTab & ":77:" & vbTab & ":77a:" & vbTab & ":78:" & vbTab & ":78a:" & vbTab & ":79:" & vbTab & ":79a:" & vbTab & ":80:" & vbTab & ":80a:" & vbTab & ":81:" & vbTab & ":81a:" & vbTab & ":82:" & vbTab & ":82a:" & vbTab & ":83:" & vbTab & ":83a:" & vbTab & ":84:" & vbTab & ":84a:" & vbTab & ":85:" & vbTab & ":85a:" & vbTab & ":86:" & vbTab & ":86a:" & vbTab & ":87:" & vbTab & ":87a:" & vbTab & ":88:" & vbTab & ":88a:" & vbTab & ":89:" & vbTab & ":89a:" & vbTab & ":90:" & vbTab & ":90a:" & vbTab & ":91:" & vbTab & ":91a:" & vbTab & ":92:" & vbTab & ":92a:"
        Resultfeild = Resultfeild + vbTab & ":93:" & vbTab & ":93a" & vbTab & ":94:" & vbTab & ":94a:" & vbTab & ":95:" & vbTab & ":95a:" & vbTab & ":96:" & vbTab & ":96a:" & vbTab & ":97:" & vbTab & ":97a:" & vbTab & ":98:" & vbTab & ":98a:" & vbTab & ":99:" & vbTab & ":99a:" & vbTab & ":100:" & vbTab & ":100a:" & vbTab & ":101:" & vbTab & ":101a" & vbTab & ":102:" & vbTab & ":102a:" & vbTab & ":103:" & vbTab & ":103a" & vbTab & ":104:" & vbTab & ":104a" & vbTab & ":105:" & vbTab & ":105a" & vbTab & ":106:" & vbTab & ":106a:" & vbTab & ":107:" & vbTab & ":107a" & vbTab & ":108:" & vbTab & ":108a:" & vbTab & ":109:" & vbTab & ":109a:" & vbTab & ":110:" & vbTab & ":110a:" & vbTab & ":111:" & vbTab & ":111a:" & vbTab & ":112:" & vbTab & ":112a:" & vbTab & ":113:" & vbTab & ":113a:" & vbTab & ":114:" & vbTab & ":114a:" & vbTab & ":115:" & vbTab & ":115a:" & vbTab & ":116:" & vbTab & ":116a:" & vbTab & ":117:" & vbTab & ":117a:" & vbTab & ":118:" & vbTab & ":118a:" & vbTab & ":119:" & vbTab & ":119a:"
        Resultfeild = Resultfeild + vbTab & ":120:" & vbTab & ":120a:" & vbTab & ":121:" & vbTab & ":121a:" & vbTab & ":122:" & vbTab & ":122a:" & vbTab & ":123:" & vbTab & ":123a:" & vbTab & ":124:" & vbTab & ":124a:" & vbTab & ":125:" & vbTab & ":125a:" & vbTab & ":126:" & vbTab & ":126a:" & vbTab & ":127:" & vbTab & ":127a:" & vbTab & ":128:" & vbTab & ":128a:" & vbTab & ":129:" & vbTab & ":129a:" & vbTab & ":130:" & vbTab & ":130a:" & vbTab & ":131:" & vbTab & ":131a:" & vbTab & ":132:" & vbTab & ":132a:" & vbTab & ":133:" & vbTab & ":133a:" & vbTab & ":134:" & vbTab & ":134a:" & vbTab & ":135:" & vbTab & ":136:" & vbTab & ":137:" & vbTab & ":138:" & vbTab & ":139:" & vbTab & ":140:" & vbTab & ":141:" & vbTab & ":142:" & vbTab & ":143:" & vbTab & ":144:" & vbTab & ":145:" & vbTab & ":146:"
        Resultfeild = Resultfeild + vbTab & ":147:" & vbTab & ":148:" & vbTab & ":149:" & vbTab & ":150:" & vbTab & ":151:" & vbTab & ":152:" & vbTab & ":153:" & vbTab & ":154:" & vbTab & ":155:" & vbTab & ":156:" & vbTab & ":157:" & vbTab & ":158:" & vbTab & ":159:" & vbTab & ":160:" & vbTab & ":161:" & vbTab & ":162:" & vbTab & ":163:" & vbTab & ":164:" & vbTab & ":165:" & vbTab & ":166:" & vbTab & ":167:" & vbTab & ":168:" & vbTab & ":169:" & vbTab & ":170:" & vbTab & ":171:" & vbTab & ":172:" & vbTab & ":173:"

End Sub

Private Function ValueOrFourSpaces(ByVal sHeader As String, ByVal sValue As String) As String
    If Trim$(sValue) <> "" Then
        ValueOrFourSpaces = sHeader & Trim$(sValue)
    Else
        ValueOrFourSpaces = sHeader & Space(4)
    End If
End Function

Private Function GetResourceRxSigFile() As String
Dim RscId As Long
Dim iPos As Long
Dim RetAct As New PracticeActivity
Dim RetAppt As SchedulerAppointment
Dim RetRes As SchedulerResource
Dim sRxSigFile As String

    RetAct.ActivityId = globalExam.iActivityId
    If (RetAct.RetrieveActivity) Then
        RscId = RetAct.ActivityCurrResId
        If (RscId > 0) Then
            Set RetRes = New SchedulerResource
            RetRes.ResourceId = RscId
            If (RetRes.RetrieveSchedulerResource) Then
                If (Trim(RetRes.ResourceSignatureFile) <> "") Then
                    sRxSigFile = Trim(RetRes.ResourceSignatureFile)
                End If
            End If
            Set RetRes = Nothing
        End If
    End If
    Set RetAct = Nothing
    
    If Trim(sRxSigFile) = "" Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = globalExam.iAppointmentId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            RscId = RetAppt.AppointmentResourceId1
        End If
        Set RetAppt = Nothing
        If (RscId > 0) Then
            Set RetRes = New SchedulerResource
            RetRes.ResourceId = RscId
            If (RetRes.RetrieveSchedulerResource) Then
                If (Trim(RetRes.ResourceSignatureFile) <> "") Then
                    sRxSigFile = Trim(RetRes.ResourceSignatureFile)
                End If
            End If
            Set RetRes = Nothing
        End If
    End If
    If Trim$(sRxSigFile) <> "" Then
        iPos = InStrPS(sRxSigFile, ".")
        If iPos > 0 Then
            sRxSigFile = Left$(sRxSigFile, iPos - 1) & "-Rx" & Mid$(sRxSigFile, iPos, Len(sRxSigFile) - (iPos - 1))
        End If
    End If
    GetResourceRxSigFile = sRxSigFile
End Function
