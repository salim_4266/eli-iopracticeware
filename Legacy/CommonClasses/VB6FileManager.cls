VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "VB6FileManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

''' <summary>
''' Enumeration of file access types where 'ReadWrite' is the default value.
''' Read is interpretted as "Access Read Shared" and
''' Write is interpretted as "Access Write Shared".
''' </summary>
Public Enum FileAccess
    ReadWrite = 0
    Read = 1
    [Write] = 2
        ReadShared = 3
    WriteShared = 4
End Enum

''' <summary>
''' Enumeration of file open modes
''' </summary>
Public Enum FileOpenMode
    Append = 0
    Binary = 1
    InputFileOpenMode = 2
    Output = 3
    Random = 4
End Enum

Private Const FileManagerType As String = "IO.Practiceware.Storage.LocalFileOptimizedFileManager"
Private FileManagerComWrapper As New comWrapper
Private FileNumberToPathMap As New Dictionary
Private FileManager As Variant
Private dirResult() As String
Private currentDirResult As Integer

Private Sub Class_Initialize()
    Dim args() As Variant, dummyResult As Variant
    Set FileManager = FileManagerComWrapper.Create(FileManagerType, args).Instance
    
    ' Set optimized file manager as globally shared default instance
    args = Array(FileManager)
    dummyResult = FileManagerComWrapper.InvokeStaticMethod("IO.Practiceware.Storage.FileManager+Initializer", "SetSharedInstance", args)
End Sub

''' <summary>
''' Gets all files matching the search pattern.  Note the path contains the search pattern.
''' </summary>
''' <returns></returns>
Public Function ListFiles(path As String) As String()
    ListFiles = FileManager.ListFiles(path, False)
End Function

''' <summary>
''' Performs a "Directory.GetFiles" operation on the FileService.  Returns the next file in the current search if no parameter is specified.
''' Returns the first file in a new search if a parameter is specified.
''' </summary>
''' <returns></returns>
''' <remarks></remarks>
Public Function Dir(Optional path As String = "", Optional attr As Integer = 0) As String
    If (path <> "") Then
        currentDirResult = 0

        If (attr <> vbDirectory) Then
            dirResult = FileManager.ListFiles(path, True)
        Else
            ' Translate Dir(directory) request into directory exists check if it doesn't end with "\"
            Dim lastCharacterInPath As String
            lastCharacterInPath = Mid(path, Len(path) - 1, 1)
            If (lastCharacterInPath <> "\\" And lastCharacterInPath <> "/") Then
                If (FileManager.DirectoryExists(path)) Then
                    ReDim dirResult(1) As String
                    dirResult(0) = path
                End If
            Else
                dirResult = FileManager.ListDirectories(path, True)
            End If
        End If
    End If


    If (Not IsEmpty(dirResult) And (UBound(dirResult) >= currentDirResult)) Then
        Dir = dirResult(currentDirResult)
    Else
        Dir = ""
    End If

    currentDirResult = currentDirResult + 1
End Function

''' <summary>
''' Checks whether directory exists
''' </summary>
''' <returns></returns>
Public Function DirectoryExists(path As String) As Boolean
    DirectoryExists = FileManager.DirectoryExists(path)
End Function
Public Function actualpath(path As String) As String
actualpath = FileManager.Read(path)
End Function
''' <summary>
''' Opens the file using a VB6 Open statement based on the provided parameters (http://msdn.microsoft.com/en-us/library/aa266177(v=vs.60).aspx).
''' This method calls the IFileManager to ensure the file is downloaded from the server and returns a file opened from redirected path (not necessarily the path passed in).
''' Tracks the redirected path in a dictionary by file number so that the file can be closed properly and pushed to the IFileService via IFileManager.
''' Replacement for "Open File For".
''' </summary>
''' <param name="path">The path</param>
''' <param name="mode">The mode</param>
''' <param name="access">The access type</param>
''' <param name="fileNumber">The file number handle</param>
Public Sub OpenFile(path As String, Optional mode As FileOpenMode = Append, Optional access As FileAccess = ReadWrite, Optional fileNumber As Long = 0, Optional bufferLength = 0)
    If (Not FileNumberToPathMap.Exists(fileNumber)) Then
    
        Dim actualpath As String
        actualpath = FileManager.Read(path)
    'MsgBox (path)
        If (bufferLength = 0) Then
            Select Case mode
                Case FileOpenMode.Append
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Append As #fileNumber
                        Case FileAccess.Write
                            Open actualpath For Append Access Write As #fileNumber
                        Case FileAccess.WriteShared
                                Open actualpath For Append Access Write Shared As #fileNumber
                    End Select
                Case FileOpenMode.Binary
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Binary As #fileNumber
                        Case FileAccess.Read
                            Open actualpath For Binary Access Read As #fileNumber
                        Case FileAccess.ReadShared
                            Open actualpath For Binary Access Read Shared As #fileNumber
                        Case FileAccess.Write
                            Open actualpath For Binary Access Write As #fileNumber
                        Case FileAccess.WriteShared
                            Open actualpath For Binary Access Write Shared As #fileNumber
                    End Select
                Case FileOpenMode.InputFileOpenMode
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Input As #fileNumber
                        Case FileAccess.Read
                            Open actualpath For Input Access Read As #fileNumber
                        Case FileAccess.ReadShared
                            Open actualpath For Input Access Read Shared As #fileNumber
                    End Select
                Case FileOpenMode.Output
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Output As #fileNumber
                        Case FileAccess.Write
                            Open actualpath For Output Access Write As #fileNumber
                        Case FileAccess.WriteShared
                            Open actualpath For Output Access Write Shared As #fileNumber
                    End Select
                Case FileOpenMode.Random
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Random As #fileNumber
                        Case FileAccess.Read
                            Open actualpath For Random Access Read As #fileNumber
                        Case FileAccess.ReadShared
                            Open actualpath For Random Access Read Shared As #fileNumber
                        Case FileAccess.Write
                            Open actualpath For Random Access Write As #fileNumber
                        Case FileAccess.WriteShared
                            Open actualpath For Random Access Write Shared As #fileNumber
                    End Select
                End Select
            Else
            Select Case mode
                Case FileOpenMode.Append
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Append As #fileNumber Len = bufferLength
                        Case FileAccess.Write
                            Open actualpath For Append Access Write As #fileNumber Len = bufferLength
                        Case FileAccess.WriteShared
                                Open actualpath For Append Access Write Shared As #fileNumber Len = bufferLength
                    End Select
                Case FileOpenMode.Binary
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Binary As #fileNumber Len = bufferLength
                        Case FileAccess.Read
                            Open actualpath For Binary Access Read As #fileNumber Len = bufferLength
                        Case FileAccess.ReadShared
                            Open actualpath For Binary Access Read Shared As #fileNumber Len = bufferLength
                        Case FileAccess.Write
                            Open actualpath For Binary Access Write As #fileNumber Len = bufferLength
                        Case FileAccess.WriteShared
                            Open actualpath For Binary Access Write Shared As #fileNumber Len = bufferLength
                    End Select
                Case FileOpenMode.InputFileOpenMode
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Input As #fileNumber Len = bufferLength
                        Case FileAccess.Read
                            Open actualpath For Input Access Read As #fileNumber Len = bufferLength
                        Case FileAccess.ReadShared
                            Open actualpath For Input Access Read Shared As #fileNumber Len = bufferLength
                    End Select
                Case FileOpenMode.Output
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Output As #fileNumber Len = bufferLength
                        Case FileAccess.Write
                            Open actualpath For Output Access Write As #fileNumber Len = bufferLength
                        Case FileAccess.WriteShared
                            Open actualpath For Output Access Write Shared As #fileNumber Len = bufferLength
                    End Select
                Case FileOpenMode.Random
                    Select Case access
                        Case FileAccess.ReadWrite
                            Open actualpath For Random As #fileNumber Len = bufferLength
                        Case FileAccess.Read
                            Open actualpath For Random Access Read As #fileNumber Len = bufferLength
                        Case FileAccess.ReadShared
                            Open actualpath For Random Access Read Shared As #fileNumber Len = bufferLength
                        Case FileAccess.Write
                            Open actualpath For Random Access Write As #fileNumber Len = bufferLength
                        Case FileAccess.WriteShared
                            Open actualpath For Random Access Write Shared As #fileNumber Len = bufferLength
                    End Select
                End Select
        End If
        
        
        Call FileNumberToPathMap.Add(fileNumber, path)
    Else
        Err.Raise 1000
    End If

End Sub

''' <summary>
''' Concludes input/output (I/O) to a file opened using the Open statement and pushes to the IFileService via IFileManager.
''' Replacement for "Close #fileNumber" (http://msdn.microsoft.com/en-us/library/aa243283(v=vs.60).aspx).
''' </summary>
''' <param name="fileNumber">The file number.</param>
Public Sub CloseFile(fileNumber As Long, Optional saveChanges As Boolean = True)
    If (FileNumberToPathMap.Exists(fileNumber)) Then
        Dim path As String
        path = FileNumberToPathMap(fileNumber)
        FileNumberToPathMap.Remove (fileNumber)
        
        Close #fileNumber
        
        If (saveChanges) Then
            FileManager.Commit (path)
        End If
    End If
End Sub

''' <summary>
''' Copies the specified source file to the destination using IFileManager.Copy (not direct file system access).
''' Replacement for "FileCopy" (http://msdn.microsoft.com/en-us/library/aa243368(v=vs.60).aspx).
''' </summary>
''' <param name="sourcePath">The source.</param>
''' <param name="destinationPath">The destination.</param>
Public Sub FileCopy(sourcePath As String, destinationPath As String)
    Call FileManager.Copy(sourcePath, destinationPath)
End Sub

''' <summary>
''' Deletes files from disk using IFileManager.Delete (not direct file system access).
''' Replacement for "Kill" (http://msdn.microsoft.com/en-us/library/aa243388(v=vs.60).aspx).
''' </summary>
''' <param name="path">The path.</param>
Public Sub Kill(path As String)
    Call FileManager.Delete(path)
End Sub

''' <summary>
''' Renames a disk file, directory, or folder IFileManager.Move (not direct file system access).
''' Replacement for "Name" (http://msdn.microsoft.com/en-us/library/aa266171(v=vs.60).aspx).
''' </summary>
''' <param name="oldPath">The old path.</param>
''' <param name="newPath">The new path.</param>
Public Sub Name(oldPath As String, newPath As String)
    Call FileManager.Move(oldPath, newPath)
End Sub

''' <summary>
''' Determines whether a file exists at the specified path using IFileManager.FileExists (not direct file system access).
''' Replacement for "IsFileThere".
''' </summary>
''' <param name="path">The path.</param>
Public Function IsFileThere(path As String) As Boolean
    On Error GoTo lIsFileThere_Error

    IsFileThere = FileManager.FileExists(path)
    Exit Function
    
    ' We want FileExists check to complete even on invalid paths, since thats what is expected from old code
lIsFileThere_Error:
    IsFileThere = False

End Function

''' <summary>
''' Create the specified directory.
''' Replacement for "MkDir".
''' </summary>
''' <param name="path">The path.</param>
Public Sub MkDir(path As String)
    FileManager.CreateDirectory path
End Sub

''' <summary>
''' Clears all files and directories at the redirected root using IFileManager.Clear
''' </summary>
Public Sub Clear()
    FileManager.ClearCache
End Sub

' Short-named methods added to make some procedures smaller (PrintDIAppointment, which is too large)


''' <summary>
''' Opens the file using a VB6 Open statement based on the provided parameters (http://msdn.microsoft.com/en-us/library/aa266177(v=vs.60).aspx).
''' This method calls the IFileManager to ensure the file is downloaded from the server and returns a file opened from redirected path (not necessarily the path passed in).
''' Tracks the redirected path in a dictionary by file number so that the file can be closed properly and pushed to the IFileService via IFileManager.
''' Replacement for "Open File For".
''' </summary>
''' <param name="path">The path</param>
''' <param name="mode">The mode</param>
''' <param name="access">The access type</param>
''' <param name="fileNumber">The file number handle</param>
Public Sub OF(path As String, Optional mode As FileOpenMode = Append, Optional access As FileAccess = ReadWrite, Optional fileNumber As Long = 0, Optional bufferLength = 0)
    OpenFile path, mode, access, fileNumber, bufferLength
End Sub

''' <summary>
''' Concludes input/output (I/O) to a file opened using the Open statement and pushes to the IFileService via IFileManager.
''' Replacement for "Close #fileNumber" (http://msdn.microsoft.com/en-us/library/aa243283(v=vs.60).aspx).
''' </summary>
''' <param name="fileNumber">The file number.</param>
Public Sub CF(fileNumber As Long)
    CloseFile fileNumber
End Sub

''' <summary>
''' Determines whether a file exists at the specified path using IFileManager.FileExists (not direct file system access).
''' Replacement for "IsFileThere".
''' </summary>
''' <param name="path">The path.</param>
Public Function IFT(path As String) As Boolean
    IFT = IsFileThere(path)
End Function

''' <summary>
''' Copies the specified source file to the destination using IFileManager.Copy (not direct file system access).
''' Replacement for "FileCopy" (http://msdn.microsoft.com/en-us/library/aa243368(v=vs.60).aspx).
''' </summary>
''' <param name="sourcePath">The source.</param>
''' <param name="destinationPath">The destination.</param>
Public Sub FC(sourcePath As String, destinationPath As String)
    Call FileCopy(sourcePath, destinationPath)
End Sub

''' <summary>
''' Returns local path to file to perform direct operations without FileManager
''' By default latest version is read
''' </summary>
Public Function GetPathForDirectIO(path As String, Optional readLatestFromServer As Boolean = True) As String
    Dim redirectedPath As String
    If (readLatestFromServer) Then
        If (FileManager.FileExists(path)) Then
            GetPathForDirectIO = FileManager.Read(path)
            Exit Function
        End If
    End If
    
    Dim args(0) As Variant
    Dim engineInstance As New comWrapper
    Dim redirectedPaths(0) As String
    ' Get redirected path
    redirectedPath = FileManager.Configuration.ToRedirectedPath(path)
    redirectedPaths(0) = redirectedPath
    args(0) = redirectedPaths
    engineInstance.SetInstance (FileManager.Engine)
    args(0) = engineInstance.InvokeMethod("WaitForIncompleteOperations", args)
    GetPathForDirectIO = redirectedPath
    
    ' Ensure all folder parts of redirected path exist
    Dim fso As New FileSystemObject
    redirectedPath = fso.GetParentFolderName(redirectedPath)
    If (Not fso.FolderExists(redirectedPath)) Then
        fso.CreateFolder (redirectedPath)
    End If
    
End Function

''' <summary>
''' Persists changes made to file directly.
''' Note: use only with FM.GetPathForDirectIO
''' </summary>
Public Sub CommitDirectWrite(path As String)
    FileManager.Commit (path)
End Sub

''' <summary>
''' Returns an IPictureDisp for the specified file using IFileManager.Read and then calling the default VB.LoadPicture method.
''' Replacement for "VB.LoadPicture" (http://msdn.microsoft.com/en-us/library/aa264946(v=vs.60).aspx).
''' </summary>
Public Function LoadPicture(path As String, Optional Size As Long, Optional colorDepth As Long, Optional x As Long, Optional Y As Long) As IPictureDisp
    Dim actualpath As String
    actualpath = FileManager.Read(path)
    Set LoadPicture = VB.LoadPicture(actualpath, Size, colorDepth, x, Y)
End Function


''' <summary>
''' Saves an IPictureDisp for the specified file using IFileManager.Read and then calling the default VB.SavePicture method.
''' Replacement for "VB.SavePicture" (http://msdn.microsoft.com/en-us/library/aa445827(v=vs.60).aspx).
''' </summary>
Public Sub SavePicture(picture As IPictureDisp, path As String)
    Dim actualpath As String
    actualpath = GetPathForDirectIO(path, False)
    VB.SavePicture picture, actualpath
    CommitDirectWrite (path)
End Sub

''' <summary>
''' Returns file length
''' </summary>
Public Function GetFileLength(path As String) As Long
    Dim filePaths(0) As String
    Dim fileRecord As Collection

    filePaths(0) = path
    Set fileRecord = GetFileInfos(filePaths).Item(1)

    If (fileRecord.Item("Exists")) Then
        GetFileLength = fileRecord.Item("Size")
    Else
        GetFileLength = -1
    End If
End Function

''' <summary>
''' Returns file infos
''' </summary>
Public Function GetFileInfos(filePaths() As String) As Collection
    Dim args(0) As Variant
    Dim fileRecords As New comWrapper
    Dim fileRecord As New comWrapper
    Dim Result As New Collection
    Dim fileRecordData As Collection
    Dim i As Integer
    
    ' fileRecord = FileManager.GetFileInfos(filePaths)
    args(0) = filePaths
    fileRecords.SetInstance (FileManagerComWrapper.InvokeMethod("GetFileInfos", args))
    
    For i = 0 To UBound(fileRecords.Instance)
        ' fileRecord = fileRecords[i]
        args(0) = i
        fileRecord.SetInstance (fileRecords.InvokeMethod("GetValue", args))
        
        Set fileRecordData = New Collection
        fileRecordData.Add fileRecord.Instance.Exists, "Exists"
        fileRecordData.Add fileRecord.Instance.Size, "Size"
        fileRecordData.Add fileRecord.Instance.FullName, "FullName"
        Result.Add fileRecordData, fileRecordData("FullName")
    Next i
    
    Set GetFileInfos = Result
End Function

''' <summary>
''' Returns whether file access is being redirected (which means that files will not be stored locally only)
''' </summary>
Public Function IsRedirectionEnabled() As Boolean
    IsRedirectionEnabled = FileManager.Configuration.IsRedirectionEnabled
End Function

''' <summary>
''' Returns file name from specified path (can be relative, full, can start with '~')
''' </summary>
Public Function GetFileName(path As String) As String
    Dim fso As New FileSystemObject
    GetFileName = fso.GetFileName(path)
    Set fso = Nothing
End Function

''' <summary>
''' Returns file extension
''' </summary>
Public Function GetFileExtension(path As String) As String
    Dim extensionIndex As Integer
    Dim FileName As String
    FileName = GetFileName(path)
    extensionIndex = InStrRev(FileName, ".")
    If (extensionIndex <= 0) Then
        GetFileExtension = ""
        Exit Function
    End If
    
    extensionIndex = InStrRev(path, ".")
    GetFileExtension = Mid(path, extensionIndex, Len(path) - extensionIndex + 1)
End Function

''' <summary>
''' Returns parent folder for specified path
''' </summary>
Public Function GetParentFolder(path As String) As String
    Dim fso As New FileSystemObject
    GetParentFolder = fso.GetParentFolderName(path)
    
    ' Trim ending \
    If (Right(GetParentFolder, 1) = "\") Then
        GetParentFolder = Left(GetParentFolder, Len(GetParentFolder) - 1)
    End If
    
    Set fso = Nothing
End Function

''' <summary>
''' Same as Path.Combine in .NET
''' </summary>
Public Function CombinePath(path As String, Name As String) As String
    Dim fso As New FileSystemObject
    CombinePath = fso.BuildPath(path, Name)
    Set fso = Nothing
End Function

''' <summary>
''' Returns location within specified file path of specified search text if file name starts with that text
''' Note: this function is used to fix statements like InStr(StoreResults, "\T") used in the app, since
''' "\T" could well point to folder name start which is wrong
''' </summary>
Public Function InFileNameStartStr(FilePath As String, fileNameStartsWith As String) As Integer
    Dim FileName As String
    FileName = GetFileName(FilePath)
    ' Check whether file name starts with specified text
    If (InStr(FileName, fileNameStartsWith) = 1) Then
        ' We want to return file name position within path, since search text was at the beginning of file name
        InFileNameStartStr = InStrRev(FilePath, FileName)
    Else
        ' Not found
        InFileNameStartStr = 0
    End If
End Function

' Compresses the file specified
Public Function Compress(sourcePath As String) As Boolean
    Dim args() As Variant
    args = Array(FileManager, sourcePath)

    Compress = FileManagerComWrapper.InvokeStaticMethod("IO.Practiceware.Storage.FileManagerExtensions", "Compress", args)
End Function

' Gets the temporary path
Public Function GetTempPath() As String
    Dim args() As Variant
    args = Array(FileManager)

    GetTempPath = FileManagerComWrapper.InvokeStaticMethod("IO.Practiceware.Storage.FileManagerExtensions", "GetTempPath", args)
End Function

' Gets the temporary name for a file
Public Function GetTempPathName() As String
    Dim args() As Variant
    args = Array(FileManager)

    GetTempPathName = FileManagerComWrapper.InvokeStaticMethod("IO.Practiceware.Storage.FileManagerExtensions", "GetTempPathName", args)
End Function

''' <summary>
''' Mark file as temporary file created by VB6 that shouldn't be redirected or stored on Cloud Storage
''' File MUST be removed by VB6 code after user action is complete
''' </summary>
Public Sub EnlistTempLocalFile(path As String)
    Dim args() As Variant, dummyResult As Variant
    args = Array(FileManager, path)

    dummyResult = FileManagerComWrapper.InvokeStaticMethod("IO.Practiceware.Storage.FileManagerExtensions", "ExcludeFileFromRedirection", args)
End Sub

