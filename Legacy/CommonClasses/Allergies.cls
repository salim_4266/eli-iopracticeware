VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAllergies"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public fldCompositeAllergyID As Long
Public fldSource As String
Public fldConceptID As String
Public fldDescription As String
Public fldStatus As String
Public fldTouchDate As String

Public AllergyTblRecordCount As Long
Public AllergyTblEof As Boolean
Public AllergyTblBof As Boolean
Private AllergyTbl As Recordset


Private Sub Class_Initialize()
Set AllergyTbl = CreateAdoRecordset
Call InitAllergy
End Sub

Private Sub Class_Terminate()
Set AllergyTbl = Nothing
End Sub

Private Sub InitAllergy()
fldCompositeAllergyID = 0
fldSource = ""
fldConceptID = ""
fldDescription = ""
fldStatus = ""
fldTouchDate = ""
End Sub

Private Sub LoadAllergy()
fldCompositeAllergyID = AllergyTbl("CompositeAllergyID")

If IsNull(AllergyTbl("Source")) Then
    fldSource = ""
Else
    fldSource = AllergyTbl("Source")
End If

If IsNull(AllergyTbl("ConceptID")) Then
    fldConceptID = ""
Else
    fldConceptID = AllergyTbl("ConceptID")
End If

If IsNull(AllergyTbl("Description")) Then
    fldDescription = ""
Else
    fldDescription = AllergyTbl("Description")
End If

If IsNull(AllergyTbl("Status")) Then
    fldStatus = ""
Else
    fldStatus = AllergyTbl("Status")
End If

If IsNull(AllergyTbl("TouchDate")) Then
    fldTouchDate = ""
Else
    fldTouchDate = AllergyTbl("TouchDate")
    If IsDate(fldTouchDate) Then
        fldTouchDate = Format(fldTouchDate, "mm/dd/yyyy")
    Else
        fldTouchDate = ""
    End If
End If

End Sub

Public Sub TblMove(act As String)
Select Case act
Case "First"
    If Not AllergyTbl.BOF Then AllergyTbl.MoveFirst
Case "Last"
    If Not AllergyTbl.EOF Then AllergyTbl.MoveLast
Case "Next"
    If Not AllergyTbl.EOF Then AllergyTbl.MoveNext
Case "Previous"
    If Not AllergyTbl.BOF Then AllergyTbl.MovePrevious
Case Else
    MsgBox "wrong ADO request"
End Select

If AllergyTbl.EOF Or AllergyTbl.BOF Then
    InitAllergy
Else
    LoadAllergy
End If

AllergyTblEof = AllergyTbl.EOF
AllergyTblBof = AllergyTbl.BOF

End Sub

Public Sub GetAllergyRS(srch As String)
Set AllergyTbl = GetRS("select * from AllergyView " & _
                     " where description like '%" & srch & "%' " & _
                     " order by description")
AllergyTblRecordCount = AllergyTbl.RecordCount

TblMove "First"

End Sub

Public Function GetAllergyDesc(AlgId As Long) As String
Dim RS As Recordset
Dim SQL As String
SQL = " select * from AllergyView " & _
      " where CompositeAllergyID = " & AlgId
Set RS = GetRS(SQL)
If RS.RecordCount = 1 Then
    GetAllergyDesc = RS("description")
End If
End Function

Public Function GetAllergyId(AlgDesc As String) As Long
Dim RS As Recordset
Dim SQL As String
SQL = " select * from AllergyView " & _
      " where CompositeAllergyID = '" & AlgDesc & "'"
Set RS = GetRS(SQL)
If RS.RecordCount = 1 Then
    GetAllergyId = RS("CompositeAllergyID")
End If
End Function



