VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAppointmentTrack"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The AppointmentTrack Object Module
Option Explicit
Public ApptTrackId     As Long
Public AppointmentId   As Long
Public UserId          As Long
Public ComputerName    As String
Public ApptTrackDate   As String
Public ApptTrackTime   As Integer
Public Action          As String
Public Comment         As String
Public ApptDate        As String
Public ApptTime        As Integer
Public ApptDoc         As Long
Public ApptLoc         As Long
Public ApptType        As Long
Private ModuleName     As String
Private TotalRecords   As Long
Private rsApptTrack    As ADODB.Recordset
Private fIsNew         As Boolean

Public Function ApplyApptTrack() As Boolean

    ApplyApptTrack = PutApptTrack

End Function

Private Sub Class_Initialize()

    Set rsApptTrack = CreateAdoRecordset
    InitAppointmentTrack True

End Sub

Private Sub Class_Terminate()

    InitAppointmentTrack False
    Set rsApptTrack = Nothing

End Sub

Public Function DeleteApptTrack() As Boolean

    DeleteApptTrack = PurgeApptTrack

End Function

Public Function FindApptTrack() As Long

    FindApptTrack = GetApptTrackList

End Function

Private Function GetApptTrack() As Boolean

Dim strSQL As String

    On Error GoTo DbErrorHandler
    GetApptTrack = False
    strSQL = "SELECT * FROM AppointmentTrack WHERE ApptTrackId = " & Trim$(Str$(ApptTrackId)) & " "
    Set rsApptTrack = Nothing
    Set rsApptTrack = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = strSQL
    rsApptTrack.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If rsApptTrack.EOF Then
        InitAppointmentTrack True
    Else 'RSAPPTTRACK.EOF = FALSE/0
        InitAppointmentTrack False
        LoadApptTrack
    End If
    GetApptTrack = True

Exit Function

DbErrorHandler:
    ModuleName = "GetApptTrack"
    PinpointPRError ModuleName, Err
    Resume LeaveFast
LeaveFast:

End Function

Private Function GetApptTrackList() As Long

Dim Ref    As String
Dim strSQL As String

    On Error GoTo DbErrorHandler
    Ref = vbNullString
    GetApptTrackList = -1
    If UserId > 0 Or AppointmentId > 0 Then
        strSQL = "Select * FROM AppointmentTrack WHERE "
        If AppointmentId > 0 Then
            strSQL = strSQL & Ref & "AppointmentId = " & Trim$(Str$(AppointmentId)) & " "
            Ref = "And "
        End If
        If LenB(Trim$(Action)) Then
            strSQL = strSQL & Ref & "ApptAction = '" & Trim$(Action) & "' "
            Ref = "And "
        End If
        strSQL = strSQL & "ORDER BY ApptTrackDate, ApptTrackTime "
        Set rsApptTrack = Nothing
        Set rsApptTrack = CreateAdoRecordset
        MyPracticeRepositoryCmd.CommandType = adCmdText
        MyPracticeRepositoryCmd.CommandText = strSQL
        rsApptTrack.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
        If Not (rsApptTrack.EOF) Then
            rsApptTrack.MoveLast
            TotalRecords = rsApptTrack.RecordCount
            GetApptTrackList = rsApptTrack.RecordCount
        End If
    End If

Exit Function

DbErrorHandler:
    ModuleName = "GetApptTrackList"
    PinpointPRError ModuleName, Err
    Resume LeaveFast
LeaveFast:

End Function

Private Function GetJustAddedRecord() As Long

    GetJustAddedRecord = 0
    If GetApptTrackList > 0 Then
        If SelectApptTrack(1) Then
            GetJustAddedRecord = rsApptTrack("ApptTrackId")
        End If
    End If

End Function

Private Sub InitAppointmentTrack(ByVal WriteOn As Boolean)

    fIsNew = WriteOn
    AppointmentId = 0
    UserId = 0
    ComputerName = vbNullString
    ApptTrackDate = vbNullString
    ApptTrackTime = 0
    Action = vbNullString
    Comment = vbNullString
    ApptDate = vbNullString
    ApptTime = 0
    ApptDoc = 0
    ApptLoc = 0
    ApptType = 0

End Sub

Private Sub LoadApptTrack()

    InitAppointmentTrack (False)
    ApptTrackId = rsApptTrack("ApptTrackId")
    If LenB(rsApptTrack("AppointmentId")) Then
        AppointmentId = rsApptTrack("AppointmentId")
    End If
    If LenB(rsApptTrack("UserId")) Then
        UserId = rsApptTrack("UserId")
    End If
    If LenB(rsApptTrack("ComputerName")) Then
        ComputerName = rsApptTrack("ComputerName")
    End If
    If LenB(rsApptTrack("ApptAction")) Then
        Action = rsApptTrack("ApptAction")
    End If
    If LenB(rsApptTrack("ApptTrackDate")) Then
        ApptTrackDate = rsApptTrack("ApptTrackDate")
    End If
    If LenB(rsApptTrack("ApptTrackTime")) Then
        ApptTrackTime = rsApptTrack("ApptTrackTime")
    End If
    If LenB(rsApptTrack("Comment")) Then
        Comment = rsApptTrack("Comment")
    End If
    If LenB(rsApptTrack("ApptDate")) Then
        ApptDate = rsApptTrack("ApptDate")
    End If
    If LenB(rsApptTrack("ApptTime")) Then
        ApptTime = rsApptTrack("ApptTime")
    End If
    If LenB(rsApptTrack("ApptDoc")) Then
        ApptDoc = rsApptTrack("ApptDoc")
    End If
    If LenB(rsApptTrack("ApptLoc")) Then
        ApptLoc = rsApptTrack("ApptLoc")
    End If
    If LenB(rsApptTrack("ApptType")) Then
        ApptType = rsApptTrack("ApptType")
    End If

End Sub

Private Function PurgeApptTrack() As Boolean

    On Error GoTo lPurgeApptTrack_Error
    On Error GoTo DbErrorHandler
    PurgeApptTrack = False
    rsApptTrack.Delete
    PurgeApptTrack = True

Exit Function

DbErrorHandler:
    ModuleName = "PurgeApptTrack"
    PinpointPRError ModuleName, Err
    Resume LeaveFast
LeaveFast:

Exit Function

lPurgeApptTrack_Error:
    LogError "CAppointmentTrack", "PurgeApptTrack", Err, Err.Description

End Function

Private Function PutApptTrack() As Boolean

    On Error GoTo DbErrorHandler
    PutApptTrack = False
    If fIsNew Then
        rsApptTrack.AddNew
    End If
    rsApptTrack("AppointmentId") = AppointmentId
    rsApptTrack("UserId") = UserId
    rsApptTrack("ComputerName") = ComputerName
    rsApptTrack("ApptAction") = Action
    rsApptTrack("ApptTrackDate") = ApptTrackDate
    rsApptTrack("ApptTrackTime") = ApptTrackTime
    rsApptTrack("Comment") = Trim$(Comment)
    rsApptTrack("ApptDate") = ApptDate
    rsApptTrack("ApptTime") = ApptTime
    rsApptTrack("ApptDoc") = ApptDoc
    rsApptTrack("ApptLoc") = ApptLoc
    rsApptTrack("ApptType") = ApptType
    rsApptTrack.Update
    If fIsNew Then
        ApptTrackId = GetJustAddedRecord
    End If
    PutApptTrack = True

Exit Function

DbErrorHandler:
    ModuleName = "PutApptTrack"
    PinpointPRError ModuleName, Err
    Resume LeaveFast
LeaveFast:

End Function

Public Function RetrieveApptTrack() As Boolean

    RetrieveApptTrack = GetApptTrack

End Function

Public Function SelectApptTrack(ByVal ListItem As Integer) As Boolean

    On Error GoTo DbErrorHandler
    SelectApptTrack = False
    If rsApptTrack.EOF Or (ListItem < 1) Or (ListItem > TotalRecords) Then
        Exit Function
    End If
    rsApptTrack.MoveFirst
    rsApptTrack.Move ListItem - 1
    InitAppointmentTrack False
    LoadApptTrack
    SelectApptTrack = True

Exit Function

DbErrorHandler:
    ModuleName = "SelectApptTrack"
    PinpointPRError ModuleName, Err
    Resume LeaveFast
LeaveFast:

End Function
