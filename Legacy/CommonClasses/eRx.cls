VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CeRx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public strPatientID As String
Public strUserId As String
Public strProviderID As String
Public nLocID As String
Public strFilter As String
Public strDestination As String
Public strAllergyFilter As String
Public strAppointmentId As Long

Public Sub ComposeeRx(Optional letNoRx As Boolean)
Dim SQL As String
Dim PatCln As New PatientClinical
Dim RetAppt As New SchedulerAppointment
Dim RetPrcCorp As New PracticeName

    'strFilter
    SQL = " Where PatientId = " & strPatientID & _
          " and ClinicalType = 'A' and Status = 'A' " & _
          " and FindingDetail like 'RX%' "


    PatCln.LoadTbl (SQL)
    
'    If Not letNoRx Then
'        If PatCln.tblRecordCount = 0 Then
'            MsgBox "NO RX"
'            GoTo ExitSub
'        End If
'    End If
    
    Set PatCln = New PatientClinical
    SQL = SQL & " and (substring(ImageDescriptor, 1, 1) in ('!', 'R', 'K') or ImageDescriptor = '')"
    PatCln.LoadTbl (SQL)
    
    Do Until PatCln.tblEOF
        strFilter = strFilter & " " & PatCln.ClinicalId
        PatCln.LoadNext
    Loop
    If Len(strFilter) > 1 Then
        strFilter = Mid(strFilter, 2)
    End If
    If strFilter = "" Then strFilter = 0
    
    'strUserId strProviderID
    RetAppt.AppointmentId = strAppointmentId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        'strUserId = RetAppt.AppointmentResourceId1
        strUserId = UserLogin.iId
        strProviderID = RetAppt.AppointmentResourceId1
        nLocID = RetAppt.AppointmentResourceId2
        'REFACTOR: There's a lot of code here duplicated.  Can probably use GetPracticeId
        If IsNumeric(nLocID) Then
            If nLocID > 1000 Then
                nLocID = nLocID - 1000
            Else
                'If Appointments.ResourceId2 < 1000, then
                'use the PracticeName.PracticeId
                'where PracticeName.PracticeType = 'P'
                'and PracticeName.LocationReference = ''
                Dim oPractice As New PracticeName
                oPractice.PracticeType = "P"
                oPractice.PracticeLocationReference = ""
                If oPractice.getPracticeId Then
                    nLocID = oPractice.PracticeId
                End If
                Set oPractice = Nothing
            End If
        Else
            nLocID = 0
        End If
    End If
    
    'nLocID
    If nLocID = 0 Then
        RetPrcCorp.PracticeType = "P"
        RetPrcCorp.PracticeLocationReference = "BLANK"
    Else
        RetPrcCorp.PracticeType = ""
        RetPrcCorp.PracticeId = nLocID
    End If
    If RetPrcCorp.getPracticeId Then
        nLocID = RetPrcCorp.PracticeId
    Else
        MsgBox "cannot find location"
        GoTo ExitSub
    End If
    
    'strDestination strAllergyFilter
    strDestination = "compose"
    strAllergyFilter = "123"
    '===============================================================================
    ' call dll

    On Error GoTo DbErrorHandler
    Dim objPracticeCode As PracticeCodes
    Dim sqlConString As String
        Dim PracticeToken As String
        sqlConString = ConnectionString
        Set objPracticeCode = New PracticeCodes
        PracticeToken = objPracticeCode.GetPracticeToken("PracticeAuthToken")
        If (strPatientID <> "" And sqlConString <> "") Then
       
        Dim frmErx As New ClinicalIntegration.frmErxHelper
               frmErx.PatientId = strPatientID
               frmErx.PracticeToken = PracticeToken
               'frmErx.sqlConString = ""
               frmErx.UserId = strUserId
               frmErx.AppointmentId = strAppointmentId
               frmErx.DBObject = IdbConnection

        frmErx.ShowDialog
               frmErx.Close
           frmErx.Dispose
                

        End If


    'Dim frm As New EPLib.frmEPSender
    'Dim EPLibHelper As New EPLib.Helper
    'EPLibHelper.PatientId = strPatientID
    'EPLibHelper.ProviderId = strProviderID
    'EPLibHelper.UserId = strUserId
    'EPLibHelper.LocId = (nLocID)
    'EPLibHelper.Filter = strFilter
    'EPLibHelper.AllergyFilter = strAllergyFilter
    'EPLibHelper.Destination = strDestination
    'EPLibHelper.DbObject = IdbConnection
    'frm.ShowDialog()


    
ExitSub:
'    Set PatCln = Nothing
'    Set RetAppt = Nothing
'    Set RetPrcCorp = Nothing
'    Set frm = Nothing
'    Set EPLibHelper = Nothing
Exit Sub

DbErrorHandler:
    Call PinpointPRError("ComposeeRx", Err)
    Resume LeaveFast
LeaveFast:
End Sub

Public Function StatusSummary() As Boolean
'Dim sqlCon As String
'    On Error GoTo lStatusSummary_Error
'        Dim PracticeToken As String
'        Dim objPracticeCode As PracticeCodes
'        sqlCon = ConnectionString
'        Set objPracticeCode = New PracticeCodes
'        PracticeToken = objPracticeCode.GetPracticeToken("PracticeAuthToken")
'        If (sqlCon <> "") Then
'            Dim oStatus As New EPLib.ErxStatus
'            oStatus.sqlConString = sqlCon
'            oStatus.Context = "report"
'            oStatus.ShowDialog
'            oStatus.Close
'            oStatus.Dispose
'        End If
Exit Function
lStatusSummary_Error:
    LogError "cERX", "StatusSummary", Err, Err.Description
End Function

Public Function ClinicalMessagingSummary() As Boolean
'    Dim oStatus As New EPLib.frmStatus
'    Dim oEPLibHelper As New EPLib.Helper
'    On Error GoTo lStatusSummary_Error
'
'    getPracticeId
'    oEPLibHelper.UserId = strUserId
'    If GeteRxRole(val(strUserId)) = "LP" Then
'        oEPLibHelper.ProviderId = strUserId
'    End If
'    oEPLibHelper.LocId = nLocID
'    oEPLibHelper.Destination = "clinicalMessaging-inbox"
'
'    oEPLibHelper.DbObject = IdbConnection
'
'    oStatus.ShowDialog
'    oStatus.Close
'    oStatus.Dispose

    Exit Function

lStatusSummary_Error:

    LogError "cERX", "ClinicalMessagingSummary", Err, Err.Description
End Function

Public Function RenewalRequests() As Boolean
'    Dim sqlCon As String
'    On Error GoTo lRequests_Error
'        Dim PracticeToken As String
'        Dim objPracticeCode As PracticeCodes
'        sqlCon = ConnectionString
'        Set objPracticeCode = New PracticeCodes
'        PracticeToken = objPracticeCode.GetPracticeToken("PracticeAuthToken")
'        Dim oStatus As New EPLib.ErxStatus
'            oStatus.sqlConString = sqlCon
'            oStatus.Context = "message"
'            oStatus.ShowDialog
'            oStatus.Close
'            oStatus.Dispose
Exit Function


'    Dim oEPLibHelper As New EPLib.Helper
'    Dim oRenewals As New frmRenewals
'
'    On Error GoTo lRequests_Error
'
'    getPracticeId
'    'oEPLibHelper.PatientId = strPatientID
'    'oEPLibHelper.ProviderID = strProviderID
'    'oEPLibHelper.LocId = nLocID
'    'oEPLibHelper.Destination = strDestination
'    oEPLibHelper.DbObject = IdbConnection
'    oRenewals.ShowDialog
'    oRenewals.Close
'    oRenewals.Dispose

    Exit Function

lRequests_Error:

    LogError "cERX", "Requests", Err, Err.Description
End Function

Private Function getPracticeId() As Boolean
Dim oPractice As PracticeName
    On Error GoTo lGetPracticeId_Error

    If nLocID > 1000 Then
        nLocID = nLocID - 1000
    ElseIf nLocID = 0 Then
        Set oPractice = New PracticeName
        oPractice.PracticeType = "P"
        oPractice.PracticeLocationReference = ""
        If oPractice.getPracticeId Then
            nLocID = oPractice.PracticeId
        End If
        Set oPractice = Nothing
    End If

    Exit Function

lGetPracticeId_Error:

    LogError "cERX", "GetPracticeId", Err, Err.Description
End Function

Public Function NewRenewalsExist() As Boolean
Dim sSQL As String
Dim rsRenewalRequests As ADODB.Recordset
Set rsRenewalRequests = CreateAdoRecordset

    On Error GoTo lNewRenewalsExist_Error

    sSQL = "select * From Patient_EPrescription_RenewalRequest Where AcceptDenyStatus Is Null"
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = sSQL
    rsRenewalRequests.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If rsRenewalRequests.EOF Then
        NewRenewalsExist = False
    Else
        NewRenewalsExist = True
    End If
    Set rsRenewalRequests = Nothing

    Exit Function

lNewRenewalsExist_Error:

    LogError "cERX", "NewRenewalsExist", Err, Err.Description

End Function

Public Function NewStatusExist() As Boolean
Dim sSQL As String
Dim rsStatus As ADODB.Recordset
Set rsStatus = CreateAdoRecordset
    On Error GoTo lNewStatusExist_Error

    sSQL = "select [RowCount] from EPrescription_Credentials"
    MyPracticeRepositoryCmd.CommandType = adCmdText
    MyPracticeRepositoryCmd.CommandText = sSQL
    rsStatus.Open MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1
    If rsStatus.EOF Then
        NewStatusExist = False
    Else
        If rsStatus("RowCount") <> "" Then
            If rsStatus("RowCount") > 0 Then
                NewStatusExist = True
            End If
        End If
    End If
    Set rsStatus = Nothing

    Exit Function

lNewStatusExist_Error:

    LogError "cERX", "NewStatusExist", Err, Err.Description
    
End Function

Private Function GeteRxRole(ByVal iResourceId As Long) As String
Dim oResource As New SchedulerResource
    oResource.ResourceId = iResourceId
    oResource.RetrieveSchedulerResource
    GeteRxRole = oResource.ResourceEndTime7
    Set oResource = Nothing
End Function


