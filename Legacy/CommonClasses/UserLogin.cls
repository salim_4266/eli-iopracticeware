VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUserLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public iId            As Long
Public sType          As String
Public sNameLong      As String
Public sPermissions   As String
Public sNameShort     As String
Public fTasksEnabled  As Boolean

Public Function AuthenticateUser(ByVal sUserID As String) As Boolean
    Dim Resource As SchedulerResource

    On Error GoTo lAuthenticateUser_Error
    
    If sUserID > -1 Then 'Valid Login
        Set Resource = New SchedulerResource
        Resource.ResourceId = sUserID
    
        AuthenticateUser = Resource.RetrieveSchedulerResource
        If AuthenticateUser Then
            With Resource
                iId = .ResourceId
                sType = .ResourceType
                sNameLong = Trim$(.ResourceDescription)
                sPermissions = .ResourcePermissions
                sNameShort = Trim$(.ResourceName)
            End With 'Resource
            Call SetTaskMonitor
        End If
       
        Set Resource = Nothing
    Else
        AuthenticateUser = False
    End If
Exit Function

lAuthenticateUser_Error:
    LogError "CUserLogin", "AuthenticateUser", Err, Err.Description
End Function

Public Function AuthenticateUserPID(ByVal sPID As String) As Boolean
    Dim Resource As SchedulerResource

    On Error GoTo lAuthenticateUserPID_Error
    
    If sPID <> "" Then 'Valid Login
        Set Resource = New SchedulerResource
       Resource.ResourcePid = sPID
    
        AuthenticateUserPID = Resource.ResourceLoginVerification
        If AuthenticateUserPID Then
            With Resource
                iId = .ResourceId
                sType = .ResourceType
                sNameLong = Trim$(.ResourceDescription)
                sPermissions = .ResourcePermissions
                sNameShort = Trim$(.ResourceName)
            End With 'Resource
        End If
       
        Set Resource = Nothing
    Else
        AuthenticateUserPID = False
    End If
Exit Function

lAuthenticateUserPID_Error:
    LogError "CUserLogin", "AuthenticateUserPID", Err, Err.Description
End Function

Private Sub Class_Initialize()
    InitUserLogin
End Sub

Public Function HasPermission(epPermission As EPermissions) As Boolean

    HasPermission = False
    If Len(sPermissions) >= epPermission Then
        If Mid$(sPermissions, epPermission, 1) = "1" Then
            HasPermission = True
        End If
    End If

End Function

Private Sub InitUserLogin()
    
    sType = vbNullString
    sNameLong = vbNullString
    sPermissions = vbNullString
    sNameShort = vbNullString
    ActiveAppointmentId = 0
    ActivePatientId = 0
    
    NullUserContext
   
    iId = 0
End Sub

Public Function LogoffUser() As Boolean
    ' Take care of locally cached files
    Dim comWrapper As New comWrapper
    Call comWrapper.Uninitialize
    ' Reinit user
    Call NullTaskMonitor
    InitUserLogin
End Function

Private Sub NullTaskMonitor()
     On Error GoTo lNullTaskMonitor_Error
    
    If CheckConfigCollection("TasksEnabled") = "T" Then
        fTasksEnabled = True
    Else
        fTasksEnabled = False
    End If
    
    'Stop Tasks monitor service
    If fTasksEnabled Then
        Dim TaskMonitorComWrapper As New comWrapper
        TaskMonitorComWrapper.Create TaskMonitorType, emptyArgs
        TaskMonitorComWrapper.InvokeMethod "Stop", emptyArgs
    End If
    Exit Sub
    
lNullTaskMonitor_Error:

    LogError "CUserLogin", "NullTaskMonitor", Err, Err.Description
End Sub

Private Sub NullUserContext()
    On Error GoTo lNullUserContext_Error

    ' Null out UserContext and unsubscribe for alerts
     
    If IsLoggedIn Then
        Dim UserContextComWrapper As New comWrapper
        UserContextComWrapper.Create UserContextType, emptyArgs
        UserContextComWrapper.InvokeMethod "Logout", emptyArgs
    End If
    
    Exit Sub
    
lNullUserContext_Error:

    LogError "CUserLogin", "NullUserContext", Err, Err.Description
End Sub

Private Function Login(sPID As String) As Long
    On Error GoTo lLogin_Error
    
    Login = -1
    Dim resourceArgs(0) As Variant
    resourceArgs(0) = sPID
    Dim UserContextComWrapper As New comWrapper
    UserContextComWrapper.Create UserContextType, emptyArgs
    Login = UserContextComWrapper.InvokeMethod("Login", resourceArgs)
    
    Exit Function
lLogin_Error:

    LogError "NetLogin", "Login", Err, Err.Description
End Function

 Public Sub SetTaskMonitor()
    On Error GoTo lSetTaskMonitor_Error
        
    If CheckConfigCollection("TasksEnabled") = "T" Then
        fTasksEnabled = True
    Else
        fTasksEnabled = False
    End If
    If fTasksEnabled Then
        'Set UserContext and subscribe to alerts
        Dim TaskMonitorComWrapper As New comWrapper
        TaskMonitorComWrapper.Create TaskMonitorType, emptyArgs
        TaskMonitorComWrapper.InvokeMethod "Start", emptyArgs
    End If
        
    Exit Sub

lSetTaskMonitor_Error:

    LogError "CUserLogin", "SetTaskMonitor", Err, Err.Description
End Sub

Public Property Get IsLoggedIn() As Boolean
    IsLoggedIn = iId > 0
End Property

Public Function IsLoginOptical() As Boolean
Select Case sType
Case "Q", "U"
    IsLoginOptical = True
Case Else
    IsLoginOptical = False
End Select
End Function
Public Function DisplayLoginScreen(Optional ByVal FullScreen As Boolean = False, Optional ByVal LogoutUser As Boolean = True) As Boolean

On Error GoTo lDisplayLoginScreen_Error

DisplayLoginScreen = False
Dim LoginId As Long

Dim loginArgs(0) As Variant
    loginArgs(0) = FullScreen

    Dim LoginComWrapper As New comWrapper
Call LoginComWrapper.Create(LoginViewManagerType, emptyArgs)
LoginId = LoginComWrapper.InvokeMethod("ShowLoginScreen", loginArgs)
If LoginId = -1 Then
    If LogoutUser Then
        Exit Function
    Else
        DisplayLoginScreen = True
        Exit Function
    End If
End If
If LogoutUser Then
    Call EstablishDirectory
    Call dbPinpointOpen
ElseIf iId = LoginId Then
        DisplayLoginScreen = True
        Exit Function
End If
If AuthenticateUser(LoginId) Then
    Call GetSoftwareLocation
    DisplayLoginScreen = True
End If
Exit Function

lDisplayLoginScreen_Error:
    LogError "CUserLogin", "DisplayLoginScreen", Err, Err.Description
End Function
