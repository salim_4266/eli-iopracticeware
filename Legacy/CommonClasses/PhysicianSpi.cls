VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPhysicianSpi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public PhysicianSpiId As Long
Public PhysicianId As Long
Public LocationId As Long
Public Spi As String
Public Phone As String
Public PhoneType As Long

Private ModuleName As String
Private PhysicTbl As ADODB.Recordset

Private Sub Class_Terminate()
Set PhysicTbl = Nothing
End Sub

Public Function GetPhysicList() As Recordset

On Error GoTo DbErrorHandler
Dim SQL As String
SQL = "SELECT * FROM PhysicianSpi where PhysicianId = " & PhysicianId
Set GetPhysicList = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetPhysicList"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function PutPhysic() As Boolean
On Error GoTo DbErrorHandler

If PhysicTbl Is Nothing Then
    Set PhysicTbl = GetPhysicList
End If
PhysicTbl.AddNew

PhysicTbl("PhysicianId") = PhysicianId
PhysicTbl("LocationId") = LocationId
PhysicTbl("Spi") = Spi
PhysicTbl("Phone") = Phone
PhysicTbl("PhoneType") = PhoneType

PhysicTbl.Update
PutPhysic = True

Exit Function
    
DbErrorHandler:
    ModuleName = "PutPhysic"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function KillPhysic() As Boolean
Dim OrderString As String
OrderString = "DELETE FROM PhysicianSpi where PhysicianId = " & PhysicianId
Set PhysicTbl = GetRS(OrderString)
Set PhysicTbl = Nothing
KillPhysic = True
Exit Function

DbErrorHandler:
    ModuleName = "KillPhysic"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

