VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDrug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public DrugId As Long
Public Drugcode As String
Public DisplayName As String
Public Family1 As String
Public Family2 As String
Public Focus As Boolean

Private ModuleName As String
Private DrugTbl As ADODB.Recordset

Private Sub Class_Initialize()
Set DrugTbl = CreateAdoRecordset
Call InitDrug
End Sub

Private Sub Class_Terminate()
Set DrugTbl = Nothing
End Sub

Private Sub InitDrug()
DrugId = 0
Drugcode = ""
DisplayName = ""
Family1 = ""
Family2 = ""
Focus = False
End Sub

Private Sub LoadDrug()
Focus = False

DrugId = DrugTbl("DrugId")
If IsNull(DrugTbl("Drugcode")) Then
    Drugcode = ""
Else
    Drugcode = DrugTbl("Drugcode")
End If
If IsNull(DrugTbl("DisplayName")) Then
    DisplayName = ""
Else
    DisplayName = DrugTbl("DisplayName")
End If
If IsNull(DrugTbl("Family1")) Then
    Family1 = ""
Else
    Family1 = DrugTbl("Family1")
End If
If IsNull(DrugTbl("Family2")) Then
    Family2 = ""
Else
    Family2 = DrugTbl("Family2")
End If
If IsNull(DrugTbl("Focus")) Then
    Focus = False
Else
    Focus = DrugTbl("Focus")
End If

End Sub

Public Function SelectDrug(IType As Integer) As Boolean
If GetDrug(IType) Then
    LoadDrug
    SelectDrug = True
End If
End Function


Private Function GetDrug(IType As Integer) As Boolean

On Error GoTo DbErrorHandler
Dim SQL As String
If IType = 0 Then
    SQL = "SELECT * FROM Drug WHERE DrugId = " & DrugId
Else
    SQL = "SELECT * FROM Drug WHERE DisplayName = '" & DisplayName & "' "
End If
Set DrugTbl = GetRS(SQL)
If DrugTbl.RecordCount > 0 Then
    GetDrug = True
End If
Exit Function
DbErrorHandler:
    ModuleName = "GetDrug"
    GetDrug = False
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetDrugList() As Recordset

On Error GoTo DbErrorHandler
Dim SQL As String
SQL = "SELECT * FROM Drug order by displayname"
Set GetDrugList = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugList"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function PutDrug() As Boolean
On Error GoTo DbErrorHandler
If DrugId > 0 Then
    If Not GetDrug(0) Then
        DrugTbl.AddNew
    End If
Else
    If Not GetDrug(1) Then
        DrugTbl.AddNew
    End If
End If

DrugTbl("Drugcode") = Drugcode
DrugTbl("DisplayName") = DisplayName
DrugTbl("Family1") = Family1
DrugTbl("Family2") = Family2
DrugTbl("Focus") = Focus

DrugTbl.Update
PutDrug = True
Exit Function

DbErrorHandler:
    ModuleName = "PutDrug"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function KillDrug() As Boolean
Dim OrderString As String
OrderString = "DELETE FROM drug where DisplayName = '" & DisplayName & "' "

MyPracticeRepositoryCmd.CommandType = adCmdText
MyPracticeRepositoryCmd.CommandText = OrderString
Set DrugTbl = Nothing
Set DrugTbl = CreateAdoRecordset

Call DrugTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, -1)
KillDrug = True
Exit Function

DbErrorHandler:
    ModuleName = "KillDrug"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetDrugListAll() As Recordset
Dim SQL As String
On Error GoTo DbErrorHandler
Call StripCharacters(DisplayName, "'")

SQL = " SELECT distinct MED_DESC as displayname ,'T' as Drugcode " & _
      " From FDBView"
If Not DisplayName = "" Then
    SQL = SQL & " WHERE MED_DESC like '" & DisplayName & "%' "
End If
SQL = SQL & " order by 1 "

Set GetDrugListAll = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugListAll"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetDrugFormsAll() As Recordset

On Error GoTo DbErrorHandler
Dim SQL As String

If DisplayName = "" Then
    Dim rsDrug As Recordset
    SQL = "select displayname from drug where drugid = " & DrugId
    Set rsDrug = GetRS(SQL)
    If Not rsDrug.EOF Then
        DisplayName = rsDrug(0)
    End If
End If

'MED_STRENGTH_UOM is not null and

SQL = "SELECT distinct MED_DOSAGE FROM FDBView " & _
      " WHERE MED_DESC LIKE '" & DisplayName & "%' " & _
      " order by 1"
Set GetDrugFormsAll = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugFormsAll"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function GetDrugStrAll(Form As String) As Recordset

On Error GoTo DbErrorHandler
Dim SQL As String
SQL = "SELECT distinct MED_STRENGTH_UOM, MED_DOSAGE " & _
      " FROM FDBView WHERE MED_DESC LIKE '" & DisplayName & "%' " & _
      " and MED_STRENGTH_UOM is not null  "

If Not Form = "" Then
    SQL = SQL & " and MED_DOSAGE = '" & Form & "' "
End If
SQL = SQL & " order by 2, 1"

Set GetDrugStrAll = GetRS(SQL)
Exit Function
DbErrorHandler:
    ModuleName = "GetDrugStrAll"
    Call PinpointPRError(ModuleName, Err)
    Resume LeaveFast
LeaveFast:
End Function

Public Function IsDrugListed(sMed_desc As String) As Boolean
Dim RS As Recordset
Dim SQL As String
    On Error GoTo lisDrugListed_Error

    SQL = "SELECT * FROM FDBView WHERE MED_DESC = '" & sMed_desc & "' "
    Set RS = GetRS(SQL)
    IsDrugListed = (RS.RecordCount > 0)

    Exit Function

lisDrugListed_Error:

    LogError "cDrug", "isDrugListed", Err, Err.Description

End Function
