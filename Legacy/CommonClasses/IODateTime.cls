VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIODateTime"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private mdtDateTime   As Date


'Use DateValue or TimeValue to isolate part of the dt and then DateSerial or TimeSerial to add onto it.
Private Sub Class_Initialize()

    mdtDateTime = 0
End Sub


Public Function GetDisplayDate(ByVal fLongFormat As Boolean) As String

'This replaces FormatTodaysDate

    If fLongFormat Then
        GetDisplayDate = Format$(mdtDateTime, "dddd, mmmm dd, yyyy")
    Else
        GetDisplayDate = Format$(mdtDateTime, "mm/dd/yyyy")
    End If
End Function


Public Function GetDisplayTime() As String

'This replaces FormatTimeNow

    GetDisplayTime = Format$(mdtDateTime, "h:nn AM/PM")
End Function


Public Function GetIODate() As String

    GetIODate = Format$(mdtDateTime, "yyyymmdd")
End Function


Public Function GetIOTime() As Long

    GetIOTime = Hour(mdtDateTime) * 60 + Minute(mdtDateTime)
End Function


Public Property Get MyDateTime() As Date

    MyDateTime = mdtDateTime
End Property


Public Property Let MyDateTime(ByVal dtMyDateTime As Date)

    mdtDateTime = dtMyDateTime
End Property


Public Function SetDateFromDisplay(ByVal sDisplayDate As String) As Boolean

    If IsDate(sDisplayDate) Then
        mdtDateTime = DateValue(sDisplayDate) + TimeSerial(Hour(mdtDateTime), Minute(mdtDateTime), Second(mdtDateTime))
    End If
End Function


Public Function SetDateFromIO(ByVal sIODate As String) As Boolean

Dim sTemp As String

    sTemp = Mid$(sIODate, 5, 2) & "/" & Mid$(sIODate, 7, 2) & "/" & Mid$(sIODate, 1, 4)
    If IsDate(sTemp) Then
        mdtDateTime = DateValue(sTemp) + TimeSerial(Hour(mdtDateTime), Minute(mdtDateTime), Second(mdtDateTime))
    End If
End Function


Public Function SetTimeFromDisplay(ByVal sDisplayTime As String) As Boolean

    If IsDate(sDisplayTime) Then
        mdtDateTime = DateSerial(Year(mdtDateTime), Month(mdtDateTime), Day(mdtDateTime)) + TimeValue(sDisplayTime)
    End If
End Function


Public Function SetTimeFromIO(ByVal iIOTime As Long) As Boolean

    If iIOTime < 1440 Then
        If iIOTime > 0 Then
            mdtDateTime = DateSerial(Year(mdtDateTime), Month(mdtDateTime), Day(mdtDateTime)) + TimeSerial(Fix(iIOTime / 60), iIOTime Mod 60, 0)
        End If
    End If
End Function

Public Function SetDateUnknown(sUnknown As String) As Boolean

If LenB(sUnknown) = 0 Then
    mdtDateTime = Now
    SetDateUnknown = True
ElseIf IsDate(sUnknown) Then
    SetDateUnknown = SetDateFromDisplay(Format(sUnknown, "mm/dd/yyyy"))
Else
    SetDateUnknown = False
    If Len(sUnknown) = 8 Then
        If IsNumeric(sUnknown) Then
            SetDateUnknown = SetDateFromIO(sUnknown)
        End If
    End If
End If
End Function
