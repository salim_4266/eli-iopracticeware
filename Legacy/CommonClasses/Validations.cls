VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Validations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function AppCancel(ApptId As Long) As Boolean
Dim ApplSch As ApplicationScheduler
Dim ApplList As ApplicationAIList
Dim Sts As String
Dim ClnRecOn As Boolean
Dim LDate As String, TDate As String
    
AppCancel = False

Set ApplSch = New ApplicationScheduler
Sts = ApplSch.GetActivityRecordStatus(ApptId)
Set ApplSch = Nothing
Set ApplList = New ApplicationAIList
ClnRecOn = ApplList.ApplIsClinicalRecordsPresent(ApptId)
Set ApplList = Nothing
If ((Sts = "H") And (ClnRecOn)) Or (Sts = "U") Or (Sts = "E") Then
    frmEventMsgs.Header = ""
    If (Sts = "H") And (ClnRecOn) Then
        frmEventMsgs.Header = "Appointments cannot be cancelled when a patient has clinical records."
    ElseIf (Sts = "U") Then
        frmEventMsgs.Header = "This appointment cannot be cancelled because it is in the process of being checked out. To cancel, exit from CheckOut then proceed to Patient Locator."
    ElseIf (Sts = "E") Then
        frmEventMsgs.Header = "Appointments cannot be cancelled when a patient is in an Exam Room."
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    If (frmEventMsgs.Header <> "") Then
        frmEventMsgs.Show 1
        Exit Function
    End If
End If

AppCancel = True

End Function
